<?php
$title = "Payment Transection audit Report";
include ('header.php');
include ('root_menu.php');
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Payment Transaction Audit Report <span style="float:right;" id="do-optimization">Perform Optimization</span></div>
            <div class="panel-body">

                <form name="frmtransactionreport" id="frmtransactionreport" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <label for="transaction-type"> Transaction Type:<span class="star">*</span></label>
                                    <select name="transaction_type" id="transaction-type">
                                        <option value="">-- Select --</option>
                                        <option value="LearnerFeePayment">Learner Fee Payments</option>
                                        <option value="ReexamPayment">Reexam Transactions</option>
                                        <option value="Correction Certificate">Correction Transactions</option>
                                        <option value="Duplicate Certificate">Duplicate Transactions</option>
                                        <option value="EOI Fee Payment">EOI Fee Payment</option>
                                        <option value="NcrFeePayment">NCR Fee Payment</option>
                                        <option value="NameAddressFeePayment">Name Address Fee Payment</option>
                                    </select>
                             </div> 
                             
                             <div class="col-md-4 form-group">     
                                <label for="dateFrom"> Date Range:<span class="star">*</span></label>
                                    <input type="text" class="form-control" name="dateFrom" id="dateFrom" readonly="true" placeholder="DD-MM-YYYY"> 
                             </div> 
                             
                             <div class="col-md-4 form-group">     
                                <label for="dateTo"> To:<span class="star">*</span></label>
                                    <input type="text" class="form-control" name="dateTo" id="dateTo" readonly="true" placeholder="DD-MM-YYYY"> 
                             </div> 
                             <div class="col-md-4 form-group"><br />     
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>
                             </div>
                        </div>
                        <p>&nbsp;</p>
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        

                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

		$("#btnSubmit").click(function () {
            showData();
        });

        $("#do-optimization").click(function () {
            doOptimization();
        });
		
		function showData() {
            $('#response').empty();
            $('#response').html("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var forminput = $("#frmtransactionreport").serialize();
            var url = "common/cftransectionauditreport.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=GETDATA&" + forminput; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    //alert(data);
                    $('#response').empty();

                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }

        function doOptimization() {
            $('#response').empty();
            $('#response').html("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Performing Optimization, please wait.....</span></p>");
            var forminput = $("#frmtransactionreport").serialize();
            var url = "common/cftransectionauditreport.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=Optimization&" + forminput; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {

                    $('#response').html(data);
                }
            });
        }

		function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

        $('#dateFrom').datepicker({
            format: "dd-mm-yyyy",
            orientation: "bottom auto",
            todayHighlight: true,
            //autoclose: true
        });

        $('#dateTo').datepicker({
            format: "dd-mm-yyyy",
            orientation: "bottom auto",
            todayHighlight: true,
            //autoclose: true
        });

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmmediumwisecount.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
    #do-optimization:hover {
        font-weight: bold;
        text-decoration: underline;
        cursor: pointer;
    }
</style>
</html>
