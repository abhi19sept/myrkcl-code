<?php
$title="Advance Course Admission";
include ('header.php'); 
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '7'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}

?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:1500px !important;">	
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
           <div class="panel-heading">Red Hat Advance Course Admission
                <div style="float:right">
                    Total Admission Count: <div style="display: inline" id="txtintakeavailable"> </div>
                </div>
            </div>
                <div class="panel-body">
				 <form name="frmredhatadmission" id="frmredhatadmission" class="form-inline" role="form" enctype="multipart/form-data">
										
					<div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>
							
					<div id="errorBox"></div>					
							 <div class="col-sm-4 form-group">     
								<label for="learnercode">Learner Name:<span class="star">*</span></label>
								<input type="text" class="form-control" maxlength="100" name="txtlname" id="txtlname" 
									style="text-transform:uppercase" onkeypress="javascript:return validLearnerName(event);" 
									placeholder="Learner Name" required="true">
								<input type="hidden" class="form-control"  name="txtLearnercode" id="txtLearnercode"/>
							</div>

							<div class="col-sm-4 form-group"> 
								<label for="fname">Father/Husband Name:<span class="star">*</span></label>
								<input type="text" class="form-control" style="text-transform:uppercase" maxlength="100" name="txtfname" 
								id="txtfname" onkeypress="javascript:return validFatherName(event);" 
								placeholder="Father Name" required="true">     
							</div>

							<div class="col-sm-4 form-group">     
								<label for="dob">Date of Birth:<span class="star">*</span></label>								
								<input type="text" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  
									placeholder="DD-MM-YYYY" required="true">
							</div>
							
							<div class="col-sm-4 form-group"> 
								<label for="bankaccount">Mobile No:<span class="star">*</span></label>
								<input type="text" class="form-control"  name="txtmobile"  id="txtmobile" placeholder="Mobile No" onkeypress="javascript:return allownumbers(event);" maxlength="10" required="true"/>
							</div>
					</div>
					                  
                 
					<div class="container" id="submit">
						<input type="button" name="btnotpsent" id="btnotpsent" class="btn btn-primary" value="Send OTP"/> 
						<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"
						style="display:none;"/>    
					</div>
					<div class="container">						
							<div id="userfields">
								
							</div>
							<div id="responses"></div>
						</div>
						
						<div class="container" style="margin-top:10px;margin-left: 20px;">	
                           <input type="button" name="btnotpvalidate" id="btnotpvalidate" class="btn btn-primary" value="Validate OTP"
						   style="display:none;"/>
						
                            <input type="button" name="btnresentotp" id="btnresentotp" class="btn btn-primary" value="Resend OTP"
							style="display:none;"/>    
                       </div>
				</div>
            </div>   
        </div>
    </form>
</div>
   
	</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
<style>
    .error{
        color:#F00;
    }
</style>
<script type="text/javascript">
                            $('#dob').datepicker({
                                format: "dd-mm-yyyy",
                                orientation: "bottom auto",
                                todayHighlight: true
                            });
</script>
<script type="text/javascript">
function validFatherName(e){
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("^[ A-Za-z]*$");

            if (key === 8 || key === 0 || key === 32) {
                keychar = "a";
            }
            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
function validLearnerName(e){
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("^[ A-Za-z]*$");

            if (key === 8 || key === 0 || key === 32) {
                keychar = "a";
            }
            return reg.test(keychar);
        }
</script>
<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>		
<script type="text/javascript">
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
			
			function availableadmissioncount() {
				$('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
               
                   $.ajax({
					type: "post",
					url: "common/cfRedHatAdmission.php",
					data: "action=FillAdmissionCount",
					success: function (data) {
						$('#response').empty();
						 if(data=='10000'){
							alert("Subscription Not Available");
							window.location.href = "frmadvancecoursemessage.php";
							$("#submit").hide();
						}
						else if(data=='00'){
							alert("You have not buy RHLS subscription.");
							window.location.href = "frmredhatmessage.php";
							$("#submit").hide();
						}
						else {
							document.getElementById('txtintakeavailable').innerHTML = data;
							$("#submit").show();
						}
						
					}
				});
            }
            availableadmissioncount();
			
			function GenerateLearnercode()
        {
            $.ajax({
                type: "post",
                url: "common/cfRedHatAdmission.php",
                data: "action=GENERATELEARNERCODE",
                success: function (data) {
					//alert(data);
                    txtLearnercode.value = data;
                }
            });
        }
        GenerateLearnercode();
        
		$("#btnotpsent").click(function (){
			
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
                    type: "post",
                    url: "common/cfRedHatAdmission.php",
                    data: "action=SendSMS&mobileno=" + txtmobile.value + "",
                    success: function (data) {
						$('#responses').empty();
						if(data=="empty"){
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Please enter mobile no." + "</span></p>");
						}
						else{
							data = $.parseJSON(data);
							$('#userfields').html(data.frm);
							$("#btnotpsent").hide(1000);
							$("#btnotpvalidate").show(1000);
							$("#btnresentotp").show(1000);
						}
												
					}
                });
			});
			
		   $("#btnotpvalidate").click(function () {
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				
				var url = "common/cfRedHatAdmission.php";
				var data = "action=VerifyOTP&otp=" + txtOTP.value +  "&oid=" + oid.value + "";
				$.ajax({
					type: "POST",
					url: url,
					data: data,
					success: function (data) {
						$('#responses').empty();						
						$("#responses").css({"margin-top": "20px"});
						if (data==0) 
						{
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Invalid OTP. Please try again." + "</span></p>");
						} 
						else if(data=='OTPBLANCK'){
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Please enter valid OTP." + "</span></p>");
						}
						else if(data=='EXPIRE'){
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Your OTP has Expired. Please try again." + "</span></p>");
						}
						else if(data=='NORECORD'){
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Your OTP has Expired or Invalid. Please try again." + "</span></p>");
						}
						else {							
							data = $.parseJSON(data);
							$('#txtmobile').attr('readonly', true);
							$("#btnSubmit").show(1000);
							$("#userfields").hide(1000);
							$("#btnotpvalidate").hide(1000);
							$("#btnresentotp").hide(1000);							
						}
					}
				});
		});
		
		$("#btnresentotp").click(function () {                
			$('#responses').empty();
			$('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				var url = "common/cfRedHatAdmission.php"; // the script where you handle the form input.
				var data = "action=RESENDOTP&mobileno=" + txtmobile.value + ""; // serializes the form's elements.
                    $.ajax({
						type: "POST",
						url: url,
						data: data,
						success: function (data) {
							$('#responses').empty();
							if (data != 0) {
								data = $.parseJSON(data);
								$('#userfields').html(data.frm);
								$("#btnotpvalidate").show(1000);
								$("#btnresentotp").show(1000);								 
							} else {
								$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Invalid Mobile Number. Please try again." + "</span></p>");
							}
						}
					});                
    	       });
			   
			   
		$("#btnSubmit").click(function () {
            if ($("#frmredhatadmission").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfRedHatAdmission.php"; // the script where you handle the form input.
                
                var dob = $('#dob').val();                
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&lname=" + txtlname.value + "&learnercode=" + txtLearnercode.value + "&parent=" + txtfname.value +
                            "&dob=" + dob + "&mobile=" + txtmobile.value + ""; // serializes the form's elements.
                    //alert (data);
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmredhatadmission.php';
                            }, 3000);
                        }
						else if(data=='something'){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Something went wrong. Please try again.</span></div>");
						}
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                        }
                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });

        });

    </script>
</html>