<?php
$title = "DD Payment Mode";
include ('header.php');
include ('root_menu.php');

    echo "<script>var UserCode='" . $_SESSION['User_Code'] . "'</script>";
    echo "<script>var LoginID='" . $_SESSION['User_LoginId'] . "'</script>";
    //echo "<script>var CenterCode=" . $_SESSION['User_Code'] . "</script>";

    echo "<script>var PayType='Correction Fee Payment'</script>";
    //echo "<script>var email='" . $_SESSION['User_EmailId'] . "'</script>";
    echo "<script>var Mode='Update'</script>";
// print_r($_POST);
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258') {
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:auto !important;"> 
	<div class="container">    
		<div class="panel panel-primary" style="margin-top:36px;">
			<div class="panel-heading">Correction Payment Transection Details</div>
				<div class="panel-body">
            <!-- <div class="jumbotron"> -->
					<form name="DDForm" id="DDForm" class="form-inline" role="form" enctype="multipart/form-data">  
						<div class="container">
							<div class="container">
								<div id="response"></div>
							</div>        
							<div id="errorBox"></div>						
							<div class="col-sm-4 form-group">     
								<label for="correction">Select Correction Name:</label>
								<select id="ddlCorrection" name="ddlCorrection" class="form-control">
								
								</select>
							</div> 
						
							<div class="col-sm-4 form-group">     
								  <label for="learnercode">Learner Code:<span class="star">*</span></label>
								  <input type="text" class="form-control" maxlength="50" name="txtLCode" id="txtLCode"  onkeypress="javascript:return allownumbers(event);" placeholder="Learner Code">
							</div>
							
							<div class="col-sm-4 form-group">                                  
									<input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
							</div>
						</div>						

			<div id="main-content" style="display:none;"> 
				  
				  <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Center Code:</label>                     
							<input type="hidden" name="txtapplicationfor" id="txtapplicationfor" class="form-control"/>
							<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
							<input type="hidden" class="form-control" maxlength="50" name="txtcid" id="txtcid"/>
							<input type="text" readonly="true" class="form-control" maxlength="50"   name="txtCenterCode" id="txtCenterCode" placeholder="Center Code" value="">
					 </div>
					 
                    <div class="col-sm-4 form-group"> 
                        <label for="ename">Learner Name:</label>                      
                        <input type="text" readonly="true"	 class="form-control" value="" maxlength="50" name="firstname" id="firstname" placeholder="Learner Name">    
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="faname">Mobile No:</label>                       
                        <input type="text" class="form-control" maxlength="50" name="phone" id="phone" value=""  placeholder="Learner Mobile">
                    </div>
					
                    <div class="col-sm-4 form-group">     
                        <label for="faname">Email ID:</label>                    
                        <input type="text" class="form-control" maxlength="50" name="email" id="email" value=""  placeholder="Learner Email">
                    </div>                            
                  </div>  

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">Payment Type:</label>
                           <input type="text" readonly="true" name="productinfo" class="form-control" id="productinfo" value=""/>
					</div>				

                    <div class="col-sm-4 form-group"> 
                        <label for="ename">Transection Reference No:</label>
						<input type="text" readonly="true" class="form-control" maxlength="50" name="transaction" id="transaction" placeholder="Tran. Ref. No:"> 
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="faname">Amount:</label>
						<input type="text" readonly="true" class="form-control" maxlength="50" name="amount" id="amount" value=""  placeholder="Total Amount">
                    </div>
				</div>			
				
                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">DD No.:</label>
                        <input type="text" class="form-control" name="ddno" id="ddno" value="" placeholder="Enter DD No." onkeypress="javascript:return allownumbers(event);"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="ename">DD Date:</label>                                
                        <input type="text" class="form-control" name="dddate" readonly="true" id="dddate" placeholder="YYYY-MM-DD">  
                    </div>
					
                    <div class="col-sm-4 form-group"> 
                        <label for="micr">MICR Code:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="11" onkeypress="javascript:return allownumbers(event);"   name="txtMicrNo" id="txtMicrNo" placeholder="EMP Bank MICR Code">
                    </div>	

                    <div class="col-sm-4 form-group"> 
                        <label for="bankdistrict">Employee Bank District:<span class="star">*</span></label>
                        <select id="ddlBankDistrict" name="ddlBankDistrict" class="form-control">

                        </select>
                    </div>
				</div>

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="bankname">Employee Bank Name:<span class="star">*</span></label>
                        <select id="ddlBankName" name="ddlBankName" class="form-control">
                            <option value="" selected="selected">Please Select</option>                
                        </select>                                                              
                    </div> 
                    <div class="col-sm-4 form-group"> 
                        <label for="branchname">Branch_Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" onkeypress="javascript:return allowchar(event);"  name="txtBranchName" id="txtBranchName"  placeholder="EMP Branch Name">
                    </div>
                    <div class="col-sm-4 form-group"> 
                        <label for="ddreceipt">Upload DD Image<span class="star">*</span></label>
                        <input type="file" class="form-control"  name="ddreceipt" id="ddreceipt" onchange="checkScanForm(this)">
                    </div>    

                </div>
				
                <div class="container">                   
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Pay Now"/>                   
                </div>
			</form>
			</div>
          </div>   
        </div>
	</div> 
  </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<script src="scripts/ddcorrectionupload.js"></script>

<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">
    $('#dddate').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true
                // autoclose: true
    });
</script>
<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>

<script language="javascript" type="text/javascript">
function checkScanForm(target) {
	var ext = $('#ddreceipt').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("ddreceipt").value = '';
			return false;
		}

    if(target.files[0].size > 200000) {
			
        //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
		alert("Image size should less or equal 200 KB");
		document.getElementById("ddreceipt").value = '';
        return false;
    }
	else if(target.files[0].size < 100000)
			{
				alert("Image size should be greater than 100 KB");
				document.getElementById("ddreceipt").value = '';
				return false;
			}
    document.getElementById("ddreceipt").innerHTML = "";
    return true;
}
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";



    $(document).ready(function () {
		
		function FillCorrectionName() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=FILL",
                    success: function (data) {
						//alert(data);
                        $("#ddlCorrection").html(data);
						 
                    }
                });
            }
        FillCorrectionName();
		
		$("#ddlCorrection").change(function(){				
				var DocCode = $('#ddlCorrection').val();
				//alert(BatchCode);
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=Fee&codes=" + DocCode + "",
                    success: function (data) {  
					//alert(data);
                    amount.value = data;				
                    }
                });
			});
		$("#ddlCorrection").change(function () {
				//showpaymentmode(ddlCorrection.value);
				GetApplicationName(ddlCorrection.value);
			});

				function GetApplicationName(val) {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=GetApplicationName&for=" + val + "",
                    success: function (data) {
						//alert(data);
						txtapplicationfor.value = data;
                        //$("#txtapplicationfor").html(data);
						 
                    }
                });
            }
		
		function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                    transaction.value = data;
                }
            });
        }
        GenerateUploadId();

        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        FillBankDistrict();

        $("#ddlBankDistrict").change(function () {
            FillBankName(this.value);
        });
        function FillBankName(districtid) {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKNAME&districtid=" + districtid,
                success: function (data) {
                    $("#ddlBankName").html(data);
                }
            });
        }


        function showData() {		
            $.ajax({
                type: "post",
                url: "common/cfPayment.php",
                data: "action=SHOW&values=" + UserCode + "",
                success: function (data) {
                    //alert($.parseJSON(data)[0]['OwnerName']);
                    data = $.parseJSON(data);                 
					//$('#amount').val(amount);
					//firstname.value = data[0].OwnerName;
					//phone.value = data[0].OwnerMobile;
					//$('#email').val(email);
					 //$('#productinfo').val(PayTypeCode);
					//$('#productinfo').val(PayType);
				}
            });
        }
        //showData();
		
		$("#btnShow").click(function () {
			//alert("hii");
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");		
					$.ajax({
						type: "post",
                        url: "common/cfddcorrectionpayoperator.php",
						data: "action=VERIFYDETAILS&values=" + txtLCode.value + "&correction=" + txtapplicationfor.value + "",
						success: function (data)
						{
							//alert(data);
							 $('#response').empty();
							 if(data == ""){
								 //alert("1");
							 $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Learnercode" + "</span></p>");
							 }
							 else {
								    //alert(data);
									data = $.parseJSON(data);																
									$("#main-content").show();
									$('#txtLCode').attr('readonly', true);									
									txtCenterCode.value = data[0].ITGK;
									firstname.value = data[0].Admission_Name;
									phone.value = data[0].Admission_Mobile;
									email.value = data[0].Admission_Email;
									txtcid.value = data[0].Correction_Id;
									$('#productinfo').val(PayType);
									$("#btnShow").hide();
							 }
							
					    }
                });		
          });

      $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfddcorrectionpayoperator.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#DDForm").serialize();
            if (Mode == 'Update')
            {

                data = "action=Update&" + forminput; // serializes the form's elements.

                //alert(data);
            }
            else
            {

            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmddcorrectionpayoperator.php";
                        }, 1000);

                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
		
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>


<script src="rkcltheme/js/jquery.validate.min.js"></script>
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?> 