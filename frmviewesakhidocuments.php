<?php
$title = "eSakhi Documents";
include ('header.php');
include ('root_menu.php');
?>
<div class="container"> 
    <div style="min-height:430px !important;max-height:1500px !important;">
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Downloads</div>
            <div class="panel-body">
                <div class='col-md-12 col-lg-12'>
                    <div id="tabledatares"></div>
<!--                    <div class="panel panel-info">
                        <div class="panel-heading">MYRKCL Process Guides</div>
                        <div class="panel-body" id="tabledatares">

                            <table class='table table-user-information table table-hover'>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>   -->

                </div>

                <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal">×</button>
                                <h3 class="modal-title">Heading</h3>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        function showData() {
            $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
            setTimeout(function () {
                $('#grid').load();
            }, 2000);
            var url = "common/cfAddeSakhiDocuments.php"; // the script where you handle the form input.
            var data;
            data = "action=SHOWPDF&status=1"; // serializes the form's elements.				 
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    $("#tabledatares").html(data);
                }
            });
        }
        showData();
    });
</script>
<style>
    .modal-dialog {width:600px;}
    .thumbnail {margin-bottom:6px;}
</style>

<script>
    $(document).ready(function () {
        $('.thumbnail').click(function () {
            $('.modal-body').empty();
            var title = $(this).parent('a').attr("title");
            $('.modal-title').html(title);
            $($(this).parents('div').html()).appendTo('.modal-body');
            $('#myModal').modal({show: true});
        });
    });
</script>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>


</body>

</html>