<?php
$title = "Transfer Learner to ilearn";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var ItPeripheralsCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var DeviceCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Activate Learning </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
                <form name="frmNcrReport" id="frmNcrReport" class="form-inline" action=""> 

                    
                    <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="cname">Course Name</label>
                        <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                onchange="try { setCustomValidity('') } catch (e) {}">  
                        </select>
                    </div>
                    <div class="col-sm-4 form-group">     
                        <label for="cname">Batch Name</label>
                        <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                onchange="try { setCustomValidity('') } catch (e) {}">  
                        </select>
                    </div>
                    </div><br>
                        <div class="container">
                     <div id="gird" style="width: 95%"></div>
                    </div>
                </form>

            </div>

        </div>  
        <div class="modal fade" id="previewModal" role="dialog">
            <div>
                <div class="modal-content" style="margin: 100px auto; width: 500px;" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-left">Warning!!</h4>
                    </div>
                    <div class="modal-body" id="rec_type">
                          <h4 class="modal-title text-left">एक्टिवेट लर्निंग करने पर लर्नर ilearn मे स्थानान्तरित हो जाएगा तथा इसके बाद लर्नर की डिटेल्स को एडिट नहीं किया जा सकेगा, इसलिए एक्टिवेट लर्निंग करने से पहले ये सुनीश्छित कर ले की लर्नर की सारी डिटेल्स सही है| </h4>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" id="btnFinalSubmit">Yes</button>
                         <button class="btn btn-primary" id="btnFinalCancle">No</button>
                                                 
                    </div>
                </div>
            </div>
        </div> 
    </div>

</div>

<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        var AckCode = "";
        var CourseCode = "";
        var BatchCode = "";
        var myBookId = "";
        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfLearnerTransferIlearn.php",
                data: "action=FILLAdmissionSummaryCourse",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            // showData();
             $.ajax({
                    type: "post",
                    url: "common/cfLearnerTransferIlearn.php",
                    data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });
        });
 $("#ddlBatch").change(function () {
    showData() 
    });
        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post", url: "common/cfLearnerTransferIlearn.php",
                data: "action=DETAILS&values=" + ddlCourse.value + "&batch=" + ddlBatch.value + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print']
                    });



                }
            });
        }



        $("#gird").on('click', '.updcount', function () {
             myBookId = $(this).attr('id');
           
            // $(".modal-body #bookId").val( myBookId );
            $('#previewModal').modal({show: true});
             AckCode = $(this).attr('AckCode');
             CourseCode = $(this).attr('CourseCode');
             BatchCode = $(this).attr('BatchCode');
             $(this).prop("disabled", true);


        });
         $("#btnFinalCancle").click(function () {
             $("#"+myBookId).prop("disabled", false);
             $('#previewModal').modal('hide');
          });
        $("#btnFinalSubmit").click(function () {
                 $.ajax({
                    type: "post",
                    url: "common/cfLearnerTransferIlearn.php",
                    data: "action=TransferLearner&lcode=" + AckCode + "&ccode=" + CourseCode + "&bcode=" + BatchCode + "",
                    success: function (data) {
                        // $("#ddlBatch").html(data);
                         AckCode = "";
                         CourseCode = "";
                         BatchCode = "";
                          $('#previewModal').modal('hide');
						   $("#"+myBookId).attr('value', 'Transferred to Ilearn');
                    }
                });
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</html>