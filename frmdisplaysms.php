<?php
header('Content-Encoding: UTF-8');
$title = "Message Master";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:300px !important;min-height:500px !important">

    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">SMS Log</div>
            <div class="panel-body">

                <form name="frmdisplaysms" id="frmdisplaysms" class="form-inline" role="form" enctype="multipart/form-data" action="">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                         <div class="container" >
                        <div class="col-sm-4" >
                            <label for="course">Select Category:<span class="star">*</span></label>
                            <select id="ddlCategory" name="ddlCategory" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Category')"
                                    onchange="try {
                                        setCustomValidity('')
                                    } catch (e) {
                                    }">  </select>
                        </div>
                         <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary" name="btnDataSync" id="btnDataSync" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing...">Show Data</button>
                         </div>
                     </div>
                    <div class="container" id="divbydate">
                    <div class="col-sm-3 form-group"> 
                        <label for="sdate">Start Date:</label>
                        <span class="star">*</span>
                        <input type="text" class="form-control" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="DD-MM-YYYY">     
                    </div>

                     <div class="col-sm-3 form-group">     
                        <label for="edate">End Date:</label>    
                        <span class="star">*</span>
                        <input type="text" class="form-control" readonly="true" name="txtenddate" id="txtenddate"  placeholder="DD-MM-YYYY" value=" <?php echo date("d-m-Y"); ?>">
                    </div>

                    <div class="col-sm-3 form-group">
                    <button type="button" class="btn btn-primary" name="btnSubmit" id="btnSubmit" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing...">Download Report</button> 
                         <!--  <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Download Report"/>   -->
                           
                    </div>

                    <div class="col-sm-3 form-group"> 
                          <button type="button" class="btn btn-primary" name="btnSubmit2" id="btnSubmit2" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing...">View Summary</button>
                          <!--  <input type="submit" name="btnSubmit2" id="btnSubmit2" class="btn btn-primary" value="View Summary"/>   --> 
                    </div>
                </div>

                        

                    </div>   
                    <div class="container">
                            <div id="grid" style="margin-top:25px; width:94%;"> </div>
                            <div id="grid2" style="margin-top:25px; width:94%;"> </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
       // autoclose: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function showCat() {
          
            
            $.ajax({
                type: "post",
                url: "common/cfDisplaySms.php",
                data: "action=FILLCAT",
                success: function (data) {
                    
                    $("#ddlCategory").html(data);

                }
            });
        }

        showCat();

        function showData() {
           $('#response').empty();
            $("#btnDataSync").button('loading');
            $.ajax({
                type: "post",
                url: "common/cfDisplaySms.php",
                data: "action=SHOWMSG&cat=" + ddlCategory.value + "",
                success: function (data) {
                    $('#response').empty();
                    
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $("#btnDataSync").button('reset');
                }
            });
        }

       
         $("#btnDataSync").click(function () {

             showData();
          });
      $("#btnSubmit").click(function () {

           $('#response').empty();
            $("#btnSubmit").button('loading');

                var data;
                 var startdate = $('#txtstartdate').val();
                var enddate = $('#txtenddate').val();
                 data = "action=SHOWrpt&startdate=" + startdate + "&enddate=" + enddate + "";
            $.ajax({
                type: "post",
                url: "common/cfDisplaySms.php",
                data: data,
                success: function (data) {
                    $('#response').empty();
                 
            
                    $("#btnSubmit").button('reset');
                    
                    window.open(data, '_blank');
                }
            });
          });
            $("#btnSubmit2").click(function () {

           $('#response').empty();
            $("#btnSubmit2").button('loading');

                var data;
                 var startdate = $('#txtstartdate').val();
                var enddate = $('#txtenddate').val();
                 data = "action=SHOWrpt2&startdate=" + startdate + "&enddate=" + enddate + "";
            $.ajax({
                type: "post",
                url: "common/cfDisplaySms.php",
                data: data,
                success: function (data) {
                    $('#response').empty();
                    
                    $("#grid").html(data);

               
                    $('#example2').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $("#btnSubmit2").button('reset');
                }
            });
          });

             $("#grid").on('click','.approvalLetter' , function (e) {
               // var Url1 = $(this).attr("href"); // Get current url
                 
               // var smstype = getURLParameter(Url1, 'smstype');
                //var typ = getURLParameter(Url1, 'typ');
                 // var smstype = $("[attribute$='smstype']")
                 // var smstype = $("[value$='smstype']")
                 var smstype = $(this).attr("name");
                 var senddate = $(this).attr("id");
                 var rolecode = $(this).attr("mode");
                 // var senddate = getURLParameter(Url1, 'startdate');
                 // var rolecode = getURLParameter(Url1, 'rolecode');
             
                var data = "action=DwldSmsLogList&smstype=" + smstype + "&senddate=" + senddate + "&rolecode=" + rolecode + "";
                $.ajax({
                type: "post",
                url: "common/cfDisplaySms.php",
                data: data,
                success: function (data) {
                    window.open(data, '_blank');
                }
            });
             });
             function getURLParameter(url, name) {
    return (RegExp(name + '=' + '(.+?)(&|$)').exec(url)||[,null])[1];
}
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });
</script>


<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
