<?php
$title="Government Entry Form";
include ('header.php'); 
include ('root_menu.php'); 

    if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
				echo "<script>var examid='" . $_REQUEST['examid'] . "'</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
        <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:46px !important;">

                <div class="panel-heading">Exam Center Details</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmexamcenterdetails" id="frmexamcenterdetails" class="form-inline">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                           
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:</label>
                                <select id="ddlDistrict" name="ddlDistrict" class="form-control" >
								 
                                </select>    
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Exam Center Code 
:</label>
                                <select id="ddlCenter" name="ddlCenter" class="form-control" >
								 
                                </select>    
                            </div>
							
							<div class="col-sm-4 form-group">
							 <label for="edistrict">Exam Tehsil Code
							<select id="ddlTehsil" name="ddlTehsil" class="form-control">
						
						    </select>
							</div>
							<div class="col-sm-4 form-group">    
								 <label for="address">Exam Center Name :</label>
                                <input type="text" class="form-control"  name="txtName" 
									id="txtName" placeholder="Exam Center Name " >
                            </div>
							
					</div>
							
					<div class="container">	
							
							
							
								 <div class="col-sm-4 form-group">    
								 <label for="address">Exam Center Address :</label>
                                <input type="text" class="form-control"  name="txtAddress" 
									id="txtAddress" placeholder="Exam Center Address " >
                            </div>
							
								 <div class="col-sm-4 form-group">    
								 <label for="address">Exam Center Mobile  :</label>
                                <input type="text" class="form-control"  name="txtMobile" 
									id="txtMobile" placeholder="Exam Center Mobile" maxlength="10" onkeypress="javascript:return allownumbers(event);">
                            </div>
							
							<div class="col-sm-4 form-group">    
								 <label for="address">Exam Center Email :</label>
                                <input type="email" class="form-control"  name="txtEmail" 
									id="txtEmail" placeholder="Exam Center Email"  maxlength="100" >
                            </div>	
									
					<div class="col-sm-4 form-group">    
								 <label for="address">Exam Center Capacity:</label>
                                <input type="text" class="form-control"  name="txtCapacity" 
									id="txtCapacity" placeholder="Exam Center Capacity" onkeypress="javascript:return allownumbers(event);"  maxlength="4">
                            </div>
								
						</div>	


						<div class="container">
								
							
								<div class="col-sm-4 form-group">    
								 <label for="address">No of Rooms:</label>
                                <input type="text" class="form-control"  name="txtRooms" 
									id="txttxtRooms" placeholder="No of Rooms"  onkeypress="javascript:return allownumbers(event);" maxlength="2">
                            </div>
							
							<div class="col-sm-4 form-group">    
								 <label for="address">Exam Center Coordinator Name:</label>
                                <input type="text" class="form-control"  name="txtCoordinator" 
									id="txtCoordinator" placeholder="Exam Center Coordinator Name" >
                            </div>	
							
							
							
								<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Event Name 
								:</label>
                                <select id="ddlEvent" name="ddlEvent" class="form-control" >
								 
                                </select>    
                            </div>
							
							
							 <div class="col-sm-4 form-group"> 
                                
                               <div id='txtexamid'></div>
							   <input type="hidden" name="txtexamid1" id="txtexamid1"  />                         </div>
                           
							


						</div>	
							
							
					<div class="container">
					
					
							
							
							 <div class="col-sm-4 form-group">     
                                								
										<div id='txtdate'></div>
										<input type="hidden" name="txtdate1" id="txtdate1"  />
							</div>
					</div>
					

                  

                       



                        <div class="container">

                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
						<div id='gird'></div>
						
					
                 </div>
            </div>   
        </div>


    </form>





</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>

 <script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
							if (Mode == 'Delete')
							{
								if (confirm("Do You Want To Delete This Item ?"))
								{
		                   		 deleteRecord();
		               		}
		           		 }
		           else if (Mode == 'Edit')
		           {
		                fillForm();
		           }
				   
				   
				   
			function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfexamcenterdetails.php",
                    data: "action=DELETE&values=" + Code + 
					"&examid="+examid+"",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmexamcenterdetails.php";
                           }, 1000);
                            Mode="Add";
                            resetForm("frmexamcenterdetails");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }
			
            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfexamcenterdetails.php",
                    data: "action=EDIT&values=" + Code + 
					"&examid="+examid+"",
                    success: function (data) {
                        
                        //alert($.parseJSON(data)[0]['Status']);
                        //alert(data);
                        data = $.parseJSON(data);
                        ddlDistrict.value = data[0].district;
                        ddlCenter.value=data[0].examcentercode;
						ddlTehsil.value=data[0].tehsil;
                        txtName.value = data[0].examcentername;
						txtAddress.value = data[0].examcenteradd;
						txtMobile.value=data[0].examcentermobile;
                        txtEmail.value = data[0].examcenteremail;
						txtCapacity.value = data[0].examcentercapacity;
						txtRooms.value = data[0].noofrooms;
						txtCoordinator.value=data[0].examcentercoordinator;
                        ddlEvent.value = data[0].date;
						txtexamid.value=data[0].examid;
                    
                    }
                });
            }
			function showData() {
		                
		                $.ajax({
		                    type: "post",
		                    url: "common/cfexamcenterdetails.php",
		                    data: "action=SHOW",
		                    success: function (data) {
		
		                        $("#gird").html(data);
								
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
								
		
		                    }
		              });
		            }
		
		            showData();
			   
				   function FillEvent() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfExamMaster.php",
                                    data: "action=FILLPUBLISHEVENT",
                                    success: function (data) {
                                        $("#ddlEvent").html(data);
                                    }
                                });
                            }
                             FillEvent(); 
				   
				  
					
                         
                              function FillDistrict() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfDistrictMaster.php",
                                    data: "action=FILL",
                                    success: function (data) {
                                        $("#ddlDistrict").html(data);
                                    }
                                });
                            }
                             FillDistrict(); 
							 
							 
							 
                          
                            $("#ddlDistrict").change(function(){
				var selDistrict = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfexamcenterdetails.php',
			          type: "post",
			          data: "action=FILLCENTER&values=" + selDistrict + "",
			          success: function(data){
						//alert(data);
						$('#ddlCenter').html(data);
			          }
			        });
                            });
							
							
							
							$("#ddlDistrict").change(function(){
				var selDistrict = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfTehsilMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selDistrict + "",
			          success: function(data){
						//alert(data);
						$('#ddlTehsil').html(data);
			          }
			        });
                            });
							
							
							
							 $("#ddlEvent").click(function(){
				var Event = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfExamMaster.php',
			          type: "post",
			          data: "action=FILLDATE&values=" + Event + "",
			          success: function(data){
						//alert(data);
						$('#txtdate').html(data);
						//$('#txtdate1').html(data);
						txtdate1.value=data;
			          }
			        });
                            });
							
							
							$("#ddlEvent").click(function(){
				var Event = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfExamMaster.php',
			          type: "post",
			          data: "action=FILLEXAMID&values=" + Event + "",
			          success: function(data){
						//alert(data);
						$('#txtexamid').html(data);
						//$('#txtexamid1').html(data);
						txtexamid1.value=data;
			          }
			        });
                            });
							
					
							
					
                           


                            $("#btnSubmit").click(function () {
							if ($("#frmexamcenterdetails").valid())
							{								
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                               var url = "common/cfexamcenterdetails.php"; // the script where you handle the form input.
								var data;
								var forminput=$("#frmexamcenterdetails").serialize();
								if (Mode == 'Add')
								{
									data = "action=ADD&" + forminput; // serializes the form's elements.
								}
                                else
                                {
                                 data = "action=UPDATE&code=" + Code +"&" + forminput;
										
                                }

                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                            window.setTimeout(function () {
                                                window.location.href = "frmexamcenterdetails.php";
                                            }, 1000);

                                            Mode = "Add";
                                            resetForm("frmexamcenterdetails");
                                        }
                                        else
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                        }
                                        showData();


                                    }
                                });
							}
                                return false; // avoid to execute the actual submit of the form.
                            });
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmexamcentervalidation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>