<?php
$title="Phrase Master Form";
include ('header.php'); 
include ('root_menu.php'); 

  if (isset($_REQUEST['code'])) {
                echo "<script>var Phrase_ID=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Phrase_ID=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>

<div class="container"> 


    <div class="panel panel-primary" style="margin-top:36px !important;">

        <div class="panel-heading">Phrase Master</div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            <form name="frmphrasemaster" id="frmphrasemaster" class="form-inline" role="form" enctype="multipart/form-data">     

                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>
                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Phrase Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" style="text-transform: uppercase;" name="txtStateName" id="txtStateName" placeholder="Phrase Name">
                    </div>

                </div>  

                <div class="container">

                    <input  style="margin-left: 16px;" type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                </div>
            </form>   


        </div>
        <div id="gird"></div>
    </div>   
</div>


    




</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            
            
                        
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfphrasemaster.php",
                    data: "action=DELETE&values=" + Phrase_ID + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                            }, 3000);
                            Mode="Add";
                            resetForm("frmphrasemaster");
                        }
                        else if (data == 'DONTDOTHIS')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Active Phrase Can Not Be Deleted.</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                            }, 3000);
                            Mode="Add";
                            resetForm("frmphrasemaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfphrasemaster.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);

                    }
                });
            }

            showData();


            $("#btnSubmit").click(function () {
                $("#frmphrasemaster").valid();
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfphrasemaster.php"; // the script where you handle the form input.
                var data;
                data = "action=ADD&name=" + txtStateName.value + ""; // serializes the form's elements.
                
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmphrasemaster.php";
                           }, 1000);

                            Mode="Add";
                            resetForm("frmphrasemaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script>

$("#frmphrasemaster").validate({
        rules: {
            txtStateName: { required: true },	ddlCountry: { required: true },		ddlStatus: { required: true }
        },			
        messages: {				
            txtStateName: { required: '<span style="color:red; font-size: 12px;">Please enter Phrase Name</span>' },
			
        },
	});

</script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>