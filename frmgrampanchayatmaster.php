<?php
$title = "Gram Panchayat Master";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var GramPanchayatCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var GramPanchayatCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Panchayat Master</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmGramPanchayatMaster" id="frmGramPanchayatMaster" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Gram Panchayat Name:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="50" name="txtGramPanchayatName" id="txtGramPanchayatName" placeholder="Gram Panchayat Name">
                        </div>







                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Panchayat Name:</label>
                            <select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >

                            </select>    
                        </div>






                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Gram Panchayat Status:</label>
                            <select id="ddlStatus" name="ddlStatus" class="form-control" >

                            </select>    
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <br />
                            <input type="button" name="btnSearch" id="btnSearch" class="btn btn-primary" value="Search"/>
                        </div>

                    </div>

                    <div class="container">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>
                    </div>

                </form>


            </div>
            <div id="gird"></div>
        </div>   
    </div>



</div>


</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }

        function FillStatus() {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlStatus").html(data);
                }
            });
        }

        FillStatus();

        function FillParent() {
            $.ajax({
                type: "post",
                url: "common/cfPanchayatSamitiMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlPanchayat").html(data);
                }
            });
        }

        FillParent();

        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfGramPanchayatMaster.php",
                data: "action=DELETE&values=" + GramPanchayatCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            $('#response').empty();
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmGramPanchayatMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    searchData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfGramPanchayatMaster.php",
                data: "action=EDIT&values=" + GramPanchayatCode + "",
                success: function (data) {
                    //alert($.parseJSON(data)[0]['GramPanchayatName']);
                    data = $.parseJSON(data);
                    txtGramPanchayatName.value = data[0].GramPanchayatName;
                    ddlDistrict.value = data[0].District;
                    ddlStatus.value = data[0].Status;

                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfGramPanchayatMaster.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });
        }

        //showData();

        function searchData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfGramPanchayatMaster.php"; // the script where you handle the form input.
            var data = "action=Search&code=" + GramPanchayatCode + "&name=" + txtGramPanchayatName.value + "&parent=" + ddlPanchayat.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $('#response').empty();
                }
            });
        }

        $("#btnSearch").click(function () {
            searchData();
        });

        $("#btnSubmit").click(function () {

            if ($("#frmGramPanchayatMaster").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfGramPanchayatMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtGramPanchayatName.value + "&parent=" + ddlPanchayat.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + GramPanchayatCode + "&name=" + txtGramPanchayatName.value + "&parent=" + ddlPanchayat.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmGramPanchayatMaster.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("frmGramPanchayatMaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        searchData();
                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmgrampanchayatmastervalidation.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>