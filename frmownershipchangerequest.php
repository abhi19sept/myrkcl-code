<?php
$title = "Ownership Change Request Form";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var OrganizationCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var OrganizationCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
$random = (mt_rand(1000, 9999));
$random .= date("y");
$random .= date("m");
$random .= date("d");
$random .= date("H");
$random .= date("i");
$random .= date("s");

echo "<script>var OrgDocId= '" . $random . "' </script>";
?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:46px !important;">

            <div class="panel-heading">Ownership Change Request Form</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmownershipchangerequest" id="frmownershipchangerequest"  role="form" action="" class="form-inline" enctype="multipart/form-data">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <input type="button" name="step1" id="step1" class="btn btn-danger" value="Initiate Ownership Change Process" style="display:none;"/>
                    <br><br>
                    <div class="panel panel-info" id="existing_details" style="display:none">
                        <div class="panel-heading">Step 1: View Existing IT-GK Details</div>
                        <div class="panel-body">
                            <div class="container">
                                <div id="errorBox"></div>
                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Name of Organization/Center:</label>
                                    <input type="text" class="form-control" name="Organization_Name_old" id="Organization_Name_old" placeholder="Name of the Organization/Center"
                                           readonly='readonly'>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">Type of Organization:</label>
                                    <input type="text" class="form-control" maxlength="50" name="Organization_Type_old" id="Organization_Type_old" readonly='readonly'>
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Email:</label>
                                    <input type="text" class="form-control" maxlength="50" name="Organization_Email_old" id="Organization_Email_old" readonly='readonly'>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Mobile:</label>
                                    <input type="text" class="form-control" maxlength="50" name="Organization_Mobile_old" id="Organization_Mobile_old" readonly='readonly'>
                                </div>
                            </div>	

                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Area Type:</label>
                                    <input type="text" class="form-control" maxlength="50" name="Organization_AreaType_old" id="Organization_AreaType_old" readonly='readonly'>
                                </div>
                                <div class="container" id="urbanArea_old">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Municipality Type:</label>
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Municipal_Type_old" id="Organization_Municipal_Type_old"
                                               readonly='readonly'>
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="address">Municipality Name:</label>
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Municipal_old" id="Organization_Municipal_old" readonly='readonly'>
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="address">Ward Number:</label>
                                        <input type="text" class="form-control" maxlength="50" name="Organization_WardNo_old" id="Organization_WardNo_old" readonly='readonly'>

                                    </div>
                                </div>
                                <div class="container" id="ruralArea_old">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Panchayat Samiti/Block:</label>
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Panchayat_old" id="Organization_Panchayat_old" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="address">Gram Panchayat:</label>
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Gram_old" id="Organization_Gram_old" readonly='readonly'>
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="address">Village:</label>
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Village_old" id="Organization_Village_old" readonly='readonly'>

                                    </div>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-sm-4 form-group">     
                                    <label for="faname">RSP Name:</label>
                                    <input type="text" class="form-control"  name="Organization_Rspname_old" id="Organization_Rspname_old" readonly='readonly'>
                                </div>
                                <div class="col-sm-4 form-group">     
                                    <label for="SelectType">District:</label>
                                    <input type="text" class="form-control" maxlength="50" name="Organization_District_old" id="Organization_District_old" readonly='readonly'>

                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="email">Tehsil:</label>
                                    <input type="text" class="form-control" maxlength="50" name="Organization_Tehsil_old" id="Organization_Tehsil_old" readonly='readonly'>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Full Address for Correspondence:</label>
                                    <textarea id="Organization_Address_old" name="Organization_Address_old" class="form-control" rows="4" cols="50" readonly></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="button" name="step2" id="step2" class="btn btn-danger" style="display:none" value="Proceed To Step 2" style="display:none"/>
                    <br><br>
                    <div class="panel panel-info" id="impreg" style="display:none">
                        <div class="panel-heading">Step 2(a): Verify Contact Details of New Owner</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-4 form-group">     
                                    <label for="SelectType">Type of Application:<span class="star">*</span></label>
                                    <select id="ddlOrgType" name="ddlOrgType" class="form-control" >
                                        <option selected="true" value="">Select</option>
                                        <option value="Ownership_Change" >Ownership Change</option>
                                    </select>
                                    <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="email">Enter Email:<span class="star">*</span></label>
                                    <input type="email" class="form-control" maxlength="50" name="txtEmail" id="txtEmail" placeholder="Email ID" required>     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="Mobile">Enter Mobile Number:<span class="star">*</span></label>
                                    <input type="text" class="form-control" minlength="10" maxlength="10" name="txtMobile" id="txtMobile" onkeypress="javascript:return allownumbers(event);" placeholder="Mobiile Number" required>
                                </div> 
                                <div class="col-sm-4 form-group">
                                    <label for="Verify">Generate OTP:<span class="star">*</span></label>
                                    <input type="button" name="btnGenerateOTP" id="btnGenerateOTP" class="btn btn-primary" value="Generate OTP"/>
                                    <label for="tnc" id="OTPSent" style="margin-top:25px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>OTP Sent</b></label></br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info" id="otpverify" style="display:none">
                        <div class="panel-heading">Step 2(b): Verify Authentication Details of New Owner</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="otpemail">Enter OTP Received on Email of New ITGK:</label>
                                    <input type="text" class="form-control" maxlength="6" name="txtOTPEmail" id="txtOTPEmail" placeholder="OTP Received on Email ID">     
                                </div>
                                <div class="col-sm-4 form-group">     
                                    <label for="otpMobile">Enter OTP Received on Mobile Number of New IT-GK:</label>
                                    <input type="text" class="form-control" maxlength="6" name="txtOTPMobile" id="txtOTPMobile"  placeholder="OTP Received on Mobiile Number">
                                </div>
                                <div class="col-sm-4 form-group">
                                    <label for="Verify">Verify Mobile No and Email Id of New ITGK:<span class="star">*</span></label>
                                    <input type="button" name="btnVerify" id="btnVerify" class="btn btn-primary" value="Verify" style="margin-top:25px"/>
                                    <!--<input type="button" name="btnVerified" id="btnVerified" class="btn btn-primary" value="Details Verified" style="margin-top:25px; display:none"/>-->
                                    <label for="tnc" id="btnVerified" style="margin-top:25px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Details Verified</b></label></br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="button" name="step3" id="step3" class="btn btn-danger" style="display:none" value="Proceed To Step 3"/>
                    <br><br>
                    <div id="main-content" style="display:none">
                        <div class="panel panel-info">
                            <div class="panel-heading">Step 3: Enter  Details of New Center</div>
                            <div class="panel-body">
                                <div class="container">

                                    <div id="errorBox"></div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="learnercode">Name of New Organization/Center:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center" oncopy="return false" onpaste="return false" oncut="return false" onkeypress="javascript:return allowchar(event);">
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="ename">Registration No:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="30" name="txtRegno" id="txtRegno" placeholder="Registration No" onkeypress="javascript:return validAddress(event);">     
                                    </div>


                                    <div class="col-sm-4 form-group">     
                                        <label for="faname">Date of Establishment:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="50" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD"  readonly="true">
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Type of Organization:<span class="star">*</span></label>
                                        <select id="txtType" name="txtType" class="form-control" >

                                        </select>    
                                    </div>
                                </div>
                                <div class="panel panel-info" id="OrgType1" style="display:none">
                                    <div class="panel-heading">Upload Documents for Proprietorship/Individual Firm</div>
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="col-sm-4 form-group">
                                                <label for="owntype">Select Ownership Type :<span class="star">*</span></label>
                                                <select id="ddlowntype" name="ddlowntype" class="form-control" >
                                                    <option value="">Select</option>
                                                    <option value="Rented">Rented</option>
                                                    <option value="Owned">Owned</option>
                                                    <option value="NOC">NOC</option>
                                                </select>                               
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label id="Rented" for="rented" style="display:none">Rent Agreement:<span class="star">*</span></label>
                                                <label id="Owned" for="owned" style="display:none">Ownership document  :<span class="star">*</span></label>
                                                <label id="NOC" for="noc" style="display:none">NOC Copy :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="owntype" id="owntype" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group" style="display:none" id="utibill"> 
                                                <label for="salpi">Latest paid Utility Bill :<span class="star">*</span></label>
                                                <input type="file" class="form-control" name="utilitybill" id="utilitybill" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="salpi">Shop Act License :<span class="star">*</span></label>
                                                <input type="file" class="form-control" name="salpi" id="salpi" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                        <div class="container">
                                            <div class="col-sm-4 form-group">
                                                <label for="panpi">PAN card copy of Proprietor :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="panpi" id="panpi" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ccpi">Cancelled Cheque :<span class="star">*</span><a data-toggle="tooltip" title="One Cancelled cheque in which ITGK wants any share/ payment from RKCL , this could be either saving bank a/c cheque or current a/c cheque."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="ccpi" id="ccpi" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                    </div>
                                </div>
                                <div class="panel panel-info" id="OrgType2" style="display:none">
                                    <div class="panel-heading">Upload Documents for Partnership Firm</div>
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="copd">Copy of Partnership Deed :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="copd" id="copd" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="al">Authorization letter :<span class="star">*</span><a data-toggle="tooltip" title="Authorization letter to sign the agreement by one partner from other partners."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="al" id="al" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ppf">PAN card of Partnership firm:<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="ppf" id="ppf" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ccpf">Cancelled cheque of firm :<span class="star">*</span><a data-toggle="tooltip" title="Cancelled cheque of Partnership firm only. No check in the name of the Partner."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>

                                                <input type="file" class="form-control"  name="ccpf" id="ccpf" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                        <div class="container">
                                            <div class="col-sm-4 form-group">
                                                <label for="rcrf">Address Proof documents :<span class="star">*</span><a data-toggle="tooltip" title="Address Proof documents such as Rent Agreement along with utility bill, if premises is in the name of firm, than ownership documents, and if premises is in the name of any partner or his relative , an NOC along with utility bill."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="apdpf" id="apdpf" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="rcrf">Registration certificate<span class="star">*</span><a data-toggle="tooltip" title="Registration certificate of Registrar of firms."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="rcrf" id="rcrf" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="sarpf">Shop Act Registration Copy :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="sarpf" id="sarpf" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                    </div>
                                </div>
                                <div class="panel panel-info" id="OrgType3" style="display:none">
                                    <div class="panel-heading">Upload Documents for Public Ltd. or Pvt Ltd.</div>
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="coi">Certificate of Incorporation :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="coi" id="coi" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="lod">List of Directors:<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="lod" id="lod" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="pan">Copy of PAN card of Company:<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="copan" id="copan" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ccc">Canceled cheque of Company :<span class="star">*</span><a data-toggle="tooltip" title="One Canceled cheque of Company in which company wants to take any share/payment from RKCL. Director's Check is not allowed"><span style="width: 1%;" class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>

                                                <input type="file" class="form-control"  name="ccc" id="ccc" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                        <div class="container">
                                            <div class="col-sm-4 form-group">Copy of Board Resolution
                                                <input type="file" class="form-control"  name="cobr" id="cobr" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="apd">Address Proof documents :<span class="star">*</span><a data-toggle="tooltip" title="Address Proof documents such as Rent Agreement along with utility bill, if premises is in the name of Company , than ownership documents, and if premises is in the name of any Director  or his relative , an NOC along with utility bill."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="apd" id="apd" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="csar">Copy of Shop Act Registration:<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="csar" id="csar" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                    </div>
                                </div>
                                <div class="panel panel-info" id="OrgType4" style="display:none">
                                    <div class="panel-heading">Upload Documents for Limited Liability Partnership (LLP)</div>
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="coillp">Certificate of Incorporation  :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="coillp" id="coillp" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="lop">List of Partners :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="lop" id="lop" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="panllp">Copy of PAN card of LLP :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="panllp" id="panllp" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ccllp">Canceled cheque of LLP :<span class="star">*</span><a data-toggle="tooltip" title="Canceled cheque of LLP in which company wants to take any share/payment from RKCL."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>

                                                <input type="file" class="form-control"  name="ccllp" id="ccllp" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="cobrllp">Copy of Board Resolution :<span class="star">*</span><a data-toggle="tooltip" title="Copy of Board Resolution authorizing for signing the agreement."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="cobrllp" id="cobrllp" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label for="apdllp">Address Proof Document :<span class="star">*</span><a data-toggle="tooltip" title="Address Proof documents such as Rent Agreement along with utility bill, if premises is in the name of LLP , than ownership documents, and if premises is in the name of any Director/Partner   or his relative , an NOC along with utility bill."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="apdllp" id="apdllp" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="csarllp">Copy of Shop Act Registration :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="csarllp" id="csarllp" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                    </div>
                                </div>
                                <div class="panel panel-info" id="OrgType5" style="display:none">
                                    <div class="panel-heading">Upload Documents for Other Types of Firm</div>
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="cor">Certificate of Registration under relevant Act :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="cor" id="cor" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="panoth">Copy of PAN Card of Organization  :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="panoth" id="panoth" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ccoth">Copy of Cancelled cheque of Organization :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="ccoth" id="ccoth" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="leb">List of Executive Body :<span class="star">*</span></label>

                                                <input type="file" class="form-control"  name="leb" id="leb" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="cbr">Copy of Board Resolution :<span class="star">*</span><a data-toggle="tooltip" title="Copy of Board Resolution authorizing for signing the agreement."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="cbr" id="cbr" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label for="adpoth">Address Proof documents :<span class="star">*</span><a data-toggle="tooltip" title="Address Proof documents such as Rent Agreement along with utility bill, if premises is in the name of  organization  , than ownership documents, and if premises is in the name of any member or his relative , an NOC along with utility bill."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="adpoth" id="adpoth" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                    </div>
                                </div>

                                <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Document Type:<span class="star">*</span></label>
                                        <select id="txtDoctype" name="txtDoctype" class="form-control" >
                                            <option value="">Select</option>
                                            <option value="PAN Card">PAN Card</option>
                                            <!--                            <option value="Voter ID Card">Voter ID Card</option>
                                                                        <option value="Driving License">Driving License</option>
                                                                        <option value="Passport">Passport</option>
                                                                        <option value="Employer ID card">Employer ID card</option>
                                                                        <option value="Government ID Card">Governments ID Card</option>
                                                                        <option value="College ID Card">College ID Card</option>
                                                                        <option value="School ID Card">School ID Card</option>-->
                                        </select>    
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="panno">PAN Card Number:<span class="star">*</span></label>
                                        <input type="text" class="form-control" minlength="10" maxlength="10" name="txtPanNo" id="txtPanNo" placeholder="PAN Card Number" onkeypress="javascript:return validAddress(event);">     
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="panno">AADHAR Card Number:<span class="star">*</span></label>
                                        <input type="text" class="form-control"  minlength="12" maxlength="12" name="txtAADHARNo" id="txtAADHARNo" placeholder="AADHAR Card Number" onkeypress="javascript:return allownumbers(event);">     
                                    </div>
                                </div>	

                            </div>
                        </div></div>
                    <input type="button" name="step4" id="step4" class="btn btn-danger" style="display:none" value="Proceed To Step 4"/>
                    <br><br>
                    <div class="panel panel-danger" id="bank_account" style="display:none;">  
                        <div class="panel-heading">Step 4: Enter New Bank Account Details </div>
                        <div class="panel-body">

                            <div class="container">       
                                <div id="errorBox1"></div>

                                <div class="col-sm-4 form-group" id='ifsc'>     
                                    <label for="order">IFSC Code:<font color="red">*</font></label>
                                    <input type="text" class="form-control" maxlength="11" name="txtIfscCode" id="txtIfscCode" placeholder="IFSC Code"  onkeypress="javascript:return validAddress(event);">
                                </div>  
                                <div class="col-sm-4 form-group">
                                    <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Validate IFSC Code" style="margin-top:25px"/> 
                                </div> 

                            </div>

                            <div id="details" style="display:none">

                                <div class="container">
                                    <div class="col-sm-4 form-group">     
                                        <label for="dob">Account Holder Name:<font color="red">*</font></label>	
                                        <input type="text" class="form-control" maxlength="100" name="txtaccountName" id="txtaccountName"  placeholder="Account Name" onkeypress="javascript:return allowchar(event);" style="text-transform:uppercase"/>
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="dob">Account Number:<font color="red">*</font></label>

                                        <input type="text" class="form-control" maxlength="20" name="txtaccountNumber" id="txtaccountNumber"  placeholder="Account Number" onpaste="return false;" onkeypress="javascript:return allownumbers(event);"/>
                                    </div>
                                    <div class="col-sm-4 form-group" >     
                                        <label for="dob">Confirm Account Number:<font color="red" >*</font></label>

                                        <input type="text" class="form-control" maxlength="20" name="txtConfirmaccountNumber" id="txtConfirmaccountNumber" onpaste="return false;"  placeholder="Account Number"  onkeypress="javascript:return allownumbers(event);"/>
                                        <span id='message'></span>
                                    </div>
                                    <div class="col-sm-5 form-group"> 
                                        <label for="status">Bank Name:<font color="red">*</font></label>

                                        <input type="text" class="form-control" maxlength="500" name="ddlBankName" id="ddlBankName" placeholder="Bank Name" onpaste="return false;"  onkeypress="javascript:return validAddress(event);">
                                    </div>
                                </div>

                                <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="status">Branch Name:<font color="red">*</font></label>
                                        <input type="text" class="form-control" maxlength="500" name="ddlBranch" id="ddlBranch" placeholder="Branch Name" onpaste="return false;"  onkeypress="javascript:return validAddress(event);">
                                    </div>   

                                    <div class="col-sm-4 form-group">     
                                        <label for="order">MICR Code:<font color="red">*</font></label>
                                        <input type="text" class="form-control" maxlength="14" name="txtMicrcode" id="txtMicrcode" placeholder="MICR Code" onpaste="return false;"  onkeypress="javascript:return allownumbers(event);">
                                    </div>  

                                    <div class="col-sm-4 form-group">     
                                        <label for="gender">Account Type:<font color="red">*</font></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="rbtaccountType_saving" name="rbtaccountType_saving" checked="checked" value="Savings"/> Savings </label>
                                        <label class="radio-inline"> <input type="radio" id="rbtaccountType_saving" name="rbtaccountType_saving" value="Current"/> Current </label>									
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="status">Pan Card No.:<font color="red">*</font></label>
                                        <input type="text" class="form-control" maxlength="11" name="txtpanno1" id="txtpanno1"
                                               placeholder="Pan Card No." onpaste="return false;" onkeypress="javascript:return validAddress(event);" style="text-transform:uppercase">
                                    </div>
                                </div>

                                <div class="container">

                                    <div class="col-sm-4 form-group"> 
                                        <label for="status">Pan Card Holder Name:<font color="red">*</font></label>
                                        <input type="text" class="form-control" maxlength="100" name="txtpanname" id="txtpanname"
                                               placeholder="Pan Card Holder Name" onkeypress="javascript:return allowchar(event);" style="text-transform:uppercase">
                                    </div>  

                                    <div class="col-sm-4 form-group"> 
                                        <label for="photo">Attach Pan Card Copy:<span class="star">*</span></label>
                                        <input type="file" class="form-control" id="pancard" name="pancard"  onchange="ValidateSingleInput(this);" >
                                        <span style="font-size:10px;">Note : PDF Allowed Size 100 - 200KB</span>
                                        <img src="images/Pencard.jpg" width="50" />
                                    </div> 

                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Select Bank Account ID Proof:<span class="star">*</span></label>
                                        <select id="ddlidproof" name="ddlidproof" class="form-control">
                                            <option value=""> Please Select </option>
                                            <option value="cheque"> Cancelled Cheque Copy </option>
                                            <option value="welcome"> Welcome Letter of Bank </option>
                                            <option value="passbook"> Passbook Copy </option>
                                        </select>    
                                    </div>	

                                    <div class="col-sm-4 form-group"> 
                                        <label for="photo">Attach Bank Id Proof:<span class="star">*</span></label>
                                        <input type="file" class="form-control" id="chequeImage" name="chequeImage"  onchange="ValidateSingleInput(this);"  >
                                        <span style="font-size:10px;">Note : PDF Allowed Size 100 - 200KB</span>
                                        <img src="images/sbi.jpg" width="50" />
                                    </div>
                                </div>

                                <div class="container"> 
                                    <div class="col-md-11"> 	

                                        <label for="learnercode" style="float:left;margin:left:15px" ><b>मैं यह स्वीकार करता हूँ कि मैं आई. टी. ज्ञान केंद्र  का Proprietor / Partner / Director / President / Chairman / Secretary / Karta / Trustee हूँ एवं मेरे द्वारा प्रवेशित Bank Account Details मेरी अथवा मेरी Firm / साझेदारी Firm / Company / Society / Trust / HUF / संस्था की ही है |  मैं यह भी स्वीकार करता हूँ कि RKCL अथवा RKCL-Escrow Account द्वारा आई. टी. ज्ञान केंद्र को किसी भी प्रकार का भुगतान उक्त बैंक खाते में किया जायेगा अतः प्रवेशित Bank Account Details में किसी भी तरह की गलती के लिए मैं स्वयं जिमेदार रहूँगा |</b></label></br>                              
                                        <label class="checkbox-inline" style="float:left;"> <span class="star"> </span><input type="checkbox" name="chk" id="chk" value="1" >
                                            I Accept 
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="button" name="step5" id="step5" class="btn btn-danger" style="display:none" value="Proceed To Step 5"/>
                    <br><br>
                    <div class="panel panel-info" id="owner_docs" style="display:none">
                        <div class="panel-heading">Step 5: Upload New Owner Documents</div>
                        <div class="panel-body">
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <div class="panel panel-danger form-group ">
                                <div class="panel-heading">Upload PAN Card</div>
                                <div class="panel-body">
                                    <div class="col-sm-12" > 
                                        <label for="photo">Attach PAN Card:<span class="star">*</span></label>
                                        <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->								  
                                        <input style=" width: 115%" id="uploadImage7" type="file" name="uploadImage7" onchange="checkPANPhoto(this);"/>	
                                        <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                        <span style="font-size:10px;">Note: PDF Allowed Size = 100 to 200 KB</span>
                                    </div>
                                </div>
                            </div>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <div class="panel panel-danger form-group">
                                <div class="panel-heading">Upload AADHAR Card</div>
                                <div class="panel-body">
                                    <div class="col-sm-12" > 
                                        <label for="photo">Attach AADHAR Card:<span class="star">*</span></label> 
                                        <!--<img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage8').click();">-->								  
                                        <input style=" width: 115%" id="uploadImage8" type="file" name="uploadImage8" onchange="checkAADHARPhoto(this);"/>	
                                        <span style="font-size:10px;">Note: PDF Allowed Size = 100 to 200 KB</span>
                                    </div>
                                </div>
                            </div>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <div class="panel panel-danger form-group">
                                <div class="panel-heading">Upload Address Proof</div>
                                <div class="panel-body">
                                    <div class="col-sm-12" > 
                                        <label for="photo">Attach Document:<span class="star">*</span></label> 
                                        <!--<img id="uploadPreview9" src="images/sampleproof.png" id="uploadPreview9" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage9').click();">-->								  
                                        <input style=" width: 115%" id="uploadImage9" type="file" name="uploadImage9" onchange="checkAddressProof(this);"/>	
                                        <span style="font-size:10px;">Note: PDF Allowed Size = 100 to 200 KB</span>
                                    </div>
                                </div>
                            </div>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            &nbsp;                        
                            <div class="panel panel-danger form-group">
                                <div class="panel-heading">Upload Ownership Transfer Application Form</div>
                                <div class="panel-body">
                                    <div class="col-sm-12" > 
                                        <label for="photo">Attach Document:<span class="star">*</span></label>  
                                        <!--<img id="uploadPreview6" src="images/sampleproof.png" id="uploadPreview6" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage6').click();">-->								  
                                        <input style=" width: 115%" id="uploadImage6" type="file" name="uploadImage6" onchange="checkNCRApp(this);"/>	
                                        <span style="font-size:10px;">Note: PDF Allowed Size = 100 to 200 KB</span>
										<a href="http://www.rkcl.in/myrkclwebsite/uploads/news/1549019892.pdf">Download Format</a>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
            </div>
            <div class="container">

                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-danger" style="display:none" value="Click here to Submit Application"/>    
            </div>
        </div>
        </form>
    </div>   
</div>
</div>
</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<!--<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>-->
<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    $('#txtEstdate').datepicker({
        format: "yyyy-mm-dd"
    });
</script>
<script>
    $('#txtConfirmaccountNumber').on('keyup', function () {
        if ($(this).val() == $('#txtaccountNumber').val()) {
            $('#message').html('confirmed').css('color', 'green');
        } else
            $('#message').html('confirm account no. should be same as account no').css('color', 'red');
    });
</script>
<script language="javascript" type="text/javascript">
    var _validFileExtensions = [".pdf"];
    function ValidateSingleInput(oInput) {
        if (oInput.type == "file") {
            var sFileName = oInput.value;

            var iFileSize = oInput.files[0].size;

            if (sFileName.length > 0) {
                if (iFileSize > 100000 && iFileSize < 2000000) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }

                } else {
                    alert("File Size should be between 100KB to 2 MB.");
                    oInput.value = "";
                }
            }
        }
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkPANPhoto(target) {
        var ext = $('#uploadImage7').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage7").value = '';
            return false;
        }

        if (target.files[0].size > 2000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage7").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        }
        document.getElementById("uploadImage7").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkAADHARPhoto(target) {
        var ext = $('#uploadImage8').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage8").value = '';
            return false;
        }

        if (target.files[0].size > 2000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage8").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage8").value = '';
            return false;
        }
        document.getElementById("uploadImage8").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkAddressProof(target) {
        var ext = $('#uploadImage9').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage9").value = '';
            return false;
        }

        if (target.files[0].size > 2000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage9").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage9").value = '';
            return false;
        }
        document.getElementById("uploadImage9").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkNCRApp(target) {
        var ext = $('#uploadImage6').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage6").value = '';
            return false;
        }

        if (target.files[0].size > 2000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage6").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage6").value = '';
            return false;
        }
        document.getElementById("uploadImage6").innerHTML = "";
        return true;
    }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        $('#txtGenerateId').val(OrgDocId);
        fillForm();

        $("#step1").click(function () {
            $.ajax({
                type: "post",
                url: "common/cfOwnershipChangeRequest.php",
                data: "action=GETDETAILS",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    var data = data.trim();
                    if (data == "[]") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Some unfair means has been used and the same has been informed to RKCL." + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>&nbsp; Some unfair means has been used and the same has been informed to RKCL.</span>");
                            window.setTimeout(function () {
                                window.location.href = "logout.php";
                            }, 4000);
                    } else {
                        data = $.parseJSON(data);
                        $("#Organization_Name_old").val(data[0].itgkname);
                        $("#Organization_Type_old").val(data[0].itgktype);
                        $("#Organization_Rspname_old").val(data[0].spname);
                        $("#Organization_Mobile_old").val(data[0].mobile);
                        $("#Organization_Email_old").val(data[0].email);
                        $("#Organization_District_old").val(data[0].district);
                        $("#Organization_Tehsil_old").val(data[0].tehsil);
                        $("#Organization_AreaType_old").val(data[0].areatype);

                        $("#Organization_Name").val(data[0].itgkname);
                        $("#Organization_Type").val(data[0].itgktype);
                        $("#Organization_Rspname").val(data[0].spname);
                        $("#Organization_Mobile").val(data[0].mobile);
                        $("#Organization_Email").val(data[0].email);
                        $("#Organization_District").val(data[0].district);
                        $("#Organization_Tehsil").val(data[0].tehsil);
                        $("#Organization_AreaType").val(data[0].areatype);

                        if (data[0].areatype == 'Urban') {
                            $("#ruralArea_old").hide();
                            $("#ruralArea").hide();

                            $("#Organization_Municipal_Type_old").val(data[0].at1);
                            $("#Organization_Municipal_old").val(data[0].at2);
                            $("#Organization_WardNo_old").val(data[0].at3);

                            $("#Organization_Municipal_Type").val(data[0].at1);
                            $("#Organization_Municipal").val(data[0].at2);
                            $("#Organization_WardNo").val(data[0].at3);
                        } else if (data[0].areatype == 'Rural') {
                            $("#urbanArea_old").hide();
                            $("#urbanArea").hide();

                            $("#Organization_Panchayat_old").val(data[0].at1);
                            $("#Organization_Gram_old").val(data[0].at2);
                            $("#Organization_Village_old").val(data[0].at3);

                            $("#Organization_Panchayat").val(data[0].at1);
                            $("#Organization_Gram").val(data[0].at2);
                            $("#Organization_Village").val(data[0].at3)
                        }

                        $("#Organization_OwnershipType_old").val(data[0].ownertype);
                        $("#Organization_Address_old").val(data[0].address);

                        $("#Organization_OwnershipType").val(data[0].ownertype);
                        $("#Organization_Address").val(data[0].address);


                        if (data[0].creationamount === 'Rural') {
                            $('#areatypecondition').show();
                            $('#panchayat').text(data[0].panchayat);
                            $('#gram').text(data[0].gram);
                        }
                        
            $('#existing_details').show(3000);
            $('#step1').hide(3000);
            $('#step2').show(3000);
                    }
                }
            });
        });
        $("#step2").click(function () {
            $('#impreg').show(3000);
            $('#step2').hide(3000);
        });
        $("#step3").click(function () {
            $('#main-content').show(3000);
            $('#step3').hide(3000);
            $('#step4').show(3000);
        });
        $("#step4").click(function () {
            if ($("#frmownershipchangerequest").valid())
            {
                $('#bank_account').show(3000);
                $('#step4').hide(3000);
            }
            return false;
        });
        $('#chk').change(function () {
            if (this.checked) {
                $('#step5').show();
                $('#chk').attr('disabled', true);
            } else {
                alert("Please select Check Box to Proceed.");
            }
        });
        $("#step5").click(function () {
            var accountno = $("#txtaccountNumber").val();
            var confirmaccountno = $("#txtConfirmaccountNumber").val();

            if (accountno != confirmaccountno) {
                alert("Account No Does not match.");
                return false;
            }
            if ($("#frmownershipchangerequest").valid())
            {
                $('#owner_docs').show(3000);
                $('#step5').hide(3000);
                $('#btnSubmit').show(3000);
            }
            return false;
        });


        function fillForm()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfOwnershipChangeRequest.php",
                data: "action=GETDETAILS",
                success: function (data) {
                    //alert(data);
                    var data = data.trim();
                    $('#response').empty();
                    if (data == "[]") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Your Renewal at MYRKCL is Pending. Kindly complete your Renewal to Apply for Ownership Change." + "</span></p>");
                        $('#step1').hide();
                    } else {
                        $('#step1').show();
                    }
                }
            });
        }


        $("#btnGenerateOTP").click(function () {
            //alert();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfOwnershipChangeRequest.php"; // the script where you handle the form input.            
            var data;
            data = "action=GenerateOTP&emailid=" + txtEmail.value + "&mobno=" + txtMobile.value + ""; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    //alert(data);
                    var data = data.trim();
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>OTP has been sent to your Mobile and Email. Please Enter OTP to Proceed</span></p>");
                        $("#otpverify").show();
                        $('#txtEmail').attr('readonly', true);
                        $('#txtMobile').attr('readonly', true);
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Invalid Details. Please Try again</span></p>");
                    }
                    //showData();


                }
            });
        });

        $("#btnVerify").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfOwnershipChangeRequest.php",
                data: "action=VERIFY&otpemail=" + txtOTPEmail.value + "&otpmobile=" + txtOTPMobile.value + "",
                success: function (data)
                {
                    //alert(data);
                    var data = data.trim();
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $("#btnVerify").hide();
                        $("#btnVerified").show();
                        $('#impreg').hide(3000);
                        $('#otpverify').hide(3000);
                        $('#txtOTPEmail').attr('readonly', true);
                        $('#txtOTPMobile').attr('readonly', true);
                        $('#step3').show(3000);
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                    }
                }
            });
        });

        $("#btnShow").click(function () {
            // alert("1");
            if (txtIfscCode.value == '') {
                alert("Plese enter IFSC Code to Proceed");
            } else {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfOwnershipChangeRequest.php"; // the script where you handle the form input.
                var data;

                data = "action=FILLDETAILS&txtIfscCode=" + txtIfscCode.value + ""; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        var data = data.trim();
                        if (data == 0) {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + "   IFSC Code Does Not Exist." + "</font></span></p>");

                        } else if (data === "Your Account Details is Already Exist. Please Use Edit Bank Details Link to Edit Bank Details.") {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + "   Your Account Details is Already Exist. Please Use Edit Bank Details Link to Edit Bank Details." + "</font></span></p>");

                        } else
                        {
                            $('#response').empty();
                            //alert(data);

                            data = $.parseJSON(data);
                            ddlBankName.value = data[0].bankname;
                            txtMicrcode.value = data[0].micrcode;
                            ddlBranch.value = data[0].branchname;
                            $("#details").show();
                            document.getElementById("ddlBankName").readOnly = true;
                            //document.getElementById("txtMicrcode").readOnly = true;
                            //document.getElementById("ddlBranch").readOnly = true;
                            $('#txtIfscCode').attr('readonly', true);
                            $("#btnShow").hide();
                        }

                    }
                });
            }
        });



        $(function () {
            $("#txtEstdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });

        function FillOrgType() {
            $.ajax({
                type: "post",
                url: "common/cfOrgTypeMaster.php",
                data: "action=FILL",
                success: function (data) {
                    var data = data.trim();
                    $("#txtType").html(data);
                }
            });
        }
        FillOrgType();

        $("#txtType").change(function () {
            var selOrgType = $(this).val();
            //alert(selOrgType);
            if (selOrgType == '1') {
                $("#OrgType1").fadeIn();
            } else {
                $("#OrgType1").hide();
            }
            if (selOrgType == '2') {
                $("#OrgType2").fadeIn();
            } else {
                $("#OrgType2").hide();
            }
            if (selOrgType == '3' || selOrgType == '4') {
                $("#OrgType3").fadeIn();
            } else {
                $("#OrgType3").hide();
            }
            if (selOrgType == '8') {
                $("#OrgType4").fadeIn();
            } else {
                $("#OrgType4").hide();
            }
            if (selOrgType == '5' || selOrgType == '6' || selOrgType == '7' || selOrgType == '9') {
                $("#OrgType5").fadeIn();
            } else {
                $("#OrgType5").hide();
            }

        });


        $("#ddlowntype").change(function () {
            var selOwnType = $(this).val();
            //alert(selOrgType);
            if (selOwnType == 'Rented') {
                $("#Rented").show();
                $("#utibill").show();
            } else {
                $("#Rented").hide();
            }
            if (selOwnType == 'Owned') {
                $("#Owned").show();
                $("#utibill").hide();
            } else {
                $("#Owned").hide();
            }
            if (selOwnType == 'NOC') {
                $("#NOC").show();
                $("#utibill").show();
            } else {
                $("#NOC").hide();
            }

        });

//        function FillParent() {
//            $.ajax({
//                type: "post",
//                url: "common/cfCountryMaster.php",
//                data: "action=FILL",
//                success: function (data) {
//                    $("#ddlCountry").html(data);
//                }
//            });
//        }
//        FillParent();
//
//        $("#ddlCountry").change(function () {
//            var selcountry = $(this).val();
//            //alert(selcountry);
//            $.ajax({
//                url: 'common/cfStateMaster.php',
//                type: "post",
//                data: "action=FILL&values=" + selcountry + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlState').html(data);
//                }
//            });
//        });
//
//
//        $("#ddlState").change(function () {
//            var selState = $(this).val();
//            //alert(selState);
//            $.ajax({
//                url: 'common/cfRegionMaster.php',
//                type: "post",
//                data: "action=FILL&values=" + selState + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlRegion').html(data);
//                }
//            });
//        });
//
//        $("#ddlRegion").change(function () {
//            var selregion = $(this).val();
//            //alert(selregion);
//            $.ajax({
//                url: 'common/cfCenterRegistration.php',
//                type: "post",
//                data: "action=FILLDistrict&values=" + selregion + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlDistrict').html(data);
//                }
//            });
//        });
//
//        $("#ddlDistrict").change(function () {
//            var selDistrict = $(this).val();
//            //alert(selregion);
//            $.ajax({
//                url: 'common/cfTehsilMaster.php',
//                type: "post",
//                data: "action=FILL&values=" + selDistrict + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlTehsil').html(data);
//                }
//            });
//        });
//
//        $("#ddlDistrict").change(function () {
//            var selDistrict = $(this).val();
//            //alert(selregion);
//            $.ajax({
//                url: 'common/cfCenterRegistration.php',
//                type: "post",
//                data: "action=FILLPanchayat&values=" + selDistrict + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlPanchayat').html(data);
//                }
//            });
//        });
//
//        $("#ddlPanchayat").change(function () {
//            var selPanchayat = $(this).val();
//            //alert(selregion);
//            $.ajax({
//                url: 'common/cfCenterRegistration.php',
//                type: "post",
//                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlGramPanchayat').html(data);
//                }
//            });
//        });
//
//        $("#ddlGramPanchayat").change(function () {
//            var selGramPanchayat = $(this).val();
//            //alert(selregion);
//            $.ajax({
//                url: 'common/cfVillageMaster.php',
//                type: "post",
//                data: "action=FILL&values=" + selGramPanchayat + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlVillage').html(data);
//                }
//            });
//        });
//
//        $("#ddlTehsil").change(function () {
//            var selTehsil = $(this).val();
//            //alert(selregion);
//            $.ajax({
//                url: 'common/cfAreaMaster.php',
//                type: "post",
//                data: "action=FILL&values=" + selTehsil + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlArea').html(data);
//                }
//            });
//        });
//
//        function FillMunicipalName() {
//            var selDistrict = ddlDistrict.value;
//            //alert(selDistrict);
//            $.ajax({
//                url: 'common/cfOrgUpdate.php',
//                type: "post",
//                data: "action=FillMunicipalName&values=" + selDistrict + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlMunicipalName').html(data);
//                }
//            });
//        }
//
//        function FillMunicipalType() {
//            var selDistrict = ddlDistrict.value;
//            //alert(selDistrict);
//            $.ajax({
//                url: 'common/cfOrgUpdate.php',
//                type: "post",
//                data: "action=FillMunicipalType",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlMunicipalType').html(data);
//                }
//            });
//        }
//
//        $("#ddlMunicipalName").change(function () {
//            var selMunicipalName = $(this).val();
//            //alert(selregion);
//            $.ajax({
//                url: 'common/cfOrgUpdate.php',
//                type: "post",
//                data: "action=FILLWardno&values=" + selMunicipalName + "",
//                success: function (data) {
//                    //alert(data);
//                    $('#ddlWardno').html(data);
//                }
//            });
//        });

//        function GetAmount()
//        {
//            var NcrFeeCategory = ddlOrgType.value;
//            if (NcrFeeCategory == 'RS-CIT')
//            {
//                if (document.getElementById('areaUrban').checked) //for radio button
//                {
//                    NcrFeeCategory = 'Urban';
//                } else {
//                    NcrFeeCategory = 'Rural';
//                }
//            }
//            $.ajax({
//                type: "post",
//                url: "common/cfOwnershipChangeRequest.php",
//                data: "action=GetNcrAmt&values=" + NcrFeeCategory + "",
//                success: function (data) {
//                    if (NcrFeeCategory == 'Urban') {
//                        jQuery("label[for='ncrfee']").html("NCR Type-Other Than Gram Panchayat AO Registration Amount");
//                        txtncrfee.value = data;
//                    } else if (NcrFeeCategory == 'Rural') {
//                        jQuery("label[for='ncrfee']").html("NCR Type-Gram Panchayat AO Registration Amount");
//                        txtncrfee.value = data;
//                    } else if (NcrFeeCategory == 'RS-CFA') {
//                        jQuery("label[for='ncrfee']").html("RS-CFA AO Registration Amount");
//                        txtncrfee.value = data;
//                    }
//                }
//            });
//        }



//        function findselected() {
//
//            var UrbanDiv = document.getElementById("areaUrban");
//            var category = document.getElementById("Urban");
//
//            if (UrbanDiv) {
//                Urban.style.display = areaUrban.checked ? "block" : "none";
//                Rural.style.display = "none";
//                GetAmount();
//                $('#showfee').show();
//            } else
//            {
//                Urban.style.display = "none";
//            }
//        }
//        function findselected1() {
//
//
//            var RuralDiv = document.getElementById("areaRural");
//            var category1 = document.getElementById("Rural");
//
//            if (RuralDiv) {
//                Rural.style.display = areaRural.checked ? "block" : "none";
//                Urban.style.display = "none";
//                GetAmount();
//                $('#showfee').show();
////                jQuery("label[for='ncrfee']").html("NCR Type-Gram Panchayat AO Registration Amount");
////                txtncrfee.value = "24000";
//            } else
//            {
//                Rural.style.display = "none";
//            }
//        }
//        $("#areaUrban").change(function () {
//            findselected();
//            FillMunicipalName();
//            FillMunicipalType();
//        });
//        $("#areaRural").change(function () {
//            findselected1();
//            FillPanchayat();
//        });

        $("#frmownershipchangerequest").submit(function () {
            //alert("1");
            if ($("#frmownershipchangerequest").valid())
            {
                $('#btnSubmit').hide();
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfOwnershipChangeRequest.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmownershipchangerequest").serialize();
//alert(forminput);
                data = forminput; // serializes the form's elements.
                var file_data7 = $("#uploadImage7").prop("files")[0];
                var file_data8 = $("#uploadImage8").prop("files")[0];
                var file_data9 = $("#uploadImage9").prop("files")[0];
                var file_data6 = $("#uploadImage6").prop("files")[0];// Getting the properties of file from file field
                var form_data = new FormData(this);                  // Creating object of FormData class
                form_data.append("panno", file_data7)
                form_data.append("aadharno", file_data8)
                form_data.append("addproof", file_data9)
                form_data.append("appform", file_data6)// Appending parameter named file with properties of file_field to form_data
                form_data.append("action", "ADD")
                form_data.append("data", data)
//alert(form_data);
                $.ajax({

                    url: url,
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data, // Setting the data attribute of ajax with file_data
                    type: 'post',
                    success: function (data)
                    {
                        //alert(data);
                        var data = data.trim();
                        if (data === "Successfully Inserted")
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Application for Ownership Transfer has been submitted Successfully.</span>");
                            window.setTimeout(function () {
                                window.location.href = "frmownershipchangerequest.php";
                            }, 3000);

                            Mode = "Add";
                            resetForm("frmownershipchangerequest");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                            $('#btnSubmit').show();
                        }



                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmownershipchangerequest_validation.js"></script>

<script src="bootcss/js/frmbankaccount_validation.js" type="text/javascript"></script>	



<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>