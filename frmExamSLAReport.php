<?php
$title = "Exam Event Wise ITGK SLA Report";
include ('header.php');
include ('root_menu.php');
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Exam Event Wise ITGK SLA Report</div>
            <div class="panel-body">
                <form name="frmreexampayment" id="frmreexampayment" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-md-6 form-group">
                            <label for="batch"> Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="batch"> Report For:<span class="star">*</span></label>
                            <select id="reportFor" name="reportFor" class="form-control">
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" style="margin-top:25px;" value="Get Report"/>    
                        </div>
                        <?php if ($_SESSION['User_UserRoll'] != 7 && $_SESSION['User_UserRoll'] != 14) { ?>
                        <div class="col-md-6 form-group">
                            <input type="submit" name="btnDownload" id="btnDownload" class="btn btn-primary" value="Download" style="display:none;margin-top:25px;"/>    
                        </div>
                        <?php } ?>
                    </div> 
                    <div id="grid" name="grid" style="margin-top:35px;"> 
                    </div> 
            </div>
        </div>   
    </div>
</form>

</body>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<script type="text/javascript">
    $(document).ready(function () {
        getAllExamEvents();
        fillReportForTypes();

        $("#ddlExamEvent").change(function () {
            $('#btnDownload').hide();
            $("#grid").html('');
        });

        $("#reportFor").change(function () {
            $('#btnDownload').hide();
            $("#grid").html('');
        });

        $('#btnDownload').click(function () {
            $('.approvalLetter').click();
        });

        $('#btnSubmit').click(function () {
            $("#grid").html('');
            $('#btnDownload').hide();
            getitgkexamslareport(ddlExamEvent.value);
        });
    });

    function fillReportForTypes() {
        $.ajax({
            type: "post",
            url: "common/cfExamSLAReport.php",
            data: "action=fillReportForTypes",
            success: function (data) {
                $("#reportFor").html(data);
            }
        });
    }

    function getAllExamEvents() {
        $.ajax({
            type: "post",
            url: "common/cfExamSLAReport.php",
            data: "action=FILLEXAMEVENT",
            success: function (data) {
                $("#ddlExamEvent").html(data);
            }
        });
    }

    function getitgkexamslareport(examid) {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfExamSLAReport.php",
            data: "action=itgkexamslareport&reportFor=" + reportFor.value + "&examid=" + examid,
            success: function (data) {
                if (data != '') {
                    $('#response').empty();
                    $('#btnDownload').show();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'print'
                        ],
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                } else {
                    $('#btnDownload').hide();
                }
            }
        });   
    }

</script>
</body>

</html>