<?php
$title = "Create Login";
include ('header.php');
include ('root_menu.php');

?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Create Login</div>
            <div class="panel-body">
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="responseSubmit"></div>

                        </div>        
                        <div id="errorBox"></div>
<div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Select Type:<span class="star">*</span></label>
                                <select id="seltype" name="seltype" class="form-control  text-uppercase" >
                                    <option value="">Select </option>
                                    
                                    <option value="DEO">DEO</option>
                                    <option value="DPO">DPO</option>
                                </select>    
                            </div>
                            
<!--                            <div class="col-sm-4 form-group"> 
                                 <label for="batch"> Select District:<span class="star">*</span></label>
                                    <select id="ddlDistrict" name="ddlDistrict" class="form-control">

                            </select>-->
                            </div>
                            
                           
                                                     
                           
			 
                        </div>
                        
                    </div>

                    <div class="container">
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" />    
                    </div>                  


            </div>
        </div>   
    </div>
</form>



</body>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        
        function FillDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FILL",
                success: function (data) {
                    //data = $.parseJSON(data);
                    $("#ddlDistrict").html(data);
                }
            });
        }
        FillDistrict();

        $("#btnSubmit").click(function () {//alert("hello");
        
               
	if ($("#form").valid())
           {			    
            $('#responseSubmit').empty();
            $('#responseSubmit').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfCreateLogin.php"; // the script where you handle the form input.txtEmpDOB
            var data;
            data = "action=ADD&txtype="+ seltype.value + ""; //&txtDistrict="+ ddlDistrict.value +"serializes the form's elements.
            
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   //alert(data);
                    if (data == 'NOTP')
                    {
                        $('#responseSubmit').empty();
                        $('#rOTP').empty();
                        $('#rOTP').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  Please Enter OTP First.</span></p>");
                    }
                    else if (data == 'InvalidOTP')
                    {
                        $('#responseSubmit').empty();
                        $('#rOTP').empty();
                        $('#rOTP').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  Invalid OTP. Please Try Again.</span></p>");
                    }
                    else
                    {
                        $('#responseSubmit').empty();
                        $('#rOTP').empty();
                        $('#btnValidate').hide(3000);
                        $('#btnValidateOTP').hide(3000);
                        $('#btnSubmit').show(3000);
                        $('#txtEmpMobile').attr('readonly', true);
                        $('#txtBankAccount').attr('readonly', true);
                        //$('#otptext').show(3000);
                        //$('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  " + data + "</span></p>");
                    }
                    //showData();


                }
            });
		 }
            return false; // avoid to execute the actual submit of the form.
        });

    });    

</script>
</body>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script>

$("#form").validate({
        rules: {
            
			seltype: { required: true}
        },			
        messages: {				
			seltype: { required: '<span style="color:red; font-size: 12px;">Please Select Type.</span>' }
			
        },
	});

</script>

<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>