<?php session_start(); 
$title = "Profile";
include ('header.php');
include ('root_menu.php'); 
    
        if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
        ?>
		<div style="min-height:400px;max-height:1500px">
<div class="container" style="margin-top:50px">

      <div class="row">
      <div class="col-md-5  toppad  pull-right col-md-offset-3 ">
         
      </div>
        <div class="col-xs-12">
		
   
          <div class="panel panel-primary" >
            <div class="panel-heading">
             User Profile
            </div>
            <div class="panel-body">
              <div class="row">
			  
				
				<div id="gird"></div>
				
				
             
                 
                </div>
              </div>
            </div>
                 <div class="panel-footer">
                        <a class="btn btn-sm btn-primary" type="button" data-toggle="tooltip" data-original-title="Broadcast Message"><i class="glyphicon glyphicon-envelope"></i></a>
                        <span class="pull-right">
                            <a class="btn btn-sm btn-warning" type="button" data-toggle="tooltip" data-original-title="Edit this user" href="edit.html"><i class="glyphicon glyphicon-edit"></i></a>
                            <a class="btn btn-sm btn-danger " type="button" data-toggle="tooltip" data-original-title="Remove this user"><i class="glyphicon glyphicon-remove"></i></a>
                        </span>
                    </div>
            
          </div>
        </div>
      </div>
    </div>
	</body>
	<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {



       if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>frmprofiledisplay.php</p>");
            $.ajax({
                type: "post",
                url: "common/cfProfileMaster.php",
                data: "action=DELETE&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmprofile.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("profile");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        
        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfProfileMaster.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);

                }
            });
        }

        showData();


     
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcorrection_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
.btn-primary {
    background-color: #428bca;
    border-color: #357ebd;
    color: #ffffff;
    margin-left: 20px !important;
}
</style>
</html> 