<script>
$(document).ready(function(){
    var fheight = $("#footer").height();
    var bodyheight = $("#main_content").height();
    var wheight = window.innerHeight;
    var finaltop = wheight - fheight-15;
    if(bodyheight < finaltop){
        $("#footer").css('top',finaltop);
        
    }else{
        $("#footer").css('top',bodyheight);
        
    }
    //$("#main_content").css('margin-bottom',fheight+'px');
    //$("#footer").css('top',bodyheight);
    //alert(fheight);
    $( window ).resize(function() {
        var fheight = $("#footer").height();
         var bodyheight = $("#main_content").height();
        var wheight = window.innerHeight;
        var finaltop = wheight - fheight-15;
        if(bodyheight < finaltop){
            $("#footer").css('top',finaltop);
        
        }else{
            $("#footer").css('top',bodyheight);

        }
        //$("#main_content").css('margin-bottom',fheight+'px');
        //$("#footer").css('top',bodyheight);
        //alert(fheight);
    }); 
});

$( document ).ajaxComplete(function() {
  var fheight = $("#footer").height();
    var bodyheight = $("#main_content").height();
    var wheight = window.innerHeight;
    var finaltop = wheight - fheight-15;
    if(bodyheight < finaltop){
        $("#footer").css('top',finaltop);
        
    }else{
        $("#footer").css('top',bodyheight);
        
    }
});

</script>


</div>
<footer  id="footer" class="footer-distributed">
    <div class="footer-limiter">
        <div class="footertext"> The programs and data stored on this system are licensed to or are the property of RKCL. This is a private computing system for use only by authorized users. Unauthorized access to any program or data on this system is not permitted. By accessing and using this system you are consenting to system monitoring for law enforcement and other purposes. Unauthorized use of this system may subject you to criminal prosecution and penalties.</div>
    
			<div class="footer-left">

				<!--<h3>Company<span>logo</span></h3>

				<p class="footer-links">
					<a href="#">Home</a>
					·
					<a href="#">Blog</a>
					·
					<a href="#">Pricing</a>
					·
					<a href="#">About</a>
					·
					<a href="#">Faq</a>
					·
					<a href="#">Contact</a>
				</p>-->

				<p class="footer-company-name">
				&copy; RKCL 2008-2017 | Best viewed in 1024 * 728 Resolution |
				
				</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>7A Jhalana Institutional Area , Behind R.T.O Jaipur -302004</span></p>
				</div>

				<!--<div>
					<i class="fa fa-phone"></i>
					<p>+1 555 123456</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">support@company.com</a></p>
				</div>-->

			</div>

			<div class="footer-right">

			<!--	<p class="footer-company-about">
					<span> <a href="" class="white"> Downloads </a>   <a href="" style="margin-left:25px;" class="white"> About Us </a> </span>
					
				</p> -->

			<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook" style="padding-top: 5px;" ></i></a>
					<a href="#"><i class="fa fa-twitter" style="padding-top: 5px;"></i></a>
					<a href="#"><i class="fa fa-linkedin" style="padding-top: 5px;"></i></a>
					<a href="#"><i class="fa fa-github" style="padding-top: 5px;"></i></a>

				</div>

			</div>
    </div>
</footer>
		
		<!-- Modal -->
  <div class="container">
  <!-- Trigger the modal with a button -->
  <div class="modal fade in" id="myStrictModal" role="dialog" style="background-color:#666;opacity:0.95;text-align:left;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header btn-primary ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="strictmodal-title" style="text-align:left;">Important Information</h4>
        </div>
        <div class="modal-body">
          <p id='strictpop' style="text-align:left;"><input type="hidden" value="' + data.currentUrl + '" name="checkonsubmit" id="checkonsubmit"></p>
        </div>
        <div class="modal-footer btn-primary ">
          <input type="hidden" name="goto" id="goto">
          <button type="button" class="btn btn-default" id="gotoBtn" data-dismiss="modal">OK</button>
        </div>
      </div>
      
    </div>
  </div>
  </div>
<?php 
	echo "<script type='text/javascript'>var currentUrl = '" . $_SERVER['PHP_SELF'] . "'</script>";
?>
<script type="text/javascript">
    $(document).ready(function () {

    	$('#gotoBtn').click(function () {
    		var gotourl = $('#goto').val();
    		if (gotourl != '') {
    			window.location.href = gotourl;
    		}
    	});

    	$(":submit").click(function () {
    		var parentFormID = $(this).closest("form").attr("id");
    		var isChecked = 0;
			var chkbx = 0;
			$('#' + parentFormID).find('input:checkbox').each(function() {
				chkbx = 1;
			});
			if (chkbx == 1) {
	    		$("#" + parentFormID + " input:checkbox:checked").each(function() {
				    isChecked = 1;
				});
    		}
			if (isChecked == 1 && $('#' + parentFormID).valid()) {
    			removePopUpOnSubmit();
    		}
    	});

        function PoPupInformation()
        {
            $.ajax({
                type: "post",
                url: "common/cfonlineusers.php",
                data: "action=getFixedPopUpContent&curl=" + currentUrl + "",
                success: function (data) {
		            if (data != '0') {
			        	data = $.parseJSON(data);
			        	if (data.currentUrl != currentUrl) {
				        	$('.strictmodal-title').html(data.subject);
				            $('#strictpop').html(data.content);
				            $('#goto').val(data.Link);
				            $("#myStrictModal").show();
			        	} else {
			        		$('#checkonsubmit').val(data.currentUrl);
			        	}
			        }
                }
            });
        }
    	PoPupInformation();

    	function removePopUpOnSubmit() {
    		var submiturl = $('#checkonsubmit').val();
    		$.ajax({
                type: "post",
                url: "common/cfonlineusers.php",
                data: "action=getFixedPopUpContent&curl=" + currentUrl + "&submiturl=" + submiturl + "",
                success: function (data) {
		            if (data != '0') {
			        	
			        }
                }
            });
    	}

    });
</script>