<?php
$title="Buy Red Hat Learning Subscription(RHLS)";
include ('header.php'); 
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
require("razorpay/checkout/manual.php");
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '7'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}
?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:auto !important;">	
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Buy Red Hat Learning Subscription(RHLS)</div>
                <div class="panel-body">
				 <form name="frmredhatsubscription" id="frmredhatsubscription" class="form-inline" role="form"
					enctype="multipart/form-data">
										
					<div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>
							
					<div id="errorBox"></div>
					
					<div id="valid">	
						<div class="col-sm-4 form-group">     
							<label for="correction">Select Subscription Quantity:</label>
								<select id="ddlQty" name="ddlQty" class="form-control">
										<option value="">Select</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
								</select>
						</div> 						
							
						<div class="col-sm-4 form-group">     
							<label for="batch"> Select Payment Mode:</label>
							<select id="paymode" name="paymode" class="form-control" onchange="toggle_visibility1('online_mode');">

							</select>									
						</div> 
					
						<div class="col-sm-4 form-group">     
							<label for="batch"> Select Payment Gateway:</label>
							<select id="gateway" name="gateway" class="form-control">									   
								<option value="razorpay">Razorpay</option>
								<option value="payu">Payu</option>								
							</select>
						</div>
					</div>					
							<input type="hidden" name="amounts" id="amounts" class="form-control"/>  
							
					</div>
					
                    <div id="gird" style="margin-top:10px;"> </div>                   
                 
					<div class="container" id="submit" style="display:none;">
						<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
					</div>
				</div>
            </div>   
        </div>
    </form>
</div>
   
	<form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
        <input type="hidden" id="txnid" name="txnid">
    </form>

  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
		
<script type="text/javascript">
        $(document).ready(function () {
			
			function checkcenterauthorization() {
				$('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
               
                $.ajax({
                    type: "post",
                    url: "common/cfBuyRedHatSubscription.php",
                    data: "action=checkauthorization",
                    success: function (data) {
						$('#response').empty();
                        if(data=='1'){
							//$("#valid").show();
						}
						 else{
							$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " You are not authorized for Red Hat Courses." + "</span></p>");
						 }
                    }
                });
            }
            //checkcenterauthorization();			
			
			
			$("#ddlQty").change(function(){				
				showpaymentmode();
                $("#gird").html('');
                $("#submit").hide();
                $("#paymode").html('');
                $.ajax({					
                    type: "post",
                    url: "common/cfBuyRedHatSubscription.php",
                    data: "action=Fee&qty=" + ddlQty.value + "",
                    success: function (data) {
						if (data==0) 
						{
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Something went wrong." + "</span></p>");
						} 
						else if(data=='blanck'){
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Please select subscription quantity." + "</span></p>");
						}
						else{
							amounts.value = data;	
						}
                    }
                });
			});			
			
			function showpaymentmode() {				
                $("#gird").html('');
                $("#submit").hide();
                $("#paymode").html('');
				$.ajax({
					type: "post",
					url: "common/cfBuyRedHatSubscription.php",
					data: "action=ShowPayMode",
					success: function (data) {
						if(data=='0'){
							$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Event is not enabled." + "</span></p>");
						}
						else{
							$("#paymode").html(data);
						}
						
					}
				});
			}
		
    		$("#paymode").change(function () {
				var mode = $('#paymode').val();
					if(mode==''){
						$("#submit").hide();
					}
					else{
						$("#submit").show();
					}    			
    		});

            
			$("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBuyRedHatSubscription.php"; // the script where you handle the form input.
                var data;
                var forminput=$("#frmredhatsubscription").serialize();
                
				if (Mode == 'Add') {
                    data = "action=ADD&" + forminput;
                    $('#txnid').val('');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data) {	
                            $('#response').empty();							
                            if (data == 0 || data == '') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Please try again, also ensure that, you have selected atleast one checkbox." + "</span></p>");
                            } else if (data != '') {
                               if (data == 'TimeCapErr') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                    alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                            } 
							else if(data == 'done'){
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Payment Already Done." + "</span></p>");
							}
							
							else if(data == 'duplicate'){
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "  Something went wrong." + "</span></p>");
							}
							else if(data == 'empty'){
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "  Please fill all details." + "</span></p>");
							}
								else {
									if ($('#gateway').val() == 'razorpay') {
										var options = $.parseJSON(data);
										<?php include("razorpay/razorpay.js"); ?>
									}
									else {
										$('#txnid').val(data);
										$('#frmpostvalue').submit();
									}									
								}
                            }
                        }
                    });
                }

                return false; // avoid to execute the actual submit of the form.
            });

        });

    </script>
</html>