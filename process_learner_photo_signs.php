<?php
    require 'DAL/classconnection.php';
    $_ObjConnection = new _Connection();
    $_ObjConnection->Connect();
	    ini_set('display_errors', 1);
     error_reporting(E_ALL);
	 
	ini_set("memory_limit", "5000M");
	
    ini_set("max_execution_time", 0);
    set_time_limit(0);

    //Example Request Url: 
    //http://10.1.1.10/copy_learner_photos_for_vmou.php?job=1&code=1219
    //http://localhost/myrkcl/process_learner_photo_signs.php?code=1223&job=1&copy=1&copysign=1
    $job = $_GET['job'];
    $start = ($job - 1) * 50000;
    $end = 50000;

    $examId = $_GET['code'];
    $isCopy = $_GET['copy'];
    $isCopySign = (isset($_GET['copysign'])) ? $_GET['copysign'] : 0;

    //Get Photo/Sign of Eligible Learners
    $selectQuery = "SELECT el.learnercode, el.coursename, el.batchname, CONCAT(el.learnercode, '_photo.png') AS photo, CONCAT(el.learnercode, '_sign.png') AS sign, REPLACE(el.examdate, ' ', '') AS examDateDir FROM tbl_finalexammapping el WHERE el.examid = $examId LIMIT $start, $end";

    // print $selectQuery = "SELECT el.learnercode, el.coursename, el.batchname, CONCAT(el.learnercode, '_photo.png') AS photo, CONCAT(el.learnercode, '_sign.png') AS sign, REPLACE(el.examdate, ' ', '') AS examDateDir FROM tbl_finalexammapping_temp el WHERE el.examid = $examId AND el.coursename LIKE('%gov%') AND el.batchname LIKE('%April 2018%')";

    $result = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);
    $report = [];
    $learnercodes = [];
    $subdirName = '';

    while ($row = mysqli_fetch_array($result[2])) {

        $subdirName = $row['examDateDir'];

        $learnerphoto = getPath('photo', $row);
        $learnersign = getPath('sign', $row);

        $copyPhoto = $copySign = '';
        $dirpath = $general->getDocRootDir() . '/upload/permission_letters/photos/' . $subdirName . '/';
        makeDir($dirpath);

        $newPhotoPath = $dirpath . $row['photo'];
        if (!file_exists($newPhotoPath)) {
            //$learnercodes[] = $row['learnercode'];
            if(file_exists($learnerphoto)) {
                $copyPhoto = $learnerphoto;
            } else {
                $learnercodes['photo'][] = $row['learnercode'];
            }
            if (!empty($copyPhoto)) {
                //makeDir($dirpath);
                if ($isCopy == 1) {
                    if (! copy($copyPhoto, $newPhotoPath) ) {
                        $learnercodes['photo'][] = $row['learnercode'];
                    }
                }
            } else {
                $learnercodes['photo'][] = $row['learnercode'];
            }
        }

        $dirpath = $general->getDocRootDir() . '/upload/permission_letters/signs/' . $subdirName . '/';
        makeDir($dirpath);

        $newPhotoPath = $dirpath . $row['sign'];
        if ($isCopySign == 1 && !file_exists($newPhotoPath)) {
            //$learnercodes[] = $row['learnercode'];
            if(file_exists($learnersign)){
                $copySign = $learnersign;
            } else {
                $learnercodes['sign'][] = $row['learnercode'];
            }

            if (!empty($copySign)) {
                //makeDir($dirpath);
                if ($isCopySign == 1) {
                    if ($isCopy && ! copy($copySign, $newPhotoPath) ) {
                        $learnercodes['sign'][] = $row['learnercode'];
                    }
                }
            } else {
                $learnercodes['sign'][] = $row['learnercode'];
            }
        }
    }

    if ($isCopy == 1) {
        print "Files are copied on bellow Paths:<br />" . $general->getDocRootDir() . '/upload/permission_letters/photos/' . $subdirName . '/' . "<br /><br />" . $general->getDocRootDir() . '/upload/permission_letters/signs/' . $subdirName . '/' . "<br /><br />";
    }

	if (isset($learnercodes['photo'])) {
		$learnercodes['photo'] = array_unique($learnercodes['photo']);
		print 'Total Photo Not Found: ' . count($learnercodes['photo']) . "<br /><br />";
	}
	if (isset($learnercodes['sign'])) {
		$learnercodes['sign'] = array_unique($learnercodes['sign']);
		print 'Total Sign Not Found: ' . count($learnercodes['sign']) . "<br /><br />";
	}

    print_r($learnercodes);


    function makeDir($path)
    {
         return is_dir($path) || mkdir($path);
    }

    function getbatchname($row) {
        $batchname = trim(ucwords($row['batchname']));
        $coursename = $row['coursename'];
        $isGovtEmp = stripos($coursename, 'gov');
        if ($isGovtEmp) {
            $batchname = $batchname . '_Gov';
        } else {
            $isWoman = stripos($coursename, 'women');
            if ($isWoman) {
                $batchname = $batchname . '_Women';
            } else {
                $isMadarsa = stripos($coursename, 'madar');
                if ($isMadarsa) {
                    $batchname = $batchname . '_Madarsa';
                }
            }
        }
        $batchname = str_replace(' ', '_', $batchname);

        return $batchname;
    }

    function getPath($type, $row) {
        global $general;
        $batchname = getbatchname($row);

        $dirPath = '/upload/admission_' . $type . '/' . $row['learnercode'] . '_' . $type . '.png';

        $imgPath = $general->getDocRootDir() . $dirPath;
        $imgPath = str_replace('//', '/', $imgPath);

        $dirPathJpg = str_replace('.png', '.jpg', $dirPath);
        $imgPathJpg = str_replace('.png', '.jpg', $imgPath);

        $imgBatchPath = $general->getDocRootDir() . '/upload/admission' . $type . '/' . $batchname . '/' . $row['learnercode'] . '_' . $type . '.png';
        $imgBatchPath = str_replace('//', '/', $imgBatchPath);
        $imgBatchPathJpg = str_replace('.png', '.jpg', $imgBatchPath);

        $path = '';
        if (file_exists($imgBatchPath)) {
            $path = $imgBatchPath;
        } elseif (file_exists($imgBatchPathJpg)) {
            $path = $imgBatchPathJpg;
        } elseif (file_exists($imgPath)) {
            $path = $imgPath;
        } elseif (file_exists($imgPathJpg)) {
            $path = $imgPathJpg;
        } else {
            $imgPath = 'http://' . $_SERVER['HTTP_HOST'] . $dirPath;
            if(@getimagesize($imgPath)) {
                $path = $imgPath;
            } else {
                $path = 'http://' . $_SERVER['HTTP_HOST'] . $dirPathJpg;
            }
        }

        return $path;
    }

?>