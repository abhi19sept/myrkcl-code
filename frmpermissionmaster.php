<?php
$title="User Permission";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var PermissionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var PermissionCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {
?>
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">User Role Master</div>
                <div class="panel-body">
                    <form name="frmpermissionmaster" id="frmpermissionmaster" class="form-inline" role="form" enctype="multipart/form-data">
						<div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
								
								
									<div class="col-sm-4 form-group">     
										<label for="entity">Entity Name:</label>
										<select id="ddlEntity" name="ddlEntity" class="form-control">

                                        </select>
									</div> 
								</div>
								
								<div class="container">
									<div class="col-sm-4 form-group">     
										<label for="name">Role Name:</label>
										<select id="ddlUserRole" name="ddlUserRole" class="form-control">

										</select>										
									</div> 
								</div>
								
								<div class="container">
									<div class="col-sm-4 form-group">     
										<label for="name">Root Menu:</label>
										<select id="ddlRootMenu" name="ddlRootMenu" class="form-control">

										</select>										
									</div> 
								</div>
								
								<div id="menuList" name="menuList" style="margin-top:35px;"> </div> 
								
								<div class="container" style="width:100%">
									<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
								</div>
                    </div>
            </div>   
        </div>
    </form>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
                        
        <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            
            function FillRole(entity) {
                
                $.ajax({
                    type: "post",
                    url: "common/cfUserRoleMaster.php",
                    data: "action=FILLBYENTITY&entity=" + entity,
                    success: function (data) {
                        //alert(data);
                        $("#ddlUserRole").html(data);
                    }
                });
            }

            FillRole();
            function FillEntity() {
                $.ajax({
                    type: "post",
                    url: "common/cfUserRoleMaster.php",
                    data: "action=FILLENTITY",
                    success: function (data) {
                        $("#ddlEntity").html(data);
                    }
                });
            }

            FillEntity();
			
			function FillRootMenu() {
                $.ajax({
                    type: "post",
                    url: "common/cfRootMenu.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlRootMenu").html(data);
                    }
                });
            }

            FillRootMenu();
            
           function showAllData(val,val1) {
              // alert(val);
                $.ajax({
                    type: "post",
                    url: "common/cfPermissionMaster.php",
                    data: "action=SHOWALL&parent="+ val +"&userrole="+ val1 +"",
                    success: function (data) {
						//alert(data);
                        $("#menuList").html(data);

                    }
                });
            }
           //showAllData();
           
            function showData(val) {
                
                $.ajax({
                    type: "post",
                    url: "common/cfPermissionMaster.php",
                    data: "action=SHOW&values="+val,
                    success: function (data) {

                        $("#gird").html(data);

                    }
                });
            }
			
			  function showChildData(val) {
                alert(val);
                $.ajax({
                    type: "post",
                    url: "common/cfPermissionMaster.php",
                    data: "action=SHOWCHILD&child="+val,
                    success: function (data) {
						alert(data);
                        $("#gird").html(data);

                    }
                });
            }
			
			
            $("#ddlEntity").change(function(){
                
                FillRole(this.value);
            });
            
            $("#ddlUserRole").change(function(){
                //showChildData(this.value);
            });
			
			 $("#ddlRootMenu").change(function(){
                showAllData(this.value,ddlUserRole.value);
            });


            $("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfPermissionMaster.php"; // the script where you handle the form input.
                var data;
                var forminput=$("#frmpermissionmaster").serialize();
                //alert(forminput);
				
				
				
				
                if (Mode == 'Add')
                {
                    data = "action=ADD&" + forminput; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href="frmpermissionmaster.php";
                            }, 1000);

                            Mode="Add";
                            resetForm("frmpermissionmaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
    </body>
    
</html>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>