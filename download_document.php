<?php

    if ($_SERVER['HTTP_HOST'] == "localhost") {
        $imageFolder = $_SERVER['DOCUMENT_ROOT'] . "/myrkcl/upload/".$_REQUEST['flag']."/";
    } else {
        $imageFolder = $_SERVER['DOCUMENT_ROOT'] . "/upload/".$_REQUEST["flag"]."/";
    }

    $path = $imageFolder . str_rot13($_REQUEST['usercode']) . "/" . str_rot13($_REQUEST['id']);

    header("Content-Description: File Transfer");
    header("Content-Type: image/jpg");
    header("Content-Disposition: attachment; filename=" . basename($path) . "");

    readfile($path);
    exit();