<?php
$title = "Learner Bio-Metric Deregister";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var UserCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>		

<div style="min-height:430px !important;max-height:auto !important;"> 
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Enter Learner Code and Submit</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmLearnerCapture" id="frmLearnerCapture" class="form-inline" action="" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                    </div>

                    <div class="container">
                        <div class="container" id="SelectedDetails" style="display:none;">
                            <div id="response"></div>
                            <div class="col-md-4 form-group">     
                                <img id="imgFinger" name="imgFinger"  class="form-control" style="width:100px;height:100px;" alt="" />								 
                            </div>

                        </div> 

                        <div id="success" style="margin-left:250px !important;"></div>

                        <div id="errorBox"></div>

                    </div>


                    <div id="errorBox" style="display:block;">	
                        <div class="container">	
                            <div class="col-md-3 "> 
                            </div>
                            <div class="col-md-3 ">     
                                <label for="biopin">Learner Code:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtLCode" id="txtLCode" placeholder="Learner Code">

                            </div>
                            <div class="col-sm-3 ">                                  
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="margin-top:25px"/>    
                            </div>
                            <div class="col-md-3 "> 
                            </div>
                        </div>
                    </div>
	
                    <div id="learnerinfo" class="container" style="display:none;">
                        <br>
                        <div align="center" class="col-md-2 col-lg-2">
                            <img id="my_image" style="width:80px; height:107px" class="img-circle img-responsive" />
                        </div>
                        <div class="table-responsive col-md-8 col-lg-8">
                            <table class="table table-bordered table-user-information">
                                <tbody>
                                    <tr>
                                        <td >Learner Code:</td>
                                        <td id="learnercode">  </td>
                                    </tr>
                                    <tr>
                                        <td >Learner Name:</td>
                                        <td id="learnername">  </td>
                                    </tr>
                                    <tr>
                                        <td >Father Name</td>
                                        <td id="fathername"> </td>
                                    </tr>
                                    <tr>
                                        <td >Date of Birth:</td>
                                        <td id="dob">  </td>
                                    </tr>
									<tr>
                                        <td >ITGK Code</td>
                                        <td id="itgkcode"> </td>
                                    </tr>
                                    <tr>
                                        <td >Course Name</td>
                                        <td id="coursename"> </td>
                                    </tr>
                                    <tr>
                                        <td >Batch Name</td>
                                        <td id="batchname"> </td>
                                    </tr>
                            </table>
                        </div>

                    </div>
						<div class="container" name="divderegreason" id="divderegreason" style="margin-top:20px; margin-left:15px;display:none;">	
                         <div class="col-md-8">     
                                <label for="biopin">Deregister Reason:<span class="star">*</span></label>

                                <textarea class="form-control" maxlength="50" name="txtderegreason" id="txtderegreason" placeholder="Reason" required="required" oninvalid="setCustomValidity('Please Enter Reason')"
                                    onchange="try {
                                        setCustomValidity('')
                                    } catch (e) {
                                    }"></textarea>

                            </div>
                        <div class="col-md-3 ">     
                            <input type="button" name="btnDeregister" id="btnDeregister" class="btn btn-primary"  value="Deregister Learner" /> 	
                        </div>				 
                    </div>

                </form>

            </div>
        </div>   
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script src="js/jquery-1.8.2.js"></script>
<script src="js/mfs100.js"></script>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        var quality = 60; //(1 to 100) (recommanded minimum 55)
        var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )



        $("#btnSubmit").click(function () {
            //alert("hhkj");		 
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
            var data;


            data = "action=GetLearnerByLID&lcode=" + txtLCode.value + "";
            //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {

                    //alert(data);
					if (data == 1){
								$('#response').empty();
								$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Invalid LearnerCode." + "</span></div>");
                            
							}
					else {
						$('#response').empty();
						data = $.parseJSON(data);
                    document.getElementById("learnercode").innerHTML = data[0].lcode;
                    document.getElementById("learnername").innerHTML = data[0].lname;
                    document.getElementById("fathername").innerHTML = data[0].lfname;
                    document.getElementById("dob").innerHTML = data[0].ldob;
                    document.getElementById("coursename").innerHTML = data[0].lcourse;
                    document.getElementById("batchname").innerHTML = data[0].lbatch;
                    document.getElementById("itgkcode").innerHTML = data[0].itgkcode;
                    var imsrc = data[0].lphoto;
                    //src="upload/admission_photo/ "
                    $("#my_image").attr("src", "upload/admission_photo/" + imsrc);
                    //learnercode.value = data[0].AdmissionCode;
                    $('#response').empty();
                    $("#learnerinfo").show();
                    $("#divderegreason").show();
					}
                    

                }
            });

            return false; // avoid to execute the actual submit of the form.
        });



        $("#btnDeregister").click(function () {
				$('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
                var data;


                 data = "action=LearnerDeregister&lcode=" + txtLCode.value + "&deregreason=" + txtderegreason.value + "";
                //alert(data);
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmLearnerBioMetricDeregister.php';
                            }, 1000);
                        }
                        else
                        {
                            var data2 = "Already Exist";
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data2 + "</span></div>");
                        }


                    }
                });

                return false; // avoid to execute the actual submit of the form.
        });

 
    });
</script>
</html>