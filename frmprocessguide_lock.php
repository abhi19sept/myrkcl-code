﻿<?php
$title = "Process Guides";
include ('header.php');
include ('root_menu.php');
?>
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">Downloads</div>
        <div class="panel-body">
            <div class='col-md-12 col-lg-12'>
                <div class="panel panel-info">
                    <div class="panel-heading">Techno Business Workshop PPTs</div>
                    <div class="panel-body">
                        <table class='table table-user-information table table-hover'>
                            <tbody>			
                                <tr>
                                    <th class="col-md-8 col-lg-8">Presentation Name:</th>
                                    <th class="col-md-2 col-lg-2" id='ifsccode'>Download PDF</th>
                                </tr>			 
                                <tr>
                                    <td class="col-md-10 col-lg-10">Biometric Installation Guide(Mantra/Startek):</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/Biometric.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>	    <tr>
                                    <td class="col-md-10 col-lg-10">CLICK Portal Guide</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CLICK_PROGRAM.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">LMS New Updates:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LMS_Meeting_Content.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>			
                                    <td class="col-md-10 col-lg-10">Support Management System Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/Support_Management_System.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">Learner Login Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/User_learner_guide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">WCD Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/WCD.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> 
                <div class="panel panel-info">
                    <div class="panel-heading">Service Provider Technical Orientation Meeting PPTs</div>
                    <div class="panel-body">


                        <table class='table table-user-information table table-hover'>
                            <tbody>
							
							
							
							
                                <tr>
                                    <th class="col-md-8 col-lg-8">Presentation Name:</th>
                                    <th class="col-md-2 col-lg-2" id='ifsccode'>Download PDF</th>
                                </tr>
								
								 <tr>
                                    <td class="col-md-10 col-lg-10">Govt. Reimbursement Process Guide for ITGKs</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/reimitgkguide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                           
						    <tr>
                                    <td class="col-md-10 col-lg-10">Govt. Reimbursement Process Guide for Learners</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/reimlearnerguide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                              			
								
								
								

                                <tr>
                                    <td class="col-md-10 col-lg-10">RKCL LMS User Guide (Updated):</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/RKCLLMSUserguide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
								
                                    <td class="col-md-10 col-lg-10">Service Provider Technical Orientation Meeting PPT:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/MYRKCL_SP.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">IT-GK Orientation by SPs PPT:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/ITGKOrientationbySPs.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                </div> 
                <div class="panel panel-info">
                    <div class="panel-heading">RKCL LMS Process Guide</div>
                    <div class="panel-body">


                        <table class='table table-user-information table table-hover'>
                            <tbody>
                                <tr>
                                    <th class="col-md-8 col-lg-8">Process Guide Name:</th>
                                    <th class="col-md-2 col-lg-2" id='ifsccode'>Download PDF</th>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">RKCL LMS User Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/RKCLLMSUserguide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">RKCL LMS Troubleshooting Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LMStroubleshooting.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">RKCL LMS Patch 1 (updated on 22-05-2017):</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LMSPatch1.zip" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
                                </tr>
								
								<tr>
                                    <td class="col-md-10 col-lg-10">RKCL LMS - Frequently Asked Questions (FAQs) - In English</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LMSFAQsEnglish.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
                                </tr>
								
								<tr>
                                    <td class="col-md-10 col-lg-10">RKCL LMS - Frequently Asked Questions (FAQs) - In Hindi</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LMSFAQsinHindi.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
                                </tr>
                              <!--  <tr>
                                    <td class="col-md-10 col-lg-10">RKCL LMS Patch 2 (updated on 15-09-2016):</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LMSPatch2.zip" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
                                </tr>
								-->
                            </tbody>
                        </table>


                    </div>
                </div>  
                <div class="panel panel-info">
                    <div class="panel-heading">RS-CIT Book PDF V 1.0</div>
                    <div class="panel-body">


                        <table class='table table-user-information table table-hover'>
                            <tbody>
                                <tr>
                                    <th class="col-md-8 col-lg-8">Chapter Name:</th>
                                    <th class="col-md-2 col-lg-2" id='ifsccode'>Download PDF</th>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">1. Introduction to Computers:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/IntroductiontoComputers.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">2. Computer System:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/ComputerSystem.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">3. Uses of Computers:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/UsesofComputers.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">4. Introduction to Internet:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/IntroductiontoInternet.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">5. Internet Applications:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/InternetApplications.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">6. Operating System:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/OperatingSystem.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">7. MS - Word Basics:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/MSWordBasics.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">8. MS - Word Advanced:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/MSWordAdvanced.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">9. MS - Excel Basics:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/MSExcelBasics.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">10. MS Excel Advanced:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/MSExcelAdvanced.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">11. MS PowerPoint Basics:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/MSPowerPointBasics.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">12. MS Access:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/MSAccess.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">13. Microsoft Outlook Basics:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/MicrosoftOutlookBasics.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">14. Latest Trends in IT:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LatestTrendsinIT.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">15. Computer Administration:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/ComputerAdministration.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">16. Computer Networking:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/ComputerNetworking.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                </div>                  
                <div class="panel panel-info">
                    <div class="panel-heading">MYRKCL Process Guides</div>
                    <div class="panel-body">

                        <table class='table table-user-information table table-hover'>
                            <tbody>

                                <tr>
                                    <th class="col-md-8 col-lg-8">Process Guide Name:</th>
                                    <th class="col-md-2 col-lg-2" id='ifsccode'>Download PDF</th>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">Online Payment Help Manual (updated on 22 April 2017)</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/RS-CITPaymentProcess1.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">NCR Help Manual:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/NCRProcess.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
								 <tr>
                                    <td class="col-md-10 col-lg-10">Service Provider Change Guidelines:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/ServiceProviderChangeGuidelines1.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
								
								
                                <tr>
                                    <td class="col-md-10 col-lg-10">Service Provider Selection Help Manual:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/ServiceProviderSelectionHelpManual.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">Add System and Update Intake Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/IntakeCapacityGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">Apply EOI Process Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/ApplyEOIProcessGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">EOI Payment Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/EOIPaymentGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">EOI Application Report:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/EOIApplicationReport.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">EOI Payment Transaction Report:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/EOIPaymentTransactionReport.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">Learner Admission Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LearnerAdmissionGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">Learner Admission Modify Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LearnerAdmissionModifyGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>
                                <tr>
                                    <td class="col-md-10 col-lg-10">Learner Admission Payment Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LearnerAdmissionPaymentGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Learner Verification Report Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LearnerVerificationReportGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>


                                <tr>
                                    <td class="col-md-10 col-lg-10">Biometric Software Installation Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/BiomatricSoftwareInstallationguide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>


                                <tr>
                                    <td class="col-md-10 col-lg-10">Biometric Registration Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/BiomatricRegistrationGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>


                                <tr>
                                    <td class="col-md-10 col-lg-10">Biometric Learner Enrollment Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/BiomatricLearnerEnrollmentGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Biometric Learner Attendance Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/BiomatricLearnerAttendanceGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Biometric Attendance Report Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/BometricAttendanceReportGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Adding Bank Details Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/AddingBankDetailsGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Correction Application Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CorrectionApplicationGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Correction Application Payment Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CorrectionApplicationPaymentGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Correction Application Report:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CorrectionApplicationReport.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Correction Payment Learner Wise Report:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CorrectionPaymentLearnerWiseReport.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Correction Transaction Summary:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CorrectionTransactionSummary.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Govt Emp Application Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/GovtEmpApplicationGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>

                                <tr>
                                    <td class="col-md-10 col-lg-10">Govt Emp Report Guide:</td>
                                    <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/GovtEmpReportGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
                                </tr>





                            </tbody>
                        </table>

                    </div>
                </div>   

            </div>

            <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal">×</button>
                            <h3 class="modal-title">Heading</h3>
                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>   
    </div>
</div>
</body>
<style>
    .modal-dialog {width:600px;}
    .thumbnail {margin-bottom:6px;}
</style>
<script>
    $(document).ready(function () {
        $('.thumbnail').click(function () {
            $('.modal-body').empty();
            var title = $(this).parent('a').attr("title");
            $('.modal-title').html(title);
            $($(this).parents('div').html()).appendTo('.modal-body');
            $('#myModal').modal({show: true});
        });
    });
</script>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>


</body>

</html>