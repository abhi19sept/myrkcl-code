<?php
$title = "IT-GK Passbook";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='Show'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:1500px !important;">
<div class="container"> 
    

    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading">IT-GK Passbook</div>
        <div class="panel-body">					
            <form name="frmpaymentrefund" id="frmpaymentrefund" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>
                    </div>        
                    <div id="errorBox"></div>

                     <div class="container">					
                    <div >
                        <label for="bydate" class="radio-inline"> 
                    <input name="radiobuttonPeriod" type="radio" id="bydate" title="bydate" value="radio1" checked="checked" >                            
                    By Date
                    </label> 
                    
                    <label for="bymonth" class="radio-inline"> 
                        <input type="radio" name="radiobuttonPeriod" value="radio2" title="bymonth" id="bymonth" >By Month
                    </label>

                    <label for="last6month" class="radio-inline">
                        
                            <input type="radio" name="radiobuttonPeriod" value="radio3" title="last6month" id="last6month" >
                             Last 6 Months
                    </label> 
                        
                    <label for="byfinancialyear" class="radio-inline">
                            <input type="radio" name="radiobuttonPeriod" value="radio4" title="byfinancialyear" id="byfinancialyear" >
                             FY (PPF Account)
                    </label>
                                    <!-- <div class="container"> -->
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View Report"/>    
                <!-- </div> -->
                </div>
                </div>
                <br>
                
                <div class="container" id="divbydate">
                    <div class="col-sm-4 form-group"> 
                        <label for="sdate">Start Date:</label>
                        <span class="star">*</span>
                        <input type="text" class="form-control" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="DD-MM-YYYY">     
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="edate">End Date:</label>    
                        <span class="star">*</span>
                        <input type="text" class="form-control" readonly="true" name="txtenddate" id="txtenddate"  placeholder="DD-MM-YYYY" value=" <?php echo date("d-m-Y"); ?>">
                    </div>

                    <div class="col-sm-4 form-group"> 
                          
                    </div>
                </div>
 
        
                <div class="container" id="divbymonth" style="display: none;">
                    <div  class="radio2 box">
                    <div class="">
                        <label for="amount" class="control-label col-md-1 col-sm-1 col-xs-3"> Year
                        </label>

                        <div class="col-md-2 col-sm-3 col-xs-6">

                            <select name="comboYear" title="year" id="year" class="form-control" >
                                <script>
                                    /*  var minYearAccSt = document.displayForm.minYearAccSt.value; */
                                    for(i='2019';i<=new Date().getFullYear();i++)
                                      document.write("<option selected='"+new Date().getFullYear()+"' value='" + i + "'>"+i+"</option>");                       
                                </script>
                               
                            </select>
                        </div>
                    </div>


                    <div class="">
                        <label for="amount" class="control-label col-md-1 col-sm-1  col-xs-3"> Month</label>
                        <div class="col-md-2 col-sm-3 col-xs-6">
                            <select name="comboMonth" title="month" id="month" class="form-control" >
                                <option value="01">
                                    January
                                </option>
                                <option value="02">
                                    February
                                </option>
                                <option value="03">
                                    March
                                </option>
                                <option value="04">
                                    April
                                </option>
                                <option value="05">
                                    May
                                </option>
                                <option value="06">
                                    June
                                </option>
                                <option value="07">
                                    July
                                </option>
                                <option value="08">
                                    August
                                </option>
                                <option value="09">
                                    September
                                </option>
                                <option value="10">
                                    October
                                </option>
                                <option value="11">
                                    November
                                </option>
                                <option value="12">
                                    December
                                </option>
                            </select>

                        </div>
                    </div>
                </div>
                </div>

                
                <div class="container" id="divbyfyear" style="display: none;">
                    <div class="radio4 box col-lg-2 col-xs-6 col-sm-3">
                    <select name="comboFinancialYear" title="financialyear" id="financialyear" class="form-control" >

                        <script>
                          var currentYear =  new Date().getFullYear();
                          var nextYear = new Date().getFullYear() - 1;  
                          var startYear = new Date().getFullYear() - 3; 
                          for(i=startYear;i<=currentYear;i++){
                            var nextGenYear = i + 1;
                            if(currentYear >= nextGenYear){
                                var dispYear = i + "-" + nextGenYear;
                                document.write("<option selected='"+nextYear + "-" + currentYear + "' value='" + dispYear + "'>"+dispYear+"</option>");
                            }
                          }                     
                        </script>
                        
                    </select>
                </div>
                </div>
                </div>  
               <div id="grid" style="margin-top:35px; width:94%;"> </div>      
        </div>
    </div>   
</div>
</form>

</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style type="text/css">
    
    table.dataTable tbody tr.myeven{
    background-color:#f2dede;
    }
    table.dataTable tbody tr.myodd{
        background-color:#bce8f1;
    }
</style>

<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
       // autoclose: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script>	

<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $("#txtstartdate, #txtenddate").datepicker();
    $("#txtenddate").change(function () {
        var txtstartdate = document.getElementById("txtstartdate").value;
        var txtenddate = document.getElementById("txtenddate").value;
        if ((Date.parse(txtenddate) <= Date.parse(txtstartdate))) {
            alert("End date should be greater than Start date");
            document.getElementById("txtenddate").value = "";
        }
    });

    $(document).ready(function () {
        // $("#divbyfyear").hide();
        //  $("#divbymonth").hide();
          $("#divbydate").show();
        var rpttype=""
        $(function() {
            $(document).on('change', '[name="radiobuttonPeriod"]' , function(){
             rpttype = $('[name="radiobuttonPeriod"]:checked').val();
           // alert(val);
                if(rpttype === "radio4"){
                    $("#divbyfyear").show();
                    $("#divbymonth").hide();
                     $("#divbydate").hide();
                }
                else if(rpttype === "radio2"){
                    $("#divbyfyear").hide();
                    $("#divbymonth").show();
                     $("#divbydate").hide();
                }
                else if(rpttype === "radio3"){
                    $("#divbyfyear").hide();
                    $("#divbymonth").hide();
                    $("#divbydate").hide();
                }
                else{
                    $("#divbydate").show();
                    $("#divbyfyear").hide();
                    $("#divbymonth").hide();
                }
              }); 
            });

        $("#btnSubmit").click(function () {
            
           
               // alert("hi");
                $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
				setTimeout(function(){$('#grid').load();}, 2000); 

                var startdate = $('#txtstartdate').val();
                var enddate = $('#txtenddate').val();
                var month = $('#month').val();
                var year = $('#year').val();
                var financialyear = $('#financialyear').val();

                var url = "common/cfITGKPassbook.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Show')
                {
                    data = "action=SHOW&startdate=" + startdate + "&enddate=" + enddate +  "&month=" + month +  "&year=" + year +  "&financialyear=" + financialyear +  "&rpttype=" + rpttype + ""; // serializes the form's elements.				 
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                         $('#response').empty();   
                            $("#grid").html(data);
                            $('#example').DataTable({



        "rowCallback": function( row, data, index ) {
            if(index%2 == 0){
                $(row).removeClass('myodd myeven');
                $(row).addClass('myodd');
            }else{
                $(row).removeClass('myodd myeven');
                 $(row).addClass('myeven');
            }
          },




                                 "ordering": false,
                        dom: 'Bfrtip',
                    "columnDefs": [
            {
                "targets": [0],
                "visible": false
               
            }
       
        ],
                        buttons: [

          {                                                                                
           extend: 'pdf',
           
           exportOptions: {
                columns: [1,2,3,4,5,6]
            }
       },

       {
           extend: 'excel',
          exportOptions: {
                columns: [1,2,3,4,5,6]
            }
       }  

                        ],
                        "scrollY":        "400px",
                        "scrollCollapse": true,
                        "paging":         false,
                          exportOptions: {
                columns: [1,2,3,4,5,6]
            }
                    });
                       

                        //showData();
                    }
                });
           
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
</html>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmadmissiontrans_valid.js" type="text/javascript"></script>	