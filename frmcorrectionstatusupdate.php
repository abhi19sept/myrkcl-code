<?php
$title = "Correction/Duplicate Certificate Form Status Update";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}

if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258' 
|| $_SESSION['User_Code'] == '6588') {
//print_r($_SESSION);
?>

<link rel="stylesheet" href="bootcss/css/bootstrap-datetimepicker.min.css">
    <script src="bootcss/js/moment.min.js"></script>
    <script src="bootcss/js/bootstrap-datetimepicker.min.js"></script>

<div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">Correction/Duplicate Certificate Form Status Update</div>
        <div class="panel-body">
            <form name="frmcorrectionstatus" id="frmcorrectionstatus" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>			
                    
					 <div class="col-sm-4 form-group">     
                        <label for="batch"> LearnerCode:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="18" name="txtlcode" id="txtlcode"
							placeholder="LearnerCode" required="true" onkeypress="javascript:return allownumbers(event);">				
                    </div>
					
                </div>                

                <div class="container">
                 	<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show"/> 
                </div>
				
				<div id="menuList" name="menuList" style="margin-top:35px;"> </div> 
        </div>
    </div>   
</div>
</form>
</div>

<div id="LearnerDetails" class="modal" style="padding-top:50px !important">            
	<div class="modal-content" style="width: 90%;">
		<div class="modal-header">
			<span class="close mm">&times;</span>
				<h6>Update Learner Process Status</h6>
		</div>
		<div class="modal-body" style="max-height: 500px;">
		<form name="frmprocess" id="frmprocess" class="form-inline" role="form" enctype="multipart/form-data">
			<div id="responses"></div>
				<div class="container">
				<div class="col-sm-3 form-group"> 
					  <label for="exam_event" style="float:left;">Process Status:<span class="star">*</span></label>
					  <input type="text" readonly="true" class="form-control" name="pstatus" id="pstatus"
						value="Delivered to Learner">    
				</div> 
				
				<div class="col-sm-5"> 
					<label for="pan" style="float:left;">Remark:<span class="star">*</span></label>
					<input type="text" class="form-control"  name="txtRemark" id="txtRemark"  placeholder="Remark" required="true">
					<input type="hidden" class="form-control"  name="txtcid" id="txtcid"  value="">
				</div>
				
				<div class="col-sm-3 form-group"> 
						  <label for="dob">Delivered Date:<span class="star">*</span></label>
					<input type="text" class="form-control" id="txtdate" name="txtdate" placeholder="Delivered Date" required="true">
				</div>
				
				<div class="col-sm-3"  style="float:left;"> 
				<input type="button" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Update" style="margin-top:24px;"/> 
				</div>
					
                </div>
		</form>		
				
		</div>
	</div>
</div>

</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<style>
.error{
 color:#F00;
 }
</style>
<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>

<script type="text/javascript"> 
  
$('#txtdate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            widgetPositioning: {horizontal: "auto", vertical: "bottom"}
        });
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showLearnerData(val) {
		 if ($("#frmcorrectionstatus").valid())
           {	
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
					type: "post",
					url: "common/cfCorrectionStatusUpdate.php",
					data: "action=ShowDetails&lcode=" + val + "",
					success: function (data) {
						$('#response').empty();
						$("#menuList").html(data);
						$('#example').DataTable();
						//$("#btnSubmit").hide();
						$('#txtlcode').attr('readonly', true);
					}
				});
			}
            return false;
        }	
		
        $("#btnSubmit").click(function () {			
			showLearnerData(txtlcode.value);			   
        });
		
		$("#menuList").on('click', '.UpdateStatus',function(){
			var cid = $(this).attr('id');
			$('#txtcid').val(cid);
			var modal = document.getElementById('LearnerDetails');
					var span = document.getElementsByClassName("mm")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";						
					}	
		});
		
		
		$("#btnSubmit1").click(function () {			
			if ($("#frmprocess").valid())
           {			   
			$.ajax({
				type: "post",
				url: "common/cfCorrectionStatusUpdate.php",
				data: "action=UpdateLearnerStatus&cid="+txtcid.value+"&reason="+txtRemark.value+"&delvdate="+txtdate.value+"",
				success: function (data) {
					if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#responses').empty();
                        $('#responses').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmcorrectionstatusupdate.php";
                        }, 2000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#responses').empty();
                        $('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }											
					
				}
			});
			}
            return false;
        });
        
    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		
</body>

</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>