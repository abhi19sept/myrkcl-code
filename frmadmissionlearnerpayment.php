<?php
$title = "Admission Payment";
include ('header.php');
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
require("razorpay/checkout/manual.php");

?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Learner Fee Payment</div>
            <div class="panel-body">
                <form name="frmfeepayment" id="frmfeepayment" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
					<div class="col-sm-4 form-group"> 
                        <label for="lcode">Learner Code:<span class="star">*</span></label>
                        <input type="text" class="form-control"  name="txtlcode"  id="txtlcode" placeholder="Learner Code"
							value="<?php echo $_SESSION['User_LearnerCode']?>" readonly="true" required="required"/>
                    </div> 
					
				<span id="afterverify" style="display:none;">				
					
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Payment Mode:</label>
                            <select id="paymode" name="paymode" class="form-control">

                            </select>
                        </div> 
                    
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Payment Gateway:</label>
                            <select id="gateway" name="gateway" class="form-control">                               
								<option value="razorpay">Razorpay</option>
								<option value="payu">Payu</option>						
                            </select>
                        </div>
				</div>
                   
				</span>
						<input type='hidden' name="ddlcourse" id="ddlcourse">
						<input type='hidden' name="ddlbatch" id="ddlbatch">
						<input type="hidden" name="amounts" id="amounts"/>
						<input type="hidden" name="txtitgk" id="txtitgk"/>
                    
                <div id="menuList" name="menuList" style="margin-top:5px;"> </div> 
				
				 <div class="container">
                        <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"
								style="display:none;"/>    
                    </div>
					
                <div class="container">
                    <input type="button" name="btnverify" id="btnverify" class="button" value="Verify Details"/>    
                </div>
			
            </div>
        </div>   
    </div>
</form>
</div>
<form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
    <input type="hidden" id="txnid" name="txnid">
</form>

</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<style>
.button{
	color: #ffffff; background-color: #428bca; border-color: #357ebd; display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.428571429;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
</style>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {        
		
	$("#btnverify").click(function (){
		$('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		$.ajax({
                type: "post",
                url: "common/cfAdmissionLearnerPayment.php",
                data: "action=verifydetails&lcode=" + txtlcode.value + "",
                success: function (data) {
					$('#response').empty();
					if(data==0){
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Please Enter Learner Code." + "</span></p>");
					}
					else if(data==1){
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " You are not authorized for payment." + "</span></p>");
					}
					else if(data==2){
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Payment already done for this learner." + "</span></p>");
					}
					else{
						data = $.parseJSON(data);
						$("#afterverify").show();
						$("#btnverify").hide();
						$("#txtlcode").attr("readonly",true);
						ddlcourse.value = data[0].Admission_Course;
						ddlbatch.value = data[0].Admission_Batch;
						txtitgk.value = data[0].Admission_ITGK_Code;
						amounts.value = data[0].RKCL_Share;
						showpaymentmode(ddlbatch.value, ddlcourse.value);
					}
                    
                }
            });
	});
       
        function showpaymentmode(val, val1) {
            $('#btnSubmit').hide();
            $("#paymode").html('');
            $.ajax({
                type: "post",
                url: "common/cfEvents.php",
                data: "action=SHOWAdmissionPay&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#paymode").html(data);						
                }
            });
        } 
		
		$("#paymode").change(function () {
            showAllData(ddlbatch.value, ddlcourse.value, paymode.value, txtlcode.value);
        });
		
		function showAllData(val, val1, val2, val3) {
            $("#menuList").html('');
            $('#btnSubmit').hide();
            $.ajax({
                type: "post",
                url: "common/cfAdmissionLearnerPayment.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "&paymode=" + val2 + "&lcode=" + val3 + "",
                success: function (data) {
                   // alert(data);
                    $("#menuList").html(data);
                    $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                    $('#btnSubmit').show();
                }
            });
        }

       
        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $('#btnSubmit').hide();
            var url = "common/cfAdmissionLearnerPayment.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmfeepayment").serialize();
            //alert(forminput);

            if (Mode == 'Add') {
                data = "action=ADD&" + forminput;
                $('#txnid').val('');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
						$('#response').empty();
                        data = data.trim();
                        if (data == 0 || data == '') {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Please try again, also ensure that, you have selected atleast one checkbox." + "</span></p>");
                        } else if (data == 'TimeCapErr') {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                        } else {
                            if ($('#gateway').val() == 'razorpay') {
                                var options = $.parseJSON(data);
                                <?php include("razorpay/razorpay.js"); ?>
                            } else {
                                $('#txnid').val(data);
                                $('#frmpostvalue').submit();
                            }
                        }
                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.
        });

    });

</script>
</body>

</html>