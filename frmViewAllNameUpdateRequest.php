<?php

    $title = "Name Update Request";
    include('header.php');
    include('root_menu.php');
    include "common/modals.php";

    if ($_SESSION["User_UserRoll"] == 11 || $_SESSION["User_UserRoll"] == 4) {

        echo "<script>window.location.href='frmNameUpdateRequestCenterDetail.php';</script>";
        die;

        ?>
        <div style="min-height:430px !important;max-height:1500px !important;">
            <div class="container">

                <div class="panel panel-primary" style="margin-top:36px !important;">
                    <div class="panel-heading">Update Name Requests raise by ITGK Centres

                    </div>
                    <div class="panel-body">
                        <div id="response"></div>
                        <div id="gird"></div>

                    </div>

                </div>
            </div>

        </div>
        <?php
        include 'common/message.php';
        include 'footer.php';

        ?>
        <style type="text/css">
            div.dt-buttons{
                margin-bottom: 10px;
            }
        </style>
        <script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                function showData() {
                    var url = "common/cfmodifyITGK.php";
                    var data;
                    data = "action=showAddressUpdateRequestTable";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data) {
                            showDatatable(data);
                        }
                    });
                }

                function showDatatable(data) {
                    $("#gird").html(data);
                    $("#example").DataTable({
                        dom: 'Bfrtip',
                        "bProcessing": true,
                        "serverSide": true,
                        "ajax": {
                            url: "common/cfmodifyITGK.php",
                            data: {
                                "action": "showNameUpdateRequest"
                            },
                            type: "POST"
                        },
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ],
                        columnDefs: [
                            {
                                targets: 1,
                                render: function (data, type, row, meta) {
                                    if (type === 'display') {
                                        data = '<a href="frmNameUpdateRequestCenterDetail.php?Code=' + encryptString(data) + '" target="_blank">' + data + ' ('+row[3]+')'+ '</a>';
                                    }
                                    return data;
                                }
                            }
                        ]
                    });
                }

                showData();
            });
        </script>
        </html>
        <?php
    } else {
        echo "<script>$('#unauthorized').modal('show')</script>";
        die;
    }