<?php

/**
 * Description of clsgetpassword
 *
 * @author Abhishek
 */
require 'DAL/classconnection.php';

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$_ObjConnection = new _Connection();

function syncRecordsForEligibleLearners($examId, $examDate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();

        $userId = (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 7) ? $_SESSION['User_LoginId'] : '';
        //Insert Re Exam Learners
        insertReExamLearners($examId, $examDate, $userId);

        $allowedFreshBatches = "SELECT Affilate_FreshBaches, Affilate_ReexamBatches FROM tbl_exammaster WHERE Affilate_Event = '" . $examId . "'";
        $_Response = $_ObjConnection->ExecuteQuery($allowedFreshBatches, Message::SelectStatement);
        $allowedBatches = mysqli_fetch_array($_Response[2]);

        //Process to Insert Previous Batches Learners
        $_Response = getPreviousBatches($examId, $allowedBatches['Affilate_ReexamBatches']);
        $filter = (!empty($userId)) ? " AND ta.Admission_ITGK_Code = '" . $userId . "'" : '';
        while ($batch = mysqli_fetch_array($_Response[2])) {
            print '<p>&nbsp;</p>';
        print $detailsSql = "SELECT '', ( SELECT Event_Name FROM tbl_events WHERE Event_Id = '" . $examId . "' ) AS Event_Name, '" . $examId . "' AS examId , ta.Admission_LearnerCode, ta.Admission_Name, ta.Admission_Fname, ta.Admission_DOB, cm.Course_Name, bm.Batch_Name, ta.Admission_ITGK_Code, od.Organization_Name, dm.District_Name, tm.Tehsil_Name, 'PreviousBatch' AS batchType, 'Eligible' AS status, '". $examDate . "' AS examDate, 'Eligible' AS remark
            FROM tbl_admission ta INNER JOIN tbl_learner_score ls ON ls.Learner_Code = ta.Admission_LearnerCode 
            INNER JOIN tbl_course_master cm ON cm.Course_Code = ta.Admission_Course 
            INNER JOIN tbl_batch_master bm ON bm.Batch_Code = ta.Admission_Batch 
            INNER JOIN tbl_user_master um ON um.User_LoginId = ta.Admission_ITGK_Code 
            INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code 
            INNER JOIN tbl_district_master dm ON dm.District_Code = ta.Admission_District 
            INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = ta.Admission_Tehsil 
            WHERE ta.Admission_Payment_Status = 1 AND ta.Admission_LearnerCode NOT IN (SELECT learnercode FROM tbl_eligiblelearners WHERE eventid = '" . $examId . "') AND ls.Score >= 11 AND ta.Admission_Batch = " . $batch['Admission_Batch'] . " " . $filter;
            $_AdmResponse = $_ObjConnection->ExecuteQuery($detailsSql, Message::SelectStatement);
            while ($row = mysqli_fetch_array($_AdmResponse[2])) {
                $countOldMapping = "SELECT count(*) AS n FROM tbl_finalexammapping_collective WHERE learnercode = '" . $row['Admission_LearnerCode'] . "'";
                $_MapResponse = $_ObjConnection->ExecuteQuery($countOldMapping, Message::SelectStatement);
                $mapCount = mysqli_fetch_array($_MapResponse[2]);
                if (empty($mapCount['n'])) {
                    $values = "('" . $row['Event_Name'] . "', '" . $row['examId'] . "', '" . $row['Admission_LearnerCode'] . "', '" . $row['Admission_Name'] . "', '" . $row['Admission_Fname'] . "', '" . $row['Admission_DOB'] . "', '" . $row['Course_Name'] . "', '" . $row['Batch_Name'] . "', '" . $row['Admission_ITGK_Code'] . "', '" . $row['Organization_Name'] . "', '" . $row['District_Name'] . "', '" . $row['Tehsil_Name'] . "', '" . $row['batchType'] . "', '" . $row['status'] . "', '" . $row['examDate'] . "', '" . $row['remark'] . "')";
                    print '<p>&nbsp;</p>';
                    print $insertFreshLearners = "INSERT IGNORE INTO tbl_eligiblelearners (eventname, eventid, learnercode, learnername, fathername, dob, coursename, batchname, itgkcode, itgkname, itgkdistrict, itgktehsil, remark, status, examdate, Reason) VALUES " . $values;
                    $_ObjConnection->ExecuteQuery($insertFreshLearners, Message::InsertStatement);
                }
            }
        }

        //Process to Insert Fresh Batches Learners
        $_Response = getPreviousBatches($examId, $allowedBatches['Affilate_FreshBaches']);
        if (mysqli_num_rows($_Response[2])) {
            while ($batch = mysqli_fetch_array($_Response[2])) {
                insertFreshLearners($examId, $examDate, $batch['Admission_Batch'], $userId);
            }
        }
    }

    function insertReExamLearners($examId, $examDate, $userId = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $filter = (!empty($userId)) ? " AND ed.itgkcode = '" . $userId . "'" : '';
        print '<p>&nbsp;</p>';
        print $insertReExamLearners = "INSERT IGNORE INTO tbl_eligiblelearners (
                SELECT '', ev.Event_Name, ed.examid, ed.learnercode, ed.learnername, ed.fathername, ed.dob,
    cm.Course_Name, bm.Batch_Name, ed.itgkcode, od.Organization_Name, dm.District_Name, tm.Tehsil_Name, 'Re-exam', 'Eligible', '". $examDate . "', 'Eligible' FROM examdata ed 
                INNER JOIN tbl_events ev ON ev.Event_Id = ed.examId
                INNER JOIN tbl_course_master cm ON cm.Course_Code = ed.coursename
                INNER JOIN tbl_batch_master bm ON bm.Batch_Code = ed.batchname
                INNER JOIN tbl_user_master um ON um.User_LoginId = ed.itgkcode
                INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code
                INNER JOIN tbl_district_master dm ON dm.District_Code = ed.itgkdistrict
                INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = ed.itgktehsil
                WHERE ed.examid = '" . $examId . "' AND ed.paymentstatus = 1 AND ev.Event_Status = 1 ". $filter . "
        )";
        $_ObjConnection->ExecuteQuery($insertReExamLearners, Message::InsertStatement);
    }

    function insertFreshLearners($examId, $examDate, $batchId, $userId = '') {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $filter = (!empty($userId)) ? " AND ta.Admission_ITGK_Code = '" . $userId . "'" : '';
        print '<p>&nbsp;</p>';
        print $insertFreshBatchLearners = "INSERT IGNORE INTO tbl_eligiblelearners (
            SELECT '', ( SELECT Event_Name FROM tbl_events WHERE Event_Id = '" . $examId . "' ) AS Event_Name, " . $examId . " AS examId , ta.Admission_LearnerCode, ta.Admission_Name, ta.Admission_Fname, ta.Admission_DOB, cm.Course_Name, bm.Batch_Name, ta.Admission_ITGK_Code, od.Organization_Name, dm.District_Name, tm.Tehsil_Name, 'FreshBatch' AS batchType, 'Eligible' AS status, '". $examDate . "' AS examDate, 'Eligible' AS remark
            FROM tbl_admission ta INNER JOIN tbl_learner_score ls ON ls.Learner_Code = ta.Admission_LearnerCode 
            INNER JOIN tbl_course_master cm ON cm.Course_Code = ta.Admission_Course 
            INNER JOIN tbl_batch_master bm ON bm.Batch_Code = ta.Admission_Batch 
            INNER JOIN tbl_user_master um ON um.User_LoginId = ta.Admission_ITGK_Code 
            INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code 
            INNER JOIN tbl_district_master dm ON dm.District_Code = ta.Admission_District 
            INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = ta.Admission_Tehsil 
            WHERE ta.Admission_Batch = " . $batchId . " " . $filter . " AND ta.Admission_Payment_Status = 1 AND ls.Score >= 11
        )";
        $_ObjConnection->ExecuteQuery($insertFreshBatchLearners, Message::InsertStatement);
    }

    function getPreviousBatches($examId, $batchIds) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        print '<p>&nbsp;</p>';
        print $previousAdmissionBatches = "SELECT distinct Admission_Batch FROM tbl_admission WHERE Admission_Batch IN (0" . $batchIds . ") AND Admission_LearnerCode NOT IN (SELECT learnercode FROM tbl_eligiblelearners WHERE eventid = '" . $examId . "') ORDER BY Admission_Batch";
        
        return $_ObjConnection->ExecuteQuery($previousAdmissionBatches, Message::SelectStatement);
    }

    syncRecordsForEligibleLearners(1217, '2017-07-16');