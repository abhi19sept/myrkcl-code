<style type="text/css">

    #printable { display: none; }

    @media print
    {
        #non-printable { display: none; }
        #printable { display: block; }
    }
</style> 
<div>
    <?php
    $title = "LIV Report";
    include ('header.php');
    //include ('root_menu.php');


    if (isset($_REQUEST['code'])) {
        echo "<script>var UserCode=" . $_REQUEST['code'] . "</script>";
        echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        echo "<script>var pdfprint='pdfprint'</script>";
    } else {
        echo "<script>var UserCode=0</script>";
        echo "<script>var Mode='Add'</script>";
        echo "<script>var pdfprint='pdfprint'</script>";
    }
    ?>
</div>


<div class="container"> 
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">Learner Information Verification Report</div>
        <div class="panel-body">
            <div>
                <div>
                    <p>Click below button to print LIVR.</p>

                    <!--<button onclick="myFunction()">Print Receipt</button>-->
                    <div class="container">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Print"/>    
                    </div>
<!--
                    <script>
                        function myFunction() {
                            window.print();
                        }
                    </script>-->
                </div>
            </div>


            <div id='receipt'></div>

        </div>
    </div>
</div>
<div id="non-printable">
    <?php include'footer.php'; ?>
    <?php include'common/message.php'; ?>
</div>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {
        //alert("hi");
        var url = "common/cfLIVRrpt.php"; // the script where you handle the form input.

        $.ajax({
            type: "POST",
            url: url,
            data: "action=PRINT&values=" + UserCode + "",
            success: function (data)
            {
               
                $('#receipt').html(data);

            }
        });
        $("#btnSubmit").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            window.open("printLIVRpdf.php?action=" + pdfprint + "&&code=" + UserCode, '_blank');

            // window.open("visitdetailsgetdatapdf.php?code="+codevalue+"&&date="+datevalue, '_blank');

            return false; // avoid to execute the actual submit of the form.
        });

        return false; // avoid to execute the actual submit of the form.

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</html>