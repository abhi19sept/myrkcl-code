<?php
$title = "IT-GK SLA Upload for Renewal";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_UserRoll'] == '11' || $_SESSION['User_UserRoll'] == '1') {
	
	date_default_timezone_set('Asia/Kolkata');
    $filename = date("Y-m-d_H-i-s");
    //echo "<script>var filea='" . $filename . "'</script>";
    ?>
    <link rel="stylesheet" href="css/datepicker.css"/>
    <script src="scripts/datepicker.js"></script>
    <div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 


            <div class="panel panel-primary" style="margin-top:36px !important;">  
                <div class="panel-heading">IT-GK SLA Upload for Renewal</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> --> 
                    <form name="frmuploadslaexcel" id="frmuploadslaexcel" class="form-inline" role="form" enctype="multipart/form-data">    
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>
								
						</div>			
								
                        <div class="container"> 

                            <div class="col-sm-4 form-group">  
                                <label>Download Sample File:</label>
                                <a href="sample_file_ITGK_Renewal.xlsx"> <input type="button" value="Download File" name="download">  </a>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="ecl">Upload IT-GK Data:<span class="star">*</span></label>
                                <input type="file" class="form-control" id="_file" name="_file" accept=".csv"/>
								<input type="hidden" id="txtfilename" name="txtfilename" class="form-control" value="<?php echo $filename; ?>">
                                <span style="font-size:10px;">Note : .xlsx Allowed Max Size =50MB</span>
                                <span class="btn submit" id="fileuploadbutton"> <input type="button" id="btnUpload" value="Upload File"/></span> 
                                <span class='btn submit'> <input type='button' id='btnReset' name='btnReset' value='Reset' /> </span>
                            </div>


                        </div>

                        <div class="container" >      
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none"/>
                        </div>                  
                    </form>
                </div>
            </div>
        </div>
    </div>
    </body>

    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>

            <!--<script src="scripts/eoitnc.js"></script>-->


    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

        $(document).ready(function () {
            //alert("hii");

            $("#btnUpload").click(function () {
                // alert("hello");
				if ($("#frmuploadslaexcel").valid()){ 
                $('#btnSubmit').show(5000);
				}
            });

            $("#btnSubmit").click(function () {
			  if ($("#frmuploadslaexcel").valid()){
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfFileUploadItgkRenewalData.php"; // the script where you handle the form input.
                var data;

                if (Mode == 'Add')
                {
                    // alert("mod");
                    data = "action=uploaditgkdata&filename=" + txtfilename.value + ""; // serializes the form's elements.
                    //alert(data);
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        var data = data.trim();
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            Mode = "Add";
                            resetForm("frmuploadslaexcel");
							window.setTimeout(function () {
                                window.location.href = "frmuploaditgkslaexcel.php";
                            }, 2000);
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmuploaditgkslaexcel.php";
                            }, 2000);
                        }
                        //showData();


                    }
                });
			  }
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });
    </script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
	<script>
		$("#frmuploadslaexcel").validate({
				rules: {					
					_file: { required: true}
				},			
				messages: {				
					_file: { required: '<span style="color:red; font-size: 12px;">Please upload IT-GK Renewal Excel Data.</span>' }
				},
			});
	</script>

    <script src="scripts/fileuploaditgksladata.js"></script>
	
    </html> 


    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>	