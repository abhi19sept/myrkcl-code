<?php
$title = "IT-GK Wise Approved Count Report";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8' 
	|| $_SESSION['User_UserRoll'] == '17') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">IT-GK Wise Approved Count Report</div>
            <div class="panel-body">

                <form name="frmitgkapprovecount" id="frmitgkapprovecount" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
								  <select id="ddlCourse" name="ddlCourse" class="form-control">
                                    
								   </select>
								  
                             </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                    <!--                                <option value="0">All Batch</option>-->
                                </select>
                            </div> 							 

                        </div>

						<div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

		function FillCourseName() {
            $.ajax({
                type: "post",
                url: "common/cfWcdItgkApprovedCount.php",
                data: "action=FillAdmissionCourse",
                success: function (data) {
					$("#ddlCourse").html(data);					
                }
            });
        }
        FillCourseName();

        $("#ddlCourse").change(function () {
           var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfWcdItgkApprovedCount.php",
                data: "action=FILLBatchName&values=" + selCourse + "",
                success: function (data) {
					$("#ddlBatch").html(data);
                }
            });

        });
	
        function showData() {
			$('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdItgkApprovedCount.php"; // the script where you handle the form input.
            data = "action=GETDATA&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					if (data == 2){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Course." + "</span></div>");
                            
						}
					else if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Batch." + "</span></div>");
                            
						}
					else {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}

                }
            });
        }

        $("#btnSubmit").click(function () {
            showData();
          return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
