<?php
$title = "Add System";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var SystemCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var SystemCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Add Systems</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmaddsystem" id="frmaddsystem" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-4 form-group">     
                            <label for="system">System Type:</label> <br/>                               
                            <label class="radio-inline"> <input type="radio" id="systemServer" checked="checked" name="system" /> Server </label>
                            <label class="radio-inline"> <input type="radio" id="systemClient" name="system" /> Client </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="processor">Processor:</label>
                            <select id="ddlprocessor" name="ddlprocessor" class="form-control">
                                <!--									<option value="" selected>Select</option>
                                                                                                        <option value="Core i3">Core i3 or Equivalent</option>
                                                                                                        <option value="Core i5">Core i5 or Equivalent</option>
                                                                                                        <option value="Core i7">Core i7 or above</option>
                                -->
                            </select>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="ram">RAM:</label>
                            <select id="ddlram" name="ddlram" class="form-control">
                                <option value="" selected>Select</option>
                                <option value="1 GB">1 GB</option>
                                <option value="2 GB">2 GB</option>
                                <option value="4 GB">4 GB</option>
                                <option value="8 GB">8 GB</option>
                                <option value="16 GB">16 GB or above</option>

                            </select>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="hdd">HDD:</label>
                            <select id="ddlhdd" name="ddlhdd" class="form-control">
                                <option value="" selected>Select</option>
                                <option value="40 GB">40 GB</option>
                                <option value="80 GB">80 GB</option>
                                <option value="80 GB">160 GB</option>
                                <option value="320 GB">320 GB</option>
                                <option value="640 GB">640 GB</option>
                                <option value="1 TB">1 TB</option>
                                <option value="2 TB">2 TB</option>
                                <option value="4 TB">4 TB</option>
                            </select>
                        </div>


                        <!--                                <div class="col-sm-4 form-group">     
                                                        <label for="ncomputing">N-Computing (X-550):</label> <br/>                               
                                                        <label class="radio-inline"> <input type="radio" id="ncomputingYes" checked="checked" name="ncomputing" /> Yes </label>
                                                        <label class="radio-inline"> <input type="radio" id="ncomputingNo" name="ncomputing" /> No </label>
                                                        </div>-->


                    </div>  
                    <div class="container">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                    </div>
                    <div id="grid" style="margin-top: 35px;">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include 'common/message.php'; ?>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode === 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        } else if (Mode === 'Edit')
        {
            fillForm();
        }

//            function FillStatus() {
//                $.ajax({
//                   
//                   
//                    success: function (data) {
//                        $("#ddlStatus").html(data);
//                    }
//                });
//            }
//
//            FillStatus();

        function FillProcessor() {
            $.ajax({
                type: "post",
                url: "common/cfProcessorMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlprocessor").html(data);
                }
            });
        }
        FillProcessor();

        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAddSystem.php",
                data: "action=DELETE&values=" + SystemCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmaddsystem.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmaddsystem");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfAddSystem.php",
                data: "action=EDIT&values=" + SystemCode + "",
                success: function (data) {
                    //alert($.parseJSON(data)[0]['Status']);
                    data = $.parseJSON(data);
                    ddlprocessor.value = data[0].Processor;
                    ddlram.value = data[0].RAM;
                    ddlhdd.value = data[0].HDD;

                }
            });
        }

        function showData() {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfAddSystem.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
//alert(data);
                }
            });
        }

        showData();


        $("#btnSubmit").click(function () {

            if ($("#frmaddsystem").valid())
            {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfAddSystem.php"; // the script where you handle the form input.
                var data;

                if (document.getElementById('systemServer').checked) //for radio button
                {
                    var system_type = 'Server';
                } else {
                    system_type = 'Client';
                }

                if (Mode === 'Add')
                {
                    data = "action=ADD&type=" + system_type + "&processor=" + ddlprocessor.value + "&ram=" + ddlram.value + "&hdd=" + ddlhdd.value + ""; // serializes the form's elements.
                    //alert(data);                    
                } else
                {
                    data = "action=UPDATE&code=" + SystemCode + "type=" + system_type + "&processor=" + ddlprocessor.value + "&ram=" + ddlram.value + "&hdd=" + ddlhdd.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            //alert("HII");
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmaddsystem.php";
                            }, 2000);

                            Mode = "Add";
                            resetForm("frmaddsystem");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmaddsystem_validation.js" type="text/javascript"></script>

</html>