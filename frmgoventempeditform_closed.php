<?php  ob_start(); 
$title="Government Entry Edit Form";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
        
    if($_SESSION['User_UserRoll']!=19){
    header('Location: frmgoventempeditformforall.php');
    exit;
    } 
 ?>
 
<link rel="stylesheet" href="css/profile_style.css">
<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
            
            
            <div style="font-size: 18px;padding: 15px 0px 0px 0px;">
                    <a href="upload/documents/Claim_Workflow.pdf" style="float: right;"  target="_blank">View Claim Workflow</a> 
            </div>
			 
<!--            <div class="panel panel-primary" style="margin-top:20px !important;">

                <div class="panel-heading">Registerd Detail With RKCL</div>
                <div class="panel-body">
                    <div id="responselen"></div>
                     <div id="gird" ></div>	
                </div>
            </div> -->
            
             <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Edit Reimbursement Application  Form</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container"  id="empidfrom"  >
                            <div class="container">
                                <div id="response"></div>

                            </div>        
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group"  style="max-width: 30%;">     
                                
                                <label for="learnercode">Please Enter Employee ID (It Starts With RJ):<span class="star">*</span></label>
                                <input type="text" class="form-control text-uppercase" maxlength="18"  name="txtEID" id="txtEID"  onkeyup="checkRJ(this.value);"   onkeypress="javascript:return validAddress(event);"   value=""  placeholder="Employee ID">
                                
                                </div>
							
                          <div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
                                <input type="button" name="btnShowRJ" id="btnShowRJ" class="btn btn-primary" value="Show Details" style="margin-top:25px; display: none;"/>  
                            </div>
			</div>
			<input type="hidden" class="form-control"  name="txtLCode" id="txtLCode" value="<?php echo $_SESSION['User_LearnerCode'];?>"  placeholder="LearnerCode">
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                            			

                    <div id="main-content" style="display:none;  margin-left: 50px;">
			<div class="container">
                            
                            <div class="col-sm-4 form-group"> 
                                <label for="ename"> Name:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control text-uppercase" maxlength="50" name="txtEmpName" id="txtEmpName" placeholder="Employee Name">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname"> Father/Husband Name:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control text-uppercase" maxlength="50" name="txtFaName" id="txtFaName"  placeholder="Employee Father Name">
                            </div>
                             <div class="col-sm-4 form-group"> 
                                <label for="dobprrof">Date of Birth:<span class="star">*</span></label>
                                <input type="text" readonly="true" maxlength="15" class="form-control" onkeypress="javascript:return allownumbers(event);"   name="txtEmpDOB" id="txtEmpDOB" placeholder="Date of Birth">
                                <input type="hidden" readonly="true" maxlength="15" class="form-control"    name="txtEmpDOBAPP" id="txtEmpDOBAPP" placeholder="Date of Appointment">
                            </div>
                             <div class="col-sm-4 form-group">     
                                <label for="address"> Office Address:<span class="star">*</span></label>
                                <textarea class="form-control  text-uppercase" rows="3" id="txtEmpAddress" onkeypress="javascript:return validAddress(event);" name="txtEmpAddress" placeholder="Emp Office Address"></textarea>
                            </div>
                           
                            
                        </div>  

                        <div class="container">                            
                            <div class="col-sm-4 form-group"> 
                                <label for="empid">Employee Id:<span class="star">*</span></label>
                                <input type="text" class="form-control "  name="txtEmpId" onkeypress="javascript:return allownumbers(event);"  id="txtEmpId" placeholder="Employee Id">
                            </div>					
                           <div class="col-sm-6 form-group"> 
                                <label for="deptname"> Department Name:<span class="star">*</span></label>
                                <textarea class="form-control" readonly="true" rows="3" id="ddlDeptName" name="ddlDeptName" placeholder="Emp Department Name"></textarea>
                                
<!--                                <select id="ddlDeptName" name="ddlDeptName" class="form-control">
								
                                </select>-->
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="designation"> Designation:<span class="star">*</span></label>
                                <input type="text"  readonly="true"  class="form-control  text-uppercase" name="ddlEmpDesignation"  id="ddlEmpDesignation" placeholder="Emp Designation">
                            </div> 			
                            <div class="col-sm-4 form-group"> 
                                <label for="gpfno"> GPF No / PRAN:<span class="star">*</span></label>
                                <input type="text"  readonly="true"  class="form-control" maxlength="20" name="txtGpfNo" onkeypress="javascript:return allownumbers(event);" id="txtGpfNo" placeholder="Emp GPF No">
                            </div>
                            
                           
                            
                            
                            
                        </div>    

                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Employee District:<span class="star">*</span></label>
                                <select id="ddlempdistrict" name="ddlempdistrict" class="form-control  text-uppercase" >								  
                                </select>    
                            </div>
                            
                            <div class="col-sm-4 form-group"> 
                                <label for="email"> Email Id:<span class="star">*</span></label>
                                <input type="email" class="form-control" name="txtEmail" id="txtEmail" placeholder="Emp EmailId">    
                            </div>
                            
                            <div class="col-sm-4 form-group">     
                                <label for="emobile"> Mobile No:<span class="star">*</span></label>
                                <input type="text"   class="form-control  text-uppercase" maxlength="10" name="txtEmpMobile" id="txtEmpMobile" placeholder="Emp Mobileno"  onkeypress="javascript:return allownumbers(event);">
                            </div> 
                            <div class="col-sm-4 form-group"> 
                                <label for="pan">PAN No:</label>
                                <input type="text" maxlength="10" class="form-control text-uppercase" onkeypress="fnValidatePAN();"  name="txtPanNo" id="txtPanNo"  placeholder="PAN No">
                                <span id="pencardvalidate" style="font-size:10px;"></span><br>
                                <span style="font-size:10px;">Note : PAN No should be 10 digit. Starting 5 should be only alphabets[A-Z] then Remaining 4 should be accepting only alphanumeric and last one is again alphabets[A-Z] .</span>
                             
                            </div> 
                                                     
                           
			 
                        </div>
						
                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="attempt">Select RS-CIT Exam Attempt:<span class="star">*</span></label>
                                <select id="ddlExamattepmt" name="ddlExamattepmt" class="form-control  text-uppercase">
                                    <option value="" selected="selected">Please Select</option>   
                                    <option value="1"> First</option> 
                                    <option value="2"> Not in 1st Attempt</option> 
                                </select>
                            </div>
                            <input type="hidden" class="form-control  text-uppercase" readonly="readonly"  name="txtFee" id="txtFee"  placeholder="Fee">
                            <input type="hidden" class="form-control  text-uppercase" readonly="readonly"  name="txtIncentive" id="txtIncentive"  placeholder="Incentive">
                            <input type="hidden" class="form-control  text-uppercase" readonly="readonly"  name="txtTotalAmt" id="txtTotalAmt"  placeholder="Total Amount">								
                            <input type="hidden" class="form-control  text-uppercase" readonly="readonly"  name="txtBatchID" id="txtBatchID"  placeholder="Batch Id">								
<!--                            <div class="col-sm-4 form-group"> 
                                <label for="pan">Fee:<span class="star">*</span></label>
                                <input type="hidden" class="form-control  text-uppercase" readonly="readonly"  name="txtFee" id="txtFee"  placeholder="Fee">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="pan">Incentive:<span class="star">*</span></label>
                                <input type="hidden" class="form-control  text-uppercase" readonly="readonly"  name="txtIncentive" id="txtIncentive"  placeholder="Incentive">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="pan">Total Amount:<span class="star">*</span></label>
                                <input type="hidden" class="form-control  text-uppercase" readonly="readonly"  name="txtTotalAmt" id="txtTotalAmt"  placeholder="Total Amount">								
                            </div>-->
                            
                            
                           <div class="col-sm-4 form-group"> 
                                <label for="marks">RS-CIT Exam Marks:<span class="star">*</span></label>
                                <input type="text" maxlength="3" class="form-control  text-uppercase allow_numeric" id="txtEmpMarks" name="txtEmpMarks" onkeypress="javascript:return allownumbers(event);"
									placeholder="RS-CIT Exam Marks"  max='100' min="40">
                            </div>
                             <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"> Bank District:<span class="star">*</span></label>
                                <select id="ddlBankDistrict" name="ddlBankDistrict" class="form-control  text-uppercase">
                                </select>
                            </div> 
                            
                            <div class="col-sm-4 form-group"> 
                                <label for="bankname"> Bank Name:<span class="star">*</span></label>
                                <select id="ddlBankName" name="ddlBankName" class="form-control text-uppercase">
                                    <option value="" selected="selected">Please Select</option>                
                                </select>                                                              
                            </div>
                            
                             
                        </div>

                        <div class="container"> 
                            
                             <div class="col-sm-4 form-group"> 
                                <label for="branchname">Branch Name:<span class="star">*</span></label>
                                <input type="text" class="form-control  text-uppercase" onkeypress="javascript:return allowchar(event);"  name="txtBranchName" id="txtBranchName"  placeholder="EMP Branch Name">
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="bankaccount"> Bank Account No:<span class="star">*</span></label>
                                <input type="text" class="form-control  text-uppercase" maxlength="18" name="txtBankAccount" onkeypress="javascript:return allownumbers(event);"  id="txtBankAccount" placeholder="Emp Bank Accountno">
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="micr">MICR Code:<span class="star">*</span></label>
                                <input type="text" maxlength="15" class="form-control  text-uppercase" onkeypress="javascript:return allownumbers(event);"   name="txtMicrNo" id="txtMicrNo" placeholder="EMP Bank MICR Code">
                            </div>
                             <div class="col-sm-4 form-group"> 
                                <label for="ifsc">IFSC Code:<span class="star">*</span></label>
                                <input type="text" maxlength="11" class="form-control text-uppercase"    onkeypress="fnValidateIFSC();"   name="txtIfscNo"  id="txtIfscNo" placeholder="EMP Bank IFSC Code">
                                <span id="ifsccode" style="font-size:10px;"></span><br>
                                <span style="font-size:10px;">Note : IFSC code should be 11 digit. Starting 4 should be only alphabets[A-Z] and Remaining 7 should be accepting only alphanumeric.</span>
                             </div> 
                            
			</div>
                        
                        
                        <div class="container">
                               
                            <div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Upload Cancel Cheque:<span class="star">*</span></label><br><br>
                                <div id="image_preview" style="padding-bottom:5px;">
					<img id="previewing" />
				</div>
                                <img  src="images/user icon big.png" id="uploadPreview2" name="filePhoto2" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">
                                <input type="hidden" class="form-control" name="txtfileReceiptphoto" id="txtfileReceiptphoto"/>
                                <input type="file" class="form-control"  name="fileReceipt" id="fileReceipt" onchange="checkPayReceipt(this)">
                                <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =50KB</span>
                            </div> 
                            <div class="col-sm-4 form-group"> 
                                <label for="permissionletter">Upload Department Forwarding Letter:<span class="star">*</span></label>
                                <div id="image_preview2" style="padding-bottom:5px;">
					<img id="previewing2" />
				</div>
                                <img  src="images/user icon big.png" id="uploadPreview3" name="filePhoto3" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage2').click();">	
                                <input type="hidden" class="form-control" name="txtfiledpLetterphoto" id="txtfiledpLetterphoto"/>
                                <input type="file" class="form-control"  name="dpLetter" id="dpLetter" onchange="checkdpLetter(this)">
                                <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =50KB</span>
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="rscitcer">RS-CIT Final / Provisional Certificate Issued By VMOU:<span class="star">*</span></label>
                                <div id="image_preview3" style="padding-bottom:5px;">
					<img id="previewing3" />
				</div>
                                <img  src="images/user icon big.png" id="uploadPreview1" name="filePhoto1" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage3').click();">	
                                <input type="hidden" class="form-control" name="txtfilerscitCerphoto" id="txtfilerscitCerphoto"/>
                                <input type="file" class="form-control"  name="rscitCer" id="rscitCer" onchange="checkrscitcer(this)">
                                <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =50KB</span>
                            </div>  
                             
                           
			</div>

			<div class="container">  
                            <div class="col-md-6"  style="padding-bottom: 10px;"> 							
				<label for="learnercode">Terms And Conditions :<span class="star">*</span></label></br>                              
                                    <label class="checkbox-inline"> <input type="checkbox" name="chk" id="chk" value="1" >
                                            <a title="" style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><span id="fix">I accept the Terms And Conditions.</span> </a>
                                    </label>								
                            </div>
                            
			</div>	
						
			<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">
					<div class="modal-content">
                                            <div class="modal-header">
                                                    <button class="close" type="button" data-dismiss="modal">×</button>
                                                    <h3 class="modal-title">Terms And Conditions</h3>
                                            </div>
                                            <div class="modal-body">
Further I hereby undertake:<br>
1.  I have passed the RS-CIT.<br>
2.  I have not claimed the amount previously for the same course.<br>
3.  As per reimbursement norms, my age at the time of admission was 55 years or less. (as on 1 st January of the year of admission )<br>
4.  Disciplinary action shall be taken if any information found false or wrong/double reimbursement claim availed.
                                            </div>
                                            <div class="modal-footer">
                                                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
				</div>
			</div>

                        
                            <div class="container"  style="padding-bottom: 11px;">
                             <div class="col-sm-3" id="otptext" style=" display: none;">     
                                                <label for="emobile" style="padding:  6px 8px 0px 0px; float: left"> OTP Code:<span class="star">*</span></label>
                                                <input class="form-control  text-uppercase" style="float: left; width: 130px;" name="txtOTP" id="txtOTP" placeholder="OTP Code"  onkeypress="javascript:return allownumbers(event);" type="password">
                             </div>
                            <div id="rOTP" class="col-sm-5"  style="padding-top: 6px;"></div>
                        </div>
                        
                           <div class="container">
                                    
                                    <div style="display: inline-block;">
                                    <input type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload"/>
                                    <input type="submit" name="btnValidate" id="btnValidate" class="btn btn-primary" value="Validate" style="display:none;"/>
                                    <input type="submit" name="btnValidateOTP" id="btnValidateOTP" class="btn btn-primary" value="Validate OTP" style="display:none;"/>
                                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>
                                    </div>
                                    <div id="responseSubmit" style="display: inline-block;padding-left: 20px;width: 80%;"></div>
                            </div>
                        
			</div>
                </div>
            </div> 
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>

<script src="scripts/govfileuploadNew_Edit.js"></script>
<style>
#errorBox{
 color:#F00;
 }
</style>

<style>
  .modal-dialog {width:700px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
<script type="text/javascript">  
  $(document).ready(function() {
	jQuery(".fix").click(function(){
        $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>




<script language="javascript" type="text/javascript">
     function checkRJ (empid){
           // alert(olr);
          // 
            var str=empid;
            if (str.match("^rj") || str.match("^RJ")) 
                {
                    // do this if begins with Hello
                    //alert("sunil");
                    $('#response').empty();
                    $("#btnShow").show();
                    $("#btnShowRJ").hide();
                    /*var reg = /^[A-Z|a-z]{4}[0-9a-zA-Z]*$/;
            
                    //var ifsc = `SBIN1234567`;
                    if(empid!=''){
                        if (empid.match(reg)) {
                             $('#response').empty();
                             $("#btnShow").show();
                             $("#btnShowRJ").hide();
                        }
                        else {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Employee ID Must Starts With RJ. And Starting 4 should be only alphabets[A-Z]." + "</span></p>");
                            $("#btnShow").hide();
                            $("#btnShowRJ").show();
                            return false;
                        }
                    }
                    */
                }
            else
            {
                if(str!='')
                {
                     $('#response').empty();
                     $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Employee ID Must Starts With RJ." + "</span></p>");
                     $("#btnShow").hide();
                     $("#btnShowRJ").show(); 
                }else{
                    $('#response').empty();
                    $("#btnShow").show();
                    $("#btnShowRJ").hide(); 
                }
                    
            }
        }
        
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }

        function fnValidatePAN(){
         $('#pencardvalidate').html('');
        }
        function fnValidateIFSC(){
            $('#ifsccode').html('');
        } 
        
       function AllowIFSC(ifsc) {//
            //var reg = /^[A-Za-z]{4}\d{7}$/;
             //ar reg = /^[A-Z|a-z]{4}[0][\w]{6}$/;
             var reg = /^[A-Z|a-z]{4}[\w]{7}$/;
            //var ifsc = `SBIN1234567`;
             if(ifsc!=''){
                if (ifsc.match(reg)) {
                     $('#ifsccode').empty();
                }
                else {
                    $('#ifsccode').empty();
                    $('#txtIfscNo').val('');
                    $('#ifsccode').append("<span style='color:red; font-size: 12px;'>Invalid IFSC Code. Please enter as per the instruction given.</span>");

                    return false;
                }
            }
        }
        
        function ValidatePANCard(pencard) {//
            var reg = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/;
            //var pencard = `ABCDE1234D`;
            //alert(pencard);
             if(pencard!=''){
                if (pencard.match(reg)) {
                     $('#pencardvalidate').empty();
                }
                else {
                    $('#pencardvalidate').empty();
                    $('#txtPanNo').val('');
                    $('#pencardvalidate').append("<span style='color:red; font-size: 12px;'>Invalid PAN. Please enter as per the instruction given.</span>");

                    return false;
                }
            }
        }

</script>
<script language="javascript" type="text/javascript">
function checkPayReceipt(target) {
    if(target.files[0].size > 0) 
            {
                        $('#btnSubmit').hide(3000);
                        $('#btnUpload').show(3000);   
            }
    
	var ext = $('#fileReceipt').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileReceipt").value = '';
			return false;
		}else{
                                var reader = new FileReader();
				reader.onload = imageIsLoaded;
				reader.readAsDataURL(target.files[0]);
                }

    if(target.files[0].size > 50000) 
            {

                //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
                        alert("Image size should less or equal 50 KB");
                        document.getElementById("fileReceipt").value = '';
                        return false;
            }
    else if(target.files[0].size < 20000)
            {
			alert("Image size should be greater than 20 KB");
			document.getElementById("fileReceipt").value = '';
			return false;
            }
    document.getElementById("fileReceipt").innerHTML = "";
    return true;
}

function imageIsLoaded(e){
		$("#file").css("color","green");
		$('#image_preview').css("display", "block");
		$('#image_preview').css("width", "80px");
		$('#image_preview').css("height", "107px");
                $('#uploadPreview2').css("display", "none");
		$('#previewing').attr('src', e.target.result);
		$('#previewing').attr('width', '80px');
		$('#previewing').attr('height', '107px');
	};
</script>

<script language="javascript" type="text/javascript">
function checkdpLetter(target) {
     if(target.files[0].size > 0) 
            {
                        $('#btnSubmit').hide(3000);
                        $('#btnUpload').show(3000);   
            }
	var ext = $('#dpLetter').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("dpLetter").value = '';
			return false;
		}else{
                                var reader = new FileReader();
				reader.onload = imageIsLoaded2;
				reader.readAsDataURL(target.files[0]);
                }

    if(target.files[0].size > 50000) {
			
        //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
		alert("Image size should less or equal 50 KB");
		document.getElementById("dpLetter").value = '';
        return false;
    }
	else if(target.files[0].size < 20000)
			{
				alert("Image size should be greater than 20 KB");
				document.getElementById("dpLetter").value = '';
				return false;
			}
    document.getElementById("dpLetter").innerHTML = "";
    return true;
}

function imageIsLoaded2(e){
		$("#file").css("color","green");
		$('#image_preview2').css("display", "block");
                $('#image_preview2').css("width", "80px");
		$('#image_preview2').css("height", "107px");
                $('#uploadPreview3').css("display", "none");
		$('#previewing2').attr('src', e.target.result);
		$('#previewing2').attr('width', '80px');
		$('#previewing2').attr('height', '107px');
	};
</script>

<script language="javascript" type="text/javascript">
function checkrscitcer(target) {
     if(target.files[0].size > 0) 
            {
                        $('#btnSubmit').hide(3000);
                        $('#btnUpload').show(3000);   
            }
	var ext = $('#rscitCer').val().split('.').pop().toLowerCase();
	if($.inArray(ext, ['png','jpg','jpeg']) == -1) 
                {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("rscitCer").value = '';
			return false;
		}
            else{
                                var reader = new FileReader();
				reader.onload = imageIsLoaded3;
				reader.readAsDataURL(target.files[0]);
                }
        if(target.files[0].size > 50000) 
                {
                        alert("Image size should less or equal 50 KB");
                        document.getElementById("rscitCer").value = '';
                        return false;
                }
	else if(target.files[0].size < 20000)
		{
			alert("Image size should be greater than 20 KB");
			document.getElementById("rscitCer").value = '';
			return false;
		}
        document.getElementById("rscitCer").innerHTML = "";
        return true;
}

function imageIsLoaded3(e){
		$("#file").css("color","green");
		$('#image_preview3').css("display", "block");
                $('#image_preview3').css("width", "80px");
		$('#image_preview3').css("height", "107px");
                $('#uploadPreview1').css("display", "none");
		$('#previewing3').attr('src', e.target.result);
		$('#previewing3').attr('width', '80px');
		$('#previewing3').attr('height', '107px');
	};
</script>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        
        ///31-5-2017///
         $(".allow_numeric").on("input", function(evt) {
            var self = $(this);
            self.val(self.val().replace(/[^\d].+/, ""));
            if ((evt.which < 48 || evt.which > 57)) 
            {
              evt.preventDefault();
            }
          });
        ///31-5-2017///
         
        $("#ddlExamattepmt").change(function(){
			FillRFee(this.value);
		});
        function FillRFee(examattepmt) {
            var txtLCode = $('#txtLCode').val();
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLREFEE&examattepmt=" + examattepmt + "&txtLCode=" + txtLCode,
                success: function (data) {
                    //$("#ddlBankName").html(data);
                    //alert(data);
                    data = $.parseJSON(data);
                    txtFee.value = data[0].Fee;
                    txtBatchID.value = data[0].BatchID;
                    txtIncentive.value = data[0].Incentive;
                    var val1 = parseInt(document.getElementById("txtFee").value);
                    var val2 = parseInt(document.getElementById("txtIncentive").value);
                    var ansD = document.getElementById("txtTotalAmt");
                    ansD.value = val1 + val2;
                }
            });
        }
        
        
        


        function FillEmpDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlempdistrict").html(data);
                }
            });
        }
        FillEmpDistrict();
        
        
        function FillEmpExamAttempt() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLEXAMATTEMPT",
                success: function (data) {
                    $("#ddlExamattepmt").html(data);
                }
            });
        }
        FillEmpExamAttempt();
        
        
	function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
        GenerateUploadId();
        
       
        
        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        FillBankDistrict();
	$("#ddlBankDistrict").change(function(){
			FillBankName(this.value);
		});
                
        function FillBankName(districtid) {//alert(districtid);
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKNAME&districtid=" + districtid,
                success: function (data) {
                    $("#ddlBankName").html(data);
                }
            });
        } 
        //FillBankName(districtid);
        
        /* This condtion is run when learner is not login */
        function FillBankNameEdit() {//alert(districtid);
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKNAME",
                success: function (data) {
                    $("#ddlBankName").html(data);
                }
            });
        }
        FillBankNameEdit();
         /* This condtion is run when learner is not login */
         
         
        function FillDobProof() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FillDobProof",
                success: function (data) {
                    $("#ddlDobProof").html(data);
                }
            });
        }
        FillDobProof();
        

      
	$("#btnShow").click(function () {
            if(txtEID.value == '')
                {  $('#response').empty();
                   $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter Employee ID First." + "</span></p>");
                }
            else{
                  
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");		
		$.ajax({
			type: "post",
                        url: "common/cfgoventryform.php",
			data: "action=DETAILSEDITEMP&values=" + txtLCode.value + "&EmpId=" + txtEID.value + "",
			//data: "action=DETAILSEDITEMP&values=" + txtLCode.value + "",
			success: function (data)
				{
				//alert(data);
				$('#response').empty();
                                if(data == 'NOE')
                                    {
                                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " You are not eligible to edit your reimbursement application form. Please check your current status." + "</span></p>");
                                    }
                                else if (data == 'NOE12')
                                    {
                                            
                                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span >  Step 3 (Upload Acknowledgement Receipt) is pending please Complete Step 3. </span></p>");
                                    }
                                else if(data == 'APIERROR')
                                    {
                                     $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Sorry, We are not able to fetch your record from Govt. database at this time. Please try after some time..  " + "</span></p>");
                                    }
                                else if(data == 'ALE')
                                    {
                                     $('#response').append("<p class='error' style='width: 86%;'><span><img src=images/error.gif width=10px /></span><span>" + "   You have already applied for reimbursement. Your status is pending for processing. You can <a href='frmgoventempeditform.php'>EDIT</a> your reimbursement application form in Step 4.  " + "</span></p>");
                                    }
                                else if(data == 'AMNTREM')
                                    {
                                     $('#response').append("<p class='error' style='width: 86%;'><span><img src=images/error.gif width=10px /></span><span>" + "   Amount has already been reimbursed to registered beneficiary's account.  " + "</span></p>");
                                    }
                                else if(data == 'WONGEMPID')
                                    {
                                     $('#response').empty();
                                     $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   This is not a valid Employee Id. Please check your Salary Slip and enter a valid Employee Id (It starts with RJ)." + "</span></p>");
                                    }
                                else if(data == 'AGE')
                                    {
                                     $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Not applicable for Reimbursement. As per GoR policy, only Govt. Employees with less than 55 years of age are allowed for Reimbursement. " + "</span></p>");
                                    }
                                else{
                                    
                                    if(data == ""){
                                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Invalid Learner Code. Please check RS-CIT receipt or VMOU Certificate to find out Learner Code." + "</span></p>");
                                                  }
                                    else{
                                    
                                            data = $.parseJSON(data);
                                             //console.log(data);
                                            //alert(data.panno);
                                            txtEmpName.value = data.EMPLOYEE_NAME;
                                            txtFaName.value = data.FATHER_OR_HUSBAND_NAME;
                                            //txtEmail.value = data.EMAIL;
                                            //txtEmpMobile.value = data.MOBILE;
                                            txtEmpId.value = txtEID.value;
                                            txtGpfNo.value = data.GPF_NO;
                                            txtEmpDOB.value = data.DOB;
                                            txtEmpDOBAPP.value = data.DOBAPP;
                                            ddlDeptName.value = data.DEPARTMENT;
                                            ddlEmpDesignation.value = data.DESIGNATION;
                                            
//                                            txtEmpName.value = data.fname;
//                                            txtFaName.value = data.faname;
//                                            txtEmail.value = data.lemailid;
//                                            txtEmpMobile.value = data.lmobileno;
//                                            txtEmpId.value = data.empid;
//                                            txtGpfNo.value = data.empgpfno;
//                                            txtEmpDOB.value = data.empdob;
//                                            ddlDeptName.value = data.gdname;
//                                            ddlEmpDesignation.value = data.designation;

                                            txtEmail.value = data.lemailid;
                                            txtEmpMobile.value = data.lmobileno;
                                            txtEmpAddress.value = data.officeaddress;
                                            txtEmpMarks.value = data.exammarks;
                                            txtPanNo.value = data.panno;
                                            txtBankAccount.value = data.empaccountno;
                                            txtBranchName.value = data.gbankname;
                                            txtIfscNo.value = data.gifsccode;
                                            txtMicrNo.value = data.gmicrcode;
                                            txtFee.value = data.fee;
                                            txtIncentive.value = data.incentive;
                                            txtTotalAmt.value = data.trnamount;
                                            txtBatchID.value = data.batchid;
                                            $("#uploadPreview1").attr('src', "upload/government_rscitcer/" + data.attach3);
                                            $("#uploadPreview2").attr('src', "upload/government_cancelcheck/" + data.attach4);
                                            $("#uploadPreview3").attr('src', "upload/government_dpletter/" + data.attach5);
                                            
                                            
                                            $("#main-content").show();
                                            $('#txtEID').attr('readonly', true);
                                            $('#txtEmpId').attr('readonly', true);
                                            $('#txtFee').attr('readonly', true);
                                            $('#txtIncentive').attr('readonly', true);
                                            $('#txtTotalAmt').attr('readonly', true);
                                            //$('#txtBankAccount').attr('readonly', true);
                                            //$('#txtLCode').attr('readonly', true);
                                            //$("#btnShow").hide();
                                            $("#empidfrom").hide();
                                        }

                                    }
                                }
                });	
                  
                }
            	
          });
		  
	$("#btnUpload").click(function () {
       // var ifsc=$('#txtIfscNo').value;
        var ifsc = $('#txtIfscNo').val();
        AllowIFSC(ifsc);
        
        var pencard = $('#txtPanNo').val();
        ValidatePANCard(pencard);
        
        if ($("#form").valid()){ 
			//$('#btnSubmit').show(3000);
                        $('#btnValidate').show(3000);
                        $('#btnUpload').hide(3000);
                    }
		});
	
        
        $("#btnSubmit").click(function () {//alert("hello");
            
            
            // IFSC no And PANCARD
        var ifsc = $('#txtIfscNo').val();
        AllowIFSC(ifsc);
        
        var pencard = $('#txtPanNo').val();
        ValidatePANCard(pencard);
        // IFSC no And PANCARD
            
	if ($("#form").valid())
           {
               
            if (confirm('Are you sure you want to submit the application.')) 
                {   
                    $('#responseSubmit').empty();
                    $('#responseSubmit').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    var url = "common/cfgoventryform.php"; // the script where you handle the form input.txtEmpDOB
                    var payreceipt = $('#txtLCode').val();
                    var dpLetter = $('#txtLCode').val();
                    var rscitCer = $('#txtLCode').val();
                    var data;
                    data = "action=EDITEMP&lcode=" + txtLCode.value + "&empname=" + txtEmpName.value + "&faname="+ txtFaName.value + 
					   "&empdistrict="+ ddlempdistrict.value + "&empmobile="+ txtEmpMobile.value + "&empemail="+ txtEmail.value + 
					   "&empaddress="+ txtEmpAddress.value + "&empdeptname="+ ddlDeptName.value + "&empid="+ txtEmpId.value + 
					   "&empgpfno="+ txtGpfNo.value + "&empdesignation="+ ddlEmpDesignation.value + "&empmarks="+ txtEmpMarks.value + 
					   "&empexamattempt="+ ddlExamattepmt.value + "&empfee="+ txtFee.value + "&empincentive="+ txtIncentive.value + 
					   "&emptotalamt="+ txtTotalAmt.value + "&empBatchID="+ txtBatchID.value + "&emppan="+ txtPanNo.value + "&empbankaccount="+ txtBankAccount.value + 
					   "&empbankdistrict="+ ddlBankDistrict.value + "&empbankname="+ ddlBankName.value + "&empbranchname="+ txtBranchName.value +
					   "&empifscno="+ txtIfscNo.value + "&empmicrno="+ txtMicrNo.value + "&fileReceipt=" + payreceipt + "&empdob=" + txtEmpDOB.value + "&empdobapp=" + txtEmpDOBAPP.value + "&empdpletter=" + dpLetter +
					   "&emprscitcer="+ rscitCer + ""; //serializes the form's elements.
            
                    $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   //alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#responseSubmit').empty();
                        $('#responseSubmit').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmgoventempAckReceipt.php";
                        }, 7000);

                        //Mode = "Add";
                        //resetForm("form");
                    }
                    else if (data == 'MOBACNT')
                    {
                        $('#responseSubmit').empty();
                        $('#btnSubmit').hide(3000);
                        $("#btnValidate").attr('value', 'Validate');
                        $('#btnValidate').show(3000);
                        $('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  This Mobile and Bank Account Numbers are already used by some other Govt. Employee while applying for reimbursement. Please enter a different Mobile & Bank Account Numbers.</span></p>");
                    }
                    else if (data == 'MOB')
                    {
                        $('#responseSubmit').empty();
                        $('#btnSubmit').hide(3000);
                        $("#btnValidate").attr('value', 'Validate');
                        $('#btnValidate').show(3000);
                        $('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'> This Mobile is already used by some other Govt. Employee while applying for reimbursement. Please enter a different Mobile Number.</span></p>");
                    }
                    else if (data == 'ACNT')
                    {
                        $('#responseSubmit').empty();
                        $('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'> This Bank Account Number is already used by some other Govt. Employee while applying for reimbursement. Please enter a different Bank Account Number.</span></p>");
                    }
                    else
                    {
                        $('#responseSubmit').empty();
                        $('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });
                }
             else
                {
                            ///window.setTimeout(function () {
                                       /// window.location.href = "frmgoventempeditform.php";
                                    ///}, 1000);
                                    return false; 
                }
            
           }
            return false; // avoid to execute the actual submit of the form.
        });
        
        
        $("#btnValidate").click(function () {//alert("hello");
        
        // IFSC no And PANCARD
        var ifsc = $('#txtIfscNo').val();
        AllowIFSC(ifsc);
        
        var pencard = $('#txtPanNo').val();
        ValidatePANCard(pencard);
        // IFSC no And PANCARD
        
	if ($("#form").valid())
           {			    
            $('#responseSubmit').empty();
            $('#responseSubmit').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfgoventryform.php"; // the script where you handle the form input.txtEmpDOB
            var data;
            data = "action=CVALIDATE&lcode=" + txtLCode.value + "&empmobile="+ txtEmpMobile.value + "&empbankaccount="+ txtBankAccount.value +""; //serializes the form's elements.
            
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   //alert(data);
                    if (data == 'MOBACNT')
                    {
                        $('#responseSubmit').empty();
                        $('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  This Mobile and Bank Account Numbers are already used by some other Govt. Employee while applying for reimbursement. Please enter a different Mobile & Bank Account Numbers.</span></p>");
                    }
                    else if (data == 'MOB')
                    {
                        $('#responseSubmit').empty();
                        $('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  This Mobile is already used by some other Govt. Employee while applying for reimbursement. Please enter a different Mobile Number.</span></p>");
                    }
                    else if (data == 'ACNT')
                    {
                        $('#responseSubmit').empty();
                        $('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'> This Bank Account Number is already used by some other Govt. Employee while applying for reimbursement. Please enter a different Bank Account Number.  </span></p>");
                    }
                    else
                    {
                        $('#responseSubmit').empty();
                        $('#rOTP').empty();
                        //$('#btnValidate').hide(3000);
                        $("#btnValidate").attr('value', 'Resend OTP');
                        $('#btnValidateOTP').show(3000);
                        $('#otptext').show(3000);
                        //$('#txtEmpMobile').attr('readonly', true);
                        //$('#txtBankAccount').attr('readonly', true);
                        $('#rOTP').append("<p class='error'><span><img src=images/correct.gif width=10px /> </span> <span style='padding-left: 7px;'>  " + data + "</span></p>");
                    }
                    //showData();


                }
            });
		 }
            return false; // avoid to execute the actual submit of the form.
        });
        
        
        $("#btnValidateOTP").click(function () {//alert("hello");
        
        // IFSC no And PANCARD
        var ifsc = $('#txtIfscNo').val();
        AllowIFSC(ifsc);
        
        var pencard = $('#txtPanNo').val();
        ValidatePANCard(pencard);
        // IFSC no And PANCARD
        
	if ($("#form").valid())
           {			    
            $('#responseSubmit').empty();
            $('#responseSubmit').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfgoventryform.php"; // the script where you handle the form input.txtEmpDOB
            var data;
            data = "action=OTPVALIDATE&empmobile="+ txtEmpMobile.value + "&txtOTP="+ txtOTP.value +""; //serializes the form's elements.
            
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   //alert(data);
                    if (data == 'NOTP')
                    {
                        $('#responseSubmit').empty();
                        $('#rOTP').empty();
                        $('#rOTP').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  Please Enter OTP First.</span></p>");
                    }
                    else if (data == 'InvalidOTP')
                    {
                        $('#responseSubmit').empty();
                        $('#rOTP').empty();
                        $('#rOTP').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  Invalid OTP. Please Try Again.</span></p>");
                    }
                    else
                    {
                        $('#responseSubmit').empty();
                        $('#rOTP').empty();
                        $('#btnValidate').hide(3000);
                        $('#btnValidateOTP').hide(3000);
                        $('#btnSubmit').show(3000);
                        $('#txtEmpMobile').attr('readonly', true);
                        $('#txtBankAccount').attr('readonly', true);
                        //$('#otptext').show(3000);
                        //$('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  " + data + "</span></p>");
                    }
                    //showData();


                }
            });
		 }
            return false; // avoid to execute the actual submit of the form.
        });
        
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmgoventry_validation_new_edit.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>