<?php
$title = "WCD Innaugration  Center List";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['DDLogin'])) {
	
    echo "<script>var Login='".$_REQUEST['DDLogin']."' </script>";
	echo "<script>var batch='".$_REQUEST['batch']."' </script>";
	
    
} else {
    echo "<script>var Login='0'</script>";
   
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">WCD Innaugration  Center List</div>
            <div class="panel-body">

                <form name="frmInnaugrationcountcenter" id="frmInnaugrationcountcenter" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                         						
                        </div>

                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showData() {			
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfInnaugrationcount.php"; // the script where you handle the form input.    

			
            data = "action=GETDETAILSPO&Login=" + Login + "&batch=" + batch + ""; //
           
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {					
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print']
                    });
                }
            });
        }       
        
		showData();
          
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
