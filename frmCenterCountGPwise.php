<?php
$title = "Admission Count District Wise";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var UserCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>		

<div style="min-height:430px !important;max-height:auto !important;"> 

    <div class="container">     
        <div class="panel panel-primary" style="margin-top:20px !important;">  
            <div class="panel-heading">IT-GK Count Gram Panchayat Wise Report</div>
            <div class="panel-body">

                <form name="frmcentercountgpwise" id="frmcentercountgpwise" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-4 form-group"> 
                            <label for="course">Select District:<span class="star">*</span></label>
                            <select id="ddlDistrict" name="ddlDistrict" class="form-control">

                            </select>
                        </div> 
                        <!--
                                                <div class="col-md-4 form-group">     
                                                    <label for="batch"> Select Batch:<span class="star">*</span></label>
                                                    <select id="ddlBatch" name="ddlBatch" class="form-control">
                        
                                                    </select>
                                                </div>					 -->
                    </div>
                    <div class="container">                       
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>                        
                    </div>

                    <div id="gird" style="margin-top:5px;"> </div>                


            </div>   
            </form>
        </div>
    </div>
</div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillDistrict() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlDistrict").html(data);
                }
            });
        }
        FillDistrict();


        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfCenterCountGPwise.php"; // the script where you handle the form input.            
            var data;
            data = "action=GETDATA&district=" + ddlDistrict.value + ""; //
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    $('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }

        $("#btnSubmit").click(function () {

            showData();


            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<!--<script src="bootcss/js/frmadmissioncountsummary_validation.js" type="text/javascript"></script>-->
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
