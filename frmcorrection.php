<?php
$title="Correction Add";
include ('header.php'); 
include ('root_menu.php');

$_SESSION['CorrectionPhoto']="";
$_SESSION['CorrectionMarksheet']="";
$_SESSION['CorrectionCertificate']="";
$_SESSION['CorrectionProvisional']="";

if (isset($_REQUEST['code'])) {
echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var Code=0</script>";
echo "<script>var Mode='Add'</script>";
}
?>

<link rel="stylesheet" href="css/datepicker.css"/>
<script src="scripts/datepicker.js"></script>
 
	
<div style="min-height:430px !important;max-height:auto !important;">   
	<div class="container"> 			
		<div class="panel panel-primary" style="margin-top:20px !important;">  
			<div class="panel-heading">Correction Application  Form</div>
				<div class="panel-body">
					<!-- <div class="jumbotron"> --> 
					<form name="form" action="common/cfcorrectionform.php" id="form" class="form-inline" role="form" 
						enctype="multipart/form-data">    
			<div class="container">
							<div class="container">
								<div id="response"></div>
							</div>        
								<div id="errorBox"></div>
								
							<div class="col-sm-4 form-group">     
							  <label for="learnercode">Learner Code:<span class="star">*</span></label>
							  <input type="text" class="form-control" maxlength="50" name="txtLCode" id="txtLCode"  onkeypress="javascript:return allownumbers(event);" placeholder="Learner Code">
							  <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
							  <input type="hidden" class="form-control" maxlength="50" name="txtBatchCode" id="txtBatchCode"/>
							  <input type="hidden" class="form-control" maxlength="50" name="txtitgkcode" id="txtitgkcode"/>
							  <input type="hidden" class="form-control" maxlength="50" name="checkedtype" id="checkedtype"/>
							  <input type="hidden" class="form-control" maxlength="50" name="action" id="action" value="ADD"/>
							</div>
							
				<div id="applyfor" style="display:none;">
					<div class="container">
						<div id="applytypeduplicate" class="col-sm-4 form-group"> 
						  <label for="application_for">Application For:<span class="star">*</span></label>
						  <select id="ddlApplication" name="ddlApplication" class="form-control" onchange="toggle_visibility1('provisional')">      
								
						  </select>
						</div>
						
						  <div class="col-sm-4 form-group" id="type" style="display:none;">
							<label for="sdate">Correction For:<span class="star">*</span></label>

                              
							<div class="multiselect">
								<div class="selectBox" onclick="showCheckboxes()">
									<select id="chk" name="chk" class="form-control" >
										<option>Select Correction For</option>
									</select>
									<div class="overSelect"></div>
								</div>
								<div id="checkboxes" style="width:101%">
									<label for="one">
										<input type="checkbox" id="Name Correction" name="chks" class="chk" value="1" />Name Correction</label>
									<label for="two">
										<input type="checkbox" id="Father/Husband Name Correction"  name="chks" class="chk" value="2" />Father/Husband Name Correction
											</label>
									<label for="three">
										<input type="checkbox" id="Photo Correction" name="chks" class="chk" value="3"/>Photo Correction</label>
								</div>
							</div>
						</div>
						
					<div id="applytypecorrection" style="display:none;">
						<div class="col-sm-4 form-group"> 
						  <label for="application_for">Application For:<span class="star">*</span></label>
						  
						<input type="text" class="form-control" maxlength="50" readonly="true" name="applytype" id="applytype"/>
						</div> 
					
						<div class="col-sm-5"> 
						  <label for="application_for">Correction Type:<span class="star">*</span></label>
						  
						<input type="text" class="form-control" maxlength="500" readonly="true" name="correctiontype" 
							id="correctiontype"/>
						</div>					
					</div>
				</div>
								
				</div>
				
				
						<div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" 
										style="margin-top:25px"/>    
                        </div>
			</div>
						
				<div class="container">
					<input type="button" name="btnapp" id="btnapp" class="btn btn-primary" value="View Application" 
							style="display:none;"/>
				</div>

				<div id="main-content" style="display:none;"> 
				  <div class="container">
						<div class="col-sm-4 form-group"> 
						  <label for="learnername">Learner Correct Name:<span class="star">*</span></label>
						  <input type="text" class="form-control text-uppercase" maxlength="50" name="txtLCorrectName" id="txtLCorrectName" onkeypress="javascript:return allowchar(event);" placeholder="Learner Correct Name">     
						</div>
						
					   <div class="col-sm-4 form-group">     
						  <label for="faname">Father/Husband Correct Name:<span class="star">*</span></label>
						  <input type="text" class="form-control text-uppercase" id="txtFCorrectName" name="txtFCorrectName" onkeypress="javascript:return allowchar(event);" placeholder="Father Correct Name">
					   </div>				  
						
					   <div class="col-sm-4 form-group" id="selecteventdiv" style="display:none;"> 
						  <label for="event_name">Exam Event Name:<span class="star">*</span></label>
						  <select id="ddlEventName" name="ddlEventName" class="form-control" >
							
						  </select> 
							<input type="hidden" class="form-control" name="txtEventName" id="txtEventName"/>
					   </div>
					   
					   <div class="col-sm-4 form-group" id="txteventdiv" style="display:none;">     
						  <label for="faname">Exam Event Name:<span class="star">*</span></label>
							<input type="text" class="form-control" name="txtEventNamediv" id="txtEventNamediv"/>
					   </div>
					   
					   <div class="col-sm-4 form-group"> 
						  <label for="t_marks">Total Marks:<span class="star">*</span></label>
						  <input type="text" maxlength="3" class="form-control" name="txtMarks" onkeypress="javascript:return allownumbers(event);" id="txtMarks" placeholder="Total Marks">    
					   </div>
				  </div>
    
				  <div class="container">    
					 
						<div class="col-sm-4 form-group">     
						  <label for="email">Learner's Email id:<span class="star">*</span></label>
						  <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="Email id">
						</div>        
						
						<div class="col-sm-4 form-group">     
						  <label for="mobile_no">Learner's Mobile No:<span class="star">*</span></label>
						  <input type="text" class="form-control" maxlength="10" id="txtMobile" name="txtMobile" onkeypress="javascript:return allownumbers(event);" placeholder="Mobile No">
						</div>
						
						<div class="col-sm-4 form-group"> 
						  <label for="dob">Learner's DOB:<span class="star">*</span></label>
						  <input type="text" class="form-control" id="txtDob" name="txtDob" placeholder="D.O.B">
						</div>
						
						<div class="col-sm-4 form-group"> 
						  <label for="event_name">ITGK District:<span class="star">*</span></label>
						  <input type="text" class="form-control" id="txtDistrictName" name="txtDistrictName" 
									placeholder="District Name">
						<input type="hidden" class="form-control" id="ddlDistrictName" name="ddlDistrictName" 
									placeholder="District Name">
						</div>
				  </div>

				
    
					<div class="container">						
						<div class="col-sm-4 form-group" id="pic" style="display:none;"> 
							<label for="photo">Learner Photo:<span class="star">*</span></label> </br>
							<div id="uploadPreview9"></div>
							<img id="uploadPreview1" src="images/user icon big.png" id="uploadPreview1" name="filePhoto" 
								width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">
						</div>
					
					
						  <div class="col-sm-4 form-group" id="photodiv" style="display:none;"> 
							  <label for="photo">Attach Learner's Photo:<span class="star">*</span></label>
							  <input type="file" class="form-control" id="filePhoto" name="filePhoto" onchange="checkPhoto(this)">
							  <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =5KB</span>
						  </div>   
						 
						  <div class="col-sm-4 form-group"> 
							  <label for="marksheet">Attach Marksheet:<span class="star">*</span></label>
							  <input type="file" class="form-control" id="fileMarkSheet" name="fileMarkSheet" onchange="checkMarksheet(this)">
							  <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =200KB</span>
						  </div>
					   
						  <div class="col-sm-4 form-group"> 
							  <label for="rs_cit_certficate">Attach Final RS-CIT Certificate:<span class="star">*</span></label>
							  <input type="file" class="form-control" id="fileCertficate" name="fileCertficate" onchange="checkCertificate(this)">
							  <span style="font-size:10px;"> Note : JPG,JPEG Allowed Max Size =200KB</span>
						  </div>
						  
						  <div class="col-sm-4 form-group" id="provisional" style="display:none;"> 
							  <label for="rs_cit_provisional">Attach RS-CIT Provisional:</label>
							  <input type="file" class="form-control" id="fileProvisional" name="fileProvisional" onchange="checkProvisional(this)">
							  <span style="font-size:10px;">Note : (In Duplicate Case)JPG,JPEG Allowed Max Size =200KB</span>
						   </div>
					
						<div class="col-sm-6 form-group"> 
							  <label for="rs_cit_provisional">Attach Correction/Duplicate Application Form:</label>
							  <input type="file" class="form-control" id="fileApplication" name="fileApplication" onchange="checkApplication(this)">
							  <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =200KB</span>
						</div>
						
						<div class="col-sm-6 form-group" id="Affidavitdiv" style="display:none;"> 
							  <label for="rs_cit_provisional">Attach Affidavit:</label>
							  <input type="file" class="form-control" id="fileAffidavit" name="fileAffidavit" onchange="checkAffidavit(this)">
							  <span style="font-size:10px;">Note : (In Duplicate Case)JPG,JPEG Allowed Max Size =200KB</span>
						</div>
					</div>

					<div class="container">
						<div class="col-sm-5"> 
							  <label for="">Correspondence Address of Learner:</label>
							  <textarea id="txtaddress" class="form-control" cols="59" rows="3" name="txtaddress"onkeypress="javascript:return validAddress(event);"></textarea>
							  						  
						</div>
						
						<div class="col-sm-3 form-group"> 
						  <label for="event_name">Learner's District:<span class="star">*</span></label>
						  <select id="ddllearnerdistrict" name="ddllearnerdistrict" class="form-control">
							
						  </select> 
						
						</div>
						
						<div class="col-sm-3 form-group"> 
						  <label for="event_name">Learner's PinCode:<span class="star">*</span></label>
						  <input type="text" class="form-control" maxlength="6" id="txtlearnerpincode" name="txtlearnerpincode" 
									placeholder="PinCode" onkeypress="javascript:return allownumbers(event);">						
						</div>
						
						
						
					</div>
						
						
				<!--		<div class="container">  
							<div class="col-md-4 form-group"> 							
								<label for="learnercode">Terms and conditions :<span class="star">*</span></label></br>                              
								<label class="checkbox-inline"> <input type="checkbox" name="chk" id="chk" value="1" >
									<a title="" style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><span id="fix">click here</span> </a>
								</label>								
                            </div>
						</div>	-->
						
						<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button class="close" type="button" data-dismiss="modal">×</button>
									<h3 class="modal-title">Terms and Conditions</h3>
								</div>
								<div class="modal-body">
									<img src="images/1.jpg" />
								</div>
								<div class="modal-footer">
									<button class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
						    </div>
						  </div>
						</div>
      
						<div class="container" style="margin-top:15px;margin-left:12px;">
							<input type="button" name="btnotpsent" id="btnotpsent" class="btn btn-primary" value="Send OTP"/> 
							<input type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload"
							style="display:none;"/>
							<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" 
							style="display:none;"/>
						</div>
						
						<div class="container">						
							<div id="userfields">
								
							</div>
							<div id="responses"></div>
						</div>
						
						<div class="container" style="margin-top:10px;margin-left: 20px;">	
                           <input type="button" name="btnotpvalidate" id="btnotpvalidate" class="btn btn-primary" value="Validate OTP"
						   style="display:none;"/>
						
                            <input type="button" name="btnresentotp" id="btnresentotp" class="btn btn-primary" value="Resend OTP"
							style="display:none;"/>    
                       </div>
				</form>
			</div>
          </div>   
        </div>
	</div> 
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>		

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
    <style>


        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;

        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
        }

        #checkboxes label {
            display: block;
        }

        #checkboxes label:hover {
            background-color: #1e90ff;
        }
    </style>
</body>

<?php include'common/message.php';?>
<?php include ('footer.php'); ?>

<script src="scripts/correctionfileupload.js"></script>

<style>
#errorBox{
 color:#F00;
 }
</style>

<style>
  .modal-dialog {width:700px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
<script type="text/javascript">  
  $(document).ready(function() {
		jQuery(".fix").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>
 <script>
	var expanded = false;

	function showCheckboxes() {
		var checkboxes = document.getElementById("checkboxes");
		if (!expanded) {
			checkboxes.style.display = "block";
			expanded = true;
		} else {
			checkboxes.style.display = "none";
			expanded = false;
		}
	}
</script>
<script type="text/javascript"> 
 $('#txtDob').datepicker({                   
	format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
});  
</script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>

<script language="javascript" type="text/javascript">
        function validAddress(e) {
    var key = window.event ? e.keyCode : e.which;
    var keychar = String.fromCharCode(key);
	var reg = new RegExp("^[ 0-9A-Za-z,-]*$");
    //var reg = new RegExp("[A-Za-z,-0-9]");

    if (key === 8 || key === 0 || key === 32) {
        keychar = "a";
    }
    return reg.test(keychar);
}
</script>

<script language="javascript" type="text/javascript">
$('.chk').on('change',function(){
    var _val = $(this).val();	
	if(_val!=''){
		$('#btnapp').show();
		var selMultiC = $.map($("input[name='chks']:checked"), function (el, i) {
         //return $(el).val();
         return $(el).attr("id");
     });
     var topivalC = selMultiC.join(", ");
		$("#correctiontype").val(topivalC);
	}
	else{
		$('#btnapp').hide();
	}
	
	 

	
});
</script>

<script type="text/javascript">
    function toggle_visibility1(id) {
		var f='';
        var e = document.getElementById(id);
		   //alert(e);
        f = document.getElementById('ddlApplication').value;        
        if (f == "9") {
            e.style.display = 'block';
			$("#main-content").show();
			$("#type").hide();			
			$("#pic").show();
			$("#photodiv").show();
			$("#Affidavitdiv").show();
			$('#btnapp').hide();
			document.getElementById("txtLCorrectName").readOnly = true;
			document.getElementById("txtFCorrectName").readOnly = true;
        }
		else if (f=='7'){
			e.style.display = 'block';
			$("#main-content").hide();
			document.getElementById("txtLCorrectName").readOnly = false;
			document.getElementById("txtFCorrectName").readOnly = false;
			$("#pic").show();
			$("#photodiv").show();
			$("#type").show();
			$("#Affidavitdiv").show();
		}
		else {
            e.style.display = 'none';
			$("#main-content").hide();
			document.getElementById("txtLCorrectName").readOnly = false;
			document.getElementById("txtFCorrectName").readOnly = false;
			$("#photodiv").show();
			$("#type").show();
			$("#Affidavitdiv").hide();
        }
    }
</script>

<script language="javascript" type="text/javascript">
function checkPhoto(target) {
	var ext = $('#filePhoto').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("filePhoto").value = '';
			return false;
		}

    if(target.files[0].size > 5050) {			       
		alert("Image size should less or equal 5 KB");
		document.getElementById("filePhoto").value = '';
        return false;
    }
	else if(target.files[0].size < 3000) {
				alert("Image size should be greater than 3 KB");
				document.getElementById("filePhoto").value = '';
				return false;
			}
    document.getElementById("filePhoto").innerHTML = "";
    return true;
}
</script>


<script language="javascript" type="text/javascript">
function checkMarksheet(target) {
	var ext = $('#fileMarkSheet').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileMarkSheet").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			        
		alert("Image size should less or equal 200 KB");
		document.getElementById("fileMarkSheet").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("fileMarkSheet").value = '';
				return false;
	}    
    document.getElementById("fileMarkSheet").innerHTML = "";
    return true;
}
</script>

<script language="javascript" type="text/javascript">
function checkCertificate(target) {	
	var ext = $('#fileCertficate').val().split('.').pop().toLowerCase();	   
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {			
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileCertficate").value = '';
			return false;
		}

		if(target.files[0].size > 200000) {	        
		alert("Image size should less or equal 200 KB");
		document.getElementById("fileCertficate").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("fileCertficate").value = '';
				return false;
	}     
    document.getElementById("fileCertficate").innerHTML = "";
    return true;
}
</script>

<script language="javascript" type="text/javascript">
function checkApplication(target) {	
	var ext = $('#fileApplication').val().split('.').pop().toLowerCase();	   
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {			
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileApplication").value = '';
			return false;
		}

		if(target.files[0].size > 200000) {	        
		alert("Image size should less or equal 200 KB");
		document.getElementById("fileApplication").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("fileApplication").value = '';
				return false;
	}     
    document.getElementById("fileApplication").innerHTML = "";
    return true;
}
</script>

<script language="javascript" type="text/javascript">
function checkProvisional(target) {
	var ext = $('#fileProvisional').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileProvisional").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			       
		alert("Image size should less or equal 200 KB");
		document.getElementById("fileProvisional").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("fileProvisional").value = '';
				return false;
	}     
    document.getElementById("fileProvisional").innerHTML = "";
    return true;
}

</script>

<script language="javascript" type="text/javascript">
function checkAffidavit(target) {
	var ext = $('#fileAffidavit').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileAffidavit").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			       
		alert("Image size should less or equal 200 KB");
		document.getElementById("fileAffidavit").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("fileAffidavit").value = '';
				return false;
	}     
    document.getElementById("fileAffidavit").innerHTML = "";
    return true;
}

</script>	

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

		function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();
			
		function FillApplicationFor() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FillApplicationFor",
                success: function (data) {
                    $("#ddlApplication").html(data);
                }
            });
        }
        FillApplicationFor();
		
		function FillLearnerDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=GetLearnerDistrict",
                success: function (data) {
                    $("#ddllearnerdistrict").html(data);
                }
            });
        }
        FillLearnerDistrict();
		
		function multisel() {
                $('#ddlBatch').multiselect({
                    nonSelectedText: 'Select Batch',
                    includeResetOption: true,
                    resetText: "Reset all",
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,

                    minHeight: 200,
                    maxHeight: '200',
                    width: '33%',
                    buttonWidth: '100%',
                    dropRight: true

                });
            }
		
		function FillExamEvent() {
			var batchcode = document.getElementById('txtBatchCode').value;			 
				if(batchcode>=90){					
					$.ajax({
						type: "post",
						url: "common/cfcorrectionform.php",
						data: "action=FillEventByLcode&values=" + txtLCode.value + "",
						success: function (data) {						
							if(data==""){								
								selectevent();								
							}
							else{								
								data = $.parseJSON(data);
								$("#txteventdiv").show();
								$("#selecteventdiv").hide();
								txtEventName.value = data[0].Event_Name;	
								txtEventNamediv.value = data[0].Event_Name;
								txtMarks.value = data[0].tot_marks;
								document.getElementById("txtEventNamediv").readOnly = true;
								document.getElementById("txtMarks").readOnly = true;
							}							
						}
					});
				}
				else{
					selectevent();					
				}            
        }
        
		function selectevent() {			
            $.ajax({
						type: "post",
						url: "common/cfcorrectionform.php",
						data: "action=FillExamEvent",
						success: function (data) {
							$("#selecteventdiv").show();
							$("#txteventdiv").hide();
							$("#ddlEventName").html(data);
						}
					});
        }
		
		$("#ddlEventName").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FillEventById&values=" + ddlEventName.value + "",
                success: function (data) {
                    txtEventName.value = data;	
                    txtEventNamediv.value = data;	
                }
            });
        });
		
		$("#ddlCorrectionType").change(function () {
            var typevalue = document.getElementById('ddlCorrectionType').value;
				if(typevalue==''){
					$("#main-content").hide();
				}
				else if(typevalue=='1'){
					$("#main-content").show();
					document.getElementById("txtLCorrectName").readOnly = false;
					document.getElementById("txtFCorrectName").readOnly = true;
				}
				else if(typevalue=='2'){
					$("#main-content").show();
					document.getElementById("txtLCorrectName").readOnly = true;
					document.getElementById("txtFCorrectName").readOnly = false;
				}
        });

			
        function FillItgkDistrict(itgkcode) {			
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FillITGKDistrict&itgkcode=" + itgkcode + "",
                success: function (data) {					
					data = $.parseJSON(data);
					txtDistrictName.value = data[0].District_Name;
					ddlDistrictName.value = data[0].District_Code_VMOU;
					$('#txtDistrictName').attr('readonly', true);
                }
            });
        }
       
       if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=DELETE&values=" + StatusCode + "",
                success: function (data) {
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }
		
		$("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");		
					$.ajax({
						type: "post",
                        url: "common/cfcorrectionform.php",
						data: "action=DETAILS&values=" + txtLCode.value + "",
						success: function (data)
						{									
							 $('#response').empty();
							 if(data == ""){
							 $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Learnercode" + "</span></p>");
							 }
							 else{
								data = $.parseJSON(data);
								txtLCorrectName.value = data[0].Admission_Name;
								txtFCorrectName.value = data[0].Admission_Fname;							
								txtEmail.value = data[0].Admission_Email;							
								txtMobile.value = data[0].Admission_Mobile;							
								txtaddress.value = data[0].Admission_Address;							
								txtBatchCode.value = data[0].Admission_Batch;
								txtitgkcode.value = data[0].Admission_ITGK_Code;
								$("#uploadPreview9").html(data[0].learner_photo);
								//$("#uploadPreview1").attr('src', "upload/admission_photo/" + data[0].photo);
								$("#uploadPreview1").hide();
								$("#main-content").hide();
								$("#applyfor").show();
								$('#txtLCode').attr('readonly', true);
								$("#btnShow").hide();
								FillExamEvent();
								FillItgkDistrict(data[0].Admission_ITGK_Code);
							 }
							
					    }
                });		
          });
			
			$("#btnotpsent").click(function (){
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
                    type: "post",
                    url: "common/cfcorrectionform.php",
                    data: "action=SendSMS&mobileno=" + txtMobile.value + "",
                    success: function (data) {						
						$('#responses').empty();
						data = $.parseJSON(data);
						$('#userfields').html(data.frm);
                        $("#btnotpsent").hide(1000);
                        $("#btnotpvalidate").show(1000);
                        $("#btnresentotp").show(1000);						
					}
                });
			});
			
		   $("#btnotpvalidate").click(function () {
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				
				var url = "common/cfcorrectionform.php";
				var data = "action=VerifyOTP&otp=" + txtOTP.value +  "&oid=" + oid.value + "";
				$.ajax({
					type: "POST",
					url: url,
					data: data,
					success: function (data) {
						$('#responses').empty();						
						$("#responses").css({"margin-top": "20px"});
						if (data==0) 
						{
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Invalid OTP. Please try again." + "</span></p>");
						} 
						else if(data=='OTPBLANCK'){
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Please enter valid OTP." + "</span></p>");
						}
						else if(data=='EXPIRE'){
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Your OTP has Expired. Please try again." + "</span></p>");
						}
						else if(data=='NORECORD'){
							$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Your OTP has Expired or Invalid. Please try again." + "</span></p>");
						}
						else {							
							data = $.parseJSON(data);
							$('#txtMobile').attr('readonly', true);
							$("#btnSubmit").show(1000);
							$("#userfields").hide(1000);
							$("#btnotpvalidate").hide(1000);
							$("#btnresentotp").hide(1000);							
						}
					}
				});
		});
		
		$("#btnresentotp").click(function () {                
			$('#responses').empty();
			$('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				var url = "common/cfcorrectionform.php"; // the script where you handle the form input.
				var data = "action=RESENDOTP&mobileno=" + txtMobile.value + ""; // serializes the form's elements.
                    $.ajax({
						type: "POST",
						url: url,
						data: data,
						success: function (data) {
							$('#responses').empty();
							if (data != 0) {
								data = $.parseJSON(data);
								$('#userfields').html(data.frm);
								$("#btnotpvalidate").show(1000);
								$("#btnresentotp").show(1000);								 
							} else {
								$('#responses').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Invalid Mobile Number. Please try again." + "</span></p>");
							}
						}
					});                
    	       });
			   

        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=EDIT&values=" + StatusCode + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    txtStatusName.value = data[0].StatusName;
                    txtStatusDescription.value = data[0].StatusDescription;
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=SHOW",
                success: function (data) {
                    $("#gird").html(data);
                }
            });
        }

		$("#btnapp").click(function () {
			$('#main-content').show(3000);
			$('#btnapp').hide();
			$('#type').hide();
			$('#applytypeduplicate').hide();
			$('#applytypecorrection').show();
			
			var c = [];
            $.each($("input[name='chks']:checked"), function(){            
                c.push($(this).val());
            });
            var chks = c.join(",");
			$("#checkedtype").val(chks);
			f = document.getElementById('ddlApplication').value;
				if(f=='7'){
					applytype.value ='Duplicate Certificate with Correction';
				}
				else{
					applytype.value ='Correction Certificate';
				}
				
			if(chks=='1'){
					$('#txtFCorrectName').attr('readonly', true);
					$("#pic").show();
				}
				else if(chks=='2'){
					$('#txtLCorrectName').attr('readonly', true);
					$("#pic").show();
				}
				else if(chks=='3'){
					$('#txtLCorrectName').attr('readonly', true);
					$('#txtFCorrectName').attr('readonly', true);
					$("#pic").show();
					$("#photodiv").show();
				}
				else if(chks=='1,3'){
					$('#txtFCorrectName').attr('readonly', true);
					$("#pic").show();
				}
				else if(chks=='2,3'){
					$('#txtLCorrectName').attr('readonly', true);
					$("#pic").show();
				}
				else if(chks=='1,2'){
					$("#pic").show();
				}
				else if(chks=='1,2,3'){
					$("#pic").show();
				}
				else{
					$('#txtLCorrectName').attr('readonly', true);
					$('#txtFCorrectName').attr('readonly', true);
					$("#pic").show();
					$("#photodiv").show();
					$("#btnotpsent").hide();
				}
		});
		
		$("#form").on('submit',(function(e) {
			if ($("#form").valid())
    {  $('#response').empty();
       $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
       e.preventDefault();
      $.ajax({ 
        url: "common/cfcorrectionform.php",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data)
          { 
			var result = data.trim();
              if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + result + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmcorrection.php";
                        }, 2000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></p>");
                    }
          },
            error: function(e) 
            {
               $("#errmsg").html(e).fadeIn();
            } 	        
    });
    }
    return false;
}));      
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcorrection_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
label {
    font-size: 13px;
}
</style>
</html>