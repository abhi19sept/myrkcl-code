<?php
$title = "WCD Learner Count List";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['batch'])) {    
    echo "<script>var batch=" . $_REQUEST['batch'] . "</script>";
    echo "<script>var mode='" . $_REQUEST['mode'] . "'</script>";
    echo "<script>var districtcode='" . $_REQUEST['districtcode'] . "'</script>";
	echo "<script>var itgkcode='" . $_REQUEST['itgk'] . "'</script>";
	echo "<script>var category='" . $_REQUEST['cat'] . "'</script>";
	echo "<script>var spcategory='" . $_REQUEST['category'] . "'</script>";
} else {   
    echo "<script>var batch='0'</script>";
    echo "<script>var mode='0'</script>";
	echo "<script>var districtcode='0'</script>";
    echo "<script>var itgkcode='0'</script>";
    echo "<script>var category='0'</script>";
    echo "<script>var spcategory='0'</script>";
}
?>
<link href="css/popup.css" rel="stylesheet" type="text/css">
    <div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 


    <div class="panel panel-primary" style="margin-top:36px !important;">  
        <div class="panel-heading">ITGK Wise Merit List for Approval of Resevered Learners</div>
        <div class="panel-body">

            <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>


                    <div class="container">
                        <div id="gird" style="margin-top:25px; width:94%;"> </div>
                    </div>
					
					<?php if($_REQUEST['mode'] == 'one')  { ?>						
					<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Approve" style="display:none; margin-top:15px;"/>    
                    </div> 
					 <?php } ?> 
                </div>   
            </form>
        </div>
    </div>
</div>
</div>


<!-- View Image Popup -->
<div id="myModalimage" class="modal">
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h6>Image Preview</h6>
    </div>
      <div class="modal-body" style="text-align: center;">
        <img id="viewphoto" src="" style="width:960px;">
    </div>
  </div>
</div>
<!-- End View Image Popup -->


<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showData() {
			 $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
              
            var url = "common/cfWcdlearnercount.php"; // the script where you handle the form input.

            var data;
            //alert (role_type);
            data = "action=GETREVLEARNERLIST&mode=" + mode + "&batch=" + batch + "&districtcode=" + districtcode + "&itgkcode=" + itgkcode + "&category=" + category+"&spcategory=" + spcategory +""; //

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					$('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        paging: false,
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						$('#btnSubmit').show();
                }
            });
        }
        showData();
		
		//View Marital Status Popup .............
			$("#gird").on("click",".viewimage",function(){ 
				var  mybtn = $(this).attr("id");
				var mybtnid = "myBtn_"+mybtn;
				var fileab = '#file_id_'+mybtn;
				var file_id = $(fileab).val();
				
				if(file_id == 'NA'){
							$("#viewphoto").attr('src',"images/not-found.png");
							//$("#heading-tittle").html('Birth Proof Not Available');
						}else{
							var url = "upload/oasis_marital_status_proof/"+file_id;
							$('#viewphoto').attr('src',url);
						}
				var modal = document.getElementById('myModalimage');
				var btn = document.getElementById(mybtnid);
				var span = document.getElementsByClassName("close")[0];
				modal.style.display = "block";
				span.onclick = function() { 
					modal.style.display = "none";
				}
				window.onclick = function(event) {
					if (event.target == modal) {
						modal.style.display = "none";
					}
				}
			});

			$("#gird").on("click",".viewimage",function(){ 
				var  mybtn = $(this).attr("id");
				var mybtnid = "myBtn_"+mybtn;
				var fileab = '#file_id_viol'+mybtn;
				var file_id = $(fileab).val();
				
				if(file_id == 'NA'){
							$("#viewphoto").attr('src',"images/not-found.png");
							//$("#heading-tittle").html('Birth Proof Not Available');
						}else{
							var url = "upload/oasis_marital_status_proof/"+file_id;
							$('#viewphoto').attr('src',url);
						}
				var modal = document.getElementById('myModalimage');
				var btn = document.getElementById(mybtnid);
				var span = document.getElementsByClassName("close")[0];
				modal.style.display = "block";
				span.onclick = function() { 
					modal.style.display = "none";
				}
				window.onclick = function(event) {
					if (event.target == modal) {
						modal.style.display = "none";
					}
				}
			});
			
			
			//View Sub-Category Status Popup .............
			$("#gird").on("click",".sub_category",function(){ 
				var  mybtn = $(this).attr("id");
				var mybtnid = "myBtn_"+mybtn;
				var fileab = '#file_id_sub'+mybtn;
				var file_id = $(fileab).val();
				
				if(file_id == 'NA'){
							$("#viewphoto").attr('src',"images/not-found.png");
							//$("#heading-tittle").html('Birth Proof Not Available');
						}else{
							var url = "upload/oasis_sub_category_proof/"+file_id;
							$('#viewphoto').attr('src',url);
						}
				var modal = document.getElementById('myModalimage');
				var btn = document.getElementById(mybtnid);
				var span = document.getElementsByClassName("close")[0];
				modal.style.display = "block";
				span.onclick = function() { 
					modal.style.display = "none";
				}
				window.onclick = function(event) {
					if (event.target == modal) {
						modal.style.display = "none";
					}
				}
			});
			
			
			//View Caste Proof Status Popup .............
			$("#gird").on("click",".caste",function(){ 
				var  mybtn = $(this).attr("id");
				var mybtnid = "myBtn_"+mybtn;
				var fileab = '#file_id_cat'+mybtn;
				var file_id = $(fileab).val();
				
				if(file_id == 'NA'){
							$("#viewphoto").attr('src',"images/not-found.png");
							//$("#heading-tittle").html('Birth Proof Not Available');
						}else{
							var url = "upload/oasis_caste_proof/"+file_id;
							$('#viewphoto').attr('src',url);
						}
				var modal = document.getElementById('myModalimage');
				var btn = document.getElementById(mybtnid);
				var span = document.getElementsByClassName("close")[0];
				modal.style.display = "block";
				span.onclick = function() { 
					modal.style.display = "none";
				}
				window.onclick = function(event) {
					if (event.target == modal) {
						modal.style.display = "none";
					}
				}
			});

		$("#btnSubmit").click(function () {	
		$('#btnSubmit').prop('disabled', true); 
			$('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdlearnercount.php"; // the script where you handle the form input.
            var formdata = $("#form").serialize();
            var data;          
				 data = "action=UpdateApproveReserveLearner&batch=" + batch + "&itgkcode=" + itgkcode + "&" + formdata; // serializes the form's elements.
			 $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
					//alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmWcdReservedLearnerCountList.php?batch=" + batch + "&itgk=" + itgkcode + "&districtcode=" + districtcode + "&mode=two&cat=" + category + "&category=" + spcategory + "";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span>          <span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });
		return false;
		  });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<style>
.table > tbody > tr > td {
     vertical-align: middle;
}
</style>
</html>
