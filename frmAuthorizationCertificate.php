<?php
$title = "Authorization Certificate";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:450px !important;max-height:auto !important">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading"> Authorization Certificate Report</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="frmsearchitgk" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
						 <div class="col-sm-4 form-group"> 
                            <label for="designation">District:<span class="star">*</span></label>
                            <select id="ddlDistrict" name="ddlDistrict" class="form-control">                                  
                            </select>
                        </div>     

                        <div class="col-sm-4 form-group"> 
                            <label for="marks">Tehsil:<span class="star">*</span></label>
                            <select id="ddlTehsil" name="ddlTehsil" class="form-control">                                 
                            </select>
                        </div>
						
						<div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
                        </div>
                    </div>
                   

                    <div id="grid" name="grid" style="margin-top:35px;"> </div> 


            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		 function FillDistrict() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        }
        FillDistrict();
		
		$("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });
		
        $("#btnShow").click(function () {
            //alert("hello");
			$('#response').empty();
			$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAuthorizationCertificate.php",
                data: "action=GetItgkDetails&district=" + ddlDistrict.value + "&tehsil=" + ddlTehsil.value + "",
                success: function (data)
				{
					if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please Select District" + "</span></div>");
                            
						}
					else if (data == 2){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +   "Please Select Tehsil" +
							"</span></div>");
                            
						}
						
					else {
						$('#response').empty();
						$("#grid").html(data);
						$('#example').DataTable({
							dom: 'Bfrtip',
							buttons: [
								'copy', 'csv', 'excel', 'pdf', 'print'
							]
						});
					}
                }
            });
         });
       
    });
   

</script>
</html>