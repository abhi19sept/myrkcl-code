<?php
$title = "RSP Address Update";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1'|| $_SESSION['User_UserRoll'] == '28') {
    //Login here with ITGK or Superadmin
} else {
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>
<style>
    .modal {z-index: 1050!important;} 
    #turmcandition{
        background-color: whitesmoke;
        text-align: justify;
        padding: 10px;
    }
    .noturm{
        margin-left: 10px !important;
    }
    .yesturm{
        margin-right: 15px;
    }
</style>
<div style="min-height:430px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Update RSP Address</div>
            <div class="panel-body">
                <div class="container">
                    <div id="grid" style="width: 94%;"></div>
                </div>  
            </div>
        </div>
    </div>
</div>
<link href="css/popup.css" rel="stylesheet" type="text/css">
<div id="myModalupdate" class="modal" style="padding-top:90px !important;">         
    <div class="modal-content" style="width: 80%;border-radius: 15px; text-align: center;">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h6>Update Address</h6>
        </div>
        <div class="modal-body" style="min-height: 175px;padding: 0 16px;" id="Eresponse">
            <form class="form-horizontal" name="frmupdateaddress" id="frmupdateaddress" enctype="multipart/form-data" style="margin-top:20px;">
                <input type="hidden" name="action" id="action" value="UPDATE"/>
                <input type="hidden" name="code" id="code" value=""/>
                <div class="form-group">
				
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">State :</label>
					<div class="col-sm-6">
						
						<select class="form-control" id="ddlState" name="ddlState" required>

						</select>
					</div>
				</div>
                
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">District :</label>
					<div class="col-sm-6">
						
						<select class="form-control" id="ddlDistrict" name="ddlDistrict" required>

						</select>
					</div>
				</div>
				
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Tehsil :</label>
					<div class="col-sm-6">
						
						<select class="form-control" id="ddlTehsil" name="ddlTehsil" required>

						</select>
					</div>
				</div>
				
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Address: </label>
					<div class="col-sm-6 form-group">
						
						 <textarea  class="form-control"  maxlength="160" rows="4" cols="100" id="txtaddress" name="txtaddress" placeholder="Enter Address"  onkeypress="javascript:return validAddress(event);" style="margin-left:15px;"></textarea>
					</div>
				</div>
				<div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6" id="responseupdate"></div>
                </div>
				<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label"></label>
                    <div class="col-sm-6 form-group">
                        <button type="button" name="btn-update-submit"  id="btn-update-submit" class="btn btn-success btn-sm">Update</button>		
                    </div>
                </div>
				
				
                </div>
                
                
               
                
                
            </form> 
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function ShowDetails() {
            $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
            setTimeout(function () {
                $('#grid').load();
            }, 2000);
            var url = "common/cfSpaddressupdate.php";
            var data;
            data = "action=Getaddress";
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    data = $.parseJSON(data);
                }
            });
        }
        ShowDetails();
        $("#btn-update-submit").click(function () 
        {
            $('#responseupdate').html("<div class='alert-success'><span><img src=images/ajax-loader.gif style='width: 15px; margin: 5px;' /></span><span>Processing.....</span></div>");
            $.ajax({ 
                url: "common/cfSpaddressupdate.php",
                type: "POST",
                data: $("#frmupdateaddress").serialize(),
                success: function(data)
                  { 
                    if(data == "empty"){
                        $('#responseupdate').empty();
                        $('#responseupdate').append("<div class='alert-success'><span><img src=images/error.gif style='width: 15px; margin: 5px;' /></span><span>All Fields Are Required.</span></div>");
                    }
                    else if(data == "Successfully Updated"){
                        $('#responseupdate').empty();
                        $('#responseupdate').append("<div class='alert-success'><span><img src=images/correct.gif style='width: 15px; margin: 5px;' /></span><span>Update Successfully..</span></div>");
                         window.setTimeout(function () {
                                $('#responseupdate').empty();
                                $("#myModalupdate").css({"display": "none"});
                        }, 3000);
                        ShowDetails();
                    }else{
                        $('#responseupdate').empty();
                        $('#responseupdate').append("<div class='alert-success'><span><img src=images/error.gif style='width: 15px; margin: 5px;' /></span><span>Error To Update Address.</span></div>");
                    }
                    
                  },
                error: function(e) 
                {
                   $("#errmsg").html(e).fadeIn();
                } 	        
            });
	});
        
        $("#grid").on("click", ".fun_update_details", function () {
            var orgcode = $(this).attr("id");
            var modal = document.getElementById('myModalupdate');
            var span = document.getElementsByClassName("close")[0];
            modal.style.display = "block";
            span.onclick = function () {
                modal.style.display = "none";
            }
            var url = "common/cfSpaddressupdate.php"; 
            var data;
            data = "action=EDIT&editid=" + orgcode + ""; 				 
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {   
                    data = $.parseJSON(data);
                    ddlState.value = data[0].Organization_State;
                    code.value = data[0].Organization_Code;
                    if(FillDistrict(data[0].Organization_State,data[0].District_Code)){
                        $("#ddlDistrict option[value="+data[0].District_Code+"]").attr("selected","selected") ;
                    }
                    if(FillTeshil(data[0].District_Code,data[0].Tehsil_Code)){
                        $("#ddlTehsil option[value="+data[0].Tehsil_Code+"]").attr("selected","selected") ;
                    }
                    txtaddress.value = data[0].Org_formatted_address; 
                }
            });
        });
        
        FillStates();
        function FillStates() {
            $.ajax({
                url: 'common/cfSpaddressupdate.php',
                type: "post",
                data: "action=FILLSTATE",
                success: function (data) {
                    $('#ddlState').html(data);
                }
            });
        }
        $("#ddlState").change(function () {
            var selState = $(this).val();
            FillDistrict(selState);
        });
        function FillDistrict(selState,district){
           $.ajax({
                url: 'common/cfSpaddressupdate.php',
                type: "post",
                data: "action=FILLDISTRICT&values=" + selState + "&dist="+district,
                success: function (data) {
                    $('#ddlDistrict').html(data);
                }
            }); 
        }
        FillDistrict();
        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            FillTeshil(selDistrict);
        });
        function FillTeshil(selDistrict,tehsil){
            $.ajax({
                url: 'common/cfSpaddressupdate.php',
                type: "post",
                data: "action=FILLTEHSHIL&values=" + selDistrict + "&tehsil="+tehsil,
                success: function (data) {
                    $('#ddlTehsil').html(data);
                }
            });
        }
        FillTeshil();
    });

</script>
<script>
    $("#frmupdateaddress").validate({
        rules: {
            txtaddress: "required",
            ddlDistrict: "required",
            ddlTehsil: "required",
            ddlState: "required"

        },
        messages: {
            txtaddress: "Please Enter Address",
            ddlDistrict: "Please Select District",
            ddlTehsil: "Please Select Tehsil",
            ddlState: "Please Select State"
        },
    });
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
        background-color: #e2d9d9;
        width: 91%;
    }
</style>
</html>
?>