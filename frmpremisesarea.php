<?php
$title = "Submit Premises Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

		


<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Update/Submit Premises Aera</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmpremisesarea" id="frmpremisesarea" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">Verify Currrent data and Update if Required.</div>
                        <div class="panel-body">

                            <div class="container">
                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Current Total Area (Theory + LAB) in Sq.Ft.:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtarea" id="txtarea" placeholder="Total Area">
                                </div>
                                
                                <div class="col-sm-4 form-group">     
                                        <label for="area">Do you have Separate Lab and Theory Rooms:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="areaUrban" name="area" value="Urban" onChange="findselected()"/> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="areaRural" name="area" value="Rural"onChange="findselected1()"/> No </label>
                                    </div>

                            </div>
                            
                            <div class="container" id="Urban" style="display:none">
                                       

                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">Enter Theory Room Area in Sq.Ft.:</label>
                                    <input type="text" class="form-control" name="TheoryRoomArea" id="TheoryRoomArea" onkeyup="calc()" value="0" placeholder="Theory Room Area" onkeypress="javascript:return allownumbers(event);">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="faname">Enter Total LAB Room Area in Sq.Ft.:</label>
                                    <input type="text" class="form-control" name="LabArea" id="LabArea" onkeyup="calc()" value="0" placeholder="LAB Area" onkeypress="javascript:return allownumbers(event);">
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Total Area (Theory + LAB) in Sq.Ft.:</label>
                                    <input type="text" class="form-control" readonly="true" name="TotalArea" id="TotalArea" onkeyup="calc()" value="0" placeholder="Total Area" onkeypress="javascript:return allownumbers(event);">  
                                </div>
                                    </div>

                                    <div class="container" id="Rural" style="display:none">
                                       <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Enter Total Area (Theory + LAB) in Sq.Ft.:</label>
                                    <input type="text" class="form-control" name="TotalArea1" id="TotalArea1" placeholder="Total Area">  
                                </div>
                                    </div>
                        </div>
                    </div>
                    <div class="container">

                        <button type="button" class="btn btn-primary" name="btnSubmit" id="btnSubmit">Submit</button>

                    </div>
                    
                    <div id="gird" name="gird" style="margin-top:35px;"> </div> 
                </form>

            </div>
        </div>
    </div>  

</div>
</body>

<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>
<script>
    function calc()
    {
        var TA = parseInt(document.getElementById('TheoryRoomArea').value);
        var LA = parseInt(document.getElementById('LabArea').value);
         var Total = TA + LA;
         
        if (Total)
        {
            document.getElementById("TotalArea").value = Total;
        }
        
    }
</script>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        
    function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfPremisesAreaRpt.php",
                data: "action=SHOW",
                success: function (data) {
                    $('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print']
                    });



                }
            });
        }

        showData();

        function FillPremisesArea() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfPremisesArea.php",
                data: "action=GETAREA",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Premises Area Not Found " + "</span></p>");
                          BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>&nbsp; Details Not Found.</span>");
                          window.setTimeout(function () {
                            window.location.href = "frmpremisesdetails.php";
                        }, 3000);
                    }
                    else {
                        data = $.parseJSON(data);
                        txtarea.value = data[0].area;     
                    }
                }
            });
        }
        FillPremisesArea();


 function findselected() {

            var UrbanDiv = document.getElementById("areaUrban");
            var category = document.getElementById("Urban");

            if (UrbanDiv) {
                Urban.style.display = areaUrban.checked ? "block" : "none";
                Rural.style.display = "none";
            } else
            {
                Urban.style.display = "none";
            }
        }
        function findselected1() {


            var RuralDiv = document.getElementById("areaRural");
            var category1 = document.getElementById("Rural");

            if (RuralDiv) {
                Rural.style.display = areaRural.checked ? "block" : "none";
                Urban.style.display = "none";
            } else
            {
                Rural.style.display = "none";
            }
        }
        $("#areaUrban").change(function () {
            findselected();
        });
        $("#areaRural").change(function () {
            findselected1();
        });
        
        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var forminput = $("#frmpremisesarea").serialize();        
               $.ajax({
                type: "post",
                url: "common/cfPremisesArea.php",
                data: "action=SubmitArea&" + forminput,
                success: function (data)
                {//alert(data);

                    if (data === "Successfully Updated")
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmpremisesarea.php";
                        }, 1000);

                    } else
                    {
                          $('#response').empty();        
                          BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span>");
//                          window.setTimeout(function () {
//                            window.location.href = "frmpremisesarea.php";
//                        }, 3000);
                    }
                }
            });
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<!--
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmchangersp_validation.js"></script>-->
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>