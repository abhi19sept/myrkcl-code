<?php
    require_once 'DAL/classconnection.php';

    $_ObjConnection = new _Connection();
	$_ObjConnection->Connect();

	ini_set("memory_limit", "5000M");
    ini_set("max_execution_time", 0);
    set_time_limit(0);
	
	date_default_timezone_set("Asia/Kolkata");

	$response = $_POST;
	$allowedIps = ['180.179.174.1', '180.179.174.2', '180.179.174.13', '180.179.174.14', '180.179.174.15', '::1'];
	//if (!empty($response['txnid']) && in_array($responsedIP, $allowedIps)) {
	if (!empty($response['txnid'])) {
		insertPayUResponseLog($response);
		$response['verify_by'] = 'VerifiedByServerApi';
	} else {
		//optimizeTransactionsRecords();
		//performReconcilation();
		//addMissingExamData();
		//fixInvalidConfirmedLearners();
		//fixIncorrectedRefundedLearners();
		//fixDuplicateConfirmedLearners();
		//getWorngConfirmed();
		
		die('over!!');
	}

/**
*	Function to get database connection object.
*/
	function dbConnection() {
		global $_ObjConnection;

		return $_ObjConnection;
	}

/**
*	Function to Insert PayU response into tbl_payutransactionresponse table as Log.
*/
	function insertPayUResponseLog($response) {
		$_ObjConnection = dbConnection();

		$responsedIP = $_SERVER['REMOTE_ADDR'];

		$responseFields = ['txnid','mihpayid','mode','status','amount','udf1','udf2','udf3','udf4','udf5','udf6','udf7','udf8','udf9','udf10','field0','field1','field2','field3','field4','field5','field6','field7','field8','field9','hash','bank_ref_no','surl','curl','furl','card_token','card_no','card_hash','productinfo','offer','discount','offer_availed','unmappedstatus','firstname','lastname','address1','address2','city','state','country','zipcode','email','phone'];
		$insert = "";
		foreach ($responseFields as $field) {
			$response[$field] = !empty($response[$field]) ? $response[$field] : '';
			$insert .= $field . " = '" . $response[$field] . "', ";
		}

		$insertResponseSql = "INSERT IGNORE INTO tbl_payutransactionresponse SET
			" . $insert . " 
			tx_key = '" . ((!empty($response['key'])) ? $response['key'] : '') . "',
			responseIP = '" . $responsedIP . "',
			response = '" . serialize($response) . "',
			serverInfo = ''";

		$_Response = $_ObjConnection->ExecuteQuery($insertResponseSql, Message::InsertStatement);
	}

/**
*	Function to process transaction response.
*/
	function processResponse($response) {
		$status = $response["status"];
		$firstname = $response["firstname"];
		$amount = $response["amount"];
		$txnid = $response["txnid"];
		$productinfo = $response["productinfo"];
		$udf1 = $response["udf1"];
		$udf2 = $response["udf2"];
		$udf3 = $response["udf3"];
		$udf4 = $response["udf4"];
		$verifyBy = $response["verify_by"];

		$email = (!empty($response['email'])) ? $response['email'] : '';
		$key = (!empty($response['key'])) ? $response['key'] : '';
		$posted_hash = (!empty($response['hash'])) ? $response['hash'] : '';

		if ($status == 'success' || $status == 'PaymentReceive' || $status == 'Money Settled') {
			//Do further process on success;
			switch ($productinfo) {
				case 'ReexamPayment':
				case 'Re Exam Event Name':
					setSuccessForReExamTransaction($txnid, 'ReexamPayment', $udf1, $udf2, $firstname, $amount, $verifyBy);
					break;
				case 'LearnerFeePayment':
				case 'Learner Fee Payment':
					setSuccessForLearnerFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $udf3, $udf4, $firstname, $amount, $verifyBy);
					break;
				case 'Correction Certificate':
				case 'Correction Fee Payment':
				case 'Duplicate Certificate':
					setSuccessForCorrectionFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $verifyBy);
					break;
			}
		} elseif ($status != 'pending') {
			//Do further process on failer;
			switch ($productinfo) {
				case 'ReexamPayment':
				case 'Re Exam Event Name':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateExamDataPaymentStatus(0, $txnid, $udf1);
					break;
				case 'LearnerFeePayment':
				case 'Learner Fee Payment':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateAdmissionPaymentStatus(0, $txnid, $udf1, $udf3, $udf4);
					break;
				case 'Correction Certificate':
				case 'Correction Fee Payment':
				case 'Duplicate Certificate':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateCorrectionPaymentStatus(0, $txnid, $udf1);
					break;
			}
		}
	}

/**
*	Function to get pending transaction as response.
*/
	function getResponseByPaymentTransaction($min = 40) {
		$_ObjConnection = dbConnection();
		$timestamp = mktime(date("H"), date("i") - $min, date("s"), date("m"), date("d"), date("Y"));
		$time = date("Y-m-d H:i:s", $timestamp);
		$selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status IN ('PaymentInProcess', 'PaymentFailure') AND Pay_Tran_Reconcile_status = 'Not Reconciled' AND (Pay_Tran_ProdInfo LIKE ('%exam%') OR Pay_Tran_ProdInfo LIKE ('%learner%') OR Pay_Tran_ProdInfo LIKE ('%Correction%') OR Pay_Tran_ProdInfo LIKE ('%Duplicate%')) AND timestamp <= '" . $time . "' ORDER BY Pay_Tran_Code DESC LIMIT 1";
		//$selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = 'ff6e2c23e11e403a0308'";
		$result = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);
		$response = [];
		if (mysqli_num_rows($result[2])) {
			$data = mysqli_fetch_array($result[2]);
			$response = setTransactionResponse($data);
		}

		return (!empty($response)) ? $response : false;
	}

	function setTransactionResponse($data) {
			$response = [
				'firstname' => $data['Pay_Tran_Fname'],
				'amount' => $data['Pay_Tran_Amount'],
				'txnid' => $data['Pay_Tran_PG_Trnid'],
				'productinfo' => $data['Pay_Tran_ProdInfo'],
				'udf1' => $data['Pay_Tran_ITGK'],
				'udf2' => $data['Pay_Tran_RKCL_Trnid'],
				'udf3' => $data['Pay_Tran_Course'],
				'udf4' => $data['Pay_Tran_Batch'],
				'verify_by' => 'VerifiedByCron'
			];

		return $response;
	}

/**
*	Function to get transaction status from PayU as response.
*/
	function getTransactionStatusByPayU($response) {
		$txnid = $response['txnid'];
		if (!empty($txnid)) {
			switch ($response['productinfo']) {
				case 'LearnerFeePayment':
				case 'Learner Fee Payment':
					$key = "Es3Ozb";
					$salt = "o9aZmTfx";
					break;
				default:
					$key = "X2ZPKM";
					$salt = "8IaBELXB";
					break;
			}
			$command = "verify_payment";
			$hash_str = $key  . '|' . $command . '|' . $txnid . '|' . $salt ;
			$hash = strtolower(hash('sha512', $hash_str));
			$request = [
				'key' => $key,
				'hash' => $hash,
				'var1' => $txnid,
				'command' => $command
			];
			
			$qs = http_build_query($request);
			//$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
			$wsUrl = "https://info.payu.in/merchant/postservice?form=2";
			$payuResponses = curlCall($wsUrl, $qs, TRUE);
			$response['key'] = $key;
			$response['hash'] = $hash;
		}
		if(!empty($payuResponses['status']) && $payuResponses['status'] == 1) {
			foreach($payuResponses['transaction_details'] as $val) {
				$status = $val['status'];
				$response['amount'] = intval($val['amt']);
			}
		} else {
			$status = 'PaymentnotDone';
		}

		$response['status'] = $status;

		return $response;
	}

/**
*	Function to make cURL request to get response.
*/
	function curlCall($wsUrl, $qs, $true) {
		$c = curl_init();
		
		curl_setopt($c, CURLOPT_URL, $wsUrl);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
	
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		
		$o = curl_exec($c);
		if (curl_errno($c)) {
		    $c_error = curl_error($c);
			if (empty($c_error)) {
			  $c_error = 'Some server error';
			}

			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);

		return $arr;		
	}

/**
*	Functions for Re Exam Transactions Start
*/
	function setSuccessForReExamTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy) {
		$_ObjConnection = dbConnection();
		$refund = checkIfRefundRequiredForReExamTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount);
		} else {
			processToRefundReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount);
		}
	}

	function processToConfirmReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount) {
		$_ObjConnection = dbConnection();
		updateExamDataPaymentStatus(1, $transactionId, $centerCode);
		$_SelectReexamtxn = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($_SelectReexamtxn, Message::SelectStatement);
		$numRows = mysqli_num_rows($response[2]);
		if(!$numRows) {
			insertReExamTransaction('Success', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId);
		} else {
			updateReExamTransactionStatus('Success', $amount, $transactionId, $centerCode);
		}
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
	}

	function isPaymentTransactionFound($productInfo, $transactionId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);

		return mysqli_num_rows($response[2]);
	}

	function updatePaymentTransactionStatus($verifyApiStatus, $transactionStatus, $transactionReconcileStatus, $transactionId, $centerCode = false) {
		$_ObjConnection = dbConnection();
		$filter = ($centerCode) ?  " AND Pay_Tran_ITGK = '" . $centerCode . "'" : '';
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status = '" . $transactionStatus . "' AND Pay_Tran_PG_Trnid = '" . $transactionId . "'" . $filter;
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_payment_transaction SET 
				pay_verifyapi_status = '" . $verifyApiStatus . "',
				Pay_Tran_Status = '" . $transactionStatus . "',
				Pay_Tran_Reconcile_status = '" . $transactionReconcileStatus . "' 
			WHERE 
			Pay_Tran_PG_Trnid = '" . $transactionId . "'" . $filter;
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}
	
	function updateExamDataPaymentStatus($paymentstatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM examdata WHERE paymentstatus = " . $paymentstatus . " AND itgkcode = '" . $centerCode . "'  AND reexam_TranRefNo = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "UPDATE examdata SET paymentstatus = '" . $paymentstatus ."' WHERE itgkcode = '" . $centerCode . "'  AND reexam_TranRefNo = '" . $transactionId . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}
	
	function insertReExamTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Status = '" . $transactionStatus . "' AND Reexam_Transaction_Amount = '" . $amount . "' AND Reexam_Transaction_Txtid = '" . $transactionId . "' AND  Reexam_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_reexam_transaction (
				Reexam_Transaction_Code,
				Reexam_Transaction_Status,
				Reexam_Transaction_Fname,
				Reexam_Transaction_Amount,
				Reexam_Transaction_Txtid,
				Reexam_Transaction_ProdInfo,
				Reexam_Transaction_CenterCode,
				Reexam_Transaction_RKCL_Txid,
				Reexam_DateTime
			) 
			SELECT CASE WHEN MAX(Reexam_Transaction_Code) IS NULL THEN 1 ELSE MAX(Reexam_Transaction_Code) + 1 END AS Reexam_Transaction_Code,
			'" . $transactionStatus . "' AS Reexam_Transaction_Status,
			'" . $firstName . "' AS Reexam_Transaction_Fname,
			'" . $amount . "' AS Reexam_Transaction_Amount,
			'" . $transactionId . "' AS Reexam_Transaction_Txtid,
			'" . $productInfo . "' AS Reexam_Transaction_ProdInfo,
			'" . $centerCode . "' AS Reexam_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Reexam_Transaction_RKCL_Txid,
			'" . date("Y-m-d H:i:s") . "' AS Reexam_DateTime 
			FROM tbl_reexam_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateReExamTransactionStatus($transactionStatus, $reExamTransactionAmount, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Status = '" . $transactionStatus . "' AND Reexam_Transaction_Amount = '" . $reExamTransactionAmount . "' AND Reexam_Transaction_Txtid = '" . $transactionId . "' AND  Reexam_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_reexam_transaction SET 
				Reexam_Transaction_Status = '" . $transactionStatus . "', Reexam_Transaction_Amount = '" . $reExamTransactionAmount . "' 
			WHERE 
				Reexam_Transaction_Txtid = '" . $transactionId . "' AND  Reexam_Transaction_CenterCode = '" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}

	function checkIfRefundRequiredForReExamTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(reexam_TranRefNo) AS ReexamTxnid FROM examdata WHERE reexam_TranRefNo = '" . $transactionId . "' AND itgkcode = '" . $centerCode . "' GROUP BY reexam_TranRefNo";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$reExamCounts = mysqli_fetch_array($response[2]);
				$reExamAmount = ($reExamCounts['ReexamTxnid'] * 300);
				if ($reExamAmount != $amount) {
					$refund = true;
				}
			} else {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}
	
	function processToRefundReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount) {
		$_ObjConnection = dbConnection();
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		$query = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertReExamTransaction('Refund', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId);
		} else {
			updateReExamTransactionStatus('Refund', $amount, $transactionId, $centerCode);
		}
		updateExamDataPaymentStatus(0, $transactionId, $centerCode);
	}

	function setForPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_payment_refund (
				Payment_Refund_Id,
				Payment_Refund_Txnid,
				Payment_Refund_Amount,
				Payment_Refund_Status,
				Payment_Refund_ProdInfo,
				Payment_Refund_ITGK,
				Payment_Refund_RKCL_Txid
			)
			SELECT CASE WHEN MAX(Payment_Refund_Id) IS NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
			'" . $transactionId . "' AS Payment_Refund_Txnid,
			'" . $amount . "' AS Payment_Refund_Amount,
			'" . $status . "' AS Payment_Refund_Status,
			'" . $productInfo . "' AS Payment_Refund_ProdInfo,
			'" . $centerCode . "' AS Payment_Refund_ITGK,
			'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid
			FROM tbl_payment_refund";

			$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "'";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (! mysqli_num_rows($response[2])) {
				$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
			}
		}
	}
/**
*	Functions of Re Exam Transactions End
*/

/**
*	Functions for Learner Fee Transactions Start
*/
	function setSuccessForLearnerFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $firstName, $amount, $verifyBy) {
		$refund = checkIfRefundRequiredForLearnerFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch);
		} else {
			processToRefundAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch);
		}
	}

	function processToConfirmAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch) {
		$_ObjConnection = dbConnection();
		updateAdmissionPaymentStatus(1, $transactionId, $centerCode, $course, $batch);
		$query = "SELECT * From tbl_admission_transaction WHERE Admission_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		$numRows = mysqli_num_rows($response[2]);
		if(!$numRows) {
			insertAdmissionTransaction('Success', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		} else {
			updateAdmissionTransactionStatus('Success', $amount, $transactionId, $centerCode, $course, $batch);
		}
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
	}

	function updateAdmissionPaymentStatus($paymentstatus, $transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_admission WHERE Admission_Payment_Status = " . $paymentstatus . " AND Admission_ITGK_Code = '" . $centerCode . "'  AND Admission_TranRefNo = '" . $transactionId . "' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "UPDATE tbl_admission SET Admission_Payment_Status = '" . $paymentstatus . "' WHERE Admission_ITGK_Code = '" . $centerCode . "' AND Admission_TranRefNo = '" . $transactionId . "' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}

	function insertAdmissionTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_admission_transaction WHERE Admission_Transaction_Status = '" . $transactionStatus . "' AND Admission_Transaction_Amount = '" . $amount . "' AND Admission_Transaction_Txtid = '" . $transactionId . "' AND  Admission_Transaction_CenterCode = '" . $centerCode . "' AND Admission_Transaction_Course = '" . $course . "' AND  Admission_Transaction_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_admission_transaction (
				Admission_Transaction_Code,
				Admission_Transaction_Status,
				Admission_Transaction_Fname,
				Admission_Transaction_Amount,
				Admission_Transaction_Txtid,
				Admission_Transaction_ProdInfo,
				Admission_Transaction_CenterCode,
				Admission_Transaction_RKCL_Txid,
				Admission_Transaction_Course,
				Admission_Transaction_Batch,
				Admission_Transaction_DateTime
			) 
			SELECT CASE WHEN MAX(Admission_Transaction_Code) Is NULL THEN 1 ELSE MAX(Admission_Transaction_Code) + 1 END AS Admission_Transaction_Code,
			'" . $transactionStatus . "' AS Admission_Transaction_Status,
			'" . $firstName . "' AS Admission_Transaction_Fname,
			'" . $amount . "' AS Admission_Transaction_Amount,
			'" . $transactionId . "' AS Admission_Transaction_Txtid,
			'" . $productInfo . "' AS Admission_Transaction_ProdInfo,
			'" . $centerCode . "' AS Admission_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Admission_Transaction_RKCL_Txid,
			'" . $course . "' AS Admission_Transaction_Course,
			'" . $batch . "' AS Admission_Transaction_Batch,
			'" . date("Y-m-d H:i:s") . "' AS Admission_Transaction_DateTime 
			FROM tbl_admission_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateAdmissionTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_admission_transaction WHERE Admission_Transaction_Status = '" . $transactionStatus . "' AND Admission_Transaction_Amount = '" . $amount . "' AND Admission_Transaction_Txtid = '" . $transactionId . "' AND  Admission_Transaction_CenterCode = '" . $centerCode . "' AND Admission_Transaction_Course = '" . $course . "' AND Admission_Transaction_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_admission_transaction SET 
				Admission_Transaction_Status = '" . $transactionStatus . "',
				Admission_Transaction_Amount = '" . $amount . "'
			WHERE 
				Admission_Transaction_Txtid = '" . $transactionId . "' AND 
				Admission_Transaction_CenterCode = '" . $centerCode . "' AND 
				Admission_Transaction_Course = '" . $course . "' AND 
				Admission_Transaction_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}

	function checkIfRefundRequiredForLearnerFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(Admission_TranRefNo) AS AdmissionTxnid, Admission_Course, Admission_Batch FROM tbl_admission WHERE Admission_TranRefNo = '" . $transactionId . "' AND Admission_ITGK_Code = '" . $centerCode . "' GROUP BY Admission_TranRefNo, Admission_Course, Admission_Batch";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['AdmissionTxnid'];
			    $course = $row['Admission_Course'];
			    $baseAmount = (($course == '1') ? 2850 : (($course == '4') ? 2700 : (($course == '5') ? 3000 : 0)));
				$admissionAmount = $payTxnid * $baseAmount;
				if ($admissionAmount != $amount) {
					$refund = true;
				}
			} else {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch) {
		$_ObjConnection = dbConnection();
		setForAdmissionPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		$query = "SELECT * FROM tbl_admission_transaction WHERE Admission_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertAdmissionTransaction('Refund', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		} else {
			updateAdmissionTransactionStatus('Refund', $amount, $transactionId, $centerCode, $course, $batch);
		}
		updateAdmissionPaymentStatus(0, $transactionId, $centerCode, $course, $batch);
	}

	function setForAdmissionPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT INTO tbl_payment_refund (
			Payment_Refund_Id,
			Payment_Refund_Txnid,
			Payment_Refund_Amount,
			Payment_Refund_Status,
			Payment_Refund_ProdInfo,
			Payment_Refund_ITGK,
			Payment_Refund_RKCL_Txid,
			Payment_Refund_Course,
			Payment_Refund_Batch
		) 
		SELECT CASE WHEN MAX(Payment_Refund_Id) Is NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
		'" . $transactionId . "' AS Payment_Refund_Txnid,
		'" . $amount . "' AS Payment_Refund_Amount,
		'" . $status . "' AS Payment_Refund_Status,
		'" . $productInfo . "' AS Payment_Refund_ProdInfo,
		'" . $centerCode . "' AS Payment_Refund_ITGK,
		'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid,
		'" . $course . "' AS Payment_Refund_Course,
		'" . $batch . "' AS Payment_Refund_Batch 
		FROM tbl_payment_refund";

		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "' AND Payment_Refund_Course = '" . $course . "' AND Payment_Refund_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

/**
*	Functions of Learner Fee Transactions End
*/

/**
*	Functions for Correction Fee Transactions Start
*/
	function setSuccessForCorrectionFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy) {
		$refund = checkIfRefundRequiredForCorrectionFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount);
		} else {
			processToRefundCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount);
		}
	}

	function processToConfirmCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount) {
		$_ObjConnection = dbConnection();
		updateCorrectionPaymentStatus(1, $transactionId, $centerCode);
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if(! mysqli_num_rows($response[2])) {
			insertCorrectionTransaction('Success', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId);
		} else {
			updateCorrectionTransactionStatus('Success', $amount, $transactionId, $centerCode);
		}
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
	}

	function updateCorrectionPaymentStatus($paymentstatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_correction_copy WHERE Correction_Payment_Status = " . $paymentstatus . " AND Correction_ITGK_Code = '" . $centerCode . "'  AND Correction_TranRefNo = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "UPDATE tbl_correction_copy SET Correction_Payment_Status = '" . $paymentstatus . "' WHERE Correction_ITGK_Code = '" . $centerCode . "'  AND Correction_TranRefNo = '" . $transactionId . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}

	function insertCorrectionTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Status = '" . $transactionStatus . "' AND Correction_Transaction_Amount = '" . $amount . "' AND Correction_Transaction_Txtid = '" . $transactionId . "' AND  Correction_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_correction_transaction (
				Correction_Transaction_Code,
				Correction_Transaction_Status,
				Correction_Transaction_Fname,
				Correction_Transaction_Amount,
				Correction_Transaction_Txtid,
				Correction_Transaction_ProdInfo,
				Correction_Transaction_CenterCode,
				Correction_Transaction_RKCL_Txid,
				Correction_Transaction_DateTime,
				Correction_Transaction_Month,
				Correction_Transaction_Year
			) 
			SELECT CASE WHEN MAX(Correction_Transaction_Code) Is NULL THEN 1 ELSE MAX(Correction_Transaction_Code) + 1 END AS Correction_Transaction_Code,
			'" . $transactionStatus . "' AS Correction_Transaction_Status,
			'" . $firstName . "' AS Correction_Transaction_Fname,
			'" . $amount . "' AS Correction_Transaction_Amount,
			'" . $transactionId . "' AS Correction_Transaction_Txtid,
			'" . $productInfo . "' AS Correction_Transaction_ProdInfo,
			'" . $centerCode . "' AS Correction_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Correction_Transaction_RKCL_Txid,
			'" . date("Y-m-d H:i:s") . "' AS Correction_Transaction_DateTime,
			" . date("m") . " AS Correction_Transaction_Month,
			" . date("Y") . " AS Correction_Transaction_Year 
			FROM tbl_correction_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateCorrectionTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Status = '" . $transactionStatus . "' AND Correction_Transaction_Amount = '" . $amount . "' AND Correction_Transaction_Txtid = '" . $transactionId . "' AND  Correction_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_correction_transaction SET 
				Correction_Transaction_Status = '" . $transactionStatus . "',
				Correction_Transaction_Amount='" . $amount . "'
			WHERE 
				Correction_Transaction_Txtid = '" . $transactionId . "' AND 
				Correction_Transaction_CenterCode='" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}

	function checkIfRefundRequiredForCorrectionFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(Correction_TranRefNo) AS CorrectionTxnid FROM tbl_correction_copy WHERE Correction_TranRefNo = '" . $transactionId . "' AND Correction_ITGK_Code = '" . $centerCode . "' GROUP BY Correction_TranRefNo";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['CorrectionTxnid'];
				$correctionAmount = $payTxnid * 200;
				if ($correctionAmount != $amount) {
					$refund = true;
				}
			} else {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount) {
		$_ObjConnection = dbConnection();
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertCorrectionTransaction('Refund', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId);
		} else {
			updateCorrectionTransactionStatus('Refund', $amount, $transactionId, $centerCode);
		}
		updateCorrectionPaymentStatus(0, $transactionId, $centerCode);
	}

	function performReconcilation() {
		$_ObjConnection = dbConnection();
		$timestamp = mktime(date("H"), date("i") - 40, date("s"), date("m"), date("d"), date("Y"));
		$time = date("Y-m-d H:i:s", $timestamp);
		$selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status IN ('PaymentInProcess', 'PaymentFailure') AND Pay_Tran_Reconcile_status = 'Not Reconciled' AND (Pay_Tran_ProdInfo LIKE ('%exam%') OR Pay_Tran_ProdInfo LIKE ('%learner%') OR Pay_Tran_ProdInfo LIKE ('%Correction%') OR Pay_Tran_ProdInfo LIKE ('%Duplicate%')) AND timestamp > '2017-03-31 00:00:00' ORDER BY Pay_Tran_Code DESC";
		//echo $selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = 'f85eda23d450fd026ec1'";
		$result = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);
		$response = [];
		if (mysqli_num_rows($result[2])) {
			while ($data = mysqli_fetch_array($result[2])) {
				$response = setTransactionResponse($data);
				$response = getTransactionStatusByPayU($response);
				processResponse($response);
			}
		}
	}

	function optimizeTransactionsRecords($timestamp = '2017-03-31 00:00:00') {
        removeDuplicatesFromTable('tbl_payment_transaction', 'Pay_Tran_PG_Trnid', 'Pay_Tran_Code', $timestamp, 'timestamp');
        //removeDuplicatesFromTable('tbl_admission_transaction', 'Admission_Transaction_Txtid', 'Admission_Transaction_Code');
        removeDuplicatesFromTable('tbl_reexam_transaction', 'Reexam_Transaction_Txtid', 'Reexam_Transaction_Code', $timestamp, 'Reexam_Transaction_timestamp');
        //removeDuplicatesFromTable('tbl_correction_transaction', 'Correction_Transaction_Txtid', 'Correction_Transaction_Code');
        removeDuplicatesFromExamDataTable();
        checkRefundedTransactions($timestamp);
        //checkForMissingTransactions('Learner', 'tbl_admission_transaction', 'Admission_Transaction_Txtid');
        checkForMissingTransactions('Exam', 'tbl_reexam_transaction', 'Reexam_Transaction_Txtid', $timestamp);
        //checkForMissingTransactions('Correction', 'tbl_correction_transaction', 'Correction_Transaction_Txtid');
        //checkForMissingTransactions('Duplicate', 'tbl_correction_transaction', 'Correction_Transaction_Txtid');
    }

    function removeDuplicatesFromTable($tableName, $tranxidField, $idField, $timestamp, $timeFieldName) {
        $_ObjConnection = dbConnection();
        $query = "SELECT $tranxidField, $idField, COUNT(*) AS n FROM $tableName WHERE $timeFieldName > '" . $timestamp . "' GROUP BY $tranxidField HAVING n > 1";
        $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        if ($response[0] == 'No Record Found') {
        } else {
	        while ($row = mysqli_fetch_array($response[2])) {
	            $query = "DELETE FROM $tableName WHERE $tranxidField = '" . $row[$tranxidField] . "' AND $idField <> '" . $row[$idField] . "'";
	            $_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
	        }
    	}
    }

    function checkRefundedTransactions($timestamp) {
        $_ObjConnection = dbConnection();
        $query = "SELECT Pay_Tran_PG_Trnid FROM tbl_payment_transaction pt INNER JOIN tbl_payment_refund pr ON pr.Payment_Refund_Txnid = pt.Pay_Tran_RKCL_Trnid AND pt.Pay_Tran_Status NOT LIKE ('%refund%') AND pt.timestamp > '" . $timestamp . "'";
        $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        if ($response[0] == 'No Record Found') {
        } else {
	        while ($row = mysqli_fetch_array($response[2])) {
	            setForReconciledAgain($row['Pay_Tran_PG_Trnid']);
	            $query = "DELETE FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $row['Pay_Tran_PG_Trnid'] . "'";
	            $_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
	        }
    	}
    }

    function checkForMissingTransactions($proInfo, $table, $refField, $timestamp) {
    	$_ObjConnection = dbConnection();
        $query = "SELECT Pay_Tran_PG_Trnid FROM tbl_payment_transaction WHERE Pay_Tran_Status LIKE '%Receive%' AND Pay_Tran_ProdInfo LIKE ('%" . $proInfo . "%') AND timestamp > '" . $timestamp . "' AND Pay_Tran_PG_Trnid NOT IN (SELECT " . $refField . " FROM " . $table . ")";
        $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        if ($response[0] == 'No Record Found') {
        } else {
	        while ($row = mysqli_fetch_array($response[2])) {
	            setForReconciledAgain($row['Pay_Tran_PG_Trnid']);
	        }
    	}
    }

    function setForReconciledAgain($trnid) {
        $_ObjConnection = dbConnection();
        $query = "UPDATE tbl_payment_transaction SET 
                    Pay_Tran_Status = 'PaymentInProcess',
                    Pay_Tran_Reconcile_status = 'Not Reconciled'
                WHERE Pay_Tran_PG_Trnid = '" . $trnid . "'";
        $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
    }

    function removeDuplicatesFromExamDataTable($examId = 1217) {
        $_ObjConnection = dbConnection();
        $query = "select *, COUNT(*) as n from examdata where examid='" . $examId . "' AND paymentstatus = 0 group by learnercode, reexam_TranRefNo having n > 1";
        $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        if ($response[0] == 'No Record Found') {
        } else {
	        while ($row = mysqli_fetch_array($response[2])) {
	            $query = "DELETE FROM examdata WHERE learnercode = '" . $row['learnercode'] . "' AND examid='" . $examId . "' AND paymentstatus = 0 AND examdata_code <> '" . $row['examdata_code'] . "' AND (reexam_TranRefNo = '" . $row['reexam_TranRefNo'] . "' OR reexam_TranRefNo IS NULL)";
	            $_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
	        }
    	}
    }
	
	function addMissingExamData() {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid IN ( '012eab4e113d9c44b9db','01770376cb199f7e0002','04c1c52a63fc49439fa6','053b647da368d4b08989','06784322c935a4b7b7c8','069483de6ccad8f20061','06c0ab3f65273f8399b4','079fd8966957ab34e2c5','0953145dcac0d0ddecc9','0a015f4e3f58d9e5251e','0f1028778f1d9d0b33bc','12a9c3ac8b10e2ac6f0f','171faaa8943873892400','18e335b975acda962dbb','1bcf9383a362fc80ba93','1fb2eb7a7b6dfcbf21c0','2446fe594b0466858ad4','24b4b2626eec1d9579e8','260faae1e90b8e2ff9fa','28381e66330c5c15a274','29a90f66c818f55c936f','2cb337888aa679ed30f5','2f7d450ec610e8e3ac1f','300980e13bee2def7dd6','305fc01efeafeaad0653','31861f6103edaf3aac79','3246774ed71e3c273d8b','33ef8e22ca9eb9e17dcd','3464472b317b2b1ca1ef','34b3171149e07441ad12','36617162306506897d19','3806d814c25b7fdc65c7','3853282c1beb77c95c90','3ba3178f2ed0eedcf54f','3cde7c859975f90105fe','3eecc28f8208a04562b4','402400f33a8e7338795f','43435fb541b8c73d25fe','43fe0961964e0cd64acd','45307e4829348ee79cb8','470f9d7d976ed1775423','494b67d2ea44bdfb623d','49d565717947d0984f82','4a26784df6d33eeae19a','4f319dd1c28bb3e67716','53b1c92106797da63ad9','53d36ea0b83a915c6999','53e71d56e70484b8f45e','59f999f0017c64f13698','5a939480c47013ec7d54','5afdd016260d2f243aea','5c04463ba13b1696b06b','5c0f84984613c6e0eb33','5c2e08a1a0755aba2d74','5df2e96595b81778eeaf','5fdbfcc9f895b526c9e8','64371dd2f2afb66953a2','64eec668bab795f873b3','6866a3b7dbafbd301720','6933a971805a3f76392e','6aa332a2b3e0b5e160c1','6abea21009d60a44d507','6c2b58ff5b7b590135fb','6ccd98cb393b761a9971','6d4bada211feac911905','6d4cb58c8dc4021eea76','6edcfa3ad179057c5441','708f95a275b329bd6b16','738a262f3c7f8b6d5a11','74562d1a4fa3aaa7a8cc','75bf5769b715a477dab7','77559ee49283f99e112b','77594bc593b91b903b79','7ca6298350ee9493f4b3','7e053a29b6706c0df5f0','7ff8d92d3c649e95e6e3','8386c58ad811e58df82b','84930714eba7c9aa41eb','8537832873653436c48c','881bcec27e7cb8e9be82','89c498aa8d7165bc153f','8c1c01b17c171b15ed6d','8d00214ec6986e720e77','8da9c96db3e0ca3e6fd2','904996d99fd4757eec71','9553e320663c7f2a5550','966ad6a2ccea055e944f','96cc72ed282d51b0d144','97273417a6b5182ce68f','9794884d461f11f2d48d','9d00946c66b57219141a','a01a2c1fbaa90603b776','a0b00d3b8e6577815124','a4c0945898dc661fcd89','a4c7d52813fa8f39366c','a5b9a519ac4de271c505','a605a3c5900583d1ec7f','a61ccef778106b39f242','a9aca661c53456b84f0b','aa6cab516122a1eadc94','ad995d01a722c813cb48','afd6c039fc37b8dc5989','b16470a5abf91a5da819','b24c18ffdf8d6faba341','b27ca4f63648edbdeab9','b54755f78016bfec7e00','b628c5e783d14971b687','ba2e9e6a767838490bb0','bd276da030967d729241','bea31bb3763ccdee2ac4','bf259e137c1b19b0b254','c382ecccc266edc7a158','c3bbef76db7ee6c1535e','caa09eb1045a0023a24b','cc543a6b5368a9f48faa','cca739378f5a57cd972a','d096a0026074ae59967d','d6de9e6a3108b527d3ad','d86dde4d8aa0a18b4989','dae732ad0e3921b81d43','dbdf80bd34370e98ea71','dd7ec68d09b12193363a','dee6e1e43d50ccf36975','dfffb0cd688a47ebbc50','e1001a2087ffe25d6130','e17aa9b0798b99948b1d','e37dd5eb19d4f0890bb0','e5297fee2838c35276f9','e5b34e4fddca65b33eac','ece118c43e1b4b107e8d','ee7e98551a6983c0d4f5','efe1445d941d08e5895d','f53f472f8f7c5b861fb1','f5e2b3162a828e83a2ed','f7072c6ff47d0a96ab1f','f8e359d99228748ad4c9','fd96e6083640bf0db4f7','fdc8beca494a6e1cad37','fec14de1550022da8f6f','ff5c89329d908a7d64ef','fffa9b9c20ff1dd306ca','88ebc38beb4b5a080ef6','a051938d076a68cb5da8','e0560553f2f747503a3c','c12826ec6a8b56efb99b','b4f69aba64a6192474db','dab620db8725eb2b36c6','b43c141e7342bc450e08','df14ccd739c0c7003fde','b5abe77856057b35c21a','2744d9565fe72494bb07','716a8cff9499888c152d')";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        if ($response[0] == 'No Record Found') {
        } else {
			$result = [];
	        while ($row = mysqli_fetch_array($response[2])) {
				$codes = explode(',', $row['Pay_Tran_AdmissionArray']);
				foreach ($codes as $code) {
					$query = "SELECT * FROM examdata WHERE examdata_code = '" . $code . "' AND examid=1217";
					$responseExamData = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
					if ($responseExamData[0] == 'No Record Found') {
						$result[] = $code;
					}
				}
			}
			print implode(',',$result);
		}
	}
	
	function fixInvalidConfirmedLearners($eventId = 1217) {
        $_ObjConnection = dbConnection();
        $query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status = 'PaymentReceive' AND timestamp > '2017-03-31 00:00:00'";
        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        $result = [];
        if (mysqli_num_rows($_Response[2])) {
            while($data = mysqli_fetch_array($_Response[2])) {

                $arr = explode(',', $data['Pay_Tran_AdmissionArray']);
                foreach ($arr as $code) {
                    $query = "SELECT * FROM examdata WHERE examid = '" . $eventId . "' AND examdata_code = '" . $code . "' AND paymentstatus = 1 AND itgkcode = '" . $data['Pay_Tran_ITGK'] . "'";
                    $_ExamResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
                    if (! mysqli_num_rows($_ExamResponse[2])) {
                        //Wrong code identified here!!
                        $query = "SELECT * FROM examdata WHERE examid = '" . $eventId . "' AND paymentstatus = 0 AND itgkcode = '" . $data['Pay_Tran_ITGK'] . "' AND batchname > 46 AND learnercode NOT IN (SELECT learnercode FROM examdata WHERE paymentstatus = 1 AND examid = '" . $eventId . "') ORDER BY examdata_code DESC LIMIT 1";
                        $_PendingResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);

                        if (mysqli_num_rows($_PendingResponse[2])) {

                            $newRow = mysqli_fetch_array($_PendingResponse[2]);
                            $newArray = str_replace($code, $newRow['examdata_code'], $data['Pay_Tran_AdmissionArray']);

                            $result[$data['Pay_Tran_PG_Trnid']][$code] = $newRow['examdata_code'];

                            $query = "UPDATE examdata SET paymentstatus = 0, reexam_TranRefNo = '' WHERE examid = '" . $eventId . "' AND examdata_code = '" . $code . "'";
                            $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);

                            $query = "UPDATE examdata SET paymentstatus = 1, reexam_TranRefNo = '" . $data['Pay_Tran_PG_Trnid'] . "', remark = 'Under395' WHERE examid = '" . $eventId . "' AND examdata_code = '" . $newRow['examdata_code'] . "' AND paymentstatus = 0";
                            $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);

                            $query = "UPDATE tbl_payment_transaction SET Pay_Tran_AdmissionArray = '" . $newArray . "' WHERE Pay_Tran_PG_Trnid = '" . $data['Pay_Tran_PG_Trnid'] . "'";
                            $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
                        }
                    }
                }
            }
        }
        print_r($result);
    }
	
    function fixIncorrectedRefundedLearners($eventId = 1217) {
    	$_ObjConnection = dbConnection();

    	$query = "SELECT * FROM tbl_payment_transaction WHERE timestamp > '2017-03-31 00:00:00' AND Pay_Tran_Status LIKE('%refund%') ORDER BY Pay_Tran_Code DESC";
    	$_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        $result = [];
        if (mysqli_num_rows($_Response[2])) {
            while($data = mysqli_fetch_array($_Response[2])) {
            	$response = [
            		'txnid'=>$data['Pay_Tran_PG_Trnid'],
            		'productinfo'=>$data['Pay_Tran_ProdInfo']
            	];
            	$response = getTransactionStatusByPayU($response);
            	$status = $response['status'];
            	if ($status == 'success' || $status == 'PaymentReceive' || $status == 'Money Settled') {
                    $query = "SELECT * FROM examdata WHERE examid = '" . $eventId . "' AND examdata_code IN ('" . $data['Pay_Tran_AdmissionArray'] . "') AND itgkcode = '" . $data['Pay_Tran_ITGK'] . "'";
                    $_ExamResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
                    if (! mysqli_num_rows($_ExamResponse[2])) {
                        //Wrong code identified here!!
                        $query = "SELECT * FROM examdata WHERE examid = '" . $eventId . "' AND itgkcode = '" . $data['Pay_Tran_ITGK'] . "' AND batchname > 46 AND learnercode NOT IN (SELECT learnercode FROM examdata WHERE paymentstatus = 1 AND examid = '" . $eventId . "') AND reexam_TranRefNo IS NULL AND paymentstatus = 0 ORDER BY examdata_code DESC";
                        $_PendingResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
                        $found = mysqli_num_rows($_PendingResponse[2]);
                        if ($found > 0 && $found >= $data['Pay_Tran_LCount']) {
                        	$codes = [];
                        	$limit = $data['Pay_Tran_LCount'];
                        	$query = "SELECT * FROM examdata WHERE examid = '" . $eventId . "' AND itgkcode = '" . $data['Pay_Tran_ITGK'] . "' AND batchname > 46 AND learnercode NOT IN (SELECT learnercode FROM examdata WHERE paymentstatus = 1 AND examid = '" . $eventId . "') AND reexam_TranRefNo IS NULL AND paymentstatus = 0 ORDER BY examdata_code DESC LIMIT " . $limit;
                        	$_PendingResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
                            while($newRow = mysqli_fetch_array($_PendingResponse[2])) {
	                            $query = "UPDATE examdata SET paymentstatus = 1, reexam_TranRefNo = '" . $data['Pay_Tran_PG_Trnid'] . "', remark = 'ReConfirm' WHERE examid = '" . $eventId . "' AND examdata_code = '" . $newRow['examdata_code'] . "' AND paymentstatus = 0";
	                            $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
	                            $codes[] = $newRow['examdata_code'];
                        	}
                        	if ($codes) {
	                        	$newArray = implode(',', $codes);
	                            $query = "UPDATE tbl_payment_transaction SET Pay_Tran_AdmissionArray = '" . $newArray . "', Pay_Tran_Status='PaymentReceive', Pay_Tran_Reconcile_status='Reconciled', pay_verifyapi_status='VerifiedAndConfirmed' WHERE Pay_Tran_PG_Trnid = '" . $data['Pay_Tran_PG_Trnid'] . "'";
	                            $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
	                            $result[$data['Pay_Tran_PG_Trnid']] = $newArray;

	                            $query = "UPDATE tbl_reexam_transaction SET Reexam_Transaction_Status = 'success' WHERE Reexam_Transaction_Txtid = '" . $data['Pay_Tran_PG_Trnid'] . "'";
	                            $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);
	                            
	                            $query = "DELETE FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $data['Pay_Tran_PG_Trnid'] . "'";
	                            $_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
                        	}
                        }
                    }
            	}
            }
        }

        print_r($result);
    }

	function getInvalidConfirmedLearners($eventId = 1217) {
		$_ObjConnection = dbConnection();
		$codes = [];
		$query = "SELECT Pay_Tran_PG_Trnid, Pay_Tran_AdmissionArray, Pay_Tran_ITGK, Pay_Tran_LCount FROM tbl_payment_transaction WHERE Pay_Tran_Status like '%receive%' AND timestamp > '2017-03-31 00:00:00'";
		$_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Response[2])) {
			while($data = mysqli_fetch_array($_Response[2])) {
				$query = "SELECT COUNT(*) AS n FROM examdata WHERE examid = '" . $eventId . "' AND itgkcode = '" . $data['Pay_Tran_ITGK'] . "' AND examdata_code IN (" . $data['Pay_Tran_AdmissionArray'] . ")";
				$_ExamResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
				if (mysqli_num_rows($_ExamResponse[2])) {
					$row = mysqli_fetch_array($_ExamResponse[2]);
					if ($row['n'] != $data['Pay_Tran_LCount']) {
						$codes[] = $data['Pay_Tran_PG_Trnid'];
					}
				} else {
					$codes[] = $data['Pay_Tran_PG_Trnid'];
				}
			}
		}

		return ($codes) ? implode(', ', $codes) : '';
	}

	function fixDuplicateConfirmedLearners($eventId = 1217) {
        $_ObjConnection = dbConnection();
        $query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status like '%receive%' AND timestamp > '2017-03-31 00:00:00' AND Pay_Tran_PG_Trnid IN ('ad914354a8caad97021b','e59884a378372e8a4988','470aee1d5874a8d8ffc6','03caa69baf2bd0122297','860435880612d9f97517','934d4f2dcaac18ba6331','163f6e70c1082aaf3a62','bd123b1f10b1c9f6c865','6a49bb4881f27d62530d','6a49bb4881f27d62530d','e59884a378372e8a4988','163f6e70c1082aaf3a62','23977056597fca2ffebe','e59884a378372e8a4988','163f6e70c1082aaf3a62','ad914354a8caad97021b','65c063339585b2fc049b','e59884a378372e8a4988','1fcf954479cd9218ee27','6b3b1e27af787282f3d6','86bcc6a2fedea73097e9','0f3397e9b7cd58a276cb','7ff8d92d3c649e95e6e3','00486891c7ea1cb5a0f6','f5a835f33a6aa31a4607','86bcc6a2fedea73097e9','00486891c7ea1cb5a0f6','98884dbe549c633508ff','e59884a378372e8a4988','ad5adbb4f14f6f967985','49383140ca29cafccb19','a485ca5014cbe928d7e8')";
        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        $result = [];
        if (mysqli_num_rows($_Response[2])) {
            while($data = mysqli_fetch_array($_Response[2])) {
                $query = "SELECT * FROM examdata WHERE examid = '" . $eventId . "' AND learnercode IN ('120160728115658789','129160831120634789','214160730104050789','268161003035528789','300161008074827789','319160621033655789','348160801013225789','360160730010602789','391160721061646789','395161026061100789','408160828020427789','427160731102528789','433160807125813789','434160828023413789','555160731033724789','558160728120338789','627160806053826789','644160828022453789','675160520082131789','692160525045931789','722161128010356789','737161025115047789','752160521033939789','770160927054140789','786160525035038789','793161119100808789','810161007073422789','838160518055628789','841160831114224789','846160424060748789','907160525061434789','998161018124018789') AND reexam_TranRefNo = '" . $data['Pay_Tran_PG_Trnid'] . "' AND paymentstatus = 1 AND itgkcode = '" . $data['Pay_Tran_ITGK'] . "' LIMIT 1";
                $_ExamResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
                if (mysqli_num_rows($_ExamResponse[2])) {
                    //Wrong code identified here!!
                	$old = mysqli_fetch_array($_ExamResponse[2]);
                	$code = $old['examdata_code'];
                    $query = "SELECT * FROM examdata WHERE examid = '" . $eventId . "' AND paymentstatus = 0 AND itgkcode = '" . $data['Pay_Tran_ITGK'] . "' AND batchname > 46 AND learnercode NOT IN (SELECT learnercode FROM examdata WHERE paymentstatus = 1 AND examid = '" . $eventId . "') ORDER BY examdata_code DESC LIMIT 1";
                    $_PendingResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);

                    if (mysqli_num_rows($_PendingResponse[2])) {

                    	$result[] = $data['Pay_Tran_PG_Trnid'];

                        $newRow = mysqli_fetch_array($_PendingResponse[2]);
                        $newArray = str_replace($code, $newRow['examdata_code'], $data['Pay_Tran_AdmissionArray']);

                        $query = "UPDATE examdata SET paymentstatus = 0, reexam_TranRefNo = '' WHERE examid = '" . $eventId . "' AND examdata_code = '" . $code . "' AND paymentstatus = 1";
                        $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);

                        $query = "UPDATE examdata SET paymentstatus = 1, reexam_TranRefNo = '" . $data['Pay_Tran_PG_Trnid'] . "', remark = 'Under395' WHERE examid = '" . $eventId . "' AND examdata_code = '" . $newRow['examdata_code'] . "' AND paymentstatus = 0";
                        $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);

                        $query = "UPDATE tbl_payment_transaction SET Pay_Tran_AdmissionArray = '" . $newArray . "' WHERE Pay_Tran_PG_Trnid = '" . $data['Pay_Tran_PG_Trnid'] . "'";
                        $_ObjConnection->ExecuteQuery($query, Message::UpdateStatement);

                    }
                }
            }
        }
        print count($result);
        print_r($result);

    }

    function getWorngConfirmed($eventId = 1217) {
    	$_ObjConnection = dbConnection();
    	$query = "SELECT * FROM examdata WHERE examid = '" . $eventId . "' AND paymentstatus = 1";
    	$_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        $result = [];
        if (mysqli_num_rows($_Response[2])) {
            while($data = mysqli_fetch_array($_Response[2])) {
            	$query = "SELECT Pay_Tran_PG_Trnid FROM tbl_payment_transaction WHERE timestamp > '2017-03-31 00:00:00' AND Pay_Tran_Status = 'PaymentReceive' AND Pay_Tran_AdmissionArray LIKE('%" . $data['examdata_code'] . "%')";
            	$_PayResponse = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            	$found = @mysqli_num_rows($_PayResponse[2]);
            	if (!$found || $found > 1) {
            		$result[$data['remark']][$data['examdata_code']] = [];
            		if ($found > 1) {
            			while ($row = mysqli_fetch_array($_PayResponse[2])) {
            				$result[$data['remark']][$data['examdata_code']][] = $row['Pay_Tran_PG_Trnid'];
            			}
            		}
            	}
            }
        }

        print_r($result);
    }

/**
*	Functions of Correction Fee Transactions End
*/

?>