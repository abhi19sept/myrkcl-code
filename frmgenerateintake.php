<?php
$title = "Generate Intake";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var SystemCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var SystemCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Generate Intake</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmgenerateintake" id="frmgenerateintake" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>  

                    <div id="grid" style="margin-top: 35px;">

                    </div>
                    <br>
                    <div class="col-md-12"> 							
                        <label for="tnc">Intake Generation Rule :</label></br>
                        <label for="tnc">1 Server System + 1 Client System = 12 Intake Capacity.</label></br> 
                        <label for="tnc">1 Extra Client System = 12 Intake Capacity.</label></br>                         
                    </div>
                    <br>
                    <div class="container">

                        <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Generate Intake"/>    
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include 'common/message.php'; ?>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode === 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAddSystem.php",
                data: "action=DELETE&values=" + SystemCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmgenerateintake.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmgenerateintake");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }




        function showData() {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfAddSystem.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
//alert(data);
                }
            });
        }

        showData();


        $("#btnSubmit").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfGenerateIntake.php",
                data: "action=GENERATEINTAKE",
                success: function (data)
                {
                    //alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                        window.setTimeout(function () {
                            Mode = "Add";
                            window.location.href = 'frmgenerateintake.php';
                        }, 3000);
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                    }

                }
            });
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</html>