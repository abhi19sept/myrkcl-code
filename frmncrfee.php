<?php
$title = "Ncr Fee Payment";
include ('header.php');
include ('root_menu.php');

echo "<script>var Mode='Add'</script>";
require("razorpay/checkout/manual.php");

?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Fee Payment</div>
            <div class="panel-body">
                <form name="frmfeepayment" id="frmfeepayment" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Organization/Center:</label>
                                <input type="text" class="form-control" readonly="" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Registration No:</label>
                                <input type="text" class="form-control" readonly="" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:</label>
                                <input type="text" class="form-control" readonly="" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:</label>
                                <input type="text" class="form-control" readonly="" name="txtType" id="txtType" placeholder="Type Of Organization">  
                            </div>
                        </div>  
                        <br>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Document Type:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Tehsil:</label>
                                <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="address">Address:</label>
                                <textarea class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road"></textarea>

                            </div>
                        </div>

                        <div>
                            <input type="hidden" name="paystatus" id="paystatus"/>
                            <input type="hidden" name="txtareatype" id="txtareatype"/>
                            <input type="hidden" name="ack" id="ack"/>
                        </div>
                         
						 <div class="container">
                            <div class="col-sm-4 form-group">
                                <label for="batch"> Select Payment Gateway:</label>
                                <select id="gateway" name="gateway" class="form-control">
                                    
                                    <option value="razorpay">Razorpay</option>
									<option value="payu">Payu</option>
                                </select>
                            </div>
                        </div>
						 
						 
						     <div class="container"> 
                                    <div class="col-md-11"> 	

                                        <label for="learnercode"><b>1. I hereby declare that SP has informed us about all the NCR rules and guidelines.</b></label><br>  
                                        <label for="learnercode"><b>2. I hereby declare that all information entered in correct and true.</b></label><br> 
                                        <label for="learnercode"><b>3. I hereby accept that at this point, it is just an interim activation of my application.</b></label><br> 
                                        <label for="learnercode"><b>4. I hereby declare that till my AO is finally approved as ITGK by RKCL, I will not do any kind marketing or admission at the address where we applied for new ITGK.</b></label><br> 
                                        <div class="container">
                                        <label class="checkbox-inline" style="float:left;"> <input type="checkbox" name="chk" id="chk" value="1" />
                                            I Accept 
                                        </label>
                                        </div>
                                    </div>
                                </div>
						 
                        <div class="container" id="divsubmit" style="display:none;">
                            <br><input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Proceed" style="display:none;"/>    
                        </div>
                    </div>
            </div>   
        </div>
        </form>
    </div>
    <form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
        <input type="hidden" id="txnid" name="txnid">
    </form>

</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">

 $('#chk').change(function () {
            if (this.checked) {
                $('#divsubmit').show();
                $('#chk').attr('disabled', true);
            } else {
                alert("Please select Check Box to Proceed.");
            }
        });
		
    function fillForm() {
        $.ajax({
            type: "post",
            url: "common/cfNcrFeePayment.php",
            data: "action=APPROVE",
            success: function (data) {
                $('#response').empty();
                if (data == "") {
                    $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Something went Wrong. Please try again." + "</span></p>");
                } else {
                    data = $.parseJSON(data);
                    txtName1.value = data[0].orgname;
                    txtRegno.value = data[0].regno;
                    txtEstdate.value = data[0].fdate;
                    txtType.value = data[0].orgtype;
                    txtDocType.value = data[0].doctype;
                    txtDistrict.value = data[0].district;
                    txtTehsil.value = data[0].tehsil;
                    txtareatype.value = data[0].type;
                    txtRoad.value = data[0].road;
                    ack.value = data[0].ack;
                    CheckPayStatus();
                }

            }
        });
    }

    function CheckPayStatus() {
        $.ajax({
            type: "post",
            url: "common/cfNcrFeePayment.php",
            data: "action=CheckPayStatus&values=" + ack.value + "",
            success: function (data) {
                paystatus.value = data;
                if (data == '0') {
                    ChkPayEvent();
                } else {
                    return false;
                }
            }
        });
    }

    function ChkPayEvent() {
        $.ajax({
            type: "post",
            url: "common/cfNcrFeePayment.php",
            data: "action=ChkPayEvent",
            success: function (data) {
                var paystatus = data;
                if (paystatus == "1") {
                    $('#btnSubmit').show();
                }
            }
        });
    }

    $(document).ready(function () {        
        fillForm();
        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $('#btnSubmit').hide();
            var url = "common/cfNcrFeePayment.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmfeepayment").serialize();
            
            if (Mode == 'Add') {
                data = "action=ADD&" + forminput;
                $('#txnid').val('');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#response').empty();
                        if (data == 0 || data == '') {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Something went Wrong. Please try again." + "</span></p>");
                        } else if (data != '') {
                            if (data == 'TimeCapErr') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                    alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                            } else {
                             if ($('#gateway').val() == 'razorpay') {
                                    var options = $.parseJSON(data);
                                    <?php include("razorpay/razorpay.js"); ?>
                                } else {
                                    $('#txnid').val(data);
                                    $('#frmpostvalue').submit();
                                }
                            }
                        }
                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.
        });
    });
</script>
</body>
</html>