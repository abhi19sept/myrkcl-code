<?php
$title = "Submitted Territory Preferences & Targets";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var ItPeripheralsCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var DeviceCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<div style="min-height:430px !important;max-height:1500px !important;">
<div class="container"> 

    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading"> Submitted Territory Preferences & Targets

        </div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            <form name="frmOrgMaster" id="frmOrgMaster" class="form-inline" action=""> 
<!--                <div class="container">
                    <div class="container">
                        <div id="response"></div>
                    </div>        
                    <div id="errorBox"></div>

                    <div class="container">
                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Select District:<span class="star">*</span></label>
                            <select id="ddlDistrict" name="ddlDistrict" class="form-control" >

                            </select>    
                        </div>
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="2MonthRural">First Quarter(May-July 2016) IT-GK Creation Target in Rural:</label>
                            <input type="text" class="form-control" name="txtTwoMonthRural" onkeyup="calc()" value="0"
                                   id="txtTwoMonthRural" placeholder=" 2 Month Rural" onkeypress="javascript:return allownumbers(event);" >
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="2MonthUrban">First Quarter(May-July 2016) IT-GK Creation Target in Urban:</label>
                            <input type="text" class="form-control" name="txtTwoMonthUrban" id="txtTwoMonthUrban" onkeyup="calc()" value="0" placeholder="2 Month Urban" onkeypress="javascript:return allownumbers(event);">    
                        </div>

                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="4MonthRural">Second Quarter(Aug-Oct 2016) IT-GK Creation Target in Rural:</label>
                            <input type="text" class="form-control" name="txtFourMonthRural" onkeyup="calc()" value="0"
                                   id="txtFourMonthRural" placeholder=" 4 Month Rural"  onkeypress="javascript:return allownumbers(event);">
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="4MonthUrban">Second Quarter(Aug-Oct 2016) IT-GK Creation Target in Urban:</label>
                            <input type="text" class="form-control" name="txtFourMonthUrban" id="txtFourMonthUrban" onkeyup="calc()" value="0" placeholder="4 Month Urban" onkeypress="javascript:return allownumbers(event);">    
                        </div>
                    </div>
                    <div class="container">

                        <div class="col-sm-4 form-group">     
                            <label for="6MonthRural">Next 6 months(Nov 2016 to Apr 2017) IT-GK Creation Target in Rural:</label>
                            <input type="text" class="form-control" name="txtSixMonthRural" onkeyup="calc()" value="0"
                                   id="txtSixMonthRural" placeholder=" 6 Month Rural" onkeypress="javascript:return allownumbers(event);" >
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="6MonthUrban">Next 6 months(Nov 2016 to Apr 2017) IT-GK Creation Target in Urban:</label>
                            <input type="text" class="form-control" name="txtSixMonthUrban" id="txtSixMonthUrban" onkeyup="calc()" value="0" placeholder="6 Month Urban" onkeypress="javascript:return allownumbers(event);">    
                        </div> 
                    </div>
                    <div class="container">
                        <div class="col-sm-5"> 
                            <label for="Total">Total IT-GK Creation Target in 12 months:</label>
                            <input type="text" readonly="true" class="form-control" name="Total" id="Total" placeholder="Total" onkeypress="javascript:return allownumbers(event);">    
                        </div>
                    </div>
                </div>
                <br>


                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                </div>-->
   <br>

                <div id="gird"></div>
             
        </div>
        </form>

    </div>   
</div>

</body>
</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>
<script>
    function calc()
    {
        var TwoMR = parseInt(document.getElementById('txtTwoMonthRural').value);
        var TwoMU = parseInt(document.getElementById('txtTwoMonthUrban').value);
        var FourMR = parseInt(document.getElementById('txtFourMonthRural').value);
        var FourMU = parseInt(document.getElementById('txtFourMonthUrban').value);
        var SixMR = parseInt(document.getElementById('txtSixMonthRural').value);
        var SixMU = parseInt(document.getElementById('txtSixMonthUrban').value);
         var Total = TwoMR + TwoMU + FourMR+ FourMU + SixMR + SixMU;
         
        if (Total)
        {
            document.getElementById("Total").value = Total;
        }
        
    }
</script>

<style>
    #errorBox
    {	color:#F00;	 } 
</style>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {



        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }

        function FillDistrict() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        }
        FillDistrict();

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfRspTarget.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }

        showData();


        $("#btnSubmit").click(function () {
            var total = document.getElementById("Total").value;
            if(total == "")
            {
                alert("Please Enter atleast one Target Field");
            }
            else{
               
            if ($("#frmOrgMaster").valid())
            {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfRspTarget.php"; // the script where you handle the form input
            var data;
            var forminput = $("#frmOrgMaster").serialize();
            if (Mode == 'Add')
            {
                data = "action=ADD&" + forminput; // serializes the form's elements.           
            }
            else
            {
                data = "action=UPDATE&code=" + ItPeripheralsCode + "&name=" + ddlDevice.value + "&available=" + avail_value + "&make=" + txtMake.value + "&model=" + txtModel.value + "&quantity=" + txtQuantity.value + "&detail=" + txtDetail.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            $('#response').empty();
                            window.location.href = "frmrsptarget.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("frmrsptarget");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });
            }
             
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmorgregistrationvalidation.js"></script>
</html>