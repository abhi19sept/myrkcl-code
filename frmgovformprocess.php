<?php
$title="Government Entry Form Process";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
$lcode=$_REQUEST['code']; 
 ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
	
	<div class="container">	
	<br/>
				                 
	   <div class="panel panel-primary" style="margin-top:0px !important;">
			 <div class="panel-heading">Registered Details With RKCL</div>
			   <div class="panel-body">
					<div class="col-sm-3 form-group"> 
						<label for="ename">ITGK-Code:</label>
						<input type="text" readonly="true" class="form-control" maxlength="50" name="txtitgkcode" id="txtitgkcode" placeholder="ITGK-Code">     
					</div>
					
					<div class="col-sm-3 form-group"> 
						<label for="ename">Employee Name:</label>
						<input type="text" readonly="true" class="form-control" maxlength="50" name="txtregEmpName" id="txtregEmpName" placeholder="Employee Name">     
					</div>

					<div class="col-sm-3 form-group">     
						<label for="faname">Father/Husband Name:</label>
						<input type="text" readonly="true" class="form-control" maxlength="50" name="txtregFaName" id="txtregFaName"  placeholder="Employee Father Name">
					</div>
					
					<div class="col-sm-3 form-group"> 
						<label for="dob">D.O.B:</label>
						<input type="text" readonly="true" class="form-control" name="txtregEmpDOB" id="txtregEmpDOB" placeholder="Date of Birth">                               
					</div>						 
					
					 <div class="col-sm-3 form-group"> 
						<label for="mobile"> Mobile No.:</label>
						<input type="mobile" readonly="true" class="form-control" name="txtregMobile" id="txtregMobile" placeholder="Mobile No">    
					</div>								
					

					<div class="col-sm-3 form-group"> 
						<label for="course">Course Name:</label>
						<input type="text" maxlength="35" readonly="true" class="form-control" name="txtCourse" id="txtCourse" placeholder="Course Name">                               
					</div>						 
					
					 <div class="col-sm-3 form-group"> 
						<label for="batch">Batch Name:</label>
						<input type="text" readonly="true" class="form-control" name="txtBatch" id="txtBatch" placeholder="Batch Name">    
					</div>
					
				 </div>			   
	   </div> 
                   
						
		<div class="panel panel-primary" style="margin-top:26px !important;">
            <div class="panel-heading">Reimbursement Application  Form</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
					
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data"> 
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Learner Code:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control" maxlength="50" onkeypress="javascript:return allownumbers(event);" name="txtLCode" id="txtLCode" placeholder="LearnerCode">
                                <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                                <input type="hidden" class="form-control"  name="LearnerCode" id="LearnerCode" value="<?php echo $lcode; ?>"/>
                                <input type="hidden" class="form-control"  name="txtitgk" id="txtitgk"/>
                            </div>                   
			</div>
                        
			 <div class="container">		     
                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Employee Name:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control" maxlength="50" onkeypress="javascript:return allowchar(event);" name="txtEmpName" id="txtEmpName" placeholder="Employee Name">     
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="faname">Father/Husband Name:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control" maxlength="50" onkeypress="javascript:return allowchar(event);" name="txtFaName" id="txtFaName"  placeholder="Employee Father Name">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="dob">Date Of Birth:<span class="star">*</span></label>
								<input type="text" maxlength="15" class="form-control" onkeypress="javascript:return allownumbers(event);" name="txtEmpDOB"
									id="txtEmpDOB" placeholder="Date of Birth">                               
                            </div>
                             <div class="col-sm-4 form-group"> 
                                <label for="dob">Date Of Appointment:<span class="star">*</span></label>
                                <input type="text" maxlength="15" class="form-control" onkeypress="javascript:return allownumbers(event);" name="txtEmpDOBAPP"
									id="txtEmpDOBAPP" placeholder="Date of Appointment">                               
                            </div>
							
							 <div class="col-sm-4 form-group" style="display:none;"> 
                                <label for="email">Employee EmailId:<span class="star">*</span></label>
                                <input type="email" readonly="true" class="form-control" name="txtEmail" id="txtEmail" placeholder="Emp EmailId">    
                            </div>
							
                         </div> 						
						
						<div class="container">							
							 <div class="col-sm-4 form-group"> 
                                <label for="pan">Employee Department:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control"  name="txtEmpDepartment" id="txtEmpDepartment"  placeholder="Employee Department">
                            </div>

							<div class="col-sm-4 form-group"> 
                                <label for="pan">Employee Designation:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control"  name="txtEmpDesignation" id="txtEmpDesignation"  placeholder="Employee Designation">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="email">Office Address:<span class="star">*</span></label>
								<textarea class="form-control"  name="txtAddress" id="txtAddress" maxlength="100" placeholder="Address" onkeypress="javascript:return allowchar(event);"> </textarea>
                                <!--<input type="text" class="form-control" name="txtAddress" id="txtAddress" placeholder="Office Address">   --> 
                            </div>	
								
							<div class="col-sm-4 form-group"> 
                                <label for="empid">Employee District:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control"  name="txtdistrict" id="txtdistrict" placeholder="Employee District">
                            </div>  
							
						 </div>
						 
						 <div class="container">								
							<div class="col-sm-4 form-group"> 
                                <label for="pan">Batch Name:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control"  name="txtBatchName" id="txtBatchName"  placeholder="Batch Name">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="pan">Batch Id:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control"  name="txtBatchId" id="txtBatchId"  placeholder="Batch Id">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="marks">RS-CIT Exam Marks:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control" id="txtEmpMarks" name="txtEmpMarks" onkeypress="javascript:return allownumbers(event);"
									placeholder="RS-CIT Exam Marks">
                            </div>	

							<div class="col-sm-4 form-group">     
                                <label for="emobile">Employee Mobile No.:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="10" name="txtEmpMobile" onkeypress="javascript:return allownumbers(event);"
									id="txtEmpMobile" placeholder="Emp Mobileno" >
                            </div>   
                        </div>
						
						 <div class="container">
							<div class="col-sm-4 form-group"> 
                                <label for="bankaccount">Employee Bank Account No.:<span class="star">*</span></label>
                                <input type="text" class="form-control"  name="txtBankAccount" maxlength="18" onkeypress="javascript:return allownumbers(event);"  id="txtBankAccount" placeholder="Emp Bank Accountno">
                            </div>
							
     					<!--	<div class="col-sm-4 form-group"> 
                                <label for="pan">IFSC Code:<span class="star">*</span></label>
                                <input type="text" class="form-control"  name="txtIFSCcode" id="txtIFSCcode"  placeholder="IFSC Code" maxlength="12">
                            </div> -->
							
							<div class="col-sm-4 form-group"> 
                                <label for="ifsc">IFSC Code:<span class="star">*</span></label>
                                <input type="text" maxlength="11" class="form-control text-uppercase" onkeypress="fnValidateIFSC();"   name="txtIFSCcode"  id="txtIFSCcode" placeholder="EMP Bank IFSC Code">
                                <span id="ifsccode" style="font-size:10px;"></span><br>
                                <span style="font-size:10px;">Note : IFSC code should be 11 digit. Starting 4 should be only alphabets[A-Z] and Remaining 7 should be accepting only alphanumeric.</span>
                             </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="pan">GPF/PRAN No.:<span class="star">*</span></label>
                                <input type="text" class="form-control"  name="txtgpf" id="txtgpf"  placeholder="GPF No.">
                            </div>	
							
							<div class="col-sm-4 form-group"> 
                                <label for="eid">Employee Id:<span class="star">*</span></label>
                                <input type="text" class="form-control" readonly="true" name="txtEmpId" id="txtEmpId" placeholder="Employee Id">
                            </div>	                            
							
						</div>
						 
						 <div class="container">								
							
							<div class="col-sm-4 form-group"> 
                                <label for="pan">Fee:<span class="star">*</span></label>
                                <input type="text" class="form-control" readonly="true" maxlength="4" name="txtFee" id="txtFee"  placeholder="Fee">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="pan">Incentive:<span class="star">*</span></label>
                                <input type="text" class="form-control" readonly="true" name="txtIncentive" maxlength="3" id="txtIncentive"  placeholder="Incentive">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="pan">Total Amount:<span class="star">*</span></label>
                                <input type="text" class="form-control" readonly="true" maxlength="4" name="txtTotalAmt" id="txtTotalAmt"  placeholder="Total Amount">			
                            </div>
							
							<div class="col-sm-6 form-group"> 
                                <label for="lot">Lot:<span class="star">*</span></label>
                                <select id="ddlLot" name="ddlLot" class="form-control">
								
                                </select>
                            </div>
							
						</div>					
					
						<div class="container">							
							<button type="button" id="showmodal" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
							<div class="col-sm-4 form-group" > 
								<label for="photo">Pre Payment Receipt:<span class="star">*</span></label> </br>
							  <button type="button" id="uploadPreview1" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
								View Payment Receipt</button>	
							</div>		
							
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Department Forwarding Letter:<span class="star">*</span></label> </br>
							  <button type="button" id="uploadPreview4" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
								View Forwarding Letter</button>
							</div>
							
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Cancel Cheque:<span class="star">*</span></label> </br>
							  <button type="button" id="uploadPreview3" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
								View Cancel Cheque</button>
							</div>						
							
							<div class="col-sm-4 form-group" > 
							  <label for="photo">RS-CIT Final Certificate:<span class="star">*</span></label> </br>
							  <button type="button" id="uploadPreview5" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
								View RS-CIT Certificate</button>
							</div>
							
						</div>	

						<div class="container">
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Birth Proof:<span class="star">*</span></label> </br>
							  <button type="button" id="uploadPreview2" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
								View Birth Proof</button>
							</div>							
						</div>
					
					<div id="aftervalidate" style="display:none;">
						<div class="container">
							<div class="col-sm-2 form-group" > 
								<label for="email">Status:</label> <br/>  
								<label class="radio-inline"><input type="radio" id="statusApprove" checked="checked" name="mstatus" /> Approved by RKCL </label>
															
							</div>
							<div class="col-sm-2 form-group"> 
								<label for="email"></label> <br/>
								
								<label class="radio-inline" style="padding-top:4px;"><input type="radio" id="statusReject" name="mstatus" /> Rejected by RKCL	</label>							
							</div>						
							
							<div class="col-sm-4 form-group" id="remark"> 
                                <label for="pan">Remark:<span class="star">*</span></label>
								<textarea class="form-control"  name="txtRemark" id="txtRemark" maxlength="100" placeholder="Remark" onkeypress="javascript:return allowchar(event);"> </textarea>
                               <!-- <input type="text" class="form-control"  name="txtRemark" id="txtRemark"  placeholder="Remark" onkeypress="javascript:return allowchar(event);">-->
                            </div>
						</div> 
						 
						
						<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
					</div>
					
					<div class="container">
                            <input type="button" name="btnValidate" id="btnValidate" class="btn btn-primary" value="Validate"/>    
                    </div>
						
						<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
						  <div class="modal-content">
							<div class="modal-header">
								<button class="close" type="button" data-dismiss="modal">×</button>
								<h3 id="heading-tittle" class="modal-title">Heading</h3>
							</div>
							<img id="viewimagesrc" class="thumbnail img-responsive" src="images/not-found.png" name="filePhoto3" width="800px" height="880px">
							<div class="modal-footer">
								<button class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						   </div>
						  </div>
						</div>
					
                </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
 .viewimage{    color: #ffffff;
    background: #3c763d none repeat scroll 0 0 !important;
    border-color: #125f13;}
</style>

<style>
  .modal-dialog {width:950px;}
.thumbnail {margin-bottom:6px; width:950px; height: 880px;}
  </style>
  <script type="text/javascript">
    $('#txtEmpDOB').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
       // autoclose: true
    });
</script> 
 
  <script type="text/javascript">
$('#txtIFSCcode').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");	
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
});

function fnValidateIFSC(){
         $('#ifsccode').html('');
     }
	 
		function AllowIFSC(ifsc) {//
		 
		 var reg = /^[A-Z|a-z]{4}[\w]{7}$/;
		 //var reg = /^[A-Z|a-z]{4}[0][\w]{6}$/;
            //var reg = /^[A-Za-z]{4}\d{7}$/;
            //var ifsc = `SBIN1234567`;
            if(ifsc!=''){
                if (ifsc.match(reg)) {
                     $('#ifsccode').empty();
                }
                else {
                    $('#ifsccode').empty();
                    $('#txtIFSCcode').val('');
                    $('#ifsccode').append("<span style='color:red; font-size: 12px;'>You have entered wrong IFSC Code.</span>");

                    return false;
                }
            }
        }
		
</script>	
  <script type="text/javascript">
  
  $(document).ready(function() {
		jQuery(".thumbnailmodal").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#showmodal').click();
});
});
  </script>
<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () { 
		
		function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();

        function FillStatus() {
            $.ajax({
                type: "post",
                url: "common/cfGovApproved.php",
                data: "action=FILLAPPROVEDSTATUS",
                success: function (data) {
					//alert(data);
                    $("#ddlstatus").html(data);
                }
            });
        }
        //FillStatus();

        function FillLot() {
            $.ajax({
                type: "post",
                url: "common/cfGovApproved.php",
                data: "action=GETALLLOT",
                success: function (data) {
                    $("#ddlLot").html(data);
                }
            });
        }
        FillLot();
		
		function FillFee() {
            $.ajax({
                type: "post",
                url: "common/cfGovApproved.php",
                data: "action=FILLGovFee&code=" + LearnerCode.value + "&mode=" + Mode  + "",
                success: function (data) {
					//alert(data);
                    data = $.parseJSON(data);
						txtFee.value = data[0].Fee;
                        txtBatchId.value = data[0].BatchId;
						txtBatchName.value = data[0].BatchName;
						txtIncentive.value = data[0].Incentive;
						var val1 = parseInt(document.getElementById("txtFee").value);
                        var val2 = parseInt(document.getElementById("txtIncentive").value);
                        var ansD = document.getElementById("txtTotalAmt");
                        ansD.value = val1 + val2;
						//txtTotalAmt.value = txtFee.value + txtIncentive.value;
                }
            });
        }
        //FillFee();
		
		function FillExamAttempt() {
            $.ajax({
                type: "post",
                url: "common/cfGovApproved.php",
                data: "action=FILLGovAttempt&code=" + LearnerCode.value + "",
                success: function (data) {                    
                   txtIncentive.value = data;
                }
            });
        }
        //FillExamAttempt();

		if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
			else if (Mode == 'Edited')
            {
				//alert(1);
                fillForm();
				fillRegisteredData();
            }
            else if (Mode == 'Edit')
            {
				//alert(1);
                fillForm();
				fillRegisteredData();
            }
			
			function fillRegisteredData()
            {               
				$.ajax({
                    type: "post",
                    url: "common/cfGovApproved.php",
                    data: "action=GetRegisteredData&values=" + LearnerCode.value + "",
                    success: function (data) {						
                        data = $.parseJSON(data);						
                        txtregEmpName.value = data[0].lname;
                        txtregFaName.value = data[0].fname;                        
						txtregMobile.value = data[0].mobile;
						txtCourse.value = data[0].course;						
						txtBatch.value = data[0].batch;						
						txtregEmpDOB.value = data[0].dob;
						txtitgkcode.value = data[0].itgk;
					}
                });
            }
			
		function fillForm()
            {               
				$.ajax({
                    type: "post",
                    url: "common/cfGovApproved.php",
                    data: "action=EDIT&values=" + LearnerCode.value + "",
                    success: function (data) {						
                        data = $.parseJSON(data);
						txtLCode.value = data[0].LearnerCode;
                                                txtEmpName.value = data[0].lname;
                                                txtFaName.value = data[0].fname;
                                                txtEmail.value = data[0].email;
						txtEmpMobile.value = data[0].mobile;
						txtdistrict.value = data[0].district;
						txtEmpMarks.value= data[0].marks;
						txtBankAccount.value=data[0].accountno;
						txtIFSCcode.value = data[0].ifsc;
						txtgpf.value = data[0].gpf;
						txtEmpDepartment.value = data[0].dname;
						txtEmpDesignation.value = data[0].designation;
						txtAddress.value = data[0].offadd;
                                                txtEmpId.value = data[0].eid;
						txtEmpDOB.value = data[0].dob;
                                                txtEmpDOBAPP.value = data[0].dobapp;
						txtitgk.value = data[0].itgk;
						
						txtFee.value = data[0].Fee;
                                                txtBatchId.value = data[0].BatchId;
						txtBatchName.value = data[0].BatchName;
						txtIncentive.value = data[0].Incentive;
						txtTotalAmt.value = data[0].TotalAmt;
					
						
						$("#uploadPreview1").click(function () {
							if(data[0].receipt == null){
								$("#viewimagesrc").attr('src',"images/not-found.png");
								$("#heading-tittle").html('Pre Payment Receipt Not Available');
							}else{
								$("#viewimagesrc").attr('src',"upload/government_payment/" + data[0].receipt);
								$("#heading-tittle").html('Pre Payment Receipt');
							}
						});	
						$("#uploadPreview2").click(function () {
							if(data[0].birth == null){
								$("#viewimagesrc").attr('src',"images/not-found.png");
								$("#heading-tittle").html('Birth Proof Not Available');
							}else{
								$("#viewimagesrc").attr('src',"upload/government_birth/" + data[0].birth);
								$("#heading-tittle").html('Birth Proof');
							}
						});
						$("#uploadPreview3").click(function () {
							if(data[0].cheque == null){
								$("#viewimagesrc").attr('src',"images/not-found.png");
								$("#heading-tittle").html('Cancel Cheque Not Available');
							}else{
								$("#viewimagesrc").attr('src',"upload/government_cancelcheck/" + data[0].cheque);
								$("#heading-tittle").html('Cancel Cheque');
							}
						});
						$("#uploadPreview4").click(function () {
							if(data[0].dpletter == null){
								$("#viewimagesrc").attr('src',"images/not-found.png");
								$("#heading-tittle").html('Department Forwarding Letter Not Available');
							}else{
								$("#viewimagesrc").attr('src',"upload/government_dpletter/" + data[0].dpletter);
								$("#heading-tittle").html('Department Forwarding Letter');
							}
						});
						$("#uploadPreview5").click(function () {
							if(data[0].certificate == null){
								$("#viewimagesrc").attr('src',"images/not-found.png");
								$("#heading-tittle").html('RS-CIT Final / Provisional Certificate Not Available');
							}else{
								$("#viewimagesrc").attr('src',"upload/government_rscitcer/" + data[0].certificate);
								$("#heading-tittle").html('RS-CIT Final / Provisional Certificate');
							}
						});
						
						//$("#uploadPreview1").attr('src',"upload/government_payment/" + data[0].receipt);
						//$("#uploadPreview2").attr('src',"upload/government_birth/" + data[0].birth);
						
						//$("#uploadPreview3").attr('src',"upload/government_cancelcheck/" + data[0].cheque);
						//$("#uploadPreview4").attr('src',"upload/government_dpletter/" + data[0].dpletter);
						//$("#uploadPreview5").attr('src',"upload/government_rscitcer/" + data[0].certificate);						
					}
                });
            }
			
			$("#btnValidate").click(function () {
					var ifsc = $('#txtIFSCcode').val();
						AllowIFSC(ifsc);
					if ($("#form").valid()) {
						
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						var dob = $('#txtEmpDOB').val();
						var url = "common/cfGovApproved.php";
						data = "action=ValidateData&learnercode=" + LearnerCode.value + "&accountno=" + txtBankAccount.value + "&mobile=" + txtEmpMobile.value + "&dob=" + dob + ""; // serializes the
						
						$.ajax({
						type: "POST",
						url: url,
						data: data,
						success: function (data)
						{
							if (data == 1) {
								$('#response').empty();
								$("#btnValidate").hide();
								$("#aftervalidate").show();
							}
							else if (data == 2) {
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Age should not be greater than 55 Years.</span></p>");
								$("#btnValidate").hide();
								$("#aftervalidate").show();
								$('#statusReject').prop('checked', true);
								$('#statusApprove').prop('disabled', true);
							}
							else
							{
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
								$("#btnValidate").hide();
								$("#aftervalidate").show();
								$('#statusReject').prop('checked', true);
								$('#statusApprove').prop('disabled', true);
								//document.getElementById("statusReject").innerHTML = $('input[name=mstatus]:checked').val();
								//$("#statusReject").checked();
							}
							//showData();

						}
            });
		}
			return false;
	});
		
        $("#btnSubmit").click(function () {
			var ifsc = $('#txtIFSCcode').val();
						AllowIFSC(ifsc);
		 if ($("#form").valid())
           {
			   if (confirm("Are you sure you want to process this learner's status?"))
            {            
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			var dob = $('#txtEmpDOB').val();
		   var url = "common/cfGovApproved.php"; // the script where you handle the form input.  
			
			if (document.getElementById('statusApprove').checked) //for radio button
                {
                    var ddlstatus = '0';
                }
                else {
                    ddlstatus = '4';
                }
				
            var data;			
            if (Mode == 'Add')
            {
            
			}
            else
            {
                data = "action=UPDATE&learnercode=" + LearnerCode.value + "&ifsc=" + txtIFSCcode.value + "&accountno=" + txtBankAccount.value + "&gpf=" + txtgpf.value + "&status=" + ddlstatus + "&lot=" + ddlLot.value + "&batchid=" + txtBatchId.value + "&fee=" + txtFee.value + "&incentive=" + txtIncentive.value + "&totalamt=" + txtTotalAmt.value + "&remark=" + txtRemark.value + "&address=" + txtAddress.value + "&mobile=" + txtEmpMobile.value + "&dob=" + dob + "&itgk=" + txtitgk.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmgovformapproval.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
					else if (data == 1) {
						$('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     This Learner is Already Being Processed. Please try again." + "</span></p>");
						  window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmgovformapproval.php';
                            }, 5000);
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });
			}
		   }
            return false;
			//return false;
		  });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmgoventryprocess_validation.js"></script>
</html>