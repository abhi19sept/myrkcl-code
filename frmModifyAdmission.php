<?php
$title = "Modify Admission";
include ('header.php');
include ('root_menu.php');
$_SESSION['ImageFile'] = "";
$_SESSION['SignFile'] = "";
$_SESSION['ScanFile'] = "";
if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
?>
<div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">Modify Admission</div>
        <div class="panel-body">
            <form name="frmmodifyadmission" id="frmmodifyadmission" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">

                    <div class="container">
                           <!--  <div id="loadingDiv" class="overlay">
                            <div class="overlay__inner">
                                <div class="overlay__content"><span class="spinner"></span></div>
                            </div>
                        </div>-->
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>


                    <div class="col-sm-4 form-group">     
                        <label for="course">Select Course:<span class="star">*</span></label>
                        <select id="ddlCourse" name="ddlCourse" class="form-control">

                        </select>

                    </div> 
                </div>

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Batch:<span class="star">*</span></label>
                        <select id="ddlBatch" name="ddlBatch" class="form-control">

                        </select>									
                    </div> 
                </div>

                <div id="menuList" name="menuList" style="margin-top:35px;"> </div> 

                <div class="container">
                <!--	<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    -->
                </div>
        </div>
    </div>   
</div>
</form>
</div>
</body>
<?php include ('footer.php'); ?>
<style>
    .overlay {
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    position: fixed;
    background: #222;
    z-index: 999;
    opacity: 0.2;
}

.overlay__inner {
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    position: absolute;
}

.overlay__content {
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
}

.spinner {
  
    display: inline-block;
    border: 16px solid #f3f3f3; /* Light grey */
      border-top: 16px solid #006fba; /* Blue */
      border-radius: 50%;
      width: 120px;
      height: 120px;
      animation: spin 2s linear infinite;
}

@keyframes spin {
  100% {
    transform: rotate(360deg);
  }
}
a {
  color: #FFFFFF;
  text-decoration: none;
}
    #errorBox
    {   color:#F00;  } 
</style>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    /* var $loading = $('#loadingDiv').hide();
    $(document)
      .ajaxStart(function () {
        $loading.show();
      })
      .ajaxStop(function () {
        $loading.hide();
      });*/
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();


        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLModifyEventBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });




        function showAllData(val, val1) {
            //alert(val);
            //alert(val1);
            $.ajax({
                type: "post",
                url: "common/cfAdmissionModify.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#menuList").html(data);
					 $('#example').DataTable();
                }
            });
        }

        $("#ddlBatch").change(function () {
            showAllData(this.value, ddlCourse.value);
        });

        if (Mode === 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }


        function deleteRecord() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAdmissionModify.php",
                data: "action=DELETE&values=" + Admission_Name + "&batchcode=" + BatchCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmModifyAdmission.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmmodifyadmission");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();
                }
            });
        }

		$("#menuList").on("click",".view_photo",function(){
			var batchname = $(this).attr("batchname");
			var image = $(this).attr("id");
			var count = $(this).attr("count");
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAdmissionModify.php",
                data: "action=ViewPhoto&batchname=" + batchname + "&image=" + image + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#"+count+"_pic").show();
                    $("#"+count+"_photo").hide();
					$("#Viewphoto_"+count).html(data);
                }
            });
		});
		
		$("#menuList").on("click",".view_sign",function(){
			var batchname = $(this).attr("batchname");
			var image = $(this).attr("id");
			var count = $(this).attr("count");
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAdmissionModify.php",
                data: "action=ViewSign&batchname=" + batchname + "&image=" + image + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#"+count+"_signature").show();
                    $("#"+count+"_sign").hide();
					$("#Viewsign_"+count).html(data);
                }
            });
		});

        $("#btnSubmits").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfPermissionMaster.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmpermissionmaster").serialize();
            //alert(forminput);




            if (Mode == 'Add')
            {
                data = "action=ADD&" + forminput; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmpermissionmaster.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmpermissionmaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>