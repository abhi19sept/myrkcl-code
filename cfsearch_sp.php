<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clssearch_sp.php';

$response = array();
$emp = new clssearchsp();


if ($_action == "SPlist") {
    $response = $emp->SPlist($_REQUEST);
	
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Service Provider Name </th>";
	echo "<th>Contact Person</th>";
	echo "<th >Mobile No.</th>";
	echo "<th >Email ID</th>";
	if(isset($_SESSION['Login'])){
		if($_SESSION['Login'] == 1 || $_SESSION['User_Code'] ==8){
			echo "<th >Action</th>";
		}
	}
    
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
		echo "<td>" . ucwords(strtolower($_Row['Rsptarget_contactperson'])). "</td>";
        echo "<td>" . $_Row['Contactperson_mobile'] . "</td>";
        echo "<td>" . $_Row['Contactperson_email'] . "</td>";
		if(isset($_SESSION['Login'])){
			if($_SESSION['Login'] == 1 || $_SESSION['User_Code'] ==8){
				echo "<td> <button id='spupdate_".$_Row['Rsptarget_User'] ."' type='button' contactp='".$_Row['Rsptarget_contactperson']."' contactpmobile='".$_Row['Contactperson_mobile']."' contactpemail='".$_Row['Contactperson_email']."' class='sp_editbtn update_spmodal' data-toggle='modal' data-target='.bs-example-modal-sm'>Update</button> </td>";
			}
        }
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
if($_action == 'update_spcontact'){
	$Rsptarget_Code = $_REQUEST['value'];
	// $contactp = $_REQUEST['contactp'];
	$response = $emp->Update_SP($Rsptarget_Code, $_REQUEST);
	echo "Contact Person for SP is recorded successfully.";
}
?>