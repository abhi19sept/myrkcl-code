<?php
$title = "GST Invoice Report";
include ('header.php');

include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var BatchCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var BatchCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '14') {
    ?>
    <!--<link rel="stylesheet" href="css/datepicker.css">
    <script src="scripts/datepicker.js"></script>-->
    <link rel="stylesheet" href="bootcss/css/bootstrap-datetimepicker.min.css">
    <script src="bootcss/js/moment.min.js"></script>
    <script src="bootcss/js/bootstrap-datetimepicker.min.js"></script>
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">GST Invoice Report</div>
            <div class="panel-body">

                <form name="form" id="form_reconcilation" class="form-inline" role="form" >
                    <div class="container" style="width:100%;">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                        <div class="container">
                            <div class="col-sm-6 form-group"> 
                                <label for="sdate">Start Date:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-4 form-group">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type="text" class="form-control" id="txtstartdate" name="txtstartdate" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="col-sm-6 form-group"> 
                                <label for="sdate">End Date:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-4 form-group">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type="text" class="form-control" id="txtenddate" name="txtenddate" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="col-sm-6 form-group"> 
                                <label for="sdate">Report Type:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-4 form-group">
                                <select id="reportType" name="reportType" class="form-control selectpicker">
                                    <option value="">Select</option>
                                    <option value="learner_fee">Learner Fee Payment</option>
                                    <option value="reexam_fee">Learner Re-Exam Payment</option>
                                    <option value="NCR_fee">NCR Payment</option>
                                    <option value="correction_fee">Correction & Duplication Payment</option>
                                    <option value="name_fee">Name & Address Change Payment</option>
                                    <option value="eoi_fee">EOI Payment</option>
                                    <option value="RenewalPenalty_fee">RenewalPenalty Payment</option>
                                    <option value="OwnerChange_fee">Ownership Change Payment</option>
                                    <option value="CorrectionBfrExam_fee">Correction Before Final Exam Payment</option>
                                    <option value="RSCFACert_fee">RS-CFA Certificate Payment</option>
                                    <option value="RSCFAReexam_fee">RS-CFA Women Re-Exam Payment</option>

                                </select>
                            </div>
                        </div>
                        <div id="learner_fee" style="display:none">

                            <div class="container" id="cour">
                                <div class="col-sm-6 form-group"> 
                                    <label for="sdate">Select Course:<span class="star">*</span></label>

                                </div>


                                <div class="col-sm-4 form-group">
                                    <div class="multiselect">
                                        <div class="selectBox" onclick="showCheckboxes()">
                                            <select id="ddlCourse" name="ddlCourse" class="form-control" >
                                                <option>Select Courses</option>
                                            </select>
                                            <div class="overSelect"></div>
                                        </div>
                                        <div id="checkboxes" >
                                            <label for="one">
                                                <input type="checkbox" id="one" class="coursechk" value="1" />RS-CIT</label>
                                            <label for="two">
                                                <input type="checkbox" id="two"  class="coursechk" value="4" />RS-CIT Government Employee</label>
                                            <label for="three">
                                                <input type="checkbox" id="three"  class="coursechk" value="5"/>RS-CFA</label>
                                            <label for="four">
                                                <input type="checkbox" id="four"  class="coursechk" value="21"/>Red-Hat</label>
                                            <label for="four">
                                                <input type="checkbox" id="four"  class="coursechk" value="23"/>RS-CIT Click</label>
												
											
<label for="five">
                                            <input type="checkbox" id="four"  class="coursechk" value="25"/>RS-CEE</label>	
											                       <label for="five">
                                            <input type="checkbox" id="four"  class="coursechk" value="28"/>RS-CAE</label>  
                                            <label for="five">
                                            <input type="checkbox" id="four"  class="coursechk" value="29"/>RS-CBC</label>  
                                            <label for="five">
                                            <input type="checkbox" id="four"  class="coursechk" value="30"/>RS-CCS</label>  
                                            <label for="five">
                                            <input type="checkbox" id="four"  class="coursechk" value="31"/>RS-CDM</label>  
                                            <label for="five">
                                            <input type="checkbox" id="four"  class="coursechk" value="32"/>RS-CDP</label> 
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="container">
                                <div class="col-sm-6 form-group"> 
                                    <label for="sdate">Select Batches:<span class="star">*</span></label>

                                </div> 
                                <div class="col-sm-4 form-group">
                                    <select id="ddlBatch" name="ddlBatch[]" multiple class="form-control" required="required">

                                    </select>
                                </div>


                            </div>

                        </div>
                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View" />    


                        </div> 
                        <div class="container">
                            <div id="grid" style="margin-top:35px;"> </div>
                        </div>
                    </div>   
                </form>
            </div>
        </div>
    </div>
    </div>

    <?php include ('footer.php'); ?>
    <?php include'common/message.php'; ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>		

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
    <style>


        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;

        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
        }

        #checkboxes label {
            display: block;
        }

        #checkboxes label:hover {
            background-color: #1e90ff;
        }
    </style>
    </body>
    <script>
                                            var expanded = false;

                                            function showCheckboxes() {
                                                var checkboxes = document.getElementById("checkboxes");
                                                if (!expanded) {
                                                    checkboxes.style.display = "block";
                                                    expanded = true;
                                                } else {
                                                    checkboxes.style.display = "none";
                                                    expanded = false;
                                                }
                                            }
    </script>
    <script type="text/javascript">
        $('#txtstartdate').parent().datetimepicker({
            format: 'DD-MM-YYYY',
            widgetPositioning: {horizontal: "auto", vertical: "bottom"}
        });
        $('#txtenddate').parent().datetimepicker({
            format: 'DD-MM-YYYY',
            widgetPositioning: {horizontal: "auto", vertical: "bottom"}
        });
        $('#txtstartdate').datetimepicker({
            format: 'DD-MM-YYYY',
            widgetPositioning: {horizontal: "auto", vertical: "bottom"}
        });
        $('#txtenddate').datetimepicker({
            format: 'DD-MM-YYYY',
            widgetPositioning: {horizontal: "auto", vertical: "bottom"}
        });
    </script>

    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        //$("#txtstartdate, #txtenddate").datetimepicker();
        $(document).ready(function () {
            function sortbatch() {

                var x = document.getElementById("ddlBatch");
                var txt = "";
                var i;
                var tmpAry = new Array();
                for (i = 0; i < x.length; i++) {
                    tmpAry[i] = new Array();
                    tmpAry[i][0] = x.options[i].text;
                    tmpAry[i][1] = x.options[i].value;
                }
                tmpAry.sort(function (a, b) {
                    return a[1] - b[1];
                });

                tmpAry.reverse();
                while (x.options.length > 0) {
                    x.options[0] = null;
                }
                for (var i = 0; i < tmpAry.length; i++) {
                    var op = new Option(tmpAry[i][0], tmpAry[i][1]);
                    x.options[i] = op;
                }
                //alert(tmpAry);
                return;


            }
            function FillCourse() {
                $.ajax({
                    type: "post",
                    url: "common/cfCourseMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlCourse").html(data);
                    }
                });
            }
            //FillCourse();

            function multisel() {
                $('#ddlBatch').multiselect({
                    nonSelectedText: 'Select Batch',
                    includeResetOption: true,
                    resetText: "Reset all",
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,

                    minHeight: 200,
                    maxHeight: '200',
                    width: '33%',
                    buttonWidth: '100%',
                    dropRight: true

                });
            }

            $('.coursechk').on('change', function () {

                var val = this.checked ? this.value : '0';
                //alert(val);
                if (val != 0) {

                    $.ajax({
                        type: "post",
                        url: "common/cfBatchMaster.php",
                        data: "action=FILLAdmissionBatchCourse&values=" + val + "",
                        success: function (data) {
                            $("#ddlBatch").append(data);
                            sortbatch();
                            $("#ddlBatch option[value='0']").remove();
                            multisel();

                            $('#ddlBatch').multiselect('rebuild');

                            $("#ddlBatch option[value='0']").remove();
                        }
                    });
                } else {
                    $("#ddlBatch").empty();
                }

            });

          //  sortbatch();
            $("#btnSubmit").click(function (e) {
                //alert("aya");
                e.preventDefault();
                $("#grid").html();
                var startd = $('#txtstartdate').val();
                var endd = $('#txtenddate').val();
                var reportType = $("#reportType").val();

                if (startd == "" || endd == "") {
                    $('#response').append("<p class='error'><span><p style='color:red;font-size:14px'>Please Fill All Required Fields.</p></span></p>");
                } else {
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    var url = "common/cfGstInvoiceRpt.php"; // the script where you handle the form input.
                    var data = $('#form_reconcilation').serialize();

                    $.ajax({
                        type: "post",
                        url: url,
                        data: "action=" + reportType + "&" + data + "",

                        success: function (data) {
//alert($("#reportType option:selected").text());
                            $("#grid").html(data);
                            $('#example').DataTable({
                                dom: 'Bfrtip',
                                buttons: [
                                    {
                                        extend: 'excelHtml5',
                                        title:  'GST_Invoice_'+$("#reportType option:selected").text()+'_'+$("#txtstartdate").val()+'to'+$("#txtenddate").val(),
                                    },
                                    {
                                        extend: 'pdfHtml5',
                                        title: 'Data export'
                                    }
                                ]
                            });/* */
                            $('#response').empty();

                            $(".approvalLetter").click();
                        }
                    });
                    //showdata();
                }
                return false; // avoid to execute the actual submit of the form.
            });


            $("#reportType").on('change', function () {

                var reportType = this.value;
                if (reportType == "learner_fee") {
                    $("#learner_fee").show();
                } else {
                    $("#learner_fee").hide();
                }

            })

        });

    </script>
    <style>
        .container .container{
            width:100%;
        }
        .form-inline .col-sm-4.report{
            display:none;
        }
        .form-inline .form-group.col-sm-4 {
            max-width: unset;
        }
        .form-inline .input-group {
            width: 100%;
        }

    </style>
    <style>
        .modal-dialog {width:600px;}
        .thumbnail {margin-bottom:6px; width:800px;}
    </style>

    </html>
    <?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "index.php";
    </script>
    <?php
}
?>
