<?php
$title = "WCD Scheme Declaration Form";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '4') {	

?>

<style>
@font-face {
font-family: 'Devlys';
src: url('fonts/Devlys.ttf') format('truetype')}
.KrutiDev_hindi_text {font-family: Devlys !important;font-size: 18px;}
.feedbackquiz{width:30%; float:right;}
.feedbackquiz p{font-family: Devlys !important; font-size: 13px; font-weight: bold; margin: 0px; padding: 0px; width: 35%;float: left;}
.answers li { list-style: upper-alpha; } 
.feedbackquiz label { margin-left: 0.2em; cursor: pointer; font-size: 20px; font-family: Devlys !important;font-size:} 


#categorylist { margin-top: 6px; display: none; } 
table {border: 1px solid #ccc;border-collapse: collapse; margin: 0; padding: 0; width: 100%;  font-family: Myriad Pro SemiExtended;}
.table-striped tbody>tr:nth-child(odd)>td, .table-striped tbody>tr:nth-child(odd)>th {background-color: #fff;}
table caption {font-size: 1.5em; margin: .5em 0 .75em;}
table tr { background: #f8f8f8; border: 1px solid #ddd; padding: .35em;}
table th,
table td { padding: .625em;text-align: left; font-size:  14px; border: 1px solid #ddd;}
table th {font-size: 17px; font-family: Devlys !important;}
@media screen and (max-width: 600px) {
table { border: 0;}
table caption { font-size: 1.3em;}
table thead { border: none; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}
table tr {border-bottom: 3px solid #ddd; display: block; margin-bottom: .625em;}
table td { border-bottom: 1px solid #ddd;display: block; font-size: .8em; text-align: right;}
table td:before { content: attr(data-label); float: left; font-weight: bold; text-transform: uppercase;}
table td:last-child { border-bottom: 0;}
.quizblog{ width: 70%; float: left; border: 1px solid #000;}
.batch{float: left; width: 20%;}
</style> 

<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			  
            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading" style="height:40px">
					<div  class="col-sm-5"> WCD Scheme Declaration Form </div>
					
				</div>
				
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
					<form name="frmfeedback" id="frmfeedback" class="form-inline" role="form" enctype="multipart/form-data">     

                        
						
		<fieldset style="border: 1px groove #ddd !important;" class="">
			
			<h4 style="text-align:center;">महिला नि:शुल्क कंप्यूटर प्रशिक्षण योजना 2019 के अंतर्गत ज्ञान केंद्र द्वारा घोषणा</h4>
			<p style="padding-left:15px;">
महिला अधिकारिता निदेशालय, जयपुर के आदेशानुसार वर्ष 2019 में प्रस्तावित महिला नि:शुल्क कंप्यूटर प्रशिक्षण योजना के संचालन हेतु आई.टी. ज्ञान केन्द्रों पर सुलभ सुविधाएँ यथा टॉयलेट्स, पानी इत्यादि की व्यवस्थाओं सम्बंधित सूचना मांगी गयी है | अतः प्रशिक्षण में रूचि रखने वाले सभी ज्ञान केन्द्रों को निम्न सूचना RKCL को प्रदान करना अनिवार्य है |</br>
<b>
सूचना RKCL को भेजने की प्रस्तावित अंतिम तिथि :- 17-जून-2019 (सोमवार) </b>

			</p>
		</fieldset>
						  
			<br>
			
		<fieldset style="border: 1px groove #ddd !important;" class="">
			<table class="table-striped">
                 <tbody>
                    <tr>
                     <th scope="col">Ø-la- </th>
                    
                  </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">1 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">क्या आपके ज्ञान केंद्र में विद्यार्थियों हेतु पीने के पानी की उचित व्यवस्था है ? </p></td>
                     <td scope="col"><input type="radio" name="qps1" value="Yes" id="q1y" style='margin-right:10px;'>
						<label for="q1a">YES</label></td>
                     <td scope="col"><input type="radio" name="qps1" value="No" id="q1n" style='margin-right:10px;'>
						<label for="q1b">NO</label></td>
                     
                    </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">2 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">क्या आपके ज्ञान केंद्र के परिसर में विद्यार्थियों हेतु टॉयलेट्स की सुविधा उपलब्ध है?</p></td>
                     <td scope="col"><input type="radio" name="qps2" value="Yes" id="q2y" style='margin-right:10px;'>
						<label for="q1a">YES</label></td>
                     <td scope="col"><input type="radio" name="qps2" value="No" id="q2n" style='margin-right:10px;'>
						<label for="q1b">NO</label></td>
                     
                    </tr>
                 </tbody>
               
                
			</table>
		</fieldset>
			
			<br>
			
		
		
		  <div class="container">
		  
                    <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"
						style="float: left;width: 6%;"/>    
                
				</div>
					</form>
				</div>
			</div>
		</div>
</div>

<!-- Modal -->
<div id="myModalimage" class="modal" style="padding-top:50px !important">
  <div class="modal-content" style="width: 90%;">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h6>Important Information</h6>
    </div>
      <div class="modal-body" style="max-height: 400px; overflow-y: scroll; text-align: center;">
        <p> “कृपया नोट करें की ITGK पर सुलभ सुविधाएँ यथा टॉयलेट्स, पानी इत्यादि की व्यवस्था की भरी गयी इस जानकारी को बाद में बदला नहीं जा सकेगा| क्या आप जानकारी की पुष्टि( Confirm) करते है?” </p>
		
		 <div class="container" style="align:center;">
		 
		 <div class="col-md-2 form-group">
		 </div>
			<div class="col-md-6 form-group" style="margin-left:20px;">
                    <input type="button" name="Submit" id="Submit" class="btn btn-primary" value="Submit"
						/>  
					 <input type="button" name="cancel" id="cancel" class="btn btn-primary" value="Cancel"
						/>    
								
            </div> 
			 
			
				</div>
				<div class="container">
			<div id="response" style="float:left;"></div>
		</div>
    </div>
  </div>
</div>

<?php include ('footer.php'); ?>
<?php include 'common/message.php' ; ?>

<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
			$(document).ready(function () {
				
				
				$("#btnSubmit").click(function () {
					if ($('input[name="qps1"]:checked').length == 0 || $('input[name="qps2"]:checked').length == 0){
						alert("Please select atleast one option");
					}
					else{
						var q1radioValue = $("input[name='qps1']:checked").val();
						var q2radioValue = $("input[name='qps2']:checked").val();
						var modal = document.getElementById('myModalimage');
							var span = document.getElementsByClassName("close")[0];
							modal.style.display = "block";
							span.onclick = function() { 
								modal.style.display = "none";
							}
					}			 
				});
				
				$("#cancel").click(function (){
					$('#response').empty();
					var modal = document.getElementById('myModalimage');
					modal.style.display = "none";
				});
				
				$("#Submit").click(function (){	
					
					var url = "common/cfWcdSchemeDeclaration.php"; // the script where you handle the form input.
					var data;
					var forminput = $("#frmfeedback").serialize();
					data = "action=ADD&" + forminput;
						$.ajax({
							type: "POST",
							url: url,
							data: data,
							success: function (data)
							{
								
							$('#response').empty();
								if (data == 1) {
									$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Something has gone wrong. Please try again." + "</span></p>");
								}
								else if (data == 2) {
									$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please complete all questions." + "</span></p>");
								}
								else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
										window.setTimeout(function () {
											window.location.href = "frmwcdschemedeclaration.php";
										}, 3000);
									}
								else
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
									}
							}
						});
				});
		
});
</script>
    <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
  
</html>
    
<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>




