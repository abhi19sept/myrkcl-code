<?php
$title = "Advance Course Summary";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var UserParentID=" . $_SESSION['User_ParentId'] . "</script>";
    echo "<script>var UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var UserParentID=0</script>";
    echo "<script>var UserRole=0</script>";
}
echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Advance Course Admission Summary</div>
            <div class="panel-body">

                <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">

                                </select>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                              
								</select>
                            </div> 

                        </div>                  

                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillCourse() {            
            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseSummary.php",
                data: "action=FILLAdmissionCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseSummary.php",
                data: "action=FILLAdvanceCourseBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);

                }
            });

        });

        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfAdvanceCourseSummary.php"; // the script where you handle the form input.
            
            var data;            
            var batchvalue = $("#ddlBatch").val();
            data = "action=GETDATA&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    
                    $('#response').empty();

                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }

        function showDataITGK() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfAdvanceCourseSummary.php"; // the script where you handle the form input.
            var role_type = '';

            var startdate = $('#txtstartdate').val();
            var enddate = $('#txtenddate').val();
            var data;
            //alert (role_type);
            data = "action=GETDATAITGK&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "&rolecode=" + UserLoginID + ""; //
            // alert(data);
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }

        $("#btnSubmit").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {

                if (User_UserRole == 7) {
                    showDataITGK();
                }
                else {
                    showData();
                }

            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
