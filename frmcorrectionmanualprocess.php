<?php
$title="Name/Duplicate Correction Form Process";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
			echo "<script>var Cid=" . $_REQUEST['cid'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
			echo "<script>var Cid=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
 ?>

<div style="min-height:430px !important;max-height:auto !important;">
	<div class="container">			 
		<div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">Name/Duplicate Correction Form</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data"> 
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
							  <label for="learnercode">Learner Code:<span class="star">*</span></label>
							  <input type="text" readonly="true" class="form-control" maxlength="50" name="txtLCode" id="txtLCode" placeholder="Learner Code">
							  <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
							</div>
							 
							<div class="col-sm-4 form-group"> 
							  <label for="learnername">Learner Correct Name:<span class="star">*</span></label>
							      <?php if($_REQUEST['Mode'] == 'Reject') { ?>
							  <input type="text" class="form-control" maxlength="50" name="txtLCorrectName" id="txtLCorrectName" placeholder="Learner Correct Name"> 
								 <?php } else {
									?>
								<input type="text" readonly="true" class="form-control" maxlength="50" name="txtLCorrectName" id="txtLCorrectName" placeholder="Learner Correct Name">  <?php } ?>
							     
							</div>
							
						   <div class="col-sm-4 form-group">     
							  <label for="faname">Father Correct Name:<span class="star">*</span></label>
							  <?php if($_REQUEST['Mode'] == 'Reject') { ?>
							  <input type="text" class="form-control" id="txtFCorrectName" name="txtFCorrectName" placeholder="Father Correct Name">
							   <?php } else {
									?>
								 <input type="text" readonly="true" class="form-control" id="txtFCorrectName" name="txtFCorrectName" placeholder="Father Correct Name">
								  <?php } ?>
						   </div>
							
							<div class="col-sm-4 form-group">     
							  <label for="faname">Application For:<span class="star">*</span></label>
							  <input type="text" readonly="true" class="form-control" id="txtapplication" name="txtapplication" placeholder="Application For">
						   </div>
                         </div> 

						<div class="container">						
							<div class="col-sm-4 form-group"> 
							  <label for="exam_event">Exam Event Name:<span class="star">*</span></label>
							  <input type="text" readonly="true" class="form-control" name="EventName" id="EventName" placeholder="Event Name">    
						    </div>  
						   
							<div class="col-sm-4 form-group"> 
							  <label for="event_name">Event Name:</label>
								<input type="hidden" name="txtEventName" id="txtEventName" class="form-control"/> 
								<input type="hidden" name="txtTransId" id="txtTransId" class="form-control"/> 
							    <select name="ddlEventName" id="ddlEventName" class="form-control">
								
								</select>
						   </div>					                         
							
						   <div class="col-sm-4 form-group"> 
                                <label for="status">Status:<span class="star">*</span></label>
                                <select id="ddlstatus" name="ddlstatus" class="form-control" onchange="toggle_visibility1('remark')">								  
                                </select>    
                            </div>							

                            <div class="col-sm-6 form-group"> 
                                <label for="lot">Lot:<span class="star">*</span></label>
                                <select id="ddlLot" name="ddlLot" class="form-control">
								
                                </select>
                            </div>                            						
                        </div>

						<div class="container">
							<div class="col-sm-4 form-group"> 
							  <label for="t_marks">Total Marks:<span class="star">*</span></label>
							  <input type="text" class="form-control" name="txtMarks" id="txtMarks" placeholder="Total Marks">    
						   </div>   
							
							<div class="col-sm-4 form-group" id="remark" style="display:none;"> 
                                <label for="pan">Remark:<span class="star">*</span></label>
                                <input type="text" class="form-control"  name="txtRemark" id="txtRemark"  placeholder="Remark">
                            </div>
							
							<div class="col-sm-4 form-group" > 
							  <label for="lname">Old Learner Name:<span class="star">*</span></label> </br>
							  <input type="text" readonly="true" class="form-control" name="txtoldlearnername" id="txtoldlearnername" placeholder="Learner Name">    
							</div>						
							
							<div class="col-sm-4 form-group" > 
							  <label for="lname">Old Father Name:<span class="star">*</span></label> </br>
							  <input type="text" readonly="true" class="form-control" name="txtoldfaname" id="txtoldfaname" placeholder="Father Name">    
							</div>							
						</div>
  
						<div class="container">
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Correction Photo:<span class="star">*</span></label> </br>
							  
							  <img id="uploadPreview1" src="images/user icon big.png" name="filePhoto1" width="150px" height="150px">								  																	  
							 
							</div>
						
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Correction Certificate:<span class="star">*</span></label> </br>
							  <a title="Correction Certificate" href="#" data-toggle="modal" data-target="#myModal">
							  <img id="uploadPreview2" class="thumbnail img-responsive" src="images/user icon big.png" name="filePhoto2" width="150px" height="150px">								  																	  
							  </a>
							</div>
							
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Correction Marksheet:<span class="star">*</span></label> </br>
							  <a title="Correction Marksheet" href="#" data-toggle="modal" data-target="#myModal">
							  <img id="uploadPreview3" class="thumbnail img-responsive" src="images/user icon big.png" name="filePhoto3" width="150px" height="150px">								  																	  
							  </a>
							</div>
							
							<div class="col-sm-4 form-group" id="provisional" > 
							  <label for="photo">Correction Provisional:<span class="star">*</span></label> </br>
							  <a title="Correction Provisional" href="#" data-toggle="modal" data-target="#myModal">
							  <img id="uploadPreview4" class="thumbnail img-responsive" src="images/user icon big.png" name="filePhoto4" width="150px" height="150px">								  																	  
							   </a>
							</div>
						</div>					
                        
					  <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                       </div>
					   
					   	 <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
						  <div class="modal-content">
							<div class="modal-header">
								<button class="close" type="button" data-dismiss="modal">×</button>
								<h3 class="modal-title">Heading</h3>
							</div>
							<div class="modal-body">
								
							</div>
							<div class="modal-footer">
								<button class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						   </div>
						  </div>
						</div>
					
                </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>
<style>
  .modal-dialog {width:800px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
  	
  <script type="text/javascript">
  
  $(document).ready(function() {
		jQuery(".thumbnail").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>
  
  <script type="text/javascript">
    function toggle_visibility1(id) {
        var e = document.getElementById(id);

        //alert(e);
        var f = document.getElementById('ddlstatus').value;
        //alert(f);
        if (f == "4")
        {
            e.style.display = 'block';
        }        
        else {
            e.style.display = 'none';
        }

    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {        
		
		function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();

        function FillStatus() {
            $.ajax({
                type: "post",
                url: "common/cfCorrectionManualApproved.php",
                data: "action=FILLAPPROVEDSTATUS",
                success: function (data) {
                    $("#ddlstatus").html(data);
                }
            });
        }
        FillStatus();

        function FillLot() {
            $.ajax({
                type: "post",
                url: "common/cfCorrectionManualApproved.php",
                data: "action=GETALLLOT",
                success: function (data) {
                    $("#ddlLot").html(data);
                }
            });
        }
        FillLot();
		
		function FillExamEvent() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FillExamEvent",
                success: function (data) {
                    $("#ddlEventName").html(data);
                }
            });
        }
        FillExamEvent();
		
		$("#ddlEventName").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FillEventById&values=" + ddlEventName.value + "",
                success: function (data) {
                    txtEventName.value = data;	
                }
            });
        });

		if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
				//alert(1);
                fillForm();
				fillOldLearnerDetails();
            }

			else if (Mode == 'Reject')
            {
				//alert(1);
                fillForm();
				fillOldLearnerDetails();
            }	
			
		function fillForm()
            {           //alert(Code);    
				$.ajax({
                    type: "post",
                    url: "common/cfCorrectionManualApproved.php",
                    data: "action=EDIT&values=" + Cid + "",
                    success: function (data) {	
						//alert(data);
                        data = $.parseJSON(data);
						txtLCode.value = data[0].LearnerCode;
                        txtLCorrectName.value = data[0].lname;
                        txtFCorrectName.value = data[0].fname;
                        txtTransId.value = data[0].txnid;
                        txtapplication.value = data[0].applicationfor;	
						EventName.value = data[0].exameventname;	
						txtMarks.value = data[0].marks;	
						$("#uploadPreview1").attr('src',"upload/correction_photo/" + data[0].photo);
						$("#uploadPreview2").attr('src',"upload/correction_certificate/" + data[0].certificate);
						$("#uploadPreview3").attr('src',"upload/correction_marksheet/" + data[0].marksheet);
						  var txtapp = document.getElementById("txtapplication").value;
						  if( txtapp == "Correction Certificate" ){
							  $("#provisional").hide();
						  }
						  else{							  
							 $("#uploadPreview4").attr('src',"upload/correction_provisional/" + data[0].provisional); 
						  }
						
					}
                });
            }
			
			function fillOldLearnerDetails()
            {           //alert(Code);    
				$.ajax({
                    type: "post",
                    url: "common/cfCorrectionManualApproved.php",
                    data: "action=GetOldValues&values=" + Code + "",
                    success: function (data) {	
						//alert(data);
                        data = $.parseJSON(data);
						txtoldlearnername.value = data[0].lname;
                        txtoldfaname.value = data[0].fname;                        
					}
                });
            }
		
        $("#btnSubmit").click(function () {	
		  if ($("#form").valid())
           {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfCorrectionManualApproved.php"; // the script where you handle the form input.            
            var data;			
            if (Mode == 'Add')
            {
            
			}
			else if (Mode == 'Reject'){
				 data = "action=UpdateRejectedLearner&cid=" + Cid + "&status=" + ddlstatus.value + "&lot=" + ddlLot.value + "&remark=" + txtRemark.value + "&eventname=" + txtEventName.value + "&marks=" + txtMarks.value + "&lname=" + txtLCorrectName.value + "&fname=" + txtFCorrectName.value + ""; // serializes the form's elements.
			}
            else
            {
                data = "action=UPDATE&cid=" + Cid + "&status=" + ddlstatus.value + "&lot=" + ddlLot.value + "&remark=" + txtRemark.value + "&eventname=" + txtEventName.value + "&marks=" + txtMarks.value + "&txnid=" + txtTransId.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmcorrectionmanualapproval.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });
			}
			return false;
		  });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcorrectionprocess_validation.js"></script>
</html>