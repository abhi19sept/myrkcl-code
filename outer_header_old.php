<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<title> RKCL - Administration</title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">		
		<meta name="description" content="Blueprint: Horizontal Drop-Down Menu" />
		<meta name="keywords" content="horizontal menu, microsoft menu, drop-down menu, mega menu, javascript, jquery, simple menu" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" href="bootcss/css/bootstrap.min.css">	
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="bootcss/js/bootstrap.min.js"></script>	
		<script src="rkcltheme/js/jquery.validate.min.js"></script>	
		<link rel="stylesheet" href="assets/header-search.css">
		<link rel="stylesheet" href="assets/footer-distributed-with-address-and-phones.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	</head>	
	<body> 
		  <header>
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<h1><a href="#"><img src="images/rkcl-logo.jpg" style="width:225px; height:100px;"></a></h1>	
					</div>					     
				</div>
			</div>		
		</header>