<?php
    $title = "Bio-Matric";
    include('header.php');
    include('root_menu.php');
    if (isset($_REQUEST['code'])) {
        echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
        echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    } else {
        echo "<script>var UserCode=0</script>";
        echo "<script>var Mode='Add'</script>";
    }
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Enter Learner Four digit Boimetric PIN and Submit</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmLearnerCapture" id="frmLearnerCapture" class="form-inline" action="" role="form"
                      enctype="multipart/form-data">

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>
                        <div id="errorBox"></div>

                    </div>

                    <div class="container">
                        <div class="container" id="SelectedDetails" style="display:none;">
                            <div id="response"></div>
                            <div class="col-md-4 form-group">
                                <img id="imgFinger" name="imgFinger" class="form-control"
                                     style="width:100px;height:100px;" alt=""/>
                            </div>

                        </div>

                        <div id="success" style="margin-left:250px !important;"></div>

                        <div id="errorBox"></div>

                    </div>


                    <div style="display:none;">
                        <div class="container">
                            <div class="col-md-4 form-group">
                                <label for="batch"> Serial No:<span class="star">*</span></label>
                                <input type="text" name="tdSerial" id="tdSerial" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Make:<span class="star">*</span></label>
                                <input type="text" name="tdMake" id="tdMake" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Model:<span class="star">*</span></label>
                                <input type="text" name="tdModel" id="tdModel" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Width:<span class="star">*</span></label>
                                <input type="text" name="tdWidth" id="tdWidth" class="form-control" readonly="true"/>
                            </div>
                        </div>

                        <div class="container">
                            <div class="col-md-4 form-group">
                                <label for="batch"> Height:<span class="star">*</span></label>
                                <input type="text" name="tdHeight" id="tdHeight" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Local MAC:<span class="star">*</span></label>
                                <input type="text" name="tdLocalMac" id="tdLocalMac" class="form-control"
                                       readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Local IP:<span class="star">*</span></label>
                                <input type="text" name="tdLocalIP" id="tdLocalIP" class="form-control"
                                       readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Status:<span class="star">*</span></label>
                                <input type="text" name="txtStatus" id="txtStatus" class="form-control"
                                       readonly="true"/>
                            </div>
                        </div>

                    </div>

                    <div id="errorBox" style="display:block;">
                        <div class="container">

                            <div class="col-md-3">
                                <label for="deviceType">Select Device Make<span class="star">*</span></label>
                                <select id="deviceType" name="deviceType" class="form-control"
                                        onchange="javascript:callAPI_initialize(this.value)">
                                    <option value="n">- - - Please Select - - -</option>
                                </select>
                            </div>

                            <div class="col-md-3 ">
                                <label for="biopin">Learner Boimetric PIN:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtBioPin" id="txtBioPin"
                                       placeholder="Biometric Pin">

                            </div>
                            <div class="col-sm-3 ">
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary"
                                       value="Submit" style="margin-top:25px"/>
                            </div>
                            <div class="col-md-3 ">
                            </div>
                        </div>
                    </div>
                    <div style="display:none;">
                        <div class="container">
                            <div class="col-md-4 form-group">
                                <label for="batch"> Quality:<span class="star">*</span></label>
                                <input type="text" name="txtQuality" id="txtQuality" value="" class="form-control"
                                       readonly="true"/>
                                <input type="hidden" name="admission_code" id="admission_code"
                                       value="<?php echo $_REQUEST['code']; ?>">
                                <input type="hidden" name="txtLearnerName" id="txtLearnerName"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> NFIQ:<span class="star">*</span></label>
                                <input type="text" name="txtNFIQ" id="txtNFIQ" value="" class="form-control"
                                       readonly="true"/>
                            </div>
                        </div>

                        <div class="container">
                            <div class="col-md-10 ">
                                <label for="batch"> Base64Encoded ISO Template:<span class="star">*</span></label>
                                <textarea id="txtIsoTemplate" rows="4" cols="12" name="txtIsoTemplate"
                                          class="form-control" readonly="true"></textarea>
                            </div>
                        </div>

                        <div class="container">
                            <div class="col-md-10">
                                <label for="batch"> Base64Encoded ISO Image:<span class="star">*</span></label>
                                <textarea id="txtIsoImage" rows="4" cols="12" name="txtIsoImage" class="form-control"
                                          readonly="true"></textarea>
                            </div>
                        </div>

                        <div class="container">
                            <div class="col-md-10">
                                <label for="batch"> Base64Encoded Raw Data:<span class="star">*</span></label>
                                <textarea id="txtRawData" rows="4" cols="12" name="txtRawData" class="form-control"
                                          readonly="true"></textarea>
                            </div>
                        </div>

                        <div class="container">
                            <div class="col-md-10">
                                <label for="batch"> Base64Encoded Wsq Image Data:<span class="star">*</span></label>
                                <textarea id="txtWsqData" rows="4" cols="12" name="txtWsqData" class="form-control"
                                          readonly="true"></textarea>
                            </div>
                        </div>
                        <textarea id="txtlearnercode" rows="4" cols="12" name="txtlearnercode" class="form-control"
                                  readonly="true"></textarea>

                    </div>
                    <div id="learnerinfo" class="container" style="display:none;">
                        <br>
                        <div align="center" class="col-md-2 col-lg-2">
                            <img id="my_image" style="width:80px; height:107px" class="img-circle img-responsive"/>
                        </div>
                        <div class="table-responsive col-md-8 col-lg-8">
                            <table class="table table-bordered table-user-information">
                                <tbody>
                                <tr>
                                    <td>Learner Code:</td>
                                    <td id="learnercode"></td>
                                </tr>
                                <tr>
                                    <td>Learner Name:</td>
                                    <td id="learnername"></td>
                                </tr>
                                <tr>
                                    <td>Father Name</td>
                                    <td id="fathername"></td>
                                </tr>
                                <tr>
                                    <td>Date of Birth:</td>
                                    <td id="dob"></td>
                                </tr>
                                <tr>
                                    <td>Course Name</td>
                                    <td id="coursename"></td>
                                </tr>
                                <tr>
                                    <td>Batch Name</td>
                                    <td id="batchname"></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div class="container" style="margin-top:20px; display: none;" id="btnCapture">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <!--<label for="deviceType">Select Device Make to Capture<span class="star">*</span></label>
                            <select id="deviceType" name="deviceType" class="form-control"
                                    onchange="javascript:callAPI(this.value)">
                                <option value="n">- - - Please Select - - -</option>
                            </select>-->

                            <input type="button" name="capture1" id="capture1" class="btn btn-primary"
                                   value="Capture Attendance"
                                   onclick="javascript:callAPI('<?php echo $_SESSION["machineID"]; ?>');">

                        </div>
                        <div class="col-md-4"></div>
                        <!--<input type="button" name="btnCapture" id="btnCapture" class="btn btn-primary"
                               style="display:none;" value="Capture Attendance"/>-->
                    </div>

                    <div class="container" style="margin-top:20px; display: none;" id="showMessage">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <p id="showMessage_ajax"></p>
                        </div>
                        <div class="col-md-4"></div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
<?php
    include('footer.php');
    include 'common/message.php';
    include 'common/modals.php';
    $mac = explode(" ", exec('getmac'));
?>
</body>
<script src="js/mfs100.js" type="text/javascript"></script>
<script src="js/star.js" type="text/javascript"></script>
<script src="js/cogent.js" type="text/javascript"></script>
<script src="js/tatvik.js" type="text/javascript"></script>

<script type="text/javascript">
 $(document).ready(function () {
 var CaptureEvent="0";
//    function CheckCaptureEvent() {
//        $.ajax({
 //           type: "post",
 //           url: "common/cfbiomatricenroll.php",
  //          data: "action=CheckCaptureEvent&biopin=" + txtBioPin.value + "",
 //           success: function (data) {
  //              if(data=="1"){
//					var CaptureEvent=1;
//					alert("Enable");
//				}
//				else{
//					var CaptureEvent=0;
//					alert("Disable");
//					}
  //          }
  //      });
 //   }

    
});

    var localIP = "<?php echo getHostByName(getHostName());?>";
    var localMAC = "<?php echo str_ireplace('-', '', $mac[0]);?>";
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    var quality = 60; //(1 to 100) (recommanded minimum 55)
    var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

    function GetInfo() {
        //alert("hii");
        document.getElementById('tdSerial').value = "";
        document.getElementById('tdMake').value = "";
        document.getElementById('tdModel').value = "";
        document.getElementById('tdWidth').value = "";
        document.getElementById('tdHeight').value = "";
        document.getElementById('tdLocalMac').value = "";
        document.getElementById('tdLocalIP').value = "";

        var res = GetMFS100Info();
        if (res.httpStaus) {

            document.getElementById('txtStatus').value = res.data.ErrorDescription;

            if (res.data.ErrorCode == "0") {
                document.getElementById('tdSerial').value = res.data.DeviceInfo.SerialNo;
                document.getElementById('tdMake').value = res.data.DeviceInfo.Make;
                document.getElementById('tdModel').value = res.data.DeviceInfo.Model;
                document.getElementById('tdWidth').value = res.data.DeviceInfo.Width;
                document.getElementById('tdHeight').value = res.data.DeviceInfo.Height;
                document.getElementById('tdLocalMac').value = res.data.DeviceInfo.LocalMac;
                document.getElementById('tdLocalIP').value = res.data.DeviceInfo.LocalIP;
            }
        }
        else {
            alert(res.err);
        }
        return false;
    }

    //GetInfo();

    $("#btnSubmit").click(function () {
	if($("#deviceType").val() == 's'){
        if($("#tdSerial").val() == ''){
            initializeStartek();
        }
        
       
    }
	
	//CheckCaptureEvent();
	
        $("#showMessage").html();
        $("#showMessage").hide();
        if ($("#deviceType").val() == "n") {
        //    $("#errorText").html("Please select biometric device");
         //   $("#errorModal_Custom").modal("show");
        } else {
            //alert("hhkj");
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
            var data;


            data = "action=GetLearnerByPIN&biopin=" + txtBioPin.value + "";
            //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data == 'invalidpin') {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + " Invalid Bio Metric PIN " + "</span></font></b></p>");
                    }
                    else {
                        //alert(data);
                        data = $.parseJSON(data);

                        //admission_code.value = data[0].AdmissionCode;
                        txtLearnerName.value = data[0].lname;
                        txtQuality.value = data[0].quality;
                        txtNFIQ.value = data[0].nfiq;
                        txtIsoTemplate.value = data[0].template;
                        txtIsoImage.value = data[0].image;
                        txtRawData.value = data[0].raw;
                        txtWsqData.value = data[0].wsq;
                        txtlearnercode.value = data[0].lcode;
                        document.getElementById("learnercode").innerHTML = data[0].lcode;
                        document.getElementById("learnername").innerHTML = data[0].lname;
                        document.getElementById("fathername").innerHTML = data[0].lfname;
                        document.getElementById("dob").innerHTML = data[0].ldob;
                        document.getElementById("coursename").innerHTML = data[0].lcourse;
                        document.getElementById("batchname").innerHTML = data[0].lbatch;
                        var imsrc = data[0].lphoto;
                        //src="upload/admission_photo/ "
                        $("#my_image").attr("src", "upload/admission_photo/" + imsrc);
                        //learnercode.value = data[0].AdmissionCode;
                        $('#response').empty();
                        $("#learnerinfo").show();

                        if (data[0].attendance_flag == 0) {
                            $("#btnCapture").show();
                        } else {
                            $("#btnCapture").hide();
                            $("#showMessage_ajax").html(data[0].attendance_flag);
                            $("#showMessage").show();
                        }
                    }


                }
            });
        }

        return false; // avoid to execute the actual submit of the form.
    });

    function Verify() {
        var quality = 60; //(1 to 100) (recommanded minimum 55)
        var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

        //alert("verify");
        try {
            var isotemplate = document.getElementById('txtIsoTemplate').value;
            alert(isotemplate);
            var res = VerifyFinger(isotemplate, isotemplate);
            alert(res);
            alert(res.httpStaus);
            if (res.httpStaus) {
                if (res.data.Status) {
                    alert("Finger matched");
                }
                else {
                    if (res.data.ErrorCode != "0") {
                        alert(res.data.ErrorDescription);
                    }
                    else {
                        alert("Finger not matched");
                    }
                }
            }
            else {
                alert(res.err);
            }
        }
        catch (e) {
            alert(e);
        }
        return false;
    }

    function SubmitDetails() {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
        var data;
        //alert("hi");
        if (Mode == 'In') {
            data = "action=In&txtQuality=" + txtQuality.value + "&txtNFIQ=" + txtNFIQ.value + "&learnercode=" + txtlearnercode.value + "";
        }
        else {
            data = "action=In&txtQuality=" + txtQuality.value + "&txtNFIQ=" + txtNFIQ.value + "&learnercode=" + txtlearnercode.value + "";
        }
        // alert(data);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
                //alert(data);
                if (data == "Successfully Inserted" || data == "Successfully Updated") {
                    $('#response').empty();
                    // $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                    Mode = "Add";
                    window.setTimeout(function () {
                        $("#btnCapture").hide();
                        $("#learnerinfo").hide();
                        window.location.reload();
                    }, 1000);

                    //resetForm("frmLearnerCapture");
                }
                else {
                    alert("Some error occurred in capturing attendance, please try again");
                    window.setTimeout(function () {
                        $('#response').empty();
                        $("#btnCapture").hide();
                        $("#learnerinfo").hide();
                        window.location.reload();
                    }, 1000);

                    // $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                }
                //showData();
            }
        });
        return false; // avoid to execute the actual submit of the form.
    }

    function FillMachines() {
        $.ajax({
            type: "post",
            url: "common/cfbiomatricenroll.php",
            data: "action=FillMachines",
            success: function (data) {
                $("#deviceType").html(data);
            }
        });
    }

    FillMachines();

    /*** MATCH ON MANTRA MACHINE ***/
    function Match() {
        var quality = 60; //(1 to 100) (recommanded minimum 55)
        var timeout = 10;
        try {
            var isotemplate = document.getElementById('txtIsoTemplate').value;
            $("#wait_modal_match").modal("show");
            var res = MatchFinger(quality, timeout, isotemplate);
            var res1 = CaptureFinger(quality, timeout);
            $("#wait_modal_match").modal("hide");

            if (res.httpStaus) {
                document.getElementById('txtStatus').value = "ErrorCode: " + res1.data.ErrorCode + " ErrorDescription: " + res1.data.ErrorDescription;

                if (res.data.Status) {
                    document.getElementById('imgFinger').src = "data:image/bmp;base64," + res1.data.BitmapData;
                    //alert("Finger matched");
                    $('#success').empty();
                    $('#success').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "     Attendance Successfully registered for Learner: " + txtLearnerName.value + "</span></font></b></p>");
                    SubmitDetails();

                } else {

                    if (res.data.ErrorCode != "0") {
                        //alert("err: "+res.data.ErrorDescription);
                        $("#error_modal_timeout").modal("show");
                    }
                    else {
                        $("#error_modal_nomatch").modal("show");
                        $('#success').empty();
                        $('#success').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "     Fingerprint does not match. Please try again." + "</span></font></b></p>");
                        window.setTimeout(function () {
                            $('#response').empty();
                            $("#btnCapture").hide();
                            $("#learnerinfo").hide();
                        }, 3000);
                    }
                }
            }
            else {
                alert(res.err);
            }
        }
        catch (e) {
            alert(e);
        }
        return false;
    }


    /*** MATCH ON STARTEK MACHINE ***/
    function MatchFP_new() {
        crossDomainAjax_capture("https://localhost:4443/FM220/GetMatchResult?MatchTmpl=" + encodeURIComponent(document.getElementById('txtIsoTemplate').value.toString()) + "&callback=?",
            function (result) {
                if (result.errorCode == 0) {

                    document.getElementById('imgFinger').src = "data:image/bmp;base64," + result.bMPBase64;
                    $('#success').empty();
                    $('#success').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "     Attendance Successfully registered for Learner: " + txtLearnerName.value + "</span></font></b></p>");
                    SubmitDetails();
                }
                else {

                    if (result.status == "Finger not placed. Time Out!" || result.status == "Capture Failed! Time Out!")
                        $("#error_modal_timeout").modal("show");

                    if (result.status == "Match Failed!")
                        $("#error_modal_nomatch").modal("show");
                }
            });
    }

      /*** MATCH ON Tatvik MACHINE ***/
        function MatchTatvik_new() {

            crossDomainAjax("https://127.0.0.1:31000", 'CAPTUREFP',
                    function (result) {
                        var ClaimedTemplateData = result.CaptureResult.Image_FMRbytes;

                        var matchtemplates = "<MatchTemplates>" +
                                "<ClaimedTemplateData>" + ClaimedTemplateData + "</ClaimedTemplateData>" +
                                "<ReferenceTemplateData>" + document.getElementById('txtIsoTemplate').value.toString() + "</ReferenceTemplateData>" +
                                "</MatchTemplates>";
                        $.ajax({
                            url: "https://127.0.0.1:31000",

                            data: matchtemplates,
                            method: "MATCHFPTEMPLATE",
                            responsetype: 'xml',
                            // jsonpCallback: "Jsonp_Callback",
                            success: function (data, success) {
                                var rd = xml2json(data);
                                var rd2 = rd.MatchResult;
                                console.log(rd2)
                                if (rd2.MatchStatus == 'Match Failed') {
                                   // alert("no");
                                    $("#error_modal_nomatch").modal("show");
                                    $('#success').empty();
                                    $('#success').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "     Fingerprint does not match. Please try again." + "</span></font></b></p>");
                                    window.setTimeout(function () {
                                        $('#response').empty();
                                        $("#btnCapture").hide();
                                        $("#learnerinfo").hide();
                                    }, 3000);
                                } else {
//alert(rd2.MatchStatus);
                                    if (rd2.MatchStatus == "Finger not placed. Time Out!" || rd2.MatchStatus == "Capture Failed! Time Out!")
                                         $("#error_modal_timeout").modal("show");

                                        if (rd2.MatchStatus == "Match succesful") {

                                            /*** ALREADY ENROLLED ****/
                                            $('#success').empty();
                                            $('#success').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "     Attendance Successfully registered for Learner: " + txtLearnerName.value + "</span></font></b></p>");

                                            SubmitDetails();
                                        }
                                }
                            },
                            error: function (jqXHR, exception) {
                                if (jqXHR.status === 0) {
                                    alert('Not connect.\n Verify Network.');
                                } else if (jqXHR.status == 404) {
                                    alert('Requested page not found. [404]');
                                } else if (jqXHR.status == 500) {
                                    alert('Internal Server Error [500].');
                                } else if (exception === 'parsererror') {
                                    alert('Requested JSON parse failed.');
                                } else if (exception === 'timeout') {
                                    alert('Time out error.');
                                } else if (exception === 'abort') {
                                    alert('Ajax request aborted.');
                                } else {
                                    alert('Uncaught Error.\n' + jqXHR.responseText);
                                }
                            }
                        });

                    });

        }

    /*** MATCH ON COGENT MACHINE ***/
    function MatchCogent_new() {

        var thresholdScore = 5000;

        /*** MACHINE INITIALIZATION STARTS CODE HERE ***/

        var strMessage = "Initialize";
        $.ajax({
            type: "GET",
            url: "https://localhost:62016/MMMCSD200Service/ActionServlet?action=" + strMessage,
            dataType: 'jsonp',
            jsonp: 'callback',
            jsonpCallback: 'myCallback',
            success: function (jsonObj) {
                //console.log(jsonObj.Message);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });


        /*** MACHINE INITIALIZATION CODE ENDS HERE ***/




        /*** CAPTURE USER FINGERPRINT STARTS HERE ***/

        var isoTemplate = null;
        $("#wait_modal_match").modal("show");
        $.ajax({
            url: "https://localhost:62016/MMMCSD200Service/ActionServlet?action=Capture&image=1",
            dataType: 'jsonp',
            crossDomain: true,
            jsonp: 'callback',
            jsonpCallback: 'myCallback',
            success: function (jsonObj1) {

                if(jsonObj1.Message === "Error: Capture Timeout."){
                    $("#wait_modal_match").modal("hide");
                    $("#errorText").html('Timeout, Please try again');
                    $("#errorModal_Custom").modal('show');
                    window.parent.opener.location.reload();
                    window.setTimeout("window.close();", 3000);
                } else {

                    isoTemplate = jsonObj1.byteIsoImage;
                    $("#wait_modal_match").modal("hide");
                    /*** MATCHING MECHANISM STARTS HERE ***/

                    $.ajax({
                        type: "GET",
                        url: "https://localhost:62016/MMMCSD200Service/ActionServlet?action=MatchFingers&byteImage1=" + isoTemplate + "&byteImage2=" + document.getElementById('txtIsoTemplate').value.toString(),
                        dataType: "jsonp",
                        crossDomain:true,
                        crossOrigin:true,
                        jsonpCallback: 'jsonpCallback',
                        header:{'Access-Control-Allow-Origin': '*'},
                        success: function (jsonObj2) {
                            //console.log(jsonObj2.Message);

                            if (jsonObj2.Message !== "Unable to match the fingers. One of the templates is NULL.") {
                                if (jsonObj2.Message === "-21"){
                                    $("#error_modal_nomatch").modal("show");
                                } else {
                                    if (jsonObj2.Message >= thresholdScore) {
                                        document.getElementById('imgFinger').src = "data:image/bmp;base64," + jsonObj2.byteImage;
                                        $('#success').empty();
                                        $('#success').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "     Attendance Successfully registered for Learner: " + txtLearnerName.value + "</span></font></b></p>");
                                        SubmitDetails();
                                    }
                                    else {
                                        $("#error_modal_nomatch").modal("show");
                                    }
                                }
                            } else {
                                $("#errorText").html('Timeout, Please try again');
                                $("#errorModal_Custom").modal('show');
                            }


                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // alert("error :::::::  "+errorThrown);
                        }
                    });

                    /*** MATCHING MECHANISM ENDS HERE ***/
                }


            },
            error: function (xhr, status, error) {
                $("#wait_modal_match").modal("hide");
                $("#error_modal_timeout").modal("show");
                $("#showData").css("display","hide");
            }
        });

        /*** CAPTURE USER FINGERPRINT ENDS HERE ***/



    }

    function callAPI_initialize(x) {
        if (x != "n") {
            switch (x) {
                case "m":
                    $.ajax({
                        type: "post",
                        url: "common/cfbiomatricenroll.php",
                        data: "action=setMachine&value=" + x + "",
                        success: function (data) {
						
                            $("#capture1").attr("onclick", "callAPI('m')");
                            $("#machine_set").modal("show");
                        }
                    });
                    break;

                case "s":
                   initializeStartek();
                    $.ajax({
                        type: "post",
                        url: "common/cfbiomatricenroll.php",
                        data: "action=setMachine&value=" + x + "",
                        success: function (data) {
                            $("#capture1").attr("onclick", "callAPI('s')");
                            $("#machine_set").modal("show");
                        }
                    });
                    break;
                case "t":     
                    $.ajax({
                        type: "post",
                        url: "common/cfbiomatricenroll.php",
                        data: "action=setMachine&value=" + x + "",
                        success: function (data) {
                            $("#capture1").attr("onclick", "callAPI('t')");
                            $("#machine_set").modal("show");
                        }
                    });
                    break;
                case "c":
                    $.ajax({
                        type: "post",
                        url: "common/cfbiomatricenroll.php",
                        data: "action=setMachine&value=" + x + "",
                        success: function (data) {
                            $("#capture1").attr("onclick", "callAPI('c')");
                            $("#machine_set").modal("show");
                        }
                    });
                    break;

                default:
                    break;
            }
        }
    }
	
	function CheckCaptureEvent() {
	
        $.ajax({
            type: "post",
            url: "common/cfbiomatricenroll.php",
            data: "action=CheckCaptureEvent&biopin=" + txtBioPin.value + "",
            success: function (data) {
			//alert(data);
                if(data=="1"){
					 CaptureEvent='1';
					//alert("Enable");
				}
				else{
					 CaptureEvent='0';
					//alert("Disable");
					}
            }
        });
		
    }
var connectmachinesrno = "";
function GetContMachineSrNo(x) {
    
      if (x !== "n") {
        switch (x) {
            case "m":
               var res = GetMFS100Info();
        if (res.httpStaus) {

            if (res.data.ErrorCode == "0") {
               connectmachinesrno = res.data.DeviceInfo.SerialNo;
            }
        }
            break;

        case "s":

          connectmachinesrno =  $("#tdSerial").val(); 
            break;

        case "c":
            var res = getCogentDeviceInfo();
            connectmachinesrno =  res.data.refine1[1];
            break;
        case "t":                                              
         var res = initializeTatvik();                                    connectmachinesrno =   $("#tdSerial").val();  
         break;
        default:
            break;
                                }
                            }  
        
    }
    
    var regmachinesrno = "";
    var matching = "";
    function GetRegMachineSrNo(x) {

            $.ajax({
            type: "post",
            url: "common/cfbiomatricenroll.php",
            data: "action=GetRegMachineSrNo",
            success: function (data) {
            var obj = $.parseJSON(data);
            var len = obj.length;
            for(var i=0; i<len; i++){
                var make1 = obj[i].make;
                regmachinesrno = obj[i].srno;
               //alert(regmachinesrno);
               //alert(connectmachinesrno);
               if(connectmachinesrno == regmachinesrno){
                    matching = "1";
                    // alert(matching);
                         $.ajax({
                            type: "post",
                            url: "common/cfbiomatricenroll.php",
                            data: "action=CheckCaptureEvent&biopin=" + txtBioPin.value + "",
                            success: function (data) {
                            //alert(data);
                            
                            
                                if(data=="1"){
                                
                                   
                                            if (x !== "n") {
                                                switch (x) {
                                                    case "m":
                                                        Match();
                                                        break;

                                                    case "s":
                                                        MatchFP_new();
                                                        break;

                                                    case "c":
                                                    getCogentDeviceInfo();
                                                        MatchCogent_new();
                                                        break;
                                                    case "t":                                                                               MatchTatvik_new();                                                                              break;
                                                    default:
                                                        break;
                                                }
                                            } else {
                                                $("#showData").css("display", "none");
                                            }
                                    
                                }
                                else{
                                     
                                    alert("Learner attendance is Not Enabled for this Course and Batch");
                                    }
                            }
                        });
                    break;
                   }
                   else{
                     matching = "0";
                                       }
                       

                }
                if(matching == "0"){
                    alert("Sorry Connected Biometric Machine is Not Registered at Your IT-GK, Please Register Biometric Machine first");
                } 

            }
        }); 
       
        return matching;
    }
    function callAPI(x) {
        GetContMachineSrNo(x);
            
        var matching1 = GetRegMachineSrNo(x);
          
    }
</script>
</html>