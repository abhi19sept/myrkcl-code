<?php
$title="Terms and Conditions";
include ('header.php'); 
include ('root_menu.php'); 

    if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Accept'</script>";
            }
if (isset($_SESSION['User_UserRoll'])) {	
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div class="container"> 

    <div class="panel panel-primary" style="margin-top:36px !important;">  
        <div class="panel-heading">Terms and Conditions</div>
        <div class="panel-body">

            <form name="frmpolicy" id="frmpolicy" class="form-inline" >
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>
                    <div class="container" style="float:left;width:98%;">
                            <span id="txtmessage"></span>
                    </div>
                    <p>&nbsp;</p>
                    <div class="container chkaccept"> 
                                <div class="col-md-1" style="width:20px !important; float:left;clear:both;"> 
                                <label class="checkbox-inline" style="float:left;"> <input type="checkbox" name="chk" id="chk" value="1" >
                                    
                                </label>
                                </div>
                            <div class="col-md-11">     
                                                            
                                <label for="learnercode" style="float:left;margin:left:15px" ><b>I accept terms and condition.</b><span class="star">*</span></label></br>                              
                                
                            </div>
                            <input type="hidden" name="policyid" id="policyid">
                        </div>
                    <br>
                    <div class="container forsubmit">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="margin-left:15px; "/>    

                    </div>
                </div>   
            </form>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>


<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showData() {
            $('#response').empty();
            $.ajax({
                type: "post",
                url: "common/cfpolicy.php",
                data: "action=GETPOLICY",
                success: function (data) {
                    if (data != 0) {
                        data = $.parseJSON(data);
                        $("#txtmessage").html(data.Message);
                        $("#policyid").val(data.id);
                        if (data.acceptedId != 0) {
                            $('.forsubmit').html('');
                            $('.chkaccept').html("<p class='success'><span><img src=images/correct.gif width=10px /></span><span>Accepted!!</span></p>");
                        }
                    } else {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Unable to get content, for this page.</span></p>");
                    }
                }
            });
        }

        showData();


     $("#btnSubmit").click(function () {
				 
				// debugger;
			 if ($("#frmpolicy").valid())
                {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfpolicy.php"; // the script where you handle the form input.
               var data;
                var forminput=$("#frmpolicy").serialize();
                data = "action=Accept&" + forminput; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data1)					
                    {
						//debugger;
					//alert(SuccessfullyInsert + '=' + data);
                        if (data1 == SuccessfullyInsert)
                        {
							
                            $('#response').empty();
                            $('#response').append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span>" + data1 + "</span></p>");
                            window.location.href = "frmpolicyview.php";
                            Mode = "Accept";
                            resetForm("frmsurvey");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img  width=10px /></span><span>" + data1 + "</span></p>");
                            window.location.href = "frmpolicyview.php";
                            Mode = "Accept";
                            resetForm("frmpolicy");
                        }
                        //showData();


                    }
                });
		   }
                return false; // avoid to execute the actual submit of the form.
            });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });
</script>

	<style>
.error {
	color: #D95C5C!important;
}
</style>


<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmpolicy_validation.js"></script>
</html>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>