<?php

$title = "UPLOAD ONLINE CLASS URL";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var UserParentID=" . $_SESSION['User_ParentId'] . "</script>";
    echo "<script>var UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var UserParentID=0</script>";
    echo "<script>var UserRole=0</script>";
}
echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
?>
    <link rel="stylesheet" href="bootcss/css/bootstrap-datetimepicker.min.css">
    <script src="bootcss/js/moment.min.js"></script>
    <script src="bootcss/js/bootstrap-datetimepicker.min.js"></script>


<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">UPLOAD ONLINE CLASS URL</div>
            <div class="panel-body">

                <form name="frmuploadonlineclassurl" id="frmuploadonlineclassurl" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                            </div>
                            <div class="col-md-4 form-group"> 
                                <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                                onchange="try{setCustomValidity('')}catch(e){}">

                                </select>
                            </div> 
                        </div>
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                            </div>
                            <div class="col-md-4 form-group"> 
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                    <!--                                <option value="0">All Batch</option>-->
                                </select>
                            </div> 

                        </div>
                        <div class="container">
                            <div class="col-sm-6 form-group"> 
                                <label for="sdate">Select Slot Date and Time:<span class="star">*</span></label>

                            </div> 
                            <div class="form-group col-sm-10">
                                <input type="text" class="form-control" id="txtstartdate" name="txtstartdate" />
                            </div>
                        </div>
                        <div class="container">
                            <div class="col-sm-6 form-group"> 
                                <label for="sdate">Enter Class MS-SharePoint URL:<span class="star">*</span></label>

                            </div> 
                            <div class="form-group col-sm-6">
                                <textarea class="form-control" cols="40" id="txtclassurl" name="txtclassurl"  rows="5" required="required" oninvalid="setCustomValidity('Please Enter Class URL')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"></textarea>
                               
                            </div>
                        </div>

                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>


<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">


    $('#txtstartdate').datetimepicker({

        sideBySide: true,
        format: 'YYYY-MM-DD HH:mm',
        widgetPositioning: {horizontal: "auto", vertical: "bottom"}
    });

</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        showDataITGK();
        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfUploadOnlineClassurl.php",
                data: "action=FILLCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfUploadOnlineClassurl.php",
                data: "action=FILLBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });
        $('#txtstartdate').on('dp.change', function(e)
            { 
                var fullDate = new Date(Date.now())
                 
                //convert month to 2 digits
                var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
                 
                var currentDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate() + " " + (fullDate.getHours()<10?'0':'') + fullDate.getHours() + ":" + (fullDate.getMinutes()<10?'0':'') + fullDate.getMinutes();

                var txtstartdate = document.getElementById("txtstartdate").value;
                if(txtstartdate != currentDate){
                   // alert(txtstartdate);
                    // chkslottime(txtstartdate); 
                }
            });
        function chkslottime(startdate) {

            
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfUploadOnlineClassurl.php"; // the script where 

            var data;
            data = "action=CHKSLOTTIME&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "&slottime=" + startdate + ""; 
            //alert(data);
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    // alert(data);
                    if(data == 1){
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>You Have Already Entered Class Details in this Time Slot</span></div>");
                    }
                    else{
                        $('#response').empty();
                        return true;
                    }
                   

                }
            });
            
                // document.getElementById("txtenddate").value = "";
            
        }


        function showDataITGK() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfUploadOnlineClassurl.php"; // the script where 

            var data;
            data = "action=GETDATAITGK&course=" + ddlCourse.value + "&batch=" + ddlBatch.value  + ""; //
            // alert(data);
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }

        function isUrl(str) {
          regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
          return regexp.test(str);

        }
        $("#btnSubmit").click(function () {
           
            if ($("#frmuploadonlineclassurl").valid())
            { 
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    // showDataITGK();
                if(isUrl(txtclassurl.value)){
                    var url = "common/cfUploadOnlineClassurl.php"; // the script where 

                    var data;
                    data = "action=ADD&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "&slottime=" + txtstartdate.value + "&classurl=" + txtclassurl.value + ""; //
                    // alert(data);
                    $.ajax({
                        type: "post",
                        url: url,
                        data: data,
                        success: function (data) {
                            //alert(data);
                            if(data == "duplicatslot"){
                                $('#response').empty();
                                $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>You Have Already Entered Class Details in this Time Slot</span></div>");
                            }
                            else{
                                    $('#response').empty();
                                    $('#response').append("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>"+ data +"</span></div>");
                                }

                        }
                    });

                }
                else{
                    $('#response').empty();
                    alert("Class URL is Invalid");
                    location.reload();

                }               

            }
            return false; // avoid to execute the actual submit of the form.
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmuploadonlineclassurl.js" type="text/javascript"></script>
</html>
