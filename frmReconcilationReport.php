<?php
$title = "ReConcilation Report";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var BatchCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var BatchCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {
    ?>
    <!--<link rel="stylesheet" href="css/datepicker.css">
    <script src="scripts/datepicker.js"></script>-->
    <link rel="stylesheet" href="bootcss/css/bootstrap-datetimepicker.min.css">
    <script src="bootcss/js/moment.min.js"></script>
    <script src="bootcss/js/bootstrap-datetimepicker.min.js"></script>
<div class="container"> 


    <div class="panel panel-primary" style="margin-top:36px !important;">  
        <div class="panel-heading">ReConcilation Report</div>
        <div class="panel-body">

            <form name="form" id="form_reconcilation" class="form-inline" role="form" >
                <div class="container" style="width:100%;">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>


                    <div class="container">
                        <div class="col-sm-6 form-group"> 
                            <label for="sdate">Start Date:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-4 form-group">
							<div class='input-group date' id='datetimepicker1'>
								<input type="text" class="form-control" id="txtstartdate" name="txtstartdate" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-sm-6 form-group"> 
                            <label for="sdate">End Date:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-4 form-group">
							<div class='input-group date' id='datetimepicker1'>
								<input type="text" class="form-control" id="txtenddate" name="txtenddate" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
                        </div>
                    </div>
					<div class="container">
                        <div class="col-sm-6 form-group"> 
                            <label for="sdate">Report Type:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-4 form-group">
                            <select id="reportType" name="reportType" class="form-control selectpicker">
								<option value="learner_fee">Learner Fee Payment</option>
								<option value="reexam_fee">Learner Re-Exam Payment</option>
								<option value="NCR_fee">NCR Payment</option>
								<option value="correction_fee">Correction & Duplication Payment</option>
								<option value="name_fee">Name & Address Change Payment</option>
								<option value="eoi_fee">EOI Payment</option>
                            </select>
                        </div>
                    </div>
					<div class="container">
                        <div class="col-sm-6 form-group"> 
                            <label for="sdate">Report:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-4 form-group report" style="display:block;">
                            <select id="report1" name="report1" class="form-control" >
								<option value="report_1">Admission Report</option>
								<option value="report_2">Admission Transaction Report</option>
								<option value="report_3">Payment Transaction Report</option>
								<option value="report_4">Admission Invoice Report</option>
								<option value="report_5">Admission Refund Report</option>
                            </select>
                        </div>
                        <div class="col-sm-4 form-group report">
                            <select id="report2" name="report2" class="form-control" >
								<option value="report_1">Re-Exam Report</option>
								<option value="report_2">Re-Exam Transaction Report</option>
								<option value="report_3">Re-Exam Payment Transaction Report</option>
								<option value="report_4">Re-Exam Invoice Report</option>
								<option value="report_5">Re-Exam Refund Report</option>
                            </select>
                        </div>
                        <div class="col-sm-4 form-group report">
                            <select id="report3" name="report3" class="form-control" >
								<option value="report_1">NCR Report</option>
								<option value="report_2">NCR Transaction Report</option>
								<option value="report_3">NCR Payment Transaction Report</option>
								<option value="report_4">NCR Invoice Report</option>
								<option value="report_5">NCR Refund Report</option>
                            </select>
                        </div>
                        <div class="col-sm-4 form-group report">
                            <select id="report4" name="report5" class="form-control" >
								<option value="report_1">Correction Report</option>
								<option value="report_2">Correction Transaction Report</option>
								<option value="report_3">Correction Payment Transaction Report</option>
								<option value="report_4">Correction Invoice Report</option>
								<option value="report_5">Correction Refund Report</option>
                            </select>
                        </div>
                        <div class="col-sm-4 form-group report">
                            <select id="report5" name="report6" class="form-control" >
								<option value="report_1">Name & Address Change Report</option>
								<option value="report_2">Name & Address Change Transaction Report</option>
								<option value="report_3">Name & Address Change Payment Transaction Report</option>
								<option value="report_4">Name & Address Change Invoice Report</option>
								<option value="report_5">Name & Address Change Refund Report</option>
                            </select>
                        </div>
                        <div class="col-sm-4 form-group report">
                            <select id="report6" name="report4" class="form-control" >
								<option value="report_1">EOI Report</option>
								<option value="report_2">EOI Transaction Report</option>
								<option value="report_3">EOI Payment Transaction Report</option>
								<option value="report_4">EOI Invoice Report</option>
								<option value="report_5">EOI Refund Report</option>
                            </select>
                        </div>
                    </div>
					
					<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View" />    
                    
                            <input type="submit" name="btnSubmit" id="btnSubmitDown" class="btn btn-primary" value="Download" />    
                    </div> 
					<div class="container">
						<div id="grid" style="margin-top:35px;"> </div>
					</div>
                </div>   
            </form>
        </div>
    </div>
</div>
</div>

<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
	$('#txtstartdate').parent().datetimepicker({
		format: 'YYYY-MM-DD',
		widgetPositioning: {horizontal: "auto", vertical: "bottom"}
	});
	$('#txtenddate').parent().datetimepicker({
		format: 'YYYY-MM-DD',
		widgetPositioning: {horizontal: "auto", vertical: "bottom"}
	});
	$('#txtstartdate').datetimepicker({
		format: 'YYYY-MM-DD',
		widgetPositioning: {horizontal: "auto", vertical: "bottom"}
	});
	$('#txtenddate').datetimepicker({
		format: 'YYYY-MM-DD',
		widgetPositioning: {horizontal: "auto", vertical: "bottom"}
	});
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
	//$("#txtstartdate, #txtenddate").datetimepicker();
    $(document).ready(function () {
		
		$("#btnSubmit").click(function (e) {
			//alert("aya");
			e.preventDefault();
			$("#grid").html();
			var startd = $('#txtstartdate').val();
			var endd = $('#txtenddate').val();
			if(startd == "" || endd == ""){
				$('#response').append("<p class='error'><span><p style='color:red;font-size:14px'>Please Fill All Required Fields.</p></span></p>");
			}
			else{
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/icons/progress_bar.gif  /></span></p>");
				var url = "common/cfReconcilationReport.php"; // the script where you handle the form input.
				var data = $('#form_reconcilation').serialize();
				//alert(data);
			   
				$.ajax({
					type: "post",
					url: url,
					data: "action=reconcilation&"+data+"",
					success: function (data) {
						//alert(data);
						$("#grid").html(data);
						$('#example').DataTable({
							dom: 'Bfrtip',
							buttons: [
								'copy', 'csv', 'excel', 'pdf', 'print'
							]
						});/* */
						$('#response').empty();
					}
				});
				//showdata();
			}
			return false; // avoid to execute the actual submit of the form.
		});
		
		$("#btnSubmitDown").click(function (e) {
			var startd = $('#txtstartdate').val();
			var endd = $('#txtenddate').val();
			if(startd == "" || endd == ""){
				$('#response').append("<p class='error'><span><p style='color:red;font-size:14px'>Please Fill All Required Fields.</p></span></p>");
			}
			else{
				$("#myModal").modal("show");    
				
				$("#btnName").click(function (e) {
					var txtName = $("#txtName").val();
					//alert(txtName);
					$("#closemodal").click();
					
					//alert("aya");
					e.preventDefault();
					$('#response').empty();
					$('#response').append("<p class='error'><span><img src=images/icons/progress_bar.gif  /></span></p>");
					var url = "common/cfReconcilationReportExcel.php"; // the script where you handle the form input.
					var data = $('#form_reconcilation').serialize();
					//alert(data);
				   
					$.ajax({
						type: "post",
						url: url,
						data: "action=reconcilation&"+data+"&download=1&fileName="+txtName+"",
						success: function (data) {
							$('#response').empty();
							$("#grid").html();
							window.location.replace(data);
						}
					}); 
					//showdata();
				});
			}
			
			return false; // avoid to execute the actual submit of the form.
		});
		    
		$("#reportType").on('change', function() {
			//alert( this.value );
			$("#grid").html("");
			var reportType = this.value;
			if(reportType == "learner_fee"){
				$(".report").css("display","none");
				$("#report1").parent().css("display","block");
			}
			if(reportType == "reexam_fee"){
				$(".report").css("display","none");
				$("#report2").parent().css("display","block");
			}
			if(reportType == "NCR_fee"){
				$(".report").css("display","none");
				$("#report3").parent().css("display","block");
			}
			if(reportType == "correction_fee"){
				$(".report").css("display","none");
				$("#report4").parent().css("display","block");
			}
			if(reportType == "name_fee"){
				$(".report").css("display","none");
				$("#report5").parent().css("display","block");
			}
			if(reportType == "eoi_fee"){
				$(".report").css("display","none");
				$("#report6").parent().css("display","block");
			}
		})
    });

</script>
<style>
.container .container{
	width:100%;
}
.form-inline .col-sm-4.report{
	display:none;
}
.form-inline .form-group.col-sm-4 {
    max-width: unset;
}
.form-inline .input-group {
    width: 100%;
}

</style>
<style>
  .modal-dialog {width:600px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
<script type="text/javascript">  
  $(document).ready(function() {
	jQuery(".fix").click(function(){
        $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>
 <!--- for modal code start form here  -->
                        <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">
					<div class="modal-content">
                                            <div class="modal-header">
                                                    <button class="close" type="button" data-dismiss="modal">×</button>
                                                    <h3 class="modal-title">Enter File Name </h3>
                                            </div>
                                            
                                            <div class="modal-body">
                                                <div id="responseName"></div>
                                                <form name="frmictname" id="frmictname" class="form-inline" role="form">
                                                
                                                <input type="text" class="form-control" name="txtName" id="txtName" style="width: 75%;">     
                                                <input type="button" name="btnName" id="btnName" class="btn btn-primary" value="Download"/>    
                                                </form>
                                            </div>
                                           
                                            <div class="modal-footer">
                                                <input type="button" style="display:none" name="btnConfirm" id="btnConfirm" class="btn btn-primary" value="OK"/> 
                                                <button class="btn btn-default" id="closemodal" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
				</div>
			</div>
                    <!--- for modal code start form here  --> 
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "index.php";
    </script>
    <?php
}
?>
