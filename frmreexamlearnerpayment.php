<?php
$title = "Re-Exam Payment";
include ('header.php');
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Re-Exam Payment</div>
            <div class="panel-body">
                <form name="frmreexampayment" id="frmreexampayment" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-10 form-group">
                            <label for="batch"> Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                            <input type="hidden" id="ddlCourse" name="ddlCourse" value="">
                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Payment Mode:</label>
                            <select id="paymode" name="paymode" class="form-control" onchange="toggle_visibility1('online_mode');">

                            </select>									
                        </div> 
                    </div>

                    <div>
                        <input type="hidden" name="amounts" id="amounts"/>
                    </div>
                    <div id="menuList" name="menuList" style="margin-top:35px;"> 


                    </div> 
                    <div class="container">
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>
                    </div>                  


            </div>
        </div>   
    </div>
</form>

<form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
    <input type="hidden" id="txnid" name="txnid">
</form>

</body>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<script type="text/javascript">
    $(document).ready(function () {
        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfReexamAapplication.php",
                data: "action=GetLearnerCourseBatch",
                success: function (data) {
                    data = $.parseJSON(data);
                    $("#ddlCourse").val(data.Admission_Course);
                }
            });
        }
        FillCourse();

        function FillEvent() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfReexamAapplication.php",
                data: "action=FillReExamEvent",
                success: function (data) {
                    $("#ddlExamEvent").html(data);
                }
            });
        }
        FillEvent();

        $("#ddlExamEvent").change(function () {

            showpaymentmode(this.value, ddlCourse.value);
        });

        function showpaymentmode(val, val1) {
            $.ajax({
                type: "post",
                url: "common/cfEvents.php",
                data: "action=SHOWReexamPay&examevent=" + val + "&course=" + val1 + "",
                success: function (data) {

                    $("#paymode").html(data);
                }
            });
        }

        $("#paymode").change(function () {
            //var selExamEvent = $(this).val();
            showAllData(paymode.value, ddlCourse.value, ddlExamEvent.value);
            getfee(paymode.value, ddlCourse.value, ddlExamEvent.value);

        });
        function showAllData(val, val1, val2) {
            $.ajax({
                type: "post",
                url: "common/cfReexamPayment.php",
                data: "action=SHOWALL&paymode=" + val + "&course=" + val1 + "&event=" + val2 + "",
                success: function (data) {

                    $("#menuList").html(data);
                    $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                    $('#btnSubmit').show();
                }
            });
        }
        function getfee(val, val1, val2)
        {
            $.ajax({
                type: "post",
                url: "common/cfReexamPayment.php",
                data: "action=Fee&paymode=" + val + "&course=" + val1 + "&event=" + val2 + "",
                success: function (data) {

                    amounts.value = data;
                }
            });
        }

        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $('#btnSubmit').hide();
            var url = "common/cfReexamPayment.php";
            var data;
            var forminput = $("#frmreexampayment").serialize();
            if (Mode == 'Add') {
                data = "action=ADD&" + forminput;
                $('#txnid').val('');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#response').empty();
                        if (data == 0) {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Please try again, also ensure that, you have selected atleast one checkbox." + "</span></p>");
                        } else if (data != '') {
                           if (data == 'TimeCapErr') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                    alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                            } else {
                                $('#txnid').val(data);
                                $('#frmpostvalue').submit();
                            }
                        }
                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.

        });

        $('#menuList').on('click', '#checkuncheckall', function (){
            checkuncheckall(this.checked);
        });
    });

    function checkuncheckall(checked) {
        var aa = document.getElementById('frmreexampayment');
        for (var i =0; i < aa.elements.length; i++) {
            aa.elements[i].checked = checked;
        }
    }

</script>
</body>

</html>