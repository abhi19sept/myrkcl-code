<?php
    $title="RKCL-Dashboard";
    include ('header.php'); 
    include ('root_menu.php');
    include ('common/message.php');
?>

<style>
/*==============================================================
	sitemap Start
==============================================================*/
.list-unstyled > li > ul {
  margin-left: 35px; }

.list-unstyled > li > ul li {
  list-style: circle; }
 ul{
	 list-style: none !important;
 }

.sitemap {
  border-left: 2px dashed #2976aa; }

.sitemap h5 {
  padding: 15px;
  margin-bottom: 10px; }

.sitemap ul {
  position: relative;
  padding-left: 25px; }

.sitemap ul li {
  min-height: 40px;
  position: relative;
  padding-left: 45px;
  margin-bottom: 10px;
  padding-top: 5px; }

.sitemap ul li:last-child {
  margin-bottom: 0; }

.sitemap ul li:before {
  content: "";
  background: url(https://deskapp-dashboard.herokuapp.com/vendors/images/page-icon.svg) no-repeat;
  width: 35px;
  height: 35px;
  position: absolute;
  left: 0;
  top: 0; }

.sitemap ul li:after {
  content: "";
  width: 25px;
  left: -25px;
  top: 40%;
  position: absolute;
  border-top: 2px dashed #2976aa; }

.sitemap ul li a {
  display: inline-block;
  color: #2976aa;
  font-weight: 500;
  font-size: 15px;
  -webkit-transition: all 0.3s ease-in-out;
  transition: all 0.3s ease-in-out; }

.sitemap ul li a:hover {
  color: #000; }

.sitemap ul li.child {
  padding-left: 0;
  padding-top: 0;
  border-left: 2px dashed #2976aa; }

.sitemap ul li.child h5 {
  padding-top: 0; }

.sitemap ul li.child:before, .sitemap ul li.child:after {
  display: none; }
</style>
<?php 
if ($_SESSION['User_UserRoll'] == '1' OR $_SESSION['User_UserRoll'] == '17' OR $_SESSION['User_UserRoll'] == '4' OR
$_SESSION['User_UserRoll'] == '7' OR $_SESSION['User_UserRoll'] == '14' OR $_SESSION['User_UserRoll'] == '2' OR $_SESSION['User_UserRoll'] == '3'OR $_SESSION['User_UserRoll'] == '4' OR $_SESSION['User_UserRoll'] == '8' OR $_SESSION['User_UserRoll'] == '9' OR $_SESSION['User_UserRoll'] == '11' OR $_SESSION['User_UserRoll'] == '15' OR $_SESSION['User_UserRoll'] == '16' OR $_SESSION['User_UserRoll'] == '17' OR $_SESSION['User_UserRoll'] == '19'  OR $_SESSION['User_UserRoll'] == '12') {	
            ?>

<div class="container" style="min-height:430px !important;margin-top:50px;">
<div class="row">
    <div class="col-md-12" >
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <span class="glyphicon glyphicon-bookmark"></span> Sitemap </h3>
        </div>
        <div class="panel-body">
		<div class="min-height-200px">
				
				<div class="mb-30">
					<div class="pb-20">
					
						
						<div class="row">
						<div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="sitemap">
								
									<h5 class="weight-500">Multi Level Sitemap</h5>
									<div id="gird"></div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
				
			
		</div>
</div>
</div>
</div>
</div>
<script>
    $(document).ready(function () 
	{
		/*Show Data for sitemap function */
		function showData() 
	    {
			//alert(1);

            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			$.ajax({
				type: "post",
				url: "common/cfsitemap.php",
				data: "action=SHOW",
				success: function (data) {
					$('#response').empty();
					//alert(data);	
					$("#gird").html(data);
					$('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
					
				
				}
				
		  });
		}
		showData();
		
	});
		
		
		


</script>
	
<?php include ('footer.php'); ?>
	
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>