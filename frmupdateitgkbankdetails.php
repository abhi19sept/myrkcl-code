<?php
$title="Update Bank Account Details";
include ('header.php'); 
include ('root_menu.php');
 
if (isset($_REQUEST['code'])) {
echo "<script>var BankAccountCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var BankAccountCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '1') {	
?>
    <div style="min-height:430px !important;max-height:1500px !important;">
          <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Update Bank Account Details</div>
                <div class="panel-body">
					<form name="frmfillbankaccount" id="frmfillbankaccount" class="form-inline" role="form"  >
				
					<div class="container" style="width:1100px;">
						<div class="container">
							<div id="response"></div>
						</div>        
							<div id="errorBox"></div>				
							<div class="container">
								<div class="col-sm-4 form-group">     
                                    <label for="Mobile">IT-GK Code:</label>
                                    <input type="text" readonly="true" maxlength="10" class="form-control"
									value="<?php echo $_SESSION['User_LoginId']; ?>" name="ITGKCode" id="ITGKCode"
									placeholder="ITGK Code">
                                </div>
								
								<div class="col-sm-4 form-group">     
                                    <input type="button" name="validate" id="validate" class="btn btn-primary"
										value="Verify" style="margin-left:8px; margin-top:23px;"/> 
                                </div>
							</div>
					</div>
					<div id="gird"> </div>
				</form>
				
	</div>
   </div>
 </div>
  </div>
 
<!-- Modal -->
<div id="myModalupdate" class="modal" style="padding-top:150px !important">            
  <div class="modal-content">
    <div class="modal-header">
      <button class="close" type="button" data-dismiss="modal">×</button>
			<h3 id="heading-tittle" class="modal-title">Update Bank Details</h3>
    </div>
      <div class="modal-body">
          <form name="frmupdbankdetailsmodal" id="frmupdbankdetailsmodal" class="form-inline" role="form"
				action="common/cfUpdateItgkBankDetails.php"	enctype="multipart/form-data">  
			<div class="container">
				<div class="container">
					<div id="responses"></div>

				</div>        
				<div id="errorBox"></div>		  
				<div class="col-sm-4 form-group" >     
					<label for="order">IFSC Code:<font color="red">*</font></label>
					<input type="text" class="form-control" maxlength="11" name="txtIfscCode" id="txtIfscCode"
						placeholder="IFSC Code" onkeypress="javascript:return validAddress(event);" required="true">
					<input type="hidden" name="autoid" id="autoid"/>
					<input type="hidden" name="itgk" id="itgk"/>
					<input type="hidden" name="accno" id="accno"/>
					<input type="hidden" name="action" id="action" value="UpdateBankDetails"/>
				</div>  
				
				<div class="col-sm-4 form-group"> 
					<label for="edistrict">Select Bank Name:<span class="star">*</span></label>
					<select id="ddlBankName" name="ddlBankName" class="form-control" required="true">
					
					</select>    
				</div>	
				
				<div class="col-sm-4 form-group"> 
				  <label for="photo">Attach Bank Id Proof:</label>
				  <input type="file" class="form-control" id="chequeImage" name="chequeImage"   onchange="BankIdProof(this)">
				   <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =200KB</span>
				  	<img id="uploadPreview2" src="images/sbi.jpg" id="uploadPreview2" name="filePhoto" width="80px" height="35px" onclick="javascript:document.getElementById('uploadImage2').click();">		
				</div> 
								
			</div>
			
				<div class="container">
                  <div class="col-sm-4 form-group last">
                     <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary valid"
							value="Submit"/>                      
                  </div>  
				</div>    
		</form>		
    </div>
  </div>
</div>


</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script type="text/javascript">
	function PreviewImage(no) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
		oFReader.onload = function (oFREvent) {
			document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
		};
	};
</script>

<script language="javascript" type="text/javascript">

function BankIdProof(target) {
	var ext = $('#chequeImage').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("chequeImage").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			        
		alert("Image size should less or equal 200 KB");
		document.getElementById("chequeImage").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("chequeImage").value = '';
				return false;
	}    
    document.getElementById("chequeImage").innerHTML = "";
    return true;
}

</script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		$("#validate").click(function () {
			$('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			$.ajax({
				type: "post",
				url: "common/cfUpdateItgkBankDetails.php",
				data: "action=AuthenticateItgk",
				success: function (data) {
					var data = data.trim();
					$('#response').empty();
					$("#gird").html('');
					if(data=="1"){
						$('#response').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + " You are not allowed to modify bank details." + "</font></span></p>");
					}
					else if (data=="2"){
						$('#response').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + " You had already modify your bank details." + "</font></span></p>");
					}
					else{
						$("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
					}
				}
			});
		});
		
		$("#gird").on("click",".view_photo",function(){
			var image = $(this).attr("image");
			var count = $(this).attr("count");
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfUpdateItgkBankDetails.php",
                data: "action=ViewPhoto&image=" + image + "",
                success: function (data) {
                    $('#response').empty();
                    $("#"+count+"_photo").hide();
					$("#Viewphoto_"+count).html(data);
					$("#Viewphoto_"+count).show();
                }
            });
		});
		
		function FillBank() {
		        $.ajax({
		            type: "post",
		            url: "common/cfUpdateItgkBankDetails.php",
		            data: "action=FILLBanks",
		            success: function (data) {
		                $("#ddlBankName").html(data);
		            }
		        });
		    }
		
		    
		
		$("#gird").on("click",".upd_details",function(){
			var autoid = $(this).attr("id");
			var itgk = $(this).attr("itgk");
			var accno = $(this).attr("accno");
				$("#autoid").val(autoid);
				$("#itgk").val(itgk);
				$("#accno").val(accno);
				FillBank();
				
			var modal = document.getElementById('myModalupdate');
			var span = document.getElementsByClassName("close")[0];
				modal.style.display = "block";
				span.onclick = function() { 
					modal.style.display = "none";
                                        }
		});
		
		$("#btnSubmit").click(function () {
			var txtIfscCode = $('#txtIfscCode').val();
			var ddlBankName = $('#ddlBankName').val();
				if(txtIfscCode==""){
					alert("Please Enter IFSC Code");
				}
				else if (ddlBankName==""){
					alert("Please Select Bank Name");
				}
				else{
					if (confirm("Are You Sure you want to Update Bank Details?"))
						{
							$("#frmupdbankdetailsmodal").submit();
						}
						else{
							
						}
				}			
		});

		$("#frmupdbankdetailsmodal").on('submit',(function(e) {
			$('#responses').empty();
			$('#responses').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
			e.preventDefault();
			$.ajax({ 
					url: "common/cfUpdateItgkBankDetails.php",
					type: "POST",
					data:  new FormData(this),
					contentType: false,
					cache: false,
					processData:false,
					success: function(data){
						var result = data.trim();
						if(result == "ifsc"){
							$('#responses').empty();
							$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Enter IFSC Code.</span></p>");
						}
						else if(result == "bank"){
							$('#responses').empty();
							$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Select Bank Name.</span></p>");
						}
						else if(result == "some"){
							$('#responses').empty();
							$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something went wrong.</span></p>");
						}
						else if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
						{
							$('#responses').empty();
							$('#responses').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + result + "</span></p>");
							window.setTimeout(function () {
								window.location.href = "frmupdateitgkbankdetails.php";
							}, 2000);

							Mode = "Add";
							resetForm("form");
						}
						else
						{
							$('#responses').empty();
							$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></p>");
						}
					}
			 });
			 return false; // avoid to execute the actual submit of the form.
            }));
		

	 function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });


</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmfillbankaccount_validation.js" type="text/javascript"></script>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>
