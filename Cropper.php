<?php
class Cropper {

	protected $FCPATH;

	function __construct($path) {
		$this->FCPATH = $path;
	}
	
	################## Crop function ######################
	public function crop($_args){
		$size = $_args['size'];
		$pic = $_args['image'];
		$placeholder = isset($_args['placeholder'])?$_args['placeholder']:1;
		$crop = isset($_args['crop'])?$_args['crop']:1;
		$stretch = isset($_args['stretch'])?$_args['stretch']:0;
		$request = isset($_args['request'])?$_args['request']:0;
		start:
		if(!@$getimagesize = getimagesize($pic)){
			$pic= $this->FCPATH."attachments/placeholder/$placeholder.png";
			if(!$getimagesize = getimagesize($pic)){
				return 0;
			}
			goto start;
		}else{
			
		}
		$temp=str_replace($this->FCPATH,'',$pic);
		$directory = explode("/",$temp);
		array_pop($directory);
		$directory = implode("/",$directory);
		$name = explode("/",$temp);
		$name = end($name);
		
		list($width, $height,$type) = $getimagesize;
		/*-----amendments-----*/
		$size[0] = $width;
		$size[1] = $height;
		/*-----amendments-----*/
		
		
		if($size and $height && $type){
			$ratio = $width / $height;
		}
		$image_return_array=array();
		
			if(!$size[0]){
				 $size[0]=$size[1]*$ratio;
			 }
			 
			 if(!$size[1]){
				 $size[1]=$size[0]/$ratio;
			 }
			################## Check File Before Crop ####################
			$folder=$size[0].'-'.$size[1].'-'.$crop;
			/*-----amendments-----*/
			$folder='cropped';
			/*-----amendments-----*/
			
			if(1){
				
				$name = explode(".",$name);
				switch($type){
					case 1:{$ext = 'gif';}break;
					case 2:{$ext = 'jpg';}break;
					case 3:{$ext = 'png';}break;
					default:{$ext = 'jpeg';}break;
				}
				$name = $name[0].".".$ext;
				
			}
			$directory = '';
			$tempURL=$this->FCPATH.'/'.$name;
			
			
			
			if(@getimagesize($tempURL)){
				return $tempURL;
			}
			
			################## Check File Before Crop ####################
			$ext = pathinfo($pic, PATHINFO_EXTENSION);
			switch ($type) {
				case 1: $im = imagecreatefromgif($pic);break;
				case 2: $im = imagecreatefromjpeg($pic);break;
				case 3: $im = imagecreatefrompng($pic);break;
			}
			$a=0; $b=0;
			$c=0; $d=0;
			
			$original_image=array('width'=>$width,"height"=>$height,"ratio"=>$width/$height);
			$cropped_image=array('width'=>$size[0],"height"=>$size[1],"ratio"=>$size[0]/$size[1]);
			
			$new_image=array();
			$ratioW = $cropped_image['width'] / $original_image['width'];
			$ratioH = $cropped_image['height'] / $original_image['height'];
			
			if($crop){
				$ratio = $ratioW > $ratioH?$ratioW:$ratioH;
				
				$new_image['width'] = $original_image['width']*$ratio;
				$new_image['height'] = $original_image['height']*$ratio;
				
				if($new_image['width']>$original_image['width'] and $new_image['height']>$original_image['height']){
					if(!$stretch){
						$new_image['width'] = $original_image['width'];
						$new_image['height'] = $original_image['height'];
					}
				}elseif($new_image['width']>$original_image['width'] or $new_image['height']>$original_image['height']){
					$ratio = $ratioW < $ratioH?$ratioW:$ratioH;
					$new_image['width'] = $original_image['width']*$ratio;
					$new_image['height'] = $original_image['height']*$ratio;
				}
			}else{
				$ratio = $ratioW < $ratioH?$ratioW:$ratioH;
				$new_image['width'] = $original_image['width']*$ratio;
				$new_image['height'] = $original_image['height']*$ratio;
			}
			$b=($cropped_image['height']-$new_image['height'])/2;
			$a=($cropped_image['width']-$new_image['width'])/2;
			$newImage = imagecreatetruecolor($size[0], $size[1]);
			$white = imagecolorallocate($newImage, 255, 255, 255);
			imagecolortransparent($newImage, $white);
			imagefill($newImage, 0, 0, $white);
			$c=0; $d=0;
			
			
			
			imagecopyresampled($newImage, $im, $a,$b,$c,$d, $new_image['width'], $new_image['height'], $original_image['width'], $original_image['height']);
			$imagePath=(str_replace("\\",'/',$this->FCPATH)).'/'.$name;
			
			if(isset($_args['type'])){
				$type = $_args['type'];
			}
			
			switch ($type) {
				case 1:imagegif($newImage,$imagePath,100);break;
				case 2:imagejpeg($newImage,$imagePath,100);break;
				case 3:imagepng($newImage,$imagePath,9);break;
			}
			imagedestroy($im);
			imagedestroy($newImage);	
			return $tempURL;
	}
}
	################## Crop function ####################
