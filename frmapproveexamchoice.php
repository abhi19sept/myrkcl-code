<?php
$title="Approve Exam Choice";
include ('header.php'); 
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_Code'] == '8550') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}	
?>
<div style="min-height:430px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Approve Exam Choice</div>
            <div class="panel-body">
				<form name="form" id="frmlearnerresult" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                    </div>
					<div>
						<div class="col-md-3 col-md-3 " style="margin-top:20px;"> </div>

						<div class="col-md-6 col-md-6 " style="margin-top:20px; align-items: center;" id="step1" >                
							<a href="#" class="btn btn-primary btn-lg btn-block" role="button">
								<span class="glyphicon glyphicon-comment"></span> <br/>Step 1<span>
									<br/>Approve Exam Choice Temp Table</span></a>
						</div>
						
						<div class="col-md-6 col-md-6" style="display:none;margin-top:20px; align-items: center;" id="step2" >
							<a href="#" class="btn btn-primary btn-lg btn-block" role="button">
								<span class="glyphicon glyphicon-comment"></span> <br/>Step 2 <span>
								<br/>Approve Exam Choice Table</span></a>
						</div>

						<div class="col-md-3 col-md-3 " style="margin-top:20px;" ></div>
					</div>
				</form>
			</div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
               
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
 $(document).ready(function () {
			
		/* funtion for Fillevent */	
		$("#step1").click(function () {				
			$('#response').empty();
			$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				var url = "common/cfApproveExamChoice.php"; // the script where you handle the form input.
                var data;
				data = "action=Updatetemp"; // serializes the form's elements.
				$.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {		alert(data);				
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $("#step2").show();
							$("#step1").hide();
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        
                    }
                });
		});
		
		$("#step2").click(function () {				
			$('#response').empty();
			$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				var url = "common/cfApproveExamChoice.php"; // the script where you handle the form input.
                var data;
				data = "action=Updatechoice"; // serializes the form's elements.
				$.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {						
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmapproveexamchoice.php";
                           }, 3000);
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        
                    }
                });
		});
						
		
});

    </script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
</html>