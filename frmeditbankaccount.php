<?php
$title="Bank Account Details";
include ('header.php'); 
include ('root_menu.php');
 
if (isset($_REQUEST['code'])) {
echo "<script>var BankAccountCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var BankAccountCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {	
            ?>
    <div style="min-height:430px !important;max-height:1500px !important;">
          <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Edit Bank Details</div>
                <div class="panel-body">
	<div id="gird"> </div>
	</div>
   </div>
 </div>
  </div>
 <script>
		$(document).ready(function() {
     $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
	
} );
	</script>


    




</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>
<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
    
			if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
			
			function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBankAccount.php",
                    data: "action=EDIT&values=" + BankAccountCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Make']);
                        //alert(data);
                        data = $.parseJSON(data);
                       
                        txtaccountName.value = data[0].Bank_Account_Name;
                        txtaccountNumber.value = data[0].Bank_Account_Number;
                        rbtaccountType_saving.value = data[0].Bank_Account_Type;
                        txtIfscCode.value = data[0].Bank_Ifsc_code;
						txtMicrcode.value = data[0].Bank_Micr_Code;
						ddlBranch.value = data[0].Bank_Branch_Name;
						
						
                        
                    }
                });
            }

			   function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfBankAccount.php",
                    data: "action=SHOWEDIT",
                    success: function (data) {

                        $("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						

                    }
                });
            }

            showData();
		
        
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>
