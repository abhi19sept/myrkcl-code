<?php
$title = "Digital Certificate";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '19') {
    //Login here with ITGK or Superadmin
} else {
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>
<style>
    .modal {z-index: 1050!important;} 
    #turmcandition{
        background-color: whitesmoke;
        text-align: justify;
        padding: 10px;
    }
    .noturm{
        margin-left: 10px !important;
    }
    .yesturm{
        margin-right: 15px;
    }
</style>
<div style="min-height:430px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Generate Digital Certificate</div>
            <div class="panel-body">

                <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">

                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">

                                </select>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">

                                </select>
                            </div> 

                        </div>

                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmitlist" id="btnSubmitlist" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        <div class="container">
                            <div id="response"></div>
                            <div id="responsecertificate" style="width: 90%;"></div>

                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>
                        <!--                        <div id="view" style="margin-top:5px; width:94%;"> </div>-->

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<link href="css/popup.css" rel="stylesheet" type="text/css">
<div id="myModalEresponse" class="modal" style="padding-top:180px !important;">         
    <div class="modal-content" style="width: 60%;border-radius: 15px; text-align: center;">
        <div class="modal-body" style="min-height: 175px;padding: 0 16px;" id="Eresponse">

        </div>
        <button class="btn btn-primary closeeresponse" style="margin-bottom: 15px;">Close</button>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">

    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")

        if (key == 8 || key == 0) {
            keychar = 8;
        }

        return reg.test(keychar);
    }

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfDigitalCertificate.php",
                data: "action=FILLLinkAadharCourse",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfDigitalCertificate.php",
                data: "action=FILLAdmissionBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });
        });

        $("#btnSubmitlist").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=20px style='margin: 5px;' /></span><span style='font-weight: bold;'> Processing please wait..... </span></p>");
                var url = "common/cfDigitalCertificate.php"; // the script where you handle the form input.     
                var data;
                var course = $('#ddlCourse').val();
                var batch = $('#ddlBatch').val();
                data = "action=GETDATAITGKWISE&course=" + course + "&batch=" + batch + ""; //            
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#response').empty();
                        $("#grid").html(data);
                        $('#example').DataTable();
                    }
                });

            }
            return false;
        });

        $("#grid").on("click", ".generate_certificate", function () {
            $('.generate_certificate').attr('disabled', 'disabled');
            $('#btnSubmitlist').attr('disabled', 'disabled');
            var edit_id = $(this).attr("id");
            $('#responsecertificate').empty();
            $('#responsecertificate').append("<div class='progress'><div id='thisProg' class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width:100%;background-color: green;font-size: 17px;color: red;'>Please wait don't click and refresh the page.</div>");
            var url = "common/cfDigitalCertificate.php"; // the script where you handle the form input.
            var data;
            data = "action=generateandaddcertificate&lcode=" + edit_id + ""; // serializes the form's elements.				 
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    $('#responsecertificate').empty();
                    var modal = document.getElementById('myModalEresponse');
                    var span = document.getElementsByClassName("closeEresponse")[0];
                    modal.style.display = "block";
                    span.onclick = function () {
                        modal.style.display = "none";
                        window.location.href = "frmDigitalCertificate.php";
                    }
                    $("#Eresponse").html(data);
                    window.setTimeout(function () {
                        Mode = "Add";
                    }, 5000);
                }
            });
        });

        $("#grid").on("click", ".update_certificate", function () {
            $('.update_certificate').attr('disabled', 'disabled');
            $('#btnSubmitlist').attr('disabled', 'disabled');
            var edit_id = $(this).attr("id");
            $('#responsecertificate').empty();
            $('#responsecertificate').append("<div class='progress'><div id='thisProg' class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width:100%;background-color: green;font-size: 17px;color: red;'>Please wait don't click and refresh the page.</div>");
            var url = "common/cfDigitalCertificate.php"; // the script where you handle the form input.
            var data;
            data = "action=generateandupdatecertificate&lcode=" + edit_id + ""; // serializes the form's elements.				 
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    $('#responsecertificate').empty();
                    var modal = document.getElementById('myModalEresponse');
                    var span = document.getElementsByClassName("closeEresponse")[0];
                    modal.style.display = "block";
                    span.onclick = function () {
                        modal.style.display = "none";
                        window.location.href = "frmDigitalCertificate.php";
                    }
                    $("#Eresponse").html(data);
                    window.setTimeout(function () {
                        Mode = "Add";
                    }, 5000);
                }
            });
        });



    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
        background-color: #e2d9d9;
        width: 91%;
    }
</style>
</html>
?>