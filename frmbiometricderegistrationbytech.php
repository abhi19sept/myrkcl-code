<?php  ob_start();
$title = "Bio Metric Deregistration By Tech";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if($_SESSION['User_UserRoll']!=1 && $_SESSION['User_UserRoll']!=8 && $_SESSION['User_UserRoll']!=28){
    session_destroy();
    header('Location: index.php');
    exit;
}
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Learners Bio Metric Deregistration</div>
            <div class="panel-body">

                <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div class="col-md-6" >
                               <input readonly="readonly" id="totalreq" style="    width: inherit;"> 
                            </div>

                        </div> 
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            
                        </div>
                        <div id="grid" style="margin-top:30px; width:94%;"> </div>
                       
                        
                        <button type="button" class="btn btn-primary" name="sbtn" id="sbtn" data-toggle="modal" data-target="#myModal" style="display:none; margin-top: 30px;">Submit</button>
   
                        <div class="modal fade" id="previewModal" role="dialog">
                            <div>
                                <div class="modal-content" style="margin: 100px auto; width: 1100px;" >
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title text-left">Are you sure you want to submit these details?</h4>
                                    </div>
                                    <div class="modal-body" id="rec_type">

                                    </div>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" id="btnFinalSubmit">Approve Selected</button>
                                         <button class="btn btn-danger" id="btnFinalReject">Reject Selected</button>
                                        <button class="btn btn-default" data-dismiss="modal">Back to Selection</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>  
                    
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function Filltotalreq() {
            //alert("hello");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBioMetricDeregistrationByTech.php"; // the script where you handle the form input.
                var data;
                data = "action=Filltotalreq"; //
                // alert(data);
                $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data2) {

                    $("#totalreq").val("Total Requests Pending: "+data2);


                    
                }
            });
        }
        Filltotalreq()
        function FillDeregistrationLearners() {
            //alert("hello");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBioMetricDeregistrationByTech.php"; // the script where you handle the form input.
                var data;
                data = "action=GETCURRENTBATCHLEARNER"; //
                // alert(data);
                $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data2) {
//                    alert(data2);
                    $('#response').empty();
                    $("#grid").html(data2);
                    $("#sbtn").show();
                    
                    var table = $('#example').DataTable({
                        dom: 'Bfrtip',
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false,
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    
                    
                    // Handle click on "Select all" control
                    $('#example-select-all').on('click', function(){
                       // Get all rows with search applied
                       var rows = table.rows({ 'search': 'applied' }).nodes();
                       // Check/uncheck checkboxes for all rows in the table
                       $('input[type="checkbox"]', rows).prop('checked', this.checked);
                    });

                    // Handle click on checkbox to set state of "Select all" control
                    $('#example tbody').on('change', 'input[type="checkbox"]', function(){
                        // If checkbox is not checked
                        if(!this.checked){
                           var el = $('#example-select-all').get(0);
                           // If "Select all" control is checked and has 'indeterminate' property
                           if(el && el.checked && ('indeterminate' in el)){
                              // Set visual state of "Select all" control
                              // as 'indeterminate'
                              el.indeterminate = true;
                           }
                        }
                     });

   
                    
                    
                }
            });
        }
        FillDeregistrationLearners();

        $("#btnSubmit").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBioMetricDeregistration.php"; // the script where you handle the form input.
                var data;
                data = "action=GETCURRENTBATCHLEARNER&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
                // alert(data);
                $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data2) {
//                    alert(data2);
                    $('#response').empty();
                    $("#grid").html(data2);
                    $("#sbtn").show();
                    
                    var table = $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    
                    
                    // Handle click on "Select all" control
                    $('#example-select-all').on('click', function(){
                       // Get all rows with search applied
                       var rows = table.rows({ 'search': 'applied' }).nodes();
                       // Check/uncheck checkboxes for all rows in the table
                       $('input[type="checkbox"]', rows).prop('checked', this.checked);
                    });

                    // Handle click on checkbox to set state of "Select all" control
                    $('#example tbody').on('change', 'input[type="checkbox"]', function(){
                        // If checkbox is not checked
                        if(!this.checked){
                           var el = $('#example-select-all').get(0);
                           // If "Select all" control is checked and has 'indeterminate' property
                           if(el && el.checked && ('indeterminate' in el)){
                              // Set visual state of "Select all" control
                              // as 'indeterminate'
                              el.indeterminate = true;
                           }
                        }
                     });

   
                    
                    
                }
            });

            }
            return false; // avoid to execute the actual submit of the form.
        });
        
   
        $("#sbtn").click(function () {
            
            //if ($("#form").valid())
            //{
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBioMetricDeregistrationByTech.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmAdmissionSummary").serialize();
                
                data = "action=AddDeregisterLearnerPreview&" + forminput; //serializes the form's elements.
                    //alert(data);
                
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        if (data == 'DONOTDOTHIS')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> Unauthorized use of this system may subject you to criminal prosecution and penalties.</span></p>");
                            $( ".close" ).trigger( "click" );
                            $( ".modal-backdrop.in").hide();
                            $('body,html').animate({
                                        scrollTop: 0
                            }, 500); 
                        }
                        else if (data == 'IDEMPTY')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> Please select at least one learner for this action.</span></p>");
                            $( ".close" ).trigger( "click" );
                            $( ".modal-backdrop.in").hide();
                            $('body,html').animate({
                                        scrollTop: 0
                            }, 500); 
                            //  window.setTimeout(function () {
                            //     window.location.href = "frmbiometricderegistrationbytech.php";
                            // }, 1000);
                        }
                        else
                        {
                            $('#response').empty();
                            $('#myModal').modal('toggle');
                            $('#previewModal').modal({show: true});
                            $('#rec_type').html(data);
                        }
                        //showData();


                    }
                });
                
                
            //}
            return false; // avoid to execute the actual submit of the form.
        });
        
        $("#btnFinalSubmit").click(function () {
            
            //if ($("#form").valid())
            //{
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBioMetricDeregistrationByTech.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmAdmissionSummary").serialize();
                
                data = "action=AddDeregisterLearner&" + forminput; //serializes the form's elements.
                    //alert(data);
                
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        if (data == 'DONE')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Sucessfully DeRegister.</span></p>");
                            $( ".close" ).trigger( "click" );
                            $( ".modal-backdrop.in").hide();
                            $('body,html').animate({
                                        scrollTop: 0
                            }, 500);
                            window.setTimeout(function () {
                                window.location.href = "frmbiometricderegistrationbytech.php";
                            }, 1000);
                            
                            
                        }
                        
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> Some Problem Accur.</span></p>");
                            $( ".close" ).trigger( "click" );
                            $( ".modal-backdrop.in").hide();
                            $('body,html').animate({
                                        scrollTop: 0
                            }, 500);
                        }
                        //showData();


                    }
                });
                
                
            //}
            return false; // avoid to execute the actual submit of the form.
        });
    $("#btnFinalReject").click(function () {
            
            //if ($("#form").valid())
            //{
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBioMetricDeregistrationByTech.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmAdmissionSummary").serialize();
                
                data = "action=RejectDeregisterLearner&" + forminput; //serializes the form's elements.
                    //alert(data);
                
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        if (data == 'DONE')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Sucessfully Rejected.</span></p>");
                            $( ".close" ).trigger( "click" );
                            $( ".modal-backdrop.in").hide();
                            $('body,html').animate({
                                        scrollTop: 0
                            }, 500);
                            window.setTimeout(function () {
                                window.location.href = "frmbiometricderegistrationbytech.php";
                            }, 1000);
                            
                            
                        }
                        
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> Some Problem Accur.</span></p>");
                            $( ".close" ).trigger( "click" );
                            $( ".modal-backdrop.in").hide();
                            $('body,html').animate({
                                        scrollTop: 0
                            }, 500);
                        }
                        //showData();


                    }
                });
                
                
            //}
            return false; // avoid to execute the actual submit of the form.
        });
         
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    
    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
