<?php
$title = "Modify NCR Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Update'</script>";
}
$random = (mt_rand(1000, 9999));
$random.= date("y");
$random.= date("m");
$random.= date("d");
$random.= date("H");
$random.= date("i");
$random.= date("s");

echo "<script>var OrgDocId= '" . $random . "' </script>";
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Modify NCR Application  Form</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmOrgMaster" id="frmOrgMaster" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="ackno">Acknowledgement Number:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="18" onkeypress="javascript:return allownumbers(event);" name="ackno" id="ackno" placeholder="Acknowledgement Number">
                            <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        </div>

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                        </div>
                    </div>


                    <div id="main-content" style="display: none;">
                        <div class="container">
                            <fieldset style="border: 1px groove #ddd !important;" class="col-sm-11">
                                <br>
                                <div class="col-sm-3">     
                                    <label for="SelectType">Select Type of Application:</label>
                                    <select id="ddlOrgType" name="ddlOrgType" class="form-control" >
                                        <option selected="true" value="">Select</option>
                                       
                                        <option value="15" >IT-GK</option>
                                    </select>  
                                    <input type="hidden" class="form-control"  name="txtGenerateId" id="txtGenerateId"/>
                                     <input type="hidden" class="form-control"  name="txtrsploginid" id="txtrsploginid"/>
                                </div>


                                <div class="col-sm-3 form-group"> 
                                    <label for="email">Enter Email:</label>
                                    <input type="text" class="form-control" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                                </div>


                                <div class="col-sm-3">     
                                    <label for="Mobile">Enter Mobile Number:</label>
                                    <input type="text" class="form-control" maxlength="10" name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                                </div>
                                <br>
                            </fieldset>
                        </div>  
                        <br>
                        <div class="container">

                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Organization/Center:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Registration No:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="30" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:<span class="star">*</span></label>
                                <select id="txtType" name="txtType" class="form-control" >

                                </select>    
                            </div>
                        </div>    

                        <div class="container">




                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Document Type:<span class="star">*</span></label>
                                <select id="txtDoctype" name="txtDoctype" class="form-control" >
                                    <option value="">Select</option>
                                    <option value="PAN Card">PAN Card</option>
                                    <!--                            <option value="Voter ID Card">Voter ID Card</option>
                                                                <option value="Driving License">Driving License</option>
                                                                <option value="Passport">Passport</option>
                                                                <option value="Employer ID card">Employer ID card</option>
                                                                <option value="Government ID Card">Governments ID Card</option>
                                                                <option value="College ID Card">College ID Card</option>
                                                                <option value="School ID Card">School ID Card</option>-->
                                </select>    
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="panno">PAN Card Number:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="10" name="txtPanNo" id="txtPanNo" placeholder="PAN Card Number">     
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Upload Proof:<span class="star">*</span></label>
                                <input type="file" class="form-control"  name="orgdocproof" id="orgdocproof" onchange="checkScanForm(this)">
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Country:<span class="star">*</span></label>
                                <select id="ddlCountry" name="ddlCountry" class="form-control" >

                                </select>    
                            </div>


                        </div>	

                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">State:<span class="star">*</span></label>
                                <select id="ddlState" name="ddlState" class="form-control" >

                                </select>    
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Divison:<span class="star">*</span></label>
                                <select id="ddlRegion" name="ddlRegion" class="form-control" >

                                </select>    
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:<span class="star">*</span></label>
                                <select id="ddlDistrict" name="ddlDistrict" class="form-control" >

                                </select>    
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Tehsil:<span class="star">*</span></label>
                                <select id="ddlTehsil" name="ddlTehsil" class="form-control" >

                                </select>    
                            </div>


                        </div>

                        <div class="container">	





                            <div class="col-sm-4 form-group"> 
                                <label for="address">Pin Code:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtPinCode" id="txtPinCode" maxlength="6" onkeypress="javascript:return allownumbers(event);" placeholder="Pin Code">    
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="address">Address:<span class="star">*</span></label>
                                <textarea class="form-control" id="txtRoad" name="txtRoad" placeholder="Address"></textarea>

                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="address">Nearest Landmark:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="12" name="txtLandmark" 
                                       id="txtLandmark" placeholder="Nearest Landmark" >

                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Police Station:</label>
                                <input type="text" class="form-control" name="txtPoliceStation" id="txtPoliceStation" value="NA" placeholder="Police Station">       

                            </div>


                        </div>    
                        <div class="container">
                            <div class="col-sm-4 form-group">     
                                <label for="area">Area Type:<span class="star">*</span></label> <br/>                               
                                <label class="radio-inline"> <input type="radio" id="areaUrban" name="area" onChange="findselected()"/> Urban </label>
                                <label class="radio-inline"> <input type="radio" id="areaRural" name="area" onChange="findselected1()"/> Rural </label>
                            </div>

                            <div class="container" id="Urban" style="display:none">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Mohalla:</label>
                                    <input type="text" class="form-control" name="txtMohalla" id="txtMohalla" value="NA" placeholder="Mohalla">      
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Ward No.:</label>
                                    <input type="text" class="form-control" name="txtWard" id="txtWard" value="NA" placeholder="Ward No">     
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Municipal Town:</label>
                                    <input type="text" class="form-control" name="txtMunicipal" id="txtMunicipal" value="NA" placeholder="Municipal Town">       
                                </div>


                            </div>
                            <div class="container" id="Rural" style="display:none">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Panchayat Samiti/Block:</label>
                                    <select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >

                                    </select>
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Gram Panchayat:</label>
                                    <select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control" >

                                    </select>
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Village:</label>
                                    <input type="text" class="form-control" name="txtVillage" id="txtVillage" value="NA" placeholder="Village">                            
                                </div>


                            </div>
                        </div> 

                        <!--                        <div class="container">
                                                    <input type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload"/>
                                                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>
                                                </div>-->
                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
                    </div>
            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script src="scripts/orgdocupload.js"></script>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
                                    $('#txtEstdate').datepicker({
                                        format: "yyyy-mm-dd"
                                    });
</script>
<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#orgdocproof').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("orgdocproof").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("orgdocproof").value = '';
            return false;
        }
        else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("orgdocproof").value = '';
            return false;
        }
        document.getElementById("orgdocproof").innerHTML = "";
        return true;
    }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        $('#txtGenerateId').val(OrgDocId);
        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLCourseName",
                success: function (data) {
                    $("#txtCourse").html(data);
                }
            });
        }

        FillCourse();


        $(function () {
            $("#txtEstdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });

        function FillOrgType() {
            $.ajax({
                type: "post",
                url: "common/cfOrgTypeMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#txtType").html(data);
                }
            });
        }
        FillOrgType();

        function FillParent() {
            $.ajax({
                type: "post",
                url: "common/cfCountryMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCountry").html(data);
                }
            });
        }
        FillParent();

        $("#ddlCountry").change(function () {
            var selcountry = $(this).val();
            //alert(selcountry);
            $.ajax({
                url: 'common/cfStateMaster.php',
                type: "post",
                data: "action=FILL&values=" + selcountry + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlState').html(data);
                }
            });
        });


        $("#ddlState").change(function () {
            var selState = $(this).val();
            //alert(selState);
            $.ajax({
                url: 'common/cfRegionMaster.php',
                type: "post",
                data: "action=FILL&values=" + selState + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlRegion').html(data);
                }
            });
        });

        $("#ddlRegion").change(function () {
            var selregion = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfDistrictMaster.php',
                type: "post",
                data: "action=FILL&values=" + selregion + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlDistrict').html(data);
                }
            });
        });

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLPanchayat&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlPanchayat').html(data);
                }
            });
        });

        $("#ddlPanchayat").change(function () {
            var selPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlGramPanchayat').html(data);
                }
            });
        });

        $("#ddlTehsil").change(function () {
            var selTehsil = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfAreaMaster.php',
                type: "post",
                data: "action=FILL&values=" + selTehsil + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlArea').html(data);
                }
            });
        });

        function findselected() {

            var UrbanDiv = document.getElementById("areaUrban");
            var category = document.getElementById("Urban");

            if (UrbanDiv) {
                Urban.style.display = areaUrban.checked ? "block" : "none";
                Rural.style.display = "none";
            }
            else
            {
                Urban.style.display = "none";
            }
        }
        function findselected1() {


            var RuralDiv = document.getElementById("areaRural");
            var category1 = document.getElementById("Rural");

            if (RuralDiv) {
                Rural.style.display = areaRural.checked ? "block" : "none";
                Urban.style.display = "none";
            }
            else
            {
                Rural.style.display = "none";
            }
        }
        $("#areaUrban").change(function () {
            findselected();
        });
        $("#areaRural").change(function () {
            findselected1();
        });

        $("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfOrgModify.php",
                data: "action=DETAILS&values=" + ackno.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Acknowledgement Number" + "</span></p>");
                    }
                    else {
                        data = $.parseJSON(data);
                        txtName1.value = data[0].orgname;
                        txtRegno.value = data[0].regno;
                        txtEstdate.value = data[0].fdate;                  
                       
                        txtEmail.value = data[0].email;
                        txtMobile.value = data[0].mobile;
                        
                        txtPanNo.value = data[0].panno;
                        txtPinCode.value = data[0].pincode;
                        //txtStreet.value = data[0].street;
                        txtRoad.value = data[0].road;
                        txtLandmark.value = data[0].landmark;
                        txtPoliceStation.value = data[0].policestation;
                        txtVillage.value = data[0].village;
                        txtMohalla.value = data[0].mohalla;
                        txtWard.value = data[0].wardno;
                        txtMunicipal.value = data[0].municipal;
                        
                        txtrsploginid.value = data[0].rsploginid;

                        $("#main-content").show();
                        $('#ackno').attr('readonly', true);
                        $("#btnShow").hide();

                    }

                }
            });
        });

        $("#btnSubmit").click(function () {
            if ($("#frmOrgMaster").valid())
            {


                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfOrgModify.php"; // the script where you handle the form input.
                var filename = $('#txtGenerateId').val();
                if (document.getElementById('areaUrban').checked) //for radio button
                {
                    var area_type = 'Urban';
                }
                else {
                    area_type = 'Rural';
                }
                //alert(filename);    

                var data;
                // alert(Mode);
                if (Mode == 'Add')
                {

                    data = "action=ADD&orgtype=" + ddlOrgType.value + "&email=" + txtEmail.value + "&mobile=" + txtMobile.value +
                            "&name=" + txtName1.value + "&type=" + txtType.value + "&regno=" + txtRegno.value +
                            "&state=" + ddlState.value + "&region=" + ddlRegion.value + "&panno=" + txtPanNo.value +
                            "&district=" + ddlDistrict.value + "&tehsil=" + ddlTehsil.value +
                            "&landmark=" + txtLandmark.value + "&road=" + txtRoad.value +
                            "&pincode=" + txtPinCode.value +
                            "&estdate=" + txtEstdate.value + "&doctype=" + txtDoctype.value +
                            "&areatype=" + area_type +
                            "&mohalla=" + txtMohalla.value + "&wardno=" + txtWard.value +
                            "&police=" + txtPoliceStation.value + "&municipal=" + txtMunicipal.value +
                            "&village=" + txtVillage.value + "&gram=" + ddlGramPanchayat.value +
                            "&panchayat=" + ddlPanchayat.value +
                            "&docproof=" + filename + "&country=" + ddlCountry.value + ""; // serializes the form's elements.
                    //alert(data);
                }
                else
                {
                    data = "action=UPDATE&code=" + ackno.value + "&rsploginid=" + txtrsploginid.value +
                            "&orgtype=" + ddlOrgType.value + "&email=" + txtEmail.value + "&mobile=" + txtMobile.value +
                            "&name=" + txtName1.value + "&type=" + txtType.value + "&regno=" + txtRegno.value +
                            "&state=" + ddlState.value + "&region=" + ddlRegion.value + "&panno=" + txtPanNo.value +
                            "&district=" + ddlDistrict.value + "&tehsil=" + ddlTehsil.value +
                            "&landmark=" + txtLandmark.value + "&road=" + txtRoad.value +
                            "&pincode=" + txtPinCode.value +
                            "&estdate=" + txtEstdate.value + "&doctype=" + txtDoctype.value +
                            "&areatype=" + area_type +
                            "&mohalla=" + txtMohalla.value + "&wardno=" + txtWard.value +
                            "&police=" + txtPoliceStation.value + "&municipal=" + txtMunicipal.value +
                            "&village=" + txtVillage.value + "&gram=" + ddlGramPanchayat.value +
                            "&panchayat=" + ddlPanchayat.value +
                            "&docproof=" + filename + "&country=" + ddlCountry.value + ""; // serializes the form's elements.
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmorgmodify.php";
                            }, 2000);

                            Mode = "Add";
                            resetForm("frmorgmaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        
                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmorgregistrationvalidation.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>