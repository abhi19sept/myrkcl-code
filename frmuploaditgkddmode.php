<?php
$title = "Upload ITGK list DD mode";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {
    ?>
    <link rel="stylesheet" href="css/datepicker.css"/>
    <script src="scripts/datepicker.js"></script>
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Upload ITGK list DD mode</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> --> 
                <form name="frmuploaditgkddmode" id="frmuploaditgkddmode" class="form-inline" role="form" enctype="multipart/form-data">    
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>

                    </div>						
                    <div class="container" id="coursemode">
                        <div class="col-sm-6 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-6 form-group"> 
                            <select id="ddlCourse" name="ddlCourse" class="form-control">									
                            </select>
                            <select id="coursecode" name="coursecode" style="display:none;" class="form-control">									
                            </select>	
                        </div> 
                    </div>
                    <div class="container" id="batchmode" >
                        <div class="col-sm-6 form-group"> 
                            <label for="course">Select Batch:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-6 form-group"> 
                            <select id="ddlBatch" name="ddlBatch" class="form-control">									
                            </select>

                        </div> 
                    </div>
                    <div class="container">
                        <div class="col-sm-6 form-group"> 
                            <label for="course">Event Name:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-6 form-group"> 

                            <select id="ddlEname" name="ddlEname" class="form-control" >

                            </select>
                        </div> 
                    </div>
                    <div class="container" style="display:none">
                        <div class="col-sm-6 form-group"> 

                            <input type="text" id="ddlEID" name="ddlEID" class="form-control" >

                            </select>
                        </div> 
                    </div>
                    <div class="container"> 
                        <div class="col-sm-4 form-group"> 
                            <label>Upload ITGK List for DD mode:<span class="star">*</span> </label>
                        </div>
                        <div class="col-sm-4 form-group">  
                            <label>Download Sample File:</label>
                            <a href="sample_file_DDmode_ITGK.xls"> <input type="button" value="Download File" name="download">  </a>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="ecl">Upload Eligible Centre List:<span class="star">*</span></label>
                            <input type="file" class="form-control" id="_file" name="_file" accept=".xls"/>
                            <span style="font-size:10px;">Note : .xls Allowed Max Size =50KB</span>
                            <span class="btn submit" id="fileuploadbutton"> <input type="button" id="btnUpload" value="Upload File"/></span> 
                            <span class='btn submit'> <input type='button' id='btnReset' name='btnReset' value='Reset' /> </span>
                        </div>


                    </div>




                    <div class="container">      
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>
                    </div>                  
                </form>
            </div>
        </div>
    </div>
    </body>

    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>

    <script src="scripts/fileuploadddmodeitgk.js"></script>
    <!--<script src="scripts/eoitnc.js"></script>-->


    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

        $(document).ready(function () {
            //alert("hii");

            function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {
                        txtGenerateId.value = data;
                    }
                });
            }
            GenerateUploadId();

            function FillCourse() {
                $.ajax({
                    type: "post",
                    url: "common/cfCourseMaster.php",
                    data: "action=FILLCourseName",
                    success: function (data) {
                        $("#ddlCourse").html(data);
                    }
                });
            }
            FillCourse();

            $("#ddlCourse").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLAdmissionBatchcode&values=" + ddlCourse.value + "",
                    success: function (data) {

                        $("#ddlBatch").html(data);
                    }
                });
            });

            $("#ddlBatch").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfManageEvent.php",
                    data: "action=FILLEventDDmode",
                    success: function (data) {
                        $("#ddlEname").html(data);

                    }
                });
            });
            $("#ddlEname").change(function () {
                filleventid();
            });
            function filleventid() {
                $.ajax({
                    type: "post",
                    url: "common/cfManageEvent.php",
                    data: "action=FILLEventIDddmode&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "&event=" + ddlEname.value + "",
                    success: function (data) {
                        ddlEID.value=data;
                    }
                });
            }

            $("#btnSubmit").click(function () {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfManageEvent.php"; // the script where you handle the form input.
                var data;

                if (Mode == 'Add')
                {
                    alert("mod");
                    data= "action=ADDddmodeitgk&eventid=" + ddlEID.value + ""; // serializes the form's elements.
                    alert(data);
                }
                else
                {

                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {   alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmuploaditgkddmode.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("frmuploaditgkddmode");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });
    </script>
    </html> 


    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>	