<?php
$title = "Final Approval Exam Choice";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code='" . $_REQUEST['code'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
	
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '1') {
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Final Approval For Exam Choice</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmexamchoicefinalapproval" id="frmexamchoicefinalapproval" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        
                            <div id="response"></div>

                             
                        <div id="errorBox"></div>


                         
                        <div class="container"  id='disapprove' style="display:none;">
                            
                            <div class="col-sm-5 form-group">     
                                <label for="learnercode">Reason for Deny:</label>
                                <input type="text" class="form-control"  name="txtReasion" id="txtReasion" maxlength="100">
                            </div>
							
							<div class="col-sm-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" style="margin-top:22px;" value="Submit"/> 
								
								 <input type="submit" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" style="margin-top:22px;" value="Cancel"/>
                            </div>
						 </div>
                    </div>
        </div>   
    </div>
</div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		
						if (Mode == 'Approve')
						{
							
								ApproveRecord();
								
							
						}
						
						
						function ApproveRecord()
						{
							$('#response').empty();
							$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
							$.ajax({
								type: "post",
								url: "common/cfexamchoicefinalapproval.php",
								data: "action=Approve&Code=" + Code + "",
								success: function (data) {
									//alert(data);
									if (data == SuccessfullyUpdate)
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
										window.setTimeout(function () {
										   window.location.href="frmexamchoicestatus.php";
									   }, 1000);
										
										Mode="Add";
										resetForm("frmexamchoicefinalapproval");
									}
									else
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
									}
									showData();
								}
							});
						}
						
						
						
						
						
						if (Mode == 'Disapprove')
						{
							$("#disapprove").show();
							
						}
						
						
						$("#btnSubmit1").click(function () 
						{
							resetForm("frmexamchoicefinalapproval");
							window.location.href = "frmexamchoicestatus.php";
						 
						});
						

						$("#btnSubmit").click(function ()
						{
						if ($("#frmexamchoicefinalapproval").valid())
						{

							$('#response').empty();
							$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
							var url = "common/cfexamchoicefinalapproval.php"; // the script where you handle the form input.            
							var data;
							var forminput=$("#frmexamchoicefinalapproval").serialize();
							if (Mode == 'Disapprove')
							{
							   data = "action=UPDATE&Code=" + Code + "&" + forminput; 
							}
							else
							{

							}
							$.ajax({
								type: "POST",
								url: url,
								data: data,
								success: function (data)
								{
									if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
										window.setTimeout(function () {
											window.location.href = "frmexamchoicestatus.php";
										}, 1000);

										Mode = "Disapprove";
										resetForm("form");
									}
									else
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
									}
									//showData();


								}
							});
						}	
							return false;
						});
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmexamchoiceapprove_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>