<?php
$title = "User Details";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code']) && !empty($_REQUEST['code'])) {
    echo "<script>var VisitCode=" . $_REQUEST['code'] . "</script>";
} else {
    echo "<script>window.location='frm_visit_requests.php'</script>";
}

$random = (mt_rand(1000, 9999));
$random .= date("y");
$random .= date("m");
$random .= date("d");
$random .= date("H");
$random .= date("i");
$random .= date("s");

echo "<script>var OrgDocId= '" . $random . "' </script>";
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<style type="text/css">

    form > div.container {
        filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
    }

    .modal-open .container-fluid, .modal-open .container{
        filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
    }
</style>
<div style="min-height:450px !important;max-height:1500px !important">
    <form name="frmvisitdetails" id="frmvisitdetails" action="common/cfvisitmaster.php" class="form-inline" enctype="multipart/form-data">
        <div class="container">
            <div class="container"><h3>Submit ITGK Visit Feedback</h3></div>
            <div class="container"><p>&nbsp;</p></div>
            <div class="container"><div id="response"></div></div>
            <div id="exTab3" class="container">
                <div class="contentwrapper"></div> 
                <ul  class="nav nav-pills">
                    <li class="active">
                        <a  href="#1b" class="tabbutton" id="visit_summary" data-toggle="tab">Visit Summary</a>
                    </li>
                    <li>
                        <a  href="#2b" class="tabbutton" id="fbtab1" data-toggle="tab">ITGK Details</a>
                        <input type="hidden" name="infodone" class="chksteps" id="tabresult1" value="0">
                    </li>
                    <li><a href="#3b" class="tabbutton" id="fbtab2" data-toggle="tab">ITGK Infrastructure</a>
                        <input type="hidden" class="chksteps" name="infradone" id="tabresult2" value="0">
                    </li>
                    <li><a href="#4b" class="tabbutton" id="fbtab3" data-toggle="tab">IT Infrastructure</a>
                        <input type="hidden" name="itinfradone" id="tabresult3" class="chksteps" value="0">
                    </li>
                    <li><a href="#5b" class="tabbutton" id="fbtab4" data-toggle="tab">Upload Photos</a>
                        <input type="hidden" name="photos" id="tabresult4" class="chksteps" value="0">
                    </li>
                    <li><a href="#6b" class="tabbutton" id="fbtab5" data-toggle="tab">Teaching/ Learning Methodology</a>
                        <input type="hidden" name="methoddone" id="tabresult5" class="chksteps" value="0">
                    </li>
                </ul>

                <div class="tab-content clearfix small">
                    <div class="tab-pane active" id="1b">
                        <div id="grid" name="grid" class="small" style="margin-top:35px;"> </div>
                    </div>

                    <div class="tab-pane" id="2b">
                        <fieldset>
                            <legend>ITGK Location</legend>
                            <div class="container">
                                <div class="col-sm-4 form-group">
                                    <label for="deptname"><b>ITGK Located on Address:</b></label> <br/> 
                                    <label class="radio-inline"> <input checked="checked" type="radio" id="Located_on_Address" value="Yes" name="Located_on_Address" /> Yes </label>
                                    <label class="radio-inline"> <input type="radio" id="Not_Located_on_Address" value="No"  name="Located_on_Address" /> No </label>
                                </div>    

                                <div class="col-sm-4 form-group"> 
                                    <label for="designation"><b>District:</b></label><br />
                                    <span id="itgk_District_Name"></span>
                                    <input type="hidden" name="ddlDistrict" id="ddlDistrict">
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <label for="marks"><b>Tehsil:</b></label><br />
                                    <span id="itgk_Tehsil_Name"></span>
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="attempt"><b>Address at the time of visit:</b></label><br />
                                    <span id="itgk_Organization_Address"></span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset><legend>Area Information</legend>
                            <div class="container">
                                <div class="col-sm-4 form-group">
                                    <label for="deptname"><b>Area details are correct:</b></label> <br/> 
                                    <label class="radio-inline"> <input checked="checked" type="radio" id="correctArea" class="correctArea" value="Yes" name="correctArea" /> Yes </label>
                                    <label class="radio-inline"> <input type="radio" id="incorrectArea" class="correctArea" value="No"  name="correctArea" /> No </label>
                                </div>
                                <div class="col-sm-4 form-group areaDetails">     
                                    <label for="area"><b>Area type:</b> <span id="itgk_Organization_AreaType"></span></label>
                                </div>
                                <div class="col-sm-4 form-group areaDetailsFields">     
                                    <label for="area"><b>Area type:</b><span class="star">*</span></label> 
                                    <label class="radio-inline"> <input type="radio" id="areaUrban" name="areaType" class="areaType" checked="checked" value="Urban" /> Urban </label>
                                    <label class="radio-inline"> <input type="radio" id="areaRural" name="areaType" class="areaType" value="Rural" /> Rural </label>
                                </div>
                            </div>
                            <div class="container areaDetails">
                                <div class="col-sm-4 form-group">
                                    &nbsp;
                                </div>
                                <div class="col-sm-4 form-group Urban areaFields"> 
                                    <label for="edistrict"><b>Municipality name: </b> </label><br /><span id="itgk_Municipality_Name"></span></label>
                                </div>

                                <div class="col-sm-4 form-group Urban areaFields"> 
                                    <label for="ddlWardno"><b>Ward No.: </b> </label><br /><span id="itgk_Ward_Name"></span></label>
                                </div>

                                <div class="col-sm-4 form-group Urban areaFields"> 
                                    <label for="landmark"><b>Landmark:</b> <br /><span id="itgk_Organization_Landmark"></span></label>
                                </div>

                                <div class="col-sm-4 form-group Rural areaFields"> 
                                    <label for="ddlPanchayat"><b>Panchayat samiti / Block: </b></label><br /><span id="itgk_Block_Name"></span></label>
                                </div>

                                <div class="col-sm-4 form-group Rural areaFields"> 
                                    <label for="edistrict"><b>Gram panchayat:</b></label><br /><span id="itgk_GP_Name"></span></label>
                                </div>

                                <div class="col-sm-4 form-group Rural areaFields"> 
                                    <label for="edistrict"><b>Village:</b></label><br /><span id="itgk_Village_Name"></span></label>
                                </div>
                            </div>
                            <div class="container areaDetails">
                                <div class="col-sm-4 form-group"> </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict"><b>Police station:</b></label><br /><span id="itgk_Organization_Police"></span></label>
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="gpfno"><b>Pincode:</b><br /><span id="itgk_Organization_PinCode"></span></label>
                                </div>
                            </div>
                            <div class="container areaDetailsFields">

                                <div class="col-sm-4 form-group">
                                    &nbsp;
                                </div>

                                <div class="col-sm-4 form-group Urban areaFields"> 
                                    <label for="edistrict"><b>Municipality name: </b></label><span class="star">*</span></label>
                                    <select id="ddlMunicipalName" name="ddlMunicipalName" class="form-control" >
                                        <option value="">Please select</option> 
                                    </select>
                                </div>

                                <div class="col-sm-4 form-group Urban areaFields"> 
                                    <label for="ddlWardno"><b>Ward No.: </b></label><span class="star">*</span></label>
                                    <select id="ddlWardno" name="ddlWardno" class="form-control" >
                                        <option value="">Please select</option> 
                                    </select>
                                </div>

                                <div class="col-sm-4 form-group Urban areaFields"> 
                                    <label for="landmark"><b>Landmark: </b></label>
                                    <input type="text" class="form-control"  name="landmark" id="fillitgk_Organization_Landmark" onkeypress="javascript:return AllowAddress(event);" placeholder="Landmark">
                                    <span id="landmark" style="font-size:10px;"></span>
                                </div>

                                <div class="col-sm-4 form-group Rural areaFields"> 
                                    <label for="ddlPanchayat"><b>Panchayat samiti / Block: </b></label><span class="star">*</span></label>
                                    <select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >
                                        <option value="">Please select</option>
                                    </select>
                                </div>

                                <div class="col-sm-4 form-group Rural areaFields"> 
                                    <label for="edistrict"><b>Gram panchayat: </b></label><span class="star">*</span></label>
                                    <select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control" >
                                        <option value="">Please select</option>
                                    </select>
                                </div>

                                <div class="col-sm-4 form-group Rural areaFields"> 
                                    <label for="edistrict"><b>Village: </b></label><span class="star">*</span></label>
                                    <select id="ddlVillage" name="ddlVillage" class="form-control" >
                                        <option value="">Please select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="container areaDetailsFields">
                                <div class="col-sm-4 form-group"> </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict"><b>Police station: </b></label><span class="star">*</span></label>
                                    <input type="text" class="form-control"  name="police_station" id="fillitgk_Organization_Police" onkeypress="javascript:return AllowAddress(event);" placeholder="Police station">
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="gpfno"><b>Pincode: </b><span class="star">*</span></label>
                                    <input type="text" class="keyup-cc form-control" maxlength="6" name="pincode" id="fillitgk_Organization_PinCode" data-mask="six-digits" placeholder="pincode">
                                    <span id="pincode" style="font-size:10px;"></span>
                                </div>
                                <div class="col-sm-4 form-group">
                                    <div class="areaDetailsFields">
                                        <label for="arearemark"><b>Remark about area:</b><span class="star">*</span></label>
                                        <input type="text" class="form-control"  name="arearemark" id="arearemark" onkeypress="javascript:return AllowAddress(event);" placeholder="Remark">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset><legend>Contact Details</legend>
                            <div class="container">
                                <div class="col-sm-4 form-group">
                                    <label for="deptname"><b>Contact details are correct:</b></label> <br/> 
                                    <label class="radio-inline"> <input checked="checked" type="radio" id="correctContact" class="correctContact" value="Yes" name="correctContact" /> Yes </label>
                                    <label class="radio-inline"> <input type="radio" id="incorrectContact" class="correctContact" value="No"  name="correctContact" /> No </label>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="gpfno"><b>Center Head / Owner:</b></label>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="gpfno"><b>Name: </b><span class="star">*</span></label>
                                    <input type="text" class="keyup-cc form-control contactDetailsFields" name="owner_name" id="fillitgk_UserProfile_FirstName" data-mask="charonly" placeholder="Full Name">
                                    <span class="contactDetailsInfo" id="itgk_UserProfile_FirstName"></span>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="bankdistrict"><b>Mobile number: </b><span class="star">*</span></label>
                                    <input type="text" placeholder="Mobile Number" class="form-control contactDetailsFields" id="fillitgk_UserProfile_Mobile" name="ownerMobileNumber" data-mask="ten-number" maxlength="10"/>
                                    <span class="contactDetailsInfo" id="itgk_UserProfile_Mobile"></span>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-sm-4 form-group">
                                    &nbsp;
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="gpfno"><b>RS-CIT Counsellor:</b></label>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="gpfno"><b>Name: </b><span class="star">*</span></label>
                                    <input type="text" class="keyup-cc form-control contactDetailsFields" name="counsellor_name" id="fillitgk_Counselor_Staff_Name" data-mask="charonly" placeholder="Full Name">
                                    <span class="contactDetailsInfo" id="itgk_Counselor_Staff_Name"></span>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="bankdistrict"><b>Mobile number: </b><span class="star">*</span></label>
                                    <input type="text" placeholder="Mobile Number" class="form-control contactDetailsFields" id="fillitgk_Counselor_Staff_Mobile" name="counsellorMobileNumber" data-mask="ten-number" maxlength="10"/>
                                    <span class="contactDetailsInfo" id="itgk_Counselor_Staff_Mobile"></span>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-sm-4 form-group">
                                    &nbsp;
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="gpfno"><b>RS-CIT Faculty:</b></label>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="gpfno"><b>Name: </b><span class="star">*</span></label>
                                    <input type="text" class="keyup-cc form-control contactDetailsFields" name="faculty_name" id="fillitgk_Faculty_Staff_Name" data-mask="charonly" placeholder="Faculty Name">
                                    <span class="contactDetailsInfo" id="itgk_Faculty_Staff_Name"></span>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="bankdistrict"><b>Mobile number: </b><span class="star">*</span></label>
                                    <input type="text" placeholder="Mobile Number" class="form-control contactDetailsFields" id="fillitgk_Faculty_Staff_Mobile" name="facultyMobileNumber" data-mask="ten-number" maxlength="10"/>
                                    <span class="contactDetailsInfo" id="itgk_Faculty_Staff_Mobile"></span>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-sm-4 form-group">
                                    &nbsp;
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="gpfno"><b>Lab Assistant/ Faculty:</b></label>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="gpfno"><b>Name: </b><span class="star">*</span></label>
                                    <input type="text" class="keyup-cc form-control contactDetailsFields" name="assistant_name" id="fillitgk_Co-ordinator_Staff_Name" data-mask="charonly" placeholder="Assistant Name">
                                    <span class="contactDetailsInfo" id="itgk_Co-ordinator_Staff_Name"></span>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="bankdistrict"><b>Mobile number: </b><span class="star">*</span></label>
                                    <input type="text" placeholder="Mobile Number" class="form-control contactDetailsFields" id="fillitgk_Co-ordinator_Staff_Mobile" name="assistantMobileNumber" data-mask="bhamashah" maxlength="10"/>
                                    <span class="contactDetailsInfo" id="itgk_Co-ordinator_Staff_Mobile"></span>
                                </div>
                            </div>
                            <div class="container contactDetailsFields">
                                <div class="col-sm-4 form-group">&nbsp;</div>
                                <div class="col-sm-4 form-group">
                                    <label for="contactremark"><b>About contacts:</b></label>
                                </div>
                                <div class="col-sm-4 form-group">
                                    <div class="">
                                        <label for="contactremark"><b>Remark:</b><span class="star">*</span></label><br />
                                        <input type="text" class="form-control"  name="contactremark" id="contactremark" onkeypress="javascript:return AllowAddress(event);" placeholder="Remark">
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <!-- End of tab 1b -->
                    </div>
                    <div class="tab-pane" id="3b">
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Department Location</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Area in Sq. Feet</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Seating Capacity</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Remark</b>:<span class="star">*</span></label>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Reception</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Sq.Ft Area" class="form-control" id="fillitgk_Reception_Area" name="receptionArea" data-mask="premium" maxlength="12"/>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Seating Capacity" class="form-control" id="fillitgk_Reception_Capacity" name="receptionSeatingCapacity" data-mask="five-digits" maxlength="5"/>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="fillitgk_Reception_Remark" name="receptionRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Waiting Area</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Sq.Ft Area" class="form-control" id="waitingArea" name="waitingArea" data-mask="premium" maxlength="12"/>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Seating Capacity" class="form-control" id="waitingAreaCapacity" name="waitingAreaCapacity" data-mask="five-digits" maxlength="5"/>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="waitingRemark" name="waitingRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Theory Room</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Sq.Ft Area" class="form-control" id="theoryArea" name="theoryArea" data-mask="premium" maxlength="12"/>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Seating Capacity" class="form-control" id="theoryAreaCapacity" name="theoryAreaCapacity" data-mask="five-digits" maxlength="5"/>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="theoryAreaRemark" name="theoryAreaRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Computer Lab</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Sq.Ft Area" class="form-control" id="labArea" name="labArea" data-mask="premium" maxlength="12"/>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Seating Capacity" class="form-control" id="labCapacity" name="labCapacity" data-mask="five-digits" maxlength="5"/>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="labRemark" name="labRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Toilet</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="ToiletAvail" value="Available" checked="checked" class="proofdoc"  name="Toilet" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="ToiletNotAvail" value="Not Available" name="Toilet" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="toiletRemark" name="toiletRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Drinking Water</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="DWAvail" value="Available" checked="checked" class="proofdoc"  name="DWAvail" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="DWNotAvail" value="Not Available" name="DWAvail" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group">
                                <input type="text" placeholder="Remark" class="form-control" id="waterRemark" name="waterRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Authorization Certificate</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="ACAvail" value="Available" checked="checked" name="ACAvail" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="ACNotAvail" value="Not Available" name="ACAvail" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group">
                                <input type="text" placeholder="Remark" class="form-control" id="ACRemark" name="ACRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Notice Board</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="NBAvail" value="Available" checked="checked" name="NBAvail" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="NBNotAvail" value="Not Available" name="NBAvail" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group">
                                <input type="text" placeholder="Remark" class="form-control" id="NBRemark" name="NBRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Enquiry Register</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="ERAvail" value="Available" checked="checked" name="ERAvail" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="ERNotAvail" value="Not Available" name="ERAvail" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group">
                                <input type="text" placeholder="Remark" class="form-control" id="ERRemark" name="ERRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Attendance Register</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="ARAvail" value="Available" checked="checked" name="ARAvail" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="ARAvailNotAvail" value="Not Available" name="ARAvail" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group">
                                <input type="text" placeholder="Remark" class="form-control" id="ARRemark" name="ARRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>
                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>500 Meter Visibility</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="poorVisiblity" value="Poor" name="Visiblity500" /> Poor </label>
                                <label class="radio-inline"> <input type="radio" id="goodVisiblity" checked="checked" value="Good" name="Visiblity500" />Good</label>
                            </div>

                            <div class="col-sm-4 form-group">
                                <label class="radio-inline"> <input type="radio" id="vgoodVisiblity" value="Very Good" name="Visiblity500" />Very Good</label>
                                <label class="radio-inline"> <input type="radio" id="execVisiblity" value="Excellent" name="Visiblity500" />Excellent</label>
                            </div>

                            <div class="col-sm-4 form-group">
                                <input type="text" placeholder="Remark" class="form-control" id="VisibilityRemark" name="VisibilityRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>ITGK Front Fascia</b>:<span class="star">*</span></label>
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="poorFrontFascia" value="Poor" name="FrontFascia" /> Poor </label>
                                <label class="radio-inline"> <input type="radio" id="goodFrontFascia" value="Good" checked="checked" name="FrontFascia" />Good</label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="vgoodFrontFascia" value="Very Good" name="FrontFascia" />Very Good</label>
                                <label class="radio-inline"> <input type="radio" id="execFrontFascia" value="Excellent" name="FrontFascia" />Excellent</label>
                            </div>

                            <div class="col-sm-4 form-group">
                                <input type="text" placeholder="Remark" class="form-control" id="FrontFaceRemark" name="FrontFaceRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <!-- End of tab 2b -->
                    </div>
                    <div class="tab-pane" id="4b">
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Printer</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="printerAvail" value="Available" checked="checked" name="printer" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="printerNotAvail" value="Not Available" name="printer" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="printerRemark" name="printerRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Scanner</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="scannerAvail" value="Available" checked="checked" name="scanner" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="scannerNotAvail" value="Not Available" name="scanner" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="scannerRemark" name="scannerRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>
                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Internet</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="internetAvail" checked="checked" value="Available" name="internet" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="internetNotAvail" value="Not Available" name="internet" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="MBPS" class="form-control" id="mbpsRemark" name="mbpsRemark" data-mask="three-digits" maxlength="3"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Headphone</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="headphoneAvail" checked="checked" value="Available" name="headphone" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="headphoneNotAvail" value="Not Available" name="headphone" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Qty." class="form-control" id="hphoneQty" name="hphoneQty" data-mask="three-digits" maxlength="3"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>LAN</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="lanAvail" value="Available" checked="checked" name="lanAvail" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="lanNotAvail" value="Not Available" name="lanAvail" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="lanRemark" name="lanRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Webcam</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="webcamAvail" value="Available" checked="checked" name="webcamAvail" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="webcamNotAvail" value="Not Available" name="webcamAvail" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="webCamRemark" name="webCamRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Power Backup</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="pbAvail" value="Available" checked="checked" name="pbAvail" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="pbNotAvail" value="Not Available" name="pbAvail" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="bkpRemark" name="bkpRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>

                        </div>

                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict"><b>Biometrics</b>:<span class="star">*</span></label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="biometricsAvail" value="Available" checked="checked" name="biometricsAvail" /> Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label class="radio-inline"> <input type="radio" id="biometricsNotAvail" value="Not Available" name="biometricsAvail" />Not Available </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <input type="text" placeholder="Remark" class="form-control" id="bioRemark" name="bioRemark" onkeypress="javascript:return AllowAddress(event);"/>
                            </div>
                        </div>
                        <div class="container"><center><h6>System Details (Which are Available & Best configured in computer LAB)</h6></center></div>
                        <div class='table-responsive'>
                            <table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'><thead><tr>
                                        <th>SNo.</th>
                                        <th>Processer <span class="star">*</span></th>
                                        <th>RAM(GB) <span class="star">*</span></th>
                                        <th>HDD(GB) <span class="star">*</span></th>
                                        <th>Qty. <span class="star">*</span></th>
                                        <th>Type(S/C) <span class="star">*</span></th>
                                        <th>Remark <span class="star">*</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td><input placeholder="Processer" class="form-control" id="fillitgk_processer1" name="processer1" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                        <td><input placeholder="RAM" class="form-control" id="fillitgk_ram1" name="ram1" data-mask="two-digits" maxlength="2"  type="text"></td>
                                        <td><input placeholder="HDD" class="form-control" id="fillitgk_hdd1" name="hdd1" data-mask="four-digits" maxlength="4"  type="text"></td>
                                        <td><input placeholder="Qty." class="form-control" id="fillitgk_qty1" name="qty1" data-mask="three-digits" maxlength="3"  type="text"></td>
                                        <td><input placeholder="Type(S/C)" class="form-control" id="fillitgk_typesc1" name="typesc1" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                        <td><input placeholder="Remark" class="form-control" id="fillitgk_systemRemark1" name="systemRemark1" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td><input placeholder="Processer" class="form-control" id="fillitgk_processer2" name="processer2" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                        <td><input placeholder="RAM" class="form-control" id="fillitgk_ram2" name="ram2" data-mask="two-digits" maxlength="2"  type="text"></td>
                                        <td><input placeholder="HDD" class="form-control" id="fillitgk_hdd2" name="hdd2" data-mask="four-digits" maxlength="4"  type="text"></td>
                                        <td><input placeholder="Qty." class="form-control" id="fillitgk_qty2" name="qty2" data-mask="three-digits" maxlength="3"  type="text"></td>
                                        <td><input placeholder="Type(S/C)" class="form-control" id="fillitgk_typesc2" name="typesc2" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                        <td><input placeholder="Remark" class="form-control" id="fillitgk_systemRemark2" name="systemRemark2" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td><input placeholder="Processer" class="form-control" id="fillitgk_processer3" name="processer3" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                        <td><input placeholder="RAM" class="form-control" id="fillitgk_ram3" name="ram3" data-mask="two-digits" maxlength="2"  type="text"></td>
                                        <td><input placeholder="HDD" class="form-control" id="fillitgk_hdd3" name="hdd3" data-mask="four-digits" maxlength="4"  type="text"></td>
                                        <td><input placeholder="Qty." class="form-control" id="fillitgk_qty3" name="qty3" data-mask="three-digits" maxlength="3"  type="text"></td>
                                        <td><input placeholder="Type(S/C)" class="form-control" id="fillitgk_typesc3" name="typesc3" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                        <td><input placeholder="Remark" class="form-control" id="fillitgk_systemRemark3" name="systemRemark3" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td><input placeholder="Processer" class="form-control" id="fillitgk_processer4" name="processer4" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                        <td><input placeholder="RAM" class="form-control" id="fillitgk_ram4" name="ram4" data-mask="two-digits" maxlength="2"  type="text"></td>
                                        <td><input placeholder="HDD" class="form-control" id="fillitgk_hdd4" name="hdd4" data-mask="four-digits" maxlength="4"  type="text"></td>
                                        <td><input placeholder="Qty." class="form-control" id="fillitgk_qty4" name="qty4" data-mask="three-digits" maxlength="3"  type="text"></td>
                                        <td><input placeholder="Type(S/C)" class="form-control" id="fillitgk_typesc4" name="typesc4" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                        <td><input placeholder="Remark" class="form-control" id="fillitgk_systemRemark4" name="systemRemark4" onkeypress="javascript:return AllowAddress(event);"  type="text"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- End of tab 3b -->
                    </div>

                    <div class="tab-pane" id="5b">
                        <div class="panel panel-info">
                            <div class="panel-heading">Upload Photos of Visited Center</div>
                            <div class="panel-body">
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <!--<input type="text" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId" value="<?php echo $random; ?>" />-->
                                <div class="panel panel-danger form-group ">
                                    <div class="panel-heading">Upload Exterior Photo</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach Exterior Photo:<span class="star">*</span></label>
                                            <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->								  
                                            <input style=" width: 115%" id="uploadImage7" type="file" name="uploadImage7" onchange="ValidateSingleInput(this);"/>	
                                            <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                            <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                                        </div>
                                    </div>
                                </div>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <div class="panel panel-danger form-group">
                                    <div class="panel-heading">Upload Reception Photo</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach Reception Photo:<span class="star">*</span></label> 
                                            <!--<img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage8').click();">-->								  
                                            <input style=" width: 115%" id="uploadImage8" type="file" name="uploadImage8" onchange="ValidateSingleInput(this);"/>	
                                            <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                                        </div>
                                    </div>
                                </div>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <div class="panel panel-danger form-group">
                                    <div class="panel-heading">Upload Classroom Photo</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach Classroom Photo:<span class="star">*</span></label> 
                                            <!--<img id="uploadPreview9" src="images/sampleproof.png" id="uploadPreview9" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage9').click();">-->								  
                                            <input style=" width: 115%" id="uploadImage9" type="file" name="uploadImage9" onchange="ValidateSingleInput(this);"/>	
                                            <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                                        </div>
                                    </div>
                                </div>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;                        
                                <div class="panel panel-danger form-group">
                                    <div class="panel-heading">Upload Lab Photo</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach Lab Photo:<span class="star">*</span></label>  
                                            <!--<img id="uploadPreview6" src="images/sampleproof.png" id="uploadPreview6" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage6').click();">-->								  
                                            <input style=" width: 115%" id="uploadImage6" type="file" name="uploadImage6" onchange="ValidateSingleInput(this);"/>	
                                            <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span10
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="6b">
                    <div class="container">

                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict"><b>LMS Status</b>:<span class="star">*</span></label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" checked="checked" id="LMSStatus" value="Installed" name="LMSStatus" /> Installed </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="LMSStatusNotAvail" value="Not Installed" name="LMSStatus" />Not Installed </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <input type="text" placeholder="Remark" class="form-control" id="lmsRemark" name="lmsRemark" onkeypress="javascript:return AllowAddress(event);"/>
                        </div>

                    </div>
                    <div class="container">

                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict"><b>Teaching Method</b>:<span class="star">*</span></label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="TeachingMethod" checked="checked" value="LMS Based" name="TeachingMethod" /> LMS Based </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="TMNotAvail" value="Offline Teaching" name="TeachingMethod" />Offline Teaching </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <input type="text" placeholder="Remark" class="form-control" id="teachRemark" name="teachRemark" onkeypress="javascript:return AllowAddress(event);"/>
                        </div>

                    </div>
                    <div class="container">

                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict"><b>Assessment</b>:<span class="star">*</span></label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="Assessment" value="Regular" checked="checked" name="Assessment" /> Regular </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="AssessmentNotAvail" value="At End of the Course" name="Assessment" />At End of the Course </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <input type="text" placeholder="Remark" class="form-control" id="assisRemark" name="assisRemark" onkeypress="javascript:return AllowAddress(event);"/>
                        </div>

                    </div>
                    <div class="container">

                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict"><b>Faculty Quality</b>:<span class="star">*</span></label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="FacultyQuality" value="Excellent" name="FacultyQuality" /> Excellent </label>
                            <label class="radio-inline"> <input type="radio" id="FacultyQualityVgood" value="Very Good" name="FacultyQuality" /> Very Good </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="FacultyQualityGood" checked="checked" value="Good" name="FacultyQuality" />Good </label>
                            <label class="radio-inline"> <input type="radio" id="FacultyQualityPoor" value="Poor" name="FacultyQuality" />Poor </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <input type="text" placeholder="Remark" class="form-control" id="facultyRemark" name="facultyRemark" onkeypress="javascript:return AllowAddress(event);" />
                        </div>

                    </div>
                    <div class="container">

                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict"><b>Learning Quality</b>:<span class="star">*</span></label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="LearningQualityExec" value="Excellent" name="LearningQuality" /> Excellent </label>
                            <label class="radio-inline"> <input type="radio" id="LearningQualityVgood" value="Very Good" name="LearningQuality" /> Very Good </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="LearningQualityGood" checked="checked" value="Good" name="LearningQuality" />Good </label>
                            <label class="radio-inline"> <input type="radio" id="LearningQualityPoor" value="Poor" name="LearningQuality" />Poor </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <input type="text" placeholder="Remark" class="form-control" id="learningRemark" name="learningRemark" onkeypress="javascript:return AllowAddress(event);" />
                        </div>

                    </div>
                    <div class="container">

                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict"><b>ITGK Infrastructure</b>:<span class="star">*</span></label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="InfraQualityExec" value="Excellent" name="InfraQuality" /> Excellent </label>
                            <label class="radio-inline"> <input type="radio" id="InfraQualityVgood" value="Very Good" name="InfraQuality" /> Very Good </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label class="radio-inline"> <input type="radio" id="InfraQualityGood" checked="checked" value="Good" name="InfraQuality" />Good </label>
                            <label class="radio-inline"> <input type="radio" id="InfraQualityPoor" value="Poor" name="InfraQuality" />Poor </label>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <input type="text" placeholder="Remark" class="form-control" id="infraqtyRemark" name="infraqtyRemark" onkeypress="javascript:return AllowAddress(event);" />
                        </div>

                    </div>
                    <div class="container">

                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict"><b>Familiar with RKCL</b></label>
                        </div>
                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict"><b>MyRKCL<span class="star">*</span></b></label><br />
                            <label class="radio-inline"> <input type="radio" id="LFeedback" value="Yes" checked="checked" name="LFeedback" />Yes </label>
                            <label class="radio-inline"> <input type="radio" id="LFeedbackNo" value="No" name="LFeedback" />No </label>
                        </div>
                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict"><b>RKCL LMS<span class="star">*</span></b></label><br />
                            <label class="radio-inline"> <input type="radio" id="LFeedback1" value="Yes" checked="checked" name="LFeedback1" />Yes </label>
                            <label class="radio-inline"> <input type="radio" id="LFeedbackNo" value="No" name="LFeedback1" />No </label>
                        </div>
                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict"><b>LMS based Assessment<span class="star">*</span></b></label><br />
                            <label class="radio-inline"> <input type="radio" id="LFeedback2" value="Yes" checked="checked" name="LFeedback2" />Yes </label>
                            <label class="radio-inline"> <input type="radio" id="LFeedbackNo" value="No" name="LFeedback2" />No </label>
                        </div>
                    </div>
                    <div id="center_batches"></div>

                    <div class="container">
                        <div class="col-sm-4 form-group">
                        <label for="bankdistrict"><b>Visitor must attend a class and interact with students (Overall Remark for Academics)</b>:<span class="star">*</span></label>
                        </div> 
                        <div class="col-sm-4 form-group">
                        <select id="txtOverAllRemark" name="txtOverAllRemark" class="form-control" >
                                        <option selected="true" value="">Select</option>
                                        <option value="Excellent" >Excellent</option>
                                        <option value="Very_Good">Very Good</option>
                                        <option value="Good" >Good</option>
                                        <option value="OK" >OK</option>
                                        <option value="Poor" >Poor</option>
                                    </select>
                        </div>
                        <!--<textarea class="form-control" style="width: 372px;" rows="3" id="txtOverAllRemark" name="txtOverAllRemark" onkeypress="javascript:return AllowAddress(event);" placeholder="Over All Remark"></textarea>-->
                    </div>
                </div>

            </div>
            <div class="container"><p>&nbsp;</p></div>
            <div class="container startfeedback">
                <div class="col-sm-4 form-group"></div>
                <div class="col-sm-4 form-group text-right"><input type="button" id="btnStart" name="start" value="Start Feedback"></div>
            </div>
            <div class="container secnext areaFieldsHide">
                <div class="col-sm-4 form-group"></div>
                <div class="col-sm-4 form-group text-right"><input type="button" id="btnNext" name="next" value="Save and Next"></div>
            </div>

            <div class="container secsubmit areaFieldsHide">
                <div class="col-sm-4 form-group"></div>
                <div class="col-sm-4 form-group text-right"><input type="submit" id="btnSubmit" name="btnSubmit" value="Submit Feedback"></div>
            </div>


        </div>
        <input type="hidden" class="form-control" maxlength="50" name="VisitCode" id="VisitCode" value="<?php echo $_REQUEST['code'] ?>"/>
         <input type="hidden" class="form-control" maxlength="50" name="action" id="action" value="savefeedback"/>
         <!-- <input type="hidden" class="form-control" maxlength="50" name="txtOverAllRemark" id="txtOverAllRemark" value=/> -->
         <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/> 

 
</div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script language="javascript" type="text/javascript">
    var _validFileExtensions = [".jpg"];
    function ValidateSingleInput(oInput) {
        if (oInput.type == "file") {
            var sFileName = oInput.value;

            var iFileSize = oInput.files[0].size;

            if (sFileName.length > 0) {
                if (iFileSize > 100000 && iFileSize < 200000) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }

                } else {
                    alert("File Size should be between 100KB to 200KB.");
                    oInput.value = "";
                }
            }
        }
        return true;
    }
</script>
<style>

    /* change border radius for the tab , apply corners on top*/

    #exTab3 .nav-pills > li > a {
        border-radius: 4px 4px 0 0 ;
        color: #ffffff !important;
        font-weight: bold;
    }

    #exTab3 .nav-pills > li {
        background-color: #f66937;
        color: #ffffff !important;
        border-radius: 4px 4px 0 0 ;
    }

    #exTab3 .nav-pills > li:hover {
        background-color: #428bca !important;
    }

    #exTab3 .tab-content {
        background-color: #f9f9f9;
        padding : 15px;
        border:1px solid #f66937;
    }


    #errorBox{
        color:#F00;
    }
    .aslink {
        cursor: pointer;
    }
    .areaFieldsHide {
        display: none !important;
    }
    .contentReadonly {
        height: 650px;
        position: absolute;
        width: 1170;
        z-index: 1000;
        margin-top: 50px;
    }

    .aboveContener {
        position: absolute;
        z-index: 2000;
        margin-top: -20px;
    }

    legend {
        background-color: #428bca;
        padding: 5px;
        color: #ffffff;
        border-radius: 4px 4px 0 0 ;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    
//       $("#txtOverAllRemark").change(function () {
//            var selOwnType = $(this).val();
//            alert(selOwnType);
//
//        });

    function AllowAddress(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9a-zA-Z.-/ ,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }

    function listvisits() {
        $("#grid").empty();
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=listvisits&VisitCode=" + VisitCode,
            success: function (data) {
                //alert(data);
                if (data != '') {
                    $('#response').empty();
                    $("#grid").html(data);
                } else {
                    window.location = 'frm_visit_requests.php';
                }
            }
        });
    }

    function getvisitfeedback() {
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=getvisitfeedback&VisitCode=" + VisitCode,
            success: function (data) {
                data = data.trim();
                if (data != '') {
                    $('#response').empty();
                    $('#response').html("<p><div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>Your feedback registred for this visit. For download please <a href='javascript:void(0);' onclick='javascript:downloadFeedback(VisitCode);'>Click Here</a></span></div></p>");
                    $('.startfeedback').remove();
                    $('.secsubmit').remove();
                    $('.secnext').remove();
                    $('.contentwrapper').addClass('contentReadonly');
                    showfeedbackdata(data);
                }
            }
        });
    }

    function downloadFeedback(VCode) {
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=doAction&id=dfb-" + VCode,
            success: function (data) {
                data = data.trim();
                if (data != '') {
                    //alert(data);
                    window.location = 'downloadvisitfb.php?path=' + data;
                }
            }
        });
    }

    function showfeedbackdata(data) {
        var fdata = $.parseJSON(data);
        $('input, select, textarea').each(
                function (index) {
                    var field = $(this);
                    var ftype = field.attr('type');
                    if (ftype != 'button') {
                        var fname = field.attr('name');
                        if (fname != 'undefined') {
                            if (ftype == 'radio') {
                                $("input[name=" + fname + "][value='" + fdata[fname] + "']").prop("checked", true);
                            } else {
                                field.val(fdata[fname]);
                            }
                        }
                    }
                });
    }

    function verifyTabsData(tid, showas) {
        var tabid = '#fbtab' + tid;
        //alert("tabid:"+tabid);
        var tabresult = '#tabresult' + tid;
        //alert("tabresult1:"+tabresult);
        var isvalid = false;
        $(tabid).click();
        if (showas != 'start') {
            isvalid = $('#frmvisitdetails').valid();
        } else if ($(tabresult).val() == 1) {
            isvalid = true;
        }

        return isvalid;
    }

    function validateTabs(id, showas) {
        var isvalid = false;
        var tabresult = '#tabresult' + id;
        //alert("tabresult:"+tabresult);
        isvalid = verifyTabsData(id, showas);
        if (isvalid) {
            $(tabresult).val(1);
            id = id + 1;
            if (id < 6)
                isvalid = validateTabs(id, showas);
        } else {
            $(tabresult).val(0);
        }

        return isvalid;
    }

    function FillPanchayat() {
        var selDistrict = ddlDistrict.value;
        $.ajax({
            url: 'common/cfCenterlocation.php',
            type: "post",
            data: "action=FILLPanchayat&values=" + selDistrict + "",
            success: function (data) {data = data.trim();
                $('#ddlPanchayat').html(data);
            }
        });
    }

    function FillMunicipalName() {
        var selDistrict = ddlDistrict.value;
        $.ajax({
            url: 'common/cfCenterlocation.php',
            type: "post",
            data: "action=FillMunicipalName&values=" + selDistrict + "",
            success: function (data) {data = data.trim();
                $('#ddlMunicipalName').html(data);
            }
        });
    }
    //$('#txtGenerateId').val(OrgDocId);
    function saveFormValues() {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
       // var formdata2 = $("#frmvisitdetails").serialize();
//        var OverAllRemarks = txtOverAllRemark.value;
//        //alert(OverAllRemarks);
//        var file_data1 = $('#uploadImage7').prop('files')[0];   
//        var file_data2 = $('#uploadImage8').prop('files')[0];
//        var file_data3 = $('#uploadImage9').prop('files')[0];
//        var file_data4 = $('#uploadImage6').prop('files')[0];
//        var form_data = new FormData(this);                  
//        form_data.append('file1', file_data1)
//        form_data.append('file2', file_data2)
//        form_data.append('file3', file_data3)
//        form_data.append('file4', file_data4)
//        form_data.append("data", formdata),
//        form_data.append('action', "savefeedback")
//        form_data.append("VisitCode", VisitCode)
//form_data.append("txtGenerateId", OrgDocId)
//form_data.append("txtOverAllRemark", OverAllRemarks)
        $.ajax({
//            type: "post",
//                cache: false,
//                contentType: false,
//                processData: false,
//            url: "common/cfvisitmaster.php",
//            data: "action=savefeedback&VisitCode=" + VisitCode + "&" + formdata2 + "",
//            
           url: "common/cfvisitmaster.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,

            //data: formdata, 
            success: function (data) {data = data.trim();
                if (data != '') {
                    $('#response').empty();
                    getvisitfeedback();
                } else {
                    $('#response').html(data);
                }
            }
        });
    }

    function fetchCenterBatches() {
        $('#center_batches').empty();
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=fetchCenterBatches&VisitCode=" + VisitCode,
            success: function (data) {data = data.trim();
                if (data != '') {
                    $('#center_batches').html(data);
                }
            }
        });
    }

    function FillITGKDetails() {
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=getITGKInfo&VisitCode=" + VisitCode,
            success: function (data) {data = data.trim();
                if (data != '') {
                    data = $.parseJSON(data);
                    $.each(data, function (key, item) {
                        var fldid = '#itgk_' + key;
                        var fldidVal = '#fillitgk_' + key;
                        item = (item) ? item : 'N/A';
                        if ($(fldid).length) {
                            $(fldid).html(item);
                        }
                        if ($(fldidVal).length) {
                            $(fldidVal).val(item);
                        }
                        if (key == 'Organization_AreaType' && item == 'Rural') {
                            $('#areaRural').click();
                        }
                        if (item == 'N/A') {
                            $(fldidVal).val('');
                            if (key == 'UserProfile_FirstName' || key == 'UserProfile_Mobile' || key == 'Counselor_Staff_Name' || key == 'Counselor_Staff_Mobile' || key == 'Faculty_Staff_Name' || key == 'Faculty_Staff_Mobile' || key == 'Co-ordinator_Staff_Name' || key == 'Co-ordinator_Staff_Mobile') {
                                $(fldid).addClass('areaFieldsHide');
                                $(fldidVal).removeClass('areaFieldsHide');
                            }
                        }
                    });
                    $('#ddlDistrict').val(data['Organization_District']);
                    FillPanchayat();
                    FillMunicipalName();
                }
            }
        });
    }

    $(document).ready(function () {

        $('.Rural').addClass('areaFieldsHide');
        $('.areaDetailsFields').addClass('areaFieldsHide');
        $('.contactDetailsFields').addClass('areaFieldsHide');
        FillITGKDetails();
        getvisitfeedback();
        listvisits();
        fetchCenterBatches();
        getvisitfeedback();

        $("#ddlMunicipalName").change(function () {
            var selMunicipalName = $(this).val();
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FILLWardno&values=" + selMunicipalName + "",
                success: function (data) {data = data.trim();
                    $('#ddlWardno').html(data);
                }
            });
        });


        $("#ddlPanchayat").change(function () {
            var selPanchayat = $(this).val();
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                success: function (data) {data = data.trim();
                    $('#ddlGramPanchayat').html(data);
                }
            });
        });

        $("#ddlGramPanchayat").change(function () {
            var selGramPanchayat = $(this).val();
            $.ajax({
                url: 'common/cfVillageMaster.php',
                type: "post",
                data: "action=FILL&values=" + selGramPanchayat + "",
                success: function (data) {data = data.trim();
                    $('#ddlVillage').html(data);
                }
            });
        });

        $("#areaUrban").click(function () {
            $('#ddlGramPanchayat').val('');
            $('#ddlPanchayat').val('');
            $('#ddlVillage').val('');
            FillMunicipalName();
        });
        $("#areaRural").click(function () {
            $('#ddlMunicipalName').val('');
            $('#ddlWardno').val('');
            FillPanchayat();
        });

        $("#btnStart").click(function () {
            validateTabs(1, 'start');
        });

        $("#btnNext").click(function () {
            validateTabs(1, 'next');
        });

        $(".correctArea").click(function () {
            var isvalid = $('#correctArea').is(':checked');
            var isvalid = $(this).val();
            if (isvalid == 'Yes') {
                $('.areaDetailsFields').addClass('areaFieldsHide');
                $('.areaDetails').removeClass('areaFieldsHide');
            } else {
                $('.areaDetails').addClass('areaFieldsHide');
                $('.areaDetailsFields').removeClass('areaFieldsHide');
            }
        });

        $(".correctContact").click(function () {
            var isvalid = $(this).val();
            if (isvalid == 'Yes') {
                $('.contactDetailsInfo').removeClass('areaFieldsHide');
                $('.contactDetailsFields').addClass('areaFieldsHide');
                isvalid = $('#frmvisitdetails').valid();
            } else {
                $('.contactDetailsInfo').addClass('areaFieldsHide');
                $('.contactDetailsFields').removeClass('areaFieldsHide');
            }
        });

        //$("#btnSubmit").click(function () {
             $("#frmvisitdetails").on('submit',(function(e) {
  e.preventDefault();
  $('#txtGenerateId').val(OrgDocId);
            var notLocated = $('#Not_Located_on_Address').is(':checked');
            if (notLocated) {
                $('#fbtab1').click();
                var isvalid = confirm('Are you sure, to submit feedback as `This itgk is not located on his registred  address` ? Press `Cancel` if located.');
                if (isvalid) {
                    $('#Located_on_Address').click();
                    $('.contentwrapper').addClass('contentReadonly');
                    $('.secsubmit').addClass('areaFieldsHide');
                    //saveFormValues();


                            $.ajax({
       
           url: "common/cfvisitmaster.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,

            //data: formdata, 
            success: function (data) {data = data.trim();
                if (data != '') {
                    $('#response').empty();
                    getvisitfeedback();
                } else {
                    $('#response').html(data);
                }
            }
        });
                } else {
                    $('#Located_on_Address').click();
                    $('.contentwrapper').removeClass('contentReadonly');
                    $('.secsubmit').removeClass('aboveContener');
                }
            } else {
                var isvalid = validateTabs(1, 'final');
                if (isvalid) {
                    var confirmSubmit = confirm('Are you sure, to register your final feedback for this ITGK ?\n\nNo further changes will possible in feedback details, after submit at this point.');
                    if (confirmSubmit) {
                        // saveFormValues();


                                $.ajax({
           
           url: "common/cfvisitmaster.php",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            processData:false,

            //data: formdata, 
            success: function (data) {data = data.trim();
                if (data != '') {
                    $('#response').empty();
                    getvisitfeedback();
                } else {
                    $('#response').html(data);
                }
            }
        });
                    }
                }
            }
        }));

        $('#Not_Located_on_Address').click(function () {
            var isvalid = confirm('Are you sure, this itgk is not located on his registred address ? Press `Cancel` if located.');
            if (isvalid) {
                $('.contentwrapper').addClass('contentReadonly');
                $('.secsubmit').removeClass('areaFieldsHide');
                $('.secsubmit').addClass('aboveContener');
                $('.startfeedback').remove();
                $('.secnext').remove();
            } else {
                $('#Located_on_Address').click();
            }
        });

        $(".areaType").change(function () {
            $('.areaFields').addClass('areaFieldsHide');
            $('.areaFields > input').val('');
            $('.' + this.value).removeClass('areaFieldsHide');
        });

        $(".tabbutton").click(function () {
            if ($('#Not_Located_on_Address').is(':checked') || !$('.secnext').length)
                return;
            var href = $(this).attr('href');
            if (href == '#1b') {
                $('.startfeedback').removeClass('areaFieldsHide');
                $('.contentReadonly').addClass('areaFieldsHide');
                $('.secsubmit').addClass('areaFieldsHide');
                $('.secnext').addClass('areaFieldsHide');
            } else if (href == '#6b') {
                $('.secsubmit').removeClass('areaFieldsHide');
                $('.contentReadonly').removeClass('areaFieldsHide');
                $('.startfeedback').addClass('areaFieldsHide');
                $('.secnext').addClass('areaFieldsHide');
            } else {
                $('.secsubmit').addClass('areaFieldsHide');
                $('.startfeedback').addClass('areaFieldsHide');
                $('.secnext').removeClass('areaFieldsHide');
                $('.contentReadonly').removeClass('areaFieldsHide');
            }
        });

        $("#grid").on('click', '.aslink', function () {
            performAction(this);
        });


    });


</script>
<script src="js/frmvisitFeedback_validation.js" type="text/javascript"></script>
</html>