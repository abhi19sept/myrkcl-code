<?php
$title = "Close Visit";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['visitcode'])) {
    echo "<script>var visitcode=" . $_REQUEST['visitcode'] . "</script>";
    //echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var DeviceCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}

echo "<script>var UserRoll='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Close Visit

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
                <form name="frmselectvisit" id="frmselectvisit" class="form-inline" action=""> 
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Select Center Code:</label>
                            <select id="ddlCC" name="ddlCC" class="form-control" >

                            </select>    
                        </div>
                        <div class="col-sm-4 form-group" id="showdiv"> 
                            <label for="edistrict">Click Here to View Details:</label>
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="View Details"/>    
                        </div>

                    </div> 
                    <div class="panel panel-info" id="main-content" style="display:none;">
                        <div class="panel-heading">ITGK Details</div>
                        <div class="panel-body">

                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="address">ITGK Code:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtITGKCode" id="txtITGKCode" placeholder="Street">
                                    <input type="hidden" class="form-control" readonly="true" name="txtvisitid" id="txtvisitid" placeholder="Street">    
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="address">ITGK Name:</label>
                                    <input type="text" class="form-control" readonly="true" id="txtITGKName" name="txtITGKName" placeholder="Road">

                                </div>
                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Visit Date:</label>
                                    <input type="text"  class="form-control" readonly="true" name="txtVisitDate" id="txtVisitDate" placeholder="Name of the Organization/Center">
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">Status:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtStatus1" id="txtStatus1" placeholder="Registration No">     
                                </div>



                            </div>

                            <!--                            <div class="container">
                                                            <div class="col-sm-4 form-group"> 
                                                                <label for="edistrict">District:</label>
                                                                <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                                                            </div>
                            
                                                            <div class="col-sm-4 form-group"> 
                                                                <label for="edistrict">Tehsil:</label>
                                                                <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                                                            </div>
                                                            <div class="col-sm-4 form-group">     
                                                                <label for="faname">Email Id:</label>
                                                                <input type="text" class="form-control" readonly="true" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                                                            </div>
                            
                                                            <div class="col-sm-4 form-group"> 
                                                                <label for="edistrict">Type of Organization:</label>
                                                                <input type="text" class="form-control" readonly="true" name="txtType" id="txtType" placeholder="Type Of Organization">  
                                                            </div>
                            
                                                        </div>-->
                            <div class="container">
                                <div style="display:none;">

                                    <div style="display:none;">
                                        <div class="container">
                                            <div class="col-md-4 form-group">
                                                <label for="batch"> Quality:<span class="star">*</span></label>
                                                <input type="text" name="txtQuality" id="txtQuality" value="" class="form-control"
                                                       readonly="true"/>
                                                <input type="hidden" name="admission_code" id="admission_code"
                                                       value="<?php echo $_REQUEST['code']; ?>">
                                                <input type="hidden" name="txtLearnerName" id="txtLearnerName"/>
                                            </div>

                                            <div class="col-md-4 form-group">
                                                <label for="batch"> NFIQ:<span class="star">*</span></label>
                                                <input type="text" name="txtNFIQ" id="txtNFIQ" value="" class="form-control"
                                                       readonly="true"/>
                                            </div>
                                        </div>

                                        <div class="container">
                                            <div class="col-md-10 ">
                                                <label for="batch"> Base64Encoded ISO Template:<span class="star">*</span></label>
                                                <textarea id="txtIsoTemplate" rows="4" cols="12" name="txtIsoTemplate"
                                                          class="form-control" readonly="true"></textarea>
                                            </div>
                                        </div>

                                        <div class="container">
                                            <div class="col-md-10">
                                                <label for="batch"> Base64Encoded ISO Image:<span class="star">*</span></label>
                                                <textarea id="txtIsoImage" rows="4" cols="12" name="txtIsoImage" class="form-control"
                                                          readonly="true"></textarea>
                                            </div>
                                        </div>

                                        <div class="container">
                                            <div class="col-md-10">
                                                <label for="batch"> Base64Encoded Raw Data:<span class="star">*</span></label>
                                                <textarea id="txtRawData" rows="4" cols="12" name="txtRawData" class="form-control"
                                                          readonly="true"></textarea>
                                            </div>
                                        </div>

                                        <div class="container">
                                            <div class="col-md-10">
                                                <label for="batch"> Base64Encoded Wsq Image Data:<span class="star">*</span></label>
                                                <textarea id="txtWsqData" rows="4" cols="12" name="txtWsqData" class="form-control"
                                                          readonly="true"></textarea>
                                            </div>
                                        </div>
                                        <textarea id="txtlearnercode" rows="4" cols="12" name="txtlearnercode" class="form-control"
                                                  readonly="true"></textarea>

                                    </div>

                                    <div class="container">
                                        <div class="col-md-4 form-group">
                                            <label for="batch"> Serial No:<span class="star">*</span></label>
                                            <input type="text" name="tdSerial" id="tdSerial" class="form-control" readonly="true"/>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label for="batch"> Make:<span class="star">*</span></label>
                                            <input type="text" name="tdMake" id="tdMake" class="form-control" readonly="true"/>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label for="batch"> Model:<span class="star">*</span></label>
                                            <input type="text" name="tdModel" id="tdModel" class="form-control" readonly="true"/>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label for="batch"> Width:<span class="star">*</span></label>
                                            <input type="text" name="tdWidth" id="tdWidth" class="form-control" readonly="true"/>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <div class="col-md-4 form-group">
                                            <label for="batch"> Height:<span class="star">*</span></label>
                                            <input type="text" name="tdHeight" id="tdHeight" class="form-control" readonly="true"/>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label for="batch"> Local MAC:<span class="star">*</span></label>
                                            <input type="text" name="tdLocalMac" id="tdLocalMac" class="form-control"
                                                   readonly="true"/>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label for="batch"> Local IP:<span class="star">*</span></label>
                                            <input type="text" name="tdLocalIP" id="tdLocalIP" class="form-control"
                                                   readonly="true"/>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label for="batch"> Status:<span class="star">*</span></label>
                                            <input type="text" name="txtStatus" id="txtStatus" class="form-control"
                                                   readonly="true"/>
                                        </div>
                                    </div>

                                </div>
                            </div> 
                            <div class="container">
                                <div class="col-md-4 form-group">
                                    <label for="deviceType">Select Bio-Metric Device :<span class="star">*</span></label>
                                    <select name="deviceType" id="deviceType" class="form-control biomval">
                                        <option value="0">- - - Please Select - - -</option>
                                        <option value="m">Mantra</option>
                                        <option value="t">Tatvik</option>
                                    </select>
                                    <span class="star bioerr"></span>
                                    <input type="hidden" name="txtid" id="txtid"/>
                                </div>
                                <div class="col-md-3 form-group" id="VisitorNameDiv">    
                                    <label for="deviceType">Select Visitor :<span class="star">*</span></label>
                                    <select name="VisitorName" id="VisitorName" class="form-control biomval">
                                    </select>
                                </div>
                                <div class="col-md-3 form-group" id="CaptureVisitor" style="display:none;">     
                                    <!--<input type="button" id="visitor_impression" class="visitor_impression" value="Capture (Visitor)" name="visitor_impression" style="margin-top: 25px;" />--> 
                                    <input type="button" name="capture2" id="capture2" class="btn btn-success"
                                           value="Capture (Visitor)"><br />
                                    <img id="imgFinger_visitor" name="imgFinger_visitor" class="form-control" style="width:80px; height:107px; margin-left: 20px;"/><br /><span class="star" id="match_visitor"></span>
                                    <input type="hidden" name="matched_visitor" id="matched_visitor"/>
                                    <div id="successvisitor"><input type="hidden" name="txtvisitorsuccess" id="txtvisitorsuccess"/></div>
                                    <div id="failvisitor"></div>
                                </div>
                            </div>
                            <br>
                            <div id="closevisit" style="display:none;">
                                <div class="col-md-2 form-group">     
                                    <label for="visit"> Select Reason to Close Visit:<span class="star">*</span></label>
                                </div>
                                <div class="col-md-6 form-group"> 
                                    <select id="ddlreason" name="ddlreason" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Visit Type')"
                                            onchange="try {
                                                        setCustomValidity('')
                                                    } catch (e) {
                                                    }"> 
                                        <option value="0">Select Reason</option> 
                                        <!--                            <option value="Closed due ITGK owner is ill">Closed due ITGK owner is ill</option>
                                                                    <option value="Closed  due to some emergency work">Closed  due to some emergency work</option>
                                                                    <option value="Shifted without information">Shifted without information</option>
                                                                    <option value="Closed due to local festival">Closed due to local festival</option>
                                                                    <option value="Closed due to some ceremony at home">Closed due to some ceremony at home</option>
                                                                    <option value="Closed due to an untoward incident in the family">Closed due to an untoward incident in the family</option> 
                                                                    <option value="Closed due to ITGK is not functional">Closed due to ITGK is not functional</option>-->

                                    </select>
                                    <!--</div>-->
                                </div>

                                <div class="container">
                                    <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Close Visit"/> </a>
                                </div>
                            </div>
                        </div>
                    </div> 


                </form>

            </div>

        </div>   
    </div>

</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script src="js/mfs100.js" type="text/javascript"></script>
<script src="js/tatvik.js" type="text/javascript"></script>
<script type="text/javascript">
                                                var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                                                var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                                                var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                                                var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                                                $(document).ready(function () {


                                                    getLocation();
                                                    var lat;
                                                    var long;
                                                    function getLocation() {
                                                        var run = 0;
                                                        navigator.geolocation.watchPosition(function (position) {
                                                            lat = position.coords.latitude;
                                                            long = position.coords.longitude;
                                                            run++;
                                                            if (run == 1) {
                                                                //FillTrackLocation(lat,long); 
                                                                FillCenterCode();
                                                            } else {

                                                            }
                                                        },
                                                                function (error) {
                                                                    if (error.code == error.PERMISSION_DENIED)
                                                                        alert("Please allow Location in Browser settings to Proceed");
                                                                    return false;
                                                                });
                                                    }



                                                    function FillCenterCode() {
                                                        $.ajax({
                                                            type: "post",
                                                            url: "common/cfProcessVisitClose.php",
                                                            data: "action=FILL&values=" + visitcode + "",
                                                            success: function (data) {
                                                                $("#ddlCC").html(data);
                                                            }
                                                        });
                                                    }
                                                    //FillCenterCode();

                                                    function FillVisitCloseReason() {
                                                        $.ajax({
                                                            type: "post",
                                                            url: "common/cfProcessVisitClose.php",
                                                            data: "action=FILLREASON",
                                                            success: function (data) {
                                                                $("#ddlreason").html(data);
                                                            }
                                                        });
                                                    }


                                                    $("#btnShow").click(function () {
                                                        $('#response').empty();
                                                        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                                                        $.ajax({
                                                            type: "post",
                                                            url: "common/cfProcessVisitClose.php",
                                                            data: "action=DETAILS&values=" + visitcode + "&cc=" + ddlCC.value + "",
                                                            success: function (data)
                                                            {
                                                                //alert(data);
                                                                $('#response').empty();
                                                                if (data == "") {
                                                                    $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select a RSP for Selection" + "</span></p>");
                                                                } else {
                                                                    data = $.parseJSON(data);
                                                                    txtvisitid.value = data[0].visitid;
                                                                    txtITGKCode.value = data[0].itgkcode;
                                                                    txtITGKName.value = data[0].itgkname;
                                                                    txtVisitDate.value = data[0].visitdate;
                                                                    txtStatus1.value = data[0].status;
//
//                        txtDistrict.value = data[0].district;
//                        txtTehsil.value = data[0].tehsil;
//                        txtStreet.value = data[0].cc;
//                        txtRoad.value = data[0].refno;

                                                                    $("#main-content").show(3000);
                                                                    $("#showdiv").hide(3000);
                                                                    $("#submitdiv").show(3000);
                                                                    FillVisitCloseReason();
                                                                }

                                                            }
                                                        });
                                                    });

                                                    loadvisitorselection();

                                                    function loadvisitorselection() {
                                                        $.ajax({
                                                            type: "post",
                                                            url: "common/cfProcessVisitClose.php",
                                                            data: "action=LoadVisitor",
                                                            success: function (data) {
                                                                if (data == '0') {
                                                                    alert("Something Went Wrong");
                                                                } else {
                                                                    $('#response').empty();
                                                                    $("#VisitorName").html(data);
                                                                }
                                                            }
                                                        });
                                                    }
                                                    $("#VisitorName").change(function () {
                                                        var VisitorId = $(this).val();
                                                        //alert(VisitorId);
                                                        if (VisitorId == '0') {
                                                            alert("Please Select Visitor");
                                                            $("#CaptureVisitor").hide();
                                                        } else {
                                                            $("#VisitorNameDiv").hide();
                                                            $("#CaptureVisitor").show();
                                                        }
                                                    });




                                                    $("#btnSubmit").click(function () {
                                                        // alert(ddlreason.value);
                                                        if (ddlreason.value == '') {
                                                            alert("Please Select Reason to Close Visit");
                                                            return false;
                                                        }
                                                        $('#response').empty();
                                                        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                                        var url = "common/cfProcessVisitClose.php"; // the script where you handle the form input.
                                                        var data;
                                                        data = "action=ADD&reason=" + ddlreason.value + "&visitcode=" + visitcode + "&cc=" + ddlCC.value + "&visitid=" + txtvisitid.value + "&lat=" + lat + "&long=" + long + "&userid=" + VisitorName.value + ""; // serializes the form's elements.

                                                        $.ajax({
                                                            type: "POST",
                                                            url: url,
                                                            data: data,
                                                            success: function (data)
                                                            {
                                                                if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                                                                {
                                                                    $('#response').empty();
                                                                    $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                                                    BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/correct.gif width=10px /></span><span>Visit Closed Successfully.</span></p>");
                                                                    window.setTimeout(function () {
                                                                        window.location.href = "frmclosevisit.php";
                                                                    }, 1000);

                                                                } else
                                                                {
                                                                    $('#response').empty();
                                                                    $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                                                }
                                                                showData();


                                                            }
                                                        });
                                                        //}
                                                        return false; // avoid to execute the actual submit of the form.
                                                    });



                                                    $("#capture2").click(function () {
                                                        getuserdata('VISITOR');
                                                    });
                                                    function getuserdata(params) {
                                                        $('#txtIsoTemplate').val('');
                                                        $('#txtIsoImage').val('');
                                                        $('#txtNFIQ').val('');
                                                        $('#txtRawData').val('');
                                                        $('#txtWsqData').val('');
                                                        $('#txtitgksuccess').val('');
                                                        $('#txtvisitorsuccess').val('');
                                                        var url = "common/cfProcessVisitClose.php"; // the script where you handle the form input.
                                                        var data;
                                                        var id = VisitorName.value;
//alert(para);
                                                        data = "action=GetLearnerByPIN&visitorname=" + id + "";
                                                        //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.

                                                        $.ajax({
                                                            type: "POST",
                                                            url: url,
                                                            data: data,
                                                            success: function (data) {
                                                                if (data == '0') {
                                                                    alert("Please Select Visitor");
                                                                }
                                                                if (data == 'invalidpin') {
                                                                    $('#response').empty();
                                                                    $('#response').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + " Invalid Bio Metric PIN " + "</span></font></b></p>");
                                                                } else {

                                                                    data = $.parseJSON(data);
                                                                    //admission_code.value = data[0].AdmissionCode;
                                                                    txtLearnerName.value = data[0].AdmissionCode;
                                                                    txtQuality.value = data[0].quality;
                                                                    txtNFIQ.value = data[0].nfiq;
                                                                    txtIsoTemplate.value = data[0].template;
                                                                    txtIsoImage.value = data[0].image;
                                                                    txtRawData.value = data[0].raw;
                                                                    txtWsqData.value = data[0].wsq;
                                                                    // txtlearnercode.value = data[0].lcode;
//                        document.getElementById("learnercode").innerHTML = data[0].lcode;
//                        document.getElementById("learnername").innerHTML = data[0].lname;
//                        document.getElementById("fathername").innerHTML = data[0].lfname;
//                        document.getElementById("dob").innerHTML = data[0].ldob;
//                        document.getElementById("coursename").innerHTML = data[0].lcourse;
//                        document.getElementById("batchname").innerHTML = data[0].lbatch;
                                                                    // var imsrc = data[0].lphoto;
                                                                    //src="upload/admission_photo/ "
                                                                    //  $("#my_image").attr("src", "upload/admission_photo/" + imsrc);
                                                                    //learnercode.value = data[0].AdmissionCode;
                                                                    callAPI(params);
                                                                    $('#response').empty();
                                                                    $("#learnerinfo").show();

                                                                    if (data[0].attendance_flag == 0) {
                                                                        $("#btnCapture").show();
                                                                    } else {
                                                                        $("#btnCapture").hide();
                                                                        $("#showMessage_ajax").html(data[0].attendance_flag);
                                                                        $("#showMessage").show();
                                                                    }
                                                                }


                                                            }
                                                        });


                                                    }


                                                    function callAPI(params) {
                                                        var x = deviceType.value;
                                                        //alert(params);
                                                        GetContMachineSrNo(x);

                                                        //var matching1 = GetRegMachineSrNo(x);
                                                        if (x !== "0") {
                                                            switch (x) {
                                                                case "m":
                                                                    Match(params);
                                                                    break;

                                                                case "t":
                                                                    MatchTatvik_new(params);
                                                                    break;
                                                                default:
                                                                    break;
                                                            }
                                                        } else {
                                                            $("#showData").css("display", "none");
                                                        }

                                                    }


                                                    /*** MATCH ON MANTRA MACHINE ***/
                                                    function Match(params) {
                                                        var quality = 60; //(1 to 100) (recommanded minimum 55)
                                                        var timeout = 10;
                                                        try {
                                                            //alert(txtIsoTemplate.value);
                                                            var isotemplate = txtIsoTemplate.value;
                                                            //alert(isotemplate);
                                                            //$("#wait_modal_match").modal("show");
                                                            var res = MatchFinger(quality, timeout, isotemplate);
                                                            var res1 = CaptureFinger(quality, timeout);
                                                            // $("#wait_modal_match").modal("hide");
                                                            //print_r(jsonencode(res));
//                 alert(JSON.stringify(res1));
//                 console.log(JSON.stringify(res1));
//console.log(res);
                                                            if (res.httpStaus) {
                                                                document.getElementById('txtStatus').value = "ErrorCode: " + res1.data.ErrorCode + " ErrorDescription: " + res1.data.ErrorDescription;
                                                                if (res.data.Status) {
                                                                    //alert("Finger matched");
                                                                    if (params == 'ITGK') {
                                                                        $('#successitgk').empty();
                                                                        $('#failitgk').empty();
                                                                        $('#successitgk').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "Success" + "</span></font></b></p>");
                                                                        successitgk = "1";
                                                                    } else {
                                                                        $('#successvisitor').empty();
                                                                        $('#failvisitor').empty();
                                                                        $('#successvisitor').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "Success" + "</span></font></b></p>");
                                                                        successvisitor = "1";
                                                                        $('#closevisit').show(2000);
                                                                    }

                                                                } else {

                                                                    if (res.data.ErrorCode != "0") {
                                                                        //alert("err: "+res.data.ErrorDescription);
                                                                        $("#error_modal_timeout").modal("show");
                                                                    } else {
                                                                        //alert("not matched");
                                                                        if (params == 'ITGK') {
                                                                            $('#successitgk').empty();
                                                                            $('#failitgk').empty();
                                                                            $('#failitgk').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "Failed" + "</span></font></b></p>");
                                                                            successitgk = "0";
                                                                        } else {
                                                                            $('#successvisitor').empty();
                                                                            $('#failvisitor').empty();
                                                                            $('#failvisitor').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "Failed" + "</span></font></b></p>");
                                                                            successvisitor = "0";
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                alert(res.err);
                                                            }
                                                        } catch (e) {
                                                            alert(e);
                                                        }
                                                        return false;
                                                    }

                                                    /*** MATCH ON Tatvik MACHINE ***/
                                                    function MatchTatvik_new(params) {
//alert(params);
                                                        crossDomainAjax("https://127.0.0.1:31000", 'CAPTUREFP',
                                                                function (result) {
                                                                    var ClaimedTemplateData = result.CaptureResult.Image_FMRbytes;

                                                                    var matchtemplates = "<MatchTemplates>" +
                                                                            "<ClaimedTemplateData>" + ClaimedTemplateData + "</ClaimedTemplateData>" +
                                                                            "<ReferenceTemplateData>" + document.getElementById('txtIsoTemplate').value.toString() + "</ReferenceTemplateData>" +
                                                                            "</MatchTemplates>";
                                                                    $.ajax({
                                                                        url: "https://127.0.0.1:31000",

                                                                        data: matchtemplates,
                                                                        method: "MATCHFPTEMPLATE",
                                                                        responsetype: 'xml',
                                                                        // jsonpCallback: "Jsonp_Callback",
                                                                        success: function (data, success) {
                                                                            var rd = xml2json(data);
                                                                            var rd2 = rd.MatchResult;
                                                                            console.log(rd2)
                                                                            if (rd2.MatchStatus == 'Match Failed') {
                                                                                //alert("no");
                                                                                $("#error_modal_nomatch").modal("show");
                                                                                if (params == 'ITGK') {
                                                                                    $('#successitgk').empty();
                                                                                    $('#failitgk').empty();
                                                                                    $('#failitgk').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "Failed" + "</span></font></b></p>");
                                                                                    successitgk = "0";
                                                                                } else {
                                                                                    $('#successvisitor').empty();
                                                                                    $('#failvisitor').empty();
                                                                                    $('#failvisitor').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "Failed" + "</span></font></b></p>");
                                                                                    successvisitor = "0";
                                                                                }
                                                                                window.setTimeout(function () {
                                                                                    $('#response').empty();
                                                                                    $("#btnCapture").hide();
                                                                                    $("#learnerinfo").hide();
                                                                                }, 3000);
                                                                            } else {
//alert(rd2.MatchStatus);
                                                                                if (rd2.MatchStatus == "Finger not placed. Time Out!" || rd2.MatchStatus == "Capture Failed! Time Out!")
                                                                                    $("#error_modal_timeout").modal("show");

                                                                                if (rd2.MatchStatus == "Match succesful") {
                                                                                    //alert("yes");
                                                                                    /*** ALREADY ENROLLED ****/
                                                                                    if (params == 'ITGK') {
                                                                                        $('#successitgk').empty();
                                                                                        $('#failitgk').empty();
                                                                                        $('#successitgk').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "Success" + "</span></font></b></p>");
                                                                                        successitgk = "1";
                                                                                    } else {
                                                                                        $('#successvisitor').empty();
                                                                                        $('#failvisitor').empty();
                                                                                        $('#successvisitor').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "Success" + "</span></font></b></p>");
                                                                                        successvisitor = "1";
                                                                                    }
                                                                                    //SubmitDetails();
                                                                                }
                                                                            }
                                                                        },
                                                                        error: function (jqXHR, exception) {
                                                                            if (jqXHR.status === 0) {
                                                                                alert('Not connect.\n Verify Network.');
                                                                            } else if (jqXHR.status == 404) {
                                                                                alert('Requested page not found. [404]');
                                                                            } else if (jqXHR.status == 500) {
                                                                                alert('Internal Server Error [500].');
                                                                            } else if (exception === 'parsererror') {
                                                                                alert('Requested JSON parse failed.');
                                                                            } else if (exception === 'timeout') {
                                                                                alert('Time out error.');
                                                                            } else if (exception === 'abort') {
                                                                                alert('Ajax request aborted.');
                                                                            } else {
                                                                                alert('Uncaught Error.\n' + jqXHR.responseText);
                                                                            }
                                                                        }
                                                                    });

                                                                });

                                                    }

                                                    var connectmachinesrno = "";
                                                    function GetContMachineSrNo(x) {

                                                        if (x !== "n") {
                                                            switch (x) {
                                                                case "m":
                                                                    var res = GetMFS100Info();
                                                                    //alert(res.data.ErrorCode);
                                                                    if (res.httpStaus) {

                                                                        if (res.data.ErrorCode == "0") {
                                                                            connectmachinesrno = res.data.DeviceInfo.SerialNo;
                                                                        }
                                                                    }
                                                                    break;

                                                                case "t":
                                                                    var res = initializeTatvik();
                                                                    connectmachinesrno = $("#tdSerial").val();
                                                                    break;
                                                                default:
                                                                    break;
                                                            }
                                                        }

                                                    }

                                                    var regmachinesrno = "";
                                                    var matching = "";


                                                    function resetForm(formid) {
                                                        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                                                    }

                                                });

</script>

</html>
