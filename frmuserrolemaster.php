<?php
$title="Child Menu Add";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var RoleCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var RoleCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {
?>
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">User Role Master</div>
                <div class="panel-body">
					<form name="frmuserrolemaster" id="frmuserrolemaster" class="form-inline" role="form" enctype="multipart/form-data">
						<div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
								
								
									<div class="col-sm-4 form-group">     
										<label for="entity">Role Entity:</label>
										<select id="ddlEntity" name="ddlEntity" class="form-control">

                                        </select>
									</div> 
								</div>
                                        
								<div class="container">
									<div class="col-sm-4 form-group">     
										<label for="name">Role Name:</label>
										<input type="text" class="form-control" maxlength="50" name="txtRoleName" id="txtRoleName" placeholder="Role Name" >
									</div> 
								</div>
                                
								<div class="container">
									<div class="col-sm-10 form-group"> 
										<label for="status">Role Status:</label>
										<select id="ddlStatus" name="ddlStatus" class="form-control">
												  
										</select>
									</div>                            
								</div>

								<div class="container">
									<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
								</div>                  

                         <div id="gird" style="margin-top:35px;"> </div>                   
                 </div>
            </div>   
        </div>
    </form>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?> 
 
 <script type="text/javascript">
	var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
	var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
	var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
	var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
	$(document).ready(function () {

	if (Mode == 'Delete')
	{
		if (confirm("Do You Want To Delete This Item ?"))
		{
			deleteRecord();
		}
	}
	else if (Mode == 'Edit')
	{
		fillForm();
	}

	function FillStatus() {
		$.ajax({
			type: "post",
			url: "common/cfStatusMaster.php",
			data: "action=FILL",
			success: function (data) {
				$("#ddlStatus").html(data);
			}
		});
	}

	FillStatus();

	function FillEntity() {
		$.ajax({
			type: "post",
			url: "common/cfUserRoleMaster.php",
			data: "action=FILLENTITY",
			success: function (data) {
				$("#ddlEntity").html(data);
			}
		});
	}

	FillEntity();
	function deleteRecord()
	{
		$('#response').empty();
		$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		$.ajax({
			type: "post",
			url: "common/cfUserRoleMaster.php",
			data: "action=DELETE&values=" + RoleCode + "",
			success: function (data) {
				//alert(data);
				if (data == SuccessfullyDelete)
				{
					$('#response').empty();
					$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
					window.setTimeout(function () {
						window.location.href = "frmuserrolemaster.php";
					}, 3000);
					Mode = "Add";
					resetForm("frmuserrolemaster");
				}
				else
				{
					$('#response').empty();
					$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
				}
				showData();
			}
		});
	}


	function fillForm()
	{
		$.ajax({
			type: "post",
			url: "common/cfUserRoleMaster.php",
			data: "action=EDIT&values=" + RoleCode + "",
			success: function (data) {
				//alert($.parseJSON(data)[0]['Status']);
				data = $.parseJSON(data);
				txtRoleName.value = data[0].RoleName;
				ddlStatus.value = data[0].Status;
				ddlEntity.value = data[0].Entity;

			}
		});
	}

	function showData() {

		$.ajax({
			type: "post",
			url: "common/cfUserRoleMaster.php",
			data: "action=SHOW",
			success: function (data) {

				$("#gird").html(data);

			}
		});
	}

	showData();


	$("#btnSubmit").click(function () {
		$('#response').empty();
		$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		var url = "common/cfUserRoleMaster.php"; // the script where you handle the form input.
		var data;
		if (Mode == 'Add')
		{
			data = "action=ADD&name=" + txtRoleName.value + "&status=" + ddlStatus.value + "&entity=" + ddlEntity.value + ""; // serializes the form's elements.
		}
		else
		{
			data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + "&entity=" + ddlEntity.value + ""; // serializes the form's elements.
		}
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			success: function (data)
			{
				if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
				{
					$('#response').empty();
					$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
					window.setTimeout(function () {
						window.location.href = "frmuserrolemaster.php";
					}, 1000);

					Mode = "Add";
					resetForm("frmuserrolemaster");
				}
				else
				{
					$('#response').empty();
					$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
				}
				showData();


			}
		});

		return false; // avoid to execute the actual submit of the form.
	});
	function resetForm(formid) {
		$(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
	}

	});

	</script>
	</html>
	 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>