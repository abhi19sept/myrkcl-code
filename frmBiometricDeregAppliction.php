<?php  ob_start();
$title = "Bio Metric Deregistration";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if($_SESSION['User_UserRoll']!=1 && $_SESSION['User_UserRoll']!=7){
    session_destroy();
    header('Location: index.php');
    exit;
}
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Learners Bio Metric Deregistration</div>
            <div class="panel-body">

                <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control"></select>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control"></select>
                            </div> 

                            <div class="col-md-4 form-group"> 
                                
                                <input style="margin-top: 24px;" type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        
                        <div class="container">
                            
                        </div>
                        <div id="grid" style="margin-top:30px; width:94%;"> </div>
                       
                        

      
                        <div class="modal fade" id="previewModal" role="dialog">
                            <div>
                                <div class="modal-content" style="margin: 100px auto; width: 800px;" >
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title text-left">Are you sure you want to submit these details?</h4>
                                    </div>
                                    <div class="modal-body" id="rec_type">

                                    </div>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="modal-footer">
                                        <button class="btn btn-primary" id="btnFinalSubmit">Submit</button>
                                        <button class="btn btn-default" data-dismiss="modal">Back to Selection</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>  
                    
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLAdmissionSummaryCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBioMetricDeregistration.php",
                data: "action=FILLCurrentOpenBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);

                }
            });

        });

        $("#btnSubmit").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBioMetricDeregistration.php"; // the script where you handle the form input.
                var data;
                data = "action=APPLYDREG&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
                // alert(data);
                $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data2) {
//                    alert(data2);
                    $('#response').empty();
                    $("#grid").html(data2);
                    $("#sbtn").show();
                    
                    var table = $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    
                    
                    // Handle click on "Select all" control
                    $('#example-select-all').on('click', function(){
                       // Get all rows with search applied
                       var rows = table.rows({ 'search': 'applied' }).nodes();
                       // Check/uncheck checkboxes for all rows in the table
                       $('input[type="checkbox"]', rows).prop('checked', this.checked);
                    });

                    // Handle click on checkbox to set state of "Select all" control
                    $('#example tbody').on('change', 'input[type="checkbox"]', function(){
                        // If checkbox is not checked
                        if(!this.checked){
                           var el = $('#example-select-all').get(0);
                           // If "Select all" control is checked and has 'indeterminate' property
                           if(el && el.checked && ('indeterminate' in el)){
                              // Set visual state of "Select all" control
                              // as 'indeterminate'
                              el.indeterminate = true;
                           }
                        }
                     });
                }
            });

            }
            return false; // avoid to execute the actual submit of the form.
        });
        

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        
       
    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
