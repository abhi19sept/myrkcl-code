<?php
$title = "Dropped/Incomplete Transaction Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important; max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Dropped/Incomplete Transaction Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                    <div class="container">
                        <div class="col-sm-3"> 
                            <label for="course">Enter Merchant Transaction Reference Number:<span class="star">*</span></label>
                            <input type="text" class="form-control" name="txttran" id="txttran" placeholder="Transaction Number">     
                        </div> 

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                        </div>
                    </div>

                    <div id="grid" name="grid" style="margin-top:35px;"> </div> 
                    
                    <br>
                    <div class="container">
                        <div class="col-sm-11"> 
                            <p class="bg-success" for="course"><span class="star">*</span><b>Disclaimer:</b>1. All Transaction shown above will be Reconciled ONLY when Payment is Confirmed at PayUMoney.</p>
							<p class="bg-success" for="course"><span class="star">*</span>2. All Transaction which are Success at PayUMoney and Your Learners are not confirmed at MYRKCL in that case, Send a mail to techsupport@rkcl.in with Merchant Transaction Reference Number, Centre Code and Amount. Please do not Reconfirm These Learners with another transaction until we Reconcile your amount</p>
                            
                        </div>
                    </div>
                        

            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        $("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfDropTranStatus.php",
                data: "action=DETAILS&values=" + txttran.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'print'
                        ]
                    });

                }
            });
        });
    });

</script>
</html>