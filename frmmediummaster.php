<?php
$title="Course Master";
include ('header.php'); 
include ('root_menu.php'); 

            
      if (isset($_REQUEST['code'])) {
                echo "<script>var MediumCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var MediumCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>

        <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Exam Type Master</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmMediummaster" id="frmMediummaster" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">	Medium Name:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtMediumName" id="txtMediumName" placeholder="Medium Name">
                            </div>



                        </div>    
						
						
						<div class="container">
						

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Medium Status:</label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control" >
								
                                </select>    
                            </div> 
						</div>



                       

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
					<div id='gird'> </div>
						
                </div>
            </div>   
        </div>


    </form>




</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>
 <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfMediumMaster.php",
                    data: "action=DELETE&values=" + MediumCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                            }, 3000);
                            Mode="Add";
                            resetForm("frmmediummaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfMediumMaster.php",
                    data: "action=EDIT&values=" + MediumCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Status']);
                        data = $.parseJSON(data);
                        txtMediumName.value = data[0].MediumName;
                        ddlStatus.value = data[0].Status;
                        
                    }
                });
            }

            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfMediumMaster.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);

                    }
                });
            }

            showData();


            $("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfMediumMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtMediumName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + MediumCode + "&name=" + txtMediumName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmmediummaster.php";
                           }, 1000);

                            Mode="Add";
                            resetForm("frmmediummaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcoursetype.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>