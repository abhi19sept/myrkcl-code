<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Government Entry Form</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bootcss/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="bootcss/js/bootstrap.min.js"></script>		
		
    </head>
    <body>
        <?php
            
            
        if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
        ?>

        <div class="container">   
            <div class="panel panel-primary">

                <div class="panel-heading">Reimbursement Application  Form</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" >     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                       
							     <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Emp Id:</label>
                                <select id="ddlempId" name="ddlempId" class="form-control" >
								  <option value="" selected="selected">Please Select</option>
								    <option value="1">1</option>
									<option value="1">1</option>
                                </select>    
                            </div>

                            
							
							
							

                        </div>
							<div class="container">
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Emp District:</label>
                                <select id="ddlempdistrict" name="ddlempdistrict" class="form-control" >
								  <option value="" selected="selected">Please Select</option>
								    <option value="1">1</option>
									<option value="1">1</option>
                                </select>    
                            </div>
							
							</div>

                        <div class="container">

							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Emp Tehsil:</label>
                                <select id="ddlempTehsil" name="ddlempTehsil" class="form-control" >
								  <option value="" selected="selected">Please Select</option>
								    <option value="1">1</option>
									<option value="1">1</option>
                                </select>    
                            </div>



                        </div>    

                        <div class="container">  

                            
						<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Emp location:</label>
                                <select id="ddlemplocation" name="ddlemplocation" class="form-control" >
								  <option value="" selected="selected">Please Select</option>
								    <option value="1">1</option>
									<option value="1">1</option>
                                </select>    
                            </div>

                         
                        </div>



                        <div class="container">
                          
                           



                            
                        </div>


                        <div class="container">
 
                          

   </div>
                        
                        <div class="container">
                            
                          
<div class="container">
                            
</div>
                          
               <div class="container">         
                         

</div>


                       

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
                </div>
            </div>   
        </div>


    </form>




</body>
<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

		
		function getempid() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlempId").html(data);
                }
            });
        }
        function FillEmpDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlempdistrict").html(data);
                }
            });
        }
        FillEmpDistrict();

        function FillEmp() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLEMPDEPARTMENT",
                success: function (data) {
                    $("#ddlDeptName").html(data);
                }
            });
        }
        FillEmpDepartment();

        function FillEmpDesignation() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLEMPDESIGNATION",
                success: function (data) {
                    $("#ddlEmpDesignation").html(data);
                }
            });
        }
        FillEmpDesignation();

        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        FillBankDistrict();

		$("#ddlBankDistrict").change(function(){
			FillBankName(this.value);
		});
        function FillBankName(districtid) {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKNAME&districtid=" + districtid,
                success: function (data) {
                    $("#ddlBankName").html(data);

                }
            });
        }
        

        function FillDobProof() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FillDobProof",
                success: function (data) {
                    $("#ddlDobProof").html(data);

                }
            });
        }
        FillDobProof();



        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=DELETE&values=" + StatusCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=EDIT&values=" + StatusCode + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    txtStatusName.value = data[0].StatusName;
                    txtStatusDescription.value = data[0].StatusDescription;
                    //alert($.parseJSON(data)[0]['StatusName']);
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);

                }
            });
        }

        //showData();		
		
        $("#btnSubmit").click(function () {
			
			$("#form").valid();
			
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfgoventryform.php"; // the script where you handle the form input.
            var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&lcode=" + txtLCode.value + "&empname=" + txtEmpName.value + "&faname="+ txtFaName.value + 
					   "&empdistrict="+ ddlempdistrict.value + "&empmobile="+ txtEmpMobile.value + "&empemail="+ txtEmail.value + 
					   "&empaddress="+ txtEmpAddress.value + "&empdeptname="+ ddlDeptName.value + "&empid="+ txtEmpId.value + 
					   "&empgpfno="+ txtGpfNo.value + "&empdesignation="+ ddlEmpDesignation.value + "&empmarks="+ txtEmpMarks.value + 
					   "&empexamattempt="+ ddlExamattepmt.value + "&emppan="+ txtPanNo.value + "&empbankaccount="+ txtBankAccount.value + 
					   "&empbankdistrict="+ ddlBankDistrict.value + "&empbankname="+ ddlBankName.value + "&empbranchname="+ txtBranchName.value +
					   "&empifscno="+ txtIfscNo.value + "&empmicrno="+ txtMicrNo.value + "&filereceipt="+ fileReceipt.value + 
					   "&filebirth="+ fileBirth.value + "&empdobproof="+ ddlDobProof.value + ""; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + StatusCode + "&name=" + txtStatusName.value + "&description=" + txtStatusDescription.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmgoventry_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>