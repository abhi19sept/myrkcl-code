<?php
$title = "Activate Exam Event";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {

   
    ?>
<div style="min-height:430px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Activate Exam Event</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmactivateexamevent" id="frmactivateexamevent" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                    <div class="container">
                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div> 
                    </div>

					<div class="container">
					
					<div class="col-sm-10 form-group">
					
						  <label for="course">Select Exam Status:<span class="star">*</span></label>
						
						<select id="ddlStatus" name="ddlStatus" class="form-control"  >
						</select>  
						
					 </div>
					 </div>
					 
					 
					 
					 <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style='margin-left:10px;'/>

									
                             </div>
                    <div id="gird" name="gird" style="margin-top:35px;"> </div> 


            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>




<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillEvent() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfactivateexamevent.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlExamEvent").html(data);
                }
            });
        }
        FillEvent();
    
	
	
	function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
			
			
			function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfactivateexamevent.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						

                    }
                });
            }
			showData();
			
			
			
			
			$("#btnSubmit").click(function () {
						
						if ($("#frmactivateexamevent").valid())
						{
							
							$('#response').empty();
							$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
							var url = "common/cfactivateexamevent.php"; // the script where you handle the form input.
							var data;
							var forminput=$("#frmactivateexamevent").serialize();
							if (Mode == 'Add')
							{
								//alert(1);
								 data = "action=ADD&" +forminput; // serializes the form's elements.
							}
							else 
							{
								//alert(2);
								data = "action=UPDATE&code=" + Code +"&" + forminput;
								
							}
							$.ajax({
								type: "POST",
								url: url,
								data: data,
								success: function (data)
								{
									if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
									{
										$('#response').empty();
										$('#response').append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
										 window.setTimeout(function () {
											window.location.href = "frmactivateexamevent.php";
										}, 1000);
										Mode = "Add";
										resetForm("frmactivateexamevent");
									}
									else
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
									}
									//showData();


								}
							});
						 }
							return false; // avoid to execute the actual submit of the form.
						});

			
			
			
	});		

    

</script>


<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmactivateexamevent_validation.js"></script>
</html>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>	
