<?php
$title = "Advance Course Admission";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var CourseBatch=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var CourseBatch=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Advance Course 
                <div style="float:right">
                    Total Admission Count: <div style="display: inline" id="txtintakeavailable"> </div>
                </div>
            </div>

            <div class="panel-body">           
                <form name="frmCourseBatch" id="frmCourseBatch" class="form-inline" role="form" method="post" action="frmadvancecourseadmission.php"> 
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Advance Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }">  </select>
                        </div> 
                    </div>                          
					
					 <div class="container">
                        <div class="col-md-6 form-group">     
                            <label for="batch">Select Advance Course Category:<span class="star">*</span></label>
                            <select id="ddlcategory" name="ddlcategory" class="form-control" required="required"
								oninvalid="setCustomValidity('Please Select Advance Course Category')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"> </select>
                        </div>
                    </div>
					
					  <div class="container">
                        <div class="col-md-6 form-group">     
                            <label for="batch"> Select Advance Course Name:<span class="star">*</span></label>
                            <select id="ddlPkg" name="ddlPkg" class="form-control" required="required"
								oninvalid="setCustomValidity('Please Select Advance Course Name')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"> </select>
                        </div>
                    </div>
					
					 
					
                    <div class="container">
                        <div class="col-md-6 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" 
								oninvalid="setCustomValidity('Please Select Batch')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"> </select>
                        </div>
                    </div>

                    <div class="container">
                        <a href="frmadvancecourseadmission.php" style="text-decoration:none;">
							<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>
						</a>
                    </div>

                    <div id="gird" style="margin-top:35px;"> </div>	
            </div>
        </div>   
    </div>
</form>
</div>
<?php include ('footer.php');
include'common/message.php'; ?>
</body>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=FILLAdvanceCourse",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val(); 				
				FillCategory(selCourse);                    
        });
		
		 function FillBatch(val) {
            
        }
		
		function FillCategory(val) {           			
            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=FILLCourseCategory&values=" + val + "",
                success: function (data) {                   
				   $("#ddlcategory").html(data);
                }
            });
        }		
		
		$("#ddlcategory").change(function () {
            var selcategory = $(this).val();			
            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=FILLAdvanceCourseName&values=" + selcategory + "",
                success: function (data) {
                    $("#ddlPkg").html(data);
                }
            });
        });
		
        $("#ddlPkg").change(function () { 			
			$.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=FILLEventBatch&values=" + ddlCourse.value + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });      
        });
		
		$("#ddlBatch").change(function () {
		      $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=FILLIntake&values=" + ddlCourse.value + "",
                success: function (data) {
					//alert(data);
					 if(data=='10000'){
						alert("Subscription Not Available");
                        window.location.href = "frmadvancecoursemessage.php";
					}
                    else {
						document.getElementById('txtintakeavailable').innerHTML = data;
					}
                    
                }
            });
		});
    });
</script>
</html>