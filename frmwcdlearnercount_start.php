<?php

date_default_timezone_set("Asia/Kolkata");
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);

$title = "WCD Learner Count Report";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var UserParentID=" . $_SESSION['User_ParentId'] . "</script>";
    echo "<script>var UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var UserParentID=0</script>";
    echo "<script>var UserRole=0</script>";
}
echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">ITGK Wise Merit List for Approval</div>
            <div class="panel-body">

                <form name="frmcenterpreferencecount" id="frmcenterpreferencecount" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
								  <select id="ddlCourse" name="ddlCourse" class="form-control">
                                    <!--                                <option value="0">All Batch</option>-->
                                </select>
                             </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                    <!--                                <option value="0">All Batch</option>-->
                                </select>
                            </div> 
							
							<div class="col-md-4 form-group">     
                                <label for="batch"> Select Category:<span class="star">*</span></label>
                                <select id="ddlCat" name="ddlCat" class="form-control ct">
									<option value="">Select Category</option>
									<option value="1">Special Category</option>
<!--									<option value="0">Non Special Category</option>-->
                                </select>
                            </div> 

                            <div class="col-md-4 form-group" style="padding-top: 25px;">
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>    
                            </div>
						
                        </div>
                        <?php if ($_SESSION['User_UserRoll'] == 1) { ?>
						<div class="container">
                            <div class="col-md-4 form-group resetMerit">     
                                <input type="submit" name="btnReSetMerit" id="btnReSetMerit" class="btn btn-primary" value="Step1: Reset Merit"/>
                            </div>
                            <div class="col-md-4 form-group setEligibility">     
                                <input type="submit" name="btnSetEligibility" id="btnSetEligibility" class="btn btn-primary" value="Step2: Set Eligibility"/>    
                            </div>
                            <div class="col-md-4 form-group setFinalPreference">     
                                <input type="submit" name="btnsetFinalPreference" id="btnsetFinalPreference" class="btn btn-primary" value="Step3: Set Final Preference"/>    
                            </div>
                            <div class="col-md-4 form-group generateMerit">     
                                <input type="submit" name="btnSetMerit" id="btnSetMerit" class="btn btn-primary" value="Step4: Generate Merit"/>    
                            </div>
                        </div>
                        <?php } ?>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>

<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

		function FillCourseName() {
            var CourseCode = '3';
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FillAdmissionCourse&values=" + CourseCode + "",
                success: function (data) {
					txtcoursename.value = data;
                    //document.getElementById('txtcoursename').innerHTML = data;
                }
            });
        }
        //FillCourseName();
		
		  function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfWcdlearnercount.php",
                data: "action=FillCourse",
                success: function (data) {
					//alert(data);
                    $("#ddlCourse").html(data);
                }
            });

        }
	FillCourse();

        $("#ddlCourse").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfWcdlearnercount.php",
                data: "action=FILLBatchName&values=" + ddlCourse.value + "",
                success: function (data) {
					//alert(data);
                    $("#ddlBatch").html(data);
                    if(ddlCourse.value == 24){
                        $(".ct option[value='0']").remove();
                    }
                    else if(ddlCourse.value == 3){
                        $(".ct").append('<option value="0">Non Special Category</option>');
                    }
                }
            });

        });
	
	
	
        function showData() {
			//$("#btnSubmit").hide();
            if (ddlCourse.value == '') {
                alert('Please select a course.');
                return;
            }if (ddlBatch.value == '') {
                alert('Please select a batch.');
                return;
            }
			if (ddlCat.value == '') {
                alert('Please select Category.');
                return;
            }
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdlearnercount.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=GETDATA&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "&cat=" + ddlCat.value + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Batch." + "</span></div>");
                            
						}
					else {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}

                }
            });
        }

        function checkIfAlreadyGeneratedBeforeReset() {
            if (ddlBatch.value == '') {
                alert('Please select a batch before proceed.');
                return;
            }
            $('.resetMerit').html("<p class='star small'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing...</span></p>");
            var url = "common/cfWcdlearnercount.php";
            data = "action=checkBeforeGenerateMerit&batch=" + ddlBatch.value + "";

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    var isok = 0;
                    if (data) {
                        isok = confirm('WCD merit for this batch already generated, are you sure to re-set merit for this batch ?');
                        if (isok) {
                           resetGeneratedMerit();
                        } else {
                            $('.resetMerit').empty();
                        }
                    } else {
                        $('.resetMerit').empty();
                    }
                }
            });
        }

        function markEligibility() {
            if (ddlBatch.value == '') {
                alert('Please select a batch for which you want to set eligibility.');
                return;
            }
            $('.setEligibility').html("<p class='star small'><span><img src=images/ajax-loader.gif width=10px /></span><span>setting eligibility, please wait for a while.....</span></p>");
            var url = "common/cfWcdlearnercount.php";
            data = "action=setEligibility&batch=" + ddlBatch.value + "";

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    if (data == 1) {
                        $('.setEligibility').empty();
                        $('.setEligibility').html("<p class='success'><span>Eligibility Set!!</span></p>");
                    } else {
                        $('.setEligibility').empty();
                        $('.setEligibility').html("<p class='star small'><span>" + data + "</span></p>");
                    }
                }
            });
        }

        function setFinalPreference() {
            if (ddlBatch.value == '') {
                alert('Please select a batch for which you want to set final preference.');
                return;
            }
            $('.setFinalPreference').html("<p class='star small'><span><img src=images/ajax-loader.gif width=10px /></span><span>setting final preference, please wait for a while.....</span></p>");
            var url = "common/cfWcdlearnercount.php";
            data = "action=setFinalPreference&batch=" + ddlBatch.value + "";

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    if (data == 1) {
                        $('.setFinalPreference').empty();
                        $('.setFinalPreference').html("<p class='success'><span>Final Preference Set!!</span></p>");
                        showData();
                    } else {
                        $('.setFinalPreference').empty();
                        $('.setFinalPreference').html("<p class='star small'><span>" + data + "</span></p>");
                    }
                }
            });
        }

        function checkIfAlreadyGenerated() {
            if (ddlBatch.value == '') {
                alert('Please select a batch before proceed.');
                return;
            }
            $('.generateMerit').html("<p class='star small'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing...</span></p>");
            var url = "common/cfWcdlearnercount.php";
            data = "action=checkBeforeGenerateMerit&batch=" + ddlBatch.value + "";

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    var isok = 0;
                    if (data) {
                        isok = confirm('WCD merit for this batch already generated, are you sure to re-generate merit for this batch ?');
                        if (isok) {
                           generateMerit();
                        } else {
                            $('.generateMerit').empty();
                        }
                    } else {
                        $('.generateMerit').empty();
                    }
                }
            });
        }

        function resetGeneratedMerit() {
            if (ddlBatch.value == '') {
                alert('Please select a batch for which you want to reset merit.');
                return;
            }
            $('.resetMerit').html("<p class='star small'><span><img src=images/ajax-loader.gif width=10px /></span><span>reset merit, please wait for a while.....</span></p>");
            var url = "common/cfWcdlearnercount.php";
            data = "action=resetMerit&batch=" + ddlBatch.value + "";

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    if (data == 1) {
                        $('.resetMerit').empty();
                        $('.resetMerit').html("<p class='success'><span>Merit Reset!!</span></p>");
                    } else {
                        $('.resetMerit').empty();
                        $('.resetMerit').html("<p class='star small'><span>" + data + "</span></p>");
                    }
                }
            });
        }

        function generateMerit() {
            if (ddlBatch.value == '') {
                alert('Please select a batch for which you want to generate merit.');
                return;
            }
            $('.generateMerit').html("<p class='star small'><span><img src=images/ajax-loader.gif width=10px /></span><span>Generating Merit, please wait for a while.....</span></p>");
            var url = "common/cfWcdlearnercount.php";
            data = "action=generateMerit&batch=" + ddlBatch.value + "";

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    if (data == 1) {
                            $('.generateMerit').empty();
                            $('.generateMerit').html("<p class='success'><span>Merit Generated</span></p>");
                    } else {
                        $('.generateMerit').empty();
                        $('.generateMerit').html("<p class='star small'><span>" + data + "</span></p>");
                    }
                }
            });
        }
	

        $("#btnSubmit").click(function () {
            showData();
          return false; // avoid to execute the actual submit of the form.
        });

        $("#btnSetMerit").click(function () {
            var alreadyGenerated = checkIfAlreadyGenerated();
            return false; // avoid to execute the actual submit of the form.
        });

        $("#btnReSetMerit").click(function () {
            var alreadyGenerated = checkIfAlreadyGeneratedBeforeReset();
            return false; // avoid to execute the actual submit of the form.
        });

        $("#btnSetEligibility").click(function () {
            var alreadyGenerated = markEligibility();
            return false; // avoid to execute the actual submit of the form.
        });

        $("#btnsetFinalPreference").click(function () {
            var alreadyGenerated = setFinalPreference();
            return false; // avoid to execute the actual submit of the form.
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<!-- <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmphotosignprocesscount.js" type="text/javascript"></script> -->
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
