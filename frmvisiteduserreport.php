<?php
$title="Visited User";
include ('header.php'); 
include ('root_menu.php'); 
    if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:1500px !important;">
        <div class="container"> 
			

            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">Visited User Report
				 
				</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmvisiteduser" id="frmvisiteduser" class="form-inline" action=""> 
					
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
							
							
							
							<div class="col-sm-4 form-group"> 
                                <label for="address">Start Date:<span class="star">*</span></label>
                                
								
								<input type="text" class="form-control" readonly="true" maxlength="50" name="txtstart" id="txtstart"  placeholder="DD-MM-YYYY">
                            </div>
						
							<div class="col-sm-4 form-group"> 
                                <label for="address">End Date:<span class="star">*</span></label>
                                  
                          <input type="text" class="form-control" readonly="true" maxlength="50" name="txtend" id="txtend"  placeholder="DD-MM-YYYY">								
                            </div>
							
							
                            
						</div>
						
						
						

						<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="margin-top:10px;margin-left:10px"/>    
                        </div>
						
						
						<div id="gird"></div>
                </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox
{	color:#F00;	 } 
</style>

<script type="text/javascript"> 
 $('#txtstart').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true,
		autoclose: true
	});  
	
	$(txtend).datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true,
		autoclose: true
	});  
		
	
	
	
	</script>



 <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
        	$("#txtend").change(function () {
    var txtstart = document.getElementById("txtstart").value;
    var txtend = document.getElementById("txtend").value;
 
    if ((Date.parse(txtstar) >= Date.parse(ttxtend))) {
        alert("End date should be greater than Start date");
        document.getElementById("txtstart").value = "";
    }
});
	
             $("#btnSubmit").click(function () {
			 if ($("#frmvisiteduser").valid())
			 {
				$('#response').empty();
		        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                               var url = "common/cfvisiteduserreport.php"; // the script where you handle the form input.
								var data;
								var forminput=$("#frmvisiteduser").serialize();
								
									data = "action=SHOW&" +forminput; // serializes the form's elements.
								
                                    $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                        $('#response').empty();
		                               
                                       $("#gird").html(data);
                                     $('#example').DataTable({
                                  dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });      

                                    }
                                });
						}
                                return false; // avoid to execute the actual submit of the form.
                            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
	
	<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmvisiteduser_validation.js" type="text/javascript"></script>
</html>