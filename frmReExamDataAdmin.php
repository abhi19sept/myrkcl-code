<?php
$title = "Re Exam Learner Data Download Admin";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading"> Re Exam Learner Data Download Admin</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmReExamDataAdmin" id="frmReExamDataAdmin" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                    <div class="container">
                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div> 
                    </div>
                    <div class="container">
                        
                        <button type="button" class="btn btn-primary" name="btnSubmit" id="btnSubmit" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing...">Export  Report</button>
                    </div>
                    


            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillEvent() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfReExamDataAdmin.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlExamEvent").html(data);
                }
            });
        }
        FillEvent();
    });
    $("#btnSubmit").click(function () {
        $('#response').empty();
          $("#btnSubmit").button('loading');

        $.ajax({
            type: "post",
            url: "common/cfReExamDataAdmin.php",
            data: "action=examdata&examevent=" + ddlExamEvent.value + "",
            success: function (data)
            {
                
                 $("#btnSubmit").button('reset');
            
                window.open(data, '_blank');
  
				
            }
        });
    });

</script>
</html>