<?php
$title = "Register query to execute";
include ('header.php');
include ('root_menu.php');
?>
<script type="text/javascript" src="js/aglr.min.js"></script>
<script type="text/javascript" src="js/ascript.js"></script>
<link href="css/extra.css" rel="stylesheet" type="text/css" />
<style type="text/css">

    form > div.container {
       filter: blur(0px) !important;
            -webkit-filter:blur(0px) !important;
            -moz-filter:blur(0px) !important;
            -o-filter:blur(0px) !important;
    }

    .modal-open .container-fluid, .modal-open .container{
        filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
    }

    #errorBox{
        color:#F00;
    }
    .aslink {
        cursor: pointer;
    }

</style>
<div style="min-height:430px !important;" ng-app="sqlModule">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading"><?php echo $title; ?></div>
            <div class="panel-body">

                <form name="frmregsql" id="frmregsql" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div> 
                        <div class="container">
                            <label for="dateFrom">What this Sql will Do?:<span class="star">*</span></label>
                            <input type="text" style="width: 90%;" class="form-control" name="subject" id="subject" ng-minlength="10" restrict-Special-Characters-Directive ng-model="subject" required> 
                            <br /><span class="star small" ng-show="frmregsql.subject.$touched && frmregsql.subject.$error.required">Value in this field is required.</span>
                            <span class="star small" ng-show="frmregsql.subject.$error.minlength">value is too short.</span>
                        </div>
                        <div class="container">&nbsp;</div>
                        <div class="container">
                            <label for="dateFrom">Enter sql query, which you want to execute:<span class="star">*</span></label>
                            <textarea type="text" style="width: 90%;" class="form-control" name="sql" id="sql" cols="20" ng-minlength="10" ng-model="sql" required ></textarea>
                            <br /><span class="star small" ng-show="frmregsql.sql.$touched && frmregsql.sql.$error.required">Value in this field is required.</span>
                            <span class="star small" ng-show="frmregsql.sql.$error.minlength">value is too short.</span>
                        </div>
                        <div class="container">&nbsp;</div>
                        <div class="container"><center><input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit Sql" ng-disabled="frmregsql.subject.$invalid || frmregsql.sql.$invalid"/> <input type="hidden" name="id" id="id" value=""></center></div>
                        <p>&nbsp;</p>

                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<div id="query_result_model" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 1150;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Query Result</h4>
            </div>
            <div class="modal-body">
                <form name="frmQryResponse" id="frmQryResponse" class="form-inline" role="form" method="POST" enctype="multipart/form-data">
                    <div class="container">
                        <div id="errorBox"></div>
                    </div>
                    <div id="query_response"></div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

		$("#btnSubmit").click(function () {
            registerSql();
        });

        $("#grid").on('click', '.aslink', function () {
            performAction(this);
        });
        $('div[id=loader-wrapper]').remove();
        showData();

        $('.close').click(function () {
            $('div[id=loader-wrapper]').remove();
        });

    });

    function performAction(eObj) {
        var act = eObj.id.split('-');
        var effOn = '#act_' + act[0] + '_' + act[1];
        if (act[0] == 'del') {
            var isok = confirm('Are you sure to remove this sql?');
            if (!isok) return; 
            $(effOn).html("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        } else if (act[0] == 'edit') {
            $('#subject').val($('#subject-'+act[1]).html());
            $('#sql').val($('#qry-'+act[1]).html());
            $('#id').val(act[1]);
            return;
        }
        $("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
        $.ajax({
            type: "post",
            url: "common/cfexereports.php",
            data: "action=exesqls&elemId=" + eObj.id,
            success: function (data) {
                if (act[0] == 'del') {
                    showData();
                    $('#response').html("<p class='star'><span>Successfully removed data.</span></p>");
                } else if (act[0] == 'execute') {
                    $("#query_response").html(data);
                    $('#example2').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $("#query_result_model").modal("show");
                }
            }
        });
    }

    function registerSql() {
            $('#response').empty();
            $('#response').html("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var forminput = $("#frmregsql").serialize();
            var url = "common/cfexereports.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=Add&" + forminput; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    //alert(data);
                    $('#response').empty();

                    $("#grid").html(data);
                    showData();
                }
            });
        }

    function showData() {
            $('#response').empty();
            $('#response').html("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $('#subject').val('');
            $('#sql').val('');
            $('#id').val('');
            var forminput = $("#frmregsql").serialize();
            var url = "common/cfexereports.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=GETDATA&" + forminput; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    //alert(data);
                    $('#response').empty();

                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
    #do-optimization:hover {
        font-weight: bold;
        text-decoration: underline;
        cursor: pointer;
    }
</style>
</html>
