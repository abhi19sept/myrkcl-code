<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Welcome To RKCL Administration</title>



        <link href="rkcltheme/css/index.css" rel="stylesheet" type="text/css" />

        <!--Menu css and js start-->

        <!--<link rel='stylesheet' type='text/css' href="menu/menu.css" />
        <script type='text/javascript' src='menu/menu_jquery.js'></script>-->

        <link rel='stylesheet' type='text/css' href="rkcltheme/menu/main.css" />

        <!--Menu css and js End-->

        <!--Banner Css and Js Start -->


        <link rel="stylesheet" href="rkcltheme/banner/demo.css" />

        <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script src="rkcltheme/banner/bjqs-1.3.min.js"></script>

        <!--Banner Css and Js End -->


        <!--Form Css and Js Start-->


        <link rel="stylesheet" type="text/css"  href="rkcltheme/css/smart-forms.css">
            <link rel="stylesheet" type="text/css"  href="rkcltheme/css/font-awesome.min.css">

    <!--<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>-->


 <!--<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>-->
                <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
                <script type="text/javascript" src="rkcltheme/js/additional-methods.min.js"></script>





                </head>

                <body>


                    <?php
                    include './rkcltheme/include/header.php';

                    include './rkcltheme/include/menu.php';
                    ?>
                    <div class="body_main_div">
                        <div class="body_sub_main_div">
                            <div class="bfb_main_div">
                                <div class="smart-forms smart-container wrap-0">
                                    <?php
                                    if (isset($_REQUEST['code'])) {
                                        echo "<script>var RoleCode=" . $_REQUEST['code'] . "</script>";
                                        echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
                                    } else {
                                        echo "<script>var RoleCode=0</script>";
                                        echo "<script>var Mode='Add'</script>";
                                    }
                                    ?>

                                    <div class="form-header">
                                        <h4>User Role Master</h4>
                                    </div>        
                                    <form name="frmuserrolemaster" id="frmuserrolemaster" action="">
                                        <div class="form-body theme-green">
                                            <div class="frm-row">
                                                <div class="section colm colm12">
                                                    <label class="option option-green" id="response">

                                                    </label>
                                                </div>
                                            </div>
                                            <div class="frm-row">
                                                <div class="section colm colm4">
                                                    <label class="field prepend-icon">Role Name</label>

                                                    <label class="field prepend-icon" >
                                                        <input type="text" id="txtRoleName" name="txtRoleName"  class="gui-input"/></label>

                                                </div> 
                                            </div>
                                            <div class="frm-row">
                                                <div class="section colm colm4">
                                                    <label class="field prepend-icon" >Role Status
                                                    </label>

                                                    <label class="field prepend-icon">
                                                        <select id="ddlStatus" name="ddlStatus" class="select">

                                                        </select>
                                                    </label>

                                                </div> 
                                            </div>
                                            <div class="frm-row">
                                                <div class="section colm colm12">
                                                    <input type="submit" id="btnSubmit" name="btnSubmit" value="Submit" class="button btn-green" />
                                                 </div> 
                                            </div>
                                             <div class="frm-row">
                                                 <div class="section colm colm12" id="grid" >
                                                     
                                                 </div> 
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>     
                    </div>
                    <!--Body End-->

                    <!--Footer Start-->
                    <?php
                    include './rkcltheme/include/footer.php';
                    include './common/message.php';
                    ?>
                    <!--Footer End-->



                    <script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {

                            if (Mode == 'Delete')
                            {
                                if (confirm("Do You Want To Delete This Item ?"))
                                {
                                    deleteRecord();
                                }
                            }
                            else if (Mode == 'Edit')
                            {
                                fillForm();
                            }

                            function FillStatus() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfStatusMaster.php",
                                    data: "action=FILL",
                                    success: function (data) {
                                        $("#ddlStatus").html(data);
                                    }
                                });
                            }

                            FillStatus();
                            function deleteRecord()
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                $.ajax({
                                    type: "post",
                                    url: "common/cfUserRoleMaster.php",
                                    data: "action=DELETE&values=" + RoleCode + "",
                                    success: function (data) {
                                        //alert(data);
                                        if (data == SuccessfullyDelete)
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                            window.setTimeout(function () {
                                                window.location.href = "frmuserrolemaster.php";
                                            }, 3000);
                                            Mode = "Add";
                                            resetForm("frmuserrolemaster");
                                        }
                                        else
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                        }
                                        showData();
                                    }
                                });
                            }


                            function fillForm()
                            {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfUserRoleMaster.php",
                                    data: "action=EDIT&values=" + RoleCode + "",
                                    success: function (data) {
                                        //alert($.parseJSON(data)[0]['Status']);
                                        data = $.parseJSON(data);
                                        txtRoleName.value = data[0].RoleName;
                                        ddlStatus.value = data[0].Status;

                                    }
                                });
                            }

                            function showData() {

                                $.ajax({
                                    type: "post",
                                    url: "common/cfUserRoleMaster.php",
                                    data: "action=SHOW",
                                    success: function (data) {

                                        $("#gird").html(data);

                                    }
                                });
                            }

                            showData();


                            $("#btnSubmit").click(function () {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                var url = "common/cfUserRoleMaster.php"; // the script where you handle the form input.
                                var data;
                                if (Mode == 'Add')
                                {
                                    data = "action=ADD&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                                }
                                else
                                {
                                    data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                                }
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                            window.setTimeout(function () {
                                                window.location.href = "frmuserrolemaster.php";
                                            }, 1000);

                                            Mode = "Add";
                                            resetForm("frmuserrolemaster");
                                        }
                                        else
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                        }
                                        showData();


                                    }
                                });

                                return false; // avoid to execute the actual submit of the form.
                            });
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
                </body>

                </html>