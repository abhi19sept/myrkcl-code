<?php
$title = "EOI DD Payment Confirm";
include ('header.php');
include ('root_menu.php');
?>
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">DD Payment Confirm</div>
        <div class="panel-body">
            <form name="frmddpaymentconfirm" id="frmddpaymentconfirm" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>


                    <div class="col-sm-4 form-group">     
                        <label for="course">Select Course:</label>
                        <select id="ddlCourse" name="ddlCourse" class="form-control">

                        </select>
                    </div> 
                </div>

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Batch:</label>
                        <select id="ddlBatch" name="ddlBatch" class="form-control">

                        </select>									
                    </div> 
                </div>

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Center Code:</label>
                        <select id="ddlcentercode" name="ddlcentercode" class="form-control">

                        </select>											
                    </div> 
                </div>

                <div id="menuList" name="menuList" style="margin-top:35px;"> </div> 

                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                </div>
        </div>
    </div>   
</div>
</form>


</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLAdmissionCourseDD",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });


        function showEOICenterCode(val, val1) {
            $.ajax({
                type: "post",
                url: "common/cfddeoipayment.php",
                data: "action=GETCENTERCODE&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlcentercode").html(data);
                }
            });
        }

        function showDdModeData(val) {
            $.ajax({
                type: "post",
                url: "common/cfddeoipayment.php",
                data: "action=GETDDMODEDATA&refno=" + val + "",
                success: function (data) {
                    //alert(data);
                    $("#menuList").html(data);
                }
            });
        }

        $("#ddlBatch").change(function () {
            showEOICenterCode(ddlBatch.value, ddlCourse.value);
        });

        $("#ddlcentercode").change(function () {
            showDdModeData(ddlcentercode.value);
        });


        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfddeoiPayment.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmddpaymentconfirm").serialize();

            data = "action=ADD&" + forminput; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmeoiddconfirm.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmddpaymentconfirm");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>