<?php
    $title = "Profile Dashboard";
    include('header.php');
    include('root_menu.php');
    include 'common/modals.php';

    if ($_SESSION["User_UserRoll"] <> 7) {
        echo "<script>$('#unauthorized').modal('show')</script>";
        die;
    }
?>

<style type="text/css">
    .card {
        box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.2);
        transition: 0.5s;
        width: 100%;
        padding: 10px 0 10px 20px;
        cursor: pointer;
    }

    .card:hover {
        box-shadow: 0 0 20px 5px rgba(0, 0, 0, 0.2);
    }

    .col-sm-3:hover {
        background: none !important;
    }

    .card > div > h4 {
        margin: 0 !important;
    }

    .glyph {
        display: inline-block;
        font-size: 60px;
        vertical-align: top;
    }

    #changeAddress:hover{
        background-color: #0086b0;
        color: #FFF;
    }

    #viewAddress:hover{
        background-color: #DD4B39;
        color: #FFF;
    }

    #changeName:hover{
        background-color: #00A65A;
        color: #FFF;
    }

    #viewName:hover{
        background-color: #FF851B;
        color: #FFF;
    }

    html {
        overflow-x: hidden !important;
    }
</style>

<div style="min-height:430px !important;max-height:auto !important">
    <div class="container" style="margin-top:50px">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-map-marker"></span> Address Requests</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <?php if ($_SESSION['User_UserRoll'] != 19) { ?>

                                <div class="col-sm-6">
                                    <div class="card" id="changeAddress" data-toggle="tooltip" data-placement="bottom" title="Click here to create Address Update Request">
                                        <div style="display: inline-block">
                                            <h4><b>Address</b></h4>
                                            <p>Change Request</p>
                                        </div>
                                        <div class="glyph">
                                            <span class="glyphicon glyphicon-map-marker"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="card" id="viewAddress" data-toggle="tooltip" data-placement="bottom" title="Click here to view all Address Update Requests">
                                        <div style="display: inline-block">
                                            <h4><b>View All</b></h4>
                                            <p>Address Requests</p>
                                        </div>
                                        <div class="glyph pull-right" style="padding-right: 15px;">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-th-list"></span> Name Requests</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">

                            <?php if ($_SESSION['User_UserRoll'] != 19) { ?>

                                <div class="col-sm-6">
                                    <div class="card" id="changeName" data-toggle="tooltip" data-placement="bottom" title="Click here to create Name Update Request">
                                        <div style="display: inline-block">
                                            <h4><b>Name</b></h4>
                                            <p>Change Request</p>
                                        </div>
                                        <div class="glyph pull-right" style="padding-right: 15px;">
                                            <span class="glyphicon glyphicon-th-list"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="card" id="viewName" data-toggle="tooltip" data-placement="bottom" title="Click here to view all Name Update Requests">
                                        <div style="display: inline-block">
                                            <h4><b>View All</b></h4>
                                            <p>Name Requests</p>
                                        </div>
                                        <div class="glyph pull-right" style="padding-right: 15px;">
                                            <span class="glyphicon glyphicon-arrow-down"></span>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();

        $("#changeAddress").on("click", function () {
            $(location).attr('href','frmupdateorgdetails_address.php');
        });

        $("#changeName").on("click", function () {
            $(location).attr('href','frmupdateorgdetails_name.php');
        });

        $("#viewAddress").on("click", function () {
            $(location).attr('href','frmAddUpdReqITGK.php');
        });

        $("#viewName").on("click", function () {
            $(location).attr('href','frmNameUpdReqITGK.php');
        });
    });
</script>
</body>
<?php include('footer.php'); ?>
</html>