<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//$title = "Print Fee Receipt";
//include ('header.php');
//include 'common/commonFunction.php';
require_once("common/cfLIVRrpt1.php");
require 'DAL/upload_ftp_doc.php';
if (isset($_REQUEST['code'])) {


//require_once("commom/cfPrintReceipt.php"); 
    $data = array();
    $data = getdata($_REQUEST['code']);
    //echo $data;
    $_row = mysqli_fetch_array($data);
	$_ObjConnection = new ftpConnection();
    $details= $_ObjConnection->ftpdetails();

	$_LearnerBatchNameFTP2 = $_REQUEST['ftpbatch'];

	$srcphoto = $details.'admission_photo/'.$_LearnerBatchNameFTP2.'/'.$_row['Admission_Photo'];
	$srcsign = $details.'admission_sign/'.$_LearnerBatchNameFTP2.'/'.$_row['Admission_Sign'];

	$photoimage =  file_get_contents($srcphoto);
	$signimage =  file_get_contents($srcsign);
	$imageData = base64_encode($photoimage);
	$imageDataSign = base64_encode($signimage);
	// <a data-fancybox='gallery' href='data:image/png;base64, ".$imageData."'><img src="small_1.jpg"></a>
	
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<button style="display:none; " id="printthis">Print this page</button>
<script type="text/javascript">
    $(document).ready(function () {


	   $("#printthis").on("click", function(){
	      window.print();
	   }); 
	  
	   $("#printthis").click(); //trigger event after listening to it.

  });

</script>
<?php
	$html1 = '<div  style="background-image:url("images/RKCL.png");min-height: 500px; background-repeat:repeat-y;">';

    $html1.= '<div align="center" ><b>Learner Information Verification Report (Learner Copy)</b></div>

  	<p>Admission Date & Time: ' . date("d-m-Y H:i", strtotime($_row['Timestamp'])) . '</p>                                                                                                                                   </p>			  
         <div class="col-md-9 col-lg-9" style="height:305px; width: 70%; float:left;">
								  	
								
         <table class="table table-user-information" style="width: 100%; height: 100%; border: 1px solid black;border-collapse: collapse;">
         <tbody>
		
         <tr>
        <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">ITGK Code :</td>
        
		
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_ITGK_Code'] . '</td>
		
		
		 </tr>
		
		
		  <tr>
        
		 </tr>
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">ITGK Name:</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Organization_Name'] . '</td>
		
	
         </tr>
         <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">District Name:</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['District_Name'] . '</td>
		
	
         </tr>
		
		 <tr>
	 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Learner Id :</td>
         <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_LearnerCode'] . '</td>
		
		 </tr>
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Form No. :</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_Code'] . '</td>
		
	
		 </tr>
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Learner Name :</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . strtoupper($_row['Admission_Name']) . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Father/Husband Name:</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . strtoupper($_row['Admission_Fname']) . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Date of Birth:</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . date_format(date_create($_row['Admission_DOB']),"d-m-Y") . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Mobile No:</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_Mobile'] . '</td>
		
	
		 </tr>
		
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Email: </td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_Email'] . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Course Name: </td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Course_Name'] . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Batch : </td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Batch_Name'] . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Medium : </td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_Medium'] . '</td>
		
	
		 </tr>
		
		
			 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Course Fee (Rs.) : </td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_Fee'] . '/-</td>
		
	
		 </tr>
		
		
		
         </tbody>
       
       
         </table>
		 
            
		
		  </div>
		 
		 
		 
		 
		  <div align="center" class="col-md-3 col-lg-3" style="height:305px;border:2px solid black; width: 29%; float:left;" >
		
		 <p align="center">Profile Picture </p >
		<img class="img-squre img-responsive" width="80" style="padding:2px;height:107px;border:1px solid #428bca;margin-top:10px;" id="ftpimg" src="data:image/png;base64, '.$imageData.'" alt="Red dot"/>

	
		 <p  align="center"  style="margin-top:10px;">Sign </p>
		<img class="img-squre img-responsive" width="80" style="padding:2px;height:35px;border:1px solid #428bca;margin-top:10px;" id="ftpimg" src="data:image/png;base64, '.$imageDataSign.'" alt="Red dot"/>

		 </div>
		 
		 
		   <div align="center" class="col-md-12 col-lg-12"  >
		  	 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
               &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
               &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; I hereby declare that I have accepted filled up application form from the learner. I have accepted all necessary documents (ID proof, DOB Proof) from the learner and verified the same before entering into the system. </p >
		   </div>
                   
			<div ><span >Date & Time: ' . date("d/m/Y") . '</span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
               &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
              <span>Learner Signature </span>
			  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
		  	 <span>IT-GK Seal & Signature </span> </div>
	<br>
        <br>
        <hr>';
        $html1.= '<div align="center"><b>Learner Information Verification Report (ITGK Copy)</b></div>

  	<p>Date & Time: Admission Date & Time: ' . date("d-m-Y H:i", strtotime($_row['Timestamp'])) . '</p>                                                                                                                                  </p>			  
         <div class="col-md-9 col-lg-9" style="height:305px;width: 70%; float:left;">
								  
								
         <table class="table table-user-information" style="width: 100%; height: 100%; border: 1px solid black;border-collapse: collapse;">
         <tbody>
		
         <tr>
        <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">ITGK Code :</td>
        
		
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_ITGK_Code'] . '</td>
		
		
		 </tr>
		
		
		  <tr>
        
		 </tr>
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">ITGK Name:</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Organization_Name'] . '</td>
		
	
         </tr>
          <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">District Name:</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['District_Name'] . '</td>
		
	
         </tr>
		
		 <tr>
	 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Learner Id :</td>
         <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_LearnerCode'] . '</td>
		
		 </tr>
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Form No. :</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_Code'] . '</td>
		
	
		 </tr>
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Learner Name :</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . strtoupper($_row['Admission_Name']) . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Father’s Name:</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . strtoupper($_row['Admission_Fname']) . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Date of Birth:</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . date_format(date_create($_row['Admission_DOB']),"d-m-Y") . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Mobile No:</td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_Mobile'] . '</td>
		
	
		 </tr>
		
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Email: </td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_Email'] . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Course Name: </td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Course_Name'] . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Batch : </td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Batch_Name'] . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Medium : </td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_Medium'] . '</td>
		
	
		 </tr>
		
		
			 <tr>
		 <td class="col-md-2 col-lg-2" style="border: 1px solid black;border-collapse: collapse;">Course Fee (Rs.) : </td>
		 <td class="col-md-3 col-lg-3" style="border: 1px solid black;border-collapse: collapse;">' . $_row['Admission_Fee'] . '/-</td>
		
	
		 </tr>
		
		
		
         </tbody>
       
       
         </table>
		 	
            
		
		  </div>
		 
		 
		 
		 
		  <div align="center" class="col-md-3 col-lg-3" style="height:305px;border:2px solid black; width: 29%; float:left;" >
		
		 <p align="center">Profile Picture </p >
		
		<img class="img-squre img-responsive" width="80" style="padding:2px;height:107px;border:1px solid #428bca;margin-top:10px;" id="ftpimg" src="data:image/png;base64, '.$imageData.'" alt="Red dot"/>

	
		 <p  align="center"  style="margin-top:10px;">Sign </p>
		<img class="img-squre img-responsive" width="80" style="padding:2px;height:35px;border:1px solid #428bca;margin-top:10px;" id="ftpimg" src="data:image/png;base64, '.$imageDataSign.'" alt="Red dot"/>
		 </div>

		 
		 
		   <div align="center" class="col-md-12 col-lg-12"  >
		  	 <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
               &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
               &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; I hereby declare that I have provided filled up application form and all necessary documents to ITGK and information given above are accurate and correct.  </p >
		   </div>
                   
          			<div ><span >Date & Time: ' . date("d/m/Y") . '</span> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
               &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
              <span>IT-GK Seal & Signature </span>
			  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
		  	 <span>Learner Signature </span> </div>';
			 $html1.= '</div>';
}

echo $html1;
die;
ob_clean();
header('Content-type: application/pdf');
//header('Content-Disposition: inline; filename="' . $yourFileName . '"');
header('Content-Transfer-Encoding: binary');
header('Accept-Ranges: bytes');

require_once 'mpdf60/mpdf.php';
$mpdf=new mPDF('c','A4','','' ,15, 10, 16, 10, 10, 10); 
$mpdf->SetHeader('|Learner Information Verification Report|');
$mpdf->setFooter(' '); // Giving page number to your footer.
$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0; 
$stylesheet = file_get_contents('css/style.css');
$mpdf->WriteHTML($stylesheet,1);
$mpdf->WriteHTML($html1, 2);
$mpdf->Output($_row['Admission_LearnerCode'] . '_LIVR.pdf', 'I');
ob_end_flush();
?>
