<?php

class DB_Functions {

    private $conn;

    function __construct() {
        require_once 'DB_Connect.php';
        $db = new Db_Connect();
        $this->conn = $db->connect();
        if (!mysqli_set_charset($this->conn, "utf8")) {
            printf("Error loading character set utf8: %s\n", mysqli_error($this->conn));
            exit();
        }
    }
function __destruct() {
        
    }
    //Abhishek
    public function PutFacultyIlearnerScore($rowdata) {

        $stmt = $this->conn->prepare("INSERT INTO tbl_facultyexamresultreport ( ResExam_examid, ResExam_obtainmarks, ResExam_totalmarks, ResExam_attempt, ResExam_batch, ResExam_course, ResExam_learner, Admission_RspName, Admission_District, ITGK_Code, DPO, Lat, Longs) VALUES (".$rowdata.")");
          if ($stmt->execute()) {
            $result = $stmt->get_result();
            // $num_of_rows = $result->num_rows;
            // $ChapterList = array();
            // while ($row = $result->fetch_assoc()) {
            //     $ChapterList[] = $row;
            // }
            // $stmt->close();

            return TRUE;
        } else {
            return NULL;
        }
    }

}

?>
