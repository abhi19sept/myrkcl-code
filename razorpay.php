<?php

require('razorpay/razorconfig.php');
require('common/generalFunctions.php');

//
// We create an razorpay order using orders api
// Docs: https://docs.razorpay.com/docs/orders
//

$itgk = $general->getTransactionItgkDetails($trnxId);
$details = [
    "pay_for" => $itgk['payfor'],
    "name"    => "ITGK(" . $itgk['Pay_Tran_ITGK'] . ")",
    "email"   => $itgk['User_EmailId'],
    "phone"   => $itgk['User_MobileNo'],
    "address" => $itgk['Organization_Address'],
    "orderId" => $itgk['Pay_Tran_Code'],
    "firstname"=> $itgk['Organization_Name'],
    "itgkcode"=> $itgk['Pay_Tran_ITGK'],
    "txnid"   => $itgk['Pay_Tran_PG_Trnid'],
    "batch"   => $itgk['Pay_Tran_Batch'],
    "course"  => $itgk['Pay_Tran_Course'],
    "amount"  => $itgk['Pay_Tran_Amount'],
];

$orderData['receipt'] = $details['orderId'];
$orderData['amount'] = $details['amount'] * 100; // 2000 rupees in paise;
$razorpayOrder = $api->order->create($orderData);
$general->saveRazorPayOrderId($details['orderId'], $razorpayOrder['id']);

$data = [
    "key"                   => $keyId,
    "amount"                => $details['amount'],
    "name"                  => $details['name'],
    "description"           => $details['pay_for'],
    "image"                 => $logo,
    "prefill"               => [
        "name"              => $details['name'],
        "email"             => $details['email'],
        "contact"           => $details['phone'],
    ],
    "notes"                 => [
        "address"           => $details['address'],
        "merchant_order_id" => $details['orderId'],
        "firstname"         => $details['firstname'],
        "itgkcode"          => $details['itgkcode'],
        "txnid"             => $details['txnid'],
        "batch"             => $details['batch'],
        "course"            => $details['course'],
    ],
    "theme"                 => $theme,
    "order_id"              => $razorpayOrder['id'],
];

echo json_encode($data);


