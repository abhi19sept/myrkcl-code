<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?php
//$User_Code=$_GET['usercode'];
//echo "$User_Code";
$User_Code = 48;
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>AO Verification</title>
        <link href="css/jquery-ui.css" rel="Stylesheet" type="text/css" />
    </head>

    <body>
        <div class="wrapper">
            <?php
            include './include/header.html';

            include './include/menu.php';

            if (isset($User_Code)) {
                echo "<script>var VerifyCode=" . $User_Code . "</script>";
                //echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var VerifyCode=0</script>";
                //echo "<script>var Mode='Add'</script>";
            }
            ?>
            <div class="main">
                <h1>User Profile Details</h1>
                <form name="frmVerifyAO" id="frmVerifyAO" action="">
                    <table border="0" cellpadding="0" cellspacing="10" width="100%" class="field">
                        <tr>
                            <td colspan="2" id="response">

                            </td>
                        </tr>

                        <tr>
                            <td nowrap="nowrap" width="30%">Select Program Support Agency</td>
                            <td>:</td>
                            <td>
                                <select id="ddlPSA" name="ddlPSA" class="text">

                                </select>
                                &nbsp;<font color="red">*</font>
                            </td>
                        </tr>

                        <tr>
                            <td nowrap="nowrap" width="30%">Select District</td>
                            <td>:</td>
                            <td>
                                <select id="ddlDLC" name="ddlDLC" class="text">

                                </select>
                                &nbsp;<font color="red">*</font>
                            </td>
                        </tr>



                        <tr>
                            <td colspan="3" align="right">
                                <p class="btn submit"><input type="submit" id="btnSubmit" name="btnSubmit" value="Submit" /></p>
                            </td>
                        </tr>

                    </table>
                </form>
            </div>
            <?php
            include './include/footer.html';
            ?>
        </div>
    </body>
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {


            function FillDLC() {
                $.ajax({
                    type: "post",
                    url: "common/cfOrgDetail.php",
                    data: "action=FILLDLC",
                    success: function (data) {
                        $("#ddlDLC").html(data);
                    }
                });
            }

            FillDLC();


            function FillPSA() {
                $.ajax({
                    type: "post",
                    url: "common/cfOrgDetail.php",
                    data: "action=FILLPSA",
                    success: function (data) {
                        $("#ddlPSA").html(data);
                    }
                });
            }
            FillPSA();


            $("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfSignUp.php"; // the script where you handle the form input.
   
                    data = "action=UPDATE&code=" + VerifyCode +
                            "&dlcname=" + ddlDLC.value + "&psaname=" + ddlPSA.value +""; // serializes the form's elements.
            
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmuserverified.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("frmOrgDetail");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
</html> 