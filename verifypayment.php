<?php
include 'common/commonFunction.php';
require('frmPayuTransactionResponse.php');
require('common/generalFunctions.php');

$error = "Unknown Error";

/*
print_r($_POST);
die;
*/

if (empty($_POST['razorpay_payment_id']) === false) {
    $verifiedTrnxId = verifyRozorPayment($_POST['razorpay_payment_id'], $_POST['razorpay_signature']);
} else {
	$verifiedTrnxId = '';
}

/*
echo $verifiedTrnxId;
die('C2');
*/

$location = '';
if (!empty($verifiedTrnxId)) {
    $response = getResponseByPaymentTransaction(0, $verifiedTrnxId);
    /*
	print_r($response);
	die('C3');
	*/
	
	if ($response) {
		$paymentResponse = $payment->GetPaymentDataByTrnxid($verifiedTrnxId);
		if ($paymentResponse) {
			$payData = mysqli_fetch_array($paymentResponse[2]);
			$response['email'] = $payData['UserProfile_Email'];
			$response['addedon'] = $payData['timestamp'];
		}
		$response['status'] = 'success';
		$response['verify_by'] = 'VerifiedByRazorPay';
		
		processResponse($response);
		$location = getResponseLocation($response);
	}
	$html = "<p>Your payment is completed successfully.</p>";
} else {
	if (!empty($_POST['razorpay_payment_id'])) {
		$payment = getRazorpayPayment($_POST['razorpay_payment_id']);
		if ($payment && !empty($payment->status)) {
			$order = $payment->notes;
			$orderId = $order->merchant_order_id;
			$response = getResponseByPaymentTransaction(0, '', $orderId);
			if (isset($response['tran_status']) && $response['tran_status'] != 'PaymentReceive' &&  $response['tran_status'] != 'PaymentToRefund') {
				$response['status'] = ($payment->status == 'captured') ? 'success' : 'RazorPaymentFailure';
				$response['amount'] = ($payment->amount / 100);
				$response['verify_by'] = 'VerifiedByRazorPay';
				$response['addedon'] = date("Y-m-d H:i:s");
				processResponse($response);

				$response['status'] = 'RazorPaymentFailure';
				$location = getResponseLocation($response);
			}
		}
	}
    $html = "<p>Your payment has been failed</p>";
}

/*
echo $html;
print_r($response);
die('C4');
*/

if (!empty($location)) {
	//header("Location:" . $location);
	require($location);
} else {
	echo $html;
}

function verifyRozorPayment($paymentId, $signature) {
	global $api;
	$verifiedTrnxId = '';
	try {
        $attributes = getRazorPayAttributes($paymentId, $signature);
        if (isset($attributes['razorpay_attributes'])) {
	        $api->utility->verifyPaymentSignature($attributes['razorpay_attributes']);
	        $verifiedTrnxId = $attributes['verifiedTrnxId'];
    	}
    } catch(SignatureVerificationError $e) {
        $verifiedTrnxId = '';
    }

    return $verifiedTrnxId;
}

function getRazorPayAttributes($paymentId, $signature) {
	global $general;
	$attributes = [];
	$payment = getRazorpayPayment($paymentId);
	/*
	print_r($payment);
	die('C1');
	*/
	if ($payment && isset($payment->amount)) {
		$order = $payment->notes;
		$orderTranxId = $general->verifyRozorpayTransactionResponse($order->merchant_order_id, ($payment->amount / 100)); //$payment->amount
		if ($orderTranxId && $payment->status == 'captured') {
			$orderDetails = $general->getTransactionItgkDetails($orderTranxId);
			if ($orderDetails) {
				$general->saveRazorPayOrderId($order->merchant_order_id, $orderDetails['razorpay_order_id'], $paymentId, $signature);
				$attributes['verifiedTrnxId'] = $orderTranxId;
				$attributes['razorpay_attributes'] = [
			        'razorpay_order_id' => $orderDetails['razorpay_order_id'],
			        'razorpay_payment_id' => $paymentId,
			        'razorpay_signature' => $signature
			    ];
			}
		}
	}

	return $attributes;
}

function getRazorpayPayment($paymentId) {
	global $api;
	
	return $api->payment->fetch($paymentId);
}

function getResponseLocation($response) {
	$response['email'] = $_SESSION['User_EmailId'];
	$_SESSION['razorpay_order_details'] = $response;
	$location = '';
	$location = ($response['status'] == 'success') ? 'frmpaysuccess.php' : 'frmpayfailure.php';

	return $location;
}