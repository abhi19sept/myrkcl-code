<?php
    require_once 'DAL/classconnection.php';

    $_ObjConnection = new _Connection();
	$_ObjConnection->Connect();
	
	date_default_timezone_set("Asia/Kolkata");

	ini_set("memory_limit", "5000M");
    ini_set("max_execution_time", 0);
    set_time_limit(0);
	
	$response = $_POST;
	$allowedIps = ['180.179.174.1', '180.179.174.2', '180.179.174.13', '180.179.174.14', '180.179.174.15', '::1'];
	//if (!empty($response['txnid']) && in_array($responsedIP, $allowedIps)) {
	if (!empty($response['txnid'])) {
		insertPayUResponseLog($response);
		$response = setKeySalt($response);
		$response['verify_by'] = 'VerifiedByServerApi';
	} else {
		$response = getResponseByPaymentTransaction();
		if ($response) {
			$response = setKeySalt($response);
			$response = getTransactionStatusByPayU($response);
		} else {
			exit;
		}
	}

	processResponse($response);

/**
*	Function to get database connection object.
*/
	function dbConnection() {
		global $_ObjConnection;

		return $_ObjConnection;
	}

/**
*	Function to Insert PayU response into tbl_payutransactionresponse table as Log.
*/
	function insertPayUResponseLog($response) {
		$_ObjConnection = dbConnection();

		$responsedIP = $_SERVER['REMOTE_ADDR'];

		$responseFields = ['txnid','mihpayid','mode','status','amount','udf1','udf2','udf3','udf4','udf5','udf6','udf7','udf8','udf9','udf10','field0','field1','field2','field3','field4','field5','field6','field7','field8','field9','hash','bank_ref_no','surl','curl','furl','card_token','card_no','card_hash','productinfo','offer','discount','offer_availed','unmappedstatus','firstname','lastname','address1','address2','city','state','country','zipcode','email','phone'];
		$insert = "";
		foreach ($responseFields as $field) {
			$response[$field] = !empty($response[$field]) ? $response[$field] : '';
			$insert .= $field . " = '" . $response[$field] . "', ";
		}

		$insertResponseSql = "INSERT IGNORE INTO tbl_payutransactionresponse SET
			" . $insert . " 
			tx_key = '" . ((!empty($response['key'])) ? $response['key'] : '') . "',
			responseIP = '" . $responsedIP . "',
			response = '" . serialize($response) . "',
			serverInfo = ''";

		$_Response = $_ObjConnection->ExecuteQuery($insertResponseSql, Message::InsertStatement);
	}

/**
*	Function to process transaction response.
*/
	function processResponse($response) {
		$status = $response["status"];
		$firstname = $response["firstname"];
		$amount = $response["amount"];
		$txnid = $response["txnid"];
		$dbData = getResponseByPaymentTransaction(0, $txnid);
        $productinfo = ($dbData) ? $dbData['productinfo'] : $response["productinfo"];
		$udf1 = $response["udf1"];
		$udf2 = $response["udf2"];
		$udf3 = $response["udf3"];
		$udf4 = $response["udf4"];
		$verifyBy = $response["verify_by"];

		$email = (!empty($response['email'])) ? $response['email'] : '';
		$key = (!empty($response['key'])) ? $response['key'] : '';
		$posted_hash = (!empty($response['hash'])) ? $response['hash'] : '';

		if ($status == 'success' || $status == 'PaymentReceive' || $status == 'Money Settled') {
			//Do further process on success;
			switch ($productinfo) {
				case 'ReexamPayment':
				case 'Re Exam Event Name':
					setSuccessForReExamTransaction($txnid, 'ReexamPayment', $udf1, $udf2, $firstname, $amount, $verifyBy, $key, $posted_hash);
					break;
				case 'LearnerFeePayment':
				case 'Learner Fee Payment':
					setSuccessForLearnerFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $udf3, $udf4, $firstname, $amount, $verifyBy, $key, $posted_hash);
					break;
				case 'Correction Certificate':
				case 'Correction Fee Payment':
				case 'Duplicate Certificate':
					setSuccessForCorrectionFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $verifyBy, $key, $posted_hash);
					break;
				case 'NcrFeePayment':
				case 'Ncr Fee Payment':
					setSuccessForNcrFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $verifyBy, $key, $posted_hash);
					break;
				case 'Name Change Fee Payment':
                case 'Address Change Fee Payment':
                	setSuccessForNameAddressChangeFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $verifyBy, $key, $posted_hash);
                    break;
                case 'EOI Fee Payment':
                	setSuccessForEOIFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $email, $amount, $verifyBy, $key, $posted_hash);
                    break;
			}
		} elseif ($status != 'pending') {
			//Do further process on failer;
			switch ($productinfo) {
				case 'ReexamPayment':
				case 'Re Exam Event Name':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateExamDataPaymentStatus(0, $txnid, $udf1);
					processReExamTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $key, $posted_hash);
					break;
				case 'LearnerFeePayment':
				case 'Learner Fee Payment':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateAdmissionPaymentStatus(0, $txnid, $udf1, $udf3, $udf4);
					processAdmissionTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $udf3, $udf4, $key, $posted_hash);
					break;
				case 'Correction Certificate':
				case 'Correction Fee Payment':
				case 'Duplicate Certificate':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateCorrectionPaymentStatus(0, $txnid, $udf1);
					processCorrectionTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $key, $posted_hash);
					break;
				case 'NcrFeePayment':
				case 'Ncr Fee Payment':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateOrgMasterPaymentStatus(0, $txnid);
					processNCRTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $key, $posted_hash);
					break;
			    case 'Name Change Fee Payment':
                case 'Address Change Fee Payment':
                	updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
                	updateNACTransactionStatus($status, $txnid, $udf1, $amount);
                	updateChangeAddressITGK(3, $txnid, $udf1);
                    break;
                case 'EOI Fee Payment':
                	updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
                	updateEOITransactionStatus($status, $firstname, $email, $amount, $txnid, $productinfo, $udf1, $udf2, $key, $posted_hash);
                	updateCourseITGKMapping(0, $txnid, $udf1);
                	processEOITransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $email, $amount, $key, $posted_hash);
                    break;
			}
		}
	}

/**
*	Function to get pending transaction as response.
*/
	function getResponseByPaymentTransaction($min = 40, $trnxid = '') {
		$_ObjConnection = dbConnection();
		$timestamp = mktime(date("H"), date("i") - $min, date("s"), date("m"), date("d"), date("Y"));
		$time = date("Y-m-d H:i:s", $timestamp);
		$filter = (!empty($trnxid)) ? " AND Pay_Tran_PG_Trnid = '" . $trnxid . "'" : '';
		$selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status IN ('PaymentInProcess', 'PaymentFailure') AND Pay_Tran_Reconcile_status = 'Not Reconciled' AND (Pay_Tran_ProdInfo LIKE ('%exam%') OR Pay_Tran_ProdInfo LIKE ('%learner%') OR Pay_Tran_ProdInfo LIKE ('%Correction%') OR Pay_Tran_ProdInfo LIKE ('%Duplicate%')) AND timestamp <= '" . $time . "' $filter ORDER BY Pay_Tran_Code ASC LIMIT 1";
		
		$selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '68d88872b4b682ed290b'";
		$result = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);
		$response = [];
		if (mysqli_num_rows($result[2])) {
			$data = mysqli_fetch_array($result[2]);
			$response = [
				'firstname' => $data['Pay_Tran_Fname'],
				'amount' => $data['Pay_Tran_Amount'],
				'txnid' => $data['Pay_Tran_PG_Trnid'],
				'productinfo' => $data['Pay_Tran_ProdInfo'],
				'udf1' => $data['Pay_Tran_ITGK'],
				'udf2' => $data['Pay_Tran_RKCL_Trnid'],
				'udf3' => $data['Pay_Tran_Course'],
				'udf4' => $data['Pay_Tran_Batch'],
				'verify_by' => 'VerifiedByCron'
			];
		}

		return (!empty($response)) ? $response : false;
	}

	function deleteFromPaymentRefund($transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "DELETE FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
	}
	

/**
*	Function to get transaction status from PayU as response.
*/
	function setKeySalt($response) {
		switch ($response['productinfo']) {
			case 'LearnerFeePayment':
			case 'Learner Fee Payment':
				$key = "Es3Ozb";
				$salt = "o9aZmTfx";
				break;
			default:
				$key = "X2ZPKM";
				$salt = "8IaBELXB";
				break;
		}

		// For Test Mode
		//$salt = "eCwWELxi";
    	//$key = 'gtKFFx';

		$response['key'] = $key;
		$response['salt'] = $salt;

    	return $response;
	}

	function getTransactionStatusByPayU($response) {
		$txnid = $response['txnid'];
		if (!empty($txnid)) {
		
			$key = $response['key'];
			$salt = $response['salt'];

			$wsUrl = "https://info.payu.in/merchant/postservice?form=2";

		// For Test Mode
		//$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
		
			$command = "verify_payment";
			$hash_str = $key  . '|' . $command . '|' . $txnid . '|' . $salt ;
			$hash = strtolower(hash('sha512', $hash_str));
			$request = [
				'key' => $key,
				'hash' => $hash,
				'var1' => $txnid,
				'command' => $command
			];
			
			$qs = http_build_query($request);
			$payuResponses = curlCall($wsUrl, $qs, TRUE);
			$response['hash'] = $hash;
		}

		if(!empty($payuResponses['status']) && $payuResponses['status'] == 1) {
			foreach($payuResponses['transaction_details'] as $val) {
				$status = $val['status'];
				$response['amount'] = intval($val['amt']);
			}
		} else {
			$status = 'PaymentnotDone';
		}

		$response['status'] = $status;

		return $response;
	}

/**
*	Function to make cURL request to get response.
*/
	function curlCall($wsUrl, $qs, $true) {
		$c = curl_init();
		
		curl_setopt($c, CURLOPT_URL, $wsUrl);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
	
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		
		$o = curl_exec($c);
		if (curl_errno($c)) {
		    $c_error = curl_error($c);
			if (empty($c_error)) {
			  $c_error = 'Some server error';
			}

			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);

		return $arr;
	}

/**
*	Functions for Re Exam Transactions Start
*/
	function setSuccessForReExamTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$refund = checkIfRefundRequiredForReExamTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		} else {
			processToRefundReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		}
	}

	function processToConfirmReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		updateExamDataPaymentStatus(1, $transactionId, $centerCode);
		processReExamTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createReExamTransactionInvoices($transactionId, $amount, $centerCode);
	}

	function createReExamTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT examdata_code AS ref_code FROM examdata WHERE itgkcode = '" . $centerCode ."' AND reexam_TranRefNo = '" . $transactionId . "'  AND paymentstatus = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		insertInvoice($_Responses, $amount, 'tbl_reexam_invoice');
	}

	function isPaymentTransactionFound($productInfo, $transactionId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);

		return mysqli_num_rows($response[2]);
	}

	function updatePaymentTransactionStatus($verifyApiStatus, $transactionStatus, $transactionReconcileStatus, $transactionId, $centerCode = false) {
		$_ObjConnection = dbConnection();
		$filter = ($centerCode) ?  " AND Pay_Tran_ITGK = '" . $centerCode . "'" : '';
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status = '" . $transactionStatus . "' AND Pay_Tran_PG_Trnid = '" . $transactionId . "'" . $filter;
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_payment_transaction SET 
				pay_verifyapi_status = '" . $verifyApiStatus . "',
				Pay_Tran_Status = '" . $transactionStatus . "',
				Pay_Tran_Reconcile_status = '" . $transactionReconcileStatus . "' 
			WHERE 
			Pay_Tran_PG_Trnid = '" . $transactionId . "'" . $filter;
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}
	
	function updateExamDataPaymentStatus($paymentstatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM examdata WHERE paymentstatus = " . $paymentstatus . " AND itgkcode = '" . $centerCode . "'  AND reexam_TranRefNo = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "UPDATE examdata SET paymentstatus = '" . $paymentstatus ."' WHERE itgkcode = '" . $centerCode . "'  AND reexam_TranRefNo = '" . $transactionId . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}
	
	function insertReExamTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Status = '" . $transactionStatus . "' AND Reexam_Transaction_Amount = '" . $amount . "' AND Reexam_Transaction_Txtid = '" . $transactionId . "' AND  Reexam_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_reexam_transaction (
				Reexam_Transaction_Code,
				Reexam_Transaction_Status,
				Reexam_Transaction_Fname,
				Reexam_Transaction_Amount,
				Reexam_Transaction_Txtid,
				Reexam_Transaction_ProdInfo,
				Reexam_Transaction_CenterCode,
				Reexam_Transaction_RKCL_Txid,
				Reexam_Transaction_Hash,
				Reexam_Transaction_Key,
				Reexam_Reconcile_Status,
				Reexam_DateTime
			) 
			SELECT CASE WHEN MAX(Reexam_Transaction_Code) IS NULL THEN 1 ELSE MAX(Reexam_Transaction_Code) + 1 END AS Reexam_Transaction_Code,
			'" . $transactionStatus . "' AS Reexam_Transaction_Status,
			'" . $firstName . "' AS Reexam_Transaction_Fname,
			'" . $amount . "' AS Reexam_Transaction_Amount,
			'" . $transactionId . "' AS Reexam_Transaction_Txtid,
			'" . $productInfo . "' AS Reexam_Transaction_ProdInfo,
			'" . $centerCode . "' AS Reexam_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Reexam_Transaction_RKCL_Txid,
			'" . $posted_hash . "' AS Reexam_Transaction_Hash,
			'" . $key . "' AS Reexam_Transaction_Key,
			'Reconciled'  AS Reexam_Reconcile_Status,
			'" . date("Y-m-d H:i:s") . "' AS Reexam_DateTime 
			FROM tbl_reexam_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateReExamTransactionStatus($transactionStatus, $reExamTransactionAmount, $transactionId, $centerCode, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "
			UPDATE tbl_reexam_transaction SET 
				Reexam_Transaction_Status = '" . $transactionStatus . "', Reexam_Transaction_Amount = '" . $reExamTransactionAmount . "', Reexam_Transaction_Hash = '" . $posted_hash . "', Reexam_Transaction_Key = '" . $key . "',
				Reexam_Reconcile_Status = 'Reconciled' 
			WHERE 
				Reexam_Transaction_Txtid = '" . $transactionId . "' AND  Reexam_Transaction_CenterCode = '" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function checkIfRefundRequiredForReExamTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(reexam_TranRefNo) AS ReexamTxnid FROM examdata WHERE reexam_TranRefNo = '" . $transactionId . "' AND itgkcode = '" . $centerCode . "' GROUP BY reexam_TranRefNo";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$reExamCounts = mysqli_fetch_array($response[2]);
				$reExamAmount = ($reExamCounts['ReexamTxnid'] * 300);
				if ($reExamAmount != $amount) {
					$refund = true;
				}
			} else {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processReExamTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertReExamTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updateReExamTransactionStatus($status, $amount, $transactionId, $centerCode, $key, $posted_hash);
		}
	}
	
	function processToRefundReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processReExamTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updateExamDataPaymentStatus(0, $transactionId, $centerCode);
		
	}

	function setForPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_payment_refund (
				Payment_Refund_Id,
				Payment_Refund_Txnid,
				Payment_Refund_Amount,
				Payment_Refund_Status,
				Payment_Refund_ProdInfo,
				Payment_Refund_ITGK,
				Payment_Refund_RKCL_Txid
			)
			SELECT CASE WHEN MAX(Payment_Refund_Id) IS NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
			'" . $transactionId . "' AS Payment_Refund_Txnid,
			'" . $amount . "' AS Payment_Refund_Amount,
			'" . $status . "' AS Payment_Refund_Status,
			'" . $productInfo . "' AS Payment_Refund_ProdInfo,
			'" . $centerCode . "' AS Payment_Refund_ITGK,
			'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid
			FROM tbl_payment_refund";

			$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "'";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (! mysqli_num_rows($response[2])) {
				$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
			}
		}
	}
/**
*	Functions of Re Exam Transactions End
*/

/**
*	Functions for Learner Fee Transactions Start
*/
	function setSuccessForLearnerFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForLearnerFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		} else {
			processToRefundAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		}
	}

	function processAdmissionTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * From tbl_admission_transaction WHERE Admission_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		$numRows = mysqli_num_rows($response[2]);
		if(!$numRows) {
			insertAdmissionTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash);
		} else {
			updateAdmissionTransactionStatus($status, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash);
		}
	}

	function processToConfirmAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		updateAdmissionPaymentStatus(1, $transactionId, $centerCode, $course, $batch);
		processAdmissionTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createAdmissionTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch);
	}

	function createAdmissionTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Admission_Code AS ref_code FROM tbl_admission WHERE Admission_ITGK_Code = '" . $centerCode ."' AND Admission_TranRefNo = '" . $transactionId . "'  AND Admission_Payment_Status = '1' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		insertInvoice($_Responses, $amount, 'tbl_admission_invoice');
	}

	function insertInvoice($_Responses, $amount, $table) {
		$_ObjConnection = dbConnection();
		$admissionTimestamp = date("Y-m-d H:i:s");
		if($_Responses[0] == "Success") {
			$count = mysqli_num_rows($_Responses[2]);
			$amt = ($amount / $count);
			$refField = ($table == 'tbl_correction_invoice') ? 'transaction_ref_id' : (($table == 'tbl_reexam_invoice') ? 'exam_data_code' : 'invoice_ref_id');
			while ($_Row = mysqli_fetch_array($_Responses[2])) {
				$_Insert = "INSERT IGNORE INTO $table (invoice_no, " . $refField . ", amount, addtime) SELECT (MAX(invoice_no) + 1) AS invoice_number, '" . $_Row['ref_code'] . "' AS invoice_ref_id, '" . $amt . "' AS amount, '" . $admissionTimestamp . "' AS addtime FROM $table";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
			}
		}
	}
	
	function updateAdmissionPaymentStatus($paymentstatus, $transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "UPDATE tbl_admission SET Admission_Payment_Status = '" . $paymentstatus . "', Admission_Date_Payment = now() WHERE Admission_ITGK_Code = '" . $centerCode . "' AND Admission_TranRefNo = '" . $transactionId . "' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function insertAdmissionTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_admission_transaction WHERE Admission_Transaction_Status = '" . $transactionStatus . "' AND Admission_Transaction_Amount = '" . $amount . "' AND Admission_Transaction_Txtid = '" . $transactionId . "' AND  Admission_Transaction_CenterCode = '" . $centerCode . "' AND Admission_Transaction_Course = '" . $course . "' AND  Admission_Transaction_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_admission_transaction (
				Admission_Transaction_Code,
				Admission_Transaction_Status,
				Admission_Transaction_Fname,
				Admission_Transaction_Amount,
				Admission_Transaction_Txtid,
				Admission_Transaction_ProdInfo,
				Admission_Transaction_CenterCode,
				Admission_Transaction_RKCL_Txid,
				Admission_Transaction_Course,
				Admission_Transaction_Batch,
				Admission_Transaction_Key,
				Admission_Transaction_Hash,
				Admission_Transaction_Reconcile_Status,
				Admission_Transaction_DateTime
			) 
			SELECT CASE WHEN MAX(Admission_Transaction_Code) Is NULL THEN 1 ELSE MAX(Admission_Transaction_Code) + 1 END AS Admission_Transaction_Code,
			'" . $transactionStatus . "' AS Admission_Transaction_Status,
			'" . $firstName . "' AS Admission_Transaction_Fname,
			'" . $amount . "' AS Admission_Transaction_Amount,
			'" . $transactionId . "' AS Admission_Transaction_Txtid,
			'" . $productInfo . "' AS Admission_Transaction_ProdInfo,
			'" . $centerCode . "' AS Admission_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Admission_Transaction_RKCL_Txid,
			'" . $course . "' AS Admission_Transaction_Course,
			'" . $batch . "' AS Admission_Transaction_Batch,
			'" . $key . "' AS Admission_Transaction_Key,
			'" . $posted_hash . "' AS Admission_Transaction_Hash,
			'Reconciled' AS Admission_Transaction_Reconcile_Status,
			'" . date("Y-m-d H:i:s") . "' AS Admission_Transaction_DateTime 
			FROM tbl_admission_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateAdmissionTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
					$updateQuery = "
			UPDATE tbl_admission_transaction SET 
				Admission_Transaction_Status = '" . $transactionStatus . "',
				Admission_Transaction_Amount = '" . $amount . "',
				Admission_Transaction_Key = '" . $key . "',
				Admission_Transaction_Hash = '" . $posted_hash . "',
				Admission_Transaction_Reconcile_Status = 'Reconciled'
			WHERE 
				Admission_Transaction_Txtid = '" . $transactionId . "' AND 
				Admission_Transaction_CenterCode = '" . $centerCode . "' AND 
				Admission_Transaction_Course = '" . $course . "' AND 
				Admission_Transaction_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function checkIfRefundRequiredForLearnerFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(Admission_TranRefNo) AS AdmissionTxnid, Admission_Course, Admission_Batch FROM tbl_admission WHERE Admission_TranRefNo = '" . $transactionId . "' AND Admission_ITGK_Code = '" . $centerCode . "' GROUP BY Admission_TranRefNo, Admission_Course, Admission_Batch";
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['AdmissionTxnid'];
			    $course = $row['Admission_Course'];
			    $baseAmount = (($course == '1') ? 2850 : (($course == '4') ? 2700 : (($course == '5') ? 3000 : 0)));
				 $admissionAmount = $payTxnid * $baseAmount;
				if ($admissionAmount != $amount) {
					$refund = true;
					//die('c3');
				}
			} else {
				$refund = true;
			//die('c1');
			}
		} else {
			$refund = true;
			//die('c2');
			
		}

		return $refund;
	}

	function processToRefundAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		setForAdmissionPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processAdmissionTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		updateAdmissionPaymentStatus(0, $transactionId, $centerCode, $course, $batch);
	}

	function setForAdmissionPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT INTO tbl_payment_refund (
			Payment_Refund_Id,
			Payment_Refund_Txnid,
			Payment_Refund_Amount,
			Payment_Refund_Status,
			Payment_Refund_ProdInfo,
			Payment_Refund_ITGK,
			Payment_Refund_RKCL_Txid,
			Payment_Refund_Course,
			Payment_Refund_Batch
		) 
		SELECT CASE WHEN MAX(Payment_Refund_Id) Is NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
		'" . $transactionId . "' AS Payment_Refund_Txnid,
		'" . $amount . "' AS Payment_Refund_Amount,
		'" . $status . "' AS Payment_Refund_Status,
		'" . $productInfo . "' AS Payment_Refund_ProdInfo,
		'" . $centerCode . "' AS Payment_Refund_ITGK,
		'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid,
		'" . $course . "' AS Payment_Refund_Course,
		'" . $batch . "' AS Payment_Refund_Batch 
		FROM tbl_payment_refund";

		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "' AND Payment_Refund_Course = '" . $course . "' AND Payment_Refund_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

/**
*	Functions of Learner Fee Transactions End
*/

/**
*	Functions for Correction Fee Transactions Start
*/
	function setSuccessForCorrectionFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForCorrectionFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		} else {
			processToRefundCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		}
	}

	function processCorrectionTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if(! mysqli_num_rows($response[2])) {
			insertCorrectionTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updateCorrectionTransactionStatus($status, $amount, $transactionId, $centerCode, $key, $posted_hash);
		}
	}

	function processToConfirmCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		updateCorrectionPaymentStatus(1, $transactionId, $centerCode);
		processCorrectionTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createCorrectionTransactionInvoices($transactionId, $amount, $centerCode);
	}

	function createCorrectionTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT cid AS ref_code FROM tbl_correction_copy WHERE Correction_ITGK_Code = '" . $centerCode ."' AND Correction_TranRefNo = '" . $transactionId . "'  AND Correction_Payment_Status = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		insertInvoice($_Responses, $amount, 'tbl_correction_invoice');
	}

	function updateCorrectionPaymentStatus($paymentstatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "UPDATE tbl_correction_copy SET Correction_Payment_Status = '" . $paymentstatus . "' WHERE Correction_ITGK_Code = '" . $centerCode . "'  AND Correction_TranRefNo = '" . $transactionId . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function insertCorrectionTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Status = '" . $transactionStatus . "' AND Correction_Transaction_Amount = '" . $amount . "' AND Correction_Transaction_Txtid = '" . $transactionId . "' AND  Correction_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_correction_transaction (
				Correction_Transaction_Code,
				Correction_Transaction_Status,
				Correction_Transaction_Fname,
				Correction_Transaction_Amount,
				Correction_Transaction_Txtid,
				Correction_Transaction_ProdInfo,
				Correction_Transaction_CenterCode,
				Correction_Transaction_RKCL_Txid,
				Correction_Transaction_Hash,
				Correction_Transaction_Key,
				Correction_Transaction_Reconcile_Status,
				Correction_Transaction_DateTime,
				Correction_Transaction_Month,
				Correction_Transaction_Year
			) 
			SELECT CASE WHEN MAX(Correction_Transaction_Code) Is NULL THEN 1 ELSE MAX(Correction_Transaction_Code) + 1 END AS Correction_Transaction_Code,
			'" . $transactionStatus . "' AS Correction_Transaction_Status,
			'" . $firstName . "' AS Correction_Transaction_Fname,
			'" . $amount . "' AS Correction_Transaction_Amount,
			'" . $transactionId . "' AS Correction_Transaction_Txtid,
			'" . $productInfo . "' AS Correction_Transaction_ProdInfo,
			'" . $centerCode . "' AS Correction_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Correction_Transaction_RKCL_Txid,
			'" . $posted_hash . "' AS Correction_Transaction_Hash,
			'" . $key . "' AS Correction_Transaction_Key,
			'Reconciled' AS Correction_Transaction_Reconcile_Status,
			'" . date("Y-m-d H:i:s") . "' AS Correction_Transaction_DateTime,
			" . date("m") . " AS Correction_Transaction_Month,
			" . date("Y") . " AS Correction_Transaction_Year 
			FROM tbl_correction_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateCorrectionTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "
			UPDATE tbl_correction_transaction SET 
				Correction_Transaction_Status = '" . $transactionStatus . "',
				Correction_Transaction_Amount='" . $amount . "'
				Correction_Transaction_Hash = '" . $posted_hash . "',
				Correction_Transaction_Key ='" . $key . "',
				Correction_Transaction_Reconcile_Status ='Reconciled'
			WHERE 
				Correction_Transaction_Txtid = '" . $transactionId . "' AND 
				Correction_Transaction_CenterCode='" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function checkIfRefundRequiredForCorrectionFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(Correction_TranRefNo) AS CorrectionTxnid FROM tbl_correction_copy WHERE Correction_TranRefNo = '" . $transactionId . "' AND Correction_ITGK_Code = '" . $centerCode . "' GROUP BY Correction_TranRefNo";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['CorrectionTxnid'];
				$correctionAmount = $payTxnid * 200;
				if ($correctionAmount != $amount) {
					$refund = true;
				}
			} else {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processCorrectionTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updateCorrectionPaymentStatus(0, $transactionId, $centerCode);
	}

/**
*	Functions of Correction Fee Transactions End
*/


/**
*	Functions of NCR Fee Transactions Start
*/
	function setSuccessForNcrFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForNCRFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmNCRTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		} else {
			processToRefundNCRTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		}
	}


	function checkIfRefundRequiredForNCRFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;

		$query = "SELECT SUM(pt.Pay_Tran_Amount) AS Pay_Tran_Amount FROM tbl_org_master om INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = om.Org_TranRefNo WHERE Org_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' AND pt.Pay_Tran_Status NOT LIKE('%Receive%') AND om.Org_PayStatus = 0 GROUP BY om.Org_TranRefNo";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2]) == 1) {
			$ncrFee = mysqli_fetch_array($response[2]);
			if ($ncrFee['Pay_Tran_Amount'] != $amount) {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundNCRTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		updateOrgMasterPaymentStatus(0, $transactionId);
		processNCRTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
	}

	function updateOrgMasterPaymentStatus($status, $transactionId) {
		$_ObjConnection = dbConnection();
		$_UpdateQuery = "UPDATE tbl_org_master SET Org_PayStatus = '" . $status . "' WHERE Org_TranRefNo = '" . $transactionId . "'";
        $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
	}

	function processNCRTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_ncr_transaction WHERE Ncr_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertNCRTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updateNCRTransactionStatus($status, $amount, $transactionId, $centerCode, $key, $posted_hash);
		}
	}

	function insertNCRTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_ncr_transaction WHERE Ncr_Transaction_Status = '" . $transactionStatus . "' AND Ncr_Transaction_Amount = '" . $amount . "' AND Ncr_Transaction_Txtid = '" . $transactionId . "' AND  Ncr_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_ncr_transaction (
				Ncr_Transaction_Code,
				Ncr_Transaction_Status,
				Ncr_Transaction_Fname,
				Ncr_Transaction_Amount,
				Ncr_Transaction_Txtid,
				Ncr_Transaction_ProdInfo,
				Ncr_Transaction_CenterCode,
				Ncr_Transaction_RKCL_Txid,
				Ncr_Transaction_Hash,
				Ncr_Transaction_Key,
				Ncr_Transaction_DateTime
			) 
			SELECT CASE WHEN MAX(Ncr_Transaction_Code) IS NULL THEN 1 ELSE MAX(Ncr_Transaction_Code) + 1 END AS Ncr_Transaction_Code,
			'" . $transactionStatus . "' AS Ncr_Transaction_Status,
			'" . $firstName . "' AS Ncr_Transaction_Fname,
			'" . $amount . "' AS Ncr_Transaction_Amount,
			'" . $transactionId . "' AS Ncr_Transaction_Txtid,
			'" . $productInfo . "' AS Ncr_Transaction_ProdInfo,
			'" . $centerCode . "' AS Ncr_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Ncr_Transaction_RKCL_Txid,
			'" . $posted_hash . "' AS Ncr_Transaction_Hash,
			'" . $key . "' AS Ncr_Transaction_Key,
			'" . date("Y-m-d H:i:s") . "' AS Ncr_Transaction_DateTime 
			FROM tbl_ncr_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateNCRTransactionStatus($transactionStatus, $reExamTransactionAmount, $transactionId, $centerCode, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "
			UPDATE tbl_ncr_transaction SET 
				Ncr_Transaction_Status = '" . $transactionStatus . "', Ncr_Transaction_Amount = '" . $reExamTransactionAmount . "', Ncr_Transaction_Hash = '" . $posted_hash . "', Ncr_Transaction_Key = '" . $key . "'
			WHERE 
				Ncr_Transaction_Txtid = '" . $transactionId . "' AND Ncr_Transaction_CenterCode = '" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function processToConfirmNCRTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		updateOrgMasterPaymentStatus(1, $transactionId);
		processNCRTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createNCRTransactionInvoices($transactionId, $amount, $centerCode);
		updateUserMasterRole($centerCode);
	}

	function createNCRTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Org_Ack AS ref_code FROM tbl_org_master WHERE  Org_TranRefNo = '" . $transactionId . "'  AND Org_PayStatus = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$orgRow = mysqli_fetch_array($_Responses[2]);
			$addTimestamp = date("Y-m-d H:i:s");
			$_Insert = "INSERT IGNORE INTO tbl_ncr_invoice (invoice_no, Ao_Itgkcode, Ao_Ack_No, amount, addtime) SELECT (MAX(invoice_no) + 1) AS invoice_number, '" . $centerCode . "' AS Ao_Itgkcode, '" . $orgRow['ref_code'] . "' AS Org_Ack, '" . $amount . "' AS Amount, '" . $addTimestamp . "' AS addtime FROM tbl_ncr_invoice";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		}
	}

	function updateUserMasterRole($centerCode) {
		$_ObjConnection = dbConnection();
		echo $updateQuery = "
			UPDATE tbl_user_master SET 
				User_UserRoll = '15'
			WHERE 
				User_LoginId = '" . $centerCode . "' AND User_UserRoll = '22'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

/**
*	Functions of NCR Fee Transactions End
*/

/**
*	Functions of Name Address Change Fee Transactions Start
*/
	function setSuccessForNameAddressChangeFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForNACFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmNACTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		} else {
			processToRefundNACTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		}
	}


	function checkIfRefundRequiredForNACFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;

		$query = "SELECT SUM(pt.Pay_Tran_Amount) AS Pay_Tran_Amount FROM tbl_address_name_transaction ant INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = ant.fld_transactionID WHERE ant.fld_transactionID = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' AND ant.fld_paymentStatus = '0' GROUP BY ant.fld_transactionID";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2]) == 1) {
			$ncrFee = mysqli_fetch_array($response[2]);
			if ($ncrFee['Pay_Tran_Amount'] != $amount) {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundNACTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		updateNACTransactionStatus('Refund', $transactionId, $centerCode, $amount);
	}

	function updateNACTransactionStatus($transactionStatus, $transactionId, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$paystatus = ($transactionStatus == 'Success') ? 1 : 0;
		$updateQuery = "
			UPDATE tbl_address_name_transaction SET 
				fld_Transaction_Status = '" . $transactionStatus . "', 
				fld_amount = '" . $amount . "', 
				fld_paymentStatus = '" . $paystatus . "'
			WHERE 
				fld_transactionID = '" . $transactionId . "' AND 
				fld_ITGK_Code = '" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function processToConfirmNACTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		updateNACTransactionStatus('Success', $transactionId, $centerCode, $amount);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createNACTransactionInvoices($transactionId, $amount, $centerCode);
		updateChangeAddressITGK(5, $transactionId, $centerCode);
	}

	function createNACTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT ant.fld_ref_no AS ref_code, ant.fld_paymentTitle, um.User_Code FROM tbl_address_name_transaction ant INNER JOIN tbl_user_master um ON um.User_LoginId = ant.fld_ITGK_Code WHERE ant.fld_transactionID = '" . $transactionId . "'  AND ant.fld_paymentStatus = '1' AND ant.fld_ITGK_Code = '" . $centerCode . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$nacRow = mysqli_fetch_array($_Responses[2]);
			$addTimestamp = date("Y-m-d H:i:s");
			$gstCode = ($nacRow['fld_paymentTitle'] == 'name') ? 15 : 16;
			$_Insert = "INSERT INTO tbl_nlc_itgk_gstinvoice (fld_invoice, fld_ITGK_Code, fld_ITGK_UserCode, fld_referenceID, fld_gst_invoice_masterID, fld_addedOn) SELECT (CONCAT('NLC/Online/', MAX(fld_ID)) AS invoice_number, '" . $centerCode . "' AS Ao_Itgkcode, '" . $nacRow['User_Code'] . "' AS User_Code, '" . $nacRow['ref_code'] . "' AS ref_code, '" . $gstCode . "' AS gstCode, '" . $addTimestamp . "' AS addtime FROM tbl_ncr_invoice";
			$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);

			$_Insert = "INSERT INTO tbl_nameaddress_invoice (invoice_no, invoice_ref_id, amount,addtime) SELECT CASE WHEN MAX(invoice_no) IS NULL THEN 1000 ELSE MAX(invoice_no) + 1 END AS invoice_no, '" . $nacRow['ref_code'] . "' AS invoice_ref_id, '" . $amount . "' as amount, '" . $addTimestamp . "' as addtime FROM tbl_nameaddress_invoice";
			$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		}
	}

	function updateChangeAddressITGK($payStatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$updateQuery = "
			UPDATE tbl_change_address_itgk ca INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_AdmissionArray = ca.fld_ref_no SET 
				ca.fld_status = '" . $payStatus . "'
			WHERE 
				pt.Pay_Tran_PG_Trnid = '" . $transactionId . "' AND ca.fld_ITGK_Code = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

/**
*	Functions of Name Address Change Fee Transactions End
*/

/**
*	Functions of EOI Fee Transactions Start
*/
	function setSuccessForEOIFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForEOIFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmEOITransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		} else {
			processToRefundEOITransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		}
	}


	function checkIfRefundRequiredForEOIFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;

		$query = "SELECT SUM(pt.Pay_Tran_Amount) AS Pay_Tran_Amount FROM tbl_courseitgk_mapping cm INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = cm.Courseitgk_TranRefNo WHERE cm.Courseitgk_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' AND cm.EOI_Fee_Confirm = '0' GROUP BY cm.Courseitgk_TranRefNo";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2]) == 1) {
			$ncrFee = mysqli_fetch_array($response[2]);
			if ($ncrFee['Pay_Tran_Amount'] != $amount) {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundEOITransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processEOITransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		updateCourseITGKMapping(0, $transactionId, $centerCode);
	}

	function processEOITransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_eoi_transaction WHERE EOI_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertEOITransaction($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updateEOITransactionStatus($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		}
	}

	function insertEOITransaction($transactionStatus, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_eoi_transaction WHERE EOI_Transaction_Status = '" . $transactionStatus . "' AND EOI_Transaction_Amount = '" . $amount . "' AND EOI_Transaction_Txtid = '" . $transactionId . "' AND  EOI_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_eoi_transaction (
				EOI_Transaction_Code,
				EOI_Transaction_Status,
				EOI_Transaction_Fname,
				EOI_Transaction_Amount,
				EOI_Transaction_Txtid,
				EOI_Transaction_ProdInfo,
				EOI_Transaction_CenterCode,
				EOI_Transaction_RKCL_Txid,
				EOI_Transaction_Hash,
				EOI_Transaction_Key,
				EOI_Payment_Mode,
				EOI_Transaction_Email,
				EOI_Transaction_timestamp
			) 
			SELECT CASE WHEN MAX(EOI_Transaction_Code) IS NULL THEN 1 ELSE MAX(EOI_Transaction_Code) + 1 END AS EOI_Transaction_Code,
			'" . $transactionStatus . "' AS EOI_Transaction_Status,
			'" . $firstName . "' AS EOI_Transaction_Fname,
			'" . $amount . "' AS EOI_Transaction_Amount,
			'" . $transactionId . "' AS EOI_Transaction_Txtid,
			'" . $productInfo . "' AS EOI_Transaction_ProdInfo,
			'" . $centerCode . "' AS EOI_Transaction_CenterCode,
			'" . $transactionTxId . "' AS EOI_Transaction_RKCL_Txid,
			'" . $posted_hash . "' AS EOI_Transaction_Hash,
			'" . $key . "' AS EOI_Transaction_Key,
			'online' AS EOI_Payment_Mode,
			'" . $email . "' AS EOI_Transaction_Email,
			'" . date("Y-m-d H:i:s") . "' AS EOI_Transaction_timestamp 
			FROM tbl_eoi_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateEOITransactionStatus($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$updateQuery = "
			UPDATE tbl_eoi_transaction SET 
				EOI_Transaction_Status = '" . $status . "',
				EOI_Transaction_Fname = '" . $firstName . "',
				EOI_Transaction_Amount = '" . $amount . "',
				EOI_Transaction_ProdInfo = '" . $productInfo . "',
				EOI_Transaction_RKCL_Txid = '" . $transactionTxId . "',
				EOI_Transaction_Hash = '" . $posted_hash . "',
				EOI_Transaction_Key = '" . $key . "',
				EOI_Payment_Mode = 'online',
				EOI_Transaction_Email = '" . $email . "',
				EOI_Transaction_timestamp = '" . date("Y-m-d H:i:s") . "'
			WHERE 
				EOI_Transaction_Txtid = '" . $transactionId . "' AND 
				EOI_Transaction_CenterCode = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

	function processToConfirmEOITransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		processEOITransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		updateCourseITGKMapping(1, $transactionId, $centerCode);
		createEOITransactionInvoices($transactionId, $amount, $centerCode);
	}

	function createEOITransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT cm.Courseitgk_EOI FROM tbl_courseitgk_mapping cm INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = cm.Courseitgk_TranRefNo WHERE cm.Courseitgk_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' AND cm.EOI_Fee_Confirm = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$eoiRow = mysqli_fetch_array($_Responses[2]);
			$eoiTimestamp = date("Y-m-d H:i:s");
			$_Insert = "INSERT INTO tbl_eoi_invoice (invoice_no, Itgkcode, eoi_code, amount, addtime) SELECT CASE WHEN MAX(invoice_no) IS NULL THEN 1000 ELSE MAX(invoice_no) + 1 END AS invoice_no, '" . $centerCode . "' AS Itgkcode, '" . $eoiRow['Courseitgk_EOI'] . "' AS eoi_code, '" . $amount . "' AS amount, '" . $eoiTimestamp . "' AS addtime FROM tbl_eoi_invoice";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		}
	}

	function updateCourseITGKMapping($payStatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$setFields = "EOI_Fee_Confirm = '" . $payStatus . "'";
		if ($payStatus) {
			$year = (date('m') > 3) ? date('Y') + 1 : date('Y');
			$expire_date = $year . "-03-31";
			$setFields .= ", CourseITGK_ExpireDate = IF (Courseitgk_Course LIKE ('%cfa%'), '" . $expire_date . "', CourseITGK_ExpireDate)";
		}
		
		$updateQuery = "
			UPDATE tbl_courseitgk_mapping SET 
				$setFields
			WHERE 
				Courseitgk_TranRefNo = '" . $transactionId . "' AND 
				Courseitgk_ITGK = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

/**
*	Functions of EOI Fee Transactions End
*/

?>