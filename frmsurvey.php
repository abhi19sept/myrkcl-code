<?php
$title = "Survey Master ";
include ('header.php');
include ('root_menu.php');


if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '14') 
{
    ?>


    <div style="min-height:450px !important;min-height:500px !important">

        <div class="container" > 


            <div class="panel panel-primary" style="margin-top:46px !important;" >

                <div class="panel-heading" id="non-printable">Survey Application</div>
                <div class="panel-body">


                    <!-- <div class="jumbotron"> -->
                    <form name="frmsurvey" id="frmsurvey"  class="form-inline">     




                        <div id="response"></div>	

                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group"> 
                            <label for="designation">Select Survey:<span class="star">*</span></label>
                            <select id="ddlSurvey" name="ddlSurvey" class="form-control">                                  
                            </select>
                        </div> 



                        <div id='gird'></div>




                        <div class="container" > 

                            <div class="col-md-11" id="inst" style="display:none"> 	

                                <label for="learnercode"  ><b>कृपया ध्यान दे कि यह सर्वे सिर्फ नेटवर्ककी राय लेने के लिए किया जा रहा है, ये आवश्यक नहीं है कि इस सर्वे के परिणाम के आधार पर RKCL RS-CIT के फी मोड का निर्धारण करें| RKCL का निर्णय अंतिम व मान्य होगा | </b></label></br>                              

                            </div>


                        </div>	



                        <div class="container" >

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>





                    </form> 

                </div>

            </div>   
        </div>

    </div>		
    </body>	


    <?php include ('footer.php'); ?>				
    <?php include'common/message.php'; ?>


    <style>
        #errorBox{
            color:#F00;
        }
    </style>



    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {





            function showData() {

                $.ajax({
                    type: "post",
                    url: "common/cfsurvey.php",
                    data: "action=SHOWSURVEY",
                    success: function (data) {

                        $("#gird").html(data);

                    }
                });
            }
            showData();




            function FillSurvey() {
                $.ajax({
                    type: "post",
                    url: "common/cfsurvey.php",
                    data: "action=FILLSurvey",
                    success: function (data) {
                        $("#ddlSurvey").html(data);
                    }
                });
            }

            FillSurvey();


            $("#ddlSurvey").change(function () {
                var Event = $(this).val();
				//alert(Event);
				if(Event=='')
				{
					$('#inst').hide();
				}
			    else
				{
					$('#inst').show();
				}
                //alert(selregion);
                $.ajax({
                    url: 'common/cfsurvey.php',
                    type: "post",
                    data: "action=GETSURVEY&values=" + Event + "",
                    success: function (data) {
                        //alert(data);
						 
                        $('#gird').html(data);

                    }
                });
            });



            $("#btnSubmit").click(function () {

                // debugger;
                if ($("#frmsurvey").valid())
                {
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    var url = "common/cfsurvey.php"; // the script where you handle the form input.
                    var data;
                    var forminput = $("#frmsurvey").serialize();
                    if (Mode == 'Add')
                    {
                        data = "action=INSERT&" + forminput; // serializes the form's elements.
                    } else
                    {

                        data = "action=UPDATE&code=" + Examcode + "&" + forminput;
                        //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                    }
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data)
                        {
							if (data==1)
                            {

                                $('#response').empty();
                                $('#response').append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span>Successfully Inserted</span></p>");
                                window.setTimeout(function () {
                                    window.location.href = "frmsurvey.php";
                                }, 3000);
                                Mode = "Add";
                                //resetForm("frmsurvey");
                            } 
							else 
                            {
                                $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                                
                            }
                            //showData();


                        }
                    });
                }
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>


    <style>
        .error {
            color: #D95C5C!important;
        }
    </style>
    <script src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmsurvey_validation.js"></script>
    </html>
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>

