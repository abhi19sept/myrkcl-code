<?php
    session_start();
    $title = "Bio-Matric";
    include('header.php');
    //include('root_menu.php');
    if (isset($_REQUEST['code'])) {
        echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
      
    } else {
        echo "<script>var UserCode=0</script>";
       
    }

?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Apply For Deregister Biometric Enrollment
            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmLearnerCapture" id="frmLearnerCapture" class="form-inline" role="form"
                      enctype="multipart/form-data">

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>
                        <div id="errorBox"></div>
                    </div>

                    <div id="showData">
                        <div class="container">
                            <div class="col-md-1">
                                <img id="imgFinger" name="imgFinger" class="form-control"
                                     style="width:80px;height:107px;"
                                     alt=""/>
                            </div>
                            <div id="grid" name="grid"></div>

                        </div>

                        <div class="container" name="divderegreason" id="divderegreason" style="margin-top:20px; margin-left:15px;">   
                         <div class="col-md-8">     
                                <label for="biopin">Deregister Reason:<span class="star">*</span></label>

                                <textarea class="form-control" maxlength="500" name="txtderegreason" id="txtderegreason" placeholder="Reason" required="required" oninvalid="setCustomValidity('Please Enter Reason')"
                                    onchange="try {
                                        setCustomValidity('')
                                    } catch (e) {
                                    }"></textarea>

                            </div>
                        <div class="col-md-3 ">     
                            <input type="button" name="btnDeregister" id="btnDeregister" class="btn btn-primary"  value="Deregister Request" />     
                        </div>               
                    </div>

                    </div>

            </div>
        </div>
    </div>
    </form>
</div>
<?php
    //include('footer.php');
    include 'common/message.php';
    include 'common/modals.php';
    $mac = explode(" ", exec('getmac'));
?>
</body>

<script language="javascript" type="text/javascript">

    window.myCallback = function (data) {
        console.log(data);
    };


    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";


    //FillMachines();

    $(document).ready(function () {

        var url = "common/cfbiomatricenroll.php";

        $.ajax({
            type: "POST",
            url: url,
            data: "action=GetLearner&values=" + AdmissionCode + "",
            success: function (data) {
                $("#grid").html(data);
            }
        });

        var url = "common/cfbiomatricenroll.php";

        $.ajax({
            type: "POST",
            url: url,
            data: "action=GetLearnerCourse&values=" + AdmissionCode + "",
            success: function (data) {
                //alert(data);
                txtLCourse.value = data;
            }
        });

        var url = "common/cfbiomatricenroll.php";

        $.ajax({
            type: "POST",
            url: url,
            data: "action=GetLearnerBatch&values=" + AdmissionCode + "",
            success: function (data) {
                //alert(data);
                txtLBatch.value = data;
            }
        });

 

        $("#btnDeregister").click(function () {
            //alert("hhkj");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBioMetricDeregistration.php"; // the script where you handle the form input.
                var data;


                 data = "action=LearnerDeregister&lcode=" + AdmissionCode + "&deregreason=" + txtderegreason.value + "";
                //alert(data);
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>Your Application for Deregister Learner Biometric Successfully Registered.</span></div>");
                            window.setTimeout(function () {
                              //  Mode = "Add";
                                window.close();
                            }, 1000);
                        }
                        else if(data == '0'){
                             $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Enter Reason.</span></div>");

                        }
                        else
                        {
                            //var data2 = "Already Exist";
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                        }


                    }
                });

                return false; // avoid to execute the actual submit of the form.
        });
    });

   
</script>
</html>