<?php
$title = "E-Certificate";
include ('header.php');
include ('root_menu.php');

echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
if($_SESSION['User_UserRoll']=='1' && $_SESSION['User_Code']=='1') {
?>
<style>
 .modal {z-index: 1050!important;} 
 #turmcandition{
       background-color: whitesmoke;
    text-align: justify;
    padding: 10px;
 }
 .noturm{
         margin-left: 10px !important;
 }
 .yesturm{
     margin-right: 15px;
 }
 </style>
<div style="min-height:430px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading"> Download E-Certificate </div>
            <div class="panel-body">

                <form name="frmEcertificate" id="frmEcertificate" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Event Name:<span class="star">*</span></label>
                                <select id="ddlEvent" name="ddlEvent" class="form-control">

                                </select>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Enter Learner Code:<span class="star">*</span></label>
                                <input type='text' id="ddllot" name="ddllot" class="form-control" />
                            </div> 

                        </div>

                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmitlist" id="btnSubmitlist" class="btn btn-primary" value="View List"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>
                        <div id="result" style="margin-top:5px; width:94%;"> </div>
                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
$(document).ready(function () {
    
        function FillEvent() {
            $.ajax({
                type: "post",
                url: "common/cfEcertificate.php",
                data: "action=FILLEventName",
                success: function (data) {                    
                    $("#ddlEvent").html(data);
                }
            });
        }
        FillEvent();
        
        $("#btnSubmitlist").click(function () {
            if ($("#frmEcertificate").valid())
            {
               ShowLearnerEcertificate();
            }
            return false; 
        });
        
        function ShowLearnerEcertificate() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfEcertificate.php";      
            var data;
            data = "action=GETDATAEcertificateRajevolt&event=" + ddlEvent.value + "&lot=" + ddllot.value + ""; //            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable();
                }
            });
        }
        
        $("#grid").on("click",".downloadEcertificate",function(){
            var docId = $(this).attr('id');            
            var aadhar = $(this).attr('name');      
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfEcertificate.php",
                data: "action=DownloadEcertificate&aadhar=" + aadhar + "&docId=" + docId + "",
                success: function (data) {
                    $('#response').empty();
                    $('#result').html(data);
                }
            });
        });
});

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>