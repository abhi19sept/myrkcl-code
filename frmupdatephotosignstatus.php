<?php
$title="Modify Admission";
include ('header.php'); 
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
echo "<script>var CenterCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var CourseCode=" . $_REQUEST['CourseCode'] . "</script>";
echo "<script>var BatchCode=" . $_REQUEST['BatchCode'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
//echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
echo "<script>var CenterCode=0</script>";
echo "<script>var CourseCode=0</script>";
echo "<script>var BatchCode=0</script>";
echo "<script>var Mode='Update'</script>";
}
if (isset($_REQUEST['Mode'])) {
	$_Mode = $_REQUEST['Mode'];
}
else{
	$_Mode = '0';
}

//print_r($_SESSION);
?>
<div style="min-height:430px !important;max-height:1500px !important">
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Photo Sign Process</div>
                <div class="panel-body">
                    <form name="frmprocess" id="frmprocess" class="form-inline" role="form" enctype="multipart/form-data">
						<div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>				
								
							</div>
							<div id="menuList" name="menuList" style="margin-top:15px;"> </div> 
							
						<?php if($_Mode == 'Pending' || $_Mode == 'Reprocess')	{?>						
								<div class="container">
								    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/> 
								</div>
						<?php } ?>
                    </div>
            </div>   
        </div>
		
    </form>
	</div>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
                        
        <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {			
			
			
			function FillLearnerDetail() {
				//alert(CenterCode);
                $.ajax({
                    type: "post",
                    url: "common/cfphotosign.php",
                    data: "action=FillLearnerDetail&values=" + CenterCode + "&course=" + CourseCode + "&batch=" + BatchCode + "&mode=" + Mode + "",
                    success: function (data) {
						//alert(data);
                        $("#menuList").html(data);
                    }
                });
            }
            FillLearnerDetail();		
			

            $("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfphotosign.php"; // the script where you handle the form input.
                var data;
                var forminput=$("#frmprocess").serialize();
                //alert(forminput);		
				
                if (Mode == 'Add')
                {
                    data = "action=ADD&" + forminput; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&" + forminput; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href="frmphotosignprocess.php";
                            }, 1000);

                            Mode="Add";
                            resetForm("frmprocess");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
    </body>
    
</html>