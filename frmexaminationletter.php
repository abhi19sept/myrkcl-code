<?php  ob_start(); 
$title="Examination Letter ";
include ('header.php'); 
include ('root_menu.php');  
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
 if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            
/* Added by Sunil: 7-2-2017
        * This condition is checking that this is coming form learner or not*/
        if(isset($_SESSION['LearnerLogin'])){
           header('Location: frmexaminationletternew.php');
           exit;
        }
        /* This condition is checking that this is coming form learner or not*/
            ?>
			
			<style type="text/css">

</style>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:450px;max-height:auto">
        <div class="container" > 
			

            <div class="panel panel-primary" style="margin-top:46px !important;" >

                <div class="panel-heading" id="non-printable">Permission Letter</div>
                <div class="panel-body">
				
			 
                    <!-- <div class="jumbotron"> -->
                    <form name="frmexameventmaster" id="frmexameventmaster" action="" class="form-inline">     

                        <div class="container">
                            <div class="container">
							
							
							
							<div id="errorBox"></div>
							<div class="col-sm-4 form-group"  id="non-printable"> 
                                <label for="edistrict">Event Name 
								:</label>
                                <select id="ddlEvent" name="code" class="form-control" >
								 
                                </select>    
                            </div>

                            <div class="col-sm-4 form-group"  id="non-printable"> 
                                <label for="edistrict">Generate By
								:</label>
                                <select id="generateBy" name="generateBy" class="form-control" >
								 	<option value="">Select</option>
								 	<option value="district">District</option>
								 	<option value="itgk">ITGKs / Centers</option>
								 	<option value="examcenter">Exam Centers</option>
								 	<option value="learners">Learner / ITGK Codes</option>
                                    <option value="job">Execute Job</option>
                                    <option value="byupdatepics">Update Photo / Sign and Generate</option>
                                    <option value="miss">Get Missing Photo / Sign</option>
                                    <option value="copy">Copy Photo / Sign to VMOU</option>
                                </select>
                            </div>

							
							<div class="col-sm-4 form-group asHidden district"  id="non-printable"> 
						        <label for="edistrict">District Name 
								:</label>
						        <select id="ddlDistrict" name="ddlDistrict" class="form-control" >
								 
						        </select>    
						    </div>

							<div class="col-sm-4 form-group asHidden itgk"  id="non-printable"> 
                                <label for="edistrict">ITGK Code 
								:</label>
                                <select id="ddlCenter" name="ddlCenter[]" class="form-control"  multiple="multiple" >
								 
                                </select>    
                            </div>
							
							<div class="col-sm-4 form-group asHidden learners"  id="non-printable" style="float:left;"> 
                                <label for="edistrict">Learner / ITGK Code(s)
								:</label>
                                <input id="learnerCode" name="learnerCode" class="form-control" type="text" />
                            </div>

                            <div class="col-sm-4 form-group asHidden job"  id="non-printable" style="float:left;"> 
                                <label for="edistrict">Execute Job
                                :</label>
                                <select id="job" name="job" class="form-control" >
                                <?php
                                    echo "<option value=''>Select Job</option>";
                                    for ($i = 1; $i<11; $i++) {
                                        echo "<option value=" . $i . ">" . $i . "</option>";
                                    }
                                ?>
                                </select>
                            </div>
                            

							<?php 
								include("frmDistrictWiseExamCenters.php");
							?>
							</div>
							
							    

                            </div> 

                        <div class="container" id="non-printable">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="show" style="margin-left:30px"/>    
                        </div>

                        <div class="container small" id="non-printable">
                            <p>&nbsp;</p>
                            <p>Before start to generate permission letters for a new exam event, you have to ensure that:</p>
                            <ul>
                                <li>`permissionletter` directory is created at location 'upload/permissionletter'</li>
                                <li>If Directory already exists, then it should be empty before start as new.</li>
                            </ul>
                        </div>
						
						
					<div id="response"></div>	
					 </form> 
				 
				</div>
					
            </div>   
        </div>
  
		</div>				
	</body>					
<?php include ('footer.php'); ?>				
<?php include'common/message.php';?>

<script>

</script>
<style>
#errorBox{
 color:#F00;
 }
</style>
<script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
                        	$(".asHidden").hide();
							if (Mode == 'Delete')
							{
								if (confirm("Do You Want To Delete This Item ?"))
								{
		                   		 deleteRecord();
		               		}
		           		 }
		           else if (Mode == 'Edit')
		           {
		                fillForm();
		           }
				   
				   
			   
				   function FillEvent() {
                                $.ajax({
                                type: "post",
                                url: "common/cflearnerdetails.php",
                                data: "action=FILLPUBLISHEVENT",
                                success: function (data) {
                                    $("#ddlEvent").html(data);
                                    getJobs();
                                }
                            });
                            }
                             FillEvent(); 

							$("#ddlDistrict").change(function(){
								var selregion = $(this).val();
								//alert(selregion);
								showItgk(selregion);
                            });
			
                          $("#btnSubmit").click(function () {
								//alert(1);
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                               var url = "common/cfexaminationeventmaster.php"; // the script where you handle the form input.
								var data;
								var forminput=$("#frmexameventmaster").serialize();
								
									data = "action=PDF&" +forminput; // serializes the form's elements.
								//alert(data);
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span></span><span>" + data + "</span></p>");
                                    }
                                });
							
                                return false; // avoid to execute the actual submit of the form.
                            });
							
							$("#generateBy").change(function(){
								showfields(this.value);
							});

                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                        function showfields(generateBy) {
                        	$(".downloadbutton").remove();
                        	$(".asHidden").hide();
                        	$("." + generateBy).show();
                        	FillDistrict();
                        	showItgk('');
                        	getExamCenters('');
	                      	if (generateBy == 'itgk' || generateBy == 'examcenter') {
	                      		$(".district").show();
	                      	} else if (generateBy == 'copy' || generateBy == 'miss') {
                                $(".job").show();
                            }

                            $('#job').prop('selectedIndex',0);
                        }

                        function showItgk(selregion) {
                        	$(".itgk").append("<p class='error processitgk'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                        	$.ajax({
					          url: 'common/cfexaminationeventmaster.php',
					          type: "post",
					          data: "action=FillByDistrict&values=" + selregion + "",
					          success: function(data){
								//alert(data);
								$('#ddlCenter').html(data);
								$(".processitgk").remove();
					          }
					        });
                        }

                    </script>

<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>