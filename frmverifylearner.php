<?php
$title = "Bio-Matric";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code']) && !empty($_REQUEST['code'])) {
    echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var CourseCode=" . $_REQUEST['Course'] . "</script>";
    echo "<script>var BatchCode='" . $_REQUEST['Batch'] . "'</script>";
} else {
    echo "<script>var AdmissionCode=0</script>";
    echo "<script>var Mode='In'</script>";
    echo "<script>var CourseCode='0'</script>";
    echo "<script>var BatchCode='0'</script>";
}
?>		

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">First time capture may take time, so wait after click the button "Click To Capture"</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmCourseBatch" id="frmCourseBatch" class="form-inline" role="form" method="post">     

                    <div class="container">
                        <div class="container" id="SelectedDetails" style="display:none;">
                            <div id="response"></div>
                            <div class="col-md-4 form-group">     
                                <img id="imgFinger" name="imgFinger"  class="form-control" style="width:100px;height:100px;" alt="" />								 
                            </div>

<!--                            <div class="col-md-4 form-group">     
                                <label for="batch"> Selected Course:</label>
                                <input type="text" readonly="true" name="txtCourse" id="txtCourse" class="form-control"/>
                            </div>

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Selected Batch:</label>
                                <input type="text" readonly="true" name="txtBatch" id="txtBatch" class="form-control"/>
                            </div>	

                            <div class="col-md-4 form-group">     
                                <br> 
                                <a href="frmverifylearner.php" style="text-decoration:none;"/>
                                <input type="button" name="change" id="change" class="btn btn-primary" value="Change Course Batch"/>
                                </a>
                            </div>	-->
                        </div> 

                        <div id="success" style="margin-left:250px !important;"></div>

                        <div id="errorBox"></div>

                    </div>



                    <div style="display:none;">
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <label for="batch"> Serial No:<span class="star">*</span></label>
                                <input type="text" name="tdSerial" id="tdSerial" class="form-control" readonly="true"/>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Make:<span class="star">*</span></label>
                                <input type="text" name="tdMake" id="tdMake" class="form-control" readonly="true"/>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Model:<span class="star">*</span></label>
                                <input type="text" name="tdModel" id="tdModel" class="form-control" readonly="true"/>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Width:<span class="star">*</span></label>
                                <input type="text" name="tdWidth" id="tdWidth" class="form-control" readonly="true"/>
                            </div>                                                        
                        </div>

                        <div class="container">							 
                            <div class="col-md-4 form-group">     
                                <label for="batch"> Height:<span class="star">*</span></label>
                                <input type="text" name="tdHeight" id="tdHeight"  class="form-control" readonly="true"/>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Local MAC:<span class="star">*</span></label>
                                <input type="text" name="tdLocalMac" id="tdLocalMac"  class="form-control" readonly="true"/>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Local IP:<span class="star">*</span></label>
                                <input type="text" name="tdLocalIP" id="tdLocalIP" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Status:<span class="star">*</span></label>
                                <input type="text" name="txtStatus" id="txtStatus" class="form-control" readonly="true"/>
                            </div> 
                        </div>				

                    </div>

                    <div class="container" id="SelectedBefore"> 
                        <div class="col-md-6 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">

                            </select>
                        </div> 

                        <div class="col-md-6 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>
                        </div> 

                    </div>					
                    <div style="display:none;">	
                        <div class="container">							 
                            <div class="col-md-4 form-group">     
                                <label for="batch"> Quality:<span class="star">*</span></label>
                                <input type="text" name="txtQuality" id="txtQuality" value="" class="form-control" readonly="true"/>
                                <input type="hidden" name="admission_code" id="admission_code" value="<?php echo $_REQUEST['code']; ?>">
                                <input type="hidden" name="txtLearnerName" id="txtLearnerName"/>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> NFIQ:<span class="star">*</span></label>
                                <input type="text" name="txtNFIQ" id="txtNFIQ" value="" class="form-control" readonly="true"/>
                            </div> 							  
                        </div>

                        <div class="container">							 
                            <div class="col-md-10 ">     
                                <label for="batch"> Base64Encoded ISO Template:<span class="star">*</span></label>
                                <textarea id="txtIsoTemplate" rows="4" cols="12" name="txtIsoTemplate" class="form-control" readonly="true"></textarea>
                            </div> 
                        </div>

                        <div class="container">							 
                            <div class="col-md-10">     
                                <label for="batch"> Base64Encoded ISO Image:<span class="star">*</span></label>
                                <textarea id="txtIsoImage" rows="4" cols="12" name="txtIsoImage" class="form-control" readonly="true"></textarea>
                            </div> 
                        </div>

                        <div class="container">							 
                            <div class="col-md-10">     
                                <label for="batch"> Base64Encoded Raw Data:<span class="star">*</span></label>
                                <textarea id="txtRawData" rows="4" cols="12" name="txtRawData" class="form-control" readonly="true"></textarea>
                            </div> 
                        </div>

                        <div class="container">							 
                            <div class="col-md-10">     
                                <label for="batch"> Base64Encoded Wsq Image Data:<span class="star">*</span></label>
                                <textarea id="txtWsqData" rows="4" cols="12" name="txtWsqData" class="form-control" readonly="true"></textarea>
                            </div> 
                        </div>
                    </div>	

                    <div id="menuList" name="menuList" style="margin-top:5px;"> </div> 

                    <div class="container" style="margin-top:20px; margin-left:15px; display:none;">						
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/> 					 
                    </div>							

            </div>
        </div>   
    </div>
</form>

</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script src="js/jquery-1.8.2.js"></script>
<script src="js/mfs100.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="bootcss/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        var quality = 60; //(1 to 100) (recommanded minimum 55)
        var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

        function GetInfo() {
            //alert("hii");
            document.getElementById('tdSerial').value = "";
            document.getElementById('tdMake').value = "";
            document.getElementById('tdModel').value = "";
            document.getElementById('tdWidth').value = "";
            document.getElementById('tdHeight').value = "";
            document.getElementById('tdLocalMac').value = "";
            document.getElementById('tdLocalIP').value = "";

            var res = GetMFS100Info();
            if (res.httpStaus) {

                document.getElementById('txtStatus').value = res.data.ErrorDescription;

                if (res.data.ErrorCode == "0") {
                    document.getElementById('tdSerial').value = res.data.DeviceInfo.SerialNo;
                    document.getElementById('tdMake').value = res.data.DeviceInfo.Make;
                    document.getElementById('tdModel').value = res.data.DeviceInfo.Model;
                    document.getElementById('tdWidth').value = res.data.DeviceInfo.Width;
                    document.getElementById('tdHeight').value = res.data.DeviceInfo.Height;
                    document.getElementById('tdLocalMac').value = res.data.DeviceInfo.LocalMac;
                    document.getElementById('tdLocalIP').value = res.data.DeviceInfo.LocalIP;
                }
            }
            else {
                alert(res.err);
            }
            return false;
        }

        GetInfo();

//        if (CourseCode != "0") {
//            //alert(CourseCode);
//            FillSelectedCourse();
//            FillSelectedBatch();
//            showLearnerData(CourseCode, BatchCode);
//            $("#SelectedDetails").show();
//            $("#SelectedBefore").hide();
//        }

        function Match() {
            var quality = 60; //(1 to 100) (recommanded minimum 55)
            var timeout = 10;
            try {
                var isotemplate = document.getElementById('txtIsoTemplate').value;
                var res = MatchFinger(quality, timeout, isotemplate);
                var res1 = CaptureFinger(quality, timeout);

                if (res.httpStaus) {
                    document.getElementById('txtStatus').value = "ErrorCode: " + res1.data.ErrorCode + " ErrorDescription: " + res1.data.ErrorDescription;
                    if (res.data.Status) {
                        document.getElementById('imgFinger').src = "data:image/bmp;base64," + res1.data.BitmapData;
                        //alert("Finger matched");
                        $('#success').empty();
                        $('#success').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "     Attendance Successfully registered for Learner: " + txtLearnerName.value + "</span></font></b></p>");
                        SubmitDetails();
                    }
                    else {
                        if (res.data.ErrorCode != "0") {
                            alert(res.data.ErrorDescription);
                        }
                        else {
                            //alert("Finger not matched");
                            $('#success').empty();
                            $('#success').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "     Fingerprint does not match. Please try again." + "</span></font></b></p>");

                        }
                    }
                }
                else {
                    alert(res.err);
                }
            }
            catch (e) {
                alert(e);
            }
            return false;
        }

//        function FillSelectedBatch() {
//            //alert("hello");
//            $.ajax({
//                type: "post",
//                url: "common/cfbiomatricenroll.php",
//                data: "action=FillSelectedBatch&values=" + BatchCode + "",
//                success: function (data) {
//                    //alert(data);
//                    txtBatch.value = data;
//                }
//            });
//        }

//        function FillSelectedCourse() {
//            //alert("hello");
//            $.ajax({
//                type: "post",
//                url: "common/cfbiomatricenroll.php",
//                data: "action=FillSelectedCourse&values=" + CourseCode + "",
//                success: function (data) {
//                    txtCourse.value = data;
//                }
//            });
//        }

        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=FILLCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                    //$("#ddlCourse").val($('option:selected', this).text() );
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=FILLBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });
        $("#ddlBatch").change(function () {
            showLearnerData(this.value, ddlCourse.value);
        });



        function Verify() {
            var quality = 60; //(1 to 100) (recommanded minimum 55)
            var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )       

            //alert("verify");
            try {
                var isotemplate = document.getElementById('txtIsoTemplate').value;
                var res = VerifyFinger(isotemplate, isotemplate);

                if (res.httpStaus) {
                    if (res.data.Status) {
                        //alert("Finger matched");
                    }
                    else {
                        if (res.data.ErrorCode != "0") {
                            alert(res.data.ErrorDescription);
                        }
                        else {
                            //alert("Finger not matched");
                        }
                    }
                }
                else {
                    alert(res.err);
                }
            }
            catch (e) {
                alert(e);
            }
            return false;
        }

        function gridcall(lid) {

            // alert("00");
            var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.       
            $.ajax({
                type: "POST",
                url: url,
                data: "action=GetVerifyLearnerData&values=" + lid + "",
                success: function (data)
                {
                    data = $.parseJSON(data);
                    admission_code.value = data[0].AdmissionCode;
                    txtLearnerName.value = data[0].lname;
                    txtQuality.value = data[0].quality;
                    txtNFIQ.value = data[0].nfiq;
                    txtIsoTemplate.value = data[0].template;
                    txtIsoImage.value = data[0].image;
                    txtRawData.value = data[0].raw;
                    txtWsqData.value = data[0].wsq;
                    $("#SelectedDetails").show();
                    Verify();
                    Match();
                }
            });
        }

        function showLearnerData(val, val1) {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=SHOWREGISTERLEARNER&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    $('#response').empty();
                    $("#menuList").html(data);
                    $('#example').DataTable();

                    $(".punchin").click(function () {

                        var lid = $(this).attr('id');
                        //alert(lid);
                        gridcall(lid);
                    });

                }
            });
        }
        
            function SubmitDetails() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmCourseBatch").serialize();
                if (Mode == 'In') {
            data = "action=In&" + forminput;
            }
                else {
            data = "action=In&" + forminput;
            }

                $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                    {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                        $('#response').empty();
                       // $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");

                        Mode = "Add";
                    //resetForm("frmLearnerCapture");
                    }
                    else
                        {
                        $('#response').empty();
                   // $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                //showData();
            }
            });
        return false; // avoid to execute the actual submit of the form.
    }

    });
</script>
</html>