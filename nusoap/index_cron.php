<?php
ini_set('display_errors', 0);
require 'BAL/clsApiMyRkclFunctions.php';
include("array2xml.php");
$loadTime = time();
require_once("nuSOAP/lib/nusoap.php");

//$namespace = "http://localhost/nusoap/index.php";
// create a new soap server
$server = new soap_server();
// configure our WSDL
$server->configureWSDL("RKCL APIs");
// set our namespace
//$server->wsdl->schemaTargetNamespace = $namespace;

//Register a method that has parameters and return types


//Create a complex type
$server->wsdl->addComplexType('MyComplexType','complexType','struct','all','',
		array( 'ID' => array('name' => 'ID','type' => 'xsd:int'),
			   'YourName' => array('name' => 'YourName','type' => 'xsd:string')));

       
$server->register
	(
		"getInfoByTableDetail", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","tablekey" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#getEmployeeInfo", 
		"rpc", 
		"encoded", 
		"Get employee information for specified"
	);

$server->register
	(
		"getTableTotalCount", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","tablename" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#getTableTotalCount", 
		"rpc", 
		"encoded", 
		"Get LMS information for specified"
	);

$server->register
	(
		"editIsNewMYRKCLRecord", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","tablename" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#editIsNewMYRKCLRecord", 
		"rpc", 
		"encoded", 
		"Get LMS information for specified"
	);

$server->register
	(
		"getLMSInfoAll", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","tablename" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#getLMSInfoAll", 
		"rpc", 
		"encoded", 
		"Get LMS information for specified"
	);
$server->register
	(
		"updateMyRkclInfoAll", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","tablename" => "xsd:string", "columnname" => "xsd:string", "lastrecordid" => "xsd:integer"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#updateMyRkclInfoAll", 
		"rpc", 
		"encoded", 
		"Update LMS information for specified"
	);


  

/* Define the method as a PHP function */

        /* Show Table Count Detail*/  
        function getTableTotalCount($cname, $hashkey, $clientip, $tablename)
	{
              //return encodeArray($tablename);  die;
                
                $information = array();
                $emp = new clsAPiClickFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                         /* if we will send the data then goes here*/
                        $information3 = $emp->getInfoTableTotalCount($tablename);
                        if($information3[0]=='Success'){
                            $_DataTable = array();
                            $_i = 0;
                            while ($_Row = mysqli_fetch_array($information3[2], MYSQLI_ASSOC)) {
                                $_Datatable[$_i] = $_Row;
                                $_i = $_i + 1;
                            }
                        }else{
                             $hashkey='No New Record Found.';
                             return encodeArray($hashkey); 
                        }
                       //$information = 'key matched';
                        return encodeArray($_Datatable);
               }else{
                    $hashkey='Credentials You have provided, Does not matched.';
                    return encodeArray($hashkey); 
                } 
               
               
	} 
       /* Show Table Count Detail*/  

     ##### for Admin Select Users  #####################    
    function getInfoByTableDetail($cname, $hashkey, $clientip, $tablekey)
	{
                $information = array();
                $emp = new clsAPiClickFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                    $_Row = mysqli_fetch_array($RkclApiPermissions[2]);
                    $rkclAPi_id=$_Row['rkclAPi_id'];// client api id
                    
                    /* For the getting the table name*/
                    $TableName = $emp->getRkclApiTableName($tablekey);
                    $Table_Row = mysqli_fetch_array($TableName[2]);
                    $ftablename=$Table_Row['funtablename'];
                    /* For the getting the table name*/
                   
                    $information2 = $emp->getRkclApiFunctionsDetail($rkclAPi_id,$ftablename);
                    $_Row2 = mysqli_fetch_array($information2[2]);
                    $Functionfileds=$_Row2['Functionfileds'];
                    $string = rtrim($Functionfileds, ", ");

                    $information3 = $emp->getDataByTabNameAndField($string,$ftablename);
                    if($information3[0]=='Success'){
                        $_DataTable = array();
                        $_i = 0;
                        while ($_Row = mysqli_fetch_array($information3[2], MYSQLI_ASSOC)) {
                            $_Datatable[$_i] = $_Row;
                            $_i = $_i + 1;
                        } 
                        
                        return encodeArray($_Datatable);
                        
                    }else{
                        $hashkey='No Record Found.';
                        return encodeArray($hashkey); 
                    }
                                       
                }else{
                    $hashkey='Credentials You have provided, Does not matched.';
                    return encodeArray($hashkey); 
                }
                
                                
	} 
     ##### for Admin Select Users  #####################
        
     function editIsNewMYRKCLRecord($cname, $hashkey, $clientip, $tablename)
	{
              //return encodeArray($tablename);  die;
                
                $information = array();
                $emp = new clsAPiClickFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                         /* if we will send the data then goes here*/
                        $information3 = $emp->UpdateIsNewMYRKCLRecord($tablename);
                        if($information3[0]=='Success'){
                            $_DataTable = array();
                            $_i = 0;
                            while ($_Row = mysqli_fetch_array($information3[2], MYSQLI_ASSOC)) {
                                $_Datatable[$_i] = $_Row;
                                $_i = $_i + 1;
                            }
                        }else{
                             //$hashkey='No New Record Found.';
                            $hashkey=1;
                             return encodeArray($hashkey); 
                        }
                       //$information = 'key matched';
                        return encodeArray($_Datatable);
               }else{
                    ///$hashkey='Credentials You have provided, Does not matched.';
                   $hashkey=2;
                    return encodeArray($hashkey); 
                } 
               
               
	}   
        
    /*  All LMS releted function are working form here  */ 
        
    function getLMSInfoAll($cname, $hashkey, $clientip, $tablename)
	{
              //return encodeArray($tablename);  die;
                
                $information = array();
                $emp = new clsAPiClickFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                         /* if we will send the data then goes here*/
                        $information3 = $emp->getInfoByFieldsTableName($tablename);
                        if($information3[0]=='Success'){
                            $_DataTable = array();
                            $_i = 0;
                            while ($_Row = mysqli_fetch_array($information3[2], MYSQLI_ASSOC)) {
                                $_Datatable[$_i] = $_Row;
                                $_i = $_i + 1;
                            }
                        }else{
                             //$hashkey='No New Record Found.';
                            $hashkey=1;
                             return encodeArray($hashkey); 
                        }
                       //$information = 'key matched';
                        return encodeArray($_Datatable);
               }else{
                    ///$hashkey='Credentials You have provided, Does not matched.';
                   $hashkey=2;
                    return encodeArray($hashkey); 
                } 
               
               
	} 
        
    function updateMyRkclInfoAll($cname, $hashkey, $clientip, $tablename, $columnname, $lastrecordid)
	{
              //return encodeArray($tablename);  die;
                
                $information = array();
                $emp = new clsAPiClickFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                       /* if lastrecord will found something then goes here*/
                        $information2 = $emp->MyRkclTableFieldUpdate($tablename,$columnname,$lastrecordid);
                        //$information = 'key matched';
                        return encodeArray($information2[0]);
                }else{
                    $hashkey='Credentials You have provided, Does not matched.';
                    return encodeArray($hashkey); 
                } 
               
               
	} 
    
    /*  All LMS releted function are working form here  */
        
    
  ############### Validatemfunction Start from here #########
     
        
   function encodeArray($array)
	{
		return base64_encode(json_encode($array));
	}     
        
        
 #####################################################
// Get our posted data if the service is being consumed
// otherwise leave this data blank.                
$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) 
                ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';

// pass our posted data (or nothing) to the soap service                    
$server->service($POST_DATA);                
exit();

?>