<?php
ini_set('display_errors', 0);
require 'BAL/clsApiGetFunctions.php';
include("array2xml.php");
$loadTime = time();
require_once("nuSOAP/lib/nusoap.php");

//$namespace = "http://localhost/nusoap/index.php";
// create a new soap server
$server = new soap_server();
// configure our WSDL
$server->configureWSDL("RKCL APIs");
// set our namespace
//$server->wsdl->schemaTargetNamespace = $namespace;

//Register a method that has parameters and return types


//Create a complex type
$server->wsdl->addComplexType('MyComplexType','complexType','struct','all','',
		array( 'ID' => array('name' => 'ID','type' => 'xsd:int'),
			   'YourName' => array('name' => 'YourName','type' => 'xsd:string')));

       
$server->register
	(
		"getInfoByTableDetail", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","tablekey" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#getEmployeeInfo", 
		"rpc", 
		"encoded", 
		"Get employee information for specified"
	);
$server->register
	(
		"getLMSInfoAll", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","tablename" => "xsd:string", "batchname" => "xsd:string", "coursename" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#getLMSInfoAll", 
		"rpc", 
		"encoded", 
		"Get LMS information for specified"
	);
$server->register
	(
		"updateLMSInfoAll", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","tablename" => "xsd:string", "columnname" => "xsd:string", "lastrecordid" => "xsd:integer", "batchname" => "xsd:string", "coursename" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#updateLMSInfoAll", 
		"rpc", 
		"encoded", 
		"Update LMS information for specified"
	);

$server->register
	(
		"getTestInfo", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","userid" => "xsd:integer"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#getTestInfo", 
		"rpc", 
		"encoded", 
		"Get test information for specified"
	);


$server->register
	(
		"insertLearnerScore", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","ITGK_code" => "xsd:string","Learner_Score" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#insertLearnerScore", 
		"rpc", 
		"encoded", 
		"Insert Learner information for specified"
	);
  

$server->register
	(
		"getAdmissionLearnerCount", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","batchname" => "xsd:string","coursename" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#getAdmissionLearnerCount", 
		"rpc", 
		"encoded", 
		"Get LMS information for specified"
	);

$server->register
	(
		"getLearnerCountForLMSTable", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#getLearnerCountForLMSTable", 
		"rpc", 
		"encoded", 
		"Get LMS information for specified table"
	);

/* Define the method as a PHP function */



     ##### for Admin Select Users  #####################    

    function getAdmissionLearnerCount($cname, $hashkey, $clientip, $batchname, $coursename)
	{
              //return encodeArray($tablename);  die;
                
                $information = array();
                $emp = new clsAPiGetFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                         /* if we will send the data then goes here*/
                        $information3 = $emp->getInfoForLearnerCount($batchname, $coursename);
                        if($information3[0]=='Success'){
                            $_DataTable = array();
                            $_i = 0;
                            while ($_Row = mysqli_fetch_array($information3[2], MYSQLI_ASSOC)) {
                                $_Datatable[$_i] = $_Row;
                                $_i = $_i + 1;
                            }
                        }else{
                             $hashkey='No New Record Found.';
                             return encodeArray($hashkey); 
                        }
                       //$information = 'key matched';
                        return encodeArray($_Datatable);
               }else{
                    $hashkey='Credentials You have provided, Does not matched.';
                    return encodeArray($hashkey); 
                } 
               
               
	} 
        



    function getInfoByTableDetail($cname, $hashkey, $clientip, $tablekey)
	{
                $information = array();
                $emp = new clsAPiGetFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                    $_Row = mysqli_fetch_array($RkclApiPermissions[2]);
                    $rkclAPi_id=$_Row['rkclAPi_id'];// client api id
                    
                    /* For the getting the table name*/
                    $TableName = $emp->getRkclApiTableName($tablekey);
                    $Table_Row = mysqli_fetch_array($TableName[2]);
                    $ftablename=$Table_Row['funtablename'];
                    /* For the getting the table name*/
                   
                    $information2 = $emp->getRkclApiFunctionsDetail($rkclAPi_id,$ftablename);
                    $_Row2 = mysqli_fetch_array($information2[2]);
                    $Functionfileds=$_Row2['Functionfileds'];
                    $string = rtrim($Functionfileds, ", ");

                    $information3 = $emp->getDataByTabNameAndField($string,$ftablename);
                    if($information3[0]=='Success'){
                        $_DataTable = array();
                        $_i = 0;
                        while ($_Row = mysqli_fetch_array($information3[2], MYSQLI_ASSOC)) {
                            $_Datatable[$_i] = $_Row;
                            $_i = $_i + 1;
                        } 
                        
                        return encodeArray($_Datatable);
                        
                    }else{
                        $hashkey='No Record Found.';
                        return encodeArray($hashkey); 
                    }
                                       
                }else{
                    $hashkey='Credentials You have provided, Does not matched.';
                    return encodeArray($hashkey); 
                }
                
                                
	} 
    ##### for Admin Select Users  #####################
        
        
        
        
        function getLearnerCountForLMSTable($cname, $hashkey, $clientip)
	{
              //return encodeArray($tablename);  die;
                
                $information = array();
                $emp = new clsAPiGetFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                         /* if we will send the data then goes here*/
                        $information3 = $emp->getLearnerCountForLMSTable();
                        if($information3[0]=='Success'){
                            $_DataTable = array();
                            $_i = 0;
                            while ($_Row = mysqli_fetch_array($information3[2], MYSQLI_ASSOC)) {
                                $_Datatable[$_i] = $_Row;
                                $_i = $_i + 1;
                            }
                        }else{
                             $hashkey='No New Record Found.';
                             return encodeArray($hashkey); 
                        }
                       //$information = 'key matched';
                        return encodeArray($_Datatable);
               }else{
                    $hashkey='Credentials You have provided, Does not matched.';
                    return encodeArray($hashkey); 
                } 
               
               
	} 
        
        
        
        
    /*  All LMS releted function are working form here  */ 
        
        
        
    function getLMSInfoAll($cname, $hashkey, $clientip, $tablename, $batchname, $coursename)
	{
              //return encodeArray($tablename);  die;
                
                $information = array();
                $emp = new clsAPiGetFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                         /* if we will send the data then goes here*/
                        $information3 = $emp->getInfoByFieldsTableName($tablename, $batchname, $coursename);
                        if($information3[0]=='Success'){
                            $_DataTable = array();
                            $_i = 0;
                            while ($_Row = mysqli_fetch_array($information3[2], MYSQLI_ASSOC)) {
                                $_Datatable[$_i] = $_Row;
                                $_i = $_i + 1;
                            }
                        }else{
                             $hashkey='No New Record Found.';
                             return encodeArray($hashkey); 
                        }
                       //$information = 'key matched';
                        return encodeArray($_Datatable);
               }else{
                    $hashkey='Credentials You have provided, Does not matched.';
                    return encodeArray($hashkey); 
                } 
               
               
	} 
        
    function updateLMSInfoAll($cname, $hashkey, $clientip, $tablename, $columnname, $lastrecordid, $batchname, $coursename)
	{
              //return encodeArray($tablename);  die;
                
                $information = array();
                $emp = new clsAPiGetFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                       /* if lastrecord will found something then goes here*/
                        $information2 = $emp->LMSTableFieldUpdate($tablename,$columnname,$lastrecordid,$batchname,$coursename);
                        //$information = 'key matched';
                        return encodeArray($information2[0]);
                }else{
                    $hashkey='Credentials You have provided, Does not matched.';
                    return encodeArray($hashkey); 
                } 
               
               
	} 
    
    /*  All LMS releted function are working form here  */
        
    /*  Insert Learner Score releted function are here  */
         function insertLearnerScore($cname, $hashkey, $clientip, $ITGK_code, $Learner_Score)
	{
                $information = array();
                $emp = new clsAPiGetFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                       
                        $Learner_Score=json_decode(base64_decode($Learner_Score), true);
                        /* if Learner_Score is array then goes here*/
                        if(count($Learner_Score) >0 || is_array($Learner_Score)){
                            
                         $information2 = $emp->LearnerScoreInsert($Learner_Score, $ITGK_code);
                            //echo "<pre>";print_r($information2);
                            return encodeArray($information2);
                            
                        }else{
                            //$information = 'No Data Found For Insert And update.';
                            $information = 2; //
                        return encodeArray($information);
                        }
                        
                }else{
                    //$hashkey='Credentials You have provided, Does not matched.';
                    $hashkey = 3;
                    return encodeArray($hashkey); 
                } 
               
               
	} 
    /*  Insert Learner Score releted function are here  */  
        
        
       function getTestInfo($cname, $hashkey, $clientip, $userid)
	{
		//return time();
                $test='Hi';
                return encodeArray($test);
	} 
  ############### Validatemfunction Start from here #########
     
        
   function encodeArray($array)
	{
		return base64_encode(json_encode($array));
	}     
        
        
 #####################################################
// Get our posted data if the service is being consumed
// otherwise leave this data blank.                
$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) 
                ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';

// pass our posted data (or nothing) to the soap service                    
$server->service($POST_DATA);                
exit();

?>