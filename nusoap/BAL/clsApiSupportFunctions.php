<?php

/*
 *  author SUNIL KUMAR BAINDARA

 */

include('DAL/classconnectionNEW.php');

$_ObjConnection = new _Connection();
$_Response = array();


class clsAPiSupportFunctions {
    //put your code here
    
    ##### for Admin Select Users  #####################
    public function getDataByTabNameAndField($string,$tablename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "Select ".$string." From ".$tablename." where IsNewRecord='Y'" ;
             $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
             return $_Response;
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    ##### for Admin Select Users  #####################

    ##### for LMS functions #####################
      
    public function getInfoByFieldsTableName($tablename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($tablename=='tbl_user_master') {
                 $_UpdateQuery = "Update $tablename set IsNewSRecord = 'P' where IsNewSRecord='Y' and User_Code > 1 and User_UserRoll=7 or  User_UserRoll='14' or  User_UserRoll='28'  or  User_UserRoll='23' limit 1000";
                 $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            elseif ($tablename=='tbl_userprofile') {
                 $_UpdateQuery = " Update $tablename set IsNewSRecord = 'P' where IsNewSRecord='Y' limit 1000";
                 $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            elseif ($tablename=='tbl_organization_detail') {
                 $_UpdateQuery = " Update $tablename set IsNewSRecord = 'P' where IsNewSRecord='Y'  limit 1000";
                 $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            }
            else {
                 //$_UpdateQuery = " Update $tablename set IsNewSRecord = 'P' where IsNewSRecord='Y'  ";
            }
            
            
            
            
            
            if ($tablename=='tbl_user_master') {
                    $_SelectQuery = "Select User_Code,User_EmailId,User_MobileNo,User_LoginId,User_UserRoll,User_ParentId,User_Rsp,User_Status,User_Type,User_Ack,User_CreatedDate,User_Timestamp From ".$tablename." where IsNewSRecord='P' and User_Code > 1 and User_UserRoll=7 or  User_UserRoll='14' or  User_UserRoll='28' limit 1000" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }elseif ($tablename=='tbl_userprofile') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewSRecord='P' limit 1000" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }elseif ($tablename=='tbl_organization_detail') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewSRecord='P' limit 1000" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
             
            }else{
             
//                    $_SelectQuery = "Select * From ".$tablename." where IsNewRecord='Y'" ;
//                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
//                    return $_Response;  
            }
             
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    
    public function SupportTableFieldUpdate($tablename,$columnname,$lastrecordid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		
		if($tablename=='tbl_user_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewSRecord='N' Where ".$columnname."<=" . $lastrecordid . " and User_Code > 1  and IsNewSRecord='P' limit 1000";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_organization_detail'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewSRecord='N' Where ".$columnname."<=" . $lastrecordid . " and IsNewSRecord='P' limit 1000";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_userprofile'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewSRecord='N' Where ".$columnname."<=" . $lastrecordid . " and IsNewSRecord='P' limit 1000";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		
				
		else{
		// no else condition found 
		}
            
             
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
   
    
    ##### for LMS functions #####################
    

    
    # Permission Releted Query Here #
    
    public function getRkclApiPermissions($ClientName,$api_key,$clientip)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_rkcl_api_permissions where ClientName = '".$ClientName."' and Apikey = '".$api_key."' and ClientIP = '".$clientip."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function getRkclApiTableName($functionname)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT funtablename FROM tbl_rkcl_api_functions where functionname = '".$functionname."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function getRkclApiFunctionsDetail($rkclAPi_id,$functionname)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT Functionfileds FROM tbl_rkcl_api_fun_details where client_rkclAPi_id  = '".$rkclAPi_id."' and Functionname = '".$functionname."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
     # Permission Releted Query Here #
    
    
    
  //put your code here 
}

?>