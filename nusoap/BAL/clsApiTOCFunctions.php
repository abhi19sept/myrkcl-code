<?php

/*
 *  author Sunil Kumar Baindara

 */

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
include('DAL/classconnectionNEW.php');

$_ObjConnection = new _Connection();
$_Response = array();


class clsAPiTOCFunctions {
    //put your code here
    
   
    public function getInfoByFieldsTableName($tablename,$batchname,$coursename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $batchsql='';
            if($batchname!=''){
                $batchsql=' and Admission_Batch="'.$batchname.'" and Admission_Course="'.$coursename.'"';
            }
            if($tablename=='tbl_admission'){
                 $_UpdateQuery = " Update $tablename set IsNewOnlineLMSRecord = 'P' where IsNewOnlineLMSRecord='Y'  ".$batchsql."  and Admission_Batch > 117 and Admission_Payment_Status=1  limit 2000";
            }
            elseif ($tablename=='tbl_user_master') {
                 $_UpdateQuery = " Update $tablename set IsNewOnlineLMSRecord = 'P' where IsNewOnlineLMSRecord='Y' and User_Code > 1 and User_UserRoll=7  limit 200";
            }
             elseif ($tablename=='tbl_courseitgk_mapping') {
                 $_UpdateQuery = " Update $tablename set IsNewOnlineLMSRecord = 'P' where IsNewOnlineLMSRecord='Y' and Courseitgk_Course !='RS-CFA'  limit 1000";
            }
            else {
                 $_UpdateQuery = " Update $tablename set IsNewOnlineLMSRecord = 'P' where IsNewOnlineLMSRecord='Y'  limit 2000";
            }
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            //return $_Response;
            
            if($tablename=='tbl_admission'){
                
                    $_SelectQuery = "Select * From ".$tablename."  where IsNewOnlineLMSRecord='P'  ".$batchsql." and Admission_Batch > 117 and Admission_Payment_Status=1 order by Admission_Code ASC Limit 2000" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                
                
            }
            elseif ($tablename=='tbl_courseitgk_mapping') {
                    $_SelectQuery = "Select Courseitgk_Code, Courseitgk_Course, Courseitgk_ITGK,
                                            Courseitgk_EOI, CourseITGK_BlockStatus, CourseITGK_UserCreatedDate, CourseITGK_UserFinalApprovalDate, CourseITGK_StartDate,
                                            CourseITGK_ExpireDate, EOI_Fee_Confirm, Courseitgk_TranRefNo,Courseitgk_timestamp,
                                           IsNewRecord From ".$tablename." where IsNewOnlineLMSRecord='P' and Courseitgk_Course !='RS-CFA'  limit 1000" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
            }
            elseif ($tablename=='tbl_user_master') {
                    $_SelectQuery = "Select User_Code, User_EmailId, User_MobileNo, User_LoginId,ifnull(User_RSP,'') as User_RSP,
                                            User_Password, User_UserRoll, User_ParentId, User_Status,
                                            IsNewRecord From ".$tablename." where IsNewOnlineLMSRecord='P' and User_Code > 1 and User_UserRoll=7  limit 200" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_organization_detail') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewOnlineLMSRecord='P'  limit 2000" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_course_master') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewOnlineLMSRecord='P'  limit 20" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_batch_master') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewOnlineLMSRecord='P'  limit 500" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_entity_master') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewOnlineLMSRecord='P'  limit 20" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_userprofile') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewOnlineLMSRecord='P'  limit 1000" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_userroll_master') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewOnlineLMSRecord='P'  limit 20" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
            }
            else{
             
//                    $_SelectQuery = "Select * From ".$tablename." where IsNewRecord='Y'" ;
//                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
//                    return $_Response;  
            }
             
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    
    public function TOCTableFieldUpdate($tablename,$columnname,$lastrecordid,$batchname,$coursename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
                $batchsql='';
                if($batchname!=''){
                    $batchsql=' and Admission_Batch="'.$batchname.'" and Admission_Course="'.$coursename.'"';
                }
		if($tablename=='tbl_admission'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewOnlineLMSRecord='N' 
			Where ".$columnname."<=" . $lastrecordid . "  ".$batchsql."    and    Admission_Batch > 117 and IsNewOnlineLMSRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_courseitgk_mapping'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewOnlineLMSRecord='N' 
			Where ".$columnname."<=" . $lastrecordid . " and Courseitgk_Course !='RS-CFA' and IsNewOnlineLMSRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_user_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewOnlineLMSRecord='N' 
			Where ".$columnname."<=" . $lastrecordid . " and User_Code > 1  and IsNewOnlineLMSRecord='P'";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_organization_detail'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewOnlineLMSRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewOnlineLMSRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_course_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewOnlineLMSRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewOnlineLMSRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_batch_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewOnlineLMSRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewOnlineLMSRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_entity_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewOnlineLMSRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewOnlineLMSRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_userprofile'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewOnlineLMSRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewOnlineLMSRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_userroll_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewOnlineLMSRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewOnlineLMSRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		
		else{
		// no else condition found 
		}
            
             
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
     public function getLearnerAttendance($lcode,$todate) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "select * from tbl_learner_attendance where Attendance_Admission_Code='".$lcode."' and Attendance_In_Time like '".$todate."%' order by Attendance_In_Time desc limit 0,1" ;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            return $_Response;
             
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    ##### for LMS functions #####################
  
    
    # Permission Releted Query Here #
    
    public function getRkclApiPermissions($ClientName,$api_key,$clientip)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_rkcl_api_permissions where ClientName = '".$ClientName."' and Apikey = '".$api_key."' and ClientIP = '".$clientip."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function getRkclApiTableName($functionname)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT funtablename FROM tbl_rkcl_api_functions where functionname = '".$functionname."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function getRkclApiFunctionsDetail($rkclAPi_id,$functionname)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT Functionfileds FROM tbl_rkcl_api_fun_details where client_rkclAPi_id  = '".$rkclAPi_id."' and Functionname = '".$functionname."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
     # Permission Releted Query Here #
    
    
    
  //put your code here 
}

?>