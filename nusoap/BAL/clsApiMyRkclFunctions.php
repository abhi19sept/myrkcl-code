<?php

/*
 *  author SUNIL KUMAR BAINDARA

 */

include('DAL/classconnectionNEW.php');

$_ObjConnection = new _Connection();
$_Response = array();


class clsAPiClickFunctions {
    //put your code here
    
    ##### for Admin Select Users  #####################
    public function getDataByTabNameAndField($string,$tablename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "Select ".$string." From ".$tablename." where IsNewMYRKCLRecord='Y'" ;
             $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
             return $_Response;
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    ##### for Admin Select Users  #####################

    public function UpdateIsNewMYRKCLRecord($tablename)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_UpdateQuery = " Update $tablename set IsNewMYRKCLRecord = 'Y' where IsNewMYRKCLRecord='P' OR IsNewMYRKCLRecord='N'  ";
            //$_UpdateQuery = " Update $tablename set IsNewRecord = 'Y' where IsNewRecord='P' OR IsNewRecord='N'  ";
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    
    ##### for LMS functions #####################
      
    public function getInfoByFieldsTableName($tablename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            if ($tablename=='tbl_tehsil_master') {
                 $_UpdateQuery = " Update $tablename set IsNewMYRKCLRecord = 'P' where IsNewMYRKCLRecord='Y' ";
            }
            else {
                 $_UpdateQuery = " Update $tablename set IsNewMYRKCLRecord = 'P' where IsNewMYRKCLRecord='Y' ";
            }
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            
            
            
           /////////////// 
            
                    
            if ($tablename=='tbl_tehsil_master') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewMYRKCLRecord='P' " ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
             
            }else{
             
                    $_SelectQuery = "Select * From ".$tablename." where IsNewMYRKCLRecord='P'" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;  
            }
             
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    
    public function MyRkclTableFieldUpdate($tablename,$columnname,$lastrecordid) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
		
		
		if($tablename=='tbl_tehsil_master'){
                        $_UpdateQuery = "Update ".$tablename." set IsNewMYRKCLRecord='N' Where ".$columnname."<=" . $lastrecordid . " and IsNewMYRKCLRecord='P'";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		else{
		// no else condition found 
                        $_UpdateQuery = "Update ".$tablename." set IsNewMYRKCLRecord='N' Where ".$columnname."<=" . $lastrecordid . " and IsNewMYRKCLRecord='P'";
                        $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
            
             
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
   
    
    ##### for LMS functions #####################
    
    public function getInfoTableTotalCount($tablename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
           
            $_SelectQuery = "Select IsNewMYRKCLRecord, count(*) as TableCount From ".$tablename." Group By IsNewMYRKCLRecord" ;
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            return $_Response;
             
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    # Permission Releted Query Here #
    
    public function getRkclApiPermissions($ClientName,$api_key,$clientip)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_rkcl_api_permissions where ClientName = '".$ClientName."' and Apikey = '".$api_key."' and ClientIP = '".$clientip."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function getRkclApiTableName($functionname)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT funtablename FROM tbl_rkcl_api_functions where functionname = '".$functionname."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function getRkclApiFunctionsDetail($rkclAPi_id,$functionname)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT Functionfileds FROM tbl_rkcl_api_fun_details where client_rkclAPi_id  = '".$rkclAPi_id."' and Functionname = '".$functionname."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
     # Permission Releted Query Here #
    
    
    
  //put your code here 
}

?>