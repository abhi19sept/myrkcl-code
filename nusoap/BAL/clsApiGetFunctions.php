<?php

/*
 *  author Sunil Kumar Baindara

 */

include('DAL/classconnectionNEW.php');

$_ObjConnection = new _Connection();
$_Response = array();


class clsAPiGetFunctions {
    //put your code here
    
    ##### for Admin Select Users  #####################
    public function getDataByTabNameAndField($string,$tablename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             $_SelectQuery = "Select ".$string." From ".$tablename." where IsNewRecord='Y'" ;
             $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
             return $_Response;
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    ##### for Admin Select Users  #####################

    public function getInfoForLearnerCount($batchname, $coursename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            /*$_SelectQuery = "select count(*) as LearnerCount from ".$tablename." 
                                    where Admission_Payment_Status=1 and Admission_Batch in(".$batch.")" ;
            
            $_SelectQuery= "select count(*) as LearnerCount from tbl_admission a 
                            INNER JOIN tbl_batch_master b on a.Admission_Batch = b.Batch_Code
                            INNER JOIN tbl_course_master c on a.Admission_Course = c.Course_Code
                            where a.Admission_Payment_Status=1 
                            and b.Batch_Name='".$batchname."'
                            and c.Course_Name='".$coursename."'";*/
            
            $_SelectQuery = "select count(*) as LearnerCount from tbl_admission 
                            where Admission_Payment_Status=1 and Admission_Batch='".$batchname."' and Admission_Course='".$coursename."'" ;
            
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            return $_Response;
             
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    ##### for LMS functions #####################
    
    
    public function getLearnerCountForLMSTable() {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            
            $_SelectQuery= "SELECT 'Entity Master' as TableName, COUNT(*) as RecordCount from tbl_Entity_Master
                            union
                            SELECT 'Userroll Master',COUNT(*) from tbl_userroll_master
                            union
                            SELECT 'User Master',COUNT(*) from tbl_user_master
                            union
                            SELECT 'UserProfile',COUNT(*) from tbl_userprofile
                            union
                            SELECT 'Organization Detail', COUNT(*) from tbl_Organization_Detail
                            union
                            SELECT 'Course Master',COUNT(*) from tbl_Course_Master
                            union
                            SELECT 'Batch Master',COUNT(*) from tbl_Batch_Master
                            union
                            SELECT 'Course ITGK Mapping',COUNT(*) from tbl_courseitgk_mapping
                            union
                            SELECT 'Admission',COUNT(*) from tbl_Admission";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            return $_Response;
             
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    
    public function getInfoByFieldsTableName($tablename,$batchname,$coursename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
            $batchsql='';
            if($batchname!=''){
                //$batchsql=' and Admission_Batch in (SELECT Batch_Code FROM tbl_batch_master WHERE Batch_Name="'.$batchname.'")';
                $batchsql=' and Admission_Batch="'.$batchname.'" and Admission_Course="'.$coursename.'"';
            }
            
            if($tablename=='tbl_admission'){
                 $_UpdateQuery = " Update $tablename set IsNewRecord = 'P' where IsNewRecord='Y' and Admission_Batch > 117 and Admission_Payment_Status=1 ";
            }
            elseif ($tablename=='tbl_user_master') {
                 $_UpdateQuery = " Update $tablename set IsNewRecord = 'P' where IsNewRecord='Y' and User_Code > 1 and User_UserRoll=7 ";
            }
             elseif ($tablename=='tbl_courseitgk_mapping') {
                 $_UpdateQuery = " Update $tablename set IsNewRecord = 'P' where IsNewRecord='Y' and Courseitgk_Course !='RS-CFA'";
            }
            else {
                 $_UpdateQuery = " Update $tablename set IsNewRecord = 'P' where IsNewRecord='Y'  ";
            }
            $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            //return $_Response;
            
            
            if($tablename=='tbl_admission'){
                
                    $_SelectQuery = "Select Admission_Code,Admission_LearnerCode,Admission_ITGK_Code,
                            ifnull(BioMatric_Status,0) as BioMatric_Status,						
                            cast(ifnull(Admission_Date,'2016-01-01') as datetime) as Admission_Date,ifnull(Admission_Batch,0) as Admission_Batch,ifnull(Admission_Course,0) as Admission_Course, ifnull(Admission_Fee,0) as Admission_Fee,				
                            ifnull(Admission_Installation_Mode,0) as Admission_Installation_Mode,
                            ifnull(Admission_PhotoUpload_Status,0) as Admission_PhotoUpload_Status, 
                            ifnull(Admission_SignUpload_Status,0) as Admission_SignUpload_Status,			
                            ifnull(Admission_PhotoProcessing_Status,'') as Admission_PhotoProcessing_Status,
			    ifnull(rejection_reason,'') as rejection_reason,						
                            ifnull(rejection_type,'') as rejection_type,
                            ifnull(Admission_Payment_Status,0) as Admission_Payment_Status ,        
                            ifnull(Admission_ReceiptPrint_Status,0) as Admission_ReceiptPrint_Status,		
                            ifnull(Admission_TranRefNo,'') as Admission_TranRefNo,					
                            ifnull(Admission_Name,'') as Admission_Name,
			    ifnull(Admission_Fname,'') as Admission_Fname,						
                            cast(ifnull(Admission_DOB,'2016-01-01') as datetime) as Admission_DOB,
                              ifnull(Admission_MTongue,'') as Admission_MTongue,ifnull(Admission_Scan,'') as Admission_Scan,						
                            ifnull(Admission_Photo,'') as Admission_Photo,	
                            ifnull(Admission_Sign,'') as Admission_Sign,
			    ifnull(Admission_Gender,'') as Admission_Gender,						
                            ifnull(Admission_MaritalStatus,'') as Admission_MaritalStatus,
                            ifnull(Admission_Medium,'') as Admission_Medium,
			    ifnull(Admission_PH, '') as Admission_PH,							
                            ifnull(Admission_PID,'') as Admission_PID,
                            ifnull(Admission_UID,'') as Admission_UID ,
			    ifnull(Admission_District,0) as Admission_District,					
                            ifnull(Admission_Tehsil,0) as Admission_Tehsil,						
                            ifnull(Admission_Address,'') as Admission_Address,
			    ifnull(Admission_PIN,'') as Admission_PIN,						
                            ifnull(Admission_Mobile,'') as Admission_Mobile,
                            ifnull(Admission_Phone,'') as Admission_Phone,                
                            ifnull(Admission_Email,'') as Admission_Email,	
                            ifnull(Admission_Qualification,0) as Admission_Qualification,				
                            ifnull(Admission_Ltype,0) as Admission_Ltype,
			    ifnull(Admission_GPFNO,'') as Admission_GPFNO,						
                            User_Code,ifnull(Admission_yearid,0) as Admission_yearid  From ".$tablename."  where IsNewRecord='P' and Admission_Batch > 117 and Admission_Payment_Status=1  ".$batchsql." order by Admission_Code ASC Limit 5000" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                
                
            }
            elseif ($tablename=='tbl_courseitgk_mapping') {
                    $_SelectQuery = "Select Courseitgk_Code, Courseitgk_Course, Courseitgk_ITGK,
                                            Courseitgk_EOI, CourseITGK_BlockStatus, CourseITGK_StartDate,
                                            CourseITGK_ExpireDate, EOI_Fee_Confirm, Courseitgk_TranRefNo,
                                           IsNewRecord From ".$tablename." where IsNewRecord='P' and Courseitgk_Course !='RS-CFA'" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
            }
            elseif ($tablename=='tbl_user_master') {
                    $_SelectQuery = "Select User_Code, User_EmailId, User_MobileNo, User_LoginId,ifnull(User_RSP,'') as User_RSP,
                                            User_Password, User_UserRoll, User_ParentId, User_Status,
                                            IsNewRecord From ".$tablename." where IsNewRecord='P' and User_Code > 1 and User_UserRoll=7" ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_organization_detail') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewRecord='P' " ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_course_master') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewRecord='P' " ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_batch_master') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewRecord='P' " ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_entity_master') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewRecord='P' " ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_userprofile') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewRecord='P' " ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
                    
            }
            elseif ($tablename=='tbl_userroll_master') {
                    $_SelectQuery = "Select * From ".$tablename." where IsNewRecord='P' " ;
                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                    return $_Response;
            }
            else{
             
//                    $_SelectQuery = "Select * From ".$tablename." where IsNewRecord='Y'" ;
//                    $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
//                    return $_Response;  
            }
             
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
    
    public function LMSTableFieldUpdate($tablename,$columnname,$lastrecordid,$batchname,$coursename) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            
                $batchsql='';
                if($batchname!=''){
                    $batchsql=' and Admission_Batch="'.$batchname.'" and Admission_Course="'.$coursename.'"';
                }
                
		if($tablename=='tbl_admission'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewRecord='N' 
			Where ".$columnname."<=" . $lastrecordid . "  ".$batchsql."  and Admission_Batch > 117 and IsNewRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_courseitgk_mapping'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewRecord='N' 
			Where ".$columnname."<=" . $lastrecordid . " and Courseitgk_Course !='RS-CFA' and IsNewRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_user_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewRecord='N' 
			Where ".$columnname."<=" . $lastrecordid . " and User_Code > 1  and IsNewRecord='P'";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_organization_detail'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_course_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_batch_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_entity_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_userprofile'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		elseif($tablename=='tbl_userroll_master'){
		    $_UpdateQuery = "Update ".$tablename." set IsNewRecord='N' Where ".$columnname."<=" . $lastrecordid . "  and IsNewRecord='P' ";
			$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
		}
		
		else{
		// no else condition found 
		}
            
             
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            
        }
        return $_Response;
    }
    
    public function LearnerScoreInsert($Learner_Score,$ITGKcode) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
               
            foreach ($Learner_Score as $value) {
             $Admission_Code= $value['Admission_Code'];
             $Score= $value['Score'];
             $Score_Per= $value['Score_Per'];
             $Learner_Code= $value['Learner_Code'];
             //$ITGKcode; 
             
             
            
            $_DuplicateQuery = "Select * From tbl_learner_score Where Admission_Code='" . $Admission_Code . "' AND Learner_Code='" . $Learner_Code . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
                {
                
                $_InsertQuery="INSERT INTO tbl_learner_score(Admission_Code, Learner_Code, Score, Score_Per, ITGK_code, AddEditTime)
                         VALUES ('" . $Admission_Code . "', '" . $Learner_Code . "', '" . $Score . "', '" . $Score_Per . "', '" . $ITGKcode . "', now());";
            
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                //return $_Response; //in both condition insert and  update we are using here 1. 
                }
            else 
                {
                $_UpdateQuery = "Update tbl_learner_score set
                                                    Score='" . $Score . "',
                                                    Score_Per='" . $Score_Per . "',
                                                    AddEditTime=now()
                                    Where Admission_Code='" . $Admission_Code . "'
                                    AND Learner_Code='" . $Learner_Code . "'";                
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                //return $_Response; // in both condition insert and  update we are using here 1.
                
            }
           
                  
       }
         return 1;
         
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
             return 0; // we are sending here 0 for error.
        }
        //return $_Response;
    }
    
    ##### for LMS functions #####################
    
  /// Added: 9-4-2017////
    public function correctionForLearnerDetail($Correction_Detail) {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
             //echo "<pre>"; print_r($Correction_Detail); //die;
            foreach ($Correction_Detail as $value) {
             
             $Correction_ITGK_Code= $value['Correction_ITGK_Code'];
             $lcode= $value['lcode'];
             $cfname= $value['cfname'];
             $cfaname= $value['cfaname'];
             $applicationfor= $value['applicationfor'];
             $Correction_Payment_Status= $value['Correction_Payment_Status'];
             //$cid= $value['cid'];
             $dob= $value['dob'];
             $SentByVMOU= $value['SentByVMOU'];
             //$ITGKcode; 
             
             
            
            $_DuplicateQuery = "Select * From tbl_correction_copy Where Correction_ITGK_Code='" . $Correction_ITGK_Code . "' AND lcode='" . $lcode . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
                {
                
                $_InsertQuery="INSERT INTO tbl_correction_copy(Correction_ITGK_Code, lcode, cfname, cfaname, Correction_Payment_Status,  dob, SentByVMOU)
                         VALUES ('" . $Correction_ITGK_Code . "', '" . $lcode . "', '" . $cfname . "', '" . $applicationfor . "', '" . $Correction_Payment_Status . "', '" . $dob . "', '" . $SentByVMOU . "')";
            
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
                //return $_Response; //in both condition insert and  update we are using here 1. 
                }
            else 
                {
                $_UpdateQuery = "Update tbl_correction_copy set
                                                    cfname='" . $cfname . "',
                                                    cfaname='" . $cfaname . "',
                                                    Correction_Payment_Status='" . $Correction_Payment_Status . "',
                                                    dob='" . $dob . "',
                                                    SentByVMOU='" . $SentByVMOU . "'
                                                  
                                    Where Correction_ITGK_Code='" . $Correction_ITGK_Code . "'
                                    AND lcode='" . $lcode . "'";                
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                //return $_Response; // in both condition insert and  update we are using here 1.
                
            }
           
                  
       }
         //return 1;
        return $_Response;
         
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
             return 0; // we are sending here 0 for error.
        }
        //return $_Response;
    }
    /// Added: 9-4-2017////
    
    # Permission Releted Query Here #
    
    public function getRkclApiPermissions($ClientName,$api_key,$clientip)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT * FROM tbl_rkcl_api_permissions where ClientName = '".$ClientName."' and Apikey = '".$api_key."' and ClientIP = '".$clientip."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function getRkclApiTableName($functionname)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT funtablename FROM tbl_rkcl_api_functions where functionname = '".$functionname."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
    public function getRkclApiFunctionsDetail($rkclAPi_id,$functionname)
    {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "SELECT Functionfileds FROM tbl_rkcl_api_fun_details where client_rkclAPi_id  = '".$rkclAPi_id."' and Functionname = '".$functionname."'";
            $_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            
        } catch (Exception $_ex) {

            $_Response[0] = $_ex->getLine() . $_ex->getTrace();
            $_Response[1] = Message::Error;
           
        }
         return $_Response;
    }
    
     # Permission Releted Query Here #
    
    
    
  //put your code here 
}

?>