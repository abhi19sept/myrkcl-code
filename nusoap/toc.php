<?php
ini_set('display_errors', 0);
require 'BAL/clsApiTOCFunctions.php';
include("array2xml.php");
$loadTime = time();
require_once("nuSOAP/lib/nusoap.php");

//$namespace = "http://localhost/nusoap/index.php";
// create a new soap server
$server = new soap_server();
// configure our WSDL
$server->configureWSDL("RKCL APIs");
// set our namespace
//$server->wsdl->schemaTargetNamespace = $namespace;

//Register a method that has parameters and return types


//Create a complex type
$server->wsdl->addComplexType('MyComplexType','complexType','struct','all','',
		array( 'ID' => array('name' => 'ID','type' => 'xsd:int'),
			   'YourName' => array('name' => 'YourName','type' => 'xsd:string')));


$server->register
	(
		"getLMSInfoAll", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","tablename" => "xsd:string","batchname" => "xsd:string","coursename" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#getLMSInfoAll", 
		"rpc", 
		"encoded", 
		"Get LMS information for specified"
	);
$server->register
	(
		"updateTOCInfoAll", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","tablename" => "xsd:string", "columnname" => "xsd:string", "lastrecordid" => "xsd:integer","batchname" => "xsd:string","coursename" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#updateLMSInfoAll", 
		"rpc", 
		"encoded", 
		"Update LMS information for specified"
	);


$server->register
	(
		"getBIOInfoLearner", 
		array("cname" => "xsd:string","hashkey" => "xsd:string","clientip" => "xsd:string","lcode" => "xsd:string"), 
		array("return" => "xsd:string"), 
		"urn:server", 
		"urn:server#getBIOInfoLearner", 
		"rpc", 
		"encoded", 
		"Get BIO Information for learner"
	);

/* Define the method as a PHP function */


        
        
        
    function getLMSInfoAll($cname, $hashkey, $clientip, $tablename, $batchname, $coursename)
	{
              //return encodeArray($tablename);  die;
                
                $information = array();
                $emp = new clsAPiTOCFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                         /* if we will send the data then goes here*/
                        $information3 = $emp->getInfoByFieldsTableName($tablename,$batchname,$coursename);
                        if($information3[0]=='Success'){
                            $_DataTable = array();
                            $_i = 0;
                            while ($_Row = mysql_fetch_array($information3[2], MYSQL_ASSOC)) {
							    $_Row['User_Password']=htmlentities($_Row['User_Password']);
                                $_Datatable[$_i] = $_Row;
                                $_i = $_i + 1;
                            }
                        }else{
                             $hashkey='No New Record Found.';
                             return encodeArray($hashkey); 
                        }
                       //$information = 'key matched';
                        return encodeArray($_Datatable);
               }else{
                    $hashkey='Credentials You have provided, Does not matched.';
                    return encodeArray($hashkey); 
                } 
               
               
	} 
        
    function updateTOCInfoAll($cname, $hashkey, $clientip, $tablename, $columnname, $lastrecordid, $batchname, $coursename)
	{
              //return encodeArray($tablename);  die;
                
                $information = array();
                $emp = new clsAPiTOCFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                
                if($RkclApiPermissions[0]=='Success'){
                       /* if lastrecord will found something then goes here*/
                        $information2 = $emp->TOCTableFieldUpdate($tablename,$columnname,$lastrecordid,$batchname,$coursename);
                        //$information = 'key matched';
                        return encodeArray($information2[0]);
                }else{
                    $hashkey='Credentials You have provided, Does not matched.';
                    return encodeArray($hashkey); 
                } 
               
               
	} 
        
         function getBIOInfoLearner($cname, $hashkey, $clientip, $lcode)
	{
	
	   date_default_timezone_set('Asia/Kolkata');
             // return encodeArray($lcode);  die;
                
                $information = array();
                $emp = new clsAPiTOCFunctions();
                $RkclApiPermissions = $emp->getRkclApiPermissions($cname, $hashkey, $clientip);
                $todate=date('Y-m-d');
                $information3 = $emp->getLearnerAttendance($lcode,$todate);
                
                if($information3[0]=='Success'){
                            $_Row = mysql_fetch_array($information3[2], MYSQL_ASSOC);
                            $Now=date('Y-m-d H:i:s');
                            $Attendance_In_Time=$_Row['Attendance_In_Time'];
                            
                            //date2 should be greater then to date1
                            $date1 = date_create($Attendance_In_Time); //date of birth
                            //creating a date object
                            $date2 = date_create($Now); //current date

                            $diff12 = date_diff($date2, $date1);
                            //$hoursDiff = $diff12->h;
                            ///$minutesDiff = $diff12->i;
                            
                            $hoursDiff = $diff12->h;
                            $minutesDiff = $diff12->i;
                            $totmint=$hoursDiff*60;
                            $minutesDiff=(int)$minutesDiff + (int)$totmint;
                            
                            //return encodeArray($minutesDiff);  die;
                            /*if($hoursDiff==1){
                                $information = 0; //
                                return encodeArray($information);  
                            }
                            else*/
                            
                            if($minutesDiff > 30){
                                
                                $information = 0; //
                                return encodeArray($information);
                                //echo "30 min se jydada h";
                            }else{
                                $information = 1; //
                                return encodeArray($information);
                                //echo "30 min se kam h";
                            }
                           
                        }else{
                    //$hashkey='Credentials You have provided, Does not matched.';
                    $hashkey=2;        
                    return encodeArray($hashkey); 
                } 
               
               
	} 
    
    /*  All LMS releted function are working form here  */
   
    
  ############### Validatemfunction Start from here #########
     
        
   function encodeArray($array)
	{
		return base64_encode(json_encode($array));
	}     
        
        
 #####################################################
// Get our posted data if the service is being consumed
// otherwise leave this data blank.                
$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) 
                ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';

// pass our posted data (or nothing) to the soap service                    
$server->service($POST_DATA);                
exit();

?>