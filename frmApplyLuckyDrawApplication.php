<?php
$title = "RS-CIT Fee Relaxation Scheme";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Application form For RS-CIT Lucky Draw Scheme(July-2020)
            </div>
            <div class="panel-body">
                <form name="frmApplyLuckyDrawApplication" id="frmApplyLuckyDrawApplication" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                        <div class="col-sm-4 form-group">     
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">

                            </select>

                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>

                        </div> 
                    </div>

                    <div id="menuList" name="menuList" style="margin-top:35px;"> </div> 

                    <div class="container">
                    <!--	<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    -->
                    </div>
                </form>
            </div>
        </div>   
    </div>

</div>
<div class="modal" id="myModal">
    <div class="modal-content">
        <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal">×</button>
            <h3 id="heading-tittle" class="modal-title">Application form For Lucky Draw Relaxation Scheme(July-2020)
</h3>
        </div>
        <div class="modal-body">
            <div class="container">
            <div id="response2"></div>

            </div>        
            <div id="errorBox2"></div>

           
            <!-- Inline CSS based on choices in "Settings" tab -->
            <style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: #ffffff !important;} .asteriskField{color: red;}</style>

            <!-- HTML Form (wrapped in a .bootstrap-iso div) -->
            <div class="bootstrap-iso">
             <div class="container-fluid">
                 
                <form name="frmApplyLuckyDrawApplicationModal" id="frmApplyLuckyDrawApplicationModal" method="post"  role="form"
                      enctype="multipart/form-data">
              <div class="row">
                <div class="col-md-12">
                <p><h4>यह योजना वर्ष 2020 के बोर्ड परीक्षा में शामिल हुए छात्रों के लिए ही मान्य है |</h4></p>
                </div>
               <div class="col-md-4 col-sm-4">
               
                 <div class="form-group ">
                  <label class="control-label " for="itgkname">
                   IT-GK Code
                  </label>
                  <input class="form-control" id="itgkcode" name="itgkcode" type="text" readonly />
                  <input class="form-control" id="acode" name="acode" type="hidden" readonly />
                  <input class="form-control" id="lcodes" name="lcodes" type="hidden" readonly />
                 </div>

                 <div class="form-group ">
                  <label class="control-label " for="name4">
                   Father Name
                  </label>
                  <input class="form-control" id="fname" name="fname" type="text" readonly />
                 </div>
                 
                 <div class="form-group ">
                  <label class="control-label " for="lemail">
                   Email Id
                  </label>
                  <input class="form-control" id="lemail" name="lemail" type="text" readonly />
                 </div>

                 <div class="form-group ">
                  <label class="control-label " for="ltehsil">
                   Tehsil
                  </label>
                  <input class="form-control" id="ltehsil" name="ltehsil" type="text" readonly />
                 </div>


                
               </div>
              <div class="col-md-4 col-sm-4">
               

                 <div class="form-group ">
                  <label class="control-label " for="name2">
                   IT-GK Name
                  </label>
                  <input class="form-control" id="itgkname" name="itgkname" type="text" readonly />
                 </div>

                 <div class="form-group ">
                  <label class="control-label " for="ldob">
                   DOB
                  </label>
                  <input class="form-control" id="ldob" name="ldob" type="text" readonly />
                 </div>

   
                 <div class="form-group ">
                  <label class="control-label " for="laddress">
                   Address
                  </label>
                  <textarea class="form-control" cols="40" id="laddress" name="laddress" readonly  rows="5"></textarea>
                 </div>


               </div>
              <div class="col-md-4 col-sm-4">
               

                 <div class="form-group ">
                  <label class="control-label " for="name3">
                   Applicant Name
                  </label>
                  <input class="form-control" id="lname" name="lname" type="text" readonly />
                 </div>

                 <div class="form-group ">
                  <label class="control-label " for="lmobile">
                   Mobile No
                  </label>
                  <input class="form-control" id="lmobile" name="lmobile" type="text" readonly />
                 </div>

                 <div class="form-group ">
                  <label class="control-label " for="ldistrict">
                   District
                  </label>
                  <input class="form-control" id="ldistrict" name="ldistrict" type="text" readonly />
                 </div>

                 <div class="form-group ">
                  <label class="control-label " for="lpin">
                   PIN Code
                  </label>
                  <input class="form-control" id="lpin" name="lpin" type="text" readonly />
                 </div>

                
               </div>
               <div class="col-md-12">
                <div class="panel panel-primary" style="margin-top:5px;">
                    <div class="panel-heading">Educational Qualification
                    </div>
                <div class="panel-body">
                     <div class="row">
                        <div class="col-md-3 form-group ">
                          <label class="control-label " for="name3">
                           Select Acadmic Qualification
                          </label>
                          <select id="acaqua" name="acaqua">
                              <option value="">Select</option>
                              <option value="ten">10th Board</option>
                              <option value="twe">12th Board</option>

                          </select>
                         </div>
                     </div>
                <div class="row" id="tendetail" style="display: none;">
                 <div class="col-md-3 form-group ">
                  <label class="control-label " for="name3">
                   Eligibility  
                  </label>
                  <input class="form-control" id="eligibility" name="eligibility" type="text" readonly  value="10th Board (Apprearing)" />
                 </div>

                 <div class="col-md-3 form-group ">
                  <label class="control-label " for="lmobile">
                   10th Board Name
                  </label>
                  <input class="form-control" id="tenboardname" name="tenboardname" type="text" />
                 </div>

                 <div class="col-md-3 form-group ">
                  <label class="control-label " for="ldistrict">
                   10th Board Rollno
                  </label>
                  <input class="form-control" id="tenboardroll" name="tenboardroll" type="text" />
                 </div>

                 <div class="col-md-3 form-group ">
                  <label class="control-label " for="lpin">
                   Examination Year
                  </label>
                  <input class="form-control" id="examyear" name="examyear" type="text" readonly  value="2020" />
                 </div>
                </div>
                <div class="row" id="twedetail" style="display: none;">
                 <div class="col-md-3 form-group ">
                  <label class="control-label " for="name3">
                   Eligibility
                  </label>
                  <input class="form-control" id="eligibility" name="eligibility" type="text" readonly  value="12th Board (Apprearing)"/>
                 </div>

                 <div class="col-md-3 form-group ">
                  <label class="control-label " for="lmobile">
                   12th Board Name
                  </label>
                  <input class="form-control" id="tweboardname" name="tweboardname" type="text" />
                 </div>

                 <div class="col-md-3 form-group ">
                  <label class="control-label " for="ldistrict">
                   12th Board Rollno
                  </label>
                  <input class="form-control" id="tweboardroll" name="tweboardroll" type="text" />
                 </div>

                 <div class="col-md-3 form-group ">
                  <label class="control-label " for="lpin">
                   Examination Year
                  </label>
                  <input class="form-control" id="tweexamyear" name="tweexamyear" type="text" readonly value="2020" />
                 </div>
                </div>
                </div>
              </div>
               </div>
                <div class="col-md-12">
                <div class="panel panel-primary" style="margin-top:5px;">
                    <div class="panel-heading">Compulsary Document Upload
                    </div>
                <div class="panel-body">
                <div class="row">
                 <div class="col-md-6 form-group ">
                  <label class="control-label " for="name3">
                   2020 Board Exam Admit Card (10th/12th whichever is applicable)  
                  </label>
                  <input style=" width: 115%" id="admitcard" type="file" name="admitcard" onchange="checkadmitcard(this);" /> 
                    <span style="font-size:10px;">Note: .pdf Allowed Size = 100 to 200 KB</span>
                 </div>
                </div>
                </div>
              </div>
               </div>
               <div class="col-md-12">
                <div class="panel panel-primary" style="margin-top:5px;">
                    <div class="panel-heading">Rules and Regulations
                    </div>
                <div class="panel-body">
                <div class="row">
                 <div class="col-md-12 form-group ">
                    <p>                      
1)  यह योजना राजस्थान बोर्ड,सीबीएसई तथा अन्य बोर्ड परीक्षा-2020 में भाग लेने वाले(10th &12th ) विद्यार्थियों के लिए है | </p><p>
2)  1001 चयनित भाग्यशाली आवेदकों को 100%* तक (25%  से 100% तक) कोर्स फीस का वहन RKCL द्वारा किया जायेगा| </p><p>
3)  चयनित विद्यार्थी को फीस की शेष राशि ज्ञान केंद्र पर जमा कराना होगा उसके पश्चात ही प्रवेश कन्फ़र्म माना जाएगा | </p> <p>
4)  एक विद्यार्थी एक ही बार आवेदन कर सकता है एवं सिर्फ एक ही ज्ञान केंद्र पर योजना का लाभ प्राप्त कर सकता है | </p><p>
5)  किसी भी विवादास्पद स्थिति में RKCL द्वारा लिया गया निर्णय अंतिम माना जायेगा |

                    </p>
                 </div>
                </div>
                </div>
              </div>
               </div>
              <div class="col-md-12">
                <div class="panel panel-primary" style="margin-top:5px;">
                    <div class="panel-heading">Declaration Applicant
                    </div>
                <div class="panel-body">
                <div class="row">
                 <div class="col-md-12 form-group ">
                    <p>                      
मै घोषणा करता/करती हूँ कि:</p><p>इस आवेदन में मेरे द्वारा दी गई सम्पूर्ण जानकारी पूर्ण रूप से सत्य है | मै यह समझता हूँ कि दी गई जानकारी में किसी भी तरह की त्रुटी पाए जाने पर योजना हेतु मेरी पात्रता को निरस्त किया जा सकता है | </p>

                 </div>
                  <div class="col-md-6 form-group ">
                  <label class="control-label " for="lpin">
                   Place
                  </label>
                  <input class="form-control" id="lplace" name="lplace" type="text" required="required" oninvalid="setCustomValidity('Please enter place')"
                                                onchange="try{setCustomValidity('')}catch(e){}"/>
                 </div>
                  <div class="col-md-6 form-group ">
                  <label class="control-label " for="lpin">
                   Applicant Signature
                  </label>
                  
                    <input style=" width: 115%" id="lsign" type="file" name="lsign" onchange="checklsign(this);" required="required" /> 
                    <span style="font-size:10px;">Note: .jpg Allowed Size = 2 to 5 KB</span>
                 </div>
                    <div class="col-md-6 form-group ">
                  <label class="control-label " for="lpin">
                   Date
                  </label>
                  <input type="text" class="form-control" readonly="true" maxlength="50" name="lsigndate" id="lsigndate"  placeholder="DD-MM-YYYY" required="required" >
                 </div>
                </div>
                </div>
              </div>
               </div>
                 <div class="col-md-12">
                <div class="panel panel-primary" style="margin-top:5px;">
                    <div class="panel-heading">Declaration IT-GK
                    </div>
                <div class="panel-body">
                <div class="row">
                 <div class="col-md-12 form-group ">
                    <p>मै घोषणा करता/करती हूँ कि:</p><p>इस आवेदन में भरे गये विवरण को मैंने सत्यापित कर लिया है| किसी भी प्रकार के त्रुटी पर आवेदन निरस्त किया जा सकता है|</p>

                 </div>
                  <div class="col-md-6 form-group ">
                  <label class="control-label " for="lpin">
                   Place
                  </label>
                  <input class="form-control" id="itgkplace" name="itgkplace" type="text" required="required" oninvalid="setCustomValidity('Please Enter Place')"
                                                onchange="try{setCustomValidity('')}catch(e){}"/>
                 </div>
                  <div class="col-md-6 form-group ">
                  <label class="control-label " for="lpin">
                   IT-GK Signature
                  </label>

                  <input style=" width: 115%" id="itgksign" type="file" name="itgksign" onchange="checkitgksign(this);" required="required" /> 
                    <span style="font-size:10px;">Note: .jpg Allowed Size = 2 to 5 KB</span>
                 </div>
                    <div class="col-md-6 form-group ">
                  <label class="control-label " for="lpin">
                   Date
                  </label>
                  <input type="text" class="form-control" readonly="true" maxlength="50" name="itgksigndate" id="itgksigndate"  placeholder="DD-MM-YYYY" required="required">
             
                 </div>
                </div>
                </div>
              </div>
               </div>
                <div class="form-group">
                  <div>
                    <input type="hidden" id="itgkdistrict" name="itgkdistrict">
                    <input type="hidden" id="itgktehsil" name="itgktehsil">
                    <input type="hidden" id="action" name="action" value="UpdLearner">
                   <button class="btn btn-primary " name="submit" type="submit">
                    Submit
                   </button>
                  </div>
                 </div>
              </div>
              </form>
             </div>
            </div>

        </div>
        </div>
</div>
</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<script type="text/javascript">
    $('#itgksigndate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "top auto",
        todayHighlight: true
    });
</script>
<script type="text/javascript">
    $('#lsigndate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "top auto",
        todayHighlight: true
    });
</script>

<script language="javascript" type="text/javascript">
    function checkitgksign(target) {
        var ext = $('#itgksign').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("itgksign").value = '';
            return false;
        }

        if (target.files[0].size > 5000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 5 KB");
            document.getElementById("itgksign").value = '';
            return false;
        } else if (target.files[0].size < 2000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("itgksign").value = '';
            return false;
        }
        document.getElementById("itgksign").innerHTML = "";
        return true;
    }
    function checklsign(target) {
        var ext = $('#lsign').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("lsign").value = '';
            return false;
        }

        if (target.files[0].size > 5000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 5 KB");
            document.getElementById("lsign").value = '';
            return false;
        } else if (target.files[0].size < 2000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("lsign").value = '';
            return false;
        }
        document.getElementById("lsign").innerHTML = "";
        return true;
    }
    function checkadmitcard(target) {
        var ext = $('#admitcard').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Document must be in PDF Format');
            document.getElementById("admitcard").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Document size should less or equal 200 KB");
            document.getElementById("admitcard").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Document size should be greater than 100 KB");
            document.getElementById("admitcard").value = '';
            return false;
        }
        document.getElementById("admitcard").innerHTML = "";
        return true;
    }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        $('#acaqua').change(function () {
            var acaquas = $(this).val();
            if (acaquas == "ten") {
                
                $("#tendetail").css("display", "block");
                $("#twedetail").css("display", "none");

            } else if(acaquas == "twe") {
                $("#twedetail").css("display", "block");
                $("#tendetail").css("display", "none");
            }
            else{
                 $("#tendetail").css("display", "none");
                 $("#twedetail").css("display", "none");
            }

            
        });
        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfApplyLuckyDrawApplication.php",
                data: "action=FILLCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();


        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfApplyLuckyDrawApplication.php",
                data: "action=FILLBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });




        function showAllData(val, val1) {
            //alert(val);
            //alert(val1);
            $.ajax({
                type: "post",
                url: "common/cfApplyLuckyDrawApplication.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#menuList").html(data);
                    $('#example').DataTable();
                }
            });
        }

        $("#ddlBatch").change(function () {
            showAllData(this.value, ddlCourse.value);
        });

        $("#menuList").on('click', '.approvalLetter', function () {
                    var course = $(this).attr('course');
                    var batch = $(this).attr('batch');
                    var lcode = $(this).attr('lcode');
                    var mode = $(this).attr('mode');

                    
                  
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
              
            var url = "common/cfApplyLuckyDrawApplication.php"; // the script where you handle the form input.
            var data;
            
            data = "action=GETLEARNERDATA&course=" + course + "&batch=" + batch + "&lcode=" + lcode + "&mode=" + mode + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {

                   
                    $('#response').empty();
                    data = $.parseJSON(data);
                    acode.value = data[0].AdmissionCode;
                    lcodes.value = data[0].lcode;
                    lname.value = data[0].lname;
                    fname.value = data[0].fname;
                    ldob.value = data[0].dob;
                    itgkcode.value = data[0].itgkcode;
                    itgkname.value = data[0].itgkname;
                    ldistrict.value = data[0].district;
                    ltehsil.value = data[0].tehsil;
                    laddress.value = data[0].address;
                    itgkdistrict.value = data[0].itgkdistrict;
                    itgktehsil.value = data[0].itgktehsil;
                    lpin.value = data[0].pin;
                    lmobile.value = data[0].mobile;
                    lemail.value = data[0].email;
                    // txtsign.value = data[0].photo;
                    // txtsign.value = data[0].sign;
                    var modal = document.getElementById('myModal');
                    var span = document.getElementsByClassName("close")[0];
                    modal.style.display = "block";
                    span.onclick = function() {
                       
                        modal.style.display = "none";                       
                    }
                   

                }
            });
        });

             
        $("#frmApplyLuckyDrawApplicationModal").on('submit',(function(e) {
               
            e.preventDefault();
            var data = new FormData(this);
            // data.append('tenboardname', tenboardname);
            // data.append('tenboardroll', tenboardroll);
            // data.append('tweboardname', tweboardname);
            // data.append('tweboardroll', tweboardroll);
            // data.append('lplace', lplace);
            // data.append('itgkplace', itgkplace);
                $.ajax({
                    url: "common/cfApplyLuckyDrawApplication.php",
                    type: "POST",
                    data:  data,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate || data == 'Successfully Inserted  ' || data == 'Successfully Updated  ')
                        {
                            $('#response2').empty();
                            $('#response2').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmApplyLuckyDrawApplication.php';
                            }, 3000);
                        } else
                        {
                            $('#response2').empty();
                            $('#response2').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>"+ data +"</span></div>");
                        }
                    }
                      });
                return false;
        }));


        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>