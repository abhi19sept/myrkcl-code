<?php
$title="OTP Details For Learner";
include ('header.php'); 
include ('root_menu.php');
 

if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '9') {	
?>
    <div style="min-height:430px !important;max-height:1500px !important;">
          <div class="container"> 
			  
			
            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Display Otp Details For Learner</div>
                <div class="panel-body">
				<div class="container">
							<div id="response"></div>
						</div>        
							<div id="errorBox"></div>
	<div id="gird"> </div>
	</div>
   </div>
 </div>
  </div>
 <script>
		$(document).ready(function() {
     $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
	
} );
	</script>



</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<style>
  .modal-dialog {width:700px;}
.thumbnail {margin-bottom:6px; width:710px;}
  </style>
<script type="text/javascript">
  
  $(document).ready(function() {
		jQuery(".fix").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
    
			

			   function showData() 
			   {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfgetlearnerotp.php",
                    data: "action=GETOTP",
                    success: function (data) {
						$('#response').empty();
                        $("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						

                    }
                });
            }
			showData();
		
        
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>
