<?php
    $title = "Organization Details";
    include('header.php');
    include('root_menu.php');
    include 'common/modals.php';

    if (!in_array($_SESSION["User_UserRoll"],[11,4])) {
        echo "<script>$('#unauthorized').modal('show')</script>";
        die;
    }

    echo "<script>var OrgCode = '" . $_SESSION['User_Code'] . "'; </script>";
?>

<style type="text/css">

    .asterisk {
        color: red;
        font-weight: bolder;
        font-size: 18px;
        vertical-align: middle;
    }

    .division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 20px;
        color: #000;
        margin-bottom: 20px;

    }

    .extra-footer-class {
        margin-top: 0;
        margin-bottom: -10px;
        padding: 16px;
        background-color: #fafafa;
        border-top: 1px solid #e5e5e5;
    }

    .ref_id {
        text-align: center;
        font-size: 20px;
        color: #000;
        margin: 0 0 10px 0;
    }

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eeeeee;
        box-shadow: inset 0 0 5px 1px #d5d5d5;
    }

    .form-control {
        border-radius: 2px;
    }

    input[type=text]:hover, textarea:hover {
        box-shadow: 0 1px 3px #aaa;
        -webkit-box-shadow: 0 1px 3px #aaa;
        -moz-box-shadow: 0 1px 3px #aaa;
    }

    .col-sm-3:hover {
        background: none !important;
    }

    .modal-open .container-fluid, .modal-open .container {
        -webkit-filter: blur(5px) grayscale(50%);
        filter: blur(5px) grayscale(50%);
    }

    .btn-success {
        background-color: #00A65A !important;
    }

    .btn-success:hover {
        color: #fff !important;
        background-color: #04884D !important;
        border-color: #398439 !important;
    }

    .addBottom {
        margin-bottom: 12px;
    }

    #image_address {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
        cursor: pointer;
    }

    #image_address:hover {
        opacity: 1;
    }

    .middle:hover {
        opacity: 1;
    }

</style>

<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>

<div class="container" id="showdata">
    <div class="panel panel-primary" style="margin-top:46px !important;">

        <div class="panel-heading">Name Update Request Made by Center : <span id="fld_ITGK_Code"></span></div>
        <div class="panel-body">

            <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateOrgDetails" name="updateOrgDetails">

                <div class="ref_id">
                    Reference ID: <span id="ref_id"></span>
                </div>

                <div class="row">
                    <div class="col-md-6"><h3 style="text-align: center">Previous</h3></div>
                    <div class="col-md-6"><h3 style="text-align: center">Requested</h3></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="division_heading">
                            Center Details
                        </div>


                        <div class="box-body" style="margin: 0 100px;">

                            <div class="form-group">

                                <div class="col-sm-12 addBottom">
                                    Organization Name
                                    <input type="text" class="form-control" name="Organization_Name_old" id="Organization_Name_old" placeholder="Name of the Organization/Center"
                                           readonly='readonly'>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="division_heading">
                            &nbsp;
                        </div>


                        <div class="box-body" style="margin: 0 100px;">

                            <div class="form-group">

                                <div class="col-sm-12 addBottom">
                                    Organization Name
                                    <input type="text" class="form-control" name="Organization_Name" id="Organization_Name" placeholder="Name of the Organization/Center"
                                           readonly='readonly'>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="division_heading">
                            Registration Document
                        </div>


                        <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#doc"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Document</button>
                        <div class="box-body collapse" style="margin: 0 100px;" id="doc">

                            <div class="form-group">
                                <div class="col-sm-12"><br><br>
                                    <iframe id="image_address" style="width: 100%; height: 500px; border: none;"></iframe>
                                    <div id="image_address_ftp"></div>
                                    <div class="middle">
                                        <div class="text">&nbsp;</div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <p>&nbsp;</p>
				
				<div class="row">
                    <div class="col-md-12">
                        <div class="division_heading">
                            Bank Account Details
                        </div>


                        <button type="button" class="btn btn-primary" id="bankdetail" name="bankdetail"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Bank Account Details</button>
                             <div class="panel panel-info" id="bankaccount" style="display:none">
									<div class="panel-heading">Bank Account Details</div>
									<div class="panel-body">
										<div id="bankdatagird"></div>

									</div>
								</div>
                    </div>
                </div>

                <p>&nbsp;</p>

                <div class="row">
                    <div class="col-md-12">
                        <div class="division_heading">
                            Remarks
                        </div>


                        <div class="box-body" style="margin: 0 100px;display: none;" id="sp_remarks">

                            <div class="form-group">
                                <div class="col-sm-12"><span id="remark_label">Please enter the remarks for action you are taking</span> <span class="asterisk">*</span>
                                    <textarea id="fld_remarks" name="fld_remarks" class="form-control" rows="4" cols="50" style="border-radius: 8px; font-family: Calibri;"></textarea>
                                </div>
                            </div>

                        </div>

                        <div class="box-body" style="margin: 0 100px;display: none;" id="rkcl_remarks">

                            <div class="form-group">
                                <div class="col-sm-12"><span id="remark_label_rkcl">Please enter the remarks for action you are taking</span> <span class="asterisk">*</span>
                                    <textarea id="fld_remarks_rkcl" name="fld_remarks_rkcl" class="form-control" rows="4" cols="50" style="border-radius: 8px; font-family: Calibri;
" onkeypress="javascript:return validAddress2(event);"></textarea>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="row" id="current_status" style="display: none;">
                    <div class="col-md-12">
                        <div class="division_heading">
                            Status Log
                        </div>

                        <div class="box-body" style="margin: 0 100px; display: none;" id="pending_submission">
                            <div class="form-group">
                                <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                    Request haven't submitted yet
                                </div>
                            </div>
                        </div>


                        <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_approval_sp">
                            <div class="form-group">
                                <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                    Request Submitted on <span id="submission_date"></span>
                                </div>
                            </div>
                        </div>

                        <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_approval_rkcl">
                            <div class="form-group">
                                <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                    Request Approved by Service Provider on <span id="submission_date_sp"></span>
                                </div>
                            </div>
                        </div>

                        <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_payment">
                            <div class="form-group">
                                <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                    Request Approved by RKCL on <span id="submission_date_rkcl"></span>
                                </div>
                            </div>
                        </div>

                        <div class="box-body" style="margin: 0 100px; display: none;" id="rkcl_rejected">
                            <div class="form-group">
                                <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                    Request has been Rejected by RKCL on <span id="rejected_date_rkcl"></span>
                                </div>
                            </div>
                        </div>

                        <div class="box-body" style="margin: 0 100px; display: none;" id="itgk_rejected">
                            <div class="form-group">
                                <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                    Request has been Rejected by ITGK on <span id="rejected_date_itgk"></span>
                                </div>
                            </div>
                        </div>

                        <div class="box-body" style="margin: 0 100px; display: none;" id="paid">
                            <div class="form-group">
                                <div class="col-sm-4 text-success">
                                    Paid on <span id="payment_date"></span>
                                </div>

                                <div class="col-sm-4 pull-right">
                                    <button type="button" class="btn btn-lg btn-primary" onclick="viewTransactionDetail('<?php echo $_REQUEST['Code'] ?>')">Click here for transaction
                                        details&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


                <!-- /.box-body -->
                <div class="box-footer extra-footer-class col-md-12" id="updateActions" style="display: none;">
                    <button type="button" class="btn btn-lg btn-primary" onclick="window.location.href='frmNameUpdateRequestCenterDetail.php'"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back
                    </button>
                    <button type="button" id="commit" class="btn btn-lg btn-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Accept</button>
                    <button type="button" id="reject" class="btn btn-lg btn-danger pull-right"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;Reject</button>
                </div>
                <!-- /.box-footer -->
        </div>


        </form>

    </div>
</div>
</div>


</body>
<?php
    include 'common/message.php';
    include 'footer.php';
?>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script type="text/javascript">

    function validAddress2(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("^[\\w.# ,-/]+$");

        if (key === 8 || key === 0 || key === 32) {
            keychar = "a";
        }
        return reg.test(keychar);
    }
	
	function showBankAccountData() {
		var CenterCode=$('#fld_ITGK_Code').text();		
            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=BANKACCOUNT&centercode=" + CenterCode + "",
                success: function (data) {

                    $("#bankdatagird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }
		
		$("#bankdetail").click(function () {			
            showBankAccountData();
            $("#bankaccount").show(3000);
        });
		

    var image;
    var usercode;

    function viewTransactionDetail(Code) {
        window.location.href = "viewTransactionDetails.php?Code=" + Code;
    }

    $("#image_address").on("click", function () {

        $("#download_document").attr("onclick", "downloadFile('" + image + "','" + usercode + "')");
        $("#view_document").modal('show');
    });

    function downloadFile(img, usc) {
        window.location.href = 'download_document.php?id=' + img + '&usercode=' + usc + "&flag=identity_proof";
    }

    $("#ref_id").html(decryptString('<?php echo $_REQUEST['Code']?>'));

    function fillForm() {

        $("#revoke_success").modal("show");

        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=FillUpdateAddressDetails&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "",
            success: function (data) {
                data = $.parseJSON(data);

                setTimeout(function () {

                    image = data[0].fld_document_encoded;
                    usercode = data[0].Organization_User_Code_encoded;
                    Organization_User_Code = data[0].Organization_User_Code;

                    $("#Organization_Name_old").val(data[0].Organization_Name_old);
                    $("#Organization_Name").val(data[0].Organization_Name);
                    $("#fld_ITGK_Code").html(data[0].Organization_ITGK_Code);
 $("#image_address").hide();
$("#image_address_ftp").html(data[0].fld_document);

                    //$("#image_address").attr("src", "upload/identity_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document+"#zoom=95");
                    $("#view_image_large").attr("src", "upload/identity_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document);

                }, 3000);

                setTimeout(function () {
                    $("#revoke_success").modal("hide");
                }, 3000);

                $("#submission_date").html(data[0].submission_date);
                $("#submission_date_sp").html(data[0].submission_date_sp);
                $("#submission_date_rkcl").html(data[0].submission_date_rkcl);

                $("#fld_remarks").val(data[0].fld_remarks);
                $("#fld_remarks_rkcl").val(data[0].fld_remarks_rkcl);

                $("#current_status").css("display", "block");

                /*** REQUEST NOT SUBMITTED ***/
                if (data[0].fld_status === "0") {
                    $("#current_status").css("display", "block");
                    $("#pending_submission").css("display", "block");
                }

                /*** REQUEST SUBMITTED, PENDING FOR APPROVAL BY SERVICE PROVIDER ***/
                if (data[0].fld_status === "1") {
                    //$("#updateActions").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                }

                /*** APPROVED BY SERVICE PROVIDER, PENDING FOR APPROVAL BY RKCL ***/
                if (data[0].fld_status === "2") {
                    $("#remark_label").html('Entered Remarks by Service Provider');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#updateActions").css("display", "block");
                    $("#rkcl_remarks, #sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#pending_for_approval_rkcl").css("display", "block");
                }

                /*** APPROVED BY RKCL, PENDING FOR PAYMENT ***/
                if (data[0].fld_status === "3") {
                    $("#remark_label").html('Entered Remarks by Service Provider');
                    $("#remark_label_rkcl").html('Entered Remarks by You');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks, #sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#pending_for_approval_rkcl").css("display", "block");
                    $("#pending_for_payment").css("display", "block");
                }

                /*** REJECTED BY RKCL ***/
                if (data[0].fld_status === "4") {
                    $("#remark_label").html('Entered Remarks by Service Provider');
                    $("#remark_label_rkcl").html('Entered Remarks by You');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks, #sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#pending_for_approval_rkcl").css("display", "block");
                    $("#rkcl_rejected").css("display", "block");
                    $("#rejected_date_rkcl").html(data[0].fld_updatedOn);
                }

                /*** PAID BY ITGK ***/
                if (data[0].fld_status === '5') {
                    $("#remark_label").html('Entered Remarks by Service Provider');
                    $("#remark_label_rkcl").html('Entered Remarks by You');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks, #sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#updateActions").css("display", "none");

                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code']?>'),
                        success: function (data2) {
                            data2 = $.parseJSON(data2);
                            $("#payment_date").html(data2[0].fld_updatedOn);
                        }
                    });

                    $("#pending_for_approval_sp").css("display", "block");  //SUBMITTED
                    $("#pending_for_approval_rkcl").css("display", "block"); //APPROVED BY SP
                    $("#pending_for_payment").css("display", "block"); // APPROVED BY RKCL
                    $("#paid").css("display", "block");
                }

                /*** REJECTED BY ITGK ***/
                if (data[0].fld_status === "6") {
                    $("#remark_label").html('Entered Remarks by Service Provider');
                    $("#remark_label_rkcl").html('Entered Remarks by You');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks, #sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#itgk_rejected").css("display", "block");
                    $("#rejected_date_itgk").html(data[0].fld_updatedOn);
                }
            }
        });
    }

    fillForm();

    $("#commit").on("click", function () {

        var $validator = $("#updateOrgDetails").validate();
        var errors;
        var pattern = new RegExp("^[\\w.# ,-/]+$");
        var fld_remarks_rkcl = $("#fld_remarks_rkcl").val();

        if (!fld_remarks_rkcl) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
            $validator.showErrors(errors);
        } else if (!pattern.test(fld_remarks_rkcl)) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">The value seems to be invalid</span>"};
            $validator.showErrors(errors);
        } else {
            $("#submitting").modal("show");
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=commit_rkcl&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "&fld_remarks_rkcl=" + fld_remarks_rkcl + "&fld_ref_id=" + decryptString('<?php echo $_REQUEST['Code']?>') + "&requestType=name",
                success: function (data) {
                    if (data.search("Successfully Inserted") >= 0 || data.search("Successfully Updated") >= 0) {
                        setTimeout(function () {
                            $("#submitting").modal("hide");
                            $("#submitted_thanks").modal("show")
                        }, 3000);
                    }
                }
            })
            ;
        }

    });

    $("#reject").on("click", function () {

        var $validator = $("#updateOrgDetails").validate();
        var errors;
        var pattern = new RegExp("^[\\w.# ,-/]+$");
        var fld_remarks_rkcl = $("#fld_remarks_rkcl").val();

        if (!fld_remarks_rkcl) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
            $validator.showErrors(errors);
        } else if (!pattern.test(fld_remarks_rkcl)) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">The value seems to be invalid</span>"};
            $validator.showErrors(errors);
        } else {
            $("#submitting").modal("show");
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=reject&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "&fld_remarks_rkcl=" + fld_remarks_rkcl + "&requestType=name",
                success: function (data) {
                    if (data.search("Successfully Inserted") >= 0 || data.search("Successfully Updated") >= 0) {
                        setTimeout(function () {
                            $("#submitting").modal("hide");
                            $("#submitted_rejected").modal("show")
                        }, 3000);
                    }
                }
            });
        }


    });

    $("#revoke").on("click", function () {

        var $validator = $("#updateOrgDetails").validate();
        var errors;
        var pattern = new RegExp("^[\\w.# ,-/]+$");
        var fld_remarks_rkcl = $("#fld_remarks_rkcl").val();

        if (!fld_remarks_rkcl) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
            $validator.showErrors(errors);
        } else if (!pattern.test(fld_remarks_rkcl)) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">The value seems to be invalid</span>"};
            $validator.showErrors(errors);
        } else {
            $("#submitting").modal("show");
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=revoke&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "&requestType=name",
                success: function (data) {
                    if (data.search("Sucscessfully Deleted") >= 0) {
                        setTimeout(function () {
                            $("#submitting").modal("hide");
                            $("#submitted_revoked").modal("show")
                        }, 3000);
                    }
                }
            });
        }
    });
</script>
<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
</html>