<?php
$title="Bank Account Details For Share Disbursement";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var BankAccountCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var BankAccountCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '15') {	
?>
	
<div style="min-height:430px !important;max-height:auto !important;">
	<div class="container"> 
		
        <div class="panel panel-primary" style="margin-top:36px !important;">  
			<div class="panel-heading">Bank Account Details </div>
			<div class="panel-body">
				<form name="frmBankAccount" id="frmBankAccount" class="form-inline" role="form"  >
					<div class="container">
						<div class="container">
							<div id="response"></div>
						</div>        
							<div id="errorBox"></div>				
							  <div class="col-sm-4 form-group" id='ifsc'>     
									<label for="order">IFSC Code:<font color="red">*</font></label>
									<input type="text" class="form-control" maxlength="11" name="txtIfscCode" id="txtIfscCode" placeholder="IFSC Code"  onkeypress="javascript:return validAddress(event);">
								</div>  
								
								
								<div class="col-sm-4 form-group">     
								
								<input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Validate IFSC Code" style="margin-top:25px"/>    
								
								
								</div> 
								
							</div>
							<div id="details" style="display:none">
							
							
							
							
							
							
							
							<div class="container">
							
							
							
							<div class="col-sm-4 form-group">     
									<label for="dob">Account Holder Name:<font color="red">*</font></label>
									<input type="hidden" class="form-control" maxlength="100" name="txtGenerateId" id="txtGenerateId"/>	
									<input type="text" class="form-control" maxlength="100" name="txtaccountName" id="txtaccountName"  placeholder="Account Name" onkeypress="javascript:return allowchar(event);" style="text-transform:uppercase"/>
								</div>
								
								<div class="col-sm-4 form-group">     
									<label for="dob">Account Number:<font color="red">*</font></label>
									
									<input type="text" class="form-control" maxlength="20" name="txtaccountNumber" id="txtaccountNumber"  placeholder="Account Number" onpaste="return false;" onkeypress="javascript:return allownumbers(event);"/>
								</div>
								
								
								<div class="col-sm-4 form-group" >     
									<label for="dob">Confirm Account Number:<font color="red" >*</font></label>
									
									<input type="text" class="form-control" maxlength="20" name="txtConfirmaccountNumber" id="txtConfirmaccountNumber" onpaste="return false;"  placeholder="Account Number"  onkeypress="javascript:return allownumbers(event);"/>
									<span id='message'></span>
								</div>
								
								
								
								<div class="col-sm-5 form-group"> 
									<label for="status">Bank Name:<font color="red">*</font></label>
									
									<input type="text" class="form-control" maxlength="500" name="ddlBankName" id="ddlBankName" placeholder="Bank Name" onpaste="return false;"  onkeypress="javascript:return validAddress(event);">
								</div>                            
							
							</div>
								
								
							
							<div class="container">
							   <div class="col-sm-4 form-group"> 
									<label for="status">Branch Name:<font color="red">*</font></label>
									<input type="text" class="form-control" maxlength="500" name="ddlBranch" id="ddlBranch" placeholder="Branch Name" onpaste="return false;"  onkeypress="javascript:return validAddress(event);">
								</div>   
							
								<div class="col-sm-4 form-group">     
									<label for="order">MICR Code:<font color="red">*</font></label>
									<input type="text" class="form-control" maxlength="14" name="txtMicrcode" id="txtMicrcode" placeholder="MICR Code" onpaste="return false;"  onkeypress="javascript:return allownumbers(event);">
								</div>  
								
								<div class="col-sm-4 form-group">     
									<label for="gender">Account Type:<font color="red">*</font></label> <br/>                               
									<label class="radio-inline"> <input type="radio" id="rbtaccountType_saving" name="rbtaccountType_saving" checked="checked" value="Savings"/> Savings </label>
									<label class="radio-inline"> <input type="radio" id="rbtaccountType_saving" name="rbtaccountType_saving" value="Current"/> Current </label>									
								</div>
								
								<div class="col-sm-4 form-group"> 
									<label for="status">Pan Card No.:<font color="red">*</font></label>
									<input type="text" class="form-control" maxlength="11" name="txtpanno" id="txtpanno"
										placeholder="Pan Card No." onpaste="return false;" onkeypress="javascript:return validAddress(event);" style="text-transform:uppercase">
								</div>  

							</div>
							
							<div class="container">
								
								<div class="col-sm-4 form-group"> 
									<label for="status">Pan Card Holder Name:<font color="red">*</font></label>
									<input type="text" class="form-control" maxlength="100" name="txtpanname" id="txtpanname"
										placeholder="Pan Card Holder Name" onkeypress="javascript:return allowchar(event);" style="text-transform:uppercase">
								</div>  
								
								<div class="col-sm-4 form-group"> 
								  <label for="photo">Attach Pan Card Copy:<span class="star">*</span></label>
								  <input type="file" class="form-control" id="pancard" name="pancard"  onchange="pancard(this)" >
								   <span style="font-size:10px;">Note : JPG,PNG Allowed Max Size =200KB</span>
								   <img src="images/Pencard.jpg" width="50" />
								</div> 

								<div class="col-sm-4 form-group"> 
									<label for="edistrict">Select Bank Account ID Proof:<span class="star">*</span></label>
									<select id="ddlidproof" name="ddlidproof" class="form-control">
									    <option value=""> Please Select </option>
										<option value="cheque"> Cancelled Cheque Copy </option>
										<option value="welcome"> Welcome Letter of Bank </option>
										<option value="passbook"> Passbook Copy </option>
									</select>    
								</div>	
							  
							  <div class="col-sm-4 form-group"> 
								  <label for="photo">Attach Bank Id Proof:<span class="star">*</span></label>
								  <input type="file" class="form-control" id="chequeImage" name="chequeImage"  onchange="BankIdProof(this)"  >
								   <span style="font-size:10px;">Note : JPG,PNG Allowed Max Size =200KB</span>
								   <img src="images/sbi.jpg" width="50" />
								</div>   
     
     
</div>
							
							<div class="panel panel-info" id="otpverify" style="display:none;">
								<div class="panel-heading">Verify Authentication Details</div>
								<div class="panel-body">
									<div class="container">
										

										<div class="col-sm-4 form-group">     
											<label for="otp">Enter OTP:</label>
											<input type="text" class="form-control" maxlength="6" name="txtOtp" id="txtOtp"  placeholder="Enter OTP">
										</div>
										
										


										<div class="col-sm-3 form-group">     
											<input type="button" name="btnVerify" id="btnVerify" class="btn btn-primary" value="Verify" style="margin-top:25px"/>
											
											
											<!--<input type="button" name="btnVerified" id="btnVerified" class="btn btn-primary" value="Details Verified" style="margin-top:25px; display:none"/>-->
											<label for="tnc" id="btnVerified" style="margin-top:25px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Details Verified</b></label></br>
										</div>
										<div class="col-sm-4 form-group">     
											
											<input type="text" class="form-control"  name="txtmobile" id="txtmobile"  readonly="true">
										</div>
									</div>
								</div>
							</div>
							
							
							
							
							
							<div class="container"> 
								
							<div class="col-md-11"> 	
															
								<label for="learnercode" style="float:left;margin:left:15px" ><b>मैं यह स्वीकार करता हूँ कि मैं आई. टी. ज्ञान केंद्र  का Proprietor / Partner / Director / President / Chairman / Secretary / Karta / Trustee हूँ एवं मेरे द्वारा प्रवेशित Bank Account Details मेरी अथवा मेरी Firm / साझेदारी Firm / Company / Society / Trust / HUF / संस्था की ही है |  मैं यह भी स्वीकार करता हूँ कि RKCL अथवा RKCL-Escrow Account द्वारा आई. टी. ज्ञान केंद्र को किसी भी प्रकार का भुगतान उक्त बैंक खाते में किया जायेगा अतः प्रवेशित Bank Account Details में किसी भी तरह की गलती के लिए मैं स्वयं जिमेदार रहूँगा |</b></label></br>                              
								
                            </div>
							
					
						</div>	
						
						<div class="container"> 
								<div class="col-md-11" > 
								<label class="checkbox-inline" style="float:left;"> <span class="star"> </span><input type="checkbox" name="chk" id="chk" value="1" >
									I Accept 
								</label>
								</div>
							<br>
							<br>
							
					
						    </div>	
						
						
						
						
							
							<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
						  <div class="modal-content">
							<div class="modal-header">
								<button class="close" type="button" data-dismiss="modal">×</button>
								<h3 class="modal-title">Help </h3>
							</div>
							<div class="modal-body">
								<img src="images/cheque.jpg" width="620" />
							</div>
							<div class="modal-footer">
								<button class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						   </div>
						  </div>
						</div>
							</div>
								<div class="container" id="sub" style="display:none">
							  <div class="col-md-1" style="width:100px;"> 							
								                              
								<a title="" style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><span id="fix"><input type="button" name="Help" id="Help" class="btn btn-primary" value="Help"  style="margin-top:10px"></span></a>    					
                            </div>
							  
							  <div class="col-md-1" style="width:100px;">
							    <input type="button" name="btnshowotp" id="btnshowotp" class="btn btn-primary" value="Generate OTP"  style="margin-left:15px;margin-top:10px" />
							  <input type="submit" name="validate" id="preview" class="btn btn-primary" value="Confirm" style="margin-left:18px;margin-top:10px;display:none;"/> 
							  </div>
                               
                        </div>
									<div id="gird"></div>
										<div class="container" id='prev' style="display:none">
						
						<div class='col-md-11 col-lg-11'>
						 <p class="btn btn-primary" >Preview </p> 
						<table class='table table-user-information'>
						<tbody>
						<tr>
						
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">IFSC Code:</td>
						<td class="col-md-3 col-lg-3" id='ifsccode'></td>
						</tr>
						<tr>
						<td  class="col-md-2 col-lg-2">Account Holder Name:</td>
						<td class="col-md-3 col-lg-3" id='Accname'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Account Number:</td>
						<td class="col-md-3 col-lg-3" id='Accnumber'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Confirm Account Number:</td>
						<td class="col-md-3 col-lg-3" id='ConAccnumber'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Bank Name:</td>
					   <td class="col-md-3 col-lg-3" id='BankName'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Branch Name:</td>
						<td class="col-md-3 col-lg-3" id='BranchName'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">MICR Code:</td>
						<td class="col-md-3 col-lg-3" id='Micrcode'></td>
						</tr>
						
						<tr>
						<td class="col-md-2 col-lg-2">Account Type:</td>
						<td class="col-md-3 col-lg-3" id='Accounttype'></td>
						</tr>
						
						
						<tr>
						<td class="col-md-2 col-lg-2">Pan Card No:</td>
						<td class="col-md-3 col-lg-3" id='Pencard'></td>
						</tr>
						
						
						<tr>
						<td class="col-md-2 col-lg-2">Pan Card Holder Name:</td>
						<td class="col-md-3 col-lg-3" id='Pancardholder'></td>
						</tr>
						
						
						<tr>
						<td class="col-md-2 col-lg-2">Bank Account ID Proof:</td>
						<td class="col-md-3 col-lg-3" id='bankid'></td>
						</tr>
						
						

						 
						</tbody>
					   </table>
					   <input type="button" name="back" id="back" class="btn btn-primary" value="Back"  /> 
					   <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="margin-left:15px"/>
						</div>
						  
						
						</div>
						
						
						
						
						
					</div>
                </div>
            </div>   
        </div>
    </form>
</div>
</body>

<?php include ('footer.php'); ?>
<?php include'common/message.php';?>

<script>
$("#preview").click(function () {
								var accountno = $("#txtaccountNumber").val();
								var confirmaccountno = $("#txtConfirmaccountNumber").val();
								
								if (accountno != confirmaccountno) {
									alert("Account No Does not match.");
									return false;
								}
								
								
								if ($("#frmBankAccount").valid())
								{	
								 
								document.getElementById("ifsccode").innerHTML = $("#txtIfscCode").val();
								document.getElementById("Accname").innerHTML = $("#txtaccountName").val();
								document.getElementById("Accnumber").innerHTML= $("#txtaccountNumber").val();
								document.getElementById("ConAccnumber").innerHTML = $("#txtConfirmaccountNumber").val();
								document.getElementById("BankName").innerHTML = $("#ddlBankName").val();
								document.getElementById("BranchName").innerHTML= $("#ddlBranch").val();
								document.getElementById("Micrcode").innerHTML = $("#txtMicrcode").val();
								document.getElementById("Accounttype").innerHTML = $('input[name=rbtaccountType_saving]:checked').val();
								document.getElementById("Pencard").innerHTML = $("#txtpanno").val();
								document.getElementById("Pancardholder").innerHTML= $("#txtpanname").val();
								document.getElementById("bankid").innerHTML = $("#ddlidproof").val();
								
								
                                            $("#prev").show();
											
											$("#details").hide();
											$("#sub").hide();
											$("#gird").hide();
											$("#ifsc").hide();
											
									        		 

                                  }
                               return false;  
                                });
								
								
								
</script>
<script src="scripts/uploadbankaccount.js"></script>


<style>
  .modal-dialog {width:700px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
<script type="text/javascript">
  
  $(document).ready(function() {
		jQuery(".fix").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>

<script>
$('#txtConfirmaccountNumber').on('keyup', function () {
    if ($(this).val() == $('#txtaccountNumber').val()) {
        $('#message').html('confirmed').css('color', 'green');
    } else $('#message').html('confirm account no. should be same as account no').css('color', 'red');
});
</script>



<script language="javascript" type="text/javascript">


function pancard(target) {
	var ext = $('#pancard').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("chequeImage").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			        
		alert("Image size should less or equal 200 KB");
		document.getElementById("chequeImage").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("chequeImage").value = '';
				return false;
	}    
    document.getElementById("chequeImage").innerHTML = "";
    return true;
}





function BankIdProof(target) {
	var ext = $('#chequeImage').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("chequeImage").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			        
		alert("Image size should less or equal 200 KB");
		document.getElementById("chequeImage").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("chequeImage").value = '';
				return false;
	}    
    document.getElementById("chequeImage").innerHTML = "";
    return true;
}






</script>
	
	<script type="text/javascript">
		var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
		var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
		var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
		var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
		$(document).ready(function () {
			
			
	      
			
			
			if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
			
			function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBankAccount.php",
                    data: "action=EDIT&values=" + BankAccountCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Make']);
                        //alert(data);
                        data = $.parseJSON(data);
                       
                        txtaccountName.value = data[0].Bank_Account_Name;
                        txtaccountNumber.value = data[0].Bank_Account_Number;
                        rbtaccountType_saving.value = data[0].Bank_Account_Type;
                        txtIfscCode.value = data[0].Bank_Ifsc_code;
						ddlBankName.value = data[0].Bank_Name;
						txtMicrcode.value = data[0].Bank_Micr_Code;
						ddlBranch.value = data[0].Bank_Branch_Name;
						txtpanno.value = data[0].Pan_No;
						txtpanname.value = data[0].Pan_Name;
						ddlidproof.value = data[0].Bank_Id_Proof;
						
						
                        
                    }
                });
            }
			
			function FillMobile() 
			{
						//alert();
						$.ajax({
							type: "post",
							url: "common/cfBankAccount.php",
							data: "action=FILLMOBILE",
							success: function (data) {
								//alert(data);
								//$("#txtmobile").html(data);
								txtmobile.value = data;
								
							}
						});
			}
		    FillMobile();
			
			
			
			
			
			
			
			 function deleteRecord()
             {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfBankAccount.php",
                    data: "action=DELETE&values=" + BankAccountCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmbankaccount.php";
                           }, 1000);
                            
                            Mode="Add";
                            resetForm("frmBankAccount");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }
			
			
			
			
			
			
			$("#btnshowotp").click(function () 
			{
							alert("OTP has been sent to your Registered Mobile Number. Please Enter OTP to Proceed");
							$('#response').empty();
							$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
							$.ajax({
								type: "post",
								url: "common/cfBankAccount.php",
								data: "action=SENDOTP",
								success: function (data)
								{
									//alert(data);
									$('#response').empty();
									if (data == SuccessfullyInsert || data == SuccessfullyUpdate) {

										$("#btnshowotp").hide();
									   $("#otpverify").show(3000);
									   //$("#verification").show();

									}
									else {

										$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   OTP already Generated, Please Try again." + "</span></p>");

									}

								}
							});
						});
						
						
						
						
			$("#btnVerify").click(function () {

						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						$.ajax({
							type: "post",
							url: "common/cfBankAccount.php",
							data: "action=VERIFY&otp=" + txtOtp.value + "",
							success: function (data)
							{
								//alert(data);
								if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
								{
									$('#response').empty();
									$("#btnVerify").hide();
									$("#btnVerified").show();
									$("#preview").show();
									 //checkvarify();
									
								   // $('#tnc').show(3000);
								}
								else
								{
									$('#response').empty();
									$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
								}
							}
						});
					});
		
		
		    function FillStatus() {
		        $.ajax({
		            type: "post",
		            url: "common/cfBankAccount.php",
		            data: "action=FILL",
		            success: function (data) {
		                $("#ddlBankName").html(data);
		            }
		        });
		    }
		
		    FillStatus();
			
			
			
			
			function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfBankAccount.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						

                    }
                });
            }

            
			
			
			function GenerateUploadId()
                   {
					$.ajax({
						type: "post",
						url: "common/cfBlockUnblock.php",
						data: "action=GENERATEID",
						success: function (data) {                      
							txtGenerateId.value = data;					
						}
					});
                  }
                  GenerateUploadId();
			
			
			
			function FillBranch() {
		        $.ajax({
		            type: "post",
		            url: "common/cfBankAccount.php",
		            data: "action=FILLBRANCH",
		            success: function (data) {
		                $("#ddlBranch").html(data);
		            }
		        });
		    }
		
		    FillBranch();
										
								
								
								$("#back").click(function () {
								
								
                                            $("#details").show();
											
											$("#prev").hide();
											
											$("#gird").show();
											$("#sub").show();
											$("#ifsc").show();
										
                                });
							
                                
			
			
			
			
			
			                  $("#btnShow").click(function () {
							  if ($("#frmBankAccount").valid())
							  {
								//alert(1);
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                               var url = "common/cfBankAccount.php"; // the script where you handle the form input.
								var data;
								var forminput=$("#frmBankAccount").serialize();
								
									data = "action=FILLDETAILS&" +forminput; // serializes the form's elements.
								
                                    $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
										//alert(data);
										if(data==0){
											$('#response').empty();
											$('#response').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + "   IFSC Code Does Not Exist." + "</font></span></p>");
											
										}
										else if(data==="Your Account Details is Already Exist. Please Use Edit Bank Details Link to Edit Bank Details."){
											$('#response').empty();
											$('#response').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + "   Your Account Details is Already Exist. Please Use Edit Bank Details Link to Edit Bank Details." + "</font></span></p>");
											
										}
										else
										{
											$('#response').empty();
                                            //alert(data);
											
                                            data = $.parseJSON(data);
											ddlBankName.value = data[0].bankname;
											txtMicrcode.value = data[0].micrcode;
											ddlBranch.value = data[0].branchname;
                                            $("#details").show();
											document.getElementById("ddlBankName").readOnly = true;
											//document.getElementById("txtMicrcode").readOnly = true;
											//document.getElementById("ddlBranch").readOnly = true;
											$('#txtIfscCode').attr('readonly', true);
											$("#btnShow").hide();
											$("#sub").show();
									        showData();		 

										}
										
                                    }
                                });
							}
                                return false; // avoid to execute the actual submit of the form.
                            });
			
			  $("#btnSubmit").click(function () {
				 
			var accountno = $("#txtaccountNumber").val();
            var confirmaccountno = $("#txtConfirmaccountNumber").val();
            
			if (accountno != confirmaccountno) {
                alert("Account No Does not match.");
                return false;
            }
            
			
			if ($("#frmBankAccount").valid())
            {
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBankAccount.php"; // the script where you handle the form input.
				var chequeimage=$('#chequeImage').val(); 
               var data;
                var forminput=$("#frmBankAccount").serialize();
                if (Mode == 'Add')
                {
                    data = "action=ADD&chequeimage=" + chequeimage + "&" + forminput; // serializes the form's elements.
                }
                else
                {
					
					data = "action=UPDATE&code=" + BankAccountCode +"&" + forminput;
                    //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                             window.setTimeout(function () {
                                window.location.href = "frmBankAccount.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmBankAccount");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			 }
                return false; // avoid to execute the actual submit of the form.
            });
		    function resetForm(formid) {
		        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
		    }
		
		});
		
	</script>
	
	<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmbankaccount_validation.js" type="text/javascript"></script>	
</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>