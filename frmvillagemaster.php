<?php
$title = "Village Master";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var VillageCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var VillageCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Village Master</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmVillageMaster" id="frmVillageMaster" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Village Name:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="50" name="txtVillageName" id="txtVillageName" placeholder="Village Name">
                        </div>




                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Panchayat Name:</label>
                            <select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >

                            </select>    
                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Gram Panchayat Name:</label>
                            <select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control" >

                            </select>    
                        </div>






                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Village Status:</label>
                            <select id="ddlStatus" name="ddlStatus" class="form-control" >

                            </select>    
                        </div>

                        

                    </div>  







                    <div class="container">
                        <div class="col-sm-4 form-group">
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    

                            <input type="button" name="btnSearch" id="btnSearch" class="btn btn-primary" value="Search"/>
                        </div>
                    </div>

                </form>


            </div>
            <div id="gird"></div>
        </div>   
    </div>



</div>


</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $("#btnSearch").click(function () {
            searchData();
        });

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }

        function FillStatus() {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlStatus").html(data);
                }
            });
        }

        FillStatus();

        function FillPanchayat() {
            $.ajax({
                type: "post",
                url: "common/cfPanchayatSamitiMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlPanchayat").html(data);
                }
            });
        }

        FillPanchayat();

        function FillParent(panchayatid) {
            $.ajax({
                type: "post",
                url: "common/cfGramPanchayatMaster.php",
                data: "action=FILLGP&panchayatid=" + panchayatid,
                success: function (data) {
                    //alert(data);
                    $("#ddlGramPanchayat").html(data);
                }
            });
        }

        $("#ddlPanchayat").change(function () {
            FillParent(this.value);
        });

        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfVillageMaster.php",
                data: "action=DELETE&values=" + VillageCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            $('#response').empty();
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmVillageMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    searchData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfVillageMaster.php",
                data: "action=EDIT&values=" + VillageCode + "",
                success: function (data) {
                    //alert($.parseJSON(data)[0]['VillageName']);
                    data = $.parseJSON(data);
                    txtVillageName.value = data[0].VillageName;
                    ddlDistrict.value = data[0].District;
                    ddlStatus.value = data[0].Status;

                }
            });
        }

        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfVillageMaster.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });
        }

        //showData();


        $("#btnSubmit").click(function () {

            if ($("#frmVillageMaster").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfVillageMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtVillageName.value + "&parent=" + ddlGramPanchayat.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + VillageCode + "&name=" + txtVillageName.value + "&parent=" + ddlGramPanchayat.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmVillageMaster.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("frmVillageMaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        searchData();


                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

     function searchData() {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        var url = "common/cfVillageMaster.php"; // the script where you handle the form input.
        var data = "action=Search&name=" + $('#txtVillageName').val() + "&panchayatid=" + $('#ddlPanchayat').val() + "&parent=" + $('#ddlGramPanchayat').val() + "&status=" + $('#ddlStatus').val() + ""; // serializes the form's elements.
        $.ajax({
            type: "post",
            url: url,
            data: data,
            success: function (data) {

                $("#gird").html(data);
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
                $('#response').empty();
            }
        });
    }

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmVillagemastervalidation.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>