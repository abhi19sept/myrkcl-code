<!DOCTYPE html>
<html lang="en">
<head>
  <title>Scholarship Application Form</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootcss/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="bootcss/js/bootstrap.min.js"></script>
</head>
<body>

    <?php            
        if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
        ?>
    
<div class="container">
  <div class="panel panel-primary">
     <div class="panel-heading">Scholarship Application  Form</div>
        <div class="panel-body">
          <!-- <div class="jumbotron"> -->
 
         <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">
          <div class="container">
            <div class="container">
              <div id="response"></div>
            </div>        
		<div id="errorBox"></div>
                
      <div class="col-sm-4 form-group">     
      <label for="learnercode">Learner Code:</label>
      <input type="text" class="form-control" maxlength="250" name="txtLCode" id="txtLCode" placeholder="Learner Code">
    </div>
     
    <div class="col-sm-4 form-group"> 
      <label for="lname">Learner Name:</label>
      <input type="text" class="form-control" maxlength="250" name="txtLName" id="txtLName" placeholder="Learner Name">     
    </div>
    
   <div class="col-sm-4 form-group">     
      <label for="faname">Father Name:</label>
      <input type="text" class="form-control" id="txtFName" name="txtFName" placeholder="Father Name">
   </div>
    
   <div class="col-sm-4 form-group">     
      <label for="mname">Mother Name:</label>
      <input type="text" class="form-control" id="txtMName" name="txtMName" placeholder="Mother Name">
   </div>
  </div>
  
   <div class="container">
   <div class="col-sm-4 form-group">     
    <label for="l_address">Learner Address:</label>
    <input type="text" class="form-control" name="txtLAddress" id="txtLAddress" placeholder="Learner Address">
   </div>
   
   <div class="col-sm-4 form-group"> 
      <label for="l_mobile">Learner Mobile:</label>
      <input type="text" class="form-control" maxlength="10" name="txtMobile" id="txtMobile" placeholder="Learner Mobile">    
   </div>      
    
    <div class="col-sm-4 form-group">     
      <label for="l_emailid">Learner Emalid:</label>
      <input type="text" class="form-control" id="txtEmail" name="txtEmail" placeholder="Learner Emalid">
    </div>
        
        <div class="col-sm-4 form-group"> 
      <label for="l_batch">Learner Batch:</label>
      <input type="text" class="form-control" id="txtBatch" name="txtBatch" placeholder="Learner Batch">
    </div>
  </div>

	<div class="container">
        <div class="col-sm-4 form-group"> 
      <label for="l_district">Learner District:</label>
      <select id="ddlempdistrict" name="ddlempdistrict" class="form-control">
        <option value="" selected="selected">Please Select</option>                
      </select>
    </div>    
   
      <div class="col-sm-4 form-group"> 
      <label for="l_tehsil">Learner Tehsil:</label>
      <select id="ddlemptehsil" name="ddlemptehsil" class="form-control">
        <option value="" selected="selected">Please Select</option>  
			<option value="1"> Test </option>
      </select>
    </div> 
     
    <div class="col-sm-4 form-group"> 
      <label for="pincode">Pincode:</label>
      <input type="text" class="form-control" id="txtPin" name="txtPin" placeholder="Pincode">
    </div>     
      
     <div class="col-sm-4 form-group"> 
      <label for="l_qualification">Learner Qualification:</label>
      <select id="ddlqualification" name="ddlqualification" class="form-control">
        <option value="" selected="selected">Please Select</option>
			<option value="1"> Test </option>
      </select>
    </div>
    </div>
    
    <div class="container">
      <div class="col-sm-4 form-group"> 
      <label for="l_roll_no">Learner Rollno:</label>
      <input type="text" class="form-control" id="txtRollNo" name="txtRollNo" placeholder="Learner Rollno">
    </div>
    
      <div class="col-sm-4 form-group"> 
      <label for="permission_letter">Upload Permission Letter Note : Only PDF Format Allowed MAX SIZE 150 KB:</label>
      <input type="file" class="form-control" id="file_permission_letter" name="file_permission_letter">
    </div>  
        
        <div class="col-sm-4 form-group"> 
      <label for="scholar_form">Upload Scholarship  Form Note : Only PDF Format Allowed MAX SIZE 150 KB:</label>
      <input type="file" class="form-control" id="file_scholar_form" name="file_scholar_form">
    </div>  
    </div>
	
	 <div class="container">
     <div class="col-sm-6 form-group">
      <label> <span style="float:right;"> Terms & Conditons </span> <br>
        <a href="#">View terms and Conditions</a>
         <span>&nbsp;*</span>
      </label>
	 </div> 
	
        <input type="radio" data-field="x_terms" name="x_terms" id="x_terms_0" value="Agreed">
          <a href="#">I Hereby Accept all(Rules & Regulations) of Scholarship Scheme</a>       
    
    </div>    
    
     <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
				</div>
            </div>   
        </div>
    </form>  
</body>

<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function FillEmpDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlempdistrict").html(data);
                }
            });
        }
        FillEmpDistrict();

        function FillEmpDepartment() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FILLEMPDEPARTMENT",
                success: function (data) {
                    $("#ddlDeptName").html(data);
                }
            });
        }
        FillEmpDepartment();

        function FillEmpDesignation() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FILLEMPDESIGNATION",
                success: function (data) {
                    $("#ddlEmpDesignation").html(data);
                }
            });
        }
        FillEmpDesignation();

        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        FillBankDistrict();


        function FillBankName() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FILLBANKNAME",
                success: function (data) {
                    $("#ddlBankName").html(data);

                }
            });
        }
        FillBankName();

        function FillDobProof() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FillDobProof",
                success: function (data) {
                    $("#ddlDobProof").html(data);

                }
            });
        }
        FillDobProof();



        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=DELETE&values=" + StatusCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=EDIT&values=" + StatusCode + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    txtStatusName.value = data[0].StatusName;
                    txtStatusDescription.value = data[0].StatusDescription;
                    //alert($.parseJSON(data)[0]['StatusName']);
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);

                }
            });
        }

        //showData();


        $("#btnSubmit").click(function () {
			
			$("#form").valid();
			
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfscholar_form.php"; // the script where you handle the form input.
            var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&lcode=" + txtLCode.value + "&lname=" + txtLName.value + "&faname="+ txtFName.value + 
					   "&mothername="+ txtMName.value + "&address="+ txtLAddress.value + "&mobileno="+ txtMobile.value + 
					   "&email="+ txtEmail.value + "&batch="+ txtBatch.value + "&district="+ ddlempdistrict.value + "&tehsil="+ ddlemptehsil.value + 
					   "&pinno="+ txtPin.value + "&qualification="+ ddlqualification.value + "&rollno="+ txtRollNo.value + 
					   "&permission_letter="+ file_permission_letter.value + "&scholar_form="+ file_scholar_form.value + ""; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + StatusCode + "&name=" + txtStatusName.value + "&description=" + txtStatusDescription.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmscholar_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>