<?php
$title = "User Track Report";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8) {
    // you are in
} else {
    header('Location: logout.php');
    exit;
}
?>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD3feOR_ZOOn407Y0CfRMrxYd2lNaJ_4XM&callback=initMap"></script>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;min-height:330px; ">  
            <div class="panel-heading">User Track Report</div>
            <div class="panel-body">
                <form name="formEnquiryReport" id="formEnquiryReport" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-3 form-group"> 
                                <label for="Type" class="headlinelabel">Select User Roll</label>
                                <select id="ddlUserroll" name="ddlUserroll" class="form-control">
                                </select>
                            </div> 
                            <div class="col-md-3 form-group"> 
                                <label for="Type" class="headlinelabel">Select User Login Id</label>
                                <select id="ddlusers" name="ddlusers" class="form-control">
                                </select>
                            </div> 
                            <div class="col-md-3 form-group">     
                                <label for="Start" class="headlinelabel"> Date From</label>
                                <input type="text" id="ddlstartdate" name="ddlstartdate" class="form-control" placeholder="YYYY-MM-YY" autocomplete="off" />
                            </div> 
                            <div class="col-md-3 form-group">     
                                <label for="End" class="headlinelabel"> Date To</label>
                                <input type="text" id="ddlenddate" name="ddlenddate" class="form-control" placeholder="YYYY-MM-YY" autocomplete="off" />
                            </div> 
                        </div>
                        <div class="container">
                            <div class="col-md-6 form-group">        
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Table View"/>    
                                <input type="button" name="btnSubmitMap" id="btnSubmitMap" class="btn btn-primary" value="Map View"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>
                        <div id="dvMap" style="height: 500px; width:94%;"></div>
                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<div id="mapresult"></div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">
$('#ddlstartdate').datepicker({                   
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        autoclose: true
}); 
$('#ddlenddate').datepicker({                   
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        autoclose: true
}); 
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
$(document).ready(function () {
    function FillCourse() {
        $.ajax({
            type: "post",
            url: "common/cfusertrackreport.php",
            data: "action=FillUserRoll",
            success: function (data) {
                $("#ddlUserroll").html(data);
            }
        });
    }
    FillCourse();

    $("#ddlUserroll").change(function () {
        var selCourse = $(this).val();
        $.ajax({
            type: "post",
            url: "common/cfusertrackreport.php",
            data: "action=FillUsers&Userroll=" + selCourse + "",
            success: function (data) {
                $("#ddlusers").html(data);
            }
        });
    });
    $("#btnSubmit").click(function () {
        if ($("#formEnquiryReport").valid())
            {
                $('#response').empty();
                $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                $.ajax({ 
                    url: "common/cfusertrackreport.php",
                    type: "POST",
                    data: $("#formEnquiryReport").serialize()+"&action=ViewTrackReport",
                    success: function(data)
                      {   
                          $('#response').empty();
                           $('#dvMap').html(" ");
                          $("#grid").html(data);
                          $('#example').DataTable({
                                      dom: 'Bfrtip',
                                      buttons: [
                                              'copy', 'csv', 'excel', 'pdf', 'print'
                                      ]
                              });
                      }	        
                });
            }
            else
            {
              return false;
            }
    });
    $("#btnSubmitMap").click(function () {
        if ($("#formEnquiryReport").valid())
            {
                $('#response').empty();
                $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                $.ajax({ 
                    url: "common/cfusertrackreport.php",
                    type: "POST",
                    data: $("#formEnquiryReport").serialize()+"&action=ViewTrackReportMap",
                    success: function(data)
                      {   
                          $('#response').empty();
                          $("#grid").html(" ");
                          $('#dvMap').html(data);
                          initMap()
                      }	        
                });
            }
            else
            {
              return false;
            }
    });
    var map;
    var markerCluster;
    function initMap() {
        map = new google.maps.Map(document.getElementById('dvMap'), {
          zoom: 3,
          center: {lat: 28.024, lng: 85.887}
        });
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            label: labels[i % labels.length]
          });
        });
        markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      }
});
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script>
$("#formEnquiryReport").validate({
    rules: {
      ddlEnquirytype: "required",
      ddlstartdate: "required",
      ddlenddate: "required"
      
    },
    messages: {
      ddlEnquirytype: "Please Select Report Type",
      ddlstartdate: "Please Select Start Date",
      ddlenddate: "Please Select End Date"
    },  
});
</script>
<style>
    .error {
        color: #D95C5C!important;
    }
    .headlinelabel{
        font-weight: bold;
    }
</style>
</html>
