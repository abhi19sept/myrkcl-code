<?php  ob_start(); 
$title="Examination Letter ";
include ('header.php'); 
include ('root_menu.php');  
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
 if ($_SESSION['User_Code'] == '1'){           
?>
<div style="min-height:450px;max-height:auto">
        <div class="container" > 
            <div class="panel panel-primary" style="margin-top:46px !important;" >

                <div class="panel-heading" id="non-printable">Gererate Result Certificates</div>
                <div class="panel-body">
				
			 
                    <!-- <div class="jumbotron"> -->
                    <form name="frmexameventmaster" id="frmexameventmaster" action="" class="form-inline">     

                        <div class="container">
                            <div class="container">
							
							
							
							<div id="errorBox"></div>
							<div class="col-sm-4 form-group"  id="non-printable"> 
                                <label for="edistrict">Event Name 
								:</label>
                                <select id="ddlEvent" name="code" class="form-control" >
								 
                                </select>    
                            </div>

                            <div class="col-sm-4 form-group"  id="non-printable"> 
                                <label for="edistrict">Generate By
								:</label>
                                <select id="generateBy" name="generateBy" class="form-control" >
								 	<option value="">Select</option>
								 	<option value="district">District</option>
								 	<option value="itgk">ITGKs / Centers</option>
								 	<option value="learners">Learner / ITGK Codes</option>
                                    <option value="job">Execute Job</option>
                                </select>
                            </div>
							
							<div class="col-sm-4 form-group asHidden district"  id="non-printable"> 
						        <label for="edistrict">District Name 
								:</label>
						        <select id="ddlDistrict" name="ddlDistrict" class="form-control" >
								 
						        </select>    
						    </div>

							<div class="col-sm-4 form-group asHidden itgk"  id="non-printable"> 
                                <label for="edistrict">ITGK Code 
								:</label>
                                <select id="ddlCenter" name="ddlCenter[]" class="form-control"  multiple="multiple" >
								 
                                </select>    
                            </div>
							
							<div class="col-sm-4 form-group asHidden learners"  id="non-printable" style="float:left;"> 
                                <label for="edistrict">Learner / ITGK Code(s)
								:</label>
                                <input id="learnerCode" name="learnerCode" class="form-control" type="text" />
                            </div>

                            <div class="col-sm-4 form-group asHidden job"  id="non-printable" style="float:left;"> 
                                <label for="edistrict">Execute Job
                                :</label>
                                <select id="job" name="job" class="form-control" >
                                <?php
                                    echo "<option value=''>Select Job</option>";
                                    for ($i = 1; $i<11; $i++) {
                                        echo "<option value=" . $i . ">" . $i . "</option>";
                                    }
                                ?>
                                </select>
                            </div>
                            

							<?php 
								include("frmDistrictWiseExamCenters.php");
							?>
							</div>
							
                            </div> 

                        <div class="container" id="non-printable">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="show" style="margin-left:30px"/>    
                        </div>

                        <div class="container small" id="non-printable">
                            <p>&nbsp;</p>
                            <p>Before start to generate certificates for a new exam event, you have to ensure that:</p>
                            <ul>
                                <li>`certificates` directory is created at location 'upload/certificates'</li>
                                <li>If Directory already exists, then it should be empty before start as new.</li>
                                <li>Tables Involves: tbl_admission, tbl_batch_master, tbl_course_master, tbl_district_master, tbl_events, tbl_organization_detail, tbl_result, tbl_user_master .</li>
                            </ul>
                        </div>
					<div id="response"></div>
					 </form> 
				 
				</div>
					
            </div>   
        </div>
  
		</div>				
	</body>					
<?php include ('footer.php'); ?>				
<?php include'common/message.php';?>

<script>

</script>
<style>
#errorBox{
 color:#F00;
 }
</style>
<script type="text/javascript">
    $(document).ready(function () {
    	$(".asHidden").hide();
	       	function FillEvent() {
                $.ajax({
                    type: "post",
                    url: "common/cfexamcertificates.php",
                    data: "action=FILLRESULTEVENTS",
                    success: function (data) {
                        $("#ddlEvent").html(data);
                    }
                });
            }
             
            FillEvent(); 

							$("#ddlDistrict").change(function(){
								var selregion = $(this).val();
								//alert(selregion);
								showItgk(selregion);
                            });
			
                          $("#btnSubmit").click(function () {
								//alert(1);
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                               var url = "common/cfexamcertificates.php"; // the script where you handle the form input.
								var data;
								var forminput=$("#frmexameventmaster").serialize();
								
									data = "action=PDF&" +forminput; // serializes the form's elements.
								//alert(data);
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span></span><span>" + data + "</span></p>");
                                    }
                                });
							
                                return false; // avoid to execute the actual submit of the form.
                            });
							
							$("#generateBy").change(function(){
								showfields(this.value);
							});

                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                        function showfields(generateBy) {
                        	$(".downloadbutton").remove();
                        	$(".asHidden").hide();
                        	$("." + generateBy).show();
                        	FillDistrict();
                        	showItgk('');
                        	getExamCenters('');
	                      	if (generateBy == 'itgk' || generateBy == 'examcenter') {
	                      		$(".district").show();
	                      	} else if (generateBy == 'copy' || generateBy == 'miss') {
                                $(".job").show();
                            }

                            $('#job').prop('selectedIndex',0);
                        }

                        function showItgk(selregion) {
                        	$(".itgk").append("<p class='error processitgk'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                        	$.ajax({
					          url: 'common/cfexamcertificates.php',
					          type: "post",
					          data: "action=FillByDistrict&values=" + selregion + "",
					          success: function(data){
								//alert(data);
								$('#ddlCenter').html(data);
								$(".processitgk").remove();
					          }
					        });
                        }

                    </script>

<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>