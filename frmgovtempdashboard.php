<?php  ob_start(); 
$title="Govt. Reimbursement Status Dashboard";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
        
if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_Code'] == 6588)

{
    // you are in
} else {
    header('Location: index.php');
    exit;
}
 ?>
 
<link rel="stylesheet" href="css/profile_style.css">

<!----  /* Show Table Count Detail*/for box css stat form here---->

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<style>
    

.block-header h2 {
    margin: 0 !important;
    color: #666 !important;
    font-weight: normal;
    font-size: 16px;
}
    /* Card ======================================== */
    .info-box .content .number {
    font-weight: normal;
    font-size: 26px;
    margin-top: -4px;
    color: #555 !important;
}
.bg-red {
  background-color: #F44336 !important;
  color: #fff; }
  .bg-red .content .text,
  .bg-red .content .number {
    color: #fff !important; }

.bg-pink {
  background-color: #E91E63 !important;
  color: #fff; }
  .bg-pink .content .text,
  .bg-pink .content .number {
    color: #fff !important; }

.bg-purple {
  background-color: #9C27B0 !important;
  color: #fff; }
  .bg-purple .content .text,
  .bg-purple .content .number {
    color: #fff !important; }

.bg-deep-purple {
  background-color: #673AB7 !important;
  color: #fff; }
  .bg-deep-purple .content .text,
  .bg-deep-purple .content .number {
    color: #fff !important; }

.bg-indigo {
  background-color: #3F51B5 !important;
  color: #fff; }
  .bg-indigo .content .text,
  .bg-indigo .content .number {
    color: #fff !important; }

.bg-blue {
  background-color: #2196F3 !important;
  color: #fff; }
  .bg-blue .content .text,
  .bg-blue .content .number {
    color: #fff !important; }

.bg-light-blue {
  background-color: #03A9F4 !important;
  color: #fff; }
  .bg-light-blue .content .text,
  .bg-light-blue .content .number {
    color: #fff !important; }

.bg-cyan {
  background-color: #00BCD4 !important;
  color: #fff; }
  .bg-cyan .content .text,
  .bg-cyan .content .number {
    color: #fff !important; }

.bg-teal {
  background-color: #009688 !important;
  color: #fff; }
  .bg-teal .content .text,
  .bg-teal .content .number {
    color: #fff !important; }

.bg-green {
  background-color: #4CAF50 !important;
  color: #fff; }
  .bg-green .content .text,
  .bg-green .content .number {
    color: #fff !important; }

.bg-light-green {
  background-color: #8BC34A !important;
  color: #fff; }
  .bg-light-green .content .text,
  .bg-light-green .content .number {
    color: #fff !important; }

.bg-lime {
  background-color: #CDDC39 !important;
  color: #fff; }
  .bg-lime .content .text,
  .bg-lime .content .number {
    color: #fff !important; }

.bg-yellow {
  background-color: #ffe821 !important;
  color: #fff; }
  .bg-yellow .content .text,
  .bg-yellow .content .number {
    color: #fff !important; }

.bg-amber {
  background-color: #FFC107 !important;
  color: #fff; }
  .bg-amber .content .text,
  .bg-amber .content .number {
    color: #fff !important; }

.bg-orange {
  background-color: #FF9800 !important;
  color: #fff; }
  .bg-orange .content .text,
  .bg-orange .content .number {
    color: #fff !important; }

.bg-deep-orange {
  background-color: #FF5722 !important;
  color: #fff; }
  .bg-deep-orange .content .text,
  .bg-deep-orange .content .number {
    color: #fff !important; }

.bg-brown {
  background-color: #795548 !important;
  color: #fff; }
  .bg-brown .content .text,
  .bg-brown .content .number {
    color: #fff !important; }

.bg-grey {
  background-color: #9E9E9E !important;
  color: #fff; }
  .bg-grey .content .text,
  .bg-grey .content .number {
    color: #fff !important; }

.bg-blue-grey {
  background-color: #607D8B !important;
  color: #fff; }
  .bg-blue-grey .content .text,
  .bg-blue-grey .content .number {
    color: #fff !important; }

.bg-black {
  background-color: #000000 !important;
  color: #fff; }
  .bg-black .content .text,
  .bg-black .content .number {
    color: #fff !important; }

.bg-white {
  background-color: #ffffff !important;
  color: #fff; }
  .bg-white .content .text,
  .bg-white .content .number {
    color: #fff !important; }

.col-red {
  color: #F44336 !important; }

.col-pink {
  color: #E91E63 !important; }

.col-purple {
  color: #9C27B0 !important; }

.col-deep-purple {
  color: #673AB7 !important; }

.col-indigo {
  color: #3F51B5 !important; }

.col-blue {
  color: #2196F3 !important; }

.col-light-blue {
  color: #03A9F4 !important; }

.col-cyan {
  color: #00BCD4 !important; }

.col-teal {
  color: #009688 !important; }

.col-green {
  color: #4CAF50 !important; }

.col-light-green {
  color: #8BC34A !important; }

.col-lime {
  color: #CDDC39 !important; }

.col-yellow {
  color: #ffe821 !important; }

.col-amber {
  color: #FFC107 !important; }

.col-orange {
  color: #FF9800 !important; }

.col-deep-orange {
  color: #FF5722 !important; }

.col-brown {
  color: #795548 !important; }

.col-grey {
  color: #9E9E9E !important; }

.col-blue-grey {
  color: #607D8B !important; }

.col-black {
  color: #000000 !important; }

.col-white {
  color: #ffffff !important; }
    /* Infobox ===================================== */
.info-box {
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
  height: 80px;
  display: flex;
  cursor: default;
  background-color: #fff;
  position: relative;
  overflow: hidden;
  margin-bottom: 30px; }
  .info-box .icon {
    display: inline-block;
    text-align: center;
    background-color: rgba(0, 0, 0, 0.12);
    width: 80px; }
    .info-box .icon i {
      color: #fff;
      font-size: 50px;
      line-height: 80px; }
    .info-box .icon .chart.chart-bar {
      height: 100%;
      line-height: 100px; }
      .info-box .icon .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box .icon .chart.chart-pie {
      height: 100%;
      line-height: 123px; }
      .info-box .icon .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box .icon .chart.chart-line {
      height: 100%;
      line-height: 115px; }
      .info-box .icon .chart.chart-line canvas {
        vertical-align: baseline !important; }
  .info-box .content {
    display: inline-block;
    padding: 7px 10px; }
    .info-box .content .text {
      font-size: 13px;
      margin-top: 3px;
      color: #555; }
    .info-box .content .number {
      font-weight: normal;
      font-size: 26px;
      margin-top: -4px;
      color: #555; }
    
</style>

<!----  /* Show Table Count Detail*/for box css stat form here---->
  <link rel="stylesheet" href="css/datepicker.css">
<link href="css/popup.css" rel="stylesheet" type="text/css">

<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			 
            
            
             <div class="panel panel-primary" style="margin-top:20px !important;">

                <div class="panel-heading">DASHBOARD</div>
                
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     
                 <legend><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;Offline And Paperless Details
                </legend>
                
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    			
                        <!-- Widgets /* Show Table Count Detail*/-->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-teal hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">playlist_add_check</i>
                                </div>
								
                                <div class="content totaldetails" id="0">
                                    <div class="text">Approved by RKCL</div>
                                    <div id="Approved_by_RKCL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait....</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-purple  hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">hourglass_empty</i>
                                </div>
                                <div class="content totaldetails" id="3">
                                    <div class="text">Pending for Processing</div>
                                    <div id="Pending_for_Processing" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-light-green hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">delete_forever</i>
                                </div>
                                <div class="content totaldetails" id="4">
                                    <div class="text">Rejected by RKCL</div>
                                    <div id="Rejected_by_RKCL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-pink hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">content_paste</i>
                                </div>
                                <div class="content totaldetails" id="12">
                                    <div class="text">Receipt Not Submited</div>
                                    <div id="Receipt_Not_Submited" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-green hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">flight_takeoff</i>
                                </div>
                                <div class="content totaldetails" id="5">
                                    <div class="text">Sent to DoIT for Budget Approval</div>
                                    <div id="Sent_to_DoIT" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-blue-grey  hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content totaldetails"  id="6">
                                    <div class="text">Amount Reimbursed to Beneficiary's Account</div>
                                    <div id="Amount_Reimbursed" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-orange hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">thumb_down</i>
                                </div>
                                <div class="content totaldetails" id="7">
                                    <div class="text">Amount Bounced by the Bank</div>
                                    <div id="Amount_Bounced" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-cyan hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">import_contacts</i>
                                </div>
                                <div class="content totaldetails" id="8">
                                    <div class="text">Awaiting Budget Approval from DoIT, GOR</div>
                                    <div id="Awaiting_Budget_Approval" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-deep-purple hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">hourglass_empty</i>
                                </div>
                                <div class="content totaldetails"  id="9">
                                    <div class="text">Pending With DoIT for Clearing</div>
                                    <div id="Pending_With_DoIT" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-lime hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">delete_forever</i>
                                </div>
                                <div class="content totaldetails" id="10">
                                    <div class="text">Claim Rejected by DoIT</div>
                                    <div id="Rejected_by_DoIT" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait..</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-brown  hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">info_outline</i>
                                </div>
                                <div class="content totaldetails"  id="11">
                                    <div class="text">No Status Found</div>
                                    <div id="No_Status_Found" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- #END# Widgets -->
                   
                </div>
                
                <legend><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;Paper Less Detail
                </legend>
                    
                    <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    			
                        <!-- Widgets /* Show Table Count Detail*/-->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-deep-orange hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">help</i>
                                </div>
                                <div class="content">
                                    <div class="text">Online Process Start</div>
                                    <div id="Online_Start_Process_Date" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20" style="font-size: 24px; font-weight: bold;">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-teal hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">playlist_add_check</i>
                                </div>
                                <div class="content paperlessdetails" id="12">
                                    <div class="text">Total Applied Paper Less </div>
                                    <div id="Total_Applied_Paper_Less" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-grey hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">playlist_add_check</i>
                                </div>
                                <div class="content paperlessdetails" id="30">
                                    <div class="text">Applied In Last 30 Days</div>
                                    <div id="Applied_From_Last_30_Days" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-indigo hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">person_add</i>
                                </div>
                                <div class="content paperlessdetails" id="14">
                                    <div class="text">Today Applied Application</div>
                                    <div id="Today_Applied_Application" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-teal hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">playlist_add_check</i>
                                </div>
								
                                <div class="content paperlessdetails" id="0">
                                    <div class="text">Approved by RKCL</div>
                                    <div id="Approved_by_RKCL_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait....</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-purple  hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">hourglass_empty</i>
                                </div>
                                <div class="content paperlessdetails" id="3">
                                    <div class="text">Pending for Processing</div>
                                    <div id="Pending_for_Processing_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-light-green hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">delete_forever</i>
                                </div>
                                <div class="content paperlessdetails" id="4">
                                    <div class="text">Rejected by RKCL</div>
                                    <div id="Rejected_by_RKCL_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-pink hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">content_paste</i>
                                </div>
                                <div class="content paperlessdetails" id="12">
                                    <div class="text">Receipt Not Submited</div>
                                    <div id="Receipt_Not_Submited_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-green hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">flight_takeoff</i>
                                </div>
                                <div class="content paperlessdetails" id="5">
                                    <div class="text">Sent to DoIT for Budget Approval</div>
                                    <div id="Sent_to_DoIT_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-blue-grey  hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content paperlessdetails"  id="6">
                                    <div class="text">Amount Reimbursed to Beneficiary's Account</div>
                                    <div id="Amount_Reimbursed_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-orange hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">thumb_down</i>
                                </div>
                                <div class="content paperlessdetails" id="7">
                                    <div class="text">Amount Bounced by the Bank</div>
                                    <div id="Amount_Bounced_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-cyan hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">import_contacts</i>
                                </div>
                                <div class="content paperlessdetails" id="8">
                                    <div class="text">Awaiting Budget Approval from DoIT, GOR</div>
                                    <div id="Awaiting_Budget_Approval_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-deep-purple hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">hourglass_empty</i>
                                </div>
                                <div class="content paperlessdetails"  id="9">
                                    <div class="text">Pending With DoIT for Clearing</div>
                                    <div id="Pending_With_DoIT_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-lime hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">delete_forever</i>
                                </div>
                                <div class="content paperlessdetails" id="10">
                                    <div class="text">Claim Rejected by DoIT</div>
                                    <div id="Rejected_by_DoIT_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait..</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-brown  hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">info_outline</i>
                                </div>
                                <div class="content paperlessdetails" id="11">
                                    <div class="text">No Status Found</div>
                                    <div id="No_Status_Found_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
						
					  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-grey  hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content paperlessdetails" id="15">
                                    <div class="text">Processed From Last 30 Days</div>
                                    <div id="Process_Deo_PL" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <!-- #END# Widgets -->
                   
                </div>
                
                        <!-- #END# Widgets -->
					<legend><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;Offline Details
                </legend>
                
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    			
                        <!-- Widgets /* Show Table Count Detail*/-->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-teal hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">playlist_add_check</i>
                                </div>
								
                                <div class="content offlinedetails" id="0">
                                    <div class="text">Approved by RKCL</div>
                                    <div id="Approved_by_RKCL_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait....</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-purple  hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">hourglass_empty</i>
                                </div>
                                <div class="content offlinedetails" id="3">
                                    <div class="text">Pending for Processing</div>
                                    <div id="Pending_for_Processing_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-light-green hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">delete_forever</i>
                                </div>
                                <div class="content offlinedetails" id="4">
                                    <div class="text">Rejected by RKCL</div>
                                    <div id="Rejected_by_RKCL_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-pink hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">content_paste</i>
                                </div>
                                <div class="content offlinedetails" id="12">
                                    <div class="text">Receipt Not Submited</div>
                                    <div id="Receipt_Not_Submited_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-green hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">flight_takeoff</i>
                                </div>
                                <div class="content offlinedetails" id="5">
                                    <div class="text">Sent to DoIT for Budget Approval</div>
                                    <div id="Sent_to_DoIT_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-blue-grey  hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content offlinedetails"  id="6">
                                    <div class="text">Amount Reimbursed to Beneficiary's Account</div>
                                    <div id="Amount_Reimbursed_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-orange hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">thumb_down</i>
                                </div>
                                <div class="content offlinedetails" id="7">
                                    <div class="text">Amount Bounced by the Bank</div>
                                    <div id="Amount_Bounced_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-cyan hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">import_contacts</i>
                                </div>
                                <div class="content offlinedetails" id="8">
                                    <div class="text">Awaiting Budget Approval from DoIT, GOR</div>
                                    <div id="Awaiting_Budget_Approval_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-deep-purple hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">hourglass_empty</i>
                                </div>
                                <div class="content offlinedetails"  id="9">
                                    <div class="text">Pending With DoIT for Clearing</div>
                                    <div id="Pending_With_DoIT_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-lime hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">delete_forever</i>
                                </div>
                                <div class="content offlinedetails" id="10">
                                    <div class="text">Claim Rejected by DoIT</div>
                                    <div id="Rejected_by_DoIT_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait..</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-brown  hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">info_outline</i>
                                </div>
                                <div class="content offlinedetails"  id="11">
                                    <div class="text">No Status Found</div>
                                    <div id="No_Status_Found_Offline" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">Pls Wait...</div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- #END# Widgets -->
                   
                </div>
                </div>
                </form>
            </div> 
        </div>
    
</div>

<!-- Modal -->
<div id="myModalimage" class="modal" style="padding-top:50px !important">
            
  <div class="modal-content" style="width: 90%;">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h6>Govt. Reimbursement Status Details</h6>
    </div>
      <div class="modal-body" style="max-height: 400px; overflow-y: scroll; text-align: center;">
	  <div id="response"></div>
        <div id="grid" ></div>		
    </div>
  </div>
</div>


<div id="myModalimageinside" class="modal" style="padding-top:50px !important">
            
  <div class="modal-content" style="width: 90%;">
    <div class="modal-header">
      <span class="close closeinside">&times;</span>
      <h6>Govt. Reimbursement Status Details</h6>
    </div>
      <div class="modal-body" style="max-height: 400px; overflow-y: scroll; text-align: center;">
	  <div id="responseinside"></div>
        <div id="gird" ></div>		
    </div>
  </div>
</div>


<div id="myModalimagelearner" class="modal" style="padding-top:50px !important">
            
  <div class="modal-content" style="width: 90%;">
    <div class="modal-header">
      <span class="close closelearner">&times;</span>
      <h6>Govt. Reimbursement Learner Details</h6>
    </div>
      <div class="modal-body" style="max-height: 400px; overflow-y: scroll; text-align: center;">
	  <div id="responselearner"></div>
        <div id="girdlearner" ></div>		
    </div>
  </div>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%; font-size:14px;
}
th, td {
    padding: 12px;
}
tr, td {
    padding: 7px;
}
</style>  
<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {      

        function FillApproved_by_RKCL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=FillApproved_by_RKCL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Approved_by_RKCL").html(data);
					}                    
                }
            });
        }
        FillApproved_by_RKCL();
		
		function FillApproved_by_RKCL_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=FillApproved_by_RKCL_Offline",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Approved_by_RKCL_Offline").html(data);
					}                    
                }
            });
        }
        FillApproved_by_RKCL_Offline();
		
		$("#gird").on("click",".learner_details",function(){					
				var val=$(this).attr("id");				
				var deo=$(this).attr("name");				
				var modal = document.getElementById('myModalimagelearner');
					var span = document.getElementsByClassName("closelearner")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#girdlearner").html("");
					}
				$('#responselearner').empty();
            $('#responselearner').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=GETLearnerDeoDetails&ldate=" + val + "&deologin=" + deo + "",
                success: function (data) {
					//alert(data);
					$('#responselearner').empty();
					$("#girdlearner").html(data);											
					$('#examplelearner').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}
            });	
					
		});	
		
		
		$(".totaldetails").click(function () {			
				var val=$(this).attr("id");				
				var modal = document.getElementById('myModalimage');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#grid").html("");
					}
				$('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=GETDetails&status=" + val + "",
                success: function (data) {
					//alert(data);
					$('#response').empty();
					$("#grid").html(data);											
					$('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}
            });	
					
		});	
        
		$(".offlinedetails").click(function () {			
				var val=$(this).attr("id");				
				var modal = document.getElementById('myModalimage');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#grid").html("");
					}
				$('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=GETOfflineDetails&status=" + val + "",
                success: function (data) {
					//alert(data);
					$('#response').empty();
					$("#grid").html(data);											
					$('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}
            });	
					
		});	
		
		$(".paperlessdetails").click(function () {			
				var val=$(this).attr("id");				
				var modal = document.getElementById('myModalimage');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#grid").html("");
					}
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=GETPaperlessDetails&status=" + val + "",
                success: function (data) {
					//alert(data);
					$('#response').empty();
					$("#grid").html(data);											
					$('#example1').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}
            });	
					
		});	
		
		$("#grid").on("click",".deodetails",function(){
			var val=$(this).attr("id");	
			var modal = document.getElementById('myModalimageinside');
					var span = document.getElementsByClassName("closeinside")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#gird").html("");
					}
					$('#responseinside').empty();
					$('#responseinside').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
					$.ajax({
					type: "post",
					url: "common/cfgovtempdashboard.php",
					data: "action=GETDeoDetails&status=" + val + "",
					success: function (data) {
						//alert(data);
						$('#responseinside').empty();
						$("#gird").html(data);											
						$('#example2').DataTable({
							dom: 'Bfrtip',
							buttons: [
								'copy', 'csv', 'excel', 'pdf', 'print'
							]
						});
					}
				});
		});
		
			
		
		
        function Fill_Pending_for_Processing() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Pending_for_Processing",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Pending_for_Processing").html(data);
					}
                }
            });
        }
        Fill_Pending_for_Processing();   

		function Fill_Pending_for_Processing_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Pending_for_Processing_Offline",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Pending_for_Processing_Offline").html(data);
					}
                }
            });
        }
        Fill_Pending_for_Processing_Offline();
        
        function Fill_Rejected_by_RKCL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Rejected_by_RKCL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
                    $("#Rejected_by_RKCL").html(data);
					}
                }
            });
        }
        Fill_Rejected_by_RKCL();
		
		function Fill_Rejected_by_RKCL_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Rejected_by_RKCL_Offline",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
                    $("#Rejected_by_RKCL_Offline").html(data);
					}
                }
            });
        }
        Fill_Rejected_by_RKCL_Offline();
        
        function Fill_Receipt_Not_Submited() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Receipt_Not_Submited",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Receipt_Not_Submited").html(data);
					}
                }
            });
        }
        Fill_Receipt_Not_Submited();
		
		function Fill_Receipt_Not_Submited_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Receipt_Not_Submited_Offline",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Receipt_Not_Submited_Offline").html(data);
					}
                }
            });
        }
        Fill_Receipt_Not_Submited_Offline();
        
        function Fill_Sent_to_DoIT() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Sent_to_DoIT",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Sent_to_DoIT").html(data);
					}
                }
            });
        }
        Fill_Sent_to_DoIT();
		
		function Fill_Sent_to_DoIT_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Sent_to_DoIT_Offline",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Sent_to_DoIT_Offline").html(data);
					}
                }
            });
        }
        Fill_Sent_to_DoIT_Offline();
        
        function Fill_Amount_Reimbursed() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Amount_Reimbursed",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Amount_Reimbursed").html(data);
					}
                }
            });
        }
        Fill_Amount_Reimbursed();
		
		function Fill_Amount_Reimbursed_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Amount_Reimbursed_Offline",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Amount_Reimbursed_Offline").html(data);
					}
                }
            });
        }
        Fill_Amount_Reimbursed_Offline();
        
        function Fill_Amount_Bounced() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Amount_Bounced",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Amount_Bounced").html(data);
					}
                }
            });
        }
        Fill_Amount_Bounced();
		
		function Fill_Amount_Bounced_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Amount_Bounced_Offline",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Amount_Bounced_Offline").html(data);
					}
                }
            });
        }
        Fill_Amount_Bounced_Offline();
        
        function Fill_Awaiting_Budget_Approval() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Awaiting_Budget_Approval",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Awaiting_Budget_Approval").html(data);
					}
                }
            });
        }
        Fill_Awaiting_Budget_Approval();
		
		
		function Fill_Awaiting_Budget_Approval_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Awaiting_Budget_Approval_Offline",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Awaiting_Budget_Approval_Offline").html(data);
					}
                }
            });
        }
        Fill_Awaiting_Budget_Approval_Offline();
        
        function Fill_Pending_With_DoIT() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Pending_With_DoIT",
                success: function (data) {					
					if(data=='error'){
						
					}
					else {
						$("#Pending_With_DoIT").html(data);
					}
                }
            });
        }
        Fill_Pending_With_DoIT();
		
		function Fill_Pending_With_DoIT_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Pending_With_DoIT_Offline",
                success: function (data) {					
					if(data=='error'){
						
					}
					else {
						$("#Pending_With_DoIT_Offline").html(data);
					}
                }
            });
        }
        Fill_Pending_With_DoIT_Offline();
        
        function Fill_Rejected_by_DoIT() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Rejected_by_DoIT",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Rejected_by_DoIT").html(data);
					}
                }
            });
        }
        Fill_Rejected_by_DoIT();
		
		
		function Fill_Rejected_by_DoIT_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Rejected_by_DoIT_Offline",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Rejected_by_DoIT_Offline").html(data);
					}
                }
            });
        }
        Fill_Rejected_by_DoIT_Offline();
        
        function Fill_No_Status_Found() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_No_Status_Found",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#No_Status_Found").html(data);
					}
                }
            });
        }
        Fill_No_Status_Found();     

		function Fill_No_Status_Found_Offline() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_No_Status_Found_Offline",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#No_Status_Found_Offline").html(data);
					}
                }
            });
        }
        Fill_No_Status_Found_Offline();
        
        function Fill_Online_Start_Process_Date() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Online_Start_Process_Date",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Online_Start_Process_Date").html(data);
					}
                }
            });
        }
        Fill_Online_Start_Process_Date();
        
        
        function Fill_Total_Applied_Paper_Less() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Total_Applied_Paper_Less",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Total_Applied_Paper_Less").html(data);
					}
                }
            });
        }
        Fill_Total_Applied_Paper_Less();
        
        function Fill_Applied_From_Last_30_Days() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Applied_From_Last_30_Days",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Applied_From_Last_30_Days").html(data);
					}
                }
            });
        }
        Fill_Applied_From_Last_30_Days();
        
        function Fill_Today_Applied_Application() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Today_Applied_Application",
                success: function (data) {					
					if(data=='error'){
						
					}
					else {
						$("#Today_Applied_Application").html(data);
					}
                }
            });
        }
        Fill_Today_Applied_Application();
        
        function Fill_Approved_by_RKCL_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Approved_by_RKCL_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Approved_by_RKCL_PL").html(data);
					}
                }
            });
        }
        Fill_Approved_by_RKCL_PL();
        
        function Fill_Pending_for_Processing_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Pending_for_Processing_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Pending_for_Processing_PL").html(data);
					}
                }
            });
        }
        Fill_Pending_for_Processing_PL();
        
        function Fill_Rejected_by_RKCL_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Rejected_by_RKCL_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Rejected_by_RKCL_PL").html(data);
					}
                }
            });
        }
        Fill_Rejected_by_RKCL_PL();
        
        function Fill_Receipt_Not_Submited_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Receipt_Not_Submited_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Receipt_Not_Submited_PL").html(data);
					}
                }
            });
        }
        Fill_Receipt_Not_Submited_PL();
        
        
        function Fill_Sent_to_DoIT_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Sent_to_DoIT_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Sent_to_DoIT_PL").html(data);
					}
                }
            });
        }
        Fill_Sent_to_DoIT_PL();
        
        function Fill_Amount_Reimbursed_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Amount_Reimbursed_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Amount_Reimbursed_PL").html(data);
					}
                }
            });
        }
        Fill_Amount_Reimbursed_PL();
        
        function Fill_Amount_Bounced_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Amount_Bounced_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {	
						$("#Amount_Bounced_PL").html(data);
					}
                }
            });
        }
        Fill_Amount_Bounced_PL();
        
        function Fill_Awaiting_Budget_Approval_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Awaiting_Budget_Approval_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Awaiting_Budget_Approval_PL").html(data);
					}
                }
            });
        }
        Fill_Awaiting_Budget_Approval_PL();
        
        function Fill_Pending_With_DoIT_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Pending_With_DoIT_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Pending_With_DoIT_PL").html(data);
					}
                }
            });
        }
        Fill_Pending_With_DoIT_PL();
        
        function Fill_Rejected_by_DoIT_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Rejected_by_DoIT_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Rejected_by_DoIT_PL").html(data);
					}
                }
            });
        }
        Fill_Rejected_by_DoIT_PL();
        
        function Fill_No_Status_Found_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_No_Status_Found_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#No_Status_Found_PL").html(data);
					}
                }
            });
        }
        Fill_No_Status_Found_PL();
		
		function Fill_Process_deo_PL() {
            $.ajax({
                type: "post",
                url: "common/cfgovtempdashboard.php",
                data: "action=Fill_Process_deo_PL",
                success: function (data) {
					if(data=='error'){
						
					}
					else {
						$("#Process_Deo_PL").html(data);
					}
                }
            });
        }
        Fill_Process_deo_PL();       
    });

</script>
</html>