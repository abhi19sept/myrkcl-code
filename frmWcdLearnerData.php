<?php
$title = "WCD Approved Learners List";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">WCD Approved Learners List</div>
            <div class="panel-body">
               
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                    <div class="container">
                        <div class="col-md-4 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">
                                    <!--                                <option value="0">All Batch</option>-->
                                </select>
                        </div> 
                    </div>
					<div class="container">
                        <div class="col-md-4 form-group"> 
                            <label for="course">Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>
                        </div> 
                    </div>
					<div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>

                    <div id="grid" name="grid" style="margin-top:35px;"> </div> 


           </form> 
        </div>   
    </div>

</div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

</body>
<style>
    #errorBox{
        color:#F00;
    }
</style>




<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfWcdLearnerItgk.php",
                data: "action=FillCourse",
                success: function (data) {
					$("#ddlCourse").html(data);
                }
            });

        }
	FillCourse();

        $("#ddlCourse").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfWcdLearnerItgk.php",
                data: "action=FILLBatchName&values=" + ddlCourse.value + "",
                success: function (data) {
					//alert(data);
                    $("#ddlBatch").html(data);
                }
            });

        });
		
    $("#btnSubmit").click(function () {
		
			$('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdLearnerItgk.php"; // the script where you handle the form input.
            var data;
            //alert (role_type);
            data = "action=showdataitgk&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
            //alert(data);
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    //alert(data);
					if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Batch." + "</span></div>");                            
						}
						else{
							 $("#response").empty();
								$("#grid").html(data);
								$("#example").DataTable({
									dom: 'Bfrtip',
									buttons: [
										'copy', 'csv', 'excel', 'pdf', 'print'
									]
								});
						}                   
                }
            });
		
    });
	function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
});
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>