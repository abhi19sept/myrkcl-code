<?php
$title="Organisation Details";
include ('header.php'); 
include ('root_menu.php'); 

    if (isset($_REQUEST['code'])) {
                echo "<script>var OrganizationCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var OrganizationCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
        <div class="container"> 
			 

            <div class="panel panel-primary" style="margin-top:46px !important;">

                <div class="panel-heading">Organization  Details</div>
                <div class="panel-body">
                    
                    <form name="frmOrgDeatil" id="frmOrgDeatil" action="" class="form-inline">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Organization Name:<font color="red">*</font></label>
                                <input type="text" class="form-control" maxlength="50" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center" readonly="true">
								<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
								<input type="hidden" class="form-control" maxlength="50" name="txtcode" id="txtcode"/>
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Registration No:<font color="red">*</font></label>
                                <input type="text" class="form-control" maxlength="50"  name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:<font color="red">*</font></label>
                                <input type="text" class="form-control" maxlength="50" name="txtEstdate" id="txtEstdate"  placeholder="DD-MM-YY" readonly='true'>
								
                            </div>
							
							 <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:<font color="red">*</font></label>
                                <select id="txtType" name="txtType" class="form-control" >
								  <option selected="true"value="">Select</option>
									<option value="1" >
										RS-CIT
									</option>
                                </select>    
                            </div>
							
							</div>
							
							
							
							
						<div class="container">

								<div class="col-sm-4 form-group"> 
                                <label for="dobprrof">Date of Birth Proof:<span class="star">*</span></label>
                                <select id="ddlDobProof" name="ddlDobProof" class="form-control">
                                                 
                                </select>
                            </div> 
                           
							<div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Upload Birth Proof:<font color="red">*</font></label>
                                <input type="file" class="form-control"  name="orgdocproof" id="orgdocproof" onchange="checkMarksheet(this)">
								<span style="font-size:10px;"> Note : JPG,JPEG Allowed Max Size =200KB</span>
                            </div>
							<div class="col-sm-4 form-group">     
                                <label for="address">House No:<font color="red">*</font></label>
                                <input type="text" class="form-control" name="txtHouseno" 
									id="txtHouseno" placeholder="House No"  >
                            </div>
						

                            <div class="col-sm-4 form-group"> 
                                <label for="address">Street:<font color="red">*</font></label>
                                <input type="text" class="form-control" name="txtStreet" id="txtStreet" placeholder="Street" >    
                            </div>
							
								
							
							
							
						</div>	
							
							
						
							
					<div class="container">	
							
							
							
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address">Nearest Landmark:<font color="red">*</font></label>
                                <input type="text" class="form-control" maxlength="12" name="txtLandmark" 
									id="txtLandmark" placeholder="Nearest Landmark"  >

                            </div>
							<div class="col-sm-4 form-group">     
                                <label for="address">Road:<font color="red">*</font></label>
                                <textarea class="form-control" rows="4" id="txtRoad" name="txtRoad" placeholder="Road" ></textarea>

                            </div>
							
					<div class="col-sm-4 form-group">     
                                <label for="address">Address:<font color="red">*</font></label>
                                <textarea class="form-control" rows="4" id="txtAddress" name="txtAddress" placeholder="Address" ></textarea>

                            </div>
							
							
					</div>		
					
					

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Update" style='margin-left:10px'/>    
                        </div>
						
						
						
						<div id="gird"></div>
                 </div>
            </div>   
        </div>


    </form>





</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script src="scripts/uploadbirthproof.js"></script>

<script language="javascript" type="text/javascript">
function checkMarksheet(target) {
	var ext = $('#orgdocproof').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("orgdocproof").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			        
		alert("Image size should less or equal 200 KB");
		document.getElementById("orgdocproof").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("orgdocproof").value = '';
				return false;
	}    
    document.getElementById("orgdocproof").innerHTML = "";
    return true;
}
</script>

	
   <script type="text/javascript">
	function PreviewImage(no) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
		oFReader.onload = function (oFREvent) {
			document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
		};
	};
</script>	
    
	<script type="text/javascript"> 
 $('#txtEstdate').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true,
		autoclose: true
	});  
	
	
	
	//$("#txtEstdate").attr("disabled", true);
	
	
	
	</script>
 <script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
						if (Mode == 'Delete')
							{
								if (confirm("Do You Want To Delete This Item ?"))
								{
		                   		 deleteRecord();
		               		}
		           		 }
		           else if (Mode == 'Edit')
		           {
		                fillForm();
		           }
				   
				   function Getdetails()
                   {
					$.ajax({
						type: "post",
						url: "common/cfOrgDetail.php",
						data: "action=Getdetails",
						success: function (data) { 
							//alert(data);
							data = $.parseJSON(data);
							txtName1.value = data[0].Organization_Name;
							txtRegno.value = data[0].Organization_RegistrationNo;
							txtEstdate.value = data[0].Organization_FoundedDate;
							txtcode.value = data[0].Organization_Code;
							
						}
					});
                  }
                  Getdetails();
				   
				   
				   function FillDobdocs() {
            $.ajax({
                type: "post",
                url: "common/cfOrgDetail.php",
                data: "action=FillDobdocs",
                success: function (data) {
                    $("#ddlDobProof").html(data);
                }
            });
        }
        FillDobdocs();
				   
				   function GenerateUploadId()
                   {
					$.ajax({
						type: "post",
						url: "common/cfBlockUnblock.php",
						data: "action=GENERATEID",
						success: function (data) {                      
							txtGenerateId.value = data;					
						}
					});
                  }
                  GenerateUploadId();
					function deleteRecord()
					{
		               $('#response').empty();
		                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		               $.ajax({
	                    type: "post",
		                    url: "common/cfOrgDetail.php",
		                   data: "action=DELETE&values=" + OrganizationCode + "",
		                   success: function (data) {
		                       //alert(data);
		                        if (data == SuccessfullyDelete)
		                        {
		                            $('#response').empty();
		                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
		                           window.setTimeout(function () {
		                               window.location.href="frmorgdetail.php";
		                          }, 1000);
		                            
		                            Mode="Add";
		                            resetForm("frmOrgDeatil");
		                        }
		                        else
		                        {
		                            $('#response').empty();
		                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
		                        }
		                       showData();
		                    }
		               });
		            }
		
		
		            function fillForm()
		            {
		                $.ajax({
		                    type: "post",
		                    url: "common/cfOrgDetail.php",
		                    data: "action=EDIT&values=" + OrganizationCode + "",
		                    success: function (data) {
		                        //alert($.parseJSON(data)[0]['Status']);
		                       data = $.parseJSON(data);
							   //alert(data);
		                        txtName1.value = data[0].Organization_Name;
		                        txtRegno.value = data[0].Organization_RegistrationNo;
								txtEstdate.value = data[0].Organization_FoundedDate;
		                        txtType.value = data[0].Organization_Type;
		                        txtRoad.value = data[0].Organization_Road;
								txtStreet.value = data[0].Organization_Street;
		                        txtHouseno.value = data[0].Organization_HouseNo;
								txtLandmark.value = data[0].Organization_Landmark;
								txtAddress.value = data[0].Organization_Address;
								ddlDobProof.value = data[0].Organization_DocType;
								 
		                        
		                    }
		                });
		            }
					
				
		
		            function showData() {
		                
		                $.ajax({
		                    type: "post",
		                    url: "common/cfOrgDetail.php",
		                    data: "action=SHOW",
		                    success: function (data) {
		
		                        $("#gird").html(data);
								
								$('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
		
		                    }
		              });
		            }
		
		            showData();
		
		
						function FillCourse() {
						$.ajax({
							type: "post",
							url: "common/cfCourseMaster.php",
							data: "action=FILLCourseName",
							success: function (data) {
							$("#txtCourse").html(data);
							}
						});
						}

            FillCourse();	
		
		
		

                            $(function() {
                                $( "#txtEstdate" ).datepicker({
                                  changeMonth: true,
                                  changeYear: true
                                });
                              });
                              function FillOrgType() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfOrgTypeMaster.php",
                                    data: "action=FILL",
                                    success: function (data) {
                                        $("#txtType").html(data);
                                    }
                                });
                            }
                             FillOrgType();
							 
							  $("#txtType").change(function(){
				var selorgtype = $(this).val(); 
				//alert(selcountry);
				$.ajax({
			          url: 'common/cfOrgDetail.php',
			          type: "post",
			          data: "action=FILL",
			          success: function(data){
						//alert(data);
						$('#txtDoctype').html(data);
			          }
			        });
                            });
							 
							 
                        	
							
			
                          

							
							
							
							
			$("#btnSubmit").click(function () {
			
			if ($("#frmOrgDeatil").valid())
            {
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfOrgDetail.php"; // the script where you handle the form input.
				var birthproof=$('#txtGenerateId').val(); 
                var data;
                var forminput=$("#frmOrgDeatil").serialize();
                if (Mode == 'Add')
                {
                    data = "action=UPDATE&birthproof=" + birthproof + "&" + forminput; // serializes the form's elements.
                }
                else
                {
					
					data = "action=UPDATE&code=" + OrganizationCode +"&" + forminput;
                    //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                             window.setTimeout(function () {
                                window.location.href = "frmorgdetail.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmorgdetail");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			 }
                return false; // avoid to execute the actual submit of the form.
            });

                            
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmorgmastervalidation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>