<?php
$title = "Eligible IT-GK for Renewal";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}

if ($_SESSION['User_UserRoll'] == '1') {	
?>

<style>
    #myProgress {
        width: 100%;
        background-color: #ddd;
    }

    #myBar {
        width: 1%;
        height: 30px;
        background-color: #4CAF50;
        text-align: center;
        line-height: 30px;
        color: white;
    }
</style>

<div style="min-height:430px !important;max-height:auto !important">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">IT-GK Eligible for Renewal</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmslaprocess" id="frmslaprocess" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">


                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-8">     
                            <label for="cname">List of IT-GK Eligible for Renewal as on TODAY</label>
                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Generate Report" style='margin-left:10px'/>    

                        </div>	
                    </div>
                    <br>
                    <div id="myProgress" style="display:none;">
                        <div id="myBar">Loading - 0%</div>
                    </div>
                    <br>
                    <!--<div id="response"></div>-->
                    <div class="panel panel-success" style="display:none;" id="report">
                        <div class="panel-heading">SLA Report as on TODAY</div>
                        <div class="panel-body">
                            <div id="grid" name="grid" style="margin-top:35px;"> </div> 
                        </div>
                    </div>

                    <div class="col-sm-4" id="uploadsla" style="display:none">     
                        <label for="cname">Click here to Upload SLA</label>
                        <input type="button" name="btnProcess" id="btnProcess" class="btn btn-primary" value="Upload SLA" style='margin-left:10px'/>    

                    </div>
                </form>

            </div>
        </div>   
    </div>
</div>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function move() {
            var elem = document.getElementById("myBar");
            var width = 0;
            var id = setInterval(frame, 500);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                    elem.innerHTML = width * 1 + '%';
                }
            }
        }

        $("#btnSubmit").click(function () {
            $('#report').hide();
            $('#myProgress').show();
            move();
            FillItgkList();

            function FillItgkList() {
                //alert("gi");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfSlaProcess.php",
                    data: "action=DETAILS",
                    success: function (data)
                    {
                        $('#myProgress').hide();
                        $('#report').show(3000);
                        $('#uploadsla').show(5000);
                        //alert(data);
                        $('#response').empty();
                        $("#grid").html(data);
                        $('#example').DataTable({
                            scrollY: 400,
                            scrollX: 100,
                            scrollCollapse: true,
                            paging: false,
//                            dom: 'Bfrtip',
//                            buttons: [
//                                'copy', 'csv', 'excel', 'pdf', 'print'
//                            ]
                        });
                    }
                });
            }
        });
        
        $("#grid").on("click",".viewdata",function(){
			var mybtn = $(this).attr("value");
			var btnnn = '#chkbook';
			var add = btnnn + mybtn;

			if (this.checked) {
				$(add).attr("disabled", false);	
			} else {
			$(add).attr("disabled", true);
			$(add).removeAttr('checked');
		  }
		});
        
        $('#grid').on('click', '#checkuncheckall', function (){
            //checkuncheckall(this.checked);
			if (this.checked){
				$('.viewdata').not(this).prop('checked', this.checked);
				$('.bookdata').not(this).prop('checked', this.checked);
				//$('.bookdata').attr("disabled", false);	
			}
			else {
				$('.bookdata').attr("checked", false);
				$('.viewdata').attr("checked", false);
				//$('.bookdata').attr("disabled", true);
			}
			
        });
        
//        function checkuncheckall(checked) {
//        var aa = document.getElementById('frmfeepayment');		
//        for (var i =0; i < aa.elements.length; i++) {
//            aa.elements[i].checked = checked;
//        }
//    }

        $("#btnProcess").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfSlaProcess.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmslaprocess").serialize();
            //alert(forminput);			

            if (Mode == 'Add')
            {
                data = "action=ADD&" + forminput; // serializes the form's elements.
            } else
            {
                //data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; SLA Uploaded Successfully.</span>");
                        window.setTimeout(function () {
                            window.location.href = "frmslaprocess.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("frmslaprocess");
                    } else if(data == 'NoDetails'){
                       $('#response').empty();
                       $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>"); 
                       BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Please select at least one check box.</span>");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });





        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>