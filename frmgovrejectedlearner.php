<?php
$title="Govt. Rejected Learner List";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var FunctionCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
?>
<div style="min-height:430px !important;max-height:1500px !important;">
	 <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Govt. Reimbursement Rejected Learner</div>
                <div class="panel-body">
				   <form name="frmgovrejectedlist" id="frmgovrejectedlist" class="form-inline" role="form" enctype="multipart/form-data">
					<div class="container">
						<div class="container">						
							<div id="response"></div>
						</div>
							<div id="errorBox"></div>								
                    </div>				
					
                         <div id="gird" style="margin-top:5px;"> </div>                   
                 </div>
            </div>   
        </div>
	</form>
    </div>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>                
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
			
            function showData() {			 
                $('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			    $.ajax({							   
                    type: "post",
                    url: "common/cfGovtRejectedLearner.php",
                    data: "action=ShowRejectedLearnerList",
                    success: function (data) {
						$('#response').empty();
                            //alert(data);
                        $("#gird").html(data);
						$('#example').DataTable();
                    }
                });
            return false;
            }
            showData();
        });
    </script>	
</html>