<?php
$title = "User Details";
include ('header.php');
include ('root_menu.php');
?>
<style type="text/css">

form > div.container {
   filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
}

    .modal-open .container-fluid, .modal-open .container{
        filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
    }
</style>
<div style="min-height:450px !important;max-height:1500px !important">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Visit Requests</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="frmvisitdetails" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>
                        <div id="errorBox"></div>
                    </div>
                    <div id="grid" name="grid" style="margin-top:35px;"> </div>
                    </form>
            </div>
        </div>   
    </div>

<div id="confirm_visit_form" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 900;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Confirm visit</h4>
            </div>
            <div class="modal-body">
                <form name="form" id="frmvisitconfirm" class="form-inline" role="form" enctype="multipart/form-data">
                        <div class="container">
                            <div id="frmvisit-response" class="star"></div>
                            <input type="hidden" name="processvisitId" id="processvisitId"/>
                        </div> 
                        <div id="visit-grid" name="visit-grid" class="small" style="margin-top:35px;"> </div>
                        <div class="container">
	                        <div class="col-md-4 form-group">
<!--	                            <label for="deviceType">Select bio-metric device :<span class="star">*</span></label>
	                            <select name="deviceType" id="deviceType" class="form-control biomval">
	                                <option value="">- - - Please Select - - -</option>
	                            </select>-->
	                            <span class="star bioerr"></span>
                                <input type="hidden" class="biomval" name="flag" id="flag"/>
	                        <input type="hidden" class="biomval" name="txtStatus" id="txtStatus"/>
                                <input type="hidden" class="biomval" name="tdLocalIP" id="tdLocalIP"/>
                                <input type="hidden" class="biomval" name="tdLocalMac" id="tdLocalMac"/>
                                <input type="hidden" class="biomval" name="tdHeight" id="tdHeight"/>
                                <input type="hidden" class="biomval" name="tdWidth" id="tdWidth"/>
                                <input type="hidden" class="biomval" name="tdModel" id="tdModel"/>
                                <input type="hidden" class="biomval" name="tdMake" id="tdMake"/>
                                <input type="hidden" class="biomval" name="tdSerial" id="tdSerial"/>

                                <input type="hidden" name="txtStatus" id="txtStatus"/>
                                <input type="hidden" name="txtQuality" id="txtQuality"/>
                                <input type="hidden" name="txtNFIQ" id="txtNFIQ"/>
                                <input type="hidden" name="txtIsoTemplate" id="txtIsoTemplate"/>
                                <input type="hidden" name="txtIsoImage" id="txtIsoImage"/>
                                <input type="hidden" name="txtRawData" id="txtRawData"/>
                                <input type="hidden" name="txtWsqData" id="txtWsqData"/>
                                <div style="display:none;">
                                    <img id="imgFinger" name="imgFinger" class="form-control" style="width:80px; height:107px;"/>
                                </div>
	                        </div>
	                    
                    <!--         <div class="col-sm-4 form-group"  id="non-printable"> 
                                <center><input type="button" id="itgk_impression" class="itgk_impression" value="Capture (ITGK)" name="itgk_impression" style="margin-top: 25px;" />  <br />
                                <img id="imgFinger_itgk" name="imgFinger_itgk" class="form-control" style="width:80px; height:107px;"/><br /><span class="star" id="match_itgk"></span></center>
                                <input type="hidden" name="matched_itgk" id="matched_itgk"/>
                            </div>
                            <div class="col-md-4 form-group">     
                                <input type="button" id="visitor_impression" class="visitor_impression" value="Capture (Visitor)" name="visitor_impression" style="margin-top: 25px;" /> <br />
                                <img id="imgFinger_visitor" name="imgFinger_visitor" class="form-control" style="width:80px; height:107px; margin-left: 20px;"/><br /><span class="star" id="match_visitor"></span>
                                <input type="hidden" name="matched_visitor" id="matched_visitor"/>
                             </div>-->

                             <div class="col-md-4 form-group" id="showData">   
                                <input type="button" name="setConfirmBtn" id="setConfirmBtn" class="btn btn-primary" value="Confirm Visit" style="margin-top: 25px; margin-left: -20px;" />
                             </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</body>
<?php 
	include'common/message.php';
	include ('footer.php'); 
	include 'common/modals.php';
    $mac = explode(" ", exec('getmac'));
?>
<style>
    #errorBox{
        color:#F00;
    }
    .aslink {
        cursor: pointer;
    }
</style>
<script src="js/mfs100.js"></script>
<script src="js/star.js"></script>
<script src="js/cogent.js"></script>
<script src="js/tatvik2.js"></script>
<script src="js/visitcontroller.js"></script>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    var quality = 60; //(1 to 100) (recommanded minimum 55)
    var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )
    var localIP = "<?php echo getHostByName(getHostName());?>";
    var localMAC = "<?php echo str_ireplace('-', '', $mac[0]);?>";

    function downloadFeedback(VCode) {
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=doAction&id=dfb-" + VCode,
            success: function (data) {
                data = data.trim();
                if (data != '') {
                    //alert(data);
                    window.location = 'downloadfile.php?path=' + data;
                }
            }
        });
    }

    function listvisits(vid, grid, response, example) {
        $("#" + grid).empty();
        $('#' + response).empty();
        $('#' + response).append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=listcentervisits&visitId=" + vid,
            success: function (data) {
                data = data.trim();
                //alert(data);
                $('#' + response).empty();
                $("#" + grid).html(data);
                $("#processvisitId").val(vid);
                $('#' + example).DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ],
                    scrollY: 400,
                    scrollCollapse: true,
                    paging: false
                });
            }
        });
    }

    function performAction(eObj) {
        var act = eObj.id.split('-');
        if (act[0] == 'confirm') {
            openVisitConfirmForm(act[1]);
        } else if (act[0] == 'dfb') {
            downloadFeedback(act[1]);
        }
    }

    function openVisitConfirmForm(vid) {
        listvisits(vid, 'visit-grid', 'frmvisit-response', 'example2')
        $("#confirm_visit_form").modal("show");
    }

    function validateMatchedBioPics(bytype) {
        var ismatched = 0;
        var mval = $('#matched_' + bytype).val();
        if (mval >= 1) {
            ismatched = mval;
        } else {
            var matchid = '#match_' + bytype;
            $(matchid).html("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Not Matched!!</span></p>");
        }

        return ismatched;
    }

    function setVisitConfirmed(matched_itgk, matched_visitor) {
        $("#please_wait").modal("show");
        var vid = $("#processvisitId").val();
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=changeVisitStatus&visitId=" + vid + "&mtigk=" + matched_itgk + "&mvisitor=" + matched_visitor,
            success: function (data) {
                data = data.trim();
                $("#please_wait").modal("hide");
                if (data == SuccessfullyUpdate) {
                    setDeviceInfoEmpty();
                    $("#confirm_visit_form").modal("hide");
                    $("#successfully_submitted").modal("show");
                } else {
                    $("#errorText").empty();
                    $("#errorText").html(data);
                    $("#errorModal_Custom").modal("show");
                }
            }
        });
    }

    $(document).ready(function () {
        
        listvisits(0, 'grid', 'response', 'example');

        filldeviceTypes();

        $("#setConfirmBtn").click(function () {
			    var vid = $("#processvisitId").val();
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=GetDetails&visitId=" + vid,
            success: function (data) {
                data = data.trim();
                data = $.parseJSON(data);
                var matched_itgk = data[0].itgk;
                var matched_visitor = data[0].visitor;
           
            setVisitConfirmed(matched_itgk, matched_visitor);
            }
        });
//            var ifvalid = $('#frmvisitconfirm').valid();
//            if (ifvalid) {
//                var matched_itgk = validateMatchedBioPics('itgk');
//                var matched_visitor = validateMatchedBioPics('visitor');
//                if (matched_itgk && matched_visitor) {
//                    setVisitConfirmed(matched_itgk, matched_visitor);
//                }
//            }
        });

        $("#deviceType").change(function () {
            resetCapture('');
            getDeviceInfo(this.value);
        });

        $("#itgk_impression").click(function () {
            resetCapture('itgk');
            reDetectMachine('itgk');
        });

        $("#visitor_impression").click(function () {
            resetCapture('visitor');
            reDetectMachine('visitor');
        });

        $("#grid").on('click', '.aslink', function () {
            $('#match_itgk').empty();
            $('#match_visitor').empty();
            $('#matched_itgk').val('');
            $('#matched_visitor').val('');
            setDeviceInfoEmpty();
            performAction(this);
        });

        $("#successfully_submitted").on('click', '.btn-default', function () {
            window.location.reload();
        });

        $("#successfully_submitted").on('click', '.close', function () {
            window.location.reload();
        });

    });

</script>
<script src="js/frmvisitconfirm_validation.js" type="text/javascript"></script>
</html>