<?php
$title = "Print Receipt";
include ('header.php');
include ('root_menu.php');
$_SESSION['ImageFile'] = "";
$_SESSION['SignFile'] = "";
$_SESSION['ScanFile'] = "";
if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Download and Print Receipt</div>
            <div class="panel-body">
                <form name="frmprintrecpsummary" id="frmprintrecpsummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                        <div class="col-sm-4 form-group">     
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">

                            </select>

                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>

                        </div> 
                    </div>

                    <div id="menuList" name="menuList" style="margin-top:35px;"> </div> 

                    <div class="container">
                    <!--	<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    -->
                    </div>
            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLAdmissionSummaryCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();


        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });




        function showAllData(val, val1) {
            //alert(val);
            //alert(val1);
            $.ajax({
                type: "post",
                url: "common/cfLIVRrpt_2001.php",
                data: "action=SHOWALLPrintRecp&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#menuList").html(data);
                    $('#example').DataTable();
                }
            });
        }

        $("#ddlBatch").change(function () {
            showAllData(this.value, ddlCourse.value);
        });

        if (Mode === 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAdmissionModify.php",
                data: "action=DELETE&values=" + Admission_Name + "&batchcode=" + BatchCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmModifyAdmission.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmmodifyadmission");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();
                }
            });
        }


        $("#btnSubmits").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfPermissionMaster.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmpermissionmaster").serialize();
            //alert(forminput);




            if (Mode == 'Add')
            {
                data = "action=ADD&" + forminput; // serializes the form's elements.
            } else
            {
                data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmpermissionmaster.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmpermissionmaster");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });

        $("#menuList").on('click', '.approvalLetter', function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            var lid = $(this).attr('id');
            $.ajax({
                type: "post",
                url: "common/cfLIVRrpt_2001.php",
                data: "action=DwnldPrintRecp&lid=" + lid,
                success: function (data) {
                    $('#response').empty();
                    window.open(data, '_blank');
                    //window.setTimeout(function () {
                    //    delgeninvoice(data);
                   // }, 3000);

                }
            });
        });

        function delgeninvoice(data) {

            $.ajax({
                type: "post",
                url: "common/cfLIVRrpt_2001.php",
                data: "action=delgeninvoice&values=" + data,
                success: function (data) {
                }
            });
        }
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>