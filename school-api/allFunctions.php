<?php
/* Hariom Android API For School Easy */
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
$json = file_get_contents('php://input');
$data = json_decode($json, TRUE);
//$response = array("error" => FALSE);
if (isset($data['FunctionName'])) {
    $FunctionName = $data['FunctionName'];

    /* Get Login Details */
    if ($FunctionName == "GetLoginDetails") {
        $UserName = $data['username'];
        $Password = $data['password'];
        $User_Device_Id = $data['user_device_id'];
        $_User = $db->checkLogin($UserName, $Password, $User_Device_Id);
        if ($_User) {
            $response["status"] = "OK";
            $response["successMessage"] = "success";
            $response["errorMessage"] = "";
            $response["statusCode"] = 200;
            if ($_User['User_UserRoll'] != 1 || $_User['User_UserRoll'] != 3) {
                $_Response_school_code = $db->findSessionId($_User['User_Code'], $_User['User_UserRoll']);
                $_User['school_code'] = $_Response_school_code['school_code'];
            }
            $response["data"] = $_User;
            echo json_encode($response);
        } else {
            $response["status"] = "failed";
            $response["successMessage"] = "";
            $response["errorMessage"] = "User Authentication Failed.";
            $response["statusCode"] = "201";
            $response["data"] = null;
            echo json_encode($response);
        }
    }

    /* Get Student Profile Detail */
    if ($FunctionName == "GetStudentProfile") {
        $UserCode = $data['User_Code'];
        $UserStatus = $data['User_Status'];
        $UserRoll = $data['User_UserRoll'];
        //$User_Device_Id = $data['user_device_id'];
        $_User = $db->getStudentProfile($UserCode, $UserStatus, $UserRoll);
        if ($_User) {
            $response["status"] = "OK";
            $response["successMessage"] = "success";
            $response["errorMessage"] = "";
            $response["statusCode"] = 200;
            $response["data"] = $_User;
            echo json_encode($response);
        } else {
            $response["status"] = "failed";
            $response["successMessage"] = "";
            $response["errorMessage"] = "No Record Found!";
            $response["statusCode"] = "201";
            $response["data"] = null;
            echo json_encode($response);
        }
    }

    /* Get LMS List For Student */
    if ($FunctionName == "GetLMS_Student_List") {
        $student_id = $data['student_id'];
        $status= $data['status'];
        $_User = $db->getStudentLMSList($student_id, $status);
        if ($_User) {

            $response["status"] = "OK";
            $response["successMessage"] = "success";
            $response["errorMessage"] = "";
            $response["statusCode"] = 200;
            $response["data"] = $_User;
            echo json_encode($response);
        } else {
            $response["status"] = "failed";
            $response["successMessage"] = "";
            $response["errorMessage"] = "No Record Found!";
            $response["statusCode"] = "201";
            $response["data"] = null;
            echo json_encode($response);
        }
    }

    /* Get LMS View For Student */
    if ($FunctionName == "GetLMS_Student_Content_View") {
        $student_id = $data['student_id'];
        $status= $data['status'];
        $_User = $db->getStudentLMSContentList($student_id, $status);
        if ($_User) {
            $response['subject_name'] = $_User['subject_name'];
            $response['medium_id'] = $_User['medium_id'];
            $response['package_name'] = $_User['package_name'];
            $response["status"] = "Success";
            //print_r($_User); die;
            echo json_encode($response);
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "No Record Found!";
            echo json_encode($response);
        }
    }
    
    #### SUNIL KUMAR BAINDARA#####
    
    if ($FunctionName == "GetQueryList") {
        $UserCode = $data['User_Code'];
        $UserStatus = $data['User_Status'];
        $UserRoll = $data['User_UserRoll'];
        //$User_Device_Id = $data['user_device_id'];
        $_User = $db->getStudentProfile($UserCode, $UserStatus, $UserRoll);
        if ($_User) {
            $response["status"] = "OK";
            $response["successMessage"] = "success";
            $response["errorMessage"] = "";
            $response["statusCode"] = 200;
            $response["data"] = $_User;
            echo json_encode($response);
        } else {
            $response["status"] = "failed";
            $response["successMessage"] = "";
            $response["errorMessage"] = "No Record Found!";
            $response["statusCode"] = "201";
            $response["data"] = null;
            echo json_encode($response);
        }
    }
    
    
    #### SUNIL KUMAR BAINDARA#####
    
    
    

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters missing!";
    echo json_encode($response);
}
?>
