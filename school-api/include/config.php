<?php

define("DB_HOST", ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "127.0.0.1") ? "10.1.1.158" : "");
define("DB_USER", ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "127.0.0.1") ? "rkclcoin" : "");
define("DB_PASSWORD", ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "127.0.0.1") ? "naresh@123" : "");
define("DB_DATABASE", ($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "127.0.0.1") ? "database_rkcl_school" : "");

/* * * PATH CONFIGURATION ** */

define("BASE_PATH", ($_SERVER['HTTP_HOST'] == "localhost") ? "http://localhost/school/" : "http://school.myrkcl.com");
define("BASE_PATH_MENU", ($_SERVER['HTTP_HOST'] == "localhost") ? "http://localhost" : "http://school.myrkcl.com");
define("PLUGIN_PATH", BASE_PATH . "plugins");
define("ASSET_PATH", BASE_PATH . "assets");
define("UPLOAD_PATH", BASE_PATH . "/upload");
define("DIR_PATH", $_SERVER["DOCUMENT_ROOT"] . '/school/');
define("NONE_PAGE", BASE_PATH . "404.php");

/* * * KEY CONFIGURATION ** */
if (!defined('SECRET_KEY')) define("SECRET_KEY", "SchoolManagementSystem");

/* * * USER ROLES DEFINED HERE ** */
if (!defined('SUPER_ADMIN_ROLL')) define("SUPER_ADMIN_ROLL", 1);
if (!defined('SCHOOL_ADMIN_ROLL')) define("SCHOOL_ADMIN_ROLL", 2);
if (!defined('SCHOOL_OWNER_ROLL')) define("SCHOOL_OWNER_ROLL", 3);
if (!defined('STUDENT_ROLL')) define("STUDENT_ROLL", 4);
if (!defined('PARENT_ROLL')) define("PARENT_ROLL", 5);
if (!defined('STAFF_ROLL')) define("STAFF_ROLL", 6);
if (!defined('PROJECT_NAME')) define("PROJECT_NAME", "School Easy");

/* * ** USER STATUS *** */

if (!defined('USER_ACTIVE')) define("USER_ACTIVE", 1);
if (!defined('USER_INACTIVE')) define("USER_INACTIVE", 2);


/*   Student Process Status */
if (!defined('STUDENT_REGISTER')) define("STUDENT_REGISTER", 0);
if (!defined('STUDENT_ADMISSION')) define("STUDENT_ADMISSION", 1);
if (!defined('STUDENT_SUCCESSFULLY_REGISTER')) define("STUDENT_SUCCESSFULLY_REGISTER", 2);

/* Set India Time Zone */
date_default_timezone_set('Asia/Kolkata');

//error_reporting(0);
//ini_set('display_errors', 0);
if (!defined('STUDENTFEEINFO')) define("STUDENTFEEINFO", "studentfee");

// for payu payment gateway
if (!defined('PAY_MODE')) define("PAY_MODE", "TEST"); // TEST //LIVE
//TEST //LIVE
if (PAY_MODE == "TEST") {
    if (!defined('KEY')) define("KEY", "gtKFFx");
    if (!defined('SALT')) define("SALT", "eCwWELxi");
    if (!defined('PAYCARDURL')) define("PAYCARDURL", "https://test.payu.in/merchant/postservice.php?form=2");
    if (!defined('PAYU_BASE_URL')) define("PAYU_BASE_URL", "https://test.payu.in");
    if (!defined('SUCCESSPAYUURL')) define("SUCCESSPAYUURL", 'http://localhost/school/school/frmpaysuccess.php');
    if (!defined('FAILUREPAYUURL')) define("FAILUREPAYUURL", 'http://localhost/school/school/frmpayfailure.php');
} else {
    // if (!defined('KEY')) define("KEY", "X2ZPKM");
    // if (!defined('SALT')) define("SALT", "8IaBELXB");
    if (!defined('PAYCARDURL')) define("PAYCARDURL", "https://info.payu.in/merchant/postservice?form=2");
    if (!defined('PAYU_BASE_URL')) define("PAYU_BASE_URL", "https://secure.payu.in");
    //if (!defined('SUCCESSPAYUURL')) define("SUCCESSPAYUURL", 'https://myrkcl.com/frmpaysuccess.php');
    //if (!defined('FAILUREPAYUURL')) define("FAILUREPAYUURL", 'https://myrkcl.com/frmpayfailure.php');
    if (!defined('SUCCESSPAYUURL')) define("SUCCESSPAYUURL", 'http://localhost/school/school/frmpaysuccess.php');
    if (!defined('FAILUREPAYUURL')) define("FAILUREPAYUURL", 'http://localhost/school/school/frmpayfailure.php');

}
// Message Type
if (!defined('STDFINDENROLL')) define("STDFINDENROLL", "FEEENROLL");
if (!defined('FEEPAY')) define("FEEPAY", "FEEPAY");
define("RKCL_SMS", 0);
define("STUDENT_REGISTRATION_SMS", 1);
define("OWNER_REGISTRATION_SMS", 2);
define("PARENT_REGISTRATION_SMS", 3);
define("STAFF_REGISTRATION_SMS", 4);
define("STAFF_DETAIL_UPDATE_SMS", 5);
define("PARENT_REGISTRATION_STUDENT_UPDATE_SMS", 6);
define("STUDENT_BIRTHDAY_WISH_SMS", 7);
define("OWNER_REGISTER_INNER_SMS", 8);
define("OWNER_STATUS_UPDATE_SMS", 9);
define("OWNER_DEACTIVATED_SMS", 10);
define("OWNER_ACTIVATION_CODE_SMS", 11);
define("OTP_OWNER_REGISTER_OUTER_SMS", 12);
define("OWNER_REGISTER_OUTER_SMS", 13);
define("SCHOOL_STATUS_DEACTIVATE_SMS", 14);
define("SCHOOL_DETAIL_UPDATE_SMS", 15);
define("SCHOOL_REGISTER_SMS", 16);
define("OTP_FORGOT_PASSWORD_SMS", 17);
define("SCHOOL_NEW_ASSIGNMENT_STUDENT_SMS", 18);
define("SCHOOL_NEW_ASSIGNMENT_PARENT_SMS", 19);

?>