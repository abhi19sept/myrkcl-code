<?php
ini_set("memory_limit", "-1");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
require_once "sendsms.php";
include_once "sendEmail.php";

class DB_Functions
{
    private $conn;

    function __construct()
    {
        require_once 'DB_Connect.php';
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }

    /* Check Login Detail */
    public function checkLogin($UserName, $Password, $User_Device_Id)
    {
        $stmt = $this->conn->prepare("Select a.*, b.* FROM tbl_user_master as a
                LEFT JOIN tbl_userroll_master AS b ON a.User_UserRoll = b.UserRoll_Code
                WHERE User_LoginId = '" . $UserName . "' AND User_Status = 1");
        //$stmt->bind_param("s", $UserName);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            $encrypted_password = $user['encrypted_password'];
            if ($encrypted_password == $Password) {
                $stmt3 = $this->conn->prepare("update tbl_user_master SET User_Device_Id = '" . $User_Device_Id . "' where User_LoginId='" . $UserName . "'");
                $stmt3->execute();
                $stmt3->close();
                return $user;
            }
        } else {
            return NULL;
        }
    }

    /* Get School Id After Login */
    public function findSessionId($user_code, $User_UserRoll)
    {
        if ($User_UserRoll == 2) {
            $stmt = $this->conn->prepare("SELECT school_code FROM tbl_school_master WHERE school_user_code = '" . $user_code . "'");
        } elseif ($User_UserRoll == 4) {
            $stmt = $this->conn->prepare("SELECT school_code FROM tbl_student_master as a 
INNER JOIN tbl_school_master as b ON a.fld_school_id = b.school_code
WHERE fld_student_id = '" . $user_code . "'");
        } elseif ($User_UserRoll == 5) {
            $stmt = $this->conn->prepare("SELECT school_code FROM tbl_student_parents_details as a 
INNER JOIN tbl_student_master as b ON a.fld_student_id = b.fld_student_id
INNER JOIN tbl_school_master as c ON b.fld_school_id = c.school_code
WHERE fld_parent_master_code = '" . $user_code . "'");
        } elseif ($User_UserRoll == 6) {
            $stmt = $this->conn->prepare("SELECT fld_school_id as school_code FROM tbl_staff_detail
WHERE fld_staff_id = '" . $user_code . "'");
        }
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;

        } else {
            return NULL;
        }

    }

    /* Get Student Details */
    public function getStudentProfile($UserCode, $UserStatus, $UserRoll)
    {

            $stmt = $this->conn->prepare("SELECT fld_registration_no, fld_studentName, gender_name, User_LoginId, User_EmailId, User_MobileNo, fld_dob, fld_placeOfBirth, f.class_name as current_class, g.section_name as section_name, caste_category_name, religion_name, fld_caste, 
fld_nationality,
 fld_motherTongue, blood_group, fld_dob, fld_image, title_name, fld_admission_date,fld_student_aadhar, k.session_year_name as session_year_name, medium_name, fld_prev_school, fld_birth_certificate, fld_caste_certificate, fld_prev_school_marksheet, fld_handicapped, admission_type, fld_educational_gap, fld_prev_school_enroll_no, fld_prev_school_start_date, fld_prev_school_end_date, fld_reason_of_leaving, 
 fld_present_address, p.Country_Name as present_Country, q.State_Name as present_State, r.District_Name as present_District, fld_present_city, fld_present_zip_code, fld_permanent_address, s.Country_Name as permanent_Country, t.State_Name as permanent_State, u.District_Name as permanent_District, fld_permanent_city, fld_permanent_zip_code, fld_father_name, w.qualification as father_qualification, fld_father_age, 
 x.occupation as father_occupation, fld_father_office_address, fld_father_email, fld_father_contact, fld_mother_name, y.qualification as mother_qualification, fld_mother_age, fld_gaurdian_name, fld_gaurdian_contact, fld_gaurdian_address

from tbl_user_master as a
LEFT JOIN  tbl_student_master as b on b.fld_student_id = a.User_Code
LEFT JOIN  tbl_title_master as c on c.id = b.fld_title
LEFT JOIN  tbl_student_admission_details as d on d.fld_student_id = a.User_Code
LEFT JOIN  tbl_gender as e on e.id = b.fld_gender
LEFT JOIN  tbl_caste_category as h on h.id = b.fld_studentCategory
LEFT JOIN  tbl_religion as i on i.id = b.fld_studentReligion
LEFT JOIN  tbl_blood_group as j on j.id = b.fld_bloodGroup
LEFT JOIN  tbl_language_medium as l on l.id = d.fld_medium
LEFT JOIN  tbl_admission_type as n on n.id = d.fld_admission_type
LEFT JOIN  tbl_student_address_details as o on o.fld_student_id = a.User_Code
LEFT JOIN  tbl_country_master as p on p.Country_Code = o.fld_present_country
LEFT JOIN  tbl_state_master as q on q.State_Code = o.fld_present_state
LEFT JOIN  tbl_district_master r ON r.District_Code = o.fld_present_district
LEFT JOIN  tbl_country_master as s on s.Country_Code = o.fld_permanent_country
LEFT JOIN  tbl_state_master as t on t.State_Code = o.fld_permanent_state
LEFT JOIN  tbl_district_master u ON u.District_Code = o.fld_permanent_district
LEFT JOIN  tbl_student_parents_details v ON v.fld_student_id = a.User_Code
LEFT JOIN  tbl_qualification w ON w.id = v.fld_father_qualification
LEFT JOIN  tbl_occupation x ON x.id = v.fld_father_occupation
LEFT JOIN  tbl_qualification y ON y.id = v.fld_mother_qualification
LEFT JOIN  tbl_student_class_mapping as z on z.fld_student_id = b.fld_student_id
LEFT JOIN  tbl_class_master as f on f.id = z.fld_class_id
LEFT JOIN  tbl_section_master as g on g.id = fld_section_id
LEFT JOIN  tbl_school_session as k on k.id = z.fld_session_id



 Where User_Code= '".$UserCode."' AND User_Status = '".$UserStatus."' AND User_UserRoll = '".$UserRoll."'");

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;

        } else {
            return NULL;
        }

    }

    /* Get LMS List For Student */
    public function getStudentLMSList($student_id, $status)
    {
        $stmt = $this->conn->prepare("SELECT b.id,b.package_name,c.subject_name,b.medium_id FROM tbl_student_class_mapping a
LEFT JOIN tbl_student_package b ON b.fld_class_id = a.fld_class_id 
AND a.fld_section_id = b.fld_section_id AND a.fld_school_id = b.fld_school_id AND a.fld_session_id = b.fld_session_id
LEFT JOIN tbl_subject_master c ON c.id = b.fld_subject_id
WHERE a.fld_student_id = $student_id AND b.ddlStatus = $status");
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

/* Get LMS Content List For Student */
    public function getStudentLMSContentList($student_id, $status)
    {
        $stmt = $this->conn->prepare("SELECT b.id,b.package_name,c.subject_name,b.medium_id FROM tbl_student_class_mapping a
LEFT JOIN tbl_student_package b ON b.fld_class_id = a.fld_class_id 
AND a.fld_section_id = b.fld_section_id AND a.fld_school_id = b.fld_school_id AND a.fld_session_id = b.fld_session_id
LEFT JOIN tbl_subject_master c ON c.id = b.fld_subject_id
WHERE a.fld_student_id = $student_id AND b.ddlStatus = $status");
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }


}

?>
