<?php
$title = "Correction/Duplicate Certificate Update Rejected Form";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}

if ($_SESSION['User_Code'] == '1' || $_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '19') {
//print_r($_SESSION);
echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
?>

<link rel="stylesheet" href="bootcss/css/bootstrap-datetimepicker.min.css">
    <script src="bootcss/js/moment.min.js"></script>
    <script src="bootcss/js/bootstrap-datetimepicker.min.js"></script>

<div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">Correction/Duplicate Certificate Update Rejected Form</div>
        <div class="panel-body">
            <form name="frmcorrectionreject" id="frmcorrectionreject" class="form-inline" role="form"
				enctype="multipart/form-data">
				<div id="response"></div>
			
				<div id="menuList" name="menuList" style="margin-top:35px;"> </div> 
        </div>
    </div>   
</div>
</form>
</div>

<div id="LearnerDetails" class="modal" style="padding-top:50px !important">            
	<div class="modal-content" style="width: 90%;">
		<div class="modal-header">
			<span class="close mm">&times;</span>
				<h6>Update Rejected Learner Application</h6>
		</div>
		<div class="modal-body" style="max-height: 500px;">
	<form name="frmprocess" action="common/cfCorrectionUpdateRejectedLearner.php" id="frmprocess" class="form-inline"
			role="form" enctype="multipart/form-data">
		<div id="responses"></div>
			<div class="container">
				<div class="col-sm-4 form-group">     
							  <label for="learnercode">Learner Code:<span class="star">*</span></label>
							  <input type="text" class="form-control" maxlength="20" name="txtLearnerCode" id="txtLearnerCode"
								placeholder="Learner Code" readonly='true'>							 
				</div>
				
				<div class="col-sm-4 form-group">     
						  <label for="mobile_no">Learner's Mobile No:<span class="star">*</span></label>
						  <input type="text" class="form-control" maxlength="10" id="txtMobile" name="txtMobile"
							onkeypress="javascript:return allownumbers(event);" placeholder="Mobile No" readonly="true">
				</div>
							
				<div class="col-sm-4 form-group"> 
					<label for="learnername">Learner Correct Name:<span class="star">*</span></label>
						<input type="text" class="form-control text-uppercase" maxlength="50" name="txtLCorrectName"
						id="txtLCorrectName" onkeypress="javascript:return allowchar(event);"
						placeholder="Learner Correct Name">					
				</div>
					
			   <div class="col-sm-4 form-group">     
					<label for="faname">Father/Husband Correct Name:<span class="star">*</span></label>
						<input type="text" class="form-control text-uppercase" id="txtFCorrectName" name="txtFCorrectName"
							onkeypress="javascript:return allowchar(event);" placeholder="Father Correct Name">
							<input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
							<input type="hidden" class="form-control" name="txtcid" id="txtcid"/>
							<input type="hidden" class="form-control" maxlength="50" name="action" id="action" value="Update"/>
							<input type="hidden" class="form-control" maxlength="50" name="txtLCode" id="txtLCode"/>
			   </div>					
			   
			</div>
			
			<div class="container">
				<div class="col-sm-4 form-group" id="pic"> 
					<div id="uploadPreview9">
                                    
                    </div>
					<label for="photo">Learner Photo:<span class="star">*</span></label> </br>
					<img id="uploadPreview1" src="images/user icon big.png" id="uploadPreview1" name="filePhoto" 
						width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">
				</div>
			
			
				  <div class="col-sm-4 form-group" id="photodiv"> 
					  <label for="photo">Attach Learner's Photo:<span class="star">*</span></label>
					  <input type="file" class="form-control" id="filePhoto" name="filePhoto" onchange="checkPhoto(this)">
					  <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =5KB</span>
				  </div>
			</div>
			<div class="container">
				<div class="col-sm-3"  style="float:left;"> 
					<input type="submit" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Update"
						style="margin-top:24px;"/> 
				</div>					
			</div>
		</form>		
				
		</div>
	</div>
</div>

</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<style>
.error{
 color:#F00;
 }
</style>
<script language="javascript" type="text/javascript">
function checkPhoto(target) {
	var ext = $('#filePhoto').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("filePhoto").value = '';
			return false;
		}

    if(target.files[0].size > 5050) {			       
		alert("Image size should less or equal 5 KB");
		document.getElementById("filePhoto").value = '';
        return false;
    }
	else if(target.files[0].size < 3000) {
				alert("Image size should be greater than 3 KB");
				document.getElementById("filePhoto").value = '';
				return false;
			}
    document.getElementById("filePhoto").innerHTML = "";
    return true;
}
</script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>

<script type="text/javascript"> 
  
$('#txtdate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            widgetPositioning: {horizontal: "auto", vertical: "bottom"}
        });
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showLearnerData() {
		 if ($("#frmcorrectionreject").valid())
           {	
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
					type: "post",
					url: "common/cfCorrectionUpdateRejectedLearner.php",
					data: "action=ShowDetails",
					success: function (data) {
						$('#response').empty();
						$("#menuList").html(data);
						$('#example').DataTable({
								dom: 'Bfrtip',
								buttons: [
									'copy', 'csv', 'excel', 'pdf', 'print'
								]
						});						
					}
				});
			}
            return false;
        }
		showLearnerData();
		
		
		$("#menuList").on('click', '.Update',function(){
			//$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
			var cid = $(this).attr('id');
			$('#txtcid').val(cid);
			
			var url = "common/cfCorrectionUpdateRejectedLearner.php"; // the script where you handle the form input.           
			var data;            
			data = "action=GetExistingDetails&cid=" + txtcid.value + ""; //

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {			
					//$('div[id=loader-wrapper]').remove();
					data = $.parseJSON(data);
					txtLCorrectName.value = data[0].cfname;
                    txtFCorrectName.value = data[0].cfaname;
                    txtLCode.value = data[0].lcode;
                    txtLearnerCode.value = data[0].lcode;
                    txtMobile.value = data[0].mobile;
					txtphoto.value = data[0].photoad;
					$("#uploadPreview9").html(data[0].photo);
                    $("#uploadPreview1").hide();
					//$("#uploadPreview1").attr('src', "upload/correction_photo/" + data[0].photo);
					
					var modal = document.getElementById('LearnerDetails');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
					}
                }
            });
	
		});
		
	$("#frmprocess").on('submit',(function(e) {
		if ($("#frmprocess").valid()) {
			$('#responses').empty();
			$('#responses').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");     
			e.preventDefault();
			  $.ajax({ 
				url: "common/cfCorrectionUpdateRejectedLearner.php",
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
				  { 
					var data = data.trim();
					 if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
							{
								$('#responses').empty();
								$('#responses').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
								window.setTimeout(function () {
									window.location.href = "frmcorrectionupdaterejected.php";
								}, 2000);

								Mode = "Add";
								resetForm("form");
							}
							else
							{
								$('#responses').empty();
								$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
							}
				  },
					error: function(e) 
					{
					   $("#errmsg").html(e).fadeIn();
					} 	        
			});
		}
    return false;
})); 
        
    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		
</body>

</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>