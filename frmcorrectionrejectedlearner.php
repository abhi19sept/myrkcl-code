<?php
$title="Correction/Duplicate Rejected Learner";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var FunctionCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
echo "<script>var CenterCode='" . $_SESSION['User_LoginId'] . "'</script>";
?>
<div style="min-height:430px !important;max-height:auto !important;">
	 <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Correction/Duplicate Rejected Learner</div>
                <div class="panel-body">
					<form name="frmcorrectionreport" id="frmcorrectionreport" class="form-inline" role="form" enctype="multipart/form-data">
						<div class="container">
							<div class="container">						
								<div id="response"></div>
							</div>
							<div id="errorBox"></div>
								<div class="col-sm-4 form-group">     
								  <label for="faname">Center Code:<span class="star">*</span></label>
								  <input type="text" readonly="true" class="form-control" id="txtitgkcode" name="txtitgkcode" placeholder="Center Code">
							   </div>
						   
								<div class="col-sm-4 form-group">     
									<label for="batch"> Select Status:<span class="star">*</span></label>
									<select id="ddlstatus" name="ddlstatus" class="form-control">

									</select>									
								</div> 

								<!--<div class="col-sm-4 form-group">     
									<label for="batch"> Select Lot:</label>
									<select id="ddllot" name="ddllot" class="form-control">

									</select>									
								</div>-->

								<div class="col-sm-4 form-group">                                  
									<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
								</div>
						</div>							
                         <div id="gird" style="margin-top:5px;"> </div>                   
                  </div>
            </div>   
        </div>
	</form>
  </div>
 </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>                
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
			txtitgkcode.value=CenterCode;
			
			function FillCorrectionApprovalStatus() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionRejectedLearner.php",
                    data: "action=GetCorrectionApprovalStatus",
                    success: function (data) {
						//alert(data);
                        $("#ddlstatus").html(data);
						 
                    }
                });
            }
            FillCorrectionApprovalStatus();
			
			$("#btnSubmit").click(function () {
				showData(ddlstatus.value);			   
			});

            function showData(val1) {
				if ($("#frmcorrectionreport").valid())
				{
					$('#response').empty();
					$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");              
						$.ajax({
							type: "post",
							url: "common/cfCorrectionRejectedLearner.php",
							data: "action=SHOW&status=" + val1 + "",
							success: function (data) {
								$('#response').empty();
									//alert(data);
								$("#gird").html(data);
								$('#example').DataTable({
								dom: 'Bfrtip',
								buttons: [
									'copy', 'csv', 'excel', 'pdf', 'print'
								]
							});
							}
						});
				}
				return false;
			}
        });

    </script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcorrectionreport_validation.js"></script>
</html>