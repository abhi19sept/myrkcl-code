<?php
$title = "Admission Payment";
include ('header.php');
include ('root_menu.php');

echo "<div style='min-height:430px !important;max-height:1500px !important;'>";
echo "<div class='container'>";

$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$udf1=$_POST["udf1"];
$udf2=$_POST["udf2"];
$udf3 = $_POST["udf3"];
$udf4 = $_POST["udf4"];
$salt = "o9aZmTfx";

require 'DAL/classconnection.php' ;
	
$_ObjConnection=new _Connection();
$_Response=array();


If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||' . $udf4 . '|' . $udf3 . '|'.$udf2.'|'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        
                  }
	else {	  

        $retHashSeq = $salt.'|'.$status.'|||||||' . $udf4 . '|' . $udf3 . '|'.$udf2.'|'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

         }
		 $hash = hash("sha512", $retHashSeq);
  
       if ($hash != $posted_hash) {
	        echo "<div class='alert alert-danger' role='alert'><b>Oh snap!</b> Payment Transaction Unsuccessful .</div>";
		   }
	   else {

echo "<br>";
    echo "<br>";
    echo "<div class='row'>";
    echo "<div class='col-md-8 col-md-offset-2'>";
    echo "<div class='panel panel-danger'>";
    echo " <div class='panel-heading'>";
    echo "    <h3 class='panel-title'>Payment Status</h3>";
    echo "  </div>";
    echo "  <div class='panel-body'>";
    echo " <table id='example' class='table table-hover table-striped'>";
    echo "  <tr class=''>";
    echo "    <td class='info' colspan='2'>";
    echo "<b>Oh snap</b> Your Payment is Failed";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Your Transaction ID is <br> You may try making the payment by clicking the link below.";
    echo "<p><a href=https://myrkcl.com/frmFeePayment.php> Try Again</a></p>";
    echo "</td>";
    echo "    <td class=''>";
    echo "$txnid";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Payment For";
    echo "</td>";
    echo "    <td class=''>";
    echo "$productinfo";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Center Code";
    echo "</td>";
    echo "    <td class=''>";
    echo "$udf1";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "User Name";
    echo "</td>";
    echo "    <td class=''>";
    echo "firstname";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Email";
    echo "</td>";
    echo "    <td class=''>";
    echo "$email";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Date";
    echo "</td>";
    echo "    <td class=''>";
    $date1 = date("d-m-Y");
    echo "$date1";
    echo "</td>";
    echo "  </tr>";
	echo "  <tr>";
    echo "<td colspan='2' align='center'>";
    echo "<input class='hide-from-printer' type='button' value='Print' onclick='window.print()'>";
    echo "</td>";
    echo "  </tr>";
    echo "</table>";

    echo "  </div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
          
		  global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				//$_User_Code =   $_SESSION['User_Code'];
			$_InsertQuery = "INSERT INTO tbl_admission_transaction (Admission_Transaction_Code, Admission_Transaction_Status, Admission_Transaction_Fname, Admission_Transaction_Amount,"
								. "Admission_Transaction_Txtid, Admission_Transaction_Hash, Admission_Transaction_Key, Admission_Transaction_ProdInfo, Admission_Transaction_Email,"
								. "Admission_Transaction_CenterCode,Admission_Transaction_RKCL_Txid,Admission_Transaction_Course,Admission_Transaction_Batch) "
								. "Select Case When Max(Admission_Transaction_Code) Is Null Then 1 Else Max(Admission_Transaction_Code)+1 End as Admission_Transaction_Code,"
								. "'" .$status. "' as Admission_Transaction_Status,'" .$firstname. "' as Admission_Transaction_Fname,'" .$amount. "' as Admission_Transaction_Amount,"
								. "'" .$txnid. "' as Admission_Transaction_Txtid,'" .$posted_hash. "' as Admission_Transaction_Hash,'" .$key. "' as Admission_Transaction_Key,"
								. "'" .$productinfo. "' as Admission_Transaction_ProdInfo,'" .$email. "' as Admission_Transaction_Email,'" .$udf1. "' as Admission_Transaction_CenterCode, '" .$udf2. "' as Admission_Transaction_RKCL_Txid, '" .$udf3. "' as Admission_Transaction_Course, '" .$udf4. "' as Admission_Transaction_Batch"
								. " From tbl_admission_transaction";            
            
            $_DuplicateQuery = "Select * From tbl_admission_transaction Where Admission_Transaction_Txtid='" . $txnid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				$_UpdatePayTranQuery = "Update tbl_payment_transaction set Pay_Tran_PG_Trnid = '" . $txnid . "', Pay_Tran_Status='PaymentFailure', Pay_Tran_Fname='" .$firstname. "' "			
								. "Where Pay_Tran_ITGK='" . $udf1 . "' AND Pay_Tran_RKCL_Trnid = '" . $udf2 . "'";								
				$_Response3=$_ObjConnection->ExecuteQuery($_UpdatePayTranQuery, Message::UpdateStatement);					
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }           
	}          
?>
<!--Please enter your website homepagge URL -->
</div>
</div>
<style>
    @media print {
        /* style sheet for print goes here */
        .hide-from-printer{  display:none; }
    }
</style>
<?php include ('footer.php'); ?>
</body>
</html>