<!DOCTYPE html>
<html lang="en">
    <head>
        <title>User Search Form</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bootcss/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="bootcss/js/bootstrap.min.js"></script>	

    </head>
    <body>
        <?php
            
            
        if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
        ?>

        <div class="container">   
            <div class="panel panel-primary">

                <div class="panel-heading">User Application  Form</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="container">
						<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Emp Id:</label>
                                <select id="ddlempId" name="ddlempId" class="form-control" >
								  
                                </select>    
                            </div>
						</div>


                            

                        </div> 
						<div class="container">
						<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Emp District:</label>
                                <select id="ddlempdistrict" name="ddlempdistrict" class="form-control" >
								  
                                </select>    
                            </div>
						</div>

                        <div class="container">

						<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Emp Tehsil:</label>
                                <select id="ddlempTehsil" name="ddlempTehsil" class="form-control">
								  
                                </select>    
                            </div>
                        </div>    
 
                        
						
						
						<div class="container">

						<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Emp Location:</label>
                                <select id="ddlempLocation" name="ddlempLocation" class="form-control" >
								  
                                </select>    
                            </div>
                        </div>    
 
                    

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
                </div>
            </div>   
        </div>


    </form>




</body>
<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		
		
		
		function FillEmpid() {
            $.ajax({
                type: "post",
                url: "common/cfusersearch.php",
                data: "action=FILLID",
                success: function (data) {
                    $("#ddlempId").html(data);
                }
            });
        }
        FillEmpid();
		
		

        function FillEmpDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfusersearch.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlempdistrict").html(data);
					
                }
            });
        }
        FillEmpDistrict();
		
		$("#ddlempdistrict").change(function(){
				//var selcountry = $(this).val();
				FillTehsilName(this.value);
		 });
			
				function FillTehsilName(districtid) {
				//alert(districtid);
				$.ajax({
			          url: 'common/cfusersearch.php',
			          type: "post",
			          data: "action=FillTehsilName&districtid=" + districtid + "",
			          success: function(data){
						//alert(data);
						$(ddlempTehsil).html(data);
			          }
			        });
				}             
        
		$("#ddlempTehsil").change(function(){
				//var selcountry = $(this).val();
				FillTehsilName(this.value);
		 });

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=DELETE&values=" + StatusCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=EDIT&values=" + StatusCode + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    txtStatusName.value = data[0].StatusName;
                    txtStatusDescription.value = data[0].StatusDescription;
                    //alert($.parseJSON(data)[0]['StatusName']);
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);

                }
            });
        }

        //showData();		
		
        $("#btnSubmit").click(function () {
			//alert("hiii");
			$("#form").valid();
			
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfusersearch.php"; // the script where you handle the form input.
            //var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&ddlempId=" + ddlempId.value + "&ddlempdistrict=" + ddlempdistrict.value + "&ddlempTehsil="+ ddlempTehsil.value + 
					   "&ddlempLocation="+ ddlempLocation.value + ""; // serializes the form's elements.
					   
				//data = "action=ADD&ddlempId=" + ddlempId.value + "&ddlempdistrict=" + ddlempdistrict.value + "&ddlempTehsil="+ ddlempTehsil.value + 
					   //"&ddlempLocation="+ ddlempLocation.value + ""; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + StatusCode + "&name=" + txtStatusName.value + "&description=" + txtStatusDescription.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmgoventry_validation.js"></script>
		



<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>