<script>
	$(document).ready(function() {
		$('.header-search form').on('click', function(e) {
			// If the form (which is turned into the search button) was
			// clicked directly, toggle the visibility of the search box.
			if(e.target == this) {
				$(this).find('input').toggle();
			}
		});
	});
</script>

<footer class="footer-distributed">

			<div class="footer-left">

				<!--<h3>Company<span>logo</span></h3>

				<p class="footer-links">
					<a href="#">Home</a>
					·
					<a href="#">Blog</a>
					·
					<a href="#">Pricing</a>
					·
					<a href="#">About</a>
					·
					<a href="#">Faq</a>
					·
					<a href="#">Contact</a>
				</p>-->

				<p class="footer-company-name">Rajasthan Knowledge Corporation Limited &copy; 2015</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>7A Jhalana Institutional Area , Behind R.T.O </br>
							Jaipur -302004, Rajasthan</span></p>
				</div>

				<!--<div>
					<i class="fa fa-phone"></i>
					<p>+1 555 123456</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">support@company.com</a></p>
				</div>-->

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span> <a href=""> Downloads </a>   <a href="" style="margin-left:25px;"> About Us </a> </span>
					
				</p> 

			<!--	<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>

				</div>-->

			</div>
		</footer>
	</body>
</html>