<?php
    $title = "Organisation Map Location";
    include ('header.php');
    include ('root_menu.php');

    if (isset($_REQUEST['code'])) {
        echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
        echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    } else {
        echo "<script>var Code=0</script>";
        echo "<script>var Mode='Update'</script>";
    }
	
?>
<!--<script src="bootcss/js/bootstrap-dialog.js"></script>
<script src="bootcss/js/bootstrap-dialog.min.js"></script> -->
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Center Map Location</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <!--<form name="frmOrgMaster" id="frmOrgMaster" class="form-inline" role="form" enctype="multipart/form-data">-->

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>
                        <div id="errorBox"></div>
                    </div>


                    <div id="main-content">
                        <div class="container">

                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">
                                <label for="learnercode">Name of Organization/Center:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtName" id="txtName" placeholder="Name of the IT-GK" readonly="">
                                
                            </div>
                            <div class="col-sm-4 form-group">
                                <label for="edistrict">District:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtDistrict" id="txtDistrict" placeholder="District" readonly="">
                                <input type="hidden" class="form-control" name="txtDistrictCode" id="txtDistrictCode" placeholder="District" readonly="">
                            </div>
						</div>
						<div class="container">
                            <div class="col-sm-4 form-group">
                                <label for="edistrict">Tehsil:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtTehsil" id="txtTehsil" placeholder="Tehsil" readonly="">
                                <input type="hidden" class="form-control" name="txtTehsilCode" id="txtTehsilCode" placeholder="Tehsil" readonly="">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label for="edistrict">Registered ITGK Address:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtAddress" id="txtAddress" placeholder="IT-GK Address" readonly="">
                                <input type="hidden" class="form-control" name="txtLat" id="txtLat" placeholder="lat" readonly="">
                                <input type="hidden" class="form-control" name="txtLong" id="txtLong" placeholder="long" readonly="">
                            </div>
                        </div>
						<div class="container">
                            <div class="col-sm-4 form-group">
                                <label for="edistrict">System Generated Google Map Address:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtSysAddress" id="txtSysAddress" placeholder="IT-GK Address" readonly="">
                            </div>
                        </div>
						<div class="container">
                            <div class="col-sm-12 form-group">
								<input type="checkbox" id="loc_terms">
								<span style="vertical-align: text-bottom;">By clicking on terms & conditions I hereby declare that the location setting up on map is true and legitable to the best of my knowledge. For any issue/dispute i will be the solely responsible. </span>
							</div>
                        </div>
                        <div class="container">

                            <label for="map">Current Location Address on Map:<span class="star">*(Drag the marker to update your center location.)</span></label>
							<input type="hidden" id="position_lat" name="position_lat" size="60">
							<input type="hidden" id="position_long" name="position_long" size="60"><br><br>
							<input type="hidden" id="position_add" name="position_add" size="60" value=""><br><br>
							<input type="hidden" id="position_org" name="position_org" value="<?php echo $_SESSION['User_Code']; ?>" size="60">
							<input type="hidden" id="markerstart_dis" name="markerstart_dis" value="" size="60">
                           
							<div id="google_map"></div>

                        </div>

                    </div>
                <!--</form>-->
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $_SESSION['User_Code']; ?>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>


<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmorgregistrationvalidation.js"></script>
<script src="bootcss/js/frmOrgmapdetails_js.js"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3LDfJa6bQsDLslWSt7MdkTD45zSyw4Ac&libraries=places"></script>
<style type="text/css">
h1.heading{padding:0px;margin: 0px 0px 10px 0px;text-align:center;font: 18px Georgia, "Times New Roman", Times, serif;}

/* width and height of google map */

#google_map {width: 95%; height: 500px; padding-bottom:20px; border:1px solid #399cff ;}

/* Marker Edit form */
.marker-edit label{display:block;margin-bottom: 5px;}
.marker-edit label span {width: 100px;float: left;}
.marker-edit label input, .marker-edit label select{height: 24px;}
.marker-edit label textarea{height: 60px;}
.marker-edit label input, .marker-edit label select, .marker-edit label textarea {width: 60%;margin:0px;padding-left: 5px;border: 1px solid #DDD;border-radius: 3px;}

/* Marker Info Window 
h1.marker-heading{color: #585858;margin: 0px;padding: 0px;font: 18px "Trebuchet MS", Arial;border-bottom: 1px dotted #D8D8D8;}*/
h1.marker-heading{
   font-family: 'Open Sans Condensed', sans-serif;
   font-size: 22px;
   font-weight: 400;
   padding: 10px;
   background-color: #48b5e9;
   color: white;
   margin: 1px;
   border-radius: 2px 2px 0 0; /* In accordance with the rounding of the default infowindow corners. */
}
/*div.marker-info-win {max-width: 300px;margin-right: -20px;} */
div.marker-info-win {
   width: 350px !important;
   top: 0 !important;
   left: 0 !important;
   border-radius: 2px 2px 0 0;
}
div.marker-info-win p{padding: 0px;margin: 10px 0px 10px 0;}
button.save-marker, button.remove-marker{
	background: #000 none repeat scroll 0 0;
    border: medium none;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
    float: right;
    padding: 4px 12px;
    margin-right: 10px;
}

/* Error */

.error {color: #D95C5C!important;}
}
.gm-style > div:first-child div:last-child > div:last-child > div:last-child > img {
    display: none;
}
.gm-style-iw + div {
    border: 1px solid red;
    display: none;
}
</style>


</html>