<?php
$title="Bulk Admission";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var EoiConfigCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var EoiConfigCode=0</script>";
echo "<script>var Mode='Add'</script>";
//
}
?>

<div style="height:65% !important;">
	<div class="container"> 
		

		<div class="panel panel-primary" style="margin-top:36px !important;">  
			<div class="panel-heading">Bulk Admission</div>
				<div class="panel-body">
					<!-- <div class="jumbotron"> --> 
					<form name="frmtmpadmissionfileupload" id="frmtmpadmissionfileupload" class="form-inline" role="form" enctype="multipart/form-data">    
						<div class="container">
							<div class="container">
								<div id="response"></div>
							</div>        
							<div id="errorBox"></div>

						</div>						
      
						<div class="container">  
							<div class="col-sm-4 form-group">  
								<label for="email">Download Sample File:</label>
								<a href="Admission_DataUpload_Template.xls"> <input type="button" value="Download File" name="download">  </a>
							</div>						
						</div>						
      
						<div class="container"> 
							<div class="col-sm-4 form-group"> 
								<label for="ecl">Upload Bulk Admission:</label>
								<input type="file" class="form-control" id="_file" name="_file" accept=".csv"/>
								<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
								<span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =50KB</span>	
							</div>
			
							<div class="col-sm-4 form-group"> 
							<span class="btn submit" id="fileuploadbutton"> <input type="button" id="btnUpload" value="Upload File"/></span> 
							<span class='btn submit'> <input type='button' id='btnReset' name='btnReset' value='Reset' /> </span>
								<div class='progress_outer'>
                                    <div id='_progress' class='progressload'></div>
                                </div>
								
							</div>	
<div class="col-sm-4 form-group">
<div class='progress_outer'>
                                    <div id='_progress1' class='progressload'></div>
                                </div>
</div>							
						</div>
						
						<div class="container">      
							<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Upload Admission"/>
						</div>                  
                    </form>
				</div>
            </div>
		</div>
		</div>
	</body>
					
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
   	<script type="text/javascript" src="DOMPurify-main/dist/purify.min.js"></script>            
	<script type="text/javascript">
		var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
		var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
		var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
		var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
		
		$(document).ready(function () {
			//alert("hii");   

				function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();
	
		$("#btnSubmit").click(function () {
			$('#response').empty();
			$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		  
		   var generateid = $('#txtGenerateId').val();
		    //alert(enddate);
		   var url = "common/cfTmpAdmissionData.php"; // the script where you handle the form input.
		   var data;
			if (Mode == 'Add')
			{
				
				data = "action=ADDTempData&generateid="+generateid+""; // serializes the form's elements.
				 //alert(data);
			}			
			$.ajax({
				type: "POST",
				url: url,
				data: data,
				success: function (data)
				{
					
					if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
					{
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
						window.setTimeout(function () {
							window.location.href = "frmtmpadmissionfileupload.php";
						}, 1000);

						Mode = "Add";
						resetForm("frmtmpadmissionfileupload");
					}
					else
					{
						
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
					}
					//showData();


				}
			});
		 
			return false; // avoid to execute the actual submit of the form.
		});
		function resetForm(formid) {
			$(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
		}	
		
});
</script>

<script src="scripts/tmpadmissionfileupload.js"></script>


</html> 
