<?php
$title = "RSP District Withdraw";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}

if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '4') {
//print_r($_SESSION);
    ?>
    <div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 			  
            <div class="panel panel-primary" style="margin-top:36px;">
                <div class="panel-heading">RSP District Withdraw</div>
                <div class="panel-body">
                    <form name="frmrsptargetapproval" id="frmrsptargetapproval" class="form-inline" role="form" enctype="multipart/form-data">
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
                            <div id="errorBox"></div>	

                            <div class="col-sm-4 form-group">     
                                <label for="batch"> Select RSP Username:</label>
                                <select id="ddlrsp" name="ddlrsp" class="form-control">

                                </select>									
                            </div>  

                            <div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
                            </div>
                        </div>   
                                                
                         <div id="main-content" style="display:none;">
                        <div class="container">
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Organization/Center:</label>
                                <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Registration No:</label>
                                <input type="text" class="form-control" readonly="true" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:</label>
                                <input type="text" class="form-control" readonly="true" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:</label>
                                <input type="text" class="form-control" readonly="true" name="txtType" id="txtType" placeholder="Type Of Organization">  
                            </div>
                        </div>







                        <div class="container">




                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Tehsil:</label>
                                <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="address">Street:</label>
                                <input type="text" class="form-control" readonly="true" name="txtStreet" id="txtStreet" placeholder="Street">    
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="address">Road:</label>
                                <input type="text" class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road">
                                <input type="hidden" name="txtorguser" id="txtorguser">
                            </div>
                        </div>
                    </div>


                        <div id="menuList" name="menuList" style="margin-top:35px;"> </div> 

                        <div class="container">
                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Withdraw" style="display:none;"/> 
                        </div>
                </div>
            </div>   
        </div>
    </form>
    </div>
    </body>
    <?php include ('footer.php'); ?>
    <?php include'common/message.php'; ?>

    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            function ShowRSPDetail(val) {
    //                if ($("#frmcorrectionapproved").valid())
    //                {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfRSPWithDraw.php",
                    data: "action=ShowDetails&user=" + val + "",
                    success: function (data) {
                        $('#response').empty();
                        $("#menuList").html(data);
                        $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                        $('#btnShow').hide();
                        $("#btnSubmit").show();
                        //$('#txtCentercode').attr('readonly', true);
                    }
                });
    //                }
                return false;
            }

            function FillRSPUser() {
                //alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfRSPWithDraw.php",
                    data: "action=FILLRSP",
                    success: function (data) {
                        //alert(data);
                        $("#ddlrsp").html(data);

                    }
                });
            }
            FillRSPUser();

            $("#ddlrsp").change(function () {
                GetUserCode(ddlrsp.value);
                $("#btnShow").show();
                $("#btnSubmit").hide();
            });
            
            function GetUserCode(val) {
                //alert(val);
                $.ajax({
                    type: "post",
                    url: "common/cfRSPTargetApproval.php",
                    data: "action=GETUSER&values=" + val + "",
                    success: function (data) {
                        //alert(data);
                        txtorguser.value=data;

                    }
                });
            }
            

            $("#btnShow").click(function () {
                //var lcode = $('#txtLearnercode').val();
                //var ccode = $('#txtCentercode').val();			
                ShowRSPDetail(ddlrsp.value);
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfSelectRsp.php",
                    data: "action=DETAILS&values=" + txtorguser.value + "",
                    success: function (data)
                    {
                        //alert(data);
                        $('#response').empty();
                        if (data == "") {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select a RSP for Selection" + "</span></p>");
                        }
                        else {
                            data = $.parseJSON(data);
                            txtName1.value = data[0].orgname;
                            txtRegno.value = data[0].regno;
                            txtEstdate.value = data[0].fdate;
                            txtType.value = data[0].orgtype;

                            txtDistrict.value = data[0].district;
                            txtTehsil.value = data[0].tehsil;
                            txtStreet.value = data[0].street;
                            txtRoad.value = data[0].road;

                            $("#main-content").show();
                            $("#btnShow").hide();
                            $('#btnSubmit').show(3000);
                        }

                    }
                });
            });

            $("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $('#btnSubmit').hide();
                var url = "common/cfRSPWithDraw.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmrsptargetapproval").serialize();
                if (Mode == 'Add') {
                    data = "action=ADD&" + forminput; // serializes the form's elements.
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        $('#response').empty();
                        if (data == 0) {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please Checked Atleast one checkbox." + "</span></p>");
                        }
                        else if (data == SuccessfullyInsert || data == SuccessfullyUpdate) {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmrspwithdraw.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("form");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                    }
                });
                return false; // avoid to execute the actual submit of the form.
            });
        });

    </script>

    <script src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmcorrectionapproval_validation.js"></script>

    </body>

    </html>

    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>