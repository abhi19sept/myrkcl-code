<?php
$title = "Renewal Penalty Payment";
include ('header.php');
include ('root_menu.php');

echo "<script>var Mode='Add'</script>";
require("razorpay/checkout/manual.php");

if ($_SESSION['User_UserRoll'] == '7') {
    ?>

    <style>
        #myProgress {
            width: 100%;
            background-color: #ddd;
        }

        #myBar {
            width: 1%;
            height: 30px;
            background-color: #4CAF50;
            text-align: center;
            line-height: 30px;
            color: white;
        }

        /*===================================== frmpayrenewalpenalty Start ===================================*/ 

        .profilebox{ width: 100%; background-color: #fff; border: 1px solid #428bca; padding: 10px 26px; text-align: center;}
        .profilepic{ width: 100%; padding: 5px;}
        .proheading{ font-size: 16px; text-align: center;}
        .persdeta{width: 100%; float: left; background-color: #f1f1f1; border: 1px solid #428bca;  margin-top: 10px;}
        .persdeta span{ width: 35%; float: left;background-color: #428bca; font-size: 16px; color: #fff; padding: 12px 20px;}
        .personalblog{ width: 94%; float: left;}
        .persdetablog{ width: 100%; float: left; padding: 15px 0px 0px 5px;}
        .persdetacont{ width: 50%; float: left;}
        .persdetacont span{width: 30%; float: left; font-size: 14px; font-family: Verdana; margin-top: 6px;}
        .persdetacont p{ width: 68%; float: left; font-size: 12px; font-family: Verdana;  padding: 7px 5px 7px 10px; margin: 0px; min-height: 33px; font-weight: bold;}

        /*===================================== Media Queries ===================================*/

        @media only screen and (max-width:1024px) and (min-width:769px){
            .profilebox{ padding: 10px 2px;}
            .persdeta span{ width: 40%;}
        }

        @media only screen and (max-width:768px) and (min-width:640px){
            .profilebox{ padding: 10px 2px;}
            .persdeta span{ width: 48%;}
            .personalblog{ width: 95%; margin-left: 15px;}
        }

        @media only screen and (max-width:639px) and (min-width:480px){
            .personalblog {float: left;width: 100%;}
            .persdeta span{ width: 60%;}
            .persdetacont{ width: 100%; margin-bottom: 10px;}
            .profilebox{ padding: 10px 2px;}
            .persdeta span{ width: 65%;}
        }

        @media all and (max-width: 360px) and (min-width: 320px) {
            .personalblog{ width: 96%;} 
            .persdeta{ width: 100%;}
            .persdeta span{ width: 100%}
            .persdetacont{ width: 100%; margin-bottom: 10px;}
            .proheading { text-align: center;}

        }

        /*===================================== frmpayrenewalpenalty End ===================================*/ 



    </style>

    <div style="min-height:430px !important;max-height:auto !important">
        <div class="container"> 

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Renewal Penalty Payment</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmpayrenewalpenalty" id="frmpayrenewalpenalty" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div id="errorBox"></div>
                            <div class="personalblog">
                                <div class="col-lg-12 col-md-12 bdrblue">
                                    <div class="persdeta">
                                        <span>IT-GK Details:</span>
                                    </div>
                                    <div class="persdetablog">
                                        <div class="persdetacont">
                                            <span>IT-GK Code :</span>
                                            <p id="cc">NA</p>
                                        </div>
                                        <div class="persdetacont">
                                            <span>IT-GK Name:</span>
                                            <p id="itgkname">NA</p>
                                        </div>

                                    </div>
                                    <div class="persdetablog">
                                        <div class="persdetacont">
                                            <span>IT-GK Email :</span>
                                            <p id="email">NA</p>
                                        </div>
                                        <div class="persdetacont">
                                            <span>IT-GK Mobile :</span>
                                            <p id="mobile">NA</p>
                                        </div>
                                    </div>
                                    <div class="persdetablog">
                                        <div class="persdetacont">
                                            <span>IT-GK Address :</span>
                                            <p id="address">NA </p>
                                        </div>
                                        <div class="persdetacont">
                                            <span>Service Provider :</span>
                                            <p id="spname">NA</p>
                                        </div>

                                    </div>
                                    <div class="persdetablog" id="areatypecondition" style="display:none">
                                        <div class="persdetacont">
                                            <span>Panchayat Samiti :</span>
                                            <p id="panchayat">NA </p>
                                        </div>
                                        <div class="persdetacont">
                                            <span>Gram Panchayat :</span>
                                            <p id="gram">NA</p>
                                        </div>

                                    </div>
                                    <!--                    <div class="persdetablog">
                                                             <div class="persdetacont">
                                                                 <span>Learner Code :</span>
                                                                 <p>112909011100001</p>
                                                            </div>
                                                            
                                                        </div>-->
                                </div>
                                <div class="col-lg-12 col-md-12 bdrblue">
                                    <div class="persdeta">
                                        <span>IT-GK Timeline :</span>
                                    </div>
                                    <div class="persdetablog">
                                        <div class="persdetacont">
                                            <span>IT-GK Creation Date :</span>
                                            <p id="creationdate">NA</p>
                                        </div>
                                        <div class="persdetacont">
                                            <span>IT-GK Previous Renewal date :</span>
                                            <p id="lastrenewaldate">NA</p>
                                        </div>
                                    </div>
                                    <div class="persdetablog">
                                        <div class="persdetacont">
                                            <span>IT-GK Expire Date :</span>
                                            <p id="expiredate">NA</p>
                                        </div>
                                        <div class="persdetacont" id="expireperiod">
                                            <span>Renewal Expired Period :</span>
                                            <p id="rep">NA</p>
                                        </div>
                                        <div class="persdetacont" id="nerep" style="display:none">
                                            <span>Renewal Expired Period :</span>
                                            <p id="nerepd" style="color:red">NA</p>
                                        </div>
                                        <div class="persdetacont" id="noteligible" style="display:none">
                                            <p style="color:red">NOT ELIGIBLE FOR RENEWAL</p>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-12 col-md-12 bdrblue" id="sladetails">
                                    <div class="persdeta">
                                        <span>IT-GK SLA Details :</span>
                                    </div>
                                    <div class="persdetablog">
                                        <div class="persdetacont">
                                            <span>IT-GK Area Type :</span>
                                            <p id="creationamount">NA</p>
                                        </div>
                                        <div class="persdetacont">
                                            <span>Admission Required for Renewal :</span>
                                            <p id="arr">NA</p>
                                        </div>
                                    </div>
                                    <div class="persdetablog">
                                        <div class="persdetacont">
                                            <span>Number of admission in last year :</span>
                                            <p id="naly">NA</p>
                                        </div>
                                        <div class="persdetacont">
                                            <span>Shortfall :</span>
                                            <p id="shortfall">NA</p>
                                        </div>
                                    </div>
                                    <div class="persdetablog">
                                        <div class="persdetacont">
                                            <span>Penalty per Shortfall :</span>
                                            <p id="pps">NA</p>
                                        </div>
                                        <div class="persdetacont">
                                            <span>Total Penalty :</span>
                                            <p id="tp" style="color:red">NA</p>
                                        </div>
                                    </div>

                                </div>    


                            </div>

                        </div>  
                        <br>
                        <br>   
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="batch"> Select Payment Gateway:</label> 
                            </div>
                            <div class="col-sm-4 form-group">     
                                <select id="gateway" name="gateway" class="form-control">
                                    <option value="payu">Payu</option>
                                    <option value="razorpay">Razorpay</option>
                                </select>
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="batch"> Select Payment Mode:</label>
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <select id="paymode" name="paymode" class="form-control">

                                </select>
                            </div>

                        </div>                     
                        <div class="container" id="paynow">
                            <div class="col-sm-4">
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-danger" value="Pay Now" 
                                       style='margin-left:500px; display:none;'/>    
                            </div>					
                        </div>
                    </form>
                </div>
            </div>   
        </div>

    </div>

    <form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
        <input type="hidden" id="txnid" name="txnid">
    </form>

    </body>
    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>
    <style>
        #errorBox{
            color:#F00;
        }
    </style>

    <script type="text/javascript">

        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            function ChkPenaltyStatus()
            {           //alert(Code);    
                $.ajax({
                    type: "post",
                    url: "common/cfPayRenewalPenalty.php",
                    data: "action=GETPENALTYDETAILS",
                    success: function (data) {

                        if (data == '') {

                        } else {

                            $('#btnSubmit').hide();
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Penalty Already Paid.</span>");
                            window.setTimeout(function () {
                                window.location.href = "frmusermasterdetails.php";
                            }, 3000);
                        }

                    }
                });
            }

            fillForm();

            function showpaymentmode() {
                $("#btnSubmit").hide();
                $("#paymode").html('');
                $.ajax({
                    type: "post",
                    url: "common/cfPayRenewalPenalty.php",
                    data: "action=ShowPenaltyPay",
                    success: function (data) {
                        if (data == 0) {
                            $("#btnSubmit").hide();
                        } else {
                            $("#paymode").html(data);
                        }
                    }
                });
            }
            $("#paymode").change(function () {
                var a = $('#paymode').val();
                if (a == 1) {
                    $("#btnSubmit").show();
                } else {
                    $("#btnSubmit").hide();
                }
            });

            function fillForm()
            {           //alert(Code);    
                $.ajax({
                    type: "post",
                    url: "common/cfPayRenewalPenalty.php",
                    data: "action=GETDETAILS",
                    success: function (data) {
                        //alert(data);
                        if (data == '') {
                            $('#btnSubmit').hide();
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; NO Penalty Found to be Paid.</span>");
                            window.setTimeout(function () {
                                window.location.href = "dashboard.php";
                            }, 3000);
                        } else {
                            data = $.parseJSON(data);
    //                    var $shortfall = data[0].shortfall;
                            //alert(data[0].year);
                            ChkPenaltyStatus();
                            showpaymentmode();
                            $('#cc').text(data[0].cc);
                            $('#itgkname').text(data[0].itgkname);
                            $('#email').text(data[0].email);
                            $('#mobile').text(data[0].mobile);
                            $('#address').text(data[0].address);
                            $('#spname').text(data[0].spname);
                            $('#creationdate').text(data[0].creationdate);
                            $('#lastrenewaldate').text(data[0].lastrenewaldate);
                            $('#expiredate').text(data[0].expiredate);
                            if (data[0].creationamount === 'Rural') {
                                $('#areatypecondition').show();
                                $('#panchayat').text(data[0].panchayat);
                                $('#gram').text(data[0].gram);
                            }
                            if (data[0].expiredate >= '2018-09-05') {
                                if ((data[0].re > '10' && data[0].days != '0') || data[0].year > '0') {
                                    $('#nerepd').text(data[0].rep);
                                    $('#sladetails').hide();
                                    $('#expireperiod').hide();
                                    $('#nerep').show();
                                    $('#noteligible').show();
                                    $('#paynow').hide();
                                } else {

                                    $('#rep').text(data[0].rep);
                                    $('#creationamount').text(data[0].creationamount);
                                    $('#arr').text(data[0].arr);
                                    $('#naly').text(data[0].naly);

                                    if (data[0].shortfall < '1') {
                                        $('#shortfall').text('NA');
                                        $('#pps').text('NA');
                                        $('#tp').text('NO PENALTY');
                                        $('#paynow').hide();
                                    } else {
                                        $('#shortfall').text(data[0].shortfall);
                                        $('#pps').text(data[0].pps);
                                        $('#tp').text(data[0].tp);
                                    }
                                }
                            } else if (data[0].expiredate < '2018-09-05') {
                                if (data[0].re > '9' || data[0].year > '0') {
                                    $('#nerepd').text(data[0].rep);
                                    $('#sladetails').hide();
                                    $('#expireperiod').hide();
                                    $('#nerep').show();
                                    $('#noteligible').show();
                                    $('#paynow').hide();
                                } else {

                                    $('#rep').text(data[0].rep);
                                    $('#creationamount').text(data[0].creationamount);
                                    $('#arr').text(data[0].arr);
                                    $('#naly').text(data[0].naly);

                                    if (data[0].shortfall < '1') {
                                        $('#shortfall').text('NA');
                                        $('#pps').text('NA');
                                        $('#tp').text('NO PENALTY');
                                        $('#paynow').hide();
                                    } else {
                                        $('#shortfall').text(data[0].shortfall);
                                        $('#pps').text(data[0].pps);
                                        $('#tp').text(data[0].tp);
                                    }
                                }
                            }

                        }
                    }
                });
            }

            $("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfPayRenewalPenalty.php"; // the script where you handle the form input.
                var data;

                if (Mode == 'Add')
                {
                    data = "action=ADD&gateway=" + gateway.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#response').empty();
                        if (data == 0) {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Something went wrong, please re-try." + "</span></p>");
                        } else if (data != '') {
                            if (data == 'TimeCapErr') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                            } else {
                                if ($('#gateway').val() == 'razorpay') {
                                    var options = $.parseJSON(data);
    <?php include("razorpay/razorpay.js"); ?>
                                } else {
                                    $('#txnid').val(data);
                                    $('#frmpostvalue').submit();
                                }
                            }
                        }
                    }
                });

                return false; // avoid to execute the actual submit of the form.

            });

            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>