<?php
$title = "Organization Form Process";
include ('header.php');
include ('root_menu.php');
?>

<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container" id="viewpage" style="display:none">			 
        <div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">Approve Organization Login Request</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmownershipchangerequest" id="frmownershipchangerequest"  role="form" action="" class="form-inline" enctype="multipart/form-data">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <br>
                    <div class="panel panel-success">
                        <div class="panel-heading">View New Center's Details</div>
                        <div class="panel-body">
                            <div class="container">

                                <div id="errorBox"></div>
                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Name of Organization/Center:</label>
                                    <input type="text" class="form-control"   name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                                    <input type="hidden" class="form-control"   name="txtOrgCode" id="txtOrgCode" placeholder="OrganizationCode">
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">Registration No:</label>
                                    <input type="text" class="form-control"   name="txtRegno" id="txtRegno" placeholder="Registration No">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="faname">Date of Establishment:</label>
                                    <input type="text" class="form-control"   name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD" readonly="true">
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">PAN Number:</label>
                                    <input type="text" class="form-control"   name="txtPanNo" id="txtPanNo" placeholder="PAN">
                                    <input type="hidden" class="form-control"   name="txtType" id="txtType" placeholder="Type Of Organization">  
                                </div>
                            </div>




                            <div class="container">




                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">AADHAR Number:</label>
                                    <input type="text" class="form-control"   name="txtAADHARNo" id="txtAADHARNo" placeholder="Document Type">   
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <label for="SelectType">Type of Application:</label>
                                    <input type="text" class="form-control"   name="txtRole" id="txtRole" readonly="readonly" placeholder="Role"/>     

                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="email">Email:</label>
                                    <input type="text" class="form-control"   name="txtEmail" id="txtEmail" placeholder="Email ID">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="Mobile">Mobile Number:</label>
                                    <input type="text" class="form-control"   name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-success">
                        <div class="panel-heading">Bank Account Details</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Bank Account Holder Name:</label>
                                    <input type="text" class="form-control"   name="txtaccountName" maxlength="100" id="txtaccountName"  placeholder="Bank Acc Name" onkeypress="javascript:return allowchar(event);" style="text-transform:uppercase"/>   

                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Bank Acc Number:</label>
                                    <input type="text" class="form-control"   name="txtaccountNumber" maxlength="21" id="txtaccountNumber"  placeholder="Bank Acc Number" onkeypress="javascript:return allownumbers(event);"/>   
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <label for="gender">Account Type:<font color="red">*</font></label> <br/>                               
                                    <label class="radio-inline"> <input type="radio" id="rbtaccountType_saving" name="rbtaccountType_saving" checked="checked" value="Savings"/> Savings </label>
                                    <label class="radio-inline"> <input type="radio" id="rbtaccountType_saving" name="rbtaccountType_saving" value="Current"/> Current </label>									
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="address">Bank Name:</label>
                                    <textarea class="form-control"   id="ddlBankName" name="ddlBankName" maxlength="110" placeholder="Bank name" onkeypress="javascript:return allowchar(event);" style="text-transform:uppercase"></textarea>

                                </div>
                            </div>
                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">IFSC Code:</label>
                                    <input type="text" class="form-control"   name="txtIfscCode" maxlength="11" id="txtIfscCode"  placeholder="IFSC Code">  
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">MICR Code:</label>
                                    <input type="text" class="form-control"   name="txtMicrcode" id="txtMicrcode"  maxlength="21" placeholder="MICR Code">   
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="address">Branch Name:</label>
                                    <input type="text" class="form-control"   name="ddlBranch" id="ddlBranch" maxlength="220" placeholder="Branch Name" onkeypress="javascript:return allowchar(event);" style="text-transform:uppercase">    
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="address">Bank PAN No:</label>
                                    <input type="text" class="form-control"   id="txtpanno1" maxlength="10" name="txtpanno1" placeholder="Pan">

                                </div>
                            </div>
                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Pan Card Holder Name:</label>
                                    <input type="text" class="form-control"   name="txtpanname" id="txtpanname" maxlength="110"  placeholder="txtPanName" onkeypress="javascript:return allowchar(event);" style="text-transform:uppercase"> 
                                </div>
                                <div class="col-sm-4 form-group">
                                    <label for="edistrict">Select Bank Account ID Proof:<span class="star">*</span></label>
                                    <select id="ddlidproof" name="ddlidproof" class="form-control">
                                        <option value=""> Please Select </option>
                                        <option value="CHEQUE"> Cancelled Cheque Copy </option>
                                        <option value="WELCOME"> Welcome Letter of Bank </option>
                                        <option value="PASSBOOK"> Passbook Copy </option>
                                    </select>    
                                </div>

                                <div class="col-sm-4 form-group" > 
                                    <label for="photo">Selected Id Proof:<label style="color:green"  for="IdProof">Bank ID Proof:</label> </label> </br>
                                    <input type="hidden" class="form-control"   name="txtBankIDProof" id="txtBankIDProof"  placeholder="Bank ID Proof">   
                                    <button type="button" id="BankIDProof" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                        View Document</button>
                                </div>

                                <div class="col-sm-4 form-group" > 
                                    <label for="photo">Bank Pan Card:<span class="star">*</span></label> </br>
                                    <button type="button" id="BankPanCard" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                        View Document</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    &nbsp;
                    &nbsp;
                    &nbsp;

                    <div class="panel panel-info">
                        <div class="panel-heading">Uploaded Owner Documents</div>
                        <div class="panel-body">	

                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Pan Card:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview1" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">AADHAR Card:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview2" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Address Proof:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview3" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">NCR Applcation Form:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview4" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">Uploaded Organization Type Documents</div>
                        <div class="panel-body">
                            <button type="button" id="showmodal" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc1"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview10" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc2"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview11" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc3"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview12" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc4"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview13" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc5"> 
                                <label for="orgtypedoc5"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview14" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc6"> 
                                <label for="orgtypedoc6"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview15" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc7"> 
                                <label for="orgtypedoc7"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview16" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                        </div>
                    </div>

                    <!--                    <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="status">Action:<span class="star">*</span></label>
                                                <select id="ddlstatus" name="ddlstatus" class="form-control" onchange="toggle_visibility1('remark')">
                                                    <option selected="true" value="">Select</option>
                                                    <option value="Approve" >Approve</option>
                                                    <option value="Reject" >Reject</option>
                                                </select>    
                                            </div>
                                            <div class="col-sm-4 form-group" id="remark" style="display:none;"> 
                                                <label for="pan">Remark:<span class="star">*</span></label>
                                                <input type="text" class="form-control"  name="txtRemark" id="txtRemark"  placeholder="Remark">
                                            </div>
                    
                    
                                        </div>		-->

                    <div class="container">

                        <input type="button" name="btnEdit" id="btnEdit" class="btn btn-danger" value="Click Here to Update Details"/>    
                    </div>
                </form>
            </div>
        </div>  

    </div>

    <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" style="width: 100%;height: 500px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h3 id="heading-tittle" class="modal-title">Heading</h3>
                </div>

                <form class="form-horizontal" name="frmeditncrfile" id="frmeditncrfile" role="form" action="" class="form-inline" enctype="multipart/form-data">
                    <iframe id="viewimagesrc" src="uploads/news/1487051012.pdf" style="width: 100%;height: 500px;border: none;"></iframe>
                    <input type="hidden" class="form-control" readonly="true" name="txtfilename" id="txtfilename">
                    <div class="panel panel-danger">
                        <div class="panel-heading">Choose Another File To Update</div>
                        <div class="panel-body">
                            <div id="response"></div>
                            <div class="col-sm-12" > 
                                <label for="photo">Attach File:<span class="star">*</span></label>
                                <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->								  
                                <input style=" width: 115%" id="uploadImage7" type="file" name="uploadImage7" onchange="checkPANPhoto(this);"/>	
                                <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                <span style="font-size:10px;">Note: PDF Allowed Size = 100 KB to 3 MB</span>
                            </div>
                        </div>
                    </div>


                    <div class="container">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Update Document"/>    
                    </div>
                </form>
<!--<img id="viewimagesrc" class="thumbnail img-responsive" src="images/not-found.png" name="filePhoto3" width="800px" height="880px">-->
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>

<script type="text/javascript">

    $(document).ready(function () {
        jQuery(".thumbnailmodal").click(function () {
            $('.modal-body').empty();
            var title = $(this).parent('a').attr("title");
            $('.modal-title').html(title);
            $($(this).parents('div').html()).appendTo('.modal-body');
            $('#showmodal').click();
        });
    });
</script>
<script language="javascript" type="text/javascript">
    function checkPANPhoto(target) {
        var ext = $('#uploadImage7').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage7").value = '';
            return false;
        }

        if (target.files[0].size > 2000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage7").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        }
        document.getElementById("uploadImage7").innerHTML = "";
        return true;
    }
</script>
<script type="text/javascript">
    $('#txtEstdate').datepicker({
        format: "yyyy-mm-dd"
    });
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        $(function () {
            $("#txtEstdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });

        fillForm();
        
        $(".close").on("click",function(){
        $("#testdoc").attr("src"," "); 
    });

        function fillForm()
        {           //alert("C");    
            $.ajax({
                type: "post",
                cache: false,
                url: "common/cfOnHoldDetails.php",
                data: "action=PROCESS",
                success: function (data) {
                    //alert(data);
                    var result = data.trim();
                    if (result == '[]') {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Ownership Application is not available to EDIT.</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/error.gif width=10px /></span><span>Ownership Application is not available to EDIT.</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "dashboard.php";
                        }, 3000);
                    } else {
                        $("#viewpage").show(3000);
                        data = $.parseJSON(data);
                        txtName1.value = data[0].orgname;
                        txtOrgCode.value = data[0].orgcode;
                        txtRegno.value = data[0].regno;
                        txtEstdate.value = data[0].fdate;
                        txtType.value = data[0].orgtype;
                        txtPanNo.value = data[0].orgpan;
                        txtAADHARNo.value = data[0].orgaadhar;
                        txtRole.value = data[0].role;

                        txtEmail.value = data[0].email;
                        txtMobile.value = data[0].mobile;

                        txtaccountName.value = data[0].bankaccname;
                        txtaccountNumber.value = data[0].bankaccno;
                        //txtBankAccType.value = data[0].bankacctype;
                        ddlBankName.value = data[0].bankname;
                        txtIfscCode.value = data[0].ifsccode;
                        txtMicrcode.value = data[0].micrcode;
                        ddlBranch.value = data[0].branchname;
                        txtpanno1.value = data[0].panno;
                        txtpanname.value = data[0].panname;
                        $("#ddlidproof").val(data[0].bankid);
                        jQuery("label[for='IdProof']").html(data[0].bankid);

                        $("#BankPanCard").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "Bankdocs/" + data[0].bankpandoc + "?nocache=" + Math.random());
                            txtfilename.value = "upload/Bankdocs/" + data[0].bankpandoc;
                        });
                        $("#BankIDProof").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "Bankdocs/" + data[0].bankiddoc + "?nocache=" + Math.random());
                            txtfilename.value = "upload/Bankdocs/" + data[0].bankiddoc;
                        });

                        $("#uploadPreview1").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRPAN/" + data[0].orgdoc + "?nocache=" + Math.random());
                            txtfilename.value = "upload/NCRPAN/" + data[0].orgdoc;
                        });
                        $("#uploadPreview2").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRUID/" + data[0].orguid + "?nocache=" + Math.random());
                            txtfilename.value = "upload/NCRUID/" + data[0].orguid;
                        });
                        $("#uploadPreview3").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRAddProof/" + data[0].orgaddproof + "?nocache=" + Math.random());
                            txtfilename.value = "upload/NCRAddProof/" + data[0].orgaddproof;
                        });
                        $("#uploadPreview4").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRAppForm/" + data[0].orgappform + "?nocache=" + Math.random());
                            txtfilename.value = "upload/NCRAppForm/" + data[0].orgappform;
                        });
//                    $("#uploadPreview1").attr('src', "upload/NCRPAN/" + data[0].orgdoc);
//                    $("#uploadPreview2").attr('src', "upload/NCRUID/" + data[0].orguid);
//                    $("#uploadPreview3").attr('src', "upload/NCRAddProof/" + data[0].orgaddproof);
//                    $("#uploadPreview4").attr('src', "upload/NCRAppForm/" + data[0].orgappform);

                        if (txtType.value == "Proprietorship/Individual")
                        {
                            jQuery("label[for='orgtypedoc1']").html("Ownership Type Document");
                            jQuery("label[for='orgtypedoc2']").html("Shop Act License");
                            jQuery("label[for='orgtypedoc3']").html("PAN card copy of Proprietor");
                            jQuery("label[for='orgtypedoc4']").html("Cancelled Cheque");

                            $("#doc5").hide();
                            $("#doc6").hide();
                            $("#doc7").hide();


                            $("#uploadPreview10").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_owntype.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_owntype.pdf";
                            });

                            $("#uploadPreview11").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_salpi.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_salpi.pdf";
                            });

                            $("#uploadPreview12").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_panpi.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_panpi.pdf";
                            });

                            $("#uploadPreview13").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_ccpi.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_ccpi.pdf";
                            });

                        }
                        if (txtType.value == "Partnership")
                        {
                            jQuery("label[for='orgtypedoc1']").html("Copy of Partnership Deed");
                            jQuery("label[for='orgtypedoc2']").html("Authorization letter");
                            jQuery("label[for='orgtypedoc3']").html("PAN card of Partnership firm");
                            jQuery("label[for='orgtypedoc4']").html("Cancelled cheque of firm");
                            jQuery("label[for='orgtypedoc5']").html("Address Proof documents");
                            jQuery("label[for='orgtypedoc6']").html("Registration certificate");
                            jQuery("label[for='orgtypedoc7']").html("Shop Act Registration Copy");

                            $("#uploadPreview10").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_copd.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_copd.pdf";
                            });

                            $("#uploadPreview11").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_al.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_al.pdf";
                            });

                            $("#uploadPreview12").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ppf.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ppf.pdf";
                            });

                            $("#uploadPreview13").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ccpf.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ccpf.pdf";
                            });

                            $("#uploadPreview14").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_apdpf.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_apdpf.pdf";
                            });

                            $("#uploadPreview15").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_rcrf.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_rcrf.pdf";
                            });

                            $("#uploadPreview16").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_sarpf.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_sarpf.pdf";
                            });

                        }
                        if (txtType.value == "Private Ltd." || txtType.value == "Public Ltd.")
                        {
                            jQuery("label[for='orgtypedoc1']").html("Certificate of Incorporation");
                            jQuery("label[for='orgtypedoc2']").html("List Of Directors");
                            jQuery("label[for='orgtypedoc3']").html("Pan Card Of Company");
                            jQuery("label[for='orgtypedoc4']").html("Cancelled Cheque");
                            jQuery("label[for='orgtypedoc5']").html("Board Resolution Copy");
                            jQuery("label[for='orgtypedoc6']").html("Address Proof Document");
                            jQuery("label[for='orgtypedoc7']").html("Shop Act Registration Copy");

                            $("#uploadPreview10").click(function () {
                                $("#viewimagesrc").attr('src', "");
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_coi.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_coi.pdf";
                            });

                            $("#uploadPreview11").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_lod.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_lod.pdf";
                            });

                            $("#uploadPreview12").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_copan.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_copan.pdf";
                            });

                            $("#uploadPreview13").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_ccc.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_ccc.pdf";
                            });

                            $("#uploadPreview14").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_cobr.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_cobr.pdf";
                            });

                            $("#uploadPreview15").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_apd.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_apd.pdf";
                            });

                            $("#uploadPreview16").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_csar.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_csar.pdf";
                            });

                        }
                        if (txtType.value == "Limited Liability Partnership (LLP)")
                        {
                            jQuery("label[for='orgtypedoc1']").html("Certificate of Incorporation");
                            jQuery("label[for='orgtypedoc2']").html("List of Partners");
                            jQuery("label[for='orgtypedoc3']").html("Copy of PAN card of LLP");
                            jQuery("label[for='orgtypedoc4']").html("Canceled cheque of LLP");
                            jQuery("label[for='orgtypedoc5']").html("Copy of Board Resolutions");
                            jQuery("label[for='orgtypedoc6']").html("Address Proof Document");
                            jQuery("label[for='orgtypedoc7']").html("Copy of Shop Act Registration");

                            $("#uploadPreview10").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_coillp.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_coillp.pdf";
                            });

                            $("#uploadPreview11").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_lop.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_lop.pdf";
                            });

                            $("#uploadPreview12").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_panllp.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_panllp.pdf";
                            });

                            $("#uploadPreview13").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_ccllp.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_ccllp.pdf";
                            });

                            $("#uploadPreview14").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_cobrllp.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_cobrllp.pdf";
                            });

                            $("#uploadPreview15").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_apdllp.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_apdllp.pdf";
                            });

                            $("#uploadPreview16").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_csarllp.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_csarllp.pdf";
                            });

                        }
                        if (txtType.value == "Trust" || txtType.value == "Society" || txtType.value == "Coorperative Society" || txtType.value == "Others")
                        {
                            jQuery("label[for='orgtypedoc1']").html("Certificate of Registration");
                            jQuery("label[for='orgtypedoc2']").html("PAN Card of Organization");
                            jQuery("label[for='orgtypedoc3']").html("Cancelled cheque of Organization");
                            jQuery("label[for='orgtypedoc4']").html("List of Executive Body");
                            jQuery("label[for='orgtypedoc5']").html("Copy of Board Resolution");
                            jQuery("label[for='orgtypedoc6']").html("Address Proof documents");

                            $("#doc7").hide();

                            $("#uploadPreview10").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cor.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cor.pdf";
                            });

                            $("#uploadPreview11").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_panoth.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_panoth.pdf";
                            });

                            $("#uploadPreview12").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_ccoth.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_ccoth.pdf";
                            });

                            $("#uploadPreview13").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_leb.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_leb.pdf";
                            });

                            $("#uploadPreview14").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cbr.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cbr.pdf";
                            });

                            $("#uploadPreview15").click(function () {
                                $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_adpoth.pdf" + "?nocache=" + Math.random());
                                txtfilename.value = "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_adpoth.pdf";
                            });

                        }
                    }
                }
            });
        }



        $("#frmeditncrfile").submit(function () {
            //alert("1");
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            //BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfEditOwnershipRequest.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmeditncrfile").serialize();
//alert(forminput);
            data = forminput; // serializes the form's elements.

            var form_data = new FormData(this);                  // Creating object of FormData class

            form_data.append("action", "ADD")
            form_data.append("data", data)
//alert(form_data);
            $.ajax({

                url: url,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data, // Setting the data attribute of ajax with file_data
                type: 'post',
                success: function (data)
                {
                    //alert(data);
                    var result = data.trim();
                    if (result === "Successfully Inserted")
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + result + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; SP-Center Agreement Uploaded Successfully.</span>");
                        window.setTimeout(function () {
                            window.location.href = "viewimagesrc.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("viewimagesrc");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + result + "</span></p>");

                        window.setTimeout(function () {
                            window.location.reload(true);
                        }, 2000);

                    }



                }
            });

            return false; // avoid to execute the actual submit of the form.
        });

        $("#btnEdit").click(function () {
            //alert("1");
            if ($("#frmownershipchangerequest").valid())
            {
                //alert("2");
                //$('#btnEdit').hide();
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfOnHoldDetails.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmownershipchangerequest").serialize();
//alert(forminput);
                data = "action=EDIT&" + forminput; // serializes the form's elements.
//                var form_data = new FormData(this);                  // Creating object of FormData class
//                form_data.append("action", "EDIT")
//                form_data.append("data", forminput)
//alert(form_data);
                $.ajax({

                    url: url,
//                    cache: false,
//                    contentType: false,
//                    processData: false,
                    data: data, // Setting the data attribute of ajax with file_data
                    type: 'post',
                    success: function (data)
                    {
                        //alert(data);
var result = data.trim();
                        if (result === "Successfully Updated")
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + result + "</span></p>");
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Application for Ownership Transfer has been Re-Submitted Successfully.</span>");
                            window.setTimeout(function () {
                                window.location.href = "frmonholddetails.php";
                            }, 3000);

                            Mode = "Add";
                            resetForm("frmownershipchangerequest");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></p>");
                            $('#btnSubmit').show();
                        }



                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmownershipchangerequest_validation.js"></script>

<script src="bootcss/js/frmbankaccount_validation.js" type="text/javascript"></script>	
</html>