<?php
$title = "Menu Management";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == 1) {
    // you are Log-in
} else {
    header('Location: logout.php');
    exit;
}
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;min-height:330px; ">  
            <div class="panel-heading">Menu Management System</div>
            <div class="panel-body">
                <div class="container">
                    <div id="response" style="width: 95%;"></div>
                </div>
                <div id="grid"> </div>
            </div>
        </div>
    </div>
</div>
<div id="ModelRootMenu" class="modal" style="background-color: #220d4a7d;">
    <div class="modal-content" style="width: 70%;margin: 150px 0 0 210px;">
        <div class="modal-header">
            <span class="close closeRoot">&times;</span>
            <h6>Manage Root Menu <span id="operation" style="color: #0a2056;text-transform: none;"></span></h6>
        </div>
        <div class="modal-body">
            <form name="frmRootMenu" id="frmRootMenu" class="form-horizontal" role="form" action="">   
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Root Menu</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" maxlength="50" name="txtRootMenu" id="txtRootMenu" placeholder="Root Menu" style="text-transform:uppercase" onkeypress="javascript:return allowchar(event);">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Display Order</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" maxlength="5" name="txtDisplayOrder" id="txtDisplayOrder" placeholder="Display Order" onkeypress="javascript:return allownumbers(event);">
                    </div>
                </div>  
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Status</label>
                    <div class="col-sm-6">
                        <select id="ddlStatus" name="ddlStatus" class="form-control">
                            <option value="">-- Please Select --</option>          
                        </select>
                    </div>
                </div>  
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <div id="responseRoot" style="text-align: center;"></div>
                    </div>
                </div>  
                <div class="form-group last">
                    <div class="col-sm-offset-3 col-sm-6">
                        <input type="hidden" name="action" id="action" value="">
                        <input type="hidden" name="txtRootCode" id="txtRootCode" value="">
                        <button type="button" name="btnAddRootMenu" id="btnAddRootMenu" class="btnyogi btn btn-success btn-sm">Submit</button>
                        <button type="reset" class="btn btn-default btn-sm">Reset</button>			
                    </div>
                </div>  
            </form>
        </div>
        <div class="modal-footer">
            <h6>Manage Root Menu</h6>
        </div>
    </div>
</div>
<div id="ModelParentFunction" class="modal" style="background-color: #220d4a7d;">
    <div class="modal-content" style="width: 70%;margin: 150px 0 0 210px;">
        <div class="modal-header">
            <span class="close closeParent">&times;</span>
            <h6>Manage Parent Function Menu <span id="operationParent" style="color: #0a2056;text-transform: none;"></span></h6>
        </div>
        <div class="modal-body">
            <form name="frmParentFunctionMaster" id="frmParentFunctionMaster" class="form-horizontal" role="form" action=""> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Root Menu</label>
                    <div class="col-sm-6">
                        <select id="ddlRootParent" name="ddlRootParent" class="form-control" readonly="ture">
                            <option value="">-- Select Root Menu --</option>          
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Menu Name</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" maxlength="50" name="txtParentFunctionName" id="txtParentFunctionName" placeholder="Menu Name" style="text-transform:uppercase" onkeypress="javascript:return allowchar(event);">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Display Order</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" maxlength="5" name="txtDisplayOrderParent" id="txtDisplayOrderParent" placeholder="Display Order" onkeypress="javascript:return allownumbers(event);">
                    </div>
                </div>  
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Status</label>
                    <div class="col-sm-6">
                        <select id="ddlStatusParent" name="ddlStatusParent" class="form-control">
                            <option value="">-- Please Select --</option>          
                        </select>
                    </div>
                </div>  
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <div id="responseParent" style="text-align: center;"></div>
                    </div>
                </div>  
                <div class="form-group last">
                    <div class="col-sm-offset-3 col-sm-6">
                        <input type="hidden" name="action" id="actionParent" value="">
                        <input type="hidden" name="txtParentCode" id="txtParentCode" value="">
                        <button type="button" name="btnAddParentMenu" id="btnAddParentMenu" class="btnyogi btn btn-success btn-sm">Submit</button>
                        <button type="reset" class="btn btn-default btn-sm">Reset</button>			
                    </div>
                </div>  
            </form>
        </div>
        <div class="modal-footer">
            <h6>Manage Parent Function Menu</h6>
        </div>
    </div>
</div>
<div id="ModelChild" class="modal" style="background-color: #220d4a7d;">
    <div class="modal-content" style="width: 70%;margin: 150px 0 0 210px;">
        <div class="modal-header">
            <span class="close closeChild">&times;</span>
            <h6>Manage Child Menu <span id="operationChild" style="color: #0a2056;text-transform: none;"></span></h6>
        </div>
        <div class="modal-body">
            <form name="frmFunctionMaster" id="frmFunctionMaster" class="form-horizontal" role="form" action=""> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Function Name</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" maxlength="50" name="txtFunctionName" id="txtFunctionName" placeholder="Menu Name" style="text-transform:uppercase" onkeypress="javascript:return allowchar(event);">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Root Menu Name</label>
                    <div class="col-sm-6">
                        <select id="ddlRootFunction" name="ddlRootFunction" class="form-control" readonly="ture">
                            <option value="">-- Select Root Menu --</option>          
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Parent Menu Name</label>
                    <div class="col-sm-6">
                        <select id="ddlParentFunction" name="ddlParentFunction" class="form-control" readonly="ture">
                            <!--                            <option value="">-- Select Parent Menu --</option>          -->
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Function URL</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" maxlength="50" name="txtFunctionURL" id="txtFunctionURL" placeholder="Menu Name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Display Order</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" maxlength="5" name="txtDisplayOrderFunction" id="txtDisplayOrderFunction" placeholder="Display Order" onkeypress="javascript:return allownumbers(event);">
                    </div>
                </div>  
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label examlebel">Function Status</label>
                    <div class="col-sm-6">
                        <select id="ddlStatusFunction" name="ddlStatusFunction" class="form-control">
                            <option value="">-- Select Status --</option>          
                        </select>
                    </div>
                </div>  
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <div id="responseChild" style="text-align: center;"></div>
                    </div>
                </div>  
                <div class="form-group last">
                    <div class="col-sm-offset-3 col-sm-6">
                        <input type="hidden" name="action" id="actionChild" value="">
                        <input type="hidden" name="txtChildCode" id="txtChildCode" value="">
                        <button type="button" name="btnAddChildMenu" id="btnAddChildMenu" class="btnyogi btn btn-success btn-sm">Submit</button>
                        <button type="reset" class="btn btn-default btn-sm">Reset</button>			
                    </div>
                </div>  
            </form>
        </div>
        <div class="modal-footer">
            <h6>Manage Child Menu</h6>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function fillMenuManagement()
        {
            $('#response').empty();
            $('#response').append("<div class='alert-success'><span ><img style='height: 20px;width: 20px;margin: 8px;' src=images/ajax-loader.gif width=10px /></span><span style='font-size: 16px;font-weight: 600;'>Please Wait Proccessing.....</span></div>");
            $.ajax({
                type: "post",
                url: "common/cfMenuManagement.php",
                data: "action=MenuManagementAction",
                success: function (data) {
                    $('#response').empty();
                    $("#grid").html(data);
                    $("#myInputRoot").on("keyup", function () {
                        var value = $(this).val().toLowerCase();
                        $("#SearchRoot .rootmenubtnfilter").filter(function () {
                            var ftr = $(this).find(".rootmenubtn").text();
                            $(this).toggle(ftr.toLowerCase().indexOf(value) > -1)
                        });
                    });
                    $(".myInputParent").on("keyup", function () {
                        var id = ($(this).attr('id'));
                        var value = $(this).val().toLowerCase();
                        $("#SearchParent_" + id + " .parentmenubtnfilter_" + id).filter(function () {
                            var ftr = $(this).find(".parentmenubtn_" + id).text();
                            $(this).toggle(ftr.toLowerCase().indexOf(value) > -1)
                        });
                    });
                    $(".myInputChild").on("keyup", function () {
                        var id = ($(this).attr('id'));
                        var value = $(this).val().toLowerCase();
                        $("#SearchChild_" + id + " .childmenubtnfilter_" + id).filter(function () {
                            var ftr = $(this).find(".childmenubtn_" + id).text();
                            $(this).toggle(ftr.toLowerCase().indexOf(value) > -1)
                        });
                    });
                }
            });
        }
        fillMenuManagement();
        function FillStatus(selected) {
            $.ajax({
                type: "post",
                url: "common/cfMenuManagement.php",
                data: "action=FILLSTATUS&select=" + selected + "",
                success: function (data) {
                    $("#ddlStatus").html(data);
                    $("#ddlStatusParent").html(data);
                    $("#ddlStatusFunction").html(data);
                }
            });
        }

        function FillDisplayOrder() {
            $.ajax({
                type: "post",
                url: "common/cfMenuManagement.php",
                data: "action=FILLDISPLAYORDER",
                success: function (data) {
                    $("#txtDisplayOrder").val(data);
                }
            });
        }
        function FillDisplayOrderParent(code) {
            $.ajax({
                type: "post",
                url: "common/cfMenuManagement.php",
                data: "action=FILLDISPLAYORDERPARENT&RootCode=" + code + "",
                success: function (data) {
                    $("#txtDisplayOrderParent").val(data);
                }
            });
        }
        function FillDisplayOrderChild(code) {
            $.ajax({
                type: "post",
                url: "common/cfMenuManagement.php",
                data: "action=FILLDISPLAYORDERCHILD&ParentCode=" + code + "",
                success: function (data) {
                    $("#txtDisplayOrderFunction").val(data);
                }
            });
        }
        function FillRootForParent(code) {
            $.ajax({
                type: "post",
                url: "common/cfMenuManagement.php",
                data: "action=FILLROOTFORPARENT&RootCode=" + code + "",
                success: function (data) {
                    $("#ddlRootParent").html(data);
                    $("#ddlRootFunction").html(data);
                }
            });
        }

        function FillParent(rootmenu) {
            $.ajax({
                type: "post",
                url: "common/cfMenuManagement.php",
                data: "action=FILLPARENT&values=" + rootmenu + "",
                success: function (data) {
                    $("#ddlParentFunction").html(data);
                }
            });
        }

        $("#ddlRootFunction").change(function () {
            //FillParent(this.value);
        });

        $("#grid").on("click", ".clickManageRootMenu", function () {
            var selected = "0";
            FillStatus(selected);
            FillDisplayOrder();
            var mybtnid = $(this).attr('id');
            $("#action").val(mybtnid);
            $("#operation").html("(Add New Root Menu)");
            var modal = document.getElementById('ModelRootMenu');
            var span = document.getElementsByClassName("closeRoot")[0];
            modal.style.display = "block";
            span.onclick = function () {
                modal.style.display = "none";
            }
        });

        $("#grid").on("click", ".clickManageParentMenu", function () {
            var code = $(this).attr('id');
            var root = "0";
            var selected = "0";
            FillStatus(selected);
            FillDisplayOrderParent(code);
            FillRootForParent(code);
            $("#actionParent").val("AddParentMenu");
            $("#operationParent").html("(Add New Parent Menu)");
            var modal = document.getElementById('ModelParentFunction');
            var span = document.getElementsByClassName("closeParent")[0];
            modal.style.display = "block";
            span.onclick = function () {
                modal.style.display = "none";
            }
        });

        $("#grid").on("click", ".clickManageChildMenu", function () {
            var code = $(this).attr('id');
            var root = $(this).attr('name');
            var selected = "0";
            FillStatus(selected);
            FillDisplayOrderChild(code);
            FillRootForParent(root);
            FillParent(code);
            $("#actionChild").val("AddChildMenu");
            $("#operationChild").html("(Add New Child Menu)");
            var modal = document.getElementById('ModelChild');
            var span = document.getElementsByClassName("closeChild")[0];
            modal.style.display = "block";
            span.onclick = function () {
                modal.style.display = "none";
            }
        });


        $("#btnAddChildMenu").click(function () {
            if ($("#frmFunctionMaster").valid())
            {
                $('#responseChild').empty();
                $('#responseChild').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                $.ajax({
                    url: "common/cfMenuManagement.php",
                    type: "POST",
                    data: $("#frmFunctionMaster").serialize(),
                    success: function (data)
                    {
                        $('#responseChild').empty();
                        data = data.replace(/\s/g, '');
                        if (data === "success")
                        {
                            $('#responseChild').empty();
                            $('#responseChild').append("<p class='alert-success'><span><img src=images/correct.gif width=10px style='width: 25px;margin: 0 10px 4px 0;' /></span><span style='color: green;'>Child Menu Added Successfully.</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmMenuManagement.php";
                            }, 2000);

                        } else if (data === "update")
                        {
                            $('#responseChild').empty();
                            $('#responseChild').append("<p class='alert-success'><span><img src=images/correct.gif width=10px style='width: 25px;margin: 0 10px 4px 0;' /></span><span style='color: green;'>Child Menu Update Successfully.</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmMenuManagement.php";
                            }, 2000);
                        } else if (data === "error")
                        {
                            $('#responseChild').empty();
                            $('#responseChild').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span style='color: red;'><span>Error To Add Child Menu.</span></p>");
                        } else if (data === "duplicate")
                        {
                            $('#responseChild').empty();
                            $('#responseChild').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span><span style='color: red;'>Child Menu Already Exists.</span></p>");
                        } else {
                            $('#responseChild').empty();
                            $('#responseChild').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span><span style='color: red;'>" + data + "</span></p>");
                        }
                    }
                });
            } else
            {
                return false;
            }
        });

        $("#btnAddParentMenu").click(function () {
            if ($("#frmParentFunctionMaster").valid())
            {
                $('#responseParent').empty();
                $('#responseParent').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                $.ajax({
                    url: "common/cfMenuManagement.php",
                    type: "POST",
                    data: $("#frmParentFunctionMaster").serialize(),
                    success: function (data)
                    {
                        $('#responseRoot').empty();
                        data = data.replace(/\s/g, '');
                        if (data === "success")
                        {
                            $('#responseParent').empty();
                            $('#responseParent').append("<p class='alert-success'><span><img src=images/correct.gif width=10px style='width: 25px;margin: 0 10px 4px 0;' /></span><span style='color: green;'>Parent Menu Added Successfully.</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmMenuManagement.php";
                            }, 2000);

                        } else if (data === "update")
                        {
                            $('#responseParent').empty();
                            $('#responseParent').append("<p class='alert-success'><span><img src=images/correct.gif width=10px style='width: 25px;margin: 0 10px 4px 0;' /></span><span style='color: green;'>Parent Menu Update Successfully.</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmMenuManagement.php";
                            }, 2000);
                        } else if (data === "error")
                        {
                            $('#responseParent').empty();
                            $('#responseParent').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span style='color: red;'><span>Error To Add Parent Menu.</span></p>");
                        } else if (data === "duplicate")
                        {
                            $('#responseParent').empty();
                            $('#responseParent').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span><span style='color: red;'>Parent Menu Already Exists.</span></p>");
                        } else {
                            $('#responseParent').empty();
                            $('#responseParent').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span><span style='color: red;'>" + data + "</span></p>");
                        }
                    }
                });
            } else
            {
                return false;
            }
        });

        $("#btnAddRootMenu").click(function () {
            if ($("#frmRootMenu").valid())
            {
                $('#responseRoot').empty();
                $('#responseRoot').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                $.ajax({
                    url: "common/cfMenuManagement.php",
                    type: "POST",
                    data: $("#frmRootMenu").serialize(),
                    success: function (data)
                    {
                        $('#responseRoot').empty();
                        data = data.replace(/\s/g, '');
                        if (data === "success")
                        {
                            $('#responseRoot').empty();
                            $('#responseRoot').append("<p class='alert-success'><span><img src=images/correct.gif width=10px style='width: 25px;margin: 0 10px 4px 0;' /></span><span style='color: green;'>Root Menu Added Successfully.</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmMenuManagement.php";
                            }, 2000);

                        } else if (data === "update")
                        {
                            $('#responseRoot').empty();
                            $('#responseRoot').append("<p class='alert-success'><span><img src=images/correct.gif width=10px style='width: 25px;margin: 0 10px 4px 0;' /></span><span style='color: green;'>Root Menu Update Successfully.</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmMenuManagement.php";
                            }, 2000);
                        } else if (data === "error")
                        {
                            $('#responseRoot').empty();
                            $('#responseRoot').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span style='color: red;'><span>Error To Add Root Menu.</span></p>");
                        } else if (data === "duplicate")
                        {
                            $('#responseRoot').empty();
                            $('#responseRoot').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span><span style='color: red;'>Root Menu Already Exists.</span></p>");
                        } else {
                            $('#responseRoot').empty();
                            $('#responseRoot').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span><span style='color: red;'>" + data + "</span></p>");
                        }
                    }
                });
            } else
            {
                return false;
            }
        });

        $("#grid").on("click", ".fun_delete_root", function () {
            var del_id = $(this).attr("id");
            var result = confirm('Are You Sure Want To Delete This Root Menu.');
            if (result == true) {
                $.ajax({
                    type: "post",
                    url: "common/cfMenuManagement.php",
                    data: "action=DELETEROOT&deleteid=" + del_id + "",
                    success: function (data) {
                        fillMenuManagement();
                    }
                });
            } else {
                return false;
            }
        });

        $("#grid").on("click", ".fun_delete_parent", function () {
            var del_id = $(this).attr("id");
            var result = confirm('Are You Sure Want To Delete This Parent Menu.');
            if (result == true) {
                $.ajax({
                    type: "post",
                    url: "common/cfMenuManagement.php",
                    data: "action=DELETEPARENT&deleteid=" + del_id + "",
                    success: function (data) {
                        fillMenuManagement();
                    }
                });
            } else {
                return false;
            }
        });

        $("#grid").on("click", ".fun_delete_child", function () {
            var del_id = $(this).attr("id");
            var result = confirm('Are You Sure Want To Delete This Child Menu.');
            if (result == true) {
                $.ajax({
                    type: "post",
                    url: "common/cfMenuManagement.php",
                    data: "action=DELETECHILD&deleteid=" + del_id + "",
                    success: function (data) {
                        fillMenuManagement();
                    }
                });
            } else {
                return false;
            }
        });

        $("#grid").on("click", ".fun_edit_child", function () {
            var editid = $(this).attr("id");
            var code = $(this).attr("name");
            $("#operationChild").html("(Update Child Menu)");
            var modal = document.getElementById('ModelChild');
            var span = document.getElementsByClassName("closeChild")[0];
            modal.style.display = "block";
            span.onclick = function () {
                $('#frmFunctionMaster')[0].reset();
                modal.style.display = "none";
            }
            $.ajax({
                type: "post",
                url: "common/cfMenuManagement.php",
                data: "action=FillChildEditData&editid=" + editid + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    $("#actionChild").val(data[0].action);
                    $("#txtChildCode").val(data[0].Function_Code);
                    $("#txtFunctionName").val(data[0].Function_Name);
                    $("#txtFunctionURL").val(data[0].Function_URL);
                    $("#txtDisplayOrderFunction").val(data[0].Function_Display);
                    FillRootForParent(code);
                    FillParent(data[0].Function_Parent);
                    FillStatus(data[0].Function_Status);
                }
            });
        });

        $("#grid").on("click", ".fun_edit_parent", function () {
            var editid = $(this).attr("id");
            $("#operationParent").html("(Update Parent Menu)");
            var modal = document.getElementById('ModelParentFunction');
            var span = document.getElementsByClassName("closeParent")[0];
            modal.style.display = "block";
            span.onclick = function () {
                $('#frmParentFunctionMaster')[0].reset();
                modal.style.display = "none";
            }
            $.ajax({
                type: "post",
                url: "common/cfMenuManagement.php",
                data: "action=FillParentEditData&editid=" + editid + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    $("#actionParent").val(data[0].action);
                    $("#txtParentFunctionName").val(data[0].Parent_Function_Name);
                    $("#txtDisplayOrderParent").val(data[0].Parent_Function_Display);
                    $("#txtParentCode").val(data[0].Parent_Function_Code);
                    FillRootForParent(data[0].Parent_Function_Root);
                    FillStatus(data[0].Parent_Function_Status);
                }
            });
        });

        $("#grid").on("click", ".fun_edit_root", function () {
            var editid = $(this).attr("id");
            $("#operation").html("(Update Root Menu)");
            var modal = document.getElementById('ModelRootMenu');
            var span = document.getElementsByClassName("close")[0];
            modal.style.display = "block";
            span.onclick = function () {
                $('#frmRootMenu')[0].reset();
                modal.style.display = "none";
            }
            $.ajax({
                type: "post",
                url: "common/cfMenuManagement.php",
                data: "action=FillRootEditData&editid=" + editid + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    $("#action").val(data[0].action);
                    $("#txtRootCode").val(data[0].root_menu_code);
                    $("#txtRootMenu").val(data[0].root_menu_name);
                    $("#txtDisplayOrder").val(data[0].root_menu_display);
                    FillStatus(data[0].root_menu_status);
                }
            });
        });

    });
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script>
    $("#frmRootMenu").validate({
        rules: {
            action: "required",
            txtRootMenu: "required",
            txtDisplayOrder: "required",
            ddlStatus: "required"

        },
        messages: {
            action: "Action Operation Not Set",
            txtRootMenu: "Please Enter Root Menu",
            txtDisplayOrder: "Please Enter Display Order",
            ddlStatus: "Please Select Status"
        },
    });
    $("#frmParentFunctionMaster").validate({
        rules: {
            action: "required",
            ddlRootParent: "required",
            txtParentFunctionName: "required",
            txtDisplayOrderParent: "required",
            ddlStatusParent: "required"

        },
        messages: {
            action: "Action Operation Not Set",
            ddlRootParent: "Please Select Root Menu",
            txtParentFunctionName: "Please Enter Parent Function Name",
            txtDisplayOrderParent: "Please Enter Display Order",
            ddlStatusParent: "Please Select Status"
        },
    });
    $("#frmFunctionMaster").validate({
        rules: {
            action: "required",
            txtFunctionName: "required",
            ddlRootFunction: "required",
            ddlParentFunction: "required",
            txtFunctionURL: "required",
            txtDisplayOrderFunction: "required",
            ddlStatusFunction: "required"
        },
        messages: {
            action: "Action Operation Not Set",
            txtFunctionName: "Please Enter Child Menu",
            ddlRootFunction: "Please Select Root Menu",
            ddlParentFunction: "Please Select Parent Menu",
            txtFunctionURL: "Please Enter URL",
            txtDisplayOrderFunction: "Please Enter Display Order",
            ddlStatusFunction: "Please Select Status"
        },
    });
</script>
<style>
    .our-faq-wrapper {
        float: left;
        width: 100%;
        /*  padding: 89px 0 60px;*/
        background:#f5f5f5;
    }

    .our-faq-wrapper {
        /*    padding: 90px 0 60px;*/
        background:#fff;  
    }
    .accordion .card {
        border: none;
        margin-bottom: 15px;
        border-bottom: solid 1px #666699;
        border-left: solid 1px #666699;
        border-right: solid 1px #cccccc;
    }
    .our-faq-wrapper .btn-link {
        display: block;
        width: 100%;
        text-align: left;
        position: relative;
        background: #666699;
        color: #fff;
        border-radius: 0;
        padding: 7px 15px 7px;
        font-size: 16px;
        overflow: hidden;
        border: none;
        font-weight:600;  
    }
    .our-faq-wrapper .btn-link:hover, .our-faq-wrapper .btn-link:focus {
        text-decoration: none;
    }
    .our-faq-wrapper .btn-link:after {
        position: absolute;
        content: '\f148';
        right: 15px;
        top: 50%;
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
        font-family: fontawesome;
    }
    .our-faq-wrapper .btn-link.collapsed:after {
        content: '\f149';
    }
    .our-faq-wrapper .btn-link:before {
        position: absolute;
        content: '';
        background: #CCCCCC;
        -webkit-transform: skew(-35deg);
        transform: skew(-35deg);
        height: 100%;
        width: 50%;
        left: 90%;
        top: 0;
    }
    .card-header {
        padding: 0;
        background: transparent;
        border-bottom: none;
    }
    .card-body {
        position: relative;
    }
    .card-body:after {
        position: absolute;
        content: '';
        height: 2px;
        width: 100%;
        left: 0;
        bottom: 0;
        background: #242c42;
        margin: 0 0 -21px 0;
    }
    .card-body:before {
        position: absolute;
        content: '';
        height: 2px;
        width: 50%;
        left: 0;
        bottom: 0;
        background: #86bc42;
        z-index: 1;
        margin: 0 0 -21px 0;
    }    
    .parentclass:before {
        background: #666699 !important;

    }
    .our-faq-wrapper .childslacc:before {
        background: #3366cc !important;
    }
    .parentclass:after {
        content: '\f067' !important;

    }
    .childslacc:after {
        right: 9px !important;
        content: '\f067' !important;

    }
    .rootmenuclass:after {
        content: '\f067' !important;
    }
    .searchrootbtn{
        color: #666699;
        border-radius: 5px;
        float: right;
        margin-right: 145px;
        border: none;
    }
    .searchrootbtn:focus{
        outline: none;
    }
</style>
</html>
