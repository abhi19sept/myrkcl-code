<?php
$title="Center Wise Status Report";
include ('header.php'); 
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258'
|| $_SESSION['User_Code'] == '6588' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
			<div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">Center Wise Status Report</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
					<form name="frmblock" id="frmclock" class="form-inline" role="form" enctype="multipart/form-data"> 
						<div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">IT-GK Code:</label>
                               <input type="text" class="form-control" maxlength="8" name="txtCenterCode" id="txtCenterCode" placeholder="IT-GK Code" onkeypress="javascript:return allownumbers(event);"/>
							   
                            </div>  
						</div>  
               
                         <div class="container">
						<p class="btn submit" id="btnviewcontainer"> 
                            <input type="button" name="btnView" id="btnView" class="btn btn-primary" value="View Details"/> 
						</p>
                        </div>    
                     <div id="grid" style="margin-top:15px;"> </div>       		
     </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>              
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";

        $(document).ready(function () {



            function GetCenterStatus() {
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfCenterStatusReport.php",
                    data: "action=GetCenterDetails&CenterCode=" + txtCenterCode.value,
                    success: function (data) {
						$('#response').empty();
                        if (data == 0)
                        {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Enter IT-GK Code</span></p>");
                        }
                        else
                        {
                            $("#grid").html(data);
                            $('#example').DataTable({
								dom: 'Bfrtip',
								buttons: [
									'copy', 'csv', 'excel', 'pdf', 'print'
								]
							});
                        }
                    }
                });
            }

            $("#btnView").click(function () {
				 				
					GetCenterStatus();
            });
        });
    </script>
</html>