<?php
$title = "Ownership Change Final Approval Process";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var CenterCode='" . $_REQUEST['cc'] . "'</script>";
    echo "<script>var Ack='" . $_REQUEST['ack'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if (isset($_REQUEST['cc'])) { 
$link_address = "frmoldcenterdashboard.php?cc=" . $_REQUEST['cc'];
} else {
$link_address = "dashboard.php";   
}
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">			 
        <div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">Final Approval of Ownership Change Application Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" action="" class="form-inline">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <div class="panel panel-success">
                            <div class="panel-heading">View Old/Existing Center's Details</div>
                            <div class="panel-body">	

                                <div class="col-sm-12" > 
                                    <!--<label for="photo">SP-Center Agreement:<span class="star">*</span></label> </br>-->
                                    <a href="<?php echo $link_address; ?>" target="_blank"><button type="button" id="OldCenterDashboard" style="width:100%" class="btn btn-primary" name="OldCenterDashboard" width="150px" height="150px">
                                            Click Here to View Old/Existing Center's Details</button></a>
                                </div>
                            </div>
                        </div>
                    <br>

                        <div id="errorBox"></div>

                        <div class="panel panel-info">
                            <div class="panel-heading">New Applied/Proposed Center Details</div>
                            <div class="panel-body">
                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">New Organization Name:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">Registration No:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                                </div>


                                <!--                        <div class="col-sm-4 form-group">     
                                                            <label for="faname">Date of Establishment:</label>
                                                            <input type="text" class="form-control" readonly="true" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                                                        </div>-->

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Type of Organization:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtType" id="txtType" placeholder="Type Of Organization">  
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Name of Service Provider:</label>
                                    <textarea class="form-control" readonly="true" name="txtrspname" id="txtrspname" placeholder="Name of the Organization/Center"></textarea>
                                </div>
                            </div>




                            <div class="container">




                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Document Type:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <label for="SelectType">Application Type:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtRole" id="txtRole" placeholder="Role"/>     

                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="email">New Owner's Email Id:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="Mobile">New Owner's Mobile Number:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                                </div>


                            </div>
                        </div>


                    <!--                    <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">District:</label>
                                                <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                                                <input type="hidden" class="form-control"  name="txtDistrictCode" id="txtDistrictCode">
                                            </div>
                    
                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">Tehsil:</label>
                                                <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                                            </div>
                    
                                            <div class="col-sm-4 form-group"> 
                                                <label for="address">Area Type:</label>
                                                <input type="text" class="form-control" readonly="true" name="txtAreaType" id="txtAreaType" placeholder="Area Type">    
                                            </div>
                    
                    
                                            <div class="col-sm-4 form-group">     
                                                <label for="address">Address:</label>
                                                <textarea class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road"></textarea>
                    
                                            </div>
                                        </div>-->
                    <div class="panel panel-success">
                        <div class="panel-heading">Payment Details of Ownership Change Application Process</div>
                        <div class="panel-body">
                            <div id="processingfeepayment"></div>

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">Uploaded Signed SP-Center Agreement</div>
                        <div class="panel-body">	

                            <div class="col-sm-12" > 
                                <label for="photo">SP-Center Agreement:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreviewagreement" style="width:100%" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <br><br><br><br> 
                <div class="container">
                    <input type="button" name="btnshowsms" id="btnshowsms" class="btn btn-default" value="Send SMS to ITGK"/>   
                </div>
                <div class="panel panel-info" id="smspanel" style="display:none">
                    <div class="panel-heading">Send SMS to ITGK</div>
                    <div class="panel-body">	
                        <div id="SMSresponse"></div>
                        <div class="container">
                            <div class="col-sm-8">     
                                <label for="learnercode">Enter SMS text:</label>
                                <textarea class="form-control" width="700px" height="103px"   name="txtsms" id="txtsms" value=""></textarea>
                            </div>
                            <br><br>
                            <div class="col-sm-4"> 
                                <input type="button" name="btnsendsms" id="btnsendsms" class="btn btn-primary" value="Send SMS to ITGK"/>    
                            </div>
                        </div>
                    </div>
                </div>
                        </div>
                    </div>
                    <!--                    <div class="panel panel-success">
                                            <div class="panel-heading">Uploaded NCR Center Visit Photos</div>
                                            <div class="panel-body">	
                    
                                                <div class="col-sm-4 form-group" > 
                                                    <label for="photo">Theory Room Photo:<span class="star">*</span></label> </br>
                                                    <button type="button" id="uploadPreviewphoto1" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                                        View Document</button>
                                                </div>
                                                <div class="col-sm-4 form-group" > 
                                                    <label for="photo">Reception Photo:<span class="star">*</span></label> </br>
                                                    <button type="button" id="uploadPreviewphoto2" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                                        View Document</button>
                                                </div>
                                                <div class="col-sm-4 form-group" > 
                                                    <label for="photo">Exterior Photo:<span class="star">*</span></label> </br>
                                                    <button type="button" id="uploadPreviewphoto3" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                                        View Document</button>
                                                </div>
                                                <div class="col-sm-4 form-group" > 
                                                    <label for="photo">Other Photo:<span class="star">*</span></label> </br>
                                                    <button type="button" id="uploadPreviewphoto4" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                                        View Document</button>
                                                </div>
                                            </div>
                                        </div>-->
                    <div class="panel panel-info">
                        <div class="panel-heading">Uploaded New Center's Owner's Documents Details</div>
                        <div class="panel-body">	

                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Pan Card:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview1" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">AADHAR Card:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview2" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Address Proof:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview3" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Ownership Change App Form:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview4" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-success">
                        <div class="panel-heading">Uploaded New Center's/Organization Type Documents Details</div>
                        <div class="panel-body">
                            <button type="button" id="showmodal" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc1"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview10" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc2"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview11" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc3"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview12" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc4"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview13" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc5"> 
                                <label for="orgtypedoc5"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview14" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc6"> 
                                <label for="orgtypedoc6"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview15" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc7"> 
                                <label for="orgtypedoc7"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview16" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                        </div>
                    </div>

                    <!--                    <div class="panel panel-info">
                                            <div class="panel-heading">Organization Details</div>
                                            <div class="panel-body">
                                                <div class="container">
                                                    <div class="col-sm-4 form-group" > 
                                                        <label for="orgdetail1">Bank Account Details</label> </br>
                                                        <button type="button" id="bankdetail" class="btn btn-primary" name="bankdetail" width="150px" height="150px">
                                                            View Details</button>	
                                                    </div>
                                                    <div class="col-sm-4 form-group" > 
                                                        <label for="orgdetail1">Owner Details</label> </br>
                                                        <button type="button" id="ownerdetail" class="btn btn-primary" name="ownerdetail" width="150px" height="150px">
                                                            View Details</button>	
                                                    </div>
                                                    <div class="col-sm-4 form-group" > 
                                                        <label for="orgdetail1">HR Details</label> </br>
                                                        <button type="button" id="hrdetail" class="btn btn-primary" name="hrdetail" width="150px" height="150px">
                                                            View Details</button>	
                                                    </div>
                                                    <div class="col-sm-4 form-group" > 
                                                        <label for="orgdetail1">System And Intake Details</label> </br>
                                                        <button type="button" id="sidetail" class="btn btn-primary" name="sidetail" width="150px" height="150px">
                                                            View Details</button>	
                                                    </div>
                                                </div>
                                                <div class="container">
                                                    <div class="col-sm-4 form-group" > 
                                                        <label for="orgdetail1">Premises Details</label> </br>
                                                        <button type="button" id="premisesdetail" class="btn btn-primary" name="premisesdetail" width="150px" height="150px">
                                                            View Details</button>	
                                                    </div>
                                                    <div class="col-sm-4 form-group" > 
                                                        <label for="orgdetail1">IT Peripherals Details</label> </br>
                                                        <button type="button" id="itdetail" class="btn btn-primary" name="itdetail" width="150px" height="150px">
                                                            View Details</button>	
                                                    </div>
                                                    <div class="col-sm-4 form-group" > 
                                                        <label for="orgdetail1">SP Details</label> </br>
                                                        <button type="button" id="showspdetail" class="btn btn-primary" name="showspdetail" width="150px" height="150px">
                                                            View Details</button>	
                                                    </div>
                                                    <div class="col-sm-4 form-group" > 
                                                        <label for="orgdetail1">Hide All Details</label> </br>
                                                        <button type="button" id="hideall" class="btn btn-primary" name="hideall" width="150px" height="150px">
                                                            Hide Details</button>	
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                    <!--                    <div class="panel panel-info" id="bankaccount" style="display:none">
                                            <div class="panel-heading">Bank Account Details</div>
                                            <div class="panel-body">
                                                <div id="bankdatagird"></div>
                    
                                            </div>
                                        </div>
                                        <div class="panel panel-info" id="ownergrid" style="display:none">
                                            <div class="panel-heading">Owner Details</div>
                                            <div class="panel-body">
                                                <div id="ownerdatagrid"></div>
                    
                                            </div>
                                        </div>
                                        <div class="panel panel-info" id="hrgrid" style="display:none">
                                            <div class="panel-heading">HR Details</div>
                                            <div class="panel-body">
                                                <div id="hrdatagrid"></div>
                                            </div>
                                        </div>
                                        <div class="panel panel-info" id="sigrid" style="display:none">
                                            <div class="panel-heading">System and Intake Details</div>
                                            <div class="panel-body">
                                                <div id="sidatagrid"></div>
                                            </div>
                                        </div>
                                        <div class="panel panel-info" id="premisesgrid" style="display:none">
                                            <div class="panel-heading">Premises Details</div>
                                            <div class="panel-body">
                                                <div id="premisesdatagrid"></div>
                                            </div>
                                        </div>
                                        <div class="panel panel-info" id="itgrid" style="display:none">
                                            <div class="panel-heading">IT Peripherals Details</div>
                                            <div class="panel-body">
                                                <div id="itdatagrid"></div>
                                            </div>
                                        </div>
                                        <div class="panel panel-info" id="locationgrid" style="display:none">
                                            <div class="panel-heading">Location Details</div>
                                            <div class="panel-body">
                                                                                <div id="googleMap"  class="mapclass"></div>
                                            </div>
                                        </div>-->
                    <!--                    <div class="panel panel-info" id="spdetail">
                                            <div class="panel-heading">Service Provider's Registered - Details</div>
                                            <div class="panel-body">
                    
                                                <div class="container">
                                                    <div class="col-sm-4 form-group">     
                                                        <label for="learnercode">Name of Organization/Center:</label>
                                                        <textarea class="form-control" readonly="true" name="txtrspname" id="txtrspname" placeholder="Name of the Organization/Center"></textarea>
                                                    </div>
                    
                    
                                                    <div class="col-sm-4 form-group"> 
                                                        <label for="ename">Address:</label>
                                                        <input type="text" class="form-control" readonly="true" name="Address" id="Address" placeholder="Registration No">     
                                                    </div>
                    
                    
                                                    <div class="col-sm-4 form-group">     
                                                        <label for="faname">Date of Establishment:</label>
                                                        <input type="text" class="form-control" readonly="true" name="txtdate" id="txtdate"  placeholder="YYYY-MM-DD">
                                                    </div>
                    
                                                    <div class="col-sm-4 form-group"> 
                                                        <label for="edistrict">Type of Organization:</label>
                                                        <input type="text" class="form-control" readonly="true" name="txtRspType" id="txtRspType" placeholder="Type Of Organization">  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                    <div class="panel panel-danger">
                        <div class="panel-heading">Verification of Approving Authority (Atleast TWO Authorities Required to Approve)</div>
                        <div class="panel-body">	
                            <div id="otpresponse"></div>
                            <label class="checkbox-inline"><input type="checkbox" name="tech" id="tech" value="1">Mr. Naresh Kumawat (Head Of Technical)</label>&nbsp;&nbsp;
                            <label class="checkbox-inline"><input type="checkbox" name="acc" id="acc" value="24">Mr. Dinesh Khandelwal (C.F.O.)</label>&nbsp;&nbsp;
                            <label class="checkbox-inline"><input type="checkbox" name="markt" id="markt" value="4">Mr. Abhay Shankar (Program Head)</label>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <input type="button" name="btnGenerateOTP" id="btnGenerateOTP" class="btn btn-primary" value="Generate OTP"/>
                            </label>
                        </div>
                    </div>

                    <div class="panel panel-info" id="otpverify" style="display:none;">
                        <div class="panel-heading">Enter OTP to Verify</div>
                        <div class="panel-body">
                            <div id="verifyresponse"></div>
                            <div class="container">
                                <div class="col-sm-4 form-group" id="divotptech" style="display:none;"> 
                                    <label for="otpTech"></label>
                                    <input type="text" class="form-control" maxlength="6" name="txtOTPTech" id="txtOTPTech" placeholder="OTP Delivered to Mr. Naresh Kumawat">     
                                </div>
                                <div class="col-sm-4 form-group" id="divotpacc" style="display:none;">     
                                    <label for="otpAcc"></label>
                                    <input type="text" class="form-control" maxlength="6" name="txtOTPAcc" id="txtOTPAcc"  placeholder="OTP Delivered to Mr. Dinesh Khandelwal">
                                </div>
                                <div class="col-sm-4 form-group" id="divotpmarkt" style="display:none;">     
                                    <label for="otpMarkt"></label>
                                    <input type="text" class="form-control" maxlength="6" name="txtOTPMarkt" id="txtOTPMarkt"  placeholder="OTP Delivered to Mr. Abhay Shankar">
                                </div>
                                <div class="col-sm-4">
                                    <label for="Verify" id="lblbtnverify">Verify Approving Authority OTP:<span class="star">*</span></label>
                                    <input type="button" name="btnVerify" id="btnVerify" class="btn btn-primary" value="Click here to Verify and Proceed" style="margin-top:20px"/>
                                    <!--<input type="button" name="btnVerified" id="btnVerified" class="btn btn-primary" value="Details Verified" style="margin-top:25px; display:none"/>-->
                                    <label for="tnc" id="btnVerified" style="margin-top:50px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Details Verified</b></label></br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-danger" id="finalsubmission" style="display:none;">
                        <div class="panel-heading">Please Approve all steps to Complete the Process. </div>
                        <div class="panel-body">
                            <div id="finalresponse"></div>
                            <div class="container">
                                <div class="col-sm-4 form-group" id="divstep1"> 
                                    <label for="usermaster">Step 1: Update New-Owner Contact Details</label>
                                    <input type="button" name="btnUser" id="btnUser" class="btn btn-danger" value="Click here to Update"/>
                                    <label for="tnc" id="lbluserupdated" style="display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Contact Details Updated</b></label></br>
                                </div>
                                <div class="col-sm-4 form-group" id="divstep2" style="display:none;">     
                                    <label for="orgdetail">Step 2: Update Organization Details</label>
                                    <input type="button" name="btnOrg" id="btnOrg" class="btn btn-danger" value="Click here to Update"/>
                                    <label for="tnc" id="lblorgupdated" style="display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Organization Details Updated</b></label></br>
                                </div>
                                <div class="col-sm-4 form-group" id="divstep3" style="display:none;">     
                                    <label for="bankaccount">Step 3: Update Bank Account Details</label>
                                    <input type="button" name="btnBank" id="btnBank" class="btn btn-danger" value="Click here to Update"/>
                                    <label for="tnc" id="lblbankupdated" style="display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Bank Account Details Updated</b></label></br>
                                </div>
                                <div class="col-sm-4 form-group" id="divstep4" style="display:none;">     
                                    <label for="spitgagreement">Step 4: Update Sp-Center Agreement</label>
                                    <input type="button" name="btnAgreement" id="btnAgreement" class="btn btn-danger" value="Click here to Update"/>
                                    <label for="tnc" id="lblagreementupdated" style="display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Sp-Center Aggrement Updated</b></label></br>
                                </div>
                                <!--                                <div class="col-sm-4">
                                                                    <label for="Verify" id="lblbtnverify">Verify Approving Authority OTP:<span class="star">*</span></label>
                                                                    <input type="button" name="btnVerify" id="btnVerify" class="btn btn-primary" value="Click here to Verify and Proceed" style="margin-top:20px"/>
                                                                    <input type="button" name="btnVerified" id="btnVerified" class="btn btn-primary" value="Details Verified" style="margin-top:25px; display:none"/>
                                                                    <label for="tnc" id="btnVerified" style="margin-top:50px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Details Verified</b></label></br>
                                                                </div>-->
                            </div>
                        </div>
                    </div>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <div class="container" id="finalsubmit" style="display:none; margin-left: 50px;">
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-danger" value="Click here to Approve"/>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span style="font-size:15px; color: red;">Note : Once approved. Action can not be revert.</span>
                    </div>
            </div>
            </form>
        </div>  

    </div>

    <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" style="width: 100%;height: 500px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h3 id="heading-tittle" class="modal-title">Heading</h3>
                </div>
                <iframe id="viewimagesrc" src="" style="width: 100%;height: 500px;border: none;"></iframe>
                <!--<img id="viewimagesrc" class="thumbnail img-responsive" src="images/not-found.png" name="filePhoto3" width="800px" height="880px">-->
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>
<!--<script type="text/javascript">
    function toggle_visibility1(id) {
        var e = document.getElementById(id);

        //alert(e);
        var f = document.getElementById('ddlstatus').value;
        //alert(f);
        if (f == "Reject")
        {
            e.style.display = 'block';
        } else {
            e.style.display = 'none';
        }

    }
</script>-->

<script type="text/javascript">

    $(document).ready(function () {
        jQuery(".thumbnailmodal").click(function () {
            $('.modal-body').empty();
            var title = $(this).parent('a').attr("title");
            $('.modal-title').html(title);
            $($(this).parents('div').html()).appendTo('.modal-body');
            $('#showmodal').click();
        });
    });
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        } else if (Mode == 'Edit')
        {
            //alert(1);
            fillForm();
            showPaymentDetails();
            //fillCenterVisitPhoto();

        }

        function showPaymentDetails() {
            $.ajax({
                type: "post",
                url: "common/cfOwnershipChangeApproval.php",
                data: "action=PAYMENTDETAIL&centercode=" + CenterCode + "",
                success: function (data) {
                    $("#processingfeepayment").html(data);
                    $('#examplepay').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }
        
        $(".close").on("click",function(){
        $("#testdoc").attr("src"," "); 
    });


        function fillForm()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfOwnershipChangeFinalApproval.php",
                data: "action=PROCESS&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    txtName1.value = data[0].orgname;
                    txtRegno.value = data[0].regno;
                    //txtEstdate.value = data[0].fdate;
                    txtType.value = data[0].orgtype;
                    txtDocType.value = data[0].doctype;
                    txtRole.value = data[0].orgcourse;

                    txtEmail.value = data[0].email;
                    txtMobile.value = data[0].mobile;
                    $("#uploadPreview1").click(function () {
                        $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRPAN/" + data[0].orgdoc);
                    });
                    $("#uploadPreview2").click(function () {
                        $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRUID/" + data[0].orguid);
                    });
                    $("#uploadPreview3").click(function () {
                        $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRAddProof/" + data[0].orgaddproof);
                    });
                    $("#uploadPreview4").click(function () {
                        $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRAppForm/" + data[0].orgappform);
                    });
                    $("#uploadPreviewagreement").click(function () {
                        $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "SPCENTERAGREEMENT/" + data[0].agreement);
                    });
//                    $("#uploadPreview1").attr('src', "upload/NCRPAN/" + data[0].orgdoc);
//                    $("#uploadPreview2").attr('src', "upload/NCRUID/" + data[0].orguid);
//                    $("#uploadPreview3").attr('src', "upload/NCRAddProof/" + data[0].orgaddproof);
//                    $("#uploadPreview4").attr('src', "upload/NCRAppForm/" + data[0].orgappform);

                    if (txtType.value == "Proprietorship/Individual")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Ownership Type Document");
                        jQuery("label[for='orgtypedoc2']").html("Shop Act License");
                        jQuery("label[for='orgtypedoc3']").html("PAN card copy of Proprietor");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled Cheque");

                        $("#doc5").hide();
                        $("#doc6").hide();
                        $("#doc7").hide();


                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_owntype.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_salpi.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_panpi.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_ccpi.pdf");
                        });

                    }
                    if (txtType.value == "Partnership")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Copy of Partnership Deed");
                        jQuery("label[for='orgtypedoc2']").html("Authorization letter");
                        jQuery("label[for='orgtypedoc3']").html("PAN card of Partnership firm");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled cheque of firm");
                        jQuery("label[for='orgtypedoc5']").html("Address Proof documents");
                        jQuery("label[for='orgtypedoc6']").html("Registration certificate");
                        jQuery("label[for='orgtypedoc7']").html("Shop Act Registration Copy");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_copd.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_al.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ppf.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ccpf.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_apdpf.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_rcrf.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_sarpf.pdf");
                        });

                    }
                    if (txtType.value == "Private Ltd." || txtType.value == "Public Ltd.")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Incorporation");
                        jQuery("label[for='orgtypedoc2']").html("List Of Directors");
                        jQuery("label[for='orgtypedoc3']").html("Pan Card Of Company");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled Cheque");
                        jQuery("label[for='orgtypedoc5']").html("Board Resolution Copy");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof Document");
                        jQuery("label[for='orgtypedoc7']").html("Shop Act Registration Copy");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_coi.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_lod.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_copan.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_ccc.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_cobr.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_apd.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_csar.pdf");
                        });

                    }
                    if (txtType.value == "Limited Liability Partnership (LLP)")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Incorporation");
                        jQuery("label[for='orgtypedoc2']").html("List of Partners");
                        jQuery("label[for='orgtypedoc3']").html("Copy of PAN card of LLP");
                        jQuery("label[for='orgtypedoc4']").html("Canceled cheque of LLP");
                        jQuery("label[for='orgtypedoc5']").html("Copy of Board Resolutions");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof Document");
                        jQuery("label[for='orgtypedoc7']").html("Copy of Shop Act Registration");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_coillp.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_lop.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_panllp.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_ccllp.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_cobrllp.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_apdllp.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_csarllp.pdf");
                        });

                    }
                    if (txtType.value == "Trust" || txtType.value == "Society" || txtType.value == "Coorperative Society" || txtType.value == "Others")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Registration");
                        jQuery("label[for='orgtypedoc2']").html("PAN Card of Organization");
                        jQuery("label[for='orgtypedoc3']").html("Cancelled cheque");
                        jQuery("label[for='orgtypedoc4']").html("List of Executive Body");
                        jQuery("label[for='orgtypedoc5']").html("Copy of Board Resolution");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof documents");

                        $("#doc7").hide();

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cor.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_panoth.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_ccoth.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_leb.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cbr.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_adpoth.pdf");
                        });

                    }
                }
            });
        }

//        function showBankAccountData() {
//
//            $.ajax({
//                type: "post",
//                url: "common/cfNcrFinalApproval.php",
//                data: "action=BANKACCOUNT&centercode=" + CenterCode + "",
//                success: function (data) {
//
//                    $("#bankdatagird").html(data);
//                    $('#example').DataTable({
//                        dom: 'Bfrtip',
//                        buttons: [
//                            'copy', 'csv', 'excel', 'pdf', 'print'
//                        ]
//                    });
//
//
//                }
//            });
//        }
//        $("#bankdetail").click(function () {
//            showBankAccountData();
//            $("#bankaccount").show(3000);
//            $("#ownergrid").hide();
//            $("#hrgrid").hide();
//            $("#sigrid").hide();
//            $("#premisesgrid").hide();
//            $("#itgrid").hide();
//            $("#spdetail").hide();
//        });

//        function showOwnerData() {
//            //alert("HII");
//            $.ajax({
//                type: "post",
//                url: "common/cfNcrFinalApproval.php",
//                data: "action=OWNERDATA&centercode=" + CenterCode + "",
//                success: function (data) {
////alert(data);
//                    $("#ownerdatagrid").html(data);
//                    $('#example1').DataTable({
//                        dom: 'Bfrtip',
//                        buttons: [
//                            'copy', 'csv', 'excel', 'pdf', 'print'
//                        ]
//                    });
////alert(data);
//                }
//            });
//        }
//
//        $("#ownerdetail").click(function () {
//            showOwnerData();
//            $("#bankaccount").hide();
//            $("#ownergrid").show(3000);
//            $("#hrgrid").hide();
//            $("#sigrid").hide();
//            $("#premisesgrid").hide();
//            $("#itgrid").hide();
//            $("#spdetail").hide();
//        });
//
//        function showHRData() {
//
//            $.ajax({
//                type: "post",
//                url: "common/cfNcrFinalApproval.php",
//                data: "action=HRDATA&centercode=" + CenterCode + "",
//                success: function (data) {
//
//                    $("#hrdatagrid").html(data);
//                    $('#example2').DataTable({
//                        dom: 'Bfrtip',
//                        buttons: [
//                            'copy', 'csv', 'excel', 'pdf', 'print'
//                        ]
//                    });
//                }
//            });
//        }
//
//        $("#hrdetail").click(function () {
//            showHRData();
//            $("#bankaccount").hide();
//            $("#ownergrid").hide();
//            $("#hrgrid").show(3000);
//            $("#sigrid").hide();
//            $("#premisesgrid").hide();
//            $("#itgrid").hide();
//            $("#spdetail").hide();
//        });
//
//        function showSIData() {
//            //alert("HII");
//            $.ajax({
//                type: "post",
//                url: "common/cfNcrFinalApproval.php",
//                data: "action=SIDATA&centercode=" + CenterCode + "",
//                success: function (data) {
////alert(data);
//                    $("#sidatagrid").html(data);
//                    $('#example3').DataTable({
//                        dom: 'Bfrtip',
//                        buttons: [
//                            'copy', 'csv', 'excel', 'pdf', 'print'
//                        ]
//                    });
//
//                }
//            });
//        }
//
//
//        $("#sidetail").click(function () {
//            showSIData();
//            $("#bankaccount").hide();
//            $("#ownergrid").hide();
//            $("#hrgrid").hide();
//            $("#sigrid").show(3000);
//            $("#premisesgrid").hide();
//            $("#itgrid").hide();
//            $("#spdetail").hide();
//        });
//
//        function showPremisesData() {
//
//            $.ajax({
//                type: "post",
//                url: "common/cfNcrFinalApproval.php",
//                data: "action=PREMISESDATA&centercode=" + CenterCode + "",
//                success: function (data) {
//
//                    $("#premisesdatagrid").html(data);
//                    $('#example4').DataTable({
//                        dom: 'Bfrtip',
//                        buttons: [
//                            'copy', 'csv', 'excel', 'pdf', 'print'
//                        ]
//                    });
//
//                }
//            });
//        }
//
//        $("#premisesdetail").click(function () {
//            showPremisesData();
//            $("#bankaccount").hide();
//            $("#ownergrid").hide();
//            $("#hrgrid").hide();
//            $("#sigrid").hide();
//            $("#premisesgrid").show(3000);
//            $("#itgrid").hide();
//            $("#spdetail").hide();
//        });
//
//        function showITData() {
//
//            $.ajax({
//                type: "post",
//                url: "common/cfNcrFinalApproval.php",
//                data: "action=ITDATA&centercode=" + CenterCode + "",
//                success: function (data) {
//
//                    $("#itdatagrid").html(data);
//                    $('#example5').DataTable({
//                        dom: 'Bfrtip',
//                        buttons: [
//                            'copy', 'csv', 'excel', 'pdf', 'print'
//                        ]
//                    });
//                }
//            });
//        }
//
//
//        $("#itdetail").click(function () {
//            showITData();
//            $("#bankaccount").hide();
//            $("#ownergrid").hide();
//            $("#hrgrid").hide();
//            $("#sigrid").hide();
//            $("#premisesgrid").hide();
//            $("#itgrid").show(3000);
//            $("#spdetail").hide();
//        });
        FillCurrentRsp();
        function FillCurrentRsp() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfOwnershipChangeApproval.php",
                data: "action=GETREGRSP&centercode=" + CenterCode + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Serice Provider Not Found " + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        txtrspname.value = data[0].rspname;
                        //Address.value = data[0].mobno;
                        //txtdate.value = data[0].date;
                        //txtRspType.value = data[0].rsptype;
                    }
                }
            });
        }
        
        
        $("#btnshowsms").click(function () {

            $("#submitdiv").hide();
            $("#btnshowsms").hide();
            $("#smspanel").show(3000);
            document.getElementById("txtsms").defaultValue = "Example: Dear ITGK, Your Ownership Change Application has been put on hold due to incomplete/incorrect documents uploaded. Kindly update document name for approval from RKCL.";

        });
        
        $("#btnsendsms").click(function () {
                $('#SMSresponse').empty();
                $('#SMSresponse').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfOwnershipChangeFinalApproval.php"; // the script where you handle the form input.            
                var data;
                if (Mode == 'Add')
                {

                } else
                {
                    //data = "action=LoginApprove&orgcode=" + Code + "&status=" + ddlstatus.value + "&remark=" + txtRemark.value + "&districtcode=" + txtDistrictCode.value + ""; // serializes the form's elements.
                    data = "action=SendSMS&sms=" + txtsms.value + "&centercode=" + CenterCode + "&mobile=" + txtMobile.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        var result = data.trim();
                        if (result == SuccessfullyInsert || result == 'Success')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>SMS has been sent to ITGK Successfully</span></p>");
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; SMS has been sent to ITGK Successfully.</span>");
                            window.setTimeout(function () {
                                window.location.href = "frmownershipchangefinalapproval.php";
                            }, 3000);

                            Mode = "Add";
                            resetForm("form");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></p>");
                        }
                        //showData();


                    }
                });

                return false;
            });
            
            

        var Technical = '';
        var Accounts = '';
        var Marketing = '';
        $("#btnGenerateOTP").click(function () {
            //alert();
            $('#otpresponse').empty();
            $('#otpresponse').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            if (document.getElementById("tech").checked) {
                Technical = '1';
            }
            if (document.getElementById("acc").checked) {
                Accounts = '24';
            }
            if (document.getElementById("markt").checked) {
                Marketing = '4';
            }
            //alert(Accounts);
            if ((Technical == "" && Accounts == "") || (Technical == "" && Marketing == "") || (Accounts == "" && Marketing == ""))
            {
                $('#otpresponse').empty();
                $('#otpresponse').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Select at least TWO Authorities.</span></p>");
                return false;
            } else {
                var url = "common/cfOwnershipChangeFinalApproval.php"; // the script where you handle the form input.            
                var data;
                data = "action=GenerateOTP&Tech=" + Technical + "&Acc=" + Accounts + "&Markt=" + Marketing + "&centercode=" + CenterCode + ""; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        var result = data.trim();
                        if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
                        {
                            $('#otpresponse').empty();
                            $('#otpresponse').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>OTP has been sent to Approving Authorities. Please Enter OTP to Proceed</span></p>");
                            $('#tech').attr('disabled', true);
                            $('#acc').attr('disabled', true);
                            $('#markt').attr('disabled', true);
                            $("#otpverify").show(3000);
                            if (document.getElementById("tech").checked) {
                                jQuery("label[for='otpTech']").html("Enter OTP Delivered to Mr. Naresh Kumawat:");
                                $("#divotptech").show();
                            }
                            if (document.getElementById("acc").checked) {
                                jQuery("label[for='otpAcc']").html("Enter OTP Delivered to Mr. Dinesh Khandelwal:");
                                $("#divotpacc").show();
                            }
                            if (document.getElementById("markt").checked) {
                                jQuery("label[for='otpMarkt']").html("Enter OTP Delivered to Mr. Abhay Shankar:");
                                $("#divotpmarkt").show();
                            }

                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Invalid Details. Please Try again</span></p>");
                        }
                        //showData();


                    }
                });
            }
        });

        $("#btnVerify").click(function () {

            $('#verifyresponse').empty();
            $('#verifyresponse').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            if (document.getElementById("tech").checked) {
                if (document.getElementById("txtOTPTech").value == '') {
                    $('#verifyresponse').empty();
                    $('#verifyresponse').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Enter OTP Delivered to Mr. Naresh Kumawat</span></p>");
                }
            }
            if (document.getElementById("acc").checked) {
                if (document.getElementById("txtOTPAcc").value == '') {
                    $('#verifyresponse').empty();
                    $('#verifyresponse').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Enter OTP Delivered to Mr. Dinesh Khandelwal</span></p>");
                }
            }
            if (document.getElementById("markt").checked) {
                if (document.getElementById("txtOTPMarkt").value == '') {
                    $('#verifyresponse').empty();
                    $('#verifyresponse').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Enter OTP Delivered to Mr. Abhay Shankar</span></p>");
                }
            }
            $.ajax({
                type: "post",
                url: "common/cfOwnershipChangeFinalApproval.php",
                data: "action=VERIFY&otptech=" + txtOTPTech.value + "&otpacc=" + txtOTPAcc.value + "&otpmrtk=" + txtOTPMarkt.value + "",
                success: function (data)
                {
                    //alert(data);
                    var result = data.trim();
                    if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
                    {
                        $('#verifyresponse').empty();
                        $("#btnVerify").hide();
                        $("#lblbtnverify").hide();
                        $("#btnVerified").show();
                        $('#txtOTPTech').attr('readonly', true);
                        $('#txtOTPAcc').attr('readonly', true);
                        $('#txtOTPMarkt').attr('readonly', true);
                        $('#finalsubmission').show(3000);
                    } else
                    {
                        $('#verifyresponse').empty();
                        $('#verifyresponse').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></div>");
                    }
                }
            });
        });

        $("#btnUser").click(function () {
            $('#btnUser').prop('disabled', true);
            //alert("hi");
            $('#finalresponse').empty();
            $('#finalresponse').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfOwnershipChangeFinalApproval.php"; // the script where you handle the form input.            
            var data;
            if (Mode == 'Add')
            {

            } else
            {
                data = "action=UpdateUserMaster&Code=" + Code + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    //alert(data);
                    var result = data.trim();
                    if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
                    {
                        $('#finalresponse').empty();
                        $('#finalresponse').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>New-Owner Contact Details Updated Successfully. Please update Organization Details.</span></p>");
                        //BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; New-Owner Contact Details Updated Successfully. Please update Organization Details.</span>");
                        $('#btnUser').hide();
                        $('#lbluserupdated').show();
                        $('#divstep2').show(3000);
                    } else
                    {
                        $('#finalresponse').empty();
                        $('#finalresponse').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></p>");
                    }
                    //showData();


                }
            });

            return false;
        });

        $("#btnOrg").click(function () {
            $('#btnOrg').prop('disabled', true);
            //alert("hi");
            $('#finalresponse').empty();
            $('#finalresponse').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfOwnershipChangeFinalApproval.php"; // the script where you handle the form input.            
            var data;
            if (Mode == 'Add')
            {

            } else
            {
                data = "action=UpdateOrgDetail&Code=" + Code + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    //alert(data);
                    var result = data.trim();
                    if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
                    {
                        $('#finalresponse').empty();
                        $('#finalresponse').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>New-Organization Details Updated Successfully. Please update Bank Account Details.</span></p>");
                        //BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; New-Organization Details Updated Successfully. Please update Bank Account Details.</span>");
                        $('#btnOrg').hide();
                        $('#lblorgupdated').show();
                        $('#divstep3').show(3000);
                    } else
                    {
                        $('#finalresponse').empty();
                        $('#finalresponse').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></p>");
                    }
                    //showData();


                }
            });

            return false;
        });

        $("#btnBank").click(function () {
            $('#btnBank').prop('disabled', true);
            //alert("hi");
            $('#finalresponse').empty();
            $('#finalresponse').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfOwnershipChangeFinalApproval.php"; // the script where you handle the form input.            
            var data;
            if (Mode == 'Add')
            {

            } else
            {
                data = "action=UpdateBankAcc&Code=" + Code + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    //alert(data);
                    var result = data.trim();
                    if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
                    {
                        $('#finalresponse').empty();
                        $('#finalresponse').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>New Bank Account Details Details Updated Successfully. Please update Agreement Details to complete the process.</span></p>");
                        //BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; ONew Bank Account Details Details Updated Successfully. Please update Agreement Details to complete the process..</span>");
                        $('#btnBank').hide();
                        $('#lblbankupdated').show();
                        $('#divstep4').show(3000);
                    } else
                    {
                        $('#finalresponse').empty();
                        $('#finalresponse').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></p>");
                    }
                    //showData();


                }
            });

            return false;
        });

        $("#btnAgreement").click(function () {
            $('#btnAgreement').prop('disabled', true);
            //alert("hi");
            $('#finalresponse').empty();
            $('#finalresponse').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfOwnershipChangeFinalApproval.php"; // the script where you handle the form input.            
            var data;
            if (Mode == 'Add')
            {

            } else
            {
                data = "action=UpdateAgreement&Code=" + Code + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    //alert(data);
                    var result = data.trim();
                    if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
                    {
                        $('#finalresponse').empty();
                        $('#finalresponse').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Ownership Change Request Approved Successfully</span></p>");
                        //BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Ownership Change Request Approved Successfully.</span>");
                        $('#btnAgreement').hide();
                        $('#lblagreementupdated').show();
                        window.setTimeout(function () {
                            window.location.href = "frmownershipchangefinalapproval.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("form");
                    } else
                    {
                        $('#finalresponse').empty();
                        $('#finalresponse').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });

            return false;
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmcorrectionprocess_validation.js"></script>
</html>