<?php
$title="Buy Red Hat Learning Subscription(RHLS)";
include ('header.php'); 
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
require("razorpay/checkout/manual.php");

?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:auto !important;">	
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Buy Red Hat Learning Subscription(RHLS)</div>
                <div class="panel-body">
				 <form name="frmredhatexpcenterpayment" id="frmredhatexpcenterpayment" class="form-inline" role="form" enctype="multipart/form-data">
										
					<div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>
							
					<div id="errorBox"></div>
					
					<div id="valid" style="display:none;">	
						<div class="col-sm-4 form-group">     
							<label for="correction">Select Authorization Type:</label>
								<select id="ddltype" name="ddltype" class="form-control">										
								</select>
						</div> 
						
								<div id="admissionid" style="display:none;">
									 <div class="col-sm-4 form-group">     
										<label for="learnercode">Learner Name:<span class="star">*</span></label>
										<input type="text" class="form-control" maxlength="50" name="txtlname" id="txtlname" style="text-transform:uppercase" onkeypress="javascript:return validLearnerName(event);" placeholder="Learner Name">
										<input type="hidden" class="form-control"  name="txtLearnercode" id="txtLearnercode"/>
									</div>

									<div class="col-sm-4 form-group"> 
										<label for="fname">Father/Husband Name:<span class="star">*</span></label>
										<input type="text" class="form-control" style="text-transform:uppercase" maxlength="50" name="txtfname" id="txtfname" onkeypress="javascript:return validFatherName(event);" placeholder="Father Name">     
									</div>

									<div class="col-sm-4 form-group">     
										<label for="dob">Date of Birth:<span class="star">*</span></label>								
										<input type="text" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="YYYY-MM-DD">
									</div>
								</div>
							
						<div class="col-sm-4 form-group">     
							<label for="batch"> Select Payment Mode:</label>
							<select id="paymode" name="paymode" class="form-control" onchange="toggle_visibility1('online_mode');">

							</select>									
						</div> 
					
						<div class="col-sm-4 form-group">     
							<label for="batch"> Select Payment Gateway:</label>
							<select id="gateway" name="gateway" class="form-control">									   
								<option value="razorpay">Razorpay</option>
								<option value="payu">Payu</option>								
							</select>
						</div>
					</div>					
							<input type="hidden" name="amounts" id="amounts" class="form-control"/>  
							
					</div>
					
                    <div id="gird" style="margin-top:10px;"> </div>                   
                 
					<div class="container" id="submit" style="display:none;">
						<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
					</div>
				</div>
            </div>   
        </div>
    </form>
</div>
   
	<form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
        <input type="hidden" id="txnid" name="txnid">
    </form>

  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
<script type="text/javascript">
                            $('#dob').datepicker({
                                format: "yyyy-mm-dd",
                                orientation: "bottom auto",
                                todayHighlight: true
                            });
</script>
<script type="text/javascript">
function validFatherName(e){
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("^[ A-Za-z]*$");

            if (key === 8 || key === 0 || key === 32) {
                keychar = "a";
            }
            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
function validLearnerName(e){
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("^[ A-Za-z]*$");

            if (key === 8 || key === 0 || key === 32) {
                keychar = "a";
            }
            return reg.test(keychar);
        }
</script>
		
<script type="text/javascript">
        $(document).ready(function () {
			
			function checkcenterauthorization() {
				$('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
               
                $.ajax({
                    type: "post",
                    url: "common/cfRedhatExperienceCenterFee.php",
                    data: "action=checkauthorization",
                    success: function (data) {
						$('#response').empty();
                        if(data=='1'){
							$("#valid").show();
							gettype();
						}
						 else{
							$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " You are not authorized center for Red hat Courses." + "</span></p>");
						 }
                    }
                });
            }
            checkcenterauthorization();
			
			function GenerateLearnercode()
        {
            $.ajax({
                type: "post",
                url: "common/cfRedhatExperienceCenterFee.php",
                data: "action=GENERATELEARNERCODE",
                success: function (data) {
					//alert(data);
                    txtLearnercode.value = data;
                }
            });
        }
        GenerateLearnercode();
			
			function gettype() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfRedhatExperienceCenterFee.php",
                    data: "action=GetAuthorizeType",
                    success: function (data) {
						$("#ddltype").html(data);
                    }
                });
            }
			
			$("#ddltype").change(function(){				
				var DocCode = $('#ddltype').val();
					if(DocCode=='2'){
						$("#admissionid").show();
					}
					else{
						$("#admissionid").hide();
					}
                showpaymentmode(DocCode);
                $("#gird").html('');
                $("#submit").hide();
                $("#paymode").html('');
                $.ajax({					
                    type: "post",
                    url: "common/cfRedhatExperienceCenterFee.php",
                    data: "action=Fee",
                    success: function (data) {						
						amounts.value = data;				
						//$("#amounts").html(data);
                    }
                });
			});			
			
			function showpaymentmode(val) {				
                $("#gird").html('');
                $("#submit").hide();
                $("#paymode").html('');
				$.ajax({
					type: "post",
					url: "common/cfRedhatExperienceCenterFee.php",
					data: "action=ShowPayMode",
					success: function (data) {
						if(data=='0'){
							$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Event is not enabled." + "</span></p>");
						}
						else{
							$("#paymode").html(data);
						}
						
					}
				});
			}
		
    		$("#paymode").change(function () {
				var mode = $('#paymode').val();
					if(mode==''){
						$("#submit").hide();
					}
					else{
						$("#submit").show();
					}    			
    		});

            
			$("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfRedhatExperienceCenterFee.php"; // the script where you handle the form input.
                var data;
                var forminput=$("#frmredhatexpcenterpayment").serialize();
                //alert(forminput);
				
                if (Mode == 'Add') {
                    data = "action=ADD&" + forminput;
                    $('#txnid').val('');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data) {	
                            $('#response').empty();							
                            if (data == 0 || data == '') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Please try again, also ensure that, you have selected atleast one checkbox." + "</span></p>");
                            } else if (data != '') {
                               if (data == 'TimeCapErr') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                    alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                            } 
							else if(data == 'done'){
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Payment Already Done." + "</span></p>");
							}
							
							else if(data == 'duplicate'){
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "  Something went wrong." + "</span></p>");
							}
							else if(data == 'empty'){
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "  Please fill all details." + "</span></p>");
							}
								else {
									if ($('#gateway').val() == 'razorpay') {
										var options = $.parseJSON(data);
										<?php include("razorpay/razorpay.js"); ?>
									}
									else {
										$('#txnid').val(data);
										$('#frmpostvalue').submit();
									}									
								}
                            }
                        }
                    });
                }

                return false; // avoid to execute the actual submit of the form.
            });

        });

    </script>
</html>