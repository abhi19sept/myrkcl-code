<?php
$title = "ReExam DD Payment Confirm";
include ('header.php');
include ('root_menu.php');
?>
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">DD Payment Confirm</div>
        <div class="panel-body">
            <form name="frmddpaymentconfirm" id="frmddpaymentconfirm" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>


                    <div class="col-sm-4 form-group">     
                        <label for="course">Select Course:</label>
                        <select id="ddlCourse" name="ddlCourse" class="form-control">

                        </select>
                        <input type="hidden" name="coursename" id="coursename"/>
                    </div> 
                </div>

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Exam Event:</label>
                        <select id="ddlEvent" name="ddlEvent" class="form-control">

                        </select>
                        <input type="hidden" name="eventname" id="eventname"/>

                 
                    </div> 
                </div>

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Center Code:</label>
                        <select id="ddlcentercode" name="ddlcentercode" class="form-control">

                        </select>											
                    </div> 
                </div>

                <div id="menuList" name="menuList" style="margin-top:35px;"> </div> 

                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                </div>
        </div>
    </div>   
</div>
</form>


</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLAdmissionSummaryCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();


        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfReexamPayment.php",
                data: "action=FILLEXAMEVENTDDConfirm&values=" + ddlCourse.value + "",
                success: function (data) {
                     $("#ddlEvent").html(data);
                    //document.getElementById('coursename').innerHTML = data;

                }
            });

        });
  $("#ddlEvent").change(function () {
                     
                showCenterCode(this.value, ddlCourse.value);
                showeventname(ddlEvent.value);
            });
            
            
        function showCenterCode(val, val1) {
            $.ajax({
                type: "post",
                url: "common/cfReexamPayment.php",
                data: "action=GETCENTERREEXAM&examevent=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlcentercode").html(data);
                }
            });
        }
         function showeventname(val) {
            $.ajax({
                type: "post",
                url: "common/cfReexamPayment.php",
                data: "action=GETEXAMEVENTNAME&examevent=" + val + "",
                success: function (data) {
                    //alert(data);
                    eventname.value = data;	
                }
            });
        }

        function showDdModeData(val) {
            $.ajax({
                type: "post",
                url: "common/cfReexamPayment.php",
                data: "action=GETDDMODEDATAREEXAM&refno=" + val + "",
                success: function (data) {
                    //alert(data);
                    $("#menuList").html(data);
                }
            });
        }


        $("#ddlcentercode").change(function () {
            showDdModeData(ddlcentercode.value);
        });


        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfReexamPayment.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmddpaymentconfirm").serialize();

            data = "action=ADDddconfirm&" + forminput; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmreexamddconfirm.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmddpaymentconfirm");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>