<?php
$title = "Learner Correction History Dashboard";
include ('header.php');
include ('root_menu.php');


if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258'
|| $_SESSION['User_Code'] == '6588' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8') {
//print_r($_SESSION);
?>
<div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">Learner Correction History Dashboard</div>
        <div class="panel-body">
            <form name="frmcorrectionapproved" id="frmcorrectionapproved" class="form-inline" role="form"
					enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>			
                    
					<div class="col-sm-4 form-group">     
                        <label for="batch"> LearnerCode:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="18" name="txtLcode" id="txtLcode"
							placeholder="LearnerCode" onkeypress="javascript:return allownumbers(event);">						
                    </div>
					
					<div class="col-sm-4 form-group">                                  
						<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" 
							value="Show Details" style="margin-top:25px"/>    
					</div>
					
                </div>              
				
				<div id="menuList" name="menuList" style="margin-top:35px;"> </div> 
        </div>
    </div>   
</div>
</form>
</div>
</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function ShowLearnerHistory(lcode) {
		 if ($("#frmcorrectionapproved").valid())
           {	
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
					type: "post",
					url: "common/cfCorrectionLearnerHistory.php",
					data: "action=ShowDetails&lcode=" + lcode + "",
					success: function (data) {
						$('#response').empty();
						$("#menuList").html(data);
						$('#example').DataTable();
						//$("#btnSubmit").hide();
						$('#txtLcode').attr('readonly', true);
					}
				});
			}
            return false;
        }

		
        $("#btnSubmit").click(function () {
			var lcode = $('#txtLcode').val();			
				ShowLearnerHistory(lcode);			   
        });
        
    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcorrectionapproval_validation.js"></script>
		
</body>

</html>

<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>