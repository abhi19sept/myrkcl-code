-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: rkcl
-- ------------------------------------------------------
-- Server version	5.5.44

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_affiliate_master`
--

DROP TABLE IF EXISTS `tbl_affiliate_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_affiliate_master` (
  `Affiliate_Code` int(11) DEFAULT NULL,
  `Affiliate_Name` varchar(50) DEFAULT NULL,
  `Affiliate_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_affiliate_master`
--

LOCK TABLES `tbl_affiliate_master` WRITE;
/*!40000 ALTER TABLE `tbl_affiliate_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_affiliate_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_area_master`
--

DROP TABLE IF EXISTS `tbl_area_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_area_master` (
  `Area_Code` int(11) NOT NULL,
  `Area_Name` varchar(500) NOT NULL,
  `Area_PinCode` varchar(6) NOT NULL,
  `Area_Tehsil` int(11) NOT NULL,
  `Area_Status` int(11) NOT NULL,
  PRIMARY KEY (`Area_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_area_master`
--

LOCK TABLES `tbl_area_master` WRITE;
/*!40000 ALTER TABLE `tbl_area_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_area_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_batch_master`
--

DROP TABLE IF EXISTS `tbl_batch_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_batch_master` (
  `Batch_Code` int(11) NOT NULL,
  `Batch_Name` varchar(100) NOT NULL,
  `Batch_StartDate` datetime NOT NULL,
  `Batch_EndDate` datetime NOT NULL,
  `Batch_Status` datetime NOT NULL,
  PRIMARY KEY (`Batch_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_batch_master`
--

LOCK TABLES `tbl_batch_master` WRITE;
/*!40000 ALTER TABLE `tbl_batch_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_batch_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_classtype_master`
--

DROP TABLE IF EXISTS `tbl_classtype_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_classtype_master` (
  `ClassType_Code` int(11) DEFAULT NULL,
  `ClassType_Name` varchar(50) DEFAULT NULL,
  `ClassType_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_classtype_master`
--

LOCK TABLES `tbl_classtype_master` WRITE;
/*!40000 ALTER TABLE `tbl_classtype_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_classtype_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_country_master`
--

DROP TABLE IF EXISTS `tbl_country_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_country_master` (
  `Country_Code` int(11) NOT NULL,
  `Country_Name` varchar(500) NOT NULL,
  `Country_Status` int(11) NOT NULL,
  PRIMARY KEY (`Country_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_country_master`
--

LOCK TABLES `tbl_country_master` WRITE;
/*!40000 ALTER TABLE `tbl_country_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_country_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_course_master`
--

DROP TABLE IF EXISTS `tbl_course_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_course_master` (
  `Course_Code` int(11) NOT NULL,
  `Course_Name` varchar(100) NOT NULL,
  `Course_Type` int(11) NOT NULL,
  `Course_Duration` int(11) NOT NULL,
  `Course_DurationType` int(11) NOT NULL,
  `Course_Medium` int(11) NOT NULL,
  `Course_FeeType` int(11) NOT NULL,
  `Course_Fee` decimal(10,0) NOT NULL,
  `Course_ExaminationType` int(11) NOT NULL,
  `Course_ClassType` int(11) NOT NULL,
  `Course_Affiliate` int(11) NOT NULL,
  `Course_Status` int(11) NOT NULL,
  PRIMARY KEY (`Course_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_course_master`
--

LOCK TABLES `tbl_course_master` WRITE;
/*!40000 ALTER TABLE `tbl_course_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_course_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_coursetype_master`
--

DROP TABLE IF EXISTS `tbl_coursetype_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_coursetype_master` (
  `CourseType_Code` int(11) DEFAULT NULL,
  `CourseType_Name` varchar(50) DEFAULT NULL,
  `CourseType_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_coursetype_master`
--

LOCK TABLES `tbl_coursetype_master` WRITE;
/*!40000 ALTER TABLE `tbl_coursetype_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_coursetype_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_district_master`
--

DROP TABLE IF EXISTS `tbl_district_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_district_master` (
  `District_Code` int(11) NOT NULL,
  `District_Name` varchar(500) NOT NULL,
  `District_State` int(11) NOT NULL,
  `District_Status` int(11) NOT NULL,
  PRIMARY KEY (`District_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_district_master`
--

LOCK TABLES `tbl_district_master` WRITE;
/*!40000 ALTER TABLE `tbl_district_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_district_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_errorcode_master`
--

DROP TABLE IF EXISTS `tbl_errorcode_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_errorcode_master` (
  `Error_Code` int(11) NOT NULL,
  `Error_Name` varchar(100) NOT NULL,
  `Error_Description` varchar(1000) NOT NULL,
  PRIMARY KEY (`Error_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_errorcode_master`
--

LOCK TABLES `tbl_errorcode_master` WRITE;
/*!40000 ALTER TABLE `tbl_errorcode_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_errorcode_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_examinationtype_master`
--

DROP TABLE IF EXISTS `tbl_examinationtype_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_examinationtype_master` (
  `ExaminationType_Code` int(11) DEFAULT NULL,
  `ExaminationType_Name` varchar(50) DEFAULT NULL,
  `ExaminationType_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_examinationtype_master`
--

LOCK TABLES `tbl_examinationtype_master` WRITE;
/*!40000 ALTER TABLE `tbl_examinationtype_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_examinationtype_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_feetype_master`
--

DROP TABLE IF EXISTS `tbl_feetype_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_feetype_master` (
  `FeeType_Code` int(11) DEFAULT NULL,
  `FeeType_Name` varchar(50) DEFAULT NULL,
  `FeeType_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_feetype_master`
--

LOCK TABLES `tbl_feetype_master` WRITE;
/*!40000 ALTER TABLE `tbl_feetype_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_feetype_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_function_master`
--

DROP TABLE IF EXISTS `tbl_function_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_function_master` (
  `Function_Code` int(11) NOT NULL,
  `Function_Name` varchar(500) NOT NULL,
  `Function_URL` varchar(500) NOT NULL,
  `Function_Status` int(11) NOT NULL,
  PRIMARY KEY (`Function_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_function_master`
--

LOCK TABLES `tbl_function_master` WRITE;
/*!40000 ALTER TABLE `tbl_function_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_function_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mudium_master`
--

DROP TABLE IF EXISTS `tbl_mudium_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mudium_master` (
  `Medium_Code` int(11) DEFAULT NULL,
  `Medium_Name` varchar(50) DEFAULT NULL,
  `Medium_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mudium_master`
--

LOCK TABLES `tbl_mudium_master` WRITE;
/*!40000 ALTER TABLE `tbl_mudium_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_mudium_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ogrnization`
--

DROP TABLE IF EXISTS `tbl_ogrnization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ogrnization` (
  `Orgnization_Code` int(11) NOT NULL,
  `Orgnization_User` int(11) NOT NULL,
  `Orgnization_Name` varchar(500) NOT NULL,
  `Orgnization_RegistrationNo` varchar(50) NOT NULL,
  `Orgnization_FoundedDate` date NOT NULL,
  `Orgnization_Course` varchar(500) NOT NULL,
  `Orgnization_Type` int(11) NOT NULL,
  `Orgnization_DocType` int(11) NOT NULL,
  `Orgnization_ScanDoc` varchar(1000) NOT NULL,
  PRIMARY KEY (`Orgnization_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ogrnization`
--

LOCK TABLES `tbl_ogrnization` WRITE;
/*!40000 ALTER TABLE `tbl_ogrnization` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ogrnization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_permission_master`
--

DROP TABLE IF EXISTS `tbl_permission_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_permission_master` (
  `Permission_Code` int(11) NOT NULL,
  `Permission_UserRoll` int(11) NOT NULL,
  `Permission_Function` int(11) NOT NULL,
  `Permission_Attribute` int(11) NOT NULL,
  `Permission_Status` int(11) NOT NULL,
  PRIMARY KEY (`Permission_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_permission_master`
--

LOCK TABLES `tbl_permission_master` WRITE;
/*!40000 ALTER TABLE `tbl_permission_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_permission_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pin_master`
--

DROP TABLE IF EXISTS `tbl_pin_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pin_master` (
  `Pin_Code` int(10) NOT NULL,
  `Pin_Number` varchar(50) NOT NULL,
  `Pin_Status` int(10) NOT NULL,
  `Pin_IssuedTo` int(10) NOT NULL,
  `Pin_RecTmStamp` datetime NOT NULL,
  PRIMARY KEY (`Pin_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pin_master`
--

LOCK TABLES `tbl_pin_master` WRITE;
/*!40000 ALTER TABLE `tbl_pin_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pin_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_state_master`
--

DROP TABLE IF EXISTS `tbl_state_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_state_master` (
  `State_Code` int(11) NOT NULL,
  `State_Name` varchar(500) NOT NULL,
  `State_Country` int(11) NOT NULL,
  `State_Status` int(11) NOT NULL,
  PRIMARY KEY (`State_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_state_master`
--

LOCK TABLES `tbl_state_master` WRITE;
/*!40000 ALTER TABLE `tbl_state_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_state_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_status_master`
--

DROP TABLE IF EXISTS `tbl_status_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_status_master` (
  `Status_Code` int(11) NOT NULL,
  `Status_Name` varchar(500) NOT NULL,
  `Status_Description` varchar(500) NOT NULL,
  PRIMARY KEY (`Status_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_status_master`
--

LOCK TABLES `tbl_status_master` WRITE;
/*!40000 ALTER TABLE `tbl_status_master` DISABLE KEYS */;
INSERT INTO `tbl_status_master` VALUES (1,'ACTIVE','ACTIVE'),(2,'DEACTIVE','DEACTIVE');
/*!40000 ALTER TABLE `tbl_status_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tehsil_master`
--

DROP TABLE IF EXISTS `tbl_tehsil_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tehsil_master` (
  `Tehsil_Code` int(11) NOT NULL,
  `Tehsil_Name` varchar(500) NOT NULL,
  `Tehsil_District` int(11) NOT NULL,
  `Tehsil_Status` int(11) NOT NULL,
  PRIMARY KEY (`Tehsil_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tehsil_master`
--

LOCK TABLES `tbl_tehsil_master` WRITE;
/*!40000 ALTER TABLE `tbl_tehsil_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tehsil_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user_master`
--

DROP TABLE IF EXISTS `tbl_user_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user_master` (
  `User_Code` int(11) NOT NULL,
  `User_EmailId` varchar(500) NOT NULL,
  `User_MobileNo` varchar(20) NOT NULL,
  `User_LoginId` varchar(20) NOT NULL,
  `User_Password` varchar(20) NOT NULL,
  `User_UserRoll` int(11) NOT NULL,
  `User_ParentId` int(11) NOT NULL,
  `User_Status` int(11) NOT NULL,
  PRIMARY KEY (`User_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user_master`
--

LOCK TABLES `tbl_user_master` WRITE;
/*!40000 ALTER TABLE `tbl_user_master` DISABLE KEYS */;
INSERT INTO `tbl_user_master` VALUES (1,'admin@rkcl.in','00000000000','superadmin','su@123',1,0,1);
/*!40000 ALTER TABLE `tbl_user_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_userprofile`
--

DROP TABLE IF EXISTS `tbl_userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_userprofile` (
  `UserProfile_Code` int(11) NOT NULL,
  `UserProfile_User` int(11) NOT NULL,
  `UserProfile_Initial` int(11) NOT NULL,
  `UserProfile_FirstName` varchar(500) NOT NULL,
  `UserProfile_MiddleName` varchar(500) NOT NULL,
  `UserProfile_LastName` varchar(500) NOT NULL,
  `UserProfile_HouseNo` varchar(500) NOT NULL,
  `UserProfile_Street` varchar(500) NOT NULL,
  `UserProfile_Road` varchar(500) NOT NULL,
  `UserProfile_Landmark` varchar(500) NOT NULL,
  `UserProfile_Area` int(11) NOT NULL,
  `UserProfile_Image` varchar(500) NOT NULL,
  `UserProfile_Gender` int(11) NOT NULL,
  `UserProfile_TagName` int(11) NOT NULL,
  `UserProfile_ParentName` varchar(500) NOT NULL,
  `UserProfile_Occupation` varchar(100) NOT NULL,
  `UserProfile_PanNo` varchar(10) NOT NULL,
  `UserProfile_AadharNo` varchar(20) NOT NULL,
  `UserProfile_STDCode` varchar(10) NOT NULL,
  `UserProfile_PhoneNo` varchar(10) NOT NULL,
  `UserProfile_Website` varchar(200) NOT NULL,
  `UserProfile_FaceBookId` varchar(20) NOT NULL,
  `UserProfile_TwitterId` varchar(20) NOT NULL,
  `UserProfile_KycScan` varchar(500) NOT NULL,
  PRIMARY KEY (`UserProfile_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_userprofile`
--

LOCK TABLES `tbl_userprofile` WRITE;
/*!40000 ALTER TABLE `tbl_userprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_userprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_userroll_master`
--

DROP TABLE IF EXISTS `tbl_userroll_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_userroll_master` (
  `UserRoll_Code` int(11) NOT NULL,
  `UserRoll_Name` varchar(500) NOT NULL,
  `UserRoll_Status` int(11) NOT NULL,
  PRIMARY KEY (`UserRoll_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_userroll_master`
--

LOCK TABLES `tbl_userroll_master` WRITE;
/*!40000 ALTER TABLE `tbl_userroll_master` DISABLE KEYS */;
INSERT INTO `tbl_userroll_master` VALUES (1,'Super Administrator',1),(2,'Administrator',1),(3,'Developer',1),(4,'RM',1),(5,'PSA',1),(6,'DLC',1),(7,'ITGK',1),(8,'Learner',1),(9,'Examination Body',1),(10,'Certification Boday',2),(11,'Account Deparrtment',1);
/*!40000 ALTER TABLE `tbl_userroll_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_veryfy`
--

DROP TABLE IF EXISTS `tbl_veryfy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_veryfy` (
  `Veryfy_Code` int(11) NOT NULL AUTO_INCREMENT,
  `Veryfy_User` int(11) DEFAULT NULL,
  `Veryfy_Pin` varchar(50) DEFAULT NULL,
  `Veryfy_Status` int(11) DEFAULT NULL,
  `Veryfy_SentDate` date DEFAULT NULL,
  `Veryfy_Date` date DEFAULT NULL,
  PRIMARY KEY (`Veryfy_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_veryfy`
--

LOCK TABLES `tbl_veryfy` WRITE;
/*!40000 ALTER TABLE `tbl_veryfy` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_veryfy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-30 17:54:03
