-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: rkcl
-- ------------------------------------------------------
-- Server version	5.5.44

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_admisssion`
--

DROP TABLE IF EXISTS `tbl_admisssion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_admisssion` (
  `Admission_Code` int(11) NOT NULL,
  `Admission_User` int(11) NOT NULL,
  `Admission_Learner_Initial` varchar(10) NOT NULL,
  `Admission_Learner_Name` varchar(100) NOT NULL,
  `Admission_Father_Initial` varchar(10) NOT NULL,
  `Admission_Father_Name` varchar(100) NOT NULL,
  `Admission_Id_Proof` varchar(100) NOT NULL,
  `Admission_Id_Number` varchar(11) NOT NULL,
  `Admission_Learner_Age` int(5) NOT NULL,
  `Admission_Learner_DOB` date NOT NULL,
  `Admission_Mother_Tongue` varchar(20) NOT NULL,
  `Admission_Course` varchar(50) NOT NULL,
  `Admission_Batch` varchar(50) NOT NULL,
  `Admission_Learner_Gender` varchar(50) NOT NULL,
  `Admission_Marital_Status` varchar(50) NOT NULL,
  `Admission_Medium` varchar(50) NOT NULL,
  `Admission_Payment_Mode` varchar(100) NOT NULL,
  `Admission_Learner_Address` varchar(500) NOT NULL,
  `Admission_Learner_Suburb` varchar(50) NOT NULL,
  `Admission_Learner_City` varchar(50) NOT NULL,
  `Admission_Learner_Pin` int(10) NOT NULL,
  `Admission_Learner_State` varchar(50) NOT NULL,
  `Admission_Learner_District` varchar(50) NOT NULL,
  `Admission_Learner_Taluka` varchar(50) NOT NULL,
  `Admission_Learner_Phone` int(11) NOT NULL,
  `Admission_Learner_Mobile1` int(11) NOT NULL,
  `Admission_Learner_Mobile2` int(11) NOT NULL,
  `Admission_Learner_Email` varchar(100) NOT NULL,
  `Admission_Learner_Qualification` varchar(100) NOT NULL,
  `Admission_Learner_Type` varchar(100) NOT NULL,
  `Admission_Learner_Physical` varchar(50) NOT NULL,
  `Admission_Learner_Photo` varchar(100) NOT NULL,
  `Admission_Learner_Sign` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_admisssion`
--

LOCK TABLES `tbl_admisssion` WRITE;
/*!40000 ALTER TABLE `tbl_admisssion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_admisssion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_affiliate_master`
--

DROP TABLE IF EXISTS `tbl_affiliate_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_affiliate_master` (
  `Affiliate_Code` int(11) DEFAULT NULL,
  `Affiliate_Name` varchar(50) DEFAULT NULL,
  `Affiliate_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_affiliate_master`
--

LOCK TABLES `tbl_affiliate_master` WRITE;
/*!40000 ALTER TABLE `tbl_affiliate_master` DISABLE KEYS */;
INSERT INTO `tbl_affiliate_master` VALUES (1,'RKCL',1),(2,'VMOU',1),(3,'DU ORG',2),(4,'DU',1);
/*!40000 ALTER TABLE `tbl_affiliate_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_area_master`
--

DROP TABLE IF EXISTS `tbl_area_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_area_master` (
  `Area_Code` int(11) NOT NULL,
  `Area_Name` varchar(500) NOT NULL,
  `Area_PinCode` varchar(6) NOT NULL,
  `Area_Tehsil` int(11) NOT NULL,
  `Area_Status` int(11) NOT NULL,
  PRIMARY KEY (`Area_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_area_master`
--

LOCK TABLES `tbl_area_master` WRITE;
/*!40000 ALTER TABLE `tbl_area_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_area_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_bank_account`
--

DROP TABLE IF EXISTS `tbl_bank_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_bank_account` (
  `Bank_Account_Code` int(11) NOT NULL,
  `Bank_User` int(11) NOT NULL,
  `Bank_Payment_type` varchar(50) NOT NULL,
  `Bank_Account_Name` varchar(500) NOT NULL,
  `Bank_Account_Number` int(11) NOT NULL,
  `Bank_Account_Type` varchar(50) NOT NULL,
  `Bank_Ifsc_code` varchar(50) NOT NULL,
  `Bank_Name` varchar(500) NOT NULL,
  `Bank_Branch_Name` varchar(500) NOT NULL,
  `Bank_Document` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_bank_account`
--

LOCK TABLES `tbl_bank_account` WRITE;
/*!40000 ALTER TABLE `tbl_bank_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_bank_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_batch_master`
--

DROP TABLE IF EXISTS `tbl_batch_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_batch_master` (
  `Batch_Code` int(11) NOT NULL,
  `Batch_Name` varchar(100) NOT NULL,
  `Batch_StartDate` datetime NOT NULL,
  `Batch_EndDate` datetime NOT NULL,
  `Batch_Status` datetime NOT NULL,
  PRIMARY KEY (`Batch_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_batch_master`
--

LOCK TABLES `tbl_batch_master` WRITE;
/*!40000 ALTER TABLE `tbl_batch_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_batch_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_classtype_master`
--

DROP TABLE IF EXISTS `tbl_classtype_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_classtype_master` (
  `ClassType_Code` int(11) DEFAULT NULL,
  `ClassType_Name` varchar(50) DEFAULT NULL,
  `ClassType_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_classtype_master`
--

LOCK TABLES `tbl_classtype_master` WRITE;
/*!40000 ALTER TABLE `tbl_classtype_master` DISABLE KEYS */;
INSERT INTO `tbl_classtype_master` VALUES (1,'ADMISSION',1),(2,'FINANCE',1);
/*!40000 ALTER TABLE `tbl_classtype_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_country_master`
--

DROP TABLE IF EXISTS `tbl_country_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_country_master` (
  `Country_Code` int(11) NOT NULL,
  `Country_Name` varchar(500) NOT NULL,
  `Country_Status` int(11) NOT NULL,
  PRIMARY KEY (`Country_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_country_master`
--

LOCK TABLES `tbl_country_master` WRITE;
/*!40000 ALTER TABLE `tbl_country_master` DISABLE KEYS */;
INSERT INTO `tbl_country_master` VALUES (1,'India',1),(2,'America',2);
/*!40000 ALTER TABLE `tbl_country_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_course_master`
--

DROP TABLE IF EXISTS `tbl_course_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_course_master` (
  `Course_Code` int(11) NOT NULL,
  `Course_Name` varchar(100) NOT NULL,
  `Course_Type` int(11) NOT NULL,
  `Course_Duration` int(11) NOT NULL,
  `Course_DurationType` int(11) NOT NULL,
  `Course_Medium` int(11) NOT NULL,
  `Course_FeeType` int(11) NOT NULL,
  `Course_Fee` decimal(10,0) NOT NULL,
  `Course_ExaminationType` int(11) NOT NULL,
  `Course_ClassType` int(11) NOT NULL,
  `Course_Affiliate` int(11) NOT NULL,
  `Course_Status` int(11) NOT NULL,
  PRIMARY KEY (`Course_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_course_master`
--

LOCK TABLES `tbl_course_master` WRITE;
/*!40000 ALTER TABLE `tbl_course_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_course_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_coursetype_master`
--

DROP TABLE IF EXISTS `tbl_coursetype_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_coursetype_master` (
  `CourseType_Code` int(11) DEFAULT NULL,
  `CourseType_Name` varchar(50) DEFAULT NULL,
  `CourseType_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_coursetype_master`
--

LOCK TABLES `tbl_coursetype_master` WRITE;
/*!40000 ALTER TABLE `tbl_coursetype_master` DISABLE KEYS */;
INSERT INTO `tbl_coursetype_master` VALUES (1,'',0),(2,'RS-CIT',0);
/*!40000 ALTER TABLE `tbl_coursetype_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_designation_master`
--

DROP TABLE IF EXISTS `tbl_designation_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_designation_master` (
  `Designation_Code` int(10) NOT NULL,
  `Designation_Name` varchar(50) NOT NULL,
  `Designation_Status` int(10) NOT NULL,
  PRIMARY KEY (`Designation_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_designation_master`
--

LOCK TABLES `tbl_designation_master` WRITE;
/*!40000 ALTER TABLE `tbl_designation_master` DISABLE KEYS */;
INSERT INTO `tbl_designation_master` VALUES (1,'Coordinat',2),(2,'Faculty',1),(3,'SuperAdmin',1),(4,'Admin',1);
/*!40000 ALTER TABLE `tbl_designation_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_district_master`
--

DROP TABLE IF EXISTS `tbl_district_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_district_master` (
  `District_Code` int(11) NOT NULL,
  `District_Name` varchar(500) NOT NULL,
  `District_Region` int(11) NOT NULL,
  `District_Status` int(11) NOT NULL,
  PRIMARY KEY (`District_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_district_master`
--

LOCK TABLES `tbl_district_master` WRITE;
/*!40000 ALTER TABLE `tbl_district_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_district_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_errorcode_master`
--

DROP TABLE IF EXISTS `tbl_errorcode_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_errorcode_master` (
  `Error_Code` int(11) NOT NULL,
  `Error_Name` varchar(100) NOT NULL,
  `Error_Description` varchar(1000) NOT NULL,
  PRIMARY KEY (`Error_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_errorcode_master`
--

LOCK TABLES `tbl_errorcode_master` WRITE;
/*!40000 ALTER TABLE `tbl_errorcode_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_errorcode_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_examinationtype_master`
--

DROP TABLE IF EXISTS `tbl_examinationtype_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_examinationtype_master` (
  `ExaminationType_Code` int(11) DEFAULT NULL,
  `ExaminationType_Name` varchar(50) DEFAULT NULL,
  `ExaminationType_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_examinationtype_master`
--

LOCK TABLES `tbl_examinationtype_master` WRITE;
/*!40000 ALTER TABLE `tbl_examinationtype_master` DISABLE KEYS */;
INSERT INTO `tbl_examinationtype_master` VALUES (1,'ONLINE',1),(2,'OFFLINE',2);
/*!40000 ALTER TABLE `tbl_examinationtype_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_feetype_master`
--

DROP TABLE IF EXISTS `tbl_feetype_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_feetype_master` (
  `FeeType_Code` int(11) DEFAULT NULL,
  `FeeType_Name` varchar(50) DEFAULT NULL,
  `FeeType_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_feetype_master`
--

LOCK TABLES `tbl_feetype_master` WRITE;
/*!40000 ALTER TABLE `tbl_feetype_master` DISABLE KEYS */;
INSERT INTO `tbl_feetype_master` VALUES (1,'SINGLE',1),(2,'DOUBLE',1);
/*!40000 ALTER TABLE `tbl_feetype_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_function_master`
--

DROP TABLE IF EXISTS `tbl_function_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_function_master` (
  `Function_Code` int(11) NOT NULL,
  `Function_Name` varchar(500) NOT NULL,
  `Function_URL` varchar(500) NOT NULL,
  `Function_Parent` int(11) NOT NULL,
  `Function_Display` int(11) DEFAULT NULL,
  `Function_Status` int(11) NOT NULL,
  PRIMARY KEY (`Function_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_function_master`
--

LOCK TABLES `tbl_function_master` WRITE;
/*!40000 ALTER TABLE `tbl_function_master` DISABLE KEYS */;
INSERT INTO `tbl_function_master` VALUES (1,'Status Master','frmstatusmaster.php',2,1,1),(2,'User Role','frmuserrolemaster.php',7,1,1),(3,'Parent Menu','frmparentfunctionmaster.php',10,1,1),(4,'Child Menu','frmfunctionmaster.php',10,2,1),(5,'Role Permission','frmpermissionmaster.php',10,3,1),(6,'Dashboard','Dashboard.php',1,1,1),(7,'Country Master','frmcountrymaster.php',2,6,1),(8,'State Master','frmstatemaster.php',2,7,1),(9,'Region Master','frmregionmaster.php',2,8,1),(10,'District Master','frmdistrictmaster.php',2,9,1),(11,'Tehsil Master','frmtehsilmaster.php',2,10,1),(12,'Area Master','frmareamaster.php',2,11,1),(13,'User Master','#',7,2,1),(14,'Bank Master','frmbankmaster.php',3,1,1),(15,'Designation','frmdesignationmaster.php',3,2,1),(16,'Device Type','frmdevicemaster.php',3,3,1),(17,'Organization Type','frmorgtypemaster.php',3,4,1),(18,'Affiliate Master','frmaffiliatemaster.php',4,1,1),(19,'Class Type Master','frmclasstypemaster.php',4,2,1),(20,'Course Type Master','frmcoursetypemaster.php',4,3,1),(21,'Fee Type Master','frmfeetypemaster.php',4,4,1),(22,'Examination Type Master','frmexamtypemaster.php',4,5,1),(23,'Medium Master','frmmediummaster.php',4,6,1),(24,'Course Creation','frmcoursemaster.php',4,7,1),(25,'Add New Exam','#',5,1,1),(26,'Update Existing Exam','#',5,2,1),(27,'Add New Exam Setting','#',5,3,1),(28,'Enable/Disable Exam Setting','#',5,4,1),(29,'View Active Exam Settings','#',5,5,1),(30,'Batch and Exam Mapping','#',5,6,1),(31,'Add Exam Event Confg.','#',5,7,1),(32,'Add Exam Center','#',5,8,1),(33,'Map Exam Location','#',5,9,1),(34,'PSA','#',6,2,1),(35,'DLC','#',6,3,1),(36,'Regional Manager','#',6,1,1),(37,'ITGK Approvel','#',6,4,1),(38,'View CPN Info','#',6,5,1),(39,'Admission','frmadmission.php',11,1,1),(40,'Course Fee','#',11,2,1),(41,'Admission Summary','#',11,3,1),(42,'Photo/Sign Approval','#',11,4,1),(43,'Due Fee Report','#',11,6,1),(44,'Application Payment','#',6,6,1),(45,'Search Learner','#',11,5,1),(46,'Share Disbursement','#',8,1,1),(47,'Reconciliation','#',8,2,1),(48,'DD Deposition','#',8,3,1),(49,'Payment','#',8,4,1),(50,'Reports','#',8,6,1),(51,'Annual Events','#',9,1,1),(52,'Batch Wise Events','#',9,2,1),(53,'Qualification','frmqualificationmaster.php',3,5,1),(54,'AO Profile','frmuserprofile.php',13,1,1),(55,'Organization Information','frmorgdetail.php',13,2,1),(56,'Premises Detail','frmpremisesdetails.php',13,3,1),(57,'IT Peripheral','frmitperipherals.php',13,4,1),(58,'HR Detail','frmhrdetail.php',13,6,1),(59,'Bank Detail','frmbankdetails.php',13,7,1);
/*!40000 ALTER TABLE `tbl_function_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_infra_detail`
--

DROP TABLE IF EXISTS `tbl_infra_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_infra_detail` (
  `Infra_Code` double NOT NULL,
  `Infra_User` double NOT NULL,
  `Infra_Lab_Name` varchar(100) NOT NULL,
  `Infra_Lab_Area` int(10) NOT NULL,
  `Infra_Lab_Length` int(10) NOT NULL,
  `Infra_Lab_Width` int(10) NOT NULL,
  `Infra_Lab_Height` int(10) NOT NULL,
  `Infra_Lab_Pc` int(10) NOT NULL,
  `Infra_Lab_Cap` int(10) NOT NULL,
  `Infra_Lab_Ac_Status` varchar(50) NOT NULL,
  `Infra_Lab_Fan_Status` varchar(50) NOT NULL,
  `Infra_Room_Name` varchar(100) NOT NULL,
  `Infra_Room_Area` int(10) NOT NULL,
  `Infra_Room_Length` int(10) NOT NULL,
  `Infra_Room_Width` int(10) NOT NULL,
  `Infra_Room_Height` int(10) NOT NULL,
  `Infra_Room_Pc` int(10) NOT NULL,
  `Infra_Room_Cap` int(10) NOT NULL,
  `Infra_Room_Ac_Status` varchar(50) NOT NULL,
  `Infra_Room_Fan_Status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_infra_detail`
--

LOCK TABLES `tbl_infra_detail` WRITE;
/*!40000 ALTER TABLE `tbl_infra_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_infra_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_it_peripherals`
--

DROP TABLE IF EXISTS `tbl_it_peripherals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_it_peripherals` (
  `IT_Peripherals_Code` int(11) NOT NULL,
  `IT_Peripherals_User` int(11) NOT NULL,
  `IT_Peripherals_Device_Type` varchar(50) NOT NULL,
  `IT_Peripherals_Availablity` varchar(20) NOT NULL,
  `IT_Peripherals_Make` varchar(500) NOT NULL,
  `IT_Peripherals_Model` varchar(500) NOT NULL,
  `IT_Peripherals_Quantity` int(10) NOT NULL,
  `IT_Peripherals_Detail` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_it_peripherals`
--

LOCK TABLES `tbl_it_peripherals` WRITE;
/*!40000 ALTER TABLE `tbl_it_peripherals` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_it_peripherals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_kyc_detail`
--

DROP TABLE IF EXISTS `tbl_kyc_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_kyc_detail` (
  `Kyc_Code` double NOT NULL,
  `User_Code` double NOT NULL,
  `Kyc_Owner_Pan` varchar(10) NOT NULL,
  `Kyc_Owner_Holdername` varchar(100) NOT NULL,
  `Kyc_Owner_Dob` date NOT NULL,
  `Kyc_Owner_Card_Type` varchar(50) NOT NULL,
  `Kyc_Owner_Reg_Date` date NOT NULL,
  `Kyc_Owner_Card_Image` varchar(50) NOT NULL,
  `Kyc_Owner_Card_Status` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_kyc_detail`
--

LOCK TABLES `tbl_kyc_detail` WRITE;
/*!40000 ALTER TABLE `tbl_kyc_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_kyc_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mastertable_master`
--

DROP TABLE IF EXISTS `tbl_mastertable_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mastertable_master` (
  `MasterTable_Code` int(11) NOT NULL,
  `MasterTable_Name` varchar(500) NOT NULL,
  `MasterTable_Decsription` varchar(500) NOT NULL,
  `MasterTable_Status` int(11) NOT NULL,
  PRIMARY KEY (`MasterTable_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mastertable_master`
--

LOCK TABLES `tbl_mastertable_master` WRITE;
/*!40000 ALTER TABLE `tbl_mastertable_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_mastertable_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mastervalues_master`
--

DROP TABLE IF EXISTS `tbl_mastervalues_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mastervalues_master` (
  `MasterValues_Code` int(11) NOT NULL,
  `MasterValues_MasterTable` int(11) NOT NULL,
  `MasterValues_Name` varchar(500) NOT NULL,
  `MasterValues_Status` int(11) NOT NULL,
  PRIMARY KEY (`MasterValues_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mastervalues_master`
--

LOCK TABLES `tbl_mastervalues_master` WRITE;
/*!40000 ALTER TABLE `tbl_mastervalues_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_mastervalues_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_medium_master`
--

DROP TABLE IF EXISTS `tbl_medium_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_medium_master` (
  `Medium_Code` int(11) DEFAULT NULL,
  `Medium_Name` varchar(50) DEFAULT NULL,
  `Medium_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_medium_master`
--

LOCK TABLES `tbl_medium_master` WRITE;
/*!40000 ALTER TABLE `tbl_medium_master` DISABLE KEYS */;
INSERT INTO `tbl_medium_master` VALUES (1,'ENGLISH',1),(2,'HINDI',1);
/*!40000 ALTER TABLE `tbl_medium_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mudium_master`
--

DROP TABLE IF EXISTS `tbl_mudium_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mudium_master` (
  `Medium_Code` int(11) DEFAULT NULL,
  `Medium_Name` varchar(50) DEFAULT NULL,
  `Medium_Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mudium_master`
--

LOCK TABLES `tbl_mudium_master` WRITE;
/*!40000 ALTER TABLE `tbl_mudium_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_mudium_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ogrnization`
--

DROP TABLE IF EXISTS `tbl_ogrnization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ogrnization` (
  `Orgnization_Code` int(11) NOT NULL,
  `Orgnization_User` int(11) NOT NULL,
  `Orgnization_Name` varchar(500) NOT NULL,
  `Orgnization_RegistrationNo` varchar(50) NOT NULL,
  `Orgnization_FoundedDate` date NOT NULL,
  `Orgnization_Course` varchar(500) NOT NULL,
  `Orgnization_Type` int(11) NOT NULL,
  `Orgnization_DocType` int(11) NOT NULL,
  `Orgnization_ScanDoc` varchar(1000) NOT NULL,
  PRIMARY KEY (`Orgnization_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ogrnization`
--

LOCK TABLES `tbl_ogrnization` WRITE;
/*!40000 ALTER TABLE `tbl_ogrnization` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ogrnization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_org_type_master`
--

DROP TABLE IF EXISTS `tbl_org_type_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_org_type_master` (
  `Org_Type_Code` int(11) NOT NULL,
  `Org_Type_Name` varchar(100) NOT NULL,
  `Org_Type_Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_org_type_master`
--

LOCK TABLES `tbl_org_type_master` WRITE;
/*!40000 ALTER TABLE `tbl_org_type_master` DISABLE KEYS */;
INSERT INTO `tbl_org_type_master` VALUES (1,'Proprietorship',1),(2,'Partnership',1),(3,'Private Ltd.',1),(4,'Public Ltd.',1),(5,'Trust',1),(6,'Society',1),(7,'Coorperative Society',1);
/*!40000 ALTER TABLE `tbl_org_type_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_organization_detail`
--

DROP TABLE IF EXISTS `tbl_organization_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_organization_detail` (
  `Organization_Code` int(11) NOT NULL,
  `Organization_User` int(11) NOT NULL,
  `Organization_Name` varchar(500) NOT NULL,
  `Organization_RegistrationNo` varchar(500) NOT NULL,
  `Organization_FoundedDate` date NOT NULL,
  `Organization_Course` varchar(500) NOT NULL,
  `Organization_Type` int(11) NOT NULL,
  `Organization_DocType` int(11) NOT NULL,
  `Organization_ScanDoc` varchar(1000) NOT NULL,
  `Organization_State` varchar(500) NOT NULL,
  `Organization_Region` varchar(500) NOT NULL,
  `Organization_District` varchar(500) NOT NULL,
  `Organization_Tehsil` varchar(500) NOT NULL,
  `Organization_Area` varchar(500) NOT NULL,
  `Organization_Landmark` varchar(500) NOT NULL,
  `Organization_Road` varchar(500) NOT NULL,
  `Organization_Street` varchar(500) NOT NULL,
  `Organization_HouseNo` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_organization_detail`
--

LOCK TABLES `tbl_organization_detail` WRITE;
/*!40000 ALTER TABLE `tbl_organization_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_organization_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_parent_function_master`
--

DROP TABLE IF EXISTS `tbl_parent_function_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_parent_function_master` (
  `Parent_Function_Code` int(11) DEFAULT NULL,
  `Parent_Function_Name` varchar(50) DEFAULT NULL,
  `Parent_Function_Display` int(11) DEFAULT NULL,
  `Parent_Function_Status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_parent_function_master`
--

LOCK TABLES `tbl_parent_function_master` WRITE;
/*!40000 ALTER TABLE `tbl_parent_function_master` DISABLE KEYS */;
INSERT INTO `tbl_parent_function_master` VALUES (1,'Dashboard',1,'1'),(2,'Master Data',2,'1'),(3,'Sub Master',3,'1'),(4,'Course Management',4,'1'),(5,'Examination Management',5,'1'),(6,'Channel Partner',6,'1'),(7,'User Management',8,'1'),(8,'Finance Management',9,'1'),(9,'Event Management',10,'1'),(10,'Menu Management',11,'1'),(11,'Learner Management',7,'1'),(12,'EOI Management',12,'1'),(13,'AO Registration Process',13,'1');
/*!40000 ALTER TABLE `tbl_parent_function_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_permission_master`
--

DROP TABLE IF EXISTS `tbl_permission_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_permission_master` (
  `Permission_Code` int(11) NOT NULL,
  `Permission_UserRoll` int(11) NOT NULL,
  `Permission_Function` int(11) NOT NULL,
  `Permission_Attribute` int(11) NOT NULL,
  `Permission_Status` int(11) NOT NULL,
  PRIMARY KEY (`Permission_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_permission_master`
--

LOCK TABLES `tbl_permission_master` WRITE;
/*!40000 ALTER TABLE `tbl_permission_master` DISABLE KEYS */;
INSERT INTO `tbl_permission_master` VALUES (1,1,1,3,1),(2,1,2,3,1),(3,1,3,3,1),(4,1,4,3,1),(5,1,5,3,1),(6,1,6,3,1),(7,1,7,3,1),(8,1,8,3,1),(9,1,9,3,1),(10,1,10,3,1),(11,1,11,3,1),(12,1,12,3,1),(13,1,13,3,1),(14,1,14,3,1),(15,1,15,3,1),(16,1,16,3,1),(17,1,17,3,1),(18,1,18,3,1),(19,1,19,3,1),(20,1,20,3,1),(21,1,21,3,1),(22,1,22,3,1),(23,1,23,3,1),(24,1,24,3,1),(25,1,25,3,1),(26,1,26,3,1),(27,1,27,3,1),(28,1,28,3,1),(29,1,29,3,1),(30,1,30,3,1),(31,1,31,3,1),(32,1,32,3,1),(33,1,33,3,1),(34,1,34,3,1),(35,1,35,3,1),(36,1,36,3,1),(37,1,37,3,1),(38,1,38,3,1),(39,1,39,3,1),(40,1,40,3,1),(41,1,41,3,1),(42,1,42,3,1),(43,1,43,3,1),(44,1,44,3,1),(45,1,45,3,1),(46,1,46,3,1),(47,1,47,3,1),(48,1,48,3,1),(49,1,49,3,1),(50,1,50,3,1),(51,1,51,3,1),(52,1,52,3,1),(53,1,53,3,1),(54,1,54,3,1),(55,1,55,3,1),(56,1,56,3,1),(57,1,57,3,1),(58,1,58,3,1),(59,1,59,3,1),(61,8,6,3,1),(62,8,1,3,1),(63,8,7,3,1),(64,8,8,3,1),(65,8,9,3,1),(66,8,10,3,1),(67,8,11,3,1),(68,8,12,3,1),(69,7,54,3,1),(70,7,55,3,1),(71,7,56,3,1),(72,7,57,3,1),(73,7,58,3,1),(74,7,59,3,1);
/*!40000 ALTER TABLE `tbl_permission_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pin_master`
--

DROP TABLE IF EXISTS `tbl_pin_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pin_master` (
  `Pin_Code` int(10) NOT NULL,
  `Pin_Number` varchar(50) NOT NULL,
  `Pin_Status` int(10) NOT NULL,
  `Pin_IssuedTo` int(10) NOT NULL,
  `Pin_RecTmStamp` datetime NOT NULL,
  PRIMARY KEY (`Pin_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pin_master`
--

LOCK TABLES `tbl_pin_master` WRITE;
/*!40000 ALTER TABLE `tbl_pin_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_pin_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_premises_details`
--

DROP TABLE IF EXISTS `tbl_premises_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_premises_details` (
  `Premises_Code` int(11) NOT NULL,
  `Premises_User` int(11) NOT NULL,
  `Premises_Area_Availablity` varchar(20) NOT NULL,
  `Premises_Area_Capacity` int(10) NOT NULL,
  `Premises_Area` int(10) NOT NULL,
  `Premises_Parking_Availablity` varchar(20) NOT NULL,
  `Premises_Parking_Type` varchar(20) NOT NULL,
  `Premises_Parking_For` varchar(20) NOT NULL,
  `Premises_Two_Wheeler_Capacity` int(10) NOT NULL,
  `Premises_Four_Wheeler_Capacity` int(10) NOT NULL,
  `Premises_Toilet_Availablity` varchar(20) NOT NULL,
  `Premises_Owner_Type` varchar(20) NOT NULL,
  `Premises_Toilet_Type` varchar(20) NOT NULL,
  `Premises_Pantry_Availablity` varchar(20) NOT NULL,
  `Premises_Pantry_Area` int(10) NOT NULL,
  `Premises_Library_Availablity` varchar(20) NOT NULL,
  `Premises_Library_Area` int(10) NOT NULL,
  `Premises_Staff_Room_Availablity` varchar(20) NOT NULL,
  `Premises_Staff_Area` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_premises_details`
--

LOCK TABLES `tbl_premises_details` WRITE;
/*!40000 ALTER TABLE `tbl_premises_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_premises_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_qualification_master`
--

DROP TABLE IF EXISTS `tbl_qualification_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_qualification_master` (
  `Qualification_Code` int(10) NOT NULL,
  `Qualification_Name` varchar(50) NOT NULL,
  `Qualification_Status` int(10) NOT NULL,
  PRIMARY KEY (`Qualification_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_qualification_master`
--

LOCK TABLES `tbl_qualification_master` WRITE;
/*!40000 ALTER TABLE `tbl_qualification_master` DISABLE KEYS */;
INSERT INTO `tbl_qualification_master` VALUES (1,'Qualifi1',2);
/*!40000 ALTER TABLE `tbl_qualification_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_region_master`
--

DROP TABLE IF EXISTS `tbl_region_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_region_master` (
  `Region_Code` double NOT NULL,
  `Region_Name` varchar(50) NOT NULL,
  `Region_State` double NOT NULL,
  `Region_Status` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_region_master`
--

LOCK TABLES `tbl_region_master` WRITE;
/*!40000 ALTER TABLE `tbl_region_master` DISABLE KEYS */;
INSERT INTO `tbl_region_master` VALUES (1,'Ajmer Division',29,1),(2,'Bharatpur Division',29,1),(3,'Bikaner Division',29,1),(4,'Jaipur Division',29,1),(5,'Jodhpur Division',29,1),(6,'Kota Division',29,1),(7,'Udaipur Division',29,1);
/*!40000 ALTER TABLE `tbl_region_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_staff_detail`
--

DROP TABLE IF EXISTS `tbl_staff_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_staff_detail` (
  `Staff_Code` int(11) NOT NULL,
  `Staff_Designation` int(11) DEFAULT NULL,
  `Staff_User` int(11) NOT NULL,
  `Staff_Name` varchar(100) NOT NULL,
  `Staff_Dob` date NOT NULL,
  `staff_Gender` varchar(10) DEFAULT NULL,
  `Staff_Qualification1` varchar(50) NOT NULL,
  `Staff_Qualification2` varchar(50) NOT NULL,
  `Staff_Qualification3` varchar(50) NOT NULL,
  `Staff_Mobile` int(10) NOT NULL,
  `Staff_Experience` int(10) NOT NULL,
  `Staff_Email_Id` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_staff_detail`
--

LOCK TABLES `tbl_staff_detail` WRITE;
/*!40000 ALTER TABLE `tbl_staff_detail` DISABLE KEYS */;
INSERT INTO `tbl_staff_detail` VALUES (1,NULL,21290041,'Ram','0000-00-00',NULL,'BA','NA','BA',2147483647,3,'as@gd.com');
/*!40000 ALTER TABLE `tbl_staff_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_state_master`
--

DROP TABLE IF EXISTS `tbl_state_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_state_master` (
  `State_Code` int(11) NOT NULL,
  `State_Name` varchar(500) NOT NULL,
  `State_Country` int(11) NOT NULL,
  `State_Status` int(11) NOT NULL,
  PRIMARY KEY (`State_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_state_master`
--

LOCK TABLES `tbl_state_master` WRITE;
/*!40000 ALTER TABLE `tbl_state_master` DISABLE KEYS */;
INSERT INTO `tbl_state_master` VALUES (1,'Rajasthan',1,1);
/*!40000 ALTER TABLE `tbl_state_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_status_master`
--

DROP TABLE IF EXISTS `tbl_status_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_status_master` (
  `Status_Code` int(11) NOT NULL,
  `Status_Name` varchar(500) NOT NULL,
  `Status_Description` varchar(500) NOT NULL,
  PRIMARY KEY (`Status_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_status_master`
--

LOCK TABLES `tbl_status_master` WRITE;
/*!40000 ALTER TABLE `tbl_status_master` DISABLE KEYS */;
INSERT INTO `tbl_status_master` VALUES (1,'ACTIVE','ACTIVE'),(2,'DEACTIVE','DEACTIVE2');
/*!40000 ALTER TABLE `tbl_status_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tehsil_master`
--

DROP TABLE IF EXISTS `tbl_tehsil_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tehsil_master` (
  `Tehsil_Code` int(11) NOT NULL,
  `Tehsil_Name` varchar(500) NOT NULL,
  `Tehsil_District` int(11) NOT NULL,
  `Tehsil_Status` int(11) NOT NULL,
  PRIMARY KEY (`Tehsil_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tehsil_master`
--

LOCK TABLES `tbl_tehsil_master` WRITE;
/*!40000 ALTER TABLE `tbl_tehsil_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tehsil_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user_master`
--

DROP TABLE IF EXISTS `tbl_user_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user_master` (
  `User_Code` int(11) NOT NULL,
  `User_EmailId` varchar(500) NOT NULL,
  `User_MobileNo` varchar(20) NOT NULL,
  `User_LoginId` varchar(20) NOT NULL,
  `User_Password` varchar(20) NOT NULL,
  `User_UserRoll` int(11) NOT NULL,
  `User_ParentId` int(11) NOT NULL,
  `User_Status` int(11) NOT NULL,
  PRIMARY KEY (`User_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user_master`
--

LOCK TABLES `tbl_user_master` WRITE;
/*!40000 ALTER TABLE `tbl_user_master` DISABLE KEYS */;
INSERT INTO `tbl_user_master` VALUES (1,'admin@rkcl.in','00000000000','superadmin','su@123',1,0,1),(2,'itgk@rkcl.in','0000000000','itgk','itgk@123',7,1,1);
/*!40000 ALTER TABLE `tbl_user_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_userprofile`
--

DROP TABLE IF EXISTS `tbl_userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_userprofile` (
  `UserProfile_Code` int(11) NOT NULL,
  `UserProfile_User` int(11) NOT NULL,
  `UserProfile_Initial` int(11) NOT NULL,
  `UserProfile_FirstName` varchar(500) NOT NULL,
  `UserProfile_MiddleName` varchar(500) NOT NULL,
  `UserProfile_LastName` varchar(500) NOT NULL,
  `UserProfile_HouseNo` varchar(500) NOT NULL,
  `UserProfile_Street` varchar(500) NOT NULL,
  `UserProfile_Road` varchar(500) NOT NULL,
  `UserProfile_Landmark` varchar(500) NOT NULL,
  `UserProfile_Area` int(11) NOT NULL,
  `UserProfile_Image` varchar(500) NOT NULL,
  `UserProfile_Gender` int(11) NOT NULL,
  `UserProfile_TagName` int(11) NOT NULL,
  `UserProfile_ParentName` varchar(500) NOT NULL,
  `UserProfile_Occupation` varchar(100) NOT NULL,
  `UserProfile_PanNo` varchar(10) NOT NULL,
  `UserProfile_AadharNo` varchar(20) NOT NULL,
  `UserProfile_STDCode` varchar(10) NOT NULL,
  `UserProfile_Mobile` varchar(10) DEFAULT NULL,
  `UserProfile_PhoneNo` varchar(10) NOT NULL,
  `UserProfile_Website` varchar(200) NOT NULL,
  `UserProfile_FaceBookId` varchar(20) NOT NULL,
  `UserProfile_TwitterId` varchar(20) NOT NULL,
  `UserProfile_KycScan` varchar(500) NOT NULL,
  PRIMARY KEY (`UserProfile_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_userprofile`
--

LOCK TABLES `tbl_userprofile` WRITE;
/*!40000 ALTER TABLE `tbl_userprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_userprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_userroll_master`
--

DROP TABLE IF EXISTS `tbl_userroll_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_userroll_master` (
  `UserRoll_Code` int(11) NOT NULL,
  `UserRoll_Name` varchar(500) NOT NULL,
  `UserRoll_Status` int(11) NOT NULL,
  PRIMARY KEY (`UserRoll_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_userroll_master`
--

LOCK TABLES `tbl_userroll_master` WRITE;
/*!40000 ALTER TABLE `tbl_userroll_master` DISABLE KEYS */;
INSERT INTO `tbl_userroll_master` VALUES (1,'Super Administrator',1),(2,'Administrator',1),(3,'Developer',1),(4,'RM',1),(5,'PSA',1),(6,'DLC',1),(7,'ITGK',1),(8,'Learner',1),(9,'Examination Body',1),(10,'Certification Boday',1),(11,'Account Deparrtment',1);
/*!40000 ALTER TABLE `tbl_userroll_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_veryfy`
--

DROP TABLE IF EXISTS `tbl_veryfy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_veryfy` (
  `Veryfy_Code` int(11) NOT NULL AUTO_INCREMENT,
  `Veryfy_User` int(11) DEFAULT NULL,
  `Veryfy_Pin` varchar(50) DEFAULT NULL,
  `Veryfy_Status` int(11) DEFAULT NULL,
  `Veryfy_SentDate` date DEFAULT NULL,
  `Veryfy_Date` date DEFAULT NULL,
  PRIMARY KEY (`Veryfy_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_veryfy`
--

LOCK TABLES `tbl_veryfy` WRITE;
/*!40000 ALTER TABLE `tbl_veryfy` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_veryfy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vw_userrolewisefunction`
--

DROP TABLE IF EXISTS `vw_userrolewisefunction`;
/*!50001 DROP VIEW IF EXISTS `vw_userrolewisefunction`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_userrolewisefunction` AS SELECT 
 1 AS `UserRole`,
 1 AS `FunctionName`,
 1 AS `FunctionURL`,
 1 AS `Display`,
 1 AS `Attribute`,
 1 AS `Parent`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_userrolewiseparentmenu`
--

DROP TABLE IF EXISTS `vw_userrolewiseparentmenu`;
/*!50001 DROP VIEW IF EXISTS `vw_userrolewiseparentmenu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_userrolewiseparentmenu` AS SELECT 
 1 AS `UserRole`,
 1 AS `ParentCode`,
 1 AS `ParentName`,
 1 AS `DisplayOrder`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `vw_userrolewisefunction`
--

/*!50001 DROP VIEW IF EXISTS `vw_userrolewisefunction`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_userrolewisefunction` AS select `a`.`Permission_UserRoll` AS `UserRole`,`b`.`Function_Name` AS `FunctionName`,`b`.`Function_URL` AS `FunctionURL`,`b`.`Function_Display` AS `Display`,`a`.`Permission_Attribute` AS `Attribute`,`b`.`Function_Parent` AS `Parent` from (`tbl_permission_master` `a` join `tbl_function_master` `b` on(((`a`.`Permission_Function` = `b`.`Function_Code`) and (`b`.`Function_Status` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_userrolewiseparentmenu`
--

/*!50001 DROP VIEW IF EXISTS `vw_userrolewiseparentmenu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_userrolewiseparentmenu` AS select distinct `a`.`Permission_UserRoll` AS `UserRole`,`c`.`Parent_Function_Code` AS `ParentCode`,`c`.`Parent_Function_Name` AS `ParentName`,`c`.`Parent_Function_Display` AS `DisplayOrder` from ((`tbl_permission_master` `a` join `tbl_function_master` `b` on((`a`.`Permission_Function` = `b`.`Function_Code`))) join `tbl_parent_function_master` `c` on(((`b`.`Function_Parent` = `c`.`Parent_Function_Code`) and (`c`.`Parent_Function_Status` = 1)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-21 11:09:02
