<?php
$title = "Learner Re-Exam Dashboard";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var OrganizationCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var OrganizationCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<link rel="stylesheet" href="css/datepicker.css">
<link rel="stylesheet" href="css/profile_style.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:46px !important;">

            <div class="panel-heading">Learner Re-Exam Dashboard</div>
            <div id="pbid" class="panel-body pb">

                <!-- <div class="jumbotron"> -->
                <form name="frmLearnerReExamDashboard" id="frmLearnerReExamDashboard"  role="form" action="" class="form-inline" enctype="multipart/form-data">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>


                    <div id="main-content" >

                        <div class="container">
                            <div class="col-sm-8">     
                                <label for="area">Select Searching Option:<span class="star">*</span></label> <br/>                               
                                <label class="radio-inline"> <input type="radio" id="searchtype" name="searchtype" value="1" checked="checked" /> Learner Code </label>

                                <label class="radio-inline"> <input type="radio" id="searchtype" name="searchtype" value="3"/> Search by Name, DOB,  IT-GK</label>
                            </div>

                        </div>
                        <br>
                        <div class="container desc" id="search1" >
                            <div class="col-sm-3">      

                                <input type="text" class="form-control" name="txtLearnerCode" id="txtLearnerCode" placeholder="Learner code">  

                            </div>
                            <div class="col-sm-3">   
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show Details"/> 
                            </div>

                        </div>

                        <div class="container desc" id="search3" style="display: none;">
                            <!--                            <div class="col-sm-3 form-group"> 
                                                            <label for="edistrict">District:<span class="star">*</span></label>
                                                            <select id="ddlDistrict" name="ddlDistrict" class="form-control valid" aria-required="true" aria-invalid="false">
                                                            </select>    
                                                        </div>-->
                            <div class="col-sm-4 ">      
                                <label for="edistrict">Learner Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtLearnerName" id="txtLearnerName" placeholder="Learner Name">  
                            </div>
                            <div class="col-sm-4 ">      
                                <label for="edistrict">IT-GK Code:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtCcode" id="txtCcode" placeholder="IT-GK Code">  
                            </div>
                            <div class="col-sm-3 ">     
                                <label for="faname">Select DOB:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtLdob" id="txtLdob" placeholder="DD-MM-YYYY">
                            </div>
                            <br><br><br><br>
                            <div class="col-sm-4">   
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show Details"/> 
                            </div>

                        </div>



                        <div class="panel-body scrollmenu" style="padding:0px !important">
                            <div class="container">
                                <div id="grid" class="col-sm-11"> </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>   
        </div>
    </div>
</div>
<link href="css/popup.css" rel="stylesheet" type="text/css">
<!-- Modal -->
<div id="myModalupdate" class="modal" style="padding-top:100px !important">

    <div class="modal-content" style="width: 90%; margin-left: 70px;">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h6>Learner Re-Exam Details</h6>
        </div>
        <div id="responseupdate">

        </div>
        <div class="modal-body scrollmenu" style="max-height: 800px;" id='formhtml'>

        </div>
    </div>
</div>

<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script>
    $(document).ready(function () {
        $("input[name$='searchtype']").click(function () {
            var test = $(this).val();

            $("div.desc").hide();
            $("#search" + test).show();
        });
    });
</script>

<style>
    #errorBox{
        color:#F00;
    }
    div.scrollmenu {

        overflow: auto;
        white-space: nowrap;
    }
    .exampannelNew {
        float: left;
        margin-right: 5px;
        width: 13.9%;
    }
      .exampannelNew1 {
        float: left;
        margin-right: 5px;
        width: 17%;
    }
    .pb{
        padding:0px !important; 
        width: 103%;
    }
    .pbb{
        padding:0px !important; 
        width: 98%;
    }
</style>
<script type="text/javascript">
    $('#txtLdob').datepicker({
        format: "dd-mm-yyyy"
    });
</script>



<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        var pbid = document.getElementById("pbid");
        $(".btn-primary").click(function () {

            pbid.classList.remove("pb");
            pbid.classList.add("pbb");
        });
        function FillEvent() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfEventMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlExamEvent").html(data);
                }
            });
        }
        FillEvent();
        function FillDistrict() {

            $.ajax({
                url: 'common/cfDistrictMaster.php',
                type: "post",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $('#ddlDistrict').html(data);
                }
            });
        }
        FillDistrict();
        $("#frmLearnerReExamDashboard").submit(function () {

            if ($("#frmLearnerReExamDashboard").valid())
            {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfLearnerExamDetails.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmLearnerReExamDashboard").serialize();
                data = "action=ReExamDETAILS&" + forminput; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        $('#response').empty();
                        //$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span></span></p>");
                        $("#grid").html(data);
                        $('#example').DataTable({
                            dom: 'Bfrtip',
                            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
                        });
                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        $("#grid").on("click", ".approvalLetter", function () {
            var learnercode = $(this).attr("id");
            var itgkcode = $(this).attr("name");
            var modal = document.getElementById('myModalupdate');
            var span = document.getElementsByClassName("close")[0];
            modal.style.display = "block";
            span.onclick = function () {
                modal.style.display = "none";
            }
            $('#responseupdate').empty();
            $('#responseupdate').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfLearnerExamDetails.php"; // the script where you handle the form input.
            var data;
            data = "action=getbysearchreexam&study_cen=" + itgkcode + "&scol_no=" + learnercode + ""; // serializes the form's elements.				 
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    //  alert(data);
                    $("#formhtml").html(data);
                    $('#responseupdate').empty();
                }
            });
        });

    });</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>




<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>