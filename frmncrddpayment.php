<?php
$title = "NCR DD Payment Mode";
include ('header.php');
include ('root_menu.php');


if (isset($_REQUEST['code'])) {
    //print_r($_POST);
    $Code = $_REQUEST['code'];
    ////  $paytype = $_POST['ddpaytype'];
//    $amount = $_POST['amount'];
    echo "<script>var UserCode='" . $_SESSION['User_Code'] . "'</script>";
    echo "<script>var LoginID='" . $_SESSION['User_LoginId'] . "'</script>";
    echo "<script>var AckCode='" . $_REQUEST['code'] . "'</script>";
    //echo "<script>var PayTypeCode=" . $_POST['paytypecode'] . "</script>";
    echo "<script>var PayType='NCRFeePayment'</script>";
    echo "<script>var amount='51000'</script>";
    echo "<script>var email='" . $_SESSION['User_EmailId'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";

    $payutxnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    echo "<script>var PayUTranID= '" . $payutxnid . "' </script>";

    $random = (mt_rand(1000, 9999));
    $random.= date("y");
    $random.= date("m");
    $random.= date("d");
    $random.= date("H");
    $random.= date("i");
    $random.= date("s");
    //echo $random;

    echo "<script>var Randomtimestamp= '" . $random . "' </script>";
}


// print_r($_POST);
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px;">

            <div class="panel-heading">NCR Payment Transection Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="DDForm" id="DDForm" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">RSP Code:</label>
                            <!--   <div class="form-control" maxlength="50"   name="txtCenterCodediv" id="txtCenterCodediv">
                                  
                              </div> -->
                            <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                            <input type="text" readonly="true" class="form-control" maxlength="50"   name="txtCenterCode" id="txtCenterCode" placeholder="Center Code" value="<?php echo $_SESSION['User_LoginId']; ?>">

                            <input type="hidden" class="form-control" name="txtAckcode" id="txtAckcode" placeholder="Ack. Code">
                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="ename">Owner Name:</label>
                            <!--                               <div class="form-control" maxlength="50" name="firstnamediv" id="firstnamediv">
                                                              
                                                          </div> -->
                            <input type="text" readonly="true"	 class="form-control text-uppercase" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" maxlength="50" name="firstname" id="firstname" placeholder="End Date">    
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="faname">Mobile No:</label>
                            <!-- <div class="form-control" maxlength="50" name="phonediv" id="phonediv">
                                 
                             </div> -->
                            <input type="text" readonly="true" class="form-control" maxlength="50" name="phone" id="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>"  placeholder="Owner Mobile">
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="faname">Email ID:</label>
                            <!--    <div class="form-control" maxlength="50" name="emaildiv" id="emaildiv">
                                   
                               </div> -->
                            <input type="text" readonly="true" class="form-control" maxlength="50" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>"  placeholder="Owner Email">
                        </div>                            
                    </div>  

                    <div class="container">
                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Payment Type:</label>
                            <input type="text" readonly="true" name="productinfo" class="form-control" id="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>"/>

                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="ename">Transaction Reference No:</label>
                            <!--  <div class="form-control" maxlength="50"  name="transactiondiv" id="transactiondiv">
                                 123456
                             </div> -->
                            <input type="text" readonly="true" class="form-control" maxlength="50" name="transaction" id="transaction" value="<?php echo $PayUTranID; ?>" placeholder="Tran. Ref. No:"> 
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="faname">Amount:</label>
                            <!--   <div class="form-control" maxlength="50" name="amountdiv" id="amountdiv">
                                  
                              </div> -->
                            <input type="text" readonly="true" class="form-control" maxlength="50" name="amount" id="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"  placeholder="Total Amount">
                        </div>
                    </div>


                    <div class="container">

                    </div>



                    <div class="container">
                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">DD No.:</label>
                            <input type="text" class="form-control" name="ddno" id="ddno" value="" placeholder="DD Number" maxlength="6" onkeypress="javascript:return allownumbers(event);"/>

                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="ename">DD Date:</label>                                
                            <input type="text" class="form-control" name="dddate" readonly="true" id="dddate" placeholder="YYYY-MM-DD">  
                        </div>
                        <div class="col-sm-4 form-group"> 
                            <label for="micr">MICR Code:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="9" onkeypress="javascript:return allownumbers(event);"   name="txtMicrNo" id="txtMicrNo" placeholder="EMP Bank MICR Code">
                        </div>	

                        <div class="col-sm-4 form-group"> 
                            <label for="bankdistrict">Employee Bank District:<span class="star">*</span></label>
                            <select id="ddlBankDistrict" name="ddlBankDistrict" class="form-control">

                            </select>
                        </div>


                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group"> 
                            <label for="bankname">Employee Bank Name:<span class="star">*</span></label>
                            <select id="ddlBankName" name="ddlBankName" class="form-control">
                                <option value="" selected="selected">Please Select</option>                
                            </select>                                                              
                        </div> 
                        <div class="col-sm-4 form-group"> 
                            <label for="branchname">Branch_Name:<span class="star">*</span></label>
                            <input type="text" class="form-control" onkeypress="javascript:return allowchar(event);"  name="txtBranchName" id="txtBranchName"  placeholder="EMP Branch Name">
                        </div>
                        <div class="col-sm-4 form-group"> 
                            <label for="ddreceipt">Upload DD Image<span class="star">*</span></label>
                            <input type="file" class="form-control"  name="ddreceipt" id="ddreceipt" onchange="checkScanForm(this)">
                        </div>    

                    </div>

                    <div class="container">
                        <div class="col-sm-4"> 

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Pay Now"/>   

                        </div>
                    </div>
                </form>

            </div>
        </div>   
    </div>
</div>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script src="scripts/ddncrupload.js"></script>

<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">
    $('#dddate').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true
                // autoclose: true
    });
</script>
<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>

<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#ddreceipt').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("ddreceipt").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("ddreceipt").value = '';
            return false;
        }
        else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("ddreceipt").value = '';
            return false;
        }
        document.getElementById("ddreceipt").innerHTML = "";
        return true;
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";



    $(document).ready(function () {
        $('#transaction').val(PayUTranID);
        $('#txtGenerateId').val(Randomtimestamp);

        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                    transaction.value = data;
                }
            });
        }
        //GenerateUploadId();

        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        FillBankDistrict();

        $("#ddlBankDistrict").change(function () {
            FillBankName(this.value);
        });
        function FillBankName(districtid) {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKNAME&districtid=" + districtid,
                success: function (data) {
                    $("#ddlBankName").html(data);
                }
            });
        }


        function showData() {


            $.ajax({
                type: "post",
                url: "common/cfPayment.php",
                data: "action=SHOW&values=" + UserCode + "",
                success: function (data) {
                    //alert(data);
                    //alert($.parseJSON(data)[0]['OwnerName']);
                    data = $.parseJSON(data);

                    $('#amount').val(amount);
                    firstname.value = data[0].OwnerName;
                    phone.value = data[0].OwnerMobile;
                    $('#email').val(email);
                    //$('#productinfo').val(PayTypeCode);
                    $('#productinfo').val(PayType);
                    txtAckcode.value = AckCode;
                }
            });
        }

        showData();

        $("#btnSubmit").click(function () {
            if ($("#DDForm").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfNcrDDPayment.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#DDForm").serialize();
                if (Mode == 'Edit')
                {

                    data = "action=Update&" + forminput; // serializes the form's elements.

                    //alert(data);
                }
                else
                {

                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmorgprintreceipt.php";
                            }, 1000);

                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmddadmissionupload_validation.js" type="text/javascript"></script>
</html> 