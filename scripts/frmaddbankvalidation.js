$("#frmaddbanks").validate({
        rules: {
            
			txtifsc: { required: true },
			ddlBankname: { required: true },
			txtMicr: { required: true },
			txtBranch: { required: true },
			txtAddress: { required: true },
			txtContact: { required: true },
			txtState: { required: true },
			txtDistrict: { required: true },
			txtCity: { required: true },
			ddlStatus: { required: true }
			
			
			
        },			
        messages: {				
			
			txtifsc: { required: '<span style="color:red; font-size: 12px;">Please Enter ifsc code</span>' },
			ddlBankname: { required: '<span style="color:red; font-size: 12px;">Please Enter Bank Name</span>' },
			txtMicr: { required: '<span style="color:red; font-size: 12px;">Please Enter Micr code</span>' },
			txtBranch: { required: '<span style="color:red; font-size: 12px;">Please Enter Branch Name</span>' },
			txtAddress: { required: '<span style="color:red; font-size: 12px;">Please Enter Address</span>' },
			txtContact: { required: '<span style="color:red; font-size: 12px;">Please Enter Contact</span>' },
			txtState: { required: '<span style="color:red; font-size: 12px;">Please Enter State</span>' },
			txtDistrict: { required: '<span style="color:red; font-size: 12px;">Please Enter Disrict</span>' },
			txtCity: { required: '<span style="color:red; font-size: 12px;">Please Enter City</span>' },
			ddlStatus: { required: '<span style="color:red; font-size: 12px;">Please Enter Status</span>' }
			
			
        },
	});