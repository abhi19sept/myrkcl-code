var _fileUpload = document.getElementById('btnUpload'), 
_file = document.getElementById('_file'), 
_progress = document.getElementById('_progress'),
_UploadId=document.getElementById('txtUploadId'),
_tblBlock=document.getElementById('tblBlock'),
_fileuploadbutton=document.getElementById('fileuploadbutton');

var upload = function(){

    if(_file.files.length === 0){
        return;
    }

    var data = new FormData();
    data.append('SelectedFile', _file.files[0]);
    data.append('UploadId',_UploadId.value);
    
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        
        if(request.readyState === 4){
            
            try {
                var resp =  dompurify.sanitize(JSON.parse(request.response));
                //alert(resp);
            } catch (e){
                var resp = {
                    status: 'error',
                    data: 'Unknown error occurred: [' + request.responseText + ']'
                };
            }
            console.log(resp.status + ': ' + resp.data);
            $("#grid").html(resp.status + ': ' + resp.data);
            /*$("#response").html(resp.status + ': ' + resp.data);*/
            if(resp.status==='success')
            {
                $("#grid").html(dompurify.sanitize(resp.data));
                _tblBlock.style["display"]="block";
                _fileuploadbutton.style["display"]="none";
               
            }
        }
    };

    request.upload.addEventListener('progress', function(e){
        _progress.style.width = Math.ceil(e.loaded/e.total) * 100 + '%';
    }, false);
    
    request.open('POST', 'common/cfBlockFileUpload.php?UploadId' + _UploadId);
    request.send(data);
    
}
if(_fileUpload)
{
_fileUpload.addEventListener('click', upload);
}
else
{
    alert(_fileUpload);
}
