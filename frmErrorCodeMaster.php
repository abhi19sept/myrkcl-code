<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Error Code Master</title>
</head>

<body>
<div class="wrapper">
<?php
            include './include/header.html';

            include './include/menu.php';
            
            if (isset($_REQUEST['code'])) {
                echo "<script>var ErrorCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var ErrorCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
         <div class="main">
         	<h1>Error Code Master</h1>
         	<form name="frmErrorCodeMaster" id="frmErrorCodeMaster" action="">
            	<table border="0" cellpadding="0" cellspacing="10" width="100%" class="field">
                	<tr>
                    	<td colspan="3" id="response">
                        	
                        </td>
                    </tr>
                	<tr>
                    	<td>
                        	Error Name
                        </td>
                        <td>
                        	:
                        </td>
                        <td>
                        	<input type="text" id="txtErrorName" name="txtErrorName" class="text" maxlength="10" />
                        </td>
                    </tr>
                    
                    <tr>
                    	<td>
                        	Error Description
                        </td>
                        <td>
                        	:
                        </td>
                        <td>
                        	<input type="text" id="txtErrorDescription" name="txtErrorDescription" class="text" maxlength="10" />
                        </td>
                    </tr>
                    
                 
                    <tr>
                    	<td>
                        	Error Status
                        </td>
                        <td>
                        	:
                        </td>
                        <td>
                        	<select id="ddlStatus" name="ddlStatus" class="text">
                            	
                            </select>
                        </td>
                    </tr>
                    <tr>
                    	<td colspan="3" align="right">
                        	<p class="btn submit"><input type="submit" id="btnSubmit" name="btnSubmit" value="Submit" /></p>
                        </td>
                    </tr>
                    <tr>
                    	<td colspan="3" id="gird">
                        	
                        </td>
                    </tr>
                </table>
            </form>
         </div>
    <?php
            include './include/footer.html';
            ?>
     </div>
</body>
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
            
            
            
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfErrorCodeMaster.php",
                    data: "action=DELETE&values=" + ErrorCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmErrorCodemaster.php";
                           }, 1000);
                            
                            Mode="Add";
                            resetForm("frmErrorCodemaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfErrorCodeMaster.php",
                    data: "action=EDIT&values=" + ErrorCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Status']);
                        data = $.parseJSON(data);
                        txtErrorName.value = data[0].ErrorName;
                        ddlStatus.value = data[0].Status;
                        
                    }
                });
            }

            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfErrorCodeMaster.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);

                    }
                });
            }

            showData();


            $("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfErrorCodeMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtErrorName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + ErrorCode + "&name=" + txtErrorName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                            }, 3000);

                            Mode="Add";
                            resetForm("frmErrorCodemaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
</html>