<?php
$title = "Admission Payment";
include ('header.php');
include ('root_menu.php');
if (isset($_SESSION)) {
    echo "<script>var Mode='Add'</script>";
    echo "<script>var Role='" . $_SESSION['User_UserRoll'] . "'</script>";
} else {

    echo "<script>var Mode='Add'</script>";
    echo "<script>var Role='0'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Madarsa Learner Confirm Report</div>
            <div class="panel-body">
                <form name="frmconfirmmadarsalearner" id="frmconfirmmadarsalearner" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                        <div class="col-sm-4 form-group">     
                            <label for="course">Select Course:</label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">
                                <option value="0">Select </option>
                                <option value="20">RS-CIT Madarsa Urdu ParaTeacher</option>
                            </select>
                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Batch:</label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>									
                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select District:</label>
                            <select id="ddlDistrict" name="ddlDistrict" class="form-control">

                            </select>									
                        </div> 
                    </div>
                    <div class="container">
                        <br><input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View Data" style="display:none;"/>    
                    </div>
                    <div id="menuList" name="menuList" style="margin-top:5px;"> </div> 


            </div>
        </div>   
    </div>
</form>
</div>

</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {



        // FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILL&values=" + selCourse + "",
                success: function (data) {

                    $("#ddlBatch").html(data);
                }
            });

        });

        $("#ddlBatch").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL&codes=29",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        });

        function showAllData(val, val1, val2) {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfConfirmMadarsaLearner.php";
            var data = "action=SHOWALLRPT&batch=" + val + "&course=" + val1 + "&district=" + val2 + "";

            $.ajax({
                type: "post",
                url: url,
                data: data,

                success: function (data) {
                    $('#response').empty();
                    $("#menuList").html(data);
                    $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false,
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });
        }

        $("#ddlDistrict").change(function () {
            $('#btnSubmit').show();

        });


        $("#btnSubmit").click(function () {
            showAllData(ddlBatch.value, ddlCourse.value, ddlDistrict.value);
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>