<?php

// certificate file locations
    $public_cert_path = 'certs/abc.cer';

// set variables

    $aadhaar_no = '408518993238';
    $name = "Siddhartha Agrawal";
    $api_version = "2.0";
    $asa_licence_key = "MAKD99YEmQEuRkehmFwDj-GAkNVcVB5yaNB7iDENmGvdEeAZ48psOms";
    $lk = "STGDOIT006";
    $ac = "public";
    $sa = "public";
    $tid = "public";
    $txn = "AuthDemoClient:public:" . date("Ymdhms");
    $ts = date('Y-m-d') . 'T' . date('H:i:s');

    $pid_block = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
    $pid_block .= "<Pid ts='" . $ts . "' ver='1.0'><Demo><Pi ms='P' name='" . $name . "' gender='M' /></Demo></Pid>";


// generate aes-256 session key
    $session_key = openssl_random_pseudo_bytes(32);


    $auth_xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
    $auth_xml .= '<authrequest uid="' . $aadhaar_no . '" tid="public" subaua="STGDOIT006" ip="117.239.28.90" fdc="NA" idc="NA" udc="RKCL123" macadd="‎68:07:15:15:09:37" lot="P" lov="302029" 
lk="MAKD99YEmQEuRkehmFwDj-GAkNVcVB5yaNB7iDENmGvdEeAZ48psOms" rc="Y">
    	<Skey ci="20171105">' . encrypt_session_key($session_key) . '</Skey>
	<Hmac>' . calculate_hmac($pid_block, $session_key) . '</Hmac>
	<Data type="X">' . encrypt_pid($pid_block, $session_key) . '</Data>
</authrequest>';

// make a request to uidai
    $ch = curl_init("http://103.203.138.120/api/aua/demo/pi/encr");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $auth_xml);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Accept: application/xml",
        "Content-Type: application/xml",
        "appname: MYRKCL"
    ));

    echo "Response from UIDAI\n";

    $result = curl_exec($ch);
    curl_close($ch);
    $xml = @simplexml_load_string($result);
    echo "<pre>";
    print_r($xml);
    /*$return_status = $xml['ret'];
    if ($return_status == "y") {
        $res = 1;
    }
    if ($return_status != "y") {
        $res = 0;
    } else {
        $res = 'Aadhaarno not exist';
    }
    echo $res;*/

    $status = $xml->auth[0]['status'];

    if($status == "y"){
        echo "<BR>Adhaar Card Exists";
    } else {
        echo "<BR>Some error Occurred";
    }


    function encrypt_pid($pid_block, $session_key)
    {
        return encrypt_using_session_key($pid_block, $session_key);
    }

    function encrypt_using_session_key($data, $session_key)
    {
        $blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
        $pad = $blockSize - (strlen($data) % $blockSize);
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $session_key, $data . str_repeat(chr($pad), $pad), MCRYPT_MODE_ECB));
    }

    function calculate_hmac($data, $session_key)
    {
        return encrypt_using_session_key(hash('sha256', $data, true), $session_key);
    }

    function public_key_validity()
    {
        global $public_cert_path;
        $certinfo = openssl_x509_parse(file_get_contents($public_cert_path));
        return date('Ymdhis', $certinfo['validTo_time_t']);
    }

    function encrypt_session_key($session_key)
    {
        global $public_cert_path;

        $pub_key = openssl_pkey_get_public(file_get_contents($public_cert_path));
        $keyData = openssl_pkey_get_details($pub_key);
        openssl_public_encrypt($session_key, $encrypted_session_key, $keyData['key'], OPENSSL_PKCS1_PADDING);
        return base64_encode($encrypted_session_key);
    }
