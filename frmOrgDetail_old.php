<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Organization Details</title>
         <link href="css/jquery-ui.css" rel="Stylesheet" type="text/css" />
    </head>

    <body>
        <div class="wrapper">
            <?php
            include './include/header.html';

            include './include/menu.php';

            if (isset($_REQUEST['code'])) {
                echo "<script>var OrganizationCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var OrganizationCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
            <div class="main">
                <h1>User Profile Details</h1>
                <form name="frmOrgDeatil" id="frmOrgDeatil" action="">
                    <table border="0" cellpadding="0" cellspacing="10" width="100%" class="field">
                        <tr>
                            <td colspan="6" id="response">
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <table border="0" cellpadding="0" cellspacing="10" width="100%" class="field">
                                    <tr align="left">
							<td nowrap="nowrap">Name of the Organization/Center</td>
							<td>:</td>
							<td colspan="3"><input type="text" name="txtName1" id="txtName1" class="text" maxlength="" /></td>
						</tr>
						
						<tr align="left">
							<td nowrap="nowrap">Registration No.</td>
							<td>:</td>
							<td colspan="3"><input type="text" name="txtRegno" id="txtRegno" class="text" maxlength="" /></td>
						</tr>
						
						<tr align="left">
							<td nowrap="nowrap">Date of Establishment</td>
							<td>:</td>
							<td colspan="3"><input type="text" name="txtEstdate" id="txtEstdate" class="text" maxlength="" placeholder="MM-DD-YYYY"/></td>
						</tr>
						
						<tr>
							<td nowrap="nowrap">Request Course</td>
							<td>:</td>
							<td>
								<select id="txtCourse" name="txtCourse" class="text">
									<option selected="true"value="">Select</option>
									<option value="1" >
										RS-CIT
									</option>
																	</option> -->
								</select>
								&nbsp;<font color="red">*</font>
							</td>
						</tr>
						
						<tr>
							<td nowrap="nowrap">Type of Organization</td>
							<td>:</td>
							<td>
								<select id="txtType" name="txtType" class="text">

								</select>
								<font color="red">*</font>
							</td>
						</tr>
						
						<tr>
							<td nowrap="nowrap">Document Type</td>
							<td>:</td>
							<td>
								<select id="txtDoctype" name="txtDoctype" class="text">
									<option value=""  selected="true">Select</option>
									<option value="Document1">
										Document1
									</option>
									<option value="Document2">
										Document2
									</option>
									<option value="Document3">
										Document3
									</option>
								</select>
								<font color="red">*</font>
							</td>
						</tr>
					
						<tr align="left">
							<td nowrap="nowrap">Upload Doc. Proof</td>
							<td>:</td>
							<td colspan="3"><input type="file" id="orgdocproof"></td>
						</tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                     <tr>
                                       <td>&nbsp;</td>
                                    </tr>
                                     <tr>
                                       <td>&nbsp;</td>
                                    </tr>
                                     <tr>
                                       <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        
                            <td style="width: 50%">
                                <table border="0" cellpadding="0" cellspacing="10" width="100%" class="field">
                                    <tr>
							<td nowrap="nowrap">Country</td>
							<td>:</td>
							<td>
								<select id="ddlCountry" name="ddlCountry" class="text">
									<option selected="true"value="">Select</option>
									<option value="India" >
										India
									</option>
								</select>
								&nbsp;<font color="red">*</font>
							</td>
						</tr>
                                    <tr>
							<td nowrap="nowrap">State</td>
							<td>:</td>
							<td>
								<select id="ddlState" name="ddlState" class="text">
<!--									<option selected="true"value="">Select</option>
									<option value="Rajasthan" >
										Rajasthan
									</option>-->
								</select>
								&nbsp;<font color="red">*</font>
							</td>
						</tr>
						
						<tr>
							<td nowrap="nowrap">Divison</td>
							<td>:</td>
							<td>
								<select id="ddlRegion" name="ddlRegion" class="text">
<!--									<option selected="true"value="">Select</option>
									<option value="Jaipur" >
										Jaipur
									</option>-->
								</select>
								&nbsp;<font color="red">*</font>
							</td>
						</tr>
					
						<tr>
							<td nowrap="nowrap">District</td>
							<td>:</td>
							<td>
								<select id="ddlDistrict" name="ddlDistrict" class="text">
<!--									<option selected="true"value="">Select</option>
									<option value="Jaipur" >
										Jaipur
									</option>-->
								</select>
								&nbsp;<font color="red">*</font>
							</td>
						</tr>
						
						<tr>
							<td nowrap="nowrap">Tehsil</td>
							<td>:</td>
							<td>
								<select id="ddlTehsil" name="ddlTehsil" class="text">
<!--									<option selected="true"value="">Select</option>
									<option value="Sanganer" >
										Sanganer
									</option>-->
								</select>
								&nbsp;<font color="red">*</font>
							</td>
						</tr>
						
						<tr>
							<td nowrap="nowrap">Area</td>
							<td>:</td>
							<td>
								<select id="ddlArea" name="ddlArea" class="text">
									<option selected="true"value="">Select</option>
									<option value="East" >
										East
									</option>
								</select>
								&nbsp;<font color="red">*</font>
							</td>
						</tr>
						
						<tr align="left">
							<td nowrap="nowrap">House No.</td>
							<td>:</td>
							<td colspan="3">
							<input type="text" name="txtHouseno" id="txtHouseno" class="text" maxlength="" /></td>
						</tr>
						
						<tr align="left">
							<td nowrap="nowrap">Street</td>
							<td>:</td>
							<td colspan="3">
							<input type="text" name="txtStreet" id="txtStreet" class="text" maxlength="" /></td>
						</tr>
					
						<tr align="left">
							<td nowrap="nowrap">Road</td>
							<td>:</td>
							<td colspan="3">
							<input type="text"  name="txtRoad" id="txtRoad" class="text" maxlength="" /></td>
						</tr>
					
						<tr align="left">
							<td nowrap="nowrap">Nearest Landmark</td>
							<td>:</td>
							<td colspan="3">
							<input type="text" name="txtLandmark" id="txtLandmark" class="text" maxlength="" /></td>
						</tr>
					
						
                                </table>
                            </td>
                            
                        </tr>
                        <tr>
                            <td colspan="3" align="right">
                                <p class="btn submit"><input type="submit" id="btnSubmit" name="btnSubmit" value="Submit" /></p>
                            </td>
                        </tr>
                        
                    </table>
                    </form>
                    </div>
                    <?php
                    include './include/footer.html';
                    ?>
                    </div>
                    </body>
                    <script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {

                            $(function() {
                                $( "#txtEstdate" ).datepicker({
                                  changeMonth: true,
                                  changeYear: true
                                });
                              });
                              function FillOrgType() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfOrgTypeMaster.php",
                                    data: "action=FILL",
                                    success: function (data) {
                                        $("#txtType").html(data);
                                    }
                                });
                            }
                             FillOrgType();
                              
                            function FillParent() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfCountryMaster.php",
                                    data: "action=FILL",
                                    success: function (data) {
                                        $("#ddlCountry").html(data);
                                    }
                                });
                            }
                             FillParent();
                             
                              $("#ddlCountry").change(function(){
				var selcountry = $(this).val(); 
				//alert(selcountry);
				$.ajax({
			          url: 'common/cfStateMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selcountry + "",
			          success: function(data){
						//alert(data);
						$('#ddlState').html(data);
			          }
			        });
                            });
            
           
                            $("#ddlState").change(function(){
				var selState = $(this).val(); 
				//alert(selState);
				$.ajax({
			          url: 'common/cfRegionMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selState + "",
			          success: function(data){
						//alert(data);
						$('#ddlRegion').html(data);
			          }
			        });
                             });
                            $("#ddlRegion").change(function(){
				var selregion = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfDistrictMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selregion + "",
			          success: function(data){
						//alert(data);
						$('#ddlDistrict').html(data);
			          }
			        });
                            });
                            $("#ddlDistrict").change(function(){
				var selDistrict = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfTehsilMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selDistrict + "",
			          success: function(data){
						//alert(data);
						$('#ddlTehsil').html(data);
			          }
			        });
                            });
                            $("#ddlTehsil").change(function(){
				var selTehsil = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfAreaMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selTehsil + "",
			          success: function(data){
						//alert(data);
						$('#ddlArea').html(data);
			          }
			        });
                            });


                            $("#btnSubmit").click(function () {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                var url = "common/cfOrgDetail.php"; // the script where you handle the form input.
                                var filename=$('#orgdocproof').val(); 
		 	        //alert(filename);    
                               
                                var data;
                                alert(Mode);
                                if (Mode == 'Add')
                                {
                                    
                                    data = "action=ADD&name="+txtName1.value+"&type="+txtType.value+"&regno="+txtRegno.value+
                                            "&course="+txtCourse.value+"&state="+ddlState.value+
                                            "&region="+ddlRegion.value+"&district="+ddlDistrict.value+
                                            "&tehsil="+ddlTehsil.value+"&area="+ddlArea.value+
                                            "&landmark="+txtLandmark.value+"&road="+txtRoad.value+
                                            "&street="+txtStreet.value+"&houseno="+txtHouseno.value+
                                            "&estdate="+txtEstdate.value+"&doctype="+txtDoctype.value+
                                            "&docproof="+filename+"&country="+ddlCountry.value+""; // serializes the form's elements.
                                     alert(data);
                                }
                                else
                                {
                                    data = "action=UPDATE&code=" + OrganizationCode + 
                                            "&name="+txtName1.value+
                                            "&type="+txtType.value+"&regno="+txtRegno.value+
                                            "&course="+txtCourse.value+"&state="+ddlState.value+
                                            "&region="+ddlRegion.value+"&district="+ddlDistrict.value+
                                            "&tehsil="+ddlTehsil.value+"&area="+ddlArea.value+
                                            "&landmark="+txtLandmark.value+"&road="+txtRoad.value+
                                            "&street="+txtStreet.value+"&houseno="+txtHouseno.value+
                                            "&estdate="+txtEstdate.value+"&doctype="+txtDoctype.value+
                                            "&docproof="+filename+"&country="+ddlCountry.value+""; // serializes the form's elements.
                                }

                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                            window.setTimeout(function () {
                                                window.location.href = "frmOrgDetail.php";
                                            }, 1000);

                                            Mode = "Add";
                                            resetForm("frmOrgDetail");
                                        }
                                        else
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                        }
                                        showData();


                                    }
                                });

                                return false; // avoid to execute the actual submit of the form.
                            });
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
                    </html> 