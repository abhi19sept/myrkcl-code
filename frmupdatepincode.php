<?php
$title="IT-GK Pin Code Update";
include ('header.php'); 
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '7') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
echo "<script>var ITGK='" . $_SESSION['User_LoginId'] . "'</script>";
?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
	<div class="container">
		<div class="panel panel-primary" style="margin-top:36px !important;">
			<div class="panel-heading">Update IT-GK Pin Code</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmupdpincode" id="frmupdpincode" class="form-inline" role="form">     

                        <div class="row">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>                           
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">IT-GK Code:</label>
                                <input type="text" class="form-control" readonly="true" maxlength="10" name="itgkcode"
									id="itgkcode"  placeholder="IT-GK Code" required="true">    
                            </div>
							
							 <div class="col-sm-4 form-group">                                  
								<input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details"
									style="margin-top:25px"/>    
							</div>
						
						</div>                   

                      </br>
						<div id="itgkdetails" style="display:none;">
							<div class="row">
                                    <div class="col-sm-3"> 
                                        <label for="edistrict">IT-GK Name:<span class="star">*</span></label>
                                        <input type="text" maxlength="200" class="form-control" name="txtCenterName" id="txtCenterName"
											readonly="true"/>
                                        <input type="hidden" class="form-control" maxlength="10" name="txtusercode" id="txtusercode"/>
                                    </div>
                                    <div class="col-sm-3"> 
                                        <label for="edistrict">District:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="50" name="txtDistrictName"
											id="txtDistrictName" readonly="true"/>                                        
                                    </div>
                                    <div class="col-sm-3"> 
                                        <label for="edistrict">Tehsil:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="100" name="txtTehsilName" id="txtTehsilName"
											readonly="true"/>                                       
                                    </div>
                                    <div class="col-sm-3"> 
                                        <label for="edistrict">Mobile:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="500" name="txtMobile" id="txtMobile" 
											readonly="true"/>
                                    </div>
                            </div></br>
							<div class="row">
									<div class="col-sm-3"> 
                                        <label for="edistrict">Address:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="500" name="txtAddress" id="txtAddress" 
											readonly="true"/>
                                    </div>
									
									<div class="col-sm-3"> 
                                        <label for="edistrict">Email:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="500" name="txtEmail" id="txtEmail" 
											readonly="true"/>
                                    </div>
									
									<div class="col-sm-3"> 
                                        <label for="edistrict">Pin Code:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="500" name="txtPincode" id="txtPincode" 
											readonly="true"/>
                                    </div>
									
							</div></br>
							<div class="container">
								<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary"
									value="Click here to Update Pin Code"/>    
                        </div>
						</div>
					</form>
				</div>
						
		</div>   
	</div>


<div class="modal" id="myModal">
    <div class="modal-content">
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal">×</button>
			<h3 id="heading-tittle" class="modal-title">Heading</h3>
		</div>
		<div class="modal-body">
			    <form name="frmupdpincodemodal" id="frmupdpincodemodal" class="form-inline" role="form"
						enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="responses"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="event">Enter Pin Code:<span class="star">*</span></label>
                                <input type="text" id="txtPinCode" name="txtPinCode" maxlength="6" minlength="6" onkeypress="javascript:return allownumbers(event);" class="form-control" required="true" placeholder="Enter Pin Code">
								<input type="hidden" name="orgusercode" id="orgusercode" class="form-control"/>
                            </div>						
						</div>                   

                        <div class="container">
							<input type="submit" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Update"/>    
                        </div>						
						
						
					</form>
		</div>
		</div>
</div>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
 .star{
	color:red;
}
</style>
<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
            $("#itgkcode").val(ITGK);
            
            $("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            if(itgkcode.value == ''){
              BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>&nbsp; Something went wrong to Proceed.</span>");  
            }
            $.ajax({
                type: "post",
                url: "common/cfUpdatePinCode.php",
                data: "action=GetDetails&values=" + itgkcode.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Center Code" + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        txtCenterName.value = data[0].orgname;
                        txtusercode.value = data[0].usercode;
                        txtDistrictName.value = data[0].districtname;
                        txtTehsilName.value = data[0].tehsilname;
                        txtAddress.value = data[0].address;
                        txtEmail.value = data[0].email;
                        txtPincode.value = data[0].pin;
                        txtMobile.value = data[0].mobile;
						$("#itgkdetails").show();
                        $('#itgkcode').attr('readonly', true);
                        $("#btnShow").hide();

                    }

                }
            });
        });
			
			$("#btnSubmit").click(function (){				
					var usercode = $("#txtusercode").val();					
					var modal = document.getElementById('myModal');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() {
						$("#orgusercode").val("");
						$("#txtPinCode").val("");
						modal.style.display = "none";						
					}
						$("#orgusercode").val(usercode);
						
					$("#heading-tittle").html('Update Pin Code');
				});

            
		$("#btnSubmit1").click(function () {
			if ($("#frmupdpincodemodal").valid())
			{				
                $('#responses').empty();
                $('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfUpdatePinCode.php"; // the script where you handle the form input.
                var data;               
                data = "action=Update&pincode=" + txtPinCode.value + "&usercode=" + orgusercode.value + ""; 
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#responses').empty();
                            $('#responses').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmupdatepincode.php";
                           }, 1000);

                            Mode="Add";
                            resetForm("frmupdpincodemodal");
                        }
                        else
                        {
                            $('#responses').empty();
                            $('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();
                    }
                });
			}
                return false; // avoid to execute the actual submit of the form.
            });
			
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>