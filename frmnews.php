<?php
$title="Government Entry Form";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
		if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '4') {	
 ?>
<div style="min-height:430px !important;max-height:1500px !important;">
        <div class="container"> 
			

            <div class="panel panel-primary" style="margin-top:36px;">

                <div class="panel-heading">News</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Set icons:</label>
                                <select id="icons" name="icons" class="form-control" >
								  <option value="" selected="selected">Please Select</option>
								    <option value="1">1</option>
									<option value="1">1</option>
                                </select>    
                            </div>

                        </div>  

                     
                        
                        <div class="container">
                           <div class="col-sm-3 form-group"> 
                                <label for="ifsc">News:</label>
                                <textarea class="form-control" rows="3" id="News" name="News" placeholder="Add News"></textarea>
                            </div> 
                            
                            
					</div>
					<div class="container">
                           <div class="col-sm-3 form-group"> 
                                <label for="ifsc">News Link:</label>
                                <textarea class="form-control" rows="3" id="Link" name="Link" placeholder="Add News link"></textarea>
                            </div> 
							 
                            
                            
					</div>
					
					
                          

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
					<div id="gird"></div>
					

                </div>
            </div>   
        </div>


    </form>
	
</div>



</body>
<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function Fillicons() {
            $.ajax({
                type: "post",
                url: "common/cfnews.php",
                data: "action=FILL",
                success: function (data) {
                    $("#icons").html(data);
                }
            });
        }
        Fillicons();

        

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfnews.php",
                data: "action=DELETE&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmnews.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmnews");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfnews.php",
                data: "action=EDIT&values=" + Code + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    icons.value = data[0].value;
                    News.value = data[0].news;
					 Link.value = data[0].link;
                    //alert($.parseJSON(data)[0]['StatusName']);
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfnews.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);

                }
            });
        }

        showData();		
		
        $("#btnSubmit").click(function () {
			
			
			
     
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfnews.php"; // the script where you handle the form input.
            var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&icons=" + icons.value + "&News=" + News.value +  "&Link=" + Link.value +
					    ""; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + Code + "&icons=" + icons.value + "&News=" + News.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmnews.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmnews");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmgoventry_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>
<?php include ('footer.php'); ?>
</html>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>