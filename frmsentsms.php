<?php
$title = "SMS Sent";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var CourseBatch=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var CourseBatch=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">SMS for Biometric Enrollment </div>

            <div class="panel-body">           
                <form name="frmsmsmodule" id="frmsmsmodule" class="form-inline" role="form" method="post" > 
                    <div class="container" style="width:100%;">

                        <div id="response"></div>


                        <div id="errorBox"></div>


                        <div class="container">
                            <div class="col-sm-10 form-group"> 
                                <label for="course">Select Recipient:<span class="star">*</span></label>
                                <select id="ddlCategory" name="ddlCategory" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Category')" onchange="try {
                                            setCustomValidity('')
                                        } catch (e) {
                                        }"> 
                                    <option value="">Please Select</option>
                                    <option value="1">ITGK</option>
                                    <option value="2">Learner</option>
                                    <option value="3">SP</option>
                                </select>
                            </div> 
                        </div>                          

                        <div class="container">
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                        onchange="try {
                                                    setCustomValidity('')
                                                } catch (e) {
                                                }">  </select>
                            </div> 

                            <div class="col-md-6 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                        onchange="try {
                                                    setCustomValidity('')
                                                } catch (e) {
                                                }"> </select>
                            </div>
                        </div>
                        <div class="container" id="itgkmsg" style="display: none">
                            <div class="col-md-12 form-group"> 
                                <label for="course">Type Message: <span class="star">*</span></label>
                                <textarea rows="4" cols="60"  id='smstextmessage' name='smstextmessage' placeholder='Enter Message' type='text' required/></textarea>
                            </div> 
                        </div>

                        <div class="container">                       
                            <input type="button" name="btn-report-submit" id="btn-report-submit" class="btn btn-primary"
                                   value="View Details"/>			
                        </div>
                        <div class="container" id="buttonpmsgcontainer" style="display: none">                       
                            <input type="button" name="buttonpmsg" id="buttonpmsg" class="btn btn-primary"
                                   value="Send Message2"/>			
                        </div>
                </form> 
                <div id="grid" style="margin-top:35px;"> </div>
            </div>
        </div>   
    </div>


</div>
<?php
include ('footer.php');
include'common/message.php';
?>
</body>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfSentSMS.php",
                data: "action=FILLCourse",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCategory").change(function () {
            var Category = $(this).val();
            if (Category == "1" || Category == "3") {
                //    alert();
                $("#itgkmsg").show();
                //  $("#buttonpmsgcontainer").show();
            } else {
                $("#itgkmsg").hide();
                // $("#buttonpmsgcontainer").hide();
            }
        });

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfSentSMS.php",
                data: "action=FILLBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });
        });

        $("#btn-report-submit").click(function () {
//            if ($("#frmsmsmodule").valid())
//            {
            $('#response').empty();
            $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
            var forminput = $("#frmsmsmodule").serialize();
            var data;
            data = "action=VIEWREPORT&" + forminput;
            $.ajax({
                url: "common/cfSentSMS.php",
                type: "POST",
                data: data,
                success: function (data)
                { //alert(data);
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

//                    window.setTimeout(function () {
//                        var Category = $("#ddlCategory").val();
//                        if (Category == "1" || Category == "3") {
//                            var r = confirm("Click Yes to send Personalize SMS");
//
//                            if (r == true) {
//                                $("#buttonpmsg").click();
//
//                            }// alert("sds"); 
//                        }
//                    }, 3000);


                }
            });
            //   }
            return false;
        });
        $("#grid").on("click", "#sendsmsbtnpop", function () {
            var modal = document.getElementById('myModaldesc');
            var span = document.getElementsByClassName("close")[0];
            modal.style.display = "block";
            span.onclick = function () {
                modal.style.display = "none";
            }
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        });

        $("#grid").on("click", "#sendsmsbtn", function () {
            $('#responsesms').empty();
            $('#responsesms').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
            $.ajax({
                type: "post",
                url: "common/cfSentSMS.php",
                data: $("#sendsmsForm").serialize(),
                success: function (data) {
                    if (data == "Success") {
                        $("#smstextmessage").val("");
                        $('#responsesms').empty();
                        $('#responsesms').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>Messages Send Successfully..</span></div>");
                    } else {
                        $('#responsesms').empty();
                        $('#responsesms').append("<div class='alert-success'><span><img src=images/warning.jpg width=10px /></span><span>Error To Send Messages.</span></div>");
                    }

                }
            });
        });


        $("#buttonpmsg").click(function () {
            var gupshupfilename = $("#gupshupfilename").val();
            // alert(gupshupfilename);
//            if ($("#frmsmsmodule").valid())
//            {
            $('#response').empty();
            $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
            var forminput = $("#frmsmsmodule").serialize();
            var data;
            data = "action=sendpmsgitgk&gupshupfilepath=" + gupshupfilename + "";
            $.ajax({
                url: "common/cfSentSMS.php",
                type: "POST",
                data: data,
                success: function (data)
                {

                    $('#response').empty();
                    $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");

                }

            });

            return false;
        });
          $("#grid").on("click", "#sendpsmsbtn", function () {
        //$("#sendpsmsbtn").click(function () {
            var gupshupfilename = $("#gupshupfilename").val();
           alert(gupshupfilename);
//            if ($("#frmsmsmodule").valid())
//            {
            $('#response').empty();
            $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
            var forminput = $("#frmsmsmodule").serialize();
            var data;
            data = "action=sendpmsgitgk&gupshupfilepath=" + gupshupfilename + "";
            $.ajax({
                url: "common/cfSentSMS.php",
                type: "POST",
                data: data,
                success: function (data)
                {

                    $('#response').empty();
                    $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");

                }

            });

            return false;
        });

    });
</script>

</html>