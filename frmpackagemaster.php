<?php
$title="SMS Module";
include ('header.php'); 
include ('root_menu.php'); 

    if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
				echo "<script>var id=" . $_REQUEST['id'] . "</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            if ($_SESSION['User_Code'] == '1') {	
            ?>
<div style="min-height:450px !important;max-height:2500px !important">
        <div class="container"> 
			 

            <div class="panel panel-primary" style="margin-top:46px !important;">

                <div class="panel-heading">Package Master</div>
                <div class="panel-body">
                    
                    <form name="frmpackagemaster" id="frmpackagemaster" class="form-inline">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            

                            <div class="col-sm-4 form-group"  > 
                                <label for="edistrict">No of Package:<font color="red">*</font></label>
								
								<input type="text" class="form-control" maxlength="50" name="txtPackage" id="txtPackage" onkeypress="javascript:return allownumbers(event);" >
                                
                            </div>
							
							
							<div class="col-sm-4 form-group"  > 
                                <label for="edistrict">Lable:<font color="red">*</font></label>
								
								<input type="text" class="form-control" maxlength="50" name="txtlable" id="txtlable" onkeypress="javascript:return validAddress(event);" >
                                
                            </div>
							
							
							
							
							 <div class="col-sm-4 form-group"  > 
                                <label for="edistrict">Price:<font color="red">*</font></label>
								
								<input type="text" class="form-control" maxlength="50" name="txtprice" id="txtprice" onkeypress="javascript:return allownumbers(event);">
                                
                            </div>



                            
							
							
					</div>
					
					
					

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style='margin-left:10px'/>    
                        </div>
						
						
						<div id="gird"></div>
						</div>
						
						
						
                 </div>
            </div>   
        </div>


    </form>

</div>



</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>
 <script type="text/javascript">
						var SuccessfullySend = "<?php echo Message::SuccessfullySend ?>";
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
						
						
						
						
						if (Mode == 'UPDATE')
						{
							if (confirm("Do You Want To Active This Item ?"))
							{
								ActiveRecord();
							}
						}
						
						
						function ActiveRecord()
						{
							$('#response').empty();
							$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
							$.ajax({
								type: "post",
								url: "common/cfpackagemaster.php",
								data: "action=UPDATE&Status=" + Code + "&id=" + id +"",
								success: function (data) {
									//alert(data);
									if (data == SuccessfullyUpdate)
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
										window.setTimeout(function () {
										   window.location.href="frmpackagemaster.php";
									   }, 1000);
										
										Mode="Add";
										resetForm("frmpackagemaster");
									}
									else
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
									}
									showData();
								}
							});
						}

						
						
						
						
						
						
						
						function showData() {
						$.ajax({
							type: "post",
							url: "common/cfpackagemaster.php",
							data: "action=SHOW",
							success: function (data) {

								$("#gird").html(data);
								 $('#example').DataTable({
								dom: 'Bfrtip',
								buttons: [
									'copy', 'csv', 'excel', 'pdf', 'print'
								]
							});
								

							}
						});
					}
					showData();
					$("#btnSubmit").click(function () {
					if ($("#frmpackagemaster").valid())
					{
				
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						var url = "common/cfpackagemaster.php"; // the script where you handle the form input.
			
						var data;
						var forminput=$(frmpackagemaster).serialize();
						if (Mode == 'Add')
						{
							data = "action=ADD&" +forminput;
						}
						else
						{
					
							data = "action=UPDATE&code=" + OrganizationCode +"&" + forminput;
							//data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
						}
						$.ajax({
						type: "POST",
						url: url,
						data: data,
						success: function (data)
						{
							if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
							{
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
								window.setTimeout(function () {
                                window.location.href = "frmpackagemaster.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmpackagemaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


						}
                });
			 }
                return false; // avoid to execute the actual submit of the form.
            });

                            
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmpackagemaster_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>