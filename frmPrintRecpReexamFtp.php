<?php
$title = "Re-exam Print Invoice";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading"> Re Exam Learner Data Invoice</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="frmreexamstatus" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                    <div class="container">
                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div> 
                    </div>

                    <div id="grid" name="grid" style="margin-top:35px;"> </div>
                    <div class="container">
                    <iframe id="testdoc" src="" style="width: 100%;height: 500px;border: none; display: none;"></iframe>
                    </div>  


            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillEvent() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfEventMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlExamEvent").html(data);
                }
            });
        }
        FillEvent();
    });
    $("#ddlExamEvent").change(function () {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

        $.ajax({
            type: "post",
            url: "common/cfPrintRecpReexamFtp.php",
            data: "action=schedule&examevent=" + ddlExamEvent.value + "",
            success: function (data)
            {
                $('#response').empty();
                $("#grid").html(data);
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ],
                    scrollY: 400,
                    scrollCollapse: true,
                    paging: false

                });

            }
        });
    });
    $("#grid").on('click', '.approvalLetter', function () {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");


        var eid = $(this).attr('eid');

        $.ajax({
            type: "post",
            url: "common/cfPrintRecpReexamFtp.php",
            data: "action=DwnldPrintRecp&eid=" + eid + "",
            success: function (data) {
             
                $('#response').empty();

                window.open(data, '_blank');
                    if(data == 'error'){
                    $('#response').empty();
                    $('#response').append("<p class='error'>Receipt Not Available Right Now.</span></p>");
                    }
                    else{
                        $("#testdoc").attr('src', data);
                    }
               // window.setTimeout(function () {
               //     delgeninvoice(data);
               // }, 30000);

            }
        });
    });

    function delgeninvoice(data) {
        
        $.ajax({
            type: "post",
            url: "common/cfPrintRecpReexamFtp.php",
            data: "action=delgeninvoice&values=" + data,
            success: function (data) {

            }
        });
    }
    function resetForm(formid) {
        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
    }


</script>
</html>