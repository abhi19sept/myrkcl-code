<?php
$title = "Payment Transection audit Report";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var UserParentID=" . $_SESSION['User_ParentId'] . "</script>";
    echo "<script>var UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var UserParentID=0</script>";
    echo "<script>var UserRole=0</script>";
}
echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Payment Trasaction Receipt</div>
            <div class="panel-body">

                <form name="frmtransactionreceipt" id="frmtransactionreceipt" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <label for="transaction-type"> Transaction ID:<span class="star">*</span></label>
                                    <input type="text" name="transaction_id" id="transaction-id" />
                             </div>
                             
                             <div class="col-md-4 form-group"><br />     
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Get Receipt"/>
                             </div>
                        </div>
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        

                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

		$("#btnSubmit").click(function () {
            showData();
        });
		
		function showData() {
            $('#response').empty();
            $('#response').html("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var forminput = $("#frmtransactionreceipt").serialize();
            var url = "common/cftransectionreceipt.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=GETDATA&" + forminput; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    
                    $('#response').empty();

                    $("#grid").html(data);

                }
            });
        }
		function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmmediumwisecount.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
