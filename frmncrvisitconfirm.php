<?php
$title = "NCR Visit Confirm";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var ItPeripheralsCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var DeviceCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
echo "<script>var UserRoll='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:1600px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading"> NCR Visit Confirm

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
                <form name="frmOrgFinal" id="frmOrgFinal" class="form-inline" action=""> 

                    <br>

                    <div style="margin-bottom: 25px;" id="gird"></div>
                </form>

            </div>

        </div>  
    </div>

    <div id="confirm_visit_form" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="width: 100%;height: 500px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Confirm visit</h4>
                </div>
                <div class="modal-body">
                    <form name="form" id="frmvisitconfirm" class="form-inline" role="form" enctype="multipart/form-data">
                        <div id="response1"></div>
                        <div class="container">
                            <div style="display:none;">

                                <div style="display:none;">
                                    <div class="container">
                                        <div class="col-md-4 form-group">
                                            <label for="batch"> Quality:<span class="star">*</span></label>
                                            <input type="text" name="txtQuality" id="txtQuality" value="" class="form-control"
                                                   readonly="true"/>
                                            <input type="hidden" name="admission_code" id="admission_code"
                                                   value="<?php echo $_REQUEST['code']; ?>">
                                            <input type="hidden" name="txtLearnerName" id="txtLearnerName"/>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <label for="batch"> NFIQ:<span class="star">*</span></label>
                                            <input type="text" name="txtNFIQ" id="txtNFIQ" value="" class="form-control"
                                                   readonly="true"/>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <div class="col-md-10 ">
                                            <label for="batch"> Base64Encoded ISO Template:<span class="star">*</span></label>
                                            <textarea id="txtIsoTemplate" rows="4" cols="12" name="txtIsoTemplate"
                                                      class="form-control" readonly="true"></textarea>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <div class="col-md-10">
                                            <label for="batch"> Base64Encoded ISO Image:<span class="star">*</span></label>
                                            <textarea id="txtIsoImage" rows="4" cols="12" name="txtIsoImage" class="form-control"
                                                      readonly="true"></textarea>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <div class="col-md-10">
                                            <label for="batch"> Base64Encoded Raw Data:<span class="star">*</span></label>
                                            <textarea id="txtRawData" rows="4" cols="12" name="txtRawData" class="form-control"
                                                      readonly="true"></textarea>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <div class="col-md-10">
                                            <label for="batch"> Base64Encoded Wsq Image Data:<span class="star">*</span></label>
                                            <textarea id="txtWsqData" rows="4" cols="12" name="txtWsqData" class="form-control"
                                                      readonly="true"></textarea>
                                        </div>
                                    </div>
                                    <textarea id="txtlearnercode" rows="4" cols="12" name="txtlearnercode" class="form-control"
                                              readonly="true"></textarea>

                                </div>

                                <div class="container">
                                    <div class="col-md-4 form-group">
                                        <label for="batch"> Serial No:<span class="star">*</span></label>
                                        <input type="text" name="tdSerial" id="tdSerial" class="form-control" readonly="true"/>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="batch"> Make:<span class="star">*</span></label>
                                        <input type="text" name="tdMake" id="tdMake" class="form-control" readonly="true"/>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="batch"> Model:<span class="star">*</span></label>
                                        <input type="text" name="tdModel" id="tdModel" class="form-control" readonly="true"/>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="batch"> Width:<span class="star">*</span></label>
                                        <input type="text" name="tdWidth" id="tdWidth" class="form-control" readonly="true"/>
                                    </div>
                                </div>

                                <div class="container">
                                    <div class="col-md-4 form-group">
                                        <label for="batch"> Height:<span class="star">*</span></label>
                                        <input type="text" name="tdHeight" id="tdHeight" class="form-control" readonly="true"/>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="batch"> Local MAC:<span class="star">*</span></label>
                                        <input type="text" name="tdLocalMac" id="tdLocalMac" class="form-control"
                                               readonly="true"/>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="batch"> Local IP:<span class="star">*</span></label>
                                        <input type="text" name="tdLocalIP" id="tdLocalIP" class="form-control"
                                               readonly="true"/>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="batch"> Status:<span class="star">*</span></label>
                                        <input type="text" name="txtStatus" id="txtStatus" class="form-control"
                                               readonly="true"/>
                                    </div>
                                </div>

                            </div>
                        </div> 
                        <!--<div id="visit-grid" name="visit-grid" class="small" style="margin-top:35px;"> </div>-->

                        <div class="container">
                            <div class="col-md-4 form-group">
                                <label for="deviceType">Select Bio-Metric Device :<span class="star">*</span></label>
                                <select name="deviceType" id="deviceType" class="form-control biomval">
                                    <option value="0">- - - Please Select - - -</option>
                                    <option value="m">Mantra</option>
                                    <option value="t">Tatvik</option>
                                </select>
                                <span class="star bioerr"></span>
                                <input type="hidden" name="txtid" id="txtid"/>
<!--                                <input type="hidden" class="biomval" name="flag" id="flag"/>
                           <input type="hidden" class="biomval" name="txtStatus" id="txtStatus"/>
                           <input type="hidden" class="biomval" name="tdLocalIP" id="tdLocalIP"/>
                           <input type="hidden" class="biomval" name="tdLocalMac" id="tdLocalMac"/>
                           <input type="hidden" class="biomval" name="tdHeight" id="tdHeight"/>
                           <input type="hidden" class="biomval" name="tdWidth" id="tdWidth"/>
                           <input type="hidden" class="biomval" name="tdModel" id="tdModel"/>
                           <input type="hidden" class="biomval" name="tdMake" id="tdMake"/>
                           <input type="text" class="biomval" name="tdSerial" id="tdSerial"/>-->

<!--                                <input type="hidden" name="txtStatus" id="txtStatus"/>
                                <input type="hidden" name="txtQuality" id="txtQuality"/>
                                <input type="hidden" name="txtNFIQ" id="txtNFIQ"/>
                                <input type="hidden" name="txtIsoTemplate" id="txtIsoTemplate"/>
                                <input type="hidden" name="txtIsoImage" id="txtIsoImage"/>
                                <input type="hidden" name="txtRawData" id="txtRawData"/>
                                <input type="hidden" name="txtWsqData" id="txtWsqData"/>-->
                                <!--                                <div style="display:none;">
                                                                    <img id="imgFinger" name="imgFinger" class="form-control" style="width:80px; height:107px;"/>
                                                                </div>-->
                            </div>
                            <div class="col-md-3 form-group" id="ITGKNameDiv">    
                                <label for="deviceType">Select ITGK :<span class="star">*</span></label>
                                <select name="ITGKName" id="ITGKName" class="form-control biomval">
                                </select>
                            </div>
                            <div class="col-sm-3 form-group"  id="nonprintable" style="display:none;"> 
                                <center>
                                    <!--<input type="button" id="itgk_impression" class="itgk_impression" value="Capture (ITGK)" name="itgk_impression" style="margin-top: 25px;" />-->  
                                    <input type="button" name="capture1" id="capture1" class="btn btn-success"
                                           value="Capture (ITGK)"><br />
                                    <img id="imgFinger_itgk" name="imgFinger_itgk" class="form-control" style="width:80px; height:107px;"/><br /><span class="star" id="match_itgk"></span></center>
                                <input type="hidden" name="matched_itgk" id="matched_itgk"/>
                                <div id="successitgk" style="margin-left:55px;"><input type="hidden" name="txtitgksuccess" id="txtitgksuccess"/></div>
                                <div id="failitgk" style="margin-left:55px;"></div>
                            </div>
                            <div class="col-md-3 form-group" id="VisitorNameDiv">    
                                <label for="deviceType">Select Visitor :<span class="star">*</span></label>
                                <select name="VisitorName" id="VisitorName" class="form-control biomval">
                                </select>
                            </div>
                            <div class="col-md-3 form-group" id="CaptureVisitor" style="display:none;">     
                                <!--<input type="button" id="visitor_impression" class="visitor_impression" value="Capture (Visitor)" name="visitor_impression" style="margin-top: 25px;" />--> 
                                <input type="button" name="capture2" id="capture2" class="btn btn-success"
                                       value="Capture (Visitor)"><br />
                                <img id="imgFinger_visitor" name="imgFinger_visitor" class="form-control" style="width:80px; height:107px; margin-left: 20px;"/><br /><span class="star" id="match_visitor"></span>
                                <input type="hidden" name="matched_visitor" id="matched_visitor"/>
                                <div id="successvisitor"><input type="hidden" name="txtvisitorsuccess" id="txtvisitorsuccess"/></div>
                                <div id="failvisitor"></div>
                            </div>

                            <div class="col-md-3 form-group" id="showData">   
                                <input type="button" name="reset" id="reset" class="btn btn-default" value="Reset Form" style="margin-top: 25px; margin-left: -20px;" /><br>
                                <input type="button" name="setConfirmBtn" id="setConfirmBtn" class="btn btn-danger" value="Confirm Visit" style="margin-top: 25px; margin-left: -20px;" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>
<script src="js/mfs100.js" type="text/javascript"></script>
<script src="js/tatvik.js" type="text/javascript"></script>
<script type="text/javascript">
    var localIP = "<?php echo getHostByName(getHostName()); ?>";

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $("#capture1").click(function () {
            getuserdata('ITGK');
        });

        $("#capture2").click(function () {
            getuserdata('VISITOR');
        });
        
         $("#reset").click(function () {
            window.setTimeout(function () {
                                window.location.href = "frmncrvisitconfirm.php";
                            }, 1000);
        });

        var successitgk = "";
        var successvisitor = "";
        $("#setConfirmBtn").click(function () {

            //alert(successitgk);
            //alert(successvisitor);

            if (successitgk == '1' && successvisitor == '1') {

                //alert("Match Successfull Data Saved");
                 $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfNcrVisitConfirm.php"; // the script where you handle the form input.
                var data;
               data = "action=ConfirmVisit&visitid=" + txtid.value + "";


                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {//alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response1').empty();
                            $('#response1').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmncrvisitconfirm.php";
                            }, 1000);

                        }
                        else
                        {
                            $('#response1').empty();
                            $('#response1').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                    }
                });
            } else {
                alert("Kindly Confirm both to Confirm Visit");
            }
        });

        function callAPI(params) {
            var x = deviceType.value;
            //alert(params);
            GetContMachineSrNo(x);

            //var matching1 = GetRegMachineSrNo(x);
            if (x !== "0") {
                switch (x) {
                    case "m":
                        Match(params);
                        break;

                    case "t":
                        MatchTatvik_new(params);
                        break;
                    default:
                        break;
                }
            } else {
                $("#showData").css("display", "none");
            }

        }

        /*** MATCH ON MANTRA MACHINE ***/
        function Match(params) {
            var quality = 60; //(1 to 100) (recommanded minimum 55)
            var timeout = 10;
            try {
                //alert(txtIsoTemplate.value);
                var isotemplate = txtIsoTemplate.value;
                //alert(isotemplate);
                //$("#wait_modal_match").modal("show");
                var res = MatchFinger(quality, timeout, isotemplate);
                var res1 = CaptureFinger(quality, timeout);
               // $("#wait_modal_match").modal("hide");
                 //print_r(jsonencode(res));
//                 alert(JSON.stringify(res1));
//                 console.log(JSON.stringify(res1));
//console.log(res);
                if (res.httpStaus) {
                    document.getElementById('txtStatus').value = "ErrorCode: " + res1.data.ErrorCode + " ErrorDescription: " + res1.data.ErrorDescription;
                    if (res.data.Status) {
                        alert("Finger matched");
                        if (params == 'ITGK') {
                                            $('#successitgk').empty();
                                            $('#failitgk').empty();
                                            $('#successitgk').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "Success" + "</span></font></b></p>");
                                            successitgk = "1";
                                        } else {
                                            $('#successvisitor').empty();
                                            $('#failvisitor').empty();
                                            $('#successvisitor').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "Success" + "</span></font></b></p>");
                                            successvisitor = "1";
                                        }

                    } else {

                        if (res.data.ErrorCode != "0") {
                            //alert("err: "+res.data.ErrorDescription);
                            $("#error_modal_timeout").modal("show");
                        } else {
                            alert("not matched");
                            if (params == 'ITGK') {
                                        $('#successitgk').empty();
                                        $('#failitgk').empty();
                                        $('#failitgk').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "Failed" + "</span></font></b></p>");
                                        successitgk = "0";
                                    } else {
                                        $('#successvisitor').empty();
                                        $('#failvisitor').empty();
                                        $('#failvisitor').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "Failed" + "</span></font></b></p>");
                                        successvisitor = "0";
                                    }
                        }
                    }
                } else {
                    alert(res.err);
                }
            } catch (e) {
                alert(e);
            }
            return false;
        }

        /*** MATCH ON Tatvik MACHINE ***/
        function MatchTatvik_new(params) {
//alert(params);
            crossDomainAjax("https://127.0.0.1:31000", 'CAPTUREFP',
                    function (result) {
                        var ClaimedTemplateData = result.CaptureResult.Image_FMRbytes;

                        var matchtemplates = "<MatchTemplates>" +
                                "<ClaimedTemplateData>" + ClaimedTemplateData + "</ClaimedTemplateData>" +
                                "<ReferenceTemplateData>" + document.getElementById('txtIsoTemplate').value.toString() + "</ReferenceTemplateData>" +
                                "</MatchTemplates>";
                        $.ajax({
                            url: "https://127.0.0.1:31000",

                            data: matchtemplates,
                            method: "MATCHFPTEMPLATE",
                            responsetype: 'xml',
                            // jsonpCallback: "Jsonp_Callback",
                            success: function (data, success) {
                                var rd = xml2json(data);
                                var rd2 = rd.MatchResult;
                                console.log(rd2)
                                if (rd2.MatchStatus == 'Match Failed') {
                                    //alert("no");
                                    $("#error_modal_nomatch").modal("show");
                                    if (params == 'ITGK') {
                                        $('#successitgk').empty();
                                        $('#failitgk').empty();
                                        $('#failitgk').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "Failed" + "</span></font></b></p>");
                                        successitgk = "0";
                                    } else {
                                        $('#successvisitor').empty();
                                        $('#failvisitor').empty();
                                        $('#failvisitor').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "Failed" + "</span></font></b></p>");
                                        successvisitor = "0";
                                    }
                                    window.setTimeout(function () {
                                        $('#response').empty();
                                        $("#btnCapture").hide();
                                        $("#learnerinfo").hide();
                                    }, 3000);
                                } else {
//alert(rd2.MatchStatus);
                                    if (rd2.MatchStatus == "Finger not placed. Time Out!" || rd2.MatchStatus == "Capture Failed! Time Out!")
                                        $("#error_modal_timeout").modal("show");

                                    if (rd2.MatchStatus == "Match succesful") {
                                        //alert("yes");
                                        /*** ALREADY ENROLLED ****/
                                        if (params == 'ITGK') {
                                            $('#successitgk').empty();
                                            $('#failitgk').empty();
                                            $('#successitgk').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "Success" + "</span></font></b></p>");
                                            successitgk = "1";
                                        } else {
                                            $('#successvisitor').empty();
                                            $('#failvisitor').empty();
                                            $('#successvisitor').append("<p class='error'><span><img src=images/correct.gif width=20px /></span><b><font color=green><span>" + "Success" + "</span></font></b></p>");
                                            successvisitor = "1";
                                        }
                                        //SubmitDetails();
                                    }
                                }
                            },
                            error: function (jqXHR, exception) {
                                if (jqXHR.status === 0) {
                                    alert('Not connect.\n Verify Network.');
                                } else if (jqXHR.status == 404) {
                                    alert('Requested page not found. [404]');
                                } else if (jqXHR.status == 500) {
                                    alert('Internal Server Error [500].');
                                } else if (exception === 'parsererror') {
                                    alert('Requested JSON parse failed.');
                                } else if (exception === 'timeout') {
                                    alert('Time out error.');
                                } else if (exception === 'abort') {
                                    alert('Ajax request aborted.');
                                } else {
                                    alert('Uncaught Error.\n' + jqXHR.responseText);
                                }
                            }
                        });

                    });

        }

        var connectmachinesrno = "";
        function GetContMachineSrNo(x) {

            if (x !== "n") {
                switch (x) {
                    case "m":
                        var res = GetMFS100Info();
                        //alert(res.data.ErrorCode);
                        if (res.httpStaus) {

                            if (res.data.ErrorCode == "0") {
                                connectmachinesrno = res.data.DeviceInfo.SerialNo;
                            }
                        }
                        break;

                    case "t":
                        var res = initializeTatvik();
                        connectmachinesrno = $("#tdSerial").val();
                        break;
                    default:
                        break;
                }
            }

        }

        var regmachinesrno = "";
        var matching = "";

        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfNcrVisitConfirm.php",
                data: "action=SHOW",
                success: function (data) {
                    $('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print']
                    });



                }
            });
        }

        showData();
        $("#gird").on('click', '.aslink', function () {
            var edit_id = $(this).attr("id");
            txtid.value = edit_id;
//            $('#match_itgk').empty();
//            $('#match_visitor').empty();
//            $('#matched_itgk').val('');
//            $('#matched_visitor').val('');
//            setDeviceInfoEmpty();
//            performAction(this);
               $.ajax({
                type: "post",
                url: "common/cfNcrVisitConfirm.php",
                data: "action=LoadVisitor&visitid=" + txtid.value + "",
                success: function (data) {
                    if(data == '0'){
                        alert("Something Went Wrong");
                    } else {
                    $('#response').empty();
                    $("#VisitorName").html(data);
                }
                }
            });
            
            $("#VisitorName").change(function () {
            var VisitorId = $(this).val();
            //alert(VisitorId);
            if(VisitorId == '0'){
                alert("Please Select Visitor");
                $("#CaptureVisitor").hide();
            } else {
                $("#VisitorNameDiv").hide();
                $("#CaptureVisitor").show();
            }
        });
        
        $.ajax({
                type: "post",
                url: "common/cfNcrVisitConfirm.php",
                data: "action=LoadITGK&visitid=" + txtid.value + "",
                success: function (data) {
                    //alert(data);
                    if(data == '0'){
                        alert("Something Went Wrong");
                    } else {
                    $('#response').empty();
                    $("#ITGKName").html(data);
                }
                }
            });
            
              $("#ITGKName").change(function () {
            var VisitorId = $(this).val();
            //alert(VisitorId);
            if(VisitorId == '0'){
                alert("Please Select Visitor");
                $("#nonprintable").hide();
            } else {
                $("#ITGKNameDiv").hide();
                $("#nonprintable").show();
            }
        });

            $("#confirm_visit_form").modal("show");
        });

        function getuserdata(params) {
            $('#txtIsoTemplate').val('');
            $('#txtIsoImage').val('');
            $('#txtNFIQ').val('');
            $('#txtRawData').val('');
            $('#txtWsqData').val('');
            $('#txtitgksuccess').val('');
            $('#txtvisitorsuccess').val('');
            var url = "common/cfNcrVisitConfirm.php"; // the script where you handle the form input.
            var data;
            if (params == 'ITGK') {
                var para = UserRoll;
                var id = ITGKName.value;
            } else {
                var para = txtid.value;
                var id = VisitorName.value;
            }
//alert(para);
            data = "action=GetLearnerByPIN&biopin=" + para + "&params=" + params + "&visitorname=" + id + "";
            //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                     if(data =='0'){
                        alert("Please Select Visitor");
                    } if (data == 'invalidpin') {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + " Invalid Bio Metric PIN " + "</span></font></b></p>");
                    } else {
                       
                        data = $.parseJSON(data);
                        //admission_code.value = data[0].AdmissionCode;
                        txtLearnerName.value = data[0].AdmissionCode;
                        txtQuality.value = data[0].quality;
                        txtNFIQ.value = data[0].nfiq;
                        txtIsoTemplate.value = data[0].template;
                        txtIsoImage.value = data[0].image;
                        txtRawData.value = data[0].raw;
                        txtWsqData.value = data[0].wsq;
                        // txtlearnercode.value = data[0].lcode;
//                        document.getElementById("learnercode").innerHTML = data[0].lcode;
//                        document.getElementById("learnername").innerHTML = data[0].lname;
//                        document.getElementById("fathername").innerHTML = data[0].lfname;
//                        document.getElementById("dob").innerHTML = data[0].ldob;
//                        document.getElementById("coursename").innerHTML = data[0].lcourse;
//                        document.getElementById("batchname").innerHTML = data[0].lbatch;
                        // var imsrc = data[0].lphoto;
                        //src="upload/admission_photo/ "
                        //  $("#my_image").attr("src", "upload/admission_photo/" + imsrc);
                        //learnercode.value = data[0].AdmissionCode;
                        callAPI(params);
                        $('#response').empty();
                        $("#learnerinfo").show();

                        if (data[0].attendance_flag == 0) {
                            $("#btnCapture").show();
                        } else {
                            $("#btnCapture").hide();
                            $("#showMessage_ajax").html(data[0].attendance_flag);
                            $("#showMessage").show();
                        }
                    }


                }
            });


        }

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>