<?php
$title="Government Entry Form";
include ('header.php'); 
include ('root_menu.php'); 

 if (isset($_REQUEST['code'])) {
                echo "<script>var DistrictCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var DistrictCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>

        <div class="container"> 
			 

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">District Master</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmdistrictmaster" id="frmdistrictmaster" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">District Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtDistrictName" id="txtDistrictName" placeholder="District Name">
                            </div>


                            




                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Country Name:</label>
                                <select id="ddlCountry" name="ddlCountry" class="form-control" >
								  
                                </select>    
                            </div>
							
							
							
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">State Name:</label>
                                <select id="ddlState" name="ddlState" class="form-control" >
								  
                                </select>    
                            </div>
							
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Region Name:</label>
                                <select id="ddlRegion" name="ddlRegion" class="form-control" >
								  
                                </select>    
                            </div>
							
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">District Status:</label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control" >
								  
                                </select>    
                            </div>

                        </div>  

                       



                       

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
						
						
                </div>
				<div id="gird"></div>
            </div>   
        </div>


    </form>




</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
            
            function FillParent() {
                $.ajax({
                    type: "post",
                    url: "common/cfCountryMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlCountry").html(data);
                    }
                });
            }

            FillParent();
            
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfDistrictMaster.php",
                    data: "action=DELETE&values=" + DistrictCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                            }, 3000);
                            Mode="Add";
                            resetForm("frmDistrictMaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfDistrictMaster.php",
                    data: "action=EDIT&values=" + DistrictCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Status']);
                        alert(data);
                        data = $.parseJSON(data);
                        txtDistrictName.value = data[0].DistrictName;
                        ddlRegion.value=data[0].Region;
                        ddlStatus.value = data[0].Status;
                        
                    }
                });
            }

            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfDistrictMaster.php",
                    data: "action=SHOW",
                    success: function (data) {
                            
                        $("#gird").html(data);

                    }
                });
            }

            showData();
            $("#ddlCountry").change(function(){
				var selcountry = $(this).val(); 
				//alert(selcountry);
				$.ajax({
			          url: 'common/cfStateMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selcountry + "",
			          success: function(data){
						//alert(data);
						$('#ddlState').html(data);
			          }
			        });
                showState(29);
                });
            
           
            $("#ddlState").change(function(){
				var selState = $(this).val(); 
			//alert(selState);
				showState(selState);
            });



            $("#btnSubmit").click(function () {
			if ($("#frmdistrictmaster").valid())
			{
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfDistrictMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtDistrictName.value + "&parent=" + ddlCountry.value + "&state=" + ddlState.value + "&region=" + ddlRegion.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + DistrictCode + "&name=" + txtDistrictName.value + "&parent=" + ddlCountry.value + "&state=" + ddlState.value + "&region=" + ddlRegion.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmdistrictmaster.php";
                           }, 1000);

                            Mode="Add";
                            resetForm("frmdistrictmaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			}
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

            function showState(selState) {
                $.ajax({
                  url: 'common/cfRegionMaster.php',
                  type: "post",
                  data: "action=FILL&values=" + selState + "",
                  success: function(data){
                    //alert(data);
                    $('#ddlRegion').html(data);
                  }
                });
            }

        });

    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmdistrictmastervalidation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
 .star{
	color:red;
}
</style>

</html>