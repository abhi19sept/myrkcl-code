<?php
$title = "Rating Data Generate";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '8'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Rating Data Generate</div>
            <div class="panel-body">

                <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Parameter:<span class="star">*</span></label>
                                <select id="ddlParameter" name="ddlParameter" class="form-control">

                                </select>
                            </div> 

                            <div class="col-md-4 form-group" id="batch" style="display:none;">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                              
								</select>
                            </div>
							
							<div class="col-sm-4 form-group" id="event" style="display:none;"> 
								<label for="designation">Select Exam Event:<span class="star">*</span></label>
								<select id="ddlExamEvent" name="ddlExamEvent" class="form-control">                                  
								</select>
							</div>

                        </div>                  

                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary"
									value="Submit" style="display:none;"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillParameters() {            
            $.ajax({
                type: "post",
                url: "common/cfRatingTempDataGenerate.php",
                data: "action=FillRatingParameters",
                success: function (data) {
					$("#ddlParameter").html(data);					
                }
            });
        }
        FillParameters();

        $("#ddlParameter").change(function () {
			$("#batch").show();
            var selParameter = $(this).val();
           $.ajax({
                type: "post",
                url: "common/cfRatingTempDataGenerate.php",
                data: "action=FillBatchforRating",
                success: function (data) {
                    $("#ddlBatch").html(data);						
                }
            });

        });
		
		$("#ddlBatch").change(function (){
			var parametervalue = $("#ddlParameter").val();
				if(parametervalue=='11' || parametervalue=='6'){
						FillExamEvent();
						$("#event").show();
				}
				else{
					$("#btnSubmit").show();
				}
		});
		
		function FillExamEvent() {            
            $.ajax({
                type: "post",
                url: "common/cfRatingTempDataGenerate.php",
                data: "action=FillExamEvent",
                success: function (data) {					
                    $("#ddlExamEvent").html(data);					
                }
            });
        }
		
		$("#ddlExamEvent").change(function (){
			$("#btnSubmit").show();
		});


        $("#btnSubmit").click(function () {
			$('#response').empty();
				$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
					var parameter=$( "#ddlParameter" ).val();
					var batch=$( "#ddlBatch" ).val();
					var events=0;
						if (parameter==''){
							alert("Please select Rating Parameter");
							$('#response').empty();
						}
						if (batch==''){
							alert("Please select batch for Rating");
							$('#response').empty();
						}
						if(parameter=='11' || parameter=='6'){
							var event=$( "#ddlExamEvent" ).val();
							if (event==''){
								alert("Please select Exam Event");
								$('#response').empty();
							}
							else{
								events=$( "#ddlExamEvent" ).val();
							}
						}
					var url = "common/cfRatingTempDataGenerate.php"; // the script where you handle the form input.
					var data;
					data = "action=ADD&parameter=" + parameter + "&batch=" + batch + "&event=" + events +""; 
							// serializes the form's elements.
							
					$.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                      if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmratingtempdatagenerate.php';
                            }, 3000);
                        }
						else if(data=='something'){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Something went wrong. Please try again.</span></div>");
						}
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                        }
                    }
                });
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
