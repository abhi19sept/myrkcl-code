<?php include ('frmwcdheader.php'); 
    session_destroy(); 
?>

    <link rel="stylesheet" href="css/datepicker.css">
    <script src="scripts/datepicker.js"></script>    

    <div class="mainblog">
        <div class="container">
            <div class="row">
               
                <div class="col-lg-3 col-md-3" style="text-align: center;"><img src="images/cm_pics.png" alt="#" height="220px"></div>
                <div class="col-lg-6 col-md-6 marginLR">
                   <!-- <P class="KrutiDev_hindi_text cont">efgykvksa@ckfydkvksa gsrq fu’kqYd daI;wVj 
izf’k{k.k ds fy;s vkWuykbu vkosnu QkeZ</P> -->
                     <center>  <span class="KrutiDev_hindi_text cont"> bafnjk efgyk 'kfDr izf'k{k.k o dkS'ky lao/kZu ;kstuk 
<br> <hr style="border-top: 2px solid #000;">efgykvksa@ckfydkvksa dks fu%'kqYd </span> <span class="cont" style="font-size:23px;"> RS-CIT / RS-CFA </span> <span class="KrutiDev_hindi_text cont"> izf'k{k.k ds fy;s vkWuykbu vkosnu QkeZ</span> </center>
                </div>
                <div class="col-lg-2 col-md-2" style="text-align: center;"><img src="images/mamta_pic.png" alt="#" height="210px"></div>
           </div>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 imporlink leftlink">
                    <h2>egRoiw.kZ fyad</h2>
                    <ul>
                       <li>
                            <span><img src="images/icon.png"></span> 
                            <a href="https://wcd.rajasthan.gov.in/content/dam/wcd-cms/dwe/schemes/RSCIT-Circular.pdf" target="_blank"><p>
								<b style='font-family: sans-serif;font-size: 15px;font-weight: normal;'>RS-CIT </b>
							;kstuk dk fooj.k tkuus gsrw ;gkWa fDyd djsa</p></a>
                        </li>
						 <li>
                            <span><img src="images/icon.png"></span> 
                            <a href="https://wcd.rajasthan.gov.in/content/dam/wcd-cms/dwe/schemes/RSCFA-Circular.pdf" target="_blank"><p>
								<b style='font-family: sans-serif;font-size: 15px;font-weight: normal;'>RS-CFA </b>
							;kstuk dk fooj.k tkuus gsrw ;gkWa fDyd djsa</p></a>
                        </li>
                        <li>
                            <span><img src="images/icon.png"></span> 
                            <a href="./upload/wcd/WCD_Application Form.pdf" target="_blank"><p>vkosnu QkeZ MkmuyksM djsa</p></a>
                        </li>
                        <li>
                            <span><img src="images/icon.png"></span> 
                            <a href="./upload/wcd/Guide to Fill Online Application Form.pdf" target="_blank"><p>vkWuykbu vkosnu djus gsrq t:jh fn’kk funsZ’k</p></a>
                        </li>
                         <li>
                            <span><img src="images/icon.png"></span> 
                            <a href="./upload/wcd/Final Scheme Advertisement WCD 2020.pdf" target="_blank"><p>
								U;wtisij esa izdkf’kr foKkiu dh izfr MkmuyksM djsa</p></a>
                        </li>
						<li>
                            <span><img src="images/icon.png"></span> 
                            <a href="./upload/wcd/For WCD 06012021 2 pm Final Eligible RSCIT IT-GK List.pdf" target="_blank"><p>;kstuk gsrq p;fur <b style='font-family: sans-serif;font-size: 15px;font-weight: normal;'>RS-CIT</b> vkbZVhthds dh lwph</p></a>
                        </li>
						<li>
                            <span><img src="images/icon.png"></span> 
                            <a href="./upload/wcd/For WCD 06012021 2 pm Final Eligible RSCFA IT-GK List.pdf" target="_blank"><p>;kstuk gsrq p;fur <b style='font-family: sans-serif;font-size: 15px;font-weight: normal;'>RS-CFA</b>
							vkbZVhthds dh lwph</p></a>
                        </li>
                   </ul>
                </div>
                
                <div class="col-lg-6 col-md-6 marginL50">
                    <div class="panel panel-default" style="margin:0px;">
            <div class="panel-heading"  style="
                 background:  #10bbef none repeat scroll 0 0 !important;color:#fff;" > 
				<b>
					<div style="text-align:center;"> आवेदन के समय पंजीकृत मोबाइल पर प्राप्त Application ID (एप्लीकेशन आई. डी.) या अन्य विवरण नीचे दिए गए स्थान पर प्रविष्ट करें व भरा हुआ आवेदन पत्र डाउनलोड करें |  </div>
				</b>

            </div>
            <div class="panel-body" style="padding: 5px 15px;">
                <form class="form-horizontal" role="form"  id="frmwcdinfo" name="frmwcdinfo">
                    <div id="response"></div>


                    <div id="entermobile">
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Application ID<span class="star">*</span>:</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="txtApplicationId"   onkeypress="javascript:return allownumbers(event);" name="txtApplicationId" maxlength="18" placeholder="Application Id" type="text">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <center><b>OR</b></center>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Applicant Name<span class="star">*</span>:</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="txtApplicantName" name="txtApplicantName" data-mask="charonly" placeholder="Applicant Name" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Father Name<span class="star">*</span>:</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="txtApplicantFName" name="txtApplicantFName" data-mask="charonly" placeholder="Father Name" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label">Date of Birth<span class="star">*</span>:</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="txtApplicantDOB" name="txtApplicantDOB" readonly="true" maxlength="50" placeholder="DD-MM-YYYY" type="text">
                            </div>
                        </div>
                        <div class="form-group">&nbsp;</div>
                        <div id="mobi" class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                
                                <button type="button" name="btnSubmit"  id="btnSubmit" class="btn btn-success btn-sm">Download</button>
                                <button type="reset" class="btn btn-default btn-sm">Reset</button>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
</div>                
                <div class="col-lg-3 col-md-3 imporlink rightlink">
                    <ul>
                        <li>
                          <a href="#"><p>vkWuykbu vkosnu vkjEHk gksus dh frfFk %& <br /><span class="hindidt"></span></p></a>
                        </li>
                        <li>&nbsp;</li>
                        <li>
                           <a href="#"><p>vkWuykbu vkosnu dh vafre frfFk %& <br /><span class="hindidt">31/01/2021
						   </span></p></a>
                        </li>
                        <li>
                            <h4><center><a href="frmdownloadapplication.php"><u>भरे गए आवेदन पत्र की प्रति यहाँ से डाउनलोड करें </u></a></center></h4>
                        </li>
                    </ul>
                </div>
                 
                
                
                
            </div>
        </div>
    </div>
            </div>
        <div class="footer" style="margin-top: 20px; margin-left:18px;">efgyk vf/kdkfjrk foHkkx jktLFkku ljdkj ds lkStU; ls</div>

<?php include ('./common/message.php'); ?>

</body>



<script type="text/javascript">
    
     function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
    
    
     function refreshCaptcha()
            {
                    var img = document.images['captchaimg'];
                    img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
            }
            
    
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    jQuery().ready(function () {
        $('#txtApplicantDOB').datepicker({
            format: "dd-mm-yyyy",
            orientation: "bottom auto",
            todayHighlight: true,
            autoclose: true
        });

        $("#btnSubmit").click(function () {
            if ((txtApplicationId.value != '' && txtApplicationId.value > 99999999999999999) || (txtApplicantName.value != '' && txtApplicantFName.value != '' && txtApplicantDOB.value != '')) {
                $('#response').empty();
                $('#response').append("<p class='state-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Authenticating.....</span></p>");
                var url = "./common/cfOasisAdmission.php";
                var forminput = $("#frmwcdinfo").serialize();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: "action=downloadApp&" + forminput,
                    success: function (data) {
                        $('#response').empty();
                        $('#response').append("<p class='state-success'><span><img src=images/success.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                });
            } else {
                $('#response').empty();
                $('#response').append("<p class='state-success'><span><img src=images/drop.png width=20px /></span><span class='star'>Invalid input details.</span></p>");
            }

            return false; // avoid to execute the actual submit of the form.
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>

<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>

</html>