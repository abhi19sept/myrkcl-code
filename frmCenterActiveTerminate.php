<?php
$title = "Update Location Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Update'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Update Center Active and Terminate</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmCenterActiveTerminate" id="frmCenterActiveTerminate" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="cc">Enter Center Code:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="javascript:return allownumbers(event);" name="txtcc" id="txtcc" placeholder="Enter Center Code">
                            <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        </div>

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                        </div>
                    </div>


                    <div id="main-content" style="display:none">
                        <div class="panel panel-info" id="impreg">
                            <div class="panel-heading">Registered IT-GK Details</div>
                            <div class="panel-body">

                                <div class="container">
                                    <div class="col-sm-4"> 
                                        <label for="edistrict">IT-GK Name:<span class="star">*</span></label>
                                        <input type="text" maxlength="10" class="form-control" name="txtCenterName" id="txtCenterName" readonly="true"/>
                                        <input type="hidden" class="form-control" maxlength="10" name="txtCenterCode" id="txtCenterCode"/>
                                    </div>
                                    <div class="col-sm-3"> 
                                        <label for="edistrict">District:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtDistrictName" id="txtDistrictName" readonly="true"/>
<!--                                        <input type="hidden" class="form-control" maxlength="10" name="txtDistrictCode" id="txtDistrictCode"/>-->
                                    </div>
                                    <div class="col-sm-3"> 
                                        <label for="edistrict">Current Tehsil:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtTehsilName" id="txtTehsilName" readonly="true"/>
<!--                                        <input type="hidden" class="form-control" maxlength="10" name="txtTehsilCode" id="txtTehsilCode"/>-->
                                    </div>
                                </div>
                                <br>
                                <div class="container">
                                    <div class="col-sm-4"> 
                                        <label for="edistrict">Service Provider Name:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtSPname" id="txtSPname" readonly="true"/>
                                    </div>
                                    <div class="col-sm-3"> 
                                        <label for="edistrict">Current Activation Status:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtActiveStatus" id="txtActiveStatus" readonly="true"/>
                                    </div>
                                    <div class="col-sm-3"> 
                                        <label for="edistrict">Last Status Change Date:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtActiveDate" id="txtActiveDate" readonly="true"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">Select IT-GK Activation Status</div>
                            <div class="panel-body">

                                <div class="col-sm-4 form-group">     
                                    <label for="area">Select Activation Status:<span class="star">*</span></label> <br/>                               
                                    <label class="radio-inline"> <input type="radio" id="CenterActive" name="ActivationStatus"/> Active </label>
                                    <label class="radio-inline"> <input type="radio" id="CenterTerminate" name="ActivationStatus"/> Terminate </label>
                                </div>

                            </div>
                        </div>


                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Click here to Update Details"/>    
                        </div>
                    </div>
                </form>
            </div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        $("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            if (txtcc.value == '') {
                BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>&nbsp; Please Enter Center Code to Proceed.</span>");
            }
            $.ajax({
                type: "post",
                url: "common/cfCenterActiveTerminate.php",
                data: "action=DETAILS&values=" + txtcc.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Center Code" + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        txtCenterName.value = data[0].orgname;
                        txtCenterCode.value = data[0].itgkcode;
                        txtActiveStatus.value = data[0].txtActiveStatus;
                        txtActiveDate.value = data[0].txtActiveDate;

                        txtDistrictName.value = data[0].districtname;
                        txtTehsilName.value = data[0].tehsilname;
                        txtSPname.value = data[0].txtSPname;



                        $("#main-content").show();
                        $('#txtcc').attr('readonly', true);
                        $("#btnShow").hide();

                    }

                }
            });
        });

        $("#btnSubmit").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfCenterActiveTerminate.php"; // the script where you handle the form input.

            //alert(filename);    
            if (document.getElementById('CenterActive').checked) //for radio button
            {
                var activation_type = 'A';
            } else if (document.getElementById('CenterTerminate').checked) {
                activation_type = 'T';
            } else {
                activation_type = txtAreaType.value;
            }
            var data;
            // alert(Mode);
            if (Mode == 'Add')
            {

                data = "action=ADD&&ddlActiveStatus=" + activation_type + "&itgkcode=" + txtCenterCode.value + ""; // serializes the form's elements.

            } else
            {
                data = "action=UPDATE&ddlActiveStatus=" + activation_type + "&itgkcode=" + txtCenterCode.value + ""; // serializes the form's elements.
            }
            //alert(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Center Details Updated Successfully.</span>");
                        window.setTimeout(function () {
                            window.location.href = "frmCenterActiveTerminate.php";
                        }, 2000);

                        Mode = "Add";
                        resetForm("frmCenterActiveTerminate");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }

                }
            });


            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

</html>