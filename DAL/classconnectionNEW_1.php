<?php

require 'common/message.php';
require 'include/config.php';
require '../common/cflog.php';
date_default_timezone_set("Asia/Kolkata");
class _Connection {

    public $connection;

    public function __construct() {
        
    }

    public function CreateConection() {
        $server = GlobalVar::DataBaseServerName;
        $unm = GlobalVar::DataBaseUserName;
        $pwd = GlobalVar::DataBasePassword;
        $db = GlobalVar::DataBaseName;

        $this->connection = new mysqli($server, $unm, $pwd, $db);
        return $this->connection;
    }

    public function Connect() {
        if (!$this->connection) {
            $connection = $this->CreateConection();
            return $connection;
        } else {
            return $this->connection;
        }
    }

    public function ExecuteQuery($_Query, $_StatementType) {
        $_Response = array();
        //this.Connect();
        $connection = $this->Connect();

        $_Data = mysqli_query($connection, $_Query);
        if (!$_Data) {
            $_Response[0] = mysqli_error($connection);
            $_Response[1] = Message::Error;
        } else {
            switch ($_StatementType) {
                case Message::SelectStatement :

                    if (mysqli_num_rows($_Data) > 0) {
                        $_Response[0] = Message::SuccessfullyFetch;
                        $_Response[1] = Message::Success;
                        $_Response[2] = $_Data;
                    } else {
                        $_Response[0] = Message::NoRecordFound;
                        $_Response[1] = Message::Success;
                        $_Response[2] = $_Data;
                    }

                    break;
                case Message::InsertStatement :
                    $_Response[0] = Message::SuccessfullyInsert;
                    $_Response[1] = Message::Success;
                    break;
                case Message::DeleteStatement :
                    $_Response[0] = Message::SuccessfullyDelete;
                    $_Response[1] = Message::Success;
                    break;
                case Message::UpdateStatement :
                    $_Response[0] = Message::SuccessfullyUpdate;
                    $_Response[1] = Message::Success;
                    break;
                case Message::MultipleStatement :
                    $_Response[0] = Message::SuccessfullyDone;
                    $_Response[1] = Message::Success;
                    break;
                default :
                    $_Response[0] = Message::InvalidRequest;
                    $_Response[1] = Message::Error;
                    break;
            }
        }
      
        return $_Response;
    }

    public function __destruct() {
        $isclosed = $this->connection->close();
 if(!$isclosed){
              m_log("connection not closed".date('Y-m-d h:i:s'));
        }
        else{
             m_log("connection closed".date('Y-m-d h:i:s'));
        }
    }

}
