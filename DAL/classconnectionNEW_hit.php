<?php

require 'common/message.php';
require 'include/config.php';
require '../common/cflog.php';
class _Connection {

    public $connection;

    public function __construct() {
        
    }

    public function CreateConection() {
        $server = GlobalVar::DataBaseServerName;
        $unm = GlobalVar::DataBaseUserName;
        $pwd = GlobalVar::DataBasePassword;
        $db = GlobalVar::DataBaseName;
        try {
            
            // 1. Create a database connection
            $connection = mysqli_connect($server,$unm,$pwd);
            if (!$connection) {
                die("Database connection failed");
            }

            // 2. Select a database to use 
            $db_select = mysqli_select_db($connection, $db);
            if (!$db_select) {
                die("Database selection failed: " . mysqli_error($connection));
            }
             
        } catch (Exception $_ex) {
            echo $_ex->getMessage();
        }
         return $this->connection = $connection;
    }

    public function Connect() {
        if (!$this->connection) {
            $connection = $this->CreateConection();
            return $connection;
        } else {
            return $this->connection;
        }
    }

    public function ExecuteQuery($_Query, $_StatementType) {
        $_Response = array();
        //this.Connect();
        $connection = $this->Connect();

        $_Data = mysqli_query($connection, $_Query);
        if (!$_Data) {
            $_Response[0] = mysqli_error($connection);
            $_Response[1] = Message::Error;
        } else {
            switch ($_StatementType) {
                case Message::SelectStatement :

                    if (mysqli_num_rows($_Data) > 0) {
                        $_Response[0] = Message::SuccessfullyFetch;
                        $_Response[1] = Message::Success;
                        $_Response[2] = $_Data;
                    } else {
                        $_Response[0] = Message::NoRecordFound;
                        $_Response[1] = Message::Success;
                        $_Response[2] = $_Data;
                    }

                    break;
                case Message::InsertStatement :
                    $_Response[0] = Message::SuccessfullyInsert;
                    $_Response[1] = Message::Success;
                    break;
                case Message::DeleteStatement :
                    $_Response[0] = Message::SuccessfullyDelete;
                    $_Response[1] = Message::Success;
                    break;
                case Message::UpdateStatement :
                    $_Response[0] = Message::SuccessfullyUpdate;
                    $_Response[1] = Message::Success;
                    break;
                case Message::MultipleStatement :
                    $_Response[0] = Message::SuccessfullyDone;
                    $_Response[1] = Message::Success;
                    break;
                default :
                    $_Response[0] = Message::InvalidRequest;
                    $_Response[1] = Message::Error;
                    break;
            }
        }
        $thread_id = mysqli_thread_id($connection);

		/* Kill connection */
		mysqli_kill($connection, $thread_id);
		

        return $_Response;
    }

    public function __destruct() {
        $isclosed = $this->connection->close();
		if(!$isclosed){
        	  m_log("connection not closed".date('Y-m-d h:i:s'));
        }
        else{
        	 m_log("connection closed".date('Y-m-d h:i:s'));
        }
    }

}
