<?php
    require '../common/message.php';

    class Database
    {
        private $host = DB_HOST;
        private $user = DB_USER;
        private $pass = DB_PASS;
        private $dbname = DB_NAME;

        private $dbh;
        private $error;
        private $stmt;

        /**
         * Database constructor.
         * Connects with database using PDO
         */
        public function __construct()
        {
            if($this->dbh == "" || is_null($this->dbh)){
                // Set DSN
                $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
                // Set options
                $options = array(
                    PDO::ATTR_PERSISTENT => true,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                );
                // Create a new PDO instance
                try {
                    $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
                } // Catch any errors
                catch (PDOException $e) {
                    $this->error = "Error in connecting with database : ".$e->getMessage()."    ";
                }
            } else {
                echo "Connection exists";
            }
        }


        /**
         * @param $query string
         * This function allows you to bind values into your SQL statements.
         * This is important because it takes away the threat of SQL Injection because you are no longer having to
         * manually include the parameters into the query string
         */
        public function prepareQuery($query){
            $this->stmt = $this->dbh->prepare($query);
        }


        /**
         * In order to prepare our SQL queries, we need to bind the inputs with the placeholders we put in place.
         * @param $param (It is the placeholder value that we will be using in our SQL statement, for example :name)
         * @param $value (It is the actual value that we want to bind to the placeholder, for example "Barry Burton")
         * @param null $type (It is the datatype of the parameter, example string)
         */
        public function bind($param, $value, $type = null){
            if (is_null($type)) {
                switch (true) {
                    case is_int($value):
                        $type = PDO::PARAM_INT;
                        break;
                    case is_bool($value):
                        $type = PDO::PARAM_BOOL;
                        break;
                    case is_null($value):
                        $type = PDO::PARAM_NULL;
                        break;
                    default:
                        $type = PDO::PARAM_STR;
                }
            }
            $this->stmt->bindParam($param, $value, $type);
        }

        /**
         * @return mixed
         * @param $_StatementType string
         * @param $resultSetType (SINGLE|ALL)
         * This method executes the prepared statement.
         */
        public function executeQuery($_StatementType, $resultSetType = "ALL"){
            $_Response = [];
            try{
                $this->stmt->execute();
                switch ($_StatementType) {
                    case Message::SelectStatement :
                        $data = ($resultSetType == "SINGLE") ? $this->stmt->fetch(PDO::FETCH_ASSOC) : $this->stmt->fetchAll(PDO::FETCH_ASSOC);
                        if ($this->stmt->rowCount() > 0) {
                            $_Response[0] = Message::SuccessfullyFetch;
                            $_Response[1] = Message::Success;
                            $_Response[2] = $data;
                        } else {
                            $_Response[0] = Message::NoRecordFound;
                            $_Response[1] = Message::Success;
                            $_Response[2] = $data;
                        }
                        break;
                    case Message::InsertStatement :
                        $_Response[0] = Message::SuccessfullyInsert;
                        $_Response[1] = Message::Success;
                        break;
                    case Message::DeleteStatement :
                        $_Response[0] = Message::SuccessfullyDelete;
                        $_Response[1] = Message::Success;
                        break;
                    case Message::UpdateStatement :
                        $_Response[0] = Message::SuccessfullyUpdate;
                        $_Response[1] = Message::Success;
                        break;
                    case Message::MultipleStatement :
                        $_Response[0] = Message::SuccessfullyDone;
                        $_Response[1] = Message::Success;
                        break;
                    default :
                        $_Response[0] = Message::InvalidRequest;
                        $_Response[1] = Message::Error;
                        break;
                }
            } catch (PDOException $pd){
                $_Response[0] = Message::Error;
                $_Response[1] = $pd->getMessage();
                $this->error = "Exception Occurred : ".$pd->getMessage()."    ";
            }
            return $_Response;
        }


        /**
         * This method dumps the the information that was contained in the Prepared Statement.
         * @return mixed
         */
        public function debugDumpParams(){
            return $this->stmt->debugDumpParams();
        }

        public function beginTransaction(){
            return $this->dbh->beginTransaction();
        }

        public function endTransaction(){
            return $this->dbh->commit();
        }

        public function cancelTransaction(){
            return $this->dbh->rollBack();
        }

        public function getLastInsertedId(){
            return $this->dbh->lastInsertId();
        }
    }