<?php
require 'common/message.php';
require 'include/config.php';
require_once 'common/generalFunctions.php';

class _Connection {

    //$_ObjGlobalVar=new GlobalVar();
    public function Connect() {
        $server = GlobalVar::DataBaseServerName;
        $unm = GlobalVar::DataBaseUserName;
        $pwd = GlobalVar::DataBasePassword ;
        $db = GlobalVar::DataBaseName;
        try {
            $con = mysql_connect($server, $unm, $pwd) or die("Could not connect: " . mysql_error());
            mysql_select_db($db, $con);
        } catch (Exception $_ex) {
            echo $_ex->getMessage();
        }
    }

    public function ExecuteQuery($_Query, $_StatementType) {
        $_Response = array();
        $_Data = mysql_query($_Query);
        if (!$_Data) {
            $_Response[0] = mysql_error();
            $_Response[1] = Message::Error;
        } else {
            switch ($_StatementType) {
                case Message::SelectStatement :

                    if (mysql_num_rows($_Data) > 0) {
                        $_Response[0] = Message::SuccessfullyFetch;
                        $_Response[1] = Message::Success;
                        $_Response[2] = $_Data;
                    } else {
                        $_Response[0] = Message::NoRecordFound;
                        $_Response[1] = Message::Success;
                        $_Response[2] = $_Data;
                    }

                    break;
                case Message::InsertStatement :
                    $_Response[0] = Message::SuccessfullyInsert;
                    $_Response[1] = Message::Success;
                    break;
                case Message::DeleteStatement :
                    $_Response[0] = Message::SuccessfullyDelete;
                    $_Response[1] = Message::Success;
                    break;
                case Message::UpdateStatement :
                    $_Response[0] = Message::SuccessfullyUpdate;
                    $_Response[1] = Message::Success;
                    break;
                case Message::MultipleStatement :
                    $_Response[0] = Message::SuccessfullyDone;
                    $_Response[1] = Message::Success;
                    break;
                default :
                    $_Response[0] = Message::InvalidRequest;
                    $_Response[1] = Message::Error;
                    break;
            }
        }
        return $_Response;
    }
	 public function learnercodegen() {
         $random= (mt_rand(100, 999));
         $random.= date("y");
         $random.= date("m");
         $random.= date("d");
         $random.= date("h");
         $random.= date("i");
         $random.= date("s");
         $random.= '789';
		 
	     return $random; 
    }


}

