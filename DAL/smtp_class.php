<?php
/**
 * This example shows making an SMTP connection with authentication.
 */

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require 'Mail/PHPMailerAutoload.php';

class sendMail{
	
	protected $mail = "";
	
	public function __construct(){
		$this->mail = new PHPMailer;
		$this->mail->isSMTP();
		$this->mail->SMTPDebug = 0;
		$this->mail->Debugoutput = 'html';
		$this->mail->Host = "mail.rkcl.in";
		$this->mail->Port = 25;
		$this->mail->SMTPAuth = true;
		$this->mail->Username = "myrkclsupport@rkcl.in";
		$this->mail->Password = 'U$er@#$0169';
		$this->mail->setFrom('myrkclsupport@rkcl.in', 'RKCL SUPPORT');
		$this->mail->smtpConnect([
		'ssl' => [
		'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
		]]);
	}
	
	public function sendEmail($to, $to_name, $subject, $html_message){
		$this->mail->addAddress($to, $to_name);
		$this->mail->Subject = $subject;
		$this->mail->msgHTML($html_message);
		if (!$this->mail->send()) {
			$error = 'Mail error: '.$mail->ErrorInfo;   
            return false;
		} else {
			 $error = 'Message sent';  
            return true;
		}
	}


         public function sendEmailHTML($to, $to_name, $subject, $html_file, $variableArray)
        {
            $this->mail->addAddress($to, $to_name);
            $this->mail->Subject = $subject;
            $path = ($_SERVER['HTTP_HOST'] == "localhost") ? "http://".$_SERVER['HTTP_HOST'] . "/myrkcl/" : "https://".$_SERVER['HTTP_HOST'] . "/";
            $templateHTML = file_get_contents($path . $html_file);
            foreach ($variableArray as $key => $value) {
                $templateHTML = str_replace("{" . $key . "}", $value, $templateHTML);
            }
            $this->mail->msgHTML($templateHTML, dirname(__FILE__));
            if (!$this->mail->send()) {
                $error = 'Mail error: ' . $this->mail->ErrorInfo;
                return false;
            } else {
                $error = 'Message sent';
                return true;
            }
        }

}