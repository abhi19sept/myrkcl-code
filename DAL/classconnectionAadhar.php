<?php
//require 'common/message.php';
require 'include/configaadhar.php';
class _Connection_Aadhar {
    
    public function Connect() {
        $server = GlobalVarAadhar::DataBaseServerName;
        $unm = GlobalVarAadhar::DataBaseUserName;
        $pwd = GlobalVarAadhar::DataBasePassword ;
        $db = GlobalVarAadhar::DataBaseName;
        try {
            
            // 1. Create a database connection
            $connection = mysqli_connect($server,$unm,$pwd);
            if (!$connection) {
                die("Database connection failed");
            }

            // 2. Select a database to use 
            $db_select = mysqli_select_db($connection, $db);
            if (!$db_select) {
                die("Database selection failed: " . mysqli_error($connection));
            }
             
        } catch (Exception $_ex) {
            echo $_ex->getMessage();
        }
        return $connection;
    }

    public function ExecuteQuery($_Query, $_StatementType) {
        $_Response = array();
        //this.Connect();
        $connection=$this->Connect();
        
        $_Data = mysqli_query($connection,$_Query);
        if (!$_Data) {
            $_Response[0] = mysqli_error($connection);
            $_Response[1] = Message::Error;
        } else {
            switch ($_StatementType) {
                case Message::SelectStatement :

                    if (mysqli_num_rows($_Data) > 0) {
                        $_Response[0] = Message::SuccessfullyFetch;
                        $_Response[1] = Message::Success;
                        $_Response[2] = $_Data;
                    } else {
                        $_Response[0] = Message::NoRecordFound;
                        $_Response[1] = Message::Success;
                        $_Response[2] = $_Data;
                    }

                    break;
                case Message::InsertStatement :
                    $_Response[0] = Message::SuccessfullyInsert;
                    $_Response[1] = Message::Success;
                    break;
                case Message::DeleteStatement :
                    $_Response[0] = Message::SuccessfullyDelete;
                    $_Response[1] = Message::Success;
                    break;
                case Message::UpdateStatement :
                    $_Response[0] = Message::SuccessfullyUpdate;
                    $_Response[1] = Message::Success;
                    break;
                case Message::MultipleStatement :
                    $_Response[0] = Message::SuccessfullyDone;
                    $_Response[1] = Message::Success;
                    break;
					// hitesh Gupta 12 feb
                case Message::OnUpdateStatement :
                    $_Response[0] = Message::SuccessfullyDone;
                    $_Response[1] = Message::Success;
                    $_Response[2] = mysqli_affected_rows($connection);
                    break;
                // end 
                default :
                    $_Response[0] = Message::InvalidRequest;
                    $_Response[1] = Message::Error;
                    break;
            }
        }
        return $_Response;
    }
}

