
<?php
include $_SERVER['DOCUMENT_ROOT'].'/include/ftp_config.php';
class ftpConnection {

    public $con = "";


	public function ftpConnect() {

		$ftp_server = DirectoryPaths::ServerHost;
        $ftp_username = DirectoryPaths::ServerUsername;
        $ftp_userpass = DirectoryPaths::ServerPassword;
        try {
            $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");

            $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);
            
            ftp_pasv($ftp_conn, true);

            return $ftp_conn;
        } catch (Exception $_ex) {
            echo $_ex->getMessage();
        }
    }

    public function ftpdetails(){
        $ftp_server = DirectoryPaths::ServerHost;
        $ftp_username = DirectoryPaths::ServerUsername;
        $ftp_userpass = DirectoryPaths::ServerPassword;
        
            try {
            $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
            return 'ftp://'.$ftp_username.':'.$ftp_userpass.'@'.$ftp_server.'/';
            //return $ftp_server;
        } catch (Exception $_ex) {
            echo $_ex->getMessage();
        }
    }
	public function ftpPathIp(){
		 $ftp_server_path = DirectoryPaths::ServerHost;
		 return 'http://'.$ftp_server_path.'/docrkcl/';
	}  
    public function LearnerBatchNameFTP($_Course,$_BatchName){

        if ($_Course == 5) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

        } elseif ($_Course == 1) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);                    

        } elseif ($_Course == 4) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

        }
        elseif ($_Course == 3) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

        }
        elseif ($_Course == 22) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

        }
        elseif ($_Course == 26) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

        }
        elseif ($_Course == 27) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

        }
        elseif ($_Course == 24) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

        }               
        
        elseif ($_Course == 23) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

        }   

         elseif ($_Course == 25) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

        }
        elseif ($_Course == 28) {

            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

        }
        elseif ($_Course == 29) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

        }
        elseif ($_Course == 30) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

        }
        elseif ($_Course == 31) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

        }
        elseif ($_Course == 32) {
            $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
            $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

        }
        return $_LearnerBatchNameFTP;
    }
}




?>