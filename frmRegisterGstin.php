<?php
$title = "Register your GSTIN in MYRKCL";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var BankAccountCode=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var BankAccountCode=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Register GSTIN in MYRKCL</div>
            <div class="panel-body">
                <form name="frmRegisterGstin" id="frmRegisterGstin" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>				

                        <div class="col-sm-12">
                            <div class="col-sm-8">
                                <label for="edistrict">Enter GSTIN No :<span class="star">*</span></label>

                                <div class="gstnno input-group">

                                    <div class="input-group-addon"><span style="font-size:10px;">State Code</span></div>
                                    <input type="text" class="in form-control" name="GSTIN1" id="GSTIN1" placeholder="1st two digits" maxlength="2"onkeypress="javascript:return allownumbers(event);" required="required" oninvalid="setCustomValidity('Please Fill State Code')" onchange="try {
                                                setCustomValidity('')
                                            } catch (e) {
                                            }">

                                    <div class="input-group-addon"><span style="font-size:10px;">PAN No.</span></div>
                                    <input type="text" class="in form-control text-uppercase" name="GSTIN2" id="GSTIN2" placeholder="PAN no." maxlength="10" onkeypress="javascript:return validPan(event);"required="required" oninvalid="setCustomValidity('Please Fill PAN No')" onchange="try {
                                                setCustomValidity('')
                                            } catch (e) {
                                            }">

                                    <div class="input-group-addon"><span style="font-size:10px;"></span></div>
                                    <input type="text" class="in form-control text-uppercase" name="GSTIN3" id="GSTIN3" placeholder="Last 3 digits" maxlength="3" onkeypress="javascript:return validPan(event);"required="required" oninvalid="setCustomValidity('Please Fill Last 3 digits')" onchange="try {
                                                setCustomValidity('')
                                            } catch (e) {
                                            }">

                                </div></div>
                            <div class="col-sm-4">
                                <label >Generated GSTIN No :</label>
                                <div class="input-group">
                                    <input type="text" class="out form-control text-uppercase"  id="GSTIN5"  name="GSTIN5" readonly="true">
                                    
                                </div> 
                                <input type="hidden" class="form-control " id="txtpanno"  name="txtpanno" >
                            </div>
                        </div>
                        <br>
                        <br>   

                        <br>
                        <br>                        


                        <div class="col-sm-10">
                            <label for="photo">GSTIN Registration Document :<span class="star">*</span></label> </br> </br> 
                            <div class="input-group" > 

                                <img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">								  
                                <input style="margin-top:5px !important;" id="uploadImage7" type="file" name="uploadImage7"  required="required" oninvalid="setCustomValidity('Please Upload Image')" onchange="try {
                                                setCustomValidity('')
                                            } catch (e) {
                                            } checkCategoryPhoto1(this);
                                        PreviewImage(7)">	
                                <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                            </div>

                        </div>
                        <br>
                        <br>                        
                        <br>
                        <br>
                        <div class="col-sm-10">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>
                        </div>
                    </div>  
                </form>
            </div>
        </div>
    </div>
</div>
</body>

<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
    ;
</script>
<script language="javascript" type="text/javascript">
    function checkCategoryPhoto1(target) {
        var ext = $('#uploadImage7').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage7").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        }
        document.getElementById("uploadImage7").innerHTML = "";
        return true;
    }
</script>



<script>
    $('#txtConfirmaccountNumber').on('keyup', function () {
        if ($(this).val() == $('#txtaccountNumber').val()) {
            $('#message').html('confirmed').css('color', 'green');
        } else
            $('#message').html('confirm account no. should be same as account no').css('color', 'red');
    });
</script>





<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $('.in').on('input', function () {
            var allvals = $('.in').map(function () {
                return this.value;
            }).get().join('');
            $('.out').val(allvals);
        });


        $('#GSTIN1').keyup(function () {
            if (this.value.length == $(this).attr('maxlength')) {
                $('#GSTIN2').focus();
            }
        });
        $('#GSTIN2').keyup(function () {
            if (this.value.length == $(this).attr('maxlength')) {
                $('#GSTIN3').focus();
            }
        });

        function loadpanno()
        {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfRegisterGstin.php",
                data: "action=EDIT&values=" + BankAccountCode + "",
                success: function (data) {

                    txtpanno.value = data;



                }
            });
        }
        loadpanno();
        $("#frmRegisterGstin").submit(function () {


            if (txtpanno.value != GSTIN2.value)
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><font color=red>PAN No entered does not match with the Bank Account Details, Pls Update Bank Acc. Details First.</font></span></p>");


            } else {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfRegisterGstin.php"; // the script where you handle the form input.

                var file_data = $("#uploadImage7").prop("files")[0];   // Getting the properties of file from file field
                var form_data = new FormData();                  // Creating object of FormData class
                form_data.append("file", file_data)              // Appending parameter named file with properties of file_field to form_data
                form_data.append("action", "UPDATE")
                form_data.append("txtGstin", GSTIN5.value)

                $.ajax({

                    url: url,
                    dataType: 'script',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data, // Setting the data attribute of ajax with file_data
                    type: 'post',
                    success: function (data)
                    {

                        if (data == 1)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>GSTIN Sussessfully Registered</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmRegisterGstin.php";
                            }, 3000);
                            Mode = "Add";
                            resetForm("frmRegisterGstin");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }



                    }
                });
            }



            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmbankaccount_validation.js" type="text/javascript"></script>	
</html>