<?php
$title = "SMS FEE PAYMENT";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var FunctionCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
echo "<script>var CenterCode='" . $_SESSION['User_LoginId'] . "'</script>";
//echo "<script>var amount= " . $_REQUEST['amount'] . " </script>";
echo "<script>var PayType= 'SMS Fee Payment' </script>";

$payutxnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
echo "<script>var PayUTranID= '" . $payutxnid . "' </script>";

$random = (mt_rand(1000, 9999));
$random.= date("y");
$random.= date("m");
$random.= date("d");
$random.= date("H");
$random.= date("i");
$random.= date("s");
//echo $random;

echo "<script>var RKCLTxnId= '" . $random . "' </script>";
?>
<div style="min-height:450px !important;max-height:2500px !important">
        <div class="container"> 
			 

            <div class="panel panel-primary" style="margin-top:46px !important;">

                <div class="panel-heading">Package Request for SMS </div>
                <div class="panel-body">
                    
                    <form name="frmreqmodule" id="frmreqmodule"  class="form-inline" enctype="multipart/form-data"> 
					
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            
							<div class="container">	
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Package:<font color="red">*</font></label>
                                <select id="ddPackage" name="ddPackage" class="form-control" >
								  
                                </select>    
                            </div>
							
							
							
							
							 <div class="col-sm-4 form-group" style="display:none;" > 
                                <label for="edistrict">Price:<font color="red">*</font></label>
							<input type="text" class="form-control" maxlength="50" name="txtprice" id="txtprice" >
                            </div>
							
							

							
							</div>
							
							
							
							<div class="container">	
                            <div class="col-sm-10 form-group"> 
                                <label for="edistrict">For Service Tax Details</label>
                                   
                            </div>
							
							</div>



                         <div class="container">
						 
						 <input type="hidden" class="form-control" maxlength="50" name="txtpkgprice" id="txtpkgprice" value="0">
						 <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId" value="<?php echo $random; ?>"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtGeneratePayUId" id="txtGeneratePayUId" value="<?php echo $payutxnid; ?>"/>
                   
						 <input type="submit" name="btnSubmit" id="btnSubmit" style="display:none !important;"class="btn btn-primary" value="Submit" style='margin-left:10px'/>

						
                    </div>   
							
							
					</div>
					
					
					<div id="gird"></div>
					<?php if ($_SESSION['User_UserRoll'] == '7' || ($_SESSION['User_UserRoll'] == '14') ) 
					{	
                     ?>
					 <input type="submit" name="btnSubmit" id="btnSubmit1" class="btn btn-primary" value="Pay Now" style='margin-left:10px'/>    		
					<?php
					} 
					else 
					{ 
				    ?>
				     <input type="submit" name="btnSubmit" style="display:none;" id="btnSubmit1" class="btn btn-primary" value="Pay Now" style='margin-left:10px'/>   
					<?php
					}
					?>
												
						</div>
						
						
						
                 </div>
            </div>   
        </div>


    </form>
	
	<form id="frmpostvalue" name="frmpostvalue" action="frmsmsfeepayment.php" method="post">
    <input type="hidden" id="code" name="code">
    <input type="hidden" id="paytypename" name="paytypename">   
    <input type="hidden" id="RKCLtranid" name="RKCLtranid"/>
    <input type="hidden" id="amount" name="amount">	
    <input type="hidden" id="txnid" name="txnid">	 
</form>
	

</div>



</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>


 <script type="text/javascript">
						var SuccessfullySend = "<?php echo Message::SuccessfullySend ?>";
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
						
						function FillPackage() {
						$.ajax({
							type: "post",
							url: "common/cfreqsms.php",
							data: "action=FILLPACKAGE",
							success: function (data) {
							$("#ddPackage").html(data);
							}
						});
						}
						FillPackage();	

						$("#ddPackage").change(function(){
							$("#btnSubmit").show();
							var ddPackage = $(this).val(); 
							//alert(ddPackage);
							$.ajax({
							url: 'common/cfreqsms.php',
								type: "post",
								data: "action=FILLPRICE&values=" + ddPackage + "",
								success: function(data)
								{
								 //alert(data);
								  
								 data = $.parseJSON(data);
								 //alert(data);
								 txtprice.value = data;
								}
							});
                        });
						
						function FillPkgPrice() {
						$.ajax({
							type: "post",
							url: "common/cfreqsms.php",
							data: "action=FillPkgPrice",
							success: function (data) {
							 txtpkgprice.value = data;
							}
						});
						}
						FillPkgPrice();	
						
						function showData() {
						$.ajax({
							type: "post",
							url: "common/cfreqsms.php",
							data: "action=SHOW",
							success: function (data) {

								$("#gird").html(data);
								 $('#example').DataTable({
								dom: 'Bfrtip',
								buttons: [
									'copy', 'csv', 'excel', 'pdf', 'print'
								]
							});
								

							}
						});
					}
					showData();
					$("#btnSubmit1").click(function () {
					
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						var url = "common/cfreqsms.php"; // the script where you handle the form input.
			
						var data;
						var forminput=$(frmreqmodule).serialize();
						if (Mode == 'Add')
						{
							data = "action=ADDPkgDetails&" +forminput;
						}
						else
						{
					
							data = "action=UPDATE&code=" + OrganizationCode +"&" + forminput;
							//data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
						}
						$.ajax({
						type: "POST",
						url: url,
						data: data,
						success: function (data)
						{
							$('#response').empty();
							if (data == 0) {
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please Checked Atleast one checkbox." + "</span></p>");
							}
                        else
                        {                            
                        $('#code').val(CenterCode);
                        $('#paytype').val(PayType);
                        
                        var payname = 'SMS Fee Payment';
						$('#paytypename').val(payname);
                        
                        $('#RKCLtranid').val(RKCLTxnId);
                        
                        $('#txnid').val(PayUTranID);
						
                        amount.value = data;  
							$('#frmpostvalue').submit();
						}
                        
					}
                });
			 //}
                return false; // avoid to execute the actual submit of the form.
            });
					
					
					
						
					$("#btnSubmit").click(function () {
					
					if ($("#frmreqmodule").valid())
					{
						$("#btnSubmit").hide();
					    $("#btnSubmit1").show();	
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						var url = "common/cfreqsms.php"; // the script where you handle the form input.
			
						var data;
						var forminput=$(frmreqmodule).serialize();
						if (Mode == 'Add')
						{
							data = "action=ADD&" +forminput;
						}
						else
						{
					
							data = "action=UPDATE&code=" + OrganizationCode +"&" + forminput;
							//data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
						}
						$.ajax({
						type: "POST",
						url: url,
						data: data,
						success: function (data)
						{
							if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
							{
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
								
								window.setTimeout(function () {
                                window.location.href = "frmreqsms.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmreqsms.php");
							
							
							
							
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
						//$("#btnSubmit1").show();


						}
                });
			 }
                return false; // avoid to execute the actual submit of the form.
            });

                            
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmreqpackagevalidation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>