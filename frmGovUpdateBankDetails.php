<?php
$title="Modify Govt. Employee Bank Details";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
 ?>
<div style="min-height:430px !important;max-height:auto !important">
        <div class="container"> 
			 
            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Modify Govt. Employee Bank Details</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Learner Code:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="18" name="txtLCode" id="txtLCode" onkeypress="javascript:return allownumbers(event);" placeholder="Learner Code">
								
                            </div>							
							
							<div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
                            </div>
						</div>
						
						<div id="grid" name="grid" style="margin-top:35px;"> </div> 
	
                </div>
            </div>   
        </div>
    </form>
</div>
<!-- Edit Popup -->
<div id="myModalupdate" class="modal">
  <div class="modal-content">
    <div class="modal-header">
      <span class="close update">&times;</span>
      <h6>Update Bank Details</h6>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" name="aboutFormupdate" id="aboutFormupdate" enctype="multipart/form-data">
            <input type="hidden" name="lcode" id="lcode" value=""/>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">Employee Bank Account No.</label>
                <div class="col-sm-6">
                    <input class="form-control" id="Textempaccountno" name="Textempaccountno" maxlength="18" placeholder="Bank Account No" onkeypress="javascript:return allownumbers(event);" type="text" required/>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-3 control-label">IFSC Code</label>
                <div class="col-sm-6">
                    <input class="form-control" id="Textgifsccode" name="Textgifsccode" onkeypress="fnValidateIFSC();" placeholder="IFSC Code" maxlength="11" type="text" required/>
					<span id="ifsccode" style="font-size:10px;"></span><br>
					<span style="font-size:10px;">Note : IFSC code should be 11 digit. Starting 4 should be only alphabets[A-Z] and Remaining 7 should be accepting only alphanumeric.</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6" id="responseupdate"></div>
            </div>
            <div class="form-group last">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="button" name="bankdetailsupdate"  id="bankdetailsupdate" class="btn btn-success btn-sm">Update</button>		
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
      <h6>Update Bank Details</h6>
    </div>
  </div>
</div>
<link rel="stylesheet" href="css/popup.css">
<!-- End Popup -->
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>
 <script type="text/javascript">
$('#Textgifsccode').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
});
function fnValidateIFSC(){
         $('#ifsccode').html('');
     }
	 
		function AllowIFSC(ifsc) {//
			
			var reg = /^[A-Z|a-z]{4}[\w]{7}$/;

			// var reg = /^[A-Z|a-z]{4}[0][\w]{6}$/;
           // var reg = /^[A-Za-z]{4}\d{7}$/;
            //var ifsc = `SBIN1234567`;
            if(ifsc!=''){
                if (ifsc.match(reg)) {
                     $('#ifsccode').empty();
                }
                else {
                    $('#ifsccode').empty();
                    $('#Textgifsccode').val('');
                    $('#ifsccode').append("<span style='color:red; font-size: 12px;'>You have entered wrong IFSC Code.</span>");

                    return false;
                }
            }
        }
</script>
<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {        

	$("#grid").on("click",".fun_update_slider",function(){
				var edit_id = $(this).attr("id");
				var mybtnid = "myBtn_"+edit_id;
				//alert(edit_id);
				var modal = document.getElementById('myModalupdate');
				var btn = document.getElementById(mybtnid);
				var span = document.getElementsByClassName("update")[0];
				modal.style.display = "block";
				span.onclick = function() { 
					modal.style.display = "none";
				}
				window.onclick = function(event) {
					if (event.target == modal) {
						modal.style.display = "none";
					}
				}
				var url = "common/cfgovupdatebankdetails.php"; // the script where you handle the form input.
				var data;
				data = "action=EDIT&editid=" + edit_id +""; // serializes the form's elements.				 
				$.ajax({
					type: "POST",
					url: url,
					data: data,
					success: function (data)
					{  
						data = $.parseJSON(data);
						Textempaccountno.value = data[0].empaccountno;
						Textgifsccode.value = data[0].gifsccode;
						lcode.value = data[0].learnercode;
					}
				});
		});
		$("#bankdetailsupdate").click(function () {
			var ifsc = $('#Textgifsccode').val();
			AllowIFSC(ifsc);
				$.ajax({
							type: "post",
							url: "common/cfgovupdatebankdetails.php",
							data: "action=BANKDETAILSUPDATE&lcode=" + lcode.value + "&accnumber=" + Textempaccountno.value + "&ifscnumber=" + Textgifsccode.value + "",
							success: function (data)
							{
								//alert(data);
								if (data == 0){
									$('#responseupdate').empty();
									$('#responseupdate').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please enter value in required fields." + "</span></div>");
								
								}
								
								else {
									$('#responseupdate').empty();
									$('#responseupdate').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
									window.setTimeout(function () {
										window.location.href = "frmGovUpdateBankDetails.php";
									}, 3000);							
																	
									//$('#txtCode').attr('readonly', true);
									//$("#btnShow").hide();
								}	
						  }
					});	
		});
	
		$("#btnShow").click(function () {
             //alert("1");			
					$.ajax({
						type: "post",
                        url: "common/cfgovupdatebankdetails.php",
						data: "action=DETAILS&values=" + txtLCode.value + "",
						success: function (data)
						{
							//alert(data);
							if (data == 1){
								$('#response').empty();
								$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please enter Learner Code." + "</span></div>");
                            
							}
							
							else {
								$('#response').empty();
								$("#grid").html(data);
								 $('#example').DataTable({
									dom: 'Bfrtip',
									buttons: [
										'copy', 'csv', 'excel', 'pdf', 'print'
									]
								});								
																
								//$('#txtCode').attr('readonly', true);
								//$("#btnShow").hide();
							}	
					  }
                });		
          });
		
        $("#btnSubmit").click(function () {		
					    
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfderegisterdevice.php"; // the script where you handle the form input.
            var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&Ccode=" + txtCode.value + "&reason=" + ddlreason.value + ""; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + StatusCode + "&name=" + txtStatusName.value + "&description=" + txtStatusDescription.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
					if (data == 1){
								$('#response').empty();
								$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Reason." + "</span></div>");
                            
							}
                    else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmderegisterdevice.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });
		 
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</html>