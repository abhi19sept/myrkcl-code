<?php
$title="Block Center";
include ('header.php'); 
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
			<div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">CENTER Wise Report</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
					<form name="frmblock" id="frmclock" class="form-inline" role="form" enctype="multipart/form-data"> 
						<div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Center Code:</label>
                               <input type="text" class="form-control" maxlength="50" name="txtCenterCode" id="txtCenterCode" placeholder="Center Code"/>							
                            </div>  
						</div>  
               
                         <div class="container">
						<p class="btn submit" id="btnviewcontainer"> 
                            <input type="button" name="btnView" id="btnView" class="btn btn-primary" value="View Detail"/> 
						</p>
                        </div>    
                     <div id="grid" style="margin-top:15px;"> </div>       		
     </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>              
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";

        $(document).ready(function () {



            function GetCenterDetail() {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=CENTERWISE&CenterCode=" + txtCenterCode.value,
                    success: function (data) {
                         //alert(data);
                        if (data == 'Error')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>No Record Found For This Center</span></p>");
                        }
                        else
                        {
                            $("#grid").html(data);
                            $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                        }

                    }
                });
            }

            $("#btnView").click(function () {
				$('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
				setTimeout(function(){$('#grid').load();}, 2000); 				
                GetCenterDetail();
            });



            function FillBlockCategory() {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=FILLBLOCKCATEGORY",
                    success: function (data) {
                        $("#ddlBlockCategory").html(data);
                    }
                });
            }
            FillBlockCategory();




            $("#btnSubmit").click(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=BLOCK&&BlockCategory=" + ddlBlockCategory.value + "&Duration=" + txtBlockDuration.value + "&DurationType=" + ddlDurationType.value + "&CenterCode=" + txtCenterCode.value + "&CenterName=" + txtCenterName.value + "&Remark=" + txtRemark.value,
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyInsert)
                        {

                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "block.php";
                            }, 1000);

                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                    }
                });
            });


        });

    </script>
</html>
