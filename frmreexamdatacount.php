<?php
$title = "Re Exam Data Count";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
        echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
        echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    } else {
        echo "<script>var UserCode=0</script>";
        echo "<script>var Mode='Add'</script>";
    }	   
?>		

<div style="min-height:430px !important;max-height:auto !important;"> 

  <div class="container">     
    <div class="panel panel-primary" style="margin-top:20px !important;">  
        <div class="panel-heading">Center Wise Reexam Count</div>
        <div class="panel-body">

            <form name="frmreexamdatacount" id="frmreexamdatacount" class="form-inline" role="form" >
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>
                   
                        <div class="col-sm-4 form-group"> 
                            <label for="course">Select Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div> 
                    
                        			 
					</div>
					
                        <div id="gird" style="margin-top:5px;"> </div>                
					
                   
                </div>   
              </form>
          </div>
      </div>
  </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
	
<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillEvent() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfEventMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlExamEvent").html(data);
                }
            });
        }
        FillEvent();
    });
    $("#ddlExamEvent").change(function () {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

        $.ajax({
            type: "post",
            url: "common/cfreexamdatacount.php",
            data: "action=GETDATA&examevent=" + ddlExamEvent.value + "",
            success: function (data)
            {
                $('#response').empty();
                $("#gird").html(data);
				$('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ],
					  scrollY: 400,
                        scrollCollapse: true,
                        paging: false

                });
				
            }
        });
    });

</script>
</html>