<?php
$title = "User Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:450px !important;max-height:1500px !important">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">IT-GK's Registred Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="frmusermasterdetails" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                   

                    <div id="grid" name="grid" style="margin-top:35px;"> </div>

                    <?php if (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == '7') {
                    echo '<p>&nbsp;</p><div class="container">
                        <center> 
                            Your authorization expire date: <span class="expiredate star">*</span>
                        </center>
                    </div>';
                    if (isset($_SESSION['ITGK_Expire_Days'])) { 
                    $btnName = (isset($_SESSION['ITGK_Expire_For'])) ? $_SESSION['ITGK_Expire_For'] : '';
                    echo '<div class="container">
                        <center>
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Renew ' . $btnName . '" style="margin-top:25px"/>
                        </center>
                    </div>';
                        } 
                    } 
                    ?>
            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillEvent() {
            //alert("hello");
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfusermasterdetails.php",
                data: "action=schedule",
                success: function (data)
                {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'print'
                        ],
                          scrollY: 400,
                            scrollCollapse: true,
                            paging: false

                    });
                }
            });
        }
        FillEvent();

        function FillExpireDate() {
            $.ajax({
                type: "post",
                url: "common/cfusermasterdetails.php",
                data: "action=expiredate",
                success: function (data)
                {
                    $(".expiredate").html(data);
                }
            });
        }
        FillExpireDate();

        $("#btnShow").click(function () {
            $.ajax({
                type: "post",
                url: "common/cfusermasterdetails.php",
                data: "action=renew",
                success: function (data)
                {
                    if (data == 1) {
                        alert("Congratulation!! Your center has been renewed.");
                        window.location.href = "frmusermasterdetails.php";
                    } else {
                        alert("Something went wrong!! Please re-try after some time.");
                    }
                }
            });
        });
    });
   

</script>
</html>