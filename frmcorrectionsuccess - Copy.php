<?php
session_start();
$title = "Correction Payment";
include ('header.php');

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$status = $_POST["status"];
$firstname = $_POST["firstname"];
$amount = $_POST["amount"];
$txnid = $_POST["txnid"];
$posted_hash = $_POST["hash"];
$key = $_POST["key"];
$productinfo = $_POST["productinfo"];
$email = $_POST["email"];
$udf1 = $_POST["udf1"];
$udf2 = $_POST["udf2"];
$salt = "8IaBELXB";

require_once 'DAL/classconnection.php';
$_ObjConnection = new _Connection();
$_Response = array();
//    $_ObjConnection->Connect();

if ($udf1 == "superadmin") {

    echo "<div class='alert alert-danger' role='alert'><b>Invalid User Input!</b></div>";
} else {
    if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
        include ('root_menu.php');
    } else {

        global $_ObjConnection;
        $_ObjConnection->Connect();

        $_GetSessionQuery = "Select * From  tbl_user_session Where User_Session_LoginId='" . $udf1 . "'";
        $_ResponseGetSession = $_ObjConnection->ExecuteQuery($_GetSessionQuery, Message::SelectStatement);


        $_Row = mysqli_fetch_array($_ResponseGetSession[2]);


        $_SESSION['Login'] = 1;
        $_SESSION['User_LoginId'] = $_Row['User_Session_LoginId'];
        $_SESSION['User_UserRoll'] = $_Row['User_Session_UserRoll'];
        $_SESSION['User_ParentId'] = $_Row['User_Session_ParentId'];
        $_SESSION['User_Status'] = $_Row['User_Session_Status'];
        $_SESSION['User_Code'] = $_Row['User_Session_UserCode'];
        $_SESSION['User_EmailId'] = $_Row['User_Session_Email'];
        $_SESSION['UserRoll_Name'] = $_Row['User_Session_UserName'];
        $_SESSION['Organization_Name'] = $_Row['User_Session_OrgName'];
        $_SESSION['LAST_ACTIVITY'] = time();

        $_SESSION['Menu'] = CreateMenubyUserRole($_SESSION['User_UserRoll']);

        include ('root_menu.php');
    }
}

function CreateMenubyUserRole($_UserRole) {
    global $emp;
    global $response;
    $_Menu = "";

    $response = GetRootMenuByUserRole($_UserRole);

    if ($response[0] == Message::SuccessfullyFetch) {

        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Menu.="<li class='dropdown menu-large'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>" . $_Row['RootName'] . "</a>";           
			  $parentmenu = GetParentMenuByUserRole($_UserRole, $_Row['RootCode']);
			  
			  if ($parentmenu[0] == Message::SuccessfullyFetch) {
                $_Menu.="<ul class='dropdown-menu megamenu row' > ";
                while ($_Row1 = mysqli_fetch_array($parentmenu[2])) {
                    $_Menu.= "<li class='col-sm-3' style='min-height:250px !important;'> <ul><li class='dropdown-header' style='color: #fff;'>" . $_Row1['ParentName'] . "</li>";
                    $childmenu = GetChildMenuUserRole($_UserRole, $_Row1['ParentCode']);
                    if ($childmenu[0] == Message::SuccessfullyFetch) {
                       
                        while ($_ChildRow = mysqli_fetch_array($childmenu[2])) {
                            $_Menu.= "
							<li><a href='" . $_ChildRow['FunctionURL'] . "'>" . $_ChildRow['FunctionName'] . "</a></li>
							
							";
                        }
                       
                    }
                    $_Menu.="</ul></li>";
                    
                }
                $_Menu.= " </ul>";
               
            }			
             $_Menu.="</li>";
        }
    }
   
    return $_Menu;
}

	function GetRootMenuByUserRole($_UserRole) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From vw_userrolewiserootmenu Where UserRole='" . $_UserRole . "' Order By DisplayOrder";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	function GetParentMenuByUserRole($_UserRole, $_RootMenu) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {
            $_SelectQuery = "Select * From vw_userrolewiseparentmenu Where UserRole='" . $_UserRole . "' and RootMenu='" . $_RootMenu . "' Order By DisplayOrder";
            //echo $_SelectQuery;
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }
	
	function GetChildMenuUserRole($_UserRole, $_Parent) {
        global $_ObjConnection, $_Response;
        $_ObjConnection->Connect();
        try {

            $_SelectQuery = "Select * From vw_userrolewisefunction where "
                    . "UserRole='" . $_UserRole . "' and Parent='" . $_Parent . "' Order by Display";

            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
        } catch (Exception $_ex) {
            $_Response[0] = $_ex->getTraceAsString();
            $_Response[1] = Message::Error;
        }
        return $_Response;
    }



echo "<div style='min-height:430px !important;max-height:1500px !important;'>";
echo "<div class='container'>";

require 'DAL/sendsms.php';

If (isset($_POST["additionalCharges"])) {
    $additionalCharges = $_POST["additionalCharges"];
    $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
} else {

    $retHashSeq = $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
}
$hash = hash("sha512", $retHashSeq);

if ($hash != $posted_hash) {
    echo "<div class='alert alert-danger' role='alert'><b>Oh snap!</b> Payment Transaction Unsuccessful .</div>";
} else {

    global $_ObjConnection;
    $_ObjConnection->Connect();
    try {
        if (isset($_POST["udf2"]) && !empty($_POST["udf2"])) {

            echo "<br>";
            echo "<br>";
            echo "<div class='row'>";
            echo "<div class='col-md-8 col-md-offset-2'>";
            echo "<div class='panel panel-success'>";
            echo " <div class='panel-heading'>";
            echo "    <h3 class='panel-title'>Payment Status</h3>";
            echo "  </div>";
            echo "  <div class='panel-body'>";
            echo " <table class='table table-hover table-bordered' >";
            echo "  <tr class=''>";
            echo "    <td class='info' colspan='2' align='center'>";
            echo "<b>Thank You</b> Your Payment is Successful.";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Your Transaction ID is ";

            echo "</td>";
            echo "    <td class=''>";
            echo "$txnid";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";

            echo "We have received a payment of Rs. ";
            echo "</td>";
            echo "    <td class=''>";
            echo "$amount";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Payment For";
            echo "</td>";
            echo "    <td class=''>";
            echo "$productinfo";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Center Code";
            echo "</td>";
            echo "    <td class=''>";
            echo "$udf1";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "User Name";
            echo "</td>";
            echo "    <td class=''>";
            echo "$firstname";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Email";
            echo "</td>";
            echo "    <td class=''>";
            echo "$email";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Date";
            echo "</td>";
            echo "    <td class=''>";
            $date1 = date("d-m-Y");
            echo "$date1";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr>";
            echo "<td colspan='2' align='center'>";
            echo "<input class='hide-from-printer' type='button' value='Print' onclick='window.print()'>";
            echo "</td>";
            echo "  </tr>";
            echo "</table>";

            echo "  </div>";
            echo "</div>";
            echo "</div>";
            echo "</div>";
			
			 global $_ObjConnection;
            $_ObjConnection->Connect();
				
			$_UpdatePayTranQuery = "Update tbl_payment_transaction set Pay_Tran_Status='PaymentReceive', Pay_Tran_Fname='" . $firstname . "' "
									. " Where Pay_Tran_ITGK='" . $udf1 . "' AND Pay_Tran_RKCL_Trnid = '" . $udf2 . "'";
            $_Response3 = $_ObjConnection->ExecuteQuery($_UpdatePayTranQuery, Message::UpdateStatement);

            if ($_Response3[0] == Message::SuccessfullyUpdate) {

                date_default_timezone_set('Asia/Calcutta');
				$correctionTimestamp  = date("Y-m-d H:i:s");
				//$correctionTimestamp = strtotime($mysqlTimestamp);
				$transactionmonth = date('m');
				$transactionyear = date('Y');
                
                $_InsertQuery = "INSERT INTO tbl_correction_transaction (Correction_Transaction_Code, Correction_Transaction_Status, Correction_Transaction_Fname, Correction_Transaction_Amount,"
								. "Correction_Transaction_Txtid, Correction_Transaction_Hash, Correction_Transaction_Key, Correction_Transaction_ProdInfo, Correction_Transaction_Email,"
								. "Correction_Transaction_CenterCode,Correction_Transaction_RKCL_Txid,Correction_Transaction_DateTime,Correction_Transaction_Month,Correction_Transaction_Year) "
								. "Select Case When Max(Correction_Transaction_Code) Is Null Then 1 Else Max(Correction_Transaction_Code)+1 End as Correction_Transaction_Code,"
								. "'" .$status. "' as Correction_Transaction_Status,'" .$firstname. "' as Correction_Transaction_Fname,'" .$amount. "' as Correction_Transaction_Amount,"
								. "'" .$txnid. "' as Correction_Transaction_Txtid,'" .$posted_hash. "' as Correction_Transaction_Hash,'" .$key. "' as Correction_Transaction_Key,"
								. "'" .$productinfo. "' as Correction_Transaction_ProdInfo,'" .$email. "' as Correction_Transaction_Email,'" .$udf1. "' as Correction_Transaction_CenterCode, '" .$udf2. "' as Correction_Transaction_RKCL_Txid, '" .$correctionTimestamp. "' as Correction_Transaction_DateTime, '" .$transactionmonth. "' as Correction_Transaction_Month, '" .$transactionyear. "' as Correction_Transaction_Year"
								. " From tbl_correction_transaction";

								
                $_DuplicateQuery = "Select * From tbl_correction_transaction Where Correction_Transaction_Txtid='" . $txnid . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
				
                if ($_Response1[0] == Message::NoRecordFound) {
                    $_Response2 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
					
                    if ($_Response2[0] == Message::SuccessfullyInsert) {
						$_UpdateQuery = "Update tbl_correction_copy set Correction_Payment_Status = '1' "			
								. "Where Correction_ITGK_Code='" . $udf1 . "' AND Correction_TranRefNo = '" . $txnid . "'";                        
                        $_Response6 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                        if ($_Response6[0] == Message::SuccessfullyUpdate) {
                            
                        } else {
                            $_UpdateCorrectionData = "Update tbl_correction_copy set Correction_Payment_Status = '0' Where Correction_ITGK_Code='" . $udf1 . "' AND Correction_TranRefNo = '" . $txnid . "'";
                            $_Response6 = $_ObjConnection->ExecuteQuery($_UpdateCorrectionData, Message::UpdateStatement);

                            $_UpdatePayTranQuery2 = "Update tbl_payment_transaction set Pay_Tran_Status='PaymentInProcess', Pay_Tran_Fname='" . $firstname . "' "
                                    . " Where Pay_Tran_ITGK='" . $udf1 . "' AND Pay_Tran_RKCL_Trnid = '" . $udf2 . "'";
                            $_Response4 = $_ObjConnection->ExecuteQuery($_UpdatePayTranQuery2, Message::UpdateStatement);

                            $_DeleteCorrectionTran = "Delete FROM tbl_correction_transaction Where Correction_Transaction_Txtid='" . $txnid . "' AND Correction_Transaction_RKCL_Txid='" . $udf2 . "'";
                            $_Response5 = $_ObjConnection->ExecuteQuery($_DeleteCorrectionTran, Message::DeleteStatement);
                        }
                    } else {
                        $_UpdatePayTranQuery2 = "Update tbl_payment_transaction set Pay_Tran_Status='PaymentInProcess', Pay_Tran_Fname='" . $firstname . "' "
                                . "Where Pay_Tran_ITGK='" . $udf1 . "' AND Pay_Tran_RKCL_Trnid = '" . $udf2 . "'";
                        $_Response4 = $_ObjConnection->ExecuteQuery($_UpdatePayTranQuery2, Message::UpdateStatement);
                    }
                } else {
                    $_UpdatePayTranQuery2 = "Update tbl_payment_transaction set Pay_Tran_Status='PaymentInProcess', Pay_Tran_Fname='" . $firstname . "' "
                            . "Where Pay_Tran_ITGK='" . $udf1 . "' AND Pay_Tran_RKCL_Trnid = '" . $udf2 . "'";
                    $_Response4 = $_ObjConnection->ExecuteQuery($_UpdatePayTranQuery2, Message::UpdateStatement);

//                    $_DeleteReexamTran = "Delete FROM tbl_reexam_transaction Where Reexam_Transaction_Txtid='" . $txnid . "' AND Reexam_Transaction_RKCL_Txid='" . $udf2 . "'";
//                    $_Response5 = $_ObjConnection->ExecuteQuery($_DeleteAdmissionTran, Message::DeleteStatement);
                }
            }
        }

			else {
            echo "<br>";
            echo "<br>";
            echo "<div class='row'>";
            echo "<div class='col-md-8 col-md-offset-2'>";
            echo "<div class='panel panel-success'>";
            echo " <div class='panel-heading'>";
            echo "    <h3 class='panel-title'>Payment Status</h3>";
            echo "  </div>";
            echo "  <div class='panel-body'>";
            echo " <table class='table table-hover table-bordered' >";
            echo "  <tr class=''>";
            echo "    <td class='info' colspan='2' align='center'>";
            echo "<b>Thank You</b> Your Payment is Successful Please wait for Confirmation.";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Your Transaction ID is ";

            echo "</td>";
            echo "    <td class=''>";
            echo "$txnid";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";

            echo "We have received a payment of Rs. ";
            echo "</td>";
            echo "    <td class=''>";
            echo "$amount";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Payment For";
            echo "</td>";
            echo "    <td class=''>";
            echo "$productinfo";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Center Code";
            echo "</td>";
            echo "    <td class=''>";
            echo "$udf1";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "User Name";
            echo "</td>";
            echo "    <td class=''>";
            echo "$firstname";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Email";
            echo "</td>";
            echo "    <td class=''>";
            echo "$email";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Date";
            echo "</td>";
            echo "    <td class=''>";
            $date1 = date("d-m-Y");
            echo "$date1";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr>";
            echo "<td colspan='2' align='center'>";
            echo "<input class='hide-from-printer' type='button' value='Print' onclick='window.print()'>";
            echo "</td>";
            echo "  </tr>";
            echo "</table>";

            echo "  </div>";
            echo "</div>";
            echo "</div>";
            echo "</div>";

            $_UpdatePayTranQuery = "Update tbl_payment_transaction set Pay_Tran_PG_Trnid = '" . $txnid . "', Pay_Tran_Status='LearnerNotConfirmed', Pay_Tran_Fname='" . $firstname . "' "
                    . "Where Pay_Tran_ITGK='" . $udf1 . "' AND Pay_Tran_RKCL_Trnid = '" . $udf2 . "'";
            $_Response3 = $_ObjConnection->ExecuteQuery($_UpdatePayTranQuery, Message::UpdateStatement);
        }
    } catch (Exception $_e) {
        $_Response[0] = $_e->getTraceAsString();
        $_Response[1] = Message::Error;
    }
}
?>	
</div>
</div>
<style>
    @media print {
        /* style sheet for print goes here */
        .hide-from-printer{  display:none; }
    }
</style>
<?php include ('footer.php'); ?>

</body>
</html>

