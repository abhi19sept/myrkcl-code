<?php
$title="Name Correction Payment";
include ('header.php'); 
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var FunctionCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
echo "<script>var CenterCode=" . $_SESSION['User_Code'] . "</script>";
  //echo "<script>var CenterCode='" . $_SESSION['User_LoginId'] . "'</script>";
  //echo "<script>var amount= " . $_REQUEST['amount'] . " </script>";
   echo "<script>var PayType= 'Correction Fee Payment' </script>";
   $payutxnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
echo "<script>var PayUTranID= '".$payutxnid."' </script>";

		 $random= (mt_rand(1000, 9999));
         $random.= date("y");
         $random.= date("m");
         $random.= date("d");
         $random.= date("H");
         $random.= date("i");
         $random.= date("s");
		 //echo $random;
	
   echo "<script>var RKCLTxnId= '".$random."' </script>"; 
//print_r($_SESSION);

require("razorpay/checkout/manual.php");
?>
<div style="min-height:430px !important;max-height:auto !important;">	
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Correction Application  Form  Report</div>
                <div class="panel-body">
				 <form name="frmcorrectionfeepayment" id="frmcorrectionfeepayment" class="form-inline" role="form" enctype="multipart/form-data">
					<div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
							
							<div class="col-sm-4 form-group">     
										<label for="correction">Select Correction Name:</label>
										<select id="ddlCorrection" name="ddlCorrection" class="form-control">
										
										</select>
							</div> 
						<div>
						
						<div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Payment Mode:</label>
                        <select id="paymode" name="paymode" class="form-control" onchange="toggle_visibility1('online_mode');">

                        </select>									
                    </div> 
					
					<div class="col-sm-4 form-group">     
                        <label for="gender">Choose Payment Gateway:</label> <br />
                        <label class="radio-inline"> <input type="radio" id="gatewayPayu" checked="checked" class="gateway" name="gateway" value="payu" onchange="toggle_visibility1('online_mode');"/> Payu </label>
                        <label class="radio-inline"> <input type="radio" id="gatewayRazor" class="gateway" name="gateway" value="razorpay" onchange="toggle_visibility1('online_mode');"/> Razorpay </label>
                    </div>

					
                </div>

                <div class="container" id="online_mode" style="display:none;">
                    <div class="col-md-12 form-group">                                
                        <label  style="display:none;" class="radio-inline"> <input type="radio" style="display:none;" id="PayUMoney" value="payumoney"  name="Role" checked="true" /> Pay Using PayUMoney </label>
                        <!-- <label class="radio-inline"> <input type="radio" id="PayDDMode" value="emitra" name="Role" /> Pay Using e-Mitra Gateway </label>                                
                        <label class="radio-inline"> <input type="radio" id="RoleITGK"  name="Role" /> ITGK </label>-->
                                                    <!--<label class="radio-inline"> <input type="radio" id="genderFemale" name="gender" /> Female </label>-->
                    </div> 
                </div>
									<input type="hidden" name="amounts" id="amounts" class="form-control"/>
									<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId" value="<?php echo $random; ?>"/>
									<input type="hidden" class="form-control" maxlength="50" name="txtGeneratePayUId" id="txtGeneratePayUId" value="<?php echo $payutxnid; ?>"/>
									<input type="hidden" name="txtapplicationfor" id="txtapplicationfor" class="form-control"/>
								</div>
					</div>
                         <div id="gird" style="margin-top:10px;"> </div>                   
                 
				 <div class="container" id="submit" style="display:none;">
									<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
								</div>
				</div>
            </div>   
        </div>
    </form>
	</div>
   
		<form id="frmpostvalue" name="frmpostvalue" action="frmcorrectionpayment.php" method="post">
                <input type="hidden" id="code" name="code">
                <input type="hidden" id="paytype" name="paytype"> 
				<input type="hidden" id="paytypename" name="paytypename"> 
				<input type="hidden" id="RKCLtranid" name="RKCLtranid"/>
				<input type="hidden" id="amount" name="amount">	
				 <input type="hidden" id="txnid" name="txnid">
        </form>		

		<form id="frmwithoutfeepostvalue" name="frmwithoutfeepostvalue" action="frmwithoutfeecorrectionpayment.php" method="post">
			<input type="hidden" id="withoutfeecode" name="withoutfeecode">
			<input type="hidden" id="withoutfeepaytype" name="withoutfeepaytype">                
			<input type="hidden" id="withoutfeeamount" name="withoutfeeamount">				
		</form>

		<form id="frmddcorrectionpayment" name="frmddcorrectionpayment" action="frmddcorrectionpayment.php" method="post">
			<input type="hidden" id="ddcode" name="ddcode">
			<input type="hidden" id="ddpaytype" name="ddpaytype">                
			<input type="hidden" id="ddamount" name="ddamount">

		</form>
			
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?> 

<script type="text/javascript">
<!--

    function toggle_visibility1(id) {
        var e = document.getElementById(id);
        var f = document.getElementById('paymode').value;
        if (f == "1")
        {
            e.style.display = 'block';
        }
        else {
            e.style.display = 'none';
        }
    }
//-->
</script>  
               
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
			
			        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                }
            });
        }
        //GenerateUploadId();
			
			function FillCorrectionName() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee_Ver1.php",
                    data: "action=FILL",
                    success: function (data) {
						//alert(data);
                        $("#ddlCorrection").html(data);
						 
                    }
                });
            }
            FillCorrectionName();
			
			$("#ddlCorrection").change(function(){				
				var DocCode = $('#ddlCorrection').val();
				//alert(BatchCode);
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee_Ver1.php",
                    data: "action=Fee&codes=" + DocCode + "",
                    success: function (data) {  
					//alert(data);
                    amounts.value = data;				
                    }
                });
			});
			
			$("#ddlCorrection").change(function () {
				showpaymentmode(ddlCorrection.value);
				GetApplicationName(ddlCorrection.value);
			});

				function GetApplicationName(val) {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee_Ver1.php",
                    data: "action=GetApplicationName&for=" + val + "",
                    success: function (data) {
						//alert(data);
						txtapplicationfor.value = data;
                        //$("#txtapplicationfor").html(data);
						 
                    }
                });
            }
			
				function showpaymentmode(val) {
					//alert(val);
					$.ajax({
						type: "post",
						url: "common/cfEvents.php",
						data: "action=SHOWCorrectionPay&code=" + val + "",
						success: function (data) {
							//alert(data);
							$("#paymode").html(data);							
						}
					});
				}
		
		$("#paymode").change(function () {
			//showData(txtapplicationfor.value, ddlCorrection.value, paymode.value);
			
			var gateway = $(".gateway").val();
            if (gateway != "") {
			    showData(txtapplicationfor.value, ddlCorrection.value, paymode.value);
            }
			
		});
		 $(".gateway").click(function () {
            if (this.value == "") {
                showData(txtapplicationfor.value, ddlCorrection.value, this.value);
            } else {
                showData(txtapplicationfor.value, ddlCorrection.value, paymode.value);
            }
        });


            function showData(val,val1,val2) {
                //alert(val);
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee_Ver1.php",
                    data: "action=SHOW&application=" + val + "&correctioncode=" + val1 + "&paymode=" + val2 + "",
                    success: function (data) {
                            //alert(data);
                        $("#gird").html(data);
						$('#example').DataTable({
							    scrollY:        400,
								scrollCollapse: true,
								paging:         false
						});
						$("#submit").show();
                    }
                });
            }

            //showData();
			$("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfCorrectionFee_Ver1.php"; // the script where you handle the form input.
                var data;
                var forminput=$("#frmcorrectionfeepayment").serialize();
                //alert(forminput);
                
                if (Mode == 'Add')
                {
                    data = "action=ADD&" + forminput; // serializes the form's elements.
                }
                else
                {
                    //data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        $('#response').empty();
                          if (data == "Invalid") {
                              $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please Checked Atleast one checkbox to pay Learner Correction Payment." + "</span></p>");
                          }
                          else {
                            //$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" +  + "</span></p>");
                            data = $.parseJSON(data);
                            if (data.payby == 'payu') {
                                $('#code').val(CenterCode);
                                $('#paytype').val(PayType); 
                                
                                var payname = 'Correction Fee Payment';
                                
                                $('#RKCLtranid').val(RKCLTxnId);
                                
                                $('#txnid').val(PayUTranID);
                                
                                var rkcltranid = $('#txtGenerateId').val();
                                $('#RKCLtranid').val(rkcltranid)
                                $('#paytypename').val(payname);
                            
                                amount.value = data.amount;
                                
                                $('#ddcode').val(CenterCode);
                                $('#ddpaytype').val(PayType);                    
                                ddamount.value = data.amount;
                                
                                var paymode2 = $('#paymode').val();
                        
                                if (paymode2 == "2") {
                                    $('#frmddcorrectionpayment').submit();
                                }
                                else if (paymode2 == "4") {
                                    $('#frmwithoutfeepostvalue').submit();
                                }
                                else {
                                    $('#frmpostvalue').submit();
                                }
                            } else {
                                var options = data;
							//	alert(options);
                                <?php include("razorpay/razorpay.js"); ?>
                            }
                        }
                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
			
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }


        });

    </script>
</html>