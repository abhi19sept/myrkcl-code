<html>
    <head>
        <title>Print Transaction Details</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="bootcss/css/bootstrap.min.css">
        <script src="bootcss/js/bootstrap.min.js"></script>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">

        <style type="text/css">

            .division_heading {
                border-bottom: 1px solid #e5e5e5;
                padding-bottom: 10px;
                font-size: 20px;
                color: #000;
                margin-bottom: 20px;

            }

            .addBottom {
                margin-bottom: 12px;
            }

        </style>

    </head>
    <body>
        <?php
            session_start();
        ?>

        <div class="container" id="showdata">
            <div class="panel panel-primary" style="margin-top:46px !important;">
                <div class="panel-heading">Transaction Details</span></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="https://myrkcl.com/images/myrkcl_logo.png" alt='Logo' data-default="MyRKCL Logo"/>
                        </div>
                        <div class="col-md-8">
                            <div class="division_heading">
                                Transaction Details for Reference ID: <span id="fld_ref_no"></span>
                            </div>


                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12 addBottom">
                                        Payment Title : <span id="fld_paymentTitle">Success</span>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Payment Status : <span class="text-success">Success</span>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        ITGK Code : <span id="fld_ITGK_Code"></span>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Transaction Reference Number : <span id="fld_transactionID"></span>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Bank Reference Number: <span id="fld_bankTransactionID"></span>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Transaction Date and Time: <span id="fld_updatedOn"></span>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Transaction Amount: <span id="fld_amount"></span>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a type="button" class="btn btn-danger" onclick="window.print();"><i class="fa fa-print" aria-hidden="true"></i>
                                &nbsp;Print</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </body>
</html>
<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>
<script>
    $.ajax({
        type: "post",
        url: "common/cfmodifyITGK.php",
        data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code']?>'),
        success: function (data2) {
            data2 = $.parseJSON(data2);
            $("#fld_ref_no").html(data2[0].fld_ref_no);
            $("#fld_paymentTitle").html(data2[0].fld_paymentTitle);
            $("#title").html(data2[0].fld_ref_no);
            $("#fld_ITGK_Code").html(data2[0].fld_ITGK_Code);
            $("#fld_transactionID").html(data2[0].fld_transactionID);
            $("#fld_bankTransactionID").html(data2[0].fld_bankTransactionID);
            $("#fld_updatedOn").html(data2[0].fld_updatedOn);
            $("#fld_amount").html(data2[0].fld_amount);
        }
    });
</script>
