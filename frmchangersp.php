<?php
$title = "RSP Selection";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Choose New Service Provider</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">Current Service Provider's Registered - Details</div>
                        <div class="panel-body">

                            <div class="container">
                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Name of Organization/Center:</label>
                                    <textarea class="form-control" readonly="true" name="txtrspname" id="txtrspname" placeholder="Name of the Organization/Center"></textarea>
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">Mobile No:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtmobno" id="txtmobno" placeholder="Registration No">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="faname">Date of Establishment:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtdate" id="txtdate"  placeholder="YYYY-MM-DD">
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Type of Organization:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtRspType" id="txtRspType" placeholder="Type Of Organization">  
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="panel panel-info">
                        <div class="panel-heading">Select New Service Provider's Registered Details</div>
                        <div class="panel-body">
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Select RSP Name:<span class="star">*</span></label>
                                <select id="ddlRsp" name="ddlRsp" class="form-control" >

                                </select>    
                            </div>

                            <div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
                            </div>  
                        </div>
                    </div>


                    <div class="panel panel-info" id="main-content" style="display:none;">
                        <div class="panel-heading">Selected New Service Provider Details</div>
                        <div class="panel-body">

                            <div class="container">
                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Name of Organization/Center:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">Mobile No:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="faname">Date of Establishment:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Type of Organization:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtType" id="txtType" placeholder="Type Of Organization">  
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">District:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Tehsil:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="address">Street:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtStreet" id="txtStreet" placeholder="Street">    
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="address">Road:</label>
                                    <input type="text" class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road">

                                </div>
                            </div>

                        </div>
                    </div> 
                    <div class="container">

                        <input type="button" name="btnshowotp" id="btnshowotp" class="btn btn-primary" value="Generate OTP" style="display:none;"/>
                    </div>

                    <div class="panel panel-info" id="otpverify" style="display:none;">
                        <div class="panel-heading">Verify Registered Authentication Details</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="email">Enter Registered Email of IT-GK:</label>
                                    <input type="text" class="form-control" maxlength="50" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="Mobile">Enter Registered Mobile Number of IT-GK:</label>
                                    <input type="text" class="form-control" maxlength="10" name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <label for="otp">Enter OTP:</label>
                                    <input type="text" class="form-control" maxlength="6" name="txtOtp" id="txtOtp"  placeholder="Enter OTP">
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <input type="button" name="btnVerify" id="btnVerify" class="btn btn-primary" value="Verify" style="margin-top:25px"/>
                                    <!--<input type="button" name="btnVerified" id="btnVerified" class="btn btn-primary" value="Details Verified" style="margin-top:25px; display:none"/>-->
                                    <label for="tnc" id="btnVerified" style="margin-top:25px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Details Verified</b></label></br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="container" id="tnc" style="display:none"> 
                        <div class="col-sm-4 form-group"> 
                            <label for="empid">Select Reason to change Service Provider:<span class="star">*</span></label>
                            <select id="ddlReason" name="ddlReason" class="form-control" required>
                                <option value="">Select</option>
                                <option value="Unsatisfactory Services by Service Provider">Unsatisfactory Services by Service Provider</option>
                                <option value="Withdrawal by Service Provider">Withdrawal by Service Provider</option>
                                <option value="Service Provider termination by RKCL">Service Provider termination by RKCL</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="Mobile">Enter Reason Remarks:<span class="star">*</span></label>
                            <textarea class="form-control" name="txtReason" id="txtReason"  placeholder="Reason" style="text-transform:uppercase" onkeypress="javascript:return validAddress(event);" required></textarea>
                        </div>
                        <div class="col-md-12"> 							
                            <label for="tnc">Terms and conditions :<span class="star">*</span></label></br>
                            <label class="col-md-12" for="tnc">1) I have read all the terms and conditions mentioned in the EOI document and agree to abide by the same. :<span class="star">*</span></label></br> 
                            <label class="col-md-12" for="tnc">2) I am selecting my Service Provider under full consciousness and I am not influenced by any kind of pressure from anyone.<span class="star">*</span></label></br> 
                            <label class="col-md-12" for="tnc">3) If my SP change request is approved by RKCL, I agree to sign an agreement as per EOI terms and conditions with the selected Service Provider within 15 days of approval.<span class="star">*</span></label></br> 
                            <label class="checkbox-inline"> <input type="checkbox" name="chk" id="chk" value="" required>
                                <a title="" style="text-decoration:none;" href="http://www.rkcl.in/download/CorrigendumRevisedEOI1.pdf" target="_blank"><span id="fix">I Accept all Terms and Conditions</span> </a>

                            </label>								
                        </div>
                    </div>	
                    <br>
                    <br>
                    <!--                    <div class="container">
                                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none"/>
                                        </div>-->
                    <br>
                    <div class="container">

                        <button type="button" class="btn btn-primary" name="btnSubmit" id="btnSubmit" data-toggle="modal" data-target="#myModal" style="display:none">Submit</button>

                        <!-- Modal -->
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Important Notification</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Are You Sure you want to change your Service Provider!</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" id="btn1" data-dismiss="modal">YES</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <br><br>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">Change Service Provider Details - History</div>
                        <div class="panel-body">
                            <div id="gird"></div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>  

</div>
</body>

<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
function CheckEligiblity() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfChangeRsp.php",
                data: "action=CHECKITGK",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "No Record Found") {
                        $('#response1').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " You are Not Allowed to Apply. " + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; You are not allowed to Apply SP Change Application.</span>");
                        window.setTimeout(function () {
                                window.location.href = "dashboard.php";
                            }, 3000);
                    }
                    else if(data == "Success")  {
                      FillRsp(); 
                      FillCurrentRsp();
                      showData();
                    } 
                    else {
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; You are not allowed to Apply SP Change Application.</span>");
                        window.setTimeout(function () {
                                window.location.href = "dashboard.php";
                            }, 3000);
                    }
                }
            });
        }
        CheckEligiblity();

        $("#btn1").click(function () {
            
            if ($("#form").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfChangeRsp.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#form").serialize();
                if (Mode == 'Add')
                {
                    data = "action=ADD&" + forminput; //serializes the form's elements.
                    //alert(data);
                }
                else
                {
                    data = "action=UPDATE&code=" + StatusCode + "&name=" + txtStatusName.value + "&description=" + txtStatusDescription.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmchangerspmsg.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("form");
                            
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> You request for changing the Service Provider is already pending with RKCL.</span></p>");
                            $( ".close" ).trigger( "click" );
                            $( ".modal-backdrop.in").hide();
                        }
                        //showData();


                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });


        function FillRsp() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfSelectRsp.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlRsp").html(data);
                }
            });
        }
     //   FillRsp();

        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post", url: "common/cfChangeRsp.php",
                data: "action=SHOW",
                success: function (data) {
                    $('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print']
                    });



                }
            });
        }

    //    showData();


        $("#ddlRsp").change(function () {
            $("#btnShow").show();
            $("#btnSubmit").hide();
            $("#btnshowotp").hide();
            $("#verification").hide();
            $("#tnc").hide();

        });

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }

        function FillCurrentRsp() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfChangeRsp.php",
                data: "action=GETRSP",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Serice Provider Not Found " + "</span></p>");
                    }
                    else {
                        data = $.parseJSON(data);
                        txtrspname.value = data[0].rspname;
                        txtmobno.value = data[0].mobno;
                        txtdate.value = data[0].date;
                        txtRspType.value = data[0].rsptype;
                    }
                }
            });
        }
//        FillCurrentRsp();

        $("#btnShow").click(function () {
            //alert("OTP has been sent to your Registered Mobile Number. Please Enter OTP to Proceed");
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfSelectRsp.php",
                data: "action=DETAILS&values=" + ddlRsp.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select a RSP for Selection" + "</span></p>");
                    }
                    else {
                        data = $.parseJSON(data);
                        txtName1.value = data[0].orgname;
                        txtRegno.value = data[0].regno;
                        txtEstdate.value = data[0].fdate;
                        txtType.value = data[0].orgtype;

                        txtDistrict.value = data[0].district;
                        txtTehsil.value = data[0].tehsil;
                        txtStreet.value = data[0].street;
                        txtRoad.value = data[0].road;

                        $("#main-content").show(3000);
                        $("#btnShow").hide(3000);
                        $("#btnshowotp").show(3000);

                    }

                }
            });
        });


        $("#btnshowotp").click(function () {
            alert("OTP has been sent to your Registered Mobile Number. Please Enter OTP to Proceed");
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfSelectRsp.php",
                data: "action=SENDOTP&values=" + ddlRsp.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate) {

                        $("#btnshowotp").hide();
                        $("#otpverify").show(3000);
                        $("#verification").show();

                    }
                    else {

                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   OTP already Generated, Please Try again." + "</span></p>");

                    }

                }
            });
        });

        $("#btnVerify").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfSelectRsp.php",
                data: "action=VERIFY&email=" + txtEmail.value + "&mobile=" + txtMobile.value + "&opt=" + txtOtp.value + "",
                success: function (data)
                {
                    //alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $("#btnVerify").hide();
                        $("#btnVerified").show();
                        $('#btnSubmit').show(3000);
                        $('#tnc').show(3000);
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                    }
                }
            });
        });


//        $("#btnSubmit").click(function () {
//            if ($("#form").valid())
//            {
//                $('#response').empty();
//                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
//                var url = "common/cfChangeRsp.php"; // the script where you handle the form input.
//                var data;
//                var forminput = $("#form").serialize();
//                if (Mode == 'Add')
//                {
//                    data = "action=ADD&" + forminput; //serializes the form's elements.
//                    //alert(data);
//                }
//                else
//                {
//                    data = "action=UPDATE&code=" + StatusCode + "&name=" + txtStatusName.value + "&description=" + txtStatusDescription.value + ""; // serializes the form's elements.
//                }
//                $.ajax({
//                    type: "POST",
//                    url: url,
//                    data: data,
//                    success: function (data)
//                    {
//                        //alert(data);
//                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
//                        {
//                            $('#response').empty();
//                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
//                            window.setTimeout(function () {
//                                window.location.href = "frmchangersp.php";
//                            }, 1000);
//
//                            Mode = "Add";
//                            resetForm("form");
//                        }
//                        else
//                        {
//                            $('#response').empty();
//                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> You have already sumitted application for Service Provider change</span></p>");
//                        }
//                        //showData();
//
//
//                    }
//                });
//            }
//            return false; // avoid to execute the actual submit of the form.
//        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmchangersp_validation.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>