<?php
$title = "Admission Payment";
include ('header.php');
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
require("razorpay/checkout/manual.php");
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Fee Payment</div>
            <div class="panel-body">
                <form name="frmfeepayment" id="frmfeepayment" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                        <div class="col-sm-4 form-group">     
                            <label for="course">Select Course:</label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">

                            </select>
                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Batch:</label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>									
                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Payment Mode:</label>
                            <select id="paymode" name="paymode" class="form-control">

                            </select>
                        </div> 
                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Payment Gateway:</label>
                            <select id="gateway" name="gateway" class="form-control">
                               
                           <option value="razorpay">Razorpay</option>
						   <option value="payu">Payu</option>
							
							
                            </select>
                        </div>
                    </div>
                    <div>
                        <input type="hidden" name="amounts" id="amounts"/>
                    </div>
                    <div id="menuList" name="menuList" style="margin-top:5px;"> </div> 

                    <div class="container">
                        <br><input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                    </div>
            </div>
        </div>   
    </div>
</form>
</div>
<form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
    <input type="hidden" id="txnid" name="txnid">
</form>

</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLAdmissionSummaryCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlBatch").change(function () {
            var BatchCode = $('#ddlBatch').val();
            //alert(BatchCode);
            $.ajax({
                type: "post",
                url: "common/cfFeePayment.php",
                data: "action=Fee&codes=" + BatchCode + "",
                success: function (data) {
                    //alert(data);
                    amounts.value = data;
                }
            });
        });

        $("#ddlCourse").change(function () {
            $("#menuList").html('');
            $('#btnSubmit').hide();
            $("#ddlBatch").html('');
            $("#paymode").html('');
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlBatch").html(data);
                }
            });

        });

        $("#ddlBatch").change(function () {
            showpaymentmode(ddlBatch.value, ddlCourse.value);
        });

        function showpaymentmode(val, val1) {
            $("#menuList").html('');
            $('#btnSubmit').hide();
            $("#paymode").html('');
            $.ajax({
                type: "post",
                url: "common/cfEvents.php",
                data: "action=SHOWAdmissionPay&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#paymode").html(data);
                }
            });
        }
        function showAllData(val, val1, val2) {
            $("#menuList").html('');
            $('#btnSubmit').hide();
            $.ajax({
                type: "post",
                url: "common/cfFeePayment.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "&paymode=" + val2 + "",
                success: function (data) {
                    // alert(data);
                    $("#menuList").html(data);
                    $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                    $('#btnSubmit').show();
                }
            });
        }

        $("#paymode").change(function () {
            showAllData(ddlBatch.value, ddlCourse.value, paymode.value);
        });

        $("#gateway").change(function () {
            if (this.value == "") {
                showAllData(ddlBatch.value, ddlCourse.value, this.value);
            } else {
                showAllData(ddlBatch.value, ddlCourse.value, paymode.value);
            }
        });

		$("#menuList").on("click",".view_photo",function(){
			var batchname = $(this).attr("batchname");
			var image = $(this).attr("image");
			var count = $(this).attr("count");
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfFeePayment.php",
                data: "action=ViewPhoto&batchname=" + batchname + "&image=" + image + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                   // $("#"+count+"_pic").show();
                    $("#"+count+"_photo").hide();
					$("#Viewphoto_"+count).html(data);
					$("#Viewphoto_"+count).show();
                }
            });
		});
		
		$("#menuList").on("click",".view_sign",function(){
			var batchname = $(this).attr("batchname");
			var image = $(this).attr("image");
			var count = $(this).attr("count");
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfFeePayment.php",
                data: "action=ViewSign&batchname=" + batchname + "&image=" + image + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    //$("#"+count+"_signature").show();
                    $("#"+count+"_sign").hide();
					$("#Viewsign_"+count).html(data);
					$("#Viewsign_"+count).show();
                }
            });
		});
		
        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $('#btnSubmit').hide();
            var url = "common/cfFeePayment.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmfeepayment").serialize();
            //alert(forminput);

            if (Mode == 'Add') {
                data = "action=ADD&" + forminput;
                $('#txnid').val('');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#response').empty();
                        data = data.trim();
                        if (data == 0 || data == '') {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Please try again, also ensure that, you have selected atleast one checkbox." + "</span></p>");
                        } else if (data == 'TimeCapErr') {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                        } else {
                            if ($('#gateway').val() == 'razorpay') {
                                var options = $.parseJSON(data);
                                <?php include("razorpay/razorpay.js"); ?>
                            } else {
                                $('#txnid').val(data);
                                $('#frmpostvalue').submit();
                            }
                        }
                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.
        });

    });

</script>
</body>

</html>