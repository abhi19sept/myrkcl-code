<?php
$title = "Learner Correction Payment";
include ('header.php');
include ('root_menu.php');

echo "<div style='min-height:430px !important;max-height:1500px !important;'>";
echo "<div class='container'>";

$status=$_POST["status"];
$firstname=$_POST["firstname"];
$amount=$_POST["amount"];
$txnid=$_POST["txnid"];
$posted_hash=$_POST["hash"];
$key=$_POST["key"];
$productinfo=$_POST["productinfo"];
$email=$_POST["email"];
$udf1=$_POST["udf1"];
$udf2=$_POST["udf2"];
$salt="8IaBELXB";

require 'DAL/classconnection.php' ;
	
$_ObjConnection=new _Connection();
$_Response=array();


If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'|||||||||'.$udf2.'|'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        
                  }
	else {	  

        $retHashSeq = $salt.'|'.$status.'|||||||||'.$udf2.'|'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

         }
		 $hash = hash("sha512", $retHashSeq);
  
       if ($hash != $posted_hash) {
	        echo "<div class='alert alert-danger' role='alert'><b>Oh snap!</b> Payment Transaction Unsuccessful .</div>";
		   }
	   else {
	echo "<br>";
    echo "<br>";
    echo "<div class='row'>";
    echo "<div class='col-md-8 col-md-offset-2'>";
    echo "<div class='panel panel-danger'>";
    echo " <div class='panel-heading'>";
    echo "    <h3 class='panel-title'>Payment Status</h3>";
    echo "  </div>";
    echo "  <div class='panel-body'>";
    echo " <table id='example' class='table table-hover table-striped'>";
    echo "  <tr class=''>";
    echo "    <td class='info' colspan='2'>";
    echo "<b>Oh snap</b> Your Payment is Failed";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Your Transaction ID is <br> You may try making the payment by clicking the link below.";
    echo "<p><a href=https://myrkcl.com/frmLearnerCorrectionFee.php> Try Again</a></p>";
    echo "</td>";
    echo "    <td class=''>";
    echo "$txnid";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Payment For";
    echo "</td>";
    echo "    <td class=''>";
    echo "$productinfo";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Center Code";
    echo "</td>";
    echo "    <td class=''>";
    echo "$udf1";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "User Name";
    echo "</td>";
    echo "    <td class=''>";
    echo "firstname";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Email";
    echo "</td>";
    echo "    <td class=''>";
    echo "$email";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Date";
    echo "</td>";
    echo "    <td class=''>";
    $date1 = date("d-m-Y");
    echo "$date1";
    echo "</td>";
    echo "  </tr>";
	echo "  <tr>";
    echo "<td colspan='2' align='center'>";
    echo "<input class='hide-from-printer' type='button' value='Print' onclick='window.print()'>";
    echo "</td>";
    echo "  </tr>";
    echo "</table>";

    echo "  </div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";

          
		  global $_ObjConnection;
        $_ObjConnection->Connect();
        try {
				//$_User_Code =   $_SESSION['User_Code'];
			$_InsertQuery = "INSERT INTO tbl_correction_transaction (Correction_Transaction_Code, Correction_Transaction_Status, Correction_Transaction_Fname, Correction_Transaction_Amount,"
								. "Correction_Transaction_Txtid, Correction_Transaction_Hash, Correction_Transaction_Key, Correction_Transaction_ProdInfo, Correction_Transaction_Email,"
								. "Correction_Transaction_CenterCode,Correction_Transaction_RKCL_Txid) "
								. "Select Case When Max(Correction_Transaction_Code) Is Null Then 1 Else Max(Correction_Transaction_Code)+1 End as Correction_Transaction_Code,"
								. "'" .$status. "' as Correction_Transaction_Status,'" .$firstname. "' as Correction_Transaction_Fname,'" .$amount. "' as Correction_Transaction_Amount,"
								. "'" .$txnid. "' as Correction_Transaction_Txtid,'" .$posted_hash. "' as Correction_Transaction_Hash,'" .$key. "' as Correction_Transaction_Key,"
								. "'" .$productinfo. "' as Correction_Transaction_ProdInfo,'" .$email. "' as Correction_Transaction_Email,'" .$udf1. "' as Correction_Transaction_CenterCode, '" .$udf2. "' as Correction_Transaction_RKCL_Txid"
								. " From tbl_correction_transaction";
            
            $_DuplicateQuery = "Select * From tbl_correction_transaction Where Correction_Transaction_Txtid='" . $txnid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				$_UpdatePayTranQuery = "Update tbl_payment_transaction set Pay_Tran_PG_Trnid = '" . $txnid . "', Pay_Tran_Status='LearnerNotConfirmed', Pay_Tran_Fname='" .$firstname. "' "			
								. "Where Pay_Tran_ITGK='" . $udf1 . "' AND Pay_Tran_RKCL_Trnid = '" . $udf2 . "'";								
				$_Response3=$_ObjConnection->ExecuteQuery($_UpdatePayTranQuery, Message::UpdateStatement);
				
				
				//header('location:frmapplyeoi.php');
            }
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;            
        }
           
		   }          
?>
<!--Please enter your website homepagge URL -->
</div>
</div>
<style>
    @media print {
        /* style sheet for print goes here */
        .hide-from-printer{  display:none; }
    }
</style>
<?php include ('footer.php'); ?>

</body>

</html>

