<?php
$title = "EOI Payment Transaction";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='Show'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">EOI Payment Transaction Report</div>
            <div class="panel-body">					
                <form name="frmeoipaymentrpt" id="frmeoipaymentrpt" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-4 form-group"> 

                        </div>						

                        <div class="col-sm-4 form-group"> 
                            <label for="sdate">Start Date:</label>
                            <span class="star">*</span>
                            <input type="text" class="form-control" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="DD-MM-YYYY">     
                        </div>

                        <div class="col-sm-4 form-group">     
                            <label for="edate">End Date:</label>
                            <span class="star">*</span>
                            <input type="text" class="form-control" readonly="true" name="txtenddate" id="txtenddate"  placeholder="DD-MM-YYYY" value=" <?php echo date("d-m-Y"); ?>">
                        </div>

                        <div class="col-sm-4 form-group"> 

                        </div>
                    </div>  

                    <div class="container">
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View Report"/>    
                    </div>

                    <div id="grid" style="margin-top:35px;"> </div> 
                    <div class="container">
                    <iframe id="testdoc" src="" style="width: 100%;height: 500px;border: none; display: none;"></iframe>
                    </div>      
            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script>		

<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $("#txtstartdate, #txtenddate").datepicker();
    $("#txtenddate").change(function () {
        var txtstartdate = document.getElementById("txtstartdate").value;
        var txtenddate = document.getElementById("txtenddate").value;
        if ((Date.parse(txtenddate) <= Date.parse(txtstartdate))) {
            alert("End date should be greater than Start date");
            document.getElementById("txtenddate").value = "";
        }
    });

    $(document).ready(function () {


        $("#btnSubmit").click(function () {
            if ($("#frmeoipaymentrpt").valid())
            {
                $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
                setTimeout(function () {
                    $('#grid').load();
                }, 2000);

                var startdate = $('#txtstartdate').val();
                var enddate = $('#txtenddate').val();
                var url = "common/cfPrintRecpEoi.php"; // the script where you handle the form input.
                var data = "action=SHOW&startdate=" + startdate + "&enddate=" + enddate + ""; // serializes the form's elements.	

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {

                        $("#grid").html(data);
                        $('#example').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]
                        });


                        //showData();
                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        $("#grid").on('click', '.approvalLetter', function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            var cid = $(this).attr('cid');
            var eid = $(this).attr('eid');

            $.ajax({
                type: "post",
                url: "common/cfPrintRecpEoi.php",
                data: "action=DwnldPrintRecp&cid=" + cid + "&eid=" + eid + "",
                success: function (data) {
                  
                    $('#response').empty();

                    window.open(data, '_blank');
                    if(data == 'error'){
                    $('#response').empty();
                    $('#response').append("<p class='error'>Receipt Not Available Right Now.</span></p>");
                    }
                    else{
                        $("#testdoc").attr('src', data);
                    }
                 //   window.setTimeout(function () {
                 //       delgeninvoice(data);
                 //   }, 3000);

                }
            });
        });

        function delgeninvoice(data) {
            //alert(data);
            $.ajax({
                type: "post",
                url: "common/cfPrintRecpEoi.php",
                data: "action=delgeninvoice&values=" + data,
                success: function (data) {

                }
            });
        }
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
</html>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmeoipaymentrpt_valid.js" type="text/javascript"></script>	