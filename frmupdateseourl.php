<?php
$title = "Update SEO URL";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var FunctionCode=0</script>";
    echo "<script>var Mode='UPDATE'</script>";
}
if ($_SESSION['User_Code'] == '1') {
    ?>
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Update SEO URL</div>
            <div class="panel-body">
                <form name="frmupdateseourl" id="frmupdateseourl" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-4">     
                            <label for="learnercode"> Root Menu Name:</label>
                            <select id="ddlRoot" name="ddlRoot" class="form-control">

                            </select>
                        </div>
                        <div class="col-md-4">     
                            <label for="learnercode"> Parent Menu Name:</label>
                            <select id="ddlParent" name="ddlParent" class="form-control">

                            </select>
                        </div>
                        <div class="col-md-4 ">     
                            <label for="learnercode"> Child Menu Name:</label>
                            <select id="ddlChild" name="ddlChild" class="form-control">

                            </select>
                        </div> 
                    </div>  
                    <div class="col-md-12" style="margin-top: 20px;">
                        <div class="col-sm-4">     
                            <label for="order">Function URL:</label>
                            <input type="text" class="form-control" name="txtFunctionURL" id="txtFunctionURL" placeholder="Function URL" >
                        </div>
                        <div class="col-sm-4">     
                            <label for="order">Enter SEO URL:</label>
                            <input type="text" class="form-control" name="txtSeoUrl" id="txtSeoUrl" placeholder="SEO URL" >
                        </div>
                        <div class="col-sm-4">    
                            <label for="order"></label>
                             <input type="submit" name="btnSubmit" id="btnUpdate" style="margin-top:24px;"class="btn btn-primary" value="UPDATE SEO URL"/>  
                        </div>
                    </div>  
                    <div class="col-md-12">
                        <div id="grid" style="margin-top:35px;"> </div>                   
                    </div>
            </div>
        </div>   
    </div>
    </form>
    </body>
    <?php include ('footer.php'); ?>
    <?php include'common/message.php'; ?>                
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {



            function FillRoot()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfRootMenu.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlRoot").html(data);
                    }
                });
            }
            FillRoot();

            function FillParent(rootmenu)
            {
                $.ajax({
                    type: "post",
                    url: "common/cfParentFunctionMaster.php",
                    data: "action=FILL&values=" + rootmenu + "",
                    success: function (data) {
                        $("#ddlParent").html(data);
                    }
                });
            }

            $("#ddlRoot").change(function ()
            {
                FillParent(this.value);
            });




            function FillChild(parent)
            {
                $.ajax({
                    type: "post",
                    url: "common/cfupdateseourl.php",
                    data: "action=FILLCHILD&values=" + parent + "",
                    success: function (data) {
                        $("#ddlChild").html(data);
                    }
                });
            }

            $("#ddlParent").change(function ()
            {
                FillChild(this.value);
            });



            function FillFunctionurl(child)
            {
                $.ajax({
                    type: "post",
                    url: "common/cfupdateseourl.php",
                    data: "action=FILLFUNCTIONURL&values=" + child + "",
                    success: function (data)
                    {

                        data = $.parseJSON(data);
                        txtFunctionURL.value = data[0].FunctionURL;
                        txtSeoUrl.value = data[0].FunctionSEO;


                    }
                });
            }

            $("#ddlChild").change(function ()
            {
                FillFunctionurl(this.value);
            });


            function showData()
            {

                $.ajax({
                    type: "post",
                    url: "common/cfupdateseourl.php",
                    data: "action=SHOW",
                    success: function (data) {
                        //alert(data);
                        $("#grid").html(data);
                        $('#example').DataTable(
                                {
                                    dom: 'Bfrtip',
                                    buttons: [
                                        'copy', 'csv', 'excel', 'pdf', 'print'
                                    ]
                                });

                    }
                });
            }
            showData();


            $("#btnUpdate").click(function ()
            {
                if ($("#frmupdateseourl").valid())
                {

                    var Child = $("#ddlChild").val();
                    //alert(Child);
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    var url = "common/cfupdateseourl.php"; // the script where you handle the form input.
                    var data;
                    if (Mode == 'UPDATE')
                    {
                        data = "action=UPDATE&code=" + Child +
                                "&SEO=" + txtSeoUrl.value + "&FUNURL="+txtFunctionURL.value; // serializes the form's elements.

                    }
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data)
                        {
                            if (data == SuccessfullyUpdate)
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                window.setTimeout(function () {
                                    window.location.href = "frmupdateseourl.php";
                                }, 1000);


                            } else
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                            }
                            showData();


                        }
                    });
                }
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid)
            {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
    <script src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmupdateseourlvalidation.js"></script>
    </html>
    <?php
} else {
    session_destroy();
    ?>
    <script>

    window.location.href = "index.php";

    </script>
    <?php
}
?>