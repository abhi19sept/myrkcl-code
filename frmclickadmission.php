<?php
$title = "Admission Form";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '7') 
{
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
if (isset($_REQUEST['code'])) {
    echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    $Mode = $_REQUEST['Mode'];
} else {
    echo "<script>var AdmissionCode=0</script>";
    echo "<script>var Mode='Add'</script>";
    $Mode = "Add";
}

?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div class="container"> 

    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading" style="height:40px">
            <div  class="col-sm-4">Admission Form </div>

            <div class="col-sm-4">
                Course: <div style="display: inline" id="txtcoursename"> </div>
            </div>

            <div class="col-sm-4">
                Batch: <div style="display: inline" id="txtbatchname"> </div>
            </div>

        </div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            <form name="frmadmission" method="post" action="common/cfClickAdmission.php" id="frmadmission" 
				class="form-inline" role="form" enctype="multipart/form-data">
				<div class="container">
                        <div class="col-sm-4 form-group" id="response"></div>
                </div>        

				<div id="errorBox"></div>
				
				<div id="chklcode">
					<fieldset style="border: 1px groove #ddd !important;" class="">
						<br>
						<div class="container">            
								
							<div class="col-sm-4 form-group"> 
								<label for="fname">CLICK LearnerCode:<span class="star">*</span></label>
								<input type="text" class="form-control" maxlength="8" name="txtclcode" id="txtclcode" 
									onkeypress="javascript:return allownumbers(event);" placeholder="LearnerCode">     
							</div>
							
							<div class="col-sm-4 form-group"> 
								<input style="margin-top:25px !important;" type="button" name="btnValidate" id="btnValidate" class="btn btn-primary" value="Validate"/>   
							</div>
						   
						</div>
						<br>
					</fieldset>
						<br><br>
				</div>  
						
			<div id="old" style="display:none;"	>
			   <div class="container">
                   
                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Learner Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" name="txtlname" id="txtlname" style="text-transform:uppercase" onkeypress="javascript:return allowchar(event);" placeholder="Learner Name">
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtAdmissionCode" id="txtAdmissionCode"/>
                        <select id="ddlBatch" style="display:none;" name="ddlBatch" class="form-control">  </select>
                        <input type="hidden" class="form-control" maxlength="50" name="txtCourseFee" id="txtCourseFee"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtInstallMode" id="txtInstallMode"/>
                        <input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
                        <input type="hidden" class="form-control"  name="txtsign" id="txtsign"/>
                        <input type="hidden" class="form-control"  name="txtLearnercode" id="txtLearnercode"/>
                        <input type="hidden" class="form-control"  name="coursecode" id="coursecode" value="23"/>
                        <input type="hidden" class="form-control"  name="batchcode" id="batchcode"/>
                        <input type="hidden" class="form-control"  name="paadharname" id="paadharname"/>
                        <input type="hidden" class="form-control"  name="paadhardob" id="paadhardob"/>
                        <input type="hidden" class="form-control"  name="paadharno" id="paadharno"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtmode" id="txtmode" value="<?php echo $Mode; ?>" />
						 <input type="hidden" name="action" id="action" value="Add"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="fname">Father/Husband Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" style="text-transform:uppercase" maxlength="50" name="txtfname" id="txtfname" onkeypress="javascript:return allowchar(event);" placeholder="Father Name">     
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="dob">Date of Birth:<span class="star">*</span></label>								
                        <input type="text" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="YYYY-MM-DD">
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="mtongue">Mother Tongue:</label>
                        <select id="ddlmotherTongue" name="ddlmotherTongue" class="form-control" >								  
                            <option value="Hindi" selected>Hindi</option>
                            <option value="English">English</option>
                        </select>    
                    </div>
                </div>  

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="gender">Gender:</label> <br/>                               
                        <label class="radio-inline"> <input type="radio" id="genderMale" checked="checked" name="gender" value="Male"/> Male </label>
                        <label class="radio-inline"> <input type="radio" id="genderFemale" name="gender" value="Female"/> Female </label>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="email">Marital Status:</label> <br/>  
                        <label class="radio-inline"><input type="radio" id="mstatusSingle" checked="checked" name="mstatus" value="Single"/> Single </label>
                        <label class="radio-inline"><input type="radio" id="mstatusMarried" name="mstatus" value="Married"/> Married	</label>							
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="address">Medium Of Study:</label> <br/> 

                        <label class="radio-inline"> 
                            <input type="radio" id="mediumHindi" name="Medium" checked="checked" />Combined (Hindi + English)</label>

                    </div>

                    <div class="col-sm-6 form-group"> 
                        <label for="deptname">Physically Challenged:</label> <br/> 
                        <label class="radio-inline"> <input type="radio" id="physicallyChStYes" name="PH" value="Yes"/> Yes </label>
                        <label class="radio-inline"> <input type="radio" id="physicallyChStNo" name="PH" checked="checked" value="No"/> No </label>
                    </div>
                </div>    

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="empid">Proof Of Identity:<span class="star">*</span></label>
                        <select id="ddlidproof" name="ddlidproof" class="form-control">
                            <option value="">Select</option>
                            <option value="PAN Card">PAN Card</option>
                            <option value="Voter ID Card">Voter ID Card</option>
                            <option value="Driving License">Driving License</option>
                            <option value="Passport">Passport</option>
                            <option value="Employer ID card">Employer ID card</option>
                            <option value="Government ID Card">Governments ID Card</option>
                            <option value="College ID Card">College ID Card</option>
                            <option value="School ID Card">School ID Card</option>
                        </select>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <!--<label for="gpfno">Aadhar No:</label>-->
                        <!--<input type="text" class="form-control" maxlength="12"  name="txtidno" id="txtidno" onkeypress="javascript:return allownumbers(event);" placeholder="Aadhar No">-->

                        <label for="empid">Do You Have Aadhar:<span class="star">*</span></label>
                        <input type="label" class="form-control" maxlength="12"  name="txtaadharno" id="txtaadharno" style="display:none" placeholder="Aadhar No">

                        <select id="ddlAadhar" name="ddlAadhar" class="form-control">
                            <option value="">Select</option>
                            <option value="y">Yes</option>
                            <option value="n">No</option>


                        </select>

                    </div>


                    <div class="col-sm-4 form-group"> 
                        <label for="designation">District:<span class="star">*</span></label>
                        <select id="ddlDistrict" name="ddlDistrict" class="form-control">                                  
                        </select>
                    </div>     

                    <div class="col-sm-4 form-group"> 
                        <label for="marks">Tehsil:<span class="star">*</span></label>
                        <select id="ddlTehsil" name="ddlTehsil" class="form-control">                                 
                        </select>
                    </div>
                </div>

                <div class="container">                          
                    <div class="col-sm-4 form-group"> 
                        <label for="attempt">Address:<span class="star">*</span></label>  
                        <textarea class="form-control" rows="3" style="text-transform:uppercase" id="txtAddress" name="txtAddress" placeholder="Address" onkeypress="javascript:return validAddress(event);"></textarea>

                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="pan">PINCODE:<span class="star">*</span></label>
                        <input type="text" class="form-control" name="txtPIN" id="txtPIN"  placeholder="PINCODE" onkeypress="javascript:return allownumbers(event);" maxlength="6"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankaccount">Mobile No:<span class="star">*</span></label>
                        <input type="text" class="form-control"  name="txtmobile"  id="txtmobile" placeholder="Mobile No" onkeypress="javascript:return allownumbers(event);" maxlength="10"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankdistrict">Phone No:</label>
                        <input type="text" placeholder="Phone No" class="form-control" id="txtResiPh" name="txtResiPh" onkeypress="javascript:return allownumbers(event);" maxlength="11"/>
                    </div>   
                </div>

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="bankname">Email Id:</label>
                        <input type="text" placeholder="Email Id" class="form-control" id="txtemail" name="txtemail" maxlength="100" />
                    </div>   

                    <div class="col-sm-4 form-group"> 
                        <label for="branchname">Qualification:<span class="star">*</span></label>
                        <select id="ddlQualification" name="ddlQualification" class="form-control">                                  
                        </select>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="ifsc">Type of Learner:<span class="star">*</span></label>
                        <select id="ddlLearnerType" name="ddlLearnerType" class="form-control" onChange="findselected()">                                 
                        </select>
                    </div> 

                   
                </div>

                <fieldset style="border: 1px groove #ddd !important;" class="">
                    <br>
                    <div class="container">
                        <div class="col-sm-1"> 

                        </div>
                        <div class="col-sm-3" style="display: none"> 
                            <label for="payreceipt">Upload Scan Application Form:<span class="star">*</span></label>
                            <input type="file" class="form-control"  name="txtUploadScanDoc" id="txtUploadScanDoc" onchange="checkScanForm(this)"/>
                        </div>											

                        <div class="col-sm-3" > 
                            <label for="photo">Attach Photo:<span class="star">*</span></label> </br>
							<div id="uploadPreview9">
                                    
                                </div>
                            <img id="uploadPreview1" src="images/samplephoto.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">								  

                          <!--  <input style="margin-top:5px !important;" id="uploadImage1" type="file" name="p1" onchange="checkPhoto(this);
                                    PreviewImage(1)"/>	-->								  
                        </div>

                        <div class="col-sm-3"> 
                            <label for="photo">Attach Signature:<span class="star">*</span></label> </br>
							<div id="uploadPreview8">
                                    
                                </div>
                            <img id="uploadPreview2" src="images/samplesign.png" id="uploadPreview2" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage2').click();">							  
                          <!--  <input id="uploadImage2" type="file" name="p2" onchange="checkSign(this);
                                    PreviewImage(2)"/> -->
                        </div>  
                      <!--  <div class="col-sm-3"> 
                            <input style="margin-top:50px !important;" type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload Photo Sign"/>   
                        </div>-->
                        <div class="col-sm-1"> 

                        </div>

                    </div>
                    <br>
                </fieldset>
                <br><br>
                <div class="container">
					<input type="submit" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Submit"/>   
                </div>
				</div>
        </div>
    </div>   
</div>

</form>
<link href="css/popup.css" rel="stylesheet" type="text/css">
<!-- Modal -->
<div id="myModalupdate" class="modal" style="padding-top:150px !important">

    <div class="modal-content" style="width: 90%;">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h6>Update Your Aadhar Number</h6>
        </div>
        <div class="modal-body" style="max-height: 800px;" id='formhtml'>
            <form name="frmCourseBatch" id="frmCourseBatch" class="form-horizontal" role="form"
                  enctype="multipart/form-data" style="margin: 25px 0 40px 0;">  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6" id="responsesendotp"></div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Enter Aadhaar Number:</label>
                    <div class="col-sm-8">
                        <input class="form-control valid" name="txtACno" id="txtACno" placeholder="Aadhaar Card Number" aria-invalid="false"  maxlength="12"  onkeypress="javascript:return allownumbers(event);"  type="text">
                    </div>
                </div>
                <!--                  <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">Pin Code Number:</label>
                                      <div class="col-sm-6">
                                          <input class="form-control valid" name="txtPINno" id="txtPINno" placeholder="Pin Number" aria-invalid="false" maxlength="6"  onkeypress="javascript:return allownumbers(event);"  type="text">
                                      </div>
                                  </div>-->
                <div class="form-group" id="otpTxt" name="otpTxt"  style=" display: none;">
                    <label for="inputEmail3" class="col-sm-3 control-label">OTP:</label>
                    <div class="col-sm-8">
                        <input class="form-control valid" name="txtotpTxt" id="txtotpTxt" placeholder="OTP" aria-invalid="false" maxlength="9"  onkeypress="javascript:return allownumbers(event);"  type="text">
                    </div>
                </div>
                <div class="form-group" >
                    <div class="col-sm-3"></div>
                    <div class="col-sm-8" id="turmcandition">
                        <p>I,the holder of above mentioned Aadhar Number, hereby give my consent to RKCL to obtain my Aadhar Number, Name and Fingerprint/Iris for authentication with UIDAI. I have no objection using my identity and biometric information for validation with Aadhar(CIDR) only for the purpose of authentication will be used for generation and issuance of Digitally Signed RS-CIT certificate.</p>
                        <div><span class="yesturm">I Agree</span> <input type="checkbox" id="yes" name="fooby[1][]" /> Yes
<!--                              <input class="noturm" type="checkbox" id="no" name="fooby[1][]" /> No-->
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
                <div class="form-group last">
                    <div class="col-sm-offset-3 col-sm-6">
                        <input type="button" name="btnOTPSubmit" style="display: none;" id="btnOTPSubmit" class="btn btn-primary" value="Submit"/>
                        <input type="button" name="btnAadharSubmit"   style="display: none;" id="btnAadharSubmit" class="btn btn-primary valid" value="Submit"/>
                    </div>
                </div>   
                <div id="FinalResult" class="form-group"></div>
                <input class="form-control valid"  name="txtTxn" id="txtTxn" type="hidden">
                <input class="form-control valid" name="txtStatus" id="txtStatus" type="hidden">
            </form>

        </div>
    </div>
</div>

<div id="myModalupdate2" class="modal" style="padding-top:150px !important">

    <div class="modal-content panel-danger" style="width: 40%;">
        <div class=" panel-heading">
            <span class="close close2">&times;</span>
            <h6>Warning</h6>
        </div>
        <div class="modal-body" style="max-height: 800px;" id='formhtml'>

            <form name="frmCourseBatch1" id="frmCourseBatch1" class="form-horizontal" role="form"
                  enctype="multipart/form-data" style="margin: 15px;">  
                <div class="form-group" >
                  
                    <div class="col-sm-12" id="turmcandition">
                        <p>क्या आप अपना आधार नंबर दर्ज नहीं करना चाहते हैं ?? यह आपको  RS-CIT फ़ाइनल एक्जाम का डिजिटल साइन्ड ई-सर्टिफिकेट जनरेट करने मे मदद करता है|</p>
                        <div><span class="yesturm">I Agree</span> <input type="checkbox" id="yes1" name="fooby[1][]" /> Yes
<!--                              <input class="noturm" type="checkbox" id="no" name="fooby[1][]" /> No-->
                        </div>
                    </div>
                    
                </div>
                <div class="form-group last">
                    <div class="col-sm-offset-6 col-sm-6">
                        <input type="button" name="btnOTPSubmit1" style="display: none;" id="btnOTPSubmit1" class="btn btn-danger" value="Submit"/>

                    </div>
                </div>  
            </form>

        </div>
    </div>
</div>
</body>

<?php
include'common/message.php';
 include ('footer.php'); ?>


<style>
    #errorBox
    {	color:#F00;	 } 
</style>

<script type="text/javascript">
                            $('#dob').datepicker({
                                format: "yyyy-mm-dd",
                                orientation: "bottom auto",
                                todayHighlight: true
                            });
</script>

<script type="text/javascript">
    $('#txtEmpId').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
</script>

<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>

<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
    ;
</script>	

<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#txtUploadScanDoc').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {
            alert("Image size should less or equal 200 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }
        document.getElementById("txtUploadScanDoc").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkPhoto(target) {
        var ext = $('#uploadImage1').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        if (target.files[0].size > 5000) {
            alert("Image size should less or equal 5 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        } else if (target.files[0].size < 3000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        document.getElementById("uploadImage1").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkSign(target) {
        var ext = $('#uploadImage2').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        if (target.files[0].size > 3000) {
            alert("Image size should less or equal 3 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        } else if (target.files[0].size < 1000)
        {
            alert("Image size should be greater than 1 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        document.getElementById("uploadImage2").innerHTML = "";
        return true;
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {
		
        var aadharname = "";
        var aadhardob = "";
        var aadharno = "";

        function FillCourseName() {
            $.ajax({
                type: "post",
                url: "common/cfClickAdmission.php",
                data: "action=FillAdmissionCourse",
                success: function (data) {
                    document.getElementById('txtcoursename').innerHTML = data;
                }
            });
        }
        FillCourseName();

        function FillBatchName() {
            $.ajax({
                type: "post",
                url: "common/cfClickAdmission.php",
                data: "action=FillAdmissionBatch",
                success: function (data) {					
					if(data=='0'){
						alert("event for the current batch is not enabled");
						$('#txtclcode').attr('readonly', true);
						$('#btnValidate').hide();
					}
					else {
							data = $.parseJSON(data);
							batchcode.value = data[0].Event_Batch;
							document.getElementById('txtbatchname').innerHTML = data[0].Batch_Name;
							AdmissionFee(data[0].Event_Batch);
							InstallMode(data[0].Event_Batch);
					}                    
                }
            });
        }
        FillBatchName();
		
		$("#btnValidate").click(function () {
			$('#response').empty();
				$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            
				$.ajax({
                type: "post",
                url: "common/cfClickAdmission.php",
                data: "action=getlearnerdetails&lcode=" + txtclcode.value + "",
                success: function (data) {
					data=data.trim();					
				   $('#response').empty();
				   if(data=='0'){					   
                        $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Enter LearnerCode.</span></p>");                   
				   }
				   else if(data=='1'){
					   $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Already Uploaded.</span></p>");                   
				   }
				   else if(data=='no'){
					   $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>No record found.</span></p>");                   
				   }
				   else{
					   data = $.parseJSON(data);
					   if(data[0].photo=='NA'){
							$('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Photo Not Available.</span></p>");
						}
						else if(data[0].sign=='NA'){
							$('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Sign Not Available.</span></p>");
						}
						else{
							   $('#txtlname').attr('readonly', true);
							   $('#txtfname').attr('readonly', true);
							   $('#dob').attr('readonly', true);
								txtlname.value = data[0].CLICK_Admission_Name;
								txtfname.value = data[0].CLICK_Admission_Fname;
								dob.value = data[0].CLICK_Admission_DOB;
								
								
								txtphoto.value = data[0].CLICK_Admission_Photo;
								txtsign.value = data[0].CLICK_Admission_Sign;
								 $("#uploadPreview9").html(data[0].photo);
							$("#uploadPreview1").hide();

							$("#uploadPreview8").html(data[0].sign);
							$("#uploadPreview2").hide();
					
							$('#txtclcode').attr('readonly', true);
							$('#btnValidate').hide();
							$('#old').show();
						}
				   }
                }
            });
		});

        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                }
            });
        }
        GenerateUploadId();


        function GenerateLearnercode()
        {
            $.ajax({
                type: "post",
                url: "common/cfClickAdmission.php",
                data: "action=GENERATELEARNERCODE",
                success: function (data) {
                    txtLearnercode.value = data;
                }
            });
        }
        GenerateLearnercode();

        function AdmissionFee(batchcode)
        {
            $.ajax({
                type: "post",
                url: "common/cfClickAdmission.php",
                data: "action=Fee&codes=" + batchcode + "",
                success: function (data) {
                    txtCourseFee.value = data;
                }
            });
        }
        

        function InstallMode(batchcode)
        {
            $.ajax({
                type: "post",
                url: "common/cfClickAdmission.php",
                data: "action=Install&values=" + batchcode + "",
                success: function (data) {
                    txtInstallMode.value = data;
                }
            });
        }
        

        $("#txtEmpId").prop("disabled", true);

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        } else if (Mode == 'Edit')
        {
            fillForm();
        }

        $("input[type='image']").click(function () {
            $("input[id='my_file']").click();
        });

        function FillDistrict() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {					
                    $("#ddlDistrict").html(data);
                }
            });
        }
        FillDistrict();

        function FillQualification() {
            $.ajax({
                type: "post",
                url: "common/cfQualificationMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlQualification").html(data);
                }
            });
        }
        FillQualification();

        function FillLearnerType() {
            $.ajax({
                type: "post",
                url: "common/cfLearnerTypeMaster.php",
                data: "action=FILL&coursecode=" + coursecode.value + "",
                success: function (data) {
                    $("#ddlLearnerType").html(data);
                }
            });
        }
        FillLearnerType();

        function findselected() {
            var selMenu = document.getElementById('ddlLearnerType');
            var txtField = document.getElementById('txtEmpId');
            if (selMenu.value != '34')
                txtField.disabled = false
            else
                txtField.disabled = true
        }

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });

        $("#ddlLearnerType").change(function () {
            findselected();
        });

        // statement block start for delete record function
        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfClickAdmission.php",
                data: "action=DELETE&values=" + AdmissionCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmLearnerProfile.php";
                        }, 1000);
                        Mode = "Add";
                        resetForm("frmLearnerProfile");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }
        // statement block end for delete function statment

        $("#ddlAadhar").change(function () {
            var selAadhar = $(this).val();
            if (selAadhar == 'y') {
                var mybtnid = "myBtn_" + selAadhar;
                var modal = document.getElementById('myModalupdate');
                var btn = document.getElementById(mybtnid);
                var span = document.getElementsByClassName("close")[0];
                modal.style.display = "block";
                span.onclick = function () {
                    modal.style.display = "none";
                    $("#btnOTPSubmit").show();
                    $("#otpTxt").hide();
                    $("#btnAadharSubmit").hide();
                    $("#txtACno").attr("readonly", false);
                }
            }
            if (selAadhar == 'n') {
                var mybtnid = "myBtn_" + selAadhar;
                var modal2 = document.getElementById('myModalupdate2');
                var btn = document.getElementById(mybtnid);
                var span = document.getElementsByClassName("close2")[0];
                modal2.style.display = "block";
                span.onclick = function () {
                    modal2.style.display = "none";
                }
            }
        });

        $('#yes').change(function () {
            if ($(this).is(":checked")) {
                var returnVal = confirm("Are you sure?");
                if (returnVal) {
                    $("#btnOTPSubmit").css("display", "block");
                } else {
                    $("#btnOTPSubmit").css("display", "none");
                }

            } else {
                $("#btnOTPSubmit").css("display", "none");
            }
        });
        $('#yes1').change(function () {
            if ($(this).is(":checked")) {
                var returnVal = confirm("Are you sure?");
                if (returnVal) {
                    $("#btnOTPSubmit1").css("display", "block");
                } else {
                    $("#btnOTPSubmit1").css("display", "none");
                }
            } else {
                $("#btnOTPSubmit1").css("display", "none");
            }
        });
        $("#btnOTPSubmit1").click(function () {
            var modal2 = document.getElementById('myModalupdate2');
            modal2.style.display = "none";
        });
        $("#btnOTPSubmit").click(function () {
            if (txtACno.value == '')
            {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter Aadhaar Number." + "</span></p>");
            } else {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/ajax-loader.gif width=40px /></span><span>Please wait we are fetching and verifying your Aadhaar Data.</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfClickAdmission.php",
                    data: "action=generateOTP&txtACno=" + txtACno.value + "",
                    success: function (data)
                    {
                        data = $.parseJSON(data);
                        $('#responsesendotp').empty();
                        if (data.status == "n")
                        {
                            $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Invalid Aadhaar Number. Please check your Aadhaar Number." + "</span></p>");
                        } else
                        { 
                            $('#responsesendotp').append("<p class='sucess'><span><img src=images/correct.gif width=10px /></span><span>" + " We have sent OTP on your registered Aadhaar Number. Please enter OTP as received on Mobile." + "</span></p>");
                            txtTxn.value = data.txn;
                            txtStatus.value = data.status;
                            $("#btnOTPSubmit").hide();
                            $("#otpTxt").show();
                            $("#btnAadharSubmit").show();
                            $("#txtACno").attr("readonly", "readonly");
                            $("#txtPINno").attr("readonly", "true");
                        }
                    }
                });
            }
        });

        $("#btnAadharSubmit").click(function () {
            if (txtotpTxt.value == '')
            {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter OTP." + "</span></p>");
            } else {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $("#txtotpTxt").attr("readonly", "true");
                $.ajax({
                    type: "post",
                    url: "common/cfClickAdmission.php",
                    data: "action=authenticateOTP&txtACno=" + txtACno.value + "&txtTxn=" + txtTxn.value + "&txtStatus=" + txtStatus.value + "&txtotpTxt=" + txtotpTxt.value + "",
                    success: function (data)
                    {
					if(txtACno.value == "55440460008"){
                            //alert(data);
                        }
                        data = $.parseJSON(data);
                        $('#responsesendotp').empty();
                        if (data.status == 'y') {
                            $('#responsesendotp').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Aadhar Detail Verified</span></p>");
                            aadharname = data.learnername;
                            aadhardob = data.learnerdob;
                            aadharno = data.learneraadhar;
							
							$('#paadharname').val(aadharname);
							$('#paadhardob').val(aadhardob);
							$('#paadharno').val(aadharno);
							
                            $('#txtaadharno').val(aadharno);
                            $("#txtaadharno").show();
                            $("#ddlAadhar").hide();
							$('#txtaadharno').prop('readonly', true);
                            window.setTimeout(function () {
								$("#myModalupdate").css("display", "none");
                            }, 3000);
                        } else {
                            $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //$("#FinalResult").html(data);
                    }
                });
            }
        });
		$("#frmadmission").on('submit',(function(e) {
			e.preventDefault();
			$('#response').empty();
			$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            
			$.ajax({ 
				url: "common/cfClickAdmission.php",
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
          {
				var result=data.trim();
              if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + result + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmclickadmission.php';
                            }, 3000);
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></div>");
                        }
          },
            error: function(e) 
            {
               $("#errmsg").html(e).fadeIn();
            } 	        
    });
}));

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        // statement block end for submit click
    });
</script>
<!--<script src="scripts/signupload.js"></script>-->
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmclickadmission_validation.js" type="text/javascript"></script>	
</html>