<?php
session_start();

$img= $_SESSION['LearnerPhoto'];

		$files = explode(",",$img);
		$tmpFile = tempnam('/tmp', '');
		$zip = new ZipArchive;
		$zipname = time().".zip"; // Zip name
		$zip->open($zipname,  ZipArchive::CREATE);
		foreach ($files as $file) {
			// download file
			$fileContent = file_get_contents($file);
			$zip->addFromString(basename($file), $fileContent);
		}
		$zip->close();

		header('Content-Type: application/zip');
		header('Content-disposition: attachment; filename=photo.zip');		
		readfile($zipname);
		unlink($zipname);		

?>