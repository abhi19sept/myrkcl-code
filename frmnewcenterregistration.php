<?php
$title = "IT-GK Registration Form";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var OrganizationCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var OrganizationCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
$random = (mt_rand(1000, 9999));
$random .= date("y");
$random .= date("m");
$random .= date("d");
$random .= date("H");
$random .= date("i");
$random .= date("s");

echo "<script>var OrgDocId= '" . $random . "' </script>";
?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:46px !important;">

            <div class="panel-heading">MYRKCL IT-GK Registration</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmNewCenterRegistration" id="frmNewCenterRegistration"  role="form" action="" class="form-inline" enctype="multipart/form-data">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <div class="panel panel-info" id="impreg">
                        <div class="panel-heading">Important Registration Details</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-4 form-group">     
                                    <label for="SelectType">Type of Application:<span class="star">*</span></label>
                                    <select id="ddlOrgType" name="ddlOrgType" class="form-control" >
                                        <option selected="true" value="">Select</option>
                                        <!--                                <option value="14" >RKCL Service Provider</option>-->
                                        <option value="15" >IT-GK</option>
                                    </select>
                                    <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="email">Enter Email:<span class="star">*</span></label>
                                    <input type="text" class="form-control" maxlength="50" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="Mobile">Enter Mobile Number:<span class="star">*</span></label>
                                    <input type="text" class="form-control" maxlength="10" name="txtMobile" id="txtMobile" onkeypress="javascript:return allownumbers(event);" placeholder="Mobiile Number">
                                </div> 
                                <div class="col-sm-4 form-group">
                                    <label for="Verify">Generate OTP:<span class="star">*</span></label>
                                    <input type="button" name="btnGenerateOTP" id="btnGenerateOTP" class="btn btn-primary" value="Generate OTP"/>
                                    <label for="tnc" id="OTPSent" style="margin-top:25px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>OTP Sent</b></label></br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info" id="otpverify" style="display:none">
                        <div class="panel-heading">Verify Registered Authentication Details</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="otpemail">Enter OTP Received on Email of IT-GK:</label>
                                    <input type="text" class="form-control" maxlength="6" name="txtOTPEmail" id="txtOTPEmail" placeholder="OTP Received on Email ID">     
                                </div>
                                <div class="col-sm-4 form-group">     
                                    <label for="otpMobile">Enter OTP Received on Mobile Number of IT-GK:</label>
                                    <input type="text" class="form-control" maxlength="6" name="txtOTPMobile" id="txtOTPMobile"  placeholder="OTP Received on Mobiile Number">
                                </div>
                                <div class="col-sm-4 form-group">
                                    <label for="Verify">Verify Mobile No and Email Id:<span class="star">*</span></label>
                                    <input type="button" name="btnVerify" id="btnVerify" class="btn btn-primary" value="Verify" style="margin-top:25px"/>
                                    <!--<input type="button" name="btnVerified" id="btnVerified" class="btn btn-primary" value="Details Verified" style="margin-top:25px; display:none"/>-->
                                    <label for="tnc" id="btnVerified" style="margin-top:25px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Details Verified</b></label></br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="main-content" style="display:none">
                        <div class="panel panel-info">
                            <div class="panel-heading">Registration Details</div>
                            <div class="panel-body">
                                <div class="container">

                                    <div id="errorBox"></div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="learnercode">Name of Organization/Center:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center" oncopy="return false" onpaste="return false" oncut="return false" onkeypress="javascript:return allowchar(event);">
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="ename">Registration No:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="30" name="txtRegno" id="txtRegno" placeholder="Registration No" onkeypress="javascript:return validAddress(event);">     
                                    </div>


                                    <div class="col-sm-4 form-group">     
                                        <label for="faname">Date of Establishment:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="50" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Type of Organization:<span class="star">*</span></label>
                                        <select id="txtType" name="txtType" class="form-control" >

                                        </select>    
                                    </div>
                                </div>
                                <div class="panel panel-info" id="OrgType1" style="display:none">
                                    <div class="panel-heading">Upload Documents for Proprietorship/Individual Firm</div>
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="col-sm-4 form-group">
                                                <label for="owntype">Select Ownership Type :<span class="star">*</span></label>
                                                <select id="ddlowntype" name="ddlowntype" class="form-control" >
                                                    <option value="">Select</option>
                                                    <option value="Rented">Rented</option>
                                                    <option value="Owned">Owned</option>
                                                    <option value="NOC">NOC</option>
                                                </select>                               
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label id="Rented" for="rented" style="display:none">Rent Agreement:<span class="star">*</span></label>
                                                <label id="Owned" for="owned" style="display:none">Ownership document  :<span class="star">*</span></label>
                                                <label id="NOC" for="noc" style="display:none">NOC Copy :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="owntype" id="owntype" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group" style="display:none" id="utibill"> 
                                                <label for="salpi">Latest paid Utility Bill :<span class="star">*</span></label>
                                                <input type="file" class="form-control" name="utilitybill" id="utilitybill" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="salpi">Shop Act License :<span class="star">*</span></label>
                                                <input type="file" class="form-control" name="salpi" id="salpi" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                        <div class="container">
                                            <div class="col-sm-4 form-group">
                                                <label for="panpi">PAN card copy of Proprietor :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="panpi" id="panpi" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ccpi">Cancelled Cheque :<span class="star">*</span><a data-toggle="tooltip" title="One Cancelled cheque in which ITGK wants any share/ payment from RKCL , this could be either saving bank a/c cheque or current a/c cheque."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="ccpi" id="ccpi" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                    </div>
                                </div>
                                <div class="panel panel-info" id="OrgType2" style="display:none">
                                    <div class="panel-heading">Upload Documents for Partnership Firm</div>
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="copd">Copy of Partnership Deed :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="copd" id="copd" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="al">Authorization letter :<span class="star">*</span><a data-toggle="tooltip" title="Authorization letter to sign the agreement by one partner from other partners."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="al" id="al" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ppf">PAN card of Partnership firm:<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="ppf" id="ppf" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ccpf">Cancelled cheque of firm :<span class="star">*</span><a data-toggle="tooltip" title="Cancelled cheque of Partnership firm only. No check in the name of the Partner."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>

                                                <input type="file" class="form-control"  name="ccpf" id="ccpf" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                        <div class="container">
                                            <div class="col-sm-4 form-group">
                                                <label for="rcrf">Address Proof documents :<span class="star">*</span><a data-toggle="tooltip" title="Address Proof documents such as Rent Agreement along with utility bill, if premises is in the name of firm, than ownership documents, and if premises is in the name of any partner or his relative , an NOC along with utility bill."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="apdpf" id="apdpf" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="rcrf">Registration certificate<span class="star">*</span><a data-toggle="tooltip" title="Registration certificate of Registrar of firms."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="rcrf" id="rcrf" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="sarpf">Shop Act Registration Copy :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="sarpf" id="sarpf" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                    </div>
                                </div>
                                <div class="panel panel-info" id="OrgType3" style="display:none">
                                    <div class="panel-heading">Upload Documents for Public Ltd. or Pvt Ltd.</div>
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="coi">Certificate of Incorporation :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="coi" id="coi" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="lod">List of Directors:<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="lod" id="lod" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="pan">Copy of PAN card of Company:<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="copan" id="copan" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ccc">Canceled cheque of Company :<span class="star">*</span><a data-toggle="tooltip" title="One Canceled cheque of Company in which company wants to take any share/payment from RKCL. Director's Check is not allowed"><span style="width: 1%;" class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>

                                                <input type="file" class="form-control"  name="ccc" id="ccc" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                        <div class="container">
                                            <div class="col-sm-4 form-group">Copy of Board Resolution
                                                <input type="file" class="form-control"  name="cobr" id="cobr" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="apd">Address Proof documents :<span class="star">*</span><a data-toggle="tooltip" title="Address Proof documents such as Rent Agreement along with utility bill, if premises is in the name of Company , than ownership documents, and if premises is in the name of any Director  or his relative , an NOC along with utility bill."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="apd" id="apd" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="csar">Copy of Shop Act Registration:<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="csar" id="csar" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                    </div>
                                </div>
                                <div class="panel panel-info" id="OrgType4" style="display:none">
                                    <div class="panel-heading">Upload Documents for Limited Liability Partnership (LLP)</div>
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="coillp">Certificate of Incorporation  :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="coillp" id="coillp" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="lop">List of Partners :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="lop" id="lop" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="panllp">Copy of PAN card of LLP :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="panllp" id="panllp" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ccllp">Canceled cheque of LLP :<span class="star">*</span><a data-toggle="tooltip" title="Canceled cheque of LLP in which company wants to take any share/payment from RKCL."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>

                                                <input type="file" class="form-control"  name="ccllp" id="ccllp" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="cobrllp">Copy of Board Resolution :<span class="star">*</span><a data-toggle="tooltip" title="Copy of Board Resolution authorizing for signing the agreement."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="cobrllp" id="cobrllp" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label for="apdllp">Address Proof Document :<span class="star">*</span><a data-toggle="tooltip" title="Address Proof documents such as Rent Agreement along with utility bill, if premises is in the name of LLP , than ownership documents, and if premises is in the name of any Director/Partner   or his relative , an NOC along with utility bill."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="apdllp" id="apdllp" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="csarllp">Copy of Shop Act Registration :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="csarllp" id="csarllp" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                    </div>
                                </div>
                                <div class="panel panel-info" id="OrgType5" style="display:none">
                                    <div class="panel-heading">Upload Documents for Other Types of Firm</div>
                                    <div class="panel-body">
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="cor">Certificate of Registration under relevant Act :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="cor" id="cor" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="panoth">Copy of PAN Card of Organization  :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="panoth" id="panoth" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="ccoth">Copy of Cancelled cheque of Organization :<span class="star">*</span></label>
                                                <input type="file" class="form-control"  name="ccoth" id="ccoth" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="leb">List of Executive Body :<span class="star">*</span></label>

                                                <input type="file" class="form-control"  name="leb" id="leb" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="cbr">Copy of Board Resolution :<span class="star">*</span><a data-toggle="tooltip" title="Copy of Board Resolution authorizing for signing the agreement."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="cbr" id="cbr" onchange="ValidateSingleInput(this);">
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label for="adpoth">Address Proof documents :<span class="star">*</span><a data-toggle="tooltip" title="Address Proof documents such as Rent Agreement along with utility bill, if premises is in the name of  organization  , than ownership documents, and if premises is in the name of any member or his relative , an NOC along with utility bill."><span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a></label>
                                                <input type="file" class="form-control"  name="adpoth" id="adpoth" onchange="ValidateSingleInput(this);">
                                            </div>
                                        </div>	
                                    </div>
                                </div>

                                <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Document Type:<span class="star">*</span></label>
                                        <select id="txtDoctype" name="txtDoctype" class="form-control" >
                                            <option value="">Select</option>
                                            <option value="PAN Card">PAN Card</option>
                                            <!--                            <option value="Voter ID Card">Voter ID Card</option>
                                                                        <option value="Driving License">Driving License</option>
                                                                        <option value="Passport">Passport</option>
                                                                        <option value="Employer ID card">Employer ID card</option>
                                                                        <option value="Government ID Card">Governments ID Card</option>
                                                                        <option value="College ID Card">College ID Card</option>
                                                                        <option value="School ID Card">School ID Card</option>-->
                                        </select>    
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="panno">PAN Card Number:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtPanNo" id="txtPanNo" placeholder="PAN Card Number" onkeypress="javascript:return validAddress(event);">     
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="panno">AADHAR Card Number:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="12" name="txtAADHARNo" id="txtAADHARNo" placeholder="AADHAR Card Number" onkeypress="javascript:return allownumbers(event);">     
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Country:<span class="star">*</span></label>
                                        <select id="ddlCountry" name="ddlCountry" class="form-control" >

                                        </select>    
                                    </div>
                                </div>	


                                <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">State:<span class="star">*</span></label>
                                        <select id="ddlState" name="ddlState" class="form-control" >

                                        </select>    
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Divison:<span class="star">*</span></label>
                                        <select id="ddlRegion" name="ddlRegion" class="form-control" >

                                        </select>    
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">District:<span class="star">*</span></label>
                                        <select id="ddlDistrict" name="ddlDistrict" class="form-control" >

                                        </select>    
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Tehsil:<span class="star">*</span></label>
                                        <select id="ddlTehsil" name="ddlTehsil" class="form-control" >

                                        </select>    
                                    </div>
                                </div>

                                <div class="container">	





                                    <div class="col-sm-4 form-group"> 
                                        <label for="address">Pin Code:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="txtPinCode" id="txtPinCode" maxlength="6" onkeypress="javascript:return allownumbers(event);" placeholder="Pin Code">    
                                    </div>


                                    <div class="col-sm-4 form-group">     
                                        <label for="address">Address:<span class="star">*</span></label>
                                        <textarea class="form-control" id="txtRoad" name="txtRoad" placeholder="Address" onkeypress="javascript:return validAddress(event);"></textarea>

                                    </div>

                                    <div class="col-sm-4 form-group">     
                                        <label for="address">Nearest Landmark:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="txtLandmark" 
                                               id="txtLandmark" placeholder="Nearest Landmark" >

                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Police Station:</label>
                                        <input type="text" class="form-control" name="txtPoliceStation" id="txtPoliceStation" value="NA" placeholder="Police Station">       

                                    </div>


                                </div>	

                                <div class="container">
                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Area Type:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="areaUrban" name="area" value="Urban" onChange="findselected()"/> Urban </label>
                                        <label class="radio-inline"> <input type="radio" id="areaRural" name="area" value="Rural"onChange="findselected1()"/> Rural </label>
                                    </div>
                                    

                                    <div class="container" id="Urban" style="display:none">
                                        <div class="col-sm-4 form-group"> 
                                            <label for="edistrict">Municipality Type:</label>
                                            <select id="ddlMunicipalType" name="ddlMunicipalType" class="form-control" >

                                            </select>                                         
                                        </div>

                                        <div class="col-sm-4 form-group"> 
                                            <label for="edistrict">Municipality Name:</label>
                                            <select id="ddlMunicipalName" name="ddlMunicipalName" class="form-control" >

                                            </select>                                            
                                        </div>

                                        <div class="col-sm-4 form-group"> 
                                            <label for="edistrict">Ward No.:</label>
                                            <select id="ddlWardno" name="ddlWardno" class="form-control" >

                                            </select>                                       
                                        </div>
                                    </div>

                                    <div class="container" id="Rural" style="display:none">
                                        <div class="col-sm-4 form-group"> 
                                            <label for="edistrict">Panchayat Samiti/Block:</label>
                                            <select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >

                                            </select>
                                        </div>
                                        <div class="col-sm-4 form-group"> 
                                            <label for="edistrict">Gram Panchayat:</label>
                                            <select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control" >

                                            </select>
                                        </div>
                                        <div class="col-sm-4 form-group"> 
                                            <label for="edistrict">Village:</label>
                                            <select id="ddlVillage" name="ddlVillage" class="form-control" >

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="container" id="showfee" style="display:none">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="ncrfee"></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtncrfee" id="txtncrfee" placeholder="NCR Fee" readonly="true">     
                                    </div>
                                </div>
                            </div></div>
                        <div class="panel panel-info">
                            <div class="panel-heading">Upload Documents</div>
                            <div class="panel-body">
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <div class="panel panel-danger form-group ">
                                    <div class="panel-heading">Upload PAN Card</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach PAN Card:<span class="star">*</span></label>
                                            <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->								  
                                            <input style=" width: 115%" id="uploadImage7" type="file" name="uploadImage7" onchange="checkPANPhoto(this);"/>	
                                            <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                            <span style="font-size:10px;">Note: PDF Allowed Size = 100 to 200 KB</span>
                                        </div>
                                    </div>
                                </div>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <div class="panel panel-danger form-group">
                                    <div class="panel-heading">Upload AADHAR Card</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach AADHAR Card:<span class="star">*</span></label> 
                                            <!--<img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage8').click();">-->								  
                                            <input style=" width: 115%" id="uploadImage8" type="file" name="uploadImage8" onchange="checkAADHARPhoto(this);"/>	
                                            <span style="font-size:10px;">Note: PDF Allowed Size = 100 to 200 KB</span>
                                        </div>
                                    </div>
                                </div>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <div class="panel panel-danger form-group">
                                    <div class="panel-heading">Upload Address Proof</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach Document:<span class="star">*</span></label> 
                                            <!--<img id="uploadPreview9" src="images/sampleproof.png" id="uploadPreview9" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage9').click();">-->								  
                                            <input style=" width: 115%" id="uploadImage9" type="file" name="uploadImage9" onchange="checkAddressProof(this);"/>	
                                            <span style="font-size:10px;">Note: PDF Allowed Size = 100 to 200 KB</span>
                                        </div>
                                    </div>
                                </div>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;                        
                                <div class="panel panel-danger form-group">
                                    <div class="panel-heading">Upload NCR Application Form</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach Document:<span class="star">*</span></label>  
                                            <!--<img id="uploadPreview6" src="images/sampleproof.png" id="uploadPreview6" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage6').click();">-->								  
                                            <input style=" width: 115%" id="uploadImage6" type="file" name="uploadImage6" onchange="checkNCRApp(this);"/>	
                                            <span style="font-size:10px;">Note: PDF Allowed Size = 100 to 200 KB</span10
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
					 <div class="container"> 
                                    <div class="col-md-11"> 	

                                        <label for="learnercode"><b>1. I hereby declare that we have informed the concerned AO owner about all the NCR rules and guidelines</b></label></br>  
                                        <label for="learnercode"><b>2. I hereby declare that all information recorded is true to our knowledge & AO is eligible for NCR application </b></label><br>  <br>  
                                        <div class="container">
                                        <label class="checkbox-inline" style="float:left;"> <input type="checkbox" name="chk" id="chk" value="1" />
                                            I Accept 
                                        </label>
                                        </div>
                                    </div>
                                </div>
                    <br>
                    <br>
                    <div class="container" id="divsubmit" style="display:none;">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                    </div>
            </div>
            </form>
        </div>   
    </div>
</div>
</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    $('#txtEstdate').datepicker({
        format: "yyyy-mm-dd"
    });
</script>

<!--<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
    ;
</script>-->
<script language="javascript" type="text/javascript">
    var _validFileExtensions = [".pdf"];
    function ValidateSingleInput(oInput) {
        if (oInput.type == "file") {
            var sFileName = oInput.value;

            var iFileSize = oInput.files[0].size;

            if (sFileName.length > 0) {
                if (iFileSize > 100000 && iFileSize < 2000000) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }

                } else {
                    alert("File Size should be between 100KB to 2 MB.");
                    oInput.value = "";
                }
            }
        }
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkPANPhoto(target) {
        var ext = $('#uploadImage7').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage7").value = '';
            return false;
        }

        if (target.files[0].size > 2000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage7").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        }
        document.getElementById("uploadImage7").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkAADHARPhoto(target) {
        var ext = $('#uploadImage8').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage8").value = '';
            return false;
        }

        if (target.files[0].size > 2000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage8").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage8").value = '';
            return false;
        }
        document.getElementById("uploadImage8").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkAddressProof(target) {
        var ext = $('#uploadImage9').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage9").value = '';
            return false;
        }

        if (target.files[0].size > 2000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage9").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage9").value = '';
            return false;
        }
        document.getElementById("uploadImage9").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkNCRApp(target) {
        var ext = $('#uploadImage6').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage6").value = '';
            return false;
        }

        if (target.files[0].size > 2000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage6").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage6").value = '';
            return false;
        }
        document.getElementById("uploadImage6").innerHTML = "";
        return true;
    }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

//       $("#theLink").hover(
//        function () {
//            $("#theDiv").fadeIn();
//        },
//        function () {
//            $("#theDiv").fadeOut();
//        }
//    );

 $('#chk').change(function () {
            if (this.checked) {
                $('#divsubmit').show();
                $('#chk').attr('disabled', true);
            } else {
                alert("Please select Check Box to Proceed.");
            }
        });
        $("#btnGenerateOTP").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfNewCenterRegistration.php"; // the script where you handle the form input.            
            var data;
            data = "action=GenerateOTP&emailid=" + txtEmail.value + "&mobno=" + txtMobile.value + ""; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {data = data.trim();
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>OTP has been sent to your Mobile and Email. Please Enter OTP to Proceed</span></p>");
                        $("#otpverify").show();
                        $('#txtEmail').attr('readonly', true);
                        $('#txtMobile').attr('readonly', true);
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Invalid Details. Please Try again</span></p>");
                    }
                    //showData();


                }
            });
        });

        $("#btnVerify").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfNewCenterRegistration.php",
                data: "action=VERIFY&otpemail=" + txtOTPEmail.value + "&otpmobile=" + txtOTPMobile.value + "",
                success: function (data)
                {data = data.trim();
                    //alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $("#btnVerify").hide();
                        $("#btnVerified").show();
                        $('#impreg').hide(3000);
                        $('#txtOTPEmail').attr('readonly', true);
                        $('#txtOTPMobile').attr('readonly', true);
                        $('#main-content').show(3000);
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                    }
                }
            });
        });

        $('#txtGenerateId').val(OrgDocId);

        $(function () {
            $("#txtEstdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });

        function FillOrgType() {
            $.ajax({
                type: "post",
                url: "common/cfOrgTypeMaster.php",
                data: "action=FILL",
                success: function (data) {data = data.trim();
                    $("#txtType").html(data);
                }
            });
        }
        FillOrgType();

        $("#txtType").change(function () {
            var selOrgType = $(this).val();
            //alert(selOrgType);
            if (selOrgType == '1') {
                $("#OrgType1").fadeIn();
            } else {
                $("#OrgType1").hide();
            }
            if (selOrgType == '2') {
                $("#OrgType2").fadeIn();
            } else {
                $("#OrgType2").hide();
            }
            if (selOrgType == '3' || selOrgType == '4') {
                $("#OrgType3").fadeIn();
            } else {
                $("#OrgType3").hide();
            }
            if (selOrgType == '8') {
                $("#OrgType4").fadeIn();
            } else {
                $("#OrgType4").hide();
            }
            if (selOrgType == '5' || selOrgType == '6' || selOrgType == '7' || selOrgType == '9') {
                $("#OrgType5").fadeIn();
            } else {
                $("#OrgType5").hide();
            }

        });


        $("#ddlowntype").change(function () {
            var selOwnType = $(this).val();
            //alert(selOrgType);
            if (selOwnType == 'Rented') {
                $("#Rented").show();
                $("#utibill").show();
            } else {
                $("#Rented").hide();
            }
            if (selOwnType == 'Owned') {
                $("#Owned").show();
                $("#utibill").hide();
            } else {
                $("#Owned").hide();
            }
            if (selOwnType == 'NOC') {
                $("#NOC").show();
                $("#utibill").show();
            } else {
                $("#NOC").hide();
            }

        });

        function FillParent() {
            $.ajax({
                type: "post",
                url: "common/cfCountryMaster.php",
                data: "action=FILL",
                success: function (data) {data = data.trim();
                    $("#ddlCountry").html(data);
                }
            });
        }
        FillParent();

        $("#ddlCountry").change(function () {
            var selcountry = $(this).val();
            //alert(selcountry);
            $.ajax({
                url: 'common/cfStateMaster.php',
                type: "post",
                data: "action=FILL&values=" + selcountry + "",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#ddlState').html(data);
                }
            });
        });


        $("#ddlState").change(function () {
            var selState = $(this).val();
            //alert(selState);
            $.ajax({
                url: 'common/cfRegionMaster.php',
                type: "post",
                data: "action=FILL&values=" + selState + "",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#ddlRegion').html(data);
                }
            });
        });

        $("#ddlRegion").change(function () {
            var selregion = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLDistrict&values=" + selregion + "",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#ddlDistrict').html(data);
                }
            });
        });

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLPanchayat&values=" + selDistrict + "",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#ddlPanchayat').html(data);
                }
            });
        });

        $("#ddlPanchayat").change(function () {
            var selPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#ddlGramPanchayat').html(data);
                }
            });
        });

        $("#ddlGramPanchayat").change(function () {
            var selGramPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfVillageMaster.php',
                type: "post",
                data: "action=FILL&values=" + selGramPanchayat + "",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#ddlVillage').html(data);
                }
            });
        });

        $("#ddlTehsil").change(function () {
            var selTehsil = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfAreaMaster.php',
                type: "post",
                data: "action=FILL&values=" + selTehsil + "",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#ddlArea').html(data);
                }
            });
        });

        function FillMunicipalName() {
            var selDistrict = ddlDistrict.value;
            //alert(selDistrict);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalName&values=" + selDistrict + "",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#ddlMunicipalName').html(data);
                }
            });
        }

        function FillMunicipalType() {
            var selDistrict = ddlDistrict.value;
            //alert(selDistrict);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalType",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#ddlMunicipalType').html(data);
                }
            });
        }

        $("#ddlMunicipalName").change(function () {
            var selMunicipalName = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FILLWardno&values=" + selMunicipalName + "",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#ddlWardno').html(data);
                }
            });
        });


        function findselected() {

            var UrbanDiv = document.getElementById("areaUrban");
            var category = document.getElementById("Urban");

            if (UrbanDiv) {
                Urban.style.display = areaUrban.checked ? "block" : "none";
                Rural.style.display = "none";
                $('#showfee').show();
                jQuery("label[for='ncrfee']").html("NCR Type-Other Than Gram Panchayat AO Registration Amount");
                txtncrfee.value = "38000";
            } else
            {
                Urban.style.display = "none";
            }
        }
        function findselected1() {


            var RuralDiv = document.getElementById("areaRural");
            var category1 = document.getElementById("Rural");

            if (RuralDiv) {
                Rural.style.display = areaRural.checked ? "block" : "none";
                Urban.style.display = "none";
                $('#showfee').show();
                jQuery("label[for='ncrfee']").html("NCR Type-Gram Panchayat AO Registration Amount");
                txtncrfee.value = "38000";
            } else
            {
                Rural.style.display = "none";
            }
        }
        $("#areaUrban").change(function () {
            findselected();
            FillMunicipalName();
            FillMunicipalType();
        });
        $("#areaRural").change(function () {
            findselected1();
            FillPanchayat();
        });

        $("#frmNewCenterRegistration").submit(function () {
            //alert("1");
            if ($("#frmNewCenterRegistration").valid())
            {

//                if (document.getElementById('areaUrban').checked) //for radio button
//                {
//                    var area_type = 'Urban';
//                } else {
//                    area_type = 'Rural';
//                }

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfNewCenterRegistration.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmNewCenterRegistration").serialize();
//alert(forminput);
            data = forminput; // serializes the form's elements.
            var file_data7 = $("#uploadImage7").prop("files")[0];
            var file_data8 = $("#uploadImage8").prop("files")[0];
            var file_data9 = $("#uploadImage9").prop("files")[0];
            var file_data6 = $("#uploadImage6").prop("files")[0];// Getting the properties of file from file field
            var form_data = new FormData(this);                  // Creating object of FormData class
            form_data.append("panno", file_data7)
            form_data.append("aadharno", file_data8)
            form_data.append("addproof", file_data9)
            form_data.append("appform", file_data6)// Appending parameter named file with properties of file_field to form_data
            form_data.append("action", "ADD")
            form_data.append("data", data)
//alert(form_data);
            $.ajax({

                url: url,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data, // Setting the data attribute of ajax with file_data
                type: 'post',
                success: function (data)
                {data = data.trim();
                    //alert(data);

                    if (data === "Successfully Inserted")
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Center Registered Successfully.</span>");
                        window.setTimeout(function () {
                            window.location.href = "frmnewcenterregistration.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("frmNewCenterRegistration");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }



                }
            });
             }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmnewcenterregistration_validation.js"></script>



<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>