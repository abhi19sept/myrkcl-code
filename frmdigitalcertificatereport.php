<?php
$title = "Digital Certificate Generate Count Report";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code='" . $_SESSION['User_LoginId'] . "'</script>";
    echo "<script>var Mode='Show'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">

<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:1500px !important;">
<div class="container"> 
    

    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading">Digital Certificate Generate Count Report</div>
        <div class="panel-body">					
            <form name="frmdigitalcertificate" id="frmdigitalcertificate" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>
                    </div>        
                    <div id="errorBox"></div>

                    <div class="col-sm-4 form-group"> 
                        <label for="event">Select Event:</label>
                        <select id="ddlevent" name="ddlevent" class="form-control" >								  
                       
					   </select> 								
                    </div>						
					
					<div class="col-sm-4 form-group"> 
                        <label for="event">Select Batch Type:</label>
                        <select id="ddlBatchType" name="ddlBatchType" class="form-control" >								  
							<option value="">Please Select</option>
							<option value="1">Fresh Batches</option>
							<option value="2">Reexam Batches</option>
					   </select> 								
                    </div>
                    
                </div>  
 

                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View Report" 
						style="margin-left:15px;"/>    
                </div>

               <div id="grid" style="margin-top:35px; width:94%;"> </div>      
        </div>
    </div>   
</div>
</form>

</div>

<div id="LearnerDetails" class="modal" style="padding-top:50px !important">            
	<div class="modal-content" style="width: 90%;">
		<div class="modal-header">
			<span class="close mm">&times;</span>
				<h6>Learner Details</h6>
		</div>
		<div class="modal-body" style="max-height: 400px; overflow-y: scroll; text-align: center;">
			<div id="responses"></div>
				<div id="gird" ></div>		
		</div>
	</div>
</div>

<!--<div id="GetLearner" class="modal fade" role="dialog">
	
		<div class="modal-content" style="width: 90%;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"> &times; </button>
				<h4 class="modal-title">Learner Details</h4>
			</div>
			<div class="modal-body">
				<div id="response"></div>
			</div>
			<div class="modal-footer">
				<button type="button" id="modal_dismiss_itgk" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	
</div>-->

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {       
		function FillEvent() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfAadharLinkCountReport.php",
                data: "action=FillEvent",
                success: function (data) {
                    $("#ddlevent").html(data);
                }
            });
        }
        FillEvent();		
	
	$("#grid").on('click', '.GetLearner',function(){
			var batchcode = $(this).attr('id');
			var modal = document.getElementById('LearnerDetails');
					var span = document.getElementsByClassName("mm")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#gird").html("");
					}					
			$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
					
			$.ajax({
				type: "post",
				url: "common/cfAadharLinkCountReport.php",
				data: "action=GetLearnerDetails&batchcode="+batchcode,
				success: function (data) {
					$('#responses').empty();
					$("#gird").html(data);											
					$('#examples').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}
			});
		});
		
		
		
        $("#btnSubmit").click(function () {
				$('#response').empty();
				$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            
                var url = "common/cfAadharLinkCountReport.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Show')
                {
                    data = "action=GetCertificateCount&eventid=" + ddlevent.value + "&batchtype=" + ddlBatchType.value + ""; // serializes the form's elements.				 
                }
                else
                {
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						$('#response').empty();
						if(data=='e'){							
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Select Exam Event.</span></div>");
						}
						else if(data=='t'){							
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Select Batch Type.</span></div>");
						}
						else if(data=='0'){							
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Something went wrong.</span></div>");
						}												
						else{
							$("#grid").html(data);
                            $('#example').DataTable();
						}
                    }
                });
           
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
</html>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmadmissiontrans_valid.js" type="text/javascript"></script>	