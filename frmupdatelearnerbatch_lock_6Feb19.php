<?php
$title = "Update Learner Batch";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var CourseBatch=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var CourseBatch=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '1') {
    
} else {
    ?>
    <script> window.location.href = "logout.php";</script> 
    <?php
}
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">RS-CIT Learner Migration</div>

            <div class="panel-body">           
                <form name="frmupdatelearnerbatch" id="frmupdatelearnerbatch" class="form-inline" role="form" method="post" > 
                    <div class="container" style="width:100%;">

                        <div id="response"></div>


                        <div id="errorBox"></div>                       

                        <div class="container">
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                        onchange="try {
                                                    setCustomValidity('')
                                                } catch (e) {
                                                }">  </select>
                            </div> 


                        </div>


                        <div class="container">                       
                            <input type="button" name="btnShowData" id="btnShowData" class="btn btn-primary"
                                   value="Show Learners"/>			
                        </div>

                        <div id="grid" style="margin-top:35px;"> 

                        </div>
                        <div class="container" id="transferdiv" style="display: none">                       
                            <input type="button" name="btn-report-submit" id="btn-report-submit" class="btn btn-primary"
                                   value="Transfer Learners"/>			
                        </div>
                </form> 

            </div>
        </div>   
    </div>
</div>

</div>
<?php
include ('footer.php');
include'common/message.php';
?>
</body>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfUpdatelearnerbatch.php",
                data: "action=FILLCourse",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();


        $("#btnShowData").click(function () {
            if ($("#frmupdatelearnerbatch").valid())
            {
                $('#response').empty();
                $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                var forminput = $("#frmupdatelearnerbatch").serialize();
                var data;
                data = "action=VIEWREPORT&" + forminput;
                $.ajax({
                    url: "common/cfUpdatelearnerbatch.php",
                    type: "POST",
                    data: data,
                    success: function (data)
                    {

                        $('#response').empty();
                        $("#grid").html(data);
                        $('#example').DataTable({

                            scrollY: 400,
                            scrollX: 100,
                            scrollCollapse: true,
                            paging: false,

                        });
                        if (data == "NoDetails") {
                            $("#transferdiv").hide();
                        } else {
                            $("#transferdiv").show();
                        }

                    }
                });
            }
            return false;
        });

        $("#ddlCourse").on('change', function () {
            $("#grid").html('');
              $("#transferdiv").hide();

        });
        $("#grid").on("click", ".viewdata", function () {
            var mybtn = $(this).attr("value");
            var btnnn = '#chkbook';
            var add = btnnn + mybtn;

            if (this.checked) {
                $(add).attr("disabled", false);
            } else {
                $(add).attr("disabled", true);
                $(add).removeAttr('checked');
            }
        });

        $('#grid').on('click', '#checkuncheckall', function () {
            //checkuncheckall(this.checked);
            if (this.checked) {
                $('.viewdata').not(this).prop('checked', this.checked);
                $('.bookdata').not(this).prop('checked', this.checked);
                $('.bookdata').attr("disabled", false);
            } else {
                $('.bookdata').attr("checked", false);
                $('.viewdata').attr("checked", false);
                $('.bookdata').attr("disabled", true);
            }

        });
        $("#btn-report-submit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfUpdatelearnerbatch.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmupdatelearnerbatch").serialize();
            //alert(forminput);			

            data = "action=UpdateBatch&" + forminput; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmupdatelearnerbatch.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmslaprocess");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });

    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
</html>