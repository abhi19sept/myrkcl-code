<?php
$title = "Push Message Notification";
include ('header.php');
include ('root_menu.php');
include ('common/message.php');
if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_LoginId']=="abhays") {
    // you are in
} else {
    header('Location: logout.php');
    exit;
}
?>
<style>
    ul.tab {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Float the list items side by side */
    ul.tab li {float: left;}

    /* Style the links inside the list items */
    ul.tab li a {
        display: inline-block;
        color: black;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of links on hover */
    ul.tab li a:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    ul.tab li a:focus, .active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>
<div style="min-height:350px !important;max-height:1500px !important">	
    <div class="container" style=" margin-top:50px">
        <div class="row">
            <div class="col-md-12" >
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-bookmark"></span> Push Message Notification </h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12 bhoechie-tab-container">
                            <div class="col-md-10 bhoechie-tab" style="width: 100%;">
                                <div class="bhoechie-tab-content active">
                                    <center>
                                        <div class="panel panel-default credit-card-box">
                                            <ul class="tab">
                                                <li><a href="javascript:void(0)" id="showsliderdata" class="tablinks active" onclick="openCity(event, 'View')">Message List</a></li>
                                                <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Add')">Send New Message   </a></li>
                                            </ul>
                                            <div id="View" class="tabcontent" style="display: block;">
<!--                                                <h3>Current Message Notification List</h3>-->
                                                <div id="grid" style="margin-top:15px;"></div> 
                                            </div>
                                            <div id="Add" class="tabcontent">
                                                <h3>Push New Message</h3>
                                                <form class="form-horizontal" action="common/cfpushsmsnotification.php" name="pushmessageForm" id="pushmessageForm" enctype="multipart/form-data">
                                                    <input type="hidden" name="action" id="action" value="AddMessage"/>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label">Send Message To </label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control" id="ddlroll" name="ddlroll" multiple="multiple" required>
                                                               
                                                            </select>
                                                            <input  id="selectedddlroll" name="selectedddlroll" type="hidden">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label">Message Description </label>
                                                        <div class="col-sm-6">
                                                            <textarea class="form-control" id="txtMessageDes" name="txtMessageDes" placeholder="Enter Message Description" type="text" required></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                                                        <div class="col-sm-6">
                                                            <select class="form-control" id="Status" name="Status" required>
                                                                <option value="">--Select Status--</option>
                                                                <option value="1">Active</option>
                                                                <option value="0">Deactive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputPassword3" class="col-sm-3 control-label">Attach File</label>
                                                        <div class="col-sm-6">
                                                            <input class="form-control" id="messagefile" name="messagefile" type="file"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-3 col-sm-6" id="response"></div>
                                                    </div>
                                                    <div class="form-group last">
                                                        <div class="col-sm-offset-3 col-sm-6">
                                                            <button type="submit" name="btn-message-submit"  id="btn-message-submit" class="btn btn-success btn-sm">Submit</button>
                                                            <button type="reset" class="btn btn-default btn-sm">Reset</button>			
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Update Process Guide Popup -->
<script>
// Message Varibale...
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    var Success = "<?php echo Message::SuccessfullyFetch; ?>";

    $("#btnShowHead").click(function () {
        $("#EnterHeading").hide();
        $("#SelectHeading").show();
    });
    $("#btnEnterHead").click(function () {
        $("#SelectHeading").hide();
        $("#EnterHeading").show();
    });
    function FillRoll() {
        $.ajax({
            type: "post",
            url: "common/cfpushsmsnotification.php",
            data: "action=FILLROLL",
            success: function (data) {
                $("#ddlroll").html(data);
            }
        });
    }
    FillRoll();
    $("#ddlroll").change(function(){
        var selMulti = $.map($("#ddlroll option:selected"), function (el, i) {
             return $(el).val();
         });
         var topival = selMulti.join(",");
        $("#selectedddlroll").val(topival);
    });
//Add New Process Guide...... 
    $("#pushmessageForm").on('submit', (function (e) {
        e.preventDefault();
        $('#response').empty();
        $('#response').append("<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>");
        $.ajax({
            url: "common/cfpushsmsnotification.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data)
            {
                //alert(data);
                if (data == SuccessfullyInsert)
                {
                    $('#response').empty();
                    $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>Message Send Successfully..</span></div>");
                    window.setTimeout(function () {
                        $('#response').empty();
                        window.location.href = "frmpushsmsnotification.php";
                        //$("#showsliderdata").click();
                    }, 2000);
                } else
                {
                    $('#response').empty();
                    $('#response').append("<div class='alert-success'><span><img src=images/warning.jpg width=10px /></span><span>Error To Send Message (" + data + ")</span></div>");
                    window.setTimeout(function () {
                        $('#response').empty();
                        //$("#showsliderdata").click();
                    }, 2000);
                }
            },
            error: function (e)
            {
                $("#errmsg").html(e).fadeIn();
            }
        });
    }));

//View Data.....Show
    $(document).ready(function () {
        $("#showsliderdata").click();
    });
    $("#showsliderdata").click(function () {
        $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
        setTimeout(function () {
            $('#grid').load();
        }, 2000);
        var url = "common/cfpushsmsnotification.php"; // the script where you handle the form input.
        var data;
        data = "action=SHOW&status=1"; // serializes the form's elements.				 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
                $("#grid").html(data);
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
                data = $.parseJSON(data);
                filesrc.value = data[0].filepath;
            }
        });
    });
//Deleting A Record From Table And File Unlink....
    $("#grid").on("click", ".fun_change_status", function () {
        var del_id = $(this).attr("id");
        var status = $(this).attr("name");
        $('#response').empty();
        var result = confirm('Are You Sure Want To Change The Status');
        if (result == true) {
            $.ajax({
                type: "post",
                url: "common/cfpushsmsnotification.php",
                data: "action=ChangeStatus&messageid=" + del_id + "&status=" + status + "",
                success: function (data) {
                    //alert(data);
                    $("#showsliderdata").click();
                }
            });
        } else {
            return false;
        }
    });
    $("#grid").on("click", ".fun_delete_message", function () {
        var del_id = $(this).attr("id");
        var status = $(this).attr("name");
        $('#response').empty();
        var result = confirm('Are You Sure Want To Delete The Message');
        if (result == true) {
            $.ajax({
                type: "post",
                url: "common/cfpushsmsnotification.php",
                data: "action=DeleteMessage&messageid=" + del_id + "&status=" + status + "",
                success: function (data) {
                    //alert(data);
                    $("#showsliderdata").click();
                }
            });
        } else {
            return false;
        }
    });
// Tab Events Changes Script....    
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>

</body>
<?php include ('footer.php'); ?>
