<?php
    $title = "Organization Details";
    include('header.php');
    include('root_menu.php');
    include 'common/modals.php';

    echo "<script>var OrgCode = '" . $_SESSION['User_Code'] . "'; </script>";


    if ($_SESSION["User_UserRoll"] <> 7) {
        echo "<script>$('#unauthorized').modal('show')</script>";
        die;
    }

?>

<script src="bootcss/js/stringEncryption.js"></script>
<script>
    var reference_number = decryptString('<?php echo $_REQUEST["code"];?>');
    var reference_number_name = decryptString('<?php echo $_REQUEST["code"];?>');
</script>

<style>

    .btn-success {
        background-color: #00A65A !important;
    }

    .btn-success:hover {
        color: #fff !important;
        background-color: #04884D !important;
        border-color: #398439 !important;
    }

    .asterisk {
        color: red;
        font-weight: bolder;
        font-size: 18px;
        vertical-align: middle;
    }

    .division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 20px;
        color: #575c5f;
        margin-bottom: 20px;

    }

    .extra-footer-class {
        margin-top: 0;
        margin-bottom: -16px;
        padding: 16px;
        background-color: #fafafa;
        border-top: 1px solid #e5e5e5;
    }

    #errorBox {
        color: #F00;
    }

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eeeeee;
        box-shadow: inset 0 0 5px 1px #d5d5d5;
    }

    .form-control {
        border-radius: 2px;
    }

    input[type=text]:hover,
    textarea:hover {
        box-shadow: 0 1px 3px #aaa;
        -webkit-box-shadow: 0 1px 3px #aaa;
        -moz-box-shadow: 0 1px 3px #aaa;
    }

    .col-sm-3:hover {
        background: none !important;
    }

    .hidden-xs {
        display: inline-block !important;
    }

    input.parsley-success,
    select.parsley-success,
    textarea.parsley-success {
        color: #468847;
        background-color: #DFF0D8;
        border: 1px solid #D6E9C6;
    }

    input.parsley-error,
    select.parsley-error,
    textarea.parsley-error {
        color: #B94A48;
        background-color: #F2DEDE;
        border: 1px solid #EED3D7;
    }

    .parsley-errors-list {
        margin: 2px 0 3px;
        padding: 0;
        list-style-type: none;
        font-size: 0.9em;
        line-height: 0.9em;
        opacity: 0;

        transition: all .3s ease-in;
        -o-transition: all .3s ease-in;
        -moz-transition: all .3s ease-in;
        -webkit-transition: all .3s ease-in;
    }

    .parsley-errors-list.filled {
        opacity: 1;
    }

    .parsley-required {
        color: tomato;
        font-family: Calibri;
        margin-top: 4px;
        font-size: 15px;
    }

    select[disabled] {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 0.01px;
        text-overflow: '';
    }
</style>

<script src="bootcss/js/parsley.min.js"></script>


<div class="container" id="showdata" style="display: none;">


    <div class="panel panel-primary" style="margin-top:46px !important;">

        <div class="panel-heading">Organization Details</div>
        <div class="panel-body">

            <div style="padding-bottom: 5px;font-size: 15px;color: #575c5f;">
                Fields marked with <span class="asterisk">*</span> are mandatory
            </div>

            <form class="form-horizontal" style="margin-top: 10px; display: none;" method="POST" id="address" name="address" enctype="multipart/form-data">

                <div class="division_heading">
                    Center Details
                </div>


                <div class="box-body" style="margin: 0 100px;">

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Organization Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center" readonly='readonly'>
                            <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Type of Organization <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <select id="txtType" name="txtType" class="form-control" data-parsley-required="true">
                                <option selected="selected" value="">Select</option>
                                <option value="1">
                                    RS-CIT
                                </option>
                            </select>
                            <input type="hidden" name="Organization_Type_old" id="Organization_Type_old">
                        </div>
                    </div>

<div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Reason of Address Change <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <select id="ddlReason" name="ddlReason" class="form-control" data-parsley-required="true">
                                <option selected="selected" value="">Select</option>
                                <option value="Shifting to own location">Shifting to own location</option>
                                 <option value="Shifting to prime business area">Shifting to prime business area</option>
                                  <option value="Non-agreement with Landlord ">Non-agreement with Landlord </option>
                            </select>
                            <input type="hidden" name="Organization_Type_old" id="Organization_Type_old">
                        </div>
                    </div>
                </div>

                <div class="division_heading">
                    Location Details
                </div>

                <div class="box-body" style="margin: 0 100px;">

                    <div class="form-group">
                        <label for="ddlCountry" class="col-sm-3 control-label">Country</label>
                        <div class="col-sm-3">
                            <input type="hidden" name="ddlCountry" id="ddlCountry">
                            <input type="text" id="country_name" name="country_name" class="form-control" readonly="readonly">
                        </div>

                        <label for="ddlState" class="col-sm-2 control-label">State </label>
                        <div class="col-sm-3">
                            <input type="hidden" id="ddlState" name="ddlState">
                            <input type="text" id="state_name" name="state_name" class="form-control" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ddlRegion" class="col-sm-3 control-label">Division/Region</label>
                        <div class="col-sm-3">
                            <input type="hidden" id="ddlRegion" name="ddlRegion">
                            <input type="text" id="region_name" name="region_name" class="form-control" readonly="readonly">
                        </div>

                        <label for="ddlDistrict" class="col-sm-2 control-label">District</label>
                        <div class="col-sm-3">
                            <input type="hidden" id="ddlDistrict" name="ddlDistrict">
                            <input type="text" id="district_name" name="district_name" class="form-control" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ddlTehsil" class="col-sm-3 control-label">Tehsil</label>
                        <div class="col-sm-8">
                            <input type="hidden" id="ddlTehsil" name="ddlTehsil">
                            <!--<select id="ddlTehsil" name="ddlTehsil" class="form-control"></select>-->
                            <input type="text" id="tehsil_name" name="tehsil_name" class="form-control" readonly="readonly">
                        </div>
                    </div>

                </div>


                <div class="division_heading">
                    Enter Area Details to Modify
                </div>


                <div class="box-body" style="margin: 0 100px;">

                    <div class="form-group">
                        <label for="Organization_NewTehsil" class="col-sm-3 control-label">Select Tehsil<span class="asterisk">*</span></label>
                        <div class="col-sm-2" style="padding-top: 7px;">
                            
                        <select id="ddlTehsilNew" name="ddlTehsilNew" class="form-control" data-parsley-required="true"></select>
                        </div>
                        <label for="area" class="col-sm-3 control-label">Select Area Type <span class="asterisk">*</span></label>

                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_AreaType" value="Rural" id="Rural">
                            <label for="Organization_AreaType">Rural</label>
                        </div>

                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_AreaType" value="Urban" id="Urban" data-toggle="tooltip">
                            <label for="Organization_AreaType">Urban</label>
                        </div>

                        <input type="hidden" id="Organization_AreaType_old" name="Organization_AreaType_old">
                    </div>

                    <div id="urbanArea" style="display: none;">
                        <div class="form-group">
                            <label for="ddlMunicipalType" class="col-sm-3 control-label">Municipality Type <span class="asterisk">*</span></label>
                            <div class="col-sm-3">
                                <select id="ddlMunicipalType" name="ddlMunicipalType" class="form-control"></select>
                                <input type="hidden" id="Organization_Municipality_Type_old" name="Organization_Municipality_Type_old">
                            </div>

                            <label for="ddlDistrict" class="col-sm-2 control-label">Municipality Name <span class="asterisk">*</span></label>
                            <div class="col-sm-3">
                                <select id="ddlMunicipalName" name="ddlMunicipalName" class="form-control"></select>
                                <input type="hidden" id="Organization_Municipal_old" name="Organization_Municipal_old">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ddlWardno" class="col-sm-3 control-label">Ward Number <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <select id="ddlWardno" name="ddlWardno" class="form-control"></select>
                                <input type="hidden" id="Organization_WardNo_old" name="Organization_WardNo_old">
                            </div>
                        </div>
                    </div>

                    <div id="ruralArea" style="display: none;">
                        <div class="form-group">
                            <label for="ddlPanchayat" class="col-sm-3 control-label">Panchayat Samiti/Block <span class="asterisk">*</span></label>
                            <div class="col-sm-3">
                                <select id="ddlPanchayat" name="ddlPanchayat" class="form-control"></select>
                                <input type="hidden" id="Organization_Panchayat_old" name="Organization_Panchayat_old">
                            </div>

                            <label for="ddlGramPanchayat" class="col-sm-2 control-label">Gram Panchayat <span class="asterisk">*</span></label>
                            <div class="col-sm-3">
                                <select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control"></select>
                                <input type="hidden" id="Organization_Gram_old" name="Organization_Gram_old">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ddlVillage" class="col-sm-3 control-label">Village <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <select id="ddlVillage" name="ddlVillage" class="form-control"></select>
                                <input type="hidden" id="Organization_Village_old" name="Organization_Village_old">
                            </div>
                        </div>
                    </div>


                </div>


                <div class="division_heading">
                    Enter Address to Modify
                </div>

                <div class="box-body" style="margin: 0 100px;">

                    <div class="form-group">
                        <label for="area" class="col-sm-3 control-label">Select Premises<br> Ownership Type <span class="asterisk">*</span></label>

                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_OwnershipType" value="Own" id="Own">
                            <label for="Organization_ownershipType">Own</label>
                        </div>

                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_OwnershipType" value="Rent" id="Rent">
                            <label for="Organization_OwnershipType">Rented</label>
                        </div>
                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_OwnershipType" value="Relative" id="Relative">
                            <label for="Organization_OwnershipType">Relative Property</label>
                        </div>
                        
                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_OwnershipType" value="Special" id="Special">
                            <label for="Organization_OwnershipType">Special Case</label>
                        </div>

                        <input type="hidden" id="Organization_OwnershipType_old" name="Organization_OwnershipType_old">
                    </div>
                    <div class="box-body" id="owner" style="margin: 0 10px; display: none;" >

                        <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Enter Owner Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="Owner_Name" id="Owner_Name" placeholder="Owner Name"
                                   data-parsley-required="true" onkeypress="javascript:return validAddress(event);">
                        </div>
                    </div>

                </div>
                <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nearest Landmark</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="landmark" id="landmark" placeholder="Landmark"
                                   data-parsley-required="true" onkeypress="javascript:return validAddress(event);">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="txtAddress" class="col-sm-3 control-label">Full Address for Correspondence&nbsp;<span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <textarea id="txtAddress" name="txtAddress" class="form-control" rows="4" cols="50" data-parsley-required="true" onkeypress="javascript:return validAddress2(event);"></textarea>
                            <input type="hidden" id="Organization_Address_old" name="Organization_Address_old">
                        </div>
                    </div>

                </div>

                <div class="division_heading">
                    Proof of Address Change
                </div>

                <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">Allowed File Type: jpg, jpeg, pdf<br>
                    Allowed Image/PDF File Size: Minimum: 50KB, Maximum, 2000KB</div>

                <br><br><br>

                <div id="onRent" style="display: none;">
                    <div class="box-body" style="margin: 0 100px;">
                        <div class="form-group">
                            <label for="rentAgreement" class="col-sm-3 control-label">Rent Agreement&nbsp;<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input id="fld_rentAgreement" name="fld_rentAgreement" type="file" class="file-loading" >
                                <div id="document_block"></div>
                                <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">
                                    Rent Agreement should be on Rs. 100 Stamp & Notarized
                                </div>
                            </div>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="box-body" style="margin: 0 100px;">
                        <div class="form-group">
                            <label for="utilityBill" class="col-sm-3 control-label">Utility Bill &nbsp;<span
                                        class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input id="fld_utilityBill" name="fld_utilityBill" type="file" class="file-loading" >
                                <div id="document_block"></div>
                                <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">Paid copy of Electricity/Water Bill in the name of premises owner</div>
                            </div>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                </div>

                <div id="onOwn" style="display: none;">
                    <div class="box-body" style="margin: 0 100px;">
                        <div class="form-group">
                            <label for="ownershipDocument" class="col-sm-3 control-label">Ownership Document&nbsp;<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input id="fld_ownershipDocument" name="fld_ownershipDocument" type="file" class="file-loading" >
                                <div id="document_block"></div>
                                <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">
                                    Evidencing support of location ownership
                                </div>
                            </div>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                </div>

                <div class="box-body" style="margin: 0 100px;">
                    <div class="form-group">
                        <label for="fld_document" class="col-sm-3 control-label">Front photo of new location&nbsp;<span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <input id="fld_document" name="fld_document" type="file" class="file-loading"  data-parsley-required="true">
                            <div id="document_block"></div>
                            <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">A clear front photo of new location</div>
                        </div>

                    </div>
                </div>

                <input type="hidden" name="action" value="updateOrgDetails" id="action">
                <input type="hidden" name="reference_number" id="reference_number">

                <!-- /.box-body -->
                <div class="box-footer extra-footer-class">
                    <button type="submit" class="btn btn-lg btn-danger">Submit</button>
                </div>
                <!-- /.box-footer -->
            </form>

            <form class="form-horizontal" style="margin-top: 10px; display: none;" method="POST" id="name" name="name" enctype="multipart/form-data">

                <div class="division_heading">
                    Center Details
                </div>


                <div class="box-body" style="margin: 0 100px;">

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Organization Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="Organization_Name" id="Organization_Name" placeholder="Name of the Organization/Center"
                                   data-parsley-required="true" onkeypress="javascript:return validAddress(event);">
                            <input type="hidden" class="form-control" maxlength="50" name="Organization_Name_old" id="Organization_Name_old"/>
                        </div>
                    </div>

                </div>


                <div class="division_heading">
                    Proof of Identity
                </div>

                <div class="box-body" style="margin: 0 100px;">

                    <div class="form-group">

                        <label for="fld_document" class="col-sm-3 control-label">Upload supporting document&nbsp;<span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <input id="fld_document_name" name="fld_document_name" type="file" class="file-loading"  data-parsley-required="true">
                            <div id="document_block"></div>
                            <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">Registration Document(For eg. Udyog Aadhaar Memorandum etc…)<br>
                                Allowed File Type: jpg, jpeg, pdf<br>
                                Allowed Image/PDF File Size: Minimum: 50KB, Maximum: 2000KB</div>

                            <br>

                        </div>

                    </div>


                </div>

                <input type="hidden" name="action" value="updateOrgDetails_name" id="action">
                <input type="hidden" name="reference_number_name" id="reference_number_name">

                <!-- /.box-body -->
                <div class="box-footer extra-footer-class">
                    <button type="submit" class="btn btn-lg btn-danger">Submit</button>
                </div>
                <!-- /.box-footer -->
            </form>

        </div>
    </div>
</div>


</body>
<?php
    include 'common/message.php';
    include 'footer.php';
?>

<script src="bootcss/js/editRequest.js"></script>


<script>

    function validAddress(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[A-Za-z,-/0-9_]");

        if (key === 8 || key === 0 || key === 32) {
            keychar = "a";
        }
        return reg.test(keychar);
    }

    function validAddress2(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("^[\\w.# ,-/]+$");

        if (key === 8 || key === 0 || key === 32) {
            keychar = "a";
        }
        return reg.test(keychar);
    }

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();

        function sendMailtoITGK() {
            var url = "common/cfmodifyITGK.php";
            var data;
            data = "action=sendEmailToITGK&reference_number=" + reference_number + "&requestType=" + decryptString('<?php echo $_REQUEST["flag"];?>'),
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        if (data === "1") {
                            $("#submitted").modal("show");
                            $("#submitting").modal("hide");
                        } else {
                            alert("Some error occurred, please try again later, redirecting you back to dashboard");
                            setTimeout(function () {
                                window.location.href = 'dashboard.php';
                            }, 2000);
                        }
                    }
                });
        }

        function sendEmailToSP() {
            var url = "common/cfmodifyITGK.php";
            var data;
            data = "action=sendEmailToSP&reference_number=" + reference_number + "&requestType=" + decryptString('<?php echo $_REQUEST["flag"];?>');
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {



                    if (data === "1") {
                        sendMailtoITGK();
                    } else {
                        alert("Some error occurred, please try again later, redirecting you back to dashboard");
                        setTimeout(function () {
                            window.location.href = 'dashboard.php';
                        }, 2000);
                    }
                }
            });
        }

        $("#address").on("submit", function (e) {

            e.preventDefault();
            var form = $(this);
            //form.parsley().validate();
            $("#submitting").modal("show");

            //if (form.parsley().isValid()) {
                var url = "common/cfmodifyITGK.php";
                var data;
                data = new FormData(this);

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function (data) {
                        var result = data.trim();
                        if (result === "Successfully Updated") {
                            sendEmailToSP();
                        } else {
                            $("#submitting").modal("hide");
                            alert(data);
                        }
                    }
                });
//            } else {
//                $("#submitting").modal("hide");
//                alert("Rural To Urban change Found");
//            }
            return false;
        });

        $("#name").on("submit", function (e) {

            e.preventDefault();
            var form = $(this);
            form.parsley().validate();
            $("#submitting").modal("show");

            if (form.parsley().isValid()) {
                var url = "common/cfmodifyITGK.php";
                var data;
                data = new FormData(this);

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function (data) {

                        if (data === "Successfully Updated") {
                            sendEmailToSP();
                        } else {
                            $("#submitting").modal("hide");
                            alert(data);
                        }
                    }
                });
            } else {
                $("#submitting").modal("hide");
            }
            return false;
        });

        $('#otp_modal_updateAddressITGK').on('hidden.bs.modal', function () {

            reference_number = $("#otp_updateAddressITGK").val();
            $("#reference_number").val(reference_number);

            $("#errorTextOTP_updateAddressITGK, #successText_updateAddressITGK, #responseText_updateAddressITGK").html("");
            $("#showError_updateAddressITGK, #showSuccess_updateAddressITGK, #responseText_updateAddressITGK, #verifyOTP_updateAddressITGK").css("display", "none");
            $("#newMobile_updateAddressITGK, #otp_updateAddressITGK").removeAttr("readonly");
            $("#newMobile_updateAddressITGK, #otp_updateAddressITGK").val("");
            $("#sendOTP_updateAddressITGK").text("Send OTP");
            $("#sendOTP_updateAddressITGK").removeClass("disabled");
        });

        function sendOTPtoITGK() {

            var phoneNumber = $("#newMobile_updateAddressITGK").val();
            var url = "common/cfmodifyITGK.php"; // the script where you handle the form input.
            var data;
            var forminput = "phoneNumber=" + phoneNumber;
            data = "action=sendOTP&" + forminput + "&fld_requestChangeType=" + decryptString('<?php echo $_REQUEST["flag"];?>'), // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        $("#responseText").html(data);
                        $("#responseText").css("display", "block");

                    }
                });
        }

        $("#sendOTP_updateAddressITGK").click(function (e) {

            e.preventDefault();

            $("#sendOTP_updateAddressITGK").text("Please wait...");
            $("#sendOTP_updateAddressITGK").addClass("disabled");

            var pattern = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;

            var phoneNumber = $("#newMobile_updateAddressITGK").val();

            if (pattern.test(phoneNumber)) {
                if (phoneNumber.length == 10) {

                    $("#errorTextOTP, #successText").html("");
                    $("#showError, #showSuccess").css("display", "none");

                    var url = "common/cfmodifyITGK.php"; // the script where you handle the form input.
                    var data;
                    var forminput = "phoneNumber=" + phoneNumber;
                    data = "action=checkMobile&" + forminput; // serializes the form's elements.


                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data) {

                            var JSONObject = JSON.parse(data);

                            if (JSONObject.total_count === "0") {
                                $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
                                $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
                                $("#errorTextOTP_updateAddressITGK").html("Mobile number not found in our records");
                                $("#showError_updateAddressITGK").css("display", "block");
                                $("#sendOTP_updateAddressITGK").text("Send OTP");
                                $("#sendOTP_updateAddressITGK").removeClass("disabled");
                            } else {
                                sendOTPtoITGK();
                            }
                        }
                    });

                } else {
                    $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
                    $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
                    $("#errorTextOTP_updateAddressITGK").html("Invalid mobile number");
                    $("#showError_updateAddressITGK").css("display", "block");
                    $("#sendOTP_updateAddressITGK").text("Send OTP");
                    $("#sendOTP_updateAddressITGK").removeClass("disabled");
                }
            }
            else {
                $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
                $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
                $("#errorTextOTP_updateAddressITGK").html("Invalid mobile number");
                $("#showError_updateAddressITGK").css("display", "block");
                $("#sendOTP_updateAddressITGK").text("Send OTP");
                $("#sendOTP_updateAddressITGK").removeClass("disabled");
            }
        });

        $("#verifyOTPbtn_updateAddressITGK").click(function () {

            $("#newMobile_updateAddressITGK").attr("readonly", "readonly");
            $("#otp_updateAddressITGK").attr("readonly", "readonly");
            if ($("#otp_updateAddressITGK").val() == "") {
                $("#otp_updateAddressITGK").removeAttr("readonly");
                $("#otp_updateAddressITGK").val("");
                $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
                $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
                $("#errorTextOTP_updateAddressITGK").html("Invalid OTP");
                $("#showError_updateAddressITGK").css("display", "block");
            } else {
                var url = "common/cfmodifyITGK.php"; // the script where you handle the form input.
                var data;
                var forminput = "otp=" + $("#otp_updateAddressITGK").val();
                forminput = forminput + "&learner=" + $("#LearnerCode").val() + "&mobile=" + $("#newMobile_updateAddressITGK").val();
                data = "action=verifyOTP&" + forminput; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        var JSONObject = JSON.parse(data);
                        if (JSONObject.total_count === "0") {
                            $('#successText_updateAddressITGK').html('');
                            $('#showSuccess_updateAddressITGK').css('display', 'none');
                            $('#errorTextOTP_updateAddressITGK').html('Invalid OTP');
                            $('#showError_updateAddressITGK').css('display', 'block');
                            $("#sendOTP_updateAddressITGK").text("Send OTP");
                            $("#sendOTP_updateAddressITGK").removeClass("disabled");
                            $("#otp_updateAddressITGK").removeAttr("readonly");
                        } else {
                            $('#showError_updateAddressITGK').css('display', 'none');
                            $('#successText_updateAddressITGK').html('OTP has been successfully verified, you can now proceed to update address<BR>Redirecting you to the correct page');
                            $('#showSuccess_updateAddressITGK').css('display', 'block');
                            $('#verifyOTP').css('display', 'block');

                            setTimeout(function () {
                                $('#loading').modal('show');
                                $('#otp_modal_updateAddressITGK').modal('hide');
                                fillForm();
                            }, 1000);
                        }
                    }
                });
            }
        });

        $("#ddlMunicipalType").change(function () {
            var reg = $("#ddlDistrict").val();
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalName&values=" + reg + "",
                success: function (data) {
                    $('#ddlMunicipalName').html(data);
                }
            });
        });

        $("#ddlMunicipalName").change(function () {
            var selMunicipalName = $(this).val();
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FILLWardno&values=" + selMunicipalName + "",
                success: function (data) {
                    $('#ddlWardno').html(data);
                }
            });
        });


        $("#ddlPanchayat").change(function () {
            var selPanchayat = $(this).val();
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                success: function (data) {
                    $('#ddlGramPanchayat').html(data);
                }
            });
        });

        $("#ddlGramPanchayat").change(function () {
            var selGramPanchayat = $(this).val();
            $.ajax({
                url: 'common/cfVillageMaster.php',
                type: "post",
                data: "action=FILL&values=" + selGramPanchayat + "",
                success: function (data) {
                    $('#ddlVillage').html(data);
                }
            });
        });

        $("#Own").click(function () {
            $("#onRent").css("display", "none");
            $("#onOwn").css("display", "block");

            $("#fld_rentAgreement").attr('data-parsley-required', 'false');
            $("#fld_utilityBill").attr('data-parsley-required', 'false');
            $("#fld_ownershipDocument").attr('data-parsley-required', 'true');
        });

        $("#Rent").click(function () {
            $("#onRent").css("display", "block");
            $("#onOwn").css("display", "none");

            $("#fld_rentAgreement").attr('data-parsley-required', 'true');
            $("#fld_utilityBill").attr('data-parsley-required', 'true');
            $("#fld_ownershipDocument").attr('data-parsley-required', 'false');
        });
        
    $("#Relative").click(function () {
        $("#onRent").css("display", "block");
        $("#onOwn").css("display", "none");
        
        jQuery("label[for='rentAgreement']").html("Ownership Document with Relative Name");
        jQuery("label[for='utilityBill']").html("NOC Certificate from Relative");
        
        $("#fld_rentAgreement").attr('data-parsley-required', 'true');
        $("#fld_utilityBill").attr('data-parsley-required', 'true');
        $("#fld_ownershipDocument").attr('data-parsley-required', 'false');
    });
    
    $("#Special").click(function () {
        $("#onRent").css("display", "block");
        $("#onOwn").css("display", "none");
        
        jQuery("label[for='rentAgreement']").html("Authority Letter by AUTHORIZED PERSON");
        jQuery("label[for='utilityBill']").html("NOC by AUTHORIZED PERSON");
        
        $("#fld_rentAgreement").attr('data-parsley-required', 'true');
        $("#fld_utilityBill").attr('data-parsley-required', 'true');
        $("#fld_ownershipDocument").attr('data-parsley-required', 'false');
    });


        $("#Rural").click(function () {
            FillPanchayat($("#ddlDistrict").val());
            $("#ruralArea").css("display", "block");
            $("#urbanArea").css("display", "none");
        });

        $("#Urban").click(function () {
            FillMunicipalType();
            FillMunicipalName($("#ddlDistrict").val());
            FILLWardno($("#ddlDistrict").val());
            $("#ruralArea").css("display", "none");
            $("#urbanArea").css("display", "block");
        });

        function FillCountry() {
            $.ajax({
                type: "post",
                url: "common/cfCountryMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCountry").html(data);
                }
            });
        }

        function FillState() {
            $.ajax({
                url: 'common/cfStateMaster.php',
                type: "post",
                data: "action=FILLALL",
                success: function (data) {
                    $('#ddlState').html(data);
                }
            });
        }

        function FillRegion(reg) {
            $.ajax({
                url: 'common/cfRegionMaster.php',
                type: "post",
                data: "action=FILL&values=" + reg + "",
                success: function (data) {
                    $('#ddlRegion').html(data);
                }
            });
        }

        function FillDistrict(reg) {
            $.ajax({
                url: 'common/cfDistrictMaster.php',
                type: "post",
                data: "action=FILL&values=" + reg + "",
                success: function (data) {
                    $('#ddlDistrict').html(data);
                }
            });
        }

        function FillTehsil(reg) {
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + reg + "",
                success: function (data) {
                    $('#ddlTehsil').html(data);
                    $('#ddlTehsilNew').html(data);
                }
            });
        }

        function FillMunicipalType() {
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalType",
                success: function (data) {
                    $('#ddlMunicipalType').html(data);
                }
            });
        }

        function FillMunicipalName(reg) {
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalName&values=" + reg + "",
                success: function (data) {
                    $('#ddlMunicipalName').html(data);
                }
            });
        }

        function FILLWardno(reg) {
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FILLWardno&values=" + reg + "",
                success: function (data) {
                    $('#ddlWardno').html(data);
                }
            });
        }

        function FillPanchayat(reg) {
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLPanchayat_NEW&values=" + reg + "",
                success: function (data) {
                    $('#ddlPanchayat').html(data);
                }
            });
        }

        function FillGramPanchayat(reg) {
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLGramPanchayat_NEW&values=" + reg + "",
                success: function (data) {
                    $('#ddlGramPanchayat').html(data);
                }
            });
        }

        function FillVillage(reg) {
            $.ajax({
                url: 'common/cfVillageMaster.php',
                type: "post",
                data: "action=FILL_NEW&values=" + reg + "",
                success: function (data) {
                    $('#ddlVillage').html(data);
                }
            });
        }

        $("#revokeReference").on("click", function () {
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=revokeReference&otp=" + reference_number + "",
                success: function (data) {
                    $("#errorModal_Custom_backdrop").modal("hide");
                    if (data === "Successfully Updated") {
                        $("#revokeText").html("Reference Number <span style='color: #00A65A'>" + reference_number + "</span> has been successfully revoked, redirecting to correct page.." +
                            ".");
                        $("#revoke_success").modal("show");
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }
                }
            });
        });


        function fillForm(data) {

            data = $.parseJSON(data);

            setTimeout(function () {

                /*$("#txtRoad").val(data.Organization_Road);
                $("#Organization_Road_old").val(data.Organization_Road);

                $("#txtStreet").val(data.Organization_Street);
                $("#Organization_Street_old").val(data.Organization_Street);

                $("#txtHouseno").val(data.Organization_HouseNo);
                $("#Organization_HouseNo_old").val(data.Organization_HouseNo);

                $("#txtLandmark").val(data.Organization_Landmark);
                $("#Organization_Landmark_old").val(data.Organization_Landmark);*/

                $("#txtAddress").val(data.Organization_Address);
                $("#Organization_Address_old").val(data.Organization_Address);

                FillCountry();
                FillState();
                FillRegion(data.Organization_State);
                FillDistrict(data.Organization_Region);
                FillTehsil(data.Organization_District);

                $("#txtType").val(data.Organization_Type);
                $("#Organization_Type_old").val(data.Organization_Type);

                $("#txtName1").val(data.Organization_Name);
                $("#Organization_Name").val(data.Organization_Name);
                $("#Organization_Name_old").val(data.Organization_Name_old);

                $("#txtRegno").val(data.Organization_RegistrationNo);
                $("#txtEstdate").val(data.Organization_FoundedDate);

                $("#" + data.Organization_AreaType).prop("checked", "true");
                $("#" + data.Organization_OwnershipType).prop("checked", "true");

                if (data.Organization_AreaType === "Rural") {

//                    $("#Urban").attr("disabled", "disabled");
//                    $("#Urban").attr("title", "You cannot change the area type from Rural to Urban");

                    $("#ruralArea").css("display", "block");
                    $("#urbanArea").css("display", "hide");

                    $("#ddlPanchayat").attr('data-parsley-required', 'true');
                    $("#ddlGramPanchayat").attr('data-parsley-required', 'true');
                    $("#ddlVillage").attr('data-parsley-required', 'true');

                    $("#ddlMunicipalType").attr('data-parsley-required', 'false');
                    $("#ddlDistrict").attr('data-parsley-required', 'false');
                    $("#ddlWardno").attr('data-parsley-required', 'false');

                    FillPanchayat(data.Organization_District);
                    FillGramPanchayat(data.Organization_Panchayat);
                    FillVillage(data.Organization_Gram);
                }

                if (data.Organization_AreaType === "Urban") {
                    $("#urbanArea").css("display", "block");
                    $("#ruralArea").css("display", "hide");

                    $("#ddlMunicipalType").attr('data-parsley-required', 'true');
                    $("#ddlDistrict").attr('data-parsley-required', 'true');
                    $("#ddlWardno").attr('data-parsley-required', 'true');

                    $("#ddlPanchayat").attr('data-parsley-required', 'false');
                    $("#ddlGramPanchayat").attr('data-parsley-required', 'false');
                    $("#ddlVillage").attr('data-parsley-required', 'false');

                    FillMunicipalType();
                    FillMunicipalName(data.Organization_District);
                    FILLWardno(data.Organization_Municipality_Raj_Code);
                }


                if (data.Organization_OwnershipType === "Own") {
                    $("#onRent").css("display", "none");
                    $("#onOwn").css("display", "block");
                    $("#fld_rentAgreement").attr('data-parsley-required', 'false');
                    $("#fld_utilityBill").attr('data-parsley-required', 'false');
                    $("#fld_ownershipDocument").attr('data-parsley-required', 'true');
                }

                if (data.Organization_OwnershipType === "Rent") {
                    $("#onRent").css("display", "block");
                    $("#onOwn").css("display", "none");
                    $("#fld_rentAgreement").attr('data-parsley-required', 'true');
                    $("#fld_utilityBill").attr('data-parsley-required', 'true');
                    $("#fld_ownershipDocument").attr('data-parsley-required', 'false');
                    $("#owner").show();
                }


            }, 500);

            setTimeout(function () {
                $("#ddlCountry").val(data.Organization_Country);
                $("#country_name").val(data.Organization_Country_Name);

                $("#ddlState").val(data.Organization_State);
                $("#state_name").val(data.Organization_State_Name);

                $("#ddlRegion").val(data.Organization_Region);
                $("#region_name").val(data.Organization_Region_Name);

                $("#ddlDistrict").val(data.Organization_District);
                $("#district_name").val(data.Organization_District_Name);

                $("#ddlTehsil").val(data.Organization_Tehsil);
                $("#ddlTehsilNew").val(data.Organization_Tehsil);
                $("#tehsil_name").val(data.Organization_Tehsil_Name);

                $("#Organization_AreaType_old").val(data.Organization_AreaType);

                $("#ddlPanchayat").val(data.Organization_Panchayat);
                $("#Organization_Panchayat_old").val(data.Organization_Panchayat);

                $("#ddlGramPanchayat").val(data.Organization_Gram);
                $("#Organization_Gram_old").val(data.Organization_Gram);

                $("#ddlVillage").val(data.Organization_Village);
                $("#Organization_Village_old").val(data.Organization_Village);

                $("#ddlMunicipalType").val(data.Organization_Municipality_Type);
                $("#Organization_Municipality_Type_old").val(data.Organization_Municipality_Type);

                $("#ddlMunicipalName").val(data.Organization_Municipality_Raj_Code);
                $("#Organization_Municipal_old").val(data.Organization_Municipality_Raj_Code);

                $("#ddlWardno").val(data.Organization_Ward_Raj_Code);
                $("#Organization_WardNo_old").val(data.Organization_Ward_Raj_Code);
            }, 5000);

            setTimeout(function () {
                $('#loading').modal('hide');
                $('#showdata').css('display', 'block');
            }, 5000);

            $("#reference_number").val(reference_number);
            $("#reference_number_name").val(reference_number);
        }

        function firstCheck_edit(flag, code) {
            $('#loading').modal('show');

            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=firstCheck_edit&fld_requestChangeType=" + decryptString(flag) + "&code=" + decryptString(code),
                success: function (data) {

                    var JSONObject = JSON.parse(data);

                    if (JSONObject.row_count === 0) {
                        $('#loading').modal('hide');
                        $("#errorText2").html('Could not find the record');
                        $("#errorModal_Custom2").modal('show');
                    } else {
                        $("#" + decryptString(flag)).css("display", "block");
                        fillForm(data);
                    }
                }
            });
        }


        $('#address').parsley();


        function FillOrgType() {
            $.ajax({
                type: "post",
                url: "common/cfOrgTypeMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#txtType").html(data);
                }
            });
        }

        FillOrgType();

        $("#txtType").change(function () {
            $.ajax({
                url: 'common/cfOrgDetail.php',
                type: "post",
                data: "action=FILL",
                success: function (data) {
                    $('#txtDoctype').html(data);
                }
            });
        });

        $("#ddlCountry").change(function () {
            var selcountry = $(this).val();
            $.ajax({
                url: 'common/cfStateMaster.php',
                type: "post",
                data: "action=FILL&values=" + selcountry + "",
                success: function (data) {
                    $('#ddlState').html(data);
                }
            });
        });
        $("#ddlState").change(function () {
            var selState = $(this).val();
            $.ajax({
                url: 'common/cfRegionMaster.php',
                type: "post",
                data: "action=FILL&values=" + selState + "",
                success: function (data) {
                    $('#ddlRegion').html(data);
                }
            });
        });
        $("#ddlRegion").change(function () {
            var selregion = $(this).val();
            $.ajax({
                url: 'common/cfDistrictMaster.php',
                type: "post",
                data: "action=FILL&values=" + selregion + "",
                success: function (data) {
                    $('#ddlDistrict').html(data);
                }
            });
        });
        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    $('#ddlTehsil').html(data);
                }
            });
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

        firstCheck_edit("<?php echo $_REQUEST['flag'];?>", "<?php echo $_REQUEST['code']?>");

        $("#fld_document, #fld_document_name, #fld_utilityBill, #fld_rentAgreement, #fld_ownershipDocument").fileinput({
            previewFileType: "image",
            allowedFileExtensions: ["jpg", "jpeg", "pdf"],
            previewClass: "bg-warning",
            showUpload: false,
            minImageWidth: 500,
            minImageHeight: 500,
            maxImageWidth: 3000,
            maxImageHeight: 3000,
            maxFileCount: 1,
            showCaption: false,
            maxFileSize: 2000,
            minFileSize: 50,
            browseClass: "btn btn-default",
            browseLabel: "Pick File",
            browseIcon: "<i class=\"fa fa-file\" aria-hidden=\"true\"></i> ",
            removeClass: "btn btn-danger",
            removeLabel: "Delete",
            removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
        });

$("#Rent").change(function () {

            $("#owner").show();
        });
        
        $("#Own").change(function () {

            $("#owner").hide();
        });

    });
</script>

</html>