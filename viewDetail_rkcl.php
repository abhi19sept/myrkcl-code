<?php
$title = "Organization Details";
include('header.php');
include('root_menu.php');
include 'common/modals.php';

if (!in_array($_SESSION["User_UserRoll"], [1, 10, 4])) {
    echo "<script>$('#unauthorized').modal('show')</script>";
    die;
}

echo "<script>var OrgCode = '" . $_SESSION['User_Code'] . "'; </script>";
echo "<script>var test = '" . $_REQUEST['Code'] . "'; </script>";
?>

<style type="text/css">

    .asterisk {
        color: red;
        font-weight: bolder;
        font-size: 18px;
        vertical-align: middle;
    }

    .division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 20px;
        color: #000;
        margin-bottom: 20px;

    }

    .extra-footer-class {
        margin-top: 0;
        margin-bottom: -10px;
        padding: 16px;
        background-color: #fafafa;
        border-top: 1px solid #e5e5e5;
    }

    .ref_id {
        text-align: center;
        font-size: 20px;
        color: #000;
        margin: 0 0 10px 0;
    }

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eeeeee;
        box-shadow: inset 0 0 5px 1px #d5d5d5;
    }

    .form-control {
        border-radius: 2px;
    }

    input[type=text]:hover, textarea:hover {
        box-shadow: 0 1px 3px #aaa;
        -webkit-box-shadow: 0 1px 3px #aaa;
        -moz-box-shadow: 0 1px 3px #aaa;
    }

    .col-sm-3:hover {
        background: none !important;
    }

    .modal-open .container-fluid, .modal-open .container {
        -webkit-filter: blur(5px) grayscale(50%);
        filter: blur(5px) grayscale(50%);
    }

    .btn-success {
        background-color: #00A65A !important;
    }

    .btn-success:hover {
        color: #fff !important;
        background-color: #04884D !important;
        border-color: #398439 !important;
    }

    .addBottom {
        margin-bottom: 12px;
    }

    #image_address, #image_ownershipDocument, #image_utilityBill, #image_rentAgreement {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
        cursor: pointer;
    }

    #image_address:hover, #image_ownershipDocument:hover, #image_utilityBill:hover, #image_rentAgreement:hover {
        opacity: 1;
    }

    .middle:hover {
        opacity: 1;
    }

    .floatingHeader {
        position: fixed;
        top: 0;
        visibility: hidden;
        z-index: 99999999;
        background-color: #0B0809;
        color: #FFF;
        opacity: 0.7;
    }

</style>

<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>

<div class="container" id="showdata">
    <div class="panel panel-primary" style="margin-top:46px !important;">

        <div class="panel-heading">Address Update Request Made by Center : <span id="fld_ITGK_Code"></span></div>
        <div class="panel-body">

            <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateOrgDetails" name="updateOrgDetails">

                <div class="ref_id">
                    Reference ID: <span id="ref_id"></span>
                </div>

                <div class="persist-area">
                    <div class="row persist-header">
                        <div class="col-md-6"><h3 style="text-align: center">Previous</h3></div>
                        <div class="col-md-6"><h3 style="text-align: center">Requested</h3></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="division_heading">
                                Center Details
                            </div>


                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12 addBottom">
                                        Organization Name
                                        <input type="text" class="form-control" name="Organization_Name_old" id="Organization_Name_old" placeholder="Name of the Organization/Center"
                                               readonly='readonly'>
                                    </div>


                                    <div class="col-sm-12 addBottom">
                                        Type of Organization
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Type_old" id="Organization_Type_old" readonly='readonly'>
                                    </div>
                                </div>


                            </div>

                            <div class="division_heading">
                                Location Details
                            </div>

                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12 addBottom">
                                        Country
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Country_old" id="Organization_Country_old" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        State
                                        <input type="text" class="form-control" maxlength="50" name="Organization_State_old" id="Organization_State_old" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Division/Region
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Region_old" id="Organization_Region_old" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        District
                                        <input type="text" class="form-control" maxlength="50" name="Organization_District_old" id="Organization_District_old" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Tehsil
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Tehsil_old" id="Organization_Tehsil_old" readonly='readonly'>
                                    </div>
                                </div>

                            </div>


                            <div class="division_heading">
                                Area Details
                            </div>


                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        Area Type
                                        <input type="text" class="form-control" maxlength="50" name="Organization_AreaType_old" id="Organization_AreaType_old" readonly='readonly'>
                                    </div>

                                </div>


                                <div id="urbanArea_old" style="display: block;">

                                    <div class="form-group">

                                        <div class="col-sm-12 addBottom">
                                            Municipality Type
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Municipal_Type_old" id="Organization_Municipal_Type_old"
                                                   readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">
                                            Municipality Name
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Municipal_old" id="Organization_Municipal_old" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">
                                            Ward Number
                                            <input type="text" class="form-control" maxlength="50" name="Organization_WardNo_old" id="Organization_WardNo_old" readonly='readonly'>
                                        </div>

                                    </div>

                                </div>

                                <div id="ruralArea_old" style="display: none;">
                                    <div class="form-group">

                                        <div class="col-sm-12 addBottom">Panchayat Samiti/Block
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Panchayat_old" id="Organization_Panchayat_old" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">Gram Panchayat
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Gram_old" id="Organization_Gram_old" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">Village
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Village_old" id="Organization_Village_old" readonly='readonly'>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="division_heading">
                                Address for Correspondence
                            </div>

                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">
                                    <div class="col-sm-12">Ownership Type
                                        <input type="text" class="form-control" name="Organization_OwnershipType_old" id="Organization_OwnershipType_old"
                                               readonly='readonly'>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">Full Address for Correspondence
                                        <textarea id="Organization_Address_old" name="Organization_Address_old" class="form-control" rows="4" cols="50" readonly></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="division_heading">
                                &nbsp;
                            </div>


                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12 addBottom">
                                        Organization Name
                                        <input type="text" class="form-control" name="Organization_Name" id="Organization_Name" placeholder="Name of the Organization/Center"
                                               readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Type of Organization
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Type" id="Organization_Type" readonly='readonly'>
                                    </div>
                                </div>


                            </div>

                            <div class="division_heading">
                                &nbsp;
                            </div>

                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12 addBottom">
                                        Country
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Country" id="Organization_Country" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        State
                                        <input type="text" class="form-control" maxlength="50" name="Organization_State" id="Organization_State" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Division/Region
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Region" id="Organization_Region" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        District
                                        <input type="text" class="form-control" maxlength="50" name="Organization_District" id="Organization_District" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Tehsil
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Tehsil" id="Organization_Tehsil" readonly='readonly'>
                                    </div>
                                </div>

                            </div>


                            <div class="division_heading">
                                &nbsp;
                            </div>


                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        Area Type
                                        <input type="text" class="form-control" maxlength="50" name="Organization_AreaType" id="Organization_AreaType" readonly='readonly'>
                                    </div>

                                </div>


                                <div id="urbanArea" style="display: block;">

                                    <div class="form-group">

                                        <div class="col-sm-12 addBottom">
                                            Municipality Type
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Municipal_Type" id="Organization_Municipal_Type" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">
                                            Municipality Name
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Municipal" id="Organization_Municipal" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">
                                            Ward Number
                                            <input type="text" class="form-control" maxlength="50" name="Organization_WardNo" id="Organization_WardNo" readonly='readonly'>
                                        </div>

                                    </div>

                                </div>

                                <div id="ruralArea" style="display: none;">
                                    <div class="form-group">

                                        <div class="col-sm-12 addBottom">Panchayat Samiti/Block
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Panchayat" id="Organization_Panchayat" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">Gram Panchayat
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Gram" id="Organization_Gram" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">Village
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Village" id="Organization_Village" readonly='readonly'>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="division_heading">
                                &nbsp;
                            </div>

                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="form-group">
                                        <div class="col-sm-12">Ownership Type
                                            <input type="text" class="form-control" name="Organization_OwnershipType" id="Organization_OwnershipType" readonly='readonly'>
                                        </div>
                                    </div>

                                    <div class="form-group" id="owner" style="display:none;">
                                        <div class="col-sm-12">Owner Name
                                            <input type="text" class="form-control" name="Owner_Name" id="Owner_Name" readonly='readonly'>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">Full Address for Correspondence
                                        <textarea id="Organization_Address" name="Organization_Address" class="form-control" rows="4" cols="50" readonly></textarea>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="division_heading">
                                Uploaded Document
                            </div>

                            <div id="onRent" style="display: none;">
                                <button type="button" id="ViewRentAgreement" class="btn btn-primary" data-toggle="collapse" data-target="#RA"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Rent
                                    Agreement
                                </button>
                                <div class="box-body collapse" style="margin: 0 100px;" id="RA">
                                    <div class="form-group">
                                        <div class="col-sm-12"><br><br>
                                            <iframe id="image_rentAgreement" style="width: 100%; height: 500px; border: none;"></iframe>
                                            <div id="image_rentAgreement_ftp"></div>
                                            <div class="middle">
                                                <div class="text">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="division_heading">
                                    &nbsp;
                                </div>

                                <button type="button" id="ViewUtilityBill" class="btn btn-primary" data-toggle="collapse" data-target="#UB"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Utility Bill</button>
                                <div class="box-body collapse" style="margin: 0 100px;" id="UB">
                                    <div class="form-group">
                                        <div class="col-sm-12"><br><br>
                                            <iframe id="image_utilityBill" style="width: 100%; height: 500px; border: none;"></iframe>
                                            <div id="image_utilityBill_ftp"></div>
                                            <div class="middle">
                                                <div class="text">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="division_heading">
                                    &nbsp;
                                </div>

                                <button type="button" class="btn btn-primary" id="bankdetail" name="bankdetail"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Bank Account Details</button>
                                <div class="panel panel-info" id="bankaccount" style="display:none">
                                    <div class="panel-heading">Bank Account Details</div>
                                    <div class="panel-body">
                                        <div id="bankdatagird"></div>

                                    </div>
                                </div>

                                <div class="division_heading">
                                    &nbsp;
                                </div>
                            </div>


                            <div id="onOwn" style="display: none;">
                                <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#OD"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Ownership Document</button>
                                <div class="box-body collapse" style="margin: 0 100px;" id="OD">
                                    <div class="form-group">
                                        <div class="col-sm-12"><br><br>
                                            <iframe id="image_ownershipDocument" style="width: 100%; height: 500px; border: none;"></iframe>
                                            <div id="image_ownershipDocument_ftp"></div>
                                            <div class="middle">
                                                <div class="text">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="division_heading">
                                    &nbsp;
                                </div>
                            </div>

                            <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#FF"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Front Photo of New Location</button>
                            <div class="box-body collapse" style="margin: 0 100px;" id="FF">
                                <div class="form-group">
                                    <div class="col-sm-12"><br><br>
                                        <iframe id="image_address" style="width: 100%; height: 500px; border: none;"></iframe>
                                        <div id="image_address_ftp"></div>
                                        <div class="middle">
                                            <div class="text">&nbsp;</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <div class="panel panel-info" id="spvisitdiv">
                        <div class="panel-heading">SP Visit Photos</div>
                        <div class="panel-body">
                            <div class="col-sm-3" >
                                <label for="photo">Visit Date:</label> </br>
                                <input type="text" class="form-control" id="EstDate" name="EstDate" readonly="readonly">
                            </div> 
                            <div class="col-sm-2" > 
                                <button type="button" id="showmodal" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
                                <label for="photo">Front Photo:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview1" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-2" > 
                                <label for="photo">Left Photo:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview2" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-2" > 
                                <label for="photo">Right Photo:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview3" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-2" > 
                                <label for="photo">Lab Space:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview4" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                        </div>
                    </div>
                    <p>&nbsp;</p>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="division_heading">
                                Remarks
                            </div>


                            <div class="box-body" style="margin: 0 100px; display: none;" id="sp_remarks">

                                <div class="form-group">
                                    <div class="col-sm-12"><span id="remark_label">Please enter the remarks for action you are taking</span> <span class="asterisk">*</span>
                                        <textarea id="fld_remarks" name="fld_remarks" class="form-control" rows="4" cols="50" style="border-radius: 8px; font-family: Calibri;"></textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="box-body" style="margin: 0 100px;" id="rkcl_remarks">

                                <div class="form-group">
                                    <div class="col-sm-12"><span id="remark_label_rkcl">Please enter the remarks for action you are taking</span> <span class="asterisk">*</span>
                                        <textarea id="fld_remarks_rkcl" name="fld_remarks_rkcl" class="form-control" rows="4" cols="50" style="border-radius: 8px; font-family: Calibri;
                                                  "
                                                  onkeypress="javascript:return validAddress2(event);"></textarea>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="row" id="current_status" style="display: none;">
                        <div class="col-md-12">
                            <div class="division_heading">
                                Status Log
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="pending_submission">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request haven't submitted yet
                                    </div>
                                </div>
                            </div>


                            <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_approval_sp">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request Submitted on <span id="submission_date"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_approval_rkcl">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request Approved by Service Provider on <span id="submission_date_sp"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_payment">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request Approved by You on <span id="submission_date_rkcl"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="rkcl_rejected">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request has been Rejected by You on <span id="rejected_date_rkcl"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="itgk_rejected">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request has been Rejected by ITGK on <span id="rejected_date_itgk"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="sp_rejected">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request has been Rejected by SP on <span id="rejected_date_sp"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="paid">
                                <div class="form-group">
                                    <div class="col-sm-4 text-success">
                                        Paid on <span id="payment_date"></span>
                                    </div>

                                    <div class="col-sm-4 pull-right" style="display: none;">
                                        <button type="button" id="mark" class="btn btn-lg btn-danger">Mark Incorrect Approval</button>
                                    </div>
                                    <div class="col-sm-12 text-danger" id="Marked" style="font-size: 15px; display: none;">
                                        RKCL Marked Incorrect Approval
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <!-- /.box-body -->
                    <div class="box-footer extra-footer-class col-md-12" id="updateActions" >
                        <button type="button" class="btn btn-lg btn-primary" onclick="window.location.href = 'frmAddressUpdateRequestCenterDetail.php'"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back
                        </button>
                        <button type="button" id="commit" class="btn btn-lg btn-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Accept</button>
                        <button type="button" id="reject" class="btn btn-lg btn-danger pull-right"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;Reject</button>
                    </div>
                </div>


                <!-- /.box-footer -->
        </div>


        </form>

    </div>
</div>
</div>
<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="width: 100%;height: 500px;">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">×</button>
                <h3 id="heading-tittle" class="modal-title">Heading</h3>
            </div>
            <div id="viewimagesrc"></div>
            <!--<iframe id="viewimagesrc" src="uploads/news/1487051012.pdf" style="width: 100%;height: 500px;border: none;"></iframe>-->
            <!--<img id="viewimagesrc" class="thumbnail img-responsive" src="images/not-found.png" name="filePhoto3" width="800px" height="880px">-->
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

</body>
<?php
include 'common/message.php';
include 'footer.php';
?>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="bootcss/js/persistentHeader.js"></script>
<script type="text/javascript">

                            $(document).ready(function () {
                                jQuery(".thumbnailmodal").click(function () {
                                    $('.modal-body').empty();
                                    var title = $(this).parent('a').attr("title");
                                    $('.modal-title').html(title);
                                    $($(this).parents('div').html()).appendTo('.modal-body');
                                    $('#showmodal').click();
                                });
                            });
</script>
<script type="text/javascript">

    var image;
    var usercode;
    var Organization_User_Code;
    var fld_ownershipDocument;
    var fld_rentAgreement;
    var fld_utilityBill;

    function validAddress2(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("^[\\w.# ,-/]+$");

        if (key === 8 || key === 0 || key === 32) {
            keychar = "a";
        }
        return reg.test(keychar);
    }

    function showBankAccountData() {
        var CenterCode = $('#fld_ITGK_Code').text();
        $.ajax({
            type: "post",
            url: "common/cfNcrFinalApproval.php",
            data: "action=BANKACCOUNT&centercode=" + CenterCode + "",
            success: function (data) {

                $("#bankdatagird").html(data);
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });


            }
        });
    }

    $("#bankdetail").click(function () {
        showBankAccountData();
        $("#bankaccount").show(3000);
    });


    function fillLocationDetails(Code) {
        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=FillLocationDetails&Code=" + Code + "&fld_ref_id=" + decryptString('<?php echo $_REQUEST['Code'] ?>') + "",
            success: function (data) {
                data = $.parseJSON(data);
                $("#Organization_Country_old").val(data[0].Organization_Country);
                $("#Organization_State_old").val(data[0].Organization_State);
                $("#Organization_Region_old").val(data[0].Organization_Region);
                $("#Organization_District_old").val(data[0].Organization_District);
                $("#Organization_Tehsil_old").val(data[0].Organization_Tehsil_Old);

                $("#Organization_Country").val(data[0].Organization_Country);
                $("#Organization_State").val(data[0].Organization_State);
                $("#Organization_Region").val(data[0].Organization_Region);
                $("#Organization_District").val(data[0].Organization_District);
                $("#Organization_Tehsil").val(data[0].Organization_Tehsil);
            }
        });
    }

    function viewTransactionDetail(Code) {
        window.location.href = "viewTransactionDetails.php?Code=" + Code;
    }

    $("#image_address").on("click", function () {

        $("#download_document").attr("onclick", "downloadFile('" + image + "','" + usercode + "')");
        $("#view_document").modal('show');
    });

    $("#image_ownershipDocument").on("click", function () {

        $("#download_document_own").attr("onclick", "downloadFile('" + fld_ownershipDocument + "','" + usercode + "')");
        $("#view_document_ownershipDocument").modal('show');
    });

    $("#image_utilityBill").on("click", function () {

        $("#download_document_utility").attr("onclick", "downloadFile('" + fld_utilityBill + "','" + usercode + "')");
        $("#view_document_utilityBill").modal('show');
    });

    $("#image_rentAgreement").on("click", function () {

        $("#download_document_agree").attr("onclick", "downloadFile('" + fld_rentAgreement + "','" + usercode + "')");
        $("#view_document_rentAgreement").modal('show');
    });


    function downloadFile(img, usc) {
        window.location.href = 'download_document.php?id=' + img + '&usercode=' + usc + "&flag=address_proof";
    }

    $("#ref_id").html(decryptString('<?php echo $_REQUEST['Code'] ?>'));

    function fillForm() {

        $("#revoke_success").modal("show");

        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=FillUpdateAddressDetails&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>') + "",
            success: function (data) {
                data = $.parseJSON(data);

                setTimeout(function () {

                    image = data[0].fld_document_encoded;
                    usercode = data[0].Organization_User_Code_encoded;
                    Organization_User_Code = data[0].Organization_User_Code;
                    EstDate.value = data[0].fld_VisitDate;
                    if(data[0].fld_MarkByRKCL != null){
                        $("#mark").hide();
                        $("#Marked").show();
                    }
                    if (data[0].Organization_OwnershipType === "Own") {
                        fld_ownershipDocument = data[0].fld_ownershipDocument_encoded;
                        $("#onRent").css("display", "none");
                        $("#onOwn").css("display", "block");
                    }

                    if (data[0].Organization_OwnershipType === "Rent") {
                        fld_rentAgreement = data[0].fld_rentAgreement_encoded;
                        fld_utilityBill = data[0].fld_utilityBill_encoded;
                        $("#onRent").css("display", "block");
                        $("#onOwn").css("display", "none");
                        $("#Owner_Name").val(data[0].Owner_Name);
                        $("#owner").css("display", "block");
                    }

                    if (data[0].Organization_OwnershipType === "Relative") {
                        fld_rentAgreement = data[0].fld_rentAgreement_encoded;
                        fld_utilityBill = data[0].fld_utilityBill_encoded;
                        $("#onRent").css("display", "block");
                        $("#onOwn").css("display", "none");
                        $("#Owner_Name").val(data[0].Owner_Name);
                        $("#owner").css("display", "block");
                        $("#ViewRentAgreement").html('Ownership Document');
                        $("#ViewUtilityBill").html('NOC Certificate');
                    }

                    if (data[0].Organization_OwnershipType === "Special") {
                        fld_rentAgreement = data[0].fld_rentAgreement_encoded;
                        fld_utilityBill = data[0].fld_utilityBill_encoded;
                        $("#onRent").css("display", "block");
                        $("#onOwn").css("display", "none");
                        $("#Owner_Name").val(data[0].Owner_Name);
                        $("#owner").css("display", "block");
                        $("#ViewRentAgreement").html('Authority Letter');
                        $("#ViewUtilityBill").html('NOC Letter by AUTHORIZED Person');
                    }


                    /*** OLD DATA ***/

                    $("#Organization_Name_old").val(data[0].Organization_Name);

                    $("#Organization_Country_old").val(data[0].Organization_Country);
                    $("#Organization_State_old").val(data[0].Organization_State);
                    $("#Organization_Region_old").val(data[0].Organization_Region);
                    $("#Organization_District_old").val(data[0].Organization_District);
                    $("#Organization_Tehsil_old").val(data[0].Organization_Tehsil);

                    $("#Organization_AreaType_old").val(data[0].Organization_AreaType_old);
                    $("#Organization_OwnershipType_old").val(data[0].Organization_OwnershipType_old);

//                    $("#image_address").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document + "#zoom=95");
//                    $("#image_rentAgreement").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_rentAgreement + "#zoom=95");
//                    $("#image_utilityBill").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_utilityBill + "#zoom=95");
//                    $("#image_ownershipDocument").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_ownershipDocument + "#zoom=95");


$("#image_rentAgreement").hide();
$("#image_rentAgreement_ftp").html(data[0].fld_rentAgreement);

$("#image_address").hide();
$("#image_address_ftp").html(data[0].fld_document);

$("#image_ownershipDocument").hide();
$("#image_ownershipDocument_ftp").html(data[0].fld_ownershipDocument);

$("#image_utilityBill").hide();
$("#image_utilityBill_ftp").html(data[0].fld_utilityBill);



//                    $.ajax({
//                        url: "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document,
//                        type: 'HEAD',
//                        error: function ()
//                        {
//                            $("#image_address").attr("src", "upload/address_proof/" + data[0].fld_document + "#zoom=95");
//                        },
//                        success: function ()
//                        {
//                            $("#image_address").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document + "#zoom=95");
//                        }
//                    });
//
//                    $.ajax({
//                        url: "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_rentAgreement,
//                        type: 'HEAD',
//                        error: function ()
//                        {
//                            $("#image_rentAgreement").attr("src", "upload/address_proof/" + data[0].fld_rentAgreement + "#zoom=95");
//                        },
//                        success: function ()
//                        {
//                            $("#image_rentAgreement").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_rentAgreement + "#zoom=95");
//                        }
//                    });
//
//                    $.ajax({
//                        url: "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_utilityBill,
//                        type: 'HEAD',
//                        error: function ()
//                        {
//                            $("#image_utilityBill").attr("src", "upload/address_proof/" + data[0].fld_utilityBill + "#zoom=95");
//                        },
//                        success: function ()
//                        {
//                            $("#image_utilityBill").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_utilityBill + "#zoom=95");
//                        }
//                    });
//
//                    $.ajax({
//                        url: "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_ownershipDocument,
//                        type: 'HEAD',
//                        error: function ()
//                        {
//                            $("#image_ownershipDocument").attr("src", "upload/address_proof/" + data[0].fld_ownershipDocument + "#zoom=95");
//                        },
//                        success: function ()
//                        {
//                            $("#image_ownershipDocument").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_ownershipDocument + "#zoom=95");
//                        }
//                    });

//                    $("#view_image_large").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document);
//                    $("#view_image_large_rentAgreement").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_rentAgreement);
//                    $("#view_image_large_utilityBill").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_utilityBill);
//                    $("#view_image_large_ownershipDocument").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_ownershipDocument);




                    $("#uploadPreview1").click(function () {
                        //$("#viewimagesrc").attr('src', "upload/AddressApprovalPhoto/" + data[0].fld_FrontPhoto + "?nocache=" + Math.random());
                        $("#viewimagesrc").html(data[0].fld_FrontPhoto);

                    });
                    $("#uploadPreview2").click(function () {
                        //$("#viewimagesrc").attr('src', "upload/AddressApprovalPhoto/" + data[0].fld_LeftPhoto + "?nocache=" + Math.random());
                        $("#viewimagesrc").html(data[0].fld_LeftPhoto);

                    });
                    $("#uploadPreview3").click(function () {
                        //$("#viewimagesrc").attr('src', "upload/AddressApprovalPhoto/" + data[0].fld_RightPhoto + "?nocache=" + Math.random());
                        $("#viewimagesrc").html(data[0].fld_RightPhoto);

                    });
                    $("#uploadPreview4").click(function () {
                        //$("#viewimagesrc").attr('src', "upload/AddressApprovalPhoto/" + data[0].fld_LabSpace + "?nocache=" + Math.random());
                        $("#viewimagesrc").html(data[0].fld_LabSpace);

                    });

                    if (data[0].Organization_AreaType_old === "Urban") {
                        $("#urbanArea_old").css('display', 'block');
                        $("#ruralArea_old").css('display', 'none');
                        $("#Organization_Municipal_Type_old").val(data[0].Organization_Municipal_Type_old);
                        $("#Organization_Municipal_old").val(data[0].Organization_Municipal_old);
                        $("#Organization_WardNo_old").val(data[0].Organization_WardNo_old);
                        $("#Organization_Panchayat_old").val('');
                        $("#Organization_Gram_old").val('');
                        $("#Organization_Village_old").val('');
                    }

                    if (data[0].Organization_AreaType_old === "Rural") {
                        $("#urbanArea_old").css('display', 'none');
                        $("#ruralArea_old").css('display', 'block');
                        $("#Organization_Municipal_Type_old").val('');
                        $("#Organization_Municipal_old").val('');
                        $("#Organization_WardNo_old").val('');
                        $("#Organization_Panchayat_old").val(data[0].Organization_Panchayat_old);
                        $("#Organization_Gram_old").val(data[0].Organization_Gram_old);
                        $("#Organization_Village_old").val(data[0].Organization_Village_old);
                    }

                    $("#Organization_Address_old").val(data[0].Organization_Address_old);


                    /*** NEW DATA ***/

                    $("#Organization_OwnershipType").val(data[0].Organization_OwnershipType);

                    $("#Organization_ITGK_Code").html(data[0].Organization_ITGK_Code);
                    $("#fld_ITGK_Code").html(data[0].Organization_ITGK_Code);
                    $("#Organization_Name").val(data[0].Organization_Name);

                    $("#Organization_Type").val(data[0].Organization_Type);
                    $("#Organization_Type_old").val(data[0].Organization_Type_old);
                    $("#Organization_Country").val(data[0].Organization_Country);
                    $("#Organization_State").val(data[0].Organization_State);
                    $("#Organization_Region").val(data[0].Organization_Region);
                    $("#Organization_District").val(data[0].Organization_District);
                    $("#Organization_Tehsil").val(data[0].Organization_Tehsil);

                    $("#Organization_AreaType").val(data[0].Organization_AreaType);

                    if (data[0].Organization_AreaType === "Urban") {
                        $("#urbanArea").css('display', 'block');
                        $("#ruralArea").css('display', 'none');
                        $("#Organization_Municipal_Type").val(data[0].Organization_Municipal_Type);
                        $("#Organization_Municipal").val(data[0].Organization_Municipal);
                        $("#Organization_WardNo").val(data[0].Organization_WardNo);
                        $("#Organization_Panchayat").val('');
                        $("#Organization_Gram").val('');
                        $("#Organization_Village").val('');
                    }

                    if (data[0].Organization_AreaType === "Rural") {
                        $("#urbanArea").css('display', 'none');
                        $("#ruralArea").css('display', 'block');
                        $("#Organization_Municipal_Type").val('');
                        $("#Organization_Municipal").val('');
                        $("#Organization_WardNo").val('');
                        $("#Organization_Panchayat").val(data[0].Organization_Panchayat);
                        $("#Organization_Gram").val(data[0].Organization_Gram);
                        $("#Organization_Village").val(data[0].Organization_Village);
                    }
                    $("#Organization_Address").val(data[0].Organization_Address);

                    $("#submission_date").html(data[0].submission_date);
                    $("#submission_date_sp").html(data[0].submission_date_sp);
                    $("#submission_date_rkcl").html(data[0].submission_date_rkcl);

                    fillLocationDetails(Organization_User_Code);

                    $("#fld_remarks").val(data[0].fld_remarks);
                    $("#fld_remarks_rkcl").val(data[0].fld_remarks_rkcl);

                }, 3000);

                setTimeout(function () {
                    $("#revoke_success").modal("hide");
                }, 3000);

                $("#current_status").css("display", "block");

                /*** REQUEST NOT SUBMITTED ***/
                if (data[0].fld_status === "0") {
                    $("#current_status").css("display", "block");
                    $("#pending_submission").css("display", "block");
                }

                /*** REQUEST SUBMITTED, PENDING FOR APPROVAL BY SERVICE PROVIDER ***/
                if (data[0].fld_status === "1") {
                    //$("#updateActions").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#revoke_div").css("display", "block");
                }

                /*** APPROVED BY SERVICE PROVIDER, PENDING FOR APPROVAL BY RKCL ***/
                if (data[0].fld_status === "2") {
                    $("#remark_label").html('Entered Remarks by Service Provider');
                    //$("#updateActions").css("display", "block");
                    $("#fld_remarks").attr("readonly", "readonly");
                    //$("#rkcl_remarks").css("display", "block");
                    $("#paid").css("display", "block");
                    $("#sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#pending_for_approval_rkcl").css("display", "block");
                     $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>'),
                        success: function (data2) {
                            data2 = $.parseJSON(data2);
                            $("#payment_date").html(data2[0].fld_updatedOn);
                        }
                    });
                }

                /*** APPROVED BY RKCL, PENDING FOR PAYMENT ***/
                if (data[0].fld_status === "3") {
                    //$("#remark_label").html('Entered Remarks by Service Provider');
                    //$("#remark_label_rkcl").html('Entered Remarks by You');
                    //$("#fld_remarks").attr("readonly", "readonly");
                    //$("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks").css("display", "none");
                    $("#spvisitdiv").css("display", "none");
                    $("#updateActions").css("display", "none");
                    
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    //$("#pending_for_approval_rkcl").css("display", "block");
                    //$("#pending_for_payment").css("display", "block");
                }

                /*** REJECTED BY RKCL ***/
                if (data[0].fld_status === "4") {
                    $("#remark_label").html('Entered Remarks by Service Provider');
                    $("#remark_label_rkcl").html('Entered Remarks by You');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks").css("display", "block");
                    $("#sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#pending_for_approval_rkcl").css("display", "block");
                    $("#rkcl_rejected").css("display", "block");
                    $("#rejected_date_rkcl").html(data[0].fld_updatedOn);
                    $("#updateActions").css("display", "none");
                    $("#paid").css("display", "block");
                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>'),
                        success: function (data2) {
                            data2 = $.parseJSON(data2);
                            $("#payment_date").html(data2[0].fld_updatedOn);
                        }
                    });
                }

                /*** PAID BY ITGK ***/
                if (data[0].fld_status === '5') {
//                    $("#remark_label").html('Entered Remarks by Service Provider');
//                    $("#remark_label_rkcl").html('Entered Remarks by You');
//                    $("#fld_remarks").attr("readonly", "readonly");
//                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
//                    $("#rkcl_remarks").css("display", "block");
//                    $("#sp_remarks").css("display", "block");
$("#mark").css("display", "none");
 $("#rkcl_remarks").css("display", "none");
$("#spvisitdiv").css("display", "none");
                    $("#submit_pending").css("display", "block");
                    $("#updateActions").css("display", "none");

                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>'),
                        success: function (data2) {
                            data2 = $.parseJSON(data2);
                            $("#payment_date").html(data2[0].fld_updatedOn);
                        }
                    });

                    $("#pending_for_approval_sp").css("display", "block");
//                    $("#pending_for_approval_rkcl").css("display", "block");
//                    $("#pending_for_payment").css("display", "block");
                    $("#paid").css("display", "block");
                }

                /*** REJECTED BY ITGK ***/
                if (data[0].fld_status === "6") {
                    $("#remark_label").html('Entered Remarks by Service Provider');
                    $("#remark_label_rkcl").html('Entered Remarks by You');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks").css("display", "block");
                    $("#sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#itgk_rejected").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#rejected_date_itgk").html(data[0].fld_updatedOn);
                }

                /*** REJECTED BY ITGK ***/
                if (data[0].fld_status === "7") {
                    $("#remark_label").html('Entered Remarks by Service Provider');
                    //$("#remark_label_rkcl").html('Entered Remarks by You');
                    $("#fld_remarks").attr("readonly", "readonly");
                    //$("#fld_remarks_rkcl").attr("readonly", "readonly");
                    //$("#rkcl_remarks").css("display", "block");
                    $("#sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#sp_rejected").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#rejected_date_sp").html(data[0].fld_updatedOn);
                }
                
                /*** RM APPROVED ***/
                if (data[0].fld_status === "9") {
                    $("#remark_label").html('Entered Remarks by Service Provider');
                    $("#remark_label_rkcl").html('Entered Remarks by You');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks").css("display", "block");
                    $("#sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#updateActions").css("display", "none");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#pending_for_approval_rkcl").css("display", "block");
                    $("#pending_for_payment").css("display", "block");
                    $("#submission_date_rkcl").html(data[0].fld_updatedOn);
                    $("#paid").css("display", "block");
                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>'),
                        success: function (data2) {
                            data2 = $.parseJSON(data2);
                            $("#payment_date").html(data2[0].fld_updatedOn);
                        }
                    });
                }
            }
        });
    }

    fillForm();

    $("#mark").on("click", function () {


        $("#submitting").modal("show");
        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=mark&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>') + "&requestType=address",
            success: function (data) {
                if (data.search("Successfully Inserted") >= 0 || data.search("Successfully Updated") >= 0) {
                    setTimeout(function () {
                        $("#submitting").modal("hide");
                        $("#submitted_thanks").modal("show")
                    }, 3000);
                }
            }
        });

    });

    $("#commit").on("click", function () {
        var $validator = $("#updateOrgDetails").validate();
        var errors;
        var pattern = new RegExp("^[\\w.# ,-/]+$");
        var fld_remarks_rkcl = $("#fld_remarks_rkcl").val();

        if (!fld_remarks_rkcl) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
            $validator.showErrors(errors);
        } else if (!pattern.test(fld_remarks_rkcl)) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">The value seems to be invalid</span>"};
            $validator.showErrors(errors);
        } else {
            $("#submitting").modal("show");
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=commit_rkcl_address&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>') + "&fld_remarks_rkcl=" + fld_remarks_rkcl + "&fld_ref_id=" + decryptString('<?php echo $_REQUEST['Code'] ?>') + "&requestType=address",
                success: function (data) {
                    if (data.search("Successfully Inserted") >= 0 || data.search("Successfully Updated") >= 0) {
                        setTimeout(function () {
                            $("#submitting").modal("hide");
                            $("#submitted_thanks").modal("show")
                        }, 3000);
                    }
                }
            });
        }

    });

    $("#reject").on("click", function () {

        var $validator = $("#updateOrgDetails").validate();
        var errors;
        var pattern = new RegExp("^[\\w.# ,-/]+$");
        var fld_remarks_rkcl = $("#fld_remarks_rkcl").val();

        if (!fld_remarks_rkcl) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
            $validator.showErrors(errors);
        } else if (!pattern.test(fld_remarks_rkcl)) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">The value seems to be invalid</span>"};
            $validator.showErrors(errors);
        } else {
            $("#submitting").modal("show");
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=reject&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>') + "&fld_remarks_rkcl=" + fld_remarks_rkcl + "&requestType=address",
                success: function (data) {
                    if (data.search("Successfully Inserted") >= 0 || data.search("Successfully Updated") >= 0) {
                        setTimeout(function () {
                            $("#submitting").modal("hide");
                            $("#submitted_rejected").modal("show")
                        }, 3000);
                    }
                }
            });
        }


    });

    $("#revoke").on("click", function () {

        var $validator = $("#updateOrgDetails").validate();
        var errors;
        var pattern = new RegExp("^[\\w.# ,-/]+$");
        var fld_remarks_rkcl = $("#fld_remarks_rkcl").val();

        if (!fld_remarks_rkcl) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
            $validator.showErrors(errors);
        } else if (!pattern.test(fld_remarks_rkcl)) {
            errors = {fld_remarks_rkcl: "<span style=\"color:red; font-size: 12px;\">The value seems to be invalid</span>"};
            $validator.showErrors(errors);
        } else {
            $("#submitting").modal("show");
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=revoke&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>') + "",
                success: function (data) {
                    if (data.search("Sucscessfully Deleted") >= 0) {
                        setTimeout(function () {
                            $("#submitting").modal("hide");
                            $("#submitted_revoked").modal("show")
                        }, 3000);
                    }
                }
            });
        }
    });
</script>
<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
</html>