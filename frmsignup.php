<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Welcome To RKCL Sign Up</title>

    </head>

    <body>
        <div class="wrapper">

            <?php
            include './include/header.html';

            include './include/menu.php';
            ?>
            <div class="main">

                <form id="frmsignup" name="frmsignup" action="">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="field">
                        <tr>
                            <td style="width: 100%;height: 400px;" align="center" valign="middle" >
                                <table border="0" cellpadding="0" cellspacing="10" width="400px" class="frmtable" >
                                    <tr>
                                        <td colspan="3">
                                            <h1> SIgn Up for RKCL</h1>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="response" align="left" colspan="3">

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            Email Id
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <input type="text" name="txtEmailId" id="txtEmailId" class="text"></input>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Mobile Number
                                        </td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            <input type="text" name="txtMobile" id="txtMobile" class="text" maxlength="10"></input>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="center">
                                            <p class="btn submit"> <input type="button" id="btnSubmit" name="btnSubmit" value="Sign Up"></input></p>



                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                     
                    </table>
                </form>

            </div>
            <?php include './include/footer.html'; ?>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
            $("#btnSubmit").click(function() {
		           var url = "common/cfSignUp.php"; // the script where you handle the form input.
		           $.ajax({
		                  type: "POST",
		                  url: url,
		                  data:"action=ADD&pagename=signup&mobile="+txtMobile.value+"&emailid="+txtEmailId.value+"", // serializes the form's elements.
		                  
                                  success: function(data)
		                  {
//                                     if (data == Success)
//                                    { 
		                      $('#response').empty();
		                      $('#response').append("<span class='error'>"+data+"</span>"); // show response from the php script.
		                      window.setTimeout(function ()
                                        {
                                            SuccessfullySignUp();
                                        }, 3000);
//                                    }
//                                    else
//                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                        resetForm("frmsignup");
//                                    }
		                  }
		                });
                                return false; // avoid to execute the actual submit of the form.
		   });
                   
                   function SuccessfullySignUp()
            {
                
                window.location.href = 'frmsignupsuccess.php';
            }
            
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });
        </script>
    </body>

</html>