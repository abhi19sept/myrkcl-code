<?php
$title = "User Details";
include ('header.php');
echo '<body><div id="loader-wrapper">
          <div id="loader"></div>
      </div>';
include ('root_menu.php');
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<link rel="stylesheet" href="css/viewbox.css">
<script src="js/jquery.viewbox.min.js"></script>

<style type="text/css">

form > div.container {
   filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
}

    .modal-open .container-fluid, .modal-open .container{
        filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
    }

    #errorBox{
        color:#F00;
    }
    .aslink {
        cursor: pointer;
    }
    .checkday {
        display: none;
    }
    .viewbox-container {
        z-index: 1200 !important;
    }
</style>
<div style="min-height:450px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">eSakhi Registration</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="frmesakhienrollments" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div> 
                        <div class="container">
                            <div id="new-reg-btn" style="float: left; width: 50%;" class="text-left">
                               <!-- <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Register eSakhi" style="margin-left:-25px"/> --> &nbsp; 
							   <!-- <input type="button" name="btneSakhiTrainee" id="btneSakhiTrainee" class="btn btn-primary" value="Register eSakhi Trainee"/> --> &nbsp; 
                                <input type="button" name="btnUpdateTrainer" id="btnUpdateTrainer" class="btn btn-primary" value="Update Master Trainer SSOID"/> 
                            </div>
                            <div id="new-reg-btn" style="float: right; width: 30%;"  class="text-center">
                                <input type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload eSakhi Training Photos"/>
                            </div>
                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <div id="grid" name="grid" style="margin-top:35px;"> </div>
                    </form>
            </div>
        </div>   
    </div>

<div id="eSakhi_Registration_model" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 900;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">eSakhi Registration</h4>
            </div>
            <div class="modal-body">
                 <form name="frmidentity" id="frmidentity" class="form-inline" role="form" method="POST" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="model_response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-4 form-group">     
                            <label class="radio-inline"> <input type="radio" id="bhamashaId" class="bhamasha_type" name="bhamasha" checked="checked" value="bhamasha_id"/> Bhamasha ID </label><br />
                            <label class="radio-inline"> <input type="radio" id="bhamashaEnroll" class="bhamasha_type" name="bhamasha" value="bhamasha_app_num"/> Bhamasha Application ID </label>
                        </div>

                        <div class="col-sm-4 form-group">
                            <div id="bhamasha_id" class="bhamashatype">
                                <label for="learnercode">Bhamasha ID <span class="star">*</span>:</label>
                                <input type="text" class="form-control" name="Bhamasha_ID" id="Bhamasha_ID" maxlength="7" data-mask="chars" style="text-transform:uppercase" placeholder="Bhamasha ID">
                            </div>
                            <div id="bhamasha_app_num" class="bhamashatype" style="display: none;">
                                <label for="learnercode">Bhamasha Application Number <span class="star">*</span>:</label>
                                <input type="text" class="form-control" name="bhamashaappnum1" id="bhamashaappnum1" maxlength="4" data-mask="alpha" style="text-transform:uppercase;width: 59px;" placeholder="Application Number"> - 
                                <input type="text" class="form-control" name="bhamashaappnum2" id="bhamashaappnum2" maxlength="4" data-mask="alpha" style="text-transform:uppercase; width: 59px;" placeholder="Application Number"> - 
                                <input type="text" class="form-control" name="bhamashaappnum3" id="bhamashaappnum3" maxlength="5" data-mask="alpha" style="text-transform:uppercase; width: 65px;" placeholder="Application Number">
                            </div>
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">SSO ID <span class="star">*</span>:</label>
                            <input type="text" class="form-control" name="SSO_ID" id="SSO_ID" data-mask="ssoId" placeholder="SSO ID">
                        </div>
                    </div> 
                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Name<span class="star">*</span>:</label>
                            <input type="text" class="form-control" style="text-transform:uppercase" name="Name" id="Name" data-mask="charonly" placeholder="Name">
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Father / Husband Name<span class="star">*</span>:</label>
                            <input type="text" class="form-control" style="text-transform:uppercase" name="F_Name" id="F_Name" data-mask="charonly" placeholder="Father / Husband Name">
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Date of Birth <span class="star">*</span>:</label>
                            <input type="text" class="form-control" name="DOB" id="DOB" readonly="true" maxlength="50" placeholder="DD-MM-YYYY">
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Mobile<span class="star">*</span>:</label>
                            <input type="text" class="form-control"  name="esakhi_mobile" id="esakhi_mobile" data-mask="ten-number" placeholder="Mobile">
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Email ID:</label>
                            <input type="text" class="form-control" name="esakhi_email" id="esakhi_email" placeholder="Email Id">
                            <span id="emailerr" class="alert-error" style="font-size:12px;font-weight: bold;"></span>
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">App Application ID:</label>
                            <input type="text" class="form-control" name="app_application_id" data-mask="eight-number" id="app_application_id" placeholder="App Application Id">
                            <span id="apperr" class="alert-error" style="font-size:12px;font-weight: bold;"></span>
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" /><br /><span class="star">*</span><span class="small">(eSakhi mobile number, SSOID and App Application Id should be unique)</span>
                        </div>
                    </div> 
            </form>
            </div>
        </div>
    </div>
</div>
</div>

<div id="eSakhi_Attendance_model" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 900;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Training Completion / Attendance Declaration of  '<span id="esakhi_name"></span>'</h4>
            </div>
            <div class="modal-body">
                 <form name="frmattendance" id="frmattendance" class="form-inline" role="form" method="POST" enctype="multipart/form-data">
                    <div class="container">
                        <div><p>इस स्क्रीन पर आपको ई-सखी की प्रशिक्षण प्रारम्भ तिथि व चारों दिन की उपस्थिति दर्ज करनी है एवं यह घोषणा करनी है कि इस ई-सखी का <br> प्रशिक्षण पूर्ण हुआ है या नहीं | ई-सखी बुकलेट के पाठ्यक्रम एवं RKCL द्वारा दिए गए दिशा-निर्देशों के अनुसार यदि आपने प्रशिक्षण पूर्ण <br>करवा दिया है तो ही प्रशिक्षण को पूर्ण मानकर “Yes” सेलेक्ट करें |</p></div>
                    </div>
                    <div class="container">
                        <div id="attendance_response"></div>
                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode"><b>Training Start Date</b> <span class="star">*</span>:</label>
                            <input type="text" class="form-control" name="Training_Start_Date" id="Training_Start_Date" readonly="true" maxlength="50" placeholder="DD-MM-YYYY">
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label class="radio-inline"><b>Day 1</b></label> <span id="attend_day1"><input type="checkbox" id="day1" class="checkday" value="1" name="days[]" /></span> <span id="checked_day1" class="greenchk"></span>
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label class="radio-inline"><b>Day 2</b></label> <span id="attend_day2"><input type="checkbox" id="day2" class="checkday" value="2" name="days[]" /></span> <span id="checked_day2" class="greenchk"></span>
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label class="radio-inline"><b>Day 3</b></label> <span id="attend_day3"><input type="checkbox" id="day3" class="checkday" value="3" name="days[]" /></span> <span id="checked_day3" class="greenchk"></span>
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label class="radio-inline"><b>Day 4</b></label> <span id="attend_day4" > <input type="checkbox" id="day4" class="checkday" value="4" name="days[]" /></span> <span id="checked_day4" class="greenchk"></span>
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">
                            <label for="learnercode"><b>Training Complition Status</b> <span class="star">*</span>:</label>
                            <select id="trainingStatus" name="trainingStatus" class="form-control">
                                <option value="">-- Select --</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="col-sm-4 form-group">
                            <label for="learnercode"><b>Remark</b>:</label>
                            <input type="text" class="form-control" style="text-transform:uppercase" name="attendance_remark" id="attendance_remark" data-mask="charonly" placeholder="Remark">
                        </div>
                    </div>
                        
                    <div class="container">
                        <div class="col-sm-4 form-group">
                            <input type="button" name="submitAttendance" id="submitAttendance" class="btn btn-primary" value="Submit" />
                            <input type="hidden" id="att_id" name="att_id" value="">
                        </div>
                    </div> 
            </form>
            </div>
        </div>
    </div>
</div>
</div>

<div id="eSakhi_Picture_model" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 1040;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">eSakhi Training Photos</h4>
            </div>
            <div class="modal-body">
                 <form name="frmpictures" id="frmpictures" action="common/cfIdentityInfo.php" method="post" class="form-inline" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="photo_response"></div>
                        </div>  
                        <div class="col-sm-4 form-group text-left"> 
                            <b>Select Photo: <span class="star">*</span></b> 
                        </div>      
                        <div class="col-sm-4 form-group text-left"> 
                            <input type="file" id="txtUploadScanDoc" onchange="checkScanForm(this);" name="file" />
                            <span  id="psize" style="font-size:10px;">Note: JPG,JPEG,PNG Allowed Size = 50 to 100 KB</span>
                        </div>
                        <div class="col-sm-4 form-group">
                            <input type="button" name="uploadphoto" id="uploadphoto" class="btn btn-primary" value="Upload" />
                            <input type="hidden" name="action" value="uploadPics" />
                        </div>
                    </div> 
                    <div class="container">
                    	<div id="photo_grid" name="photo_grid" style="margin-top:35px;border:1px solid #999; width: 83%; padding: 10px;"><center class="small">No photo found.</center></div>
                	</div>
            </form>
            </div>
        </div>
    </div>
</div>
</div>


<div id="eSakhi_Trainer_model" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 1150;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Update eSakhi Trainer Details</h4>
            </div>
            <div class="modal-body">
                 <form name="frmTrainerInfo" id="frmTrainerInfo" class="form-inline" role="form" method="POST" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="trainer_response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div> 
                    <div class="container" id="trainer_list">
                    </div> 
            </form>
            </div>
        </div>
    </div>
</div>
</div>

<div id="eSakhi_Trainees_Registration_model" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 1050;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Trainees of eSakhi Registration</h4>
            </div>
            <div class="modal-body">
                 <form name="frmesakhitrainees" id="frmesakhitrainees" class="form-inline" role="form" method="POST" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="trainee_response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-4 form-group">     
                            <label for="designation">Select eSakhi:<span class="star">*</span></label>
                            <select id="ddleSakhi" name="ddleSakhi" class="form-control"></select>
                        </div>
                        <div class="col-sm-4 form-group">
                            <label for="learnercode">Trainee SSO ID <span class="star">*</span>:</label>
                            <input type="text" class="form-control" name="TraineeSSO_ID" id="TraineeSSO_ID" data-mask="ssoId" placeholder="SSO ID">
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Trainee Name<span class="star">*</span>:</label>
                            <input type="text" class="form-control" style="text-transform:uppercase" name="TraineeName" id="TraineeName" data-mask="charonly" placeholder="Name">
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Trainee Mobile:</label>
                            <input type="text" class="form-control" name="TraineeMobile" id="TraineeMobile" data-mask="ten-number" placeholder="Mobile No.">
                        </div>
                    </div> 
                    <div class="container">
                        <div class="col-sm-4 form-group"></div>
                        <div class="col-sm-4 form-group text-right">
                            <input type="submit" name="btnTraineeSubmit" id="btnTraineeSubmit" class="btn btn-primary" value="Submit" />
                        </div>
                    </div> 
                    <div class="container" id="trainee_list">
                    </div>
            </form>
            </div>
        </div>
    </div>
</div>
</div>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); 
    include 'common/modals.php';
    $mac = explode(" ", exec('getmac'));
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#DOB').datepicker({
            format: "dd-mm-yyyy",
            orientation: "bottom auto",
            todayHighlight: true,
            autoclose: true
        });

        $('#Training_Start_Date').datepicker({
            format: "dd-mm-yyyy",
            orientation: "bottom auto",
            todayHighlight: true,
            autoclose: true
        });

        $("#eSakhi_Registration_model").on('click', '.close', function () {
            getData();
        });

        $("#eSakhi_Trainees_Registration_model").on('change', '#ddleSakhi', function () {
            $("#trainee_list").empty();
            getTraineeData(this.value);
        });

        $("#grid").on('click', '.aslink', function () {
            performAction(this);
        });

        $("#eSakhi_Trainees_Registration_model").on('click', '.aslink', function () {
            performAction(this);
        });

        $("#trainer_list").on('click', '#submitTrainer', function () {
            saveTrainerDetails();
        });

        $("#grid").on('click', '#submiteSakhi', function () {
            saveeSakhiDetails();
        });

        $("#photo_grid").on('click', '#links', function () {
            var vb = $('.image-link').viewbox();
            vb.trigger('viewbox.next');
            vb.trigger('viewbox.prev');
        });

        $('.bhamasha_type').click(function () {
            var btype = $(this).val();
            $('.bhamashatype').hide();
            $('.bhamashatype :input').val('');
            $('#' + btype).show();
        });

        $("#btnShow").click(function () {
            $("#resetBtn").click();
            $("#eSakhi_Registration_model").modal("show");
        });

        $("#btneSakhiTrainee").click(function () {
            $("#eSakhi_Trainees_Registration_model").modal("show");
        });

        $("#btnUpload").click(function () {
        	displayList();
            $("#eSakhi_Picture_model").modal("show");
        });

        $("#btnUpdateTrainer").click(function () {
            $("#eSakhi_Trainer_model").modal("show");
        });

        FilleSakhiDdl();
        getData();
        getTrainers();

        $("#uploadphoto").click(function () {
        	var photo = $('#txtUploadScanDoc').val();
        	if (photo == '') {
        		$('#photo_response').html("<p class='error'><span><img src=images/drop.png width=10px /></span><span class='star small'>Please select any photo for upload.</span></p>");
        	} else {
                uploadFiles();        		
        	}
        });

        $("#submitAttendance").click(function () {
            var ifvalid = $('#frmattendance').valid();
            if (!ifvalid) return;
            $('#attendance_response').html("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var chkbox = 0;
            $('.checkday').each(function() {
                if ($(this).is(':checked')) {
                    chkbox = 1;
                }
            });
            if (chkbox) {
            	var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
	            var forminput = $("#frmattendance").serialize();
	            //alert(forminput);
	            var data = "action=AddAttendance&" + forminput;

	            $.ajax({
	                type: "POST",
	                url: url,
	                data: data,
	                success: function (data)
	                {
	                    $('#attendance_response').html("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
	                    getAttendance($("#att_id").val());
	                }
	            });
            } else {
            	$('#attendance_response').html("<p class='error'><span><img src=images/drop.png width=10px /></span><span class='star small'>Please select the day of attendance.</span></p>");
            }
        });

        $("#btnTraineeSubmit").click(function () {
            var ifvalid = $('#frmesakhitrainees').valid();
            if (!ifvalid) return;
            $('#trainee_response').empty();
            $('#trainee_response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            
            var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmesakhitrainees").serialize();
            //alert(forminput);
            data = "action=ADDTrainee&" + forminput;

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    // alert(data);
                    $('#trainee_response').empty();
                    $('#trainee_response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                    $('#TraineeSSO_ID').val('');
                    $('#TraineeName').val('');
                    $('#TraineeMobile').val('');
                    getTraineeData(ddleSakhi.value);
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });

        $("#btnSubmit").click(function () {
            var ifvalid = $('#frmidentity').valid();
            if (!ifvalid) return;
            var validAppId = validateAppId(app_application_id.value);
            if (!validAppId) return;

            $('#model_response').empty();
            $('#model_response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            
            var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmidentity").serialize();
            //alert(forminput);
            data = "action=ADD&" + forminput;

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    // alert(data);
                    $('#model_response').empty();
                    $('#model_response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
			        $('#Bhamasha_ID').val('');
			        $('#SSO_ID').val('');
			        $('#Name').val('');
			        $('#F_Name').val('');
			        $('#DOB').val('');
                    $('#app_application_id').val('');
                    //getData();
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });

        $("#esakhi_email").blur(function () {
            validateEmail(this.value);
        });

        $("#app_application_id").blur(function () {
            validateAppId(this.value);
        });

    });

	function validateEmail(email) {
        if (email=='') return true;
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var isvalid = regex.test(email);
        $('#emailerr').html('');
        if (!isvalid) {
            $('#emailerr').html('Please enter valid email id');
        }
        return isvalid;
    }

    function validateAppId(appid) {
        var isvalid = true;
        var app_id = parseInt(appid);
        var min_id = parseInt(1);
        $('#apperr').html('');
        if (app_id < min_id) {
           $('#apperr').html('<span class="small star">Please enter valid app application id</span>');
            isvalid = false;
        }

        return isvalid;
    }

    function getTraineeData(eSakhi_id) {
        $('#trainee_list').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
        var data = "action=TraineeInfo&esakhi=" + eSakhi_id;
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
                $("#trainee_list").html(data);
                $('#example3').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ],
                    scrollY: 400,
                    scrollCollapse: true,
                    paging: false
                });
            }
        });
    }

    function FilleSakhiDdl() {
        //alert();
        $.ajax({
            type: "post",
            url: "common/cfIdentityInfo.php",
            data: "action=FilleSakhi",
            success: function (data) {
                $("#ddleSakhi").html(data);
            }
        });
    }

    function getAttendance(att_id) {
    	$('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
    	var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
        var data = "action=getAttendance&att_id=" + att_id;

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
            	data = data.split('|');
            	var atten = data[3].split(',');
            	$('#attendance_remark').val(data[2]);
                $('#Training_Start_Date').val(data[1]);
                $('#trainingStatus').val(data[0]);
            	$('.checkday').show();
                $('.greenchk').empty();
            	for (var i = 0; i < 4 ; i++) {
            		if (atten[i] == 1 || atten[i] == 2 || atten[i] == 3 || atten[i] == 4) {
            			$('#day' + atten[i]).hide();
            			$('#checked_day' + atten[i]).html('<img src=images/correct.gif width=20px />');
            			$('#day' + atten[i]).attr('checked','checked');
            		}
            	};
                $('#response').empty();
                //getAttendance($("#att_id").val());
            }
        });
    }

    function saveeSakhiDetails() {

        $('#response').html("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
        var forminput = $("#frmesakhienrollments").serialize();
        //alert(forminput);
        var data = "action=updateeSakhiInfo&" + forminput;

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
                $('#response').html("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                getData();
            }
        });
    }

    function saveTrainerDetails() {
        $('#trainer_response').html("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
        var forminput = $("#frmTrainerInfo").serialize();
        //alert(forminput);
        var data = "action=updateTrainerInfo&" + forminput;

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
                $('#trainer_response').html("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                getTrainers();
            }
        });
    }

    function performAction(eObj) {
        var act = eObj.id.split('-');
        if (act[0] == 'Delete') {
            removeRecord(eObj);
        } else if (act[0] == 'addattendance') {
            $('#esakhi_name').html(act[2]);
            $('#att_id').val(act[1]);
            $('#attendance_response').empty();
            getAttendance(act[1]);
            addattendance(eObj);
        } else if (act[0] == 'DeleteTrainee') {
            removeTraineeRecord(eObj);
        }
    }

    function addattendance(eObj) {
        $("#eSakhi_Attendance_model").modal("show");
    }

    function removeRecord(eObj) {
        var ok = confirm("Are you sure to remove this record ?");
        if (! ok) return;
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfIdentityInfo.php",
            data: "action=PerformAction&elemId=" + eObj.id,
            success: function (data) {
                $('#response').html("<p class='error'><span><img src=images/correct.gif width=10px /></span><span class=small>Record has been removed successfully.</span></p>");
                getData();
            }
        });
    }

    function removeTraineeRecord(eObj) {
        var ok = confirm("Are you sure to remove this record of trainee ?");
        if (! ok) return;
        $('#trainee_response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfIdentityInfo.php",
            data: "action=PerformAction&elemId=" + eObj.id,
            success: function (data) {
                $('#trainee_response').html("<p class='error'><span><img src=images/correct.gif width=10px /></span><span class=small>Trainee record has been removed successfully.</span></p>");
                getTraineeData(ddleSakhi.value);
            }
        });
    }

    function getData() {
        var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
        var data = "action=Info";
        $('#model_response').empty();
        $('#Bhamasha_ID').val('');
        $('#SSO_ID').val('');
        $('#Name').val('');
        $('#F_Name').val('');
        $('#DOB').val('');
        $('#app_application_id').val('');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
                $('#response').empty();
                $("#grid").html(data);
                /*
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ],
                    scrollY: 400,
                    scrollCollapse: true,
                    paging: false
                }); 
                */
            }
        });
    }

    function getTrainers() {
        var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
        var data = "action=trainersList";
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
                $('#trainer_response').empty();
                $("#trainer_list").html(data);
            }
        });
    }

</script>
<script type="text/javascript" src="scripts/jquery.form.js"></script>
<script language="javascript" type="text/javascript">

    function uploadFiles() {
        var target = document.getElementById('txtUploadScanDoc');
        $('#frmpictures').ajaxForm({
            //display the uploaded images
            target:'#photo_response',
            beforeSubmit:function(e){
                $('#photo_response').html('Uploading images...');
            },
            success:function(e){
                $('#photo_response').empty();
                target.value = '';
                displayList();
            },
            error:function(e){
            }
        }).submit();
    }

    function displayList() {
        var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
        var data = "action=eSakhiPhotos&elemId=";
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
                $('#photo_response').empty();
                $("#photo_grid").html(data);
            }
        });
    }

    function checkScanForm(target) {
        var ext = $('#txtUploadScanDoc').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }

        if (target.files[0].size > 100000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 100 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }
        else if (target.files[0].size < 50000)
        {
            alert("Image size should be greater than 50 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }
        document.getElementById("txtUploadScanDoc").innerHTML = "";
        return true;
    }

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmidentity_validation.js" type="text/javascript"></script>
</html>