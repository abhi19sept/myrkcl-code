<?php
$title="Software Download";
include ('header.php'); 
include ('root_menu.php');

?>
<div style="min-height:430px !important;max-height:1500px !important;">
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Software Download</div>
                <div class="panel-body">
                     <div class='col-md-11 col-lg-11'>
						<table class='table table-user-information table table-hover'>
						<tbody>
                                                 <tr>
						<th class="col-md-8 col-lg-8">Software Name:</th>
                                                <th class="col-md-2 col-lg-2" id='ifsccode'>Download</th>
						</tr>
						<tr>
						<td class="col-md-10 col-lg-10">Biometric Driver Software (Client Service) Download (Released on 07August 2018) for Tatvik - TMF20</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Biometric Driver Software (Client Service) Download (Released on 07August 2018) for Tatvik - TMF20" href="download/TatvikTMF20Service-v1.0.2.zip" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
						</tr>
						
						<tr>
						<td class="col-md-10 col-lg-10">Biometric Driver Software (Client Service) Download (Released on May22 2018) for Mantra - MFS100</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Biometric Driver Software Download(Released on June 6 2017) for Mantra - MFS100" href="download/MFS100ClientService.rar" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
						</tr>
						
						<!--<tr>
						<td class="col-md-10 col-lg-10">Biometric Driver Software (Windows 10) Download (Released on March 7 2018) for Mantra - MFS100</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Biometric Driver Software Download(Released on June 6 2017) for Startek - FM220" href="download/MFS100DriverWin10.rar" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
						</tr>
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Biometric Driver Software (For Other Windows Versions) Download (Released on March 7 2018) for Mantra - MFS100</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Biometric Driver Software Download(Released on June 6 2017) for Startek - FM220" href="download/MFS100Driver.rar" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
						</tr>-->
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Biometric Driver Software Download(Released on June 6 2017) for Startek - FM220</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Biometric Driver Software Download(Released on June 6 2017) for Startek - FM220" href="download/Startek_Setup.zip" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
						</tr>
						
						<tr>
						<td class="col-md-10 col-lg-10">Biometric Driver Software Download(Released on June 6 2017) for Cogent - CSD200</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Biometric Driver Software Download(Released on June 6 2017) for Cogent - CSD200" href="download/Cogent_CSD_200_Driver.zip" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
						</tr>
						
						
						                             <tr>
						<td class="col-md-10 col-lg-10">RKCL Office 2010 Activator:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/office10.rar" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
						</tr>
						
                                                <tr>
						<td class="col-md-10 col-lg-10">RKCL Windows Server ® 2008 Activator:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/RKCLWindowsServer2008activator.rar" target="_blank"><img class="thumbnail img-responsive" src="images/download.png"  width="30"></a></td>
						</tr>
						<tr>
						<td class="col-md-10 col-lg-10">RKCL Windows 10 Activator:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/RKCLWindows10EnterpriseActivator(beta).rar" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						<tr>
						<td class="col-md-10 col-lg-10">RS-CIT Updated Assignment Package for LMS (Hindi)</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="http://rkcl.in/download/Revised%20RSCIT%20Assessment%20Hindi.zip" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						<tr>
						<td class="col-md-10 col-lg-10">RS-CIT Updated Assignment Package for LMS (English)</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="http://rkcl.in/download/Revised%20RSCIT%20Assessment%20English.zip" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						<tr>
						<td class="col-md-10 col-lg-10">Practice Skill Test</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Practice Skill Test"
												href="download/Practice Skill Test V2.zip" target="_blank">
												<img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						
                                                <tr>
						<td class="col-md-10 col-lg-10"></td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'></td>
						</tr><!--
						<tr>
						<td class="col-md-10 col-lg-10">EOI Application Report:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/EOIApplicationReport.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						<tr>
						<td class="col-md-10 col-lg-10">EOI Payment Transaction Report:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/EOIPaymentTransactionReport.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						<tr>
						<td class="col-md-10 col-lg-10">Learner Admission Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LearnerAdmissionGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						<tr>
						<td class="col-md-10 col-lg-10">Learner Admission Modify Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LearnerAdmissionModifyGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						<tr>
						<td class="col-md-10 col-lg-10">Learner Admission Payment Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LearnerAdmissionPaymentGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						
						<tr>
						<td class="col-md-10 col-lg-10">Learner Verification Report Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/LearnerVerificationReportGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						
						
						<tr>
						<td class="col-md-10 col-lg-10">Biomatric Software Installation Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/BiomatricSoftwareInstallationguide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						
						
						<tr>
						<td class="col-md-10 col-lg-10">Biomatric Registration Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/BiomatricRegistrationGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						
						
						<tr>
						<td class="col-md-10 col-lg-10">Biomatric Learner Enrollment Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/BiomatricLearnerEnrollmentGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
						
                                                <tr>
						<td class="col-md-10 col-lg-10">Biomatric Learner Attendance Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/BiomatricLearnerAttendanceGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Biometric Attendance Report Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/BometricAttendanceReportGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Adding Bank Details Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/AddingBankDetailsGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Correction Application Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CorrectionApplicationGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Correction Application Payment Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CorrectionApplicationPaymentGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Correction Application Report:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CorrectionApplicationReport.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Correction Payment Learner Wise Report:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CorrectionPaymentLearnerWiseReport.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Correction Transaction Summary:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/CorrectionTransactionSummary.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Govt Emp Application Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/GovtEmpApplicationGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
                                                
                                                <tr>
						<td class="col-md-10 col-lg-10">Govt Emp Report Guide:</td>
                                                <td class="col-md-1 col-lg-1" id='ifsccode'><a title="Image 1" href="download/GovtEmpReportGuide.pdf" target="_blank"><img class="thumbnail img-responsive" src="images/dwnpdf.png"  width="30"></a></td>
						</tr>
                                                
                                                
						

						 -->
						</tbody>
					   </table>
					  
						</div>
  
  <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
		<button class="close" type="button" data-dismiss="modal">×</button>
		<h3 class="modal-title">Heading</h3>
	</div>
	<div class="modal-body">
		
	</div>
	<div class="modal-footer">
		<button class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
   </div>
  </div>
</div>
						
            </div>   
        </div>
    </div>
</div>
  </body>
  <style>
  .modal-dialog {width:600px;}
.thumbnail {margin-bottom:6px;}
  </style>
  <script>
  $(document).ready(function() {
$('.thumbnail').click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
                        
       
    </body>
    
</html>