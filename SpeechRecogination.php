<?php 
$title = "Speech Recognition";
include ('header.php');
include ('root_menu.php');
if($_SESSION['User_UserRoll']=='8' || $_SESSION['User_UserRoll']=='1' || $_SESSION['User_UserRoll']=='28') {
	
}else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
$select = "";
if(isset($_REQUEST["lag"])){
	$lag = $_REQUEST["lag"];
	$name = "";
	if($lag == "en-IN"){
		$name = "English";
		$select = "<option value=''>--Select Language--</option><option selected='selected' value='".$lag."'>".$name."</option><option value='hi-IN'>Hindi</option>";
	}
	else if($lag == "hi-IN"){
		$name = "Hindi";
		$select = "<option value=''>--Select Language--</option><option selected='selected' value='".$lag."'>".$name."</option><option value='en-IN'>English</option>";
	}
	
}
else{
	$lag = "en-IN";
}
echo "<script>var lag = '".$lag."'</script>";
$player="";
if(isset($_POST["btnsubmit"])){
	$txt = $_POST["txtval"];
	$txt = htmlspecialchars($txt);
	$txt = rawurlencode($txt);
	$html = file_get_contents('https://translate.google.com/translate_tts?ie=UTF-8&client=gt&q='.$txt.'&tl=en-IN');
	$player = "<audio controls='controls' autoplay><source src='data:audio/mpeg;base64,".base64_encode($html)."'></source></audio>";
}

?>
<div style="    text-align: center;margin: 50px 0 50px 0;">
<select id="bhasaselect" name="bhasaselect" onchange="myFunction()" style="height: 30px;" >
<?php if(isset($_REQUEST["lag"])){ echo $select ; } else{?>
	<option value="">--Select Language--</option>
	<option value="en-IN">English</option>
	<option value="hi-IN">Hindi</option>
<?php } ?>
</select>
<input  id="action" type="button" onclick="toggle()" value="Start" class="btn btn-primary" style="margin: 0 0 5px 0;" /><br>
<textarea type="text" name="q" id="transcript" placeholder="Speak" class="keyboardInput" rows="8" cols="125"></textarea>
<form method="POST">
<h6>Convert Text To Audio<h6>
<textarea id='txtval' name="txtval" rows="8" cols="100"></textarea>
<input type="submit" id="btnsubmit" name="btnsubmit" class="btn btn-primary" value="Play" />
<div><?php echo $player; ?></div>
</form>
</div>
<script>
function myFunction() {
  var x = document.getElementById("bhasaselect").value;
  if(x==""){
	  alert("Please Select Language");
  }else{
	 window.location.href = "SpeechRecogination.php?lag="+x; 
  }
  
}
function toggle() {
	 var y = document.getElementById("bhasaselect").value;
	  if(y==""){
		  
		  alert("Please Select Language");
		  return false;
	  }
  if(listening) {
    recognition.stop();  
    listening = false;
  document.getElementById('action').value = "Start";
  }
  else {
    recognition.start();
    listening = true;
  document.getElementById('action').value = "Stop";
  }
}

var recognition = new webkitSpeechRecognition();
recognition.continuous = true;
recognition.interimResults = true;
recognition.lang = lag;

var listening = false;
	
recognition.onresult = function(event) {
	
    var interim_transcript = '';
    var final_transcript = '';

    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;
         document.getElementById('transcript').value = final_transcript;
        toggle();
      } else {
        interim_transcript += event.results[i][0].transcript;
            document.getElementById('transcript').value = interim_transcript;
      }
    }
    console.log(interim_transcript, final_transcript);

  };
</script>
<?php
include ('footer.php');
?>