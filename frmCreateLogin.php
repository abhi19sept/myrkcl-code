<?php ob_start();
$title = "Create Login";
include ('header.php');
include ('root_menu.php');


if($_SESSION['User_UserRoll']!=1 && $_SESSION['User_UserRoll']!=8){
    session_destroy();
    header('Location: index.php');
    exit;
}
  
?>

<style>
    .marginB15{ margin-bottom: 15px;}
    .w100{width: 100%;}
    .userdet{background-color: #ED66B2; padding: 10px 10px 5px 10px; margin-bottom: 10px; color: #fff;}
    .panel-heading-new {
    background-color: #86469D;
    border-color: #019de2;
    color: #fff;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    padding: 10px 15px;
}

.btn-primary {
    background-color: #86469C;
    border-color: #357ebd;
    color: #ffffff;
}
.division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 18px;
        color: #575c5f;
        margin-bottom: 20px;
        padding-left: 10px;
        padding-top: 15px;
        width: 96%; margin-left: 20px;

    }

</style>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>


<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading-new ">Create Login</div>
            <div class="panel-body">
                <form name="formCreateLogin" id="formCreateLogin" class="form-inline" role="form" enctype="multipart/form-data">
                   
                        <div class="container">
                            <div id="response"></div>
                            <div id="errorBox"></div>
                        </div>        
                        <div class="container w100">
                            <div class="form-group1 marginB15 userdet"> 
                                <label for="edistrict">User Detail:</label>
                        
                            </div>
                        </div>
                    
                        <div class="container w100">
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> Select Entity Type:<span class="star">*</span></label>
                                    <select id="selEntity" name="selEntity" class="form-control">

                                    </select>
                            </div>
                            <div class="col-sm-3 form-group1 marginB15"> 
                                <label for="edistrict">Select User Roll:<span class="star">*</span></label>
                                <select id="selUserRoll" name="selUserRoll" class="form-control  text-uppercase" >
                                    
                                </select>    
                            </div>
                            <div id="userlo" style=" display: none">
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> ITGK District:</label>
                                    <select   id="selDistrict1" name="selDistrict1" class="form-control">

                                    </select>
                            </div>
                            
                             <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> Select RSP :</label>
                                    <select id="selRSP" name="selRSP" class="form-control">

                                    </select>
                            </div>
                             <input type="hidden" class="form-control text-uppercase"  name="txtRSP" id="txtRSP"  placeholder="RSP">
                          
                            </div>
                            
                             <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> User Name:<span class="star">*</span></label>
                                    <input type="text" class="form-control text-uppercase" name="txtUsername" id="txtUsername"  placeholder="User Name">
                           </div>
                            
                            <div class="col-sm-3 form-group1 marginB15">  
                                 <label for="batch"> Password:<span class="star">*</span></label>
                                     <input type="text" class="form-control text-uppercase"  name="txtPassword" id="txtPassword"  placeholder="Password">
                            </div>
                         
                       
                            
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> RePassword:<span class="star">*</span></label>
                                     <input type="text" class="form-control text-uppercase" name="txtRePassword" id="txtRePassword"  placeholder="RePassword">
                            </div>

                            
                           <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> Email:<span class="star">*</span></label>
<!--                                     <input type="text" class="form-control text-uppercase"  name="txtEmail" id="txtEmail"  placeholder="Email">-->
                                     <input type="text" placeholder="Email Id" class="form-control" id="txtEmail" name="txtEmail" maxlength="100" />
                                        <span id="emailerr" class="alert-error" style="font-size:12px;font-weight: bold;"></span>
                            </div>
                           

                       
                            
                             <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> Mobile Number:<span class="star">*</span></label>
                                     <input type="text" class="form-control text-uppercase" onkeypress="javascript:return allownumbers(event);"  maxlength="10" name="txtMobile" id="txtMobile"  placeholder="Mobile Number">
                            </div>
                          
                        </div>
                    
                    
                    
                    <div class="container w100" style="margin-top: 20px;">
                            <div class="form-group1 marginB15 userdet"> 
                                <label for="edistrict">Organization  Detail:</label>
                        
                            </div>
                        </div>
                    <div class="division_heading">
                    Center Details
                </div>
                     <div class="container w100">
                            <div class="col-sm-3 form-group1 marginB15"> 
                                <label for="edistrict">Organization Name:<span class="star">*</span></label>
                                 <input type="text" class="form-control text-uppercase" maxlength="50" name="Organization_Name" id="Organization_Name" onkeypress="javascript:return allowchar(event);"  placeholder="Organization Name">   
                                 <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                            </div>
                            
                           <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> Registration Number:<span class="star">*</span></label>
                                     <input type="text" class="form-control text-uppercase" maxlength="50" name="Organization_RegistrationNo"  onkeypress="javascript:return validAddress(event);" id="Organization_RegistrationNo"  placeholder="Registration Number">
                            </div>
                            
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> Founded Date:<span class="star">*</span></label>
                                 <input type="text" class="form-control" maxlength="50" name="txtEstdate" id="txtEstdate"  placeholder="DD-MM-YY" readonly='true'>
                                     <!--<input type="text" class="form-control text-uppercase" maxlength="15" name="Organization_FoundedDate" id="Organization_FoundedDate"  placeholder=" Founded Date">-->
                            </div>
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> Course of Organization:</label>
                                  <input type="text" class="form-control text-uppercase" maxlength="15"  onkeypress="javascript:return validAddress(event);" name="Organization_Course" id="Organization_Course"  placeholder=" Course of Organization">  
                            </div>
                         
                        </div>
                        
                    
                    
                    
                     <div class="container w100">
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="edistrict">Type of Organization:<span class="star">*</span></label>
                                <select id="selTypeOrg" name="selTypeOrg" class="form-control" >

                                </select>   
                            </div>
                            
                            
<!--                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> House Number:</label>
                                    
                                    <input type="text"  onkeypress="javascript:return validAddress(event);" class="form-control text-uppercase" maxlength="15" name="Organization_HouseNo" id="Organization_HouseNo"  placeholder="House Number">
                            </div>
                         <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch">Street:</label>
                                     <input type="text"  onkeypress="javascript:return validAddress(event);" class="form-control text-uppercase" maxlength="15" name="Organization_Street" id="Organization_Street"  placeholder=" Street">
                            </div>
                         
                          <div class="col-sm-3 form-group1 marginB15"> 
                                     <label for="batch"> Road:</label>
                                     <input type="text"  onkeypress="javascript:return validAddress(event);" class="form-control text-uppercase" maxlength="15" name="Organization_Road" id="Organization_Road"  placeholder="Road">
                            </div>-->
                           
                         
                        </div>
                        
                    
                     <div class="division_heading">
                    Location Details
                </div>
                    
                    
                     <div class="container w100">
                         <div class="col-sm-3 form-group1 marginB15"> 
                                <label for="edistrict">Country:<span class="star">*</span></label>
                                <select id="selCountry" name="selCountry" class="form-control">

                                    </select>
                            </div>
                         
                         <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> State:<span class="star">*</span></label>
                                    <select id="selState" name="selState" class="form-control">

                                    </select>
                            </div> 
                             <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> Division:<span class="star">*</span></label>
                                    <select id="selRegion" name="selRegion" class="form-control">

                                    </select>
                            </div>
                            
                          
                            
                           
                          <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> District:<span class="star">*</span></label>
                                    <select id="selDistrict" name="selDistrict" class="form-control">

                                    </select>
                            </div>
                           
                         
                        </div>
                        
                    
                    
                    
                     
                        
                   
                    
                     
                     <div class="container w100">
                            
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> Tehsil:<span class="star">*</span></label>
                                    <select id="selTehsil" name="selTehsil" class="form-control">

                                    </select>
                            </div> 
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch">Nearest Police Station:<span class="star">*</span></label>
                                     <input type="text"  onkeypress="javascript:return validAddress(event);" class="form-control text-uppercase" maxlength="15" name="Organization_Police" id="Organization_Police"  placeholder="Police">
                            </div>
                            <div class="col-sm-3 form-group1 marginB15"> 
                                     <label for="batch">Latitute:</label>
                                     <input type="text" class="form-control text-uppercase" maxlength="15" name="Organization_lat" id="Organization_lat"  placeholder="latitute">
                            </div>
                         <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch">Longitute:</label>
                                    <input type="text" class="form-control text-uppercase" maxlength="15" name="Organization_long" id="Organization_long"  placeholder="longitute">
                            </div>
<!--                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> Area Type:<span class="star">*</span></label>
                                 <select id="selAreaType" name="selAreaType" class="form-control">

                                    </select>
                            </div>
                          <div class="col-sm-3 form-group1 marginB15"> 
                                <label for="edistrict">Mohalla:<span class="star">*</span></label>
                                <input type="text" class="form-control text-uppercase" maxlength="15" name="Organization_Mohalla" id="Organization_Mohalla"  placeholder=" Mohalla">    
                            </div>
                            
                           <div class="col-sm-3 form-group1 marginB15"> 
                                     <label for="batch"> Ward Number:<span class="star">*</span></label>
                                     <input type="text" class="form-control text-uppercase" maxlength="15" name="Organization_WardNo" id="Organization_WardNo"  placeholder=" Ward Number">
                            </div>-->
                        </div>
                        
                    
                    
                <div class="division_heading">
                    Area Details
                </div>
                    <div class="container w100">
                            
                            
                            
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch"> House Number:</label>
                                    
                                    <input type="text"  onkeypress="javascript:return validAddress(event);" class="form-control text-uppercase" maxlength="15" name="Organization_HouseNo" id="Organization_HouseNo"  placeholder="House Number">
                            </div>
                         <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch">Street:</label>
                                     <input type="text"  onkeypress="javascript:return validAddress(event);" class="form-control text-uppercase" maxlength="15" name="Organization_Street" id="Organization_Street"  placeholder=" Street">
                            </div>
                         
                          <div class="col-sm-3 form-group1 marginB15"> 
                                     <label for="batch"> Road:</label>
                                     <input type="text"  onkeypress="javascript:return validAddress(event);" class="form-control text-uppercase" maxlength="15" name="Organization_Road" id="Organization_Road"  placeholder="Road">
                            </div>
                           <div class="col-sm-3 form-group1 marginB15"> 
                                     <label for="batch"> Address:<span class="star">*</span></label>
                                      <textarea class="form-control  text-uppercase"  onkeypress="javascript:return validAddress(event);" rows="3" id="Organization_Address" onkeypress="javascript:return validAddress(event);" name="Organization_Address" placeholder="Address"></textarea>
                                     
                            </div>
                         
                        </div>
                    <div class="container w100">
                            <div class="col-sm-3 form-group1 marginB15"> 
                                <label for="edistrict">Landmark:<span class="star">*</span></label>
                                <input type="text"  onkeypress="javascript:return validAddress(event);" class="form-control text-uppercase" maxlength="15" name="Organization_Landmark" id="Organization_Landmark"  placeholder="Landmark">    
                            </div>
                            
                           <div class="col-sm-3 form-group1 marginB15"> 
                                  <label for="address"> Area:</label>
                               
                             <input type="text"  onkeypress="javascript:return validAddress(event);" class="form-control text-uppercase" maxlength="15" name="Organization_Area" id="Organization_Area"  placeholder="Area">    
                            
                            </div>
                            
                           <div class="col-sm-3 form-group1 marginB15"> 
                                <label for="edistrict">PinCode:<span class="star">*</span></label>
                                 <input type="text"  onkeypress="javascript:return allownumbers(event);" class="form-control text-uppercase" maxlength="15" name="Organization_PinCode" id="Organization_PinCode"  placeholder="PinCode">
                            </div> 
                           
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch">Formatted Address:</label>
                                  <textarea class="form-control  text-uppercase"  onkeypress="javascript:return validAddress(event);" rows="3" id="txtEmpAddress" onkeypress="javascript:return validAddress(event);" name="txtEmpAddress" placeholder="Formatted Address"></textarea>
                               
                            </div>
                         
                        </div>
                    
                    <div class="container w100">
                   <div class="col-sm-3 form-group1 marginB15">   
                        <label for="area">Area Type:<span class="star">*</span></label> <br/>                               
                        <label class="radio-inline"> <input type="radio" id="areaUrban" name="area" value="Urban" onChange="findselected()"/> Urban </label>
                        <label class="radio-inline"> <input type="radio" id="areaRural" name="area" value="Rural" onChange="findselected1()"/> Rural </label>
                        <input type="hidden" class="form-control" name="arearadio" id="arearadio" value="" placeholder="area">       
                   </div>

                    <div  id="Urban" style="display:none">
                      


                       

<!--                       <div class="col-sm-3 form-group1 marginB15"> 
                            <label for="edistrict">Municipal Town:</label>
                            <input type="text" class="form-control" name="txtMunicipal" id="txtMunicipal" value="NA" placeholder="Municipal Town">       
                        </div>-->
                            <div class="col-sm-3 form-group1 marginB15"> 
                                <label for="edistrict">Municipal Type:</label>
                                <select id="selMuncitype2" name="selMuncitype2" class="form-control">

                                    </select>
                            </div>
                            <div class="col-sm-3 form-group1 marginB15"> 
                                <label for="edistrict">Municipal Name:</label>
                                <select id="selMuncitype" name="selMuncitype" class="form-control">

                                    </select>
                            </div>
                            


                            <div class="col-sm-3 form-group1 marginB15">
                                <label for="edistrict">Ward No.:</label>
                                 <select id="txtWard" name="txtWard" class="form-control">

                                 </select>
<!--                                <input type="text" class="form-control" name="txtWard" id="txtWard" value="NA" placeholder="Ward No">     -->
                            </div>

                            <div class="col-sm-3 form-group1 marginB15">
<!--                                <label for="edistrict">Mohalla:</label>-->
                                <input type="hidden" class="form-control text-uppercase" maxlength="15" name="Organization_Municipal" id="Organization_Municipal"  placeholder="Municipal">
                                <input type="hidden" class="form-control" name="txtMohalla" id="txtMohalla" value="NA" placeholder="Mohalla">      
                            </div>
                    </div>
                        
                    <div  id="Rural" style="display:none">
                       <div class="col-sm-3 form-group1 marginB15">
                            <label for="edistrict">Panchayat Samiti/Block:</label>
                            <select id="selPanchayat" name="selPanchayat" class="form-control" >
                               
                            </select>
                        </div>
                        
                        <div class="col-sm-3 form-group1 marginB15">
                            <label for="edistrict">Gram Panchayat:</label>
                            <select id="selGramPanchayat" name="selGramPanchayat" class="form-control" >
                               
                            </select>
                        </div>
                        
                       <div class="col-sm-3 form-group1 marginB15">
                            <label for="edistrict">Village:</label>
                            <select id="selVillage" name="selVillage" class="form-control" >
                               
                            </select>
                        </div>

                        
                    </div>
                        
                    </div> 
                    

                    
                    <div class="division_heading">
                    Document Details
                </div>
                    
                    
                    
                    
                    
                    
                     <div class="container w100">
                            
                            
                            
                            
                         <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="edistrict">Document Type:<span class="star">*</span></label>
                        <select id="selDoctype" name="selDoctype" class="form-control" >
                            <option value="">Select</option>
                            <option value="PAN_Card">PAN Card</option>
                            
                        </select>     
                         </div>
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="batch">PAN Number:<span class="star">*</span></label>
                                     
                                <input type="text" maxlength="10"  onkeypress="javascript:return validAddress(event);" class="form-control text-uppercase" onkeypress="fnValidatePAN();"  name="Organization_PAN" id="Organization_PAN"  placeholder="PAN Number">
                                <span id="pencardvalidate" style="font-size:10px;"></span><br>
                                <span style="font-size:10px;">Note : PAN No should be 10 digit. Starting 5 should be only alphabets[A-Z] then Remaining 4 should be accepting only alphanumeric and last one is again alphabets[A-Z] .</span>
                             
                            </div>
                            <div class="col-sm-3 form-group1 marginB15"> 
                                 <label for="payreceipt">Upload Scan Document:<font color="red">*</font></label>
                                 <div id="image_preview" style="padding-bottom:5px;">
					<img id="previewing" />
				</div>
                                <input type="file" class="form-control"  name="orgdocproof" id="orgdocproof" onchange="checkScanForm(this)">
				<span style="font-size:10px;"> Note : JPG,JPEG Allowed Max Size =200KB</span>
                                
                            </div>  
                           
                         
                        </div>
                        
                        
                    
                    
                                 
<!--                    <div class="container w100">
                         <div class="col-sm-3 form-group1 marginB15"> 
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" />    
                        </div>    
                    </div> -->
                    
<!--                    <div class="container w100"   style="padding-bottom: 11px;">
                             <div class="col-sm-3  form-group1 marginB15" id="otptext" style=" display: none;">     
                                    <label for="emobile" style="padding:  6px 8px 0px 0px; float: left"> OTP Code:<span class="star">*</span></label>
                                    <input class="form-control  text-uppercase" style="float: left; width: 130px;" name="txtOTP" id="txtOTP" placeholder="OTP Code"  onkeypress="javascript:return allownumbers(event);" type="password">
                             </div>
                            <div id="rOTP" class="col-sm-5"  style="padding-top: 6px;"></div>
                        </div>-->
                        
                           <div class="container w100">
                                    
                                    <div style="display: inline-block;">
                                    <input type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload"/>
<!--                                <input type="submit" name="btnValidate" id="btnValidate" class="btn btn-primary" value="Validate" style="display:none;"/>
                                    <input type="submit" name="btnValidateOTP" id="btnValidateOTP" class="btn btn-primary" value="Validate OTP" style="display:none;"/>
                                    -->
                                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>
                                    </div>
                                    <div id="responseSubmit2" style="display: inline-block;padding-left: 20px;width: 70%;"></div>
                            </div>

                </form>
            </div>
        </div>   
    </div>




</body>
</div>
<!--                <div id="viewimgakc" name="viewimgakc">
                  <a title=""  style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><span id="fix">View uploaded receipt</span> </a> 
                </div>-->
                        <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
                             <div class="modal-dialog" style="z-index:100%">
					<div class="modal-content">
                                            <div class="modal-header" style=" background-color: #439943">
                                                    <button class="close" type="button" data-dismiss="modal">×</button>
                                                    <h3 class="modal-title" style="font-size: 24px;">OTP Varification</h3>
                                            </div> 
                                            <div class="modal-body"  style="text-align: center;">
                                                    
                                                
                                                
                                                <div class="container w100"   style="padding-bottom: 11px;">
                                                    <div id="rOTP" class="col-sm-12"  style="padding-top: 6px;"></div>
                                                    <div id="responseSubmit" style="display: inline-block;padding-left: 20px;width: 70%;"></div>
                                                    <div class="col-sm-12  form-group1 marginB15" id="otptext">     
                                                           <label for="emobile" style="padding:   6px 45px 0px 45px; float: left"> OTP Code:<span class="star">*</span></label>
                                                           <input class="form-control  text-uppercase" style="float: left; width: 130px;" name="txtOTP" id="txtOTP" placeholder="OTP Code"  onkeypress="javascript:return allownumbers(event);" type="password">
                                                          
                                                            <input type="button" name="btnValidateOTP" id="btnValidateOTP" class="btn btn-primary" value="Validate OTP" />
                                                            
                                                    </div>
                                                    
                                                  
                                                </div>
                         
                                            </div>
                                            <div class="modal-footer">
                                                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
				</div>
			</div>    


<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<script src="scripts/uploadscandoc.js"></script>
<script type="text/javascript"> 
 $('#txtEstdate').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true,
		autoclose: true
	});  

</script>



<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#orgdocproof').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("orgdocproof").value = '';
            return false;
        }else{
                                var reader = new FileReader();
				reader.onload = imageIsLoaded;
				reader.readAsDataURL(target.files[0]);
                }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
                alert("Image size should less or equal 200 KB");
                document.getElementById("orgdocproof").value = '';
            
		$('#image_preview').css("display", "none");
		$('#previewing').attr('src', '');
		$('#previewing').attr('width', '');
		$('#previewing').attr('height', '');
            return false;
        }
        else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("orgdocproof").value = '';
            $('#image_preview').css("display", "none");
		$('#previewing').attr('src', '');
		$('#previewing').attr('width', '');
		$('#previewing').attr('height', '');
            return false;
        }
        document.getElementById("orgdocproof").innerHTML = "";
        return true;
    }
    
    function imageIsLoaded(e){
		//$("#file").css("color","green");
		$('#image_preview').css("display", "block");
		$('#previewing').attr('src', e.target.result);
		$('#previewing').attr('width', '150px');
		$('#previewing').attr('height', '150px');
	};
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        
       /* fill country*/
        $(function() {
                 $( "#txtEstdate" ).datepicker({
                    changeMonth: true,
                    changeYear: true
                });
            });
        
        
        
        $("#txtemail").blur(function () {
            validateEmail(this.value);
        });

        function validateEmail(email) {
            if (email=='') return true;
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var isvalid = regex.test(email);
            $('#emailerr').html('');
            if (!isvalid) {
                $('#emailerr').html('Please enter valid email id');
            }
            return isvalid;
        }
        
        
        function GenerateUploadId()
                   {
			$.ajax({
				type: "post",
				url: "common/cfBlockUnblock.php",
				data: "action=GENERATEID",
				success: function (data) {                      
				txtGenerateId.value = data;					
				}
			});
                  }
        GenerateUploadId();
        
        
        function FILLDistrict1() {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FILL",
                success: function (data) {
                    $("#selDistrict1").html(data);
                }
            });
        }
        FILLDistrict1(); 
        
         /* fill RSP*/
        $("#selDistrict1").change(function(){
			FillRSP(this.value);
		});
                
        function FillRSP(districtid) {//alert(districtid);
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillRSP&districtid=" + districtid,
                success: function (data) {
                    $("#selRSP").html(data);
                }
            });
        } 
        
        
         /* fill User RSP*/
        $("#selRSP").change(function(){
			FillUserRSP(this.value);
		});
                
        function FillUserRSP(rspid) {//alert(districtid);
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillUserRSP&rspid=" + rspid,
                success: function (data) {
                    txtRSP.value = data;
                }
            });
        } 
        
        
        
            
        function FILLEntity() {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FILLEntity",
                success: function (data) {
                    $("#selEntity").html(data);
                }
            });
        }
        FILLEntity();    
        
         /* fill state*/
        $("#selEntity").change(function(){
			FillUserRoll(this.value);
                        
		});
                
        function FillUserRoll(entityid) {//alert(districtid);
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillUserRoll&entityid=" + entityid,
                success: function (data) {
                    $("#selUserRoll").html(data);
                }
            });
        } 
        $("#selUserRoll").change(function(){
			//alert(this.value);
                        if(this.value==7){
                          //itgkuser.style.display = ""; 
                          $('#userlo').show(3000);
                        }else{
                          $('#userlo').hide(3000);
                        }
                        
		});
         
            
            
        function FILLCountry() {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FILLCountry",
                success: function (data) {
                    //data = $.parseJSON(data);
                    $("#selCountry").html(data);
                }
            });
        }
        FILLCountry();
        
        
         /* fill state*/
        $("#selCountry").change(function(){
			FillStateName(this.value);
		});
                
        function FillStateName(countryid) {//alert(districtid);
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillState&countryid=" + countryid,
                success: function (data) {
                    $("#selState").html(data);
                }
            });
        } 
        
        
         /* fill Region*/
        $("#selState").change(function(){
			FillRegionName(this.value);
		});
                
        function FillRegionName(stateid) {//alert(districtid);
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillRegion&stateid=" + stateid,
                success: function (data) {
                    $("#selRegion").html(data);
                }
            });
        } 
        
        
        
         /* fill District*/
         $("#selRegion").change(function(){
			FillDistrict(this.value);
		});
         function FillDistrict(regionid) {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillDistrict&regionid=" + regionid,
                success: function (data) {
                    $("#selDistrict").html(data);
                }
            });
        }
        
        
        
        
         /* fill tehsil*/
         $("#selDistrict").change(function(){
			FillTehsil(this.value);
			FillMuncitype(this.value);
			FillPanchayat(this.value);
		});
         function FillTehsil(disticrid) {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillTehsil&disticrid=" + disticrid,
                success: function (data) {
                    $("#selTehsil").html(data);
                }
            });
        }
                
         function FillMuncitype(disticrid) {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillMuncitype&disticrid=" + disticrid,
                success: function (data) {
                    $("#selMuncitype").html(data);
                }
            });
        }
                
         function FillPanchayat(disticrid) {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FILLPanchayat&disticrid=" + disticrid,
                success: function (data) {
                    $("#selPanchayat").html(data);
                }
            });
        }
        
        
        
        $("#selPanchayat").change(function(){
			FILLGramPanchayat(this.value);
		});
        function FILLGramPanchayat(panchayatid) {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FILLGramPanchayat&panchayatid=" + panchayatid,
                success: function (data) {
                    $("#selGramPanchayat").html(data);
                }
            });
        }
        
        
        $("#selGramPanchayat").change(function(){
			FILLVillage(this.value);
		});
        function FILLVillage(panchayatid) {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FILLVillage&panchayatid=" + panchayatid,
                success: function (data) {
                 $("#selVillage").html(data);
                    
                    
                }
            });
        }
        
        
        
        
        
        /* fill tehsil*///
        function FillMuncitype2() {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillMuncitype2",
                success: function (data) {
                    $("#selMuncitype2").html(data);
                }
            });
        }
        
        FillMuncitype2();
        
         $("#selMuncitype").change(function(){
			FillMunicipalName(this.value);
			FillWardName(this.value);
		});
         function FillMunicipalName(Municipalid) {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillMunicipalName&Municipalid=" + Municipalid,
                success: function (data) {//alert(data);
                    //$("#Organization_Municipal").html(data);
                    Organization_Municipal.value = data;
                }
            });
        }
        
        function FillWardName(Municipalid) {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillWardName&Municipalid=" + Municipalid,
                success: function (data) {//alert(data);
                    $("#txtWard").html(data);
                    //txtWard.value = data;
                }
            });
        }
        
        
        
        
         /* fill tehsil*/
         $("#selTehsil").change(function(){
			FillArea(this.value);
		});
         function FillArea(tehsilid) {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FillArea&tehsilid=" + tehsilid,
                success: function (data) {
                    //$("#selAreaType").html(data);
                }
            });
        }
        
        /* fill org type*/
         function FillOrgType() {
            $.ajax({
                type: "post",
                url: "common/cfCreateLogin.php",
                data: "action=FILLORGType",
                success: function (data) {
                    $("#selTypeOrg").html(data);
                }
            });
        }
        FillOrgType();
        
        
        
        
        
        
        function findselected() {

            var UrbanDiv = document.getElementById("areaUrban");
            var category = document.getElementById("Urban");

            if (UrbanDiv) {
                Urban.style.display = areaUrban.checked ? "block" : "none";
                Rural.style.display = "none";
            }
            else
            {
                Urban.style.display = "none";
            }
        }
        function findselected1() {


            var RuralDiv = document.getElementById("areaRural");
            var category1 = document.getElementById("Rural");

            if (RuralDiv) {
                Rural.style.display = areaRural.checked ? "block" : "none";
                Urban.style.display = "none";
            }
            else
            {
                Rural.style.display = "none";
            }
        }
        $("#areaUrban").change(function () {
            findselected();
            //alert(this.value);
            arearadio.value = this.value;
        });
        $("#areaRural").change(function () {
            findselected1();
            arearadio.value = this.value;
        });
        
        
        
        function fnValidatePAN(){
         $('#pencardvalidate').html('');
        }
        
        function ValidatePANCard(pencard) {
            var reg = /[a-zA-z]{5}\d{4}[a-zA-Z]{1}/;
            //var pencard = `ABCDE1234D`;
            //alert(pencard);
            if(pencard!=''){
                if (pencard.match(reg)) {
                     $('#pencardvalidate').empty();
                }
                else {
                    $('#pencardvalidate').empty();
                    $('#Organization_PAN').val('');
                    $('#pencardvalidate').append("<span style='color:red; font-size: 12px;'>You have entered wrong PAN No.</span>");

                    return false;
                }
            }
        }
        
//        $("#btnUpload21").click(function () {
//        alert("sunil");
//         });
        
        $("#btnUpload").click(function () {
        
        var pencard = $('#Organization_PAN').val();
       ValidatePANCard(pencard);
        // PANCARD
        
         if ($("#formCreateLogin").valid())
           {			    
            $('#responseSubmit').empty();
            $('#responseSubmit').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfCreateLogin.php"; // the script where you handle the form input.txtEmpDOB
            var data;
            data = "action=CVALIDATE&orgmobile="+ txtMobile.value +""; //serializes the form's elements.
            $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data)
                        {
                           //alert(data);
                            
                           // $('#btnValidate').show(3000);
                            //$('#btnUpload').hide(3000);
                            $('#responseSubmit2').empty();
                            $('#rOTP').empty();
                            $('#btnSubmit').hide(3000);
                            $("#btnUpload").attr('value', 'Resend OTP');
                            $('#btnValidateOTP').show(3000);
                            $('#otptext').show(3000);
                            $("#myModal").modal("show");
                            $('#responseSubmit').empty();
                            $('#rOTP').append("<p class='sucess'><span><img src=images/correct.gif width=10px /> </span> <span style='padding-left: 7px;'>  " + data + "</span></p>");


                        }
                    });
            
           }
           return false; // avoid to execute the actual submit of the form.           
                    
                    
                    
        });
        
        
        $("#btnValidateOTP").click(function () {//alert("hello");
        
        var pencard = $('#Organization_PAN').val();
        ValidatePANCard(pencard);
        // PANCARD
      
	if ($("#formCreateLogin").valid())
           {			    
            $('#responseSubmit').empty();
            $('#responseSubmit').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfCreateLogin.php"; // the script where you handle the form input.txtEmpDOB
            var data;
            data = "action=OTPVALIDATE&orgmobile="+ txtMobile.value + "&txtOTP="+ txtOTP.value +""; //serializes the form's elements.
            
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   //alert(data);
                    if (data == 'NOTP')
                    {
                        $('#responseSubmit').empty();
                        $('#rOTP').empty();
                        $('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  Please enter OTP first.</span></p>");
                    }
                    else if (data == 'InvalidOTP')
                    {
                        $('#responseSubmit').empty();
                        $('#rOTP').empty();
                        $('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  Invalid OTP. Please try again.</span></p>");
                    }
                    else
                    {
                        $('#responseSubmit').empty();
                        $('#rOTP').empty();
                        $('#btnUpload').hide(3000);
                        $('#btnValidateOTP').hide(3000);
                        $('#btnSubmit').show(3000);
                        $('#txtMobile').attr('readonly', true);
                        $("#myModal").modal("hide");
                        //$('#txtBankAccount').attr('readonly', true);
                        //$('#otptext').show(3000);
                        //$('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /> </span> <span style='padding-left: 7px;'>  " + data + "</span></p>");
                    }
                    //showData();


                }
            });
		 }
            return false; // avoid to execute the actual submit of the form.
        });
        
        
        $("#btnSubmit").click(function () {
        if ($("#formCreateLogin").valid())
                   {
                       if (confirm('Are you sure you want to submit the application.')) 
                            {
                                $('#responseSubmit').empty();
                                $('#responseSubmit').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                var url = "common/cfCreateLogin.php"; // the script where you handle the form input.txtEmpDOB
                                var data;
                                //var birthproof=$('#txtGenerateId').val(); 
                                var forminput=$("#formCreateLogin").serialize();
                                data = "action=ADDUSER&" + forminput; // serializes the form's elements.

                                $.ajax({
                                type: "POST",
                                url: url,
                                data: data,
                                success: function (data)
                                {
                                  // alert(data);
                                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                                        {
                                            $('#responseSubmit').empty();
                                            $('#responseSubmit').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>You have Successfully Inserted your form.</span></p>");
                                             window.setTimeout(function () {
                                                window.location.href = "frmCreateLogin.php";
                                            }, 5000);
                                        }
                                    else
                                        {
                                            $('#responseSubmit').empty();
                                            $('#responseSubmit').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                        }



                                }
                            });
                            }
                    }
                    return false; // avoid to execute the actual submit of the form.
        });

    });    

</script>
</body>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmcreatelogin_validation.js"></script>
<!--<script>

$("#form").validate({
        rules: {
            
			seltype: { required: true}
        },			
        messages: {				
			seltype: { required: '<span style="color:red; font-size: 12px;">Please Select Type.</span>' }
			
        },
	});

</script>-->

<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>