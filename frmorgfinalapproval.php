<?php
$title = "Final Approval NCR Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code='" . $_REQUEST['code'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '4' || $_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258') {
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Final Approval NCR Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmOrgMaster" id="frmOrgMaster" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
<!--                        <div class="col-sm-4 form-group">     
                            <label for="ackno">Enter AO Center Code:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="18" onkeypress="javascript:return allownumbers(event);" name="ackno" id="ackno" placeholder="AO Center Code" required>

                        </div>

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                        </div>-->
                    </div>


                    <div id="main-content" style="display:none;">
                        <div class="container">
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Organization/Center:</label>
                                <input type="text" class="form-control" readonly="" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Registration No:</label>
                                <input type="text" class="form-control" readonly="" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:</label>
                                <input type="text" class="form-control" readonly="" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:</label>
                                <input type="text" class="form-control" readonly="" name="txtType" id="txtType" placeholder="Type Of Organization">  
                            </div>
                        </div>  
                        <br>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Document Type:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                                <input type="hidden" class="form-control"  name="txtDistrictCode" id="txtDistrictCode">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Tehsil:</label>
                                <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="address">Address:</label>
                                <textarea class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road"></textarea>

                            </div>
                        </div>    

                        <div class="container">


                        </div>	


                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>

                    </div>
            </div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function fillForm()
        {           //alert(val);    
            $.ajax({
                type: "post",
                url: "common/cfOrgFinalApproval.php",
                data: "action=APPROVE&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Some Details are Pending for eligiblity of APPROVAL" + "</span></p>");
                    }
                    else {
                        data = $.parseJSON(data);
                        txtName1.value = data[0].orgname;
                        txtRegno.value = data[0].regno;
                        txtEstdate.value = data[0].fdate;
                        txtType.value = data[0].orgtype;
                        txtDocType.value = data[0].doctype;

                        txtDistrict.value = data[0].district;
                        txtDistrictCode.value = data[0].districtcode;
                        txtTehsil.value = data[0].tehsil;
                        //txtStreet.value = data[0].street;
                        txtRoad.value = data[0].road;

                        $('#main-content').show(3000);
                        $('#btnShow').hide();
                        $('#ackno').attr('readonly', true);

                    }

                }
            });
        }
        fillForm();

//        $("#btnShow").click(function () {
//            //var lcode = $('#txtLearnercode').val();
//            //var ccode = $('#txtCentercode').val();
//            if (ackno.value) {
//
//                fillForm(ackno.value);
//            }
//            else {
//                alert("Please Enter AO Center Code to Proceed");
//            }
//        });



        $("#btnSubmit").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfOrgFinalApproval.php"; // the script where you handle the form input.            
            var data;
            if (Mode == 'Add')
            {
                data = "action=ADD&centercode=" + Code + ""; // serializes the form's elements.
            }
            else
            {

            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmorgfinalprocess.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });

            return false;
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>