<?php
$title = "Admission Payment";
include ('header.php');
include ('root_menu.php');
if (isset($_SESSION)) {
    echo "<script>var Mode='Add'</script>";
    echo "<script>var Role='" . $_SESSION['User_UserRoll'] . "'</script>";
} else {

    echo "<script>var Mode='Add'</script>";
    echo "<script>var Role='0'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Madarsa Learner Confirmation</div>
            <div class="panel-body">
                <form name="frmconfirmmadarsalearner" id="frmconfirmmadarsalearner" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                        <div class="col-sm-4 form-group">     
                            <label for="course">Select Course:</label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">
                                <option value="0">Select </option>
                                <option value="20">RS-CIT Madarsa Urdu ParaTeacher</option>
                            </select>
                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Batch:</label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>									
                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select District:</label>
                            <select id="ddlDistrict" name="ddlDistrict" class="form-control">

                            </select>									
                        </div> 
                    </div>

                    <div id="menuList" name="menuList" style="margin-top:5px;"> </div> 

                    <div class="container">
                        <br><input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                    </div>
            </div>
        </div>   
    </div>
</form>
</div>

</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {



        // FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILL&values=" + selCourse + "",
                success: function (data) {

                    $("#ddlBatch").html(data);
                }
            });

        });

        $("#ddlBatch").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL&codes=29",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        });

        function showAllData(val, val1, val2) {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfConfirmMadarsaLearner.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "&district=" + val2 + "",
                success: function (data) {
                    $('#response').empty();
                    $("#menuList").html(data);
                    $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                    $('#btnSubmit').show();
                }
            });
        }

        $("#ddlDistrict").change(function () {
            showAllData(ddlBatch.value, ddlCourse.value, ddlDistrict.value);
        });


        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $('#btnSubmit').hide();
            var url = "common/cfConfirmMadarsaLearner.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmconfirmmadarsalearner").serialize();


            if (Mode == 'Add')
            {
                data = "action=ADD&" + forminput; // serializes the form's elements.
            } else
            {
                //data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    alert(data);
                    $('#response').empty();
                    if (data == 0) {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please Checked Atleast one checkbox." + "</span></p>");
                    } else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                        window.setTimeout(function () {
                            Mode = "Add";
                            window.location.href = 'frmconfirmmadarsalearner.php';
                        }, 4000);
                    }

                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>