<?php
$title = "Manage MyRKCL Events";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var BatchCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var BatchCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {
    ?>
    <!--<link rel="stylesheet" href="css/datepicker.css">
    <script src="scripts/datepicker.js"></script>-->
    <link rel="stylesheet" href="bootcss/css/bootstrap-datetimepicker.min.css">
    <script src="bootcss/js/moment.min.js"></script>
    <script src="bootcss/js/bootstrap-datetimepicker.min.js"></script>


    <div style="min-height:430px !important;max-height:1500px !important;">
        <div class="container"> 


            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">Manage MyRKCL Event</div>
                <div class="panel-body">					
                    <form name="frmeditevent" id="frmeditevent" class="form-inline" role="form" enctype="multipart/form-data">

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Event Category:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-6 form-group"> 

                                <select id="ddlCat" name="ddlCat" class="form-control" onchange="toggle_visibility2('batchmode', 'examevent', 'coursemode', 'correctionevent', 'eoimode')">		

                                </select>
                            </div> 
                        </div>
                        <div class="container">
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Event Name:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-6 form-group"> 

                                <select id="ddlEname" name="ddlEname" class="form-control" onchange="toggle_visibility1('paymode', 'coursemode', 'batchmode')" >

                                </select>
                            </div> 
                        </div>
                        <div class="container" id="paymode" style="display:none;">
                            <div class="col-sm-6 form-group" > 
                                <label for="install">Payment Mode:<span class="star">*</span></label>

                            </div>
                            <div class="col-sm-6 form-group"> 

                                <select id="ddlPaymode" name="ddlPaymode" class="form-control">

                                </select>
                            </div> 
                        </div>

                        <div class="container" id="correctionevent" style="display:none;">
                            <div class="col-sm-6 form-group" > 
                                <label for="install">Correction Event Name:<span class="star">*</span></label>

                            </div>
                            <div class="col-sm-6 form-group"> 

                                <select id="ddlcorrectionevent" name="ddlcorrectionevent" class="form-control">

                                </select>
                            </div> 
                        </div>

                        <div class="container" id="coursemode">
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-6 form-group"> 
                                <select id="ddlCourse" name="ddlCourse" class="form-control">									
                                </select>
                                <select id="coursecode" name="coursecode" style="display:none;" class="form-control">									
                                </select>	
                            </div> 
                        </div>

                        <div class="container" id="eoimode" style="display:none;">
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Select EOI:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-6 form-group"> 
                                <select id="ddlEOI" name="ddlEOI" class="form-control">									
                                </select>
                                <select id="eoicode" name="eoicode" style="display:none;" class="form-control">									
                                </select>	
                            </div> 
                        </div>

                        <div class="container" id="examevent" style="display:none;">
                            <div class="col-sm-6 form-group" > 
                                <label for="install">Exam Event Name:<span class="star">*</span></label>

                            </div>
                            <div class="col-sm-6 form-group"> 

                                <select id="ddlexamevent" name="ddlexamevent" class="form-control">

                                </select>
                            </div> 
                        </div>
                        <div class="container" id="batchmode" >
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Select Batch:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-6 form-group"> 
                                <select id="ddlBatch" name="ddlBatch" class="form-control">									
                                </select>

                            </div> 
                        </div>



                        <div class="container">
                            <input type="submit" name="btnView" id="btnView" class="btn btn-primary" value="View"/>    
                        </div>


                        <div class="container" id ="sdate" style="display: none">
                            <div class="col-sm-6 form-group"> 
                                <label for="sdate">Start Date:<span class="star">*</span></label>

                            </div> 
                            <div class="form-group col-sm-6">
                                <input type="text" class="form-control" id="txtstartdate" name="txtstartdate" readonly="true"/>
                            </div>
                        </div>
                        <div class="container" id ="edate" style="display: none">
                            <div class="col-sm-6 form-group"> 
                                <label for="sdate">End Date:<span class="star">*</span></label>

                            </div> 
                            <div class="form-group col-sm-6" >
                                <input type="text" class="form-control" id="txtenddate" name="txtenddate" readonly="true"/>
                                <input type="hidden" class="form-control" id="eventid" name="eventid" readonly="true"/>
                            </div>

                            <div class="form-group col-sm-6" >
                                <input type="text" class="form-control" id="txtnewenddate" name="txtnewenddate" placeholder="Set New Date" />
                            </div>
                        </div>

                        <div class="container" id ="sub" style="display: none">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Sumbit"/>    
                        </div>
                        <br>
                        <div class="container" id ="ssssub">
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Enabled Events"/>    
                        </div>

                        <div id="grid" style="margin-top:35px;"> </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>


    <script type="text/javascript">


        $('#txtstartdate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            widgetPositioning: {horizontal: "auto", vertical: "bottom"}
        });
        $('#txtnewenddate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            widgetPositioning: {horizontal: "auto", vertical: "bottom"}
        });
    </script>


    <script type="text/javascript">
        var rscfacertificatepayment=0;
        var nameaddressevent = 0;
        var ncrpaymentevent = 0;
        var ownerchangeevent = 0;
        var renewalpenaltyevent = 0;
        var nameaadharupdevent = 0;
        var correctionevent = 0;
        var reexemevent = 0;
        var eoi = 0;
        function toggle_visibility1(id, id6, id7) {
            var e = document.getElementById(id);
            var x = document.getElementById(id6);
            var y = document.getElementById(id7);

            //alert(e);
            var f = document.getElementById('ddlEname').value;
            //alert(f);
            if (f == "3")
            {
                e.style.display = 'block';
            } else if (f == "5")
            {
                e.style.display = 'block';
            } else if (f == "7")
            {
                e.style.display = 'block';
            } else if (f == "8")
            {
                e.style.display = 'block';
            } else if (f == "14")
            {
                e.style.display = 'block';
            } else if (f == "15")
            {
                e.style.display = 'block';
            } else if (f == "16")
            {
                e.style.display = 'block';
            }                    
            else if (f == "18")
            {
                e.style.display = 'block';
            }
            else if (f == "20")
            {
                e.style.display = 'block';
            } else if (f == "17")
            {

                x.style.display = 'block';
                y.style.display = 'block';
            } else {
                e.style.display = 'none';
            }

        }
        function toggle_visibility2(id1, id2, id3, id4, id5) {
            var e = document.getElementById(id1);

            var g = document.getElementById('ddlCat').value;

            var h = document.getElementById(id2);

            var i = document.getElementById(id3);

            var k = document.getElementById(id4);

            var L = document.getElementById(id5);

            // var h = document.getElementById('ddlexamevent').value;
            //alret (h);
            if (g == "2")
            {
                e.style.display = 'none';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'block';
                window.nameaddressevent = 0;
                window.ncrpaymentevent = 0;
                window.ownerchangeevent = 0;
                window.renewalpenaltyevent = 0;
                window.nameaadharupdevent = 0;
                 window.rscfacertificatepayment=0;

            } else if (g == "3")
            {
                e.style.display = 'none';
                h.style.display = 'block';
                k.style.display = 'none';
                L.style.display = 'none';
                window.nameaddressevent = 0;
                window.ncrpaymentevent = 0;
                window.ownerchangeevent = 0;
                window.renewalpenaltyevent = 0;
                window.nameaadharupdevent = 0;
                 window.rscfacertificatepayment=0;
            } else if (g == "4")
            {
                i.style.display = 'none';
                e.style.display = 'none';
                h.style.display = 'none';
                k.style.display = 'block';
                L.style.display = 'none';
                window.nameaddressevent = 0;
                window.ncrpaymentevent = 0;
                window.ownerchangeevent = 0;
                window.renewalpenaltyevent = 0;
                window.nameaadharupdevent = 0;
                 window.rscfacertificatepayment=0;
            } else if (g == "9")
            {
                i.style.display = 'none';
                e.style.display = 'none';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'none';
                window.nameaddressevent = 1;
                window.ncrpaymentevent = 0;
                window.ownerchangeevent = 0;
                window.renewalpenaltyevent = 0;
                window.nameaadharupdevent = 0;
                 window.rscfacertificatepayment=0;
            } else if (g == "6")
            {
                i.style.display = 'none';
                e.style.display = 'none';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'none';
                window.nameaddressevent = 0;
                window.ncrpaymentevent = 1;
                window.ownerchangeevent = 0;
                window.renewalpenaltyevent = 0;
                window.nameaadharupdevent = 0;
                 window.rscfacertificatepayment=0;
            } else if (g == "11")
            {
                i.style.display = 'none';
                e.style.display = 'none';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'none';
                window.nameaddressevent = 0;
                window.ncrpaymentevent = 0;
                window.ownerchangeevent = 1;
                window.renewalpenaltyevent = 0;
                window.nameaadharupdevent = 0;
                 window.rscfacertificatepayment=0;
            } else if (g == "12")
            {
                // i.style.display = 'none';
                // e.style.display = 'none';
                i.style.display = 'block';
                e.style.display = 'block';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'none';
                window.nameaddressevent = 0;
                window.ncrpaymentevent = 0;
                window.ownerchangeevent = 0;
                window.renewalpenaltyevent = 0;
                window.nameaadharupdevent = 1;
                 window.rscfacertificatepayment=0;
            } else if (g == "13")
            {
                i.style.display = 'none';
                e.style.display = 'none';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'none';
                window.nameaddressevent = 0;
                window.ncrpaymentevent = 0;
                window.ownerchangeevent = 0;
                window.renewalpenaltyevent = 1;
                window.nameaadharupdevent = 0;
                 window.rscfacertificatepayment=0;
            } else if (g == "14")
            {
                e.style.display = 'block';
                i.style.display = 'block';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'none';
                window.nameaddressevent = 0;
                window.ncrpaymentevent = 0;
                window.ownerchangeevent = 0;
                window.renewalpenaltyevent = 0;
                window.nameaadharupdevent = 0;
                window.rscfacertificatepayment=1;
            }
            else if (g == "15")
            {
                e.style.display = 'block';
                i.style.display = 'block';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'none';
                window.nameaddressevent = 0;
                window.ncrpaymentevent = 0;
                window.ownerchangeevent = 0;
                window.renewalpenaltyevent = 0;
                window.nameaadharupdevent = 0;
                window.rscfacertificatepayment=1;
            }
            else
            {
                e.style.display = 'block';
                i.style.display = 'block';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'none';
                window.nameaddressevent = 0;
                window.ncrpaymentevent = 0;
                window.ownerchangeevent = 0;
                window.renewalpenaltyevent = 0;
                window.nameaadharupdevent = 0;
                window.rscfacertificatepayment=0;
            }
        }

        //-->
    </script> 

    <script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
    </script>

    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $("#txtstartdate, #txtenddate").datetimepicker();
        $("#txtenddate").change(function () {

            var txtstartdate = document.getElementById("txtstartdate").value;
            var txtenddate = document.getElementById("txtenddate").value;
            if ((Date.parse(txtenddate) <= Date.parse(txtstartdate))) {
                alert("End date should be greater than Start date");
                document.getElementById("txtenddate").value = "";
            }
        });
        $(document).ready(function () {
            $("#btnShow").click(function () {
                FillAllEvent();
            });

            function FillAllEvent() {
                $.ajax({
                    type: "post",
                    url: "common/cfManageEvent.php",
                    data: "action=SHOWALLEVENT",
                    success: function (data) {
                        $('#response').empty();
                        $("#grid").html(data);
                        $('#example').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]
                        });
                    }
                });
            }


            function FillCategory() {
                $.ajax({
                    type: "post",
                    url: "common/cfManageEvent.php",
                    data: "action=FILLCategory",
                    success: function (data) {
                        $("#ddlCat").html(data);
                    }
                });
            }
            FillCategory();


            $("#ddlCat").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfManageEvent.php",
                    data: "action=FILLEvent&category=" + ddlCat.value + "",
                    success: function (data) {
                        $("#ddlBatch").append("<option value='0'>SELECT</option>");
                        // $("#ddlexamevent").append("<option value='0'>SELECT</option>");
                        $("#ddlEname").html(data);
                        $("#sdate").hide();
                        $("#edate").hide();
                        $("#sub").hide();
                        $("#btnView").show();

                    }
                });
            });
            function FillCourse() {
                $.ajax({
                    type: "post",
                    url: "common/cfCourseMaster.php",
                    data: "action=FILLCourseName",
                    success: function (data) {
                        $("#ddlCourse").html(data);
                    }
                });
            }
            FillCourse();


            $("#ddlEname").change(function () {

                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=FILL",
                    success: function (data) {
                        //alert(data);
                        $("#ddlcorrectionevent").html(data);

                    }
                });
            });

            $("#ddlCourse").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLAdmissionBatchcode&values=" + ddlCourse.value + "",
                    success: function (data) {

                        $("#ddlBatch").html(data);
                    }
                });
            });

            $("#ddlCourse").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfApplyEOI.php",
                    data: "action=FILLEOIBYCOURSE&values=" + ddlCourse.value + "",
                    success: function (data) {
                        $("#ddlEOI").html(data);
                    }
                });
            });

            $("#ddlCourse").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfReexamAapplication.php",
                    data: "action=FILLEXAMEVENTMANAGE&values=" + ddlCourse.value + "",
                    success: function (data) {
                        $("#ddlexamevent").html(data);
                    }
                });
            });

            $("#ddlEOI").change(function () {
                $("#sdate").hide();
                $("#edate").hide();
                $("#sub").hide();
                $("#btnView").show();
            });
            $("#ddlBatch").change(function () {
                $("#sdate").hide();
                $("#edate").hide();
                $("#sub").hide();
                $("#btnView").show();
            });
            $("#ddlcorrectionevent").change(function () {
                $("#sdate").hide();
                $("#edate").hide();
                $("#sub").hide();
                $("#btnView").show();
            });

            function FillPaymentMode() {
                $.ajax({
                    type: "post",
                    url: "common/cfManageEvent.php",
                    data: "action=FILLPaymentMode",
                    success: function (data) {
                        $("#ddlPaymode").html(data);
                    }
                });
            }
            FillPaymentMode();


            $("#btnView").click(function () {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfManageEvent.php";
                var data1;
                //data1 = "action=SHOWDATA&course=" + ddlCourse.value + "";
                data1 = "action=SHOWDATA&course=" + ddlCourse.value + "&eoi=" + ddlEOI.value + "&batch=" + ddlBatch.value + "&category=" + ddlCat.value + "&eventname=" + ddlEname.value + "&paymode=" + ddlPaymode.value + "&reexemevent=" + ddlexamevent.value + "&correctionevent=" + ddlcorrectionevent.value + "&nameaddressevent=" + nameaddressevent + "&ncrpaymentevent=" + ncrpaymentevent + "&nameaadharupdevent=" + nameaadharupdevent + "&renewalpenaltyevent=" + renewalpenaltyevent + "&ownerchangeevent=" + ownerchangeevent + "&rscfacertificatepayment=" + rscfacertificatepayment + "";
                //alert(data1);
                $.ajax({
                    type: "post",
                    url: url,
                    data: data1,
                    success: function (data) {
                        $('#response').empty();
                        data = $.parseJSON(data);

                        // var j = document.getElementById('ddlCat').value;
                        $("#btnView").hide();
                        $("#sdate").show();
                        $("#edate").show();
                        $("#sub").show();
                        $("#txtenddate").val('');
                        $("#txtstartdate").val('');
                        txtstartdate.value = data[0].StartDate;
                        txtenddate.value = data[0].EndDate;
                        eventid.value = data[0].EventID;
                    }
                });
                return false;
            });

            $("#btnSubmit").click(function () {
                //            if ($("#frmmanageevent").valid())
                //            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfManageEvent.php"; // the script where you handle the form input.
                var data;


                data = "action=UPDATE&eventid=" + eventid.value + "&enddate=" + txtnewenddate.value + ""; //
                //alert(data);
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                $('#txtenddate').val('');
                                $('#txtstartdate').val('');
                                window.location.href = 'frmEditEvent.php';

                            }, 1000);
                        } else
                        {
                            var data2 = "Already Exist";
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data2 + "</span></div>");
                        }


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });



        });
    </script>
    </html>
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>
<!--<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmmanageevent_validation.js" type="text/javascript"></script>	-->