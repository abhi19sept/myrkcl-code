<?php
    $title = "Learner Attendance";
    include('header.php');
    include('root_menu.php');
    //print_r($_SESSION);
    echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
    echo "<script>var User_UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
?>
<link rel="stylesheet" href="css/datepicker.css">
<link rel="stylesheet" href="morris/morris.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="morris/morris.min.js"></script>
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">

    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">Learner Attendance</div>
            <div class="panel-body">

                <form name="frmlearnerattendance" id="frmlearnerattendance" class="form-inline" role="form"
                      enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-sm-4 form-group">
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">

                                </select>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">

                                </select>
                            </div>

                        </div>


                        <div class="col-md-12">
                            <div id="attendance_heading" style="display: none;">Attendance This Month</div>
                            <div class="chart-responsive">
                                <div class="chart" id="line-chart" style="height: 300px;display: none;">
                                </div>
                            </div>
                        </div>


                        <div id="gird" style="margin-top:25px; width:94%;"></div>


                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary"
                                   value="Submit"/>

                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<?php include 'common/message.php'; ?>
</body>

<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        // autoclose: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
//alert("hello");
        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLCourseName",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }

        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });


        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfLearnerAttendance.php"; // the script where you handle the form input.

            var data;
            data = "action=GETDATA&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    showDatatable(data);
                }
            });
        }

        function showDatatable(data) {

            $('#response').empty();
            $("#gird").html(data);

            $("#example").DataTable({
                dom: 'Bfrtip',
                "bProcessing": true,
                "serverSide": true,
                "ajax": {
                    url: "common/cfLearnerAttendance.php",
                    data: {
                        "action": "GETDATA1",
                        "course": ddlCourse.value,
                        "batch": ddlBatch.value
                    },
                    type: "post"
                },
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columnDefs: [
                    {
                        targets: 1,
                        render: function (data, type, row, meta) {
                            if (type === 'display') {
                                data = '<a href="frmAttendanceList.php?course=' + encodeURIComponent(ddlCourse.value)
                                    + '&batch=' + ddlBatch.value + '&rolecode=' + data + '&mode=Show" target="_blank">' + data + '</a>';
                            }

                            return data;
                        }
                    }
                ]
            });

            $("#sum").html("Calculating...");

            var data1;
            var url = "common/cfLearnerAttendance.php";
            data1 = "action=GETDATA2&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //

            $.ajax({
                type: "post",
                url: url,
                data: data1,
                success: function (data) {
                    $("#sum").html(data);
                }
            });
        }

        function createGraph() {

            $("#line-chart").empty();

            var url = "common/cfLearnerAttendance.php";
            data = "action=getAttendanceGraph&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "&role=" + UserLoginID + "";

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    var graphdata = JSON.parse(data);
                    var mydata = [];

                    $.each(graphdata, function (i, v) {
                        mydata.push({y: i, item1: v});
                    });

                    var line = new Morris.Line({
                        element: 'line-chart',
                        resize: true,
                        data: mydata,
                        xkey: 'y',
                        ykeys: ['item1'],
                        labels: ['Present'],
                        lineColors: ['#3c8dbc'],
                        parseTime:false,
                        hideHover: 'auto',
                        xLabelAngle: 45,
                        /* xLabelFormat: function (timestamp) {
                            var date = new Date(timestamp);
                            return date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
                        },*/
                        dateFormat: function (timestamp) {
                            var date = new Date(timestamp);
                            return date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
                        }
                    });
                }
            });
        }

        function showDataITGK() {
            $('#response').empty().append("<p class='error'><span><img src=images/ajax-loader.gif width='10px' /></span>&nbsp;<span>Processing.....</span></p>");
            var url = "common/cfLearnerAttendance.php";

            $.ajax({
                type: "post",
                url: url,
                data: {
                    "action" : "GETDATAITGK",
                    "course" : encodeURIComponent(ddlCourse.value),
                    "batch": encodeURIComponent(ddlBatch.value),
                    "role": encodeURIComponent(UserLoginID)
                },
                success: function (data) {
                    createGraph();

                    $("#line-chart").css("display", "block");
                    $("#attendance_heading").css("display", "block");
                    $('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }


        $("#btnSubmit").click(function () {
            if ($("#frmlearnerattendance").valid()) {
                if (User_UserRole == 7) {
                    showDataITGK();
                }
                else {
                    showData();
                }
            }

            return false;
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmlearnerattendance.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C !important;
    }
</style>
</html>
