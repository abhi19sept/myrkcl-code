<?php
function curlPost($url, $data_json = NULL) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 300,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data_json,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: 5c22b1ed-b568-1c45-3d0d-ebff096e4cda"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    return $response;
}
$data_json = json_encode(["FunctionName" => "GET_IP_INFO"]);
$url = "http://click.rkcl.in/ipWebservice/allFunctions.php";
$curl_request = curlPost($url, $data_json);
print_r($curl_request);

?>