<?php
ob_start(); 
$title="Admission Fee Wavier Scheme";
include ('header.php'); 
include ('root_menu.php'); 
if ($_SESSION['User_UserRoll'] == '1'|| $_SESSION['User_UserRoll'] == '4') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
 ?>
 
<link rel="stylesheet" href="css/profile_style.css">

<!----  /* Show Table Count Detail*/for box css stat form here---->

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<style>
    

.block-header h2 {
    margin: 0 !important;
    color: #666 !important;
    font-weight: normal;
    font-size: 16px;
}
    /* Card ======================================== */
    .info-box .content .number {
    font-weight: normal;
    font-size: 26px;
    margin-top: -4px;
    color: #555 !important;
}
.bg-red {
  background-color: #F44336 !important;
  color: #fff; }
  .bg-red .content .text,
  .bg-red .content .number {
    color: #fff !important; }

.bg-pink {
  background-color: #E91E63 !important;
  color: #fff; }
  .bg-pink .content .text,
  .bg-pink .content .number {
    color: #fff !important; }

.bg-purple {
  background-color: #9C27B0 !important;
  color: #fff; }
  .bg-purple .content .text,
  .bg-purple .content .number {
    color: #fff !important; }

.bg-deep-purple {
  background-color: #673AB7 !important;
  color: #fff; }
  .bg-deep-purple .content .text,
  .bg-deep-purple .content .number {
    color: #fff !important; }

.bg-indigo {
  background-color: #3F51B5 !important;
  color: #fff; }
  .bg-indigo .content .text,
  .bg-indigo .content .number {
    color: #fff !important; }

.bg-blue {
  background-color: #2196F3 !important;
  color: #fff; }
  .bg-blue .content .text,
  .bg-blue .content .number {
    color: #fff !important; }

.bg-light-blue {
  background-color: #03A9F4 !important;
  color: #fff; }
  .bg-light-blue .content .text,
  .bg-light-blue .content .number {
    color: #fff !important; }

.bg-cyan {
  background-color: #00BCD4 !important;
  color: #fff; }
  .bg-cyan .content .text,
  .bg-cyan .content .number {
    color: #fff !important; }

.bg-teal {
  background-color: #009688 !important;
  color: #fff; }
  .bg-teal .content .text,
  .bg-teal .content .number {
    color: #fff !important; }

.bg-green {
  background-color: #4CAF50 !important;
  color: #fff; }
  .bg-green .content .text,
  .bg-green .content .number {
    color: #fff !important; }

.bg-light-green {
  background-color: #8BC34A !important;
  color: #fff; }
  .bg-light-green .content .text,
  .bg-light-green .content .number {
    color: #fff !important; }

.bg-lime {
  background-color: #CDDC39 !important;
  color: #fff; }
  .bg-lime .content .text,
  .bg-lime .content .number {
    color: #fff !important; }

.bg-yellow {
  background-color: #ffe821 !important;
  color: #fff; }
  .bg-yellow .content .text,
  .bg-yellow .content .number {
    color: #fff !important; }

.bg-amber {
  background-color: #FFC107 !important;
  color: #fff; }
  .bg-amber .content .text,
  .bg-amber .content .number {
    color: #fff !important; }

.bg-orange {
  background-color: #FF9800 !important;
  color: #fff; }
  .bg-orange .content .text,
  .bg-orange .content .number {
    color: #fff !important; }

.bg-deep-orange {
  background-color: #FF5722 !important;
  color: #fff; }
  .bg-deep-orange .content .text,
  .bg-deep-orange .content .number {
    color: #fff !important; }

.bg-brown {
  background-color: #795548 !important;
  color: #fff; }
  .bg-brown .content .text,
  .bg-brown .content .number {
    color: #fff !important; }

.bg-grey {
  background-color: #9E9E9E !important;
  color: #fff; }
  .bg-grey .content .text,
  .bg-grey .content .number {
    color: #fff !important; }

.bg-blue-grey {
  background-color: #607D8B !important;
  color: #fff; }
  .bg-blue-grey .content .text,
  .bg-blue-grey .content .number {
    color: #fff !important; }

.bg-black {
  background-color: #000000 !important;
  color: #fff; }
  .bg-black .content .text,
  .bg-black .content .number {
    color: #fff !important; }

.bg-white {
  background-color: #ffffff !important;
  color: #fff; }
  .bg-white .content .text,
  .bg-white .content .number {
    color: #fff !important; }

.col-red {
  color: #F44336 !important; }

.col-pink {
  color: #E91E63 !important; }

.col-purple {
  color: #9C27B0 !important; }

.col-deep-purple {
  color: #673AB7 !important; }

.col-indigo {
  color: #3F51B5 !important; }

.col-blue {
  color: #2196F3 !important; }

.col-light-blue {
  color: #03A9F4 !important; }

.col-cyan {
  color: #00BCD4 !important; }

.col-teal {
  color: #009688 !important; }

.col-green {
  color: #4CAF50 !important; }

.col-light-green {
  color: #8BC34A !important; }

.col-lime {
  color: #CDDC39 !important; }

.col-yellow {
  color: #ffe821 !important; }

.col-amber {
  color: #FFC107 !important; }

.col-orange {
  color: #FF9800 !important; }

.col-deep-orange {
  color: #FF5722 !important; }

.col-brown {
  color: #795548 !important; }

.col-grey {
  color: #9E9E9E !important; }

.col-blue-grey {
  color: #607D8B !important; }

.col-black {
  color: #000000 !important; }

.col-white {
  color: #ffffff !important; }
    /* Infobox ===================================== */
.info-box {
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
  height: 80px;
  display: flex;
  cursor: default;
  background-color: #fff;
  position: relative;
  overflow: hidden;
  margin-bottom: 30px; }
  .info-box .icon {
    display: inline-block;
    text-align: center;
    background-color: rgba(0, 0, 0, 0.12);
    width: 80px; }
    .info-box .icon i {
      color: #fff;
      font-size: 50px;
      line-height: 80px; }
    .info-box .icon .chart.chart-bar {
      height: 100%;
      line-height: 100px; }
      .info-box .icon .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box .icon .chart.chart-pie {
      height: 100%;
      line-height: 123px; }
      .info-box .icon .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box .icon .chart.chart-line {
      height: 100%;
      line-height: 115px; }
      .info-box .icon .chart.chart-line canvas {
        vertical-align: baseline !important; }
  .info-box .content {
    display: inline-block;
    padding: 7px 10px; }
    .info-box .content .text {
      font-size: 20px;
      margin-top: 3px;
      color: #555; }
    .info-box .content .number {
      font-weight: normal;
      font-size: 26px;
      margin-top: -4px;
      color: #555; }
	  .grid{
        
    display: table-footer-group !important;
	  }
	  
	  /* Absolute Center Spinner */
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}

/* Transparent Overlay */
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
    background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

  background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 150ms infinite linear;
  -moz-animation: spinner 150ms infinite linear;
  -ms-animation: spinner 150ms infinite linear;
  -o-animation: spinner 150ms infinite linear;
  animation: spinner 150ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}

.info-box.bg-light-green.hover-zoom-effect {
    cursor: pointer;
}

.info-box.bg-cyan.hover-expand-effect {
    cursor: pointer;
}

.info-box.bg-purple.hover-zoom-effect {
    cursor: pointer;
}
.info-box.bg-teal.hover-expand-effect {
	
	 cursor: pointer;
}  
.info-box.bg-pink.hover-zoom-effect {
    cursor: pointer;
}
.info-box.bg-orange.hover-expand-effect {
    cursor: pointer;
}
}
</style>

<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			<div class="panel panel-primary" style="margin-top:20px !important;">
                <div class="panel-heading">Learner Discount Offers </div>
				
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data"> 
					<div class="container">
                                <div id="response"></div>

                            </div> 
					<legend><i class="fa fa-location-arrow" aria-hidden="true"></i>&nbsp;(Discount Offers)
					</legend>
					<div class="container">
						<div class="col-md-4 form-group"> 
							<label for="course">Select Course:<span class="star">*</span></label>
								<select id="ddlCourse" name="ddlCourse" class="form-control" required="true">
								</select>
						</div> 

						<div class="col-md-4 form-group">     
							<label for="batch"> Select Batch:<span class="star">*</span></label>
							<select id="ddlBatch" name="ddlBatch" class="form-control" required="true">
							</select>
						</div> 
						
						<div class="col-md-4 form-group">     
							<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show"
								style="margin-top:25px;"/>    
						</div> 						
					</div>
			
			<div id="start" style="display:none;">	
                 <legend><i class="fa fa-location-arrow" aria-hidden="true"></i>
				 <img src="images/hundred.png" style="width:225px;">
                </legend>
               
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
					
					
					
                    			
                        <!-- Widgets /* Show Table Count Detail*/-->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-cyan hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
								
                                <div class="content districtwise" id="1">
                                    <div class="text">District Level Reservation</div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-orange hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">hourglass_empty</i>
                                </div>
                                <div class="content remaining" id="1">
                                    <div class="text">Rest Available Seats (Randomly) </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-purple  hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">content_paste</i>
                                </div>
                                <div class="content report" id="1">
                                    <div class="text">View Level -1 Report </div>
                                   
                                </div>
                            </div>
                        </div>
                     <!--   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-pink hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">thumb_down</i>
                                </div>
                                <div class="content Reset" id="1">
                                    <div class="text">Reset</div>
                                    
                                </div>
                            </div>
                        </div> -->
                     <div id="grid1" class="grid"> </div>
                        <!-- #END# Widgets -->
                   
                </div>
                
                <legend><i class="fa fa-location-arrow" aria-hidden="true"></i><img src="images/fifty.png" style="width:225px;">
                </legend>
                    
                    <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    			
                        <!-- Widgets /* Show Table Count Detail*/-->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-light-green hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                
								<div class="content districtwise" id="2">
                                    <div class="text">District Level Reservation </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-teal hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">hourglass_empty</i>
                                </div>
                                <div class="content remaining" id="2">
                                    <div class="text">Rest Available Seats (Randomly) </div>
                                  
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-cyan hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">content_paste</i>
                                </div>
                                 <div class="content report" id="2">
                                    <div class="text">View Level - 2 Reports</div>
                                    
                                </div>
                            </div>
                        </div>
                        
                     <!--   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-indigo hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">thumb_down</i>
                                </div>
                                <div class="content Reset" id="2">
                                    <div class="text">Reset</div>
                                   
                                </div>
                            </div>
                        </div> -->
                        
                     <div id="grid2" class="grid"> </div>  
                        <!-- #END# Widgets -->
                   
                </div>
                
                        <!-- #END# Widgets -->
					<legend><i class="fa fa-location-arrow" aria-hidden="true"></i><img  src="images/twentyfive.png" style="width:225px;">
                </legend>
                
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    			
                        <!-- Widgets /* Show Table Count Detail*/-->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-teal hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
								
                                <div class="content districtwise" id="3">
                                    <div class="text">District Level Reservation</div>
                                 
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-purple  hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">hourglass_empty</i>
                                </div>
                                <div class="content remaining" id="3">
                                    <div class="text">Rest Available Seats (Randomly)</div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-light-green hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">content_paste</i>
                                </div>
                                <div class="content report" id="3">
                                    <div class="text">View Level - 3 Reports</div>
                                   
                                </div>
                            </div>
                        </div>
                    <!--   <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-pink hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">thumb_down</i>
                                </div>
                                <div class="content Reset" id="3">
                                    <div class="text">Reset</div>
                                    
                                </div>
                            </div>
                        </div> -->
                       
                         <div id="grid3" class="grid"> </div>
                        <!-- #END# Widgets -->
                   
                </div>
            </div>
				</div>
                </form>
				
				
            </div> 
        </div>
    
</div>


</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
table, td, th {    
    border: 1px solid #ddd;
    text-align: left;
}

table {
    border-collapse: collapse;
    width: 100%; font-size:14px;
}
th, td {
    padding: 12px;
}
tr, td {
    padding: 7px;
}
</style>  
<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {      

      	function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfAdmissionFeeWavier.php",
                data: "action=FillCourse",
                success: function (data) {
					$("#ddlCourse").html(data);
                }
            });

        }
		FillCourse();

        $("#ddlCourse").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfAdmissionFeeWavier.php",
                data: "action=FILLBatchName&values=" + ddlCourse.value + "",
                success: function (data) {
					$("#ddlBatch").html(data);
                }
            });

        });
		
		
		  function showData() {
			if (ddlCourse.value == '') {
                alert('Please select a course.');
                return;
            }
			if (ddlBatch.value == '') {
                alert('Please select a batch.');
                return;
            }
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfAdmissionFeeWavier.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=CheckAlreadyGenerated&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Batch." + "</span></div>");
                            
					}
				/*	else if (data == 'yes'){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Fee wavier is already generated for this batch." + "</span></div>");
                            
					}*/
					else if (data == 'no'){
							$('#response').empty();
							$('#start').show(); 
                   }
				   else{
					   $('#response').empty();
							$('#start').show();
				   }
                }
            });
        }

        $("#btnSubmit").click(function () {
            showData();
          return false; // avoid to execute the actual submit of the form.
        });
		
		$(".districtwise").click(function () {
            var id = $(this).attr('id');			
				$('#response').empty();
                $('#response').append("<div class='loading'>Loading&#8230;</div>");
                var url = "common/cfAdmissionFeeWavier.php"; // the script where you handle the form input.
                var data;               
                data = "action=DistrictWiseLottery&levelid=" + id + "&batchcode=" + ddlBatch.value + ""; 
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Something went wrong." + "</span></div>");
                            
						}
                       else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                          /*  window.setTimeout(function () {
                               window.location.href="frmadmissionfeewavier.php";
                           }, 1000);*/
 
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();
                    }
                });
        });
		
		$(".remaining").click(function () {
            var id = $(this).attr('id');					
				$('#response').empty();
                $('#response').append("<div class='loading'>Loading&#8230;</div>");
                var url = "common/cfAdmissionFeeWavier.php"; // the script where you handle the form input.
                var data;               
                data = "action=RemainingSeatWiseLottery&levelid=" + id + "&batchcode=" + ddlBatch.value + ""; 
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Something went wrong." + "</span></div>");
                            
						}
                       else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                           /* window.setTimeout(function () {
                               window.location.href="frmadmissionfeewavier.php";
                           }, 1000);*/
 
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();
                    }
                });
        });	
		
		$(".report").click(function () {
			var id = $(this).attr('id');
			$('#response').empty();
                $('#response').append("<div class='loading'>Loading&#8230;</div>");
                var url = "common/cfAdmissionFeeWavier.php"; // the script where you handle the form input.
                var data;               
                data = "action=LearnerWiseLotteryReport&levelid=" + id + "&batchcode=" + ddlBatch.value + ""; 
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						//alert(data);
						if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Something went wrong." + "</span></div>");                            
						}
						else{
							$('#response').empty();
								if(id=='1'){
									$("#grid2").html('');
									$("#grid3").html('');
									$("#grid1").html(data);
									$('#example1').DataTable({
										dom: 'Bfrtip',
										buttons: [
											'copy', 'csv', 'excel', 'pdf', 'print'
										]
									});
								}
								else if(id=='2'){
									$("#grid1").html('');
									$("#grid3").html('');
									$("#grid2").html(data);
									$('#example2').DataTable({
										dom: 'Bfrtip',
										buttons: [
											'copy', 'csv', 'excel', 'pdf', 'print'
										]
									});
								}
								else if(id=='3'){
									$("#grid2").html('');
									$("#grid1").html('');
									$("#grid3").html(data);
									$('#example3').DataTable({
										dom: 'Bfrtip',
										buttons: [
											'copy', 'csv', 'excel', 'pdf', 'print'
										]
									});
								}
							
						}
						
                       
                        //showData();
                    }
                });
		});	
		
		$(".Reset").click(function () {
            var id = $(this).attr('id');					
				$('#response').empty();
                $('#response').append("<div class='loading'>Loading&#8230;</div>");
                var url = "common/cfAdmissionFeeWavier.php"; // the script where you handle the form input.
                var data;               
                data = "action=ResetGeneratedSeats&levelid=" + id + "&batchcode=" + ddlBatch.value + ""; 
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						
						if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Something went wrong." + "</span></div>");
                            
						}
                       else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                           /* window.setTimeout(function () {
                               window.location.href="frmadmissionfeewavier.php";
                           }, 1000);*/
 
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();
                    }
                });
        });	
	});

</script>
</html>