<html>
	<head>	

        <?php ob_start();  ?>

                                    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119573760-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119573760-1');
</script>
	
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 		
		<meta name="description" content="Blueprint: Horizontal Drop-Down Menu" />
		<meta name="keywords" content="horizontal menu, microsoft menu, drop-down menu, mega menu, javascript, jquery, simple menu" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="favicon.ico">		
		<link rel="stylesheet" href="bootcss/css/bootstrap.min.css">	
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="bootcss/js/bootstrap.min.js"></script>			
		<script src="rkcltheme/js/jquery.validate.min.js"></script>	
        <script src="js/inputmask/dist/jquery.inputmask.bundle.js"></script>
		<script src="js/input-mask.js"></script>

		<link rel="stylesheet" href="assets/header-search.css">
		<link rel="stylesheet" href="assets/footer-distributed-with-address-and-phones.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">				
		<script src="bootcss/js/jquery.dataTables.min.js"></script>	
		<script src="bootcss/js/dataTables.bootstrap.min.js"></script>			
		<link rel="stylesheet" href="bootcss/css/dataTables.bootstrap.min.css">		
            <!-- 		<link href="css/jquery.datepick.css" rel="stylesheet" type="text/css"/> -->
            <script type="text/javascript" src="scripts/validate.js"></script>
			<title><?php echo $title; ?></title>
			<link rel="stylesheet" href="css/style.css">
<!-- <script src="scripts/Menu.js" type="text/javascript"></script> -->
	<!--For all grid export and paging operations-->

	
	<script src="bootcss/js/bootstrap-dialog.js"></script>
<script src="bootcss/js/bootstrap-dialog.min.js"></script>

<link href="bootcss/css/bootstrap-dialog.css" rel="stylesheet" />

<link rel="stylesheet" href="bootcss/css/bootstrap-dialog.min.css">
<link rel="stylesheet" href="bootcss/css/popup.css">
	
	
	
	

 <!-- <script src="bootcss/js/jquery-1.11.3.min.js"></script> -->
<script src="bootcss/js/jquery.dataTables.min.js"></script>
<script src="bootcss/js/dataTables.buttons.min.js"></script>
<script src="bootcss/js/buttons.flash.min.js"></script>
<script src="bootcss/js/jszip.min.js"></script>
<script src="bootcss/js/pdfmake.min.js"></script>
<script src="bootcss/js/vfs_fonts.js"></script>
<script src="bootcss/js/buttons.html5.min.js"></script>
<script src="bootcss/js/buttons.print.min.js"></script>
<link rel="stylesheet" href="bootcss/css/jquery.dataTables.min.css">	
<link rel="stylesheet" href="bootcss/css/buttons.dataTables.min.css">	
<!--For all grid export and paging operations-->



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.2/css/fileinput.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.2/js/fileinput.js"></script>




<link  href="bootcss/css/jquery.fancybox.min.css" rel="stylesheet">
<script src="bootcss/js/jquery.fancybox.min.js"></script>

<script>

//if(history.replaceState) history.replaceState({}, "", "http://myrkcl.com");        
//window.history.pushState("http://myrkcl.com", "Rkcl-Admin", "");   



	 
    //window.oncontextmenu = function () {
   // return false;
//}
    document.onkeydown = function (e) { 
   // if (window.event.keyCode == 123 ||  e.button==2 || window.event.keyCode == 16 || e.button==16)
    if (window.event.keyCode == 16 || e.button==16 || window.event.keyCode == 17 || e.button==17 || window.event.keyCode == 116 || e.button==116 ) 

		
    return false;
}

    var isNS = (navigator.appName == "Netscape") ? 1 : 0;

    if (navigator.appName == "Netscape")
        document.captureEvents(Event.MOUSEDOWN || Event.MOUSEUP);

    function mischandler() {

        return false;

    }

    function mousehandler(e) {

        var myevent = (isNS) ? e : event;

        var eventbutton = (isNS) ? myevent.which : myevent.button;

        if ((eventbutton == 2) || (eventbutton == 3))
            return false;

    }

    document.oncontextmenu = mischandler;

    document.onmousedown = mousehandler;

    document.onmouseup = mousehandler;

</script>	
	
	
	</head>
	<body>
		
		