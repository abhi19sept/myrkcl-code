<?php
$title = "MYRKCL Copy Tables From Database";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_UserRoll'] == '8') {

    ?>
   <link rel="stylesheet" href="css/datepicker.css">
    <script src="scripts/datepicker.js"></script>
    
    <div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 


            <div class="panel panel-primary" style="margin-top:36px !important;">  
                <div class="panel-heading">MYRKCL Copy Tables From Database</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> --> 
                    <form name="frmcpydbtables" id="frmcpydbtables" class="form-inline" role="form" enctype="multipart/form-data">    
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>
															
								<div class="col-sm-4 form-group"> 
									<label for="status">Select Tables:<span class="star">*</span></label>
									<select id="ddltables" name="ddltables" class="form-control">								  
									
									</select>  									
								</div>		
							
							<div class="col-sm-4 form-group" id="course" style="display:none;"> 
								<label for="course">Select Course:<span class="star">*</span></label>
									<select id="ddlCourse" name="ddlCourse" class="form-control"
										required="required" oninvalid="setCustomValidity('Please Select Course')" onchange="try{setCustomValidity('')}catch(e){}"> 
									</select>
							</div> 
					
							<div class="col-md-4 form-group" id="batch" style="display:none;">     
								<label for="batch"> Select Batch:<span class="star">*</span></label>
									<select id="ddlBatch" name="ddlBatch" class="form-control" 
										required="required" oninvalid="setCustomValidity('Please Select Batch')" onchange="try{setCustomValidity('')}catch(e){}">
									</select>
							</div>
							
						
                            <div class="col-sm-4 form-group" id ="sdate" style="display: block"> 
                                <label for="sdate">Start Date:<span class="star">*</span></label>                           
                                <input type="text" class="form-control" id="txtstartdate" name="txtstartdate" readonly="true"
								placeholder="Start Date"/>
                            </div>
                        
                            <div class="col-sm-4 form-group" id ="edate" style="display: block"> 
                                <label for="sdate">End Date:<span class="star">*</span></label>                            
                                <input type="text" class="form-control" id="txtenddate" name="txtenddate" readonly="true"
								placeholder="End Date"/>
                            </div>
							
							<div class="col-sm-4 form-group" id ="range" style="display: none"> 
                                <label for="sdate">Select Range:<span class="star">*</span></label>                           
									<select id="ddlRange" name="ddlRange" class="form-control">									
										<option value="">Select Range</option>
										<option value="1">1 - 5000</option>
										<option value="2">5001 - 10000</option>
										<option value="3">10001 - 15000</option>
										<option value="4">15001 - 20000</option>										
									</select>
                            </div>
							
							<div class="col-sm-4 form-group" id ="MappingCourse" style="display: none"> 
                                <label for="sdate">Select Course:<span class="star">*</span></label>  
									<select id="ddlMappingCourse" name="ddlMappingCourse" class="form-control"
										required="required" oninvalid="setCustomValidity('Please Select Course')" 
												onchange="try{setCustomValidity('')}catch(e){}"> 
									</select>
							</div>						
							
							<div class="col-sm-4 form-group" id="GovtEmpStatus" style="display: none"> 
								<label for="status">Status:<span class="star">*</span></label>
								<select id="ddlGovtEmpStatus" name="ddlGovtEmpStatus" class="form-control">								  
								
								</select> 									 
							</div>
							
							<div class="col-sm-4 form-group" id ="EoiCode" style="display: none"> 
                                <label for="sdate">Select EOI:<span class="star">*</span></label>  
									<select id="ddlEoi" name="ddlEoi" class="form-control"
										required="required" oninvalid="setCustomValidity('Please Select EOI')" 
												onchange="try{setCustomValidity('')}catch(e){}"> 
									</select>
							</div>	
						
					</div>

                        <div class="container" >      
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" 
									value="Submit" style="display:none"/>
                        </div>                  
                    </form>
                </div>
            </div>
        </div>
    </div>
    </body>

    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>
	<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        autoclose: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        autoclose: true
    });
</script>	
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

        $(document).ready(function () {
            //alert("hii");


          function FillTables() {
            $.ajax({
					type: "post",
					url: "common/cfCopyDbTables.php",
					data: "action=GetTables",
					success: function (data) {
						$("#ddltables").html(data);
					}
				});
			}
			FillTables();

			$("#ddltables").change(function () {
				var TableId = $(this).val();
				//alert(TableId);
				 if(TableId == '1' || TableId == '2'){
					 //ddlBatch.value == '';
					 $("#ddlBatch").html('');
						$.ajax({
						type: "post",
						url: "common/cfCopyDbTables.php",
						data: "action=GetCourse",
						success: function (data) {
								$("#ddlCourse").html(data);
							}
						});
							$('#course').show();
							$('#batch').show();
							$('#btnSubmit').hide();
							$('#sdate').hide();
							$('#edate').hide();
							$('#range').hide();
							$('#MappingCourse').hide();
							$('#GovtEmpStatus').hide();
							$('#EoiCode').hide();
					$("#ddlCourse").change(function () {
						$('#btnSubmit').hide();
						var Course = $(this).val();
							$.ajax({
								type: "post",
								url: "common/cfCopyDbTables.php",
								data: "action=GetBatch&values=" + Course + "",
								success: function (data) {
										$("#ddlBatch").html(data);
									}
							});							
					});
					
					$("#ddlBatch").change(function () {
						if(ddlBatch.value == ''){
							$('#btnSubmit').hide();
						}
						else {
							$('#btnSubmit').show();
						}
												
					});
				 }
				 
				 else if(TableId == '13' || TableId == '14' || TableId == '4'){
						$('#sdate').hide();
						$('#edate').hide();
						$('#range').show();
						$('#course').hide();
						$('#batch').hide();
						$('#MappingCourse').hide();
						$('#GovtEmpStatus').hide();
						$('#EoiCode').hide();
						$('#btnSubmit').show();
				 }
				 else if(TableId == '10'){
						$.ajax({
						type: "post",
						url: "common/cfCopyDbTables.php",
						data: "action=GetCourse",
						success: function (data) {
								$("#ddlMappingCourse").html(data);
							}
						});
						$('#sdate').hide();
						$('#edate').hide();
						$('#range').hide();
						$('#course').hide();
						$('#batch').hide();
						$('#MappingCourse').show();
						$('#GovtEmpStatus').hide();
						$('#EoiCode').hide();
						$('#btnSubmit').show();
				 }
				 else if(TableId == '12'){
						$.ajax({
						type: "post",
						url: "common/cfCopyDbTables.php",
						data: "action=GetGovtStatus",
						success: function (data) {
								$("#ddlGovtEmpStatus").html(data);
							}
						});
						$('#sdate').hide();
						$('#edate').hide();
						$('#range').hide();
						$('#course').hide();
						$('#batch').hide();
						$('#MappingCourse').hide();
						$('#EoiCode').hide();
						$('#GovtEmpStatus').show();
						$('#btnSubmit').show();
				 }
				 else if(TableId == '11'){
						$.ajax({
						type: "post",
						url: "common/cfCopyDbTables.php",
						data: "action=GetEOICode",
						success: function (data) {
								$("#ddlEoi").html(data);
							}
						});
						$('#sdate').hide();
						$('#edate').hide();
						$('#range').hide();
						$('#course').hide();
						$('#batch').hide();
						$('#MappingCourse').hide();
						$('#EoiCode').show();
						$('#GovtEmpStatus').hide();
						$('#btnSubmit').show();
				 }
				 else {
						$('#sdate').show();
						$('#edate').show();
						$('#range').hide();
						$('#course').hide();
						$('#batch').hide();
						$('#MappingCourse').hide();						
						$('#EoiCode').hide();
						$('#GovtEmpStatus').hide();
						$('#btnSubmit').show();
				 }
			});
			
			
			
            $("#btnSubmit").click(function () {
			 
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfCopyDbTables.php"; // the script where you handle the form input.
                var data;
				var forminput = $("#frmcpydbtables").serialize();
                if (Mode == 'Add')
                {                    
                     data = "action=UpdateTable&" + forminput;
                    //alert(data);
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						//alert(data);
                        
					if(data==1){
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Successfully Updated.</span></p>");
						window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmcpydbtables.php';
                            }, 3000);
					}
					else {
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something Happned Wrong.</span></p>");
					}
                    }
                });
			 
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });
    </script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
	<script>
		$("#frmcpydbtables").validate({
				rules: {					
					_file: { required: true}, ddlstatus: { required: true }
				},			
				messages: {				
					_file: { required: '<span style="color:red; font-size: 12px;">Please upload Govt. Excel Data.</span>' },
					ddlstatus: { required: '<span style="color:red; font-size: 12px;">Please Select Application Status.</span>' }
				},
			});
	</script>
</html> 

    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>	