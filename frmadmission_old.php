<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Admission Form</title>
    <!--		<link rel="stylesheet" type="text/css" href="./css/tab.css">
      </link>-->
    <link rel="stylesheet" type="text/css" href="./css/admin.css">
    </link>
    <script src="./scripts/jquery-1.10.1.min.js"></script>
    <script src="./scripts/tab.js"></script>
    <link href="css/jquery-ui.css" rel="Stylesheet" type="text/css" />
  </head>
  <body>
    <div class="wrapper">
      <?php
        include './include/header.html';
        
        include './include/menu.php';
        
        if (isset($_REQUEST['code'])) {
            echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var AdmissionCode=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
        ?>
      <div class="main">
          <form name="frmLearnerProfile" id="frmLearnerProfile" action="">
           <table class="field" cellpadding="0" cellspacing="10" border="0" width="100%">
                  <tr>
                    <td colspan="3" id="response">
                    </td>
                  </tr>
           </table>
        <div class="tabs">
          <ul class="tab-links">
            <li class="active"><a href="#tab1">Personal Detail</a></li>
            <li><a href="#tab2">Contact Details</a></li>
            <li><a href="#tab3">Photo/Sign Upload</a></li>
            <li><a href="#tab4">Print Receipt</a></li>
          </ul>
            
          <div class="tab-content">
              
            <div id="tab1" class="tab active">
              
                <table class="field" cellpadding="0" cellspacing="10" border="0" width="100%">
                  
                  <tr>
                    <td colspan="2">
                      Learner Name 
                      <select name="ddllearnertitle" class="text" id="ddllearnertitle" style="">
                        <option value="---" selected="">---</option>
                        <option value="Mr">Mr</option>
                        <option value="Ms">Ms</option>
                        <option value="Miss">Miss</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Dr">Dr</option>
                      </select>
                      <font color="red">*</font> <input type="text" style="width: 400px;" id="txtlname" class="text" name="txtlname" maxlength="50" placeholder="Learner Complete Name"> <font color="red"> * </font> 
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      Father's Name 
                      <select name="ddlparenttitle" class="text" id="ddlparenttitle" style="">
                        <option value="---" selected="">---</option>
                        <option value="Mr">Mr</option>
                        <option value="Ms">Ms</option>
                        <option value="Miss">Miss</option>
                        <option value="Mrs">Mrs</option>
                        <option value="Dr">Dr</option>
                      </select>
                      <font color="red">*</font> <input type="text" style="width: 400px;" id="txtfname" class="text" name="txtfname" maxlength="50" placeholder="Father's Name"> <font color="red"> * </font>
                    </td>
                  </tr>
                  <tr>
                    <td align="left">
                      Proof of Identity&nbsp;&nbsp; 
                      <select name="ddlidproof" class="text" id="ddlidproof" style="width: 111px">
                        <option value="">Select</option>
                        <option value="PAN Card">PAN Card</option>
                        <option value="Voter ID Card">Voter ID Card</option>
                        <option value="Driving License">Driving License</option>
                        <option value="Passport">Passport</option>
                        <option value="Employer ID card">Employer ID card</option>
                        <option value="Government&#39;s ID Card">Government's ID Card</option>
                        <option value="College ID Card">College ID Card</option>
                        <option value="School ID Card">School ID Card</option>
                      </select>
                      <font color="red"> * </font>
                    </td>
                    <td>Enter Number <input type="text" class="text" id="txtidno" name="txtidno" style="width: 137px"> </span> 
                    </td>
                  </tr>
                  <tr>
                    <td align="left">Age (in years) <input type="text" class="text" style="" size="2" maxlength="2" name="age" id="age" value=""> <font color="red"> * </font>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td align="left">Date of birth <font size="1"></font>
                        <input type="text" name="dob" id="dob" class="text" maxlength="" placeholder="MM-DD-YYYY"/>
                         <font color="red"> * </font>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Mother Tongue 
                      <select name="ddlmotherTongue" id="ddlmotherTongue" class="text" style="width: 114px">
                        <option value="">Select</option>
                        <option value="Marathi">Marathi</option>
                        <option value="Hindi">Hindi</option>
                        <option value="English">English</option>
                      </select>
                      <font color="red"> * </font>
                    </td>
                    <td>
                      <fieldset style="width: 210px;">
                        <legend>Medium of
                          Study<font color="red"> * </font>
                        </legend>
                        <br>
                        &nbsp;&nbsp;<input type="radio" name="medium" id="mediumEnglish" value="English">English
                        &nbsp;&nbsp;<input type="radio" name="medium" id="mediumHindi" value="Hindi" >Hindi
                        <br>
                        <br>
                      </fieldset>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <fieldset style="width: 210px;">
                        <legend>Gender<font color="red"> * </font></legend>
                        <br>
                        &nbsp;&nbsp; <input type="radio" id="genderMale" name="gender" value="Male">Male &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" id="genderFemale" name="gender" value="Female">Female
                        <br>
                      </fieldset>
                    </td>
                    <td colspan="2">
                      <fieldset style="width: 318px">
                        <legend>Marital
                          Status<font color="red"> * </font>
                        </legend>
                        <br>
                        &nbsp;&nbsp; <input type="radio" id="mstatusSingle" name="mstatus" value="Single">Single &nbsp;&nbsp;&nbsp;&nbsp; 
                        <input type="radio" id="mstatusMarried" name="mstatus" value="Married">Married
                        <br>
                      </fieldset>
                    </td>
                  </tr>
                  
                </table>
              
            </div>
            <div id="tab2" class="tab">
              
                <table class="field" cellpadding="0" cellspacing="10" border="0" width="100%">
                  <tbody>
                    <tr>
                      <td align="left" bgcolor="CCCCCC" colspan="3"><b>Correspondence
                        Address</b>
                      </td>
                    </tr>
                    
                    <tr>
                      <td nowrap="nowrap">State : &nbsp;
                       
                       
                                <select id="ddlState" name="ddlState" class="text">

                                </select>
                                &nbsp;<font color="red">*</font>
                        </td>
                        <td nowrap="nowrap">Divison : &nbsp;
                       
                       
                                <select id="ddlRegion" name="ddlRegion" class="text">

                                </select>
                                &nbsp;<font color="red">*</font>
                        </td>
                        <td nowrap="nowrap">District : &nbsp;
                       
                     
                                <select id="ddlDistrict" name="ddlDistrict" class="text">

                                </select>
                                &nbsp;<font color="red">*</font>
                        </td>
                    </tr>
                      <tr>
                          <td nowrap="nowrap">Tehsil : &nbsp;
							
                                    <select id="ddlTehsil" name="ddlTehsil" class="text">

                                    </select>
                                    &nbsp;<font color="red">*</font>
                            </td>
                            <td nowrap="nowrap">Area : &nbsp;

                                    <select id="ddlArea" name="ddlArea" class="text">
                                         
                                    </select>
                                    &nbsp;<font color="red">*</font>
                            </td>
                            <td nowrap="nowrap">PIN : &nbsp;
                        <input type="text" name="txtPIN" id="txtPIN" placeholder="pincode" class="text" maxlength="" />
                      </td>
                      </tr>
                      <tr>
                      <td nowrap="nowrap" width="17%">House No. : &nbsp;
                        <input type="text" nname="txtHouseno" id="txtHouseno" placeholder="House Number" class="text" maxlength="" />
                      </td>
               
                      <td nowrap="nowrap">Street : &nbsp;
                        <input type="text" name="txtStreet" id="txtStreet" placeholder="Street" class="text" maxlength="" />
                      </td>
                
                      <td nowrap="nowrap">Road : &nbsp;
                        <input type="text"  name="txtRoad" id="txtRoad" placeholder="Road" class="text" maxlength="" />
                      </td>
                 
                    </tr>
                    
                    <tr>
                      <td align="left" bgcolor="CCCCCC" colspan="3"><b>Contact
                        Details</b>
                      </td>
                    </tr>
                    <tr>
                      <td>Phone(Residence)</td>
                      <td><input type="text" class="text" name="txtResiPh" align="left" id="txtResiPh"></td>
                    </tr>
                    <tr>
                      <td align="left" colspan="2">Mobile (Self)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>+91</b>
                        <input type="text" class="text" name="txtmobile" id="txtmobile" maxlength="10" size="10" value=""> <input type="hidden" id="editMobile" value="1">
                      </td>
                      <td colspan="1">
                        Email&nbsp;&nbsp;
                        <input type="text" class="text" name="txtemail" id="txtemail" maxlength="100" value="" style="width: 260px;">
                      </td>
                    </tr>
                    <tr style="display:">
                      <td align="left" colspan="3">
                        Mobile(Other)&nbsp; <b>+91</b> <input type="text" class="text" name="mobile2" id="mobile2" maxlength="10" size="10" value="">&nbsp;&nbsp; 
                        <select id="mobileRelation" class="text" name="mobileRelation">
                          <option value="0" selected="">Select Mobile belongs to</option>
                          <option value="Parent">Parent</option>
                          <option value="Relative">Relative</option>
                          <option value="Friend">Friend</option>
                          <option value="Neighbour">Neighbour</option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" bgcolor="CCCCCC" colspan="3"><b>Other
                        Information</b>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" colspan="3">
                        Qualification&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <select name="ddlQualification" class="text" id="ddlQualification" style="width: 150px;">
                         
                        </select>
                        <font color="red">*</font>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" colspan="2">
                        Type of
                        Learner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        <select name="ddlLearnerType" class="text" id="ddlLearnerType" style="width: 150px;">
                          
                        </select>
                        <font color="red"> * </font>
                      </td>
                      <td colspan="1"><span id="divgpf" style="position: relative;"> GPF/CPF
                        No.&nbsp;&nbsp;&nbsp;<input type="text" class="text" name="govtNo" id="govtNo" value="" style="width: 160px;" maxlength="45"> </span>
                      </td>
                    </tr>
                    <tr>
                      <td align="left" colspan="2">Physically
                        Challenged&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="radio" class="text" id="physicallyChStNo" name="physicallyChSt" value="No">No
                        &nbsp;&nbsp;&nbsp;&nbsp;                       <input type="radio" class="text" id="physicallyChStYes" name="physicallyChSt" value="Yes">Yes
                      </td>
                    </tr>
                    
                  </tbody>
                </table>
              
            </div>
            <div id="tab3" class="tab">
             
                <table class="field" cellpadding="0" cellspacing="10" border="0" width="100%">
                  <tbody>
                    <tr>
                      <td style="vertical-align: top;">
                        <fieldset style="width: 274px;float: left;">
                          <legend> Photo/Sign
                            Upload 
                          </legend>
                          Upload Photo<input type="file" name="lphoto" class="text" id="lphoto" ><br /><br />
                          Upload Sign<input type="file" name="lsign" class="text" id="lsign" >
                        </fieldset>
                      </td>
                      <td style="vertical-align: top;">
                        <fieldset style="width: 98px;">
                          <legend>Photo</legend>
                          <span id="viewphoto"><img src="./images/photo.jpg"></span>
                        </fieldset>
                        <br>
                        <fieldset style="width: 98px">
                          <legend>Sign</legend>
                          <span id="viewsign"><img src="./images/sign.jpg"></span>
                        </fieldset>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="3" align="right">
                        <p class="btn submit"><input type="submit" id="btnSubmit" name="btnSubmit" value="Submit" /></p>
                      </td>
                    </tr>
                  </tbody>
                </table>
            
            </div>
            <div id="tab4" class="tab">
              
                <td align="center" colspan="3"><input type="button" name="save" id="save" value="PRINT RECEIPT">&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <font color="red"><span id="pwait"></span></font></td>
             
            </div>

          </div>
               
        </div>
        </form>

      </div>

      <?php
        include './include/footer.html';
        ?>
    </div>
  </body>
  <script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
    
        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }
        
       function FillState() {
            $.ajax({
                type: "post",
                url: "common/cfStateMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlState").html(data);
                }
            });
        }
         FillState();
                             
         $("#ddlState").change(function(){
            var selState = $(this).val(); 
            //alert(selState);
            $.ajax({
              url: 'common/cfRegionMaster.php',
              type: "post",
              data: "action=FILL&values=" + selState + "",
              success: function(data){
                            //alert(data);
                            $('#ddlRegion').html(data);
              }
            });
         });
        $("#ddlRegion").change(function(){
            var selregion = $(this).val(); 
            //alert(selregion);
            $.ajax({
              url: 'common/cfDistrictMaster.php',
              type: "post",
              data: "action=FILL&values=" + selregion + "",
              success: function(data){
                            //alert(data);
                            $('#ddlDistrict').html(data);
              }
            });
        });
        $("#ddlDistrict").change(function(){
            var selDistrict = $(this).val(); 
            //alert(selregion);
            $.ajax({
              url: 'common/cfTehsilMaster.php',
              type: "post",
              data: "action=FILL&values=" + selDistrict + "",
              success: function(data){
                            //alert(data);
                            $('#ddlTehsil').html(data);
              }
            });
        });
        $("#ddlTehsil").change(function(){
            var selTehsil = $(this).val(); 
            //alert(selregion);
            $.ajax({
              url: 'common/cfAreaMaster.php',
              type: "post",
              data: "action=FILL&values=" + selTehsil + "",
              success: function(data){
                            //alert(data);
                            $('#ddlArea').html(data);
              }
            });
        });
        function FillQyalification() {
                $.ajax({
                    type: "post",
                    url: "common/cfQualificationMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlQualification").html(data);
                    }
                });
            }
            FillQyalification();
            
        function FillLearnerType() {
                $.ajax({
                    type: "post",
                    url: "common/cfLearnerTypeMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlLearnerType").html(data);
                    }
                });
            }
            FillLearnerType();
        
        
        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=DELETE&values=" + AdmissionCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                           window.location.href="frmLearnerProfile.php";
                       }, 1000);
                        
                        Mode="Add";
                        resetForm("frmLearnerProfile");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }
    
    
        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=EDIT&values=" + AdmissionCode + "",
                success: function (data) {
                    //alert($.parseJSON(data)[0]['Status']);
                    data = $.parseJSON(data);
                    txtlname.value = data[0].lname;
                    txtfname.value = data[0].fname;
                    dob.value = data[0].dob;
//                    ddlmotherTongue.value = data[0].motherTongue;
//                    medium_type.value = data[0].medium_type; 
//                    gender_type.value = data[0].gender_type;
//                    marital_type.value = data[0].marital_type; 
                    txtHouseno.value = data[0].Houseno;
                    txtStreet.value = data[0].Street;
                    txtRoad.value = data[0].Road;
                    txtResiPh.value = data[0].ResiPh; 
                    txtmobile.value = data[0].mobile;
//                    physical_status.value = data[0].physical_status;
//                    txtemail.value = data[0].email;
//                    lphoto.value = data[0].lphoto;
//                    lsign.value = data[0].lsign;
                }
            });
        }
    
//        function showData() {
//            
//            $.ajax({
//                type: "post",
//                url: "common/cfAdmission.php",
//                data: "action=SHOW",
//                success: function (data) {
//    
//                    $("#gird").html(data);
//    
//                }
//            });
//        }
    
//        showData();
    
                      $(function() {
                                  $( "#dob" ).datepicker({
                                    changeMonth: true,
                                    changeYear: true
                                  });
                                });
                                
        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfAdmission.php"; // the script where you handle the form input.
            if (document.getElementById('mediumEnglish').checked) //for radio button
                {
                    var medium_type = 'English';
                }
                else {
                    medium_type = 'Hindi';
                }
                
                if (document.getElementById('genderMale').checked) //for radio button
                {
                    var gender_type = 'Male';
                }
                else {
                    gender_type = 'Female';
                }
                if (document.getElementById('mstatusSingle').checked) //for radio button
                {
                    var marital_type = 'Single';
                }
                else {
                    marital_type = 'Married';
                }
                if (document.getElementById('physicallyChStYes').checked) //for radio button
                {
                    var physical_status = 'Yes';
                }
                else {
                    physical_status = 'No';
                }
            var lphoto = $('#lphoto').val();
            var lsign = $('#lsign').val();
            var data;
            if (Mode == 'Add')
            {
                data = "action=ADD&learnertitle=" + ddllearnertitle.value + "&lname=" + txtlname.value +
                        "&parenttitle=" + ddlparenttitle.value + "&parent=" + txtfname.value + 
                        "&idproof=" + ddlidproof.value +
                        "&idno=" + txtidno.value + "&dob=" + dob.value +
                        "&mothertongue=" + ddlmotherTongue.value + "&medium=" + medium_type +
                        "&gender=" + gender_type + "&marital_type=" + marital_type +
                        "&area=" + ddlArea.value + "&houseno=" + txtHouseno.value +
                        "&streetno=" + txtStreet.value + "&road=" + txtRoad.value +
                        "&resiph=" + txtResiPh.value + "&mobile=" + txtmobile.value +
                        "&qualification=" + ddlQualification.value + "&learnertype=" + ddlLearnerType.value +
                        "&physicallychallenged=" + physical_status + 
                        "&email=" + txtemail.value + "&lphoto=" + lphoto +
                        "&lsign=" + lsign + ""; // serializes the form's elements.
                
            }
            else
            {
                data = "action=UPDATE&code=" + AdmissionCode + "&learnertitle=" + ddllearnertitle.value + "&lname=" + txtlname.value +
                        "&parenttitle=" + ddlparenttitle.value +"&parent=" + txtfname.value +
                        "&idproof=" + ddlidproof.value +
                        "&idno=" + txtidno.value + "&dob=" + dob.value +
                        "&mothertongue=" + ddlmotherTongue.value + "&medium=" + medium_type +
                        "&gender=" + gender_type + "&marital_type=" + marital_type +
                        "&area=" + ddlArea.value + "&houseno=" + txtHouseno.value +
                        "&streetno=" + txtStreet.value + "&road=" + txtRoad.value +
                        "&resiph=" + txtResiPh.value + "&mobile=" + txtmobile.value +
                        "&qualification=" + ddlQualification.value + "&learnertype=" + ddlLearnerType.value +
                        "&physicallychallenged=" + physical_status +
                        "&email=" + txtemail.value + "&lphoto=" + lphoto +
                        "&lsign=" + lsign + ""; // serializes the form's elements.
            }
            //alert(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            $('#response').empty();
                        }, 3000);
    
                        Mode="Add";
                        window.location.href = 'frmadmission.php';
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                   
    
    
                }
            });
    
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    
    });
    
  </script>
</html>