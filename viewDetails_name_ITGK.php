<?php
    $title = "Organization Details";
    include('header.php');
    include('root_menu.php');
    include 'common/modals.php';
    if ($_SESSION["User_UserRoll"] == 14 || $_SESSION["User_UserRoll"] == 7) {
        echo "<script>var OrgCode = '" . $_SESSION['User_Code'] . "'; </script>";
        ?>
        <style type="text/css">

            .asterisk {
                color: red;
                font-weight: bolder;
                font-size: 18px;
                vertical-align: middle;
            }

            .division_heading {
                border-bottom: 1px solid #e5e5e5;
                padding-bottom: 10px;
                font-size: 20px;
                color: #000;
                margin-bottom: 20px;

            }

            .extra-footer-class {
                margin-top: 0;
                margin-bottom: -10px;
                padding: 16px;
                background-color: #fafafa;
                border-top: 1px solid #e5e5e5;
            }

            .ref_id {
                text-align: center;
                font-size: 20px;
                color: #000;
                margin: 0 0 10px 0;
            }

            .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
                cursor: not-allowed;
                background-color: #eeeeee;
                box-shadow: inset 0 0 5px 1px #d5d5d5;
            }

            .form-control {
                border-radius: 2px;
            }

            input[type=text]:hover, textarea:hover {
                box-shadow: 0 1px 3px #aaa;
                -webkit-box-shadow: 0 1px 3px #aaa;
                -moz-box-shadow: 0 1px 3px #aaa;
            }

            .col-sm-3:hover {
                background: none !important;
            }

            .modal-open .container-fluid, .modal-open .container {
                -webkit-filter: blur(5px) grayscale(50%);
                filter: blur(5px) grayscale(50%);
            }

            .btn-success {
                background-color: #00A65A !important;
            }

            .btn-success:hover {
                color: #fff !important;
                background-color: #04884D !important;
                border-color: #398439 !important;
            }

            .addBottom {
                margin-bottom: 12px;
            }

            #image_address {
                opacity: 1;
                display: block;
                width: 100%;
                height: auto;
                transition: .5s ease;
                backface-visibility: hidden;
                cursor: pointer;
            }

            #image_address:hover {
                opacity: 1;
            }

            .middle:hover {
                opacity: 1;
            }

        </style>

        <div class="container" id="showdata">


            <div class="panel panel-primary" style="margin-top:46px !important;">

                <div class="panel-heading">Name Update Request Made by Center : <span id="fld_ITGK_Code"></span></div>
                <div class="panel-body">

                    <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateOrgDetails" name="updateOrgDetails">

                        <div class="ref_id">
                            Reference ID: <span id="ref_id"></span>
                        </div>

                        <div class="row">
                            <div class="col-md-6"><h3 style="text-align: center">Previous</h3></div>
                            <div class="col-md-6"><h3 style="text-align: center">Requested</h3></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="division_heading">
                                    Center Details
                                </div>


                                <div class="box-body" style="margin: 0 100px;">

                                    <div class="form-group">

                                        <div class="col-sm-12 addBottom">
                                            Organization Name
                                            <input type="text" class="form-control" name="Organization_Name_old" id="Organization_Name_old" placeholder="Name of the Organization/Center"
                                                   readonly='readonly'>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="division_heading">
                                    &nbsp;
                                </div>


                                <div class="box-body" style="margin: 0 100px;">

                                    <div class="form-group">

                                        <div class="col-sm-12 addBottom">
                                            Organization Name
                                            <input type="text" class="form-control" name="Organization_Name" id="Organization_Name" placeholder="Name of the Organization/Center"
                                                   readonly='readonly'>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="division_heading">
                                    Registration Document
                                </div>

                                <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#REGDOC"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Document
                                </button>


                                <div class="box-body collapse" style="margin: 0 100px;" id="REGDOC">

                                    <div class="form-group">
                                        <div class="col-sm-12"><br><br>
                                            <iframe id="image_address" style="width: 100%; height: 500px; border: none;"></iframe>
                                            <div id="image_address_ftp"></div>
                                            <div class="middle">
                                                <div class="text">&nbsp;</div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <p>&nbsp;</p>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="division_heading">
                                    Remarks
                                </div>


                                <div class="box-body" style="margin: 0 100px;">

                                    <div class="form-group">
                                        <div class="col-sm-12">Remarks by service provider <span class="asterisk">*</span>
                                            <textarea id="fld_remarks" name="fld_remarks" class="form-control" rows="4" cols="50" style="border-radius: 8px; font-family: Calibri;" readonly
                                            ></textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="box-body" style="margin: 0 100px;display: none;" id="rkcl_remarks">

                                    <div class="form-group">
                                        <div class="col-sm-12"><span id="remark_label">Remarks by RKCL</span> <span class="asterisk">*</span>
                                            <textarea id="fld_remarks_rkcl" name="fld_remarks_rkcl" class="form-control" rows="4" cols="50" style="border-radius: 8px; font-family: Calibri;
"></textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>



                        <div class="row" id="current_status" style="display: none;">
                            <div class="col-md-12">
                                <div class="division_heading">
                                    Status Log
                                </div>

                                <div class="box-body" style="margin: 0 100px; display: none;" id="pending_submission">
                                    <div class="form-group">
                                        <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                            Request haven't submitted yet
                                        </div>
                                    </div>
                                </div>


                                <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_approval_sp">
                                    <div class="form-group">
                                        <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                            Request Submitted on <span id="submission_date"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_approval_rkcl">
                                    <div class="form-group">
                                        <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                            Request Approved by Service Provider on <span id="submission_date_sp"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_payment">
                                    <div class="form-group">
                                        <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                            Request Approved by RKCL on <span id="submission_date_rkcl"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body" style="margin: 0 100px; display: none;" id="rkcl_rejected">
                                    <div class="form-group">
                                        <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                            Request has been Rejected by RKCL on <span id="rejected_date_rkcl"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body" style="margin: 0 100px; display: none;" id="itgk_rejected">
                                    <div class="form-group">
                                        <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                            Request has been Rejected by ITGK on <span id="rejected_date_itgk"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body" style="margin: 0 100px; display: none;" id="paid">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-success">
                                            Paid on <span id="payment_date"></span>
                                        </div>

                                        <div class="col-sm-4 pull-right">
                                            <button type="button" class="btn btn-lg btn-primary" onclick="viewTransactionDetail('<?php echo $_REQUEST['Code'] ?>')">Click here for transaction
                                                details&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="box-footer extra-footer-class col-md-12" id="revoke_div" style="display: none;">
                            <button type="button" class="btn btn-lg btn-primary" onclick="window.location.href='frmAddUpdReqITGK.php'"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</button>
                            <button type="button" id="revoke" class="btn btn-lg btn-danger pull-right"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;Revoke this Request</button>
                        </div>

                        <div class="box-footer extra-footer-class col-md-12" id="edit_div" style="display: none;">
                            <button type="button" class="btn btn-lg btn-primary" onclick="window.location.href='frmAddUpdReqITGK.php'"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</button>
                            <button type="button" id="edit_submit" class="btn btn-lg btn-danger pull-right"><i class="fa fa-edit" aria-hidden="true"></i>&nbsp;&nbsp;Edit and Submit</button>
                        </div>




                    </form>

                    <div id="showPayment" style="display: none"></div>
                </div>

            </div>
        </div>
        </div>


        </body>
        <?php
        include 'common/message.php';
        include 'footer.php';
        $random = (mt_rand(1000, 9999));
        $random .= date("y");
        $random .= date("m");
        $random .= date("d");
        $random .= date("H");
        $random .= date("i");
        $random .= date("s");

        $rkcltransactionid = $random;

        $payutxnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);

        ?>

        <script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>
        <script type="text/javascript" src="bootcss/js/secure.js"></script>
        <script src="rkcltheme/js/jquery.validate.min.js"></script>
        <script type="text/javascript">

            var image;
            var usercode;

            function viewTransactionDetail(Code){
                window.location.href="viewTransactionDetails.php?Code="+Code;
            }

            $("#image_address").on("click", function () {

                $("#download_document").attr("onclick", "downloadFile('" + image + "','" + usercode + "')");
                $("#view_document").modal('show');
            });

            function downloadFile(img, usc) {
                window.location.href='download_document.php?id='+img+'&usercode='+usc+"&flag=identity_proof";
            }

            $("#ref_id").html(decryptString('<?php echo $_REQUEST['Code']?>'));

            function editRequest(code){
                window.location.href='editRequest.php?flag='+encryptString('address')+'&code='+code;
            }

            function fillForm() {

                $("#revoke_success").modal("show");

                $.ajax({
                    type: "post",
                    url: "common/cfmodifyITGK.php",
                    data: "action=FillUpdateAddressDetails&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "&Command="+'<?php echo base64_encode('Name Change');?>',
                    success: function (data) {
                        data = $.parseJSON(data);

                        setTimeout(function () {

                            image = data[0].fld_document_encoded;
                            usercode = data[0].Organization_User_Code_encoded;

                            /*** OLD DATA ***/

                            $("#Organization_Name_old").val(data[0].Organization_Name_old);
 $("#image_address").hide();
$("#image_address_ftp").html(data[0].fld_document);
                            //$("#image_address").attr("src", "upload/identity_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document+"#zoom=95");
                            $("#view_image_large").attr("src", "upload/identity_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document);

                            /*** NEW DATA ***/

                            $("#Organization_ITGK_Code").html(data[0].Organization_ITGK_Code);
                            $("#fld_ITGK_Code").html(data[0].Organization_ITGK_Code);
                            $("#Organization_Name").val(data[0].Organization_Name);

                        }, 3000);

                        $("#submission_date").html(data[0].submission_date);
                        $("#submission_date_sp").html(data[0].submission_date_sp);
                        $("#submission_date_rkcl").html(data[0].submission_date_rkcl);

                        setTimeout(function () {
                            $("#revoke_success").modal("hide");
                        }, 3000);

                        $("#current_status").css("display", "block");

                        $("#fld_remarks").val(data[0].fld_remarks);
                        $("#fld_remarks_rkcl").val(data[0].fld_remarks_rkcl);

                    /*** REQUEST NOT SUBMITTED ***/
                        if(data[0].fld_status === "0"){
                            $("#current_status").css("display","block");
                            $("#pending_submission").css("display","block");
                            $("#edit_div").css("display","block");
                            $("#edit_submit").attr("onclick","editRequest('<?php echo $_REQUEST["Code"]?>')");
                        }

                        /*** REQUEST SUBMITTED, PENDING FOR APPROVAL BY SERVICE PROVIDER ***/
                        if(data[0].fld_status === "1"){
                            $("#submit_pending").css("display","block");
                            $("#current_status").css("display","block");
                            $("#pending_for_approval_sp").css("display","block");
                            //$("#revoke_div").css("display","block");
                        }

                        /*** APPROVED BY SERVICE PROVIDER, PENDING FOR APPROVAL BY RKCL ***/
                        if (data[0].fld_status === "2") {
                            $("#fld_remarks").attr("readonly","readonly");
                            $("#fld_remarks_rkcl").attr("readonly","readonly");
                            $("#rkcl_remarks").css("display","block");
                            $("#submit_pending").css("display","block");
                            $("#current_status").css("display", "block");
                            $("#pending_for_approval_sp").css("display","block");
                            $("#pending_for_approval_rkcl").css("display", "block");
                        }

                        /*** APPROVED BY RKCL, PENDING FOR PAYMENT ***/
                        if(data[0].fld_status === "3"){
                            $("#fld_remarks").attr("readonly","readonly");
                            $("#fld_remarks_rkcl").attr("readonly","readonly");
                            $("#rkcl_remarks").css("display","block");
                            $("#submit_pending").css("display","block");
                            $("#current_status").css("display", "block");
                            $("#pending_for_approval_sp").css("display","block");
                            $("#pending_for_approval_rkcl").css("display", "block");
                            $("#pending_for_payment").css("display", "block");

                            $("#showPayment").html('<div class="box-footer extra-footer-class col-md-12">' +
                                '<form id="frmpostvalue" name="frmpostvalue" action="frmnameaddressfee.php" method="post"> ' +
                                '<button type="submit" id="paynow" class="btn btn-lg btn-danger pull-right"><i class="fa fa-credit-card" aria-hidden="true"></i>&nbsp;&nbsp;Proceed to Payment Page&nbsp;&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>' +
                                '</button>' +
                                '</form></div>');

                            $('#udf1').val(decryptString('<?php echo $_REQUEST['Code']?>'));
                            $('#key').val(data[0].key);
                            $('#RKCLtranid').val('<?php echo $rkcltransactionid;?>');
                            $('#txnid').val('<?php echo $payutxnid;?>');
                            $('#paytypename, #productinfoname, #productinfo').val('<?php echo ('Name Change');?>');
                            $('#firstname').val('<?php echo $_SESSION['Organization_Name'];?>');
                            $('#email').val('<?php echo $_SESSION['User_EmailId'];?>');
                            $('#amount').val(data[0].fld_amount);
                            $('#surl').val(data[0].surl);
                            $('#furl').val(data[0].furl);
                            $('#curl').val(data[0].curl);
                            $('#requestType').val('name');

                            $("#showPayment").css('display', 'block');
                        }

                        /*** REJECTED BY RKCL ***/
                        if(data[0].fld_status === "4"){
                            $("#remark_label").html('Entered Remarks');
                            $("#fld_remarks").attr("readonly","readonly");
                            $("#fld_remarks_rkcl").attr("readonly","readonly");
                            $("#rkcl_remarks").css("display","block");
                            $("#submit_pending").css("display","block");
                            $("#current_status").css("display","block");
                            $("#pending_for_approval_sp").css("display","block");
                            $("#pending_for_approval_rkcl").css("display", "block");
                            $("#rkcl_rejected").css("display","block");
                            $("#rejected_date_rkcl").html(data[0].fld_updatedOn);
                        }

                        /*** PAID BY ITGK ***/
                        if (data[0].fld_status === '5') {
                            $("#remark_label").html('Entered Remarks');
                            $("#fld_remarks").attr("readonly","readonly");
                            $("#fld_remarks_rkcl").attr("readonly","readonly");
                            $("#rkcl_remarks").css("display","block");
                            $("#submit_pending").css("display","block");
                            $("#updateActions").css("display", "none");

                            $.ajax({
                                type: "post",
                                url: "common/cfmodifyITGK.php",
                                data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code']?>'),
                                success: function (data2) {
                                    data2 = $.parseJSON(data2);
                                    $("#payment_date").html(data2[0].fld_updatedOn);
                                }
                            });

                            $("#pending_for_approval_sp").css("display","block");  //SUBMITTED
                            $("#pending_for_approval_rkcl").css("display", "block"); //APPROVED BY SP
                            $("#pending_for_payment").css("display", "block"); // APPROVED BY RKCL
                            $("#paid").css("display", "block");
                        }

                        /*** REJECTED BY ITGK ***/
                        if(data[0].fld_status === "6"){
                            $("#remark_label").html('Entered Remarks');
                            $("#fld_remarks").attr("readonly","readonly");
                            $("#fld_remarks_rkcl").attr("readonly","readonly");
                            $("#rkcl_remarks").css("display","block");
                            $("#submit_pending").css("display","block");
                            $("#current_status").css("display","block");
                            $("#pending_for_approval_sp").css("display","block");
                            $("#itgk_rejected").css("display","block");
                            $("#rejected_date_itgk").html(data[0].fld_updatedOn);
                        }
                    }
                });

                $.ajax({
                    type: "post",
                    url: "common/cfmodifyITGK.php",
                    data: "action=getCenterDetails&Code=" + decryptString('<?php echo $_REQUEST['Code']?>'),
                    success: function (data) {
                        $('#phone').val(data);
                    }
                });
            }

            fillForm();

            $("#commit").on("click", function () {

                var $validator = $("#updateOrgDetails").validate();
                var errors;

                if (!$("#fld_remarks").val()) {
                    errors = {fld_remarks: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
                    $validator.showErrors(errors);
                } else {
                    $("#submitting").modal("show");
                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=commit&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "&fld_remarks=" + $("#fld_remarks").val() + "fld_ref_id=" + decryptString('<?php echo $_REQUEST['Code']?>'),
                        success: function (data) {
                            if (data === "Successfully Inserted") {
                                setTimeout(function () {
                                    $("#submitting").modal("hide");
                                    $("#submitted_thanks").modal("show")
                                }, 3000);
                            }
                        }
                    });
                }

            });

            $("#reject").on("click", function () {

                var $validator = $("#updateOrgDetails").validate();
                var errors;

                if (!$("#fld_remarks").val()) {
                    errors = {fld_remarks: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
                    $validator.showErrors(errors);
                } else {
                    $("#submitting").modal("show");
                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=reject&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "&fld_remarks=" + $("#fld_remarks").val(),
                        success: function (data) {
                            if (data === "Successfully Updated") {
                                setTimeout(function () {
                                    $("#submitting").modal("hide");
                                    $("#submitted_rejected").modal("show")
                                }, 3000);
                            }
                        }
                    });
                }


            });

            $("#revoke").on("click", function () {

                var $validator = $("#updateOrgDetails").validate();
                var errors;

                if (!$("#fld_remarks").val()) {
                    errors = {fld_remarks: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
                    $validator.showErrors(errors);
                } else {
                    $("#submitting").modal("show");
                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=revoke&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "",
                        success: function (data) {
                            if (data === "Sucscessfully Deleted") {
                                setTimeout(function () {
                                    $("#submitting").modal("hide");
                                    $("#submitted_revoked").modal("show")
                                }, 3000);
                            }
                        }
                    });
                }
            });
        </script>
        <?php
    } else {
        echo "<script>$('#unauthorized').modal('show')</script>";
        die;
    }


?>
</html>