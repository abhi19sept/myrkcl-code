<?php
$title="Course Master";
include ('header.php'); 
include ('root_menu.php'); 
 if (isset($_REQUEST['code'])) {
echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var Code=0</script>";
echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {		
?>
<div style="min-height:430px !important;max-height:1500px !important;">
        <div class="container"> 
			 

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Course Master</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmCourseMaster" id="frmCourseMaster" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Course Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtName" id="txtName" placeholder="Course Name">
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Course Type:<span class="star">*</span></label>
								<select id="ddlCourseType" name="ddlCourseType" class="form-control" >
								 
                                </select>
                                     
                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="learnercode">Duration:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtDuration" id="txtDuration" placeholder="Course Duration">
                            </div>



                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Duration Type:<span class="star">*</span></label>
                                <select id="ddlDuration" name="ddlDuration" class="form-control" >
								  <option value="">Please Select</option>
								    <option value="Hours">Hours</option>
                            <option value="Days">Days</option>
                            <option value="Month">Month</option>
                            <option value="Year">Year</option>
                                </select>    
                            </div>                           

                        </div>  

                        <div class="container">

                             
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Medium:<span class="star">*</span></label>
                                <select id="ddlMedium" name="ddlMedium" class="form-control" >
								
                                </select>    
                            </div>
							
							


							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Examination Type:<span class="star">*</span></label>
                                <select id="ddlExamination" name="ddlExamination" class="form-control" >
								
								 
                                </select>    
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Class Type:<span class="star">*</span></label>
                                <select id="ddlClasstype" name="ddlClasstype" class="form-control" >						
								 
                                </select>    
                            </div>  
							
						<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Course Affiliate:<span class="star">*</span></label>
                                <select id="ddlAffiliate" name="ddlAffiliate" class="form-control" >
								
								 
                                </select>    
                            </div> 

                                       

                        </div> 		
						
						<div class="container">
						 

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Status:<span class="star">*</span></label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control" >
								 
                                </select>    
                            </div> 
						</div>                      

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>						
                </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
        function allowchar(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[A-Za-z,-/]")

            if (key == 8 || key == 0 || key == 32) {
                keychar = "a";
            }


            return reg.test(keychar);
        }
        function validAddress(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[A-Za-z,-/&0-9]")

            if (key == 8 || key == 0 || key == 32) {
                keychar = "a";
            }


            return reg.test(keychar);
        }
        function validpolicyno(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[/0-9]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
        function disableBtn(btnID, newText) {

            var btn = document.getElementById(btnID);
            setTimeout("setImage('" + btnID + "')", 10);
            btn.disabled = true;
            btn.value = newText;
        }
        function setImage(btnID) {
            var btn = document.getElementById(btnID);
            btn.style.background = 'url(12501270608.gif)';
        }
    </script>
	
	
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		
		
		
        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }


        function FillCourseType() {
            $.ajax({
                type: "post",
                url: "common/cfCourseTypeMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCourseType").html(data);
                }
            });
        }
        FillCourseType();
		
		function Medium() {
            $.ajax({
                type: "post",
                url: "common/cfMediumMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlMedium").html(data);
                }
            });
        }
        Medium();
		
		
		function FeeType() {
            $.ajax({
                type: "post",
                url: "common/cfFeeTypeMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlFeeType").html(data);
                }
            });
        }
        FeeType();
		
		
		function FillExamination() {
            $.ajax({
                type: "post",
                url: "common/cfExamTypeMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlExamination").html(data);
                }
            });
        }
        FillExamination();
		
		
		
		function FillClass() {
            $.ajax({
                type: "post",
                url: "common/cfClassTypeMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlClasstype").html(data);
                }
            });
        }
        FillClass();
		
		
		
		
		function FillAffiliate() {
            $.ajax({
                type: "post",
                url: "common/cfAffiliateMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlAffiliate").html(data);
                }
            });
        }
        FillAffiliate();
		
		
		
		
		function FillStatus() {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlStatus").html(data);
                }
            });
        }
        FillStatus();



        function fillForm()
                        {
							//alert("HII");
                            $.ajax({
                                type: "post",
                                url: "common/cfCourseMaster.php",
                                data: "action=EDIT&values=" + Code + "",
                                success: function (data) {
									//alert(data);
                                    //alert($.parseJSON(data)[0]['Status']);
                                    data = $.parseJSON(data);
                                    txtName.value = data[0].Course_Name;
								
									ddlCourseType.value = data[0].Course_Type;
									
									ddlDuration.value = data[0].Course_DurationType;
									txtDuration.value = data[0].Course_Duration;
									ddlMedium.value = data[0].Course_Medium;
									
									
									ddlExamination.value = data[0].Course_ExaminationType;
									ddlClasstype.value = data[0].Course_ClassType;
									ddlAffiliate.value = data[0].Course_Affiliate;
									ddlStatus.value = data[0].Course_Status;
									
                                    
                                }
                            });
                        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);

                }
            });
        }

        //showData();		
		
        $("#btnSubmit").click(function () {
			if ($("#frmCourseMaster").valid())
             {
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");   		
            
            var url = "common/cfCourseMaster.php"; // the script where you handle the form input.
            var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&txtName=" + txtName.value + "&ddlCourseType=" + ddlCourseType.value + "&txtDuration="+ txtDuration.value + "&Duration="+ ddlDuration.value +
					   "&ddlMedium="+ ddlMedium.value +
						
						"&ddlExamination="+ ddlExamination.value +
						"&ddlClasstype="+ ddlClasstype.value +
						"&ddlAffiliate="+ ddlAffiliate.value +
						"&ddlStatus="+ ddlStatus.value + ""; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + Code + "&txtName=" + txtName.value + "&ddlCourseType=" + ddlCourseType.value + "&txtDuration="+ txtDuration.value + 
					   "&ddlMedium="+ ddlMedium.value + "&Duration="+ ddlDuration.value +
						
						"&ddlExamination="+ ddlExamination.value +
						"&ddlClasstype="+ ddlClasstype.value +
						"&ddlAffiliate="+ ddlAffiliate.value +
						"&ddlStatus="+ ddlStatus.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmdisplaycourses.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmdisplaycourses");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });
		}
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcoursetype.js" type="text/javascript"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>