<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta charset="UTF-8">
     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    
		<title> RKCL - Administration</title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">		
		<meta name="description" content="Blueprint: Horizontal Drop-Down Menu" />
		<meta name="keywords" content="horizontal menu, microsoft menu, drop-down menu, mega menu, javascript, jquery, simple menu" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" href="bootcss/css/bootstrap.min.css">	
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="bootcss/js/bootstrap.min.js"></script>	
		<script src="rkcltheme/js/jquery.validate.min.js"></script>	
		<script src="js/inputmask/dist/jquery.inputmask.bundle.js"></script>
		<script src="js/input-mask.js"></script>	
		<link rel="stylesheet" href="assets/header-search.css">
		<link rel="stylesheet" href="assets/footer-distributed-with-address-and-phones.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <!--  Bootstrap Style -->

    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/wcd_style.css" rel="stylesheet" type="text/css">
    
        
     <!--  Font-Awesome Style -->
   
	</head>	
	<body oncontextmenu="return false;" >   
	<div style="width:100%; margin:0 auto;">
		<div class="header">
            <img src="images/header_bg.jpg" alt="header_bg" width="100%" height="115px">
        </div>