<?php  ob_start(); 
$title="Government Entry Form";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
    
 ?>
 
<link rel="stylesheet" href="css/profile_style.css">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			 
            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Reimbursement Acknowledgement Receipt </div>
                <div class="panel-body">
                    
                    
                  
                     <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                          <div class="container" id="forcheckinglcode" name="forcheckinglcode">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Please Enter LearnerCode:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="18" onkeypress="javascript:return allownumbers(event);" name="txtLCode" id="txtLCode" placeholder="LearnerCode">
				<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                            </div>
							
                            <div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShowLearner" id="btnShowLearner" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
                            </div>
			</div>
                         
                         
                         
                         
                         
                         
                         <div id="responselen"></div>
                    <div id="pdfak" style="display:none; padding-bottom: 30px;">
                    <iframe id="filesrc"  src="/myrkcl/upload/government_reimbursement/<?php //echo $_SESSION['User_LearnerCode'];?>_PRE_AKN_PAYMENT_RECEIPT.pdf#zoom=130" style="width: 100%; height: 500px; border: none;"></iframe>
                    </div>
                    <div class="col-sm-4 form-group"> 
                     
                     <a id="hrefid" href="frmreimbursementackreceipt .php?learnercode=<?php //echo $_SESSION['User_LearnerCode'];?>">
                     <input type="button" style="display:none;" name="btnShow" id="btnShow" class="btn btn-primary" value="Download" style="margin-top:25px"/> 
                     </a>
                   </div>
                         
                         
                         
                         
                         <div class="container" id="empid" style="display:none; padding-bottom: 30px;">
                                  
                            <div id="errorBox"></div>
                            <div id="responseBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Please Enter Employee ID:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="18"  name="txtEID" id="txtEID"  value=""  placeholder="EmployeeID">
                                <input type="text" class="form-control"  name="txtLCode" id="txtLCode" value=""  placeholder="LearnerCode">
				
                            </div>
							
                            <div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShowEmp" id="btnShowEmp" class="btn btn-primary" value="Submit" style="margin-top:25px"/>    
                            </div>
			</div>
                         
                     </from>
                    
                </div>
            </div> 
            

        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        
        
        /* Added new function for getting detail of learner*/
                                
        function showAckReceipt () {
            $('#responselen').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span> System is Checking / Producing the Reimbursement Acknowledgement Receipt......</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=CHECKEMPRECIPTSHOW&values=" + txtLCode.value + "",
                success: function (data) {
                  //alert(data); 
                  if(data=='NORECD')
                    {
                       window.location.href = "frmgoventempform.php"; 
                    }
                  else if(data=='DONE')
                    {
                        $.ajax({
                            type: "post",
                            url: "common/cfgoventryform.php",
                            data: "action=EMPACKDETAILS&values=" + txtLCode.value + "",
                            success: function (data) {
                                //alert(data);
                                if(data=='DONE')
                                {
                                   window.location.href = "frmgoventempAckReceiptShow.php?LearnerCode=" + txtLCode.value; 
                                }else{
                                   window.location.href = "frmgoventempAckReceipt.php";  
                                }
                                
                            }
                        });
                    }
                  else if(data=='NOEMPID')
                    {
                        $('#responselen').empty();
                        $('#empid').show(3000); 
                    }
                   else{
                        
                    }
                }
            });
        }

        //showAckReceipt();
        /* Added new function for getting detail of learner*/

    });
    
     $("#btnShowLearner").click(function () {
     
     var txtLCode = $('#txtLCode').val();
     
        if(txtLCode == '')
                {   $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter Learner Code First." + "</span></p>");
                }
            else
            {   $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span> System is Checking / Producing the Reimbursement Acknowledgement Receipt......</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=CHECKEMPRECIPTSHOW&values=" + txtLCode + "",
                success: function (data) {
                  //alert(data); 
                  if(data=='NORECD')
                    {
                       //window.location.href = "frmgoventempform.php";
                       $('#response').empty();
                       $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Invalid Learner Code." + "</span></p>");
                    }
                  else if(data=='DONE')
                    {
                        $.ajax({
                            type: "post",
                            url: "common/cfgoventryform.php",
                            data: "action=EMPACKDETAILS&values=" + txtLCode + "",
                            success: function (data) {
                                //alert(data);
                                if(data=='DONE')
                                {
                                  window.location.href = "frmgoventempAckReceiptShow.php?LearnerCode=" + txtLCode; 
                                }
                                else if(data == 'AMNTREM')
                                {
                                     $('#response').empty();
                                     //$("#myModal").modal("hide");
                                     $('#response').append("<p class='error' style='width: 86%;'><span><img src=images/error.gif width=10px /></span><span>" + "   Amount Reimbursed to Beneficiary's Account.  " + "</span></p>");
                                }
                                else
                                {
                                   window.location.href = "frmgoventempAckReceipt.php";  
                                }
                                
                            }
                        });
                    }
                   else if(data=='NOEMPID')
                    {
                        $('#responselen').empty();
                        $('#empid').show(3000); 
                    }
                   else{
                        
                    }
                }
            }); 
            }
    
         });
    
    
    
    $("#btnShowEmp").click(function () {//alert("hello");
    if(txtEID.value == '')
                {
                    $('#responseBox').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter Employee ID First." + "</span></p>");
                }
            else{
                    $('#responseBox').empty();
                    $('#responseBox').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");		
                    $.ajax({
			type: "post",
                        url: "common/cfgoventryform.php",
			data: "action=UPDATEEMPRECIPT&values=" + txtLCode.value + "&EmpId=" + txtEID.value + "",
			success: function (data)
				{
				//alert(data);
				
                                if(data == 'NOEMPID')
                                    {
                                     $('#responseBox').empty();
                                     $('#responseBox').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please enter employee ID." + "</span></p>");
                                    }
                                else if(data == 'APIERROR')
                                    {
                                     $('#responseBox').empty();
                                     $('#responseBox').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   API Error." + "</span></p>");
                                    }
                               else if(data == 'WONGEMPID')
                                    {
                                     $('#responseBox').empty();
                                     $('#responseBox').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   You Have Entered Invalid Employee ID." + "</span></p>");
                                    }
                                else{
                                    
                                    $('#responseBox').empty();
                                    $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + "Sucessfully Upadte" + "</span></p>");
                                    window.setTimeout(function () {
                                        window.location.href = "frmgoventempAckReceiptforall.php";
                                    }, 1000);
                                    }
                                }
                });	
                }
          });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmgoventry_validation_new.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>