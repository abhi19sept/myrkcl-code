<?php
$title = "Rkcl Enquiry Report";
include ('header.php');
include ('root_menu.php');

if ($_SESSION["User_UserRoll"] == '1' || $_SESSION["User_UserRoll"] == '4' || $_SESSION["User_UserRoll"] == '8') 
{
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Rkcl Enquiry Report</div>
            <div class="panel-body">

                <form name="frmrkclenquiryreport" id="frmrkclenquiryreport" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        
                        <div class="row">
                            <div class="col-md-4 form-group">     
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Click here to see Report"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		
		
		
		
		
		$("#btnSubmit").click(function (e) 
		{

				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                e.preventDefault();
                $.ajax({
                type: "post",
                url: "common/cfrkclenquiryreport.php",
                data: "action=SHOW",
                success: function (data) {
                    $('#response').empty();
                    //alert(data);	
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }

            });

            
        });
		function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>

<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
 <?php 
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?> 
