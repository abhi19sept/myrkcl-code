<?php
    require_once 'DAL/classconnection.php';

    $_ObjConnection = new _Connection();
	$_ObjConnection->Connect();
	
	date_default_timezone_set("Asia/Kolkata");

	$response = $_POST;
	$allowedIps = ['180.179.174.1', '180.179.174.2', '180.179.174.13', '180.179.174.14', '180.179.174.15', '::1'];
	//if (!empty($response['txnid']) && in_array($responsedIP, $allowedIps)) {
	if (!empty($response['txnid'])) {
		insertPayUResponseLog($response);
		$response['verify_by'] = 'VerifiedByServerApi';
	} else {
		performReconcilation();
		die('over!!');
	}

/**
*	Function to get database connection object.
*/
	function dbConnection() {
		global $_ObjConnection;

		return $_ObjConnection;
	}

/**
*	Function to Insert PayU response into tbl_payutransactionresponse table as Log.
*/
	function insertPayUResponseLog($response) {
		$_ObjConnection = dbConnection();

		$responsedIP = $_SERVER['REMOTE_ADDR'];

		$responseFields = ['txnid','mihpayid','mode','status','amount','udf1','udf2','udf3','udf4','udf5','udf6','udf7','udf8','udf9','udf10','field0','field1','field2','field3','field4','field5','field6','field7','field8','field9','hash','bank_ref_no','surl','curl','furl','card_token','card_no','card_hash','productinfo','offer','discount','offer_availed','unmappedstatus','firstname','lastname','address1','address2','city','state','country','zipcode','email','phone'];
		$insert = "";
		foreach ($responseFields as $field) {
			$response[$field] = !empty($response[$field]) ? $response[$field] : '';
			$insert .= $field . " = '" . $response[$field] . "', ";
		}

		$insertResponseSql = "INSERT IGNORE INTO tbl_payutransactionresponse SET
			" . $insert . " 
			tx_key = '" . ((!empty($response['key'])) ? $response['key'] : '') . "',
			responseIP = '" . $responsedIP . "',
			response = '" . serialize($response) . "',
			serverInfo = ''";

		$_Response = $_ObjConnection->ExecuteQuery($insertResponseSql, Message::InsertStatement);
	}

/**
*	Function to process transaction response.
*/
	function processResponse($response) {
		$status = $response["status"];
		$firstname = $response["firstname"];
		$amount = $response["amount"];
		$txnid = $response["txnid"];
		$productinfo = $response["productinfo"];
		$udf1 = $response["udf1"];
		$udf2 = $response["udf2"];
		$udf3 = $response["udf3"];
		$udf4 = $response["udf4"];
		$verifyBy = $response["verify_by"];

		$email = (!empty($response['email'])) ? $response['email'] : '';
		$key = (!empty($response['key'])) ? $response['key'] : '';
		$posted_hash = (!empty($response['hash'])) ? $response['hash'] : '';

		if ($status == 'success' || $status == 'PaymentReceive' || $status == 'Money Settled') {
			//Do further process on success;
			switch ($productinfo) {
				case 'ReexamPayment':
				case 'Re Exam Event Name':
					setSuccessForReExamTransaction($txnid, 'ReexamPayment', $udf1, $udf2, $firstname, $amount, $verifyBy);
					break;
				case 'LearnerFeePayment':
				case 'Learner Fee Payment':
					setSuccessForLearnerFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $udf3, $udf4, $firstname, $amount, $verifyBy);
					break;
				case 'Correction Certificate':
				case 'Correction Fee Payment':
				case 'Duplicate Certificate':
					setSuccessForCorrectionFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $verifyBy);
					break;
			}
		} elseif ($status != 'pending') {
			//Do further process on failer;
			switch ($productinfo) {
				case 'ReexamPayment':
				case 'Re Exam Event Name':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateExamDataPaymentStatus(0, $txnid, $udf1);
					break;
				case 'LearnerFeePayment':
				case 'Learner Fee Payment':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateAdmissionPaymentStatus(0, $txnid, $udf1, $udf3, $udf4);
					break;
				case 'Correction Certificate':
				case 'Correction Fee Payment':
				case 'Duplicate Certificate':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateCorrectionPaymentStatus(0, $txnid, $udf1);
					break;
			}
		}
	}

/**
*	Function to get pending transaction as response.
*/
	function getResponseByPaymentTransaction($min = 40) {
		$_ObjConnection = dbConnection();
		$timestamp = mktime(date("H"), date("i") - $min, date("s"), date("m"), date("d"), date("Y"));
		$time = date("Y-m-d H:i:s", $timestamp);
		$selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status IN ('PaymentInProcess', 'PaymentFailure') AND Pay_Tran_Reconcile_status = 'Not Reconciled' AND (Pay_Tran_ProdInfo LIKE ('%exam%') OR Pay_Tran_ProdInfo LIKE ('%learner%') OR Pay_Tran_ProdInfo LIKE ('%Correction%') OR Pay_Tran_ProdInfo LIKE ('%Duplicate%')) AND timestamp <= '" . $time . "' ORDER BY Pay_Tran_Code DESC LIMIT 1";
		//$selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = 'ff6e2c23e11e403a0308'";
		$result = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);
		$response = [];
		if (mysqli_num_rows($result[2])) {
			$data = mysqli_fetch_array($result[2]);
			$response = $this->setTransactionResponse($data);
		}

		return (!empty($response)) ? $response : false;
	}

	function setTransactionResponse($data) {
			$response = [
				'firstname' => $data['Pay_Tran_Fname'],
				'amount' => $data['Pay_Tran_Amount'],
				'txnid' => $data['Pay_Tran_PG_Trnid'],
				'productinfo' => $data['Pay_Tran_ProdInfo'],
				'udf1' => $data['Pay_Tran_ITGK'],
				'udf2' => $data['Pay_Tran_RKCL_Trnid'],
				'udf3' => $data['Pay_Tran_Course'],
				'udf4' => $data['Pay_Tran_Batch'],
				'verify_by' => 'VerifiedByCron'
			];

		return $response;
	}

/**
*	Function to get transaction status from PayU as response.
*/
	function getTransactionStatusByPayU($response) {
		$txnid = $response['txnid'];
		if (!empty($txnid)) {
			$key = "X2ZPKM";
			$salt = "8IaBELXB";
			$command = "verify_payment";
			$hash_str = $key  . '|' . $command . '|' . $txnid . '|' . $salt ;
			$hash = strtolower(hash('sha512', $hash_str));
			$request = [
				'key' => $key,
				'hash' => $hash,
				'var1' => $txnid,
				'command' => $command
			];
			
			$qs = http_build_query($request);
			//$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
			$wsUrl = "https://info.payu.in/merchant/postservice?form=2";
			$payuResponses = curlCall($wsUrl, $qs, TRUE);
			$response['key'] = $key;
			$response['hash'] = $hash;
		}
		if(!empty($payuResponses['status']) && $payuResponses['status'] == 1) {
			foreach($payuResponses['transaction_details'] as $val) {
				$status = $val['status'];
				$response['amount'] = intval($val['amt']);
			}
		} else {
			$status = 'PaymentnotDone';
		}

		$response['status'] = $status;

		return $response;
	}

/**
*	Function to make cURL request to get response.
*/
	function curlCall($wsUrl, $qs, $true) {
		$c = curl_init();
		
		curl_setopt($c, CURLOPT_URL, $wsUrl);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
	
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		
		$o = curl_exec($c);
		if (curl_errno($c)) {
		    $c_error = curl_error($c);
			if (empty($c_error)) {
			  $c_error = 'Some server error';
			}

			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);

		return $arr;		
	}

/**
*	Functions for Re Exam Transactions Start
*/
	function setSuccessForReExamTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy) {
		$_ObjConnection = dbConnection();
		$refund = checkIfRefundRequiredForReExamTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount);
		} else {
			processToRefundReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount);
		}
	}

	function processToConfirmReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount) {
		$_ObjConnection = dbConnection();
		updateExamDataPaymentStatus(1, $transactionId, $centerCode);
		$_SelectReexamtxn = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($_SelectReexamtxn, Message::SelectStatement);
		$numRows = mysqli_num_rows($response[2]);
		if(!$numRows) {
			insertReExamTransaction('Success', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId);
		} else {
			updateReExamTransactionStatus('Success', $amount, $transactionId, $centerCode);
		}
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
	}

	function isPaymentTransactionFound($productInfo, $transactionId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);

		return mysqli_num_rows($response[2]);
	}

	function updatePaymentTransactionStatus($verifyApiStatus, $transactionStatus, $transactionReconcileStatus, $transactionId, $centerCode = false) {
		$_ObjConnection = dbConnection();
		$filter = ($centerCode) ?  " AND Pay_Tran_ITGK = '" . $centerCode . "'" : '';
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status = '" . $transactionStatus . "' AND Pay_Tran_PG_Trnid = '" . $transactionId . "'" . $filter;
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_payment_transaction SET 
				pay_verifyapi_status = '" . $verifyApiStatus . "',
				Pay_Tran_Status = '" . $transactionStatus . "',
				Pay_Tran_Reconcile_status = '" . $transactionReconcileStatus . "' 
			WHERE 
			Pay_Tran_PG_Trnid = '" . $transactionId . "'" . $filter;
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}
	
	function updateExamDataPaymentStatus($paymentstatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM examdata WHERE paymentstatus = " . $paymentstatus . " AND itgkcode = '" . $centerCode . "'  AND reexam_TranRefNo = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "UPDATE examdata SET paymentstatus = '" . $paymentstatus ."' WHERE itgkcode = '" . $centerCode . "'  AND reexam_TranRefNo = '" . $transactionId . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}
	
	function insertReExamTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Status = '" . $transactionStatus . "' AND Reexam_Transaction_Amount = '" . $amount . "' AND Reexam_Transaction_Txtid = '" . $transactionId . "' AND  Reexam_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_reexam_transaction (
				Reexam_Transaction_Code,
				Reexam_Transaction_Status,
				Reexam_Transaction_Fname,
				Reexam_Transaction_Amount,
				Reexam_Transaction_Txtid,
				Reexam_Transaction_ProdInfo,
				Reexam_Transaction_CenterCode,
				Reexam_Transaction_RKCL_Txid,
				Reexam_DateTime
			) 
			SELECT CASE WHEN MAX(Reexam_Transaction_Code) IS NULL THEN 1 ELSE MAX(Reexam_Transaction_Code) + 1 END AS Reexam_Transaction_Code,
			'" . $transactionStatus . "' AS Reexam_Transaction_Status,
			'" . $firstName . "' AS Reexam_Transaction_Fname,
			'" . $amount . "' AS Reexam_Transaction_Amount,
			'" . $transactionId . "' AS Reexam_Transaction_Txtid,
			'" . $productInfo . "' AS Reexam_Transaction_ProdInfo,
			'" . $centerCode . "' AS Reexam_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Reexam_Transaction_RKCL_Txid,
			'" . date("Y-m-d H:i:s") . "' AS Reexam_DateTime 
			FROM tbl_reexam_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateReExamTransactionStatus($transactionStatus, $reExamTransactionAmount, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Status = '" . $transactionStatus . "' AND Reexam_Transaction_Amount = '" . $reExamTransactionAmount . "' AND Reexam_Transaction_Txtid = '" . $transactionId . "' AND  Reexam_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_reexam_transaction SET 
				Reexam_Transaction_Status = '" . $transactionStatus . "', Reexam_Transaction_Amount = '" . $reExamTransactionAmount . "' 
			WHERE 
				Reexam_Transaction_Txtid = '" . $transactionId . "' AND  Reexam_Transaction_CenterCode = '" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}

	function checkIfRefundRequiredForReExamTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(reexam_TranRefNo) AS ReexamTxnid FROM examdata WHERE reexam_TranRefNo = '" . $transactionId . "' AND itgkcode = '" . $centerCode . "' GROUP BY reexam_TranRefNo";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$reExamCounts = mysqli_fetch_array($response[2]);
				$reExamAmount = ($reExamCounts['ReexamTxnid'] * 300);
				if ($reExamAmount != $amount) {
					$refund = true;
				}
			} else {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}
	
	function processToRefundReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount) {
		$_ObjConnection = dbConnection();
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		$query = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertReExamTransaction('Refund', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId);
		} else {
			updateReExamTransactionStatus('Refund', $amount, $transactionId, $centerCode);
		}
		updateExamDataPaymentStatus(0, $transactionId, $centerCode);
	}

	function setForPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_payment_refund (
				Payment_Refund_Id,
				Payment_Refund_Txnid,
				Payment_Refund_Amount,
				Payment_Refund_Status,
				Payment_Refund_ProdInfo,
				Payment_Refund_ITGK,
				Payment_Refund_RKCL_Txid
			)
			SELECT CASE WHEN MAX(Payment_Refund_Id) IS NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
			'" . $transactionId . "' AS Payment_Refund_Txnid,
			'" . $amount . "' AS Payment_Refund_Amount,
			'" . $status . "' AS Payment_Refund_Status,
			'" . $productInfo . "' AS Payment_Refund_ProdInfo,
			'" . $centerCode . "' AS Payment_Refund_ITGK,
			'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid
			FROM tbl_payment_refund";

			$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "'";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (! mysqli_num_rows($response[2])) {
				$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
			}
		}
	}
/**
*	Functions of Re Exam Transactions End
*/

/**
*	Functions for Learner Fee Transactions Start
*/
	function setSuccessForLearnerFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $firstName, $amount, $verifyBy) {
		$refund = checkIfRefundRequiredForLearnerFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch);
		} else {
			processToRefundAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch);
		}
	}

	function processToConfirmAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch) {
		$_ObjConnection = dbConnection();
		updateAdmissionPaymentStatus(1, $transactionId, $centerCode, $course, $batch);
		$query = "SELECT * From tbl_admission_transaction WHERE Admission_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		$numRows = mysqli_num_rows($response[2]);
		if(!$numRows) {
			insertAdmissionTransaction('Success', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		} else {
			updateAdmissionTransactionStatus('Success', $amount, $transactionId, $centerCode, $course, $batch);
		}
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
	}

	function updateAdmissionPaymentStatus($paymentstatus, $transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_admission WHERE Admission_Payment_Status = " . $paymentstatus . " AND Admission_ITGK_Code = '" . $centerCode . "'  AND Admission_TranRefNo = '" . $transactionId . "' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "UPDATE tbl_admission SET Admission_Payment_Status = '" . $paymentstatus . "' WHERE Admission_ITGK_Code = '" . $centerCode . "' AND Admission_TranRefNo = '" . $transactionId . "' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}

	function insertAdmissionTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_admission_transaction WHERE Admission_Transaction_Status = '" . $transactionStatus . "' AND Admission_Transaction_Amount = '" . $amount . "' AND Admission_Transaction_Txtid = '" . $transactionId . "' AND  Admission_Transaction_CenterCode = '" . $centerCode . "' AND Admission_Transaction_Course = '" . $course . "' AND  Admission_Transaction_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_admission_transaction (
				Admission_Transaction_Code,
				Admission_Transaction_Status,
				Admission_Transaction_Fname,
				Admission_Transaction_Amount,
				Admission_Transaction_Txtid,
				Admission_Transaction_ProdInfo,
				Admission_Transaction_CenterCode,
				Admission_Transaction_RKCL_Txid,
				Admission_Transaction_Course,
				Admission_Transaction_Batch,
				Admission_Transaction_DateTime
			) 
			SELECT CASE WHEN MAX(Admission_Transaction_Code) Is NULL THEN 1 ELSE MAX(Admission_Transaction_Code) + 1 END AS Admission_Transaction_Code,
			'" . $transactionStatus . "' AS Admission_Transaction_Status,
			'" . $firstName . "' AS Admission_Transaction_Fname,
			'" . $amount . "' AS Admission_Transaction_Amount,
			'" . $transactionId . "' AS Admission_Transaction_Txtid,
			'" . $productInfo . "' AS Admission_Transaction_ProdInfo,
			'" . $centerCode . "' AS Admission_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Admission_Transaction_RKCL_Txid,
			'" . $course . "' AS Admission_Transaction_Course,
			'" . $batch . "' AS Admission_Transaction_Batch,
			'" . date("Y-m-d H:i:s") . "' AS Admission_Transaction_DateTime 
			FROM tbl_admission_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateAdmissionTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_admission_transaction WHERE Admission_Transaction_Status = '" . $transactionStatus . "' AND Admission_Transaction_Amount = '" . $amount . "' AND Admission_Transaction_Txtid = '" . $transactionId . "' AND  Admission_Transaction_CenterCode = '" . $centerCode . "' AND Admission_Transaction_Course = '" . $course . "' AND Admission_Transaction_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_admission_transaction SET 
				Admission_Transaction_Status = '" . $transactionStatus . "',
				Admission_Transaction_Amount = '" . $amount . "'
			WHERE 
				Admission_Transaction_Txtid = '" . $transactionId . "' AND 
				Admission_Transaction_CenterCode = '" . $centerCode . "' AND 
				Admission_Transaction_Course = '" . $course . "' AND 
				Admission_Transaction_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}

	function checkIfRefundRequiredForLearnerFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(Admission_TranRefNo) AS AdmissionTxnid, Admission_Course, Admission_Batch FROM tbl_admission WHERE Admission_TranRefNo = '" . $transactionId . "' AND Admission_ITGK_Code = '" . $centerCode . "' GROUP BY Admission_TranRefNo, Admission_Course, Admission_Batch";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['AdmissionTxnid'];
			    $course = $row['Admission_Course'];
			    $baseAmount = (($course == '1' || $course == '4') ? 1000 : (($course == '5') ? 3000 : 0));
				$admissionAmount = $payTxnid * $baseAmount;
				if ($admissionAmount != $amount) {
					$refund = true;
				}
			} else {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch) {
		$_ObjConnection = dbConnection();
		setForAdmissionPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		$query = "SELECT * FROM tbl_admission_transaction WHERE Admission_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertAdmissionTransaction('Refund', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		} else {
			updateAdmissionTransactionStatus('Refund', $amount, $transactionId, $centerCode, $course, $batch);
		}
		updateAdmissionPaymentStatus(0, $transactionId, $centerCode, $course, $batch);
	}

	function setForAdmissionPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT INTO tbl_payment_refund (
			Payment_Refund_Id,
			Payment_Refund_Txnid,
			Payment_Refund_Amount,
			Payment_Refund_Status,
			Payment_Refund_ProdInfo,
			Payment_Refund_ITGK,
			Payment_Refund_RKCL_Txid,
			Payment_Refund_Course,
			Payment_Refund_Batch
		) 
		SELECT CASE WHEN MAX(Payment_Refund_Id) Is NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
		'" . $transactionId . "' AS Payment_Refund_Txnid,
		'" . $amount . "' AS Payment_Refund_Amount,
		'" . $status . "' AS Payment_Refund_Status,
		'" . $productInfo . "' AS Payment_Refund_ProdInfo,
		'" . $centerCode . "' AS Payment_Refund_ITGK,
		'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid,
		'" . $course . "' AS Payment_Refund_Course,
		'" . $batch . "' AS Payment_Refund_Batch 
		FROM tbl_payment_refund";

		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "' AND Payment_Refund_Course = '" . $course . "' AND Payment_Refund_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

/**
*	Functions of Learner Fee Transactions End
*/

/**
*	Functions for Correction Fee Transactions Start
*/
	function setSuccessForCorrectionFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy) {
		$refund = checkIfRefundRequiredForCorrectionFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount);
		} else {
			processToRefundCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount);
		}
	}

	function processToConfirmCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount) {
		$_ObjConnection = dbConnection();
		updateCorrectionPaymentStatus(1, $transactionId, $centerCode);
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if(! mysqli_num_rows($response[2])) {
			insertCorrectionTransaction('Success', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId);
		} else {
			updateCorrectionTransactionStatus('Success', $amount, $transactionId, $centerCode);
		}
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
	}

	function updateCorrectionPaymentStatus($paymentstatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_correction_copy WHERE Correction_Payment_Status = " . $paymentstatus . " AND Correction_ITGK_Code = '" . $centerCode . "'  AND Correction_TranRefNo = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "UPDATE tbl_correction_copy SET Correction_Payment_Status = '" . $paymentstatus . "' WHERE Correction_ITGK_Code = '" . $centerCode . "'  AND Correction_TranRefNo = '" . $transactionId . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}

	function insertCorrectionTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Status = '" . $transactionStatus . "' AND Correction_Transaction_Amount = '" . $amount . "' AND Correction_Transaction_Txtid = '" . $transactionId . "' AND  Correction_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_correction_transaction (
				Correction_Transaction_Code,
				Correction_Transaction_Status,
				Correction_Transaction_Fname,
				Correction_Transaction_Amount,
				Correction_Transaction_Txtid,
				Correction_Transaction_ProdInfo,
				Correction_Transaction_CenterCode,
				Correction_Transaction_RKCL_Txid,
				Correction_Transaction_DateTime,
				Correction_Transaction_Month,
				Correction_Transaction_Year
			) 
			SELECT CASE WHEN MAX(Correction_Transaction_Code) Is NULL THEN 1 ELSE MAX(Correction_Transaction_Code) + 1 END AS Correction_Transaction_Code,
			'" . $transactionStatus . "' AS Correction_Transaction_Status,
			'" . $firstName . "' AS Correction_Transaction_Fname,
			'" . $amount . "' AS Correction_Transaction_Amount,
			'" . $transactionId . "' AS Correction_Transaction_Txtid,
			'" . $productInfo . "' AS Correction_Transaction_ProdInfo,
			'" . $centerCode . "' AS Correction_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Correction_Transaction_RKCL_Txid,
			'" . date("Y-m-d H:i:s") . "' AS Correction_Transaction_DateTime,
			" . date("m") . " AS Correction_Transaction_Month,
			" . date("Y") . " AS Correction_Transaction_Year 
			FROM tbl_correction_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateCorrectionTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Status = '" . $transactionStatus . "' AND Correction_Transaction_Amount = '" . $amount . "' AND Correction_Transaction_Txtid = '" . $transactionId . "' AND  Correction_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_correction_transaction SET 
				Correction_Transaction_Status = '" . $transactionStatus . "',
				Correction_Transaction_Amount='" . $amount . "'
			WHERE 
				Correction_Transaction_Txtid = '" . $transactionId . "' AND 
				Correction_Transaction_CenterCode='" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}

	function checkIfRefundRequiredForCorrectionFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(Correction_TranRefNo) AS CorrectionTxnid FROM tbl_correction_copy WHERE Correction_TranRefNo = '" . $transactionId . "' AND Correction_ITGK_Code = '" . $centerCode . "' GROUP BY Correction_TranRefNo";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['CorrectionTxnid'];
				$correctionAmount = $payTxnid * 200;
				if ($correctionAmount != $amount) {
					$refund = true;
				}
			} else {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount) {
		$_ObjConnection = dbConnection();
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertCorrectionTransaction('Refund', $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId);
		} else {
			updateCorrectionTransactionStatus('Refund', $amount, $transactionId, $centerCode);
		}
		updateCorrectionPaymentStatus(0, $transactionId, $centerCode);
	}

	function performReconcilation() {
		$_ObjConnection = dbConnection();
		$timestamp = mktime(date("H"), date("i") - 40, date("s"), date("m"), date("d"), date("Y"));
		$time = date("Y-m-d H:i:s", $timestamp);
		echo $selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status IN ('PaymentInProcess', 'PaymentFailure') AND Pay_Tran_Reconcile_status = 'Not Reconciled' AND (Pay_Tran_ProdInfo LIKE ('%exam%') OR Pay_Tran_ProdInfo LIKE ('%learner%') OR Pay_Tran_ProdInfo LIKE ('%Correction%') OR Pay_Tran_ProdInfo LIKE ('%Duplicate%')) AND timestamp <= '" . $time . "' ORDER BY Pay_Tran_Code DESC ";
		//echo $selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = 'f85eda23d450fd026ec1'";
		
		//die;
		$result = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);
		$response = [];
		if (mysqli_num_rows($result[2])) {
			while ($data = mysqli_fetch_array($result[2])) {
				$response = setTransactionResponse($data);
				$response = getTransactionStatusByPayU($response);
				processResponse($response);
			}
		}
	}

/**
*	Functions of Correction Fee Transactions End
*/

?>