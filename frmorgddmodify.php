<?php
$title = "Modify NCR DD Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Update'</script>";
}
$random = (mt_rand(1000, 9999));
$random.= date("y");
$random.= date("m");
$random.= date("d");
$random.= date("H");
$random.= date("i");
$random.= date("s");

echo "<script>var OrgDocId= '" . $random . "' </script>";
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Modify NCR DD Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="DDForm" id="DDForm" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="ackno">Acknowledgement Number:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="18" onkeypress="javascript:return allownumbers(event);" name="ackno" id="ackno" placeholder="Acknowledgement Number">
                            <input type="hidden" class="form-control" name="txtGenerateId" id="txtGenerateId"/>
                            <input type="hidden" class="form-control" name="txnid" id="txnid"/>
                        </div>

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                        </div>
                    </div>


                    <div id="main-content" style="display: none;">
                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">DD No.:</label>
                                <input type="text" class="form-control" name="ddno" id="ddno" value="" placeholder="DD Number" maxlength="6" onkeypress="javascript:return allownumbers(event);"/>

                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">DD Date:</label>                                
                                <input type="text" class="form-control" name="dddate" readonly="true" id="dddate" placeholder="YYYY-MM-DD">  
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">DD Amount.:</label>
                                <input type="text" class="form-control" name="ddamount" id="ddamount" value="" readonly=""/>

                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict">Employee Bank District:<span class="star">*</span></label>
                                <select id="ddlBankDistrict" name="ddlBankDistrict" class="form-control">

                                </select>
                            </div>



                        </div> 
                        <br>
                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="bankname">Employee Bank Name:<span class="star">*</span></label>
                                <select id="ddlBankName" name="ddlBankName" class="form-control">
                                    <option value="" selected="selected">Please Select</option>                
                                </select>                                                              
                            </div> 

                            <div class="col-sm-4 form-group"> 
                                <label for="branchname">Branch_Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" onkeypress="javascript:return allowchar(event);"  name="txtBranchName" id="txtBranchName"  placeholder="EMP Branch Name">
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="ddreceipt">Upload DD Image<span class="star">*</span></label>
                                <input type="file" class="form-control"  name="ddreceipt" id="ddreceipt" onchange="checkScanForm(this)">
                            </div>    

                        </div>   
                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
                    </div>
                </form>

            </div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script src="scripts/ddncrupload.js"></script>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
<script type="text/javascript">
                                    $('#dddate').datepicker({
                                        format: "yyyy-mm-dd"
                                    });
</script>
<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#orgdocproof').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("orgdocproof").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("orgdocproof").value = '';
            return false;
        }
        else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("orgdocproof").value = '';
            return false;
        }
        document.getElementById("orgdocproof").innerHTML = "";
        return true;
    }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $('#txtGenerateId').val(OrgDocId);
//        $('#transaction').val(PayUTranID);


        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
//                    transaction.value = data;
                }
            });
        }
        //GenerateUploadId();

        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        FillBankDistrict();

        $("#ddlBankDistrict").change(function () {
            FillBankName(this.value);
        });
        function FillBankName(districtid) {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKNAME&districtid=" + districtid,
                success: function (data) {
                    $("#ddlBankName").html(data);
                }
            });
        }




        $(function () {
            $("#dddate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });


        $("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfOrgDDModify.php",
                data: "action=DETAILS&values=" + ackno.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Acknowledgement Number" + "</span></p>");
                    }
                    else {
                        data = $.parseJSON(data);
                        ddno.value = data[0].ddno;
                        dddate.value = data[0].dddate;
                        txtBranchName.value = data[0].ddbranch;
                        txnid.value = data[0].ddtxnid;
                        ddamount.value = data[0].ddamount;

                        $("#main-content").show(3000);
                        $('#ackno').attr('readonly', true);
                        $("#btnShow").hide();

                    }

                }
            });
        });

        $("#btnSubmit").click(function () {
            if ($("#DDForm").valid())
            {


                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfOrgDDModify.php"; // the script where you handle the form input.
                //var filename = $('#txtGenerateId').val();
                var data;
                // alert(Mode);
                var forminput = $("#DDForm").serialize();
                if (Mode == 'Update')
                {

                    data = "action=Update&" + forminput; // serializes the form's elements.

                    //alert(data);
                }
                else
                {

                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmorgddmodify.php";
                            }, 2000);

                            Mode = "Add";
                            resetForm("frmorgddmodify");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }

                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmddadmissionupload_validation.js"></script>


</html>