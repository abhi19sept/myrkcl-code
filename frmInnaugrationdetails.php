<?php
$title="Inauguration Details";
include ('header.php'); 
include ('root_menu.php'); 
//echo $_SESSION['User_UserRoll'];
if (isset($_REQUEST['code'])) 
{
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} 
else 
{
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_UserRoll'] == '7') 
{?>
			
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;" >
        <div class="container"> 
			<div class="panel panel-primary" style="margin-top:46px !important;">

                <div class="panel-heading">Inauguration Details </div>
                <div class="panel-body">
                    
                    <form name="frmInnaugrationdetails" id="frmInnaugrationdetails"  class="form-inline">     
						
                       
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
							
							
							
							
							<div id="search">
                            <div class="col-md-4 form-group"> 
                                <label for="course"> Course Name <span class="star">*</span></label>
                              
								<select id="ddlCourse" name="ddlCourse" class="form-control" required >
                                  
                                </select>
								
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch <span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control" required >
                                  
                                </select>
                            </div> 
							
							
							<div class="col-sm-4 form-group">     
								
								<input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show" style="margin-top:25px"/>    
							</div>
								
							
								

                            </div>
							
						<div   id="details" style="display:none">
							<div class="container">
							<div class="row">
								<div class="col-sm-4 form-group">     
									<label for="learnercode">Name of Public Representative <font color="red">*</font></label>
									<input type="text" class="form-control" maxlength="50" name="txtName" id="txtName" placeholder="The name of the people's Representative" >
									
									
								</div>
								<div class="col-sm-4 form-group"> 
									<label for="edistrict">Designation <font color="red">*</font></label>
									<select id="txtdesig" name="txtdesig" class="form-control" >
									  
									</select>    
								</div>
							
								<div class="col-sm-4">     
									<label for="address">Area Details of Public Representative <font color="red">*</font></label>
									
									<input type="text" class="form-control" name="txtarea" 
										id="txtarea" placeholder="Details of people's Representative"  >

								</div>
								
								<div class="col-sm-4 form-group">     
									<label for="faname">Inauguration  / Closure Date <font color="red">*</font></label>
									<input type="text" class="form-control" maxlength="50" name="txtenddate" id="txtenddate"  placeholder="DD-MM-YY" readonly='true'>
									
								</div>
							</div>
							
							
							<div class="row">
								


								<div class="col-sm-4 form-group">     
									<label for="address">Other present officers <font color="red"></font></label>
									<input type="text" class="form-control" name="txtotherofficer" 
										id="txtotherofficer" placeholder="Other Attendees"  >
								</div>
								
								<div class="col-sm-4 form-group"> 
									<label for="payreceipt">Upload Photo1 <font color="red">*</font></label>
									<input type="file" class="form-control"  name="image1" id="image1" onchange="Getimage1(this)">
									<span style="font-size:10px;"> Note : JPG,PNG Allowed Max Size =80-100 KB each</span>
								</div>
								
								
								<div class="col-sm-4 form-group"> 
									<label for="payreceipt">Upload Photo2 <font color="red">*</font></label>
									<input type="file" class="form-control"  name="image2" id="image2" onchange="Getimage2(this)">
									<span style="font-size:10px;"> Note : JPG,PNG Allowed Max Size =80-100 KB each</span>
								</div>
															
								
								<div class="col-sm-4 form-group"> 
									<input style="margin-top:25px !important;" type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload Photo"/>   
								</div>
							</div>
							
							
                        </div>  
							
							
						
							<div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style='display:none;' />

									
                             </div>
						
							
						 </div>
						 <div class="row">
						  <div class="col-xs-12">
						 <div id="gird" ></div>
						 </div>
						 </div>
						</form>	
				   </div>

				</div>   
            </div>
			
		</div>


</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
 
</style>
  

<script src="scripts/uploaddocuments.js"></script>

<script language="javascript" type="text/javascript">


function Getimage1(target) {
	var ext = $('#image1').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("image1").value = '';
			return false;
		}

	if(target.files[0].size > 100000) {			        
		alert("Image size should less or equal 100 KB");
		document.getElementById("image1").value = '';
        return false;
    }
	else if(target.files[0].size < 80000) {
				alert("Image size should be greater than 80 KB");
				document.getElementById("image1").value = '';
				return false;
	}    
    document.getElementById("image1").innerHTML = "";
    return true;
}





function Getimage2(target) {
	var ext = $('#image2').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("image2").value = '';
			return false;
		}

	if(target.files[0].size > 100000) {			        
		alert("Image size should less or equal 100 KB");
		document.getElementById("image2").value = '';
        return false;
    }
	else if(target.files[0].size < 80000) {
				alert("Image size should be greater than 80 KB");
				document.getElementById("image2").value = '';
				return false;
	}    
    document.getElementById("image2").innerHTML = "";
    return true;
}

</script>

	
      
<script type="text/javascript">
var today = new Date();
var lastDate = new Date(today.getFullYear(), today.getMonth(0)-1, 31);
var startDate = new Date('2018-09-04');
var ToEndDate = new Date('2018-12-20');
//ToEndDate.setDate(ToEndDate.getDate() + 365);
$('#txtenddate').datepicker({                   
	format: "yyyy-mm-dd",
	orientation: "bottom auto",
	todayHighlight: true,
	autoclose: true,
	minDate: 0,
	startDate: startDate,
    endDate: ToEndDate
});  
</script>
 <script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
					
						$("#btnUpload").click(function () 
						{
							$('#btnSubmit').show(1000);
						});

						/*for Course  function */
						function FillCourse() {
							//alert("hello");
							$.ajax({
								type: "post",
								url: "common/cfInnaugrationdetails.php",
								data: "action=FILLCOURSE",
								success: function (data) {
									//alert(data);
									
									$("#ddlCourse").html(data);
								}
							});
						}
						FillCourse();
						
						/*for Batch function */
						$("#ddlCourse").change(function () 
						{
							//alert("hello");
							var selCourse = $(this).val();
							$.ajax({
								type: "post",
								url: "common/cfInnaugrationdetails.php",
								data: "action=FILLBATCH&values=" + selCourse + "",
								success: function (data) {
									
									//alert(data);
									$("#ddlBatch").html(data);
								}
							});
						
						});

						
							
						/*for Get Itgk details function */
						$("#btnShow").click(function () 
						{
							
							if ($("#frmInnaugrationdetails").valid())
							{
							    //alert(ddlBatch.value);
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                               var url = "common/cfInnaugrationdetails.php"; // the script where you handle the form input.
								var data;
								var forminput=$("#frmInnaugrationdetails").serialize();
								
									data = "action=FILLDETAILS&" +forminput; // serializes the form's elements.
								
                                    $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
										$('#response').empty();
										if (data == 1) 
										{
											$('#response').empty();
                                            //alert(data);
											$("#details").show(1000);
											$("#search").hide();
											showData();										
										}										
										else
										{
											$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
											$("#details").hide();
										}	

                                    }
                                });
							}
                                return false; // avoid to execute the actual submit of the form.
						});
									
				   		/*for show funtion */			
						function showData()
						{
							
							$.ajax({
								type: "post",
								url: "common/cfInnaugrationdetails.php",
								data: "action=SHOW",
								success: function (data) {
										
									$("#gird").html(data);
									$('#example').DataTable({
										dom: 'Bfrtip',
										buttons: [
											'copy', 'csv', 'excel', 'pdf', 'print'
										]
									});
			
								}
						  });
						}
						showData();
		
						/*for Designation funtion */
						function FillDesinationname() 
						{
							$.ajax({
								type: "post",
								url: "common/cfInnaugrationdetails.php",
								data: "action=FILLDESIGNATION",
								success: function (data) {
								$("#txtdesig").html(data);
								}
							});
						}
						FillDesinationname();	
		
						/*for add funtion */
						$("#btnSubmit").click(function () 
						{
						
						if ($("#frmInnaugrationdetails").valid())
						{
							
							$('#response').empty();
							$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
							var url = "common/cfInnaugrationdetails.php"; // the script where you handle the form input.
							var data;
							var forminput=$("#frmInnaugrationdetails").serialize();
							if (Mode == 'Add')
							{
								//alert(1);
								 data = "action=ADD&" +forminput; // serializes the form's elements.
							}
							else 
							{
								//alert(2);
								data = "action=UPDATE&code=" + Code +"&" + forminput;
								//data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
							}
							$.ajax({
								type: "POST",
								url: url,
								data: data,
								success: function (data)
								{
									if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
										 window.setTimeout(function () {
											window.location.href = "frmInnaugrationdetails.php";
										}, 1000);
										Mode = "Add";
										resetForm("frmInnaugrationdetails");
										
									}
									else
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
									}
									//showData();


								}
							});
						 }
							return false; // avoid to execute the actual submit of the form.
						});

                            
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmInnaugrationdetailsvalidation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>