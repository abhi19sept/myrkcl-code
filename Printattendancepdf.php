<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$title = "Print Attendance sheet";
include ('header.php');

    ini_set('display_errors', 1);
    error_reporting(E_ALL);
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
 
set_time_limit(0);

//include 'common/commonFunction.php';
require_once("common/cfattendancepdf.php");

require_once 'mpdf60/mpdf.php';
		$data = array();

		$center3 = [];
		$center = [];
		$requestArgs = $_REQUEST;

		$examEventId = (!empty($_REQUEST["code"])) ? $_REQUEST["code"] : 1219;
		$requestArgs['code'] = $examEventId;

		$centers = getCenters($requestArgs);
		
		if(! mysqli_num_rows($centers)) {
			die('No Record Found!!');
		}
		
		//print_r($center3); die;
		$dirPath = $_SERVER['DOCUMENT_ROOT'] . '/upload/attendancesheet';
		$dirPathBkp = $_SERVER['DOCUMENT_ROOT'] . '/upload/attendancesheet_' . date("d-m-Y_h_i_s_a") . '_bkp';
		//rename($dirPath, $dirPathBkp);

		makeDir($dirPath);

		$skipCodes = [];
		
		while($centerRow = mysqli_fetch_array($centers)) {
	
			$dirPath = $_SERVER['DOCUMENT_ROOT'] . '/upload/attendancesheet/' . $centerRow['examcenterdistrict'] .'/';
			$path = $dirPath . $centerRow['examcentercode'] . '_attendancesheet.pdf';

			/*if (file_exists($path) || in_array($centerRow['examcentercode'], $skipCodes)) {
				continue;
			}*/

			$data = getdata($examEventId, $centerRow['examcentercode']);
		
			//print_r($_REQUEST["centercode"]);
			
			$_Count = 1;
			
			$html='<center><TABLE cellpadding=0 cellspacing=0 class="t0" border="1" style="margin-left:3px; " width="1000" >'
			;
			
			while($row= mysqli_fetch_array($data))
				
			{
				
			$photoPath = getPath('photo', $row);
			
			//print_r($row['centercode'].',');
	$html.='<TR  style=" ">
	<td style="padding:10px;font-size:15px;" class="tr0 td0"  align="center" width="100" height="60"><P class="p4 ft1" align="center" style="font-size:17px;"> '.$_Count.' </P></td>
	<td style="padding:10px;font-size:15px;" class="tr0 td1" width="280" height="70" align="left"><P class="p5 ft2" align="center" style="font-size:15px;">'.$row['learnercode'].'<br>'.$row['learnername'].'<br>
	'.$row['fathername'].'</P></td>
	<td style="padding:10px;font-size:15px;" class="tr0 td1" align="center" width="100" height="60"><P class="p5 ft2" align="center" style="font-size:17px;">'.$row['rollno'].'</P></td>
	<td style="padding:10px;font-size:17px;" class="tr0 td1"  align="center" width="130" height="60"><P class="p5 ft2" align="center"></P></td>
	<td style="padding:10px;font-size:17px;"  class="tr0 td1" align="center" width="130" height="60"><P class="p5 ft2" align="center" style="font-size:17px;"></P></td>
	<td  style="padding:10px;font-size:15px;" class="tr0 td1" width="130" height="75"><P class="p5 ft2" align="center"><IMG src="'.$photoPath.'"   style="width:130px;height:75px;text-align:center">
    </P></td>
	<td style="padding:10px;font-size:15px;" class="tr0 td1" align="center" width="140" height="75"><P class="p5 ft2" align="center">
     </P></td></TR>';
	 $centername=$row['examcentername'];
	 $centeraddress=$row['examcenteraddress'];
	 $centerdistrict=$centerRow['examcenterdistrict'];
	 $centertehsil=$row['examcentertehsil'];
			$_Count++;
	
		}
		
		$html.='</TABLE></center>';
		
			
			
			
			
			
			//echo $head;
           //echo $html;
		   //die();
		   ///genrate($html1);
		  $dirPath = $_SERVER['DOCUMENT_ROOT'] . '/upload/attendancesheet/' . $centerdistrict .'/';
		  makeDir($dirPath);
		  $path = $dirPath . $centerRow['examcentercode'] . '_attendancesheet.pdf';

		   $mpdf=new mPDF('win-1252', 'A4', '', '', 15, 10, 76, 5, 10, 5); 
			$mpdf->SetHeader('<DIV id="page_1"  style="background-image: url(images/422900248x2.jpg);width:1000px;border:1px solid #333;">
<DIV id="dimg1">
</DIV>
<TABLE cellpadding=0 cellspacing=0 class="t0"  style="margin-top:30px" width="700">
<TR>
<Th class="tr0 td0"  style="margin-left:100px !important;" ><p style="font-size:15px">VARDHMAN MAHAVEER OPEN UNIVERSITY, KOTA</p>
</Th>
</TR>
<TR>
<Th class="tr0 td0"  style="margin-left:100px !important;"><p><NOBR>RS-CIT Examination ' . $centerRow['eventname'] . '</p>
</Th>

</tr>
<TR>
<Th class="tr0 td0"  style="margin-left:100px !important;"><p><NOBR>Attendance Sheet</p>
</Th>

</tr>
</TABLE>
<TABLE cellpadding=0 cellspacing=0 class="t0" style="margin-top:30px;" width="1000">
<TR>
	<Th class="tr0 td0" align="left" style="padding:10px;" colspan="3" ><p>
	Exam Center Code and Name: '. $centerRow['examcentercode'] .',
	'. $centername.'
	</p>
	</Th>
	
	<Th class="tr0 td0" style="padding:10px;"  align="left" colspan="5">
	</Th>
</TR>
<TR>
	<Th class="tr0 td0" style="padding:10px;"  align="left" colspan="3" >
	<p>
	Exam Center Address: '. $centeraddress.'
	</p>
	
	</Th>
	
	<Th class="tr0 td0" style="padding:10px;"  align="left" colspan="5">
	<p>

	
	</p>
	
	</Th>
</TR>
<TR>
	<Th class="tr0 td0" style="padding:10px;" align="left" colspan="3">
	<p>
	Exam District and Tehsil:'. $centerdistrict.','. $centertehsil.'
	</p>
	</Th>
	<Th style="padding:10px;"  class="tr0 td0" colspan="5">
	<p>
	</p>
</TR>
<TR>
	<Th style="padding:10px;"  class="tr0 td0" align="left" colspan="3">
	<p>
	Exam Date & Time:  ' . $centerRow['examdate'] . ' Sunday, ' . $centerRow['examtime'] . '  
	</p>
	</Th>
	
	<Th style="padding:10px;"  class="tr0 td0" colspan="5">
	<p>
	ROOM / HALL …………………… 
	</p>
	</Th>
	
	
</TR>
</table>
<TABLE cellpadding=0 cellspacing=0 class="t0" border="1"  width="1000">
<TR>
	<Th class="tr0 td0"  width="100"><P class="p4 ft1">Sr. No. </P></Th>
	<Th class="tr0 td1"  width="280"><P class="p5 ft2">Learner Code<br>Learner Name<br>Father / Husband Name</P></Th>
	<Th class="tr0 td1"  width="100"><P class="p5 ft2">ROLL No.</P></Th>
	<Th class="tr0 td1"  width="130"><P class="p5 ft2">OMR Sheet No.</P></Th>
	<Th class="tr0 td1"  width="130"><P class="p5 ft2">Question Booklet </P></Th>
	<Th class="tr0 td1"  width="150"><P class="p5 ft2">Candidate   Photo</P></Th>
	<Th class="tr0 td1"  width="140"><P class="p5 ft2">Candidates Signature</P></Th>
</TR>
</table>
</DIV></DIV>');
$mpdf->SetFooter( '<table style="border:none;" ><tr>
<td width="300" style="font-size:15px;">Invigilator Signature
</td>
<td width="300" style="font-size:15px;">Seal of the Examination Center
</td>
<td width="300" style="font-size:15px;">Signature of Center Superintendent</td>
</tr></table><div align="right"><b>Page:{PAGENO} of {nbpg}</b></div>'	);

	$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');
	//$html = iconv("UTF-8", "UTF-8//IGNORE", $html);
	
				$mpdf->SetDisplayMode('fullpage');

				$mpdf->list_indent_first_level = 0; 

				$mpdf->WriteHTML($stylesheet,1);
				$mpdf->WriteHTML($html, 2);
				$mpdf->Output($path, 'F');  
				 
			
	}
	
	
function getPath($type, $row) {
	$batchname = trim(ucwords($row['batchname']));
    $coursename = $row['coursename'];
    $isGovtEmp = stripos($coursename, 'gov');
    if ($isGovtEmp) {
        $batchname = $batchname . '_Gov';
    } else {
        $isWoman = stripos($coursename, 'women');
        if ($isWoman) {
            $batchname = $batchname . '_Women';
        } else {
            $isMadarsa = stripos($coursename, 'madar');
            if ($isMadarsa) {
                $batchname = $batchname . '_Madarsa';
            }
        }
    }

    $batchname = str_replace(' ', '_', $batchname);
    $dirPath = '/upload/admission_' . $type . '/' . $row['learnercode'] . '_' . $type . '.png';
    $imgPath = $_SERVER['DOCUMENT_ROOT'] . $dirPath;
	$imgPath = str_replace('//', '/', $imgPath);

    $dirPathJpg = str_replace('.png', '.jpg', $dirPath);
    $imgPathJpg = str_replace('.png', '.jpg', $imgPath);

    $imgBatchPath = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_' . $type . '/' . $batchname . '/' . $row['learnercode'] . '_' . $type . '.png';
	$imgBatchPath = str_replace('//', '/', $imgBatchPath);
    $imgBatchPathJpg = str_replace('.png', '.jpg', $imgBatchPath);

    $path = '';
    if (file_exists($imgPath)) {
    	$path = $imgPath;
    } elseif (file_exists($imgPathJpg)) {
    	$path = $imgPathJpg;
    } elseif (file_exists($imgBatchPath)) {
    	$path = $imgBatchPath;
    } elseif (file_exists($imgBatchPathJpg)) {
    	$path = $imgBatchPathJpg;
    } else {
    	$imgPath = 'http://' . $_SERVER['HTTP_HOST'] . $dirPath;
    	if(@getimagesize($imgPath)) {
    		$path = $imgPath;
    	} else {
    		$path = 'http://' . $_SERVER['HTTP_HOST'] . $dirPathJpg;
    	}
    }

    return $path;
}



function makeDir($path)
{
     return is_dir($path) || mkdir($path);
}


?>
