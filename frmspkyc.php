<?php
$title = "SP KYC";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '14') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
echo "<script>var UserRoll='" . $_SESSION['User_UserRoll'] . "'</script>";
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading"> SP KYC

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
			<form name="frmspkyc" id="frmspkyc" class="form-inline" action="">
				
				<div class="row">
                    <div class="col-md-2 form-group">     
                        <label for="visit"> Select KYC Type:<span class="star">*</span></label>
                    </div>
                    <div class="col-md-6 form-group"> 
                        <select id="txtKycType" name="txtKycType" class="form-control" required>
							<option value=""> Please Select  </option>
							<option value="1">  Details of Directors / Partner / Proprietor / Management member of Trust / Society with
												their shareholding  </option>
							<option value="2"> Disclosure of interest of Directors / Partners/ Governing Board members and their 
												relatives with relation in other Service Provider/ ITGK  </option>
							<option value="3"> Details of all SP owned ITGKs </option>
							<option value="4"> Details of Authorized person/ Officer for every district </option>
							<option value="5"> Office details for RKCL communication  </option>
						</select>
                        
                    </div>
				</div>
                  
			</form>
				
				<div id="form1" style="display:none;">
					<form id="spkyc_form_1" name="spkyc_form_1"  method="post" >
						
					<div class="row">
						<div class="col-md-2 form-group">     
							<label for="visit"> Select Company Type:<span class="star">*</span></label>
						</div>
						<div class="col-md-6 form-group"> 
							<select id="txtType" name="txtType" class="form-control" required="required"> </select>
							<!--</div>-->
						</div>
					</div>
					
					<div id="typechange" style="display:none;">
					
						<fieldset style="border: 1px solid #eee !important;padding: 15px;" >
						<div class="row">
							<div class="col-sm-3 form-group"> 
								<label for="name">Enter Name:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm1name1" id="txtfrm1name1" placeholder="Enter Name" onkeypress="javascript:return allowchar(event);"
								autocomplete="off"	required/> 
								<input type="hidden" id="countuser" name="countuser" value="1" >
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="Designation">Enter Designation:<span class="star">*</span></label>
							<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm1designation1" id="txtfrm1designation1"
									placeholder="Enter Designation" onkeypress="javascript:return allowchar(event);"
									autocomplete="off" required/>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="Address">Enter Address:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="250" name="txtfrm1address1" id="txtfrm1address1"
									placeholder="Enter Address" onkeypress="javascript:return validAddress(event);"
									autocomplete="off" required/>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="Address">Date of Appointment:<span class="star">*</span></label>
								<input type="text" readonly="true" class="form-control appointment" maxlength="100" name="txtfrm1doa1" id="txtfrm1doa1"
									placeholder="Date of Appointment" autocomplete="off" required/>     
							</div>
							
							
						</div>
						<div class="row">
							<div class="col-sm-3 form-group"> 
								<label for="email">Enter Email:<span class="star">*</span></label>
								<input type="text" class="form-control" maxlength="100" name="txtfrm1email1" id="txtfrm1email1"
									placeholder="Enter Email" autocomplete="off" required/>     
							</div>
							 <div class="col-sm-3 form-group text-center" style="margin-top:20px;">
									<div id="veremaildiv1" class="btn btn-primary"><a style="cursor:pointer;color: #ffffff;width: 140px;" type="button" id="1" name="otpemailSubmitlink" class="otpemailSubmitlink">Verify Email Id</a></div>
									<div id="sendemailotpres1" style="float:right;margin: 5px 58px 0 0;"></div>
									<input type="hidden" id="verfiyEmailSet1" name="verfiyEmailSet1" value="" >
                              </div>
							<div class="col-sm-3 form-group"> 
								<label for="mobile">Enter Mobile:<span class="star">*</span></label>
								<input type="text" class="form-control phoneNumberclass" maxlength="10" name="txtfrm1mobile1" id="txtfrm1mobile1"
									placeholder="Enter Mobile" autocomplete="off" onkeypress="javascript:return allownumbers(event);"
									minlength="10" required/>     
							</div>
							  <div class="col-sm-3 form-group text-center" style="margin-top:20px;">
									<div id="verdiv1" class="btn btn-primary"><a style="cursor:pointer;color: #ffffff;width: 140px;" type="button" id="1" name="otpSubmitlink" class="otpSubmitlink">Verify Mobile No.</a></div>
									<div id="sendotpres1" style="float:right;margin: 5px 58px 0 0;"></div>
									<input type="hidden" id="verfiyMobileSet1" name="verfiyMobileSet1" value="" >
                              </div>
						</div>
						<div class="row">
							<div class="col-sm-3 form-group"> 
								<label for="Shareholding">Enter Shareholding:<span class="star">*</span></label>
							<input type="text" class="form-control" maxlength="10" name="txtfrm1Shareholding1" id="txtfrm1Shareholding1"
									placeholder="Enter Shareholding" autocomplete="off" required/>     
							</div>						
                   
							<div class="col-sm-3 form-group"> 
								<label for="pan">Enter PAN:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" pattern="^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$"
								maxlength="10" name="txtfrm1pan1"
									id="txtfrm1pan1" placeholder="Enter PAN" autocomplete="off" required/>  
								
                                <span style="font-size:10px;">Note : PAN No should be 10 digit. Starting 5 should be only alphabets[A-Z] then Remaining 4 should be accepting only alphanumeric and last one is again alphabets[A-Z] .</span>
							</div>
							
						</div>
						
						
						</fieldset>
					</div>
							<script>
                                    $(document).ready(function () {
                                        var max_fields = 100; //maximum input boxes allowed
                                        var wrapper = $("#typechange"); //Fields wrapper
                                        var add_button = $("#addstudent"); //Add button ID

                                        var x = 1; //initlal text box count
                                        $(add_button).click(function (e) { 
                                            e.preventDefault();
                                            if (x == 100) {
                                                alert('You Can Add 100 KYC Details At A Time');
                                            }
                                            if (x < max_fields) { //max input box allowed
                                                x++; //text box increment
												$('#countuser').val(x);
                                                $(wrapper).append('<br><div class="panel panel-default" id="remove' + x + '"><div class="panel-heading" role="tab" id="headingOne"><h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#' + x + '" aria-expanded="false" aria-controls="collapseOne" class="collapsed">Click Here To Minimize Or Maximize <span class="glyphicon glyphicon-circle-arrow-down"></span></a><a href="#" id="move' + x + '" class="remove_field_sub_topic" style="float: right;"><span class="glyphicon glyphicon-trash"></span></a></h4></div><div id="' + x + '" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false"><div class="panel-body"><div class="col-sm-3"><label for="Enter NAME">Enter NAME</label><div class="form-group"><input type="text" onkeypress="javascript:return allowchar(event);" class="form-control text-uppercase" id="txtfrm1name' + x + '" name="txtfrm1name' + x + '" value="" placeholder="Enter NAME" autocomplete="off" required /></div></div><div class="col-sm-3"><label for="Enter Designation">Enter Designation</label><div class="form-group"><input type="text"  class="form-control text-uppercase" id="txtfrm1designation' + x + '" autocomplete="off" name="txtfrm1designation' + x + '" value="" placeholder="Enter Designation" onkeypress="javascript:return allowchar(event);" required /></div></div><div class="col-sm-3"><label for="Enter Address">Enter Address</label><div class="form-group"><input type="text" class="form-control text-uppercase" id="txtfrm1address' + x + '" name="txtfrm1address' + x + '" maxlength="500" autocomplete="off" value="" placeholder="Enter Address" onkeypress="javascript:return validAddress(event);" required /></div></div><div class="col-sm-3"><label for="Date of Appointment">Date of Appointment</label><div class="form-group"><input type="text" autocomplete="off" class="form-control appointment" id="txtfrm1doa' + x + '" name="txtfrm1doa' + x + '" readonly="true" value="" placeholder="Date of Appointment" required /></div></div><div class="row"><div class="col-sm-3"><label for="Enter Email">Enter Email</label><div class="form-group"><input type="text" class="form-control" id="txtfrm1email' + x + '" autocomplete="off" name="txtfrm1email' + x + '" value="" placeholder="Enter Email" required /></div></div><div class="col-sm-3 text-center" style="margin-top:20px;"><div class="btn btn-primary" id="veremaildiv' + x + '"><a style="cursor:pointer;color:white;width: 140px;" type="button" id="' + x + '" name="otpemailSubmitlink" class="otpemailSubmitlink">Verify Email Id</a></div><div id="sendemailotpres' + x + '" style="float:right;margin: 5px 58px 0 0;"></div><input type="hidden" id="verfiyEmailSet' + x + '" name="verfiyEmailSet' + x + '" value="" ></div><div class="col-sm-3"><label for="Enter Mobile">Enter Mobile</label><div class="form-group"><input type="text" class="form-control phoneNumberclass" id="txtfrm1mobile' + x + '" name="txtfrm1mobile' + x + '" minlength="10" autocomplete="off" value="" placeholder="Enter Mobile" onkeypress="javascript:return allownumbers(event);" maxlength="10" required /></div></div><div class="col-sm-3 text-center" style="margin-top:20px;"><div class="btn btn-primary" id="verdiv' + x + '"><a style="cursor:pointer;color:white;width: 140px;" type="button" id="' + x + '" name="otpSubmitlink" class="otpSubmitlink">Verify Mobile No.</a></div><div id="sendotpres' + x + '" style="float:right;margin: 30px 165px 0 0;"></div><input type="hidden" id="verfiyMobileSet' + x + '" name="verfiyMobileSet' + x + '" value="" ></div></div><div class="col-sm-3"><label for="Enter Shareholding">Enter Shareholding</label><div class="form-group"><input type="text" class="form-control" id="txtfrm1Shareholding' + x + '" name="txtfrm1Shareholding' + x + '" value="" maxlength="12" autocomplete="off" placeholder="Enter Shareholding" required /></div></div><div class="col-sm-3"><label for="Enter PAN">Enter PAN</label><div class="form-group"><input type="text" class="form-control text-uppercase" id="txtfrm1pan' + x + '" name="txtfrm1pan' + x + '" pattern="^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$" value="" maxlength="10" autocomplete="off" placeholder="Enter PAN" required /><span style="font-size:10px;">Note : PAN No should be 10 digit. Starting 5 should be only alphabets[A-Z] then Remaining 4 should be accepting only alphanumeric and last one is again alphabets[A-Z] .</span></div></div></div></div></div>'); //add input box 
$('.appointment').datepicker({
			format: "yyyy-mm-dd",
			orientation: "bottom auto",
			todayHighlight: true
		});												
                                            }
                                        });

                                        $(wrapper).on("click", ".remove_field_sub_topic", function (e) {
                                            var id = $(this).attr('id');
                                            e.preventDefault();
                                            $('#re' + id).remove();
                                            x--;                                            
                                        })
                                    });
                                </script>
						<br>	
						<div id="typechange1" style="display:none;">														
							<div class="row">
								<div class="col-md-10 form-group">     
								<input class="btndubmitclass" type="submit"  id="registrationBtn" name="registrationBtn" value="Submit" />
								</div>
								<div class="col-md-2 form-group"> 
									<div class="actions clearfix">                                       
                                            <a href="#" role="menuitem" id="addstudent" class="addstudent">+ Add More</a>
									</div>									
								</div>
							</div>							
						</div>
						</form>
					</div>		
				<div>	
				<div id="form2" style="display:none;">
					<form id="spkyc_form_2" name="spkyc_form_2"  method="post" >
					<fieldset style="border: 1px solid #eee !important;padding: 15px;" id='displayform2'>
					<div class="row">
							<div class="col-sm-3 form-group"> 
								<label for="name">Enter Name:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm2name1"
									id="txtfrm2name1"
									autocomplete="off" placeholder="Enter Name" onkeypress="javascript:return allowchar(event);" 
									required/>
								<input type="hidden" id="countuser2" name="countuser2" value="1" >									
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="relation">Enter Relation:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm2relation1"
								id="txtfrm2relation1"
								autocomplete="off"	placeholder="Enter Relation" onkeypress="javascript:return allowchar(event);" 
								required/>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="deptname">Interested With:</label> <br/> 
								<label class="radio-inline"> <input type="radio" id="IWSP" name="frm2IW1" value="SP" checked="checked"/> Service Provider </label>
								
								<label class="radio-inline"> </label>
								<label class="radio-inline"> <input type="radio" id="IWITGK" value="ITGK" name="frm2IW1"/> ITGK </label>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="Designation">Enter SP/ITGK Name:<span class="star">*</span></label>
							<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm2iwname1"
								id="txtfrm2iwname1"
							autocomplete="off"	placeholder="Enter Name" onkeypress="javascript:return allowchar(event);" required/>     
							</div>
					</div>
					<div class="row">
							<div class="col-sm-3 form-group"> 
								<label for="name">Nature of Interest:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm2nature1"
								id="txtfrm2nature1"
								autocomplete="off" placeholder="Nature of Interest" onkeypress="javascript:return allowchar(event);" 
								required/>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="relation">Remark:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm2remark1"
								id="txtfrm2remark1"
									placeholder="Remark" autocomplete="off" onkeypress="javascript:return allowchar(event);" required/>
									
							</div>
					</div>
				</fieldset>	
						<script>
                                    $(document).ready(function () {
                                        var max_fields = 100; //maximum input boxes allowed
                                        var wrapper = $("#displayform2"); //Fields wrapper
                                        var add_button = $("#addinterest"); //Add button ID

                                        var x = 1; //initlal text box count
                                        $(add_button).click(function (e) { 
                                            e.preventDefault();
                                            if (x == 100) {
                                                alert('You Can Add 100 KYC Details At A Time');
                                            }
                                            if (x < max_fields) { //max input box allowed
                                                x++; //text box increment
												$('#countuser2').val(x);
                                                $(wrapper).append('</br><div class="panel panel-default" id="remove' + x + '"><div class="panel-heading" role="tab" id="headingOne"><h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#' + x + '" aria-expanded="false" aria-controls="collapseOne" class="collapsed">Click Here To Minimize Or Maximize <span class="glyphicon glyphicon-circle-arrow-down"></span></a><a href="#" id="move' + x + '" class="remove_field_sub_topic" style="float: right;"><span class="glyphicon glyphicon-trash"></span></a></h4></div><div id="' + x + '" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false"><div class="panel-body"><div class="col-sm-3"><label for="Enter NAME">Enter NAME</label><div class="form-group text-uppercase"><input type="text" autocomplete="off" onkeypress="javascript:return allowchar(event);" class="form-control" id="txtfrm2name' + x + '" name="txtfrm2name' + x + '" value="" placeholder="Enter NAME" required /></div></div><div class="col-sm-3"><label for="Enter Email">Enter Relation</label><div class="form-group"><input type="text" class="form-control text-uppercase" id="txtfrm2relation' + x + '" name="txtfrm2relation' + x + '" value="" placeholder="Enter Relation"  onkeypress="javascript:return allowchar(event);" autocomplete="off" required /></div></div><div class="col-sm-3"><label for="Enter Mobile">Interested With</label><div class="form-group"><input type="radio" id="IWSP' + x + '" name="frm2IW' + x + '" checked="checked" value="SP"/> Service Provider <input type="radio" id="IWITGK' + x + '" name="frm2IW' + x + '" value="ITGK"/> ITGK </div></div><div class="col-sm-3"><label for="Enter Name">Enter SP/ITGK Name</label><div class="form-group"><input type="text" autocomplete="off" class="form-control text-uppercase" id="txtfrm2iwname' + x + '" name="txtfrm2iwname' + x + '" value="" placeholder="Enter Name" onkeypress="javascript:return allowchar(event);" required /></div></div><div class="col-sm-3"><label for="Enter NOI">Nature of Interest</label><div class="form-group"><input type="text" autocomplete="off" class="form-control text-uppercase" id="txtfrm2nature' + x + '" name="txtfrm2nature' + x + '" value="" placeholder="Nature of Interest" onkeypress="javascript:return allowchar(event);" required /></div></div><div class="col-sm-3"><label for="Remark">Remark</label><div class="form-group"><input type="text" autocomplete="off" onkeypress="javascript:return allowchar(event);" class="form-control text-uppercase" id="txtfrm2remark' + x + '" name="txtfrm2remark' + x + '" value="" placeholder="Remark" required /></div></div></div></div></div>'); //add input box                                                
                                            }
                                        });

                                        $(wrapper).on("click", ".remove_field_sub_topic", function (e) {
                                            var id = $(this).attr('id');
                                            e.preventDefault();
                                            $('#re' + id).remove();
                                            x--;                                            
                                        })
                                    });
                                </script><br>
							<div class="row">
								<div class="col-md-10 form-group">     
									<input class="btndubmitclass1" type="submit"  id="registrationBtn2" name="registrationBtn2"
										value="Submit" />
								</div>
								<div class="col-md-2 form-group"> 
									<div class="actions clearfix">                                       
                                            <a href="#" role="menuitem" id="addinterest" class="addinterest">+ Add More</a>
									</div>									
								</div>
							</div>	
							</form>
				</div>
				</div>
				<div id="form3" style="display:none;">
					<form id="spkyc_form_3" name="spkyc_form_3"  method="post" >
					<fieldset style="border: 1px solid #eee !important;padding: 15px;" id='displayform3'>
					<div class="row">
							<div class="col-sm-3 form-group"> 
								<label for="name">ITGK Code:<span class="star">*</span></label>
								<input type="text" class="form-control" maxlength="8" minlength="8" name="txtfrm3itgkcode1"
									id="txtfrm3itgkcode1" autocomplete="off"
									placeholder="ITGK Code" onkeypress="javascript:return allownumbers(event);" required/> 
								<input type="hidden" id="countuser3" name="countuser3" value="1" >
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="relation">ITGK Name:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm3itgkname1"
								id="txtfrm3itgkname1"
						autocomplete="off" placeholder="ITGK Name" onkeypress="javascript:return allowchar(event);" required/>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="Remark">Remark:<span class="star">*</span></label>
							<input type="text" class="form-control text-uppercase" maxlength="500" name="txtfrm3remark1"
							id="txtfrm3remark1"
							autocomplete="off"	placeholder="Remark" onkeypress="javascript:return allowchar(event);" required/>     
							</div>
					</div>
					</fieldset>	
							<script>
                                    $(document).ready(function () {
                                        var max_fields = 100; //maximum input boxes allowed
                                        var wrapper = $("#displayform3"); //Fields wrapper
                                        var add_button = $("#addowneditgk"); //Add button ID

                                        var x = 1; //initlal text box count
                                        $(add_button).click(function (e) { 
                                            e.preventDefault();
                                            if (x == 100) {
                                                alert('You Can Add 100 KYC Details At A Time');
                                            }
                                            if (x < max_fields) { //max input box allowed
                                                x++; //text box increment
												$('#countuser3').val(x);
                                                $(wrapper).append('<br><div class="panel panel-default" id="remove' + x + '"><div class="panel-heading" role="tab" id="headingOne"><h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#' + x + '" aria-expanded="false" aria-controls="collapseOne" class="collapsed">Click Here To Minimize Or Maximize <span class="glyphicon glyphicon-circle-arrow-down"></span></a><a href="#" id="move' + x + '" class="remove_field_sub_topic" style="float: right;"><span class="glyphicon glyphicon-trash"></span></a></h4></div><div id="' + x + '" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false"><div class="panel-body"><div class="col-sm-3"><label for="Enter NAME">ITGK Code</label><div class="form-group"><input type="text" autocomplete="off" onkeypress="javascript:return allownumbers(event);" class="form-control" id="txtfrm3itgkcode' + x + '" maxlength="8" minlength="8" name="txtfrm3itgkcode' + x + '" value="" placeholder="ITGK Code" required /></div></div><div class="col-sm-3"><label for="Enter Email">ITGK Name</label><div class="form-group"><input type="text" class="form-control text-uppercase" id="txtfrm3itgkname' + x + '" name="txtfrm3itgkname' + x + '" value="" placeholder="ITGK Name" onkeypress="javascript:return allowchar(event);" required autocomplete="off"/></div></div><div class="col-sm-3"><label for="Enter Mobile">Remark</label><div class="form-group"><input type="text" class="form-control text-uppercase" id="txtfrm3remark' + x + '" name="txtfrm3remark' + x + '" value="" placeholder="Remark" onkeypress="javascript:return allowchar(event);" autocomplete="off" required /></div></div></div></div></div>'); //add input box                                                
                                            }
                                        });

                                        $(wrapper).on("click", ".remove_field_sub_topic", function (e) {
                                            var id = $(this).attr('id');
                                            e.preventDefault();
                                            $('#re' + id).remove();
                                            x--;                                            
                                        })
                                    });
                                </script> <br>
							<div class="row">
								<div class="col-md-10 form-group">     
									<input class="btndubmitclass" type="submit"  id="registrationBtn3" name="registrationBtn3"
											value="Submit" />
								</div>
								<div class="col-md-2 form-group"> 
									<div class="actions clearfix">                                       
                                            <a href="#" role="menuitem" id="addowneditgk" class="addowneditgk">+ Add More</a>
									</div>									
								</div>
							</div>
						</form>
				</div>
				
				<div id="form4" style="display:none;">
					<form id="spkyc_form_4" name="spkyc_form_4"  method="post" >
					<fieldset style="border: 1px solid #eee !important;padding: 15px;" id='displayform4'>
					<div class="row">
							<div class="col-sm-3 form-group"> 
								<label for="name">Enter Name:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm4name1"
								id="txtfrm4name1"
							autocomplete="off"	placeholder="Enter Name" onkeypress="javascript:return allowchar(event);" required/> 
								<input type="hidden" id="countuser4" name="countuser4" value="1" >
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="Designation">Enter Designation:<span class="star">*</span></label>
							<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm4designation1"
							id="txtfrm4designation1"
						autocomplete="off" placeholder="Enter Designation" onkeypress="javascript:return allowchar(event);" required/>
						
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="mobile">Enter Mobile:<span class="star">*</span></label>
								<input type="text" class="form-control" maxlength="10" minlength="10" name="txtfrm4mobile1"
									id="txtfrm4mobile1" autocomplete="off"
									placeholder="Enter Mobile" onkeypress="javascript:return allownumbers(event);" required/>     
							</div>
							<div class="col-sm-3 form-group text-center" style="margin-top:20px;">
									<div id="verfrm4div1" class="btn btn-primary"><a style="cursor:pointer;color: #ffffff;width: 140px;" type="button" id="1" name="otpfrm4Submitlink" class="otpfrm4Submitlink">Verify Mobile No.</a></div>
									<div id="sendfrm4otpres1" style="float:right;margin: 5px 58px 0 0;"></div>
									<input type="hidden" id="verfiyfrm4MobileSet1" name="verfiyfrm4MobileSet1" value="" >
                              </div>							
					</div>
					<div class="row">
							<div class="col-sm-3 form-group"> 
								<label for="Address">Date of Appointment:<span class="star">*</span></label>
								<input type="text" class="form-control appointment" maxlength="100" name="txtfrm4doa1" id="txtfrm4doa1"
								autocomplete="off" readonly="true"	placeholder="Date of Appointment" required/>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="Address">Districts Managed:<span class="star">*</span></label>
									<select id="ddlfrm4District1" name="ddlfrm4District1[]" multiple class="form-control multdist">
                                  
									</select>
								<div id="distres1"></div>
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="Address">Stationed At:<span class="star">*</span></label>
								<select id="ddlfrm4stationat1" name="ddlfrm4stationat1" class="form-control" required>
                                  
									</select>    
							</div>
					</div> </fieldset>
							<script>
                                    $(document).ready(function () {
                                        var max_fields = 100; //maximum input boxes allowed
                                        var wrapper = $("#displayform4"); //Fields wrapper
                                        var add_button = $("#adddistrictmanaged"); //Add button ID

                                        var x = 1; //initlal text box count
                                        $(add_button).click(function (e) { 
                                            e.preventDefault();
                                            if (x == 100) {
                                                alert('You Can Add 100 KYC Details At A Time');
                                            }
                                            if (x < max_fields) { //max input box allowed
                                                x++; //text box increment
												$('#countuser4').val(x);
                                                $(wrapper).append('<br><div class="panel panel-default" id="remove' + x + '"><div class="panel-heading" role="tab" id="headingOne"><h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#' + x + '" aria-expanded="false" aria-controls="collapseOne" class="collapsed">Click Here To Minimize Or Maximize <span class="glyphicon glyphicon-circle-arrow-down"></span></a><a href="#" id="move' + x + '" class="remove_field_sub_topic" style="float: right;"><span class="glyphicon glyphicon-trash"></span></a></h4></div><div id="' + x + '" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false"><div class="panel-body"><div class="col-sm-3"><label for="Enter NAME">Enter NAME</label><div class="form-group"><input type="text" autocomplete="off" onkeypress="javascript:return allowchar(event);" class="form-control text-uppercase" id="txtfrm4name' + x + '" name="txtfrm4name' + x + '" value="" placeholder="Enter NAME" required /></div></div><div class="col-sm-3"><label for="Enter Designation">Enter Designation</label><div class="form-group"><input type="text"  class="form-control text-uppercase" id="txtfrm4designation' + x + '" name="txtfrm4designation' + x + '" value="" placeholder="Enter Designation" onkeypress="javascript:return allowchar(event);" required autocomplete="off"/></div></div><div class="col-sm-3"><label for="Enter Mobile">Enter Mobile</label><div class="form-group"><input type="text" class="form-control" id="txtfrm4mobile' + x + '" name="txtfrm4mobile' + x + '" maxlength="10" minlength="10" value="" placeholder="Enter Mobile" autocomplete="off" onkeypress="javascript:return allownumbers(event);" required /></div></div><div class="col-sm-3 text-center" style="margin-top:20px;"><div class="btn btn-primary" id="verfrm4div' + x + '"><a style="cursor:pointer;color:white;width: 140px;" type="button" id="' + x + '" name="otpfrm4Submitlink" class="otpfrm4Submitlink">Verify Mobile No.</a></div><div id="sendfrm4otpres' + x + '" style="float:right;margin: 5px 58px 0 0;"></div><input type="hidden" id="verfiyfrm4MobileSet' + x + '" name="verfiyfrm4MobileSet' + x + '" value="" ></div><div class="col-sm-3"><label for="Enter Address">Date of Appointment</label><div class="form-group"><input type="text" class="form-control appointment" autocomplete="off" readonly="true" id="txtfrm4doa' + x + '" name="txtfrm4doa' + x + '" value="" placeholder="Date of Appointment" required /></div></div><div class="col-sm-3"><label for="District">Districts Managed</label><div class="form-group"><select id="ddlfrm4District' + x + '" name="ddlfrm4District' + x + '[]" multiple class="form-control multdist2"></select><div id="distres' + x + '"></div></div></div><div class="col-sm-3"><label for="Enter PAN">Stationed At</label><div class="form-group"><select id="ddlfrm4stationat' + x + '" name="ddlfrm4stationat' + x + '" class="form-control" required></select></div></div></div></div></div>'); //add input box  
												FillManageDistrict(x);
												FillStationDistrict(x);
												$('.appointment').datepicker({
													format: "yyyy-mm-dd",
													orientation: "bottom auto",
													todayHighlight: true
												});	
												
                                            }
                                        });

                                        $(wrapper).on("click", ".remove_field_sub_topic", function (e) {
                                            var id = $(this).attr('id');
                                            e.preventDefault();
                                            $('#re' + id).remove();
                                            x--;                                            
                                        })
                                    });
                                </script> <br>
							<div class="row">
								<div class="col-md-10 form-group">     
									<input class="btndubmitclass" type="submit"  id="registrationBtn4" name="registrationBtn4"
											value="Submit" />
								</div>
								<div class="col-md-2 form-group"> 
									<div class="actions clearfix">                                       
                                            <a href="#" role="menuitem" id="adddistrictmanaged" class="adddistrictmanaged">+ Add More</a>
									</div>									
								</div>
							</div>
						</form>
				</div>
				
				<div id="form5" style="display:none;">
                                    <form id="spkyc_form_5" name="spkyc_form_5"  method="post" role="form" enctype="multipart/form-data">
                                        <input type="hidden" id="action" name="action" value="insertform5">
					<div class="row">
							<div class="col-sm-3 form-group"> 
								<label for="email">Contact Person:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm5cp1" id="txtfrm5cp1"
									placeholder="Contact Person" onkeypress="javascript:return allowchar(event);" required/>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="Designation">Enter Designation:<span class="star">*</span></label>
							<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm5designation1" id="txtfrm5designation1"
									placeholder="Enter Designation" onkeypress="javascript:return allowchar(event);" required/>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="name">Enter Address:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm5address1" id="txtfrm5address1"
									placeholder="Enter Address" onkeypress="javascript:return validAddress(event);" required/>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="name">Enter SP Name:<span class="star">*</span></label>
								<input type="text" class="form-control text-uppercase" maxlength="100" name="txtfrm5name1" id="txtfrm5name1"
									placeholder="Enter SP Name" onkeypress="javascript:return allowchar(event);" required/>			
							</div>
												
					</div>
					<div class="row">
							<div class="col-sm-3 form-group"> 
								<label for="email">Enter Email:<span class="star">*</span></label>
								<input type="text" class="form-control" maxlength="100" name="txtfrm5email1" id="txtfrm5email1"
									placeholder="Enter Email" required/>     
							</div>
							<div class="col-sm-3 form-group text-center" style="margin-top:20px;">
									<div id="verfrm5emaildiv1" class="btn btn-primary">
										<a style="cursor:pointer;color: #ffffff;width: 140px;" type="button" id="1"
										name="otpfrm5emailSubmitlink" class="otpfrm5emailSubmitlink">Verify Email Id</a>
									</div>
									<div id="sendfrm5emailotpres1" style="float:right;margin: 5px 58px 0 0;"></div>
									<input type="hidden" id="verfiyfrm5EmailSet1" name="verfiyfrm5EmailSet1" value="" >
                              </div>
							<div class="col-sm-3 form-group"> 
								<label for="mobile">Enter Mobile:<span class="star">*</span></label>
								<input type="text" class="form-control" maxlength="10" name="txtfrm5mobile1" id="txtfrm5mobile1"
									placeholder="Enter Mobile" onkeypress="javascript:return allownumbers(event);" required/>     
							</div>
							<div class="col-sm-3 form-group text-center" style="margin-top:20px;">
								<div id="verfrm5div1" class="btn btn-primary">
									<a style="cursor:pointer;color: #ffffff;width: 140px;" type="button" id="1"
										name="otpfrm5Submitlink" class="otpfrm5Submitlink">Verify Mobile No.</a>
								</div>
								<div id="sendfrm5otpres1" style="float:right;margin: 30px 165px 0 0;"></div>
								<input type="hidden" id="verfiyfrm5MobileSet1" name="verfiyfrm5MobileSet1" value="" >
							</div>
					</div>
                                            <div class="row">
							<div class="col-sm-4 form-group"> 
								<label for="email">Front office with Name and Address Board:<span class="star">*</span></label>
								<input type="file" class="form-control text-uppercase" maxlength="100" name="txtfrm5file1" id="txtfrm5file1"
									required/>     
							</div>
							<div class="col-sm-3 form-group"> 
								<label for="Designation">Reception or Interior Office:<span class="star">*</span></label>
							<input type="file" class="form-control text-uppercase" maxlength="100" name="txtfrm5file2" id="txtfrm5file2"
									required/>     
							</div>
							<div class="col-sm-5 form-group"> 
								<label for="Designation">Attach Memorandum & Articles of Association / Partnership Deed / Society bylaws / Trust deed:<span class="star">*</span></label>
							<input type="file" class="form-control text-uppercase" maxlength="100" name="txtfrm5file3" id="txtfrm5file3"
									required/>     
							</div>
							
												
					</div>
						<div class="row">
								<div class="col-md-10 form-group">     
									<input class="btndubmitclass" type="submit"  id="registrationBtn5" name="registrationBtn5"
											value="Submit" />
								</div>
								
							</div>
					</form>
				</div>

            </div>

        </div>   
    </div>

</div>
<div id="verifyEmailModel" class="modal">
    <div class="modal-content" style="width: 35%;margin: 75px 0 0 450px;">
        <div class="modal-header" style="background-color: #019de2;color: white;">
            <span class="close closes" style="color:white;">&times;</span>
      <h6 style="font-size: 15px;font-weight: bold;">Verify Email</h6>
    </div>
      <div class="modal-body" style="text-align: center;">
          <form id="verifyemailForm" name="verifyemailForm" method="POST">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div><h4>Please Enter OTP Sent On Email</h4></div>
                        <div id="responseEmailOtp"></div>
                        <div class="col-md-6">
                            <label for="VERIFY Email">OTP</label>
                            <div class="form-group">
                                <input type="text" maxlength="6" class="form-control companyN" id="verifyEmailText" name="verifyEmailText" value="" placeholder="ENTER OTP" onkeypress="javascript:return allownumbers(event);" required />
                                <input type="hidden"  id="useridemailmodel" name="useridemailmodel"  />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <ul role="menu" aria-label="Pagination" style="margin-top: 22px;">
                                    <input class="btndubmitclassotp" type="button"  id="otpEmailSubmit" name="otpEmailSubmit"
										value="Verify" />
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
          </form>
    </div>
  </div>
</div>

<div id="verifyfrm5EmailModel" class="modal">
    <div class="modal-content" style="width: 35%;margin: 75px 0 0 450px;">
        <div class="modal-header" style="background-color: #019de2;color: white;">
            <span class="close frm5" style="color:white;">&times;</span>
      <h6 style="font-size: 15px;font-weight: bold;">Verify Email</h6>
    </div>
      <div class="modal-body" style="text-align: center;">
          <form id="verifyfrm5emailForm" name="verifyfrm5emailForm" method="POST">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div><h4>Please Enter OTP Sent On Email</h4></div>
                        <div id="responsefrm5EmailOtp"></div>
                        <div class="col-md-6">
                            <label for="VERIFY Email">OTP</label>
                            <div class="form-group">
                                <input type="text" maxlength="6" class="form-control companyN" id="verifyfrm5EmailText"
								name="verifyfrm5EmailText" value="" placeholder="ENTER OTP"
								onkeypress="javascript:return allownumbers(event);" required />
                                <input type="hidden"  id="useridfrm5emailmodel" name="useridfrm5emailmodel"  />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <ul role="menu" aria-label="Pagination" style="margin-top: 22px;">
                                    <input class="btndubmitclassotp" type="button"  id="otpfrm5EmailSubmit" name="otpfrm5EmailSubmit"
										value="Verify" />
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
          </form>
    </div>
  </div>
</div>

<div id="verifyMobileModel" class="modal">
    <div class="modal-content" style="width: 35%;margin: 75px 0 0 450px;">
        <div class="modal-header" style="background-color: #019de2;color: white;">
            <span class="close" style="color:white;">&times;</span>
      <h6 style="font-size: 15px;font-weight: bold;">Verify Mobile Number</h6>
    </div>
      <div class="modal-body" style="text-align: center;">
          <form id="verifyobileForm" name="verifyobileForm" method="POST">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div><h4>Please Enter OTP Sent On ******<span id="mnootp"></span></h4></div>
                        <div id="responseOtp"></div>
                        <div class="col-md-6">
                            <label for="VERIFY MOBILE">OTP</label>
                            <div class="form-group">
                                <input type="text" maxlength="6" class="form-control companyN" id="verifyMobileText" name="verifyMobileText" value="" placeholder="ENTER OTP" onkeypress="javascript:return allownumbers(event);" required />
                                <input type="hidden"  id="useridmodel" name="useridmodel"  />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <ul role="menu" aria-label="Pagination" style="margin-top: 22px;">
                                    <input class="btndubmitclassotp" type="button"  id="otpSubmit" name="otpSubmit" value="Verify" />
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
          </form>
    </div>
  </div>
</div>

<div id="verifyfrm4MobileModel" class="modal">
    <div class="modal-content" style="width: 35%;margin: 75px 0 0 450px;">
        <div class="modal-header" style="background-color: #019de2;color: white;">
            <span class="close closefrm4" style="color:white;">&times;</span>
      <h6 style="font-size: 15px;font-weight: bold;">Verify Mobile Number</h6>
    </div>
      <div class="modal-body" style="text-align: center;">
          <form id="verifymobileForm4" name="verifymobileForm4" method="POST">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div><h4>Please Enter OTP Sent On ******<span id="mnfrm4ootp"></span></h4></div>
                        <div id="responsefrm4Otp"></div>
                        <div class="col-md-6">
                            <label for="VERIFY MOBILE">OTP</label>
                            <div class="form-group">
                                <input type="text" maxlength="6" class="form-control companyN" id="verifyfrm4MobileText" name="verifyfrm4MobileText" value="" placeholder="ENTER OTP" onkeypress="javascript:return allownumbers(event);" required />
                                <input type="hidden"  id="useridfrm4model" name="useridfrm4model"  />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <ul role="menu" aria-label="Pagination" style="margin-top: 22px;">
                                    <input class="btndubmitclassotp" type="button"  id="otpfrm4Submit" name="otpfrm4Submit"
										value="Verify" />
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
          </form>
    </div>
  </div>
</div>

<div id="verifyfrm5MobileModel" class="modal">
    <div class="modal-content" style="width: 35%;margin: 75px 0 0 450px;">
        <div class="modal-header" style="background-color: #019de2;color: white;">
            <span class="close closefrm5" style="color:white;">&times;</span>
      <h6 style="font-size: 15px;font-weight: bold;">Verify Mobile Number</h6>
    </div>
      <div class="modal-body" style="text-align: center;">
          <form id="verifymobileForm5" name="verifymobileForm5" method="POST">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div><h4>Please Enter OTP Sent On ******<span id="mnfrm5ootp"></span></h4></div>
                        <div id="responsefrm5Otp"></div>
                        <div class="col-md-6">
                            <label for="VERIFY MOBILE">OTP</label>
                            <div class="form-group">
                                <input type="text" maxlength="6" class="form-control companyN" id="verifyfrm5MobileText" 
								name="verifyfrm5MobileText" value="" placeholder="ENTER OTP"
								onkeypress="javascript:return allownumbers(event);" required />
                                <input type="hidden"  id="useridfrm5models" name="useridfrm5models"  />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <ul role="menu" aria-label="Pagination" style="margin-top: 22px;">
                                    <input class="btndubmitclassotp" type="button"  id="otpfrm5Submit" name="otpfrm5Submit"
										value="Verify" />
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
          </form>
    </div>
  </div>
</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>		

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
    <style>

        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;

        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
        }

        #checkboxes label {
            display: block;
        }

        #checkboxes label:hover {
            background-color: #1e90ff;
        }
    </style>
	<script type="text/javascript">
		$('.appointment').datepicker({
			format: "yyyy-mm-dd",
			orientation: "bottom auto",
			todayHighlight: true
		});
	</script>
	<script>
                                            var expanded = false;

                                            function showCheckboxes() {
                                                var checkboxes = document.getElementById("checkboxes");
                                                if (!expanded) {
                                                    checkboxes.style.display = "block";
                                                    expanded = true;
                                                } else {
                                                    checkboxes.style.display = "none";
                                                    expanded = false;
                                                }
                                            }
    </script>
<style>
    .breadcrumbs {
        position: absolute;
        display: inline;
        display: inline-block;
        left: 0;
        width: 100%;
        padding: 0 20px;
        width: 100%;
        background: rgba(6, 48, 87, 0.9);
        color: #fff;
        font-size: 15px;
        line-height: 35px;
        text-align: left;
        z-index: 0;
    }
    .breadcrumbs a{
       color: #ffffff;
    }
    .breadcrumbs a:hover{
        color: #ffc14f;
    }
    .activecls{
        color: #ffc14f !important
    }
</style>

<script>

			
		function multisel() {
                $('#ddlfrm4District1').multiselect({
                    nonSelectedText: 'Select District',
                    includeResetOption: true,
                    resetText: "Reset all",
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,

                    minHeight: 200,
                    maxHeight: '200',
                    width: '33%',
                    buttonWidth: '100%',
                    dropRight: true

                });
            }
			
			function multisele(x) {
                $('#ddlfrm4District' + x).multiselect({
                    nonSelectedText: 'Select District',
                    includeResetOption: true,
                    resetText: "Reset all",
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,

                    minHeight: 200,
                    maxHeight: '200',
                    width: '33%',
                    buttonWidth: '100%',
                    dropRight: true

                });
            }
			
function FillManageDistrict(x) {
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    if (typeof x == 'undefined') {
						//$('#ddlfrm4District1').html(data);
							$("#ddlfrm4District1").append(data);                            
                            $("#ddlfrm4District1 option[value='0']").remove();
                            multisel();
                            $('#ddlfrm4District1').multiselect('rebuild');
                            $("#ddlfrm4District1 option[value='0']").remove();
					} else {
						
							$('#ddlfrm4District' + x).append(data);                            
                            $("#ddlfrm4District option[value='0']" + x).remove();
                            multisele(x);
                            $('#ddlfrm4District' + x).multiselect('rebuild');
                            $("#ddlfrm4District option[value='0']" + x).remove();
					}
                }
            });
        }
        FillManageDistrict();
		
		function FillStationDistrict(x) {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    if (typeof x == 'undefined') {
						$('#ddlfrm4stationat1').html(data);
					} else {
						$('#ddlfrm4stationat' + x).html(data);
					}
                }
            });
        }
        FillStationDistrict();
		
		
		$("#spkyc_form_1").on("click", ".otpSubmitlink", function () {
            var  x = $(this).attr("id");
            $("#useridmodel").val(x);
            var mtext = "#txtfrm1mobile"+x;
            var mobile = $(mtext).val();
			if(!mobile.match('[0-9]{10}')){
                alert("Please Enter Valid Mobile Number");
                return false;
            }else{
                
                $('#sendotpres'+x).empty();
                $('#sendotpres'+x).append("<p class='error'><span><img src=images/ajax-loader.gif style='width: 10px;' /></span><span style='font-size: 15px;margin: 5px; font-weight: bold;'>Processing.....</span></p>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                
                data = "action=GENERATEOTP&mobile=" + mobile; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						var data = data.trim();
                       if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            var lastFive = mobile.substr(mobile.length - 4);
                            $("#mnootp").html(lastFive);
                            var modal = document.getElementById('verifyMobileModel');
                            var span = document.getElementsByClassName("close")[0];
                            modal.style.display = "block";
                            span.onclick = function() { 
                                modal.style.display = "none";
                                $('#sendotpres'+x).empty();
                            }
//                            window.onclick = function(event) {
//                                if (event.target == modal) {
//                                    modal.style.display = "none";
//                                }
//                            } 
                        } else
                        {
                            $('#sendotpres'+x).empty();
                            $('#sendotpres'+x).append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>OTP Not Generated.</span></p>");
                        }
                        
                    }
                });
            }     
        });
		
		$("#spkyc_form_1").on("click", ".otpemailSubmitlink", function () {
			//alert("bka");
            var  x = $(this).attr("id");
            $("#useridemailmodel").val(x);
            var mtext = "#txtfrm1email"+x;
            var email = $(mtext).val();
			//alert(email);
            if(!email.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')){
                alert("Please Enter Valid Email Id");
                return false;
            }else{
                
                $('#sendemailotpres'+x).empty();
                $('#sendemailotpres'+x).append("<p class='error'><span><img src=images/ajax-loader.gif style='width: 10px;' /></span><span style='font-size: 15px;margin: 5px; font-weight: bold;'>Processing.....</span></p>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                
                data = "action=GENERATEEMAILOTP&email=" + email; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                       var data = data.trim();
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            var modal = document.getElementById('verifyEmailModel');
                            var span = document.getElementsByClassName("closes")[0];
                            modal.style.display = "block";
                            span.onclick = function() { 
                                modal.style.display = "none";
                                $('#sendemailotpres'+x).empty();
                            }
//                            window.onclick = function(event) {
//                                if (event.target == modal) {
//                                    modal.style.display = "none";
//                                }
//                            } 
                        } else
                        {
                            $('#sendemailotpres'+x).empty();
                            $('#sendemailotpres'+x).append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>OTP Not Generated.</span></p>");
                        }
                        
                    }
                });
            }     
        });
		
		$("#spkyc_form_4").on("click", ".otpfrm4Submitlink", function () {
            var  x = $(this).attr("id");
            $("#useridfrm4model").val(x);
            var mtext = "#txtfrm4mobile"+x;
            var mobile = $(mtext).val();
			if(!mobile.match('[0-9]{10}')){
                alert("Please Enter Valid Mobile Number");
                return false;
            }else{
                
                $('#sendfrm4otpres'+x).empty();
                $('#sendfrm4otpres'+x).append("<p class='error'><span><img src=images/ajax-loader.gif style='width: 10px;' /></span><span style='font-size: 15px;margin: 5px; font-weight: bold;'>Processing.....</span></p>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                
                data = "action=GENERATEOTP&mobile=" + mobile; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						var data = data.trim();
                       if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            var lastFive = mobile.substr(mobile.length - 4);
                            $("#mnfrm4ootp").html(lastFive);
                            var modal = document.getElementById('verifyfrm4MobileModel');
                            var span = document.getElementsByClassName("closefrm4")[0];
                            modal.style.display = "block";
                            span.onclick = function() { 
                                modal.style.display = "none";
                                $('#sendfrm4otpres'+x).empty();
                            }
//                            window.onclick = function(event) {
//                                if (event.target == modal) {
//                                    modal.style.display = "none";
//                                }
//                            } 
                        } else
                        {
                            $('#sendfrm4otpres'+x).empty();
                            $('#sendfrm4otpres'+x).append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>OTP Not Generated.</span></p>");
                        }
                        
                    }
                });
            }     
        });
		
		$("#spkyc_form_5").on("click", ".otpfrm5emailSubmitlink", function () {
			var  x = $(this).attr("id");
            $("#useridfrm5emailmodel").val(x);
            var mtext = "#txtfrm5email"+x;
            var email = $(mtext).val();
			if(!email.match('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')){
                alert("Please Enter Valid Email Id");
                return false;
            }else{
                
                $('#sendfrm5emailotpres'+x).empty();
                $('#sendfrm5emailotpres'+x).append("<p class='error'><span><img src=images/ajax-loader.gif style='width: 10px;' /></span><span style='font-size: 15px;margin: 5px; font-weight: bold;'>Processing.....</span></p>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                
                data = "action=GENERATEEMAILOTP&email=" + email; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                       var data = data.trim();
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            var modal = document.getElementById('verifyfrm5EmailModel');
                            var span = document.getElementsByClassName("frm5")[0];
                            modal.style.display = "block";
                            span.onclick = function() { 
                                modal.style.display = "none";
                                $('#sendfrm5emailotpres'+x).empty();
                            }
//                            window.onclick = function(event) {
//                                if (event.target == modal) {
//                                    modal.style.display = "none";
//                                }
//                            } 
                        } else
                        {
                            $('#sendfrm5emailotpres'+x).empty();
                            $('#sendfrm5emailotpres'+x).append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>OTP Not Generated.</span></p>");
                        }
                        
                    }
                });
            }     
        });
		
		$("#spkyc_form_5").on("click", ".otpfrm5Submitlink", function () {
            var  x = $(this).attr("id");
            $("#useridfrm5models").val(x);
            var mtext = "#txtfrm5mobile"+x;
            var mobile = $(mtext).val();
			if(!mobile.match('[0-9]{10}')){
                alert("Please Enter Valid Mobile Number");
                return false;
            }else{
                
                $('#sendfrm5otpres'+x).empty();
                $('#sendfrm5otpres'+x).append("<p class='error'><span><img src=images/ajax-loader.gif style='width: 10px;' /></span><span style='font-size: 15px;margin: 5px; font-weight: bold;'>Processing.....</span></p>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                
                data = "action=GENERATEOTP&mobile=" + mobile; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						var data = data.trim();
                       if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            var lastFive = mobile.substr(mobile.length - 4);
                            $("#mnfrm5ootp").html(lastFive);
                            var modal = document.getElementById('verifyfrm5MobileModel');
                            var span = document.getElementsByClassName("closefrm5")[0];
                            modal.style.display = "block";
                            span.onclick = function() { 
                                modal.style.display = "none";
                                $('#sendfrm5otpres'+x).empty();
                            }
//                            window.onclick = function(event) {
//                                if (event.target == modal) {
//                                    modal.style.display = "none";
//                                }
//                            } 
                        } else
                        {
                            $('#sendfrm5otpres'+x).empty();
                            $('#sendfrm5otpres'+x).append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>OTP Not Generated.</span></p>");
                        }
                        
                    }
                });
            }     
        });
		
        $("#otpSubmit").click(function () {
            if ($("#verifyobileForm").valid())
              {
              $('#responseOtp').empty();
              $('#responseOtp').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                var otp = $("#verifyMobileText").val();
                var id = $("#useridmodel").val();
                var user = "#txtfrm1mobile"+id;
                var verdiv = '#verdiv'+id;
                var mobile = $(user).val();
                data = "action=VERIFYOTPFUN&otp=" + otp +"&mobile="+mobile; // serializes the form's elements.          
                $.ajax({ 
                    type: "POST",
                    url: url,
                    data: data,
                  success: function(data)
                    { 
					var data = data.trim();
                        if (data == SuccessfullyUpdate)
                          { 
                              $("#txtfrm1mobile"+id).attr("readonly", "true");
                              $('#sendotpres'+id).empty();
                              $("#verfiyMobileSet"+id).val("varified");
                              $(verdiv).html("Verified");
                              $('#responseOtp').empty();
                              $('#responseOtp').append("<p class='error'><span><img src=images/correct.gif   style='width: 20px;' /></span><span style='font-size: 15px;margin: 5px; font-weight: bold;'>Mobile Number Verified Seccessfully.</span></p>");
                              window.setTimeout(function () {
                                $("#verifyMobileText").val("");
								 $('#responseOtp').empty();
                                var modal = document.getElementById('verifyMobileModel');
                                modal.style.display = "none";
                            }, 1000);
                          }
                          else
                          {
                              $('#responseOtp').empty();
                              $('#responseOtp').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>Invalid OTP</span></p>");
                              
                          }
                    }	        
              });
              }
              return false;
          });
		  
		$("#otpEmailSubmit").click(function () {
            if ($("#verifyemailForm").valid())
              {
              $('#responseEmailOtp').empty();
              $('#responseEmailOtp').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                var otp = $("#verifyEmailText").val();
                var id = $("#useridemailmodel").val();
                var user = "#txtfrm1email"+id;
                var verdiv = '#veremaildiv'+id;
                var mobile = $(user).val();
                data = "action=VERIFYOTPEmail&otp=" + otp +"&email="+mobile; // serializes the form's elements.          
                $.ajax({ 
                    type: "POST",
                    url: url,
                    data: data,
                  success: function(data)
                    { 
					var data = data.trim();
                        if (data == SuccessfullyUpdate)
                          { 
                              $("#txtfrm1email"+id).attr("readonly", "true");
                              $('#sendemailotpres'+id).empty();
                              $("#verfiyEmailSet"+id).val("varified");
                              $(verdiv).html("Verified");
                              $('#responseEmailOtp').empty();
                              $('#responseEmailOtp').append("<p class='error'><span><img src=images/correct.gif   style='width: 20px;' /></span><span style='font-size: 15px;margin: 5px; font-weight: bold;'>Email Id Verified Successfully.</span></p>");
                              window.setTimeout(function () {
                                $("#verifyEmailText").val("");
								$('#responseEmailOtp').empty();
                                var modal = document.getElementById('verifyEmailModel');
                                modal.style.display = "none";
                            }, 1000);
                          }
                          else
                          {
                              $('#responseEmailOtp').empty();
                              $('#responseEmailOtp').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>Invalid OTP</span></p>");
                              
                          }
                    }	        
              });
              }
              return false;
          });
		  
		$("#otpfrm4Submit").click(function () {
            if ($("#verifymobileForm4").valid())
              {
              $('#responsefrm4Otp').empty();
              $('#responsefrm4Otp').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                var otp = $("#verifyfrm4MobileText").val();
                var id = $("#useridfrm4model").val();
                var user = "#txtfrm4mobile"+id;
                var verdiv = '#verfrm4div'+id;
                var mobile = $(user).val();
                data = "action=VERIFYOTPFUN&otp=" + otp +"&mobile="+mobile; // serializes the form's elements.          
                $.ajax({ 
                    type: "POST",
                    url: url,
                    data: data,
                  success: function(data)
                    { 
						var data = data.trim();
                        if (data == SuccessfullyUpdate)
                          { 
                              $("#txtfrm4mobile"+id).attr("readonly", "true");
                              $('#sendfrm4otpres'+id).empty();
                              $("#verfiyfrm4MobileSet"+id).val("varified");
                              $(verdiv).html("Verified");
                              $('#responsefrm4Otp').empty();
                              $('#responsefrm4Otp').append("<p class='error'><span><img src=images/correct.gif   style='width: 20px;' /></span><span style='font-size: 15px;margin: 5px; font-weight: bold;'>Mobile Number Verified Successfully.</span></p>");
                              window.setTimeout(function () {
                                $("#verifyfrm4MobileText").val("");
								 $('#responsefrm4Otp').empty();
                                var modal = document.getElementById('verifyfrm4MobileModel');
                                modal.style.display = "none";
                            }, 1000);
                          }
                          else
                          {
                              $('#responsefrm4Otp').empty();
                              $('#responsefrm4Otp').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>Invalid OTP</span></p>");
                              
                          }
                    }	        
              });
              }
              return false;
          });
		    
		$("#otpfrm5EmailSubmit").click(function () {
            if ($("#verifyfrm5emailForm").valid())
              {
              $('#responsefrm5EmailOtp').empty();
              $('#responsefrm5EmailOtp').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                var otp = $("#verifyfrm5EmailText").val();
                var id = $("#useridfrm5emailmodel").val();
                var user = "#txtfrm5email"+id;
                var verdiv = '#verfrm5emaildiv'+id;
                var mobile = $(user).val();
                data = "action=VERIFYOTPEmail&otp=" + otp +"&email="+mobile; // serializes the form's elements.          
                $.ajax({ 
                    type: "POST",
                    url: url,
                    data: data,
                  success: function(data)
                    { 
					var data = data.trim();
                        if (data == SuccessfullyUpdate)
                          { 
                              $("#txtfrm5email"+id).attr("readonly", "true");
                              $('#sendfrm5emailotpres'+id).empty();
                              $("#verfiyfrm5EmailSet"+id).val("varified");
                              $(verdiv).html("Verified");
                              $('#responsefrm5EmailOtp').empty();
                              $('#responsefrm5EmailOtp').append("<p class='error'><span><img src=images/correct.gif   style='width: 20px;' /></span><span style='font-size: 15px;margin: 5px; font-weight: bold;'>Email Id Verified Successfully.</span></p>");
                              window.setTimeout(function () {
                                $("#verifyfrm5EmailText").val("");
								$('#responsefrm5EmailOtp').empty();
                                var modal = document.getElementById('verifyfrm5EmailModel');
                                modal.style.display = "none";
                            }, 1000);
                          }
                          else
                          {
                              $('#responsefrm5EmailOtp').empty();
                              $('#responsefrm5EmailOtp').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>Invalid OTP</span></p>");
                              
                          }
                    }	        
              });
              }
              return false;
          });
		  
		  $("#otpfrm5Submit").click(function () {
            if ($("#verifymobileForm5").valid())
              {
              $('#responsefrm5Otp').empty();
              $('#responsefrm5Otp').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                var otp = $("#verifyfrm5MobileText").val();
                var id = $("#useridfrm5models").val();
                var user = "#txtfrm5mobile"+id;
                var verdiv = '#verfrm5div'+id;
                var mobile = $(user).val();
                data = "action=VERIFYOTPFUN&otp=" + otp +"&mobile="+mobile; // serializes the form's elements.          
                $.ajax({ 
                    type: "POST",
                    url: url,
                    data: data,
                  success: function(data)
                    { 
					var data = data.trim();
                        if (data == SuccessfullyUpdate)
                          { 
                              $("#txtfrm5mobile"+id).attr("readonly", "true");
                              $('#sendfrm5otpres'+id).empty();
                              $("#verfiyfrm5MobileSet"+id).val("varified");
                              $(verdiv).html("Verified");
                              $('#responsefrm5Otp').empty();
                              $('#responsefrm5Otp').append("<p class='error'><span><img src=images/correct.gif   style='width: 20px;' /></span><span style='font-size: 15px;margin: 5px; font-weight: bold;'>Mobile Number Verified Successfully.</span></p>");
                              window.setTimeout(function () {
                                $("#verifyfrm5MobileText").val("");
								 $('#responsefrm5Otp').empty();
                                var modal = document.getElementById('verifyfrm5MobileModel');
                                modal.style.display = "none";
                            }, 1000);
                          }
                          else
                          {
                              $('#responsefrm5Otp').empty();
                              $('#responsefrm5Otp').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>Invalid OTP</span></p>");
                              
                          }
                    }	        
              });
              }
              return false;
          });
		
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


          function FillOrgType() {
            $.ajax({
                type: "post",
                url: "common/cfSpKyc.php",
                data: "action=FillCompType",
                success: function (data) {
					var data = data.trim();
					$("#txtType").html(data);
                }
            });
        }
        FillOrgType();
		
		
		
		
		$("#txtKycType").change(function () {
			var SelKycType = $(this).val();
				 $.ajax({
					type: "post",
					url: "common/cfSpKyc.php",
					data: "action=CheckExistence&SelKycType="+SelKycType,
					success: function (data) {
						var data = data.trim();
						if(data=='no'){
							$('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>You have already submitted details.</span></p>");
							$("#form1").hide();$("#form2").hide();$("#form3").hide();$("#form4").hide();$("#form5").hide();
						}
						else if(data=='wrong'){
							$('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>Something went wrong.</span></p>");
							$("#form1").hide();$("#form2").hide();$("#form3").hide();$("#form4").hide();$("#form5").hide();
						}
						else{
							$('#response').empty();
							if(SelKycType=='1'){
								$("#form1").show();
								$("#form2").hide();$("#form3").hide();$("#form4").hide();$("#form5").hide();
							}
							else if(SelKycType=='2'){
								$("#form2").show();
								$("#form1").hide();$("#form3").hide();$("#form4").hide();$("#form5").hide();
							}
							else if(SelKycType=='3'){
								$("#form3").show();
								$("#form1").hide();$("#form2").hide();$("#form4").hide();$("#form5").hide();
							}
							else if(SelKycType=='4'){
								$("#form4").show();
								$("#form1").hide();$("#form2").hide();$("#form3").hide();$("#form5").hide();
							}
							else if(SelKycType=='5'){
								$("#form5").show();
								$("#form1").hide();$("#form2").hide();$("#form3").hide();$("#form4").hide();
							}
							else{
								$("#form1").hide();$("#form2").hide();$("#form3").hide();$("#form4").hide();$("#form5").hide();
							}
						}
					}
				});
			
				
		});
		
		$("#txtType").change(function () {
            var selOrgType = $(this).val();
				
				if (selOrgType == '1' || selOrgType == '2' || selOrgType == '3' || selOrgType == '4' || selOrgType == '5' ||
					selOrgType == '6' || selOrgType == '7' || selOrgType == '8' || selOrgType == '9' )
					{
						$("#typechange").show();
						$("#typechange1").show();
					}
				else{
					$("#typechange").hide();
					$("#typechange1").hide();
				}
			
		});
		
		$("#spkyc_form_1").on("submit",function (e) {
			e.preventDefault();
			
			$('#response').empty();
                $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#spkyc_form_1").serialize();
                data = "action=insertform1&" + forminput; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    { 
					var data = data.trim();
						if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {                           
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
							window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmspkyc.php';
                            }, 3000);
                        } else
                        {
                           // $(".phoneNumberclass").attr("disabled", "disabled");
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>" + data + "</span></p>");
                        }
                        
                    }
                });
		});
		
		$("#spkyc_form_2").on("submit",function (e) {
			e.preventDefault();
			$('#response').empty();
                $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#spkyc_form_2").serialize();
                data = "action=insertform2&" + forminput; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						var data = data.trim();
						if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {                           
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
							window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmspkyc.php';
                            }, 3000);
                        } else
                        {
                           // $(".phoneNumberclass").attr("disabled", "disabled");
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>" + data + "</span></p>");
                        }
                        
                    }
                });
		});
		
		$("#spkyc_form_3").on("submit",function (e) {
			e.preventDefault();
			$('#response').empty();
                $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#spkyc_form_3").serialize();
                data = "action=insertform3&" + forminput; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						var data = data.trim();
                       if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {                           
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
							window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmspkyc.php';
                            }, 3000);
                        } else
                        {
                           // $(".phoneNumberclass").attr("disabled", "disabled");
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>" + data + "</span></p>");
                        }
                        
                    }
                });
		});
		
		$("#spkyc_form_4").on("submit",function (e) {
			e.preventDefault();
			
				var flag=0;
				var limit = $('#countuser4').val();
				for (var i=1; i<=limit; i++){
					//var options = $("#ddlfrm4District" + i + > option:selected);
				if($("#ddlfrm4District"+i).length){
					
					var options = $('#ddlfrm4District' + i +' > option:selected');
					if(options.length == 0){
						flag++;
						 //alert('Please Select District');
						 $('#distres' + i).empty();
						$('#distres' + i).append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>Please Select District</span></p>");
						 return false;
					 }
					 else{
						 $('#distres' + i).empty();
					 }
				}
				}
				
				if(flag>0){
					
				}				 
				else{
					 		 
				$('#response').empty();
                $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfSpKyc.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#spkyc_form_4").serialize();
                data = "action=insertform4&" + forminput; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						var data = data.trim();
                       if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {                           
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
							window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmspkyc.php';
                            }, 3000);
                        } else
                        {
                           // $(".phoneNumberclass").attr("disabled", "disabled");
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>" + data + "</span></p>");
                        }
                        
                    }
			});}
			
		});
		
		$("#spkyc_form_5").on("submit",function (e) {
			e.preventDefault();
			//alert("5");
			$('#response').empty();
                $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                //var url = "common/cfSpKyc.php"; // the script where you handle the form input.
               // var data;
               // var forminput = $("#spkyc_form_5").serialize();
               // data = "action=insertform5&" + forminput; // serializes the form's elements.
                $.ajax({
                    url: "common/cfSpKyc.php",
                    type: "POST",
                    data:  new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    {
						var data = data.trim();
                       if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {                           
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
							window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmspkyc.php';
                            }, 3000);
                        } else
                        {
                           // $(".phoneNumberclass").attr("disabled", "disabled");
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>" + data + "</span></p>");
                        }
                        
                    }
                });
		});
       

        
    });

</script>
</html>
