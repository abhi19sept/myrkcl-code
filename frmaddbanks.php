<?php
$title="Add Bank Information ";
include ('header.php'); 
include ('root_menu.php'); 
    if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
if ($_SESSION['User_UserRoll'] == '9' || $_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '8') {

   
    ?>

<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			

            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">Add Bank Information 
				 
				</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmaddbanks" id="frmaddbanks" class="form-inline" action=""> 
					
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
							
							<div class="container">
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">IFSC Code :<span class="star">*</span></label>
                                
								 <input type="text" class="form-control"  name="txtifsc" 
									id="txtifsc" placeholder="IFSC Code" onkeypress="javascript:return validAddress(event);" >
								
							   
                            </div>
							
							 
							<div class="col-sm-4 form-group">     
                                <label for="address"> Bank Name :<span class="star">*</span></label>
								
								
								<select id="ddlBankname" name="ddlBankname" class="form-control">
								<option value=""> Please Select </option>
										<option value="0"> Bank Name</option>	   
									</select> 
                           							 
                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address">Micr Code:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="25" name="txtMicr" 
									id="txtMicr" placeholder=" Micr Code" onkeypress="javascript:return allownumbers(event);" >
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="address">Branch Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtBranch" id="txtBranch" placeholder="Branch Name" onkeypress="javascript:return allowchar(event);">    
                            </div>
							</div>
							
							
							<div class="container">
							<div class="col-sm-4 form-group">     
                                <label for="address">Bank Address.<span class="star">*</span></label>
                                <input type="text" class="form-control" id="txtAddress" name="txtAddress" placeholder="Bank Address" maxlength="100" onkeypress="javascript:return validAddress(event);">

                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address">Bank Contact:<span class="star">*</span></label>
                                <input type="text" class="form-control" id="txtContact" name="txtContact" placeholder="Bank Contact" onkeypress="javascript:return allownumbers(event);">

                            </div>
							
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address"> Bank State :<span class="star">*</span></label>
								
								
								<select id="txtState" name="txtState" class="form-control">
								<option value=""> Please Select </option>
										<option value="Rajasthan"> Rajasthan</option>	   
									</select> 
                           
								
								
                          
							 
                            </div>
							
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address"> Bank District :<span class="star">*</span></label>
								
								
								<select id="txtDistrict" name="txtDistrict" class="form-control">
									   
									</select> 
                           
							 
						</div>
						
						
						</div>
						
						
						
						<div class="container">
						<div class="col-sm-4 form-group">     
                                <label for="address"> Bank City:<span class="star">*</span></label>
								
								
								
								
								<input type="text" class="form-control" id="txtCity" name="txtCity" placeholder="City" onkeypress="javascript:return allowchar(event);">
								
								
                              
                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address">Active :<span class="star">*</span></label>
                            <select id="ddlStatus" name="ddlStatus" class="form-control">
									    
									</select> 
                            </div>
							<input type="button" class="btn btn-primary" value="OR"  style='margin-top:25px;'/>
							<input type="button" name="Add More Bank" id="AddMore" class="btn btn-primary" value="Add More Bank"  style='margin-top:25px;'/> 
							
							<div class="col-sm-4 form-group" id='bank' style='display:none;'> 
													
                                <label for="address"> Bank Name:</label>
								<input type="text" class="form-control" id="txtbank" name="txtbank" placeholder="txtbank" onkeypress="javascript:return allowchar(event);">
								
								
                              
                            </div>
						</div>
						
						</div>
						 
						<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="margin-top:10px;margin-left:25px;"/>    
                        </div>
						
						
						<div id="grid"></div>
                </div>
            </div>   
        </div>
    </form>
	
	</div>

</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox
{	color:#F00;	 } 
</style>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>


 <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
			
		$("#AddMore").click(function () {
			$("#bank").show(2000); 
        });	
		
		   $("#btnSubmit").click(function () {
			if ($("#frmaddbanks").valid())
            {
				$('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfaddbanks.php"; // the script where you handle the form input.
				
                var data;
                var forminput=$("#frmaddbanks").serialize();
                if (Mode == 'Add')
                {
                    data = "action=ADD&" +forminput; // serializes the form's elements.
                }
                else
                {
					
					data = "action=UPDATE&code=" + Code +"&" + forminput;
                    //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                             window.setTimeout(function () {
                                window.location.href = "frmaddbanks.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmaddbanks");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			 }
                return false; // avoid to execute the actual submit of the form.
            });
		
		
		
		if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
			
			function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfaddbanks.php",
                    data: "action=DELETE&values=" + Code + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmaddbankss.php";
                           }, 1000);
                            
                            Mode="Add";
                            resetForm("frmaddbanks");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();
                    }
                });
            }
			
			
 
            /*  function showData() 
			 {
                
				$('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfaddbanks.php",
                    data: "action=SHOW",
                    success: function (data) {

						$('#response').empty();
                        $("#grid").html(data);
						$('#example').DataTable({
							dom: 'Bfrtip',
							buttons: [
								'copy', 'csv', 'excel', 'pdf', 'print'
							]
						});
						

                    }
                });
            }

            showData();
			  */
			
			
          

            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
			
         
			
			function FillDistrict() {
                $.ajax({
                    type: "post",
                    url: "common/cfaddbanks.php",
                    data: "action=FILLDISTRICT",
                    success: function (data) {
                        $("#txtDistrict").html(data);
                    }
                });
            }
            FillDistrict();
			
			
			function FillBanks() {
                $.ajax({
                    type: "post",
                    url: "common/cfaddbanks.php",
                    data: "action=FILLBANKS",
                    success: function (data) {
                        $("#ddlBankname").html(data);
                    }
                });
            }
            FillBanks();
			
		
			
         
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
	
	<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmaddbankvalidation.js" type="text/javascript"></script>	
</html>
  <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>	
