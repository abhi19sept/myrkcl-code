<?php
$title = "DD Payment Mode";
include ('header.php');
include ('root_menu.php');


// Code End for Payment Gateway-----------------------
if (isset($_POST['ddcode'])) {
    //print_r($_POST);
    $Code = $_POST['ddcode'];
    $paytype = $_POST['ddpaytype'];
//    $amount = $_POST['amount'];
    echo "<script>var UserCode='" . $_SESSION['User_Code'] . "'</script>";
    echo "<script>var CenterCode='" . $_POST['ddcode'] . "'</script>";
    //echo "<script>var PayTypeCode=" . $_POST['paytypecode'] . "</script>";
    echo "<script>var PayType='" . $_POST['ddpaytype'] . "'</script>";
    echo "<script>var amount=" . $_POST['ddamount'] . "</script>";
    echo "<script>var email='" . $_SESSION['User_EmailId'] . "'</script>";
    echo "<script>var Mode='Update'</script>";
}

//print_r($_POST);
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div class="container"> 


    <div class="panel panel-primary" style="margin-top:36px;">

        <div class="panel-heading">Payment Transection Details</div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            <form name="DDForm" id="DDForm" class="form-inline" role="form" enctype="multipart/form-data">     

                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>

                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Center Code:</label>

                        <input type="hidden" name="surl" value="http://localhost/myrkcladmin/admissionsuccess.php" size="64" />
                        <input type="hidden" name="furl" value="http://localhost/myrkcladmin/admissionfailure.php" size="64" />
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>

                        <input readonly="true" type="text" class="form-control" maxlength="50"   name="txtCenterCode" id="txtCenterCode" placeholder="Center Code" value="<?php echo $Code ?>">
                    </div>


                    <div class="col-sm-4 form-group" style="display: none"> 
                        <label for="ename">Owner Name:</label>
                        <!--   <div class="form-control" maxlength="50" name="firstnamediv" id="firstnamediv">
                              
                          </div> -->
                        <input  readonly="true" type="text" class="form-control text-uppercase" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" maxlength="50" name="firstname" id="firstname" placeholder="End Date">    
                    </div>


                    <div class="col-sm-4 form-group" style="display: none">     
                        <label for="faname">Mobile No:</label>
                        <!-- <div class="form-control" maxlength="50" name="phonediv" id="phonediv">
                             
                         </div> -->
                        <input type="text" readonly="true" class="form-control" maxlength="50" name="phone" id="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>"  placeholder="Owner Mobile">
                    </div>
                    <div class="col-sm-4 form-group" style="display: none">     
                        <label for="faname">Email ID:</label>
                        <!--    <div class="form-control" maxlength="50" name="emaildiv" id="emaildiv">
                               
                           </div> -->
                        <input type="text" readonly="true" class="form-control" maxlength="50" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>"  placeholder="Owner Email">
                    </div>  
                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">Payment Type:</label>
                        <input type="text" readonly="true" class="form-control" name="productinfo" id="productinfo" value="<?php echo $paytype; ?>"/>

                    </div>


                    <div class="col-sm-4 form-group"> 
                        <label for="ename">Transection Reference No:</label>
                        <!--  <div class="form-control" maxlength="50"  name="transactiondiv" id="transactiondiv">
                             123456
                         </div> -->
                        <input type="text" readonly="true" class="form-control" maxlength="50" name="transaction" id="transaction" placeholder="Tran. Ref. No:" value="123456">  
                    </div>


                    <div class="col-sm-4 form-group">     
                        <label for="faname">Amount:</label>
                        <!--   <div class="form-control" maxlength="50" name="amountdiv" id="amountdiv">
                              
                          </div> -->
                        <input type="text" readonly="true" class="form-control" maxlength="50" name="amount" id="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"  placeholder="Total Amount">
                    </div>
                </div>  

                <div class="container">

                </div>



                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">DD No.:</label>
                        <input type="text"  class="form-control" placeholder="Enter DD No." name="ddno" id="ddno" value="" onkeypress="javascript:return allownumbers(event);"/>

                    </div>


                    <div class="col-sm-4 form-group"> 
                        <label for="ename">DD Date:</label>                                
                        <input type="text" class="form-control" name="dddate" readonly="true" id="dddate" placeholder="YYYY-MM-DD">  
                    </div>
                    <div class="col-sm-4 form-group"> 
                        <label for="micr">MICR Code:<span class="star">*</span></label>
                        <input type="text" class="form-control" onkeypress="javascript:return allownumbers(event);"   name="txtMicrNo" id="txtMicrNo" placeholder="EMP Bank MICR Code">
                    </div>	

                    <div class="col-sm-4 form-group"> 
                        <label for="bankdistrict">Employee Bank District:<span class="star">*</span></label>
                        <select id="ddlBankDistrict" name="ddlBankDistrict" class="form-control">

                        </select>
                    </div>


                </div>

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="bankname">Employee Bank Name:<span class="star">*</span></label>
                        <select id="ddlBankName" name="ddlBankName" class="form-control">
                            <option value="" selected="selected">Please Select</option>                
                        </select>                                                              
                    </div> 
                    <div class="col-sm-4 form-group"> 
                        <label for="branchname">Branch_Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" onkeypress="javascript:return allowchar(event);"  name="txtBranchName" id="txtBranchName"  placeholder="EMP Branch Name">
                    </div>
                    <div class="col-sm-4 form-group"> 
                        <label for="ddreceipt">Upload DD Image<span class="star">*</span></label>
                        <input type="file" class="form-control"  name="ddreceipt" id="ddreceipt" onchange="checkScanForm(this)">
                    </div>     

                </div>



                <div class="container">
                    <div class="col-sm-4"> 

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Pay Now"/>   

                    </div>

                </div>





        </div>
    </div>   
</div>


</form>


</div>
</div>  



</div>




</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>
<script type="text/javascript">
    $('#dddate').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true
                // autoclose: true
    });
</script>

<script language="javascript" type="text/javascript">
function checkScanForm(target) {
	var ext = $('#ddreceipt').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("ddreceipt").value = '';
			return false;
		}

    if(target.files[0].size > 200000) {
			
        //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
		alert("Image size should less or equal 200 KB");
		document.getElementById("ddreceipt").value = '';
        return false;
    }
	else if(target.files[0].size < 100000)
			{
				alert("Image size should be greater than 100 KB");
				document.getElementById("ddreceipt").value = '';
				return false;
			}
    document.getElementById("ddreceipt").innerHTML = "";
    return true;
}
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {
        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                }
            });
        }
        GenerateUploadId();

        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        FillBankDistrict();

        $("#ddlBankDistrict").change(function () {
            FillBankName(this.value);
        });
        function FillBankName(districtid) {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKNAME&districtid=" + districtid,
                success: function (data) {
                    $("#ddlBankName").html(data);
                }
            });
        }

        function showData() {
            $.ajax({
                type: "post",
                url: "common/cfPayment.php",
                data: "action=SHOW&values=" + UserCode + "",
                success: function (data) {
                    //alert($.parseJSON(data)[0]['OwnerName']);
                    data = $.parseJSON(data);
                    // document.getElementById('txtCenterCodediv').innerHTML = CenterCode;
                    ////  document.getElementById('amountdiv').innerHTML = amount;
                    ////  document.getElementById('firstnamediv').innerHTML = data[0].OwnerName;
                    //  document.getElementById('phonediv').innerHTML = data[0].OwnerMobile;
                    //// document.getElementById('emaildiv').innerHTML = email;

                    $('#udf1').val(CenterCode);
                    $('#amount').val(amount);
                    firstname.value = data[0].OwnerName;
                    phone.value = data[0].OwnerMobile;
                    $('#email').val(email);
                    //$('#productinfo').val(PayTypeCode);
                    $('#productinfo').val(PayType);
                }
            });
        }

        showData();

        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfddReexamPayment.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#DDForm").serialize();
            if (Mode == 'Update')
            {

                data = "action=Update&" + forminput; // serializes the form's elements.

                //alert(data);
            }
            else
            {

            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmreexampayment.php";
                        }, 3000);
                        //
                        //                        Mode = "Add";
                        //                        resetForm("frmOrgDetail");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>




<!--<script src="scripts/ddadmissionupload.js"></script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>-->
</html> 