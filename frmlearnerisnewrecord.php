<?php
$title = "Learner IsNewRecord";
include ('header.php');
include ('root_menu.php');

echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
if($_SESSION['User_UserRoll']=='8' || $_SESSION['User_UserRoll']=='28' || $_SESSION['User_UserRoll']=='1') {
?>
<style>
 .modal {z-index: 1050!important;} 
 #turmcandition{
       background-color: whitesmoke;
    text-align: justify;
    padding: 10px;
 }
 .noturm{
         margin-left: 10px !important;
 }
 .yesturm{
     margin-right: 15px;
 }
 </style>
<div style="min-height:430px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading"> Update Learner IsNewRecord Status For iLearn Transfer</div>
            <div class="panel-body">
                <form name="frmIsnewrecord" id="frmIsnewrecord" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <label for="batch"> Enter Learner Code:<span class="star">*</span></label>
                                <input type='text' id="ddllot" name="ddllot" class="form-control" required="required"/>
                            </div> 
                        </div>
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                            </div>
                        </div>
                        <div id="result" style="margin-top:5px; width:94%;"> </div>
                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
$(document).ready(function () {
    
        $("#btnSubmit").click(function () {
            if ($("#frmIsnewrecord").valid())
            {
               ShowLearnerEcertificate();
            }
            return false; 
        });
        function ShowLearnerEcertificate() {
            var lcode = $("#ddllot").val();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cflearnerisnewrecord.php";      
            var data;
            data = "action=UpdateINewRecord&lcode=" + lcode + "";         
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    $('#response').empty();
                    $("#result").html(data);
                }
            });
        }
});

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>