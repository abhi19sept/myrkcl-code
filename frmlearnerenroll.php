<?php
    session_start();
    $title = "Bio-Matric";
    include('header.php');
    //include('root_menu.php');
    if (isset($_REQUEST['code'])) {
        echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
        echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    } else {
        echo "<script>var UserCode=0</script>";
        echo "<script>var Mode='Add'</script>";
    }

?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">First time capture may take time, so wait after click the button "Click To
                Capture"
            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmLearnerCapture" id="frmLearnerCapture" class="form-inline" role="form"
                      enctype="multipart/form-data">

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>
                        <div id="errorBox"></div>
                    </div>

                    <!--<div class="container">
                        <div class="col-md-12 form-group">
                            <label for="deviceType">Select Device Make<span class="star">*</span></label>
                            <select id="deviceType" name="deviceType" class="form-control"
                                    onchange="javascript:callAPI_initialize(this.value)">
                                <option value="n">- - - Please Select - - -</option>
                            </select>
                        </div>
                    </div>-->

                    <div id="showData" style="display: none;">
                        <div class="container">
                            <div class="col-md-1">
                                <img id="imgFinger" name="imgFinger" class="form-control"
                                     style="width:80px;height:107px;"
                                     alt=""/>
                            </div>
                            <div id="grid" name="grid"></div>
                        </div>

                        <div style="display:none;">
                            <div class="container">
                                <div class="col-md-4 form-group">
                                    <label for="batch"> Serial No:<span class="star">*</span></label>
                                    <input type="text" name="tdSerial" id="tdSerial" class="form-control"
                                           readonly="true"/>

                                    <input type="hidden" name="txtLCourse" id="txtLCourse" class="form-control"
                                           readonly="true"/>

                                    <input type="hidden" name="txtLBatch" id="txtLBatch" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Make:<span class="star">*</span></label>
                                    <input type="text" name="tdMake" id="tdMake" class="form-control" readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Model:<span class="star">*</span></label>
                                    <input type="text" name="tdModel" id="tdModel" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Width:<span class="star">*</span></label>
                                    <input type="text" name="tdWidth" id="tdWidth" class="form-control"
                                           readonly="true"/>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-md-4 form-group">
                                    <label for="batch"> Height:<span class="star">*</span></label>
                                    <input type="text" name="tdHeight" id="tdHeight" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Local MAC:<span class="star">*</span></label>
                                    <input type="text" name="tdLocalMac" id="tdLocalMac" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Local IP:<span class="star">*</span></label>
                                    <input type="text" name="tdLocalIP" id="tdLocalIP" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Status:<span class="star">*</span></label>
                                    <input type="text" name="txtStatus" id="txtStatus" class="form-control"
                                           readonly="true"/>
                                </div>
                            </div>

                        </div>


                        <div class="container" style="display:none">
                            <div class="col-md-4 form-group">
                                <label for="batch"> Quality:<span class="star">*</span></label>
                                <input type="text" name="txtQuality" id="txtQuality" value="" class="form-control"
                                       readonly="true"/>
                                <input type="hidden" name="admission_code" id="admission_code"
                                       value="<?php echo $_REQUEST['code']; ?>">
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> NFIQ:<span class="star">*</span></label>
                                <input type="text" name="txtNFIQ" id="txtNFIQ" value="" class="form-control"
                                       readonly="true"/>
                            </div>
                        </div>

                        <div style="display:none;">
                            <div class="container">
                                <div class="col-md-10 ">
                                    <label for="batch"> Base64Encoded ISO Template:<span class="star">*</span></label>
                                    <textarea id="txtIsoTemplate" rows="4" cols="12" name="txtIsoTemplate"
                                              class="form-control" readonly="true"></textarea>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-md-10">
                                    <label for="batch"> Base64Encoded ISO Image:<span class="star">*</span></label>
                                    <textarea id="txtIsoImage" rows="4" cols="12" name="txtIsoImage"
                                              class="form-control"
                                              readonly="true"></textarea>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-md-10">
                                    <label for="batch"> Base64Encoded Raw Data:<span class="star">*</span></label>
                                    <textarea id="txtRawData" rows="4" cols="12" name="txtRawData" class="form-control"
                                              readonly="true"></textarea>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-md-10">
                                    <label for="batch"> Base64Encoded Wsq Image Data:<span class="star">*</span></label>
                                    <textarea id="txtWsqData" rows="4" cols="12" name="txtWsqData" class="form-control"
                                              readonly="true"></textarea>
                                </div>
                            </div>
                        </div>


                        <div class="container" style="margin-top:15px;">
                          <input type="button" name="btnVerify" id="btnVerify" class="btn btn-info"
                                   value="Verify FingerPrint Duplicity" style="display: none;" />
                
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-info"
                                   style="display:none" value="Submit"/>
                        </div>
                    </div>

            </div>
        </div>
    </div>
    </form>
</div>
<?php
    //include('footer.php');
    include 'common/message.php';
    include 'common/modals.php';
    $mac = explode(" ", exec('getmac'));
?>
</body>
<script src="js/mfs100.js"></script>
<script src="js/star.js"></script>
<script src="js/cogent.js"></script>
<script src="js/tatvik.js"></script>
<script language="javascript" type="text/javascript">

    window.myCallback = function (data) {
        console.log(data);
    };

    var localIP = "<?php echo getHostByName(getHostName());?>";
    var localMAC = "<?php echo str_ireplace('-', '', $mac[0]);?>";
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    var quality = 60; //(1 to 100) (recommanded minimum 55)
    var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

    /*** FILL MACHINES LISTING HERE ***/
    function FillMachines() {
        $.ajax({
            type: "post",
            url: "common/cfbiomatricenroll.php",
            data: "action=FillMachines",
            success: function (data) {
                //alert(data);
                $("#deviceType").html(data);
            }
        });
    }

    //FillMachines();

    $(document).ready(function () {

        var url = "common/cfbiomatricenroll.php";

        $.ajax({
            type: "POST",
            url: url,
            data: "action=GetLearner&values=" + AdmissionCode + "",
            success: function (data) {
                $("#grid").html(data);
            }
        });

        var url = "common/cfbiomatricenroll.php";

        $.ajax({
            type: "POST",
            url: url,
            data: "action=GetLearnerCourse&values=" + AdmissionCode + "",
            success: function (data) {
                //alert(data);
                txtLCourse.value = data;
				var url = "common/cfbiomatricenroll.php";

        $.ajax({
            type: "POST",
            url: url,
            data: "action=GetLearnerBatch&values=" + AdmissionCode + "",
            success: function (data) {
                //alert(data);
                txtLBatch.value = data;
				           window.setTimeout(function () {
                     $("#btnVerify").show();
                    }, 3000);

            }
        });
            }
        });



        $("#btnVerify").click(function () {

            var isotemplate = "";

            isotemplate = $("#txtIsoTemplate").val();

            var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
            // $("#matching_records").modal("show");
            $.ajax({
                type: "POST",
                url: url,
                data: "action=GetExistingLearnerData&course=" + txtLCourse.value + "&batch=" + txtLBatch.value + "",
                success: function (data) {
                    if (data == "") {
                        $("#matching_records").modal("hide");
                        $("#duplicity_checked").modal("show");
                        $("#btnSubmit").show();
                        $("#btnVerify").hide();
                    } else {

                        data = $.parseJSON(data);
                        var flag = 0;

                        <?php

                        /*** IF MANTRA MACHINE SELECTED ***/

                        if($_SESSION['machineID'] == "m"){

                        ?>

                        try {
                            $.each(data, function (i, item) {
                                var res = VerifyFinger(isotemplate, data[i].BioMatric_ISO_Template);
                                if (res.httpStaus) {
                                    if (res.data.Status) {
                                        flag = 1;
                                    }
                                    else {
                                        if (res.data.ErrorCode != "0") {
                                            alert("Error description = " + res.data.ErrorDescription);
                                        }/*
                                        else {
                                            alert("Finger not matched");
                                        }*/
                                    }
                                }
                                else {
                                    alert(res.err);
                                }

                            });
                            if (flag == 1) {
                                window.parent.opener.location.reload();
                                $("#matching_records").modal("hide");
                                $("#already_enrolled").modal("show");
                                // window.setTimeout("window.close();", 3000);
                            }
                            else {
                                $("#matching_records").modal("hide");
                                $("#duplicity_checked").modal("show");
                                $("#btnSubmit").show();
                                $("#btnVerify").hide();
                            }
                        }
                        catch (e) {
                            alert(e);
                        }
                        return false;
                        //});
                        <?php
                        }
                        ?>

                        <?php

                        /*** IF STARTEK MACHINE SELECTED ***/

                        if($_SESSION['machineID'] == "s"){
                        ?>

                        var matchCountStar = 0;

                        $.each(data, function (i, item) {
                            crossDomainAjax_capture("https://localhost:4443/FM220/GetMatchResult?MatchTmpl=" + encodeURIComponent(data[i].BioMatric_ISO_Template) + "&callback=?",
                                function (result) {
                                    if (result.errorCode == 0) {

                                        matchCountStar += 1;

                                        /*** ALREADY ENROLLED ****/

                                        window.parent.opener.location.reload();
                                        $("#matching_records").modal("hide");
                                        $("#already_enrolled").modal("show");
                                       // window.setTimeout("window.close();", 3000);

                                        return false;
                                    }
                                    else {

                                        if (result.status == "Finger not placed. Time Out!" || result.status == "Capture Failed! Time Out!"){
                                            $("#error_modal_timeout").modal("show");
                                            return false;
                                        }


                                        if (result.status == "Match Failed!") {

                                            /*** DUPLICITY CHECKED ***/
                                            $("#matching_records").modal("hide");
                                            $("#duplicity_checked").modal("show");
                                            $("#btnSubmit").show();
                                            $("#btnVerify").hide();
                                        }
                                    }
                                });
                        });

                        <?php
                        }
                        ?>
                        <?php

                        /*** IF TATVIK MACHINE SELECTED ***/

                        if($_SESSION['machineID'] == "t"){

                        ?>
                        console.log(data)
                        var referenceTemplateData = $('#txtIsoTemplate').val();
                        $.each(data, function (i, item) {
                            var matchtemplates =  "<MatchTemplates>"+
                                "<ClaimedTemplateData>" + data[i].BioMatric_ISO_Template + "</ClaimedTemplateData>"+
                                "<ReferenceTemplateData>"+ referenceTemplateData +"</ReferenceTemplateData>"+
                                "</MatchTemplates>";


                            $.ajax({
                                url: "https://127.0.0.1:31000",

                                data: matchtemplates,
                                method: "MATCHFPTEMPLATE",
                                responsetype:'xml',
                                // jsonpCallback: "Jsonp_Callback",
                                success: function (data, success) {
                                    var rd = xml2json(data);
                                    var rd2 = rd.MatchResult;
                                    console.log(rd2)
                                    if (rd2.MatchStatus == 'Match Failed') {


                                        /*** DUPLICITY CHECKED ***/
                                        $("#matching_records").modal("hide");
                                        $("#duplicity_checked").modal("show");
                                        $("#btnSubmit").show();
                                        $("#btnVerify").hide();
                                    }
                                    else {

                                        if (rd2.MatchStatus == "Finger not placed. Time Out!" || rd2.MatchStatus == "Capture Failed! Time Out!")
                                            $("#error_modal_timeout").modal("show");

                                        if (rd2.MatchStatus == "Match succesful") {

                                            /*** ALREADY ENROLLED ****/

                                            window.parent.opener.location.reload();
                                            $("#matching_records").modal("hide");
                                            $("#already_enrolled").modal("show");
                                         alert("This Finger Is Already Enrolled");
                                            window.setTimeout("window.close();", 3000);
                                        }
                                    }
                                },
                                error: function (jqXHR, exception) {
                                    if (jqXHR.status === 0) {
                                        alert('Not connect.\n Verify Network.');
                                    } else if (jqXHR.status == 404) {
                                        alert('Requested page not found. [404]');
                                    } else if (jqXHR.status == 500) {
                                        alert('Internal Server Error [500].');
                                    } else if (exception === 'parsererror') {
                                        alert('Requested JSON parse failed.');
                                    } else if (exception === 'timeout') {
                                        alert('Time out error.');
                                    } else if (exception === 'abort') {
                                        alert('Ajax request aborted.');
                                    } else {
                                        alert('Uncaught Error.\n' + jqXHR.responseText);
                                    }
                                }
                            });

                        });

                        <?php
                        }
                        ?>

                        <?php

                        /*** IF COGENT MACHINE SELECTED ***/

                        if($_SESSION['machineID'] == "c"){
                        ?>

                        $("#please_wait_verify_duplicity").modal("show");
                        //match_cogent(isotemplate, data);

                        var thresholdScore = 5000, matched = 0;


                        var i = 0, howManyTimes = data.length;

                        function f() {

                            var templatefromdb = data[i].BioMatric_ISO_Template;

                            $.ajax({
                                type: "GET",
                                url: "https://localhost:62016/MMMCSD200Service/ActionServlet?action=MatchFingers&byteImage1=" + isotemplate + "&byteImage2=" + templatefromdb,
                                dataType: "jsonp",
                                crossDomain: true,
                                crossOrigin: true,
                                header: {'Access-Control-Allow-Origin': '*'},
                                success: function (jsonObj2) {

                                    if (jsonObj2.Message !== "Unable to match the fingers. One of the templates is NULL.") {
                                        if (jsonObj2.Message === "-21") {

                                            $("#wait_modal_match").modal("hide");
                                            $("#errorText").html('Error in Capturing the fingerprint, please try ' +
                                                'again');
                                            $("#errorModal_Custom").modal('show');
                                            window.parent.opener.location.reload();
                                            window.setTimeout("window.close();", 3000);
                                        } else {
                                            if (jsonObj2.Message >= thresholdScore) {

                                                /*** ALREADY ENROLLED ****/

                                                matched = 1;


                                            }
                                            else {

                                                /*** DUPLICITY CHECKED ***/
                                            }
                                        }
                                    } else {
                                        alert("Else error = " + jsonObj2.Message);
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    alert("error in " + i + " :::::::  " + errorThrown);
                                }
                            });

                            i++;
                            if (i < howManyTimes) {
                                setTimeout(f, 1000);
                            }
                        }

                        f();


                        setTimeout(function () {

                            $("#please_wait_verify_duplicity").modal("hide");

                            if (matched == 1) {
                                window.parent.opener.location.reload();
                                $("#matching_records").modal("hide");
                                $("#already_enrolled").modal("show");
                                window.setTimeout("window.close();", 3000);
                            } else {
                                $("#matching_records").modal("hide");
                                $("#duplicity_checked").modal("show");
                                $("#btnSubmit").show();
                                $("#btnVerify").hide();
                            }
                        }, 2000);




                        <?php
                        }?>
                    }
                }
            });
        });

        $("#btnSubmit").click(function () {
            //alert("hhkj");
            $("#please_wait").modal("show");
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmLearnerCapture").serialize();

            data = "action=Add&" + forminput;
            //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate) {
                       $("#please_wait").modal("hide");
                        $("#successfully_submitted").modal("show");
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.parent.opener.location.reload();
                        window.setTimeout("window.close();", 3000);
                        Mode = "Add";
                        //opener.location.reload();

                        //resetForm("frmLearnerCapture");
                        //window.close();
                    }
                    else {
                        $("#errorText").empty();
                        $("#errorText").html(data);
                        $("#please_wait").modal("hide");
                        $("#errorModal_Custom").modal("show");
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
    });

    function initializeMantra_capture() {
        $("#wait_modal_match").modal("show");
        try {
            document.getElementById('txtStatus').value = "";
            document.getElementById('imgFinger').src = "data:image/bmp;base64,";
            document.getElementById('txtQuality').value = "";
            document.getElementById('txtNFIQ').value = "";
            document.getElementById('txtIsoTemplate').value = "";
            document.getElementById('txtIsoImage').value = "";
            document.getElementById('txtRawData').value = "";
            document.getElementById('txtWsqData').value = "";

            var res = CaptureFinger(quality, timeout);
            if (res.httpStaus) {
                if (res.data.ErrorCode == "0") {
                    $("#wait_modal_match").modal("hide");
                    $("#showData").css("display", "block");
                    document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;
                    document.getElementById('txtQuality').value = res.data.Quality;
                    document.getElementById('txtNFIQ').value = res.data.Nfiq;
                    document.getElementById('txtIsoTemplate').value = res.data.IsoTemplate;
                    document.getElementById('txtIsoImage').value = res.data.IsoImage;
                    document.getElementById('txtRawData').value = res.data.RawData;
                    document.getElementById('txtWsqData').value = res.data.WsqImage;
                } else {
                    $("#wait_modal_match").modal("hide");
                    $("#error_modal_timeout").modal("show");
                    $("#showData").css("display", "hide");
                    window.parent.opener.location.reload();
                    window.setTimeout("window.close();", 3000);
                }
            }
            else {
                $("#wait_modal_match").modal("hide");
                $("#error_modal").modal("show");
                $("#showData").css("display", "hide");
                window.parent.opener.location.reload();
                window.setTimeout("window.close();", 3000);
            }
        }
        catch (e) {
            $("#wait_modal_match").modal("hide");
            $("#error_modal").modal("show");
            $("#showData").css("display", "hide");
            window.parent.opener.location.reload();
            window.setTimeout("window.close();", 3000);
        }
        return false;
    }

    function callAPI_initialize(x) {
        $("#showDevice").css("display", "none");
        if (x != "n") {
            switch (x) {
                case "m":
                    initializeMantra_capture();
                    break;

                case "s":
                    initializeStartek_capture();
                    break;

                case "c":
                    intitializeCogent_capture();
                    break;
                    
                case "t":
                    initializeTatvik_capture();
                    break;
                    
                default:
                    break;
            }
        } else {
            $("#showDevice").css("display", "none");
            $("#showData").css("display", "none");
        }
    }

    callAPI_initialize("<?php echo $_SESSION['machineID']?>");
</script>
</html>