<?php
$title = "Ownership Change Fee Payment";
include ('header.php');
include ('root_menu.php');

echo "<script>var Mode='Add'</script>";

require("razorpay/checkout/manual.php");
?>
<div id="googleMap"  class="mapclass"></div>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Ownership Change Fee Payment</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmownershipfee" id="frmownershipfee" role="form" action="" class="form-inline" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>

                    <div id="main-content" style="display:none;">
                        <div class="container">
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Organization/Center:</label>
                                <input type="text" class="form-control" readonly="" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">

<!--<input type="hidden" class="form-control" maxlength="50" name="txtCenterCode" id="txtCenterCode"/>-->
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Registration No:</label>
                                <input type="text" class="form-control" readonly="" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:</label>
                                <input type="text" class="form-control" readonly="" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:</label>
                                <input type="text" class="form-control" readonly="" name="txtType" id="txtType" placeholder="Type Of Organization">  
                            </div>
                        </div>  
                        <br>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Document Type:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Application Type:</label>
                                <input type="text" class="form-control" readonly="true" name="txtRole" id="txtRole"  placeholder="Application Type">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">New Owner Email:</label>
                                <input type="text" class="form-control" readonly="true" name="txtEmail" id="txtEmail"  placeholder="Email">   
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="address">New Owner Mobile:</label>
                                <input type="text" class="form-control" readonly="true" id="txtMobile" name="txtMobile" placeholder="Mobile">
                                <input type="hidden" class="form-control" readonly="true" id="txtAck" name="txtAck" placeholder="Ack">
                            </div>
                        </div>  

                        <div>
                            <input type="hidden" name="paystatus" id="paystatus"/>
                            <input type="hidden" name="txtareatype" id="txtareatype"/>
                            <input type="hidden" name="ack" id="ack"/>
                        </div>

                        <div class="container">
                        <div class="col-sm-4 form-group boxsize">     
                            <label for="batch"> Select Payment Gateway:</label>
                            <select id="gateway" name="gateway" class="form-control">
                                <option value="">Select</option>
                                <option value="razorpay">Razorpay</option>
                                <option value="payu">Payu</option>
                            </select>
                        </div>
                        </div>
                        <div class="container">
                            <br><input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Proceed" style="display:none;"/>    
                        </div>

                    </div>
                </form>
            </div>
        </div>   
    </div>
</div>
<form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
    <input type="hidden" id="txnid" name="txnid">
</form>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function fillForm()
        {           //alert(val);    
            $.ajax({
                type: "post",
                url: "common/cfOwnershipFeePayment.php",
                data: "action=APPROVE",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " You are Not Eligible for Payment of Ownership Change." + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        txtName1.value = data[0].orgname;
                        txtRegno.value = data[0].regno;
                        txtEstdate.value = data[0].fdate;
                        txtType.value = data[0].orgtype;
                        txtDocType.value = data[0].doctype;
                        txtRole.value = data[0].orgcourse;

                        txtEmail.value = data[0].email;
                        txtMobile.value = data[0].mobile;
                        txtAck.value = data[0].ack;
                        $('#main-content').show(3000);
                        CheckPayStatus();
                    }

                }
            });
        }
        fillForm();

        function CheckPayStatus() {
            $.ajax({
                type: "post",
                url: "common/cfOwnershipFeePayment.php",
                data: "action=CheckPayStatus&values=" + txtAck.value + "",
                success: function (data) {
                    paystatus.value = data;
                    if (data == '0') {
                        ChkPayEvent();
                    } else {
                        return false;
                    }
                }
            });
        }

        function ChkPayEvent() {
            $.ajax({
                type: "post",
                url: "common/cfOwnershipFeePayment.php",
                data: "action=ChkPayEvent",
                success: function (data) {
                    var paystatus = data;
                    if (paystatus == "1") {
                        $('#btnSubmit').show();
                    }
                }
            });
        }

        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $('#btnSubmit').hide();
            var url = "common/cfOwnershipFeePayment.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmownershipfee").serialize();

            if (Mode == 'Add') {
                data = "action=ADD&" + forminput;
                $('#txnid').val('');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                       // alert(data);
                        $('#response').empty();
                        if (data == 0 || data == '') {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Something went Wrong. Please try again." + "</span></p>");
                        } else if (data != '') {
                            if (data == 'TimeCapErr') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for Ownership Change , Please try again after 15 minutues.</span></p>");
                                alert('You have already initiated the payment for Ownership Change , Please try again after 15 minutues.');
                            } else {
                                if ($('#gateway').val() == 'razorpay') {
                                    var options = $.parseJSON(data);
<?php include("razorpay/razorpay.js"); ?>
                                } else {
                                    $('#txnid').val(data);
                                    $('#frmpostvalue').submit();
                                }
                            }
                        }
                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
</html>
