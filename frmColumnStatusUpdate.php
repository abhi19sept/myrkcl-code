<?php
/* Hariom Awasthi */
$title = "Column Status Update";
include('header.php');
include('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
//echo "<pre>"; print_r($_SESSION);
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_UserRoll'] == '8'){
?>

<link rel="stylesheet" href="css/profile_style.css">
<div style="min-height:450px !important;">

    <div class="container">

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading" id="non-printable">Column Status Update Dashboard</div>
            <div class="panel-body">
                <form name="frmitgkdetails" id="frmitgkdetails" action="" class="form-inline">

                    <div class="container">


                        <div id="response"></div>
                        <div id="errorBox"></div>

                        <div class="col-sm-4 form-group">
                            <label for="learnercode">Enter LoginId:<span class="star">*</span></label>
                            <input type="text" maxlength="12" class="form-control" name="itgkCode" id="itgkCode" 
								placeholder="Enter LoginId">
                        </div>

                        <div class="col-sm-6 form-group">
                            <label for="columnForUpdate">Select Column to be Update:<span class="star">*</span></label>
                            <select id="columnForUpdate" name="columnForUpdate" class="form-control">
                                <option value=""> - - - Select Column - - - </option>
                                <option value="IsNewRecord">MyRKCL Status</option>
                                <option value="IsNewCRecord">Click Status</option>
                                <option value="IsNewSRecord">Support Status</option>
                                <option value="IsNewOnlineLMSRecord">ILearn Status</option>
                            </select>
                        </div>

                        <div class="container">
                            <label for="itgkCode"></label>
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="UPDATE"
								style="margin-top:25px"/>
                        </div>
                    </div>

                    <div id="gird"></div>
                </form>

            </div>

        </div>
    </div>
</div>

</body>
<?php
include 'footer.php';
include 'common/message.php';
?>

<style>
    #errorBox {
        color: #F00;
    }
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $("#btnSubmit").click(function () {

            if ($("#frmitgkdetails").valid()) {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfSupportStatusUpdate.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmitgkdetails").serialize();
                data = "action=DETAILS&" + forminput; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#response').empty();
                        //$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span></span></p>");
                        $("#gird").html(data);
                        $('#example').DataTable({
                            dom: 'Bfrtip',
                            buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
                        });

                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });


</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmUpdateITGKStatus.js"></script>
<style>
    .error {
        color: #D95C5C !important;
    }
</style>

</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>