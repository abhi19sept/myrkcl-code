<?php
$title = "WCD Batch Learners Teams Logins";
include ('header.php');
include ('root_menu.php');
if (isset($_SESSION)) {
    //echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var Role='" . $_SESSION['User_UserRoll'] . "'</script>";
} else {

    echo "<script>var Mode='Add'</script>";
    echo "<script>var Role='0'</script>";
}
?>		
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">WCD Batch Learners Teams Logins</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmmarkwcdattendance" id="frmmarkwcdattendance" class="form-inline" role="form" method="post">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                    </div>


                    <div id="detailsdiv">				


                        <div class="container"> 
                            <div class="col-md-6 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">
                                    <option value="0" selected>Select Course</option>
                                    <option value="3">RS-CIT Women</option>
                                    <option value="24">RS-CFA Women</option>
                                </select>
                            </div> 

                            <div class="col-md-6 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">

                                </select>
                            </div> 

                            
                        </div>	

                        <div id="grid" name="grid" style="margin-top:35px;"> </div> 
                    </div>

            </div>
        </div>   
    </div>
</form>
</div>

<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
                $.ajax({
                    type: "post",
                    url: "common/cfWCDTeamsLogins.php",
                    data: "action=FILLEventBatch&values=" + selCourse + "",
                    success: function (data) {

                        $("#ddlBatch").html(data);
                    }
                });


        });

        function showAllData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfWCDTeamsLogins.php",
                data: "action=SHOWALL&batch=" + ddlBatch.value + "&course=" + ddlCourse.value + "",
                success: function (data)
                {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $('#btnSubmit').show();

                    //showData();
                }
            });
        }


        $("#ddlBatch").change(function () {
               
            showAllData();
  
        
            // showAllData(this.value, ddlCourse.value);
        });

    });
</script>
</html>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
