<?php
$title = "Payment Summary Category";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['headname'])) {
    echo "<script>var StartDate='" . trim($_REQUEST['sdate']) . "'</script>";
    echo "<script>var EndDate='" . trim($_REQUEST['edate']) . "'</script>";
    echo "<script>var mode='" . $_REQUEST['mode'] . "'</script>";
    echo "<script>var HeadName='" . $_REQUEST['headname'] . "'</script>";
} else {
    echo "<script>var StartDate='0'</script>";
    echo "<script>var EndDate='0'</script>";
    echo "<script>var mode='0'</script>";
    echo "<script>var HeadName='0'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Payment Summary Category</div>
            <div class="panel-body">					
                <form name="frmPaySumCategory" id="frmPaySumCategory" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                    </div>  

                    <div id="grid" style="margin-top:5px;"> </div>      
            </div>
        </div>   
    </div>
</form>

</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";


    $(document).ready(function () {

        $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
        setTimeout(function () {
            $('#grid').load();
        }, 2000);

       
        var url = "common/cfPaymentSummary.php"; // the script where you handle the form input.
//        var data;
//        if (Mode == 'Show')
//        {
           var data = "action=SHOWcategory&startdate=" + StartDate + "&enddate=" + EndDate + "&headname=" + HeadName + "&Mode=" + mode + ""; // serializes the form's elements.				 
            //alert(data);
//        }
//        else
//        {
//        }
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {

                $("#grid").html(data);
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });


                //showData();
            }
        });

        return false; // avoid to execute the actual submit of the form.

    });
</script>
</html>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
