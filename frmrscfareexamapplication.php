<?php ob_start();
$title = "RS-CFA Re-Exam Application";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var FunctionCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">RS-CFA Re-Exam Application</div>
            <div class="panel-body">
                <form name="frmrscfareexamapplication" id="frmrscfareexamapplication" class="form-inline" role="form"
					enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">

                            </select>
                        </div> 
                    </div>  
                   
                    <div class="container">
                        <div class="col-md-6 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>
                        </div> 

                    </div>
                    <div id="grid" name="grid" style="margin-top:35px;"></div> 
                    
            </div>
        </div>   
    </div>
</form>
</body>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>                
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillCourse() {
			$('#response').empty();
            $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfRscfaReexamApplication.php",
                data: "action=FillCourse",
                success: function (data) {						
					if(data=='1'){
						$('#response').empty();
                        $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + "   You are not authorised for RS-CFA Course." + "</span></p>");
					}else{
						$('#response').empty();
						$("#ddlCourse").html(data);
					}
                    
                }
            });
        }
        FillCourse();
		
		$("#ddlCourse").change(function () {
            var selCourse = $(this).val();          
                $.ajax({
                    type: "post",
                    url: "common/cfRscfaReexamApplication.php",
                    data: "action=FillBatch&values=" + selCourse + "",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });
            
        });
		
        function showAllData(val, val1) {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfRscfaReexamApplication.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                    
                }
            });
        }
        $("#ddlBatch").change(function () {
            //alert(this.value);
            showAllData(this.value, ddlCourse.value);

        });
		
			$("#grid").on('click', '.updcount', function () {
				AckCode = $(this).attr('AckCode');
				CourseCode = $(this).attr('CourseCode');
				BatchCode = $(this).attr('BatchCode');
				$(this).prop("disabled", true);
				$('#response').empty();
				$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
					$.ajax({
                    type: "post",
                    url: "common/cfRscfaReexamApplication.php",
                    data: "action=ApplyForReexamApplication&lcode=" + AckCode + "&ccode=" + CourseCode + "&bcode=" + BatchCode + "",
                    success: function (data) {
                         AckCode = "";
                         CourseCode = "";
                         BatchCode = "";
                         // $("#"+myBookId).attr('value', 'Transferred to Ilearn');
						 if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
						{
							$('#response').empty();
							$('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
							window.setTimeout(function () {
								Mode = "Add";
								window.location.href = 'frmrscfareexamapplication.php';
							}, 3000);
						} else
						{
							$('#response').empty();
							$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
						}
                    }
                });

        });
        
    });

    

</script>
</html>