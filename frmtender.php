<?php
    $title="RKCL-Dashboard";
    include ('header.php'); 
    include ('root_menu.php');
    include ('common/message.php');
?>
<link href="css/slidermodel.css" rel="stylesheet">	
<div style="min-height:350px !important;max-height:1500px !important">	
    <div class="container" style=" margin-top:50px">
    <div class="row">
    <div class="col-md-12" >
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <span class="glyphicon glyphicon-bookmark"></span> Tender </h3>
        </div>
        <div class="panel-body">
            <div class="col-md-12 bhoechie-tab-container">
                <div class="col-md-10 bhoechie-tab" style="width: 100%;">
                  <div class="bhoechie-tab-content active">
                    <center>
                        <div class="panel panel-default credit-card-box">
                          <ul class="tab">
                              <li><a href="javascript:void(0)" id="showsliderdata" class="tablinks active" onclick="openCity(event, 'View')">View</a></li>
                              <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Add')">Add New</a></li>
                            </ul>
                            <div id="View" class="tabcontent" style="display: block;">
                              <h3>Current Tender List</h3>
                              <div id="grid" style="margin-top:15px;"></div> 
                            </div>
                            <div id="Add" class="tabcontent">
                              <h3>Add New Tender</h3>
                              <form class="form-horizontal" action="common/cftender.php" name="sliderForm" id="sliderForm" enctype="multipart/form-data">
                                  <input type="hidden" name="action" id="action" value="Addslider"/>
                                  <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">Tender Tittle</label>
                                      <div class="col-sm-6">
                                          <input class="form-control" id="imageTittle" name="imageTittle" placeholder="Enter Tender Tittle" type="text" required/>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
                                      <div class="col-sm-6">
                                          <select class="form-control" id="sliderStatus" name="sliderStatus" required>
                                              <option value="">--Select Status--</option>
                                              <option value="active">Active</option>
                                              <option value="deactive">Deactive</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label for="inputPassword3" class="col-sm-3 control-label">Attach File</label>
                                      <div class="col-sm-6">
                                          <input class="form-control" id="silederImage" name="silederImage" type="file" required/>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <div class="col-sm-offset-3 col-sm-6" id="response"></div>
                                  </div>
                                  <div class="form-group last">
                                      <div class="col-sm-offset-3 col-sm-6">
                                          <button type="submit" name="btn-slider-submit"  id="btn-slider-submit" class="btn btn-success btn-sm">Submit</button>
                                          <button type="reset" class="btn btn-default btn-sm">Reset</button>			
                                      </div>
                                  </div>
                              </form>
                            </div>
                        </div>
                    </center>
                  </div>
                 </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
</div>
<!-- View Policy Popup -->
<div id="myModalimage" class="modal">
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h6>Current Tender Description File</h6>
    </div>
      <div class="modal-body" style="text-align: center;">
        <iframe id="filesrc"  src="" style="width: 100%;height: 500px;border: none;"></iframe>
    </div>
  </div>
</div>
<!-- End View Policy-->

<!-- Update Policy -->
<div id="myModalupdateslider" class="modal">
  <div class="modal-content">
    <div class="modal-header">
      <span class="close update">&times;</span>
      <h2>Update Tender</h2>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" action="common/cftender.php" name="sliderFormupdate" id="sliderFormupdate" enctype="multipart/form-data">
        <input type="hidden" name="action" id="action" value="Updateslider"/>
        <input type="hidden" name="sliderid" id="sliderid" value=""/>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Tender Tittle</label>
            <div class="col-sm-6">
                <input class="form-control" id="imageTittleupdate" name="imageTittleupdate" placeholder="Enter Tender Tittle" type="text" required/>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Status</label>
            <div class="col-sm-6">
                <select class="form-control" id="sliderStatusupdate" name="sliderStatusupdate" required>
                    <option value="">--Select Status--</option>
                    <option value="active">Active</option>
                    <option value="deactive">Deactive</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-3 control-label">Attach File</label>
            <div class="col-sm-6">
                <input type="hidden" id="oldimagefile" name="oldimagefile" value="" />
                <input class="form-control" id="silederImageupdate" name="silederImageupdate" type="file" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6" id="responseupdate"></div>
        </div>
        <div class="form-group last">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" name="btn-sliderupdate-submit"  id="btn-sliderupdate-submit" class="btn btn-success btn-sm">Update</button>			
            </div>
        </div>
    </form>
    </div>
    <div class="modal-footer">
      <h3>Update Tender</h3>
    </div>
  </div>
</div>
<!-- End Update Policy Popup -->

<script>
// Message Varibale...
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
var Success = "<?php echo Message::SuccessfullyFetch; ?>";

//Add New Policy...... 
$("#sliderForm").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({ 
        url: "common/cftender.php",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data)
          { 
              if (data == SuccessfullyInsert)
                { //alert(data);
                    $('#response').empty();
                    $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>Tender Added Successfully..</span></div>");
                     window.setTimeout(function () {
                        $('#response').empty();
                        $("#showsliderdata").click();
                    }, 3000);
                   $('#imageTittle').val("");
                   $('#sliderStatus').val("");
                   $('#silederImage').val("");
                }
                else
                {
                    $('#response').empty();
                    $('#response').append("<div class='alert-success'><span><img src=images/warning.jpg width=10px /></span><span>Error To add Tender Please Try Again</span></div>");
                    window.setTimeout(function () {
                        $('#response').empty();
                        $("#showsliderdata").click();
                    }, 3000);
                    //resetForm("newsForm");
                  $('#imageTittle').val("");
                   $('#sliderStatus').val("");
                   $('#silederImage').val("");
                }
          },
            error: function(e) 
            {
               $("#errmsg").html(e).fadeIn();
            } 	        
    });
}));

//View Data.....Show
$(document).ready(function(){
    $("#showsliderdata").click();
});
$("#showsliderdata").click(function () {
    $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
    setTimeout(function(){$('#grid').load();}, 2000); 
    var url = "common/cftender.php"; // the script where you handle the form input.
    var data;
    data = "action=SHOW&status=1"; // serializes the form's elements.				 
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (data)
        {
            $("#grid").html(data);
            $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
            });
            data = $.parseJSON(data);
            filesrc.value = data[0].filepath;
        }
    });
});

//View Policy Popup .............
$("#grid").on("click",".viewimage",function(){
    var  mybtn = $(this).attr("id");
    var mybtnid = "myBtn_"+mybtn;
    var fileab = '#file_id_'+mybtn;
    var file_id = $(fileab).val();
    var url = "upload/tender/"+file_id;
    $('#filesrc').attr('src',url);
    var modal = document.getElementById('myModalimage');
    var btn = document.getElementById(mybtnid);
    var span = document.getElementsByClassName("close")[0];
    modal.style.display = "block";
    span.onclick = function() { 
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
});

//Deleting A Record From Table And File Unlink....
$("#grid").on("click",".fun_delete_slider",function(){
    var del_id = $(this).attr("id");
    var fileab = '#file_id_'+del_id;
    var file_id = $(fileab).val();
    $('#response').empty();
    var result = confirm('Are You Sure Want To Delete This Tender');
     if (result == true) {
        $.ajax({
            type: "post",
            url: "common/cftender.php",
            data: "action=DELETE&deleteid=" + del_id + "&fileid=" + file_id + "",
            success: function (data) {
                $("#showsliderdata").click();
            }
        });
    }else {
        return false;
    }
});

//Update Policy Popup .............
$("#grid").on("click",".fun_update_slider",function(){
    var  mybtn = $(this).attr("id");
    var mybtnid = "myBtn_"+mybtn;
    var modal = document.getElementById('myModalupdateslider');
    var btn = document.getElementById(mybtnid);
    var span = document.getElementsByClassName("update")[0];
    modal.style.display = "block";
    span.onclick = function() { 
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
});

//Fill Form To Update....
$("#grid").on("click",".fun_update_slider",function(){
        var edit_id = $(this).attr("id");
        var fileab = '#file_id_'+edit_id;
        var file_id = $(fileab).val();
        var url = "common/cftender.php"; // the script where you handle the form input.
        var data;
        data = "action=EDIT&editid=" + edit_id + "&fileid=" + file_id + ""; // serializes the form's elements.				 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {  
                data = $.parseJSON(data);
                sliderid.value = data[0].sliderid;
                imageTittleupdate.value = data[0].imagetittle;
                sliderStatusupdate.value = data[0].status;
                oldimagefile.value = data[0].photo;
            }
        });
});

//Update Policy......
$("#sliderFormupdate").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({ 
        url: "common/cftender.php",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data)
          { 
				if (data == SuccessfullyUpdate)
                {
                    $('#responseupdate').empty();
                    $('#responseupdate').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>Tender Update Successfully..</span></div>");
                     window.setTimeout(function () {
                        $('#responseupdate').empty();
                        $("#myModalupdateslider").css({"display": "none"});
                    }, 1000);
                    $("#showsliderdata").click();
                    $('#sliderid').val("");
                    $('#imageTittleupdate').val("");
                    $('#sliderStatusupdate').val("");
                    $('#oldimagefile').val("");
                    $('#silederImageupdate').val("");
				}
				else
                {
					$('#responseupdate').empty();
                    $('#responseupdate').append("<div class='alert-success'><span><img src=images/warning.jpg width=10px /></span><span>Error To add Tender Please Try Again</span></div>");
                    window.setTimeout(function () {
                        $('#responseupdate').empty();
                        $("#showsliderdata").click();
                    }, 3000);
				}
          },
            error: function(e) 
            {
               $("#errmsg").html(e).fadeIn();
            } 	        
    });
}));

// Tab Events Changes Script....    
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
</body>
<?php include ('footer.php'); ?>
</html>
   