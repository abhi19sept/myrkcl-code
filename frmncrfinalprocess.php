<?php
$title = "Verify NCR Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code='" . $_REQUEST['code'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var Ack='" . $_REQUEST['ack'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div id="googleMap"  class="mapclass"></div>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Verify NCR Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmOrgMaster" id="frmOrgMaster" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <!--                        <div class="col-sm-4 form-group">     
                                                    <label for="ackno">Enter AO Center Code:<span class="star">*</span></label>
                                                    <input type="text" class="form-control" maxlength="18" onkeypress="javascript:return allownumbers(event);" name="ackno" id="ackno" placeholder="AO Center Code" required>
                        
                                                </div>
                        
                                                <div class="col-sm-4 form-group">                                  
                                                    <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                                                </div>-->
                    </div>


                    <div id="main-content" style="display:none;">
                        <div class="container">
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Organization/Center:</label>
                                <input type="text" class="form-control" readonly="" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Registration No:</label>
                                <input type="text" class="form-control" readonly="" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:</label>
                                <input type="text" class="form-control" readonly="" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:</label>
                                <input type="text" class="form-control" readonly="" name="txtType" id="txtType" placeholder="Type Of Organization">  
                            </div>
                        </div>  
                        <br>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Document Type:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                                <input type="hidden" class="form-control"  name="txtDistrictCode" id="txtDistrictCode">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Tehsil:</label>
                                <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="address">Address:</label>
                                <textarea class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road"></textarea>

                            </div>
                        </div>  

                        <div class="panel panel-info">
                            <div class="panel-heading">Organization Details</div>
                            <div class="panel-body">
                                <div class="container">
                                    <div class="col-sm-4 form-group" > 
                                        <label for="orgdetail1">Bank Account Details</label> </br>
                                        <button type="button" id="bankdetail" class="btn btn-primary" name="bankdetail" width="150px" height="150px">
                                            View Details</button>	
                                    </div>
                                    <div class="col-sm-4 form-group" > 
                                        <label for="orgdetail1">Owner Details</label> </br>
                                        <button type="button" id="ownerdetail" class="btn btn-primary" name="ownerdetail" width="150px" height="150px">
                                            View Details</button>	
                                    </div>
                                    <div class="col-sm-4 form-group" > 
                                        <label for="orgdetail1">HR Details</label> </br>
                                        <button type="button" id="hrdetail" class="btn btn-primary" name="hrdetail" width="150px" height="150px">
                                            View Details</button>	
                                    </div>
                                    <div class="col-sm-4 form-group" > 
                                        <label for="orgdetail1">System And Intake Details</label> </br>
                                        <button type="button" id="sidetail" class="btn btn-primary" name="sidetail" width="150px" height="150px">
                                            View Details</button>	
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="col-sm-4 form-group" > 
                                        <label for="orgdetail1">Premises Details</label> </br>
                                        <button type="button" id="premisesdetail" class="btn btn-primary" name="premisesdetail" width="150px" height="150px">
                                            View Details</button>	
                                    </div>
                                    <div class="col-sm-4 form-group" > 
                                        <label for="orgdetail1">IT Peripherals Details</label> </br>
                                        <button type="button" id="itdetail" class="btn btn-primary" name="itdetail" width="150px" height="150px">
                                            View Details</button>	
                                    </div>
<!--                                    <div class="col-sm-4 form-group" > 
                                        <label for="orgdetail1">Location Details</label> </br>
                                        <button type="button" id="locationdetail" class="btn btn-primary" name="locationdetail" width="150px" height="150px">
                                            View Details</button>	
                                    </div>-->
                                    <div class="col-sm-4 form-group" > 
                                        <label for="orgdetail1">Hide All Details</label> </br>
                                        <button type="button" id="hideall" class="btn btn-primary" name="hideall" width="150px" height="150px">
                                            Hide Details</button>	
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info" id="bankaccount" style="display:none">
                            <div class="panel-heading">Bank Account Details</div>
                            <div class="panel-body">
                                <div id="bankdatagird"></div>

                            </div>
                        </div>
                        <div class="panel panel-info" id="ownergrid" style="display:none">
                            <div class="panel-heading">Owner Details</div>
                            <div class="panel-body">
                                <div id="ownerdatagrid"></div>

                            </div>
                        </div>
                        <div class="panel panel-info" id="hrgrid" style="display:none">
                            <div class="panel-heading">HR Details</div>
                            <div class="panel-body">
                                <div id="hrdatagrid"></div>
                            </div>
                        </div>
                        <div class="panel panel-info" id="sigrid" style="display:none">
                            <div class="panel-heading">System and Intake Details</div>
                            <div class="panel-body">
                                <div id="sidatagrid"></div>
                            </div>
                        </div>
                        <div class="panel panel-info" id="premisesgrid" style="display:none">
                            <div class="panel-heading">Premises Details</div>
                            <div class="panel-body">
                                <div id="premisesdatagrid"></div>
                            </div>
                        </div>
                        <div class="panel panel-info" id="itgrid" style="display:none">
                            <div class="panel-heading">IT Peripherals Details</div>
                            <div class="panel-body">
                                <div id="itdatagrid"></div>
                            </div>
                        </div>
<!--                        <div class="panel panel-info" id="locationgrid">
                            <div class="panel-heading">Location Details</div>
                            <div class="panel-body">
                                <div id="map" style="width: 100%; height: 300px;"></div> 
                            </div>
                        </div>-->


<div class="container"> 
                                    <div class="col-md-11"> 	

                                        <label for="learnercode"><b>I Accept that I have verified all the details entered by IT-GK during NCR Applicaiton Process. All Details Entered by IT-GK are correct.</b></label><br> <br>  
                                        <div class="container">
                                        <label class="checkbox-inline" style="float:left;"> <input type="checkbox" name="chk" id="chk" value="1" />
                                            I Accept 
                                        </label>
                                        </div>
                                    </div>
                                </div>
<br>
                        <div class="container" id="divsubmit" style="display:none;">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Verify"/>    
                        </div>

                    </div>
            </div>
        </div>   
    </div>
</div>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>-->
 <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_neIgI1ZydFgm3aEVbQmwSfNkaYqE5zo&callback=initMap&sensor=false&libraries=places"
  type="text/javascript"></script>
 

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

$('#chk').change(function () {
            if (this.checked) {
                $('#divsubmit').show();
                $('#chk').attr('disabled', true);
            } else {
                alert("Please select Check Box to Proceed.");
            }
        });
        
        function fillForm()
        {           //alert(val);    
            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=APPROVE&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Some Details are Pending for eligiblity of VERIFICATION" + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        txtName1.value = data[0].orgname;
                        txtRegno.value = data[0].regno;
                        txtEstdate.value = data[0].fdate;
                        txtType.value = data[0].orgtype;
                        txtDocType.value = data[0].doctype;

                        txtDistrict.value = data[0].district;
                        txtDistrictCode.value = data[0].districtcode;
                        txtTehsil.value = data[0].tehsil;
                        //txtStreet.value = data[0].street;
                        txtRoad.value = data[0].road;
                        
                        var lat = data[0].lat;
                        var long = data[0].long;

                        $('#main-content').show(3000);
                        $('#btnShow').hide();
                        $('#ackno').attr('readonly', true);

function initialize() {
   var latlng = new google.maps.LatLng(lat,long);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: false,
      anchorPoint: new google.maps.Point(0, -29)
   });
    var infowindow = new google.maps.InfoWindow();   
    google.maps.event.addListener(marker, 'click', function() {
      var iwContent = '<div id="iw_container">' +
      '<div class="iw_title"><b>Location</b> : Noida</div></div>';
      // including content to the infowindow
      infowindow.setContent(iwContent);
      // opening the infowindow in the current map and at the current marker location
      infowindow.open(map, marker);
    });
}
google.maps.event.addDomListener(window, 'load', initialize);

                    }

                }
            });
        }
        fillForm();

        function showBankAccountData() {

            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=BANKACCOUNT&centercode=" + Code + "",
                success: function (data) {

                    $("#bankdatagird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }
        $("#bankdetail").click(function () {
            showBankAccountData();
            $("#bankaccount").show(3000);
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#locationgrid").hide();
        });

        function showOwnerData() {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=OWNERDATA&centercode=" + Code + "",
                success: function (data) {
//alert(data);
                    $("#ownerdatagrid").html(data);
                    $('#example1').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
//alert(data);
                }
            });
        }

        $("#ownerdetail").click(function () {
            showOwnerData();
            $("#bankaccount").hide();
            $("#ownergrid").show(3000);
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#locationgrid").hide();
        });

        function showHRData() {

            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=HRDATA&centercode=" + Code + "",
                success: function (data) {

                    $("#hrdatagrid").html(data);
                    $('#example2').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }

        $("#hrdetail").click(function () {
            showHRData();
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").show(3000);
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#locationgrid").hide();
        });

        function showSIData() {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=SIDATA&centercode=" + Code + "",
                success: function (data) {
//alert(data);
                    $("#sidatagrid").html(data);
                    $('#example3').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });
        }


        $("#sidetail").click(function () {
            showSIData();
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").show(3000);
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#locationgrid").hide();
        });

        function showPremisesData() {

            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=PREMISESDATA&centercode=" + Code + "",
                success: function (data) {

                    $("#premisesdatagrid").html(data);
                    $('#example4').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });
        }

        $("#premisesdetail").click(function () {
            showPremisesData();
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").show(3000);
            $("#itgrid").hide();
            $("#locationgrid").hide();
        });

        function showITData() {

            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=ITDATA&centercode=" + Code + "",
                success: function (data) {

                    $("#itdatagrid").html(data);
                    $('#example5').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }


        $("#itdetail").click(function () {
            showITData();
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").show(3000);
            $("#locationgrid").hide();
        });
        


        $("#locationdetail").click(function (){
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#locationgrid").show(3000);
        });

        $("#hideall").click(function () {
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#locationgrid").hide();
        });



        $("#btnSubmit").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfNcrFinalApproval.php"; // the script where you handle the form input.            
            var data;
            if (Mode == 'Add')
            {
                data = "action=ADD&centercode=" + Code + "&ack=" + Ack + ""; // serializes the form's elements.
            } else
            {

            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Center Approved Successfully.</span>");
                        window.setTimeout(function () {
                            window.location.href = "frmspfinalapproval.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("form");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });

            return false;
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
</html>
