<?php
session_start();
$title = "Profile";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:450px !important;max-height:1500px !important"> 
    <div class="container" style="margin-top:50px;"> 
        <div class="panel panel-primary">
            <div class="panel-heading">User Profile</div> 
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->

                <form name="form" id="profile" class="form-inline" role="form" enctype="multipart/form-data">    

                    <div class="container">
                        <div id="response"></div>

                        <div id="errorBox"></div>




                        <div class="col-sm-4 form-group">     
                            <label for="FirstName"> FullName:<span class="star">*</span></label>
                            <input type="text" class="form-control" id="txtFirstName" name="txtFirstName" placeholder="UserProfile FirstName">
                        </div>

                        <div class="col-sm-4 form-group">     
                            <label for="HouseNo"> House No:<span class="star">*</span></label>
                            <input type="text" class="form-control" id="txtHouseNo" name="txtHouseNo" placeholder="UserProfile HouseNo" maxlength="12">
                        </div>  
                        <div class="col-sm-4 form-group">     
                            <label for="DOB"> DOB:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="10" id="txtDOB" name="txtDOB" placeholder="dd-mm-yy">
                        </div>
                        <div class="col-sm-4 form-group"> 
                            <label for="MTongue"> Mother Toungue:<span class="star">*</span></label>
                            <select id="ddlMTongue" name="ddlMTongue" class="form-control" >
                                <option value="" selected="selected">Please Select</option> 
                                <option value="English"> English </option>
                                <option value="Hindi"> Hindi </option>
                            </select>    
                        </div>

                    </div>


                    <div class="container">  

                        <div class="col-sm-4 form-group"> 
                            <label for="Area"> Address :<span class="star">*</span></label>
                            <input type="text" class="form-control" id="txtAddress" name="txtAddress" placeholder="Address">
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="Gender"> Gender:<span class="star">*</span></label>
                            <select id="ddlGender" name="ddlGender" class="form-control" >
                                <option value="" selected="selected">Please Select</option> 
                                <option value="Male"> Male </option>
                                <option value="Female"> Female</option>
                            </select>    
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="txtParentTitle"> FatherName:<span class="star">*</span></label>
                            <input type="text" class="form-control" id="txtFatherName" name="txtFatherName" placeholder="FatherName">
                        </div>
                        <div class="col-sm-4 form-group"> 
                            <label for="Qualification"> Qualification:<span class="star">*</span></label>
                            <select id="ddlQualification" name="ddlQualification" class="form-control" >
                                <option value="" selected="selected">Please Select</option> 


                            </select>    
                        </div>


                    </div>



                    <div class="container">
                        <div class="col-sm-4 form-group"> 
                            <label for="Occupation"> Occupation:<span class="star">*</span></label>
                            <input type="text" class="form-control" id="txtOccupation" name="txtOccupation" placeholder="Occupation">
                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="ddl Challenged"> PhysicallyChallenged:</label>
                            <select id="ddlChallenged" name="ddlChallenged" class="form-control" >
                                <option value="" selected="selected">Please Select</option> 
                                <option value="Yes"> Yes </option>
                                <option value="No"> No</option>
                            </select>    
                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="AadharNo"> AadharNo:<span class="star">*</span></label>
                            <input type="text" class="form-control" id="txtAadharNo" name="txtAadharNo" placeholder="AadharNo" maxlength="12">
                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="STDCode"> STDCode:<span class="star">*</span></label>
                            <input type="text" class="form-control" id="txtSTDCode" name="txtSTDCode" placeholder="STDCode" maxlength="6">
                        </div>


                    </div>








                    <div class="container">



                        <div class="col-sm-4 form-group"> 
                            <label for="Mobile"> Mobile: <span class="star">*</span></label>
                            <input type="text" class="form-control" id="txtMobile" name="txtMobile" placeholder="Mobile" onkeypress="javascript:return allownumbers(event);" maxlength="10">
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="Mobile"> Email: <span class="star">*</span></label>
                            <input type="text" class="form-control" id="txtEmail" name="txtEmail" placeholder="Email">
                        </div>






                    </div>




                    <div class="container">





                        <div class="col-sm-4 form-group"> 
                            <label for="Website"> FaceBookId:<span class="star">*</span></label>
                            <input type="text" class="form-control" id="txtFaceBookId" name="txtFaceBookId" placeholder="FaceBookId">
                        </div>
                        <div class="col-sm-4 form-group"> 
                            <label for="TwitterId"> TwitterId:<span class="star">*</span> </label>
                            <input type="text" class="form-control" id="txtTwitterId" name="txtTwitterId" placeholder="TwitterId">
                        </div>



                    </div>






                    <div class="container">      
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"  style="margin-left:18px;"/>
                    </div>
                    
                    <div id="grid" style="margin-top: 35px;">

                    </div>
                </form>





            </div>


        </div>

    </div>
</div>
</body>

<script type="text/javascript">
    $('#txtDOB').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        autoclose: true
    });
</script>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {



        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }
        function FillCourseType() {
            $.ajax({
                type: "post",
                url: "common/cfQualificationMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlQualification").html(data);
                }
            });
        }
        FillCourseType();

        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfProfileMaster.php",
                data: "action=DELETE&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmprofile.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmprofile");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }

        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfProfileMaster.php",
                data: "action=EDIT&values=" + Code + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    //alert(data);
                    txtFirstName.value = data[0].UserProfile_FirstName;
                    txtHouseNo.value = data[0].UserProfile_HouseNo;
                    txtDOB.value = data[0].UserProfile_DOB;
                    ddlMTongue.value = data[0].UserProfile_MTongue;
                    txtAddress.value = data[0].UserProfile_Address;
                    ddlGender.value = data[0].UserProfile_Gender;
                    txtFatherName.value = data[0].UserProfile_FatherName;
                    ddlQualification.value = data[0].UserProfile_Qualification;
                    txtOccupation.value = data[0].UserProfile_Occupation;
                    ddlChallenged.value = data[0].UserProfile_PhysicallyChallenged;
                    txtAadharNo.value = data[0].UserProfile_AadharNo;
                    txtSTDCode.value = data[0].UserProfile_STDCode;
                    txtMobile.value = data[0].UserProfile_Mobile;
                    txtEmail.value = data[0].UserProfile_Email;
                    txtFaceBookId.value = data[0].UserProfile_FaceBookId;
                    txtTwitterId.value = data[0].UserProfile_TwitterId;

                }
            });
        }

        function showData() {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfUserProfile.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
//alert(data);
                }
            });
        }

        showData();


        $("#btnSubmit").click(function () {
            if ($("#profile").valid())
            {


                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfProfileMaster.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#profile").serialize();
                if (Mode == 'Add')
                {
                    data = "action=ADD&" + forminput; // serializes the form's elements.
                }
                else
                {

                    data = "action=UPDATE&code=" + Code + "&" + forminput;

                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmprofile.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("profile");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmuserprofile_validation.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html> 