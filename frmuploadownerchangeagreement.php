<?php
$title = "Upload SP Center Agreement";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code='" . $_REQUEST['code'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var Ack='" . $_REQUEST['ack'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}

$random = (mt_rand(1000, 9999));
$random .= date("y");
$random .= date("m");
$random .= date("d");
$random .= date("H");
$random .= date("i");
$random .= date("s");

echo "<script>var OrgDocId= '" . $random . "' </script>";
?>
<div id="googleMap"  class="mapclass"></div>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Upload SP Center Agreement</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmuploadagreement" id="frmuploadagreement" role="form" action="" class="form-inline" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>

                    <div id="main-content" style="display:none;">
                        <div class="container">
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Organization/Center:</label>
                                <input type="text" class="form-control" readonly="" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                                <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                                <!--<input type="hidden" class="form-control" maxlength="50" name="txtCenterCode" id="txtCenterCode"/>-->
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Registration No:</label>
                                <input type="text" class="form-control" readonly="" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:</label>
                                <input type="text" class="form-control" readonly="" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:</label>
                                <input type="text" class="form-control" readonly="" name="txtType" id="txtType" placeholder="Type Of Organization">  
                            </div>
                        </div>  
                        <br>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Document Type:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Application Type:</label>
                                <input type="text" class="form-control" readonly="true" name="txtRole" id="txtRole"  placeholder="Application Type">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">New Owner Email:</label>
                                <input type="text" class="form-control" readonly="true" name="txtEmail" id="txtEmail"  placeholder="Email">   
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="address">New Owner Mobile:</label>
                                <input type="text" class="form-control" readonly="true" id="txtMobile" name="txtMobile" placeholder="Mobile">
                                <input type="hidden" class="form-control" readonly="true" id="txtAck" name="txtAck" placeholder="Ack">
<button type="button" id="showmodal" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
                            </div>
                        </div>  


                        &nbsp;
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <div class="panel panel-danger form-group " id="uploadagreement">
                            <div class="panel-heading">Upload SP-Center Agreement</div>
                            <div class="panel-body">
                                <div class="col-sm-12" > 
                                    <label for="photo">Attach Agreement Copy:<span class="star">*</span></label>
                                    <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->								  
                                    <input style=" width: 115%" id="uploadImage7" type="file" name="uploadImage7" onchange="checkPANPhoto(this);
                                            PreviewImage(7)"/>	
                                    <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                    <span style="font-size:10px;">Note: PDF Allowed Size = 100 KB to 3 MB</span>
                                     <a href="http://www.rkcl.in/myrkclwebsite/uploads/news/1549019803.pdf">Download Format</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-danger form-group " id="viewagreement" style="display:none">
                            <div class="panel-heading">SP-Center Agreement</div>
                            <div class="panel-body">
                                <div class="col-sm-12" > 
                                    <label for="orgtypedoc7">View/Update Uploaded Agreement</label> </br>
                                    <button type="button" id="uploadPreview1" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                        View Agreement</button>
                                </div>
                            </div>
                        </div>


                        <div class="container" id="uploadbutton">
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Upload Agreement"/>    
                        </div>

                    </div>
                </form>
            </div>
        </div>   
    </div>
    <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" style="width: 100%;height: 500px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h3 id="heading-tittle" class="modal-title">Heading</h3>
                </div>

                <form class="form-horizontal" name="frmeditncrfile" id="frmeditncrfile" role="form" action="" class="form-inline" enctype="multipart/form-data">
                    <iframe id="viewimagesrc" src="uploads/news/1487051012.pdf" style="width: 100%;height: 500px;border: none;"></iframe>
                    <input type="hidden" class="form-control" readonly="true" name="txtfilename" id="txtfilename">
                    <div class="panel panel-danger">
                        <div class="panel-heading">Choose Another File To Update</div>
                        <div class="panel-body">
                            <div id="response1"></div>
                            <div class="col-sm-12" > 
                                <label for="photo">Attach File:<span class="star">*</span></label>
                                <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->								  
                                <input style=" width: 115%" id="uploadImage72" type="file" name="uploadImage72" onchange="checkAgreement(this);"/>	
                               
                                    <input type="hidden" class="form-control" readonly="true" id="txtAckUpdate" name="txtAckUpdate" placeholder="Ack">
                                <span style="font-size:10px;">Note: PDF Allowed Size = 100 KB to 3 MB</span>
                            </div>
                        </div>
                    </div>


                    <div class="container">
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Update Document"/>    
                    </div>
                </form>
<!--<img id="viewimagesrc" class="thumbnail img-responsive" src="images/not-found.png" name="filePhoto3" width="800px" height="880px">-->
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>
<script type="text/javascript">

    $(document).ready(function () {
        jQuery(".thumbnailmodal").click(function () {
            $('.modal-body').empty();
            var title = $(this).parent('a').attr("title");
            $('.modal-title').html(title);
            $($(this).parents('div').html()).appendTo('.modal-body');
            $('#showmodal').click();
        });
    });
</script>

<script language="javascript" type="text/javascript">
    function checkPANPhoto(target) {
        var ext = $('#uploadImage7').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage7").value = '';
            return false;
        }

        if (target.files[0].size > 5000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("File size should less or equal 5 MB");
            document.getElementById("uploadImage7").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("File size should be greater than 100 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        }
        document.getElementById("uploadImage7").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkAgreement(target) {
        var ext = $('#uploadImage72').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage72").value = '';
            return false;
        }

        if (target.files[0].size > 5000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("File size should less or equal 5 MB");
            document.getElementById("uploadImage72").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("File size should be greater than 100 KB");
            document.getElementById("uploadImage72").value = '';
            return false;
        }
        document.getElementById("uploadImage72").innerHTML = "";
        return true;
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function fillForm()
        {           //alert(val);    
            $.ajax({
                type: "post",
                url: "common/cfUploadOwnerChangeAgreement.php",
                data: "action=APPROVE",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Some Details are Pending for eligiblity of Agreement" + "</span></p>");
                        return false;
                    }
                    if(data != ""){
                        //alert("hi");
                    data = $.parseJSON(data);
                    //alert(data[0].Orgstatus);
                    if(data[0].Orgstatus == 'Approved' || data[0].Orgstatus == 'Rejected') {
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + "   Ownership Change Application has been" + data[0].Orgstatus + " by RKCL." + "</span></p>");
                    } else if(data[0].Orgstatus == 'Pending' && data[0].orgdoc != '0'){
                      $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + "   SP-ITGK Aggrement is already uploaded for Ownership Change Process." + "</span></p>");  
                    }
                    else if(data[0].Orgstatus == 'Pending' || data[0].Orgstatus == 'OnHold')  {
                        //data = $.parseJSON(data);
                    txtName1.value = data[0].orgname;
                    txtRegno.value = data[0].regno;
                    txtEstdate.value = data[0].fdate;
                    txtType.value = data[0].orgtype;
                    txtDocType.value = data[0].doctype;
                    txtRole.value = data[0].orgcourse;

                    txtEmail.value = data[0].email;
                    txtMobile.value = data[0].mobile;
                    txtAck.value = data[0].ack;
                    txtAckUpdate.value = data[0].ack;
                    
                        if(data[0].orgdoc != '0' && data[0].Orgstatus == 'OnHold'){
                           $('#viewagreement').show(3000);
                           $('#uploadagreement').hide();
                           $('#uploadbutton').hide();
                           
                        }
                        $("#uploadPreview1").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "SPCENTERAGREEMENT/" + data[0].orgdoc + "?nocache="+Math.random());
                            txtfilename.value = data[0].orgdoc;
                        });
                        $('#main-content').show(3000);
                    }

                }
                }
            });
        }
        fillForm();
        
        $("#frmeditncrfile").submit(function () {
            //alert("1");
            $('#response1').empty();
            $('#response1').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            //BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfUploadOwnerChangeAgreement.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmeditncrfile").serialize();
//alert(forminput);
            data = forminput; // serializes the form's elements.

            var form_data = new FormData(this);                  // Creating object of FormData class

            form_data.append("action", "UPDATE")
            form_data.append("data", data)
//alert(form_data);
            $.ajax({

                url: url,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data, // Setting the data attribute of ajax with file_data
                type: 'post',
                success: function (data)
                {
                    //alert(data);
                    var result = data.trim();
                    if (result === "Successfully Inserted")
                    {
                        $('#response1').empty();
                        $('#response1').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + result + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; SP-Center Agreement Uploaded Successfully.</span>");
                        window.setTimeout(function () {
                            window.location.href = "viewimagesrc.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("viewimagesrc");
                    } else
                    {
                        $('#response1').empty();
                        $('#response1').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + result + "</span></p>");
                        
                        window.setTimeout(function () {
                        window.location.reload(true);
                        }, 2000);
                        
                    }



                }
            });

            return false; // avoid to execute the actual submit of the form.
        });

        $('#txtGenerateId').val(OrgDocId);
        $('#txtCenterCode').val(Code);
        $("#frmuploadagreement").submit(function () {
            //alert("1");
//            if ($("#frmuploadagreement").valid())
//            {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            //BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfUploadOwnerChangeAgreement.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmuploadagreement").serialize();
//alert(forminput);
            data = forminput; // serializes the form's elements.

            var form_data = new FormData(this);                  // Creating object of FormData class

            form_data.append("action", "ADD")
            form_data.append("data", data)
//alert(form_data);
            $.ajax({

                url: url,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data, // Setting the data attribute of ajax with file_data
                type: 'post',
                success: function (data)
                {
                    //alert(data);
                    var result = data.trim();
                    if (result === "Successfully Updated")
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + result + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; SP-Center Agreement Uploaded Successfully.</span>");
                        window.setTimeout(function () {
                            window.location.href = "frmuploadownerchangeagreement.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("frmuploadownerchangeagreement");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></p>");
                    }



                }
            });
            // }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
</html>
