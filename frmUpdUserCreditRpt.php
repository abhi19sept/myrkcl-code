<?php  ob_start();
$title = "IT-GK Passbook Report";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '25') {
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">IT-GK Passbook Report</div>
            <div class="panel-body">

                <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container" id="divbydate">
                    <div class="col-sm-4 form-group"> 
                        <label for="sdate">Center Code:</label>
                        
                        <input type="text" class="form-control" name="txtCenter" id="txtCenter" placeholder="can be left empty">     
                    </div>
                    <div class="col-sm-4 form-group"> 
                        <label for="sdate">Sheet Uploaded Start Date:</label>
                        <span class="star">*</span>
                        <input type="text" class="form-control" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="DD-MM-YYYY">     
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="edate">Sheet Uploaded End Date:</label>    
                        <span class="star">*</span>
                        <input type="text" class="form-control" readonly="true" name="txtenddate" id="txtenddate"  placeholder="DD-MM-YYYY" value=" <?php echo date("d-m-Y"); ?>">
                    </div>

                    <div class="col-sm-4 form-group"> 
                          <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View Report"/>    
                    </div>
                </div>

                        <div id="grid" style="margin-top:30px; width:94%;"> </div>
                    
                        
                    </div>  
                    
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
       // autoclose: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {



        $("#btnSubmit").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfITGKPassbook.php"; // the script where you handle the form input.
                var data;
                 var startdate = $('#txtstartdate').val();
                var enddate = $('#txtenddate').val();
                 data = "action=SHOWrpt&startdate=" + startdate + "&enddate=" + enddate + "&txtCenter=" + txtCenter.value + "";
                // alert(data);
                $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data2) {
//                    alert(data2);
                    $('#response').empty();
                    $("#grid").html(data2);
                    $("#sbtn").show();
                    
                    var table = $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    
                    
                    // Handle click on "Select all" control
                    $('#example-select-all').on('click', function(){
                       // Get all rows with search applied
                       var rows = table.rows({ 'search': 'applied' }).nodes();
                       // Check/uncheck checkboxes for all rows in the table
                       $('input[type="checkbox"]', rows).prop('checked', this.checked);
                    });

                    // Handle click on checkbox to set state of "Select all" control
                    $('#example tbody').on('change', 'input[type="checkbox"]', function(){
                        // If checkbox is not checked
                        if(!this.checked){
                           var el = $('#example-select-all').get(0);
                           // If "Select all" control is checked and has 'indeterminate' property
                           if(el && el.checked && ('indeterminate' in el)){
                              // Set visual state of "Select all" control
                              // as 'indeterminate'
                              el.indeterminate = true;
                           }
                        }
                     });
                }
            });

            }
            return false; // avoid to execute the actual submit of the form.
        });
        

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        
       
    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>

    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>  
