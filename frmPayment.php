<?php
// Code Start for Payment Gateway-----------------------
$MERCHANT_KEY = "7EcJIS";

// Merchant Salt as provided by Payu
$SALT = "cJDbbB7A";

// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://secure.payu.in";

$action = '';

$posted = array();
if(!empty($_POST)) {
   // print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 	
  }
 // print_r($posted[$key]);
}

$formError = 0;

if(empty($posted['txnid'])) {
  // Generate random transaction id
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|||||||||";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
	  // echo "IF RUN";
    $formError = 1;
  } else {
	 //  echo "NOT RUN";
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>

<html>
	<head>		
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 		
		<meta name="description" content="Blueprint: Horizontal Drop-Down Menu" />
		<meta name="keywords" content="horizontal menu, microsoft menu, drop-down menu, mega menu, javascript, jquery, simple menu" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">		
		<link rel="stylesheet" href="bootcss/css/bootstrap.min.css">	
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="bootcss/js/bootstrap.min.js"></script>			
		<link rel="stylesheet" href="assets/header-search.css">
		<link rel="stylesheet" href="assets/footer-distributed-with-address-and-phones.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">				
		<script src="bootcss/js/jquery.dataTables.min.js"></script>	
		<script src="bootcss/js/dataTables.bootstrap.min.js"></script>			
		<link rel="stylesheet" href="bootcss/css/dataTables.bootstrap.min.css">		
            <!-- 		<link href="css/jquery.datepick.css" rel="stylesheet" type="text/css"/> -->
            <script type="text/javascript" src="scripts/validate.js"></script>
			
			<link rel="stylesheet" href="css/style.css">
<script src="scripts/Menu.js" type="text/javascript"></script>


  
	</head>
	<body onload="submitPayuForm()">
		
	<?php include ('root_menu.php'); 



// Code End for Payment Gateway-----------------------
if(isset($_POST['code'])) {
	//print_r($_POST);
    $Code = $_POST['code'];
    //$paytype = $_POST['paytype'];
//    $amount = $_POST['amount'];
    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var CenterCode='" . $_POST['code'] . "'</script>";
     echo "<script>var PayTypeCode=" . $_POST['paytypecode'] . "</script>";
	 echo "<script>var PayType='" . $_POST['paytype'] . "'</script>";
    echo "<script>var amount=" . $_POST['amount'] . "</script>";
    echo "<script>var email='" . $_SESSION['User_EmailId'] . "'</script>";
     echo "<script>var Mode='Update'</script>";
}

 //print_r($_POST);
?>

<div class="container"> 
    

    <div class="panel panel-primary" style="margin-top:36px;">

        <div class="panel-heading">Payment Transection Details</div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            <form action="<?php echo $action; ?>" method="post" name="payuForm" id="payuForm" class="form-inline" role="form">     

                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>

                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Center Code:</label>
                      <!--   <div class="form-control" maxlength="50"   name="txtCenterCodediv" id="txtCenterCodediv">
                            
                        </div> -->
						 <input type="text" class="form-control" maxlength="50" name="udf1" id="udf1" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>" placeholder="End Date">    
						  <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
						  <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
						  <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
						  <input type="hidden" name="surl" value="http://localhost/myrkcladmin/success.php" size="64" />
						  <input type="hidden" name="furl" value="http://localhost/myrkcladmin/failure.php" size="64" />
						  <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
                        <!--<input type="text" class="form-control" maxlength="50"   name="txtCenterCode" id="txtCenterCode" placeholder="Center Code" value="<?php echo $Code ?>">-->
                    </div>


                    <div class="col-sm-4 form-group"> 
                        <label for="ename">Owner Name:</label>
                      <!--   <div class="form-control" maxlength="50" name="firstnamediv" id="firstnamediv">
                            
                        </div> -->
                        <input type="text" class="form-control" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" maxlength="50" name="firstname" id="firstname" placeholder="End Date">    
                    </div>


                    <div class="col-sm-4 form-group">     
                        <label for="faname">Mobile No:</label>
                       <!-- <div class="form-control" maxlength="50" name="phonediv" id="phonediv">
                            
                        </div> -->
                        <input type="text" class="form-control" maxlength="50" name="phone" id="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>"  placeholder="Owner Mobile">
                    </div>
                    <div class="col-sm-4 form-group">     
                        <label for="faname">Email ID:</label>
                     <!--    <div class="form-control" maxlength="50" name="emaildiv" id="emaildiv">
                            
                        </div> -->
                        <input type="text" class="form-control" maxlength="50" name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>"  placeholder="Owner Email">
                    </div>                            
                </div>  

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">Payment Type:</label>
                           <input type="text" name="productinfoname" id="productinfoname" value="<?php echo (empty($posted['productinfoname'])) ? '' : $posted['productinfoname'] ?>"/>
						   <input type="hidden" name="productinfo" id="productinfo" value="<?php echo (empty($posted['productinfoval'])) ? '' : $posted['productinfoval'] ?>"/>
                    </div>
					

                    <div class="col-sm-4 form-group"> 
                        <label for="ename">Transection Reference No:</label>
                       <!--  <div class="form-control" maxlength="50"  name="transactiondiv" id="transactiondiv">
                            123456
                        </div> -->
						<input type="text" class="form-control" maxlength="50" name="transaction" id="transaction" placeholder="Tran. Ref. No:" value="123456">  
                    </div>


                    <div class="col-sm-4 form-group">     
                        <label for="faname">Amount:</label>
                      <!--   <div class="form-control" maxlength="50" name="amountdiv" id="amountdiv">
                            
                        </div> -->
                       <input type="text" class="form-control" maxlength="50" name="amount" id="amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>"  placeholder="Total Amount">
                    </div>
				</div>
				 <div class="container">
                    <div class="col-sm-4"> 
					<?php if(!$hash) { ?>
                        <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Pay Now"/>   
							 <?php } ?>
                    </div>

                </div>





        </div>
    </div>   
</div>


</form>


</div>
</div>  



</div>




</body>
<?php include'common/message.php'; ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";



    $(document).ready(function () {

        function showData() {
		
		
            $.ajax({
                type: "post",
                url: "common/cfPayment.php",
                data: "action=SHOW&values=" + UserCode + "",
                success: function (data) {
                    //alert($.parseJSON(data)[0]['OwnerName']);
                    data = $.parseJSON(data);
                   // document.getElementById('txtCenterCodediv').innerHTML = CenterCode;
                  ////  document.getElementById('amountdiv').innerHTML = amount;
                  ////  document.getElementById('firstnamediv').innerHTML = data[0].OwnerName;
                  //  document.getElementById('phonediv').innerHTML = data[0].OwnerMobile;
                   //// document.getElementById('emaildiv').innerHTML = email;
					
					$('#udf1').val(CenterCode);
					 $('#amount').val(amount);
					firstname.value = data[0].OwnerName;
					phone.value = data[0].OwnerMobile;
					 $('#email').val(email);
					 $('#productinfo').val(PayTypeCode);
					 $('#productinfoname').val(PayType);
					 
			

                }
            });
        }

        showData();

      /*  $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfPayment.php"; // the script where you handle the form input.
            var data;
            //alert(Mode);
            if (Mode == 'Update')
            {

                data = "action=Update&code=" + CenterCode + "&paytypecode=" + PayTypeCode + "&TranRefNo=" + 123456 + ""; // serializes the form's elements.
                
        //alert(data);
            }
            else
            {
                
            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmapplyeoi.php";
                        }, 3000);
//
//                        Mode = "Add";
//                        resetForm("frmOrgDetail");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });*/
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  
<?php include ('footer.php'); ?>
</html> 