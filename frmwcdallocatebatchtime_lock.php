<?php
$title = "Learner Allocate Batch Timing";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '7') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Allocate Learner's Batch Timing</div>
            <div class="panel-body">

                <form name="frmbatchtime" id="frmbatchtime" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
								  <select id="ddlCourse" name="ddlCourse" class="form-control">
                                    
								   </select>
								  
                             </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                    <!--                                <option value="0">All Batch</option>-->
                                </select>
                            </div> 							 

                        </div>

						<div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal">
    <div class="modal-content">
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal">×</button>
			<h3 id="heading-tittle" class="modal-title">Verify OTP Received on Email-Id</h3>
		</div>
		<div class="modal-body">
			    <form name="frmwcdemailotp" id="frmwcdemailotp" class="form-inline" role="form"> 
					<div class="container">
						<div class="container">
							<div id="responses"></div>

						</div>        
							<div id="errorBox"></div>
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Enter OTP:</label>
                                <input type="text" class="form-control" maxlength="6" name="verifyotp"
									id="verifyotp"  placeholder="Enter OTP" required="true">
								<input type="hidden" name="finalemail" id="finalemail" value="">
								<input type="hidden" name="finallcode" id="finallcode" value="">
								<input type="hidden" name="finaltime" id="finaltime" value="">
								<input type="hidden" name="finalbatch" id="finalbatch" value="">
                            </div>			
							
						</div>									
						
                        <div class="container">
							  <input type="button" name="btnfinalVerify" id="btnfinalVerify" class="btn btn-primary"
								value="Verify OTP" style="margin-top:10px" />							
                        </div>					
				</form>
		</div>
	</div>
</div>

<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

		function FillCourseName() {
            $.ajax({
                type: "post",
                url: "common/cfWcdAllocateBatchTime.php",
                data: "action=FillAdmissionCourse",
                success: function (data) {
					$("#ddlCourse").html(data);					
                }
            });
        }
        FillCourseName();

        $("#ddlCourse").change(function () {
			$("#grid").html('');
           var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfWcdAllocateBatchTime.php",
                data: "action=FILLBatchName&values=" + selCourse + "",
                success: function (data) {
					$("#ddlBatch").html(data);
                }
            });

        });
	
        function showData() {
			$('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdAllocateBatchTime.php"; // the script where you handle the form input.
            data = "action=GETDATA&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					if (data == 2){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Course." + "</span></div>");
                            
						}
					else if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Batch." + "</span></div>");
                            
						}
					else {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
						buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}

                }
            });
        }

        $("#btnSubmit").click(function () {
            showData();
          return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
		
		$("#grid").on('click', '.updtime',function(){
			var lcode = $(this).attr('id');
			var count = $(this).attr('name');
			var ltime = $("#ddltime"+count).val();			
				if(ddlCourse.value=='24'){
					var email = $(".updemail"+count).val();
					if(ltime==''){
						alert("Please Select Allocation Time for Learner");
					}
					else if(email==''){
						alert("Please Enter Email-ID of Learner");
					}
					
					else{
						if(IsEmail(email)==false){
						  $('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Enter Valid E-Mail Id.</span></p>");
						  return false;
						}
						else{
							$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						var url = "common/cfWcdAllocateBatchTime.php"; // the script where you handle the form input.
						var data;
               
						data = "action=Updaterscfabatchtime&lcode=" + lcode + "&allottime=" + ltime + "&batch=" + ddlBatch.value + "&email=" + email + ""; 
               
					$.ajax({
						type: "POST",
						url: url,
						data: data,
						success: function (data)
						{
							$('#response').empty();
							if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
							{
								$('#response').empty();
								$('#finalemail').val(email);
								$('#finaltime').val(ltime);
								$('#finallcode').val(lcode);
								$('#finalbatch').val(ddlBatch.value);
								var modal = document.getElementById('myModal');
								var span = document.getElementsByClassName("close")[0];
								modal.style.display = "block";
								span.onclick = function() {							
									modal.style.display = "none";
									window.setTimeout(function () {
										window.location.href = 'frmwcdallocatebatchtime.php';
									}, 1000);
								}								                           
							}
							else if(data=='0'){
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something went wrong.</span></p>");
							}
							else if(data==00){
									$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   OTP already Generated, Please Try again." + "</span></p>");
								}
							else if(data==1){
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Email Id already Exist." + "</span></p>");								
							}
							else{
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
							}                       
						}
					});
						}
						
					}
				}
				else{
					if(ltime==''){
						alert("Please Select Allocation Time for Learner");
					}else{
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						var url = "common/cfWcdAllocateBatchTime.php"; // the script where you handle the form input.
						var data;
               
						data = "action=Updaterscitbatchtime&lcode=" + lcode + "&allottime=" + ltime + "&batch=" + ddlBatch.value + ""; 
               
					$.ajax({
						type: "POST",
						url: url,
						data: data,
						success: function (data)
						{
							if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
							{
								$('#response').empty();
								showData();								
							}
							else if(data=='0'){
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something went wrong.</span></p>");
							}
							else
							{
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
							}                       
						}
					});
				}
			}				
		});
		
		 function IsEmail(email) {
			  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			  if(!regex.test(email)) {
				return false;
			  }else{
				return true;
			  }
			}
		
	$("#btnfinalVerify").click(function () {
		if ($("#frmwcdemailotp").valid())
		{
            $('#responses').empty();
            $('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfWcdAllocateBatchTime.php",
                data: "action=Veifyemailotp&otp=" + verifyotp.value + "&email=" + finalemail.value + "&batch=" + finalbatch.value + "&lcode=" + finallcode.value + "&allottime=" + finaltime.value + "",
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {						
                        $('#responses').empty();
                        $('#responses').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
								window.setTimeout(function () {
								   window.location.href="frmwcdallocatebatchtime.php";
							   }, 1000); 
                    }
                    else
                    {
                        $('#responses').empty();
                        $('#responses').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");						
                    }
                }
            });
			}
			return false;
        });
		
    });
</script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
