<?php
$title = "Ownership Change Approval Process";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var CenterCode='" . $_REQUEST['cc'] . "'</script>";
    echo "<script>var Ack='" . $_REQUEST['ack'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">			 
        <div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">Approve Ownership Change Request</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" action="" class="form-inline">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <br>
                    <div class="container">

                        <div id="errorBox"></div>


                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">New Organization Name:</label>
                            <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="ename">Registration No:</label>
                            <input type="text" class="form-control" readonly="true" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                        </div>



                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Type of Organization:</label>
                            <input type="text" class="form-control" readonly="true" name="txtType" id="txtType" placeholder="Type Of Organization">  
                        </div>
                    </div>




                    <div class="container">




                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Document Type:</label>
                            <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                        </div>

                        <div class="col-sm-4 form-group">     
                            <label for="SelectType">Application Type:</label>
                            <input type="text" class="form-control" readonly="true" name="txtRole" id="txtRole" placeholder="Role"/>     

                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="email">New Owner Email:</label>
                            <input type="text" class="form-control" readonly="true" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="Mobile">New Owner Mobile Number:</label>
                            <input type="text" class="form-control" readonly="true" name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                        </div>


                    </div>	

                    <div class="panel panel-success">
                        <div class="panel-heading">Details of Ownership Change Fee</div>
                        <div class="panel-body">
                            <div id="processingfeepayment"></div>

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">Uploaded New Owner Documents</div>
                        <div class="panel-body">	

                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Pan Card:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview1" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">AADHAR Card:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview2" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Address Proof:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview3" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Ownership Change Applcation Form:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview4" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-success">
                        <div class="panel-heading">Uploaded Organization Type Documents</div>
                        <div class="panel-body">
                            <button type="button" id="showmodal" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc1"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview10" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc2"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview11" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc3"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview12" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc4"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview13" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc5"> 
                                <label for="orgtypedoc5"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview14" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc6"> 
                                <label for="orgtypedoc6"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview15" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc7"> 
                                <label for="orgtypedoc7"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview16" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                        </div>
                    </div>



            </div>
            </form>
        </div>  

    </div>

    <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" style="width: 100%;height: 500px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h3 id="heading-tittle" class="modal-title">Heading</h3>
                </div>
                <iframe id="viewimagesrc" src="uploads/news/1487051012.pdf" style="width: 100%;height: 500px;border: none;"></iframe>
                <!--<img id="viewimagesrc" class="thumbnail img-responsive" src="images/not-found.png" name="filePhoto3" width="800px" height="880px">-->
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>
<!--<script type="text/javascript">
    function toggle_visibility1(id) {
        var e = document.getElementById(id);

        //alert(e);
        var f = document.getElementById('ddlstatus').value;
        //alert(f);
        if (f == "Reject")
        {
            e.style.display = 'block';
        } else {
            e.style.display = 'none';
        }

    }
</script>-->

<script type="text/javascript">

    $(document).ready(function () {
        jQuery(".thumbnailmodal").click(function () {
            $('.modal-body').empty();
            var title = $(this).parent('a').attr("title");
            $('.modal-title').html(title);
            $($(this).parents('div').html()).appendTo('.modal-body');
            $('#showmodal').click();
        });
    });
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        } else if (Mode == 'Edit')
        {
            //alert(1);
            fillForm();
            showPaymentDetails();
            //existingITGKDetails();
            //fillCenterVisitPhoto();
            //fillSPCenterAgreement();

        }

        function showPaymentDetails() {
            $.ajax({
                type: "post",
                url: "common/cfOwnershipChangeApproval.php",
                data: "action=PAYMENTDETAIL&centercode=" + CenterCode + "",
                success: function (data) {
                    $("#processingfeepayment").html(data);
                    $('#examplepay').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }
        
        function fillForm()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfOwnershipChangeApproval.php",
                data: "action=PROCESS&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    txtName1.value = data[0].orgname;
                    txtRegno.value = data[0].regno;
                    //txtEstdate.value = data[0].fdate;
                    txtType.value = data[0].orgtype;
                    txtDocType.value = data[0].doctype;
                    txtRole.value = data[0].orgcourse;

                    txtEmail.value = data[0].email;
                    txtMobile.value = data[0].mobile;
                    $("#uploadPreview1").click(function () {
                        $("#viewimagesrc").attr('src', "upload/NCRPAN/" + data[0].orgdoc);
                    });
                    $("#uploadPreview2").click(function () {
                        $("#viewimagesrc").attr('src', "upload/NCRUID/" + data[0].orguid);
                    });
                    $("#uploadPreview3").click(function () {
                        $("#viewimagesrc").attr('src', "upload/NCRAddProof/" + data[0].orgaddproof);
                    });
                    $("#uploadPreview4").click(function () {
                        $("#viewimagesrc").attr('src', "upload/NCRAppForm/" + data[0].orgappform);
                    });
//                    $("#uploadPreview1").attr('src', "upload/NCRPAN/" + data[0].orgdoc);
//                    $("#uploadPreview2").attr('src', "upload/NCRUID/" + data[0].orguid);
//                    $("#uploadPreview3").attr('src', "upload/NCRAddProof/" + data[0].orgaddproof);
//                    $("#uploadPreview4").attr('src', "upload/NCRAppForm/" + data[0].orgappform);

                    if (txtType.value == "Proprietorship/Individual")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Ownership Type Document");
                        jQuery("label[for='orgtypedoc2']").html("Shop Act License");
                        jQuery("label[for='orgtypedoc3']").html("PAN card copy of Proprietor");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled Cheque");

                        $("#doc5").hide();
                        $("#doc6").hide();
                        $("#doc7").hide();


                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_owntype.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_salpi.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_panpi.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_ccpi.pdf");
                        });

                    }
                    if (txtType.value == "Partnership")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Copy of Partnership Deed");
                        jQuery("label[for='orgtypedoc2']").html("Authorization letter");
                        jQuery("label[for='orgtypedoc3']").html("PAN card of Partnership firm");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled cheque of firm");
                        jQuery("label[for='orgtypedoc5']").html("Address Proof documents");
                        jQuery("label[for='orgtypedoc6']").html("Registration certificate");
                        jQuery("label[for='orgtypedoc7']").html("Shop Act Registration Copy");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_copd.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_al.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ppf.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ccpf.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_apdpf.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_rcrf.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_sarpf.pdf");
                        });

                    }
                    if (txtType.value == "Private Ltd." || txtType.value == "Public Ltd.")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Incorporation");
                        jQuery("label[for='orgtypedoc2']").html("List Of Directors");
                        jQuery("label[for='orgtypedoc3']").html("Pan Card Of Company");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled Cheque");
                        jQuery("label[for='orgtypedoc5']").html("Board Resolution Copy");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof Document");
                        jQuery("label[for='orgtypedoc7']").html("Shop Act Registration Copy");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_coi.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_lod.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_copan.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_ccc.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_cobr.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_apd.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_csar.pdf");
                        });

                    }
                    if (txtType.value == "Limited Liability Partnership (LLP)")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Incorporation");
                        jQuery("label[for='orgtypedoc2']").html("List of Partners");
                        jQuery("label[for='orgtypedoc3']").html("Copy of PAN card of LLP");
                        jQuery("label[for='orgtypedoc4']").html("Canceled cheque of LLP");
                        jQuery("label[for='orgtypedoc5']").html("Copy of Board Resolutions");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof Document");
                        jQuery("label[for='orgtypedoc7']").html("Copy of Shop Act Registration");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_coillp.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_lop.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_panllp.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_ccllp.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_cobrllp.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_apdllp.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_csarllp.pdf");
                        });

                    }
                    if (txtType.value == "Trust" || txtType.value == "Society" || txtType.value == "Coorperative Society" || txtType.value == "Others")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Registration");
                        jQuery("label[for='orgtypedoc2']").html("PAN Card of Organization");
                        jQuery("label[for='orgtypedoc3']").html("Cancelled cheque");
                        jQuery("label[for='orgtypedoc4']").html("List of Executive Body");
                        jQuery("label[for='orgtypedoc5']").html("Copy of Board Resolution");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof documents");

                        $("#doc7").hide();

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cor.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_panoth.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_ccoth.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_leb.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cbr.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_adpoth.pdf");
                        });

                    }
                }
            });
        }
        

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmcorrectionprocess_validation.js"></script>
</html>