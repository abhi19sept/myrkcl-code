<?php
$title = "Release Intake";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var BatchCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var BatchCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Batch Master </div>
            <div class="panel-body">					
                <form name="frmreleaseintake" id="frmreleaseintake" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div> 
                        <div class="col-sm-4 form-group"> 
                            <label for="lbatch">Select Course:</label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control" >
                                <option value=""> Select Course: </option>
                                <option value="1"> RS-CIT</option>
                            </select>    
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="lbatch">Select Batch To Allocate Intake:</label>
                            <select id="ddlallocate" name="ddlallocate" class="form-control" >									
                            </select>    
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="rbatch">Select Batch To Release Intake:</label>
                            <select id="ddlrelease" name="ddlrelease" class="form-control" >									
                            </select>    
                        </div>
                    </div>                  	

                    <div class="container">
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                    </div>

                    <div id="gird" style="margin-top:35px;"> </div>
            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {

        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLCourseName",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        // FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfReleaseIntake.php",
                data: "action=FILLAllocate&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlallocate").html(data);
                }
            });
        });

        $("#ddlallocate").change(function () {
            var selBatch = $(this).val();

            $.ajax({
                type: "post",
                url: "common/cfReleaseIntake.php",
                data: "action=FILLRelease&values=" + selBatch + "&course=" + ddlCourse.value + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlrelease").html(data);
                }
            });

        });


        $("#btnSubmit").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");


            var url = "common/cfReleaseIntake.php"; // the script where you handle the form input.
            var data;
            if (Mode == 'Add')
            {
                data = "action=ADD&allocate=" + ddlallocate.value + "&release=" + ddlrelease.value + "&course=" + ddlCourse.value + ""; // serializes the form's elements.				 
            } else
            {
                data = "action=UPDATE&code=" + EoiConfigCode +
                        "&name=" + txtEoiName.value + "&course=" + ddlCourse.value +
                        "&startdate=" + txtstartdate.value + "&enddate=" + txtenddate.value +
                        "&pfees=" + txtPFees.value + "&cfees=" + txtCFees.value +
                        "&eclist=" + eclist + "&tncdoc=" + tncdoc + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmbatchmaster.php";
                        }, 1000);
                        Mode = "Add";
                        resetForm("frmbatchmaster");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
</html>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmbatchmaster_validation.js" type="text/javascript"></script>	