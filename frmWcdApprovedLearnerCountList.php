<?php
$title = "WCD Learner Count List";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['batch'])) {    
    echo "<script>var batch=" . $_REQUEST['batch'] . "</script>";
    echo "<script>var mode='" . $_REQUEST['mode'] . "'</script>";
    echo "<script>var districtcode='" . $_REQUEST['districtcode'] . "'</script>";
	echo "<script>var itgkcode='" . $_REQUEST['itgk'] . "'</script>";
} else {   
    echo "<script>var batch='0'</script>";
    echo "<script>var mode='0'</script>";
	echo "<script>var districtcode='0'</script>";
    echo "<script>var itgkcode='0'</script>";
}
?>
    <div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 


    <div class="panel panel-primary" style="margin-top:36px !important;">  
        <div class="panel-heading">Learner Preference Count List</div>
        <div class="panel-body">

            <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>


                    <div class="container">
                        <div id="gird" style="margin-top:25px; width:94%;"> </div>
                    </div>
					
					<?php if($_REQUEST['mode'] == 'one')  { ?>						
					<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Reported All" style="display:none; margin-top:15px;"/>    
                    </div> 
					 <?php } ?> 
                </div>   
            </form>
        </div>
    </div>
</div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showData() {
			 $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
              
            var url = "common/cfWcdLearnerItgk.php"; // the script where you handle the form input.

            var data;
            //alert (role_type);
            data = "action=GETLEARNERLIST&mode=" + mode + "&batch=" + batch + "&districtcode=" + districtcode + "&itgkcode=" + itgkcode + ""; //

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					$('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						$('#btnSubmit').show();
                }
            });
        }
        showData();
		
		$("#btnSubmit").click(function () {	
			$('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdLearnerItgk.php"; // the script where you handle the form input.            
            var data;           
				 data = "action=UpdateApproveReportedLearner&batch=" + batch + "&itgkcode=" + itgkcode + ""; // serializes the form's elements.
           
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {					
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmWcdApprovedLearnerCountList.php?batch=" + batch + "&itgk=" + itgkcode + "&districtcode=" + districtcode + "&mode=one";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span>          <span>" + data + "</span></p>");
                    }
                  }
            });
		return false;
		  });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });

</script>
</html>
