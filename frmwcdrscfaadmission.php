<?php
include ('frmwcdheader.php');
//include ('root_menu.php'); 

if (!isset($_SESSION['OTP_Mobile'])) {
    header("location:frmlearnerotpverify.php");
}

if (isset($_REQUEST['code'])) {
    echo "<script>var OrganizationCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var OrganizationCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
$random = (mt_rand(1000, 9999));
$random.= date("y");
$random.= date("m");
$random.= date("d");
$random.= date("H");
$random.= date("i");
$random.= date("s");

echo "<script>var OrgDocId= '" . $random . "' </script>";
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:391px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
             <div class="panel-heading" style="height:auto !important">
			 <div class="container">
                <div  class="col-sm-4"> Application Form </div>

                <div class="col-sm-4">
                    Course: <div style="display: inline" id="txtcoursename"> </div>
                </div>

                <div class="col-sm-4">
                    Application Month: <div style="display: inline" id="txtbatchname"> </div>
                </div>

			</div>


            </div>

            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmoasisadmission" id="frmoasisadmission" class="form-inline" role="form" enctype="multipart/form-data"> 
                    <div class="container">
                        <div id="errorBox"></div>
                        <div class="col-sm-3 form-group"> 
                            <label for="designation">District:<span class="star">*</span></label>
                            <select id="ddlDistrict" name="ddlDistrict" class="form-control">                                  
                            </select>
                        </div>     

                        <div class="col-sm-3 form-group"> 
                            <label for="marks">Tehsil:<span class="star">*</span></label>
                            <select id="ddlTehsil" name="ddlTehsil" class="form-control">                                 
                            </select>
                        </div>
                        <div class="col-sm-3">     
                            <label for="batch"> Select Center Preference 1:</label>
                            <select id="ddlCenter" name="ddlCenter" class="form-control">

                            </select>									
                        </div>  
                        <div class="col-sm-3">     
                            <label for="batch"> Select Center Preference 2:</label>
                            <select id="ddlCenter2" name="ddlCenter2" class="form-control">

                            </select>									
                        </div>  

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" style="margin-top:25px;display:none;"/>    
                        </div>
                    </div>
                    <br>
                    <div id="CenterDetail" style="display:none;">
                        <div class="panel panel-info">
                            <div class="panel-heading">Your Prefered Center Detail 1</div>
                            <div class="panel-body">
                                <div class="container">
                                    <div class="col-sm-4 form-group">     
                                        <label for="learnercode">Organization/Center Name:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="ename">Mobile No:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtMobile" id="txtMobile" placeholder="Registration No">     
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Address:</label>
                                        <input type="text" class="form-control boxfontsize" readonly="true" name="txtITGKAddress" id="txtITGKAddress"  placeholder="District" style="width: 510px;">
                                        <div id="applicationsC1"></div>   
                                    </div>
                                </div>

                            </div>
                        </div>
						
						
						<div class="panel panel-info">
                            <div class="panel-heading">Your Prefered Center Detail 2</div>
                            <div class="panel-body">
                                <div class="container">
                                    <div class="col-sm-4 form-group">     
                                        <label for="learnercode">Organization/Center Name:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtName2" id="txtName2" placeholder="Name of the Organization/Center">
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="ename">Mobile No:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtMobile2" id="txtMobile2" placeholder="Registration No">     
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Address:</label>
                                        <input type="text" class="form-control boxfontsize" readonly="true" name="txtITGKAddress2" id="txtITGKAddress2"  placeholder="District" style="width: 510px;">
                                        <div id="applicationsC2"></div>
                                    </div>
                                </div>                               

                            </div>
                        </div>

                    </div>

                    <div id="main-content" style="display:none;">
                        <br>
                        <div class="panel panel-info">
                            <div class="panel-heading">Applicant Details</div>
                            <div class="panel-body">
                                <div class="container">

                                    <div class="col-sm-4 form-group">     
                                        <label for="learnercode">Applicant Name:<span class="star">*</span></label>
                                        <input type="text" class="form-control text-uppercase" maxlength="50" name="txtlname" id="txtlname" data-mask="charonly" placeholder="Applicant Name">
                                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                                        <input type="hidden" class="form-control" maxlength="50" name="txtAdmissionCode" id="txtAdmissionCode"/>
                                        <select id="ddlBatch" style="display:none;" name="ddlBatch" class="form-control">  </select>
                                        <input type="hidden" class="form-control" maxlength="50" name="txtCourseFee" id="txtCourseFee"/>
                                        <input type="hidden" class="form-control" maxlength="50" name="txtInstallMode" id="txtInstallMode"/>
                                        <input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
                                        <input type="hidden" class="form-control"  name="txtsign" id="txtsign"/>
                                        <input type="hidden" class="form-control" maxlength="50" name="txtmode" id="txtmode" value="1" />
                                        <input type="hidden" class="form-control"  name="txtLearnercode" id="txtLearnercode"/>
                                        <input type="hidden" class="form-control"  name="txtbatchcode" id="txtbatchcode"/>
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="fname">Father Name:<span class="star">*</span></label>
                                        <input type="text" class="form-control text-uppercase" maxlength="50" name="txtfname" id="txtfname" data-mask="charonly" placeholder="Father's Name">     
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="mname">Mother's Name:<span class="star">*</span></label>
                                        <input type="text" class="form-control text-uppercase" maxlength="50" name="txtmothername" id="txtmothername" data-mask="charonly" placeholder="Mother's Name">     
                                    </div>

                                    <div class="col-sm-4 form-group">     
                                        <label for="dob">Date of Birth:<span class="star">*</span></label>								
                                        <input type="text" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="DD-MM-YYYY">
                                    </div>

                                </div>  

                                <div class="container">
                                    <div class="col-sm-4 form-group">     
                                        <label for="gender">Gender:</label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="genderMale" disabled="true" name="gender" value="Male"/> Male </label>
                                        <label class="radio-inline"> <input type="radio" id="genderFemale" checked="checked" name="gender" value="Female" onChange="findselected()"/> Female </label>
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="email">Marital Status:<span class="star">*</span></label>
											<select id="mstatus" name="mstatus" class="form-control proofdoc" >
												<option value="">Select</option>
												<option value="Single">Single</option>
												<option value="Married">Married</option>
												<option value="Divorcee">Divorcee</option>
												<option value="Widow">Widow</option>
                                                <option value="Saperated">Saperated</option>
											</select> 
                                    </div>

                                    <div class="col-sm-4 form-group">     
                                        <label for="address">Medium Of Study:</label> <br/> 
                                        <label class="radio-inline"> <input type="radio" id="mediumEnglish" value="English" name="Medium" /> English </label>
                                        <label class="radio-inline"> <input type="radio" id="mediumHindi" value="Hindi" name="Medium" checked="checked" /> Hindi </label>
                                    </div>

                                    <div class="col-sm-6 form-group"> 
                                        <label for="deptname">Physically Challenged:</label> <br/> 
                                        <label class="radio-inline"> <input type="radio" id="physicallyChStYes" value="Yes" class="proofdoc"  name="PH" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="physicallyChStNo" value="No" class="proofdoc" name="PH" checked="checked"/> No </label>
                                    </div>
                                </div>    
							<input type="hidden" name="workcategory" class="proofdoc" value = "Other"
								id="categoryOther" name="SubCategory"/>
							
                          <div class="container" style="display: none;">

                                    <div class="col-sm-12 " style='height: 55px;'>     
                                        <label for="subcategory">Female Reservation Category:</label> <br/>
										<label class="radio-inline"> <input type="radio" name="workcategory" class="proofdoc" value = "Aaganwadi" id="categoryAagan" name="SubCategory" /> Graduate Aaganwadi Worker </label> 
										<label class="radio-inline"> <input type="radio" name="workcategory" class="proofdoc" value = "Saathin" id="categorySaathin" name="SubCategory" /> Saathin </label>
                                        <label class="radio-inline"> <input type="radio" name="workcategory" class="proofdoc" checked value = "Other" id="categoryOther" name="SubCategory" /> Other </label> 
                                    </div>

                                </div>
                                <div class="container">

                                    <div class="col-sm-12 " style='height: 55px;'>     
                                    <label for="subcategory"> Victim of Violence: </label> <br />
                                        <label class="radio-inline"> <input type="radio" value="1" class="proofdoc" name="socialcategory" id="categoryViolenceYes" /> Yes </label> 
                                        <label class="radio-inline"> <input type="radio" checked value="0" class="proofdoc" name="socialcategory" id="categoryViolenceNo" /> No </label> 
                                    </div>
                                </div>

                                <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="empid">Proof Of Identity:</label>
                                        <select id="ddlidproof" name="ddlidproof" class="form-control proofdoc">
                                            <option value="">Select</option>
                                            <option value="PAN Card">PAN Card</option>
                                            <option value="Voter ID Card">Voter ID Card</option>
                                            <option value="Driving License">Driving License</option>
                                            <option value="Passport">Passport</option>
                                            <option value="Employer ID card">Employer ID card</option>
                                            <option value="Government ID Card">Governments ID Card</option>
                                            <option value="College ID Card">College ID Card</option>
                                            <option value="School ID Card">School ID Card</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="gpfno">Aadhaar No:<span class="star">*</span></label>
                                        <input type="text" class="keyup-cc form-control" maxlength="12" name="txtidno" id="txtidno" data-mask="adhar" placeholder="Aadhar No">
                                        <span id="ifsccode" style="font-size:10px;"></span>
                                        
                                        
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="bankdistrict">Jan Aadhar / Bhamashah No:</label>
                                        <input type="text" placeholder="Bhamashah No" class="form-control" id="txtBhamashahNo" name="txtBhamashahNo" data-mask="alpha" maxlength="13"/>
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="bankname">Email Id:</label>
                                        <input type="text" placeholder="Email Id" class="form-control" id="txtemail" name="txtemail" maxlength="100" />
                                        <span id="emailerr" class="alert-error" style="font-size:12px;font-weight: bold;"></span>
                                    </div>
                                   
                                </div>

                                <div class="container">                          
                                    <div class="col-sm-4 form-group"> 
                                        <label for="attempt">Address:<span class="star">*</span></label>  
                                        <textarea class="form-control" rows="3" id="txtAddress" onkeypress="javascript:return AllowAddress(event);" name="txtAddress" placeholder="Address"></textarea>
                                        <span id="addr" style="font-size:10px;"></span>
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="pan">PINCODE:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="txtPIN" id="txtPIN" data-mask="six-digits" placeholder="PINCODE" />
                                        <span id="pincode" style="font-size:10px;"></span>
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="bankaccount">Mobile No:<span class="star">*</span></label>
                                        <input type="text" class="form-control" readonly value="<?php echo $_SESSION['OTP_Mobile']; ?>"  name="txtApplicantmobile"  id="txtApplicantmobile" placeholder="Mobile No" data-mask="ten-number"/>
                                    </div>

                                     <div class="col-sm-4 form-group"> 
                                        <label for="bankdistrict">Phone No:</label>
                                        <input type="text" placeholder="Phone No"  class="form-control" id="txtResiPh" name="txtResiPh" data-mask="adhar" maxlength="10"/>
                                        <span id="pon" style="font-size:10px;"></span>
                                    </div>
                                </div>
								
								<div class="container">
                    <div class="col-sm-4 form-group"> 
                                    <label for="legislative_assembly">Legislative Assembly Area:</label><span class="star">*</span></label>
                                    <select id="legislative_assembly" name="Legislative_Assembly" class="form-control legislative_assembly" >

                                    </select>                                       
                                </div> 
                    <div class="col-sm-4 form-group">     
                        <label for="area">Area Type:<span class="star">*</span></label> <br/>
                        <label class="radio-inline"> <input type="radio" id="areaUrban" name="area" value="Urban" /> Urban </label>
                        <label class="radio-inline"> <input type="radio" id="areaRural" name="area" value="Rural" /> Rural </label>
                    </div>

                    <div class="container" id="Urban" style="display:none">

                                 <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Municipality Name:</label><span class="star">*</span></label>
                                    <select id="ddlMunicipalName" name="ddlMunicipalName" class="form-control" >

                                    </select>                                            
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Ward No.:</label><span class="star">*</span></label>
                                    <select id="ddlWardno" name="ddlWardno" class="form-control" >

                                    </select>
                                </div>

                            </div>
                    </div>
                            
                            <div class="container" id="Rural" style="display:none">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Panchayat Samiti/Block:</label><span class="star">*</span></label>
                                    <select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >

                                    </select>
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Gram Panchayat:</label><span class="star">*</span></label>
                                    <select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control" >

                                    </select>
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Village:</label><span class="star">*</span></label>
                                    <select id="ddlVillage" name="ddlVillage" class="form-control" >

                                    </select>
                                </div>
                                  
                            </div>
                            
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">Caste Category</div>
                            <div class="panel-body">
                                <div class="container">

                                    <div class="col-sm-4 form-group"> 
                                        <label for="category">Select Category:<span class="star">*</span></label>
                                        <select id="ddlCategory" name="ddlCategory" class="proofdoc form-control">
                                        </select>
                                    </div> 
                                    
                                    <div class="col-sm-4 form-group"> 
                                        <label for="caste">Caste:<span class="star">*</span></label>
                                        <input type="text" placeholder="Caste" class="form-control" id="txtcaste" name="txtcaste" data-mask="charonly" maxlength="100" />
                                    </div>  
                                    
                                    <div class="col-sm-4 form-group">     
                                        <label for="minoity">Do you belong to Minority?:</label> <br/> 
                                        <label class="radio-inline"> <input type="radio" id="minorityYes" value="Yes" name="Minority" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="minorityNo" value="No" name="Minority" checked="checked" /> No </label>
                                    </div>
                                  
                                    <div class="col-sm-4 form-group"> 
                                        <label for="religion">Select Religion:<span class="star">*</span></label>
                                        <select id="ddlReligion" name="ddlReligion" class="form-control">                                 
                                        </select>
                                    </div> 

                                    <div class="form-group1" style="margin-left: 15px;">     
                                        <label for="bpl">Do you belong to BPL category?:</label> 
                                        <label class="radio-inline"> <input type="radio" id="bplYes" value="Yes" name="BPL" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="bplNo" value="No" name="BPL" checked="checked" /> No </label>
                                    </div>
									
									 <div class="col-sm-4 form-group" id="tsp" style="display:none;">     
                                        <label for="bpl">Choose T.S.P:</label> <br/> 
                                        <label class="radio-inline"> <input type="radio" id="tspvalue" value="TSP" name="TSP"  onChange="findtsp()"/> T.S.P </label>
                                        <label class="radio-inline"> <input type="radio" id="nontsp" value="NonTSP" name="TSP" onChange="findnontsp()"/> Non T.S.P </label>
                                    </div>
									
									<div id="subtspcategory" style='display:none;'>
									 <div class="col-sm-4 form-group" >     
                                        <label for="subtspcategory">Select District:</label> 
										<select id="ddlTspDistrict" name="ddlTspDistrict" class="form-control">                                  
										
										</select>
									</div>     

									<div class="col-sm-4 form-group"> 
										<label for="marks">Tehsil:</label>
											<select id="ddlTspTehsil" name="ddlTspTehsil" class="form-control">                                 
											</select>
									</div>
								</div>
									
									<div class="col-sm-4 form-group" id="subnontspcategory" style='display:none;'>     
                                        <label for="bpl">Choose Non T.S.P:</label> <br/> 
                                        <label class="radio-inline"> <input type="radio" id="ntsp" value="Mada" name="Nontspval" /> Mada </label>
                                        <label class="radio-inline"> <input type="radio" id="nontspb" value="Bikhri" name="Nontspval" /> Bikhri </label>
                                    </div>

                                </div>

                            </div>
                        </div>
						<br>
					<div class="panel panel-info">
                            <div class="panel-heading">Minimum Qualification Details (10th Class / SSC)</div>
                            <div class="panel-body">
                                <div class="container">

                                    <div class="col-sm-4 form-group"> 
                                        <label for="university">Select Board:<span class="star">*</span></label>
                                        <select id="ddlMinBoard" name="ddlMinBoard" class="form-control">                                 
                                        </select>
                                    </div> 
                                   
									<div class="col-sm-4 form-group"> 
                                        <label for="rollno">Roll No.:<span class="star">*</span></label>
                                        <input type="text" placeholder="Roll No" class="form-control" id="txtMinRollNo" name="txtMinRollNo" maxlength="20" />
                                    </div> 
									
                                    <div class="col-sm-4 form-group"> 
                                        <label for="percentage">Percentage:<span class="star">*</span></label>
                                        <input type="text" placeholder="Percentage" class="form-control" id="txtMinPercentage" name="txtMinPercentage" data-mask="percent" min="33" maxlength="6" />
                                    </div>  
                                    
                                    <div class="col-sm-4 form-group"> 
                                        <label for="division">Select Division:<span class="star">*</span></label>
                                        <select id="ddlMinDivision" name="ddlMinDivision" class="form-control">                                 
                                        </select>
                                    </div> 
                                    
                                    <div class="col-sm-4 form-group"> 
                                        <label for="year">Year:<span class="star">*</span></label>
                                        <select id="txtMinYear" name="txtMinYear" class="form-control">
                                            <?php 
                                                for ($yr = date("Y"); $yr >= 1950; $yr--) {
                                                    echo '<option value"' . $yr . '">' . $yr . '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="month">Month of passing:<span class="star">*</span></label>
                                        <select id="ddlMinMonth" name="ddlMinMonth" class="form-control">                                 
                                        </select>
                                    </div>
                               
                            </div>
                    </div>
                        <br>
						 <div class="panel-heading">Higher Secondary Qualification Details (12th Class / HSC)</div>
                            <div class="panel-body">
                                <div class="container">
                                   <div class="col-sm-12"> 
                                        <label for="caste"> Select the type of school of passing 12th  Class / HSC:<span class="star">*</span></label>
                                        <label class="radio-inline"> <input type="radio" id="MinSchoolGovt" class="isMinSchoolType" value="1" name="ddlMinSchoolType" /> Government School </label>
                                        <label class="radio-inline"> <input type="radio" id="MinSchoolPvt" class="isMinSchoolType" value="2" name="ddlMinSchoolType" />Private or Other School</label>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="university">Select Board:<span class="star">*</span></label>
                                        <select id="ddlHSCBoard" name="ddlHSCBoard" class="form-control">                                 
                                        </select>
                                    </div>
									
									<div class="col-sm-4 form-group"> 
                                        <label for="rollno">Roll No.:<span class="star">*</span></label>
                                        <input type="text" placeholder="Roll No" class="form-control" id="txtHSCRollNo" name="txtHSCRollNo" maxlength="20" />
                                    </div> 
                                   
                                    <div class="col-sm-4 form-group"> 
                                        <label for="percentage">Percentage:<span class="star">*</span></label>
                                        <input type="text" placeholder="Percentage" class="form-control" id="txtHSCPercentage" name="txtHSCPercentage" data-mask="percent" min="33" maxlength="6" />
                                    </div>  
                                    
                                    <div class="col-sm-4 form-group"> 
                                        <label for="division">Select Division:<span class="star">*</span></label>
                                        <select id="ddlHSCDivision" name="ddlHSCDivision" class="form-control">                                 
                                        </select>
                                    </div> 
                                    
                                    <div class="col-sm-4 form-group"> 
                                        <label for="year">Year:<span class="star">*</span></label>
                                        <select id="txtHSCYear" name="txtHSCYear" class="form-control">
                                            <?php 
                                                for ($yr = date("Y"); $yr >= 1950; $yr--) {
                                                    echo '<option value"' . $yr . '">' . $yr . '</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                
                               
							   <div class="col-sm-4 form-group"> 
                                        <label for="month">Month of passing:<span class="star">*</span></label>
                                        <select id="ddlHSCMonth" name="ddlHSCMonth" class="form-control">                                 
                                        </select>
                                    </div>
                               
                            </div>
                        </div>
                        <br>
                        <div class="panel panel-info">
                            <div class="panel-heading">Highest Qualification Details</div>
                            <div class="panel-body">
                                <div class="container ishigherEdu">
                                    <div class="form-group1" style="margin-left: 15px;"> 
                                         <label for="branchname">Do you have Graduation Degree after 12th / HSC? <span class="star">*</span></label> 
                                        <label class="radio-inline"> <input type="radio" id="hrEduYes" class="ishrEdu" value="1" name="hrEdu" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="hrEduNo" checked class="ishrEdu" value="0" name="hrEdu" /> No </label>
										<input type="hidden" name="pg" id="pg" value="rscfa"/>
                                    </div>
                                </div>
                                <div class="container higherEdu" style="display:none;">
									<div class="col-sm-4 form-group"> 
                                        <label for="branchname">Qualification:<span class="star">*</span></label>
                                        <select id="ddlQualification" name="ddlQualification" class="form-control">                                  
                                        </select>
                                    </div>
									
                                    <div class="col-sm-4 form-group"> 
                                        <label for="university">Select University/Board:<span class="star">*</span></label>
                                        <select id="ddlBoard" name="ddlBoard" class="form-control">                                 
                                        </select>
                                    </div>

									<div class="col-sm-4 form-group"> 
                                        <label for="rollno">Roll No.:<span class="star">*</span></label>
                                        <input type="text" placeholder="Roll No" class="form-control" id="txtHighRollNo" name="txtHighRollNo" maxlength="20" />
                                    </div>
                                   
                                    <div class="col-sm-4 form-group"> 
                                        <label for="percentage">Percentage:<span class="star">*</span></label>
                                        <input type="text" placeholder="Percentage" class="form-control" id="txtPercentage" name="txtPercentage" data-mask="percent" min="33" maxlength="6" />
                                    </div>  
                                   
                                    <div class="col-sm-4 form-group"> 
                                        <label for="division">Select Division:<span class="star">*</span></label>
                                        <select id="ddlDivision" name="ddlDivision" class="form-control">                                 
                                        </select>
                                    </div> 
                                <div class="col-sm-4 form-group"> 
                                    <label for="year">Year:<span class="star">*</span></label>
                                    <select id="txtYear" name="txtYear" class="form-control">
                                        <?php 
                                            for ($yr = date("Y"); $yr >= 1950; $yr--) {
                                                echo '<option value"' . $yr . '">' . $yr . '</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="month">Month of passing:<span class="star">*</span></label>
                                    <select id="ddlMonth" name="ddlMonth" class="form-control">                                 
                                    </select>
                                </div>
                               
                            </div>
                        </div>
                        <br>
                        <div class="panel panel-info">
                            <div class="panel-heading">Attach Photo Sign</div>
                            <div class="panel-body">
                                <div class="container">
                                  <div class="col-sm-2" > 
                                        <label for="photo">Attach Photo:<span class="star">*</span></label> </br> </br> 
                                        <img id="uploadPreview1" src="images/samplephoto.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px">								  

                                        <input style="margin-top:5px !important;" id="uploadImage1" type="file" name="uploadImage1" onchange="checkPhoto(this);
                                                PreviewImage(1)"/>
                                            <div id="psize1">
                                                    <span  style="font-size:10px;">Note: JPG,JPEG,PNG Allowed Size = 3 to 5 KB</span>
                                            </div>
					
                                    </div>
                                    <div class="col-sm-1"> 

                                    </div>
                                    <div class="col-sm-2"> 
                                        <label for="photo">Attach Signature:<span class="star">*</span></label> </br> </br>
                                        <img id="uploadPreview2" src="images/samplesign.png" id="uploadPreview2" name="filePhoto" width="80px" height="107px">							  
                                        <input id="uploadImage2" type="file" name="uploadImage2" onchange="checkSign(this);
                                                PreviewImage(2)"/>
                                         <div id="psize2">
                                                <span   style="font-size:10px;">Note: JPG,JPEG,PNG Allowed Size = 1 to 3 KB</span>
                                         </div>
                                    </div>  
                                    <div class="col-sm-1"> 

                                    </div>
                               
							   </div>
                                
                                <div class="container">
                                    <div id="AttachPhotoSign" class="col-sm-4" style=" display: none;"> Photo and Sign Uploaded Sucessfully.</div>
                                    <input type="hidden" name="uploadpics" id="uploadpics" class="checkpoint" value="0">
                                    <div class="col-sm-2" id="btn_1"> 
                                        <div id="AttachPhotoSign" style=" display: none; padding-top: 10px;"> Photo and Sign Uploaded Sucessfully.</div>
                                        <input style="margin-top:50px !important;" type="button" name="btnUploadPhoto" id="btnUploadPhoto" class="btn btn-primary" value="Upload Photo Sign"/>   
                                        
                                    </div>
                                    <div class="col-sm-1"> 

                                    </div>

                                </div>
                            </div>
                        </div>

                        <br>
						
						 <div class="panel panel-info">
                            <div class="panel-heading">Attach Qualification Documents</div>
                            <div class="panel-body">
                                <div class="container">
                                    <div class="col-sm-2" > 
                                        <label for="photo">Attach Minimum Education Certificate (10th / SSC):<span class="star">*</span></label> </br>
                                        <img id="uploadPreview3" src="images/samplecertificate.png" id="uploadPreview3" name="fileMinQualifyCertificate" width="80px" height="107px">
<!--                                         onclick="javascript:document.getElementById('uploadImage3').click();"-->

                                        <input style="margin-top:5px !important;" id="uploadImage3" type="file" name="uploadImage3" onchange="checkProof(this, 3); PreviewImage(3)"/>	
					<span  id="psize3" style="font-size:10px;">Note: JPG,JPEG,PNG Allowed Size = 50 to 100 KB</span>
                                    </div>
									<div class="col-sm-1"> 

                                    </div>
                                    <div class="col-sm-2"> 
										
										<label for="photo">Attach Higher Secondary Certificate (12th / HSC):<span class="star">*</span></label> </br>
                                        <img id="uploadPreview13" src="images/samplecertificate.png" id="uploadPreview13" name="fileHSCQualifyCertificate" width="80px" height="107px">
<!--                                         onclick="javascript:document.getElementById('uploadImage3').click();"-->

                                        <input style="margin-top:5px !important;" id="uploadImage13" type="file"
										name="uploadImage13" onchange="checkProof(this, 13); PreviewImage(13)"/>	
					<span  id="psize5" style="font-size:10px;">Note: JPG,JPEG,PNG Allowed Size = 50 to 100 KB</span>
					
                                    </div>
                                    <div  id="highestEduProof" style="display:none;">
    					<div class="col-sm-2" > 
                                            <label for="photo">Attach Highest Education Certificate<span id="highedu"></span>:<span class="star">*</span></label> </br>
                                            <img id="uploadPreview4" src="images/samplecertificate.png" id="uploadPreview4" name="fileCertificate" width="80px" height="107px" >								  

                                            <input style="margin-top:5px !important;" id="uploadImage4" type="file" name="uploadImage4" onchange="checkProof(this, 4); PreviewImage(4)"/>	
                                            <span  id="psize4" style="font-size:10px;">Note: JPG,JPEG,PNG Allowed Size = 50 to 100 KB</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-1"> 

                                    </div>
                                </div>
                                <div class="container">
                                    
                                        <div id="AttachPhotoSign2" class="col-sm-4" style=" display: none;"> Qualification Document Uploaded Sucessfully.</div>
                                        <input type="hidden" name="uploadedudocs" id="uploadedudocs" class="checkpoint" value="0">
                                        <div class="col-sm-2" id="btn_2"> 
                                        <input style="margin-top:50px !important;" type="button" name="btnUploadCertificate" id="btnUploadCertificate" class="btn btn-primary" value="Upload Qualification Documents"/>
                                        </div>
                                   
                                    <div class="col-sm-1"> 

                                    </div>

                                </div>
                            </div>
                        </div>
						
						<br>
						<div class="panel panel-info" id="proof_documents">
                            <div class="panel-heading">Attach Proof Documents <span style="font-size:10px;"><b>Allowed Format: JPG, JPEG, PNG & Size = 50 to 100 KB</b></span></div>
                            <div class="panel-body">
                                <div class="container">
                                   <div class="col-sm-4" style="margin-bottom: 50px;"> 
                                        <label for="photo">Attach Age Proof Document:<span class="star">*</span></label> </br>
                                        <img id="uploadPreview5" src="images/sampleproof.png" id="uploadPreview5" name="fileProof" width="80px" height="107px">							  
                                        <input style="margin-top:5px !important;" id="uploadImage5" type="file" name="uploadImage5" onchange="checkProof(this, 5); PreviewImage(5)"/>
                                    </div>

                                    <div class="col-sm-4" style="margin-bottom: 50px;"> 
                                        <label for="photo">Attach Aadhaar Card:<span class="star">*</span></label> </br>
                                        <img id="uploadPreview12" src="images/sampleproof.png" id="uploadPreview12" name="fileCasteCertificate" width="80px" height="107px">                                  

                                        <input style="margin-top:5px !important;" id="uploadImage12" type="file" name="uploadImage12" onchange="checkProof(this, 12);
                                                PreviewImage(12)"/>  
                                    </div>

                                     <div class="col-sm-4" id="proofid6" style="margin-bottom: 50px; display:none;"> 
                                        <label for="photo">Attach Identity Proof Document<span id="idproof"></span>:<span class="star">*</span></label> </br>
                                        <img id="uploadPreview6" src="images/sampleproof.png" id="uploadPreview6" name="fileCasteCertificate" width="80px" height="107px">                                  

                                        <input style="margin-top:5px !important;" id="uploadImage6" type="file" name="uploadImage6" onchange="checkProof(this, 6);
                                                PreviewImage(6)"/>  
                                    </div>
                                   
									
                                    <div class="col-sm-4" id="proofid7" style="margin-bottom: 50px; display:none;"> 
                                        <label for="photo">Attach <span id="subcat"></span> Proof Certificate:<span class="star">*</span></label> </br>
                                        <img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="fileCasteCertificate" width="80px" height="107px" >								  

                                        <input style="margin-top:5px !important;" id="uploadImage7" type="file" name="uploadImage7" onchange="checkProof(this, 7);
                                                PreviewImage(7)"/>	
                                    </div>

                                    <div class="col-sm-4" id="proofid8" style="display:none; margin-bottom: 50px;"> 
                                        <label for="photo">Attach Caste Proof Certificate<span id="casttype"></span>:<span class="star">*</span></label> </br>
                                        <img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="fileCasteCertificate" width="80px" height="107px">                                  

                                        <input style="margin-top:5px !important;" id="uploadImage8" type="file" name="uploadImage8" onchange="checkProof(this, 8);
                                                PreviewImage(8)"/>  
                                    </div>
                                   

                                    <div class="col-sm-4" id="proofid9" style="display:none; margin-bottom: 50px;"> 
                                        <label for="photo">Attach Proof of Physical Disability:<span class="star">*</span></label> </br>
                                        <img id="uploadPreview9" src="images/sampleproof.png" id="uploadPreview9" name="fileCasteCertificate" width="80px" height="107px" >                                  

                                        <input style="margin-top:5px !important;" id="uploadImage9" type="file" name="uploadImage9" onchange="checkProof(this, 9);
                                                PreviewImage(9)"/>  
                                    </div>

                                    <div class="col-sm-4" id="proofid10" style="display:none; margin-bottom: 50px;"> 
                                        <label for="photo">Attach Proof of Marital Status<span id="mrgstatus"></span>:<span class="star">*</span></label> </br>
                                        <img id="uploadPreview10" src="images/sampleproof.png" id="uploadPreview10" name="fileCasteCertificate" width="80px" height="107px">                                  

                                        <input style="margin-top:5px !important;" id="uploadImage10" type="file" name="uploadImage10" onchange="checkProof(this, 10);
                                                PreviewImage(10)"/>  
                                    </div>

                                    <div class="col-sm-4" id="proofid11" style="display:none; margin-bottom: 50px;"> 
                                        <label for="photo">Attach Proof of Victim of Violence:<span class="star">*</span></label> </br>
                                        <img id="uploadPreview11" src="images/sampleproof.png" id="uploadPreview11" name="fileCasteCertificate" width="80px" height="107px" >                                  

                                        <input style="margin-top:5px !important;" id="uploadImage11" type="file" name="uploadImage11" onchange="checkProof(this, 11);
                                                PreviewImage(11)"/>  
                                    </div>
								
								 </div>
                                <div class="container">
                                    
                                        <div id="AttachPhotoSign3" class="col-sm-4" style=" display: none;"> Proof Documents Uploaded Sucessfully.</div>
                                        <input type="hidden" name="uploadproofdocs" id="uploadproofdocs" class="checkpoint" value="0">
                                        <div class="col-sm-2" id="btn_3"> 
                                        <input style="margin-top:50px !important;" type="button" name="btnUploadProof" id="btnUploadProof" class="btn btn-primary" value="Upload Proof Documents"/>
                                        </div>
                                    
                                        
                                    <div class="col-sm-1"> 

                                    </div>
                                    <div class="col-sm-2"> 
                                        <span id="msgproofdocs"></span>
                                    </div>
                                    <div class="col-sm-1"> 

                                    </div>

                                </div>
                            </div>
                        </div>

                        <br>
                        <div class="panel panel-info" id="proof_documents">
                            <div class="panel-heading"><center><b>Declaration</b></center></div>
                            <div class="panel-body">
                                <div class="container">
                                   <div> <p><b>I hereby declare that :</b></p>
                                    <ol style="line-height: 2;">
                                        <li>I have not applied at any other ITGK, under this scheme this year.</li>
                                        <li>I have applied for only one course (RS-CIT or RS-CFA) under this scheme this year.</li>
										<li>I have not taken benefit of this course or any other similar course in the past under any scheme run by Rajasthan Government.</li>
                                        <li>All the information and documents submitted along with my application form are true/correct up to best of my knowledge and belief.<br />If found incorrect, RKCL/WCD has the right to cancel my application.</li>
                                    </ol>           
                                    </div>
                                </div>
                                <p><label class="radio-inline"><b>I have agreed with all of my declarations as mentioned above.</b> <input type="checkbox" id="terms" class="terms" value="1" name="terms" /> </label></p>
                            </div>
                        </div>
						
                        <br><br>
                        <div class="container">
                            <div id="response"></div>
                        </div>
                        <div class="container">
                            <button class="btn btn-success" id="showpreview" style="display: none;"><i class="glyphicon glyphicon-eye-open" ></i> Preview form</button>
                            <input type="button" name="btnValidate" id="btnValidate" class="btn btn-primary" value="Validate" />
                           <!--  <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" /> -->
                            <input type="button" name="btnCancel" id="btnCancel" class="btn btn-primary" value="Cancel" />
                        </div>
                    </div>
                </form>


            </div>
        </div>

    </div>   

</div>
<div class="container">
    <div class="row">

<!--    <div class="text-center">
        <a class="btn btn-success" href="#successModal" data-toggle="modal"><i class="glyphicon glyphicon-eye-open"></i> Valid form</a>
        <button class="btn btn-success" id="showpreview"><i class="glyphicon glyphicon-eye-open"></i> Preview form</button>
    </div>
 -->

    <!--######################## Modal #############################-->


    <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width:100%">
            <div class="modal-content">
                <div class="modal-header modal-header-warning">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">[ × ]</button>
                    <h2><i class="glyphicon glyphicon-cloud"></i>Preview of Information Filled in Application Form</h2>
                </div>
                <div class="modal-body">
                 <form name="frmoasisadmissionrecheck" id="frmoasisadmissionrecheck" class="form-inline" role="form" enctype="multipart/form-data"> 
                 
                  <div class="panel panel-info">
                            <div class="panel-heading">Your Prefered Center Detail 1</div>
                            <div class="panel-body">
                               
                                    <div class="col-sm-4"> 
                                   
                                        <label for="learnercode">Organization/Center Name:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtName1m" id="txtName1m" placeholder="Name of the Organization/Center">
                                    </div>


                                    <div class="col-sm-4"> 
                                        <label for="ename">Mobile No:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtMobilem" id="txtMobilem" placeholder="Registration No">     
                                    </div>


                                    <div class="col-sm-4"> 
                                        <label for="edistrict">Address:</label>
                                        
                                        <textarea class="form-control"  readonly="true" rows="4" cols="50" name="txtITGKAddressm" id="txtITGKAddressm"  placeholder="District">  </textarea>
                                        <div id="applicationsC1"></div>   
                                    </div>
                                

                            </div>
                        </div>
                        
                        
                        <div class="panel panel-info">
                            <div class="panel-heading">Your Prefered Center Detail 2</div>
                            <div class="panel-body">
                                
                                    <div class="col-sm-4">     
                                        <label for="learnercode">Organization/Center Name:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtName2m" id="txtName2m" placeholder="Name of the Organization/Center">
                                    </div>


                                    <div class="col-sm-4"> 
                                        <label for="ename">Mobile No:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtMobile2m" id="txtMobile2m" placeholder="Registration No">     
                                    </div>


                                    <div class="col-sm-4"> 
                                        <label for="edistrict">Address:</label>
    
                                        <textarea  class="form-control" readonly="true" rows="4" cols="50" name="txtITGKAddress2m" id="txtITGKAddress2m"  placeholder="District">  </textarea>

                                       <div id="applicationsC2"></div>
                                    </div>
                                                            

                            </div>
                        </div>

                    
                    
                    
                    
                    
                    <div id="main-content" >
                        <br>
                        <div class="panel panel-info">
                            <div class="panel-heading">Applicant Details</div>
                            <div class="panel-body">
                                
                                
                                     <div class="col-sm-4"> 
                                        <label for="ename">Applicant Photo:</label>
                                        <img id="uploadPreview1modal" src="" name="filePhotomodal" width="80px" height="107px">  
                                    </div>    
                                    <div class="col-sm-4"> 
                                        <label for="ename">Applicant Sign:</label>
                                        <img id="uploadPreview2modal" src="" name="filePhotomodal" width="80px" height="107px">  
                                    </div>
                                    <div class="col-sm-4">     
                                        <label for="learnercode">Applicant Name:<span class="star">*</span></label>
                                        <input type="text" class="form-control text-uppercase" readonly="true" maxlength="50" name="txtlnamem" id="txtlnamem" data-mask="charonly" placeholder="Applicant Name">
                                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                                        <input type="hidden" class="form-control" maxlength="50" name="txtAdmissionCodem" id="txtAdmissionCodem"/>
                                        <select id="ddlBatch" style="display:none;" name="ddlBatch" class="form-control">  </select>
                                        <input type="hidden" class="form-control" maxlength="50" name="txtCourseFeem" id="txtCourseFeem"/>
                                        <input type="hidden" class="form-control" maxlength="50" name="txtInstallModem" id="txtInstallModem"/>
                                        <input type="hidden" class="form-control" name="txtphotom" id="txtphotom"/>
                                        <input type="hidden" class="form-control"  name="txtsignm" id="txtsignm"/>
                                        <input type="hidden" class="form-control" maxlength="50" name="txtmodem" id="txtmodem" value="1" />
                                        <input type="hidden" class="form-control"  name="txtLearnercode" id="txtLearnercodem"/>
                                        <input type="hidden" class="form-control"  name="txtbatchcode" id="txtbatchcodem"/>
                                    </div>

                                    <div class="col-sm-4"> 
                                        <label for="fname">Father Name:<span class="star">*</span></label>
                                        <input type="text"  readonly="true" class="form-control text-uppercase" maxlength="50" name="txtfnamem" id="txtfnamem" data-mask="charonly" placeholder="Father's Name">     
                                    </div>

                                    <div class="col-sm-4"> 
                                        <label for="mname">Mother's Name:<span class="star">*</span></label>
                                        <input type="text" readonly="true" class="form-control text-uppercase" maxlength="50" name="txtmothernamem" id="txtmothernamem" data-mask="charonly" placeholder="Mother's Name">     
                                    </div>

                                    <div class="col-sm-4">     
                                        <label for="dob">Date of Birth:<span class="star">*</span></label>                              
                                        <input type="text" readonly="true" class="form-control" readonly="true" maxlength="50" name="dobm" id="dobm"  placeholder="DD-MM-YYYY">
                                    </div>
                                    
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="ename">Gender:</label>
                                        <input type="text" class="form-control" readonly="true" name="genderMalem" id="genderMalem" >     
                                    </div>
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="ename">Marital Status:</label>
                                        <input type="text" class="form-control" readonly="true" name="mstatusm" id="mstatusm" >     
                                    </div>
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="ename">Medium Of Study:</label>
                                        <input type="text" class="form-control" readonly="true" name="mediumEnglishm" id="mediumEnglishm" >     
                                    </div>
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="ename">Physically Challenged:</label>
                                        <input type="text" class="form-control" readonly="true" name="physicallyChStYesm" id="physicallyChStYesm" >     
                                    </div>
                                    
                                    
                                   <!--  <div class="col-sm-4"> 
                                        <label for="ename">Female Reservation Category:</label>
                                        <input type="text" class="form-control" readonly="true" name="workcategorym" id="workcategorym" >     
                                    </div> -->
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="ename">Victim of Violence:</label>
                                        <input type="text" class="form-control" readonly="true" name="proofdocm" id="proofdocm" >     
                                    </div>
                                    
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="ename">Proof Of Identity:</label>
                                        <input type="text" class="form-control" readonly="true" name="ddlidproofm" id="ddlidproofm" >     
                                    </div>

                        
                                    <div class="col-sm-4"> 
                                        <label for="gpfno">Aadhaar No:<span class="star">*</span></label>
                                        <input type="text" class="keyup-cc form-control" readonly="true" maxlength="12" name="txtidnom" id="txtidnom" data-mask="adhar" placeholder="Aadhar No">
                                        <span id="ifsccode" style="font-size:10px;"></span>
                                        
                                        
                                    </div>


                                    <div class="col-sm-4"> 
                                        <label for="bankdistrict">Jan Aadhar / Bhamashah No:</label>
                                        <input type="text" placeholder="Bhamashah No"  readonly="true" class="form-control" id="txtBhamashahNom" name="txtBhamashahNom" data-mask="alpha" maxlength="13"/>
                                    </div>

                                    <div class="col-sm-4"> 
                                        <label for="bankname">Email Id:</label>
                                        <input type="text" placeholder="Email Id" readonly="true" class="form-control" id="txtemailm" name="txtemailm" maxlength="100" />
                                        <span id="emailerr" class="alert-error" readonly="true" style="font-size:12px;font-weight: bold;"></span>
                                    </div>
                                   
                                                  
                                    

                                    <div class="col-sm-4"> 
                                        <label for="pan">PINCODE:<span class="star">*</span></label>
                                        <input type="text" class="form-control"  readonly="true" name="txtPINm" id="txtPINm" data-mask="six-digits" placeholder="PINCODE" />
                                        <span id="pincode" style="font-size:10px;" readonly="true"></span>
                                    </div>

                                    <div class="col-sm-4"> 
                                        <label for="bankaccount">Mobile No:<span class="star">*</span></label>
                                        <input type="text" class="form-control" readonly="true" readonly value="<?php echo $_SESSION['OTP_Mobile']; ?>"  name="txtApplicantmobilem"  id="txtApplicantmobilem" placeholder="Mobile No" data-mask="ten-number"/>
                                    </div>

                                     <div class="col-sm-4"> 
                                        <label for="bankdistrict">Phone No:</label>
                                        <input type="text" placeholder="Phone No" readonly="true" class="form-control" id="txtResiPhm" name="txtResiPhm" data-mask="adhar" maxlength="10"/>
                                        <span id="pon" style="font-size:10px;"></span>
                                    </div>
                               
                                    
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="bankdistrict">Legislative Assembly Area:</label>
                                        <input type="text" class="form-control" id="Legislative_Assemblym" readonly="true" name="Legislative_Assemblym" maxlength="10"/>
                                        <span id="pon" style="font-size:10px;"></span>
                                    </div>
                                    
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="bankdistrict">Area Type:</label>
                                        <input type="text"  class="form-control" id="areaUrbanm" readonly="true" name="areaUrbanm"  maxlength="10"/>
                                        <span id="pon" style="font-size:10px;"></span>
                                    </div>
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="attempt">Address:<span class="star">*</span></label>  
                                        <textarea class="form-control" rows="2" id="txtAddressm" readonly="true" name="txtAddressm" placeholder="Address"></textarea>
                                        <span id="addr" style="font-size:10px;"></span>
                                    </div>
                                    
                                    <div class="col-sm-4"> 
                                        <label for="bankdistrict">Municipality Name:</label>
                                        <input type="text"  class="form-control" id="ddlMunicipalNamem"  readonly="true" name="ddlMunicipalNamem" maxlength="10"/>
                                        <span id="pon" style="font-size:10px;"></span>
                                    </div>
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="bankdistrict">Ward No:</label>
                                        <input type="text"   class="form-control" id="ddlWardnom"  readonly="true" name="ddlWardnom"  maxlength="10"/>
                                        <span id="pon" style="font-size:10px;"></span>
                                    </div>
                                    
                                <div class="col-sm-4"> 
                                        <label for="bankdistrict">Panchayat Samiti/Block:</label>
                                        <input type="text"   class="form-control" id="ddlPanchayatm" readonly="true" name="ddlPanchayatm"  maxlength="10"/>
                                        <span id="pon" style="font-size:10px;"></span>
                                    </div>  
                                    
                                    
                                    
                                <div class="col-sm-4"> 
                                        <label for="bankdistrict">Gram Panchayat:</label>
                                        <input type="text"   class="form-control" id="ddlGramPanchayatm" readonly="true" name="ddlGramPanchayatm" maxlength="10"/>
                                        <span id="pon" style="font-size:10px;"></span>
                                    </div>



                                    <div class="col-sm-4"> 
                                        <label for="bankdistrict">Village:</label>
                                        <input type="text" class="form-control" id="ddlVillagem" readonly="true" name="ddlVillagem"  maxlength="10"/>
                                        <span id="pon" style="font-size:10px;"></span>
                                    </div>  
                                                                    
                                
                            </div>
                        </div>
                        
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">Caste Category</div>
                            <div class="panel-body">
                            
                            
                            <div class="col-sm-4"> 
                                        <label for="bankdistrict">Select Category:</label>
                                        <input type="text" class="form-control" id="ddlCategorym"  readonly="true" name="ddlCategorym"  maxlength="10"/>
                                        <span id="pon" style="font-size:10px;"></span>
                                    </div>  
                                

                                   
                                    <div class="col-sm-4"> 
                                        <label for="caste">Caste:<span class="star">*</span></label>
                                        <input type="text" class="form-control" id="txtcastem" readonly="true" name="txtcastem"  maxlength="100" />
                                    </div>  
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="caste">Do you belong to Minority?:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="minorityYesm" name="minorityYesm"  maxlength="100" />
                                    </div> 


                                    <div class="col-sm-4"> 
                                        <label for="caste">Select Religion:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlReligionm" name="ddlReligionm"  maxlength="100" />
                                    </div> 


                                    <div class="col-sm-4"> 
                                        <label for="caste">Do you belong to BPL category?:<span class="star">*</span></label>
                                        <input type="text" class="form-control" readonly="true" id="bplYesm" name="bplYesm" maxlength="100" />
                                    </div> 


                                    <div class="col-sm-4"> 
                                        <label for="caste">Choose T.S.P:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" id="tspvaluem" readonly="true" name="tspvaluem"  maxlength="100" />
                                    </div> 


                                    <div class="col-sm-4"> 
                                        <label for="caste">Select District:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlTspDistrictm" name="ddlTspDistrictm"  maxlength="100" />
                                    </div> 
                                    
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="caste">Tehsil:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlTspTehsilm" name="ddlTspTehsilm"  maxlength="100" />
                                    </div> 
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="caste">Choose Non T.S.P:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ntspm" name="ntspm" maxlength="100" />
                                    </div> 


                            </div>
                        </div>
                        <br>
                        <div class="panel panel-info">
                            <div class="panel-heading">Minimum Qualification Details (10th Class / SSC)</div>
                            <div class="panel-body">
                            
                            
                                    <div class="col-sm-4"> 
                                        <label for="caste"> Board:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlMinBoardm" name="ddlMinBoardm"  maxlength="100" />
                                    </div> 
                                

                                   
                                    
                                    <div class="col-sm-4"> 
                                        <label for="rollno">Roll No.:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="txtMinRollNom" name="txtMinRollNom" maxlength="20" />
                                    </div> 
                                   
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Percentage:<span class="star">*</span></label>
                                        <input type="text"  class="form-control"  readonly="true"  id="txtMinPercentagem" name="txtMinPercentagem" min="33" maxlength="6" />
                                    </div>  
                                    
                                    
                                     <div class="col-sm-4"> 
                                        <label for="percentage">Division:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlMinDivisionm" name="ddlMinDivisionm"  min="33" maxlength="6" />
                                    </div> 
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Year:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="txtMinYearm" name="txtMinYearm"  min="33" maxlength="6" />
                                    </div> 
                                    
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Month of passing:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlMinMonthm" name="ddlMinMonthm"  min="33" maxlength="6" />
                                    </div> 
                                    
                                   
                          
                        </div>
                        <br>
                        <div class="panel-heading">Higher Secondary Qualification Details (12th Class / HSC)</div>
                            <div class="panel-body">
                            
                                    <label for="percentage">Type of school of passing 10th Class / SSC <span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlMinSchoolTypem" name="ddlMinSchoolTypem"  min="33" maxlength="6" />
                                    <div class="col-sm-4"> 
                                        <label for="caste"> Board:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlHSCBoardm" name="ddlHSCBoardm"  maxlength="100" />
                                    </div> 
                                

                                   
                                    
                                    <div class="col-sm-4"> 
                                        <label for="rollno">Roll No.:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="txtHSCRollNom" name="txtHSCRollNom" maxlength="20" />
                                    </div> 
                                   
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Percentage:<span class="star">*</span></label>
                                        <input type="text"  class="form-control"  readonly="true"  id="txtHSCPercentagem" name="txtHSCPercentagem" min="33" maxlength="6" />
                                    </div>  
                                    
                                    
                                     <div class="col-sm-4"> 
                                        <label for="percentage">Division:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlHSCDivisionm" name="ddlHSCDivisionm"  min="33" maxlength="6" />
                                    </div> 
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Year:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="txtHSCYearm" name="txtHSCYearm"  min="33" maxlength="6" />
                                    </div> 
                                    
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Month of passing:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlHSCMonthm" name="ddlHSCMonthm"  min="33" maxlength="6" />
                                    </div> 
                                    
                                   
                          
                        </div>
                        <br>
                        <div class="panel panel-info">
                            <div class="panel-heading">Highest Qualification Details</div>
                            <div class="panel-body">
                               
                                    
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="percentage">You have Graduation  Degree after 12th / HSC <span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="hrEduYesm" name="hrEduYesm"  min="33" maxlength="6" />
                                    </div> 
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Qualification<span class="star">*</span></label>
                                        <input type="text"  class="form-control"  readonly="true" id="ddlQualificationm" name="ddlQualificationm" min="33" maxlength="6" />
                                    </div> 
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Select University/Board:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlBoardm" name="ddlBoardm" min="33" maxlength="6" />
                                    </div> 
                               
                                    <div class="col-sm-4"> 
                                        <label for="rollno">Roll No.:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="txtHighRollNom" name="txtHighRollNom" maxlength="20" />
                                    </div> 
                                   
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Percentage:<span class="star">*</span></label>
                                        <input type="text"  class="form-control"  readonly="true" id="txtPercentagem" name="txtPercentagem" min="33" maxlength="6" />
                                    </div>
                                        
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Select Division:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="ddlDivisionm" name="ddlDivisionm" min="33" maxlength="6" />
                                    </div>
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Year:<span class="star">*</span></label>
                                        <input type="text"  class="form-control" readonly="true" id="txtYearm" name="txtYearm" min="33" maxlength="6" />
                                    </div>
                                    
                                    
                                    
                                    <div class="col-sm-4"> 
                                        <label for="percentage">Month of passing:<span class="star">*</span></label>
                                        <input type="text"  class="form-control"  readonly="true" id="ddlMonthm" name="ddlMonthm" min="33" maxlength="6" />
                                    </div>
                                    
                                   
                                    
                                
                               
                        </div>
                        </div>

                        <br>        
                        
                        <div class="container">
                            <div id="response1"></div>
                        </div>
                    </div>
                </form>


            </div>
        
                <div class="clearfix"></div>
                <div class="modal-footer">
                   <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close Preview</button>
                    <button type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary pull-left"> Submit</button>
                    
                </div>
        </div>
        
        
    
                  

                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</body>
<?php include'./common/message.php'; ?>
<?php include ('footer.php'); ?>
<script src="scripts/oasisadmissionfileupload.js"></script>
<script src="scripts/oasisadmissioncertificateupload.js"></script>
<script src="scripts/oasisadmissionproofupload.js"></script>
<script src="scripts/oasisadmissionhscupload.js"></script>

<style>
    #errorBox
    {	color:#F00;	 }
    .error{
        color:#f00;
        font-size: 11px;
    } 
    .alert-error
    {   color:#F00; margin-bottom: 20px;  } 
</style>
<script type="text/javascript">
    function showpreviewmodal(){
         // $("#showpreview").click(function () {
         // $("#Legislative_Assemblym").val("abc");
         // uploadPreview1modal
         // $('.img1 img').attr('src');
        $("#uploadPreview1modal").attr("src",$('#uploadPreview1').attr('src'));
        $("#uploadPreview2modal").attr("src",$('#uploadPreview2').attr('src'));
        $("#txtName2m").val($("#txtName2").val());
        $("#txtName1m").val($("#txtName1").val());
        $("#txtMobilem").val($("#txtMobile").val());
        $("#txtITGKAddressm").val($("#txtITGKAddress").val());
        $("#txtMobile2m").val($("#txtMobile2").val());
        $("#txtITGKAddress2m").val($("#txtITGKAddress2").val());
        $("#txtlnamem").val($("#txtlname").val());
        $("#txtfnamem").val($("#txtfname").val());
        $("#txtmothernamem").val($("#txtmothername").val());
        $("#dobm").val($("#dob").val());
        $("#mstatusm").val($("#mstatus").val());
        $("#genderMalem").val("Female");

        $('#mediumEnglishm').val($("input[name='Medium']:checked").val());
        $('#physicallyChStYesm').val($("input[name='PH']:checked").val());
        // $('#workcategorym').val($("input[name='workcategory']:checked").val());
        if($("input[name='socialcategory']:checked").val() == 1){
            $('#proofdocm').val("Yes");
        }
        else{
            $('#proofdocm').val("No");
        }
        $("#ddlidproofm").val($("#ddlidproof").val());
        $("#txtidnom").val($("#txtidno").val());
        $("#txtBhamashahNom").val($("#txtBhamashahNo").val());
        $("#txtemailm").val($("#txtemail").val());
        $("#txtAddressm").val($("#txtAddress").val());
        $("#txtPINm").val($("#txtPIN").val());
        $("#txtApplicantmobilem").val($("#txtApplicantmobile").val());
        $("#txtResiPhm").val($("#txtResiPh").val());
        $("#Legislative_Assemblym").val($("#legislative_assembly option:selected").text());
        // document.getElementById("Legislative_Assemblym").value = "abc";
        $("#txtResiPhm").val($("#txtResiPh").val());
        $('#areaUrbanm').val($("input[name='area']:checked").val());
        // $( "#myselect option:selected" ).text();
         // alert($("#legislative_assembly option:selected").text());
        $("#ddlPanchayatm").val($("#ddlPanchayat option:selected").text());
        $("#ddlGramPanchayatm").val($("#ddlGramPanchayat option:selected").text());
        $("#ddlVillagem").val($("#ddlVillage option:selected").text());
        $("#ddlMunicipalNamem").val($("#ddlMunicipalName option:selected").text());
        $("#ddlWardnom").val($("#ddlWardno option:selected").text());
        $("#ddlCategorym").val($("#ddlCategory option:selected").text());
        $("#txtcastem").val($("#txtcaste").val());
        $('#minorityYesm').val($("input[name='Minority']:checked").val());
        $("#ddlReligionm").val($("#ddlReligion option:selected").text());
        $('#bplYesm').val($("input[name='BPL']:checked").val());
        if($("input[name='ddlMinSchoolType']:checked").val() == 1){
            $('#ddlMinSchoolTypem').val("Government School");
        }
        else if($("input[name='ddlMinSchoolType']:checked").val() == 2){
            $('#ddlMinSchoolTypem').val("Private or Other School");
        }
        else{
            $('#ddlMinSchoolTypem').val("");
        }
        $("#ddlMinBoardm").val($("#ddlMinBoard option:selected").text());
        $("#ddlMinDivisionm").val($("#ddlMinDivision option:selected").text());
        $("#txtMinYearm").val($("#txtMinYear option:selected").text());
        $("#ddlMinMonthm").val($("#ddlMinMonth option:selected").text());
        $("#txtMinRollNom").val($("#txtMinRollNo").val());
        $("#txtMinPercentagem").val($("#txtMinPercentage").val());
        if($("input[name='hrEdu']:checked").val() == 1){
            $('#hrEduYesm').val("Yes");
        }
        else{
            $('#hrEduYesm').val("No");
        }
        $("#ddlQualificationm").val($("#ddlQualification option:selected").text());
        $("#ddlBoardm").val($("#ddlBoard option:selected").text());
        $("#ddlDivisionm").val($("#ddlDivision option:selected").text());
        $("#txtYearm").val($("#txtYear option:selected").text());   
        $("#ddlMonthm").val($("#ddlMonth option:selected").text());
        $("#txtHighRollNom").val($("#txtHighRollNo").val());
        $("#txtPercentagem").val($("#txtPercentage").val());
        $('#tspvaluem').val($("input[name='TSP']:checked").val());
        $("#ddlTspDistrictm").val($("#ddlTspDistrict option:selected").text());
        $("#ddlTspTehsilm").val($("#ddlTspTehsil option:selected").text());
        $('#ntspm').val($("input[name='Nontspval']:checked").val());
        $("#ddlHSCBoardm").val($("#ddlHSCBoard option:selected").text());
        $("#ddlHSCDivisionm").val($("#ddlHSCDivision option:selected").text());
        $("#txtHSCYearm").val($("#txtHSCYear option:selected").text());
        $("#ddlHSCMonthm").val($("#ddlHSCMonth option:selected").text());
        $("#txtHSCRollNom").val($("#txtHSCRollNo").val());
        $("#txtHSCPercentagem").val($("#txtHSCPercentage").val());

        $('#successModal').modal({show: true});
     // });
 }

    function AllowIFSC(ifsc) {//
            //var reg = /^[1-9]{6}\d*$/;
            var reg = /^[1-9][0-9]*$/;
            //var ifsc = `SBIN1234567`;
           
            if(ifsc!=''){
                if (ifsc.match(reg)) {
                     $('#ifsccode').empty();
                }
                else {
                    $('#ifsccode').empty();
                    //$('#txtIfscNo').val('');
                    $('#ifsccode').append("<span style='color:red; font-size: 12px;'>You have entered wrong Aadhar No.</span>");

                    return false;
                }
            }else{ //alert(ifsc);
                    $('#ifsccode').empty();
            }
        }

        function AllowAddress(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9a-zA-Z.-/ ,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
    $('#dob').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        autoclose: true
    });
</script>

<script type="text/javascript">
    function PreviewImage(no) {
		
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
</script>	

<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#txtUploadScanDoc').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }
        else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }
        document.getElementById("txtUploadScanDoc").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkPhoto(target) {
        var ext = $('#uploadImage1').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage1").value = '';
            return false;
        }

        if (target.files[0].size > 5000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 5 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        else if (target.files[0].size < 3000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        document.getElementById("uploadImage1").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkSign(target) {
        var ext = $('#uploadImage2').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage2").value = '';
            return false;
        }

        if (target.files[0].size > 3000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 3 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        else if (target.files[0].size < 1000)
        {
            alert("Image size should be greater than 1 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        document.getElementById("uploadImage2").innerHTML = "";
        return true;
    }

    function checkProof(target, sn) {
		if(target.files[0].size > 0) 
            {
                      $('#btn_3').show(3000);   
                      $('#AttachPhotoSign3').hide(3000);   
            }
        var fname = target.value;
        var ext = fname.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            target.value = '';
            document.getElementById("uploadPreview" + sn).src = 'images/sampleproof.png';
            return false;
        }

        if (target.files[0].size > 100000) {
            alert("Image size should less or equal 100 KB");
            target.value = '';
            document.getElementById("uploadPreview" + sn).src = 'images/sampleproof.png';
            return false;
        }
        else if (target.files[0].size < 50000)
        {
            alert("Image size should be greater than 50 KB");
            target.value = '';
            document.getElementById("uploadPreview" + sn).src = 'images/sampleproof.png';
            return false;
        }
        
        return true;
    }

</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    function showproofdoc(docid) {
        $('#proofid' + docid).show();
    }

    function clearproofdoc(docid) {
        document.getElementById("uploadImage" + docid).value = '';
        document.getElementById("uploadPreview" + docid).src = 'images/sampleproof.png';
        $('#proofid' + docid).hide();
    }

    $(document).ready(function () {
        
        
        
        //alert(BatchCode);

        $('.ishrEdu').click(function () {
            toogleHigherEdu(this.value);
        });

        function toogleHigherEdu(vl) {
            $('#ddlBoard').val('');
            if (vl == 1) {
                $('.higherEdu').show(1000);
                $('#highestEduProof').show();
                $('#btn_2').show();   
                $('#uploadedudocs').val(0);   
                $('#AttachPhotoSign2').hide();
                $('#uploadImage4').show();
                $('#uploadImage4').val('');
                $("#uploadPreview4").attr('src', 'images/samplecertificate.png');
            } else {
                //alert($('#uploadPreview3').attr('src'));
                //alert("sunil");
                if($('#uploadPreview3').attr('src')!='images/samplecertificate.png')
                {
                  $('#btn_2').hide();    
                }
				$('#highestEduProof').hide();
                $('.higherEdu').hide(1000);
            }
        }

        $('.proofdoc').click(function () {
            switch(this.id) {
                case 'mstatus': 
                    if (this.value == 'Divorcee' || this.value == 'Widow' || this.value == 'Saperated') {
                        $('#uploadImage10').show(3000);
                        $('#btn_3').show();
                        $('#uploadproofdocs').val(0);
                        $('#AttachPhotoSign3').hide();
                        $('#uploadImage10').val('');
                        $("#uploadPreview10").attr('src', 'images/sampleproof.png');
                        $("#uploadPreview10").attr('style', 'display: block');
                        $('#mrgstatus').html('(' + this.value + ')');
                        showproofdoc(10);
                    } else {
                        clearproofdoc(10);
                        if($('#uploadPreview5').attr('src')!='images/sampleproof.png' && $('#uploadPreview6').attr('src')!='images/sampleproof.png')
                            {
                              if($('#ddlCategory').val() == '2' || $('#ddlCategory').val() == '3' || $('#categoryAagan').is(':checked') || $('#categorySaathin').is(':checked')  || $('#categoryViolenceYes').is(':checked') || $('#physicallyChStYes').is(':checked') ) { 
                                  //alert("it's checked"); 
                              }else{
                              $('#btn_3').hide();
                              }    
                            }
                    }
                    break;
                case 'physicallyChStYes':
                case 'physicallyChStNo': 
                    if (this.value == 'Yes') {
                        showproofdoc(9);
                        $('#uploadImage9').show(3000);
                        $('#btn_3').show();
                        $('#uploadproofdocs').val(0);
                        $('#AttachPhotoSign3').hide();
                        $('#uploadImage9').val('');
                        $("#uploadPreview9").attr('src', 'images/sampleproof.png');
                        $("#uploadPreview9").attr('style', 'display: block');
                     } else {
                        clearproofdoc(9);
                        if($('#uploadPreview5').attr('src')!='images/sampleproof.png' && $('#uploadPreview6').attr('src')!='images/sampleproof.png')
                            {
                              if($('#mstatus').val() == 'Divorcee' || $('#mstatus').val() == 'Widow' || $('#ddlCategory').val() == '2' || $('#ddlCategory').val() == '3' || $('#categoryAagan').is(':checked') || $('#categorySaathin').is(':checked')  || $('#categoryViolenceYes').is(':checked') ) { 
                                  //alert("it's checked"); 
                              }else{
                              $('#btn_3').hide();
                              }    
                            }
                    }
                    break;
                case 'categoryViolenceYes':
                case 'categoryViolenceNo': 
                    if (this.value == 1) {
                        showproofdoc(11);
                        $('#uploadImage11').show();
                        $('#btn_3').show();
                        $('#uploadproofdocs').val(0);
                        $('#AttachPhotoSign3').hide();
                        $('#uploadImage11').val('');
                        $("#uploadPreview11").attr('src', 'images/sampleproof.png');
                        $("#uploadPreview11").attr('style', 'display: block');
                    } else {
                        clearproofdoc(11);
                        if($('#uploadPreview5').attr('src')!='images/sampleproof.png' && $('#uploadPreview6').attr('src')!='images/sampleproof.png')
                            {
                              if($('#mstatus').val() == 'Divorcee' || $('#mstatus').val() == 'Widow' || $('#ddlCategory').val() == '2' || $('#ddlCategory').val() == '3' || $('#categoryAagan').is(':checked') || $('#categorySaathin').is(':checked') || $('#physicallyChStYes').is(':checked')  ) { 
                                  //alert("it's checked"); 
                              }else{
                              $('#btn_3').hide();
                              }    
                            }
                    }
                    break;
                case 'ddlCategory':
                    if (this.value == 2 || this.value == 3) {
                        $('#uploadImage8').show();
                        $('#btn_3').show();
                        $('#uploadproofdocs').val(0);
                        $('#AttachPhotoSign3').hide();
                        $('#uploadImage8').val('');
                        $("#uploadPreview8").attr('src', 'images/sampleproof.png');
                        $("#uploadPreview8").attr('style', 'display: block');
                        var casttype = 'SC';
                        if (this.value == 3) casttype = 'ST';
                        $('#casttype').html('(' + casttype + ')');
                        showproofdoc(8);
                    } else {
                        clearproofdoc(8);
                        if($('#uploadPreview5').attr('src')!='images/sampleproof.png' && $('#uploadPreview6').attr('src')!='images/sampleproof.png')
                            {
                              if($('#mstatus').val() == 'Divorcee' || $('#mstatus').val() == 'Widow' || $('#mstatus').val() == 'Saperated' || $('#categoryAagan').is(':checked') || $('#categorySaathin').is(':checked')  || $('#categoryViolenceYes').is(':checked') || $('#physicallyChStYes').is(':checked') ) { 
                                  //alert("it's checked"); 
                              }else{
                              $('#btn_3').hide();
                              }    
                            }
                    }
                    break;
                case 'categoryAagan':
                case 'categorySaathin':
                case 'categoryOther':
                    FillQualification();
                    if (this.value != 'Other') {
                        $('#subcat').html('(' + this.value + ')');
                        showproofdoc(7);
                        if (this.id == 'categoryAagan') {
                            $('.ishigherEdu').hide();
                            $('#hrEduYes').prop('checked', true);
                            toogleHigherEdu(1);
                        } else {
                            $('.ishigherEdu').show();
                        }
                        $('#uploadImage7').show(3000);
                        $('#btn_3').show();
                        $('#uploadproofdocs').val(0);
                        $('#AttachPhotoSign7').hide();
                        $('#uploadImage7').val('');
                        $("#uploadPreview7").attr('src', 'images/sampleproof.png');
                        $("#uploadPreview7").attr('style', 'display: block');
                    } else {
                        clearproofdoc(7);
                        if($('#uploadPreview5').attr('src')!='images/sampleproof.png' && $('#uploadPreview6').attr('src')!='images/sampleproof.png')
                            {
                              if($('#mstatus').val() == 'Divorcee' || $('#mstatus').val() == 'Widow' || $('#mstatus').val() == 'Saperated' || $('#ddlCategory').val() == '2' || $('#ddlCategory').val() == '3' || $('#physicallyChStYes').is(':checked')  || $('#categoryViolenceYes').is(':checked') ) { 
                                  //alert("it's checked"); 
                              }else{
                              $('#btn_3').hide();
                              }
                            }
                    }
                    break;
                case 'ddlidproof':
                	if (this.value != ''){
                		$('#idproof').html('(' + this.value + ')');
                		showproofdoc(6);
                	} else {
                		clearproofdoc(6);
                	}
                    
                    break;
            }
        });

        $('#ddlQualification').change(function () {
            $('#ddlBoard').val('');
        });
        $('#ddlidproof').change(function () {
            if (this.value != ''){
                $('#idproof').html('(' + this.value + ')');
                showproofdoc(6);
            } else {
                clearproofdoc(6);
            }
        });

        function FillIntake() {
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=FILLIntake&values=" + BatchCode + "",
                success: function (data) {
                    document.getElementById('txtintakeavailable').innerHTML = data;
                    if (document.getElementById('txtintakeavailable').innerHTML == '0') {
                        alert("Intake Not Available");
                        window.location.href = "frmmessage.php";
                    }
                }
            });
        }
//        FillIntake();

        $("#ddlTehsil").change(function () {
            var selTehsil = $(this).val();
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfWcdRscfaAdmission.php",
                data: "action=FILLCenterOasispref1&values=" + selTehsil + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlCenter").html(data);
                    

                }
            });
        });
		
		$("#ddlTehsil").change(function () {
            resetMainForm();
            var selTehsil = $(this).val();
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfWcdRscfaAdmission.php",
                data: "action=FILLCenterOasispref2&values=" + selTehsil + "",
                success: function (data) {
                    //alert(data);
                   
                    $("#ddlCenter2").html(data);

                }
            });
        });
		
//        FillCenter();

        $("#ddlCenter").change(function () {
            resetMainForm();
            fillCentersInfo();
        });
        
        $("#ddlCenter2").change(function () {
            resetMainForm();
            $("#btnShow").hide();
            // $("#btnSubmit").hide();
            fillCentersInfo();
        });

        function FillCourseName() {
            var CourseCode = '24';
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FillAdmissionCourse&values=" + CourseCode + "",
                success: function (data) {
                    document.getElementById('txtcoursename').innerHTML = data;
                }
            });
        }
        FillCourseName();


        function FillBatchCode() {
            var CourseCode = '24';
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLBatchCode&values=" + CourseCode + "",
                success: function (data) {
                    //alert(data);
                    txtbatchcode.value = data;
                    if (document.getElementById('txtbatchcode').value == '') {
                        alert("Batch Closed");
                        window.location.href = "frmbatchclosemsg.php";
                    }
                    else {
                        FillBatchName(txtbatchcode.value);
                        AdmissionFee(txtbatchcode.value);
                        InstallMode(txtbatchcode.value);
                    }

                }
            });
        }
        FillBatchCode();

        function FillBatchName(val) {
            // var CourseCode = '3';
            //var batchcode = $('#txtbatchcode').val();
            //var batchcode = document.getElementById('txtbatchcode').value;
            //alert(val);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FillAdmissionBatch&values=" + val + "",
                success: function (data) {
                    //alert(data);
                    document.getElementById('txtbatchname').innerHTML = data;
                }
            });
        }
		
		$("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLPanchayat&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlPanchayat').html(data);
                }
            });
        });
        
        $("#ddlPanchayat").change(function () {
            var selPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlGramPanchayat').html(data);
                }
            });
        });


        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                }
            });
        }
        GenerateUploadId();

        function GenerateLearnercode()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=GENERATELEARNERCODE",
                success: function (data) {
                    txtLearnercode.value = data;
                }
            });
        }
        GenerateLearnercode();
		
		function findselectedArea() {

            var UrbanDiv = document.getElementById("areaUrban");
            var category = document.getElementById("Urban");

            if (UrbanDiv) {
                Urban.style.display = areaUrban.checked ? "block" : "none";
                Rural.style.display = "none";
            }
            else
            {
                Urban.style.display = "none";
            }
        }
        function findselectedArea1() { 
            var RuralDiv = document.getElementById("areaRural");
            var category1 = document.getElementById("Rural");

            if (RuralDiv) {
                Rural.style.display = areaRural.checked ? "block" : "none";
                Urban.style.display = "none";
            }
            else
            {
                Rural.style.display = "none";
            }
        }
        
        $("#areaUrban").change(function () {
            $('#ddlGramPanchayat').val('');
            $('#ddlPanchayat').val('');
            $('#ddlVillage').val('');


            findselectedArea();
            FillMunicipalName();
            FillMunicipalType();
        });
        $("#areaRural").change(function () {
            $('#ddlMunicipalName').val('');
            $('#ddlMunicipalType').val('');
            $('#ddlWardno').val('');

            findselectedArea1();
            FillPanchayat();
        });

        $("#txtemail").blur(function () {
            validateEmail(this.value);
        });

        function validateEmail(email) {
            if (email=='') return true;
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var isvalid = regex.test(email);
            $('#emailerr').html('');
            if (!isvalid) {
                $('#emailerr').html('Please enter valid email id');
            }
            return isvalid;
        }

        function FillPanchayat() {
            var selDistrict = ddlDistrict.value;
            //alert(selDistrict);
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FILLPanchayat&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlPanchayat').html(data);
                }
            });
        }

        function FillMunicipalName() {
            var selDistrict = ddlDistrict.value;
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillMunicipalName&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlMunicipalName').html(data);
                }
            });
        }
        
        function FillMunicipalType() {
            var selDistrict = ddlDistrict.value;
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalType",
                success: function (data) {
                    //alert(data);
                    $('#ddlMunicipalType').html(data);
                }
            });
        }
        
         $("#ddlMunicipalName").change(function () {
            var selMunicipalName = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FILLWardno&values=" + selMunicipalName + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlWardno').html(data);
                }
            });
        });
        
        
        $("#ddlPanchayat").change(function () {
            var selPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlGramPanchayat').html(data);
                }
            });
        });
        
        $("#ddlGramPanchayat").change(function () {
            var selGramPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfVillageMaster.php',
                type: "post",
                data: "action=FILL&values=" + selGramPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlVillage').html(data);
                }
            });
        });

        function AdmissionFee(val)
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=Fee&codes=" + val + "",
                success: function (data) {
                    // alert(data);
                    txtCourseFee.value = data;
                }
            });
        }


        function InstallMode(val)
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=Install&values=" + val + "",
                success: function (data) {
                    //alert(data);
                    txtInstallMode.value = data;
                }
            });
        }


//        $("#txtGPFno").prop("disabled", true);

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            //alert(1);
            fillForm();
        }

        $("input[type='image']").click(function () {
            $("input[id='my_file']").click();
        });

        function FillDistrict() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        }
        FillDistrict();
		
		function FillTSPDistrict() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfWcdRscfaAdmission.php",
                data: "action=FILLTSPDistrict",
                success: function (data) {
                    $("#ddlTspDistrict").html(data);
                }
            });
        }
        FillTSPDistrict();
		
		$("#ddlTspDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfWcdRscfaAdmission.php',
                type: "post",
                data: "action=FILLTSPTehsil&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTspTehsil').html(data);
                }
            });
        });


        function FillQualification() {
            var isGraduate = $('#categoryAagan').is(':checked');
            $.ajax({
                type: "post",
                url: "common/cfWcdRscfaAdmission.php",
                data: "action=FILLHigher",
                success: function (data) {
                    $("#ddlQualification").html(data);
                }
            });
        }
        FillQualification();

        function FillLearnerType() {
            $.ajax({
                type: "post",
                url: "common/cfCategoryMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCategory").html(data);
                }
            });
        }
        FillLearnerType();
		
		function findtsp() {

            var Tsp = document.getElementById("tspvalue");
				//alert("hii");
            var category = document.getElementById("subtspcategory");
            if (Tsp) {
                subtspcategory.style.display = "block";
				subnontspcategory.style.display = "none";
            }
            else
            {
                //subnontspcategory.style.display = "block";
				subtspcategory.style.display = "none";
            }
        }
		
		function findnontsp() {

            var NonTsp = document.getElementById("nontsp");
				//alert("hii/hello");
            var category = document.getElementById("subnontspcategory");
            if (NonTsp) {
                subtspcategory.style.display = "none";
				subnontspcategory.style.display = "block";
            }
            else
            {
                subnontspcategory.style.display = "none";
				//subtspcategory.style.display = "none";
            }
        }
		
		$("#ddlCategory").change(function () {
			 var selcategory = $(this).val();
			   if(selcategory == '3') {
				   $("#tsp").show();
			   }
			   else {
				   $("#tsp").hide();
				   $("#subtspcategory").hide();
				   $("#subnontspcategory").hide();
			   }
		});
		
			$("#ddlCategory").change(function () {
			 var selcategories = $(this).val();
			   if(selcategories == '1') {
				   $("#caste").hide();
			   }
			   else {
				   $("#caste").show();
				}
		});
		
			$("#tspvalue").change(function () {
				findtsp();
			});
			
			$("#nontsp").change(function () {
				findnontsp();
			});
		
        function FillReligion() {
            $.ajax({
                type: "post",
                url: "common/cfReligionMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlReligion").html(data);
                }
            });
        }
        FillReligion();

        function FillBoard() {
            $.ajax({
                type: "post",
                url: "common/cfBoardMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //$("#ddlBoard").html(data);
					$("#ddlMinBoard").html(data);
					$("#ddlHSCBoard").html(data);
                }
            });
        }
        FillBoard();
		
		function FillHighestBoard() {
            $.ajax({
                type: "post",
                url: "common/cfBoardMaster.php",
                data: "action=FILLHighBoard",
                success: function (data) {
                    $("#ddlBoard").html(data);
					//$("#ddlMinBoard").html(data);
                }
            });
        }
        FillHighestBoard();

        function FillDivision() {
            $.ajax({
                type: "post",
                url: "common/cfDivisionMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDivision").html(data);
					$("#ddlMinDivision").html(data);
					$("#ddlHSCDivision").html(data);
                }
            });
        }
        FillDivision();

        function FillMonth() {
            var year = $("#txtYear").val();
            $.ajax({
                type: "post",
                url: "common/cfMonthMaster.php",
                data: "action=FILLByYear&year=" + year,
                success: function (data) {
                    $("#ddlMonth").html(data);
                }
            });
        }
        FillMonth();

        function FillMinMonth() {
            var year = $("#txtMinYear").val();
            $.ajax({
                type: "post",
                url: "common/cfMonthMaster.php",
                data: "action=FILLByYear&year=" + year,
                success: function (data) {
                    $("#ddlMinMonth").html(data);
                    $("#ddlHSCMonth").html(data);
                }
            });
        }
        FillMinMonth();

        function findselected() {
//            var gender = document.getElementByName('gender');
//            var subcategory = document.getElementByName('subcategory');
//            if (gender.value == 'Female')
//                subcategory.show();
//            else
//                subcategory.hide();
            var Female = document.getElementById("genderFemale");
            var category = document.getElementById("subcategory");
            if (Female) {
                subcategory.style.display = genderFemale.checked ? "block" : "none";
            }
            else
            {
                subcategory.style.display = "none";
            }
        }

        $("#txtYear").change(function () {
            FillMonth();
        });

        $("#txtMinYear").change(function () {
            FillMinMonth();
        });

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            resetMainForm();
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILLNew&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
            fillLegislativeAssembly(selDistrict);
        });

        function resetMainForm() {
            //resetForm('frmoasisadmission');
            $("#main-content").hide();
            $("#btnShow").hide();
            $('#CenterDetail').hide();
        }

        function fillLegislativeAssembly(selDistrict) {
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILLAssembly&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('.legislative_assembly').html(data);
                }
            });
        }

        $("#genderFemale").change(function () {
            findselected();
        });
        $("#genderMale").change(function () {
            findselected();
        });

        // statement block start for delete record function
        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=DELETE&values=" + AdmissionCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmLearnerProfile.php";
                        }, 1000);
                        Mode = "Add";
                        resetForm("frmLearnerProfile");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }
        // statement block end for delete function statment

        function fillForm()
        {

            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=EDIT&values=" + AdmissionCode + "",
                success: function (data) {

                    data = $.parseJSON(data);
                    txtlname.value = data[0].lname;
                    txtfname.value = data[0].fname;
                    dob.value = data[0].dob;
                    txtAdmissionCode.value = data[0].AdmissionCode;
                    txtphoto.value = data[0].photo;
                    txtsign.value = data[0].sign;
                    $("#uploadPreview1").attr('src', "upload/admission_photo/" + data[0].photo);
                    $("#uploadPreview2").attr('src', "upload/admission_sign/" + data[0].sign);
                    $("#uploadPreview3").attr('src', "upload/admission_certificate/" + data[0].certificate);
                    $("#uploadPreview20").attr('src', "upload/admission_certificate/" + data[0].certificate);
                    $("#uploadPreview4").attr('src', "upload/admission_proof/" + data[0].proof);
                    //p1.value = data[0].photo;
                    //p2.value = data[0].sign; 
                }
            });
        }

        $("#btnShow").click(function () {
            fillCentersInfo();
        });

        function fillCentersInfo() {
            var a = $('#ddlCenter').val();
            var b = $('#ddlCenter2').val();
            var batch = $('#txtbatchcode').val();

            if (a == "0" || b == "0")
            {
                if (a == "0") setCenter1Info('', '', '',''); else setCenter2Info('', '', '','');
                alert("Please Select Both Center Preferences");
            }
            else if (a == b) {
                alert("Please Select Two Different Preferences");
                $('#ddlCenter2').val('0');
            } 
            else {
                $('#response').empty();
                $('#response').append("<p class='alert-error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfWcdRscfaAdmission.php",
                    data: "action=CENTERDETAILS&values=" + ddlCenter.value + "&batchcode=" + batch,
                    success: function (data)
                    {
                        //alert(data);
                        $('#response').empty();
                        if (data == "") {
                            $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select a RSP for Selection" + "</span></p>");
                        }
                        else {
                            data = $.parseJSON(data);
                            setCenter1Info(data[0].orgname, data[0].regno, data[0].itgk_address, data[0].applications);
                            showcenterdetail2(batch);                           
                        }
                    }
                });
            }
        }

        function setCenter1Info(orgname, regno, address, applications) {
            txtName1.value = orgname;
            txtMobile.value = regno;
            txtITGKAddress.value = address;
            $('#applicationsC1').html(applications);
        }

        function setCenter2Info(orgname, regno, address, applications) {
            txtName2.value = orgname;
            txtMobile2.value = regno;
            txtITGKAddress2.value = address;
            $('#applicationsC2').html(applications);
        }
		
		function showcenterdetail2(batch)
        {

            $.ajax({
                type: "post",
                url: "common/cfWcdRscfaAdmission.php",
                data: "action=CENTERDETAILS&values=" + ddlCenter2.value + "&batchcode=" + batch,
                success: function (data) {

                    data = $.parseJSON(data);
                    setCenter2Info(data[0].orgname, data[0].regno, data[0].itgk_address, data[0].applications);
					$("#main-content").show(1000);
					$("#btnShow").hide(1000);
					$('#CenterDetail').show(1000);
                }
            });
        }

        $("#btnUploadProof").click(function () {
            uploadFiles('proof_documents');
        });

        function uploadFiles(sid) {

//            var form_data = new FormData($(this).parents('#proof_documents')[0]);
            return true;
            var file_data = $('#' + sid).prop('files')[0];
            alert(file_data);
            
            var form_data = new FormData();
            form_data.append('file', file_data);
            
            $('#msgproofdocs').empty();
            $('#msgproofdocs').append("<p class='alert-error'><span><i class='fa-check-square'></i></span><span>uploading please wait.....</span></p>");
            
            $.ajax({
                url: 'common/cfWcdRscfaAdmission.php', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(script_response) {
                    alert(script_response);
                    $('#response').empty();
                    $('#response').append("<p class='msgproofdocs'><span><i class='fa-check-square-o'></i></span><span>" + script_response + "</span></p>");
                }
            });
        }

        function checkuploadedDocs() {
            var upload = 0;
            if ($('#uploadpics').val() == 1 && $('#uploadedudocs').val() == 1 && $('#uploadproofdocs').val() == 1) {
                upload = 1;
            }

            return upload;
        }

        function checkValidAgeName() {
            var dob = $('#dob').val();
            var formdata = $("#frmoasisadmission").serialize();
            var data = "action=ValidateAgeName&dob=" + dob + "&lname=" + txtlname.value + "&parent=" + txtfname.value + "&aadhaar=" + txtidno.value + "&batch=" + txtbatchcode.value + "&" + formdata;
            var url = "common/cfWcdRscfaAdmission.php";
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    $('#response').empty();
                    if (data == 1) {
                        var isuploaded = checkuploadedDocs();
                        if (isuploaded) {
                            // submitDetails();
                              showpreviewmodal();
                        } else {
                            alert('Please try to upload your documents again.');
                        }
                    } else if (data == 'ageInvalid') {
                        var currentTime = new Date();
                        var msg = 'RSCIT women scheme is only applicable for applicants having age between 16 to 40 years.';
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + msg + "</span></div>");
                        alert(msg);
                    } else if (data == 'accInvalid') {
                        var msg = "Dear applicant you are not eligible for this scheme, \n as you have already taken benefit under this scheme previously.";
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + msg + "</span></div>");
                        alert(msg);
                        window.location = 'frmlearnerotpverify.php';
                    } else if (data == 'accPending') {
                        var msg = 'Dear applicant you have already applied in this scheme.';
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + msg + "</span></div>");
                        alert(msg);
                        window.location = 'frmlearnerotpverify.php';
                    } else if (data == 'aadhaarExists') {
                        var msg = 'Dear applicant your Aadhaar Card number already registred.';
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + msg + "</span></div>");
                        alert(msg);
                        window.location = 'frmlearnerotpverify.php';
                    } else {
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                        alert(data);
                    }
                }
            });

            return false;
        }

        $("#btnCancel").click(function () {
            window.location = 'frmlearnerotpverify.php';
        });
        $("#btnSubmit").click(function () {
            submitDetails();
        });
        $("#btnValidate").click(function () {
            validateform();
        });

        function validateform() {
            $('#response').empty();
            $('#response').append("<div class='alert-error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Validating...</span></div>");
            var ifvalid = $('#frmoasisadmission').valid();
            var isvalidemail = validateEmail($('#txtemail').val());
            if(ifvalid && isvalidemail) {
               checkValidAgeName();
            } else {
                $('#response').empty();
                $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please complete all required fields.</span></div>");
            }

            return ifvalid;
        }

        function submitDetails() {
            var ok = confirm('Are you sure, to submit your application details ?');
            if (ok) {
                savedetails();
            }
        }

        function savedetails() {
            var data;
            var url = "common/cfWcdRscfaAdmission.php";
            if (Mode == 'Add') {
                var formdata = $("#frmoasisadmission").serialize();
                data = "action=ADD&" + formdata; // serializes the form's elements.
            } else {
                var lphoto = $('#txtLearnercode').val();
                var lsign = $('#txtLearnercode').val();
                var dob = $('#dob').val();
                data = "action=UPDATE&lname=" + txtlname.value + "&parent=" + txtfname.value + "&dob=" + dob + "&lphoto=" + lphoto + "&lsign=" + lsign + "&Code= " + txtAdmissionCode.value + ""; // serializes the form's elements.
            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                { 
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate) {
                        $('#response').empty();
                        $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                        window.setTimeout(function () {
                            Mode = "Add";
                            window.location.href = 'frmwcdrscfaadmission.php';
                        }, 3000);
                    } else {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                        resetToUpload();
                    }
                }
            });
        }

        $('.btn-primary').hover(
          function() {
            $( this ).addClass( "text-uppercase" );
          }, function() {
            $( this ).removeClass( "text-uppercase" );
          }
        );

        function resetToUpload() {
            var fid = '';
            $('#AttachPhotoSign').hide();
            $('#AttachPhotoSign2').hide();
            $('#AttachPhotoSign3').hide();
            $('#btn_1').show();
            $('#btn_2').show();
            $('#btn_3').show();
            $('#uploadpics').val('0');
            $('#uploadedudocs').val('0');
            $('#uploadproofdocs').val('0');
            for (var i = 1; i <= 21; i++) {
                fid = '#uploadImage' + i;
                $(fid).show();
                $(fid).val('');
                document.getElementById("uploadPreview" + i).src = 'images/samplephoto.png';
            }
        }

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        // statement block end for submit click
    });

</script>

<!--<script src="scripts/signupload.js"></script>-->
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmoasisadmission_validation.js" type="text/javascript"></script>	
</html>