<?php
$title = "ITGK Ranking Management System";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll']=='4' || $_SESSION['User_UserRoll']=='8'
|| $_SESSION['User_UserRoll']=='9' || $_SESSION['User_UserRoll']=='2' || $_SESSION['User_UserRoll']=='7' ){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}


echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
?>
<link rel="stylesheet" href="css/datepicker.css">
<!--<link rel="stylesheet" href="css/starrr.css">-->
<script src="scripts/datepicker.js"></script>

<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jstars.js"></script>


<style>

.ratingblog{ width: 100%; float: left; box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2); margin: 0px 0px 20px 0px;}
.ratingjan{ background-color: #009656; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingjan h3{ margin: 10px 0 5px;}
.ratingfeb{ background-color: #dd4a38; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingmarch{ background-color: #41658b; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingapril{ background-color: #f4960f; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingboxbg{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbg p{color: #009656; margin: 0px;}
    
.ratingboxbgfeb{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgfeb p{color: #dd4a38; margin: 0px;} 
.ratingfeb h3{ margin: 10px 0 5px;}
    
.ratingboxbgmarch{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgmarch p{color: #41658b; margin: 0px;}
.ratingmarch h3{ margin: 10px 0 5px;}
    
.ratingboxbgapril{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgapril p{color: #f4960f; margin: 0px;}
.ratingapril h3{ margin: 10px 0 5px;}
    
.ratingboxcont{ font-size: 30px; color: #009656; line-height: 30px; font-weight: bold; margin-bottom: 5px;}
    
.ratingmay{ background-color: #35a79c; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingjune{ background-color: #f37736; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingjuly{ background-color: #88d8b0; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingaugu{ background-color: #7bc043; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingsept{ background-color: #0392cf; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingoct{ background-color: #536872; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingnove{ background-color: #adcbe3; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}
.ratingdece{ background-color: #2a4d69; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}

    

.ratingboxcontfeb{ font-size: 30px; color: #dd4a38; line-height:30px; font-weight: bold; margin-bottom: 5px;}
.ratingboxcontmarch{ font-size: 30px; color: #41658b; line-height: 30px; font-weight: bold; margin-bottom: 5px;}
.ratingboxcontapril{ font-size: 30px; color: #f4960f; line-height: 30px; font-weight: bold; margin-bottom: 5px;}

.ratingboxcontmay{ font-size: 30px; color: #35a79c; line-height:30px; font-weight: bold; margin-bottom: 5px;}
.ratingboxcontjune{ font-size: 30px; color: #f37736; line-height: 30px; font-weight: bold; margin-bottom: 5px;}
.ratingboxcontjuly{ font-size: 30px; color: #88d8b0; line-height: 30px; font-weight: bold; margin-bottom: 5px;}
.ratingboxcontaugu{ font-size: 30px; color: #7bc043; line-height:30px; font-weight: bold; margin-bottom: 5px;}
.ratingboxcontsept{ font-size: 30px; color: #0392cf; line-height: 30px; font-weight: bold; margin-bottom: 5px;}
.ratingboxcontoct{ font-size: 30px; color: #536872; line-height: 30px; font-weight: bold; margin-bottom: 5px;}
.ratingboxcontnove{ font-size: 30px; color: #adcbe3; line-height:30px; font-weight: bold; margin-bottom: 5px;}
.ratingboxcontdece{ font-size: 30px; color: #2a4d69; line-height: 30px; font-weight: bold; margin-bottom: 5px;}

 
.ratingboxbgmay{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgmay p{color: #35a79c; margin: 0px;}
.ratingboxbgmay h3{ margin: 10px 0 5px;}

.ratingboxbgjune{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgjune p{color: #f37736; margin: 0px;}
.ratingboxbgjune h3{ margin: 10px 0 5px;}
    
.ratingboxbgjuly{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgjuly p{color: #88d8b0; margin: 0px;}
.ratingboxbgjuly h3{ margin: 10px 0 5px;}
    
.ratingboxbgaugu{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgaugu p{color: #7bc043; margin: 0px;}
.ratingboxbgaugu h3{ margin: 10px 0 5px;}
    
.ratingboxbgsept{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgsept p{color: #0392cf; margin: 0px;}
.ratingboxbgsept h3{ margin: 10px 0 5px;}
    
.ratingboxbgoct{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgoct p{color: #536872; margin: 0px;}
.ratingboxbgoct h3{ margin: 10px 0 5px;}
    
.ratingboxbgnove{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgnove p{color: #adcbe3; margin: 0px;}
.ratingboxbgnove h3{ margin: 10px 0 5px;}
    
.ratingboxbgdece{ background-color: #fff; color: #000; font-size: 20px; text-align: center; padding: 10px;}
.ratingboxbgdece p{color: #2a4d69; margin: 0px;}
.ratingboxbgdece h3{ margin: 10px 0 5px;}
    
    
.viewall { background-color: #2daae1; color: #fff; text-align: center; text-transform: uppercase; padding: 5px;}

.chromeframe {
    margin: 0.2em 0;
    background: #ccc;
    color: #000;
    padding: 0.2em 0;
}


#loader-wrapper {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1000;
    background: rgba(0, 0, 0, 0.4);
}
#loader {
    display: block;
    position: relative;
    left: 50%;
    top: 50%;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border-radius: 50%;
    border: 3px solid transparent;
    border-top-color: #3498db;

    -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
    animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */

    z-index: 1001;
}

#loader:before {
    content: "";
    position: absolute;
    top: 5px;
    left: 5px;
    right: 5px;
    bottom: 5px;
    border-radius: 50%;
    border: 3px solid transparent;
    border-top-color: #e74c3c;

    -webkit-animation: spin 3s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
    animation: spin 3s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
}

#loader:after {
    content: "";
    position: absolute;
    top: 15px;
    left: 15px;
    right: 15px;
    bottom: 15px;
    border-radius: 50%;
    border: 3px solid transparent;
    border-top-color: #f9c922;

    -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
    animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
}

@-webkit-keyframes spin {
    0%   {
        -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(0deg);  /* IE 9 */
        transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
    }
    100% {
        -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(360deg);  /* IE 9 */
        transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
    }
}
@keyframes spin {
    0%   {
        -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(0deg);  /* IE 9 */
        transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
    }
    100% {
        -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(360deg);  /* IE 9 */
        transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
    }
}

#loader-wrapper .loader-section {
    position: fixed;
    top: 0;
    width: 51%;
    height: 100%;
    background: #222222;
    z-index: 1000;
}

#loader-wrapper .loader-section.section-left {
    left: 0;
}

#loader-wrapper .loader-section.section-right {
    right: 0;
}

.loaded #loader {
    opacity: 0;
    -webkit-transition: all 1s ease-out;
    transition: all 1s ease-out;
}
.loaded #loader-wrapper {
    opacity: 0;
    visibility: hidden;
    -webkit-transition: all 2s;
    transition: all 2s;
}

/* JavaScript Turned Off */
.no-js #loader-wrapper {
    display: none;
}
.no-js h1 {
    color: #222222;
}





/* ==========================================================================
   Helper classes
   ========================================================================== */

/*
 * Image replacement
 */

.ir {
    background-color: transparent;
    border: 0;
    overflow: hidden;
    /* IE 6/7 fallback */
    *text-indent: -9999px;
}

.ir:before {
    content: "";
    display: block;
    width: 0;
    height: 150%;
}

/*
 * Hide from both screenreaders and browsers: h5bp.com/u
 */

.hidden {
    display: none !important;
    visibility: hidden;
}

/*
 * Hide only visually, but have it available for screenreaders: h5bp.com/v
 */

.visuallyhidden {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
}

/*
 * Extends the .visuallyhidden class to allow the element to be focusable
 * when navigated to via the keyboard: h5bp.com/p
 */

.visuallyhidden.focusable:active,
.visuallyhidden.focusable:focus {
    clip: auto;
    height: auto;
    margin: 0;
    overflow: visible;
    position: static;
    width: auto;
}

/*
 * Hide visually and from screenreaders, but maintain layout
 */

.invisible {
    visibility: hidden;
}

/*
 * Clearfix: contain floats
 *
 * For modern browsers
 * 1. The space content is one way to avoid an Opera bug when the
 *    `contenteditable` attribute is included anywhere else in the document.
 *    Otherwise it causes space to appear at the top and bottom of elements
 *    that receive the `clearfix` class.
 * 2. The use of `table` rather than `block` is only necessary if using
 *    `:before` to contain the top-margins of child elements.
 */

.clearfix:before,
.clearfix:after {
    content: " "; /* 1 */
    display: table; /* 2 */
}

.clearfix:after {
    clear: both;
}

/*
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */

.clearfix {
    *zoom: 1;
}

/* ==========================================================================
   EXAMPLE Media Queries for Responsive Design.
   These examples override the primary ('mobile first') styles.
   Modify as content requires.
   ========================================================================== */

@media only screen and (min-width: 35em) {
    /* Style adjustments for viewports that meet the condition */
}

@media print,
(-o-min-device-pixel-ratio: 5/4),
(-webkit-min-device-pixel-ratio: 1.25),
(min-resolution: 120dpi) {
    /* Style adjustments for high resolution devices */
}

/* ==========================================================================
   Print styles.
   Inlined to avoid required HTTP connection: h5bp.com/r
   ========================================================================== */

@media print {
    * {
        background: transparent !important;
        color: #000 !important; /* Black prints faster: h5bp.com/s */
        box-shadow: none !important;
        text-shadow: none !important;
    }

    a,
    a:visited {
        text-decoration: underline;
    }

    a[href]:after {
        content: " (" attr(href) ")";
    }

    abbr[title]:after {
        content: " (" attr(title) ")";
    }

    /*
     * Don't show links for images, or javascript/internal links
     */

    .ir a:after,
    a[href^="javascript:"]:after,
    a[href^="#"]:after {
        content: "";
    }

    pre,
    blockquote {
        border: 1px solid #999;
        page-break-inside: avoid;
    }

    thead {
        display: table-header-group; /* h5bp.com/t */
    }

    tr,
    img {
        page-break-inside: avoid;
    }

    img {
        max-width: 100% !important;
    }

    @page {
        margin: 0.5cm;
    }

    p,
    h2,
    h3 {
        orphans: 3;
        widows: 3;
    }

    h2,
    h3 {
        page-break-after: avoid;
    }
}

.ajax-load {
    padding: 10px 0;
    width: 100%;
}

.table-scrollable {
    width: 100%;
    overflow-x: auto;
    overflow-y: hidden;
    margin: 10px 0 !important;
}

.profile-user-img {
    margin: 0 auto;
    width: 150px;
    padding: 3px;
    border: 3px solid #d2d6de;
}

</style>

	
    <style>

        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;

        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
        }

        #checkboxes label {
            display: block;
        }

        #checkboxes label:hover {
            background-color: #1e90ff;
        }
    </style>
	
	
	
<div style="min-height:430px !important;max-height:auto !important;"> 

  <div class="container">     
    <div class="panel panel-primary" style="margin-top:20px !important;">  
        <div class="panel-heading">District Wise IT-GK Ranking Report</div>
        <div class="panel-body">

            <form name="frmsprating" id="frmsprating" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>
                    <div class="container">
					
							
					</div>
					<?php if($_SESSION['User_UserRoll']=='1' || $_SESSION['User_UserRoll']=='4' ||
						$_SESSION['User_UserRoll']=='8' || $_SESSION['User_UserRoll']=='9' || $_SESSION['User_UserRoll']=='2')
						{ ?> 
							<div class="col-sm-4 form-group"> 
								<label for="designation">District:<span class="star">*</span></label>
								<select id="ddlDistrict" name="ddlDistrict" class="form-control">                                  
								</select>
							</div> 
						</div>
						
						<div class="container">
							
							<div class='content' id="rating" style="width: 94%;"></div>
						</div>
							<?php } ?>
                                     
                  </form>
          </div>
      </div>
  </div>
   
</div>

<!-- Modal -->
<div id="myModalimage" class="modal" style="padding-top:50px !important">
  <div class="modal-content" style="width: 90%;">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h6>Month Wise Rating Star</h6>
    </div>
      <div class="modal-body" style="max-height: 400px; overflow-y: scroll; text-align: center;">
        <div id="grid" ></div>
		 
    </div>
  </div>
</div>

<div id="myModalimages" class="modal" style="padding-top:50px !important">
  <div class="modal-content" style="width: 90%;">
    <div class="modal-header">
      <span class="close closes">&times;</span>
      <h6>Rating Parameters</h6>
    </div>
      <div class="modal-body" style="max-height: 400px; overflow-y: scroll; text-align: center;">
        <div id="gird" ></div>
		 
    </div>
  </div>
</div>

<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {		
		function FillDistrict() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        }
        
		$("#ddlDistrict").change(function () {
			var selDistrict = $(this).val();
			showDistrictWiseData(selDistrict);
		});
		
		$("#rating").on("click",".getdistrictwise",function(){
			var val=$(this).attr("id");
			$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
				var url = "common/cfItgkRating.php"; // the script where you handle the form input.           
				var data;            
				data = "action=GetDistrictmonthwiseRating&itgkcode=" + val + ""; //

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {					
                    $('div[id=loader-wrapper]').remove();
					$("#grid").html(data);
					
					var modal = document.getElementById('myModalimage');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
					}
                }
            });
				
		});
		
		$("#grid").on("click",".params",function(){
			var val=$(this).attr("id");
			var itgk=$(this).attr("name");
			var month='';
			//var itgkcode='';
				if(val=='mnth0'){
					month='January 2019'
				}
				else if(val=='mnth1'){
					month='February 2019'
				}
				else if(val=='mnth2'){
					month='March 2019'
				}
				else if(val=='mnth3'){
					month='April 2019'
				}
				else if(val=='mnth4'){
					month='May 2019'
				}
				else if(val=='mnth5'){
					month='June 2019'
				}
				else if(val=='mnth6'){
					month='July 2019'
				}
				else if(val=='mnth7'){
					month='August 2019'
				}
				else if(val=='mnth8'){
					month='September 2019'
				}
				else if(val=='mnth9'){
					month='October 2019'
				}
				else if(val=='mnth10'){
					month='November 2019'
				}
				else if(val=='mnth11'){
					month='December 2019'
				}
			$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
				var url = "common/cfItgkRating.php"; // the script where you handle the form input.           
				var data;            
				data = "action=GetDistrictItgkRatingparameters&itgkcode=" + itgk + "&mnth=" + month + ""; //
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    $('div[id=loader-wrapper]').remove();
					$("#gird").html(data);
					$('#exampledist').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
					var modal = document.getElementById('myModalimages');
					var span = document.getElementsByClassName("closes")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
					}
                }
            });
				
		});
		
		
        function showDistrictWiseData(districtcode) {
			$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
            var url = "common/cfItgkRating.php"; // the script where you handle the form input.           
            var data;            
            data = "action=GetDistrictWiseData&districtcode=" + districtcode + ""; //
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					$('div[id=loader-wrapper]').remove();
					$("#rating").html(data);
					$('#Allexample').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}					
            });			
        }
	
		
		if(User_UserRole == 7) {					
			showITGKWiseData();
		}
		else {					
					FillDistrict();
					//showDistrictWiseData();
				} 
     
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>


<style>
.error {
	color: #D95C5C!important
}
</style>
</html>
