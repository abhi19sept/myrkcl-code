<?php
$title = "Learner Correction Before Final Exam History";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['lcode'])) {
    echo "<script>var learnercode='" . $_REQUEST['lcode'] . "'</script>";
    echo "<script>var itgkcode=" . $_REQUEST['itgkcode'] . "</script>";
} else {
    echo "<script>var learnercode=0</script>";
     echo "<script>var itgkcode=0</script>";
}
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Learner Correction Before Final Exam History</div>
            <div class="panel-body">

                <form name="frmcenterpreferencecount" id="frmcenterpreferencecount" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        showData();
        function showData() {
            //$("#btnSubmit").hide();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfModifyAdmForAadhar.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=AdmEditHistory&lcode=" + learnercode + "&itgkcode=" + itgkcode + ""; //

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }
        $("#grid").on("click",".view_photo",function(){
            
            var batchname = $(this).attr("batchname");
            var image = $(this).attr("image");
            var count = $(this).attr("count");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfModifyAdmForAadhar.php",
                data: "action=ViewPhoto&batchname=" + batchname + "&image=" + image + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    //$("#"+count+"_pic").show();
                    $("#"+count+"_photo").hide();
                    $("#Viewphoto_"+count).html(data);
                    $("#Viewphoto_"+count).show();

                }
            });
        });
        $("#grid").on("click",".view_photoafter",function(){
            
            var batchname = $(this).attr("batchname");
            var image = $(this).attr("image");
            var count = $(this).attr("count");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfModifyAdmForAadhar.php",
                data: "action=ViewPhotoAfter&batchname=" + batchname + "&image=" + image + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    //$("#"+count+"_pic").show();
                    $("#"+count+"_photoafter").hide();
                    $("#Viewphotoafter_"+count).html(data);
                    $("#Viewphotoafter_"+count).show();

                }
            });
        });        
        $("#grid").on("click",".view_photonoedit",function(){
            
            var batchname = $(this).attr("batchname");
            var image = $(this).attr("image");
            var count = $(this).attr("count");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfModifyAdmForAadhar.php",
                data: "action=ViewPhotonoedit&batchname=" + batchname + "&image=" + image + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    //$("#"+count+"_pic").show();
                    $("#"+count+"_photonoedit").hide();
                    $("#Viewphotonoedit_"+count).html(data);
                    $("#Viewphotonoedit_"+count).show();

                }
            });
        })
        $("#btnSubmit").click(function () {
            showData();
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<!-- <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmphotosignprocesscount.js" type="text/javascript"></script> -->
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
