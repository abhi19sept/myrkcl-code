<?php
$title="Correction Add";
include ('header.php'); 
include ('root_menu.php');

$_SESSION['CorrectionPhoto']="";
$_SESSION['CorrectionMarksheet']="";
$_SESSION['CorrectionCertificate']="";
$_SESSION['CorrectionProvisional']="";

if (isset($_REQUEST['code'])) {
echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var Code=0</script>";
echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258') {
?>

<link rel="stylesheet" href="css/datepicker.css"/>
<script src="scripts/datepicker.js"></script>
 
<div style="min-height:430px !important;max-height:auto !important;">   
	<div class="container"> 			
		<div class="panel panel-primary" style="margin-top:36px !important;">  
			<div class="panel-heading">Correction Application  Form</div>
				<div class="panel-body">
					<!-- <div class="jumbotron"> --> 
					<form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">    
						<div class="container">
							<div class="container">
								<div id="response"></div>
							</div>        
								<div id="errorBox"></div>
								
							<div class="col-sm-4 form-group">     
							  <label for="learnercode">Learner Code:<span class="star">*</span></label>
							  <input type="text" class="form-control" maxlength="50" name="txtLCode" id="txtLCode"  onkeypress="javascript:return allownumbers(event);" placeholder="Learner Code">
							  <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
							</div>
							
							<div class="col-sm-4 form-group">     
							  <label for="centercode">CenterCode:<span class="star">*</span></label>
							  <input type="text" class="form-control" maxlength="50" name="txtCenterCode" id="txtCenterCode"  onkeypress="javascript:return allownumbers(event);" placeholder="CenterCode">
							</div>
	
							<div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                            </div>
						</div>						

				<div id="main-content" style="display:none;"> 
				  <div class="container">
						<div class="col-sm-4 form-group"> 
						  <label for="learnername">Learner Correct Name:<span class="star">*</span></label>
						  <input type="text" class="form-control" maxlength="50" name="txtLCorrectName" id="txtLCorrectName" onkeypress="javascript:return allowchar(event);" placeholder="Learner Correct Name">     
						</div>
						
					   <div class="col-sm-4 form-group">     
						  <label for="faname">Father Correct Name:<span class="star">*</span></label>
						  <input type="text" class="form-control" id="txtFCorrectName" name="txtFCorrectName" onkeypress="javascript:return allowchar(event);" placeholder="Father Correct Name">
					   </div>
						
					   <div class="col-sm-4 form-group"> 
						  <label for="application_for">Application For:<span class="star">*</span></label>
						  <select id="ddlApplication" name="ddlApplication" class="form-control" onchange="toggle_visibility1('provisional')">      
								
						  </select>    
					   </div> 
						
					   <div class="col-sm-4 form-group"> 
						  <label for="event_name">Exam Event Name:<span class="star">*</span></label>
						  <select id="ddlEventName" name="ddlEventName" class="form-control" >
							
						  </select> 
							<input type="hidden" class="form-control" name="txtEventName" id="txtEventName"/>
					   </div>
				  </div>
    
				  <div class="container">    
					   <div class="col-sm-4 form-group"> 
						  <label for="t_marks">Total Marks:<span class="star">*</span></label>
						  <input type="text" maxlength="3" class="form-control" name="txtMarks" onkeypress="javascript:return allownumbers(event);" id="txtMarks" placeholder="Total Marks">    
					   </div>
					 
						<div class="col-sm-4 form-group">     
						  <label for="email">Email id:<span class="star">*</span></label>
						  <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="Email id">
						</div>        
						
						<div class="col-sm-4 form-group">     
						  <label for="mobile_no">Mobile No:<span class="star">*</span></label>
						  <input type="text" class="form-control" maxlength="10" id="txtMobile" name="txtMobile" onkeypress="javascript:return allownumbers(event);" placeholder="Mobile No">
						</div>
						
						<div class="col-sm-4 form-group"> 
						  <label for="dob">Dob:<span class="star">*</span></label>
						  <input type="text" class="form-control" id="txtDob" name="txtDob" placeholder="D.O.B">
						</div>
				  </div> 
    
					<div class="container">   
						  <div class="col-sm-4 form-group" id="photodiv"> 
							  <label for="photo">Attach Photo:<span class="star">*</span></label>
							  <input type="file" class="form-control" id="filePhoto" name="filePhoto" onchange="checkPhoto(this)">
							  <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =5KB</span>
						  </div>   
						 
						  <div class="col-sm-4 form-group"> 
							  <label for="marksheet">Attach 10th Marksheet:<span class="star">*</span></label>
							  <input type="file" class="form-control" id="fileMarkSheet" name="fileMarkSheet" onchange="checkMarksheet(this)">
							  <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =200KB</span>
						  </div>
					   
						  <div class="col-sm-4 form-group"> 
							  <label for="rs_cit_certficate">Attach RS-CIT Certificate:<span class="star">*</span></label>
							  <input type="file" class="form-control" id="fileCertficate" name="fileCertficate" onchange="checkCertificate(this)">
							  <span style="font-size:10px;"> Note : JPG,JPEG Allowed Max Size =200KB</span>
						  </div>
					
						   <div class="col-sm-4 form-group" id="provisional" style="display:none;"> 
							  <label for="rs_cit_provisional">Attach RS-CIT Provisional:</label>
							  <input type="file" class="form-control" id="fileProvisional" name="fileProvisional" onchange="checkProvisional(this)">
							  <span style="font-size:10px;">Note : (In Duplicate Case)JPG,JPEG Allowed Max Size =200KB</span>
						   </div>
						</div>
						
						<div class="container">  
							<div class="col-md-4 form-group"> 							
								<label for="learnercode">Terms and conditions :<span class="star">*</span></label></br>                              
								<label class="checkbox-inline"> <input type="checkbox" name="chk" id="chk" value="1" >
									<a title="" style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><span id="fix">click here</span> </a>
								</label>								
                            </div>
						</div>	
						
						<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button class="close" type="button" data-dismiss="modal">×</button>
									<h3 class="modal-title">Terms and Conditions</h3>
								</div>
								<div class="modal-body">
									<img src="images/1.jpg" />
								</div>
								<div class="modal-footer">
									<button class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
						    </div>
						  </div>
						</div>
      
						<<div class="container">
							<input type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload"/>
							<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>
						</div>
				</form>
			</div>
          </div>   
        </div>
	</div> 
  </div>
</div>
</body>

<?php include'common/message.php';?>
<?php include ('footer.php'); ?>

<script src="scripts/correctionfileupload.js"></script>

<style>
#errorBox{
 color:#F00;
 }
</style>

<style>
  .modal-dialog {width:700px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
<script type="text/javascript">  
  $(document).ready(function() {
		jQuery(".fix").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>

<script type="text/javascript"> 
 $('#txtDob').datepicker({                   
	format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
});  
</script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>

<script type="text/javascript">
    function toggle_visibility1(id) {
        var e = document.getElementById(id);
		   //alert(e);
        var f = document.getElementById('ddlApplication').value;
        //alert(f);
        if (f == "2") {
            e.style.display = 'block';
        }
		else {
            e.style.display = 'none';
        }
    }
</script>

<script language="javascript" type="text/javascript">
function checkPhoto(target) {
	var ext = $('#filePhoto').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("filePhoto").value = '';
			return false;
		}

    if(target.files[0].size > 5050) {			       
		alert("Image size should less or equal 5 KB");
		document.getElementById("filePhoto").value = '';
        return false;
    }
	else if(target.files[0].size < 3000) {
				alert("Image size should be greater than 3 KB");
				document.getElementById("filePhoto").value = '';
				return false;
			}
    document.getElementById("filePhoto").innerHTML = "";
    return true;
}
</script>


<script language="javascript" type="text/javascript">
function checkMarksheet(target) {
	var ext = $('#fileMarkSheet').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileMarkSheet").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			        
		alert("Image size should less or equal 200 KB");
		document.getElementById("fileMarkSheet").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("fileMarkSheet").value = '';
				return false;
	}    
    document.getElementById("fileMarkSheet").innerHTML = "";
    return true;
}
</script>

<script language="javascript" type="text/javascript">
function checkCertificate(target) {	
	var ext = $('#fileCertficate').val().split('.').pop().toLowerCase();	   
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {			
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileCertficate").value = '';
			return false;
		}

		if(target.files[0].size > 200000) {	        
		alert("Image size should less or equal 200 KB");
		document.getElementById("fileCertficate").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("fileCertficate").value = '';
				return false;
	}     
    document.getElementById("fileCertficate").innerHTML = "";
    return true;
}
</script>

<script language="javascript" type="text/javascript">
function checkProvisional(target) {
	var ext = $('#fileProvisional').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileProvisional").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			       
		alert("Image size should less or equal 200 KB");
		document.getElementById("fileProvisional").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("fileProvisional").value = '';
				return false;
	}     
    document.getElementById("fileProvisional").innerHTML = "";
    return true;
}

</script>	

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

		function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();
			
		function FillApplicationFor() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionformoperator.php",
                data: "action=FillApplicationFor",
                success: function (data) {
                    $("#ddlApplication").html(data);
                }
            });
        }
        FillApplicationFor();
		
		
		function FillExamEvent() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionformoperator.php",
                data: "action=FillExamEvent",
                success: function (data) {
                    $("#ddlEventName").html(data);
                }
            });
        }
        FillExamEvent();
		
		$("#ddlEventName").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionformoperator.php",
                data: "action=FillEventById&values=" + ddlEventName.value + "",
                success: function (data) {
                    txtEventName.value = data;	
                }
            });
        });

			
        function FillEmpDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionformoperator.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlempdistrict").html(data);
                }
            });
        }
        //FillEmpDistrict();

        
        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionformoperator.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        //FillBankDistrict();


       if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=DELETE&values=" + StatusCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }
		
		$("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");		
					$.ajax({
						type: "post",
                        url: "common/cfcorrectionformoperator.php",
						data: "action=DETAILS&values=" + txtLCode.value + "&centercode=" + txtCenterCode.value + "",
						success: function (data)
						{
							alert(data);
							 $('#response').empty();
							 if(data == ""){
							 $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Learnercode and CenterCode" + "</span></p>");
							 }
							 else{
									data = $.parseJSON(data);
									txtLCorrectName.value = data[0].Admission_Name;
									txtFCorrectName.value = data[0].Admission_Fname;							
									$("#main-content").show();
									$('#txtLCode').attr('readonly', true);
									$('#txtCenterCode').attr('readonly', true);
									$("#btnShow").hide();
							 }
							
					    }
                });		
          });


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=EDIT&values=" + StatusCode + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    txtStatusName.value = data[0].StatusName;
                    txtStatusDescription.value = data[0].StatusDescription;
                    //alert($.parseJSON(data)[0]['StatusName']);
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);

                }
            });
        }

        //showData();

		$("#btnUpload").click(function () {
			$('#btnSubmit').show(3000);
		});
		
        $("#btnSubmit").click(function () {
			if ($("#form").valid())
           {	
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfcorrectionformoperator.php"; // the script where you handle the form input.
			var dob = $('#txtDob').val();
		    //var dddate = $('#txtDdDate').val();
            var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&lcode=" + txtLCode.value + "&centercode="+ txtCenterCode.value + "&lcorrectname=" + txtLCorrectName.value + "&faname="+ txtFCorrectName.value + 
					   "&application_for="+ ddlApplication.value + "&examname="+ txtEventName.value + "&totalmarks="+ txtMarks.value + 
					   "&email="+ txtEmail.value + "&mobileno="+ txtMobile.value + "&dob="+ dob + "&genratedid=" + txtGenerateId.value + ""; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + StatusCode + "&name=" + txtStatusName.value + "&description=" + txtStatusDescription.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmcorrectionoperator.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });
		   }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcorrection_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?> 