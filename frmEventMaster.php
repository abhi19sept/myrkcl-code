<?php
$title="Government Entry Form";
include ('header.php'); 
include ('root_menu.php'); 

  if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
 <div style="min-height:430px !important;max-height:1500px !important;">
        <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Create Event</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmEventMaster" id="frmEventMaster" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Event Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtEvent" id="txtEvent" placeholder="Event Name">
                            </div>


							
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">State Status:</label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control" >
								  
                                </select>    
                            </div>

                        </div>  

                       



                       

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
						
						
                </div>
				<div id="gird" style="margin-top:25px; margin-left:25px; width:94%;"> </div>
            </div>   
        </div>


    </form>

</div>


</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
            
           
            
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfEventMaster.php",
                    data: "action=DELETE&values=" + Code + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                            }, 3000);
                            Mode="Add";
                            resetForm("frmEventMaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfEventMaster.php",
                    data: "action=EDIT&values=" + Code + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Status']);
                        data = $.parseJSON(data);
                        txtEvent.value = data[0].Event_Name;
                        
                        ddlStatus.value = data[0].Event_Status;
                        
                    }
                });
            }

           function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfEventMaster.php",
                    data: "action=SHOW",
                    success: function (data) {
						//alert(data);
                        $("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                    }
                });
            }

            showData();


            $("#btnSubmit").click(function () {
				$("#frmEventMaster").valid();
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfEventMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtEvent.value  + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + Code + "&name=" + txtEvent.value  + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmEventMaster.php";
                           }, 1000);

                            Mode="Add";
                            resetForm("frmEventMaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmEventMasterValidation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>