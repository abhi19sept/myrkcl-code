<?php
$title="WCD Learner Details List";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
			echo "<script>var Batch='" . $_REQUEST['month'] . "'</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
			echo "<script>var Batch=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
 ?>

<div style="min-height:430px !important;max-height:auto !important;">
	<div class="container">			 
		<div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">WCD Learner Details</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data"> 
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
							  <label for="learnercode">Application Id:</label>
							  <input type="text" readonly="true" class="form-control" maxlength="50" name="txtLCode" id="txtLCode" placeholder="Application Id">
							  <input type="hidden" name="txtcategorycode" id="txtcategorycode"	class="form-control">
								<input type="hidden" name="txtapplicantid" id="txtapplicantid"	class="form-control" value="<?php echo $_REQUEST['code']; ?>">
								 <input type="hidden" name="txtitgk" id="txtitgk" class="form-control">
								 <input type="hidden" name="txtdistrict" id="txtdistrict" class="form-control">
								
							</div>
							 
							<div class="col-sm-4 form-group"> 
							  <label for="learnercode">Learner Name:</label>
                                <input type="text" readonly="true" class="form-control text-uppercase" name="txtlname" id="txtlname" placeholder="Learner Name">
							</div>
							
						   <div class="col-sm-4 form-group">     
							   <label for="fname">Father's Name:</label>
                               <input type="text" readonly="true" class="form-control text-uppercase" name="txtfname" id="txtfname" placeholder="Father's Name"> 
						   </div>
							
							<div class="col-sm-4 form-group">     
								<label for="dob">Date of Birth:</label>								
								<input type="text" class="form-control" readonly="true" name="dob" id="dob"  placeholder="YYYY-MM-DD">
							</div>
                         </div> 
						
						<div class="container">
							<div class="col-sm-4 form-group"> 
								<label for="email">Marital Status:</label>
								<input type="text" class="form-control" readonly="true" name="mstatus" id="mstatus"  placeholder="Marital Status">
							</div> 
						   
						   <div class="col-sm-4 form-group"> 
							  <label for="t_marks">Mobile No.:</label>
							  <input type="text" class="form-control" readonly="true" name="txtMobile" id="txtMobile" placeholder="Mobile No.">    
						   </div> 
						   
						   <div class="col-sm-4 form-group"> 
								<label for="category">Category:</label>
								<input type="text" class="form-control" readonly="true" name="txtCategory" id="txtCategory" placeholder="Category">
						   </div> 
						   
						   <div class="col-sm-4 form-group"> 
								<label for="caste">Caste:</label>
								<input type="text" placeholder="Caste" readonly="true" class="form-control" id="txtcaste" name="txtcaste"/>
							</div> 
														
						</div>
						
						<div class="container">
							<div class="col-sm-4 form-group"> 
								<label for="attempt">Address:</label>  
								<textarea class="form-control" rows="3" readonly="true" id="txtAddress" name="txtAddress" placeholder="Address"></textarea>
							</div>
						   
						   <div class="col-sm-4 form-group"> 
								<label for="branchname">Qualification:</label>
								<input type="text" placeholder="Qualification" readonly="true" class="form-control" id="txtQualification" name="txtQualification"/>
							</div>
								
							<div class="col-sm-4 form-group"> 
								<label for="branchname">Minimum Qualification Percentage:</label>
								<input type="text" placeholder="Minimum Qualification Percentage" readonly="true" class="form-control" id="txtminpercent" name="txtminpercent"/>
							</div>
							
							<div class="col-sm-4 form-group"> 
								<label for="branchname">Highest Qualification Percentage:</label>
								<input type="text" placeholder="Highest Qualification Percentage" readonly="true" class="form-control" id="txthighpercent" name="txthighpercent"/>
							</div>
						</div>
						
						<div class="container">
						<?php if($_REQUEST['Mode'] == 'Reported') { ?>	
							<div class="col-sm-4 form-group"> 
                                <label for="status">Status:<span class="star">*</span></label>
                                <select id="ddlstatus" name="ddlstatus" class="form-control" onchange="toggle_visibility1('remark')">
										<option value=""> Select Status </option>
										<option value="Not"> Not Reported </option>
                                </select>    
                            </div>	
							
						   <?php } ?>  					
						</div>
						
						 <br>
                        <div class="panel panel-info">
                            <div class="panel-heading">Photo Sign</div>
                            <div class="panel-body">
                                <div class="container">
                                  <div class="col-sm-2" > 
                                        <label for="photo">Photo:</label> </br> </br> 
                                        <img id="uploadPreview1" src="images/samplephoto.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px">
                                    </div>
                                    <div class="col-sm-1"> 

                                    </div>
                                    <div class="col-sm-2"> 
                                        <label for="photo">Signature:</label> </br> </br>
                                        <img id="uploadPreview2" src="images/samplesign.png" id="uploadPreview2" name="filePhoto" width="80px" height="107px">
									</div>  
                                    <div class="col-sm-1"> 

                                    </div>
                               
							   </div>
                                
                            </div>
                        </div>

                        <br>
						
						 <div class="panel panel-info">
                            <div class="panel-heading">Qualification Documents</div>
                            <div class="panel-body">
                                <div class="container">
									<div class="col-sm-2" > 
                                        <label for="photo">Minimum Education Certificate:</label> </br>
										<a title="Minimum Education Certificate" href="#" data-toggle="modal" data-target="#myModal">
                                      <img id="uploadPreview5" class="thumbnail img-responsive" src="images/samplecertificate.png" id="uploadPreview5"
											name="fileMinQualifyCertificate" width="150px" height="150px">
									  </a> </div>
                                    <div class="col-sm-1"> 

                                    </div>
									<div class="col-sm-2" > 
                                        <label for="photo">Highest Education Certificate:</label> </br>
										<a title="Highest Education Certificate" href="#" data-toggle="modal" data-target="#myModal">
                                        <img id="uploadPreview3" class="thumbnail img-responsive" src="images/samplecertificate.png" id="uploadPreview3"
												name="fileCertificate" width="150px" height="150px">
										</a>
									</div>
                                    <div class="col-sm-1"> 

                                    </div>
                                </div>
                              </div>
                        </div>
						
						<br>
						<div class="panel panel-info">
                            <div class="panel-heading">Proof Documents</div>
                            <div class="panel-body">
                                <div class="container">
                                   <div class="col-sm-2"> 
                                        <label for="photo">Age Proof Document:</label> </br>
										 <a title="Age Proof Document" href="#" data-toggle="modal" data-target="#myModal">
                                        <img id="uploadPreview4" class="thumbnail img-responsive" src="images/sampleproof.png" id="uploadPreview4" name="fileProof" width="150px" height="150px">
										</a>
									</div>									
									<div class="col-sm-1"> 

                                    </div>
									
									<div class="col-sm-2" id="caste"> 
                                        <label for="photo">Caste Proof Certificate:</label> </br>
										<a title="Caste Proof Certificate" href="#" data-toggle="modal" data-target="#myModal">
                                        <img id="uploadPreview6" class="thumbnail img-responsive" src="images/sampleproof.png" id="uploadPreview6" name="fileCasteCertificate" width="150px" height="150px">
										</a>
									</div>
                                    <div class="col-sm-1"> 

                                    </div>
								
								 </div>
                               </div>
                        </div>
					<?php if($_REQUEST['Mode'] == 'Reported') { ?>					
					  <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                      </div> 
					<?php } ?>  
					   	 <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
						  <div class="modal-content">
							<div class="modal-header">
								<button class="close" type="button" data-dismiss="modal">×</button>
								<h3 class="modal-title">Heading</h3>
							</div>
							<div class="modal-body">
								
							</div>
							<div class="modal-footer">
								<button class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						   </div>
						  </div>
						</div>
					
                </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>
<style>
  .modal-dialog {width:800px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
  	
  <script type="text/javascript">
  
  $(document).ready(function() {
		jQuery(".thumbnail").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>
  
  
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {        
		
		if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
		else if (Mode == 'Reported')
            {				
                fillForm();			
            }	
           
			else (Mode == 'Reject')
            {				
                fillForm();
				//fillOldLearnerDetails();
            }	
			
		function fillForm()
            {           //alert(Code);    
				$.ajax({
                    type: "post",
                    url: "common/cfWcdLearnerItgk.php",
                    data: "action=EDIT&batch=" + Batch + "&lcode=" + txtapplicantid.value + "",
                    success: function (data) {	
						//alert(data);
                        data = $.parseJSON(data);
						txtLCode.value = data[0].LearnerCode;
                        txtlname.value = data[0].lname;
                        txtfname.value = data[0].fname;
                        dob.value = data[0].dob;	
						mstatus.value = data[0].mstatus;
						txtdistrict.value = data[0].district;				
						txthighpercent.value = data[0].highper;				
						txtminpercent.value = data[0].minper;				
						
						txtitgk.value = data[0].itgk;
						txtMobile.value = data[0].mobile;
						txtCategory.value = data[0].categoryname;
						txtcategorycode.value = data[0].category;
						txtQualification.value = data[0].qualification;	
						txtcaste.value = data[0].caste;
						txtAddress.value = data[0].address;
						$("#uploadPreview1").attr('src',"upload/oasis_photo/" + data[0].photo);
						$("#uploadPreview2").attr('src',"upload/oasis_sign/" + data[0].sign);
						$("#uploadPreview3").attr('src',"upload/oasis_highest_certificate/" + data[0].highcert);
						
						$("#uploadPreview4").attr('src',"upload/oasis_age_proof/" + data[0].ageproof);
						$("#uploadPreview5").attr('src',"upload/oasis_minimum_certificate/" + data[0].mincert);						
						
						  var txtcat = document.getElementById("txtcategorycode").value;
						  if( txtcat == "1" ){
							  $("#caste").hide();
						  }
						  else{							  
							 $("#uploadPreview6").attr('src',"upload/oasis_caste_proof/" + data[0].casteproof); 
						  }
						
					}
                });
            }
			
			
        $("#btnSubmit").click(function () {	
		 if ($("#form").valid())
          {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdLearnerItgk.php"; // the script where you handle the form input.            
            var data;			
            if (Mode == 'Add')
            {
            
			}
			else {
				 data = "action=UpdateNotReportingLearner&lcode=" + txtapplicantid.value + "&status=" + ddlstatus.value + ""; // serializes the form's 
			}
           
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmWcdApprovedLearnerCountList.php?batch=" + Batch + "&itgk=" + txtitgk.value + "&districtcode=" + txtdistrict.value + "&mode=one";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
				}
            });
		}
			return false;
		  });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmwcdrejectedlearner_validation.js"></script>
</html>