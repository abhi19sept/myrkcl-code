<style type="text/css">

    #printable { display: none; }

    @media print
    {
        #non-printable { display: none; }
        #printable { display: block; }
    }
</style> 
<div id="non-printable">
    <?php
    $title = "Print Fee Receipt";
    include ('header.php');
    include ('root_menu.php');


    if (isset($_REQUEST['code'])) {
        echo "<script>var UserCode=" . $_REQUEST['code'] . "</script>";
        echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    } else {
        echo "<script>var UserCode=0</script>";
        echo "<script>var Mode='Add'</script>";
    }
    ?>
</div>


<div class="container"> 
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">Print Fee Receipt</div>
        <div class="panel-body">
            <div id="non-printable">
                <div>
                    <p>Click the button to print the Receipt.</p>

                    <button onclick="myFunction()">Print Receipt</button>

                    <script>
                        function myFunction() {
                            window.print();
                        }
                    </script>
                </div>
            </div>

            <div id="non-printable">
                <center>
                    <DIV style="width:1000px;" >
                        <div align="center">
                            <p style="text-align:center "><div style="display: inline" id="CourseName"> </div> FEE RECEIPT (ITGK Copy) - Original</p>
                        <p><div style="display: inline">
                            <img id="lphoto" width="80" height="107" alt="photo"/>
                        </div> </p>
                            <p>
                                <img id="lsign" width="80" height="35" alt="sign"/>
                            </p>
                        </div>


                        <div style="float:left ">
                            ITGK Name: <div style="display: inline" id="CenterName"> </div> (ITGK Code: <div style="display: inline" id="CenterCode"> </div>) <br>
						<span style="margin-right:116px;">	Receipt No: 4788 </span>
						 </div>
						 <div style="float:right">
                               Form No: 1506280047
                        </div>
                        </BR>
                        </BR>

                        <div style="text-align:justify; margin-top:25px;">
                            
                                We have received <div style="display:inline" id="CourseFee"> </div> /-(Rs. Two Thousand Eight Hundred Fifty only) in cash from the applicant <div style="display: inline" id="LearnerName"> </div> Father/Husband's Name: <div style="display: inline" id="FatherName"> </div> with DOB: <div style="display: inline" id="DOB"> </div> and Learner ID: <div style="display: inline" id="LearnerCode"> </div> towards tuition fee, internal assessment, learning facilitation, course material charges, examination and enrolment fee of <div style="display: inline" id="CourseName"> </div> in Regular with single instalment Mode for the batch commencing on <div style="display: inline" id="BatchStartDate"> </div> in <div style="display: inline" id="Medium"> </div> medium. This fee has been received by us on behalf of Rajasthan Knowledge Corporation Ltd. (RKCL), Vardhaman Mahaveer Open University and ourselves and we undertake to remit the respective shares to RKCL and VMOU. We also undertake to issue the course material to you at the beginning of the course, cost of which is included in this fee. In case we fail to remit the above mentioned fee received from you to RKCL and VMOU before the due date for any reason as a result of which there is a deficiency in provision of services, we undertake the full responsibility for the damage and loss to you and shall be duty bound to refund the fee received and in lump sum to you immediately. We have also received your photograph and signature. We have attested them against the proof of identity submitted by you as attached to your application form. We undertake to upload the same and scan copy of application form against your learner ID <div style="display: inline" id="LearnerCode1"> </div> on RKCL's website on or before the last date stipulated by RKCL.
                           
                        </div>
                        <div style="float:left; margin-top:25px;">
                           Seal and Signature of ITGK: <br>
                         <span style="margin-right:80px;"> Date: <?php echo date("d/m/Y"); ?> </span>
						 </div>
						 <div style="float:right; margin-top:25px;">
                          Place: <div style="display: inline" id="District"> </div><BR>
                          Center Coordinator: <div style="display: inline" id="CenterCoordinator"> </div>
                         
                        </div>
                    </DIV>
                    <!--   /////////////////////////////////////////////////////////////////////// -->


                    <DIV style="width:1000px; margin-top:120px;" >
                        <hr>
                        <div align="center">
                            <p style="text-align:center "><div style="display: inline" id="CourseName1"> </div> FEE RECEIPT (Applicant Copy) - Original</p>
                        <p><div style="display: inline">
                            <img id="lphoto1" width="80" height="107" alt="photo"/>
                        </div> </p>
                            <p>
                                <img id="lsign1" width="80" height="35" alt="sign"/>
                            </p>
                        </div>


                        <div style="float:left ">
                            ITGK Name: <div style="display: inline" id="CenterName1"> </div> (ITGK Code: <div style="display: inline" id="CenterCode1"> </div>) <br>
						<span style="margin-right:116px;">	Receipt No: 4788 </span>
						 </div>
						 <div style="float:right">
                               Form No: 1506280047
                        </div>
                        </BR>
                        </BR>

                        <div style="text-align:justify; margin-top:25px;">
                            
                                We have received <div style="display:inline" id="CourseFee1"> </div> /-(Rs. Two Thousand Eight Hundred Fifty only) in cash from the applicant <div style="display: inline" id="LearnerName1"> </div> Father/Husband's Name: <div style="display: inline" id="FatherName1"> </div> with DOB: <div style="display: inline" id="DOB1"> </div> and Learner ID: <div style="display: inline" id="LearnerCode2"> </div> towards tuition fee, internal assessment, learning facilitation, course material charges, examination and enrolment fee of <div style="display: inline" id="CourseName1"> </div> in Regular with single instalment Mode for the batch commencing on <div style="display: inline" id="BatchStartDate1"> </div> in <div style="display: inline" id="Medium1"> </div> medium. This fee has been received by us on behalf of Rajasthan Knowledge Corporation Ltd. (RKCL), Vardhaman Mahaveer Open University and ourselves and we undertake to remit the respective shares to RKCL and VMOU. We also undertake to issue the course material to you at the beginning of the course, cost of which is included in this fee. In case we fail to remit the above mentioned fee received from you to RKCL and VMOU before the due date for any reason as a result of which there is a deficiency in provision of services, we undertake the full responsibility for the damage and loss to you and shall be duty bound to refund the fee received and in lump sum to you immediately. We have also received your photograph and signature. We have attested them against the proof of identity submitted by you as attached to your application form. We undertake to upload the same and scan copy of application form against your learner ID <div style="display: inline" id="LearnerCode3"> </div> on RKCL's website on or before the last date stipulated by RKCL.
                           
                        </div>
                        <div style="float:left; margin-top:25px;">
                           Seal and Signature of ITGK: <br>
                         <span style="margin-right:80px;"> Date:  <?php echo date("d/m/Y"); ?> </span>
						 </div>
						 <div style="float:right; margin-top:25px;">
                          Place: <div style="display: inline" id="District1"> </div><BR>
                          Center Coordinator: <div style="display: inline" id="CenterCoordinator1"> </div>
                         
                        </div>
                    </DIV>
                    <DIV style="width:1000px; margin-top:120px;" >
                        <hr>
                        <p>Please contact feedback@rkcl.in for any feedback, grievances or any difficulties during RS-CIT training. You can also send an SMS</p>
                        <p> to 9220092200 by typing RKCL {Center Code} {Query}.</p>
                    </div>
                </center>
            </div>
        </div>
    </div>
</div>
<div id="non-printable">
<?php include'footer.php'; ?>
<?php include'common/message.php'; ?>
</div>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {

        var url = "common/cfPrintReceipt.php"; // the script where you handle the form input.
       alert(UserCode);
        $.ajax({
            type: "POST",
            url: url,
            data: "action=EDIT&values=" + UserCode + "",
            success: function (data)
            {
                alert(data);
                //alert($.parseJSON(data)[0]['DesignationName']);
                data = $.parseJSON(data);
                    document.getElementById('CourseName').innerHTML = data[0].CourseName;
                    //document.getElementById('LearnerPhoto').innerHTML = data[0].LearnerPhoto;
                    var lphoto = 'upload/admission_photo/' + data[0].LearnerPhoto;
					//alert(lphoto);
                    $("#lphoto").attr("src",lphoto);
                    var lsign = 'upload/admission_sign/' + data[0].LearnerSign;
                     $("#lsign").attr("src",lsign);
                    document.getElementById('CenterName').innerHTML = data[0].CenterName;
                    document.getElementById('CenterCode').innerHTML = data[0].CenterCode;
                    document.getElementById('CourseFee').innerHTML = data[0].CourseFee;
                    document.getElementById('LearnerName').innerHTML = data[0].LearnerName;
                    document.getElementById('FatherName').innerHTML = data[0].FatherName;
                    document.getElementById('DOB').innerHTML = data[0].DOB;
                    document.getElementById('LearnerCode').innerHTML = data[0].LearnerCode;
                    document.getElementById('BatchStartDate').innerHTML = data[0].BatchStartDate;
                    document.getElementById('Medium').innerHTML = data[0].Medium;
                    document.getElementById('District').innerHTML = data[0].District;
                    document.getElementById('CenterCoordinator').innerHTML = data[0].CenterCoordinator;
					
					document.getElementById('CourseName1').innerHTML = data[0].CourseName;
                    //document.getElementById('LearnerPhoto').innerHTML = data[0].LearnerPhoto;
                    var lphoto = 'upload/admission_photo/' + data[0].LearnerPhoto;
					//alert(lphoto);
                    $("#lphoto1").attr("src",lphoto);
                    var lsign = 'upload/admission_sign/' + data[0].LearnerSign;
                     $("#lsign1").attr("src",lsign);
                    document.getElementById('CenterName1').innerHTML = data[0].CenterName;
                    document.getElementById('CenterCode1').innerHTML = data[0].CenterCode;
                    document.getElementById('CourseFee1').innerHTML = data[0].CourseFee;
                    document.getElementById('LearnerName1').innerHTML = data[0].LearnerName;
                    document.getElementById('FatherName1').innerHTML = data[0].FatherName;
                    document.getElementById('DOB1').innerHTML = data[0].DOB;
                    document.getElementById('LearnerCode1').innerHTML = data[0].LearnerCode;
					document.getElementById('LearnerCode2').innerHTML = data[0].LearnerCode;
					document.getElementById('LearnerCode3').innerHTML = data[0].LearnerCode;
                    document.getElementById('BatchStartDate1').innerHTML = data[0].BatchStartDate;
                    document.getElementById('Medium1').innerHTML = data[0].Medium;
                    document.getElementById('District1').innerHTML = data[0].District;
                    document.getElementById('CenterCoordinator1').innerHTML = data[0].CenterCoordinator;


            }
        });

        return false; // avoid to execute the actual submit of the form.

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</html>