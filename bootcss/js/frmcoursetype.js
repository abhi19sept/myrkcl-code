$("#frmCourseMaster").validate({
        rules: {
            txtName: { required: true },	
 ddlCourseType : { required: true },	
 txtDuration: { required: true },	
 ddlDuration: { required: true },
 ddlMedium: { required: true },	
 ddlFeeType : { required: true },	
 txtFee: { required: true },	
 ddlExamination: { required: true },	
 ddlClasstype : { required: true },	
 ddlAffiliate: { required: true },	
 ddlStatus: { required: true },	
 
        },			
        messages: {				
			txtName: { required: '<span style="color:red; font-size: 12px;">Please enter Course Name</span>' },
			ddlCourseType: { required: '<span style="color:red; font-size: 12px;">Please enter Course Type</span>' },
			
			txtDuration: { required: '<span style="color:red; font-size: 12px;">Please enter Course Duration </span>' },
			ddlDuration: { required: '<span style="color:red; font-size: 12px;">Please select Duration Type</span>' },
			ddlMedium: { required: '<span style="color:red; font-size: 12px;">Please enter Course Medium </span>' },
			ddlFeeType: { required: '<span style="color:red; font-size: 12px;">Please enter Fee  Type </span>' },
			
			txtFee: { required: '<span style="color:red; font-size: 12px;">Please enter Coures Fee</span>' },
			ddlExamination: { required: '<span style="color:red; font-size: 12px;">Please enter Examination Type</span>' },
			ddlClasstype: { required: '<span style="color:red; font-size: 12px;">Please enter Class Type</span>' },
			
			ddlAffiliate: { required: '<span style="color:red; font-size: 12px;">Please enter Affilated Course</span>' },
			ddlStatus: { required: '<span style="color:red; font-size: 12px;">Please enter Course Status</span>' }
			
        },
	});