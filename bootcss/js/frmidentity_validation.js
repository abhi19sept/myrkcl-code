$("#frmidentity").validate({

        rules: {
        	Bhamasha_ID: { required: true, minlength:7 },
        	SSO_ID: { required: true },
        	Name: { required: true },
        	F_Name: { required: true },
        	DOB: { required: true },
            esakhi_mobile: { required: true },
            Trainer_SSO_ID: { required: true },
            trainer_contact: { required: true },
            bhamashaappnum1: { required: true, minlength:4 },
            bhamashaappnum2: { required: true, minlength:4 },
            bhamashaappnum3: { required: true, minlength:5 },
            app_application_id: { maxlength:8 },
        }, 
        messages: {				
        	Bhamasha_ID: { required: '<span style="color:red; font-size: 12px;">Bhamasha ID is required.</span>',
                        minlength: '<span class="star small">Please enter minimum 7 charecters of valid ID</span>'
                 },
		    SSO_ID: { required: '<span style="color:red; font-size: 12px;">SSO ID is required.</span>' },
        	Name: { required: '<span style="color:red; font-size: 12px;">Your name is required.</span>' },
        	F_Name: { required: '<span style="color:red; font-size: 12px;">Father/Husband name is required.</span>' },
        	DOB: { required: '<span style="color:red; font-size: 12px;">Date of birth is required.</span>' },
            esakhi_mobile: { required: '<span style="color:red; font-size: 12px;">eSakhi mobile is required.</span>' },
            Trainer_SSO_ID: { required: '<span style="color:red; font-size: 12px;">SSO ID is required.</span>' },
            trainer_contact: { required: '<span style="color:red; font-size: 12px;">Mobile number is required.</span>' },
            bhamashaappnum1: { required: '<span style="color:red; font-size: 12px;">required.</span>', 
                minlength: '<span class="star small">4 Chars</span>', },
            bhamashaappnum2: { required: '<span style="color:red; font-size: 12px;">required.</span>',
                minlength: '<span class="star small">4 Chars</span>', },
            bhamashaappnum3: { required: '<span style="color:red; font-size: 12px;">required.</span>',
                minlength: '<span class="star small">5 Chars</span>', },
            app_application_id: { maxlength: '<span class="star small">App application id must be in 8 digits max.</span>', }
        },
	});

$("#frmesakhitrainees").validate({

        rules: {
            ddleSakhi: { required: true },
            TraineeSSO_ID: { required: true },
            TraineeName: { required: true },
        },
        messages: {             
            ddleSakhi: { required: '<span style="color:red; font-size: 12px;">Please select eSakhi name.</span>' },
            TraineeSSO_ID: { required: '<span style="color:red; font-size: 12px;">SSO ID is required.</span>' },
            TraineeName: { required: '<span style="color:red; font-size: 12px;">Your name is required.</span>' },
        },
    });

$("#frmattendance").validate({

        rules: {
            Training_Start_Date: { required: true },
            trainingStatus: { required: true },
        },
        messages: {             
            Training_Start_Date: { required: '<span style="color:red; font-size: 12px;">Please select training start date.</span>' },
            trainingStatus: { required: '<span style="color:red; font-size: 12px;">Please select training complition status.</span>' },
        },
    });