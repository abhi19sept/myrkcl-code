    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        onload();
		var mapCenter, geocoder, map, infowindow, contentString;
		var GLOBAL = {};


        function onload() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfCenterlocation.php",
                data: "action=Center_locationdetail",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Acknowledgement Number" + "</span></p>");
                    }
                    else {
                        data = $.parseJSON(data);
                        txtName.value = data[0].orgname;
                        txtDistrict.value = data[0].district;
                        txtTehsil.value = data[0].tehsil;
                        txtDistrictCode.value = data[0].districtcode;
                        txtLat.value = data[0].lat;
                        txtLong.value = data[0].lng;
                        txtAddress.value = data[0].address;
						txtSysAddress.value = data[0].system_address;
						$('#position_lat').val(txtLat.value);
						$('#position_long').val(txtLong.value);
						mapCenter = new google.maps.LatLng(txtLat.value , txtLong.value); //Google map Coordinates
						geocoder = new google.maps.Geocoder;
						codeLatLng(parseFloat(txtLat.value),parseFloat(txtLong.value), "start");

                        map_initialize();
                    }

                }
            });
        }
		
		function map_initialize()
		{
			var googleMapOptions = 
			{ 
				center: mapCenter, // map center
				zoom: 10, //zoom level, 0 = earth view to higher value
				maxZoom: 20,
				minZoom: 5,
				zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL //zoom control size
			},
				scaleControl: true, // enable scale control
				mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
			};
		
		   	map = new google.maps.Map(document.getElementById("google_map"), googleMapOptions);			
			
			var point 	= new google.maps.LatLng(parseFloat(txtLat.value),parseFloat(txtLong.value));
			
			create_marker(point, txtName.value, txtSysAddress.value, true, true, false, "http://localhost/maps/icons/pin_blue.png");
			
		}
		
		function codeLatLng(lat, lng, dragstatus) {
			var latlng = new google.maps.LatLng(lat, lng);
			var getlocality = false;
			geocoder.geocode({latLng: latlng}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[1]) {
						var arrAddress = results;
						$('#position_add').val(results[1].formatted_address);
						$.each(arrAddress, function(i, address_component) {
							if (address_component.types[0] == "administrative_area_level_2" && getlocality == false) {
								itemLocality = address_component.address_components[0].long_name;
								getlocality = true;
								if(dragstatus == "start"){
									$("#markerstart_dis").val(itemLocality);
								}				
								else{
									var current_dis  = $("#markerstart_dis").val();
									var updated_info = $(infowindow.getContent());
									updated_info = updated_info.find('p').html(results[1].formatted_address);
									infowindow.setContent(updated_info);
									if(itemLocality != current_dis){
										BootstrapDialog.alert("You can drag marker in your district only which is registered in database.");
										dragstatus.setPosition(GLOBAL.startDragPosition);
										$('#position_lat').val(GLOBAL.startDragPosition.lat)
										$('#position_long').val(GLOBAL.startDragPosition.lng);
										delete GLOBAL.startDragPosition;
									}
								}
							}
						});
					} 
					else {
						BootstrapDialog.alert("Please select a proper Location.");
						dragstatus.setPosition(GLOBAL.startDragPosition);
						$('#position_lat').val(GLOBAL.startDragPosition.lat)
						$('#position_long').val(GLOBAL.startDragPosition.lng);
						delete GLOBAL.startDragPosition;
					}
				} else {
					BootstrapDialog.alert("Geocoder failed due to: " + status);
					dragstatus.setPosition(GLOBAL.startDragPosition);
					$('#position_lat').val(GLOBAL.startDragPosition.lat)
					$('#position_long').val(GLOBAL.startDragPosition.lng);
					delete GLOBAL.startDragPosition;
				}
			});
		}
		//############### Create Marker Function ##############
		function create_marker(MapPos, MapTitle, MapDesc,  InfoOpenDefault, DragAble, Removable, iconPath)
		{	  	  		  
			//new marker
			var marker = new google.maps.Marker({
				position: MapPos,
				map: map,
				draggable:DragAble,
				animation: google.maps.Animation.DROP,
				title:"Address",
				icon: iconPath
			});
			
			
			
			//Content structure of info Window for the Markers
			contentString = $('<div class="marker-info-win">'+
			'<div class="marker-inner-win"><span class="info-content">'+
			'<h1 class="marker-heading">'+MapTitle+'</h1><p>'+MapDesc+'</p></span></br><button name="save-marker" id="save_marker" class="save-marker">Save Marker</button>'+'</div></div>');	

			
			//Create an infoWindow
			infowindow = new google.maps.InfoWindow();
			//set the content of infoWindow
			infowindow.setContent(contentString[0]);

			//Find remove button in infoWindow
			var saveBtn 	= contentString.find('button.save-marker')[0];
			
			if(typeof saveBtn !== 'undefined') //continue only when save button is present
			{
				
			}
			google.maps.event.addDomListener(saveBtn, "click", function(event) {
				save_marker();
			});
						  
			if(InfoOpenDefault) //whether info window should be open by default
			{
			  infowindow.open(map,marker);
			}
			google.maps.event.addListener(marker, 'dragstart', function(evt){
				GLOBAL.startDragPosition = this.getPosition();
			});
			google.maps.event.addListener(marker, 'dragend', function (event) {
				$('#position_lat').val(this.getPosition().lat())
				$('#position_long').val(this.getPosition().lng());
				codeLatLng(this.getPosition().lat(), this.getPosition().lng(), marker);
			});
		}
	
		//############### Save Marker Function ##############
		function save_marker()
		{
			//Save new marker using jQuery Ajax
			var mlat = $('#position_lat').val(); //get marker position
			var mlong = $('#position_long').val(); //get marker position
			var madd = $('#position_add').val(); //get marker position
			var morg = $('#position_org').val(); //get marker position
			if($('#loc_terms').prop("checked") == true){
				$.ajax({
					url: 'common/cfCenterlocation.php',
					type: "post",
					data: "action=MapSave&lat=" + mlat + "&long="+mlong+"&org_code="+morg+'&madd='+madd,
					success:function(data){
						// if(confirm('your confirmation text')){
							// document.location.reload(true);
						// }
						BootstrapDialog.alert(data);
						setTimeout(function(){
						  location.reload();
						}, 2000);
					},
					error:function (xhr, ajaxOptions, thrownError){
						BootstrapDialog.alert(thrownError); //throw any errors
					}
				});
			}
			else{
				BootstrapDialog.alert("Please check the terms & conditions.");
			}
			
		}

    });
