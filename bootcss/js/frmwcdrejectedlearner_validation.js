$("#form").validate({
        rules: {
            txtRemark: { required: true },	ddlstatus: { required: true }
        },			
        messages: {				
			txtRemark: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Remark</span>' },
			ddlstatus: 					{ required: '<span style="color:red; font-size: 12px;">Please select Status</span>' }
        },
	});