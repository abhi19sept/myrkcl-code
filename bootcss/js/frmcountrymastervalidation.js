$("#frmCountryMaster").validate({
        rules: {
            txtCountryName: { required: true },	ddlStatus: { required: true }
        },			
        messages: {				
			txtCountryName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Country Name</span>' },
			ddlStatus: 			{ required: '<span style="color:red; font-size: 12px;">Please choose status</span>' }
			
        },
	});