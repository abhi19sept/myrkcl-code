$("#frmBankAccount").validate({
        rules: {
            txtIfscCode: { required: true },
			txtaccountName: { required: true },
			txtaccountNumber: { required: true },
			rbtaccountType_saving: { required: true },
			
			txtConfirmaccountNumber: { required: true },
			
			
			txtIfscCode: { required: true },
			rbtaccountType_saving: { required: true },
			ddlBankName: { required: true },
			ddlBranch: { required: true },
			txtMicrcode: { required: true },
			chequeImage: { required: true },
			txtpanno: { required: true },
			txtpanname: { required: true },
			pancard: { required: true },
			ddlidproof: { required: true },
			chk: { required: true },
			txtCentercode: { required: true }
			
			
        },			
        messages: {				
			
			txtaccountName: { required: '<span style="color:red; font-size: 12px;">Please Enter Account Name</span>' },
			txtaccountNumber: { required: '<span style="color:red; font-size: 12px;">Please Enter Account Number</span>' },
			rbtaccountType_saving: { required: '<span style="color:red; font-size: 12px;">Please Enter Account Type</span>' },
			txtIfscCode: { required: '<span style="color:red; font-size: 12px;">Please Enter IFSC Code</span>' },
			rbtaccountType_saving: { required: '<span style="color:red; font-size: 12px;">Please Enter Account Type</span>' },
			ddlBankName: { required: '<span style="color:red; font-size: 12px;">Please Enter Bank Name</span>' },
			ddlBranch: { required: '<span style="color:red; font-size: 12px;">Please Enter Branch name</span>' },
			txtMicrcode: { required: '<span style="color:red; font-size: 12px;">Please Enter MICR Code</span>' },
			chequeImage: { required: '<span style="color:red; font-size: 12px;">Please upload image</span>' },
			txtpanno: { required: '<span style="color:red; font-size: 12px;">Please Enter Pan Number</span>' },
			txtpanname: { required: '<span style="color:red; font-size: 12px;">Please Enter Pan Card Holder Name </span>' },
			pancard: { required: '<span style="color:red; font-size: 12px;">Please upload Pan Card image</span>' },
			ddlidproof: { required: '<span style="color:red; font-size: 12px;">Please Select Bank Account ID Proof </span>' },
			
			txtConfirmaccountNumber: { required: '<span style="color:red; font-size: 12px;">Confirm Account Number</span>' },
			chk: { required: '<span style="color:red; font-size: 12px;">Please Select Check Box to Accept Terms and Contdition</span>' },
			txtIfscCode: { required: '<span style="color:red; font-size: 12px;">Please Enter IFSC Code</span>' },
			txtCentercode: { required: '<span style="color:red; font-size: 12px;">Please Enter Center Code </span>' }
			
			
        },
	});