$("#frmMunicipalMaster").validate({
        rules: {
            txtMunicipalName: { required: true },	ddlDistrict: { required: true },ddlStatus: { required: true }
        },			
        messages: {				
			txtMunicipalName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Municipal Name</span>' },
			ddlDistrict: 			{ required: '<span style="color:red; font-size: 12px;">Please enter District</span>' },
			ddlStatus: 				{ required: '<span style="color:red; font-size: 12px;">Please enter status</span>' }
			
        },
	});