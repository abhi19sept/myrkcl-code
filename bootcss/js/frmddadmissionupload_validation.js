$("#DDForm").validate({
    rules: {
        ddno: {required: true, minlength: 6, maxlength: 6},
        dddate: {required: true},
        txtMicrNo: {required: true, minlength: 9, maxlength: 9},
        ddlBankDistrict: {required: true},
        ddreceipt: {required: true},
        ddlBankName: {required: true},
        txtBranchName: {required: true}



    },
    messages: {
        ddno: {required: '<span style="color:red; font-size: 12px;">Please Enter DD No</span>',
            minlength: '<span style="color:red;">Please enter at least 6 digits</span>',
            maxlength: '<span style="color:red;">Please enter not more than 6 digits</span>'},
        dddate: {required: '<span style="color:red; font-size: 12px;">Please Select Date</span>'},
        txtMicrNo: {required: '<span style="color:red; font-size: 12px;">Please Enter Micr No</span>',
            minlength: '<span style="color:red;">Please enter at least 9 digits</span>',
            maxlength: '<span style="color:red;">Please enter not more than 9 digits</span>'},
        ddlBankDistrict: {required: '<span style="color:red; font-size: 12px;">Please Select District</span>'},
        ddreceipt: {required: '<span style="color:red; font-size: 12px;">Please Attach DD image</span>'},
        ddlBankName: {required: '<span style="color:red; font-size: 12px;">Please Select Bank</span>'},
        txtBranchName: {required: '<span style="color:red; font-size: 12px;">Please Select Branch</span>'}


    }
});