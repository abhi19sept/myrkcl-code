$("#frmbatchmaster").validate({
        rules: {
                        txtBatchName: { required: true },	txtstartdate: { required: true },		txtenddate: { required: true },
						ddlStatus: { required: true },	ddlCourse: { required: true },	ddlinstallmode: { required: true },	ddlcertificatecode: { required: true },
						ddlFinancial: { required: true },	ddlYear: { required: true },	txtCourseFee: { required: true },	txtShare: { required: true }
        },			
        messages: {				
			txtBatchName: { required: '<span style="color:red; font-size: 12px;">Please Enter Batch Name</span>' },							
			txtstartdate: { required: '<span style="color:red; font-size: 12px;">Please Select start date</span>' },
			txtenddate: { required: '<span style="color:red; font-size: 12px;">Please Select end date</span>' },
			ddlStatus: { required: '<span style="color:red; font-size: 12px;">Please Select Batch Status</span>' },
			ddlCourse: { required: '<span style="color:red; font-size: 12px;">Please Select your Course</span>' },
			ddlinstallmode: { required: '<span style="color:red; font-size: 12px;">Please Select Installation Mode</span>' },
			ddlcertificatecode: { required: '<span style="color:red; font-size: 12px;">Please Select Certification Code</span>' },
			ddlFinancial: { required: '<span style="color:red; font-size: 12px;">Please Select Financial Year</span>' },
			ddlYear: { required: '<span style="color:red; font-size: 12px;">Please Select Year</span>' },
			txtCourseFee: { required: '<span style="color:red; font-size: 12px;">Please Enter Course Fees</span>' },
			txtShare: { required: '<span style="color:red; font-size: 12px;">Please Enter RKCL Share</span>' }
        }
	});