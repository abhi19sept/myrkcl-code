$("#form").validate({
        rules: {
            txtLCode: { required: true },	txtEmpName: { required: true },		txtFaName: { required: true },		dlempdistrict: { required: true },
			txtEmpMobile: { required: true, minlength:10, maxlength: 10 },	txtEmail: { required: true, email: true },	 txtEmpAddress: { required: true },	
			ddlDeptName: { required: true },		txtEmpId: { required: true },	 txtGpfNo: { required: true },	
			ddlEmpDesignation: { required: true },		txtEmpMarks: { required: true },		ddlExamattepmt: { required: true },
			txtPanNo: { required: true },	txtBankAccount: { required: true },		ddlBankDistrict: { required: true },	
			ddlBankName: { required: true },   txtBranchName: { required: true },	txtIfscNo: { required: true },		txtMicrNo: { required: true },
			fileReceipt: { required: true}, fileBirth: { required: true}, ddlDobProof: { required:true}, chk: { required: true}
        },			
        messages: {				
			txtLCode: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your learner code</span>' },
			txtEmpName: 			{ required: '<span style="color:red; font-size: 12px;">Please enter your name</span>' },
			txtFaName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your learner code</span>' },
			dlempdistrict: 			{ required: '<span style="color:red; font-size: 12px;">Please select your district</span>' },
			txtEmpMobile: 			{ required: '<span style="color:red; font-size: 12px;">Please enter your mobile no.</span>',
										minlength:'<span style="color:red;">Please enter at least 10 digits</span>',
										maxlength:'<span style="color:red;">Please enter not more than 10 digits</span>'},
			txtEmail: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your email</span>', 
										email: '<span style="color:red; font-size: 12px;">Please enter a VALID email</span>' },
			txtEmpAddress: 			{ required: '<span style="color:red; font-size: 12px;">Please enter your address</span>' },
			ddlDeptName: 			{ required: '<span style="color:red; font-size: 12px;">Please select your department</span>' },
			txtEmpId: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your employee id</span>' },
			txtGpfNo: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your GPF No.</span>' },
			ddlEmpDesignation: 		{ required: '<span style="color:red; font-size: 12px;">Please select designation</span>' },
			txtEmpMarks: 			{ required: '<span style="color:red; font-size: 12px;">Please enter your Marks</span>' },
			ddlExamattepmt: 		{ required: '<span style="color:red; font-size: 12px;">Please select attempt</span>' },
			txtPanNo: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your PAN No</span>' },
			txtBankAccount: 		{ required: '<span style="color:red; font-size: 12px;">Please enter your bank account</span>' },
			ddlBankDistrict: 		{ required: '<span style="color:red; font-size: 12px;">Please enter your bank district</span>' },
			ddlBankName: 			{ required: '<span style="color:red; font-size: 12px;">Please select your bank name</span>' },
			txtBranchName: 			{ required: '<span style="color:red; font-size: 12px;">Please enter your branch name</span>' },
			txtIfscNo: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your IFSC No.</span>' },
			txtMicrNo: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your MICR No.</span>' },
			fileReceipt:			{ required: '<span style="color:red; font-size: 12px;">Please upload payment receipt.</span>' },
			fileBirth: 				{ required: '<span style="color:red; font-size: 12px;">Please upload birth proof.</span>' },
			ddlDobProof:			{ required: '<span style="color:red; font-size: 12px;">Please select Date of Birth Proof</span>' },
			chk: 					{ required: '<span style="color:red; font-size: 12px;">Please check first</span>'}
        },
	});