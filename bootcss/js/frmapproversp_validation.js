$("#form").validate({
        rules: {
            ddlstatus: { required: true },	txtRemark: { required: true }
        },			
        messages: {				
			ddlstatus: 				{ required: '<span style="color:red; font-size: 12px;">Please select Process Status</span>' },
			txtRemark:		 			{ required: '<span style="color:red; font-size: 12px;">Please Enter Remarks</span>' }
        },
	});