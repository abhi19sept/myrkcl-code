$("#frmmarkwcdattendance").validate({
        rules: {
            	
 ddlCourse : { required: true },	
 
 ddlBatch: { required: true },

 dob: { required: true },
 
 txtclassurl: { required: true }, 

 classdur: { required: true }, 
 
        },			
        messages: {				

			ddlCourse: { required: '<span style="color:red; font-size: 12px;">Please Select Course Name</span>' },
	
			ddlBatch: { required: '<span style="color:red; font-size: 12px;">Please select Batch Name</span>' },
			
			dob: { required: '<span style="color:red; font-size: 12px;">Please select Date</span>' },
			
			txtclassurl: { required: '<span style="color:red; font-size: 12px;">Please Enter Slot URL</span>' },

			classdur: { required: '<span style="color:red; font-size: 12px;">Please Class Duration</span>' }
			
        },
	});