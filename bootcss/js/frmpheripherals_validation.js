$("#frmItPeripherals").validate({
        rules: {
                        ddlDevice: { required: true },	
						availableYes: { required: true },	
						txtMake: { required: true },	
						txtModel: { required: true },	
						txtQuantity: { required: true },
						txtDetail: { required: true }
							
                    
        },			
        messages: {				
			ddlDevice: { required: '<span style="color:red; font-size: 12px;">Please Select Device</span>' },
			availableYes: { required: '<span style="color:red; font-size: 12px;">Please choose</span>' },
			txtMake: { required: '<span style="color:red; font-size: 12px;">Please Enter Make</span>' },
			txtModel: { required: '<span style="color:red; font-size: 12px;">Please Enter Model no</span>' },
			txtQuantity: { required: '<span style="color:red; font-size: 12px;">Please Enter NO of Items</span>' },
			txtDetail: { required: '<span style="color:red; font-size: 12px;">Please Enter Details</span>' }
			
        }
	});