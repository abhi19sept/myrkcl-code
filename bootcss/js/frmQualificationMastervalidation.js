$("#frmQualificationMaster").validate({
        rules: {
            txtQualificationName: { required: true },	ddlStatus: { required: true }
        },			
        messages: {				
			txtQualificationName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Qualification</span>' },
			ddlStatus: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Status</span>' }
			
			
        },
	});