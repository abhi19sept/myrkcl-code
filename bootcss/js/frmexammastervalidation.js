$("#basicform").validate({
        rules: {
            ddlAffilate: { required: true },	
			ddlEvent: { required: true },
		    ddlCourse: { required: true },	
			ddlAssessment: { required: true },
			ddlFee: { required: true },	
			txtEnd: { required: true },
			txtDescription: { required: true },	
			ddlFreshBaches: { required: true },
			ddlReexamBatches: { required: true },
			ddlFreshBaches: { required: true },
			ddlReexamBatches: { required: true },
			
			txtEventAstart: { required: true },
			txtEventAEnd: { required: true },
			ddlStatus: { required: true }
			
			
			
			
			
        },			
        messages: {				
			ddlAffilate: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Affiliate Name</span>' },
			ddlEvent: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Event Name</span>' },
			
			ddlCourse: 			{ required: '<span style="color:red; font-size: 12px;">Please enter course</span>' },
			
			ddlAssessment: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Assesment</span>' },
			ddlFee: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Fee</span>' },
			txtEnd: 			{ required: '<span style="color:red; font-size: 12px;">Please enter End date</span>' },
			
			
			
			txtDescription: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Description</span>' },
			ddlFreshBaches: 				{ required: '<span style="color:red; font-size: 12px;">Please enter fresh bach</span>' },
			ddlReexamBatches: 			{ required: '<span style="color:red; font-size: 12px;">Please enter re exam batch/span>' },
			
			
			
			
		
			txtEventAstart: 			{ required: '<span style="color:red; font-size: 12px;">Please select start date</span>' },
			txtEventAEnd: 			{ required: '<span style="color:red; font-size: 12px;">Please select End date</span>' },
			ddlStatus: 			{ required: '<span style="color:red; font-size: 12px;">Please enter status</span>' }
			
			
			
			
        },
	});