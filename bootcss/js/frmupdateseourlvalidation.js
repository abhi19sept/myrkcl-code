$("#frmupdateseourl").validate({
        rules: 
		{
            ddlRoot: { required: true },
			ddlParent: { required: true },	
			txtFunctionURL: { required: true },	
			txtSeoUrl: { required: true },
			ddlChild: { required: true }
			
						
        },			
        messages: {				
			ddlRoot: { required: '<span style="color:red; font-size: 12px;">Select Root Menu </span>' },
			ddlParent: { required: '<span style="color:red; font-size: 12px;">Select Parent Menu </span>' },
			txtFunctionURL: { required: '<span style="color:red; font-size: 12px;">Enter function Url </span>' },
			txtSeoUrl: { required: '<span style="color:red; font-size: 12px;">Enter SEO Url</span>' },
			ddlChild: { required: '<span style="color:red; font-size: 12px;">Select Child Menu</span>' }
			
			
        },
});