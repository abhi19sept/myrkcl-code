$("#frmStateMaster").validate({
        rules: {
            txtStateName: { required: true },	ddlCountry: { required: true },		ddlStatus: { required: true }
        },			
        messages: {				
			txtStateName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter State Name</span>' },
			ddlCountry: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Country Name</span>' },
			ddlStatus: 				{ required: '<span style="color:red; font-size: 12px;">Please enter status</span>' }
			
        },
	});