$("#frmcategorymaster").validate({
        rules: {
            txtCategoryName: { required: true },	ddlStatus: { required: true }
        },			
        messages: {				
			txtCategoryName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Category Name</span>' },
			ddlStatus: 			{ required: '<span style="color:red; font-size: 12px;">Please choose status</span>' }
			
        },
	});