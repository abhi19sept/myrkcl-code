var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    }, decode: function (e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r)
            }
            if (a != 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    }, _utf8_encode: function (e) {
        e = e.replace(/rn/g, "n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    }, _utf8_decode: function (e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t
    }
}

function encryptString(str) {
    var encryptedString = '';
    encryptedString = Base64.encode(str);
    return encryptedString;
}

function decryptString(str) {
    var decryptString;
    decryptString = Base64.decode(str);
    return decryptString;
}

var reference_number = '';
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    function sendMailtoITGK() {
        var url = "common/cfmodifyITGK.php";
        var data;
        data = "action=sendEmailToITGK&reference_number=" + reference_number + "&requestType=address";
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
                if (data === "1") {
                    $("#submitted").modal("show");
                    $("#submitting").modal("hide");
                } else {
                    alert("Some error occurred, please try again later, redirecting you back to dashboard 2");
                    setTimeout(function () {
                        window.location.href = 'dashboard.php';
                    }, 2000);
                }
            }
        });
    }

    function sendEmailToSP() {
        var url = "common/cfmodifyITGK.php";
        var data;
        data = "action=sendEmailToSP&reference_number=" + reference_number + "&requestType=address";
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
                if (data === "1") {
                    sendMailtoITGK()
                } else {
                    alert("Some error occurred, please try again later, redirecting you back to dashboard 1");
                    setTimeout(function () {
                        window.location.href = 'dashboard.php';
                    }, 2000);
                }
            }
        });
    }

    $("#updateOrgDetails").on("submit", function (e) {
if(ddlreason.value == '' || landmark.value == ''){
    alert("Please Select All Fields.");
} else {
        e.preventDefault();
        var form = $(this);
        form.parsley().validate();
        $("#submitting").modal("show");

        if (form.parsley().isValid()) {
            var url = "common/cfmodifyITGK.php";
            var data;
            data = new FormData(this);

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                processData: false,
                contentType: false,
                success: function (data) {

                    if (data === "Successfully Updated") {
                        sendEmailToSP();
                    } else {
                        $("#submitting").modal("hide");
                        alert(data);
                    }
                }
            });
        } else {
            $("#submitting").modal("hide");
        }
    }
        return false;
    });

    $('#otp_modal_updateAddressITGK').on('hidden.bs.modal', function () {

        reference_number = $("#otp_updateAddressITGK").val();
        $("#reference_number").val(reference_number);

        $("#errorTextOTP_updateAddressITGK, #successText_updateAddressITGK, #responseText_updateAddressITGK").html("");
        $("#showError_updateAddressITGK, #showSuccess_updateAddressITGK, #responseText_updateAddressITGK, #verifyOTP_updateAddressITGK").css("display", "none");
        $("#newMobile_updateAddressITGK, #otp_updateAddressITGK").removeAttr("readonly").val("");
        $("#sendOTP_updateAddressITGK").text("Send OTP").removeClass("disabled");
    });

    function sendOTPtoITGK() {

        var phoneNumber = $("#newMobile_updateAddressITGK").val();
        var url = "common/cfmodifyITGK.php"; // the script where you handle the form input.
        var data;
        var forminput = "phoneNumber=" + phoneNumber;
        data = "action=sendOTP&" + forminput + "&fld_requestChangeType=address"; // serializes the form's elements.

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
                $("#responseText").html(data).css("display", "block");

            }
        });
    }

    $("#sendOTP_updateAddressITGK").click(function (e) {

        e.preventDefault();

        $("#sendOTP_updateAddressITGK").text("Please wait...").addClass("disabled");

        var pattern = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;

        var phoneNumber = $("#newMobile_updateAddressITGK").val();

        if (pattern.test(phoneNumber)) {
            if (phoneNumber.length === 10) {

                $("#errorTextOTP, #successText").html("");
                $("#showError, #showSuccess").css("display", "none");

                var url = "common/cfmodifyITGK.php";
                var data;
                var forminput = "phoneNumber=" + phoneNumber;
                data = "action=checkMobile&" + forminput;


                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {

                        var JSONObject = JSON.parse(data);

                        if (JSONObject.total_count === "0") {
                            $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
                            $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
                            $("#errorTextOTP_updateAddressITGK").html("Mobile number not found in our records");
                            $("#showError_updateAddressITGK").css("display", "block");
                            $("#sendOTP_updateAddressITGK").text("Send OTP").removeClass("disabled");
                        } else {
                            sendOTPtoITGK();
                        }
                    }
                });

            } else {
                $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
                $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
                $("#errorTextOTP_updateAddressITGK").html("Invalid mobile number");
                $("#showError_updateAddressITGK").css("display", "block");
                $("#sendOTP_updateAddressITGK").text("Send OTP").removeClass("disabled");
            }
        }
        else {
            $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
            $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
            $("#errorTextOTP_updateAddressITGK").html("Invalid mobile number");
            $("#showError_updateAddressITGK").css("display", "block");
            $("#sendOTP_updateAddressITGK").text("Send OTP").removeClass("disabled");
        }
    });

    $("#verifyOTPbtn_updateAddressITGK").click(function () {

        $("#newMobile_updateAddressITGK").attr("readonly", "readonly");
        $("#otp_updateAddressITGK").attr("readonly", "readonly");
        if ($("#otp_updateAddressITGK").val() === "") {
            $("#otp_updateAddressITGK").removeAttr("readonly").val("");
            $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
            $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
            $("#errorTextOTP_updateAddressITGK").html("Invalid OTP");
            $("#showError_updateAddressITGK").css("display", "block");
        } else {
            var url = "common/cfmodifyITGK.php"; // the script where you handle the form input.
            var data;
            var forminput = "otp=" + $("#otp_updateAddressITGK").val();
            forminput = forminput + "&learner=" + $("#LearnerCode").val() + "&mobile=" + $("#newMobile_updateAddressITGK").val();
            data = "action=verifyOTP&" + forminput; // serializes the form's elements.
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    var JSONObject = JSON.parse(data);
                    if (JSONObject.total_count === "0") {
                        $('#successText_updateAddressITGK').html('');
                        $('#showSuccess_updateAddressITGK').css('display', 'none');
                        $('#errorTextOTP_updateAddressITGK').html('Invalid OTP');
                        $('#showError_updateAddressITGK').css('display', 'block');
                        $("#sendOTP_updateAddressITGK").text("Send OTP").removeClass("disabled");
                        $("#otp_updateAddressITGK").removeAttr("readonly");
                    } else {
                        $('#showError_updateAddressITGK').css('display', 'none');
                        $('#successText_updateAddressITGK').html('OTP has been successfully verified, you can now proceed to update address<BR>Redirecting you to the correct page');
                        $('#showSuccess_updateAddressITGK').css('display', 'block');
                        $('#verifyOTP').css('display', 'block');

                        setTimeout(function () {
                            $('#loading').modal('show');
                            $('#otp_modal_updateAddressITGK').modal('hide');
                            fillForm();
                        }, 1000);
                    }
                }
            });
        }
    });

    $("#ddlMunicipalType").change(function () {
        var reg = $("#ddlDistrict").val();
        $.ajax({
            url: 'common/cfOrgUpdate.php',
            type: "post",
            data: "action=FillMunicipalName&values=" + reg + "",
            success: function (data) {
                $('#ddlMunicipalName').html(data);
            }
        });
    });

    $("#ddlMunicipalName").change(function () {
        var selMunicipalName = $(this).val();
        $.ajax({
            url: 'common/cfOrgUpdate.php',
            type: "post",
            data: "action=FILLWardno&values=" + selMunicipalName + "",
            success: function (data) {
                $('#ddlWardno').html(data);
            }
        });
    });


    $("#ddlPanchayat").change(function () {
        var selPanchayat = $(this).val();
        $.ajax({
            url: 'common/cfCenterRegistration.php',
            type: "post",
            data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
            success: function (data) {
                $('#ddlGramPanchayat').html(data);
            }
        });
    });

    $("#ddlGramPanchayat").change(function () {
        var selGramPanchayat = $(this).val();
        $.ajax({
            url: 'common/cfVillageMaster.php',
            type: "post",
            data: "action=FILL&values=" + selGramPanchayat + "",
            success: function (data) {
                $('#ddlVillage').html(data);
            }
        });
    });


    $("#Own").click(function () {
        $("#onRent").css("display", "none");
        $("#onOwn").css("display", "block");

        $("#fld_rentAgreement").attr('data-parsley-required', 'false');
        $("#fld_utilityBill").attr('data-parsley-required', 'false');
        $("#fld_ownershipDocument").attr('data-parsley-required', 'true');
    });

    $("#Rent").click(function () {
        $("#onRent").css("display", "block");
        $("#onOwn").css("display", "none");
        
        $("#fld_rentAgreement").attr('data-parsley-required', 'true');
        $("#fld_utilityBill").attr('data-parsley-required', 'true');
        $("#fld_ownershipDocument").attr('data-parsley-required', 'false');
    });
    
$("#Relative").click(function () {
        $("#onRent").css("display", "block");
        $("#onOwn").css("display", "none");
        
        jQuery("label[for='rentAgreement']").html("Ownership Document with Relative Name");
        jQuery("label[for='utilityBill']").html("NOC Certificate from Relative");
        
        $("#fld_rentAgreement").attr('data-parsley-required', 'true');
        $("#fld_utilityBill").attr('data-parsley-required', 'true');
        $("#fld_ownershipDocument").attr('data-parsley-required', 'false');
    });
    
    $("#Special").click(function () {
        $("#onRent").css("display", "block");
        $("#onOwn").css("display", "none");
        
        jQuery("label[for='rentAgreement']").html("Authority Letter by AUTHORIZED PERSON");
        jQuery("label[for='utilityBill']").html("NOC by AUTHORIZED PERSON");
        
        $("#fld_rentAgreement").attr('data-parsley-required', 'true');
        $("#fld_utilityBill").attr('data-parsley-required', 'true');
        $("#fld_ownershipDocument").attr('data-parsley-required', 'false');
    });
    
    $("#Rural").click(function () {
        FillPanchayat($("#ddlDistrict").val());
        $("#ruralArea").css("display", "block");
        $("#urbanArea").css("display", "none");
    });

    $("#Urban").click(function () {
        FillMunicipalType();
        FillMunicipalName($("#ddlDistrict").val());
        FILLWardno($("#ddlDistrict").val());
        $("#ruralArea").css("display", "none");
        $("#urbanArea").css("display", "block");
    });

    function FillCountry() {
        $.ajax({
            type: "post",
            url: "common/cfCountryMaster.php",
            data: "action=FILL",
            success: function (data) {
                $("#ddlCountry").html(data);
            }
        });
    }

    function FillState() {
        $.ajax({
            url: 'common/cfStateMaster.php',
            type: "post",
            data: "action=FILLALL",
            success: function (data) {
                $('#ddlState').html(data);
            }
        });
    }

    function FillRegion(reg) {
        $.ajax({
            url: 'common/cfRegionMaster.php',
            type: "post",
            data: "action=FILL&values=" + reg + "",
            success: function (data) {
                $('#ddlRegion').html(data);
            }
        });
    }

    function FillDistrict(reg) {
        $.ajax({
            url: 'common/cfDistrictMaster.php',
            type: "post",
            data: "action=FILL&values=" + reg + "",
            success: function (data) {
                $('#ddlDistrict').html(data);
            }
        });
    }

    function FillTehsil(reg) {
        $.ajax({
            url: 'common/cfTehsilMaster.php',
            type: "post",
            data: "action=FILL&values=" + reg + "",
            success: function (data) {
                $('#ddlTehsil').html(data);
                $('#ddlTehsilNew').html(data);
            }
        });
    }

    function FillMunicipalType() {
        $.ajax({
            url: 'common/cfOrgUpdate.php',
            type: "post",
            data: "action=FillMunicipalType",
            success: function (data) {
                $('#ddlMunicipalType').html(data);
            }
        });
    }

    function FillMunicipalName(reg) {
        $.ajax({
            url: 'common/cfOrgUpdate.php',
            type: "post",
            data: "action=FillMunicipalName&values=" + reg + "",
            success: function (data) {
                $('#ddlMunicipalName').html(data);
            }
        });
    }

    function FILLWardno(reg) {
        $.ajax({
            url: 'common/cfOrgUpdate.php',
            type: "post",
            data: "action=FILLWardno&values=" + reg + "",
            success: function (data) {
                $('#ddlWardno').html(data);
            }
        });
    }

    function FillPanchayat(reg) {
        $.ajax({
            url: 'common/cfCenterRegistration.php',
            type: "post",
            data: "action=FILLPanchayat_NEW&values=" + reg + "",
            success: function (data) {
                $('#ddlPanchayat').html(data);
            }
        });
    }

    function FillGramPanchayat(reg) {
        $.ajax({
            url: 'common/cfCenterRegistration.php',
            type: "post",
            data: "action=FILLGramPanchayat_NEW&values=" + reg + "",
            success: function (data) {
                $('#ddlGramPanchayat').html(data);
            }
        });
    }

    function FillVillage(reg) {
        $.ajax({
            url: 'common/cfVillageMaster.php',
            type: "post",
            data: "action=FILL_NEW&values=" + reg + "",
            success: function (data) {
                $('#ddlVillage').html(data);
            }
        });
    }

    $("#revokeReference").on("click", function () {
        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=revokeReference&otp=" + reference_number + "",
            success: function (data) {
                $("#errorModal_Custom_backdrop").modal("hide");
                if (data === "Successfully Updated") {
                    $("#revokeText").html("Reference Number <span style='color: #00A65A'>" + reference_number + "</span> has been successfully revoked, redirecting to correct page.." +
                        ".");
                    $("#revoke_success").modal("show");
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
            }
        });
    });

    firstCheck();

    //fillForm();


    function firstCheck() {
        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=firstCheck&fld_requestChangeType=address",
            success: function (data) {

                var JSONObject = JSON.parse(data);

                if (JSONObject.total_count === "-1") {
                    $('#otp_modal_updateAddressITGK').modal('show');
                } else {
                    /***reference_number = JSONObject.ref_no;
                     $("#errorText_backdrop").html("Address update request has been already submitted, the reference number is: <span style='color: #00A65A'>" + JSONObject.ref_no + "</span>, however you can revoke this reference number to re-submit the form again.");
                     $("#errorModal_Custom_backdrop").modal('show');***/

                    if (JSONObject.fld_status === "0") {
                        reference_number = JSONObject.ref_no;
                        $("#editRequest_backdrop").html("Address update request has been already opened and pending for submission, the reference number is: <span style='color: #00A65A'>" + JSONObject.ref_no + "</span>, Click on Edit this Request to complete form submission.");

                        $("#editRequest_button").attr("onclick", "window.location.href='editRequest.php?flag=" + encryptString('address') + "&code=" + encryptString(reference_number) + "'");

                        $("#editRequest").modal('show');
                    }

                    if (JSONObject.fld_status === "1") {
                        reference_number = JSONObject.ref_no;
                        $("#editRequest_backdrop").html("Address update request has been already submitted, the reference number is: <span style='color: #00A65A'>" + JSONObject.ref_no + "</span>, however you can re-submit the form again, until it is approved by your Service Provider");

                        $("#editRequest_button").attr("onclick", "window.location.href='editRequest.php?flag=" + encryptString('address') + "&code=" + encryptString(reference_number) + "'");

                        $("#editRequest").modal('show');
                    }

                    if (JSONObject.fld_status === "5") {
                        reference_number = JSONObject.ref_no;
                        $("#editRequest_block_backdrop").html("Address update request has been already submitted, and Paid, the reference number is: <span style='color: #00A65A'>" + JSONObject.ref_no + "</span>, You cannot re-submit the form at this point.");
                        $("#editRequest_block").modal('show');
                    }

                    if (JSONObject.fld_status === "3") {
                        reference_number = JSONObject.ref_no;
                        $("#editRequest_block_backdrop").html("Address update request has been already submitted, and it is pending for payment, the reference number is: <span style='color: #00A65A'>" + JSONObject.ref_no + "</span>, You cannot re-submit the form at this point.");
                        $("#editRequest_block").modal('show');
                    }
                }
            }
        });
    }

    function fillForm() {

        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=EDIT_NEW&values=" + OrgCode + "",
            success: function (data) {

                $('#updateOrgDetails').parsley().destroy();

                data = $.parseJSON(data);

                setTimeout(function () {
                    $("#txtRoad").val(data[0].Organization_Road);
                    $("#Organization_Road_old").val(data[0].Organization_Road);
                    $("#txtStreet").val(data[0].Organization_Street);
                    $("#Organization_Street_old").val(data[0].Organization_Street);
                    $("#txtHouseno").val(data[0].Organization_HouseNo);
                    $("#Organization_HouseNo_old").val(data[0].Organization_HouseNo);
                    $("#txtLandmark").val(data[0].Organization_Landmark);
                    $("#Organization_Landmark_old").val(data[0].Organization_Landmark);
                    $("#txtAddress").val(data[0].Organization_Address);
                    $("#Organization_Address_old").val(data[0].Organization_Address);

                    FillCountry();
                    FillState();
                    FillRegion(data[0].Organization_State);
                    FillDistrict(data[0].Organization_Region);
                    FillTehsil(data[0].Organization_District);

                    $("#txtType").val(data[0].Organization_Type);
                    $("#Organization_Type_old").val(data[0].Organization_Type);

                    $("#txtName1").val(data[0].Organization_Name);
                    $("#txtRegno").val(data[0].Organization_RegistrationNo);
                    $("#txtEstdate").val(data[0].Organization_FoundedDate);

                    $("#" + data[0].Organization_AreaType).prop("checked", "true");

                    if (data[0].Organization_AreaType === "Rural") {
                      //  $("#Urban").attr("disabled", "disabled").attr("title", "You cannot change the area type from Rural to Urban");
                        $("#ruralArea").css("display", "block");
                        $("#urbanArea").css("display", "hide");
                        $("#ddlPanchayat").attr('data-parsley-required', 'true');
                        $("#ddlGramPanchayat").attr('data-parsley-required', 'true');
                        $("#ddlVillage").attr('data-parsley-required', 'true');
                        $("#ddlMunicipalType").attr('data-parsley-required', 'false');
                        $("#ddlDistrict").attr('data-parsley-required', 'false');
                        $("#ddlWardno").attr('data-parsley-required', 'false');
                        FillPanchayat(data[0].Organization_District);
                        FillGramPanchayat(data[0].Organization_Panchayat);
                        FillVillage(data[0].Organization_Gram);
                    }

                    if (data[0].Organization_AreaType === "Urban") {
                        $("#urbanArea").css("display", "block");
                        $("#ruralArea").css("display", "hide");
                        $("#ddlMunicipalType").attr('data-parsley-required', 'true');
                        $("#ddlDistrict").attr('data-parsley-required', 'true');
                        $("#ddlWardno").attr('data-parsley-required', 'true');
                        $("#ddlPanchayat").attr('data-parsley-required', 'false');
                        $("#ddlGramPanchayat").attr('data-parsley-required', 'false');
                        $("#ddlVillage").attr('data-parsley-required', 'false');
                        FillMunicipalType();
                        FillMunicipalName(data[0].Organization_District);
                        FILLWardno(data[0].Organization_Municipality_Raj_Code);
                    }

                    $("#" + data[0].Organization_OwnershipType).prop("checked", "true");

                    if (data[0].Organization_OwnershipType === "Own") {
                        $("#onRent").css("display", "none");
                        $("#onOwn").css("display", "block");
                        $("#fld_rentAgreement").attr('data-parsley-required', 'false');
                        $("#fld_utilityBill").attr('data-parsley-required', 'false');
                        $("#fld_ownershipDocument").attr('data-parsley-required', 'true');
                    }

                    if (data[0].Organization_OwnershipType === "Rent") {
                        $("#onRent").css("display", "block");
                        $("#onOwn").css("display", "none");
                        $("#fld_rentAgreement").attr('data-parsley-required', 'true');
                        $("#fld_utilityBill").attr('data-parsley-required', 'true');
                        $("#fld_ownershipDocument").attr('data-parsley-required', 'false');
                        $("#owner").css("display", "block");
                    }

                }, 500);

                setTimeout(function () {
                    $("#ddlCountry").val(data[0].Organization_Country);
                    $("#country_name").val(data[0].Organization_Country_Name);

                    $("#ddlState").val(data[0].Organization_State);
                    $("#state_name").val(data[0].Organization_State_Name);

                    $("#ddlRegion").val(data[0].Organization_Region);
                    $("#region_name").val(data[0].Organization_Region_Name);

                    $("#ddlDistrict").val(data[0].Organization_District);
                    $("#district_name").val(data[0].Organization_District_Name);

                    $("#ddlTehsil").val(data[0].Organization_Tehsil);
                    $("#ddlTehsilNew").val(data[0].Organization_Tehsil);
                    $("#tehsil_name").val(data[0].Organization_Tehsil_Name);

                    $("#Organization_AreaType_old").val(data[0].Organization_AreaType);
                    $("#Organization_OwnershipType_old").val(data[0].Organization_OwnershipType);

                    $("#ddlPanchayat").val(data[0].Organization_Panchayat);
                    $("#Organization_Panchayat_old").val(data[0].Organization_Panchayat);

                    $("#ddlGramPanchayat").val(data[0].Organization_Gram);
                    $("#Organization_Gram_old").val(data[0].Organization_Gram);

                    $("#ddlVillage").val(data[0].Organization_Village);
                    $("#Organization_Village_old").val(data[0].Organization_Village);

                    $("#ddlMunicipalType").val(data[0].Organization_Municipality_Type);
                    $("#Organization_Municipality_Type_old").val(data[0].Organization_Municipality_Type);

                    $("#ddlMunicipalName").val(data[0].Organization_Municipality_Raj_Code);
                    $("#Organization_Municipal_old").val(data[0].Organization_Municipality_Raj_Code);

                    $("#ddlWardno").val(data[0].Organization_Ward_Raj_Code);
                    $("#Organization_WardNo_old").val(data[0].Organization_Ward_Raj_Code);
                }, 1000);

                setTimeout(function () {
                    $('#loading').modal('hide');
                    $('#showdata').css('display', 'block');
                }, 2000);
            }
        });

        $('#updateOrgDetails').parsley();
    }


    function FillOrgType() {
        $.ajax({
            type: "post",
            url: "common/cfOrgTypeMaster.php",
            data: "action=FILL",
            success: function (data) {
                $("#txtType").html(data);
            }
        });
    }

    FillOrgType();

    $("#txtType").change(function () {
        $.ajax({
            url: 'common/cfOrgDetail.php',
            type: "post",
            data: "action=FILL",
            success: function (data) {
                $('#txtDoctype').html(data);
            }
        });
    });

    $("#ddlCountry").change(function () {
        var selcountry = $(this).val();
        $.ajax({
            url: 'common/cfStateMaster.php',
            type: "post",
            data: "action=FILL&values=" + selcountry + "",
            success: function (data) {
                $('#ddlState').html(data);
            }
        });
    });
    $("#ddlState").change(function () {
        var selState = $(this).val();
        $.ajax({
            url: 'common/cfRegionMaster.php',
            type: "post",
            data: "action=FILL&values=" + selState + "",
            success: function (data) {
                $('#ddlRegion').html(data);
            }
        });
    });
    $("#ddlRegion").change(function () {
        var selregion = $(this).val();
        $.ajax({
            url: 'common/cfDistrictMaster.php',
            type: "post",
            data: "action=FILL&values=" + selregion + "",
            success: function (data) {
                $('#ddlDistrict').html(data);
            }
        });
    });
    $("#ddlDistrict").change(function () {
        var selDistrict = $(this).val();
        $.ajax({
            url: 'common/cfTehsilMaster.php',
            type: "post",
            data: "action=FILL&values=" + selDistrict + "",
            success: function (data) {
                $('#ddlTehsil').html(data);
            }
        });
    });
    
    $("#Rent").change(function () {

            $("#owner").show();
        });
        
        $("#Own").change(function () {

            $("#owner").hide();
        });

    function resetForm(formid) {
        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
    }

});