$("#form").validate({
        rules: {
            txtLCode: { required: true },	txtLCorrectName: { required: true },	txtFCorrectName: { required: true },	ddlApplication: { required: true },
			txtMarks: { required: true },	txtEmail: { required: true, email: true },	 	
			txtMobile: { required: true, minlength:10, maxlength: 10}
        },			
        messages: {				
			txtLCode: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your learner code</span>' },
			txtLCorrectName: 		{ required: '<span style="color:red; font-size: 12px;">Please enter your name</span>' },
			txtFCorrectName:		{ required: '<span style="color:red; font-size: 12px;">Please enter your Father name</span>' },
			ddlApplication: 		{ required: '<span style="color:red; font-size: 12px;">Please select your application for</span>' },
			
			txtMarks: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your marks</span>' },
			txtEmail: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your email</span>', 
										email: '<span style="color:red; font-size: 12px;">Please enter a VALID email</span>' },			
			txtMobile: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your mobile no.</span>',
										minlength:'<span style="color:red;">Please enter at least 10 digits</span>',
										maxlength:'<span style="color:red;">Please enter not more than 10 digits</span>'}
			
        },
	});