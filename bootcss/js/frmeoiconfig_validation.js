$("#frmEoiConfig").validate({
        rules: {
                        txtEoiName: { required: true },	ddlCourse: { required: true },	txtstartdate: { required: true },		
                        txtenddate: { required: true }, txtPFees: { required: true },
                        txtCFees: { required: true }
        },			
        messages: {				
			txtEoiName: { required: '<span style="color:red; font-size: 12px;">Please Enter EOI Name</span>' },			
			ddlCourse: { required: '<span style="color:red; font-size: 12px;">Please Select your Course</span>' },			
			txtstartdate: { required: '<span style="color:red; font-size: 12px;">Please Select start date</span>' },
			txtenddate: { required: '<span style="color:red; font-size: 12px;">Please Select end date</span>' },
			txtPFees: { required: '<span style="color:red; font-size: 12px;">Please Enter Processing Fees</span>' },
			txtCFees: { required: '<span style="color:red; font-size: 12px;">Please Enter Course Fees</span>' }
        }
	});