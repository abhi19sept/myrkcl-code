$("#frmWardMaster").validate({
        rules: {
            txtWardName: { required: true },ddlMunicipality: { required: true },ddlStatus: { required: true }
        },			
        messages: {				
			txtWardName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Ward Name</span>' },
			ddlMunicipality: 			{ required: '<span style="color:red; font-size: 12px;">Please select District</span>' },
			ddlStatus: 				{ required: '<span style="color:red; font-size: 12px;">Please select status</span>' }
			
        },
	});