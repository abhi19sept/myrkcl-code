$("#frmTehsilMaster").validate({
        rules: {
            txtTehsilName: { required: true },	ddlDistrict: { required: true },ddlStatus: { required: true }
        },			
        messages: {				
			txtTehsilName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Tehsil Name</span>' },
			ddlDistrict: 			{ required: '<span style="color:red; font-size: 12px;">Please enter District</span>' },
			ddlStatus: 				{ required: '<span style="color:red; font-size: 12px;">Please enter status</span>' }
			
        },
	});