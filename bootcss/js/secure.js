document.onkeydown = function (e) {

    if (e.ctrlKey &&
        (e.keyCode === 65 ||
            e.keyCode === 66 ||
            e.keyCode === 67 ||
            e.keyCode === 68 ||
            e.keyCode === 70 ||
            e.keyCode === 80 ||
            e.keyCode === 82 ||
            e.keyCode === 83 ||
            e.keyCode === 87 ||
            e.keyCode === 88 ||
            e.keyCode === 89 ||
            e.keyCode === 90 ||
            e.keyCode === 86 ||
            e.keyCode === 85 ||
            e.keyCode === 117)) {
        return false;
    }
};

$('body').bind('copy paste', function (e) {
    e.preventDefault();
});

document.addEventListener('contextmenu', function (e) {
    e.preventDefault();
});

$(document).keyup(function (evtobj) {
    if (!(evtobj.altKey || evtobj.ctrlKey || evtobj.shiftKey)) {
        if (evtobj.keyCode == 16) {
            return false;
        }
        if (evtobj.keyCode == 17) {
            return false;
        }
    }
});


function disableF5(e) {
    if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault();
}


window.onload = window.history.forward();
window.onpageshow = function (evt) {
    if (evt.persisted) {
        window.history.forward();
    }
}


$(document).on("keydown", disableF5);