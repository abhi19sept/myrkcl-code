$("#frmInnaugrationdetails").validate({
        rules: {
            txtName: { required: true },
			txtdesig: { required: true },	
			txtarea: { required: true },	
			txtenddate: { required: true },
			
			ddlCourse: { required: true },
			ddlBatch: { required: true },
			
			image1: { required: true },
			image2: { required: true }
			
						
        },			
        messages: {				
			txtName: { required: '<span style="color:red; font-size: 12px;">Please Enter Name of Public Representative</span>' },
			txtdesig: { required: '<span style="color:red; font-size: 12px;">Please Enter Designation</span>' },
			txtarea: { required: '<span style="color:red; font-size: 12px;">Please Enter Area Details of Public Representative</span>' },
			txtenddate: { required: '<span style="color:red; font-size: 12px;">Please Enter Inauguration  / Closure Date</span>' },
			ddlCourse: { required: '<span style="color:red; font-size: 12px;">Please select Course</span>' },
			ddlBatch: { required: '<span style="color:red; font-size: 12px;">Please select Batch</span>' },
			
			image1: { required: '<span style="color:red; font-size: 12px;">Please Enter Document  Image</span>' },
			image2: { required: '<span style="color:red; font-size: 12px;">Please Enter Document  Image</span>' }
			
			
        },
	});