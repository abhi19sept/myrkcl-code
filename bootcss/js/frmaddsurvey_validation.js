$("#frmaddsurvey").validate({
        rules: {
                        txtSurvey: { required: true },	txtStartdate: { required: true },
						txtEnddate: { required: true }
                    
        },			
        messages: {				
			txtSurvey: { required: '<span style="color:red; font-size: 12px;">Please Enter Survey Name</span>' },
			txtStartdate: { required: '<span style="color:red; font-size: 12px;">Please Enter Survey Start Date</span>' },
			txtEnddate: { required: '<span style="color:red; font-size: 12px;">Please Enter Survey End Date</span>' }
			
        }
	});