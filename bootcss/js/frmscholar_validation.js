$("#form").validate({
        rules: {
            txtLCode: { required: true },	txtLName: { required: true },	txtFName: { required: true },	txtMName: { required: true },
			txtLAddress: { required: true }, txtMobile : { required: true, minlength:10, maxlength: 10},	txtEmail: { required: true, email: true },	 	
			txtBatch: { required: true },	 ddlempdistrict: { required: true },	ddlemptehsil: { required: true },		txtPin: { required: true },
			ddlqualification: { required: true },	txtRollNo: { required: true }			
        },			
        messages: {				
			txtLCode: { required: '<span style="color:red; font-size: 12px;">Please enter your learner code</span>' },
			txtLName: { required: '<span style="color:red; font-size: 12px;">Please enter your name</span>' },
			txtFName: { required: '<span style="color:red; font-size: 12px;">Please enter your Father name</span>' },
			txtMName: { required: '<span style="color:red; font-size: 12px;">Please enter your Mother name</span>' },
			txtLAddress: { required: '<span style="color:red; font-size: 12px;">Please enter your address</span>' },
			txtMobile: { required: '<span style="color:red; font-size: 12px;">Please enter your mobile no.</span>',
							minlength:'<span style="color:red;">Please enter at least 10 digits</span>',
							maxlength:'<span style="color:red;">Please enter not more than 10 digits</span>'},
			txtEmail: { required: '<span style="color:red; font-size: 12px;">Please enter your email</span>', 
						email: '<span style="color:red; font-size: 12px;">Please enter a VALID email</span>' },
			txtBatch: { required: '<span style="color:red; font-size: 12px;">Please enter your Batch</span>' },
			ddlempdistrict: { required: '<span style="color:red; font-size: 12px;">Please select your District</span>' },
			ddlemptehsil: { required: '<span style="color:red; font-size: 12px;">Please select your Tehsil</span>' },
			txtPin: { required: '<span style="color:red; font-size: 12px;">Please enter your Pin No.</span>' },
			ddlqualification: { required: '<span style="color:red; font-size: 12px;">Please select your qualification</span>' },
			txtRollNo: { required: '<span style="color:red; font-size: 12px;">Please enter your Roll No.</span>' }	
        },
	});