$("#frmitgkdetails").validate({
    rules: {
        itgkCode: { required: true },
        columnForUpdate: {required: true}

    },
    messages: {
        itgkCode: { required: '<span style="color:red; font-size: 12px;">Please Enter ITGK Code</span>' },
        columnForUpdate: { required: '<span style="color:red; font-size: 12px;">Please Column to be update</span>' }
    }
});