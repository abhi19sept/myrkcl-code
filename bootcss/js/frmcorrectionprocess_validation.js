$("#form").validate({
        rules: {
            ddlstatus: { required: true },	ddlLot: { required: true }
        },			
        messages: {				
			ddlstatus: 				{ required: '<span style="color:red; font-size: 12px;">Please select Process Status</span>' },
			ddlLot:		 			{ required: '<span style="color:red; font-size: 12px;">Please select Lot No.</span>' }
        },
	});