$("#frmownershipchangerequest").validate({
        rules: {
            txtName1: { required: true },
                        ddlOrgType: { required: true },
			txtEmail: { required: true }, 
			txtMobile: { required: true },
                        
			txtRegno: { required: true },	
			txtEstdate: { required: true },	
			txtCourse: { required: true },
			txtType: { required: true }, 
			txtDoctype: { required: true },
                        txtPanNo: { required: true },
                        txtAADHARNo: { required: true },
                        
			txtIfscCode: { required: true },	
			txtaccountName: { required: true },	
			txtaccountNumber: { required: true },
			ddlBankName: { required: true }, 
			ddlBranch: { required: true },
			txtMicrcode: { required: true },	
                        txtpanno1: { required: true },	
			txtpanname: { required: true },
			ddlidproof: { required: true }, 
                        
			txtStreet: { required: true },
			area: { required: true },
			txtRoad: { required: true },
			txtLandmark: { required: true }
			
						
        },			
        messages: {	
                        ddlOrgType: { required: '<span style="color:red; font-size: 12px;">Please select organisation type</span>' },
			txtEmail: { required: '<span style="color:red; font-size: 12px;">Please enter Email Id</span>' },
			txtMobile: { required: '<span style="color:red; font-size: 12px;">Please enter Mobile No</span>' },
			txtName1: { required: '<span style="color:red; font-size: 12px;">Please enter organisation Name</span>' },
			txtRegno: { required: '<span style="color:red; font-size: 12px;">Please enter Registration no</span>' },
			txtEstdate: { required: '<span style="color:red; font-size: 12px;">Please enter Date</span>' },
			txtCourse: { required: '<span style="color:red; font-size: 12px;">Please enter course name</span>' },
			txtType: { required: '<span style="color:red; font-size: 12px;">Please enter type</span>' },
			txtDoctype: { required: '<span style="color:red; font-size: 12px;">Please enter doc type</span>' },
                        txtPanNo: { required: '<span style="color:red; font-size: 12px;">Please enter PAN Card Number</span>' },
                        txtAADHARNo: { required: '<span style="color:red; font-size: 12px;">Please enter AADHAR Card Number</span>' },
                        
			txtIfscCode: { required: '<span style="color:red; font-size: 12px;">Please enter IFSC Code</span>' },
			txtaccountName: { required: '<span style="color:red; font-size: 12px;">Please enter Account Holder Name</span>' },
			txtaccountNumber: { required: '<span style="color:red; font-size: 12px;">Please enter Account Number</span>' },
			ddlBankName: { required: '<span style="color:red; font-size: 12px;">Please enter Bank Name</span>' },
			ddlBranch: { required: '<span style="color:red; font-size: 12px;">Please enter Branch Name</span>' },
			txtMicrcode: { required: '<span style="color:red; font-size: 12px;">Please enter MICR Code</span>' },
                        txtpanno1: { required: '<span style="color:red; font-size: 12px;">Please enter Pan Card No</span>' },
			txtpanname: { required: '<span style="color:red; font-size: 12px;">Please enter Pan Card Holder Name</span>' },
			ddlidproof: { required: '<span style="color:red; font-size: 12px;">Please enter Select Bank Account ID Proof</span>' },
                        
			txtStreet: { required: '<span style="color:red; font-size: 12px;">Please enter street no</span>' },
			txtRoad: { required: '<span style="color:red; font-size: 12px;">Please enter road</span>' },
                        area: { required: '<span style="color:red; font-size: 12px;">Please Select Area Type</span>' },
			txtLandmark: { required: '<span style="color:red; font-size: 12px;">Please enter landmark</span>' }
			
			
        },
	});