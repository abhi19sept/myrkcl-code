$("#frmdistrictmaster").validate({
        rules: {
            txtDistrictName: { required: true },
			ddlCountry: { required: true },	
			ddlState: { required: true },
			ddlRegion: { required: true },
			ddlStatus: { required: true }
        },			
        messages: {				
			txtDistrictName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Distrct  Name</span>' },
			ddlCountry: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Country Name</span>' },
			ddlState: 				{ required: '<span style="color:red; font-size: 12px;">Please enter state name</span>' },
			ddlRegion: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Region</span>' },
			ddlStatus: 				{ required: '<span style="color:red; font-size: 12px;">Please enter status</span>' }
			
        },
	});