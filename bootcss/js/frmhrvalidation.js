$("#frmHRDetail").validate({
        rules: {
                        ddlDesignation: { required: true },	
						txtName: { required: true },	
						usergender1: { required: true },	
						txtDob: { required: true },	
						txtMobile: { required: true },
						txtEmail: { required: true , email: true },
						ddlQualification1: { required: true },
						txtExperience: { required: true },
						txtQualification2: { required: true },
						txtQualification3: { required: true }
							
                    
        },			
        messages: {				
			ddlDesignation: { required: '<span style="color:red; font-size: 12px;">Please Select Designation</span>' },
			txtName: { required: '<span style="color:red; font-size: 12px;">Please Enter Name</span>' },
			usergender1: { required: '<span style="color:red; font-size: 12px;">Please Selcet Dob</span>' },
			txtDob: { required: '<span style="color:red; font-size: 12px;">Please Enter DOB</span>' },
			txtMobile: { required: '<span style="color:red; font-size: 12px;">Please Enter Mobile</span>' },
			txtEmail: { required: '<span style="color:red; font-size: 12px;">Please Enter Email</span>' },
			ddlQualification1: { required: '<span style="color:red; font-size: 12px;">Please select Qualification</span>' },
			txtExperience: { required: '<span style="color:red; font-size: 12px;">Please Enter Experience</span>' },
			txtQualification2: { required: '<span style="color:red; font-size: 12px;">Please Enter Qualification</span>' },
			txtQualification3: { required: '<span style="color:red; font-size: 12px;">Please Enter Qualification</span>' }
			
        }
	});