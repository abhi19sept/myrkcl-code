$("#frmpremisesdetail").validate({
        rules: {
            receptionYesNo: { required: true },	
 txtreceptionSeatingCapacity : { required: true },	
 txtreceptionAreaInSqFt: { required: true },	
 txtreceptionDetails: { required: true },	
 parkingYesNo : { required: true },	
 parkingType: { required: true },	
 parkingTW: { required: true },	
 txttwCapacity : { required: true },	
 txtfwCapacity: { required: true },	
 toiletYesNo: { required: true },	
 toiletOwnershipType : { required: true },

toiletType: { required: true },	
 pantryYesNo : { required: true },	
 txtpantryDetails: { required: true },	
 libraryYesNo: { required: true },	
 txtlibraryDetails  : { required: true },
staffRoomYesNo  : { required: true } ,
txtstaffRoomDetails  : { required: true } 
        },			
        messages: {				
			receptionYesNo: { required: '<span style="color:red; font-size: 12px;">Please Enter Reception Availability</span>' },
			txtreceptionSeatingCapacity: { required: '<span style="color:red; font-size: 12px;">Please Enter Reception Seating Capacity</span>' },
			
			txtreceptionAreaInSqFt: { required: '<span style="color:red; font-size: 12px;">Please Enter Area in squre fit.</span>' },
			txtreceptionDetails: { required: '<span style="color:red; font-size: 12px;">Please Enter Reception Details </span>' },
			parkingYesNo: { required: '<span style="color:red; font-size: 12px;">Please Enter Parking yes or no</span>' },
			
			parkingType: { required: '<span style="color:red; font-size: 12px;">Please Enter Parking type</span>' },
			parkingTW: { required: '<span style="color:red; font-size: 12px;">Please Enter for Two Wheeler</span>' },
			txttwCapacity: { required: '<span style="color:red; font-size: 12px;">Please Enter Two Wheeler capacity</span>' },
			
			txtfwCapacity: { required: '<span style="color:red; font-size: 12px;">Please Enter Four Wheeler capacity</span>' },
			toiletYesNo: { required: '<span style="color:red; font-size: 12px;">Please Enter yes/no</span>' },
			toiletOwnershipType: { required: '<span style="color:red; font-size: 12px;">Please Enter Ownership type</span>' },
			
			toiletType: { required: '<span style="color:red; font-size: 12px;">Please Enter Toilet type</span>' },
			pantryYesNo: { required: '<span style="color:red; font-size: 12px;">Please Enter penetry yes/no.</span>' },
			txtpantryDetails: { required: '<span style="color:red; font-size: 12px;">Please Enter penetry Capacity</span>' },
			staffRoomYesNo: { required: '<span style="color:red; font-size: 12px;">Please Enter staff room yes/no</span>' },
			
			txtlibraryDetails: { required: '<span style="color:red; font-size: 12px;">Please Enter library Capacity</span>' },
			staffRoomYesNo: { required: '<span style="color:red; font-size: 12px;">Please Enter staff room yes/no</span>' },
			
			txtstaffRoomDetails: { required: '<span style="color:red; font-size: 12px;">Please Enter staff room Capacity.</span>' }
        },
	});