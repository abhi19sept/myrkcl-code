$("#frmlogin").validate({
    rules: {
        txtUserName: {required: true, minlength: 4},
        txtPassword: {required: true, minlength: 8}        
    },
    messages: {
        txtUserName: {required: '<span style="color:red; font-size: 12px;"><i class="fa fa-arrow-up"></i>&nbsp;Please enter UserName</span>'},
        txtPassword: {required: '<span style="color:red; font-size: 12px;"><i class="fa fa-arrow-up"></i>&nbsp;Please enter Password.</span>'},
      },
    
});

//txtPanNo: { required: true },