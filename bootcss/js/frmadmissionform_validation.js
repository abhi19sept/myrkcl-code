$("#frmAdmissionForm").validate({
        rules: {
                        txtlname: { required: true },	txtfname: { required: true },		dob: { required: true, minlength:10, maxlength: 10 },		
                        ddlmotherTongue: { required: true }, txtmobile: { required: true, minlength:10, maxlength: 10 },                        
                        txtAddress: { required: true },	gender: { required: true }, mstatus: { required: true }, Medium: { required: true },	
			PH: { required: true },	ddlidproof: { required: true },	
			ddlDistrict: { required: true }, ddlTehsil: { required: true },	txtPIN: { required: true, minlength:6, maxlength: 6 },	
			ddlQualification: { required: true },  ddlLearnerType: { required: true }, txtGPFno: { required: true }	, txtUploadScanDoc: { required: true },
			p1: { required: true }, p2: { required: true }
        },			
        messages: {				
			txtlname: { required: '<span style="color:red; font-size: 12px;">Please enter Learner Name</span>' },
			txtfname: { required: '<span style="color:red; font-size: 12px;">Please enter Father Name</span>' },
			dob: { required: '<span style="color:red; font-size: 12px;">Please Select Learner DOB</span>',
                                            minlength:'<span style="color:red;">Incorrect Date</span>',
                                            maxlength:'<span style="color:red;">Incorrect Date</span>'},
			ddlmotherTongue: { required: '<span style="color:red; font-size: 12px;">Please select your Mother Tongue</span>' },
			txtmobile: { required: '<span style="color:red; font-size: 12px;">Please enter your mobile no.</span>',
							minlength:'<span style="color:red;">Please enter at least 10 digits</span>',
							maxlength:'<span style="color:red;">Please enter not more than 10 digits</span>'},                        
			txtAddress: { required: '<span style="color:red; font-size: 12px;">Please enter address</span>' },
			gender: { required: '<span style="color:red; font-size: 12px;">Please select Gender</span>' },
			mstatus: { required: '<span style="color:red; font-size: 12px;">Please select Marital Status</span>' },
			Medium: { required: '<span style="color:red; font-size: 12px;">Please select Medium of Study</span>' },
			PH: { required: '<span style="color:red; font-size: 12px;">Please select Physical Status</span>' },
			ddlidproof: { required: '<span style="color:red; font-size: 12px;">Please select ID Proof</span>' },			
			ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Please select district</span>' },
			ddlTehsil: { required: '<span style="color:red; font-size: 12px;">Please select Tehsil</span>' },
			txtPIN: { required: '<span style="color:red; font-size: 12px;">Please enter PIN</span>',
						minlength:'<span style="color:red;">Pin Code must be 6 digits</span>',
							maxlength:'<span style="color:red;">Pin Code must be 6 digits</span>'},
			ddlQualification: { required: '<span style="color:red; font-size: 12px;">Please select Qualification</span>' },
			ddlLearnerType: { required: '<span style="color:red; font-size: 12px;">Please select Learner Type</span>' },
			txtGPFno: { required: '<span style="color:red; font-size: 12px;">Please enter your GPF No.</span>' },
			txtUploadScanDoc: { required: '<span style="color:red; font-size: 12px;">Please attach your scan form</span>' },
			p1: 				{ required: '<span style="color:red; font-size: 12px;">Please upload your photo.</span>' },
			p2: 				{ required: '<span style="color:red; font-size: 12px;">Please upload your sign.</span>' }
        }
	});