$("#Aologinform").validate({
        rules: {
                        txtmobile: { required: true, minlength:10, maxlength: 10 },	txtemailid: { required: true }		
                    
        },			
        messages: {				
			txtmobile: { required: '<span style="color:red; font-size: 12px;">Please enter Mobile</span>',
							minlength:'<span style="color:red;">Please enter at least 10 digits</span>',
							maxlength:'<span style="color:red;">Please enter not more than 10 digits</span>' },
			txtemailid: { required: '<span style="color:red; font-size: 12px;">Please enter email</span>' }
			
        }
	});