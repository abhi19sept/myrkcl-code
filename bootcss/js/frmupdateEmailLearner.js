var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    }, decode: function (e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r)
            }
            if (a != 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    }, _utf8_encode: function (e) {
        e = e.replace(/rn/g, "n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    }, _utf8_decode: function (e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t
    }
}

function encryptString(str) {
    var encryptedString = '';
    encryptedString = Base64.encode(str);
    return encryptedString;
}

function decryptString(str) {
    var decryptString;
    decryptString = Base64.decode(str);
    return decryptString;
}

var reference_number = '';
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    fillForm();

    function fillForm() {
        $('#loading').modal('show');
        $.ajax({
            type: "post",
            url: "common/cflearnerdetails.php",
            data: "action=getLearnerEmailMobile",
            success: function (data) {

                $('#updateLearnerDetails').parsley().destroy();

                data = $.parseJSON(data);

                setTimeout(function () {
                    $("#Admission_Email").val(data.Admission_Email);
                    $('#loading').modal('hide');
                    $('#showdata').css('display', 'block');
                }, 500);
            }
        });

        $('#updateLearnerDetails').parsley();
    }

    $("#updateLearnerDetails").on("submit", function (e) {

        $("#success_email, #error_email").css("display", "none");
        $("#success_email, #success_email").html("");

        e.preventDefault();
        var form = $(this);
        form.parsley().validate();
        $("#submitting").modal("show");

        if (form.parsley().isValid()) {
            var url = "common/cflearnerdetails.php";
            var data;
            data = new FormData(this);

            var emailID = $("#Admission_Email").val();

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data === "1" || data === "Successfully Inserted") {
                        $("#submitting").modal("hide");
                        $("#success_email").css("display", "block").html("A verification email has been sent to <b>"+emailID+"</b>");
                        $("#error_email").css("display", "none").html("");
                    } else {
                        $("#submitting").modal("hide");
                        $("#success_email").css("display", "none").html("");
                        $("#error_email").css("display", "block").html("Something went wrong, please try again");
                    }
                }
            });
        } else {
            $("#submitting").modal("hide");
        }
        return false;
    });

    function resetForm(formid) {
        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
    }

});