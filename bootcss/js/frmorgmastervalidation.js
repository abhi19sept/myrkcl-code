$("#frmOrgDeatil").validate({
        rules: {
            txtName1: { required: true },
			txtRegno: { required: true },	
			txtEstdate: { required: true },	
			txtType: { required: true },
			orgdocproof: { required: true }, 
			ddlCountry: { required: true },
			ddlState: { required: true },	
			ddlRegion: { required: true },	
			ddlDistrict: { required: true },
			ddlTehsil: { required: true }, 
			//ddlArea: { required: true },
			txtHouseno: { required: true },	
			txtStreet: { required: true },
			txtLandmark: { required: true }, 
			txtAddress: { required: true },
			ddlDobProof: { required: true },
			txtRoad: { required: true }
						
        },			
        messages: {				
			txtName1: { required: '<span style="color:red; font-size: 12px;">Please Enter organisation Name</span>' },
			txtRegno: { required: '<span style="color:red; font-size: 12px;">Please Enter Registration no</span>' },
			txtEstdate: { required: '<span style="color:red; font-size: 12px;">Please Enter Date</span>' },
			txtCourse: { required: '<span style="color:red; font-size: 12px;">Please Enter course name</span>' },
			txtType: { required: '<span style="color:red; font-size: 12px;">Please Enter type</span>' },
			txtDoctype: { required: '<span style="color:red; font-size: 12px;">Please Enter Document  Image</span>' },
			orgdocproof: { required: '<span style="color:red; font-size: 12px;">Please Enter doc proof</span>' },
			ddlCountry: { required: '<span style="color:red; font-size: 12px;">Please Enter country Name</span>' },
			ddlState: { required: '<span style="color:red; font-size: 12px;">Please Enter your state</span>' },
			ddlRegion: { required: '<span style="color:red; font-size: 12px;">Please Enter Region</span>' },
			ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Please Enter district</span>' },
			ddlTehsil: { required: '<span style="color:red; font-size: 12px;">Please Enter tehsil</span>' },
			//ddlArea: { required: '<span style="color:red; font-size: 12px;">Please Enter Area Name</span>' },
			txtHouseno: { required: '<span style="color:red; font-size: 12px;">Please Enter House no</span>' },
			txtStreet: { required: '<span style="color:red; font-size: 12px;">Please Enter street no</span>' },
			txtRoad: { required: '<span style="color:red; font-size: 12px;">Please Enter road</span>' },
			txtLandmark: { required: '<span style="color:red; font-size: 12px;">Please Enter landmark</span>' },
			txtAddress: { required: '<span style="color:red; font-size: 12px;">Please Enter Address</span>' },
			ddlDobProof: { required: '<span style="color:red; font-size: 12px;">Please select Dob Proof</span>' },
			txtRoad: { required: '<span style="color:red; font-size: 12px;">Please Enter Road Detials</span>' },
			
        },
	});