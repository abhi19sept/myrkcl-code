$("#payuForm").validate({

        rules: {
        	udf1: { required: true },
        	firstname: { required: true },
        	phone: { required: true },
        	email: { required: true },
        	productinfo: { required: true },
        	amount: { required: true },
        	udf2: { required: true },

			creditnum: { required: true },
			creditmonth: { required: true },
			credityear: { required: true },			
			creditcvv: { required: true },			
			creditname: { required: true },
			
			debitnum: { required: true },
			debitmonth: { required: true },
			debityear: { required: true },			
			debitcvv: { required: true },			
			debitname: { required: true },
			
			ddlbankname: { required: true }
			
			
			
        },			
        messages: {				
        	udf1: { required: '<span style="color:red; font-size: 12px;">Center code is required.</span>' },
			firstname: { required: '<span style="color:red; font-size: 12px;">Owner name is required.</span>' },
        	phone: { required: '<span style="color:red; font-size: 12px;">Mobile number is required.</span>' },
        	email: { required: '<span style="color:red; font-size: 12px;">Email is required.</span>' },
        	productinfo: { required: '<span style="color:red; font-size: 12px;">Payment type is required.</span>' },
        	amount: { required: '<span style="color:red; font-size: 12px;">Amount is required.</span>' },
        	udf2: { required: '<span style="color:red; font-size: 12px;">Ref. no. is required.</span>' },

			creditnum: { required: '<span style="color:red; font-size: 12px;">Please Enter Credit Card Number</span>' },
			creditmonth: { required: '<span style="color:red; font-size: 12px;">Please Select Month</span>' },
			credityear: { required: '<span style="color:red; font-size: 12px;">Please Select Year</span>' },
			creditcvv: { required: '<span style="color:red; font-size: 12px;">Please Enter CVV Number</span>' },
			creditname: { required: '<span style="color:red; font-size: 12px;">Please Enter Card Name</span>' },
			
			debitnum: { required: '<span style="color:red; font-size: 12px;">Please Enter Debit Card Number</span>' },
			debitmonth: { required: '<span style="color:red; font-size: 12px;">Please Select Month</span>' },
			debityear: { required: '<span style="color:red; font-size: 12px;">Please Select Year</span>' },
			debitcvv: { required: '<span style="color:red; font-size: 12px;">Please Enter CVV Number</span>' },
			debitname: { required: '<span style="color:red; font-size: 12px;">Please Enter Card Name</span>' },
			
			ddlbankname: { required: '<span style="color:red; font-size: 12px;">Please Select Bank Name</span>' }
        },

	});