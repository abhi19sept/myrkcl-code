$(document).ready(function() {
	var mapCenter = new google.maps.LatLng(26.960514208305806, 74.76696749999996); //Google map Coordinates
	var geocoder = new google.maps.Geocoder;
	var map;
	var directionsDisplay;
    var directionsService;
	
	map_initialize(); // initialize google map
	
	//############### Google Map Initialize ##############
	function map_initialize()
	{
			var googleMapOptions = 
			{ 
				center: mapCenter, // map center
				zoom: 7, //zoom level, 0 = earth view to higher value
				maxZoom: 20,
				minZoom: 5,
				zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL //zoom control size
			},
				scaleControl: true, // enable scale control
				mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
			};
		
		   	map = new google.maps.Map(document.getElementById("google_map"), googleMapOptions);		
			
			
			//Right Click to Drop a New Marker
			google.maps.event.addListener(map, 'rightclick', function(event){
				//Edit form to be displayed with new marker
				var EditForm = '<p><div class="marker-edit">'+'<form action="ajax-save.php" method="POST" name="SaveMarker" id="SaveMarker">'+			'<label for="pDesc"><span>Address :</span><textarea id="fld_address" name="pDesc" class="save-desc" placeholder="Enter Address" maxlength="150"></textarea></label>'+'</form>'+'</div></p><button name="save-marker" class="save-marker">Save Marker Details</button>';
				
				

				//Drop a new Marker with our Edit Form
				create_marker(event.latLng, 'New Marker', EditForm, true, true, true, "http://localhost/maps/icons/pin_green.png");
				
				var $address = document.getElementById("fld_address");
				
				geocoder.geocode({'location': event.latLng}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                $address.value = results[1].formatted_address;
                            } else {
                                BootstrapDialog.alert('No results found');
                            }
                        } else {
                            BootstrapDialog.alert('Geocoder failed due to: ' + status);
                        }
                    });
			});
										
	}
	
	//############### Create Marker Function ##############
	function create_marker(MapPos, MapTitle, MapDesc,  InfoOpenDefault, DragAble, Removable, iconPath)
	{	  	  		  
		//new marker
		var marker = new google.maps.Marker({
			position: MapPos,
			map: map,
			draggable:DragAble,
			animation: google.maps.Animation.DROP,
			title:"Address",
			icon: iconPath
		});
		
		//Content structure of info Window for the Markers
		var contentString = $('<div class="marker-info-win">'+'<div class="marker-inner-win"><span class="info-content"><h1 class="marker-heading">'+MapTitle+'</h1>'+MapDesc+'</span></div></div>');	

		
		//Create an infoWindow
		var infowindow = new google.maps.InfoWindow();
		//set the content of infoWindow
		infowindow.setContent(contentString[0]);

		//Find remove button in infoWindow

		var saveBtn = contentString.find('button.save-marker')[0];


		
		if(typeof saveBtn !== 'undefined') //continue only when save button is present
		{
			//add click listner to save marker button
			google.maps.event.addDomListener(saveBtn, "click", function(event) {
				var mReplace = contentString.find('span.info-content'); //html to be replaced after success
				//var mName = contentString.find('input.save-name')[0].value; //name input field value
				var mDesc  = contentString.find('textarea.save-desc')[0].value; //description input field value
				//var mType = contentString.find('select.save-type')[0].value; //type of marker
				
				if( mDesc =='')
				{
					BootstrapDialog.alert("Please enter Name and Description!");
				}else{
					save_marker(marker, mDesc, mReplace); //call save marker function
				}
			});
		}
		
		//add click listner to save marker button		 
		google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker); // click on marker opens info window 
	    });
		  
		if(InfoOpenDefault) //whether info window should be open by default
		{
		  infowindow.open(map,marker);
		}
	}
	
	//############### Remove Marker Function ##############
	function remove_marker(Marker)
	{
		
		/* determine whether marker is draggable 
		new markers are draggable and saved markers are fixed */
		if(Marker.getDraggable()) 
		{
			Marker.setMap(null); //just remove new marker
		}
		else
		{
			//Remove saved marker from DB and map using jQuery Ajax
			var mLatLang = Marker.getPosition().toUrlValue(); //get marker position
			var myData = {del : 'true', latlang : mLatLang}; //post variables
			$.ajax({
				type: "POST",
				url: "map_process.php",
				data: myData,
				success:function(data){
					Marker.setMap(null); 
					BootstrapDialog.alert(data);
				},
				error:function (xhr, ajaxOptions, thrownError){
					BootstrapDialog.alert(thrownError); //throw any errors
				}
			});
		}

	}
	
	//############### Save Marker Function ##############
	function save_marker(Marker, mAddress, replaceWin)
	{
		//Save new marker using jQuery Ajax
		var mLatLang = Marker.getPosition().toUrlValue(); //get marker position
		var myData = {address : mAddress, latlang : mLatLang}; //post variables
		console.log(replaceWin);		
		$.ajax({
		  type: "POST",
		  url: "map_process.php",
		  data: myData,
		  success:function(data){
				replaceWin.html(data); //replace info window with new html
				Marker.setDraggable(true); //set marker to fixed
				Marker.setIcon('http://localhost/maps/icons/pin_blue.png'); //replace icon
            },
            error:function (xhr, ajaxOptions, thrownError){
                BootstrapDialog.alert(thrownError); //throw any errors
            }
		});
	}

	function calculateAndDisplayRoute(directionsService, directionsDisplay, start, end) {
        directionsService.route({
			origin: start,
			destination: end,
			travelMode: 'DRIVING'
        }, function(response, status) {
			if (status === 'OK') {
				directionsDisplay.setDirections(response);
			} else {
				window.alert('Directions request failed due to ' + status);
			}
        });
	}
	
	$("#frmcenter_learner").validate({
		errorPlacement: function(error, element) {
            if (element.attr("name") == "area") {
                error.insertAfter("#areaErrorMessageBox");
               // error.html($("#areaErrorMessageBox"));
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
			ddlContact: { required: true },
			ddlDistrict: { required: true },
			ddlTehsil: { required: true },
			area: { required: true },
            ddlMunicipalType: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Urban"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			},
            ddlMunicipalName: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Urban"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			},
            ddlWardno: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Urban"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			},
            ddlPanchayat: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Rural"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			},
            ddlGramPanchayat: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Rural"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			},
            ddlVillage: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Rural"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			}
			
						
        },			
        messages: {	
			ddlContact: { required: '<span style="color:red; font-size: 12px;">Please fill your contact no.</span>' },
			ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Select your District</span>' },
			ddlTehsil: { required: '<span style="color:red; font-size: 12px;">Select your Tehsil</span>' },
			area: { required: '<span style="color:red; font-size: 12px;">Select your Area Type</span>' },
			ddlMunicipalType: { required: '<span style="color:red; font-size: 12px;">Select your Municipality Type.</span>' },
			ddlMunicipalName: { required: '<span style="color:red; font-size: 12px;">Select your Municipality Name:</span>' },
			ddlWardno: { required: '<span style="color:red; font-size: 12px;">Select your Ward No.</span>' },
			ddlPanchayat: { required: '<span style="color:red; font-size: 12px;">Select your Panchayat Samiti/Block.</span>' },
			ddlGramPanchayat: { required: '<span style="color:red; font-size: 12px;">Select your Gram Panchayat.</span>' },
			ddlVillage: { required: '<span style="color:red; font-size: 12px;">Select your Village.</span>' },
        },
	});
	
	$("#btnSubmit").click(function(e){
		if($("#frmcenter_learner").valid()){
			e.preventDefault();
			$(".map-direction h2").html("");
			$(".right-panel").html("");
			var selDistrict = $("#ddlDistrict option:selected").text();
			var selTehsil = $("#ddlTehsil option:selected").text();
			var selarea = $("input[name=area]:checked").val();
			var selward = $("#ddlWardno option:selected").text();
			var samiti = $("#ddlPanchayat option:selected").text();
			
			var input_data = "action=Learnerloc&selDistrict="+selDistrict+"&selTehsil="+selTehsil+"&selarea="+selarea+"&selward="+selward+"&samiti="+samiti;
			if(selDistrict != 0){
				var googleMapOptions = 
					{ 
						center: mapCenter, // map center
						zoom: 7, //zoom level, 0 = earth view to higher value
						maxZoom: 20,
						minZoom: 2,
						zoomControlOptions: {
						style: google.maps.ZoomControlStyle.SMALL //zoom control size
					},
						scaleControl: true, // enable scale control
						mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
					};
					
				map = new google.maps.Map(document.getElementById("google_map"), googleMapOptions);	
				
				
				var bounds = new google.maps.LatLngBounds();
				
					
				$.ajax({
					url: 'common/cfCenterlocation.php',
					type: "post",
					data: input_data,
					success: function (data) {
						if(data == "fail"){
							BootstrapDialog.alert("Address not found by map.");
						}
						else{ 
							var learner_latLng = data.replace('--', ',');
							latLng_start = learner_latLng.split(',');
							start_lat = latLng_start[0];
							start_lng = latLng_start[1];
							
							$.ajax({
								url: 'common/cfCenterlocation.php',
								type: "post",
								data: "action=Nearestcenter&value="+data,
								success: function (data) {
									var marker_count = $(data).find("marker").length;
									if(marker_count > 0){
										
										var name 		= "Your Location";
										var address 	= "";
										var point 	= new google.maps.LatLng(parseFloat(start_lat),parseFloat(start_lng));
										create_marker(point, name, address, true, false, false, "images/icons/pin_green.png");
										bounds.extend(point);
										
										var chtml = "<h3>"+marker_count+" Nearest ITGK Found.</h3><table><tr><th>S.No.</th><th>ITGK Name</th><th>ITGK Address</th><th>Send location to your Mobile</th></tr>";
										
										var i = 1;
										$(data).find("marker").each(function () {
												var name 		= $(this).attr('name');
												var orgadd 		= $(this).attr('orgadd');
												var sms 		= $(this).attr('sms');
												var distance 		= $(this).attr('distance');
												var address 	= '<p>'+ $(this).attr('address') +'</p>';
												//var type 		= $(this).attr('type');
												var point 	= new google.maps.LatLng(parseFloat($(this).attr('lat')),parseFloat($(this).attr('lng')));
												create_marker(point, name, address, false, false, false, "images/icons/pin_red.png");
												

												bounds.extend(point);
												var end = $(this).attr('lat')+','+$(this).attr('lng');
												var start = learner_latLng;
												directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
												directionsService = new google.maps.DirectionsService;
												
												directionsDisplay.setMap(map);
												directionsDisplay.setPanel(document.getElementById('right-panel_'+i));
												
												$("#directions_"+i+" h2").html(name);
												calculateAndDisplayRoute(directionsService, directionsDisplay, start, end);
												chtml+= "<tr><td>"+i+".</td><td>"+name+"</td><td>"+orgadd+"</td><td><button id='nearloc_"+i+"' sms='"+sms+"' type='button' class='btn btn-primary send_locmodal' data-toggle='modal' data-target='.bs-example-modal-sm'>Send</button></td></tr>";
												i++;
												
										});
										chtml+= "</table><div class='indicates'><img src='images/icons/pin_green.png'> indicates your location.<br> <img src='images/icons/pin_red.png'> indicates ITGK location.</div>";
										$("#marker_count").html(chtml);
										
										map.fitBounds(bounds);
									}
									else{
										BootstrapDialog.alert("No Center Found.");   
									}
								}
							});
							

						}
					}
				});
			}
			else{
				BootstrapDialog.alert("Please select all required field.");
			}
		}
	});
	
	
});

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        FillStates();
		function FillStates() {
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillStates",
                success: function (data) {
                    //alert(data);
                    $('#ddlState').html(data);
					FillDistricts();
                }
            });
        }
		
		function FillDistricts() {
			var state = $('#ddlState').val();
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillDistricts&values="+state,
                success: function (data) {
					//alert(data);
                    $('#ddlDistrict').html(data);
					$("input:radio").attr("checked", false);
                }
            });
        }
		
		$("#ddlState").change(function(){
			$("input:radio").attr("checked", false);
			var state = $(this).val();
			$('#ddlDistrict_box').css('display','block');
			$.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillDistricts&values="+state,
                success: function (data) {
                    $('#ddlDistrict').html(data);
					$('#ddlTehsil').html("");
					$("input:radio").attr("checked", false);
					
					$('#ddlGramPanchayat').val('');
					$('#ddlPanchayat').val('');
					$('#ddlVillage').val('');
					$('#ddlMunicipalName').val('');
					$('#ddlMunicipalType').val('');
					$('#ddlWardno').val('');
					$("#Urban").css('display','none');
					$("#Rural").css('display','none');
                }
            });
		}); 
		$("#ddlDistrict").change(function(){
			$("input:radio").attr("checked", false);
			var district = $(this).val();
			$('#ddlTehsil_box').css('display','block');
			$.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillTehsils&values="+district,
                success: function (data) {
                    $('#ddlTehsil').html(data);
					
					
					$('#ddlGramPanchayat').val('');
					$('#ddlPanchayat').val('');
					$('#ddlVillage').val('');
					$('#ddlMunicipalName').val('');
					$('#ddlMunicipalType').val('');
					$('#ddlWardno').val('');
					$("#Urban").css('display','none');
					$("#Rural").css('display','none');
                }
            });
		}); 

        function FillPanchayat() {
            var selDistrict = ddlDistrict.value;
            //alert(selDistrict);
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FILLPanchayat&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlPanchayat').html(data);
                }
            });
        }

		function FillMunicipalName() {
            var selDistrict = ddlDistrict.value;
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillMunicipalName&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlMunicipalName').html(data);
                }
            });
        }
		
		function FillMunicipalType() {
            var selDistrict = ddlDistrict.value;
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalType",
                success: function (data) {
                    //alert(data);
                    $('#ddlMunicipalType').html(data);
                }
            });
        }
		
		 $("#ddlMunicipalName").change(function () {
            var selMunicipalName = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FILLWardno&values=" + selMunicipalName + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlWardno').html(data);
                }
            });
        });
		
		
        $("#ddlPanchayat").change(function () {
            var selPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlGramPanchayat').html(data);
                }
            });
        });
        
        $("#ddlGramPanchayat").change(function () {
            var selGramPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfVillageMaster.php',
                type: "post",
                data: "action=FILL&values=" + selGramPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlVillage').html(data);
                }
            });
        });
		
		function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }

        function findselected() {

            var UrbanDiv = document.getElementById("areaUrban");
            var category = document.getElementById("Urban");

            if (UrbanDiv) {
                Urban.style.display = areaUrban.checked ? "block" : "none";
                Rural.style.display = "none";
            }
            else
            {
                Urban.style.display = "none";
            }
        }
        function findselected1() { 
            var RuralDiv = document.getElementById("areaRural");
            var category1 = document.getElementById("Rural");

            if (RuralDiv) {
                Rural.style.display = areaRural.checked ? "block" : "none";
                Urban.style.display = "none";
            }
            else
            {
                Rural.style.display = "none";
            }
        }
		
        $("#areaUrban").change(function () {
			$('#ddlGramPanchayat').val('');
			$('#ddlPanchayat').val('');
			$('#ddlVillage').val('');


            findselected();
			FillMunicipalName();
			FillMunicipalType();
        });
        $("#areaRural").change(function () {
			$('#ddlMunicipalName').val('');
			$('#ddlMunicipalType').val('');
			$('#ddlWardno').val('');

            findselected1();
            FillPanchayat();
        });        
		$("#marker_count").on("click",".send_locmodal",function(){
			$(".send_locmodal").removeClass("active_send_locmodal");
			$(this).addClass("active_send_locmodal");
		});
		$(".sendotp_btn").click(function(){
			if($("#ddlContact").val().length == 10){
				send_locotp();
			}
			else{
				var filter = /[^0-9]/;

				if (filter.test(phoneNumber)) {
				  if(phoneNumber.length==10){
					   var validate = true;
				  } else {
					  BootstrapDialog.alert('Please put 10  digit mobile number');
					  var validate = false;
				  }
				}
				else {
				  BootstrapDialog.alert('Not a valid number');
				  var validate = false;
				}
			}
		});
		$(".modal-footer").on('click','#modal_dismiss',function(){
			$("#otpval_locmodal").val("");
			$("#ddlContactOTP").val("");
			$("#ddlContact").val("");
			$("#OTP_divblock").css("display","none");
			$(".sendotp_btn").css("display","inline");
			$(".sendsms").css("display","none");
			$("#ddlContactOTP_error").html("");
		});
		$(".modal-footer").on('click','.sendsms',function(){
			var mobileno = $("#ddlContact").val();
			//var contactotp = $("#otpval_locmodal").val();
			var filledotp = $("#ddlContactOTP").val();
			var sms = $(".active_send_locmodal").attr('sms');
			var input_data = "action=sendsms&sms="+sms+"&mobileno="+mobileno+"&filledotp="+filledotp;
			if(filledotp != ""){
				$.ajax({
					url: 'common/cfCenterlocation.php',
					type: "post",
					data: input_data,
					success: function (data) { 
						if(data == "success"){ 
							$("#modal_dismiss").click();
							BootstrapDialog.alert("ITGK Location's details with google map link sent successfully on mobile no. "+mobileno+"\n Click on link received at your mobile no. to start navigation.");
						}
						else{
							$("#ddlContactOTP_error").html("invalid OTP.");
						}
						
					}
				});
			}else{
				$("#ddlContactOTP_error").html("Please enter OTP.");
			}
		});
		
    });
	
	
	function send_locotp() {
			if($("#ddlContact").val().length == 10){
				// var otp = Math.floor(100000 + Math.random() * 900000);   
				// otp = String(otp);
				// otp = otp.substring(0,6);
				//$("#otpval_locmodal").val(otp);
				//var encodadedotp = Base64.encode(otp);
				$("#ddlContactOTP_error").html("");
				var mobileno = $("#ddlContact").val();
				//var sms = "OTP for mobile no verification is "+otp;
				var input_data = "action=sendotp&mobileno="+mobileno;
				$.ajax({
					url: 'common/cfCenterlocation.php',
					type: "post",
					data: input_data,
					success: function (data) {
						$("#OTP_divblock").css("display","block");
						$(".sendotp_btn").css("display","none");
						$(".sendsms").css("display","inline");
					}
				});
			}
		}
		
