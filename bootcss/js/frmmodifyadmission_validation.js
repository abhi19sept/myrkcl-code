$("#frmadmission").validate({
        rules: {
                        txtlname: { required: true },	txtfname: { required: true },	dob: { required: true, minlength:10, maxlength: 10 },
						gender: { required: true }, mstatus: { required: true }, Medium: { required: true }, PH: { required: true },
						txtAddress: { required: true },	txtPIN: { required: true, minlength:6, maxlength: 6 }, txtmobile: { required: true, minlength:10, maxlength: 10 }
        },			
        messages: {				
			txtlname: { required: '<span style="color:red; font-size: 12px;">Please enter Learner Name</span>' },
			txtfname: { required: '<span style="color:red; font-size: 12px;">Please enter Father Name</span>' },
			dob: { required: '<span style="color:red; font-size: 12px;">Please Select Learner DOB</span>',
                                            minlength:'<span style="color:red;">Incorrect Date</span>',
                                            maxlength:'<span style="color:red;">Incorrect Date</span>'},
			
			gender: { required: '<span style="color:red; font-size: 12px;">Please checked</span>' },
			mstatus: { required: '<span style="color:red; font-size: 12px;">Please checked</span>' },
			Medium: { required: '<span style="color:red; font-size: 12px;">Please checked</span>' },
			PH: { required: '<span style="color:red; font-size: 12px;">Please checked</span>' },
			
			txtAddress: { required: '<span style="color:red; font-size: 12px;">Please enter address</span>' },
			txtPIN: { required: '<span style="color:red; font-size: 12px;">Please enter PIN</span>',
						minlength:'<span style="color:red;">Pin Code must be 6 digits</span>',
							maxlength:'<span style="color:red;">Pin Code must be 6 digits</span>'},
			txtmobile: { required: '<span style="color:red; font-size: 12px;">Please enter your mobile no.</span>',
							minlength:'<span style="color:red;">Please enter at least 10 digits</span>',
							maxlength:'<span style="color:red;">Please enter not more than 10 digits</span>'}
        }
	});