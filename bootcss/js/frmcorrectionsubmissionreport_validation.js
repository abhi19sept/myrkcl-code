$("#frmcorrectionsubmission").validate({
        rules: {
            ddllot: { required: true },	ddlstatus: { required: true }
        },			
        messages: {				
			ddllot: 				{ required: '<span style="color:red; font-size: 12px;">Please select Lot</span>' },
			ddlstatus: 					{ required: '<span style="color:red; font-size: 12px;">Please select Status</span>' }
        },
	});