$("#form").validate({
        rules: {
            
			
                        txtReason: { required: true, minlength:50, maxlength: 300 },
			ddlReason: { required: true }
			
			
        },			
        messages: {				
			
			
                        txtReason: { required: '<span style="color:red; font-size: 12px;">Please Enter Reason Details</span>' ,
                            minlength:'<span style="color:red;">Please enter at least 50 Characters</span>',
                            maxlength:'<span style="color:red;">Please enter not more than 300 Characters</span>'},
			ddlReason: { required: '<span style="color:red; font-size: 12px;">Please Select Reason</span>' }
			
			
        },
	});