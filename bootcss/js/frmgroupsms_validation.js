$("#frmgroupsmsmodule").validate({
        rules: {
                        ddlEntity: { required: true },
						ddlRsp: { required: true },
						ddlDistrict: { required: true },
						ddlTehsil: { required: true },
						ddlCenterrsp: { required: true },
						ddlCourse: { required: true },
						ddlBatch: { required: true },
						ddllearner: { required: true },
						txtmobile: { required: true },
						txtMessage: { required: true }
                    
        },			
        messages: {				
			ddlEntity: { required: '<span style="color:red; font-size: 12px;">Please Enter Entity Name</span>' },
			ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Please Enter District Name</span>' },
			ddlTehsil: { required: '<span style="color:red; font-size: 12px;">Please Enter Tehsil Name</span>' },
			ddlCenterrsp: { required: '<span style="color:red; font-size: 12px;">Please Enter Center Name </span>' },
			ddlCourse: { required: '<span style="color:red; font-size: 12px;">Please Enter Course Name</span>' },
			ddlBatch: { required: '<span style="color:red; font-size: 12px;">Please Enter Batch Name</span>' },
			ddllearner: { required: '<span style="color:red; font-size: 12px;">Please Enter learner Name</span>' },
			txtmobile: { required: '<span style="color:red; font-size: 12px;">Please Enter mobile Name</span>' },
			txtMessage: { required: '<span style="color:red; font-size: 12px;">Please Enter Message Name</span>' }
			
			
        }
	});