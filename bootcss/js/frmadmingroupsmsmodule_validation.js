$("#frmadmingroupsmsmodule").validate({
        rules: {
                        txtentity: { required: true },
						ddlDistrict: { required: true },
						ddlTehsil: { required: true },
						ddlRsp: { required: true },
						ddlCenter: { required: true },
						ddllearner: { required: true },
						txtmobile: { required: true },
						txtMessage: { required: true }                    
        },			
        messages: {				
			ddlEntity: { required: '<span style="color:red; font-size: 12px;">Please Enter Entity Name</span>' },
			ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Please Enter District Name</span>' },
			ddlCenter: { required: '<span style="color:red; font-size: 12px;">Please Enter Tehsil Name </span>' },
			ddlCourse: { required: '<span style="color:red; font-size: 12px;">Please Enter RSP Name</span>' },
			ddlBatch: { required: '<span style="color:red; font-size: 12px;">Please Enter Center Name</span>' },
			ddllearner: { required: '<span style="color:red; font-size: 12px;">Please Enter learner Name</span>' },
			txtmobile: { required: '<span style="color:red; font-size: 12px;">Please Enter Mobile Name</span>' },
			txtMessage: { required: '<span style="color:red; font-size: 12px;">Please Enter Message Name</span>' }
			
			
        }
	});
$("#frmadmingroupsmsmodule1").validate({
    rules: {

        'usertype': { required: true },
        'txtMessagecsv': { required: true }
    },
    errorPlacement: function (error, element) {
        // error.insertAfter(element);  // default function
        error.insertBefore(element);
    },
});

$("#frmadmingroupsmsmodule3").validate({
    rules: {

        'usertype3': { required: true },
        'txtNumber': { required: true },
        'txtMessagesimple': { required: true }
    },
    errorPlacement: function (error, element) {
        // error.insertAfter(element);  // default function
        error.insertBefore(element);
    },
});

$("#frmadmingroupsmsmodule2").validate({
    rules: {

        'usertype2': { required: true },

        'txtMessagePer': { required: true }
    },
    errorPlacement: function (error, element) {
        // error.insertAfter(element);  // default function
        error.insertBefore(element);
    },
});