$("#frmbiometricenrollment").validate({
        rules: {
            ddlStatus: { required: true }, ddlCourse: { required: true }, ddlBatch: { required: true }
        },			
        messages: {			
			ddlStatus: 					{ required: '<span style="color:red; font-size: 12px;">Please select Status</span>' },
			ddlCourse: 					{ required: '<span style="color:red; font-size: 12px;">Please select Course</span>' },
			ddlBatch: 					{ required: '<span style="color:red; font-size: 12px;">Please select Batch</span>' }
        },
	});