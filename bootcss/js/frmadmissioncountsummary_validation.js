$("#frmadmissioncountsummary").validate({
        rules: {
            ddlCourse: { required: true },	ddlBatch: { required: true }
        },			
        messages: {				
			ddlCourse: 				{ required: '<span style="color:red; font-size: 12px;">Please select Course</span>' },
			ddlBatch:		 		{ required: '<span style="color:red; font-size: 12px;">Please select Batch</span>' }
        },
	});