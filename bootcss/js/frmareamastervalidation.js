$("#frmArealMaster").validate({
        rules: {
            txtAreaName: { required: true },	
			txtAreaPin: { required: true },	
			ddlCountry: { required: true },
			 ddlState: { required: true },	
			ddlRegion: { required: true },	
			ddlDistrict: { required: true },
			 ddlTehsil: { required: true },	
			ddlStatus: { required: true }
			
        },			
        messages: {				
			txtAreaName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Area Name</span>' },
			txtAreaPin: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Area Pin</span>' },
			ddlCountry: 				{ required: '<span style="color:red; font-size: 12px;">Please enter country Name</span>' },
			ddlState: 				{ required: '<span style="color:red; font-size: 12px;">Please enter State Name</span>' },
			ddlRegion: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Region Name</span>' },
			ddlDistrict: 				{ required: '<span style="color:red; font-size: 12px;">Please enter district name</span>' },
			ddlTehsil: 			{ required: '<span style="color:red; font-size: 12px;">Please enter tehsil Name</span>' },
			ddlStatus: 				{ required: '<span style="color:red; font-size: 12px;">Please enter status</span>' }
			
        },
	});