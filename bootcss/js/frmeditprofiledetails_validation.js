$("#frmeditprofiledetails").validate({
        rules: {
            txtName1: { required: true },
			txtMobile: { required: true },
			txtEmail: { required: true }
			
						
        },			
        messages: {				
			txtName1: { required: '<span style="color:red; font-size: 12px;">Please Enter  Name</span>' },
			txtMobile: { required: '<span style="color:red; font-size: 12px;">Please Enter Mobile No</span>' },
			txtEmail: { required: '<span style="color:red; font-size: 12px;">Please Enter Email </span>' }
		
			
        },
	});