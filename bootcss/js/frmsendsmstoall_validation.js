$("#frmsendsmstoall").validate({
        rules: {
                        txtentity: { required: true },
						ddlDistrict: { required: true },
						ddlTehsil: { required: true },
						ddlrsp: { required: true },
						ddlCenter: { required: true },
						
						txtmobile: { required: true },
						txtMessage: { required: true }                    
        },			
        messages: {				
			txtentity: { required: '<span style="color:red; font-size: 12px;">Please Enter Entity Name</span>' },
			ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Please Enter District Name</span>' },
			ddlCenter: { required: '<span style="color:red; font-size: 12px;">Please Enter Center Name </span>' },
			ddlrsp: { required: '<span style="color:red; font-size: 12px;">Please Enter RSP Name</span>' },
			
			txtmobile: { required: '<span style="color:red; font-size: 12px;">Please Enter Mobile Name</span>' },
			txtMessage: { required: '<span style="color:red; font-size: 12px;">Please Enter Message Name</span>' }
			
			
        }
	});