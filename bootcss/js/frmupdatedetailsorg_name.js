var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    }, decode: function (e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r)
            }
            if (a != 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    }, _utf8_encode: function (e) {
        e = e.replace(/rn/g, "n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    }, _utf8_decode: function (e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t
    }
}

function encryptString(str) {
    var encryptedString = '';
    encryptedString = Base64.encode(str);
    return encryptedString;
}

function decryptString(str) {
    var decryptString;
    decryptString = Base64.decode(str);
    return decryptString;
}

var reference_number = '';
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    function sendMailtoITGK() {
        var url = "common/cfmodifyITGK.php";
        var data;
        data = "action=sendEmailToITGK&reference_number=" + reference_number+"&requestType=name",
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
                if (data === "1") {
                    $("#submitted").modal("show");
                    $("#submitting").modal("hide");
                } else {
                    alert("Some error occurred, please try again later, redirecting you back to dashboard");
                    setTimeout(function () {
                        window.location.href = 'dashboard.php';
                    }, 2000);
                }
            }
        });
    }

    function sendEmailToSP() {
        var url = "common/cfmodifyITGK.php";
        var data;
        data = "action=sendEmailToSP&reference_number=" + reference_number+"&requestType=name",
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
                if (data === "1") {
                    sendMailtoITGK();
                } else {
                    alert("Some error occurred, please try again later, redirecting you back to dashboard");
                    setTimeout(function () {
                        window.location.href = 'dashboard.php';
                    }, 2000);
                }
            }
        });
    }

    $("#updateOrgDetails").on("submit", function (e) {

        e.preventDefault();
        var form = $(this);
        form.parsley().validate();
        $("#submitting").modal("show");

        if (form.parsley().isValid()){
            var url = "common/cfmodifyITGK.php";
            var data;
            data = new FormData(this);

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data) {
                    if (data === "Successfully Updated") {
                        sendEmailToSP();
                    } else {
                        $("#submitting").modal("hide");
                        alert(data);
                    }
                }
            });
        } else {
            $("#submitting").modal("hide");
        }
        return false;
    });

    $('#otp_modal_updateAddressITGK').on('hidden.bs.modal', function () {

        reference_number = $("#otp_updateAddressITGK").val();
        $("#reference_number").val(reference_number);

        $("#errorTextOTP_updateAddressITGK, #successText_updateAddressITGK, #responseText_updateAddressITGK").html("");
        $("#showError_updateAddressITGK, #showSuccess_updateAddressITGK, #responseText_updateAddressITGK, #verifyOTP_updateAddressITGK").css("display", "none");
        $("#newMobile_updateAddressITGK, #otp_updateAddressITGK").removeAttr("readonly");
        $("#newMobile_updateAddressITGK, #otp_updateAddressITGK").val("");
        $("#sendOTP_updateAddressITGK").text("Send OTP");
        $("#sendOTP_updateAddressITGK").removeClass("disabled");
    });

    function sendOTPtoITGK() {

        var phoneNumber = $("#newMobile_updateAddressITGK").val();
        var url = "common/cfmodifyITGK.php"; // the script where you handle the form input.
        var data;
        var forminput = "phoneNumber=" + phoneNumber;
        data = "action=sendOTP&" + forminput+"&fld_requestChangeType=name"; // serializes the form's elements.

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
                $("#responseText").html(data);
                $("#responseText").css("display", "block");

            }
        });
    }

    $("#sendOTP_updateAddressITGK").click(function (e) {

        e.preventDefault();

        $("#sendOTP_updateAddressITGK").text("Please wait...");
        $("#sendOTP_updateAddressITGK").addClass("disabled");

        var pattern = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;

        var phoneNumber = $("#newMobile_updateAddressITGK").val();

        if (pattern.test(phoneNumber)) {
            if (phoneNumber.length == 10) {

                $("#errorTextOTP, #successText").html("");
                $("#showError, #showSuccess").css("display", "none");

                var url = "common/cfmodifyITGK.php"; // the script where you handle the form input.
                var data;
                var forminput = "phoneNumber=" + phoneNumber;
                data = "action=checkMobile&" + forminput; // serializes the form's elements.


                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {

                        var JSONObject = JSON.parse(data);

                        if (JSONObject.total_count === "0") {
                            $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
                            $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
                            $("#errorTextOTP_updateAddressITGK").html("Mobile number not found in our records");
                            $("#showError_updateAddressITGK").css("display", "block");
                            $("#sendOTP_updateAddressITGK").text("Send OTP");
                            $("#sendOTP_updateAddressITGK").removeClass("disabled");
                        } else {
                            sendOTPtoITGK();
                        }
                    }
                });

            } else {
                $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
                $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
                $("#errorTextOTP_updateAddressITGK").html("Invalid mobile number");
                $("#showError_updateAddressITGK").css("display", "block");
                $("#sendOTP_updateAddressITGK").text("Send OTP");
                $("#sendOTP_updateAddressITGK").removeClass("disabled");
            }
        }
        else {
            $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
            $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
            $("#errorTextOTP_updateAddressITGK").html("Invalid mobile number");
            $("#showError_updateAddressITGK").css("display", "block");
            $("#sendOTP_updateAddressITGK").text("Send OTP");
            $("#sendOTP_updateAddressITGK").removeClass("disabled");
        }
    });

    $("#verifyOTPbtn_updateAddressITGK").click(function () {

        $("#newMobile_updateAddressITGK").attr("readonly", "readonly");
        $("#otp_updateAddressITGK").attr("readonly", "readonly");
        if ($("#otp_updateAddressITGK").val() == "") {
            $("#otp_updateAddressITGK").removeAttr("readonly");
            $("#otp_updateAddressITGK").val("");
            $("#responseText_updateAddressITGK, #successText_updateAddressITGK").html('');
            $("#responseText_updateAddressITGK, #showSuccess_updateAddressITGK").css("display", "none");
            $("#errorTextOTP_updateAddressITGK").html("Invalid OTP");
            $("#showError_updateAddressITGK").css("display", "block");
        } else {
            var url = "common/cfmodifyITGK.php"; // the script where you handle the form input.
            var data;
            var forminput = "otp=" + $("#otp_updateAddressITGK").val();
            forminput = forminput + "&learner=" + $("#LearnerCode").val() + "&mobile=" + $("#newMobile_updateAddressITGK").val();
            data = "action=verifyOTP&" + forminput; // serializes the form's elements.
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    var JSONObject = JSON.parse(data);
                    if (JSONObject.total_count === "0") {
                        $('#successText_updateAddressITGK').html('');
                        $('#showSuccess_updateAddressITGK').css('display', 'none');
                        $('#errorTextOTP_updateAddressITGK').html('Invalid OTP');
                        $('#showError_updateAddressITGK').css('display', 'block');
                        $("#sendOTP_updateAddressITGK").text("Send OTP");
                        $("#sendOTP_updateAddressITGK").removeClass("disabled");
                        $("#otp_updateAddressITGK").removeAttr("readonly");
                    } else {
                        $('#showError_updateAddressITGK').css('display', 'none');
                        $('#successText_updateAddressITGK').html('OTP has been successfully verified, you can now proceed to update address<BR>Redirecting you to the correct page');
                        $('#showSuccess_updateAddressITGK').css('display', 'block');
                        $('#verifyOTP').css('display', 'block');

                        setTimeout(function () {
                            $('#loading').modal('show');
                            $('#otp_modal_updateAddressITGK').modal('hide');
                            fillForm();
                        }, 1000);
                    }
                }
            });
        }
    });

    $("#revokeReference").on("click", function () {
        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=revokeReference&otp=" + reference_number + "",
            success: function (data) {
                $("#errorModal_Custom_backdrop").modal("hide");
                if (data === "Successfully Updated") {
                    $("#revokeText").html("Reference Number <span style='color: #00A65A'>" + reference_number + "</span> has been successfully revoked, redirecting to correct page.." +
                        ".");
                    $("#revoke_success").modal("show");
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
            }
        });
    });

    firstCheck();

    //fillForm();

    function firstCheck() {

        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=firstCheck&fld_requestChangeType=name",
            success: function (data) {

                var JSONObject = JSON.parse(data);

                if (JSONObject.total_count === "-1" || JSONObject.fld_status === "5") {
                    $('#otp_modal_updateAddressITGK').modal('show');
                } else {
                    /***reference_number = JSONObject.ref_no;
                     $("#errorText_backdrop").html("Address update request has been already submitted, the reference number is: <span style='color: #00A65A'>" + JSONObject.ref_no + "</span>, however you can revoke this reference number to re-submit the form again.");
                     $("#errorModal_Custom_backdrop").modal('show');***/

                    if (JSONObject.fld_status === "0") {
                        reference_number = JSONObject.ref_no;
                        $("#editRequest_backdrop").html("Name Update Request has been already opened and pending for submission, the reference number is: <span style='color: #00A65A'>" + JSONObject.ref_no + "</span>, Click on Edit this Request to complete form submission.");

                        $("#editRequest_button").attr("onclick","window.location.href='editRequest.php?flag="+encryptString('name')+"&code="+encryptString(reference_number)+"'");

                        $("#editRequest").modal('show');
                    }

                    if (JSONObject.fld_status === "1") {
                        reference_number = JSONObject.ref_no;
                        $("#editRequest_backdrop").html("Name Update Request has been already submitted, the reference number is: <span style='color: #00A65A'>" + JSONObject.ref_no + "</span>, however you can re-submit the form again, until it is approved by your Service Provider");

                        $("#editRequest_button").attr("onclick","window.location.href='editRequest.php?flag="+encryptString('name')+"&code="+encryptString(reference_number)+"'");

                        $("#editRequest").modal('show');
                    }

                    if (JSONObject.fld_status === "2") {
                        reference_number = JSONObject.ref_no;
                        $("#editRequest_block_backdrop").html("Name Update Request has been already submitted, and approved by Service Provider, the reference number is: <span style='color: #00A65A'>" + JSONObject.ref_no + "</span>, You cannot re-submit the form at this point.");
                        $("#editRequest_block").modal('show');
                    }

                    if (JSONObject.fld_status === "3") {
                        reference_number = JSONObject.ref_no;
                        $("#editRequest_block_backdrop").html("Name Update Request has been already submitted, and it is pending for payment, the reference number is: <span style='color: #00A65A'>" + JSONObject.ref_no + "</span>, You cannot re-submit the form at this point.");
                        $("#editRequest_block").modal('show');
                    }

                    if(JSONObject.fld_status >=4){
                        $("#unauthorized").modal('show');
                    }
                }
            }
        });
    }

    function fillForm() {

        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=EDIT_NEW&values=" + OrgCode + "",
            success: function (data) {

                $('#updateOrgDetails').parsley().destroy();

                data = $.parseJSON(data);

                setTimeout(function () {
                    $("#Organization_Name").val(data[0].Organization_Name);
                    $("#Organization_Name_old").val(data[0].Organization_Name);
                    $('#loading').modal('hide');
                    $('#showdata').css('display', 'block');
                }, 500);
            }
        });

        $('#updateOrgDetails').parsley();
    }

    function resetForm(formid) {
        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
    }

});