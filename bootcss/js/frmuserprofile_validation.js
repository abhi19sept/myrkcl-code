$("#profile").validate({
        rules: {
            txtFirstName: { required: true },	txtHouseNo: { required: true },	txtDOB: { required: true },	ddlMTongue: { required: true },
			txtAddress: { required: true },		ddlGender: { required: true },		txtFatherName: { required: true, },	 	
			ddlQualification: { required: true},		txtOccupation: { required: true },	 		ddlChallenged: { required: true },	
			txtAadharNo: { required: true },		txtSTDCode: { required: true },		txtMobile: { required: true },
			txtEmail: { required: true, email: true },	txtFaceBookId: { required: true },		txtTwitterId: { required: true },
			
        },			
        messages: {				
			txtFirstName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your First Name</span>' },
			txtHouseNo: 		{ required: '<span style="color:red; font-size: 12px;">Please enter House No</span>' },
			txtDOB:		{ required: '<span style="color:red; font-size: 12px;">Please Enter DOB</span>' },
			ddlMTongue: 		{ required: '<span style="color:red; font-size: 12px;">Please select Mother Tounge</span>' },
			txtAddress: 			{ required: '<span style="color:red; font-size: 12px;">Please Enter Address</span>' },
			txtFatherName: 				{ required: '<span style="color:red; font-size: 12px;">Please Enter father Name</span>' },
			
			ddlQualification: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Qualification</span>' },
			txtOccupation: 				{ required: '<span style="color:red; font-size: 12px;">Please Enter Occupation</span>' },
			ddlChallenged: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Challenged or Not Challanged</span>' },
			txtAadharNo: 		{ required: '<span style="color:red; font-size: 12px;">Please Enter Adhar No.</span>' },
			txtSTDCode: 			{ required: '<span style="color:red; font-size: 12px;">Please enter STD Code</span>' },
			txtMobile: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Mobile No</span>' },
			txtEmail: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Email</span>',
                                                                    email: '<span style="color:red; font-size: 12px;">Please enter a VALID email</span>'},
			txtFaceBookId: 				{ required: '<span style="color:red; font-size: 12px;">Please Enter facebook iD.</span>' },
			txtTwitterId: 			{ required: '<span style="color:red; font-size: 12px;">Please Enter twitter ID</span>' }
			
        },
	});