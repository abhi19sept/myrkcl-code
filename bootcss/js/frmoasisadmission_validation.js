$("#frmoasisadmission").validate({
        rules: {
            txtlname: { required: true },
            txtfname: { required: true },
            txtmothername: { required: true },
            txtidno: { required: true, minlength:12 },
            txtBhamashahNo: { minlength:13 },
            dob: { required: true },		
            ddlmotherTongue: { required: true }, 
            txtApplicantmobile: { required: true, minlength:10, maxlength: 10 },                       
            txtAddress: { required: true },	
            gender: { required: true }, 
            mstatus: { required: true }, 
            Medium: { required: true },	
			PH: { required: true },	
			ddlDistrict: { required: true }, 
			ddlTehsil: { required: true },	
			txtPIN: { required: true, minlength:6, maxlength: 6, min: 6 },
			txtResiPh: { minlength:10, maxlength: 10, min: 999999999 },
			ddlQualification: { required: true, min: 1 },  
			ddlCategory: { required: true }, 
			txtcaste: { required: true },
            ddlReligion: { required: true }, 
            ddlMinBoard: { required: true }, 
            txtMinPercentage: { required: true },
            ddlMinDivision: { required: true }, 
            ddlMinSchoolType: { required: true }, 
            ddlMinMonth: { required: true }, 
            txtMinYear: { required: true },
			SubCategory: { required: true },
			ddlBoard: { required: true }, 
			txtPercentage: { required: true },
            ddlDivision: { required: true }, 
            ddlMonth: { required: true }, 
            txtYear: { required: true },
            txtUploadScanDoc: { required: true }, 
            uploadImage1: { required: true }, 
            uploadImage2: { required: true }, 
            uploadImage3: { required: true }, 
            uploadImage4: { required: true }, 
            uploadImage5: { required: true }, 
            uploadImage6: { required: true }, 
            uploadImage7: { required: true }, 
            uploadImage8: { required: true }, 
            uploadImage9: { required: true }, 
            uploadImage10: { required: true }, 
            uploadImage11: { required: true },
            uploadImage12: { required: true }, 
            ddlPanchayat: { required: true }, 
            ddlGramPanchayat: { required: true },
            TSP: { required: true },
            Nontspval:{ required: true },
            ddlTspDistrict: { required: true },
            ddlTspTehsil: { required: true },
            ddlMunicipalName: { required: true },
            ddlWardno: { required: true },
            ddlPanchayat: { required: true },
            ddlGramPanchayat: { required: true },
            ddlVillage: { required: true, min: 1 },
            area: { required: true },
            terms: { required: true },
            txtName1: { required: true },
            txtName2: { required: true },
            txtMobile: { required: true },
            txtMobile2: { required: true },
            txtITGKAddress: { required: true },
            txtITGKAddress2: { required: true },
            ddlCenter: { required: true, min: 1 },
            ddlCenter2: { required: true, min: 1 },
            Legislative_Assembly:{required: true}
        },
        messages: {				
			txtlname: { required: '<span class="error">Please enter Applicant Name</span>' },
			txtfname: { required: '<span class="error">Please enter Father Name</span>' },
			txtidno: {  required: '<span class="error">Please enter Aadhaar No.</span>',
                        minlength: '<span class="error">Please enter at least 12 digits</span>' },
			txtBhamashahNo: { minlength: '<span class="error">Please enter at least 13 digits</span>' },
                        txtmothername: { required: '<span class="error">Please enter Mother Name</span>' },
			dob: { required: '<span class="error">Please Select Applicant DOB</span>',
                                            minlength:'<span class="error">Incorrect Date</span>',
                                            maxlength:'<span class="error">Incorrect Date</span>'},
			ddlmotherTongue: { required: '<span class="error">Please select your Mother Tongue</span>' },
			txtApplicantmobile: { required: '<span class="error">Please enter your mobile no.</span>',
							minlength:'<span class="error">Please enter at least 10 digits</span>',
							maxlength:'<span class="error">Please enter not more than 10 digits</span>'},
                       
			txtAddress: { required: '<span class="error">Please enter address</span>' },
			gender: { required: '<span class="error">Please select Gender</span>' },
			mstatus: { required: '<span class="error">Please select Marital Status</span>' },
			Medium: { required: '<span class="error">Please select Medium of Study</span>' },
			PH: { required: '<span class="error">Please select Physical Status</span>' },
			ddlDistrict: { required: '<span class="error">Please select district</span>' },
			ddlTehsil: { required: '<span class="error">Please select Tehsil</span>' },
			txtPIN: { required: '<span class="error">Please enter PIN</span>',
						minlength:'<span class="error">Pin Code must be 6 digits</span>',
							maxlength:'<span class="error">Pin Code must be 6 digits</span>',
							min:'<span class="error">Pin Code must be 6 digits</span>'},
			txtResiPh: { required: '<span class="error">Please enter phone number</span>',
						minlength:'<span class="error">Phone number must be 10 digits</span>',
							maxlength:'<span class="error">Phone number must be 10 digits</span>',
							min:'<span class="error">Invalid phone number</span>'},
			ddlQualification: { min: '<span class="error">Please select Qualification</span>' },
			ddlCategory: { required: '<span class="error>Please select Applicant Category</span>' },
			txtcaste: { required: '<span class="error">Please enter Caste</span>' },
                        ddlReligion: { required: '<span class="error">Please select Applicant Religion</span>' },
                        ddlBoard: { required: '<span class="error">Please select University/Board</span>' },
                        txtPercentage: { required: '<span class="error">Please enter Percentage</span>' },
                        ddlDivision: { required: '<span class="error">Please select Division</span>' },
                        ddlMonth: { required: '<span class="error">Please select Month</span>' },
                        txtYear: { required: '<span class="error">Please enter Year of Passing</span>' },
						
			ddlMinBoard: { required: '<span class="error">Please select University/Board</span>' },
                        txtMinPercentage: { required: '<span class="error">Please enter Percentage</span>' },
                        ddlMinDivision: { required: '<span class="error">Please select Division</span>' },
                        ddlMinSchoolType: { required: '<span class="error">Please select School Type</span>' },
                        ddlMinMonth: { required: '<span sclass="error">Please select Month</span>' },
                        txtMinYear: { required: '<span sclass="error">Please enter Year of Passing</span>' },
						SubCategory: { required: '<span class="error">Please select SubCategory</span>' },
			txtUploadScanDoc: { required: '<span class="error">Please attach your scan form</span>' },
			uploadImage1: 	{ required: '<span class="error">Please upload your photo.</span>' },
			uploadImage2: 	{ required: '<span class="error">Please upload your sign.</span>' },
			uploadImage3: 	{ required: '<span class="error">Please attach HSC(10th) Education Certificate / Marksheet.</span>' },
			uploadImage4: 	{ required: '<span class="error">Please attach Highest Education Certificate / Final Marksheet.</span>' },
			uploadImage5: 	{ required: '<span class="error">Please attach proof of Age.</span>' },
			uploadImage6: 	{ required: '<span class="error">Please attach Identity Proof.</span>' },
			uploadImage7: 	{ required: '<span class="error">Please attach Sub-Category proof document.</span>' },
			uploadImage8: 	{ required: '<span class="error">Please attach your Caste Certificate.</span>' },
			uploadImage9: 	{ required: '<span class="error">Please attach your physical disability Certificate.</span>' },
			uploadImage10: 	{ required: '<span class="error">Please attach Marital status document.</span>' },
			uploadImage11: 	{ required: '<span class="error">Please attach proof of Victim of Violence.</span>' },
            uploadImage12:  { required: '<span class="error">Please attach Aadhaar card.</span>' },
			ddlPanchayat: { required: '<span class="error">Please select Panchayat</span>' },
			ddlGramPanchayat: { required: '<span class="error">Please select Gram Panchayat</span>' },
			TSP: { required: '<span style="color:red; font-size: 10px; position: absolute; width: 125px; margin-top: 10px;">Please select T.S.P.</span>' },
			Nontspval:{ required: '<span style="color:red; font-size: 10px; position: absolute; width: 125px; margin-top: 10px;">Please select Non T.S.P.</span>' },
            ddlTspDistrict: { required: '<span class="error">Please select District here.</span>' },
			ddlTspTehsil: { required: '<span class="error">Please select this Tehsil.</span>' },
			ddlMunicipalName: { required: '<span class="error">Please select Municipality Name.</span>' },
            ddlWardno: { required: '<span class="error">Please select ward name.</span>' },
            ddlPanchayat: { required: '<span class="error">Please select panchayat samiti.</span>' },
            ddlGramPanchayat: { required: '<span class="error">Please select gram panchayat.</span>' },
            ddlVillage: { required: '<span class="error">Please select your Village.</span>',
                          min: '<span class="error">Please select your Village.</span>'
                         },
            area: { required: '<span style="color:red; font-size: 10px; position: absolute; width: 140px; margin-top: 10px;">Please select area type.</span>' },
            terms: { required: '<span class="error"> Please check here, as acceptance of your declarations.</span>' },
            txtName1: { required: '<span class="error">Name of center 1 is required.</span>' },
            txtName2: { required: '<span class="error">Name of center 2 is required.</span>' },
            txtMobile: { required: '<span class="error">Mobile number of center 1 is required.</span>' },
            txtMobile2: { required: '<span class="error">Mobile number of center 2 is required.</span>' },
            txtITGKAddress: { required: '<span class="error">Address of center 1 is required.</span>' },
            txtITGKAddress2: { required: '<span class="error">Address of center 2 is required.</span>' },
            ddlCenter: { required: '<span class="error">Please select center preference 1.</span>',
                         min: '<span class="error">Please select center preference 1.</span>' },
            ddlCenter2: { required: '<span class="error">Please select center preference 2.</span>',
                         min: '<span class="error">Please select center preference 2.</span>' },
            Legislative_Assembly:{required: '<span class="error">Please select your legislative assembly.</span>'}
        }
	});