$("#frmApplyEoi").validate({
        rules: {
     ddlEOI: { required: true },	
     chk: { required: true },	
	 
        },			
        messages: {				
			ddlEOI: { required: '<span style="color:red; font-size: 12px;">Please select EOI Name</span>' },
			chk: { required: '<span style="color:red; font-size: 12px;">Please Accept Terms and Conditions</span>' }
			
        },
	});