$("#frmOrgMaster").validate({
        rules: {
            txtName1: { required: true },
                        ddlOrgType: { required: true },
			txtEmail: { required: true }, 
			txtMobile: { required: true },
                        
			txtRegno: { required: true },	
			txtEstdate: { required: true },	
			txtCourse: { required: true },
			txtType: { required: true }, 
			txtDoctype: { required: true },
                        txtPanNo: { required: true },
			orgdocproof: { required: true },	
			ddlCountry: { required: true },	
			ddlState: { required: true },
			ddlRegion: { required: true }, 
			ddlDistrict: { required: true },
			ddlTehsil: { required: true },	
                        txtPinCode: { required: true },	
			ddlArea: { required: true },
			txtHouseno: { required: true }, 
			txtStreet: { required: true },
			area: { required: true },
			txtRoad: { required: true },
			txtLandmark: { required: true }
			
						
        },			
        messages: {	
                        ddlOrgType: { required: '<span style="color:red; font-size: 12px;">Please select organisation type</span>' },
			txtEmail: { required: '<span style="color:red; font-size: 12px;">Please enter Email Id</span>' },
			txtMobile: { required: '<span style="color:red; font-size: 12px;">Please enter Mobile No</span>' },
			txtName1: { required: '<span style="color:red; font-size: 12px;">Please enter organisation Name</span>' },
			txtRegno: { required: '<span style="color:red; font-size: 12px;">Please enter Registration no</span>' },
			txtEstdate: { required: '<span style="color:red; font-size: 12px;">Please enter Date</span>' },
			txtCourse: { required: '<span style="color:red; font-size: 12px;">Please enter course name</span>' },
			txtType: { required: '<span style="color:red; font-size: 12px;">Please enter type</span>' },
			txtDoctype: { required: '<span style="color:red; font-size: 12px;">Please enter doc type</span>' },
                        txtPanNo: { required: '<span style="color:red; font-size: 12px;">Please enter PAN Card Number</span>' },
			orgdocproof: { required: '<span style="color:red; font-size: 12px;">Please enter doc proof</span>' },
			ddlCountry: { required: '<span style="color:red; font-size: 12px;">Please enter country Name</span>' },
			ddlState: { required: '<span style="color:red; font-size: 12px;">Please enter your state</span>' },
			ddlRegion: { required: '<span style="color:red; font-size: 12px;">Please enter Region</span>' },
			ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Please enter district</span>' },
			ddlTehsil: { required: '<span style="color:red; font-size: 12px;">Please enter tehsil</span>' },
                        txtPinCode: { required: '<span style="color:red; font-size: 12px;">Please enter PIN Code</span>' },
			ddlArea: { required: '<span style="color:red; font-size: 12px;">Please enter Area Name</span>' },
			txtHouseno: { required: '<span style="color:red; font-size: 12px;">Please enter House no</span>' },
			txtStreet: { required: '<span style="color:red; font-size: 12px;">Please enter street no</span>' },
			txtRoad: { required: '<span style="color:red; font-size: 12px;">Please enter road</span>' },
                        area: { required: '<span style="color:red; font-size: 12px;">Please Select Area Type</span>' },
			txtLandmark: { required: '<span style="color:red; font-size: 12px;">Please enter landmark</span>' }
			
			
        },
	});