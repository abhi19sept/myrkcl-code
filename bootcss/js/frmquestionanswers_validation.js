$("#frmquestionanswers").validate({
        rules: {
                        ddlSurvey: { required: true },
						txtQuestion: { required: true},
						txtoption1: { required: true },
						txtoption2: { required: true },
						txtoption3: { required: true }
						
                    
        },			
        messages: {				
			ddlSurvey: { required: '<span style="color:red; font-size: 12px;">Please Enter Survey Name</span>' },
			txtQuestion: { required: '<span style="color:red; font-size: 12px;">Please Enter Question</span>' },
			txtoption1: { required: '<span style="color:red; font-size: 12px;">Please Enter option1</span>' },
			txtoption2: { required: '<span style="color:red; font-size: 12px;">Please Enter option2</span>' },
			txtoption3: { required: '<span style="color:red; font-size: 12px;">Please Enter option3</span>' }
			
        }
	});