$("#frmvisiteduser").validate({
        rules: {
                        txtstart: { required: true },	
						txtend: { required: true }
						
        },			
        messages: {				
			txtstart: { required: '<span style="color:red; font-size: 12px;">Please Enter Start Date</span>' },
			txtend: { required: '<span style="color:red; font-size: 12px;">Please Enter End Date</span>' }
			
			
        }
	});