$("#frmexamcenterdetails").validate({
        rules: {
            ddlDistrict: { required: true },
			ddlCenter: { required: true },	
			ddlTehsil: { required: true },	
			txtName: { required: true },
			txtAddress: { required: true }, 
		
			txtCapacity: { required: true },	
			txttxtRooms: { required: true },
			 
			ddlEvent: { required: true }
			
			
        },			
        messages: {				
			ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Please Select District Code</span>' },
			ddlCenter: { required: '<span style="color:red; font-size: 12px;">Please Select Center Code</span>' },
			ddlTehsil: { required: '<span style="color:red; font-size: 12px;">Please Select tehsil Code</span>' },
			txtName: { required: '<span style="color:red; font-size: 12px;">Please enter Center name</span>' },
			txtAddress: { required: '<span style="color:red; font-size: 12px;">Please enter Center Address</span>' },
			
			txtCapacity: { required: '<span style="color:red; font-size: 12px;">Please enter Capacity</span>' },
			txttxtRooms: { required: '<span style="color:red; font-size: 12px;">Please enter Rooms</span>' },
			
			ddlEvent: { required: '<span style="color:red; font-size: 12px;">Please enter Events</span>' }
			
			
			
			
        },
	});