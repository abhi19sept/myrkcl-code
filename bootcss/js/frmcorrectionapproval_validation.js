$("#frmcorrectionapproved").validate({
        rules: {
            txtCentercode: { required: true },	txtLcode: { required: true }, ddlstatus: { required: true }
        },			
        messages: {				
			txtCentercode: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your center code</span>' },
			txtLcode: 				{ required: '<span style="color:red; font-size: 12px;">Please enter your learner code</span>' },
			ddlstatus: 					{ required: '<span style="color:red; font-size: 12px;">Please select status</span>' }
        },
	});