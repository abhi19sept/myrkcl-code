$("#frmRegionMaster").validate({
        rules: {
            txtRegionName: { required: true },	
			ddlCountry: { required: true },
			 ddlState: { required: true },	
			ddlStatus: { required: true }
			
			
			
        },			
        messages: {				
			txtRegionName: 				{ required: '<span style="color:red; font-size: 12px;">Please enter Region Name</span>' },
			ddlCountry: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Country Name</span>' },
			
			ddlState: 			{ required: '<span style="color:red; font-size: 12px;">Please enter State</span>' },
			
			ddlStatus: 			{ required: '<span style="color:red; font-size: 12px;">Please enter Status</span>' }
			
			
        },
	});