<?php
 require_once 'DB_Connect.php';
/**
 * @author Ravi Tamada
 * @link https://www.androidhive.info/2012/01/android-login-and-registration-with-php-mysql-and-sqlite/ Complete tutorial
 */
class DB_Functions {

    private $conn;

    // constructor
    function __construct() {
       
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
		//mysqli_set_charset($this->conn, "utf8");
		
		/* change character set to utf8 */
		//if (!mysqli_set_charset($this->conn, "utf8")) {
		//	printf("Error loading character set utf8: %s\n", mysqli_error($this->conn));
		//	exit();
		//} 
    }

    // destructor

    /**
     * Storing new user
     * returns user details
     */
    public function storeUser($name, $email, $password) {
        $uuid = uniqid('', true);
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt

        $stmt = $this->conn->prepare("INSERT INTO users(unique_id, name, email, encrypted_password, salt, created_at) VALUES(?, ?, ?, ?, ?, NOW())");
        $stmt->bind_param("sssss", $uuid, $name, $email, $encrypted_password, $salt);
        $result = $stmt->execute();
        $stmt->close();

        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM users WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            return $user;
        } else {
            return false;
        }
    }
	public function addCenterLocation($ITGK_Code, $latitude, $longitude) {
        $uuid = uniqid('', true);
        
        $stmt = $this->conn->prepare("INSERT INTO tbl_centerlocation(ITGK_Code, latitude, longitude, created_at, Updated_at) VALUES(?, ?, ?, NOW(), NOW())");
        $stmt->bind_param("sss", $ITGK_Code, $latitude, $longitude);
        $result = $stmt->execute();
        $stmt->close();

        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM tbl_centerlocation WHERE ITGK_Code = ?");
            $stmt->bind_param("s", $ITGK_Code);
            $stmt->execute();
            $centerLocation = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            return $centerLocation;
        } else {
            return false;
        }
    }

    /**
     * Get user by email and password
     */
    public function getUserByEmailAndPassword($email, $password) {

        $stmt = $this->conn->prepare("SELECT * FROM users WHERE email = ?");

        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            // verifying user password
            //$salt = $user['salt'];
            $DB_password = $user['User_Password'];
            //$hash = $this->checkhashSSHA($salt, $password);
            // check for password equality
            if ($encrypted_password == $hash) {
                // user authentication details are correct
                return $user;
            }
        } else {
            return NULL;
        }
    }
	public function getUserByItgkAndPassword($Itgk, $password,$User_device_id) {

        $stmt = $this->conn->prepare("select User_Code,User_EmailId,User_MobileNo,User_LoginId,User_Password,User_UserRoll from tbl_user_master where User_LoginId = ? and User_Status = '1' ");
        
        $stmt->bind_param("s", $Itgk);
		

        if ($stmt->execute()) {
			
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            // verifying user password
            //$salt = $user['salt'];
            $encrypted_password = $user['User_Password'];
            //$hash = $this->checkhashSSHA($salt, $password);
            // check for password equality
            if ($encrypted_password == $password) {
                // user authentication details are correct
				$stmt3 = $this->conn->prepare("update tbl_user_master SET User_device_id = '".$User_device_id."' where User_LoginId='".$Itgk."'");

				$stmt3->execute();
				$stmt3->close();
                return $user;
            }
        } else {
            return NULL;
        }
    }
	public function getNearestCenter($latitude, $longitude) {

        //$stmt = $this->conn->prepare("SELECT id,ITGK_Code,latitude, longitude, SQRT(POW(69.1 * (latitude - ?), 2) + POW(69.1 * (? - longitude) * COS(latitude / 57.3), 2)) AS distance FROM tbl_centerlocation order by distance limit 5");
		
		$stmt = $this->conn->prepare("Select Organization_User, Organization_Name,b.User_MobileNo ,b.User_LoginId,a.Organization_Address , COALESCE(f.latitude,COALESCE(Organization_lat,0)) AS latitude, COALESCE(f.longitude,COALESCE(Organization_long,0)) AS longitude,SQRT(POW(69.1 * (COALESCE(f.latitude,COALESCE(Organization_lat,0)) - ?), 2) + POW(69.1 * (? - COALESCE(f.longitude,COALESCE(Organization_long,0))) * COS(COALESCE(f.latitude,COALESCE(Organization_lat,0)) / 57.3), 2)) AS distance  From tbl_organization_detail as a join tbl_user_master as b on a.Organization_User = b.User_Code join tbl_courseitgk_mapping as c on c.Courseitgk_ITGK = b.User_LoginId left join tbl_centerlocation as f on f.ITGK_Code = b.User_LoginId Where b.User_Status = '1' AND b.User_UserRoll = '7' AND Courseitgk_Course='RS-CIT' AND CourseITGK_BlockStatus='unblock' AND EOI_Fee_Confirm='1' AND CURDATE() >= CourseITGK_StartDate AND CURDATE() <= CourseITGK_ExpireDate order by distance limit 5");


        $stmt->bind_param("ss", $latitude, $longitude);

        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$center = array();
			while ($row = $result->fetch_assoc()) {
				$center[] = $row;
			}
            $stmt->close();

            return $center;
			
        } else {
            return NULL;
        }
    }
	public function getCourseList() {

        $stmt = $this->conn->prepare("Select Course_Code,Course_Name From tbl_course_master where Course_Code in ('1','5')");

        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$courses = array();
			while ($row = $result->fetch_assoc()) {
				$courses[] = $row;
			}
            $stmt->close();

            return $courses;
			
        } else {
            return NULL;
        }
    }
	public function GetDistrictList() {

        $stmt = $this->conn->prepare("select District_Code,District_Name from tbl_district_master where District_StateCode = '29' and District_Status = 1
");

        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$districts = array();
			while ($row = $result->fetch_assoc()) {
				$districts[] = $row;
			}
            $stmt->close();

            return $districts;
			
        } else {
            return NULL;
        }
    }
	public function GetTehsilList($district) {

        $stmt = $this->conn->prepare("Select Tehsil_Code,Tehsil_Name From tbl_tehsil_master Where Tehsil_Status='1' AND Tehsil_District = '".$district."'");

        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$tehsils = array();
			while ($row = $result->fetch_assoc()) {
				$tehsils[] = $row;
			}
            $stmt->close();

            return $tehsils;
			
        } else {
            return NULL;
        }
    }

	public function GetMunicipalityList($district) {

     $stmt = $this->conn->prepare("Select Municipality_Code,Municipality_Name,Municipality_Raj_Code From tbl_municipality_master Where Municipality_District = '".$district."'");


        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$Municipalities = array();
			while ($row = $result->fetch_assoc()) {
				$Municipalities[] = $row;
			}
            $stmt->close();

            return $Municipalities;
			
        } else {
            return NULL;
        }
    }
	
	
	public function Fill_batch_attendence_app($Course_Code) {
            $stmt = $this->conn->prepare("select Batch_Code,Batch_Name from tbl_batch_master as a inner join tbl_event_management as b
                on a.Batch_Code=b.Event_Batch
                where Event_Course='".$Course_Code."' and Event_Category='10' and Event_Name='13' and Event_LearnerAttendance='1'
                AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate");
                if ($stmt->execute()) {                        
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getbatch = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getbatch[] = $row;
                    }
                    $stmt->close();

                    return $Getbatch;
            
                } else {
                    return NULL;
                }
            
        }

	public function GetWardList($municipality) {

        $stmt = $this->conn->prepare("Select Ward_Code,Ward_Name From tbl_ward_master Where Ward_Municipality_Code='" . $municipality . "'");

        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$Wards = array();
			while ($row = $result->fetch_assoc()) {
				$Wards[] = $row;
			}
            $stmt->close();

            return $Wards;
			
        } else {
            return NULL;
        }
    }

	public function GetPanchayatSamitiList($district) {

        $stmt = $this->conn->prepare("Select Block_Code,Block_Name From tbl_panchayat_samiti Where Block_District='" . $district . "'");

        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$PanchayatSamiti = array();
			while ($row = $result->fetch_assoc()) {
				$PanchayatSamiti[] = $row;
			}
            $stmt->close();

            return $PanchayatSamiti;
			
        } else {
            return NULL;
        }
    }
	public function GetGramPanchayatList($PanchayatCode) {

        $stmt = $this->conn->prepare("Select GP_Code,GP_Name"
                    . " From  tbl_gram_panchayat Where GP_Block='" . $PanchayatCode . "'");

        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$GramPanchayat = array();
			while ($row = $result->fetch_assoc()) {
				$GramPanchayat[] = $row;
			}
            $stmt->close();

            return $GramPanchayat;
			
        } else {
            return NULL;
        }
    }
	public function GetVillageList($gram) {

        $stmt = $this->conn->prepare("Select Village_Code,Village_Name From tbl_village_master where Village_Status='1' AND Village_GP='" . $gram . "'");

        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$Village = array();
			while ($row = $result->fetch_assoc()) {
				$Village[] = $row;
			}
            $stmt->close();

            return $Village;
			
        } else {
            return NULL;
        }
    }

	public function GetCenterList($CourseCode,$DistrictCode,$TehsilCode,$AreaType,$MunicipalityCode,$WardCode,$SamitiCode,$GramCode,$VillageCode) {
		$_SelectQuery = "";
		if($CourseCode=='1'){
					$_Course='RS-CIT';
		}
		else if($CourseCode=='5'){
			$_Course='RS-CFA';
		}
		if(isset($AreaType) && $AreaType != ''){
			if($AreaType =='Urban'){

				
			  $_SelectQuery = "Select Organization_User, Organization_Name, b.User_EmailId, b.User_MobileNo ,
						d.District_Name, e.Tehsil_Name, b.User_LoginId,a.Organization_Address , COALESCE(f.latitude,COALESCE(Organization_lat,0)) AS latitude, COALESCE(f.longitude,COALESCE(Organization_long,0)) AS longitude From tbl_organization_detail as a join tbl_user_master as b on a.Organization_User = b.User_Code
						join tbl_courseitgk_mapping as c on c.Courseitgk_ITGK = b.User_LoginId join
						tbl_district_master as d on d.District_Code = a.Organization_District join 
						tbl_tehsil_master as e on e.Tehsil_Code = a.Organization_Tehsil left join tbl_centerlocation as f on f.ITGK_Code = b.User_LoginId 
						Where b.User_Status = '1' AND b.User_UserRoll = '7' AND Courseitgk_Course='" . $_Course ."'
						AND CourseITGK_BlockStatus='unblock' AND Organization_State = '29'
						AND Organization_District = '".$DistrictCode."' AND 
						EOI_Fee_Confirm='1' AND CURDATE() >= CourseITGK_StartDate AND 
						CURDATE() <= CourseITGK_ExpireDate
						AND Organization_AreaType = '".$AreaType."'
						AND Organization_Municipal = '".$MunicipalityCode."'";
				
				if(isset($WardCode) && $WardCode != '' && $WardCode != '0'){
					$_SelectQuery .= " AND Organization_WardNo = '".$WardCode."'";
				}
					$_SelectQuery .= " order by Organization_Name ASC";
			}
			else if($AreaType == 'Rural'){
				$_SelectQuery = "Select Organization_User, Organization_Name, b.User_EmailId, b.User_MobileNo ,
						d.District_Name, e.Tehsil_Name, b.User_LoginId,a.Organization_Address , COALESCE(f.latitude,COALESCE(Organization_lat,0)) AS latitude, COALESCE(f.longitude,COALESCE(Organization_long,0)) AS longitude From tbl_organization_detail as a join tbl_user_master as b on a.Organization_User = b.User_Code
						join tbl_courseitgk_mapping as c on c.Courseitgk_ITGK = b.User_LoginId join
						tbl_district_master as d on d.District_Code = a.Organization_District join 
						tbl_tehsil_master as e on e.Tehsil_Code = a.Organization_Tehsil 
						left join tbl_centerlocation as f on f.ITGK_Code = b.User_LoginId 
						Where b.User_Status = '1' AND b.User_UserRoll = '7' AND Courseitgk_Course='" . $_Course ."'
						AND CourseITGK_BlockStatus='unblock' AND Organization_State = '29'
						AND Organization_District = '".$DistrictCode."' AND 
						EOI_Fee_Confirm='1' AND CURDATE() >= CourseITGK_StartDate AND 
						CURDATE() <= CourseITGK_ExpireDate
						AND Organization_AreaType = '".$AreaType."' AND 
						Organization_Panchayat = '".$SamitiCode."'";
				if(isset($GramCode) && $GramCode != '' && $GramCode != 0){
					$_SelectQuery .= " AND Organization_Gram = '".$GramCode."'";
				}
				if(isset($VillageCode) && $VillageCode != '' && $VillageCode != 0){
					$_SelectQuery .= " AND Organization_Village = '".$VillageCode."'";
				}
					$_SelectQuery .= " order by Organization_Name ASC";
			}
		}
		
        $stmt = $this->conn->prepare($_SelectQuery);
        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$Centers = array();
			while ($row = $result->fetch_assoc()) {
				$Centers[] = $row;
			}
            $stmt->close();

            return $Centers;
			
        } else {
            return NULL;
        }
    }
	public function GetSPList($DistrictCode) {

        $stmt = $this->conn->prepare("Select a.Rsptarget_Code, a.Rsptarget_User, a.Rsptarget_contactperson, a.Contactperson_mobile, a.Contactperson_email , b.User_EmailId ,b.User_MobileNo, c.Organization_Name,c.Org_formatted_address from tbl_rsptarget as a Join tbl_user_master as b on a.Rsptarget_User = b.User_LoginId join tbl_organization_detail as c on c.Organization_User = b.User_Code where Rsptarget_District = '".$DistrictCode."' AND Rsptarget_Status = 'Approved' order by c.Organization_Name ASC");

        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$SPs = array();
			while ($row = $result->fetch_assoc()) {
				$SPs[] = $row;
			}
            $stmt->close();

            return $SPs;
			
        } else {
            return NULL;
        }
    }
    /**
     * Check user is existed or not
     */
    public function isUserExisted($email) {
        $stmt = $this->conn->prepare("SELECT email from users WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }
    public function isCenterExisted($ITGK_Code) {
		$stmt = $this->conn->prepare("SELECT * FROM tbl_centerlocation WHERE ITGK_Code = ?");
        $stmt->bind_param("s", $ITGK_Code);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    /**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     */
    public function hashSSHA($password) {

        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

    /**
     * Decrypting password
     * @param salt, password
     * returns hash string
     */
    public function checkhashSSHA($salt, $password) {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);

        return $hash;
    }
	public function GET_LEARNER_OTP($_LearnerCode) {
		$stmt = $this->conn->prepare("Select * From tbl_admission Where Admission_LearnerCode='" . $_LearnerCode . "'");

        if ($stmt->execute()) {
			
			$Learner = $stmt->get_result()->fetch_assoc();
			
            $stmt->close();

            return $Learner;
			
        } else {
            return NULL;
        }
	}
	public function SEND_LEARNER_OTP($_LearnerCode, $_OTP,$User_device_id ) {
		$stmt = $this->conn->prepare("Select * From tbl_admission as a join tbl_ao_learner_register as b on a.Admission_Mobile = b.AO_Mobile Where Admission_LearnerCode='".$_LearnerCode."' AND b.AO_Status = 0 AND AO_Device = 'app' ");

        if ($stmt->execute()) {
			
			$Learner = $stmt->get_result()->fetch_assoc();
			
            $stmt->close();
			
			$TableOtp = $Learner['AO_OTP'];
			$TableOtp_ID = $Learner['AO_Code'];
			
			if($TableOtp == $_OTP){
				
				
				$stmt2 = $this->conn->prepare("DELETE FROM tbl_ao_learner_register WHERE AO_Code = '".$TableOtp_ID."'");
				$stmt3 = $this->conn->prepare("update tbl_admission SET User_device_id = '".$User_device_id."' where Admission_LearnerCode='".$_LearnerCode."'");

				$stmt2->execute();
				$stmt2->close();
				
				$stmt3->execute();
				$stmt3->close();
				return $Learner;
			}
			else{
				return NULL;
			}
			
        } else {
            return NULL;
        }
	}
	public function AddLernerMob($_Mobile) {
        function OTP($length = 6, $chars = '1234567890') {
            $chars_length = (strlen($chars) - 1);
            $string = $chars{rand(0, $chars_length)};
            for ($i = 1; $i < $length; $i = strlen($string)) {
                $r = $chars{rand(0, $chars_length)};
                if ($r != $string{$i - 1})
                    $string .= $r;
            }
            return $string;
        }
		
		/*function SendSMS($_MobileNo, $_SMS) {
			$request = "";
			$param['method'] = "sendMessage";
			$param['send_to'] = $_MobileNo;
			$param['msg'] = $_SMS;
			$param['userid'] = "2000134086";
			$param['password'] = "Rkcl@123!";
			$param['v'] = "1.1";
			$param['msg_type'] = "UNICODE_TEXT";
			$param['auth_scheme'] = "PLAIN";

			foreach ($param as $key => $val) {
				$request.= $key . "=" . urlencode($val);

				$request.= "&";
			}
			$request = substr($request, 0, strlen($request) - 1);

			$url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?" . $request;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$curl_scraped_page = curl_exec($ch);
			curl_close($ch);
		   // echo $curl_scraped_page;
		}
*/
        $_OTP = OTP();
        //$_SMS = "OTP for your Learner Login in MYRKCL Application is " . $_OTP;
          $_SMS = $_OTP." is the OTP for your Learner Login in MYRKCL Application.";


        try {
            /* Here it checking that the OTP in which we are sending that is allredy in our database or not.*/
			
			$stmt = $this->conn->prepare("Select * FROM tbl_ao_learner_register WHERE AO_Mobile = '" . $_Mobile . "' AND AO_Status = '0' AND AO_Device = 'app'");
			if ($stmt->execute()) {
				$result = $stmt->get_result();

				/* Get the number of rows */
				$num_of_rows = $result->num_rows;
				
				if ($num_of_rows > 0) {
					$LearnerOtp = $result->fetch_assoc();
					$stmt->close();
					
					$old_OTP=$LearnerOtp['AO_OTP'];
					//$_OLD_SMS = "OTP for your Learner Login in MYRKCL Application is  " . $old_OTP;
					
					$_OLD_SMS = $old_OTP. "is the OTP for your Learner Login in MYRKCL Application.";
					
					SendSMS($_Mobile, $_OLD_SMS);
					return true;
				} else {
					$stmt2 = $this->conn->prepare("Insert Into tbl_ao_learner_register(AO_Code,AO_Mobile,AO_OTP, AO_Device) Select Case When Max(AO_Code) Is Null Then 1 Else Max(AO_Code)+1 End as AO_Code, '" . $_Mobile . "' as AO_Mobile,'" . $_OTP . "' as AO_OTP, 'app' as AO_Device From tbl_ao_learner_register");
					$stmt2->execute();
					$stmt2->close();
					$stmt->close();
					
					SendSMS($_Mobile, $_SMS);
					return false;
				}
            } else {
            return NULL;
        }
            
        } catch (Exception $_e) {
            $_Response[0] = $_e->getTraceAsString();
            $_Response[1] = Message::Error;
            return $_Response;
        }
        
    }
	public function GetAllLearner($_LearnerCode) {
//		$stmt = $this->conn->prepare("SELECT a.*, b.Course_Name, c.Batch_Name, c.Batch_StartDate, d.* 
//                                                    FROM tbl_admission a
//                                                    LEFT JOIN tbl_course_master b ON a.Admission_Course= b.Course_Code 
//                                                    LEFT JOIN tbl_batch_master c ON a.Admission_Batch = c.Batch_Code
//                                                    LEFT JOIN tbl_result d ON a.Admission_LearnerCode = d.scol_no
//                                                    WHERE a.Admission_LearnerCode='" . $_LearnerCode . "'");
		$stmt = $this->conn->prepare("SELECT a.Admission_Payment_Status,a.Admission_ITGK_Code,a.Admission_Name,a.Admission_Fname,a.Admission_DOB,a.Admission_Mobile,a.Admission_Address,a.Admission_Email,a.Admission_LearnerCode,
                                        a.Admission_Medium,a.Admission_Date_LastModified,a.Admission_Date_Payment,a.Admission_Date,a.Admission_Batch,a.Admission_Photo,a.Admission_Sign, 
                                        b.Course_Name, c.Batch_Name, c.Batch_StartDate, d.exam_date 
                                        FROM tbl_admission a
                                        LEFT JOIN tbl_course_master b ON a.Admission_Course= b.Course_Code 
                                        LEFT JOIN tbl_batch_master c ON a.Admission_Batch = c.Batch_Code
                                        LEFT JOIN tbl_result d ON a.Admission_LearnerCode = d.scol_no
                                        WHERE a.Admission_LearnerCode='" . $_LearnerCode . "'");

        if ($stmt->execute()) {
			
			$Learner = $stmt->get_result()->fetch_assoc();
            $stmt->close();
			
			return $Learner;
			
			
        } else {
            return NULL;
        }
	}
	public function CenterDetails($_actionvalue) {
//		$stmt = $this->conn->prepare("SELECT a.* , b.* 
//                        FROM tbl_user_master a
//                        LEFT JOIN tbl_organization_detail b
//                        ON a.User_Code=b.Organization_User
//                        WHERE a.User_UserRoll=7 AND User_LoginId='" . $_actionvalue . "'");
		$stmt = $this->conn->prepare("SELECT a.User_Code,a.User_LoginId,a.User_EmailId,a.User_MobileNo,
                                        b.Organization_Name,b.Organization_RegistrationNo,b.Organization_Address 
                                        FROM tbl_user_master a
                                        LEFT JOIN tbl_organization_detail b
                                        ON a.User_Code=b.Organization_User
                                        WHERE a.User_UserRoll=7 AND User_LoginId='" . $_actionvalue . "'");

        if ($stmt->execute()) {
			
			$Result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
			
			return $Result;
			
			
        } else {
            return NULL;
        }
	}
	public function SPDetails($_actionvalue) {
//		$stmt = $this->conn->prepare("Select b.*,a.*
//                                FROM tbl_user_master AS a 
//                                INNER JOIN tbl_organization_detail AS b ON a.User_Rsp=b.Organization_User
//                                where a.User_LoginId='".$_actionvalue."'");
		$stmt = $this->conn->prepare("Select b.Organization_FoundedDate,b.Organization_HouseNo,b.Organization_Street,b.Organization_Road,b.Organization_Landmark,b.Org_formatted_address,b.Organization_Name,b.Organization_Address,a.User_Rsp,a.User_EmailId,a.User_MobileNo
                                FROM tbl_user_master AS a 
                                INNER JOIN tbl_organization_detail AS b ON a.User_Rsp=b.Organization_User
                                where a.User_LoginId='".$_actionvalue."'");

        if ($stmt->execute()) {
			
			$Result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
			
			return $Result;
			
			
        } else {
            return NULL;
        }
	}
	public function GetAllLearnerScore($_actionvalue , $_Itgkcode) {
		//$stmt = $this->conn->prepare("SELECT * FROM tbl_learner_score WHERE Learner_Code='" . $_actionvalue . "' AND ITGK_code='" . $_Itgkcode . "'");
		$stmt = $this->conn->prepare("SELECT Learner_Score_Id,Admission_Code,Learner_Code,Score,ITGK_code,AddEditTime 
                        FROM tbl_learner_score WHERE Learner_Code='" . $_actionvalue . "' AND ITGK_code='" . $_Itgkcode . "'");

        if ($stmt->execute()) {
			
			$Result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
			
			return $Result;
			
			
        } else {
            return NULL;
        }
	}
	public function GetLearnerAllMarks($_actionvalue) {
//		$stmt = $this->conn->prepare("SELECT a.*, b.Event_Name FROM tbl_result a
//                                        LEFT JOIN tbl_events b 
//                                        ON a.exameventnameID= b.Event_Id 
//                                        WHERE a.scol_no ='" . $_actionvalue . "'");
		$stmt = $this->conn->prepare("SELECT a.tot_marks,a.exam_event_name,a.exam_date,a.result, 
                                                b.Event_Name FROM tbl_result a
                                                LEFT JOIN tbl_events b 
                                                ON a.exameventnameID= b.Event_Id 
                                                WHERE a.scol_no ='" . $_actionvalue . "'");

        if ($stmt->execute()) {
			
			$Result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
			
			return $Result;
			
			
        } else {
            return NULL;
        }
	}
	public function checkGovtLearnerStatus($_actionvalue, $_ITGK ) {
		$stmt = $this->conn->prepare("Select * from tbl_govempentryform where learnercode='".$_actionvalue."' AND Govemp_ITGK_Code='" . $_ITGK . "' ");

        if ($stmt->execute()) {
			
			$Result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
			
			return $Result;
			
			
        } else {
            return NULL;
        }
	}
	public function GetAllRDETAILS($_actionvalue, $_ITGK ) {
//		$stmt = $this->conn->prepare("SELECT a.*,b.cstatus,c.lotname FROM tbl_govempentryform a
//                                LEFT JOIN tbl_capcategory b ON  a.trnpending = b.capprovalid
//                                LEFT JOIN tbl_clot c ON a.lot = c.lotid 
//                                WHERE a.learnercode='" . $_actionvalue . "' AND a.Govemp_ITGK_Code='" . $_ITGK . "'");
		$stmt = $this->conn->prepare("SELECT a.Govemp_ITGK_Code,a.empid,a.designation,a.officeaddress,a.empgpfno,a.empaccountno,a.gifsccode,a.exammarks,a.aattempt,a.datetime,a.fee,a.incentive,
                                a.incentive,
                                b.cstatus,c.lotname FROM tbl_govempentryform a
                                LEFT JOIN tbl_capcategory b ON  a.trnpending = b.capprovalid
                                LEFT JOIN tbl_clot c ON a.lot = c.lotid 
                                WHERE a.learnercode='" . $_actionvalue . "' AND a.Govemp_ITGK_Code='" . $_ITGK . "'");

        if ($stmt->execute()) {
			
			$Result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
			
			return $Result;
			
			
        } else {
            return NULL;
        }
	}
	
	public function checkcorrectionStatus($_actionvalue, $_ITGK ) {
		$stmt = $this->conn->prepare("SELECT * FROM tbl_correction_copy WHERE lcode='" . $_actionvalue . "' AND Correction_ITGK_Code='" . $_ITGK . "'");

        if ($stmt->execute()) {
			
			$Result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
			
			return $Result;
			
			
        } else {
            return NULL;
        }
	}
	
	public function GetCorrectionDuplicateCertificate($_actionvalue, $_ITGK ) {
//		$stmt = $this->conn->prepare("Select a.*,b.cstatus from tbl_correction_copy a
//                                left join tbl_capcategory b 
//                                on  a.dispatchstatus = b.capprovalid 
//                                where a.lcode='".$_actionvalue."' AND a.Correction_ITGK_Code='" . $_ITGK . "' "
//                                . "order by a.applicationdate desc");
		$stmt = $this->conn->prepare("Select a.applicationfor,a.Correction_Payment_Status,a.Correction_TranRefNo,b.cstatus from tbl_correction_copy a
                                left join tbl_capcategory b 
                                on  a.dispatchstatus = b.capprovalid 
                                where a.lcode='".$_actionvalue."' AND a.Correction_ITGK_Code='" . $_ITGK . "' order by a.applicationdate desc");

        if ($stmt->execute()) {
			
			$Result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
			
			return $Result;
			
			
        } else {
            return NULL;
        }
	}
	
	public function Get_News() {
		$this->conn->prepare('SET character_set_results=utf8');
		$stmt = $this->conn->prepare("select id,value,news,link,loginid,ip,Timestamp,Status from tbl_news_master_app");
		if ($stmt->execute()) {
			$result = $stmt->get_result();
			$num_of_rows = $result->num_rows;
			$news = array();
			while ($row = $result->fetch_assoc()) {
				$news[] = $row;
			}
            $stmt->close();

            return $news;
			
        } else {
            return NULL;
        }
	}
	public function Get_Slides() {
		$stmt = $this->conn->prepare("select * from tbl_sliderimage where status = 'active'");
		if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$slider = array();
			while ($row = $result->fetch_assoc()) {
				$slider[] = $row;
			}
            $stmt->close();

            return $slider;
			
        } else {
            return NULL;
        }
	}
	
	public function GetExamEvent() {
		$stmt = $this->conn->prepare("Select a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id "
                    . " INNER JOIN tbl_event_management as c ON b.Event_Id = c.Event_ReexamEvent WHERE "
                    . " c.Event_Name='6' AND NOW() >= c.Event_Startdate AND NOW() <= c.Event_Enddate");
		if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$ReExamEvent = array();
			while ($row = $result->fetch_assoc()) {
				$ReExamEvent[] = $row;
			}
            $stmt->close();

            return $ReExamEvent;
			
        } else {
            return NULL;
        }
	}
	public function AddLearnerReexamApp($Admission_Codes, $_EventID, $User_Role){
		
		foreach ($Admission_Codes as $code){
			$stmt = $this->conn->prepare("SELECT Admission_LearnerCode, Admission_Name, Admission_Fname, Admission_DOB, Admission_Course, Admission_Batch, Admission_ITGK_Code, Admission_District, Admission_Tehsil FROM tbl_admission WHERE Admission_LearnerCode = '" . $code . "'");
			if ($stmt->execute()) {
				$result = $stmt->get_result();

				/* Get the number of rows */
				$num_of_rows = $result->num_rows;
				$Learner = $result->fetch_assoc();
				
				
				$stmt2 = $this->conn->prepare("INSERT INTO examdata (examid, learnercode, learnername, fathername, dob, coursename, batchname, itgkcode, itgkname, itgkdistrict, itgktehsil, remark, status, paymentstatus, learnertype, reexamapplyby) VALUES ('" . $_EventID . "', '" . $Learner['Admission_LearnerCode'] . "', '" . $Learner['Admission_Name'] . "', '" . $Learner['Admission_Fname'] . "', '" . $Learner['Admission_DOB'] . "', '" . $Learner['Admission_Course'] . "', '" . $Learner['Admission_Batch'] . "', '" . $Learner['Admission_ITGK_Code'] . "', '1', '" . $Learner['Admission_District'] . "', '" . $Learner['Admission_Tehsil'] . "', 'Active', '1', '0', 'reexam', '" . $User_Role. "')");
				$stmt2->execute();
				$stmt2->close();
				$stmt->close();

				return $Learner;
				
			} else {
				return NULL;
			}
		}
	}
	public function GetLearnerCourseBatch($learnerCode) {
		$stmt = $this->conn->prepare("SELECT Admission_Course, Admission_Batch FROM tbl_admission WHERE Admission_LearnerCode = '" . $learnerCode . "'");
		if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$ReExamEventCourses = $result->fetch_assoc();
			// while ($row = $result->fetch_assoc()) {
				// $ReExamEventCourses = $row;
			// }
            $stmt->close();

            return $ReExamEventCourses;
			
        } else {
            return NULL;
        }
	}
	public function GetAllReexam($batch, $course, $examevent, $learnerCode = '',$_ITGK_Code) {
		if($learnerCode){
			$stmt = $this->conn->prepare("Select Course_Name from tbl_course_master where Course_Code='" . $course . "'");
			if ($stmt->execute()) {
				$result = $stmt->get_result();

				/* Get the number of rows */
				$num_of_rows = $result->num_rows;
				$row = $result->fetch_assoc();
				$courseName = $row['Course_Name'];
				$stmt->close();
				$stmt2 = $this->conn->prepare("SELECT a.Admission_Payment_Status,a.Admission_LearnerCode,a.Admission_Code,a.Admission_Course,a.Admission_Batch,a.Admission_Name,a.Admission_Fname,a.Admission_DOB, b.Course_Name as coursename, $examevent AS exameventid FROM tbl_admission AS a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code INNER JOIN tbl_learner_score ls ON ls.Learner_Code = a.Admission_LearnerCode WHERE a.Admission_Batch = '" . $batch . "' AND Admission_ITGK_Code='" . $_ITGK_Code . "' AND a.Admission_Payment_Status = '1' AND ls.Score >= 11 AND a.Admission_LearnerCode NOT IN (SELECT distinct scol_no FROM tbl_result WHERE result LIKE 'Pass') AND a.Admission_LearnerCode = '" . $learnerCode . "' ORDER BY a.Admission_Name");
				//$stmt2 = $this->conn->prepare("SELECT a.*, b.Course_Name as coursename, $examevent AS exameventid FROM tbl_admission AS a inner join tbl_course_master as b on a.Admission_Course=b.Course_Code INNER JOIN tbl_learner_score ls ON ls.Learner_Code = a.Admission_LearnerCode WHERE a.Admission_Batch = '" . $batch . "' AND Admission_ITGK_Code='" . $_ITGK_Code . "' AND a.Admission_Payment_Status = '1' AND ls.Score >= 11 AND a.Admission_LearnerCode NOT IN (SELECT distinct scol_no FROM tbl_result WHERE result LIKE 'Pass') AND a.Admission_LearnerCode = '" . $learnerCode . "' ORDER BY a.Admission_Name");
				if ($stmt2->execute()) {
					$result2 = $stmt2->get_result();

					/* Get the number of rows */
					$num_of_rows = $result2->num_rows;
					$ReExamEventCourses = array();
					while ($row = $result2->fetch_assoc()) {
						$ReExamEventCourses[] = $row;
					}
					$stmt2->close();

					return $ReExamEventCourses;
					
				} else {
					return NULL;
				}
				
			} else {
				return NULL;
			}
		}
	}
	public function CheckIfAlreadyAppliedForReExam($learnerCode, $ExamEvent) {
		$stmt = $this->conn->prepare("SELECT * FROM examdata WHERE examid = '" . $ExamEvent . "' AND learnercode = '" . $learnerCode . "'");
		if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$ReExamEvent = $result->fetch_assoc();
            $stmt->close();

            return $ReExamEvent;
			
        } else {
            return NULL;
        }
	}
	public function GetReexamBatch($_ExamEvent, $_CourseCode, $batchId = 0) {
		$stmt = $this->conn->prepare("Select Affilate_ReexamBatches from tbl_exammaster where Affilate_Event = '" . $_ExamEvent . "'");
		if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$ReExamEvent = $result->fetch_assoc();
			$_Batch = $ReExamEvent['Affilate_ReexamBatches'];
            $stmt->close();
			
			echo $filter = ($batchId) ? " AND b.Batch_Code = '" . $batchId . "'" : '';

            $_SelectQuery = "Select a.Affilate_ReexamBatches, b.Batch_Name, b.Batch_Code, c.Course_Code FROM tbl_exammaster"
                    . " as a INNER JOIN tbl_batch_master as b ON b.Batch_Code IN ($_Batch) inner join tbl_course_master"
                    . " as c on c.Course_Code= b.Course_Code  where c.Course_Code = '" . $_CourseCode . "' and"
                    . " a.Affilate_Event = '" . $_ExamEvent . "' " . $filter;

            $stmt2 = $this->conn->prepare($_SelectQuery);
			if ($stmt2->execute()) {
				$result = $stmt2->get_result();

				/* Get the number of rows */
				$num_of_rows = $result->num_rows;
				$ReExamBatch = $result->fetch_assoc();
				$stmt->close();

				return $ReExamBatch;
				
			} else {
				return NULL;
			}
			
        } else {
            return NULL;
        }
	}
	public function GetExamEventPayment() {
		$stmt = $this->conn->prepare("Select a.Affilate_Event, b.Event_Name FROM tbl_exammaster as a INNER JOIN tbl_events as b ON a.Affilate_Event = b.Event_Id INNER JOIN tbl_event_management as c ON b.Event_Id = c.Event_ReexamEvent WHERE  c.Event_Name='6' AND NOW() >= c.Event_Startdate AND NOW() <= c.Event_Enddate");
		if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$ReExamEvent = array();
			while ($row = $result->fetch_assoc()) {
				//if($row['Affilate_Event'] != 'null' || $row['Affilate_Event'] != 0){
					$stmt2 = $this->conn->prepare("Select a.Event_Payment, b.payment_mode From tbl_event_management AS a INNER JOIN tbl_payment_mode AS b ON a.Event_Payment = b.payment_id WHERE a.Event_ReexamEvent = '" . $row['Affilate_Event'] . "' AND Event_Name='7' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate");
					if ($stmt2->execute()) {
						$result2 = $stmt2->get_result();

						/* Get the number of rows */
						$num_of_rows = $result2->num_rows;
						$ReExamEventStatus = $result2->fetch_assoc();
						if($ReExamEventStatus){
							$ReExamEvent[] = $row;
						}
						$stmt2->close();				
					}		
				//}
			}
            $stmt->close();

            return $ReExamEvent;
			
        } else {
            return NULL;
        }
	}
	public function Get_ShowAllLearnerToPay($paymode, $course, $event,$LearnerCode, $User_Role,$Itgk) {
		$stmt = $this->conn->prepare("SELECT Event_Payment FROM tbl_event_management WHERE Event_ReexamEvent = '" . $event . "' AND Event_Payment = '" . $paymode . "' AND Event_Name = '7' AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate");
		if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$_getEvent = $result->fetch_assoc();
			
            $stmt->close();
			
			$filter = ($_getEvent['Event_Payment'] == '2') ? "AND coursename = '" . $course . "'" : "AND examid = '" . $event . "'" ;
			$filter .= (isset($User_Role) && $User_Role == 19) ? " AND a.learnercode='" . $LearnerCode . "'" : '';
			$stmt2 = $this->conn->prepare("SELECT a.*, b.Batch_Name, tr.ReExam_fee as fee FROM examdata as a INNER JOIN tbl_batch_master as b ON a.batchname = b.Batch_Code INNER JOIN tbl_exammaster ex ON ex.Affilate_Event = a.examid INNER JOIN tbl_reexamfeemaster tr ON tr.ReExam_Id = ex.Affilate_Fee WHERE itgkcode = '" . $Itgk . "' AND learnertype = 'reexam' AND paymentstatus = '0' " . $filter . " ORDER BY a.learnername");
			if ($stmt2->execute()) {
				$result2 = $stmt2->get_result();

				/* Get the number of rows */
				$num_of_rows = $result2->num_rows;
				$ReExamLearnersPay = array();
				while ($row = $result2->fetch_assoc()) {
					$ReExamLearnersPay[] = $row;
				}
				$stmt2->close();

				return $ReExamLearnersPay;
				
			} else {
				return NULL;
			}
            return $ReExamEvent;
			
        } else {
            return NULL;
        }
	}
        
        public function Fatchallnotification($user) {
            
		$stmt = $this->conn->prepare("select N_id,Ntittle,Ntype,Nuserid,Nmessage,Nstatus,Ntimestamp from tbl_appnotification where Nuserid='".$user."'");
		if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$GetnotifyAll = array();
                        while ($row = $result->fetch_assoc()) {
                                $GetnotifyAll[] = $row;
                        }
                        $stmt->close();

                        return $GetnotifyAll;
            
                } else {
                    return NULL;
                }
                
	}
        
        public function UpdatenotifyStatus($notification_id) {
            
            $stmt = $this->conn->prepare("update tbl_appnotification set Nstatus='2' where N_id='".$notification_id."'");
            $stmt->execute();
            $res  = mysqli_affected_rows($this->conn);
            $stmt->close();
            return $res;
            
	}
        public function GetStatusBio($itgk_code) {
            
            $stmt = $this->conn->prepare("select BioMatric_Register_Code,BioMatric_SerailNo,BioMatric_Make,BioMatric_Model,BioMatric_Local_IP,BioMatric_Status,BioMatric_Flag from tbl_biomatric_register where BioMatric_ITGK_Code='".$itgk_code."'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getstatus = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getstatus[] = $row;
                    }
                    $stmt->close();

                    return $Getstatus;
            
                } else {
                    return NULL;
                }
            
	}
        
        public function MachineRegisterBio($BioMatric_ITGK_Code,$BioMatric_SerailNo,$BioMatric_Make,$BioMatric_Model,
                        $BioMatric_Width,$BioMatric_Height,$BioMatric_Local_MAC,$BioMatric_Local_IP,$BioMatric_Status,
                        $BioMatric_Reason,$BioMatric_Flag) {
         
						
				//As per 19-06-19 no need to check Biometric m/c duplicacy..
				
          $stmt = $this->conn->prepare("select BioMatric_Register_Code from tbl_biomatric_register where 
										BioMatric_SerailNo='".$BioMatric_SerailNo."' and BioMatric_Flag='1' 
										AND BioMatric_ITGK_Code='".$BioMatric_ITGK_Code."'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $stmt->close();
                    if($num_of_rows>0){
                        return 0;
                    }else{
                        $stmt = $this->conn->prepare("INSERT INTO tbl_biomatric_register(BioMatric_ITGK_Code,BioMatric_SerailNo,BioMatric_Make,BioMatric_Model,BioMatric_Width,BioMatric_Height,BioMatric_Local_MAC,BioMatric_Local_IP,BioMatric_Status,BioMatric_Reason,BioMatric_Flag,Attendance_Device) VALUES('".$BioMatric_ITGK_Code."','".$BioMatric_SerailNo."','".$BioMatric_Make."','".$BioMatric_Model."','".$BioMatric_Width."','".$BioMatric_Height."','".$BioMatric_Local_MAC."','".$BioMatric_Local_IP."','".$BioMatric_Status."','".$BioMatric_Reason."','".$BioMatric_Flag."','App')");
                        $result = $stmt->execute();
                        $stmt->close();
                        return 1;
                    }
            
                } else {
                    return NULL;
                }
            
	}
        
      public function Fill_course_app($itgk_code) {
            
            $stmt = $this->conn->prepare("select distinct a.Course_Code,a.Course_Name from tbl_course_master as a inner join tbl_courseitgk_mapping as b on a.Course_Name=b.Courseitgk_Course where Courseitgk_ITGK='".$itgk_code."' AND CourseITGK_BlockStatus='unblock'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getcourse = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getcourse[] = $row;
                    }
                    $stmt->close();

                    return $Getcourse;
            
                } else {
                    return NULL;
                }
            
	}
     /* public function Fill_batch_app($Course_Code) {
            
            $stmt = $this->conn->prepare("select Batch_Code,Batch_Name from tbl_batch_master where Course_Code='".$Course_Code."'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getbatch = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getbatch[] = $row;
                    }
                    $stmt->close();

                    return $Getbatch;
            
                } else {
                    return NULL;
                }
            
	} */
	
	public function Fill_batch_app($Course_Code) {
            
            $stmt = $this->conn->prepare("select Batch_Code,Batch_Name from tbl_batch_master where Course_Code='".$Course_Code."'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getbatch = array();
                    while ($row = $result->fetch_assoc()) {
						if($row["Batch_Code"] == '236' || $row["Batch_Code"] == '237' || $row["Batch_Code"] == '232' || $row["Batch_Code"] == '233'){
							
						}else{
							$Getbatch[] = $row;
						}
                            
                    }
                    $stmt->close();

                    return $Getbatch;
            
                } else {
                    return NULL;
                }
            
	}
      public function Learner_list_attendence_Enrollment($itgk_code,$Course_Code,$Batch_Code) {
            
            $stmt = $this->conn->prepare("select Admission_Code,Admission_LearnerCode,Admission_Name,Admission_Fname,Admission_DOB,Admission_ITGK_Code,Admission_RspName,Admission_Photo,Admission_Mobile from tbl_admission where Admission_Course='".$Course_Code."' AND Admission_Batch='".$Batch_Code."' AND Admission_ITGK_Code='".$itgk_code."' AND Admission_Payment_Status='1' AND BioMatric_Status='0'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getbatch = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getbatch[] = $row;
                    }
                    $stmt->close();

                    return $Getbatch;
            
                } else {
                    return NULL;
                }
            
	}
        
      public function Learner_attendence_Enrollment($itgk_code,$Course_Code,$Batch_Code,
                        $Admission_Code,$Admission_LearnerCode,$Admission_Name,$Admission_Fname,$Admission_DOB,$Admission_RspName,
                        $Admission_Photo,$BioMatric_Quality,$BioMatric_NFIQ,$BioMatric_ISO_Template,
                        $BioMatric_ISO_Image,$BioMatric_Raw_Data,$BioMatric_WSQ_Image,$Admission_Mobile) {
            $stmt = $this->conn->prepare("SELECT IFNULL(MAX(BioMatric_Admission_BioPIN),0) AS biopin FROM tbl_biomatric_registration WHERE BioMatric_ITGK_Code='" . $itgk_code . "'");
            if ($stmt->execute()) {
                $result = $stmt->get_result();
                $Re = $result->fetch_assoc();
                if ($Re["biopin"] == 0) {
                    $biopin = '1000';
                } else {
                    $biopin = $Re['biopin'] + 1;
                }
                $stmt->close();
            $stmtcheck = $this->conn->prepare("select BioMatric_Code from tbl_biomatric_registration where BioMatric_ITGK_Code='".$itgk_code."' AND BioMatric_Admission_LCode='".$Admission_LearnerCode."'");
            if ($stmtcheck->execute()) {
               $resultcheck = $stmtcheck->get_result();
               $num_of_rows = $resultcheck->num_rows;
                if($num_of_rows > '0'){
                    $result = "Already Anroll";
                    return $result;
                }else{
                    $stmt1 = $this->conn->prepare("INSERT INTO tbl_biomatric_registration(BioMatric_Code,BioMatric_ITGK_Code,BioMatric_Admission_Code,BioMatric_Admission_Course,BioMatric_Admission_Batch,BioMatric_Quality,"
                                . "BioMatric_NFIQ,BioMatric_ISO_Template,BioMatric_ISO_Image,BioMatric_Raw_Data,BioMatric_WSQ_Image,BioMatric_Admission_BioPIN, "
                                . "BioMatric_Admission_LCode,BioMatric_Admission_Name,BioMatric_Admission_Fname,BioMatric_Admission_DOB,BioMatric_Admission_Photo, "
                                . "BioMatric_Admission_RspName,Attendance_Device) "
                                . "SELECT CASE WHEN Max(BioMatric_Code) IS NULL THEN 1 ELSE Max(BioMatric_Code)+1 END AS BioMatric_Code,"
                                . "'" . $itgk_code . "' AS BioMatric_ITGK_Code,'" . $Admission_Code . "' AS BioMatric_Admission_Code,"
                                . "'" . $Course_Code . "' AS BioMatric_Admission_Course,'" . $Batch_Code . "' AS BioMatric_Admission_Batch,'" . $BioMatric_Quality . "' AS BioMatric_Quality,'" . $BioMatric_NFIQ . "' AS BioMatric_NFIQ,"
                                . "'" . $BioMatric_ISO_Template . "' AS BioMatric_ISO_Template,'" . $BioMatric_ISO_Image . "' AS BioMatric_ISO_Image,'" . $BioMatric_Raw_Data . "' AS BioMatric_Raw_Data,'" . $BioMatric_WSQ_Image . "' AS BioMatric_WSQ_Image,"
                                . "'" . $biopin . "'AS BioMatric_Admission_BioPIN,"
                                . "'" . $Admission_LearnerCode . "' AS BioMatric_Admission_LCode,'" . $Admission_Name . "' AS BioMatric_Admission_Name,"
                                . "'" . $Admission_Fname . "' AS BioMatric_Admission_Fname,'" . $Admission_DOB . "' AS BioMatric_Admission_DOB,'" . $Admission_Photo . "' AS BioMatric_Admission_Photo, "
                                . "'" . $Admission_RspName . "' AS BioMatric_Admission_RspName,'App' as Attendance_Device"
                                . " FROM tbl_biomatric_registration");
                    if ($stmt1->execute()) {
                        $result1 = $stmt1->get_result();
                        $stmt1->close();
                        $stmt2 = $this->conn->prepare("SELECT * FROM tbl_biomatric_registration WHERE BioMatric_Admission_Code='" . $Admission_Code . "'");
                        if ($stmt2->execute()) {
                            $result2 = $stmt2->get_result();
                            $stmt2->close();
                            $stmt3 = $this->conn->prepare("UPDATE tbl_admission SET BioMatric_Status='1' WHERE Admission_Code = '" . $Admission_Code . "' AND Admission_LearnerCode = '" . $Admission_LearnerCode . "' AND Admission_ITGK_Code = '" . $itgk_code . "'");
                            if ($stmt3->execute()) {
                                $result3 = $stmt3->get_result();
                                $stmt3->close();
                                $_SMS = "Dear " . $Admission_Name . ", your bio-pin for attendance is " . $biopin;
                            
                                SendSMS($Admission_Mobile, $_SMS);
                                $result = "Enroll Success";
                                return $result;
                            }else{
                                $stmt4 = $this->conn->prepare("UPDATE tbl_admission SET BioMatric_Status='0' WHERE Admission_Code = '" . $Admission_Code . "' AND Admission_LearnerCode = '" .$Admission_LearnerCode . "' AND Admission_ITGK_Code = '" . $itgk_code . "'");
                                if ($stmt4->execute()) {
                                    $result4 = $stmt4->get_result();
                                    $stmt4->close();
                                    $result = "Error to enroll";
                                    return $result;
                                }
                            }
                        }
                        else{
                                $stmt5 = $this->conn->prepare("UPDATE tbl_admission SET BioMatric_Status='0' WHERE Admission_Code = '" . $Admission_Code . "' AND Admission_LearnerCode = '" .$Admission_LearnerCode . "' AND Admission_ITGK_Code = '" . $itgk_code . "'");
                                if ($stmt5->execute()) {
                                    $result5 = $stmt5->get_result();
                                    $stmt5->close();
                                    $result = "Error to enroll";
                                    return $result;
                                }
                            }
                    }
                }  
            }

            } else {
                return NULL;
            }
            
	}

        public function Learner_detail_mark_attendence($itgk_code,$BioMatric_Admission_BioPIN) {
            
            $stmt = $this->conn->prepare("SELECT a.*, b.Course_Name, c.Batch_Name FROM tbl_biomatric_registration AS a INNER JOIN tbl_course_master AS b 
                                ON a.BioMatric_Admission_Course = b.Course_Code INNER JOIN tbl_batch_master AS c ON a.BioMatric_Admission_Batch = c.Batch_Code 
                                WHERE BioMatric_Admission_BioPIN='".$BioMatric_Admission_BioPIN."' AND BioMatric_ITGK_Code='".$itgk_code."'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getlearner = array();
                    $row = $result->fetch_assoc();
                    if($num_of_rows>0){
                    $Getlearner[] = $row;
                    $stmt->close();
                    return $Getlearner;
                    }
                } else {
                    return NULL;
                }
            
	}
        public function Learner_mark_attendence($Attendance_ITGK_Code,$Attendance_Admission_Code,
                        $Attendance_Quality,$Attendance_NFIQ) {
            
            $stmt = $this->conn->prepare("INSERT INTO tbl_learner_attendance(Attendance_Code,Attendance_ITGK_Code,Attendance_Admission_Code,Attendance_Quality,"
                    . "Attendance_NFIQ,Attendance_In_Time,Attendance_Device) "
                    . "SELECT CASE WHEN Max(Attendance_Code) IS NULL THEN 1 ELSE Max(Attendance_Code)+1 END AS Attendance_Code,"
                    . "'" . $Attendance_ITGK_Code . "' AS Attendance_ITGK_Code,'" . $Attendance_Admission_Code . "' AS Attendance_Admission_Code,"
                    . "'" . $Attendance_Quality . "' AS Attendance_Quality,'" . $Attendance_NFIQ . "' AS Attendance_NFIQ,"
                    . "  CURRENT_TIMESTAMP AS Attendance_In_Time,'App' as Attendance_Device"
                    . " FROM tbl_learner_attendance");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $stmt->close();
                    $status = "Success";
                    return $status;
            
                } else {
                    return NULL;
                }
            
	}
        
        public function Learner_finger_match($BioMatric_Admission_Course,$BioMatric_Admission_Batch,$BioMatric_ITGK_Code) {
                $stmt1 = $this->conn->prepare("Select Batch_Name, Batch_Code FROM tbl_batch_master WHERE
						Course_Code = '".$BioMatric_Admission_Course."' ORDER BY Batch_Code DESC LIMIT 4");
                if ($stmt1->execute()){
					$result = $stmt1->get_result();
					$batches = array();
					$Getbatches='';
					while ($_Row = $result->fetch_assoc()) {
                            $Getbatches .= $_Row['Batch_Code'].",";
                    }
					$batches = rtrim($Getbatches, ',');
				                
            $stmt = $this->conn->prepare("select BioMatric_ISO_Template from tbl_biomatric_registration where BioMatric_Admission_Course='".$BioMatric_Admission_Course."' AND BioMatric_Admission_Batch in($batches) AND BioMatric_ITGK_Code='".$BioMatric_ITGK_Code."'");
			
			if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
		}
		else{
			return NULL;
		}
            
	}
	  /*** CODE ADDED BY SIDDHARTHA STARTS HERE ***/

        /***
         * @function: getAndroidVersion
         * @param : none
         * @return mixed/array
         * @Description : returns a single row fetched from database
         * as array containing the latest version code, name and url of android application
         ***/

        public function getAndroidVersion()
        {
            $resultArray = [];
            $query = "SELECT current_version_code, current_version_name, current_app_url FROM tbl_android_version ORDER BY fld_updatedOn DESC LIMIT 1";
            $stmt = $this->conn->prepare($query);
            if ($stmt->execute()) {
                $result = $stmt->get_result();
                if ($result->num_rows > 0) {
                    $resultArray = $result->fetch_assoc();
                    $resultArray["status"] = "SUCCESS";
                } else {
                    $resultArray = [
                        "status"    => "ERROR",
                        "error_msg" => "No Record Found"
                    ];
                }
            }
            $stmt->close();
            return $resultArray;
        }

        /***
         * @function: insertAndroidUserData
         * @param array
         * @return mixed/array
         * @Description : inserts the user data in android user data table and checks for duplicate
         * entry based upon line 1 or line 2 and imei number 1 or 2
         ***/

        public function insertAndroidUserData($postedArray)
        {
            $resultArray = [];
            $insertQuery = "INSERT INTO tbl_android_user_data(service_provider, line_1_number, line_2_number, sim_id, imei_1_number, imei_2_number, fld_firebase_token, fld_addedOn) VALUES (?,?,?,?,?,?,?,NOW())";

            $stmt2 = $this->conn->prepare($insertQuery);
            $stmt2->bind_param("sssssss", $postedArray["service_provider"], $postedArray["line_1_number"], $postedArray["line_2_number"], $postedArray["sim_id"], $postedArray["imei_1_number"], $postedArray["imei_2_number"], $postedArray["fld_firebase_token"]);

            $result2 = $stmt2->execute();

            if ($result2) {
                $resultArray = [
                    "status"     => "SUCCESS",
                    "status_msg" => "Successfully Inserted"
                ];
            }
            $stmt2->close();
            return $resultArray;


        }
public function Learner_scane_data($Mobile,$Learner) {
            
           $stmt = $this->conn->prepare("insert into tbl_scane_certificate (Scan_Mobile,Scan_Learner)values('".$Mobile."','".$Learner."')");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $stmt->close();
                    $status = "Success";
                    return $status;
            
                } else {
                    return NULL;
                }
	}

        /*** CODE ADDED BY SIDDHARTHA ENDS HERE ***/

 public function getUserByItgkAndPasswordNEW($Itgk, $password, $User_device_id) {

        //$stmt = $this->conn->prepare("select User_Code,User_EmailId,User_MobileNo,User_LoginId,User_Password,User_UserRoll from tbl_user_master where User_LoginId = ? and User_Status = '1' ");
        function encryptPasswordLogin($password, $hashkey) {
                    return md5(sha1($password . $hashkey));
            }
        $stmt = $this->conn->prepare("Select a.User_Code,a.User_EmailId,a.User_MobileNo,a.User_LoginId,
            a.User_UserRoll,a.User_ParentId,a.User_Rsp,a.User_Status,
            b.UserRoll_Name, 
            c.Organization_Name,c.Organization_District 
            FROM tbl_user_master as a 
            INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
            INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
            WHERE a.User_LoginId = ? and a.User_Status = 1");
        
        $stmt->bind_param("s", $Itgk);
	
        if ($stmt->execute()) 
            {
			
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            // verifying user password
            //echo "<pre>"; print_r($user);die;
            if(count($user) >0)
                {
                    //echo "<pre>"; print_r($user);
                        //* now the checking the record in the new table. if yes then check the password. if password is right then good...other wise error. *//
                        
                        $stmt2 = $this->conn->prepare("Select a.User_Code,a.User_EmailId,a.User_MobileNo,a.User_LoginId,
                        a.User_UserRoll,a.User_ParentId,a.User_Rsp,a.User_Status,
                        b.UserRoll_Name, 
                        c.Organization_Name,c.Organization_District,
                        d.Open_Sesame,d.Open_Sesame_En,d.Phrase_Ke,d.Open_Sesame_Update_Date,
                        e.Phrase_Ke
                        FROM tbl_user_master as a 
                        INNER JOIN tbl_userroll_master as b ON a.User_UserRoll = b.UserRoll_Code 
                        INNER JOIN tbl_organization_detail as c on a.User_Code=c.Organization_User 
                        INNER JOIN tbl_into_s as d on a.User_Code=d.User_Code
                        LEFT JOIN tbl_phrase_ke as e on d.Phrase_Ke=e.Phrase_ID
                        WHERE d.User_LoginId = ? ");

                        $stmt2->bind_param("s", $Itgk);
                         if ($stmt2->execute()) 
                           {
                                $user2 = $stmt2->get_result()->fetch_assoc();
                                $stmt2->close();
                                // verifying user password
                               // echo "<pre>"; print_r($user2);die;
                                if(count($user2) >0)
                                 {
                                        $txtPassword = encryptPasswordLogin($password, $user2["Phrase_Ke"]); //echo "<br>";
                                        $hash = $user2["Open_Sesame_En"];
                                        if (password_verify($txtPassword, $hash)) 
                                        {
                                            $stmt3 = $this->conn->prepare("update tbl_user_master SET User_device_id = '".$User_device_id."' where User_LoginId='".$Itgk."'");
                                            $stmt3->execute();
                                            $stmt3->close();
                                            return $user2;
                                        } 
                                        else 
                                        {
                                            return 'WORNGPASSWORD';  // wrong password   
                                        }
                                 }
                                else
                                {
                                    return 'NORECORDINNEWTABLE';// No Record found in new table
                                }
                         
                            }
                }
            else{
                    
                    return "NoRecordFound"; // no record found 
                }

            } 
        else {
            return 'SOMEERROR';
        }
    }
    
	 public function Add_Rate_Learner_To_ITGK($learner_code,$itgk_code,$rating,$feedback_meesage) {
            $stmt2 = $this->conn->prepare("select * from tbl_rate_learner_to_itgk where
								Learner_To_ITGK_lcode='".$learner_code."'");
				if($stmt2->execute()){
					$result = $stmt2->get_result();
                    $num_of_rows = $result->num_rows;
					$stmt2->close();
                    if($num_of_rows>0){
						return NULL;
					}
					else{
						$stmt = $this->conn->prepare("select Admission_Batch from tbl_admission where
								Admission_LearnerCode='".$learner_code."' and Admission_ITGK_Code='".$itgk_code."'");
			if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
					$stmt->close();
                    if($num_of_rows>0){
						$row = $result->fetch_assoc();
						$LearnerBatch = $row['Admission_Batch'];
                        $stmt1 = $this->conn->prepare("INSERT INTO tbl_rate_learner_to_itgk(Learner_To_ITGK_lcode,
								Learner_To_ITGK_code,Learner_To_ITGK_rate,Learner_To_ITGK_remark,Learner_To_ITGK_batch) 
									VALUES('".$learner_code."','".$itgk_code."','".$rating."',
									'".$feedback_meesage."','".$LearnerBatch."')");
							if ($stmt1->execute()){								
									$stmt1->close();
									$status = "Success";
									return $status;
							}
							else{
								return NULL; 
							}							
                    }else{
						return NULL;                       
                    }            
			} else {
                    return NULL;
                } 
					}
				}                      
	}
public function Fill_batch_app_bio_att($Course_Code) {
            
            $stmt = $this->conn->prepare("select Batch_Code,Batch_Name from tbl_batch_master as a
            inner join tbl_event_management as b on a.Batch_Code=b.Event_Batch
            where Event_Course='".$Course_Code."' and Event_LearnerAttendance='1' 
            AND NOW() >= Event_Startdate AND NOW() <= Event_Enddate and Event_Category='10' and Event_Name='13'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getbatch = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getbatch[] = $row;
                    }
                    $stmt->close();

                    return $Getbatch;
            
                } else {
                    return NULL;
                }
            
	}

public function Add_Rate_DPO_To_ITGK($User_LoginId,$itgk_code,$rating,$feedback_message,$platform) {
			$stmt2 = $this->conn->prepare("select DATEDIFF(CURDATE(),DPO_To_ITGK_datetime) AS days from
										tbl_rate_dpo_to_itgk WHERE DPO_To_ITGK_code='".$itgk_code."' and 
										DPO_To_ITGK_dpocode='".$User_LoginId."' order by DPO_To_ITGK_id DESC LIMIT 1");
				if ($stmt2->execute()){
					$result = $stmt2->get_result();
                    $num_of_rows = $result->num_rows;
					$stmt2->close();
                    if($num_of_rows>0){
						$row = $result->fetch_assoc();
						$difference = $row['days'];
							if($difference>30){
								$stmt1 = $this->conn->prepare("INSERT INTO tbl_rate_dpo_to_itgk(DPO_To_ITGK_dpocode,
								DPO_To_ITGK_code,DPO_To_ITGK_rate,DPO_To_ITGK_remark,DPO_To_ITGK_platform) 
									VALUES('".$User_LoginId."','".$itgk_code."','".$rating."',
									'".$feedback_message."','".$platform."')");
								if ($stmt1->execute()){								
									$stmt1->close();
									$status = "Success";
									return $status;
								}
								else{
									return NULL; 
								}
							}else{
								return NULL;
							}
					}
					else{
						$stmt1 = $this->conn->prepare("INSERT INTO tbl_rate_dpo_to_itgk(DPO_To_ITGK_dpocode,
								DPO_To_ITGK_code,DPO_To_ITGK_rate,DPO_To_ITGK_remark,DPO_To_ITGK_platform) 
									VALUES('".$User_LoginId."','".$itgk_code."','".$rating."',
									'".$feedback_message."','".$platform."')");
							if ($stmt1->execute()){								
									$stmt1->close();
									$status = "Success";
									return $status;
							}
							else{
								return NULL; 
							}					
					}
				}	
				
	 }
	
	public function GetITGKDISTRICTLIST($DistrictCode) 
	{

        $stmt = $this->conn->prepare("Select User_LoginId, Organization_Name From tbl_organization_detail as a join 
									tbl_user_master as b on a.Organization_User = b.User_Code
									Where b.User_Status = '1' AND b.User_UserRoll = '7' and
									Organization_District = '".$DistrictCode."' ");
		 
        if ($stmt->execute()) 
		{
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$ITGKs = array();
			while ($row = $result->fetch_assoc()) {
				$ITGKs[] = $row;
			}
            $stmt->close();

            return $ITGKs;
			
        } else {
            return NULL;
        }
    }
	
	public function GetItgkRatingByDPO($itgk_code) {

     $stmt = $this->conn->prepare("select * from tbl_rate_dpo_to_itgk where DPO_To_ITGK_code='".$itgk_code."'
									order by DPO_To_ITGK_id DESC");


        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$itgk_code = array();
			while ($row = $result->fetch_assoc()) {
				$itgk_code[] = $row;
			}
            $stmt->close();

            return $itgk_code;
			
        } else {
            return NULL;
        }
    }
	
	public function GetITGKRatingByLearner($itgk_code,$Course_code,$Batch_code) {

     $stmt = $this->conn->prepare("select a.Learner_To_ITGK_lcode,Learner_To_ITGK_code,Learner_To_ITGK_rate,
									Learner_To_ITGK_remark, Learner_To_ITGK_datetime,b.Batch_Name,c.Course_Name,
									upper(d.Admission_Name) as Learner_Name from tbl_rate_learner_to_itgk as a
									inner join tbl_batch_master as b on a.Learner_To_ITGK_batch=b.Batch_Code
									inner join tbl_admission as  d on a.Learner_To_ITGK_lcode=d.Admission_LearnerCode
									inner join tbl_course_master as c on c.Course_Code=d.Admission_Course
									where Learner_To_ITGK_code='".$itgk_code."' and Learner_To_ITGK_batch='".$Batch_code."'
									and Admission_Course='".$Course_code."' order by Learner_To_ITGK_id DESC");
        if ($stmt->execute()) {
			$result = $stmt->get_result();

			/* Get the number of rows */
			$num_of_rows = $result->num_rows;
			$itgk_code = array();
			while ($row = $result->fetch_assoc()) {
				$itgk_code[] = $row;
			}
            $stmt->close();

            return $itgk_code;
			
        } else {
            return NULL;
        }
    }
	
	public function View_visit_details($SP_User_Code) {
            
        //    $stmt = $this->conn->prepare("SELECT vr.*, um.User_LoginId, um.User_EmailId, um.User_MobileNo, od.Organization_Name, vf.feedbackdata FROM tbl_visit_requests vr INNER JOIN tbl_user_master um ON vr.request_by = um.User_Code 
        //    INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code LEFT JOIN tbl_visit_feedback vf ON vf.visitid = vr.id WHERE vr.request_by = '" . $SP_User_Code . "'  and vr.status='0' ORDER BY vr.id DESC");
																
			$stmt = $this->conn->prepare("select a.*,b.feedbackdata,b.overall_feedback,c.Tehsil_Name as Tehsil_Name,c.District_Name as District_Name,c.RSP_Name as Organization_Name, c.ITGK_Name as ITGK_NAME,c.ITGKEMAIL as User_EmailId, c.ITGKMOBILE as User_MobileNo FROM tbl_visit_requests AS A LEFT JOIN tbl_visit_feedback AS b on a.id=b.visitid LEFT join vw_itgkname_distict_rsp as c ON a.itgk_code=c.ITGKCODE WHERE a.request_by = '" . $SP_User_Code . "' and a.status='0' ORDER BY a.id DESC");
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
        }
        
        
        //visit purpose api
        public function Visit_Purpose() {
            
            $stmt = $this->conn->prepare("Select VisitType_Code,VisitType_Name,"
                    . "Status_Name From tbl_visittype_master as a inner join tbl_status_master as b "
                    . "on a.VisitType_Status=b.Status_Code where VisitType_Status='1'");
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
        }
        
                //Center Code api
        public function Center_Code_List($SP_User_Code) {
            
            $stmt = $this->conn->prepare("select a.itgk_code as ITGKCODE,a.visit_date ,a.status from tbl_visit_requests as a 
                                where a.status='0' and a.request_by='" . $SP_User_Code . "'");
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
        }
        
                //ITGK Details api
        public function ITGK_Details($CenterCode,$SP_User_Code) {
            
            $stmt = $this->conn->prepare("select a.id as visitid,a.itgk_code as ITGKCODE,a.visit_date AS VISITDATE,a.status AS STATUS,b.ITGK_Name from tbl_visit_requests as a 
                                inner join vw_itgkname_distict_rsp as b on a.itgk_code=b.ITGKCODE 
                                where a.status='0' and a.itgk_code='" .  $CenterCode . "' and a.request_by='" . $SP_User_Code . "'");
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
        }
        // Load Reason
        public function Visit_Close_Reason() {
            
            $stmt = $this->conn->prepare("select * from tbl_visitclosereason_master where VisitClose_Status='1'");
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
        }
        
        
                //Close Visit api
        public function Close_Visit($Reason,$Date,$Lat,$Long,$VisitId,$Spid) {
            
            $stmt = $this->conn->prepare("update tbl_visit_requests set status='2', close_reason='" . $Reason . "',"
                     . "close_date='" . $Date . "',close_Lat='" . $Lat . "', close_Long='" . $Long . "', spid='" . $Spid . "' where id='" . $VisitId . "'");
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $res  = mysqli_affected_rows($this->conn);
            $stmt->close();
            return $res;
                   
            
                } else {
                    return NULL;
                }
            
        }
		
		//after Close Visit api
// Confirm Visit by User_Code
	public function Confirm_Visit_by_UserCode($User_Code) {
            
            $stmt = $this->conn->prepare("select BioMatric_ISO_Template from tbl_biomatric_enrollments where userId='" . $User_Code . "'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
	}
	
	// Confirm Visit by User_LoginId
	public function Confirm_Visit_by_LoginId($User_LoginId) {
            
            $stmt = $this->conn->prepare("select a.BioMatric_ISO_Template from tbl_biomatric_enrollments as a inner join tbl_user_master as b on a.userId=b.User_Code where  b.User_LoginId='" . $User_LoginId . "'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
	}
	
	// Load Visitor
	public function Load_Visitor($User_Code) {
            
            $stmt = $this->conn->prepare("select id,fullname,BioMatric_ISO_Template from tbl_biomatric_enrollments WHERE userId='" . $User_Code . "' and status='1'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
	}
	
		//Confirm Visit api
	public function Confirm_Visit($itgkId,$visitorId,$id,$Lat,$Long) {
            
            $stmt = $this->conn->prepare("UPDATE tbl_visit_requests SET status = '1', confirm_by_itgk = '" . $itgkId . "', confirm_visiter_id = '" . $visitorId . "', confirm_date = '" . time() . "',close_Lat='" . $Lat . "', close_Long='" . $Long . "' WHERE itgk_code = '" . $itgkId . "' AND id = '" . $id . "'");
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $res  = mysqli_affected_rows($this->conn);
            $stmt->close();
            return $res;
                   
            
                } else {
                    return NULL;
                }
            
	}
	
		public function ITGK_View_visit_details($User_LoginId) {
            
        //    $stmt = $this->conn->prepare("SELECT vr.*, um.User_Code, vf.feedbackdata, vf.overall_feedback, tm.Tehsil_Name, dm.District_Name, odm.Organization_Name, od.Organization_Name AS ITGK_NAME, uc.User_EmailId, uc.User_MobileNo FROM tbl_visit_requests vr INNER JOIN tbl_user_master um ON vr.request_by = um.User_Code INNER JOIN tbl_user_master uc ON uc.User_LoginId = vr.itgk_code INNER JOIN tbl_organization_detail odm ON odm.Organization_User = um.User_Code INNER JOIN tbl_organization_detail od ON od.Organization_User = uc.User_Code INNER JOIN tbl_tehsil_master tm ON tm.Tehsil_Code = od.Organization_Tehsil INNER JOIN tbl_district_master dm ON dm.District_Code = od.Organization_District LEFT JOIN tbl_visit_feedback vf ON vf.visitid = vr.id WHERE vr.itgk_code = '" . $User_LoginId . "' ORDER BY vr.id DESC");
			
			 $stmt = $this->conn->prepare("select a.*,b.feedbackdata,b.overall_feedback,c.Tehsil_Name as Tehsil_Name,c.District_Name as District_Name,c.RSP_Name as Organization_Name, c.ITGK_Name as ITGK_NAME,c.ITGKEMAIL as User_EmailId, c.ITGKMOBILE as User_MobileNo FROM tbl_visit_requests AS A LEFT JOIN tbl_visit_feedback AS b on a.id=b.visitid LEFT join vw_itgkname_distict_rsp as c ON a.itgk_code=c.ITGKCODE WHERE a.itgk_code = '" . $User_LoginId . "' ORDER BY a.id DESC");
										
		if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
	}
	
	//Schedule Visit Functions
	
	//Load ITGK List
	
	     //Center Code api
        public function ITGK_List($SP_user_Code) {
            
				$stmt = $this->conn->prepare("SELECT Courseitgk_ITGK FROM tbl_user_master um  
									INNER JOIN tbl_courseitgk_mapping as ac on ac.Courseitgk_ITGK=um.User_LoginId
									WHERE NOW()<DATE_ADD(CourseITGK_ExpireDate, INTERVAL +11 MONTH) 
									and Courseitgk_Course='RS-CIT' AND um.User_Rsp = '" . $SP_user_Code . "' ORDER BY um.User_LoginId");
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
        }
		
		  //Center Details api
        public function ITGK_Details_For_schedule($ITGK_Code) {
            
				$stmt = $this->conn->prepare("SELECT * FROM vw_itgkname_distict_rsp WHERE ITGKCODE = '" . $ITGK_Code . "'");
                if ($stmt->execute()) {
                    $result = $stmt->get_result();
                    $num_of_rows = $result->num_rows;
                    $Getfinger = array();
                    while ($row = $result->fetch_assoc()) {
                            $Getfinger[] = $row;
                    }
                    $stmt->close();

                    return $Getfinger;
            
                } else {
                    return NULL;
                }
            
        }
		
			//Set Visit api

		//Set Visit api
	public function Set_Visit($requestBy,$itgkCode,$visitDate) {
            
            $stmt = $this->conn->prepare("INSERT IGNORE INTO tbl_visit_requests SET request_by =  '" . $requestBy . "', itgk_code = '" . $itgkCode . "', visit_date = Date_Format('" . $visitDate . "', '%d-%m-%Y'), adddate = '" . time() . "'");
		if ($stmt->execute()){								
									$stmt->close();
									$status = "Success";
									return $status;
							} else {
                    return NULL;
                }
            
	}


}

?>