<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();

$json = file_get_contents('php://input');
$data = json_decode($json, TRUE);

// json response array
$response = array("error" => FALSE);

if(isset($data['FunctionName'])) {

    // receiving the post params
    $FunctionName = $data['FunctionName'];


	if($FunctionName == "NearestCenter") {

			
		$latitude = $data['latitude'];
		$longitude = $data['longitude'];
		
		// Get all Nearest Centers
		$centerlocation = $db->getNearestCenter($latitude, $longitude);
		if ($centerlocation) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			foreach($centerlocation as $centerloc){
				$response["center"][$i]["ITGK_Code"] = $centerloc["User_LoginId"];
				$response["center"][$i]["latitude"] = $centerloc["latitude"];
				$response["center"][$i]["longitude"] = $centerloc["longitude"];
				$response["center"][$i]["distance"] = $centerloc["distance"];
				$response["center"][$i]["Address"] = $centerloc["Organization_Address"];
				$response["center"][$i]["Name"] = $centerloc["Organization_Name"];
				$response["center"][$i]["MobileNo"] = $centerloc["User_MobileNo"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Nearest Center Found!";
			echo json_encode($response);
		}
		
	} 
	
	if($FunctionName == "AddCenterLocation"){
		
		$ITGK_Code = $data['ITGK_Code'];
		$latitude = $data['latitude'];
		$longitude = $data['longitude'];
		
		
		// check if user is already existed with the same email
		if ($db->isCenterExisted($ITGK_Code)) {
			// user already existed
			$response["error"] = TRUE;
			$response["error_msg"] = "Center Location Already exist.";
			echo json_encode($response);
		} else {
			// create a new center location
			$centerlocation = $db->addCenterLocation($ITGK_Code, $latitude, $longitude);
			if ($centerlocation) {
				// center location stored successfully
				$response["error"] = FALSE;
				$response["id"] = $centerlocation["id"];
				$response["center"]["ITGK_Code"] = $centerlocation["ITGK_Code"];
				$response["center"]["latitude"] = $centerlocation["latitude"];
				$response["center"]["longitude"] = $centerlocation["longitude"];
				echo json_encode($response);
			} else {
				// center location failed to store
				$response["error"] = TRUE;
				$response["error_msg"] = "Unknown error occurred in updating center location!";
				echo json_encode($response);
			}
		}
	}
	if($FunctionName == "GetCourseList"){
		
		// Get all Nearest Centers
		$courses = $db->getCourseList();
		if ($courses) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			//$response["course"][$i]["Course_Code"] = 0;
			//$response["course"][$i]["Course_Name"] = "Select Course";
			//$i++;
			foreach($courses as $course){
				$response["course"][$i]["Course_Code"] = $course["Course_Code"];
				$response["course"][$i]["Course_Name"] = $course["Course_Name"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Course Found!";
			echo json_encode($response);
		}
	}
	if($FunctionName == "GetDistrictList"){
		
		// Get all Nearest Centers
		$districts = $db->GetDistrictList();
		if ($districts) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			//$response["district"][$i]["District_Code"] = 0;
			//$response["district"][$i]["District_Name"] = "Select District";
			//$i++;
			foreach($districts as $district){
				$response["district"][$i]["District_Code"] = $district["District_Code"];
				$response["district"][$i]["District_Name"] = $district["District_Name"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No District Found!";
			echo json_encode($response);
		}
	}
	if($FunctionName == "GetTehsilList"){
		$district  = $data['District'];
		// Get all Nearest Centers
		$tehsils = $db->GetTehsilList($district);
		if ($tehsils) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			// $response["tehsil"][$i]["Tehsil_Code"] = 0;
			// $response["tehsil"][$i]["Tehsil_Name"] = "Select Tehsil";
			// $i++;
			
			foreach($tehsils as $tehsil){
				$response["tehsil"][$i]["Tehsil_Code"] = $tehsil["Tehsil_Code"];
				$response["tehsil"][$i]["Tehsil_Name"] = $tehsil["Tehsil_Name"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Tehsil Found!";
			echo json_encode($response);
		}
	}
	if($FunctionName == "GetMunicipalityList"){
		$district  = $data['District'];
		// Get all Nearest Centers
		$Municipalities = $db->GetMunicipalityList($district);
		if ($Municipalities) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			// $response["Municipality"][$i]["Municipality_Code"] = 0;
			// $response["Municipality"][$i]["Municipality_Name"] = "Select Municipality";
			// $i++;
			foreach($Municipalities as $Municipality){
				$response["Municipality"][$i]["Municipality_Code"] = $Municipality["Municipality_Code"];
				$response["Municipality"][$i]["Municipality_Name"] = $Municipality["Municipality_Name"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Municipality Found!";
			echo json_encode($response);
		}
	}
	if($FunctionName == "GetWardList"){
		$municipality  = $data['Municipality'];
		// Get all Nearest Centers
		$Wards = $db->GetWardList($municipality);
		if ($Wards) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			// $response["Ward"][$i]["Ward_Code"] = 0;
			// $response["Ward"][$i]["Ward_Name"] = "Select Ward";
			// $i++;
			foreach($Wards as $Ward){
				$response["Ward"][$i]["Ward_Code"] = $Ward["Ward_Code"];
				$response["Ward"][$i]["Ward_Name"] = $Ward["Ward_Name"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Ward Found!";
			echo json_encode($response);
		}
	}
	if($FunctionName == "GetPanchayatSamitiList"){
		$district  = $data['District'];
		// Get all Nearest Centers
		$PanchayatSamiti = $db->GetPanchayatSamitiList($district);
		if ($PanchayatSamiti) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			// $response["Panchayat"][$i]["Block_Code"] = 0;
			// $response["Panchayat"][$i]["Block_Name"] = "Select Panchayat Samiti/Block";
			// $i++;
			foreach($PanchayatSamiti as $Panchayat){
				$response["Panchayat"][$i]["Block_Code"] = $Panchayat["Block_Code"];
				$response["Panchayat"][$i]["Block_Name"] = $Panchayat["Block_Name"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Ward Found!";
			echo json_encode($response);
		}
	}
	if($FunctionName == "GetGramPanchayatList"){
		$block  = $data['Block'];
		// Get all Nearest Centers
		$GramPanchayat = $db->GetGramPanchayatList($block);
		if ($GramPanchayat) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			// $response["Gram"][$i]["GP_Code"] = 0;
			// $response["Gram"][$i]["GP_Name"] = "Select Gram Panchayat";
			// $i++;
			foreach($GramPanchayat as $Gram){
				$response["Gram"][$i]["GP_Code"] = $Gram["GP_Code"];
				$response["Gram"][$i]["GP_Name"] = $Gram["GP_Name"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Gram Panchayat Found!";
			echo json_encode($response);
		}
	}
	if($FunctionName == "GetVillageList"){
		$gram  = $data['Gram'];
		// Get all Nearest Centers
		$Villages = $db->GetVillageList($gram);
		if ($Villages) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			// $response["Village"][$i]["Village_Code"] = 0;
			// $response["Village"][$i]["Village_Name"] = "Select Village";
			// $i++;
			foreach($Villages as $Village){
				$response["Village"][$i]["Village_Code"] = $Village["Village_Code"];
				$response["Village"][$i]["Village_Name"] = $Village["Village_Name"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Gram Panchayat Found!";
			echo json_encode($response);
		}
	}
	if($FunctionName == "GetCenterList"){

		$CourseCode  = $data['CourseCode'];
		$DistrictCode  = $data['DistrictCode'];
		$TehsilCode  = $data['TehsilCode'];
		$AreaType  = $data['AreaType'];
		$MunicipalityCode  = $data['MunicipalityCode'];
		$WardCode  = $data['WardCode'];
		$SamitiCode  = $data['SamitiCode'];
		$GramCode  = $data['GramCode'];
		$VillageCode  = $data['VillageCode'];
		// Get all Nearest Centers
		$Centers = $db->GetCenterList($CourseCode,$DistrictCode,$TehsilCode,$AreaType,$MunicipalityCode,$WardCode,$SamitiCode,$GramCode,$VillageCode);
		if ($Centers) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			
			foreach($Centers as $Center){
				$response["Center"][$i]["Organization_User"] = $Center["Organization_User"];
				$response["Center"][$i]["Organization_Name"] = $Center["Organization_Name"];
				$response["Center"][$i]["User_EmailId"] = $Center["User_EmailId"];
				$response["Center"][$i]["User_MobileNo"] = $Center["User_MobileNo"];
				$response["Center"][$i]["District_Name"] = $Center["District_Name"];
				$response["Center"][$i]["Tehsil_Name"] = $Center["Tehsil_Name"];
				$response["Center"][$i]["User_LoginId"] = $Center["User_LoginId"];
				$response["Center"][$i]["Organization_Address"] = $Center["Organization_Address"];
				$response["Center"][$i]["Org_latitude"] = $Center["latitude"];
				$response["Center"][$i]["Org_longitude"] = $Center["longitude"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Center Found!";
			echo json_encode($response);
		}
	}

	if($FunctionName == "GetSPList"){

		$DistrictCode  = $data['DistrictCode'];
		// Get all SP 
		$SPs = $db->GetSPList($DistrictCode);
		if ($SPs) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			
			foreach($SPs as $SP){
				$response["SP"][$i]["Organization_Name"] = $SP["Organization_Name"];
				$response["SP"][$i]["contactperson"] = $SP["Rsptarget_contactperson"];
				$response["SP"][$i]["Contactperson_mobile"] = $SP["Contactperson_mobile"];
				$response["SP"][$i]["Contactperson_email"] = $SP["Contactperson_email"];
				$response["SP"][$i]["Contactperson_address"] = $SP["Org_formatted_address"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Service Provider Found!";
			echo json_encode($response);
		}
	}
	
	if($FunctionName == "GET_LEARNER_OTP"){

		$LearnerCode  = $data['LearnerCode'];
		// Get all SP 
		$Learner = $db->GET_LEARNER_OTP($LearnerCode);
		// echo "<pre>";
		// print_r($Learner);
		// echo "</pre>";
		if ($Learner) {
			// center location stored successfully
			//$response["error"] = FALSE;
			$_MobileNo = $Learner['Admission_Mobile'];
			//echo $_MobileNo;
			if( empty($_MobileNo))
                {
                        $response["error"] = TRUE;
						$response["error_msg"] = "Mobile Number Not Found!";
                }
                else
                {
                        $result = $db->AddLernerMob($_MobileNo);
                        $response["error"] = FALSE;

                }
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Center Found!";
			echo json_encode($response);
		}
	}
	
	if($FunctionName == "SEND_LEARNER_OTP"){

		$LearnerCode  = $data['LearnerCode'];
		$LearnerOTP  = $data['LearnerOTP'];
		$User_device_id = $data['User_device_id'];
		// Get all SP 
		$Learner = $db->SEND_LEARNER_OTP($LearnerCode,$LearnerOTP,$User_device_id );
		// echo "<pre>";
		// print_r($Learner);
		// echo "</pre>";
		if ($Learner) {
			
			$response["error"] = FALSE;
			$response["data"] = $Learner;
			$response["data"]['User_UserRoll'] = 19;
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "Verify LearnerCode Again.";
			echo json_encode($response);
		}
	}

	if($FunctionName == "GET_LEARNER_DETAIL"){
                
		$LearnerCode  = $data['LearnerCode'];
		
		//print_r($_POST);die;
        $Learner = $db->GetAllLearner($LearnerCode);
//		 echo "<pre>";
//		 print_r($Learner);
//		 echo "</pre>";
        $Learner["exam_date"]=($Learner["exam_date"]!='')?  date("d-m-Y", strtotime($Learner['exam_date'])) :'';
        
        if(!$Learner)
        {
			$response["error"] = TRUE;
			$response["error_msg"] = "No Detail Found.";
        }
        else
        {
			$response["error"] = FALSE;
			
            $LearnerC = $db->CenterDetails($Learner["Admission_ITGK_Code"]);
			
			$Learner['center'] = $LearnerC ;
			
			if($Learner['Admission_DOB']=='' || $Learner['Admission_DOB']=='(NULL)'  || $Learner['Admission_DOB']=='0000-00-00 00:00:00')
				{ 
						$Learner['Admission_DOB']="Not Available";
				}
			else
				{
						$Learner['Admission_DOB'] = date("d M Y", strtotime($Learner['Admission_DOB']));
				}
           

            
            if($Learner['Admission_Photo']) {
                $Learner['Admission_Photo'] = "https://myrkcl.com/upload/admission_photo/". $Learner['Admission_LearnerCode'] . "_photo.png";
            }
            else
            {
                $Learner['Admission_Photo'] = "https://myrkcl.com/upload/admission_photo/blank.png";

            }
            
            if($Learner['Admission_Sign']!='')
            {
                $Learner['Admission_Sign']= "https://myrkcl.com/upload/admission_sign/" . $Learner['Admission_LearnerCode'] . "_sign.png";
            }
            else
            {
                $Learner['Admission_Sign']="https://myrkcl.com/upload/admission_sign/blank.png";

            }
           
                           
			if($Learner['Admission_Batch']>='90')
			{          
					   
				if($Learner['Admission_Date_LastModified']=='' || $Learner['Admission_Date_LastModified']=='(NULL)' || $Learner['Admission_Date_LastModified']=='0000-00-00 00:00:00')
				{ 
					$Learner['Admission_Date_LastModified']="Not Available";
				}
				else
				{
					$Learner['Admission_Date_LastModified'] = date("d M Y", strtotime($Learner['Admission_Date_LastModified']));
				}

				if($Learner['Admission_Date_Payment']=='' || $Learner['Admission_Date_Payment']=='(NULL)'  || $Learner['Admission_Date_Payment']=='0000-00-00 00:00:00')
				{ 
					$Learner['Admission_Date_Payment']="Not Available";
				}
				else
				{
					$Learner['Admission_Date_Payment'] = date("d M Y", strtotime($Learner['Admission_Date_Payment']));
				}

				if($Learner['Admission_Date']=='' || $Learner['Admission_Date']=='(NULL)'  || $Learner['Admission_Date']=='0000-00-00 00:00:00')
				{ 
					$Learner['Admission_Date'] = "Not Available";
				}
				else
				{
					$Learner['Admission_Date'] = date("d M Y", strtotime($Learner['Admission_Date']));
				}
					   
			}
            else
            {
                if($Learner['Admission_Batch']=='57' || $Learner['Admission_Batch']=='58')
                { 
                    if($Learner['Admission_Date_LastModified']=='' || $Learner['Admission_Date_LastModified']=='(NULL)' || $Learner['Admission_Date_LastModified']=='0000-00-00 00:00:00')
					{ 
						$Learner['Admission_Date_LastModified']="Not Available";
					}
					else
					{
						$Learner['Admission_Date_LastModified'] = date("d M Y", strtotime($Learner['Admission_Date_LastModified']));
					}
                                   
                    if($Learner['Admission_Date_Payment']=='' || $Learner['Admission_Date_Payment']=='(NULL)'  || $Learner['Admission_Date_Payment']=='0000-00-00 00:00:00')
                    { 
                        $Learner['Admission_Date_Payment']="Not Available";
                    }
                    else
                    {
                        $Learner['Admission_Date_Payment'] = date("d M Y", strtotime($Learner['Admission_Date_Payment']));
					}
                           
//                  if($Learner['Admission_Date']=='' || $Learner['Admission_Date']=='(NULL)'  || $Learner['Admission_Date']=='0000-00-00 00:00:00')
//                  { 
//                      $Admission_Date="Not Available";
//                  }
//                  else
//                  {
//                      $Admission_Date = date("d M Y", strtotime($Learner['Admission_Date']));
//                  }
					  
					$Learner['Admission_Date']="Not Available";
                }
				else
				{
					$Admission_Date_LastModified="Not Available";
					$Admission_Date_Payment="Not Available";
					$Admission_Date="Not Available";
				}
                     
            }        
            
            $LearnerSP = $db->SPDetails($LearnerC["User_LoginId"]);
			
                
            $LearnerSP['SP_Address'] =$LearnerSP['Organization_HouseNo']." ".$LearnerSP['Organization_Street']." ".$LearnerSP['Organization_Road']." ".$LearnerSP['Organization_Landmark']." ".$LearnerSP['Org_formatted_address'] ;

			
            $LearnerSP['Organization_FoundedDate'] = date("d M Y", strtotime($LearnerSP['Organization_FoundedDate']));
			
			$Learner['SPDetails'] = array(
                            'status'=>'found',
                            'detail'=>$LearnerSP
                        );
           
            if($Learner['Admission_Payment_Status']==1)
            {
                $Learner['Admission_Payment_Status'] ='Confirm';
                
                
            }
			else{ 
				$Learner['Admission_Payment_Status']='Not Confirm';
			}

            


            $LearnerSco = $db->GetAllLearnerScore($LearnerCode, $Learner["Admission_ITGK_Code"]);

            
            if(!$LearnerSco)
            {
				$Learner['Admission_Score']['status'] = "No Detail Found.";
            }
            else
            {
                
                $Learner['Batch_StartDate'] = date("d M Y", strtotime($Learner['Batch_StartDate']));
				$Learner['Admission_Score']['status'] = "found";
				
                                
                                $Learner['Admission_Score'] = array(
                                    'status'=>$Learner['Admission_Score']['status'],
                                    'detail'=>$LearnerSco
                                ) ;
                
            }
			


            $LearnerLAM = $db->GetLearnerAllMarks($LearnerCode);
            
            if(!$LearnerLAM)
            {
				$Learner['Admission_Marks']['status'] = "No Detail Found.";
            }
            else
            {
				$Learner['Admission_Marks']['status'] = "found";
				
                                $Learner['Admission_Marks'] = array(
                                    'status'=>$Learner['Admission_Marks']['status'],
                                    'detail'=>$LearnerLAM
                                ) ;
            }

           
            $responseNew = $db->checkGovtLearnerStatus($LearnerCode,$Learner["Admission_ITGK_Code"]);
			
            if($responseNew)
            {
                $LearnerDET = $db->GetAllRDETAILS($LearnerCode,$Learner["Admission_ITGK_Code"]);
                

                if($LearnerDET['aattempt']==1)
				{
					$LearnerDET['aattempt']='First';
				}
				else
				{
					$LearnerDET['aattempt']='Not in 1st Attempt';
				}

                
                $LearnerDET['datetime'] = date("d M Y", strtotime($LearnerDET['datetime']));
                
                
                $total = intval (trim($LearnerDET['fee']))+ intval (trim($LearnerDET['incentive']));
				$LearnerDET['totalFeeAmt'] = $total;
				$Learner['Admission_Reimbursement']['status'] = "found";
                                $garr = array();
				$garr['Reimbursement'] = $LearnerLAM; 
				$garr['detail'] = $LearnerDET;
				
				$Learner['Admission_Reimbursement'] = array(
                                    'status'=>$Learner['Admission_Reimbursement']['status'],
                                    'detail'=>$garr['detail']
                                ) ;
				
            }
			else
			{ 
				$Learner['Admission_Reimbursement']['status'] = "No Detail Found.";
			}
                        
                         
                         

            

            $responseCS = $db->checkcorrectionStatus($LearnerCode,$Learner["Admission_ITGK_Code"]);
            if($responseCS)
            {
                $LearnerDET = $db->GetCorrectionDuplicateCertificate($LearnerCode,$Learner["Admission_ITGK_Code"]);
                
                if($LearnerDET['Correction_Payment_Status']==1)
				{
					$LearnerDET['Correction_Payment_Status']="Payment DONE";
				}
				else
				{
					$LearnerDET['Correction_Payment_Status']="Payment Not DONE";
				}
				$Learner['Admission_Correction']['status'] = "found";
				$garr['Correction'] = $LearnerLAM;
				$garr['DuplicateCertificate'] = $LearnerDET;
				
                                
                                $Learner['Admission_Correction'] = array(
                                    'status'=>$Learner['Admission_Correction']['status'],
                                    'detail'=>$garr['DuplicateCertificate']
                                ) ;
            }
            else
			{
				$Learner['Admission_Correction']['status'] = "Not Available.";
			}     
            
			$response['data'] = $Learner;

        }
        // echo "<pre>";
		// print_r($response);
		// echo "</pre>";
		echo json_encode($response);
	}
	if($FunctionName == "Channel_Partner_Login"){
		// receiving the post params
		$Itgk = $data['username'];
		$password = $data['password'];
		$User_device_id = $data['User_device_id'];

		// get the user by email and password
		$user = $db->getUserByItgkAndPassword($Itgk, $password,$User_device_id);

		if ($user != false) {
			// use is found
			$response["error"] = FALSE;
			$response["User_Code"] = $user["User_Code"];
			$response["user"]["User_Code"] = $user["User_Code"];
			$response["user"]["User_MobileNo"] = $user["User_MobileNo"];
			$response["user"]["User_LoginId"] = $user["User_LoginId"];
			$response["user"]["User_Password"] = $user["User_Password"];
			$response["user"]["User_UserRoll"] = $user["User_UserRoll"];
			$response["user"]["User_EmailId"] = $user["User_EmailId"];
			echo json_encode($response);
		} else {
			// user is not found with the credentials
			$response["error"] = TRUE;
			$response["error_msg"] = "Login credentials are wrong. Please try again! " ;
			echo json_encode($response);
		}
	}
	if($FunctionName == "Get_News"){
		$news = $db->Get_News();
		if ($news) {
			$response["data"] = $news;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
	if($FunctionName == "Get_Slides"){
		
		$Slides = $db->Get_Slides();
		
		if ($Slides) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			
			foreach($Slides as $Slide){
				$response["Slides"][$i]["title"] = $Slide["imagetittle"];
				$response["Slides"][$i]["photo"] = "https://myrkcl.com/Upload/slider/".$Slide["photo"];
				$response["Slides"][$i]["photo"] = $_SERVER['SERVER_NAME']."/MyRkcl/uploads/".$Slide["photo"];
				$response["Slides"][$i]["link"] = $Slide["link"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No News Found.";
			echo json_encode($response);
		}
	}
	if($FunctionName == "GetExamEvent"){
		$ExamEvents = $db->GetExamEvent();
		
		if ($ExamEvents) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			
			foreach($ExamEvents as $ExamEvent){
				
				$response["ExamEvents"][$i]["Event_ID"] = $ExamEvent["Affilate_Event"];
				$response["ExamEvents"][$i]["Event_Name"] = $ExamEvent["Event_Name"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Record Found.";
			echo json_encode($response);
		}
	}
	if($FunctionName == "GetExamEventPayment"){
		$ExamEvents = $db->GetExamEventPayment();
		
		if ($ExamEvents) {
			// center location stored successfully
			$response["error"] = FALSE;
			$i = 0;
			
			foreach($ExamEvents as $ExamEvent){
				
				$response["ExamEvents"][$i]["Event_ID"] = $ExamEvent["Affilate_Event"];
				$response["ExamEvents"][$i]["Event_Name"] = $ExamEvent["Event_Name"];
				$i++;
			}
			
			echo json_encode($response);
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Record Found.";
			echo json_encode($response);
		}
	}
	if($FunctionName == "SHOWApplyingLEARNER"){
		$Event_ID = $data['Event_ID'];
		$LearnerCode = $data['LearnerCode'];
		$Itgk = $data['ITGK_Code'];
		$learnerCourseBatch = $db->GetLearnerCourseBatch($LearnerCode);
		//print_r($learnerCourseBatch);
		if ($learnerCourseBatch && !empty($Event_ID)) {
			//foreach($learnerCourseBatch as $CourseBatch){
				
				$AllReexamls = $db->GetAllReexam($learnerCourseBatch['Admission_Batch'], $learnerCourseBatch['Admission_Course'], $Event_ID, $LearnerCode,$Itgk);
				$AlreadyAppliedForReExam = $db->CheckIfAlreadyAppliedForReExam($LearnerCode, $Event_ID);
				if ($AllReexamls) {
					// center location stored successfully
					$response["error"] = FALSE;
					$i = 0;
					
					foreach($AllReexamls as $AllReexaml){
						
						$response["AllReexamls"][$i] = $AllReexaml;
						if(!$AlreadyAppliedForReExam){
							$batchResponse = $db->GetReexamBatch($AllReexaml['exameventid'], $AllReexaml['Admission_Course'], $AllReexaml['Admission_Batch']);
							if($batchResponse){
								$response["AllReexamls"][$i]['batchResponse'] = 1;
							}
							else{
								$response["AllReexamls"][$i]['batchResponse'] = 0;
							}
							$response["AllReexamls"][$i]['AlreadyAppliedForReExam'] = 0;
							$response["AllReexamls"][$i]['paymentstatus'] = 0;
						}
						else{
							$response["AllReexamls"][$i]['AlreadyAppliedForReExam'] = 1;
							$response["AllReexamls"][$i]['paymentstatus'] = $AlreadyAppliedForReExam['paymentstatus'];
						}
						$i++;
					}
					
					echo json_encode($response);
				} else {
					// center location failed to store
					$response["error"] = TRUE;
					$response["error_msg"] = "No Record Found.";
					echo json_encode($response);
				}
				//$_Response = $db->CheckIfAlreadyAppliedForReExam($_SESSION['User_LearnerCode'], $_POST['ExamEvent']);
				//$alreadyApplied = ($_Response[0] != Message::NoRecordFound) ? 1 : 0;
			//}
			
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Record Found.";
			echo json_encode($response);
		}
	}
	if($FunctionName == "AddLearnerReexamApp"){
		$Event_ID = $data['Event_ID'];
		$Itgk = $data['ITGK_Code'];
		$User_Role = $data['User_Role'];
		$Admission_Codes_str = $data['Admission_Codes'];
		$Admission_Codes = explode(",", $Admission_Codes_str);
		$ExamEvents = $db->AddLearnerReexamApp($Admission_Codes,$Event_ID,$User_Role);
		//print_r($ExamEvents);
		if ($ExamEvents) {
			
			$response["error"] = FALSE;
			$response["response"] = "Successfully Added For Payment.";
			echo json_encode($response);
			
		} else {
			// center location failed to store
			$response["error"] = TRUE;
			$response["error_msg"] = "No Record Found.";
			echo json_encode($response);
		}
	}
	if($FunctionName == "Get_ShowAllLearnerToPay"){
		$LearnerCode = $data['LearnerCode'];
		$User_Role = $data['User_Role'];
		$Event_ID = $data['Event_ID'];
		$Itgk = $data['ITGK_Code'];
		$paymode = 1;
		$learnerCourseBatch = $db->GetLearnerCourseBatch($LearnerCode);
		//print_r($learnerCourseBatch);
		if ($learnerCourseBatch) {
			$Course = $learnerCourseBatch['Admission_Course'];
			$Learners = $db->Get_ShowAllLearnerToPay($paymode, $Course,$Event_ID,$LearnerCode, $User_Role,$Itgk);
			if ($Learners) {
				// center location stored successfully
				$response["error"] = FALSE;
				$i = 0;
				
				foreach($Learners as $Learner){
					$response["Learners"][$i] = $Learner;
					$i++;
				}
				
				echo json_encode($response);
			} else {
				// center location failed to store
				$response["error"] = TRUE;
				$response["error_msg"] = "No News Found.";
				echo json_encode($response);
			}
		}
	}
        // Get all notification --anoop
        if($FunctionName == "GET_ALL_NOTIFICATION"){
                if(@($data['Admission_LearnerCode'])){
                    $user = $data['Admission_LearnerCode'];
                }elseif(@($data['user_id'])){
                    $user = $data['user_id'];
                } else {
                    
                exit();}
		$Getnotify = $db->Fatchallnotification($user);
		if ($Getnotify) {
			$response["error"] = FALSE;
			$response["response"] = "Successfully";
                        $response['notifcations'] =  $Getnotify;
			echo json_encode($response);
		} else {
			$response["error"] = TRUE;
			$response["error_msg"] = "No Record Found.";
                        $response['notifcations'] =  [];
			echo json_encode($response);
		}
	}
        if($FunctionName == "READ_NOTIFICATION"){
                $notification_id = $data['notification_id'];
		$GetnotifyStatus = $db->UpdatenotifyStatus($notification_id);
		if ($GetnotifyStatus) {
			$response["error"] = FALSE;
			$response["response"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["error_msg"] = "error";
			echo json_encode($response);
		}
	}
        if($FunctionName == "CHECK_BIO_METRIC_DEVICE_REGISTRATION"){
                $itgk_code = $data['itgk_code'];
		$GetStatusBio = $db->GetStatusBio($itgk_code);
		if ($GetStatusBio) {
			$response["data"] = $GetStatusBio;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
        if($FunctionName == "REGISTRATION_BIO_METRIC_MACHINE"){
                $BioMatric_ITGK_Code = $data['BioMatric_ITGK_Code'];
                $BioMatric_SerailNo = $data['BioMatric_SerailNo'];
                $BioMatric_Make = $data['BioMatric_Make'];
                $BioMatric_Model = $data['BioMatric_Model'];
                $BioMatric_Width = $data['BioMatric_Width'];
                $BioMatric_Height = $data['BioMatric_Height'];
                $BioMatric_Local_MAC = $data['BioMatric_Local_MAC'];
                $BioMatric_Local_IP = $data['BioMatric_Local_IP'];
                $BioMatric_Status = $data['BioMatric_Status'];
                $BioMatric_Reason = $data['BioMatric_Reason'];
                $BioMatric_Flag = $data['BioMatric_Flag'];
		$RegisterBio = $db->MachineRegisterBio($BioMatric_ITGK_Code,$BioMatric_SerailNo,$BioMatric_Make,$BioMatric_Model,
                        $BioMatric_Width,$BioMatric_Height,$BioMatric_Local_MAC,$BioMatric_Local_IP,$BioMatric_Status,
                        $BioMatric_Reason,$BioMatric_Flag);
		if ($RegisterBio) {
			$response["data"] = $RegisterBio;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
        
        if($FunctionName == "FILL_COURSE_APP"){
                $itgk_code = $data['Course_ITGK_Code'];
		$Getcourse = $db->Fill_course_app($itgk_code);
		if ($Getcourse) {
			$response["data"] = $Getcourse;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
        if($FunctionName == "FILL_BATCH_APP"){
                $itgk_code = $data['Course_ITGK_Code'];
                $Course_Code = $data['Course_Code'];
		$Getbatch = $db->Fill_batch_app($Course_Code);
		if ($Getbatch) {
			$response["data"] = $Getbatch;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
        if($FunctionName == "LEARNER_LIST_ATTENDENCE_ENROLLMENT"){
                $itgk_code = $data['ITGK_Code'];
                $Course_Code = $data['Course_Code'];
                $Batch_Code = $data['Batch_Code'];
		$Learnerlist = $db->Learner_list_attendence_Enrollment($itgk_code,$Course_Code,$Batch_Code);
		if ($Learnerlist) {
			$response["data"] = $Learnerlist;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
        if($FunctionName == "LEARNER_ATTENDENCE_ENROLLMENT"){
                $itgk_code = $data['ITGK_Code'];
                $Course_Code = $data['Course_Code'];
                $Batch_Code = $data['Batch_Code'];
                $Admission_LearnerCode = $data['Admission_LearnerCode'];
                $Admission_Code = $data['Admission_Code'];
                $Admission_Mobile = $data['Admission_Mobile'];
                $Admission_Name = $data['Admission_Name'];
                $Admission_Fname = $data['Admission_Fname'];
                $Admission_DOB = $data['Admission_DOB'];
                $Admission_RspName = $data['Admission_RspName'];
                $Admission_Photo = $data['Admission_Photo'];
                $BioMatric_Quality = $data['BioMatric_Quality'];
                $BioMatric_NFIQ = $data['BioMatric_NFIQ'];
                $BioMatric_ISO_Template = $data['BioMatric_ISO_Template'];
                $BioMatric_ISO_Image = $data['BioMatric_ISO_Image'];
                $BioMatric_Raw_Data = $data['BioMatric_Raw_Data'];
                $BioMatric_WSQ_Image = $data['BioMatric_WSQ_Image'];
		$LearnerEnroll = $db->Learner_attendence_Enrollment($itgk_code,$Course_Code,$Batch_Code,
                        $Admission_Code,$Admission_LearnerCode,$Admission_Name,$Admission_Fname,$Admission_DOB,$Admission_RspName,
                        $Admission_Photo,$BioMatric_Quality,$BioMatric_NFIQ,$BioMatric_ISO_Template,
                        $BioMatric_ISO_Image,$BioMatric_Raw_Data,$BioMatric_WSQ_Image,$Admission_Mobile);
		if ($LearnerEnroll) {
			$response["data"] = $LearnerEnroll;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
        if($FunctionName == "LEARNER_MARK_LIST_ATTENDENCE"){
                $itgk_code = $data['ITGK_Code'];
                $Course_Code = $data['Course_Code'];
                $Batch_Code = $data['Batch_Code'];
		$Learnerlist = $db->Learner_list_attendence_Enrollment($itgk_code,$Course_Code,$Batch_Code);
		if ($Learnerlist) {
			$response["data"] = $Learnerlist;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
        
        if($FunctionName == "LEARNER_GET_MARK_ATTENDENCE"){
                $itgk_code = $data['ITGK_Code'];
                $BioMatric_Admission_BioPIN = $data['BioMatric_Admission_BioPIN'];
		$Learnerlist = $db->Learner_detail_mark_attendence($itgk_code,$BioMatric_Admission_BioPIN);
		if ($Learnerlist) {
			$response["data"] = $Learnerlist;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
        
        if($FunctionName == "LEARNER_MARK_ATTENDENCE"){
                $Attendance_ITGK_Code = $data['Attendance_ITGK_Code'];
                $Attendance_Admission_Code = $data['Attendance_Admission_Code'];
                $Attendance_Quality = $data['Attendance_Quality'];
                $Attendance_NFIQ = $data['Attendance_NFIQ'];
		$Learnerlist = $db->Learner_mark_attendence($Attendance_ITGK_Code,$Attendance_Admission_Code,
                        $Attendance_Quality,$Attendance_NFIQ);
		if ($Learnerlist) {
			$response["data"] = $Learnerlist;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
        
        if($FunctionName == "LEARNER_FINGER_MATCH"){
                $BioMatric_Admission_Course = $data['BioMatric_Admission_Course'];
                $BioMatric_Admission_Batch = $data['BioMatric_Admission_Batch'];
                $BioMatric_ITGK_Code = $data['BioMatric_ITGK_Code'];
		$Learnerlistfinger = $db->Learner_finger_match($BioMatric_Admission_Course,$BioMatric_Admission_Batch,$BioMatric_ITGK_Code);
		if ($Learnerlistfinger) {
			$response["data"] = $Learnerlistfinger;
                        $response["error"] = FALSE;
			$response["msg"] = "Successfully";
			echo json_encode($response);
			
		} else {
			$response["error"] = TRUE;
			$response["msg"] = "error";
			echo json_encode($response);
		}
	}
	
	 /*** CODE ADDED BY SIDDHARTHA STARTS HERE ***/

        /***
         * @Condition : GET_ANDROID_VERSION
         * @return : String : Returns array as json encoded string containing the application version code, name and url
         * fetched from database.
         */

        if ($FunctionName == "GET_ANDROID_VERSION") {
            $responseArray = [];
            $getAndroidVersion = $db->getAndroidVersion();

            if ($getAndroidVersion["status"] == "SUCCESS") {
                $responseArray = [
                    "error"                => FALSE,
                    "current_version_code" => $getAndroidVersion["current_version_code"],
                    "current_version_name" => $getAndroidVersion["current_version_name"],
                    "current_app_url"      => $getAndroidVersion["current_app_url"]
                ];
            } else {
                $responseArray = [
                    "error"     => TRUE,
                    "error_msg" => "Could not find the records, please try again"
                ];
            }
            echo json_encode($responseArray);
        }

        /***
         * @Condition: INSERT_ANDROID_USER_DATA
         * @return : String : Returns array as json encoded string containing the status of data if successfully
         * inserted or returns error in either case
         */

        if ($FunctionName == "INSERT_ANDROID_USER_DATA") {

            $responseArray = [];
            if ($data["line_1_number"] == "" || $data["imei_1_number"] == "") {
                $responseArray = [
                    "error"     => TRUE,
                    "error_msg" => "Primary parameters missing in inserted android user data"
                ];
            } else {
                $insertAndroidUserData = $db->insertAndroidUserData($data);
                if ($insertAndroidUserData["status"] == "SUCCESS") {
                    $responseArray = [
                        "error"     => FALSE,
                        "error_msg" => $insertAndroidUserData["status_msg"]
                    ];
                }
            }
            echo json_encode($responseArray);
        }

        /*** CODE ADDED BY SIDDHARTHA ENDS HERE ***/
    
if($FunctionName == "Channel_Partner_Login_NEW"){
		// receiving the post params
		$Itgk = $data['username'];
		$password = $data['password'];
		$User_device_id = $data['User_device_id'];

		// get the user by email and password
		$user = $db->getUserByItgkAndPasswordNEW($Itgk, $password,$User_device_id);
                if ($user =='NoRecordFound')
                {
                        $response["error"] = TRUE;
			$response["error_msg"] = "Your have entered Wrong or Invalid  Username or Your Center may be inactive or block! " ;
			echo json_encode($response);
                }
                elseif($user =='NORECORDINNEWTABLE')
                {
                        $response["error"] = TRUE;
			$response["error_msg"] = "Your MYRKCL password has been expired, Please change your password on myrkcl.com.! " ;
			echo json_encode($response);
                }
                elseif($user =='WORNGPASSWORD')
                {
                        $response["error"] = TRUE;
			$response["error_msg"] = "Your have entered wrong password! " ;
			echo json_encode($response);
                }
                elseif($user =='SOMEERROR')
                {
                        $response["error"] = TRUE;
			$response["error_msg"] = "Some Error!!!! " ;
			echo json_encode($response);
                }
                else
                {
                        $response["error"] = FALSE;
			$response["User_Code"] = $user["User_Code"];
			$response["user"]["User_Code"] = $user["User_Code"];
			$response["user"]["User_MobileNo"] = $user["User_MobileNo"];
			$response["user"]["User_LoginId"] = $user["User_LoginId"];
			//$response["user"]["User_Password"] = $user["User_Password"];
			$response["user"]["User_UserRoll"] = $user["User_UserRoll"];
			$response["user"]["User_EmailId"] = $user["User_EmailId"];
			echo json_encode($response);
                }

		
	}
    
} 


else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters missing!";
    echo json_encode($response);
} 

?>
