<?php
$title = "Admission Summary";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var UserParentID=" . $_SESSION['User_ParentId'] . "</script>";
    echo "<script>var UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var UserParentID=0</script>";
    echo "<script>var UserRole=0</script>";
}
echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Admission Summary Report WCD</div>
            <div class="panel-body">

                <form name="frmAdmSummaryRptWCD" id="frmAdmSummaryRptWCD" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
							<div class="col-md-4 form-group"> 
                                <label for="course"> Course Name <span class="star">*</span></label>
                              
								<select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                        onchange="try {
                                                    setCustomValidity('')
                                                } catch (e) {
                                                }" >
                                  
                                </select>
								
                            </div> 
							
                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                        onchange="try {
                                                    setCustomValidity('')
                                                } catch (e) {
                                                }"> </select>
                            </div>

                            <div class="col-md-4 form-group" style="margin-top:23px;">     
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Click to Generate Report"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		
		function FillCourse() 
		{
			//alert("hello");
			$.ajax({
				type: "post",
				url: "common/cfAdmSummaryRptWCD.php",
				data: "action=FILLCOURSE",
				success: function (data) {
					//alert(data);
					
					$("#ddlCourse").html(data);
				}
			});
		}
		FillCourse();
						
						
        $("#ddlCourse").change(function (){
			var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfAdmSummaryRptWCD.php",
                data: "action=FILLBATCH&values=" + selCourse + "",
                success: function (data) {
					//alert(data);
                    $("#ddlBatch").html(data);
                }
            });
        });
       // FillBatch();


        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfAdmSummaryRptWCD.php"; // the script where you handle the form input.

            var data;
            //alert (data);
            data = "action=GETDATA&batchcode=" + ddlBatch.value + ""; //

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {

                    $('#response').empty();

                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }

        function showDataPO() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfAdmSummaryRptWCD.php"; // the script where you handle the form input.

            //alert (role_type);
            data = "action=GETDATAPO&rolecode=" + UserLoginID + "&batchcode=" + ddlBatch.value + ""; //
            // alert(data);
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                 //   alert(data);
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }


        $("#btnSubmit").click(function () {


            if (User_UserRole == 17) {

                //alert(1);
                showDataPO();
            } else {
                //alert(2);
                showData();
            }

        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>

<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
