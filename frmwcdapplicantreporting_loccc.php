<?php
$title = "WCD Learner Applicant Reported Process";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">WCD Learner Applicant Reported Process</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     
					<div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
								  <select id="txtcoursename" name="txtcoursename" class="form-control">
                                   
								</select>
                             </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                   
								</select>
                            </div> 
						
                        </div>

						<div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View Details"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModalupdate" class="modal" style="padding-top:150px !important">
            
  <div class="modal-content" style="width: 90%;">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h6>REPORTING PROCESS</h6>
    </div>
      <div class="modal-body" style="max-height: 800px;" id='formhtml'>
          <form name="frmCourseBatch" action="common/cfWcdApplicantReporting.php" id="frmCourseBatch" class="form-horizontal"
			role="form" enctype="multipart/form-data" style="margin: 25px 0 40px 0;">
				<div class="container">
                            <div id="responses"></div>

                        </div>        
			<div class="container">
				  <div class="col-sm-4 form-group">     
                        <label for="address">Learner Status:</label> <br/> 

                        <label class="radio-inline"> 
                            <input type="radio" id="Status" name="Status" required="required"/>Reported</label>

                    </div>
					
                  <div class="col-sm-4 form-group">
					<label for="address">Allocate Batch Start Time:</label>
					<input type="hidden" name="lcode" id="lcode" value=""/>
					<input type="hidden" name="batch" id="batch" value=""/>
					<input type="hidden" name="course" id="course" value=""/>
					<input type="hidden" name="itgk" id="itgk" value=""/>
					<input type="hidden" class="form-control" maxlength="50" name="action" id="action"
						value="UpdateApproveReportedLearner"/>
                      <select id='ddltime' name='ddltime' class='form-control' required="required">
						<option value=''>Select Time</option>
						<option value='08 AM'>08 AM</option>
						<option value='09 AM'>09 AM</option>
						<option value='10 AM'>10 AM</option>
						<option value='11 AM'>11 AM</option>
						<option value='12 PM'>12 PM</option>
						<option value='01 PM'>01 PM</option>
						<option value='02 PM'>02 PM</option>
						<option value='03 PM'>03 PM</option>
						<option value='04 PM'>04 PM</option>
						<option value='05 PM'>05 PM</option>
						<option value='06 PM'>06 PM</option>
					</select>
                  </div>
				  
				    <div class="col-sm-4 form-group" id="proofid6" style="text-align: center;"> 
						<label for="photo">Attach Document<span id="idproof"></span>:<span class="star">*</span></label> </br>
						<img id="uploadPreview6" src="images/sampleproof.png" id="uploadPreview6" name="fileCasteCertificate" width="80px" height="107px">                                  

						<input style="margin-top:5px !important;margin-left: 120px;" id="uploadImage6" type="file" name="uploadImage6"
							onchange="checkProof(this, 6); PreviewImage(6)" required="required"/> 
						
					</div>
						
			</div>
			
		<div class="container">	
			<div class="col-sm-4 form-group" id="email" style="display:none;">
				 <label for="bankname">Email Id:</label>
                 <input type="text" placeholder="Email Id" class="form-control" id="txtemail" name="txtemail" maxlength="100" />
			</div> 
		</div>
             <!--     <div class="form-group" >
                      <div class="col-sm-3"></div>
                      <div class="col-sm-8" id="turmcandition">
                          <p>घोषणा:- मैं यह शपथपूर्वक घोषणा करता(ती) हूँ की महिला विभाग की योजनान्तर्गत चयनित आवेदक द्वारा स्वयं से ITGK पर रिपोर्टिंग की प्रक्रिया पूर्ण करने के उपरांत ही योजना हेतु “REPORTED” चिन्हित किया जा रहा है तथा आवेदक द्वारा दिए गए  स्व:हस्ताक्षरित दस्तावेज को ही MYRKCL पर अपलोड किया जा रहा है |.</p>
                          <div><input type="checkbox" id="yes" name="fooby[1][]" /> Yes
                          </div>
                      </div>
                      <div class="col-sm-2"></div>
                  </div>-->
			<div class="container">
                  <div class="col-sm-4 form-group last">
                     <input type="button" name="btnSubmitModal" id="btnSubmitModal" class="btn btn-primary valid"
							value="Submit"/>
                      
                  </div>  
             </div>    
              </form>		
    </div>
  </div>
</div>

   <div class="modal fade" id="previewModal" role="dialog">
            <div>
                <div class="modal-content" style="margin: 100px auto; width: 500px;" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-left">Disclaimer</h4>
                    </div>
                    <div class="modal-body" id="rec_type">
						 
                           <p>घोषणा:- मैं यह शपथपूर्वक घोषणा करता(ती) हूँ की महिला विभाग की योजनान्तर्गत चयनित आवेदक द्वारा स्वयं से ITGK पर रिपोर्टिंग की प्रक्रिया पूर्ण करने के उपरांत ही योजना हेतु “REPORTED” चिन्हित किया जा रहा है तथा आवेदक द्वारा दिए गए  स्व:हस्ताक्षरित दस्तावेज को ही MYRKCL पर अपलोड किया जा रहा है |.</p>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" id="btnFinalSubmit">Yes</button>
                         <button class="btn btn-primary" id="btnFinalCancle">No</button>
                                                 
                    </div>
                </div>
            </div>
        </div> 

<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
</body>


<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
		
    }
	
	function checkProof(target, sn) {
        if(target.files[0].size > 0) 
            {
                      $('#btn_3').show(3000);   
                      $('#AttachPhotoSign3').hide(3000);   
            }
        var fname = target.value;
        var ext = fname.split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG JPG Format');
            target.value = '';
            document.getElementById("uploadPreview" + sn).src = 'images/sampleproof.png';
            return false;
        }

        if (target.files[0].size > 200000) {
            alert("Image size should less or equal 200 KB");
            target.value = '';
            document.getElementById("uploadPreview" + sn).src = 'images/sampleproof.png';
            return false;
        }
        else if (target.files[0].size < 10000)
        {
            alert("Image size should be greater than 10 KB");
            target.value = '';
            document.getElementById("uploadPreview" + sn).src = 'images/sampleproof.png';
            return false;
        }
        
        return true;
    }
</script>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		
        function FillCourseName() {
            $.ajax({
                type: "post",
                url: "common/cfWcdApplicantReporting.php",
                data: "action=FillAdmissionCourse",
                success: function (data) {
					 $("#txtcoursename").html(data);
					
                 }
            });
        }
        FillCourseName();

        $("#txtcoursename").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfWcdApplicantReporting.php",
                data: "action=FILLBatchName&values=" + txtcoursename.value + "",
                success: function (data) {					
                    $("#ddlBatch").html(data);
                }
            });

        });
		
    function showData() {			
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdApplicantReporting.php"; // the script where you handle the form input.           
            data = "action=showdata&course=" + txtcoursename.value + "&batch=" + ddlBatch.value + ""; //            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {					
					if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Batch." + "</span></div>");
                            
						}
					else {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}

                }
            });
        }

        $("#btnSubmit").click(function () {
            showData();
          return false; // avoid to execute the actual submit of the form.
        });
		
		$("#btnSubmitModal").click(function () {
			var dtime = $("#ddltime").val();
			if (document.getElementById('Status').checked) //for radio button
                {
                    var lstatus = 'yes';
                } else {
                    lstatus = 'no';
                }
			var updfiledoc = $("#uploadImage6").val();
			
			var coursecode = $('#txtcoursename').val();
				
			if(lstatus=="no"){
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Select Status for Reporting.</span></p>");
			}
			else if(dtime==""){
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Select Batch Allocation Start Time for Reporting.</span></p>");
			}
			
			else if(updfiledoc==""){
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Upload Document for Reporting.</span></p>");
			}
			else if(coursecode == '24')	{
				var email=$('#txtemail').val();
					if(email==""){
						$('#responses').empty();
						$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please enter email for Reporting.</span></p>");
					}
					else if(IsEmail(email)==false){
						  $('#responses').empty();
						$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Enter Valid E-Mail Id.</span></p>");
						  return false;
						}
					
					else{
						$('#responses').empty();
						$('#previewModal').modal({show: true});
					}
				}
			else{
				$('#responses').empty();
				$('#previewModal').modal({show: true});
			}
				
             
          });
		  $("#btnFinalCancle").click(function () {            
             $('#previewModal').modal('hide');
          });
        $("#btnFinalSubmit").click(function () {
			if (confirm("Are You Sure you want to mark this applicant as “Reported”?"))
            {
                $('#previewModal').modal('hide');
				$("#frmCourseBatch").submit();
            }
			else{
				
			}

        });
		
		function IsEmail(email) {
			  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			  if(!regex.test(email)) {
				return false;
			  }else{
				return true;
			  }
			}
		
		$("#frmCourseBatch").on('submit',(function(e) {
			$('#responses').empty();
			$('#responses').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
			e.preventDefault();
				$.ajax({ 
				url: "common/cfWcdApplicantReporting.php",
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data)
          { 
			if(data == "t"){
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Select DateTime for Reported.</span></p>");
			}
			else if(data == "d"){
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Upload Document for Reported.</span></p>");
			}
			else if(data == "s"){
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Select Status for Reported.</span></p>");
			}
			else if(data == "e"){
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Enter Email for Reported.</span></p>");
			}
            else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#responses').empty();
                        $('#responses').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmwcdapplicantreporting.php";
                        }, 2000);

                        Mode = "Add";
                        resetForm("form");
                    }
			else
			{
				$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
			}
          },
            error: function(e) 
            {
               $("#errmsg").html(e).fadeIn();
            } 	        
    });
		}));
		
		$("#grid").on("click",".upd_details",function(){
			var lcode = $(this).attr("id");
			var batch = $(this).attr("batch");
			var course = $(this).attr("course");
			var itgk = $(this).attr("itgk");
				$("#lcode").val(lcode);
				$("#batch").val(batch);
				$("#course").val(course);
				$("#itgk").val(itgk);
				
			var coursecode = $('#txtcoursename').val();
			if(coursecode == '3'){
				$('#email').hide();
			}
			else{
				$('#email').show();
			}
			
				
			var modal = document.getElementById('myModalupdate');
			var span = document.getElementsByClassName("close")[0];
				modal.style.display = "block";
				span.onclick = function() { 
					modal.style.display = "none";
                                        }
		});
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });
	
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
