<?php  ob_start(); 
$title="Government Entry Form";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
       
 ?>
 
<link rel="stylesheet" href="css/profile_style.css">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8" />
<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
            
             <div style="font-size: 18px;padding: 15px 0px 0px 0px;">
                    <a href="upload/documents/Claim_Workflow.pdf" style="float: right;"  target="_blank">View Claim Workflow</a> 
                </div> 
			 
            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Upload Reimbursement Acknowledgement Receipt </div>
                <div class="panel-body">
                    <div id="responselen"></div>
                
                    
                  
                     <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                         <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                               
                                <label for="learnercode">Please Enter Learner Code:<span class="star">*</span></label>
                                <input type="hidden" class="form-control" maxlength="18"  name="txtEID" id="txtEID"  value=""  placeholder="EmployeeID">
                                
                                <input type="text" class="form-control"  name="txtLCode" onkeypress="javascript:return allownumbers(event);"  id="txtLCode" value="<?php //echo $_SESSION['User_LearnerCode'];?>"  placeholder="LearnerCode">
				<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                            </div>
							
                           <div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
                            </div>
			</div>                                                     
                            
                                  
                        
                         
                     </from>
                    
                </div>
            </div> 
            

        </div>
    
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    
    $(document).ready(function () {
     
        //showData();

	$("#btnShow").click(function () {//alert("hello");
            if(txtLCode.value == '')
                {
                   $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter Learner Code First." + "</span></p>");
                }
            else{
                  
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");		
		$.ajax({
			type: "post",
                        url: "common/cfgoventryform.php",
			data: "action=SENDLCODETONEXTPAGE2&values=" + txtLCode.value + "",
			success: function (data)
				{
				//alert(data);
				$('#response').empty();
                                if(data == 'Success')
                                    {
                                      window.location.href = "frmgoventempUploadAckReceiptforalls.php?lcode="+ txtLCode.value + "";
                                    }
                                else if(data == 'AMNTREM')
                                    {
                                     $('#responselen').empty();
                                     $("#myModal").modal("hide");
                                     $('#responselen').append("<p class='error' style='width: 86%;'><span><img src=images/error.gif width=10px /></span><span>" + "   Amount Reimbursed to Beneficiary's Account.  " + "</span></p>");
                                    }
                                else{
                                    $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Invalid Learner Code." + "</span></p>");
                                    }
                                }
                });	
                  
                }
            	
          });
	 
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>


</html>