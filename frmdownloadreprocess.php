<?php
$title="Admission Re-Process Photo Sign";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var FunctionCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
?>
<div style="min-height:430px !important;max-height:auto !important;">
	 <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Admission Re-Process Photo Sign</div>
                <div class="panel-body">
					<form name="frmdownloadreprocessphotosign" id="frmdownloadreprocessphotosign" class="form-inline" role="form" enctype="multipart/form-data">
						<div class="container">
							<div class="container">						
								<div id="response"></div>
							</div>
							<div id="errorBox"></div>								
						</div>							
							<div id="gird" style="margin-top:5px;"> </div> 
						
						<div class="container">
							<div class="col-sm-4 form-group" id="download" style="display:none;">
									<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Download Photo"/>    
							</div>
							
							<div class="col-sm-4 form-group" id="downloadsign" style="display:none;">
									<input type="button" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Download Sign"/>    
							</div>
						</div>
						
							<div class="container" id="photonotfound" style="display:none; margin-top:25px;">
								<label for="batch"> Learner Photo Not Available:</label>
							  <?php if(isset($_SESSION['PhotoNotAvailable'])) { ?>
							    <textarea readonly name="notfound" id="notfound" class="form-control" cols="2" rows="10" style="width:800px;"> <?php echo $_SESSION['PhotoNotAvailable'] ?>  </textarea>								
							  <?php } ?>

							</div>
							
							<div class="container" id="signnotfound" style="display:none; margin-top:25px;">
								<label for="batch"> Learner Sign Not Available:</label>
							  <?php if(isset($_SESSION['SignNotAvailable'])) { ?>
							    <textarea readonly name="notfound" id="notfound" class="form-control" cols="2" rows="10" style="width:800px;"> <?php echo $_SESSION['SignNotAvailable'] ?>  </textarea>								
							  <?php } ?>

							</div>
                  </div>
            </div>   
        </div>
	</form>
</div>  
		<form id="frmdownloadlearnerphoto" name="frmdownloadlearnerphoto" action="frmdownloadreprocessphoto.php" method="post">
			<input type="hidden" id="dldcode" name="ddlcode" value="photo">
		</form>		
		
		<form id="frmdownloadlearnersign" name="frmdownloadlearnersign" action="frmdownloadreprocesssign.php" method="post">
			<input type="hidden" id="dldcode" name="ddlcode" value="photo">
		</form>	
		
 </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>                
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {			
		
            function showDataToDownload() {
					$('#response').empty();
					$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");              
						$.ajax({
							type: "post",
							url: "common/cfdownloadreprocessdata.php",
							data: "action=ShowDetailsToDownload",
							success: function (data) {
								$('#response').empty();
								 //alert(data);
								$("#gird").html(data);
								$('#example').DataTable();								
								$('#download').show();
								$('#downloadsign').show();
							}
						});
              return false;
            }
			
			showDataToDownload();
			
				$("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfdownloadreprocessdata.php"; // the script where you handle the form input.
                var data;
                var forminput=$("#frmdownloadreprocessphotosign").serialize();
                //alert(forminput);				
                if (Mode == 'Add') {
                    data = "action=Download&" + forminput; // serializes the form's elements.
                }
                else {
                    //data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						//alert(data);
						$('#response').empty();
                       if (data == "yes" || data == SuccessfullyUpdate) {
							var mode = data;
							if (mode == "yes") {
								$('#frmdownloadlearnerphoto').submit();								
								$('#photonotfound').show();																
								$("#btnSubmit").hide();
								
								$('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + "   Downloaded Successfull" + "</span></div>");
							}
						}
					}
                });
                return false; // avoid to execute the actual submit of the form.
            });
			
			
				$("#btnSubmit1").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfdownloadreprocessdata.php"; // the script where you handle the form input.
                var data;
                var forminput=$("#frmdownloadreprocessphotosign").serialize();
                //alert(forminput);				
                if (Mode == 'Add') {
                    data = "action=DownloadSign&" + forminput; // serializes the form's elements.
                }
                else {
                    //data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						//alert(data);
						$('#response').empty();
                       if (data == "yes" || data == SuccessfullyUpdate) {
							var mode = data;
							if (mode == "yes") {
								$('#frmdownloadlearnersign').submit();								
								//$('#photonotfound').show();
								$('#signnotfound').show();								
								$("#btnSubmit1").hide();
								
								$('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + "   Downloaded Successfull" + "</span></div>");
							}
						}
					}
                });
                return false; // avoid to execute the actual submit of the form.
            });
			
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }
        });
    </script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
	<script src="bootcss/js/frmcorrectionreport_validation.js"></script>
</html>