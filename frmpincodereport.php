<?php
$title = "ITGK report by PinCode";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Update'</script>";
}
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8' || $_SESSION['User_UserRoll'] == '23') {
    
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">ITGK report by PinCode</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmpincodereport" id="frmpincodereport" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="cc">Enter PinCode:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="6" minlength="6" onkeypress="javascript:return allownumbers(event);" name="txtpincode" id="txtpincode" placeholder="Enter PinCode">
                            <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        </div>

                        <div class="col-sm-8">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Report" style="margin-top:25px"/>    
                            <label for="tnc" id="btnVerified" style="margin-top:35px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>List Of IT-GKs with PinCode.</b></label>&nbsp;&nbsp;
                            <input type="button" name="btnReset" id="btnReset" class="btn btn-danger" value="Reset" onclick="window.location.href = 'frmpincodereport.php'" style="display: none;"/>    
                        </div>
                    </div>
                    <br>
                    <div class="panel panel-success" style="display:none;" id="report">
                        <div class="panel-heading">List of IT-GKs</div>
                        <div class="panel-body">
                            <div id="grid" name="grid" style="margin-top:35px;"> </div> 
                        </div>
                    </div>
                </form>
            </div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {




        $("#btnShow").click(function () {
            if (txtpincode.value == '' || txtpincode.value == '0' || txtpincode.value == '000000') {
                alert("Please enter valid PinCode.");
                return false;
            } else {


                //alert("gi");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfPinCodeReport.php",
                    data: "action=DETAILS&values=" + txtpincode.value + "",
                    success: function (data)
                    {//alert(data);
                        if(data =='0'){
                             $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Invalid PinCode OR No data found.</span></p>");
                            BootstrapDialog.alert("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Invalid PinCode OR No data found.</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmpincodereport.php";
                            }, 3000);
                
                        } else {
                            
                $("#btnShow").hide();
                $("#btnReset").show();
                $("#btnVerified").show();
                        $('#report').show(3000);
                        $('#txtpincode').attr('readonly', true);
                        //alert(data);
                        $('#response').empty();
                        $("#grid").html(data);
                        $('#example').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]
                        });
                        }
                    }
                });
            }
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    
    });

</script>

</html>