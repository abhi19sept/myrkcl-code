<?php
$title="Bio-Matric";
include ('header.php'); 
include ('root_menu.php');	
if (isset($_REQUEST['code'])) {
        echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
        echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    } else {
        echo "<script>var UserCode=0</script>";
        echo "<script>var Mode='Add'</script>";
    }	   
?>		

<div style="min-height:430px !important;max-height:auto !important;"> 
	<div class="container">
		<div class="panel panel-primary" style="margin-top:20px;">
			<div class="panel-heading">First time capture may take time, so wait after click the button "Click To Capture"</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
						<form name="frmLearnerCapture" id="frmLearnerCapture" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>												
						</div>
				 <div class="container">
							 <div class="col-md-1">     
                                 <img id="imgFinger" name="imgFinger"  class="form-control" style="width:80px;height:107px;" alt="" />
							 </div>
							 <div id="grid" name="grid"> </div> 
						</div>
						
			<div style="display:none;">
			 <div class="container">
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Serial No:<span class="star">*</span></label>
                                    <input type="text" name="tdSerial" id="tdSerial" class="form-control" readonly="true"/>
									
									<input type="hidden" name="txtLCourse" id="txtLCourse" class="form-control" readonly="true"/>
									
									<input type="hidden" name="txtLBatch" id="txtLBatch" class="form-control" readonly="true"/>
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Make:<span class="star">*</span></label>
                                    <input type="text" name="tdMake" id="tdMake" class="form-control" readonly="true"/>
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Model:<span class="star">*</span></label>
                                    <input type="text" name="tdModel" id="tdModel" class="form-control" readonly="true"/>
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Width:<span class="star">*</span></label>
                                    <input type="text" name="tdWidth" id="tdWidth" class="form-control" readonly="true"/>
							 </div>                                                        
                        </div>
						
						<div class="container">							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Height:<span class="star">*</span></label>
                                    <input type="text" name="tdHeight" id="tdHeight"  class="form-control" readonly="true"/>
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Local MAC:<span class="star">*</span></label>
                                    <input type="text" name="tdLocalMac" id="tdLocalMac"  class="form-control" readonly="true"/>
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Local IP:<span class="star">*</span></label>
                                    <input type="text" name="tdLocalIP" id="tdLocalIP" class="form-control" readonly="true"/>
							 </div>

							<div class="col-md-4 form-group">     
                                <label for="batch"> Status:<span class="star">*</span></label>
                                    <input type="text" name="txtStatus" id="txtStatus" class="form-control" readonly="true"/>
							 </div> 
						</div>				
			
			</div>
										
						
						<div class="container" style="display:none">							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Quality:<span class="star">*</span></label>
                                    <input type="text" name="txtQuality" id="txtQuality" value="" class="form-control" readonly="true"/>
									<input type="hidden" name="admission_code" id="admission_code" value="<?php echo $_REQUEST['code']; ?>">
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> NFIQ:<span class="star">*</span></label>
                                    <input type="text" name="txtNFIQ" id="txtNFIQ" value="" class="form-control" readonly="true"/>
							 </div> 							  
						</div>
						
				<div style="display:none;">
					<div class="container">							 
							 <div class="col-md-10 ">     
                                <label for="batch"> Base64Encoded ISO Template:<span class="star">*</span></label>
                                    <textarea id="txtIsoTemplate" rows="4" cols="12" name="txtIsoTemplate" class="form-control" readonly="true"></textarea>
							 </div> 
					    </div>
						
						<div class="container">							 
							 <div class="col-md-10">     
                                <label for="batch"> Base64Encoded ISO Image:<span class="star">*</span></label>
                                    <textarea id="txtIsoImage" rows="4" cols="12" name="txtIsoImage" class="form-control" readonly="true"></textarea>
							 </div> 
					    </div>
						
						<div class="container">							 
							 <div class="col-md-10">     
                                <label for="batch"> Base64Encoded Raw Data:<span class="star">*</span></label>
                                    <textarea id="txtRawData" rows="4" cols="12" name="txtRawData" class="form-control" readonly="true"></textarea>
							 </div> 
					    </div>
						
						<div class="container">							 
							 <div class="col-md-10">     
                                <label for="batch"> Base64Encoded Wsq Image Data:<span class="star">*</span></label>
                                    <textarea id="txtWsqData" rows="4" cols="12" name="txtWsqData" class="form-control" readonly="true"></textarea>
							 </div> 
					    </div>
					</div>	
					
					
					
					 <div class="container" style="margin-top:15px;">					         
                            <input type="button" name="btnVerify" id="btnVerify" class="btn btn-primary" value="Verify FingerPrint Duplicacy"/> 					 
						   <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" style="display:none" value="Submit"/> 					 
                     </div>							
						
                </div>
            </div>   
        </div>
    </form>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
</body>
<script src="js/jquery-1.8.2.js"></script>
 <script src="js/mfs100.js"></script>

 <script language="javascript" type="text/javascript">        
	var quality = 60; //(1 to 100) (recommanded minimum 55)
	var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )
		
		function Match() {
            try {
                var isotemplate = document.getElementById('txtIsoTemplate').value;
                var res = MatchFinger(quality, timeout, isotemplate);			
				 var res1 = CaptureFinger(quality, timeout);
         
                if (res.httpStaus) {
					document.getElementById('txtStatus').value = "ErrorCode: " + res1.data.ErrorCode + " ErrorDescription: " + res1.data.ErrorDescription;
                    if (res.data.Status) {
						document.getElementById('imgFinger').src = "data:image/bmp;base64," + res1.data.BitmapData;
                        alert("Finger matched");
                    }
                    else {
                        if (res.data.ErrorCode != "0") {
                            alert(res.data.ErrorDescription);
                        }
                        else {
                            alert("Finger not matched");
                        }
                    }
                }
                else {
                    alert(res.err);
                }
            }
            catch (e) {
                alert(e);
            }
            return false;
        }				
</script>
 
	<script type="text/javascript">
		var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
		var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
		var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
		var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
		$(document).ready(function () {
			
			var quality = 60; //(1 to 100) (recommanded minimum 55)
        var timeout = 60; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

        function GetInfo() {
			//alert("hii");
            document.getElementById('tdSerial').value = "";
            document.getElementById('tdMake').value = "";
            document.getElementById('tdModel').value = "";
            document.getElementById('tdWidth').value = "";
            document.getElementById('tdHeight').value = "";
            document.getElementById('tdLocalMac').value = "";
            document.getElementById('tdLocalIP').value = "";

            var res = GetMFS100Info();
            if (res.httpStaus) {

                document.getElementById('txtStatus').value = res.data.ErrorDescription;

                if (res.data.ErrorCode == "0") {
                    document.getElementById('tdSerial').value = res.data.DeviceInfo.SerialNo;
                    document.getElementById('tdMake').value = res.data.DeviceInfo.Make;
                    document.getElementById('tdModel').value = res.data.DeviceInfo.Model;
                    document.getElementById('tdWidth').value = res.data.DeviceInfo.Width;
                    document.getElementById('tdHeight').value = res.data.DeviceInfo.Height;
                    document.getElementById('tdLocalMac').value = res.data.DeviceInfo.LocalMac;
                    document.getElementById('tdLocalIP').value = res.data.DeviceInfo.LocalIP;
                }
            }
            else {
                alert(res.err);
            }
            return false;
        }
		
	GetInfo();
	
			
		
			
			 var quality = 60; //(1 to 100) (recommanded minimum 55)
        var timeout = 60; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

        

        function Capture() {
			//alert("h1");
            try {
				//alert("h2");
                document.getElementById('txtStatus').value = "";
                document.getElementById('imgFinger').src = "data:image/bmp;base64,";
                document.getElementById('txtQuality').value = "";
                document.getElementById('txtNFIQ').value = "";
                document.getElementById('txtIsoTemplate').value = "";
                document.getElementById('txtIsoImage').value = "";
                document.getElementById('txtRawData').value = "";
                document.getElementById('txtWsqData').value = "";
				
					alert(quality);
					alert(timeout);
					
                var res = CaptureFinger(quality, timeout);
                if (res.httpStaus) {
					//alert("h3");
                    document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;
					alert(res.data.ErrorCode);
                    if (res.data.ErrorCode == "0") {
						//alert("h4");
                        document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;
                        document.getElementById('txtQuality').value = res.data.Quality;
                        document.getElementById('txtNFIQ').value = res.data.Nfiq;
						alert(res.data.IsoTemplate);
                        document.getElementById('txtIsoTemplate').value = res.data.IsoTemplate;
                        document.getElementById('txtIsoImage').value = res.data.IsoImage;
                        document.getElementById('txtRawData').value = res.data.RawData;
                        document.getElementById('txtWsqData').value = res.data.WsqImage;
						
                    }
                }
                else {
                    alert(res.err);
                }
            }
            catch (e) {
                alert(e);
            }
            return false;
        }	
			
			
	    var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
       
        $.ajax({
            type: "POST",
            url: url,
            data: "action=GetLearner&values=" + AdmissionCode + "",
            success: function (data)
            {
				//alert(data);
				$("#grid").html(data);
				Capture();
			}
		});
		
		var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
       
        $.ajax({
            type: "POST",
            url: url,
            data: "action=GetLearnerCourse&values=" + AdmissionCode + "",
            success: function (data)
            {
				//alert(data);
				txtLCourse.value = data;					
			}
		});
		
		var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
       
        $.ajax({
            type: "POST",
            url: url,
            data: "action=GetLearnerBatch&values=" + AdmissionCode + "",
            success: function (data)
            {
				//alert(data);
				txtLBatch.value = data;				
			}
		});
		
		$("#btnVerify").click(function () {
			var isotemplate = document.getElementById('txtIsoTemplate').value;
            alert(isotemplate);
			alert(txtLCourse.value);
			alert(txtLBatch.value);
			
            var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.			 
			$.ajax({
				type: "POST",
				url: url,
				data: "action=GetExistingLearnerData&course=" + txtLCourse.value + "&batch=" + txtLBatch.value + "",				 
				success: function (data)
				{					
			        alert(data);
					if(data == ""){
						alert("Your Finger Print Duplicacy Checked Click on Submit Button to Register you Finger");
						$("#btnSubmit").show();
						$("#btnVerify").hide();
					}
		else {
						alert(data);
					data = $.parseJSON(data);
					
					alert(data);
					var flag =0;
					//var i=0;
					//$.each(data , function (index, value){
							//console.log(index + ': ' + value);
						//alert("hdhf");
					try {
						$.each(data, function(i, item){
							alert(i);
							alert(item);
							alert(data[i].BioMatric_ISO_Template);
						var res = VerifyFinger(isotemplate, data[i].BioMatric_ISO_Template);
						alert(res);
						alert(res.httpStaus);
						alert(res.data.Status);
						if (res.httpStaus)
							{
								if (res.data.Status) {
									flag=1;
								//alert("Finger matched");
								}
								else {
										if (res.data.ErrorCode != "0") {
										alert(res.data.ErrorDescription);
										}
										else {
											//alert("Finger not matched");
											//$("#btnSubmit").show();
											//$("#btnVerify").hide();
										}
									  }
							}
						else {
							alert(res.err);
						}
						   
					 });
					 if(flag==1){
							   alert("Finger Print is already enrolled , try with correct finger");
						   }
						   else{
							   alert("Your Finger Print Duplicacy Checked Click on Submit Button to Register you Finger");
								$("#btnSubmit").show();
								$("#btnVerify").hide();
						   }
					}
            catch (e) {
                alert(e);
            }
            return false;	
					//});
				  }
				}
			});		 
		});			
			
		$("#btnSubmit").click(function () {
         //alert("hhkj");		 
		$('#response').empty();
		$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
	    var data;
		var forminput=$("#frmLearnerCapture").serialize();
		
			data = "action=Add&" + forminput;
			//data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
		
		$.ajax({
			type: "POST",
			url: url,
			data: data,
			success: function (data)
			{
				if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
				{
					$('#response').empty();
					$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
					 window.setTimeout("window.close();", 3000);
					Mode = "Add";
					//resetForm("frmLearnerCapture");
					//window.close();
				}
				else
				{
					$('#response').empty();
					$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
				}
				//showData();
			}
		});
 
		return false; // avoid to execute the actual submit of the form.
	});


		});
	</script>
	</html>