<?php
$title="Govt. Entry Form Status Report";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var FunctionCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
?>
<div style="min-height:430px !important;max-height:1500px !important;">
	 <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Reimbursement Application Form Status Report</div>
                <div class="panel-body">
				   <form name="frmgovlearnerstatusreport" id="frmgovlearnerstatusreport" class="form-inline" role="form" enctype="multipart/form-data">
					<div class="container">
						<div class="container">						
							<div id="response"></div>
						</div>
							<div id="errorBox"></div>
								 <div class="col-md-4 form-group"> 
									<label for="eid">Employee Id:<span class="star">*</span></label>
									<input type="text" name="txteid" id="txteid" class="form-control" placeholder="Employee Id" maxlength="20">
								</div> 
					
								<div class="col-sm-4 form-group">                                  
									<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
								</div>
                    </div>				
					
                         <div id="gird" style="margin-top:5px;"> </div>                   
                 </div>
            </div>   
        </div>
	</form>
    </div>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?> 
 
<script type="text/javascript">
$('#txteid').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
});
</script>
              
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {			
			
			$("#btnSubmit").click(function () {			
				//var eid = $('#txtCentercode').val();			
				showData(txteid.value);			   
        });
        

            function showData(val1) {
			 if ($("#frmgovlearnerstatusreport").valid())
           {
                $('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			    $.ajax({							   
                    type: "post",
                    url: "common/cfGovLearnerStatusReport.php",
                    data: "action=SHOW&eid=" + val1 + "",
                    success: function (data) {
						$('#response').empty();
                            //alert(data);
                        $("#gird").html(data);
						$('#example').DataTable({						
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    }
                });
				}
            return false;
            }

            //showData();

        });

    </script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmgovlearnerstatus_validation.js"></script>
</html>