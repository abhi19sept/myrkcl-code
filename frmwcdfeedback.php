<?php
$title = "WCD Feedback Form";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var VisitCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var CC=" . $_REQUEST['cc'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var OrganizationCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:46px !important;">

            <div class="panel-heading">WCD Feedback Form</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmwcdfeedback" id="frmwcdfeedback"  role="form" action="" class="form-inline" enctype="multipart/form-data">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <div id="main-content">
                        <div class="panel panel-info">
                            <div class="panel-heading">ITGK Details</div>
                            <div class="panel-body">

                                <div id="errorBox"></div>
                                <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">ITGK Code:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="ITGKCode" id="ITGKCode" readonly="true" placeholder="ITGK Code">   
                                    <input type="hidden" class="form-control" name="txtVisitCode" id="txtVisitCode" readonly="true" placeholder="ITGK Code">
									<input type="hidden" class="form-control" name="RSCFACheck" id="RSCFACheck" readonly="true" placeholder="ITGK Code">
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="learnercode">Name of ITGK:<span class="star">*</span></label>
                                        <textarea class="form-control" name="txtName1" id="txtName1" readonly="true" placeholder="Name of the Organization/Center" oncopy="return false" onpaste="return false" oncut="return false" onkeypress="javascript:return allowchar(event);"></textarea>
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">ITGK Mobile:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="ITGKMobile" id="ITGKMobile" readonly="true" placeholder="ITGK Mobile">   
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">ITGK Email:<span class="star">*</span></label>
                                        <textarea class="form-control" name="ITGKEmail" id="ITGKEmail" readonly="true" placeholder="Name of SP"></textarea>   
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">ITGK District:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="ITGKDistrict" id="ITGKDistrict" readonly="true" placeholder="AO Code">   
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">ITGK Tehsil:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="ITGKTehsil" id="ITGKTehsil" readonly="true" placeholder="AO Code">   
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="learnercode">SP Code:<span class="star">*</span></label>
                                        <textarea class="form-control" name="SPCode" id="SPCode" readonly="true" placeholder="Name of the Organization/Center" oncopy="return false" onpaste="return false" oncut="return false" onkeypress="javascript:return allowchar(event);"></textarea>
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">SP Name:<span class="star">*</span></label>
                                        <textarea class="form-control" name="SPName" id="SPName" readonly="true" placeholder="Name of SP"></textarea>   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading">Feedback Form (Women Scheme).</div>
                            <div class="panel-body">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Query</th>
                                            <th scope="col">Response</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>No. of working PC available at ITGK</td>
                                            <td><input type="text" class="form-control" maxlength="2" name="desktop" id="desktop" value="0" placeholder="System Count" onkeypress="javascript:return allownumbers(event);">     </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Availability of Toilets within Premises</td>
                                            <td><label class="radio-inline"> <input type="radio" id="Ytoilet" name="toilet" value="Yes" /> Yes </label>
                                                <label class="radio-inline"> <input type="radio" id="Ntoilet" name="toilet" value="No"/> No </label></td>
                                        </tr>
                                        <tr id="Stoiletdiv">
                                            <th scope="row">3</th>
                                            <td>If YES than please confirm is separate Toilet for females</td>
                                            <td><label class="radio-inline"> <input type="radio" id="YStoilet" name="Stoilet" value="Yes" /> Yes </label>
                                                <label class="radio-inline"> <input type="radio" id="NStoilet" name="Stoilet" value="No"/> No </label></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">4</th>
                                            <td>Availability of Drinking Water for Learners</td>
                                            <td><label class="radio-inline"> <input type="radio" id="Ywater" name="water" value="Yes" /> Yes </label>
                                                <label class="radio-inline"> <input type="radio" id="Nwater" name="water" value="No"/> No </label></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">5</th>
                                            <td>Women Batch (Dec 2019) Inaugurated by Public Representative?</td>
                                            <td><label class="radio-inline"> <input type="radio" id="Yinaguarated" name="inaguarated" value="Yes" /> Yes </label>
                                                <label class="radio-inline"> <input type="radio" id="Ninaguarated" name="inaguarated" value="No"/> No </label></td>
                                        </tr>
                                        <tr id="uploaddiv">
                                            <th scope="row">6</th>
                                            <td>If YES than please confirm if ITGK has uploaded the details on MYRKCL Portal for both the courses (RS-CIT/RS-CFA) separately, if applicable?</td>
                                            <td><label class="radio-inline"> <input type="radio" id="Yupload" name="upload" value="Yes" /> Yes </label>
                                                <label class="radio-inline"> <input type="radio" id="Nupload" name="upload" value="No"/> No </label></td>
                                        </tr>
                                        <tr id="plandiv">
                                            <th scope="row">7</th>
                                            <td>If No than ask ITGK and enter when is it planned?</td>
                                            <td><input type="text" class="form-control" name="txtEstdate" id="txtEstdate" placeholder="Plan Date" readonly="true"></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">8</th>
                                            <td>Has Service Provider representative informed ITGK about compulsory Biometric Attendance for WCD Scheme?
                                            (For RS-CIT Women min 43 and For RS-CFA Women min 33 Nos.)</td>
                                            <td><label class="radio-inline"> <input type="radio" id="Yinformed" name="informed" value="Yes" /> Yes </label>
                                                <label class="radio-inline"> <input type="radio" id="Ninformed" name="informed" value="No"/> No </label></td>
                                        </tr>
                                        <tr id="FName">
                                            <th scope="row">9</th>
                                            <td>Name of RS-CFA Faculty (If Applicable)</td>
                                            <td><input type="text" class="form-control" name="FacultyName" id="FacultyName" placeholder="Faculty Name" onkeypress="javascript:return allowchar(event);"></td>
                                        </tr>
                                        <tr id="FQuali">
                                            <th scope="row">10</th>
                                            <td>Qualification of Faculty</td>
                                            <td><select name="ddlQualification" id="ddlQualification" class="form-control">
											<option value="0">Select Qualification</option>
											<option value="B.Com">B.Com</option>
											  <option value="M.Com">M.Com </option>
											  <option value="CA-Inter">CA-Inter </option>
											  <option value="ICWA-Inter">ICWA-Inter </option>
											  <option value="CS-Executive">CS-Executive </option>
											  <option value="MBA (Finance)">MBA (Finance) </option>
											  <option value="Certificate in Financial Accounting">Certificate in Financial Accounting </option>
											  <option value="Diploma in Financial Accounting">Diploma in Financial Accounting </option>
											  <option value="Degree in Financial Accounting">Degree in Financial Accounting</option>
											</td></select>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading">Feedback Form (Women Scheme) Present Learners Details.</div>
                            <div class="panel-body">
							<div>     
								<label for="area">Learners Available during Visit?:<span class="star">*</span></label> <br/>                               
								<label class="radio-inline"> <input type="radio" id="YLearner" name="Learner" value="Yes" /> Yes </label>
								<label class="radio-inline"> <input type="radio" id="NLearner" name="Learner" value="No"/> No </label>
							</div>
							<div id="Selection" style="display:none;">
                                <div class="col-sm-4 form-group"> 
                                <label for="Course">Select Course:<span class="star">*</span></label>
                                <select name="CourseName" id="CourseName" class="form-control">
                                </select>
                                </div>
                                <div class="col-sm-4 form-group"> 
                                <label for="Course">Select Batch:<span class="star">*</span></label>
                                <select name="ddlBatch" id="ddlBatch" class="form-control">
                                </select>
                                </div>
                                <div>
<div style="margin-bottom: 25px;" id="gird"></div>
                                </div>
								</div>
                            </div>
                        </div>

                        
                        <div class="container" id="divsubmit">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>

                    </div>
                </form>
            </div>   
        </div>
    </div>
</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    $('#txtEstdate').datepicker({
        format: "yyyy-mm-dd"
    });
</script>



<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
$('#txtVisitCode').val(VisitCode);

        $(function () {
            $("#txtEstdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });

		function CheckEligible() {
            $.ajax({
                type: "post",
                url: "common/cfWcdFeedback.php",
                data: "action=CheckEligible&cc=" + CC + "",
                success: function (data) {
					//alert(data);
					if(data == 'No Record Found'){
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Some unfair means has been used and the same has been informed to RKCL." + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>&nbsp; Some unfair means has been used and the same has been informed to RKCL.</span>");
                        window.setTimeout(function () {
                            window.location.href = "logout.php";
                        }, 4000);
					} else if (data =='Success'){
						CheckRSCFA();
						FillForm();
					} else {
						
					}
                }
            });
        }
        CheckEligible();
		
		  function CheckRSCFA() {
            $.ajax({
                type: "post",
                url: "common/cfWcdFeedback.php",
                data: "action=Check&cc=" + CC + "",
                success: function (data) {
					//alert(data);
					if(data == 'No Record Found'){
						$("#FName").hide();
						$("#FQuali").hide();
					} else if (data =='Success'){
						$("#RSCFACheck").val("1");
						$("#FName").show();
						$("#FQuali").show();
					} else {
						
					}
                }
            });
        }
        

        function FillForm() {
            $.ajax({
                type: "post",
                url: "common/cfWcdFeedback.php",
                data: "action=GETDETAILS&cc=" + CC + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "[]") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Some unfair means has been used and the same has been informed to RKCL." + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>&nbsp; Some unfair means has been used and the same has been informed to RKCL.</span>");
                        window.setTimeout(function () {
                            window.location.href = "logout.php";
                        }, 4000);
                    } else {
                        data = $.parseJSON(data);
                        $("#ITGKCode").val(data[0].itgkcode);
                        $("#txtName1").val(data[0].itgkname);
                        $("#ITGKMobile").val(data[0].itgkmobile);
                        $("#ITGKEmail").val(data[0].itgkemail);

                        $("#ITGKDistrict").val(data[0].district);
                        $("#ITGKTehsil").val(data[0].tehsil);
                        $("#SPCode").val(data[0].spcode);
                        $("#SPName").val(data[0].spname);
                    }
                }
            });
        }

        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfWcdfeedback.php",
                data: "action=FillCourse",
                success: function (data) {
                    $("#CourseName").html(data);
                }
            });
        }
        FillCourse();
        
        $("#CourseName").change(function () {
			$("#gird").html('');
           var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfWcdfeedback.php",
                data: "action=FILLBatchName&values=" + selCourse + "",
                success: function (data) {
					$("#ddlBatch").html(data);
                }
            });

        });
        
        $("#ddlBatch").change(function () {
            var batch = $(this).val();
            //alert(selcountry);
            $.ajax({
                url: 'common/cfWcdfeedback.php',
                type: "post",
                data: "action=GetLearners&values=" + CourseName.value + "&CenterCode=" + CC + "&batch=" + batch + "" ,
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
						scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                        
                    });
                }
            });
        });



        $("#Ytoilet").change(function () {
            $("#Stoiletdiv").show();
        });
        $("#Ntoilet").change(function () {
            $("#Stoiletdiv").hide();
        });

        $("#Yinaguarated").change(function () {
            $("#uploaddiv").show();
            $("#plandiv").hide();
        });
        $("#Ninaguarated").change(function () {
            $("#plandiv").show();
            $("#uploaddiv").hide();
        });
		$("#YLearner").change(function () {
            $("#Selection").show();
        });
		$("#NLearner").change(function () {
            $("#Selection").hide();
        });

        $("#frmwcdfeedback").submit(function () {
            //alert("1");
//            if ($("#frmwcdfeedback").valid())
//            {

//                if (document.getElementById('areaUrban').checked) //for radio button
//                {
//                    var area_type = 'Urban';
//                } else {
//                    area_type = 'Rural';
//                }

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                //BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfwcdFeedback.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmwcdfeedback").serialize();
//alert(forminput);
                data = "action=SubmitFeedback&" + forminput; // serializes the form's elements.
                //alert(data);
//            var file_data7 = $("#uploadImage7").prop("files")[0];
//            var file_data8 = $("#uploadImage8").prop("files")[0];
//            var file_data9 = $("#uploadImage9").prop("files")[0];
//            var file_data6 = $("#uploadImage6").prop("files")[0];// Getting the properties of file from file field
//            var form_data = new FormData(this);                  // Creating object of FormData class
//            form_data.append("panno", file_data7)
//            form_data.append("aadharno", file_data8)
//            form_data.append("addproof", file_data9)
//            form_data.append("appform", file_data6)// Appending parameter named file with properties of file_field to form_data
//            form_data.append("action", "ADD")
//            form_data.append("data", data)
//alert(form_data);
                $.ajax({
                    url: url,
//                cache: false,
//                contentType: false,
//                processData: false,
                    data: data, // Setting the data attribute of ajax with file_data
                    type: 'post',
                    success: function (data)
                    {
                        //alert(data);

                        if (data === "Successfully Inserted")
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Center Feedback Registered Successfully.</span>");
                            window.setTimeout(function () {
                                window.location.href = "frmwcdvisit.php";
                            }, 3000);

                            Mode = "Add";
                            resetForm("frmwcdvisit");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span>");
                        }



                    }
                });
           // }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<!--<script src="bootcss/js/frmwcdfeedback_validation.js"></script>-->



<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>