<?php
$title = "Modify NCR Application";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Modify NCR Application</div>
            <div class="panel-body">
                <form name="frmeditncrregistration" id="frmeditncrregistration" class="form-inline" role="form" enctype="multipart/form-data">

                    <div id="menuList" name="menuList" style="margin-top:35px;"> </div> 
                </form>
            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showAllOrgDetail(val, val1) {
//                if ($("#frmcorrectionapproved").valid())
//                {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfEditNewCenterRegistration.php",
                data: "action=ShowDetails",
                success: function (data) {
                    $('#response').empty();
                    $("#menuList").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print']
                    });
                }
            });
//                }
            return false;
        }

        showAllOrgDetail();


    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmcorrectionapproval_validation.js"></script>

</body>

</html>