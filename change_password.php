<?php
$title="Change Password";
include ('header.php'); 
include ('root_menu.php');
       
if (isset($_REQUEST['code'])) {
echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var Code=0</script>";
echo "<script>var Mode='Change'</script>";
}

?>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<style>
    /* Style all input fields */
    .inputsearch { width: 100%; padding: 12px; border: 1px solid #ccc; border-radius: 4px; box-sizing: border-box; margin-top: 6px; margin-bottom: 16px;}

    /* Style the submit button */
    input[type=submit] { background-color: #4CAF50; color: white;}

    /* Style the container for inputs */

    /* The message box is shown when the user clicks on the password field */
    #message { display:none; background: #f1f1f1; color: #000; position: relative; padding: 10px 20px; margin-top: 0px;}
    #message p { padding: 5px 35px; font-size: 18px;}

    /* Add a green text color and a checkmark when the requirements are right */
    .valid { color: green;}
    .valid:before { position: relative; left: -35px; content: "✔";}

    /* Add a red text color and an "x" when the requirements are wrong */
    .invalid { color: red;}
    .invalid:before { position: relative; left: -35px; content: "✖";}
    
    .field-icon {
        float: left;
        position: absolute;
        z-index: 2;
        right: 25px;
        top: 42px;
        /*  margin-left: -25px;
          margin-top: -130px;
          padding-left: -5px;
          position: relative;
          z-index: 2;*/
    }
</style>
<div style="height:100% !important">	
    <div class="container">   
        <div class="panel panel-primary" style="margin-top:50px;">
            <div class="panel-heading">Change Password</div>
            <div class="panel-body">
                <form class="form-inline" role="form" name="change_password_form" id="change_password_form">

                    <div class="container">
                        <div id="response"></div>
                    </div>
                    <div class="container">
                        <div id="userfields"  class="col-lg-6 col-md-6">

                                <label for="old"> Old Password </label>
                                <input type="password" name="old_password" id="old_password" class="inputsearch" maxlength="20" placeholder="Old Password"/>            
                                <br>
                        </div>
                        
                        <div id="userfields2"  class="col-lg-5 col-md-5" style="margin-right: 20px; display: none">
                                <label for="psw">New Password</label>
                                <span toggle="#psw" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                <input class="inputsearch" type="password" id="psw" name="psw" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
                                 <br>
                                <label for="psw">Re-Enter New Password</label>
                                <input class="inputsearch" type="password" id="repsw" name="repsw" required>
                                


                        </div>
                        <div id="message121" class="col-lg-6 col-md-6"  style="display: none">
                            <h3>Password must contain the following:</h3>
                            <p id="length" class="invalid">At least 8 characters </p>
                            <p id="letter" class="invalid">At least one lowercase </p>
                            <p id="capital" class="invalid">At least one uppercase</p>
                            <p id="number" class="invalid">At least one number</p>
                            <p id="special" class="invalid">At least one special characters</p>
                            <p>e.g. <b>Rkcl@4321</b></p>
                    </div>
                    </div>
                    
                    <div class="container">
                        <div  class="col-lg-6 col-md-6">
<!--                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/> -->
                            
                            <input type="button" id="btnOldpass" name="btnOldpass"  class="btn btn-primary"
                                                value="SUBMIT" style="padding:6px;"/>
                        
                        <input type="button" name="btnValidateOTP" id="btnValidateOTP" class="btn btn-primary"
                        value="VALIDATE OTP" style="padding:6px;display: none;"/>

                        <input type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary"
                        value="SUBMIT" style="padding:6px;display: none;"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</div>	
	<?php include ('footer.php'); ?>
	<?php include'common/message.php';?>

        <script>
            
            $(".toggle-password").click(function() {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
              input.attr("type", "text");
            } else {
              input.attr("type", "password");
            }
          });  
            
    var myInput = document.getElementById("psw");
    var letter = document.getElementById("letter");
    var capital = document.getElementById("capital");
    var number = document.getElementById("number");
    var special = document.getElementById("special");
    var length = document.getElementById("length");

// When the user clicks on the password field, show the message box
    myInput.onfocus = function () {
        document.getElementById("message").style.display = "block";
    }

// When the user clicks outside of the password field, hide the message box
    myInput.onblur = function () {
        document.getElementById("message").style.display = "none";
    }

// When the user starts to type something inside the password field
    myInput.onkeyup = function () {
        var flag='T';
        // Validate lowercase letters
        var lowerCaseLetters = /[a-z]/g;
        if (myInput.value.match(lowerCaseLetters)) {
            letter.classList.remove("invalid");
            letter.classList.add("valid");
        } else {
            letter.classList.remove("valid");
            letter.classList.add("invalid");
            flag='F';
        }

        // Validate capital letters
        var upperCaseLetters = /[A-Z]/g;
        if (myInput.value.match(upperCaseLetters)) {
            capital.classList.remove("invalid");
            capital.classList.add("valid");
        } else {
            capital.classList.remove("valid");
            capital.classList.add("invalid");
            flag='F';
        }

        // Validate numbers
        var numbers = /[0-9]/g;
        if (myInput.value.match(numbers)) {
            number.classList.remove("invalid");
            number.classList.add("valid");
        } else {
            number.classList.remove("valid");
            number.classList.add("invalid");
            flag='F';
        }
        
        //  Special Characters
        var SpecialCharacters = /[!@#$%^&*()]/g;
        if (myInput.value.match(SpecialCharacters)) {
            special.classList.remove("invalid");
            special.classList.add("valid");
        } else {
            special.classList.remove("valid");
            special.classList.add("invalid");
            flag='F';
        }
        // Validate length
        if (myInput.value.length >= 8) {
            length.classList.remove("invalid");
            length.classList.add("valid");
        } else {
            length.classList.remove("valid");
            length.classList.add("invalid");
            flag='F';
        }
        if(flag=='T')
        {
            $('#btnsubmit').show();     
        }else{
            $('#btnsubmit').hide();  
        }
    }
</script>	

	<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
	
    $(document).ready(function () {
		
	$("#btnOldpass").click(function () {
           		
            if ($('#old_password').length && $("#change_password_form").valid()) 
                {			
                    setProcessing();
                    var url = "common/cfchangepasswordNew.php"; // the script where you handle the form input.
                    var forminput = $("#change_password_form").serialize();
		    var data = "action=OLDPASSWORD&" + forminput; // serializes the form's elements.
	            //alert(data);
	            $.ajax({
	                type: "POST",
	                url: url,
	                data: data,
	                success: function (data)
	                {
                            $('#response').empty();
	                    if (data == 'MATCHED') {
	                    	//data = $.parseJSON(data);
	                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span>&nbsp;<span style='color: green;'>Congratulations!!! Old Password Matched.</span></p>");
	                        $("#userfields2").show(1000);
	                        $("#message121").show(1000);
                                $("#userfields").hide(1000);
                                $("#btnOldpass").hide(1000);
                                //$("#btnsubmit").show(1000);
	                    } else {
	                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> Old Password mismatch / invalid !! </span></p>");
	                    }
	                }
	            });
        	
                }
            return false; // avoid to execute the actual submit of the form.
        });
    
        $("#btnsubmit").click(function () {
        setProcessing();
        var newpass = $("#psw").val();
        var renewpass = $("#repsw").val();
        if (newpass != renewpass) {
        	$('#response').empty();
        	$('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Password not matched." + "</span></p>");
        }
        var url = "common/cfchangepasswordNew.php";
        var data = "action=setNewPassword&pass=" + newpass +  "&CnPass=" + renewpass + "";
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data) {
            	$('#response').empty();
                if (data == 1) 
                {
                    $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span style='color: green;'>  Password successfully changed.</span></p>");
                    window.setTimeout(function ()
                    {
                        window.location.href = 'dashboard.php';
                    }, 1000);
                } 
                else if (data == 'MORE3') 
                {
                    $('#response').append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span style='color: red;'>  Please DO NOT USE the same PASSWORD that you have user last 3 time.</span></p>");
                } 
                else {
                    $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "  Password mismatch / invalid !!" + "</span></p>");
                }
            }
        });
    });
        
        function setProcessing() {
    	$('#response').empty();
	    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
    }
		
	function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script>

$("#change_password_form").validate({
        rules: {
                        old_password: { required: true, minlength: 8, maxlength: 20}
						
        },			
        messages: {				
			old_password: { required: '<span style="color:red; font-size: 12px;">Please Enter Old Password.</span>',
                        		minlength:'<span style="color:red;">Please enter at least 8 characters</span>'}
			
			
        }
	});

</script>		
<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>