<?php
    $title = "ITGK : Update Address Request";
    include('header.php');
    include('root_menu.php');
    include 'common/modals.php';

    /*if (isset($_REQUEST['code'])) {
        echo "<script>var OrgCode=" . $_REQUEST['code'] . "</script>";
        echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    } else {
        echo "<script>var OrgCode=0</script>";
        echo "<script>var Mode='Add'</script>";
    }*/

    echo "<script>var OrgCode = '" . $_SESSION['User_Code'] . "'; </script>";


    if ($_SESSION["User_UserRoll"] <> 7) {
        echo "<script>$('#unauthorized').modal('show')</script>";
        die;
    }

?>


<style>

    .btn-success {
        background-color: #00A65A !important;
    }

    .btn-success:hover {
        color: #fff !important;
        background-color: #04884D !important;
        border-color: #398439 !important;
    }

    .asterisk {
        color: red;
        font-weight: bolder;
        font-size: 18px;
        vertical-align: middle;
    }

    .division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 20px;
        color: #575c5f;
        margin-bottom: 20px;

    }

    .extra-footer-class {
        margin-top: 0;
        margin-bottom: -16px;
        padding: 16px;
        background-color: #fafafa;
        border-top: 1px solid #e5e5e5;
    }

    #errorBox {
        color: #F00;
    }

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eeeeee;
        box-shadow: inset 0 0 5px 1px #d5d5d5;
    }

    .form-control {
        border-radius: 2px;
    }

    input[type=text]:hover,
    textarea:hover {
        box-shadow: 0 1px 3px #aaa;
        -webkit-box-shadow: 0 1px 3px #aaa;
        -moz-box-shadow: 0 1px 3px #aaa;
    }

    .col-sm-3:hover {
        background: none !important;
    }

    .hidden-xs {
        display: inline-block !important;
    }

    input.parsley-success,
    select.parsley-success,
    textarea.parsley-success {
        color: #468847;
        background-color: #DFF0D8;
        border: 1px solid #D6E9C6;
    }

    input.parsley-error,
    select.parsley-error,
    textarea.parsley-error {
        color: #B94A48;
        background-color: #F2DEDE;
        border: 1px solid #EED3D7;
    }

    .parsley-errors-list {
        margin: 2px 0 3px;
        padding: 0;
        list-style-type: none;
        font-size: 0.9em;
        line-height: 0.9em;
        opacity: 0;

        transition: all .3s ease-in;
        -o-transition: all .3s ease-in;
        -moz-transition: all .3s ease-in;
        -webkit-transition: all .3s ease-in;
    }

    .parsley-errors-list.filled {
        opacity: 1;
    }

    .parsley-required, .parsley-pattern {
        color: tomato;
        font-family: Calibri;
        margin-top: 4px;
        font-size: 15px;
    }

    select[disabled] {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 0.01px;
        text-overflow: '';
    }
</style>

<script src="bootcss/js/parsley.min.js"></script>

<div class="container" id="showdata" style="display: none;">


    <div class="panel panel-primary" style="margin-top:46px !important;">

        <div class="panel-heading">Organization Details</div>
        <div class="panel-body">

            <div style="padding-bottom: 5px;font-size: 15px;color: #575c5f;">
                Fields marked with <span class="asterisk">*</span> are mandatory
            </div>

            <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateOrgDetails" name="updateOrgDetails" enctype="multipart/form-data">

                <div class="division_heading">
                    Center Details
                </div>


                <div class="box-body" style="margin: 0 100px;">

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Organization Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center" readonly='readonly'>
                            <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Type of Organization <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <select id="txtType" name="txtType" class="form-control" data-parsley-required="true">
                                <option selected="selected" value="">Select</option>
                                <option value="1">
                                    RS-CIT
                                </option>
                            </select>
                            <input type="hidden" name="Organization_Type_old" id="Organization_Type_old">
                        </div>
                    </div>

<div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Reason of Address Change <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <select id="ddlReason" name="ddlReason" class="form-control" data-parsley-required="true">
                                <option selected="selected" value="">Select</option>
                                <option value="Shifting to own location">Shifting to own location</option>
                                 <option value="Shifting to prime business area">Shifting to prime business area</option>
                                  <option value="Non-agreement with Landlord ">Non-agreement with Landlord </option>
                            </select>
                            <input type="hidden" name="Organization_Type_old" id="Organization_Type_old">
                        </div>
                    </div>
                </div>

                <div class="division_heading">
                    Location Details
                </div>

                <div class="box-body" style="margin: 0 100px;">

                    <div class="form-group">
                        <label for="ddlCountry" class="col-sm-3 control-label">Country</label>
                        <div class="col-sm-3">
                            <input type="hidden" name="ddlCountry" id="ddlCountry">
                            <input type="text" id="country_name" name="country_name" class="form-control" readonly="readonly">
                        </div>

                        <label for="ddlState" class="col-sm-2 control-label">State </label>
                        <div class="col-sm-3">
                            <input type="hidden" id="ddlState" name="ddlState">
                            <input type="text" id="state_name" name="state_name" class="form-control" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ddlRegion" class="col-sm-3 control-label">Division/Region</label>
                        <div class="col-sm-3">
                            <input type="hidden" id="ddlRegion" name="ddlRegion">
                            <input type="text" id="region_name" name="region_name" class="form-control" readonly="readonly">
                        </div>

                        <label for="ddlDistrict" class="col-sm-2 control-label">District</label>
                        <div class="col-sm-3">
                            <input type="hidden" id="ddlDistrict" name="ddlDistrict">
                            <input type="text" id="district_name" name="district_name" class="form-control" readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ddlTehsil" class="col-sm-3 control-label">Tehsil</label>
                        <div class="col-sm-8">
                            <input type="hidden" id="ddlTehsil" name="ddlTehsil">
                            <input type="text" id="tehsil_name" name="tehsil_name" class="form-control" readonly="readonly">
                            <!--<select id="ddlTehsil" name="ddlTehsil" class="form-control"></select>-->
                        </div>
                    </div>

                </div>


                <div class="division_heading">
                    Enter Area Details to Modify
                </div>


                <div class="box-body" style="margin: 0 100px;">
                    
                    <div class="form-group">
                    <label for="Organization_NewTehsil" class="col-sm-3 control-label">Select Tehsil<span class="asterisk">*</span></label>
                        <div class="col-sm-2" style="padding-top: 7px;">
                            
                        <select id="ddlTehsilNew" name="ddlTehsilNew" class="form-control" data-parsley-required="true"></select>
                        </div>
                        <label for="area" class="col-sm-3 control-label">Select Area Type <span class="asterisk">*</span></label>

                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_AreaType" value="Rural" id="Rural">
                            <label for="Organization_AreaType">Rural</label>
                        </div>

                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_AreaType" value="Urban" id="Urban">
                            <label for="Organization_AreaType">Urban</label>
                        </div>

                        <input type="hidden" id="Organization_AreaType_old" name="Organization_AreaType_old">
                    </div>

                    <div id="urbanArea" style="display: none;">
                        <div class="form-group">
                            <label for="ddlMunicipalType" class="col-sm-3 control-label">Municipality Type <span class="asterisk">*</span></label>
                            <div class="col-sm-3">
                                <select id="ddlMunicipalType" name="ddlMunicipalType" class="form-control"></select>
                                <input type="hidden" id="Organization_Municipality_Type_old" name="Organization_Municipality_Type_old">
                            </div>

                            <label for="ddlDistrict" class="col-sm-2 control-label">Municipality Name <span class="asterisk">*</span></label>
                            <div class="col-sm-3">
                                <select id="ddlMunicipalName" name="ddlMunicipalName" class="form-control"></select>
                                <input type="hidden" id="Organization_Municipal_old" name="Organization_Municipal_old">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ddlWardno" class="col-sm-3 control-label">Ward Number <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <select id="ddlWardno" name="ddlWardno" class="form-control"></select>
                                <input type="hidden" id="Organization_WardNo_old" name="Organization_WardNo_old">
                            </div>
                        </div>
                    </div>

                    <div id="ruralArea" style="display: none;">
                        <div class="form-group">
                            <label for="ddlPanchayat" class="col-sm-3 control-label">Panchayat Samiti/Block <span class="asterisk">*</span></label>
                            <div class="col-sm-3">
                                <select id="ddlPanchayat" name="ddlPanchayat" class="form-control"></select>
                                <input type="hidden" id="Organization_Panchayat_old" name="Organization_Panchayat_old">
                            </div>

                            <label for="ddlGramPanchayat" class="col-sm-2 control-label">Gram Panchayat <span class="asterisk">*</span></label>
                            <div class="col-sm-3">
                                <select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control"></select>
                                <input type="hidden" id="Organization_Gram_old" name="Organization_Gram_old">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ddlVillage" class="col-sm-3 control-label">Village <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <select id="ddlVillage" name="ddlVillage" class="form-control"></select>
                                <input type="hidden" id="Organization_Village_old" name="Organization_Village_old">
                            </div>
                        </div>
                    </div>


                </div>


                <div class="division_heading">
                    Enter Address to Modify
                </div>

                <div class="box-body" style="margin: 0 100px;">


                    <div class="form-group">
                        <label for="area" class="col-sm-3 control-label">Select Premises<br> Ownership Type <span class="asterisk">*</span></label>

                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_OwnershipType" value="Own" id="Own">
                            <label for="Organization_ownershipType">Own</label>
                        </div>

                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_OwnershipType" value="Rent" id="Rent">
                            <label for="Organization_OwnershipType">Rented</label>
                        </div>
                        
                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_OwnershipType" value="Relative" id="Relative">
                            <label for="Organization_OwnershipType">Relative Property</label>
                        </div>
                        
                        <div class="col-sm-2" style="padding-top: 7px;">
                            <input type="radio" name="Organization_OwnershipType" value="Special" id="Special">
                            <label for="Organization_OwnershipType">Special Case</label>
                        </div>

                        <input type="hidden" id="Organization_OwnershipType_old" name="Organization_OwnershipType_old">
                    </div>
                    
                     <div class="box-body" id="owner" style="margin: 0 10px; display: none;" >

                        <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Enter Owner Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="Owner_Name" id="Owner_Name" placeholder="Owner Name"
                                   data-parsley-required="true" onkeypress="javascript:return validAddress(event);">
                        </div>
                    </div>

                </div>

<div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Nearest Landmark</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name="landmark" id="landmark" placeholder="Landmark"
                                   data-parsley-required="true" onkeypress="javascript:return validAddress(event);">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtAddress" class="col-sm-3 control-label">Full Address for Correspondence&nbsp;<span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <textarea id="txtAddress" name="txtAddress" class="form-control" rows="4" cols="50" data-parsley-required="true" data-parsley-pattern="^[\w.# ,-]+$" onkeypress="javascript:return validAddress2(event);"></textarea>
                            <input type="hidden" id="Organization_Address_old" name="Organization_Address_old">
                        </div>
                    </div>

                </div>

                <div class="division_heading">
                    Proof of Address Change
                </div>

                <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">Allowed File Type: jpg, jpeg, pdf<br>
                    Allowed Image/PDF File Size: Minimum: 50KB, Maximum: 2000KB</div>

                <br><br><br>

                <div id="onRent" style="display: none;">
                    <div class="box-body" style="margin: 0 100px;">
                        <div class="form-group">
                            <label for="rentAgreement" class="col-sm-3 control-label">Rent Agreement&nbsp;<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input id="fld_rentAgreement" name="fld_rentAgreement" type="file" class="file-loading">
                                <div id="document_block"></div>
                                <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">
                                    Rent Agreement should be on Rs. 100 Stamp & Notarized
                                </div>
                            </div>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="box-body" style="margin: 0 100px;">
                        <div class="form-group">
                            <label for="utilityBill" class="col-sm-3 control-label">Utility Bill &nbsp;<span
                                        class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input id="fld_utilityBill" name="fld_utilityBill" type="file" class="file-loading">
                                <div id="document_block"></div>
                                <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">Paid copy of Electricity/Water Bill in the name of premises owner</div>
                            </div>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                </div>

                <div id="onOwn" style="display: none;">
                    <div class="box-body" style="margin: 0 100px;">
                        <div class="form-group">
                            <label for="ownershipDocument" class="col-sm-3 control-label">Ownership Document&nbsp;<span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input id="fld_ownershipDocument" name="fld_ownershipDocument" type="file" class="file-loading">
                                <div id="document_block"></div>
                                <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">
                                    Evidencing support of location ownership
                                </div>
                            </div>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                </div>

                <div class="box-body" style="margin: 0 100px;">
                    <div class="form-group">
                        <label for="fld_document" class="col-sm-3 control-label">Front photo of new location&nbsp;<span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <input id="fld_document" name="fld_document" type="file" class="file-loading" data-parsley-required="true">
                            <div id="document_block"></div>
                            <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">A clear front photo of new location</div>
                        </div>

                    </div>
                </div>

                <input type="hidden" name="action" value="updateOrgDetails" id="action">
                <input type="hidden" name="reference_number" id="reference_number" value="<?php echo "<script>reference_number</script>";?>">

                <!-- /.box-body -->
                <div class="box-footer extra-footer-class">
                    <button type="submit" class="btn btn-lg btn-danger">Submit</button>
                </div>
                <!-- /.box-footer -->
            </form>

        </div>
    </div>
</div>


</body>
<?php
    include 'common/message.php';
    include 'footer.php';
?>

<script src="bootcss/js/frmupdateorgdetails_address.js"></script>

<script>

    function validAddress2(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("^[\\w.# ,-/]+$");

        if (key === 8 || key === 0 || key === 32) {
            keychar = "a";
        }
        return reg.test(keychar);
    }


    $("#fld_document, #fld_utilityBill, #fld_rentAgreement, #fld_ownershipDocument").fileinput({
        previewFileType: "image",
        allowedFileExtensions: ["jpg", "jpeg", "pdf"],
        previewClass: "bg-warning",
        showUpload: false,
        minImageWidth: 500,
        minImageHeight: 500,
        maxImageWidth: 3000,
        maxImageHeight: 3000,
        maxFileCount: 1,
        showCaption: false,
        maxFileSize: 2000,
        minFileSize: 50,
        browseClass: "btn btn-default",
        browseLabel: "Pick File",
        browseIcon: "<i class=\"fa fa-file\" aria-hidden=\"true\"></i> ",
        removeClass: "btn btn-danger",
        removeLabel: "Delete",
        removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
    });
//    
//    $("#Rent").change(function () {
//
//            $("#OwnerName").show();
//        });
//        
//        $("#Own").change(function () {
//
//            $("#OwnerName").hide();
//        });
        
</script>

</html>