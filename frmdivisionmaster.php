<?php
$title="Division Master";
include ('header.php'); 
include ('root_menu.php'); 

   
            if (isset($_REQUEST['code'])) {
                echo "<script>var DivisionCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var DivisionCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
<div style="min-height:430px !important;max-height:1500px !important;">
        <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Division Master</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmdivisionmaster" id="frmdivisionmaster" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="relegionname">Division Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtDivisionName" id="txtDivisionName" placeholder="Division Name">
                            </div>


                            




                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Division Status:</label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control" >
								  
                                </select>    
                            </div>
							
							
							
							
							
							
						

                        </div>  

                       



                       

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
						
						
                </div>
				<div id="gird"></div>
            </div>   
        </div>

</div>
    </form>




</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
 .star{
	color:red;
}
</style>

<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
            
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfDivisionMaster.php",
                    data: "action=DELETE&values=" + DivisionCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                            }, 3000);
                            Mode="Add";
                            resetForm("frmdivisionmaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfDivisionMaster.php",
                    data: "action=EDIT&values=" + DivisionCode + "",
                    success: function (data) {
                       alert(data);
                        //alert($.parseJSON(data)[0]['CountryName']);
                        data = $.parseJSON(data);
                        txtDivisionName.value = data[0].DivisionName;
                        ddlStatus.value = data[0].Status;
                        
                    }
                });
            }

            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfDivisionMaster.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);

                    }
                });
            }

            showData();


            $("#btnSubmit").click(function () {
			if ($("#frmdivisionmaster").valid())
			{
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfDivisionMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtDivisionName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + DivisionCode + "&name=" + txtDivisionyName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmdivisionmaster.php";
                           }, 1000);

                            Mode="Add";
                            resetForm("frmdivisionmaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			}
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmdivisionmastervalidation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>