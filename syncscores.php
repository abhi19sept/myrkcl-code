<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
date_default_timezone_set("Asia/Kolkata");

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require 'BAL/clsexamdetails.php';
$response = array();
$emp = new clsexamdetails();

$_action = (isset($_REQUEST["action"]) ? $_REQUEST["action"] : '');

if ($_action == "syncScores" && !empty($_REQUEST['examid'])) {
    $emp->syncLearnerScores($_REQUEST['examid'], $_REQUEST['type']);
    print 'Scores Synced for ' . $_REQUEST['type'] . '';
    exit;
}
