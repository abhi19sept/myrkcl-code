<?php
$title="Bank Account Details For Share Disbursement";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var BankAccountCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var BankAccountCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
?>
	
<div style="min-height:430px !important;max-height:auto !important;">
	<div class="container"> 
		
        <div class="panel panel-primary" style="margin-top:36px !important;">  
			<div class="panel-heading">Bank Account Details </div>
			<div class="panel-body">
				<form name="frmBankAccount" id="frmBankAccount" class="form-inline" role="form"  >
					<div class="container">
						<div class="container">
							<div id="response"></div>
						</div>        
							<div id="errorBox"></div>				
							
							
							<div class="col-sm-4 form-group" id='ifsc'>     
									<label for="order">IFSC Code:<font color="red">*</font></label>
									<input type="text" class="form-control" maxlength="11" name="txtIfscCode" id="txtIfscCode" placeholder="IFSC Code"  onkeypress="javascript:return validAddress(event);">
								</div>  
								
								
								<div class="col-sm-4 form-group">     
								
								<input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Validate IFSC Code" style="margin-top:25px"/>    
								
								
								</div> 
								
							</div>
							
							
							<div id="details" style="display:none">
							
							
							<div class="container">
							
							
							
							<div class="col-sm-4 form-group">     
									<label for="dob">Account Holder Name:<font color="red">*</font></label>
									<input type="hidden" class="form-control" maxlength="100" name="txtGenerateId" id="txtGenerateId"/>	
									<input type="text" class="form-control" maxlength="100" name="txtaccountName" id="txtaccountName"  placeholder="Account Name"/>
								</div>
								
								<div class="col-sm-4 form-group">     
									<label for="dob">Account Number:<font color="red">*</font></label>
									
									<input type="text" class="form-control" maxlength="20" name="txtaccountNumber" id="txtaccountNumber"  placeholder="Account Number" onpaste="return false;" onkeypress="javascript:return validAddress(event);"/>
								</div>
								
								
								<div class="col-sm-4 form-group" >     
									<label for="dob">Confirm Account Number:<font color="red" >*</font></label>
									
									<input type="text" class="form-control" maxlength="20" name="txtConfirmaccountNumber" id="txtConfirmaccountNumber" onpaste="return false;"  placeholder="Account Number"  onkeypress="javascript:return validAddress(event);"/>
									<span id='message'></span>
								</div>
								
								
								
								<div class="col-sm-5 form-group"> 
									<label for="status">Bank Name:</label>
									
									<input type="text" class="form-control" maxlength="500" name="ddlBankName" id="ddlBankName" placeholder="Bank Name" onpaste="return false;"  onkeypress="javascript:return validAddress(event);">
								</div>                            
							
							</div>
								
								
							
							<div class="container">
							   <div class="col-sm-4 form-group"> 
									<label for="status">Branch Name:</label>
									<input type="text" class="form-control" maxlength="500" name="ddlBranch" id="ddlBranch" placeholder="Branch Name" onpaste="return false;"  onkeypress="javascript:return validAddress(event);">
								</div>   
							
								<div class="col-sm-4 form-group">     
									<label for="order">MICR Code:</label>
									<input type="text" class="form-control" maxlength="14" name="txtMicrcode" id="txtMicrcode" placeholder="MICR Code" onpaste="return false;"  onkeypress="javascript:return validAddress(event);">
								</div>  
								
								<div class="col-sm-4 form-group">     
									<label for="gender">Account Type:<font color="red">*</font></label> <br/>                               
									<label class="radio-inline"> <input type="radio" id="rbtaccountType_saving" name="rbtaccountType_saving" checked="checked" value="Savings"/> Savings </label>
									<label class="radio-inline"> <input type="radio" id="rbtaccountType_saving" name="rbtaccountType_saving" value="Current"/> Current </label>									
								</div>
								
								<div class="col-sm-4 form-group"> 
									<label for="status">Pan Card No.:</label>
									<input type="text" class="form-control" maxlength="11" name="txtpanno" id="txtpanno"
										placeholder="Pan Card No." onpaste="return false;" onkeypress="javascript:return validAddress(event);">
								</div>  

							</div>
							
							<div class="container">
								
								<div class="col-sm-4 form-group"> 
									<label for="status">Pan Card Holder Name:</label>
									<input type="text" class="form-control" maxlength="100" name="txtpanname" id="txtpanname"
										placeholder="Pan Card Holder Name">
								</div>  
								
								<div class="col-sm-4 form-group"> 
								  <label for="photo">Attach Pan Card Copy:<span class="star">*</span></label>
								  <input type="file" class="form-control" id="pancard" name="pancard"  onchange="pancard(this)" >
								   <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =200KB</span>
								   <img src="images/Pencard.jpg" width="50" />
								</div> 

								<div class="col-sm-4 form-group"> 
									<label for="edistrict">Select Bank Account ID Proof:<span class="star">*</span></label>
									<select id="ddlidproof" name="ddlidproof" class="form-control">
									    <option value=""> Please Select </option>
										<option value="cheque"> Cancelled Cheque Copy </option>
										<option value="welcome"> Welcome Letter of Bank </option>
										<option value="passbook"> Passbook Copy </option>
									</select>    
								</div>	
							  
							  <div class="col-sm-4 form-group"> 
								  <label for="photo">Attach Bank Id Proof:<span class="star">*</span></label>
								  <input type="file" class="form-control" id="chequeImage" name="chequeImage"  onchange="BankIdProof(this)"  >
								   <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =200KB</span>
								   <img src="images/sbi.jpg" width="50" />
								</div>   
     
     
</div>
							
							
							
							<div class="container"> 
								<div class="col-md-1" style="width:20px !important;"> 
								<label class="checkbox-inline" style="float:left;"> <input type="checkbox" name="chk" id="chk" value="1" >
									
								</label>
								</div>
							<div class="col-md-11"> 	
															
								<label for="learnercode" style="float:left;margin:left:15px" ><b>I hereby declare that the above Bank and PAN card details are correct and accurate & belongs  only to me/my organization which is an authorized  ITGK of RKCL.I accept that RKCL shall transfer Share in RS-CIT course fee/ others programs (Pending as well as in future) only in this Bank A/c.  I/ my organization will be responsible for any consequences due to incorrect /inaccurate details submitted by me.</b><span class="star">*</span></label></br>                              
								
                            </div>
							
							
							
						</div>	
							
							<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
						  <div class="modal-content">
							<div class="modal-header">
								<button class="close" type="button" data-dismiss="modal">×</button>
								<h3 class="modal-title">Help </h3>
							</div>
							<div class="modal-body">
								<img src="images/cheque.jpg" width="620" />
							</div>
							<div class="modal-footer">
								<button class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						   </div>
						  </div>
						</div>
							</div>
							
							
	
							  <div class="container" id="sub" style="display:none">
							  <div class="col-md-1" style="width:100px;"> 							
								                              
								<a title="" style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><span id="fix"><input type="button" name="Help" id="Help" class="btn btn-primary" value="Help"  style="margin-top:10px"></span></a>    					
                            </div>
							  
							  <div class="col-md-1" style="width:100px;">
							  <input type="submit" name="validate" id="preview" class="btn btn-primary" value="Verify" style="margin-left:18px;margin-top:10px"/> 
							  </div>
                               
                        </div>
						
						<div id="gird"></div>
						
						<div class="container" id='prev' style="display:none">
						
						<div class='col-md-11 col-lg-11'>
						<table class='table table-user-information'>
						<tbody>
						<tr>
						<td class="col-md-2 col-lg-2">IFSC Code:</td>
						<td class="col-md-3 col-lg-3" id='ifsccode'></td>
						</tr>
						<tr>
						<td  class="col-md-2 col-lg-2">Account Holder Name:</td>
						<td class="col-md-3 col-lg-3" id='Accname'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Account Number:</td>
						<td class="col-md-3 col-lg-3" id='Accnumber'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Confirm Account Number:</td>
						<td class="col-md-3 col-lg-3" id='ConAccnumber'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Bank Name:</td>
					   <td class="col-md-3 col-lg-3" id='BankName'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Branch Name:</td>
						<td class="col-md-3 col-lg-3" id='BranchName'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">MICR Code:</td>
						<td class="col-md-3 col-lg-3" id='Micrcode'></td>
						</tr>
						
						<tr>
						<td class="col-md-2 col-lg-2">Account Type:</td>
						<td class="col-md-3 col-lg-3" id='Accounttype'></td>
						</tr>
						
						
						<tr>
						<td class="col-md-2 col-lg-2">Pan Card No:</td>
						<td class="col-md-3 col-lg-3" id='Pencard'></td>
						</tr>
						
						
						<tr>
						<td class="col-md-2 col-lg-2">Pan Card Holder Name:</td>
						<td class="col-md-3 col-lg-3" id='Pancardholder'></td>
						</tr>
						
						
						<tr>
						<td class="col-md-2 col-lg-2">Bank Account ID Proof:</td>
						<td class="col-md-3 col-lg-3" id='bankid'></td>
						</tr>
						
						

						 
						</tbody>
					   </table>
					   <input type="button" name="back" id="back" class="btn btn-primary" value="back"  /> 
					   <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="margin-left:15px"/>
						</div>
						  
						
						</div>
						
						
					</div>
                </div>
            </div>   
        </div>
    </form>
</div>
</body>

<?php include ('footer.php'); ?>
<?php include'common/message.php';?>

<script>
$("#preview").click(function () {
								var accountno = $("#txtaccountNumber").val();
								var confirmaccountno = $("#txtConfirmaccountNumber").val();
								
								if (accountno != confirmaccountno) {
									alert("Account No Does not match.");
									return false;
								}
								
								
								if ($("#frmBankAccount").valid())
								{	
								 
								document.getElementById("ifsccode").innerHTML = $("#txtIfscCode").val();
								document.getElementById("Accname").innerHTML = $("#txtaccountName").val();
								document.getElementById("Accnumber").innerHTML= $("#txtaccountNumber").val();
								document.getElementById("ConAccnumber").innerHTML = $("#txtConfirmaccountNumber").val();
								document.getElementById("BankName").innerHTML = $("#ddlBankName").val();
								document.getElementById("BranchName").innerHTML= $("#ddlBranch").val();
								document.getElementById("Micrcode").innerHTML = $("#txtMicrcode").val();
								document.getElementById("Accounttype").innerHTML = $('input[name=rbtaccountType_saving]:checked').val();
								document.getElementById("Pencard").innerHTML = $("#txtpanno").val();
								document.getElementById("Pancardholder").innerHTML= $("#txtpanname").val();
								document.getElementById("bankid").innerHTML = $("#ddlidproof").val();
								
								
                                            $("#prev").show();
											
											$("#details").hide();
											$("#sub").hide();
											$("#gird").hide();
											$("#ifsc").hide();
											
									        		 

                                  }
                               return false;  
                                });
								
								
								
</script>
<script src="scripts/uploadbankaccount.js"></script>


<style>
  .modal-dialog {width:700px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
<script type="text/javascript">
  
  $(document).ready(function() {
		jQuery(".fix").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>

<script>
$('#txtConfirmaccountNumber').on('keyup', function () {
    if ($(this).val() == $('#txtaccountNumber').val()) {
        $('#message').html('confirmed').css('color', 'green');
    } else $('#message').html('confirm account no. should be same as account no').css('color', 'red');
});
</script>



<script language="javascript" type="text/javascript">


function pancard(target) {
	var ext = $('#pancard').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("chequeImage").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			        
		alert("Image size should less or equal 200 KB");
		document.getElementById("chequeImage").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("chequeImage").value = '';
				return false;
	}    
    document.getElementById("chequeImage").innerHTML = "";
    return true;
}





function BankIdProof(target) {
	var ext = $('#chequeImage').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("chequeImage").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			        
		alert("Image size should less or equal 200 KB");
		document.getElementById("chequeImage").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("chequeImage").value = '';
				return false;
	}    
    document.getElementById("chequeImage").innerHTML = "";
    return true;
}






</script>
	
	<script type="text/javascript">
		var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
		var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
		var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
		var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
		$(document).ready(function () {
			
			
	      
			
			
			if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
			
			function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBankAccount.php",
                    data: "action=EDIT&values=" + BankAccountCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Make']);
                        //alert(data);
                        data = $.parseJSON(data);
                       
                        txtaccountName.value = data[0].Bank_Account_Name;
                        txtaccountNumber.value = data[0].Bank_Account_Number;
                        rbtaccountType_saving.value = data[0].Bank_Account_Type;
                        txtIfscCode.value = data[0].Bank_Ifsc_code;
						ddlBankName.value = data[0].Bank_Name;
						txtMicrcode.value = data[0].Bank_Micr_Code;
						ddlBranch.value = data[0].Bank_Branch_Name;
						txtpanno.value = data[0].Pan_No;
						txtpanname.value = data[0].Pan_Name;
						ddlidproof.value = data[0].Bank_Id_Proof;
						
						
                        
                    }
                });
            }
			
			
			
			
			
			
			
			
			
			 function deleteRecord()
             {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfBankAccount.php",
                    data: "action=DELETE&values=" + BankAccountCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmbankaccount.php";
                           }, 1000);
                            
                            Mode="Add";
                            resetForm("frmBankAccount");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }
		
		
		    function FillStatus() {
		        $.ajax({
		            type: "post",
		            url: "common/cfBankAccount.php",
		            data: "action=FILL",
		            success: function (data) {
		                $("#ddlBankName").html(data);
		            }
		        });
		    }
		
		    FillStatus();
			
			
			
			
			function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfBankAccount.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						

                    }
                });
            }

            
			
			
			function GenerateUploadId()
                   {
					$.ajax({
						type: "post",
						url: "common/cfBlockUnblock.php",
						data: "action=GENERATEID",
						success: function (data) {                      
							txtGenerateId.value = data;					
						}
					});
                  }
                  GenerateUploadId();
			
			
			
			function FillBranch() {
		        $.ajax({
		            type: "post",
		            url: "common/cfBankAccount.php",
		            data: "action=FILLBRANCH",
		            success: function (data) {
		                $("#ddlBranch").html(data);
		            }
		        });
		    }
		
		    FillBranch();
										
								
								
								$("#back").click(function () {
								
								
                                            $("#details").show();
											
											$("#prev").hide();
											
											$("#gird").show();
											$("#sub").show();
											$("#ifsc").show();
										
                                });
							
                                
			
			
			
			
			
			                  $("#btnShow").click(function () {
								//alert(1);
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                               var url = "common/cfBankAccount.php"; // the script where you handle the form input.
								var data;
								var forminput=$("#frmBankAccount").serialize();
								
									data = "action=FILLDETAILS&" +forminput; // serializes the form's elements.
								
                                    $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
										$('#response').empty();
                                           
											alert(data);
                                            data = $.parseJSON(data);
											
											ddlBankName.value = data[0].bankname;
											txtMicrcode.value = data[0].micrcode;
											ddlBranch.value = data[0].branchname;
                                            $("#details").show();
											$('#txtIfscCode').attr('readonly', true);
											$("#btnShow").hide();
											$("#sub").show();
									        showData();		 

                                    }
                                });
							
                                return false; // avoid to execute the actual submit of the form.
                            });
			
			  $("#btnSubmit").click(function () {
				 
			var accountno = $("#txtaccountNumber").val();
            var confirmaccountno = $("#txtConfirmaccountNumber").val();
            
			if (accountno != confirmaccountno) {
                alert("Account No Does not match.");
                return false;
            }
            
			
			if ($("#frmBankAccount").valid())
            {
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBankAccount.php"; // the script where you handle the form input.
				var chequeimage=$('#chequeImage').val(); 
               var data;
                var forminput=$("#frmBankAccount").serialize();
                if (Mode == 'Add')
                {
                    data = "action=ADD&chequeimage=" + chequeimage + "&" + forminput; // serializes the form's elements.
                }
                else
                {
					
					data = "action=UPDATE&code=" + BankAccountCode +"&" + forminput;
                    //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                             window.setTimeout(function () {
                                window.location.href = "frmBankAccount.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmBankAccount");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			 }
                return false; // avoid to execute the actual submit of the form.
            });
		    function resetForm(formid) {
		        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
		    }
		
		});
		
	</script>
	
	<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmbankaccount_validation.js" type="text/javascript"></script>	
</html>