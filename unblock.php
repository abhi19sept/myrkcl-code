<?php
$title="Block Center";
include ('header.php'); 
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
			<div class="container"> 
			 

            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">UnBlock A Center</div>
                <div class="panel-body"> 
					<form name="frmblock" id="frmblock" class="form-inline" role="form" enctype="multipart/form-data"> 
						<div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
								 <div class="col-sm-4 form-group">     
                                <label for="learnercode">Center Code:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtCenterCode" id="txtCenterCode" placeholder="Center Code"/>								
                            </div>  
						</div>  
						
						 <div class="container">
						<p class="btn submit" id="btnviewcontainer"> 
                            <input type="button" name="btnView" id="btnView" class="btn btn-primary" value="View Detail"/> 
						</p>
                        </div>
					
						
						<div id="tblBlock" style="display:none;">
						<div class="container">                            
                            <div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Center Name:</label>
								<input type="text" id="txtCenterName" name="txtCenterName" class="form-control"  readonly="true">                               
                            </div>											
							
								<div class="col-sm-4 form-group" > 
								  <label for="photo">Tehsil:</label> 
								   <input type="text" id="txtTehsil" name="txtTehsil" class="form-control" disabled="true">								  									  
								</div>
							
							<div class="col-sm-4 form-group" > 
								  <label for="photo">District:</label> 
								   <input type="text" id="txtDistrict" name="txtDistrict" class="form-control" disabled="true">								  									  
								</div>
								
								<div class="col-sm-4 form-group" > 
								  <label for="photo"> Mobile No:</label> 
								   <input type="text" id="txtMobile" name="txtMobile" class="form-control" disabled="true">								  									  
								</div>						
						</div>
						
						<div class="container">                            
<!--                            <div class="col-sm-4 form-group"> 
                                <label for="payreceipt">DLC:</label>
								<input type="text" id="txtDLC" name="txtDLC" class="form-control" disabled="true">                               
                            </div>											
							
								<div class="col-sm-4 form-group" > 
								  <label for="photo">PSA:</label> 
								   <input type="text" id="txtPSA" name="txtPSA" class="form-control" disabled="true">								  									  
								</div>-->
                                                                <div class="col-sm-4 form-group" > 
								  <label for="photo">Service Provider:</label> 
								   <input type="text" id="txtSP" name="txtSP" class="form-control" disabled="true">								  									  
								</div>
							
							<div class="col-sm-4 form-group" > 
								  <label for="photo">Category:</label> 
								  <select id="ddlBlockCategory" name="ddlBlockCategory" class="form-control">
                                  </select>								 							  									  
							</div>
								
							<div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Date:</label>
								<input type="text" id="txtBlockDate" name="txtBlockDate" class="form-control" placeholder="YYYY-MM-DD">                               
                            </div>	
											
						</div>
						
						<div class="container"> 							
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Reason:</label> 
							   <textarea id="txtRemark" name="txtRemark" class="form-control"></textarea>								  									  
							</div>		
						</div>
						
						<div class="container">
						<div class="col-sm-5">     
                                <label for="address">Block Category:</label> <br/> 
                              <label class="radio-inline"> <input type="radio" onclick="toggle_visibility1('ddlcourse');" id="center" name="blockmethod" checked="checked"/> Block Center </label>
							  <label class="radio-inline"> <input type="radio" onclick="toggle_visibility('ddlcourse');" id="course" name="blockmethod"  /> Block Course </label>
                            </div>
							
						<div class="col-sm-4 form-group" id="ddlcourse" style="display:none;"> 
						  <label for="photo">Select Course:</label> 
						   <select id="ddlCourse" name="ddlCourse[]"  multiple = "multiple" class="form-control">
									
							</select>						  									  
						</div>	
							
				</div>
						
						<div class="container" style="margin-top:15px;">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Unblock Center"/>    
                         <!--   <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Unblock Center"/>   --> 
                        </div>						
					</div>
              </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>      

<script type="text/javascript"> 
 $('#txtBlockDate').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
	});  
	</script>
	
 <script type="text/javascript">
<!--
    function toggle_visibility(id) {
       var e = document.getElementById(id);       
          e.style.display = 'block';
    }
	
	function toggle_visibility1(id) {
       var e = document.getElementById(id);      
          e.style.display = 'none';
    }
//-->
</script>   
                  
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
     //   var SuccessfullyDone = "<?php echo Message::OnUpdateStatement ?>";
        $(document).ready(function () {

            function GetCenterDetail() {
				//alert("HUI");
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GETCENTERDETAIL1&CenterCode=" + txtCenterCode.value,
                    success: function (data) {
						//alert(data);
                        $('#response').empty();
                        if (data == 'Error')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Center Already UnBlocked. or Not Found Please Check your Center Code</span></p>");
                        }
                        else
                        {
                            data = $.parseJSON(data);
                           
                            txtCenterName.value = data.CenterName;
                            txtTehsil.value=data.Tehsil;
                            txtDistrict.value=data.District;
                            txtMobile.value=data.Mobile;
                            
                            txtSP.value=data.SP;
                            tblBlock.style['display'] = "block";
                            btnviewcontainer.style['display'] = "none";
                            //txtCenterCode.disabled = true;
                        }

                    }
                });
            }
			
			function GetCourse(){
				$.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GETUnBlockCourse&CenterCode=" + txtCenterCode.value,
                    success: function (data) { 
						//alert(data);
					//data = $.parseJSON(data);
						$('#ddlCourse').html(data);
					}
                });
			}

            $("#btnView").click(function () {
				 $('#response').empty();
                GetCenterDetail();
				GetCourse();
                                 $('#txtCenterCode').attr('readonly', true);
                //GetCenterDetail();
            });



            function FillBlockCategory() {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=FILLUNBLOCKCATEGORY",
                    success: function (data) {
                        $("#ddlBlockCategory").html(data);
                    }
                });
            }
            FillBlockCategory();

            $("#btnReset").click(function () {
                window.location.href = "block.php";

            });


            $("#btnSubmit").click(function () {
				$('#response').empty();
				$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				var url = "common/cfBlockUnblock.php"; // the script where you handle the form input.
				if (document.getElementById('center').checked) //for radio button
					{
						var radiocheck = 'no';
					}
					else {
					radiocheck = 'yes';
					}
				 var data;				
				 var forminput=$("#frmblock").serialize();				 
				 data = "action=UNBLOCK&radiovaue=" + radiocheck + "&" + forminput;
                $.ajax({
                    type: "POST",
                        url: url,
                        data: data,
                    success: function (data) {                        
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate || data >0)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Successfully UnBlocked.</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "unblock.php";
                            }, 1000);
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>IT-GK Already UnBlocked.</span></p>");
                        }
                    }
                });
				 return false; 
            });

				function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }
        });

    </script>
</html>
