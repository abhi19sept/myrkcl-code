<?php
$title = "Message Master";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
    echo "<script>var UserCode='" . $_SESSION['User_Code'] . "'</script>";
    echo "<script>var UserParentID='" . $_SESSION['User_ParentId'] . "'</script>";
    echo "<script>var UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
    echo "<script>var MessageCode='" . $_REQUEST['code'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var UserParentID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var MessageCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '3' || $_SESSION['User_Code'] == '4' || $_SESSION['User_Code'] == '11' || $_SESSION['User_Code'] == '28'  || $_SESSION['User_Code'] == '8' || $_SESSION['User_Code'] == '14') {
?>
<link rel="stylesheet" href="css/datepicker.css">
<link rel="stylesheet" href="css/bootstrap-multiselect.css">
    <style>
        .marginB20{
            margin-bottom:20px;
        }
        #hint{
            position:absolute;
            z-index: 99999;
        }
        td{
            background: lavender;

        }
    </style>
<script src="scripts/datepicker.js"></script>
<div class="container"> 

    <div class="panel panel-primary" style="margin-top:36px !important;">  
        <div class="panel-heading">Message Board</div>
        <div class="panel-body">

            <form name="frmmessagemaster" id="frmmessagemaster" class="form-horizontal form-validate form-inline" novalidate >

                        <div id="response"></div>


                    <div id="errorBox"></div>


                    <div class="col-md-12 marginB20">

                            <label for="Mesage" class="col-sm-2">Select Category:</label>
                            <div class="col-md-6">
                            <select required id="txtcat" name="txtcat[]" class="form-control"><br>


                            </select>
                                </div>
                            <button type="button" name="addCat" id="addCat" class="btn btn-primary"><i class="fa fa-plus"></i>Add New Category</button>

                    </div>
                <div class=" col-md-12 marginB20">

                    <label for="Mesage" class="col-sm-2">Template Title:</label>
                    <div class="col-sm-8">
                        <input type="title" required class="form-control" name="txttitle" id="txttitle" placeholder="Enter Title">

                    </div>
                </div>



                    <div class="col-md-12 marginB20">

                            <label for="Mesage" class="col-sm-2">Enter Message:</label>

                        <div class="col-sm-8">
                            <textarea required class="form-control" maxlength="5000" rows="4" cols="100" name="txtMessageTemp" id="txtMessageTemp" placeholder="Enter Message"></textarea>

                        </div>
                    </div>

                <div class="col-md-12 marginB20">

                    <label for="Mesage" class="col-sm-2">Select Type:</label>
                    <div class="col-md-6">
                        <select required id="txttype" name="txttype" class="form-control"><br>


                        </select>
                    </div>
                    <button type="button" name="addType" id="addType" class="btn btn-primary"><i class="fa fa-plus"></i>Add New Type</button>

                </div>


                <div class="col-md-12">

                    <label for="Mesage" class="col-sm-2">Message For:</label>
                    <div class="col-sm-8">
                        <input type="text" required class="form-control"  name="txtmessagefor" id="txtmessagefor">
                    </div>
                </div>



                    <br>
                    <br>
                    <div class="col-md-12">

                        <input type="submit" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Submit"/>

                    </div>
                    <div class="col-md-12">
                        <div id="gird" style="margin-top:25px;"> </div>
                    </div>

                    <!--					<div class="container">
                                                                    <div class="container">
                                                                            <div id="response"></div>
                                                                    </div>        
                                                                            <div id="errorBox"></div>
                                                                                    <div bgcolor="CCCCCC" align="left" colspan="3">     
                                                                                            Learner List
                                                                                    </div>
                                                    
                                                                            <div id="gird" style="margin-top:25px;"> </div>	
                                                            </div>-->

            </form>
        </div>
        </div>
</div>



    <div tabindex="-1" class="modal fade" id="catModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">

                    <div id="responsemodal"></div>

                                <label for="txtname" >Name:</label>
                                <input type="text" id="txtname" name="txtname" class="form-control">
                                <input type="hidden" id="txtmestype" name="txtmestype" class="form-control">



                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary" id="submitCat">Add</button>
                </div>
            </div>
        </div>
    </div>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        // autoclose: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script>	

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $("#btnSubmit1").on('click',function () {

            // debugger;
            if ($("#frmmessagemaster").valid())
            {
                 $('#response').empty();
                 $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                 var url = "common/cfMessageMaster.php"; // the script where you handle the form input.
                var data;
                var cat =$("#txtcat").val();
                var type =$("#txttype").val();
                var Mfor =$("#txtmessagefor").val();
                if (cat == null || cat === false) {
                    cat = '';
                }
                if (type == null || type === false) {
                    type = '';
                }


                 var txt =$("#txtMessageTemp").val();
                 var title =$("#txttitle").val();

                 if (Mode == 'Add')
                 {
                     data = "action=ADD&txt="+txt+"&title="+title+"&cat="+cat+"&type="+type+"&Mfor="+Mfor; // serializes the form's elements.
                 }
                 else
                 {

                     data = "action=UPDATE&id="+MessageCode+"&txt="+txt+"&title="+title+"&cat="+cat+"&type="+type+"&Mfor="+Mfor;
                     //console.log(data);return false;
                     //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                 }

                 $.ajax({
                     type: "post",
                     url: "common/cfMessageMaster.php",
                     data: data,
                     success: function (data1)
                     {

                         if (data1 == SuccessfullyInsert)
                         {

                             $('#response').empty();
                             $('#response').append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span>" + data1 + "</span></p>");

                             Mode = "Add";
                             resetForm("frmmessagemaster");
                         }
                         else if(data1 == SuccessfullyUpdate){


                                 $('#response').empty();
                                 $('#response').append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span>" + data1 + "</span></p>");

                                 Mode = "Edit";
                                 //resetForm("frmmessagemaster");

                         }
                         else
                         {
                             $('#response').empty();
                             $('#response').append("<p class='error'><span><img  width=10px /></span><span>" + data1 + "</span></p>");

                             Mode = "Add";
                            // resetForm("frmmessagemaster");
                         }
                         showData();


                     }
                 });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        if (Mode == 'Edit')
        {
            fillForm();
        }
$("#txtenddate").change(function () {
    var startDate = document.getElementById("txtstartdate").value;
    var endDate = document.getElementById("txtenddate").value;
 
    if ((Date.parse(startDate) >= Date.parse(endDate))) {
        alert("End date should be greater than Start date");
        document.getElementById("txtenddate").value = "";
    }
});
//                        function FillStatus() {
//                            $.ajax({
//                                type: "post",
//                                url: "common/cfStatusMaster.php",
//                                data: "action=FILL",
//                                success: function (data) {
//                                    $("#ddlStatus").html(data);
//                                }
//                            });
//                        }
//
//                        FillStatus();







        function fillForm()
        {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfMessageMaster.php",
                data: "action=EDIT&values=" + MessageCode + "",
                success: function (data) {
                    //alert(data);
                    //alert($.parseJSON(data)[0]['Status']);
                    data = $.parseJSON(data);
                    txtMessageTemp.value = data[0].Message_Text;
                    txttitle.value = data[0].Message_Title;
                    txtcat.value = data[0].Message_Cat_ID;
                    txttype.value = data[0].Message_Type_ID;
                    txtmessagefor.value = data[0].Message_For;
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfMessageMaster.php",
                data: "action=SHOW",
                success: function (data) {
                    //alert(data);
                    $("#gird").html(data);
                   var t =  $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    t.on( 'order.dt search.dt', function () {
                        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            cell.innerHTML = i+1;
                        } );
                    }).draw();

                }
            });
        }

        showData();

    });


    function FillCategories()
    {
        //alert("HII");

        $.ajax({
            type: "post",
            url: "common/cfMessageMaster.php",
            data: "action=FILLCAT",
            success: function (data) {
                var parseData = $.parseJSON(data);
                $.each(parseData, function (i, v) {
                    $("#txtcat").append('<option value="' + v.Message_Cat_ID + '" >' + v.Message_Cat_Name + '</option>');
                });

                /*$('#txtcat').multiselect({
                        includeSelectAllOption: true,
                        enableClickableOptGroups: true,
                        numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true,

                        templates: {
                            button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                            filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                            filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                        }
                });*/
            }
        });
    }
    FillCategories();
    function FillTypes()
    {
        //alert("HII");

        $.ajax({
            type: "post",
            url: "common/cfMessageMaster.php",
            data: "action=FILLTYPE",
            success: function (data) {
                var parseData = $.parseJSON(data);
                $.each(parseData, function (i, v) {
                    $("#txttype").append('<option value="' + v.Message_Type_ID + '" >' + v.Message_Type_Name + '</option>');
                });
               /* $('#txttype').multiselect({
                    includeSelectAllOption: true,
                    enableClickableOptGroups: true,
                    numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true,

                    templates: {
                        button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                        filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                        filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                    }
                });*/
            }
        });
    }


    FillTypes();

    $('#addCat').on('click', function() {
        $('#responsemodal').empty();
        $('#catModal').modal('show')
        $('.modal-title').html('Add Message Category');
        $('#txtmestype').val('category')
    });

    $('#addType').on('click', function() {
        $('#responsemodal').empty();
        $('#txtname').val('')
        $('#catModal').modal('show')
        $('.modal-title').html('Add Message Type');
        $('#txtmestype').val('type')
    });

    $('#submitCat').on('click', function() {
        var entity  = $('#txtmestype').val()
        if(entity=='category'){
           if($('#txtname').val()==''){
             $('#responsemodal').html("<p class='error'>Please Enter Name</p>")
               return false;
           }
            $.ajax({
                type: "post",
                url: "common/cfMessageMaster.php",
                data: "action=ADDCAT&name="+$('#txtname').val(),
                success: function (data) {

                    if (data == 'Successfully Inserted'){
                        $('#responsemodal').empty();

                        $('#responsemodal').append("<p class='success'>Submitted Successfully</p>");

                        $("#txtcat").empty();
                        $("#txtcat").multiselect('destroy');
                        FillCategories();
                        window.setTimeout(function () {
                            $('#txtname').val('');
                            $('#catModal').modal('hide')

                        }, 1000);
                    }
                    else{
                        $('#responsemodal').empty();

                        $('#responsemodal').append("<p class='error'>Duplicate Entry '"+$('#txtname').val()+"'</p>");
                    }

                },
                error: function (jqxhr, status, msg) {
                   console.log('fsdfsdf')
                }
            });
        }

        if(entity=='type'){
            if($('#txtname').val()==''){
                $('#responsemodal').html("<p class='error'>Please Enter Name</p>")
                return false;
            }
            $.ajax({
                type: "post",
                url: "common/cfMessageMaster.php",
                data: "action=ADDTYPE&name="+$('#txtname').val(),
                success: function (data) {
                    if (data == 'Successfully Inserted') {
                        $('#responsemodal').empty();

                        $('#responsemodal').append("<p class='success'>Submitted Successfully</p>");
                        $("#txttype").empty();
                        $("#txttype").multiselect('destroy');
                        FillTypes();
                        window.setTimeout(function () {
                            $('#txtname').val('');
                            $('#catModal').modal('hide')

                        }, 1000);
                    }
                    else{
                        $('#responsemodal').append("<p class='error'>Duplicate Entry '" + $('#txtname').val() + "'</p>");
                    }
                }
            });
        }

    });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    $('body').on('click','.deleteRecord',function(){

        var id = $(this).attr('data-id');
        var tr = $(this);
        var table = $('#example').DataTable();
        if (confirm("Do You Want To Delete This Item ?"))
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfMessageMaster.php",
                data: "action=DELETE&values=" + id + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        table
                            .row( tr.parents('tr') )
                            .remove()
                            .draw();

                    }

                }
            });
        }

    })


</script>

	<style>
.error {
	color: #D95C5C!important;
}
</style>
    <?php include ('footer.php'); ?>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmmessagemaster_validation.js"></script>
    <script src="js/bootstrap-multiselect.js"></script>

</html>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>