<?php
$title = "WCD Innaugration count Report";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' OR $_SESSION['User_UserRoll'] == '17' OR $_SESSION['User_UserRoll'] == '4') {	
            ?>


<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">WCD Inauguration Count Report</div>
            <div class="panel-body">

                <form name="frmInauguration" id="frmInauguration" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        
                         	<div class="col-md-4 form-group"> 
                                <label for="course"> Course Name <span class="star">*</span></label>
                              
								<select id="ddlCourse" name="ddlCourse" class="form-control" required >
                                  
                                </select>
								
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                  
                                </select>
                            </div> 
												
                        

                        

                    </div> 
					<div class="row">
						  <div class="col-xs-12">
						 <div id="grid" ></div>
						 </div>
						 </div>
					
                </form>
            </div>
        </div>
    </div>
</div>

<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
						/*for Course  function */
						function FillCourse() 
						{
							//alert("hello");
							$.ajax({
								type: "post",
								url: "common/cfInnaugrationdetails.php",
								data: "action=FILLCOURSE",
								success: function (data) {
									//alert(data);
									
									$("#ddlCourse").html(data);
								}
							});
						}
						FillCourse();
						
						
						/*for Batch function */
						$("#ddlCourse").change(function () 
						{
							var selCourse = $(this).val();
							//alert(selCourse);
							$.ajax({
								type: "post",
								url: "common/cfBatchMaster.php",
								data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
								success: function (data) {
									
									//alert(data);
									$("#ddlBatch").html(data);
								}
							});
						});

						$("#ddlBatch").change(function () 
						{
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

						$.ajax({
							type: "post",
							url: "common/cfInnaugrationcount.php",
							data: "action=GETDETAILS&batch=" + ddlBatch.value + "",
							success: function (data)
							{
								$('#response').empty();
								$("#grid").html(data);
								$('#example').DataTable({
									dom: 'Bfrtip',
									buttons: [
										'copy', 'csv', 'excel', 'print'
									],
									  scrollY: 400,
										scrollCollapse: true,
										paging: false

								});
								
							}
						});
					});
          
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>
