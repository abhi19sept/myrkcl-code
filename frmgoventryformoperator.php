<?php
$title="Government Entry Form";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
		if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '30') {
 ?>

<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			 
            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Reimbursement Application  Form</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">LearnerCode:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="18" onkeypress="javascript:return allownumbers(event);" name="txtLCode" id="txtLCode" placeholder="LearnerCode">
								<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                            </div>
							
							<div class="col-sm-4 form-group">     
                                <label for="learnercode">CenterCode:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="18" onkeypress="javascript:return allownumbers(event);" name="txtCenterCode" id="txtCenterCode" placeholder="CenterCode">
							</div>
							
							<div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                            </div>
						</div>
						

                    <div id="main-content" style="display:none;">
					     <div class="container">
							<div class="col-sm-4 form-group"> 
                                <label for="ename">Employee Name:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control text-uppercase" maxlength="50" name="txtEmpName" id="txtEmpName" placeholder="Employee Name">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Employee Father/Husband Name:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control text-uppercase" maxlength="50" name="txtFaName" id="txtFaName"  placeholder="Employee Father Name">
                            </div>
							
							 <div class="col-sm-4 form-group"> 
                                <label for="email">Employee EmailId:<span class="star">*</span></label>
                                <input type="email" class="form-control" name="txtEmail" id="txtEmail" placeholder="Emp EmailId">    
                            </div>
                            
							<div class="col-sm-4 form-group">     
                                <label for="emobile">Employee Mobileno:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="10" name="txtEmpMobile" id="txtEmpMobile" placeholder="Emp Mobileno" >
                            </div>
                        </div>  

                        <div class="container">                            
								
							<div class="col-sm-4 form-group">     
                                <label for="address">Employee Office Address:<span class="star">*</span></label>
                                <textarea class="form-control" rows="3" id="txtEmpAddress" onkeypress="javascript:return validAddress(event);" name="txtEmpAddress" placeholder="Emp Office Address"></textarea>
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="gpfno">Employee GPF No:<span class="star">*</span></label>
                                <input type="text"  class="form-control" maxlength="20" name="txtGpfNo" onkeypress="javascript:return allownumbers(event);" id="txtGpfNo" placeholder="Emp GPF No">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Employee District:<span class="star">*</span></label>
                                <select id="ddlempdistrict" name="ddlempdistrict" class="form-control" >								  
                                </select>    
                            </div>							

                            <div class="col-sm-6 form-group"> 
                                <label for="deptname">Employee Department Name:<span class="star">*</span></label>
                                <select id="ddlDeptName" name="ddlDeptName" class="form-control">
								
                                </select>
                            </div>
                        </div>    

                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="empid">Employee Id:<span class="star">*</span></label>
                                <input type="text" class="form-control"  name="txtEmpId" onkeypress="javascript:return allownumbers(event);"  id="txtEmpId" placeholder="Employee Id">
                            </div>                            

                            <div class="col-sm-4 form-group"> 
                                <label for="designation">Employee Designation:<span class="star">*</span></label>
                                <select id="ddlEmpDesignation" name="ddlEmpDesignation" class="form-control">
                                          
                                </select>
                            </div>     

                            <div class="col-sm-4 form-group"> 
                                <label for="marks">RS-CIT Exam Marks:<span class="star">*</span></label>
                                <input type="text" maxlength="3" class="form-control" id="txtEmpMarks" name="txtEmpMarks" onkeypress="javascript:return allownumbers(event);"
									placeholder="RS-CIT Exam Marks">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="attempt">Select RS-CIT Exam Attempt:<span class="star">*</span></label>
                                <select id="ddlExamattepmt" name="ddlExamattepmt" class="form-control">
                                    <option value="" selected="selected">Please Select</option>   
                                    <option value="1"> 1</option>>
                                    <option value="2"> 2</option>>
                                </select>
                            </div>
                        </div>
						
                        <div class="container">
     						<div class="col-sm-4 form-group"> 
                                <label for="pan">PAN No:<span class="star">*</span></label>
                                <input type="text" maxlength="10" class="form-control text-uppercase" onkeypress="javascript:return validAddress(event);"  name="txtPanNo" id="txtPanNo"  placeholder="PAN No">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="bankaccount">Employee Bank Accountno:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="18" name="txtBankAccount" onkeypress="javascript:return allownumbers(event);"  id="txtBankAccount" placeholder="Emp Bank Accountno">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict">Employee Bank District:<span class="star">*</span></label>
                                <select id="ddlBankDistrict" name="ddlBankDistrict" class="form-control">
                                </select>
                            </div> 

							<div class="col-sm-4 form-group"> 
                                <label for="bankname">Employee Bank Name:<span class="star">*</span></label>
                                <select id="ddlBankName" name="ddlBankName" class="form-control text-uppercase">
                                    <option value="" selected="selected">Please Select</option>                
                                </select>                                                              
                            </div>  
                        </div>

                        <div class="container">         
							<div class="col-sm-4 form-group"> 
                                <label for="branchname">Branch_Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" onkeypress="javascript:return allowchar(event);"  name="txtBranchName" id="txtBranchName"  placeholder="EMP Branch_Name">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="ifsc">IFSC Code:<span class="star">*</span></label>
                                <input type="text" maxlength="11" class="form-control text-uppercase" onkeypress="javascript:return validAddress(event);"  name="txtIfscNo"  id="txtIfscNo" placeholder="EMP Bank IFSC Code">
                            </div> 
                            
                              <div class="col-sm-4 form-group"> 
                                <label for="micr">MICR Code:<span class="star">*</span></label>
                                <input type="text" maxlength="15" class="form-control" onkeypress="javascript:return allownumbers(event);"   name="txtMicrNo" id="txtMicrNo" placeholder="EMP Bank MICR Code">
                            </div>

							<div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Pre Payment Receipt:<span class="star">*</span></label>
                                <input type="file" class="form-control"  name="fileReceipt" id="fileReceipt" onchange="checkPayReceipt(this)">
                            </div>  
						</div>
                        
                        <div class="container">                      
                            <div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Upload Birth Proof:<span class="star">*</span></label>
                                <input type="file" class="form-control"  name="fileBirth" id="fileBirth" onchange="checkdob(this)">
                            </div> 
      
							<div class="col-sm-4 form-group"> 
                                <label for="dobprrof">Date of Birth Proof:<span class="star">*</span></label>
                                <select id="ddlDobProof" name="ddlDobProof" class="form-control">
                                                 
                                </select>
                            </div> 
						</div>

						<div class="container">  
							<div class="col-md-4 form-group"> 							
								<label for="learnercode">Terms and conditions :<span class="star">*</span></label></br>                              
								<label class="checkbox-inline"> <input type="checkbox" name="chk" id="chk" value="1" >
									<a title="" style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><span id="fix">click here</span> </a>
								</label>								
                            </div>
						</div>	
						
						<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button class="close" type="button" data-dismiss="modal">×</button>
									<h3 class="modal-title">Terms and Conditions</h3>
								</div>
								<div class="modal-body">
									<img src="images/1.jpg" />
								</div>
								<div class="modal-footer">
									<button class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
						    </div>
						  </div>
						</div>

                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
					</div>
                </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>

<script src="scripts/govfileupload.js"></script>
<style>
#errorBox{
 color:#F00;
 }
</style>

<style>
  .modal-dialog {width:700px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
  
<script type="text/javascript">  
  $(document).ready(function() {
		jQuery(".fix").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script language="javascript" type="text/javascript">
function checkPayReceipt(target) {
	var ext = $('#fileReceipt').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileReceipt").value = '';
			return false;
		}

    if(target.files[0].size > 50000) {
			
        //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
		alert("Image size should less or equal 50 KB");
		document.getElementById("fileReceipt").value = '';
        return false;
    }
	else if(target.files[0].size < 20000)
			{
				alert("Image size should be greater than 20 KB");
				document.getElementById("fileReceipt").value = '';
				return false;
			}
    document.getElementById("fileReceipt").innerHTML = "";
    return true;
}
</script>

<script language="javascript" type="text/javascript">
function checkdob(target) {
	var ext = $('#fileBirth').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("fileBirth").value = '';
			return false;
		}

    if(target.files[0].size > 50000) {
			
        //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
		alert("Image size should less or equal 50 KB");
		document.getElementById("fileBirth").value = '';
        return false;
    }
	else if(target.files[0].size < 20000)
			{
				alert("Image size should be greater than 20 KB");
				document.getElementById("fileBirth").value = '';
				return false;
			}
    document.getElementById("fileBirth").innerHTML = "";
    return true;
}
</script>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function FillEmpDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryformoperator.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlempdistrict").html(data);
                }
            });
        }
        FillEmpDistrict();
		
		function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();

        function FillEmpDepartment() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryformoperator.php",
                data: "action=FILLEMPDEPARTMENT",
                success: function (data) {
                    $("#ddlDeptName").html(data);
                }
            });
        }
        FillEmpDepartment();

        function FillEmpDesignation() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryformoperator.php",
                data: "action=FILLEMPDESIGNATION",
                success: function (data) {
                    $("#ddlEmpDesignation").html(data);
                }
            });
        }
        FillEmpDesignation();

        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryformoperator.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        FillBankDistrict();

		$("#ddlBankDistrict").change(function(){
			FillBankName(this.value);
		});
        function FillBankName(districtid) {
            $.ajax({
                type: "post",
                url: "common/cfgoventryformoperator.php",
                data: "action=FILLBANKNAME&districtid=" + districtid,
                success: function (data) {
                    $("#ddlBankName").html(data);
                }
            });
        }        

        function FillDobProof() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryformoperator.php",
                data: "action=FillDobProof",
                success: function (data) {
                    $("#ddlDobProof").html(data);
                }
            });
        }
        FillDobProof();

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }

        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=DELETE&values=" + StatusCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=EDIT&values=" + StatusCode + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    txtStatusName.value = data[0].StatusName;
                    txtStatusDescription.value = data[0].StatusDescription;
                    //alert($.parseJSON(data)[0]['StatusName']);
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=SHOW",
                success: function (data) {
                    $("#gird").html(data);
                }
            });
        }

        //showData();

		$("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");		
					$.ajax({
						type: "post",
                        url: "common/cfgoventryformoperator.php",
						data: "action=DETAILS&values=" + txtLCode.value + "&centercode=" + txtCenterCode.value +"",
						success: function (data)
						{
							//alert(data);
							 $('#response').empty();
							 if(data == ""){
							 $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Learnercode and CenterCode" + "</span></p>");
							 }
							 else{
							data = $.parseJSON(data);
							txtEmpName.value = data[0].Admission_Name;
							txtFaName.value = data[0].Admission_Fname;
							txtEmail.value = data[0].Admission_Email;
							txtEmpMobile.value = data[0].Admission_Mobile;
							txtEmpAddress.value = data[0].Admission_Address;
							txtGpfNo.value = data[0].Admission_GPFNO;
							
							$("#main-content").show();
							$('#txtLCode').attr('readonly', true);
							$('#txtCenterCode').attr('readonly', true);
							$("#btnShow").hide();
							 }
							
					    }
                });		
          });
		
        $("#btnSubmit").click(function () {			
			if ($("#form").valid())
           {			    
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfgoventryform.php"; // the script where you handle the form input.
            var payreceipt = $('#txtLCode').val();
            var birthproof = $('#txtLCode').val();
            var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&lcode=" + txtLCode.value + "&centercode=" + txtCenterCode.value +"&empname=" + txtEmpName.value + "&faname="+ txtFaName.value + 
					   "&empdistrict="+ ddlempdistrict.value + "&empmobile="+ txtEmpMobile.value + "&empemail="+ txtEmail.value + 
					   "&empaddress="+ txtEmpAddress.value + "&empdeptname="+ ddlDeptName.value + "&empid="+ txtEmpId.value + 
					   "&empgpfno="+ txtGpfNo.value + "&empdesignation="+ ddlEmpDesignation.value + "&empmarks="+ txtEmpMarks.value + 
					   "&empexamattempt="+ ddlExamattepmt.value + "&emppan="+ txtPanNo.value + "&empbankaccount="+ txtBankAccount.value + 
					   "&empbankdistrict="+ ddlBankDistrict.value + "&empbankname="+ ddlBankName.value + "&empbranchname="+ txtBranchName.value +
					   "&empifscno="+ txtIfscNo.value + "&empmicrno="+ txtMicrNo.value + "&fileReceipt=" + payreceipt + "&fileBirth=" + birthproof +
					   "&empdobproof="+ ddlDobProof.value + ""; //serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + StatusCode + "&name=" + txtStatusName.value + "&description=" + txtStatusDescription.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   //alert(data);
				   if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmgoventryformoperator.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });
		 }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmgoventry_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?> 