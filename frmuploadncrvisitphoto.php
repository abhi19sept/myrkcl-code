<?php
$title = "Upload NCR Center Visit Photos";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code='" . $_REQUEST['code'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var Ack='" . $_REQUEST['ack'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}

$random = (mt_rand(1000, 9999));
$random .= date("y");
$random .= date("m");
$random .= date("d");
$random .= date("H");
$random .= date("i");
$random .= date("s");

echo "<script>var OrgDocId= '" . $random . "' </script>";
?>
<div id="googleMap"  class="mapclass"></div>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Upload NCR Center Visit Photos</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmuploadncrphoto" id="frmuploadncrphoto" role="form" action="" class="form-inline" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>

                    <div id="main-content" style="display:none;">
                        <div class="container">
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Organization/Center:</label>
                                <input type="text" class="form-control" readonly="" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                                <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                                <input type="hidden" class="form-control" maxlength="50" name="txtCenterCode" id="txtCenterCode"/>
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Registration No:</label>
                                <input type="text" class="form-control" readonly="" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:</label>
                                <input type="text" class="form-control" readonly="" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:</label>
                                <input type="text" class="form-control" readonly="" name="txtType" id="txtType" placeholder="Type Of Organization">  
                            </div>
                        </div>  
                        <br>
                        <div class="container">

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Document Type:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                                <input type="hidden" class="form-control"  name="txtDistrictCode" id="txtDistrictCode">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Tehsil:</label>
                                <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="address">Address:</label>
                                <textarea class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road"></textarea>

                            </div>
                        </div>  

                        <div class="panel panel-info">
                            <div class="panel-heading">Upload NCR Center Visit Photos</div>
                            <div class="panel-body">
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <div class="panel panel-danger form-group ">
                                    <div class="panel-heading">Upload Theory Room Photo</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach Photo:<span class="star">*</span></label>
                                            <img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">								  
                                            <input style=" width: 115%" id="uploadImage7" type="file" name="uploadImage7" onchange="checkPANPhoto(this);
                                                    PreviewImage(7)"/>	
                                            <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                            <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                                        </div>
                                    </div>
                                </div>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <div class="panel panel-danger form-group">
                                    <div class="panel-heading">Upload Reception Photo</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach Photo:<span class="star">*</span></label> 
                                            <img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage8').click();">								  
                                            <input style=" width: 115%" id="uploadImage8" type="file" name="uploadImage8" onchange="checkAADHARPhoto(this);
                                                    PreviewImage(8)"/>	
                                            <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                                        </div>
                                    </div>
                                </div>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <div class="panel panel-danger form-group">
                                    <div class="panel-heading">Upload Exterior Photo</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach Photo:<span class="star">*</span></label> 
                                            <img id="uploadPreview9" src="images/sampleproof.png" id="uploadPreview9" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage9').click();">								  
                                            <input style=" width: 115%" id="uploadImage9" type="file" name="uploadImage9" onchange="checkAddressProof(this);
                                                    PreviewImage(9)"/>	
                                            <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                                        </div>
                                    </div>
                                </div>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                &nbsp;                        
                                <div class="panel panel-danger form-group">
                                    <div class="panel-heading">Upload Other Photo</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12" > 
                                            <label for="photo">Attach Photo:<span class="star">*</span></label>  
                                            <img id="uploadPreview6" src="images/sampleproof.png" id="uploadPreview6" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage6').click();">								  
                                            <input style=" width: 115%" id="uploadImage6" type="file" name="uploadImage6" onchange="checkNCRApp(this);
                                                    PreviewImage(6)"/>	
                                            <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span10
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                    <div class="container">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Upload Photos"/>    
                    </div>

            </div>
            </form>
        </div>
    </div>   
</div>
</div>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>

<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
    ;
</script>
<script language="javascript" type="text/javascript">
    function checkPANPhoto(target) {
        var ext = $('#uploadImage7').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg']) == -1) {
            alert('Image must be in jpg Format');
            document.getElementById("uploadImage7").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        }
        document.getElementById("uploadImage7").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkAADHARPhoto(target) {
        var ext = $('#uploadImage8').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg']) == -1) {
            alert('Image must be in jpg Format');
            document.getElementById("uploadImage8").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("uploadImage8").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage8").value = '';
            return false;
        }
        document.getElementById("uploadImage8").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkAddressProof(target) {
        var ext = $('#uploadImage9').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg']) == -1) {
            alert('Image must be in jpg Format');
            document.getElementById("uploadImage9").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("uploadImage9").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage9").value = '';
            return false;
        }
        document.getElementById("uploadImage9").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkNCRApp(target) {
        var ext = $('#uploadImage6').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg']) == -1) {
            alert('Image must be in jpg Format');
            document.getElementById("uploadImage6").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("uploadImage6").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage6").value = '';
            return false;
        }
        document.getElementById("uploadImage6").innerHTML = "";
        return true;
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function fillForm()
        {           //alert(val);    
            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=APPROVE&values=" + Code + "",
                success: function (data) {
                    var data = data.trim();
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Some Details are Pending for eligiblity of APPROVAL" + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        txtName1.value = data[0].orgname;
                        txtRegno.value = data[0].regno;
                        txtEstdate.value = data[0].fdate;
                        txtType.value = data[0].orgtype;
                        txtDocType.value = data[0].doctype;

                        txtDistrict.value = data[0].district;
                        txtDistrictCode.value = data[0].districtcode;
                        txtTehsil.value = data[0].tehsil;
                        //txtStreet.value = data[0].street;
                        txtRoad.value = data[0].road;
                        $('#main-content').show(3000);
                    }

                }
            });
        }
        fillForm();

        $('#txtGenerateId').val(OrgDocId);
        $('#txtCenterCode').val(Code);
        $("#frmuploadncrphoto").submit(function () {
//            alert("1");
//            if ($("#frmuploadncrphoto").valid())
//            {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            //BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfNCRCenterVisit.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmuploadncrphoto").serialize();
//alert(forminput);
            data = forminput; // serializes the form's elements.
//            var file_data7 = $("#uploadImage7").prop("files")[0];
//            var file_data8 = $("#uploadImage8").prop("files")[0];
//            var file_data9 = $("#uploadImage9").prop("files")[0];
//            var file_data6 = $("#uploadImage6").prop("files")[0];// Getting the properties of file from file field
            var form_data = new FormData(this);                  // Creating object of FormData class
//            form_data.append("panno", file_data7)
//            form_data.append("aadharno", file_data8)
//            form_data.append("addproof", file_data9)
//            form_data.append("appform", file_data6)// Appending parameter named file with properties of file_field to form_data
            form_data.append("action", "ADD")
            form_data.append("data", data)
//alert(form_data);
            $.ajax({

                url: url,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data, // Setting the data attribute of ajax with file_data
                type: 'post',
                success: function (data)
                {
                    var data = data.trim();
                    //alert(data);

                    if (data === "Successfully Inserted")
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Center Visit Photos Uploaded Successfully.</span>");
                        window.setTimeout(function () {
                            window.location.href = "frmncrcentervisit.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("frmncrcentervisit");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }



                }
            });
            // }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
</html>
