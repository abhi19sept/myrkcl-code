<?php session_start(); ?>
<!--Top Part Start-->
<div class="top_main_div">
	<div class="top_sub_main_div">
    	<!--contact details Start-->
    	<div class="top_left_right_div">
        
       <div class="contact_detail">
           <?php if(isset($_SESSION['Login'])) { if($_SESSION['Login']===1) { ?>
           <ul>
               <li>
                   <a href='#' title="">Home</a>
               </li>
               <li>
                   <a href='#' title="">Dashboard</a>
               </li>
                <li>
                   <a href='#' title="">My Account</a>
               </li>
                <li>
                   <a href='#' title="">Change Password</a>
               </li>
                <li><a href='#' title="">News & Notifications</a></li>
           </ul>
           <?php }
           } else { ?>
           <ul>
               <li>
                   <a href='#' title=""></a>
               </li>
               <li>
                   <a href='#' title=""></a>
               </li>
                <li>
                   <a href='#' title=""></a>
               </li>
                <li>
                   <a href='#' title=""></a>
               </li>
                <li><a href='#' title=""></a></li>
           </ul>
           <?php } ?>
	</div>
        
        </div>
        <!--contact details Start-->
        <!--social icon Start-->
        <div class="top_left_right_div">
        	<div class="socialBg1">
            	<ul>
                	<li>
                    <a href="#" class="fa fa-facebook" title="Facebook"></a>
                    </li>
                    <li>
                    <a href="#" class="fa fa-twitter" title="Twitter"></a>
                    </li>
                    <li>
                    <a href="#" class="fa fa-linkedin" title="Linkedin"></a>
                    </li>
                    <li>
                    <a href="#" class="fa fa-skype" title="Skype"></a>
                    </li>
                     <li>
                    <a href="#" class="fa fa-google-plus-square" title="Google Plus"></a>
                    </li>
                 </ul>
			</div>
        </div>
        <!--social icon Start-->
    </div>
</div>
<!--Top Part End-->

<!--Header Start-->
<div class="header_main_div">
	<div class="header_sub_main_div">
    	<div class="logo_div">
            <a href="index.php"><img src="rkcltheme/images/logo.jpg" ></a>
        </div>
        <div class="header_right-menu">
        	<ul>
            	
                <li>
                	
                    <span class="fa fa-user"></span>
                    Welcome <?php echo (
                            isset($_SESSION['Login']) ? 
                            ($_SESSION['Login']===1 ? 
                            $_SESSION['User_LoginId'] . 
                            " <a href='#'> Logout</a>" : 
                        'Gust <a href="index.php">Login</a>') : 
                        'Gust <a href="index.php">Login</a>'); ?> 
               </li>
            </ul>
        </div>
    </div>
</div>
<!--Header End-->