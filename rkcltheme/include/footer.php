<div class="footer_main_div">
    <div class="footer_sub_main_div">
        <div class="footer_left_text_div">
            Designed & Developed by <a href="http://www.rkcl.in" target="_blank" class="footer_companey_name_text_div" title="RKCL">
                Rajasthan Knowledge Corporation Limited</a>
        </div>
        <div class="footer_left_text_div">
            <div class="contact_detail_footer">
                <ul>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">FAQ'S</a></li>
                    <li><a href="#">Downloads</a></li>
                   
                </ul>
            </div>
        </div>
    </div>
</div>