<?php
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
ini_set("upload_max_filesize", "855M");
ini_set("post_max_size", "1000M");
ini_set("max_execution_time", "30000000000");
ini_set("max_input_time", "30000000000");
$title = "Upload IT-GK/SP Passbook";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '25') {

    $filename = date("Y-m-d_H-i-s");
    echo "<script>var filea='" . $filename . "'</script>";
    ?>
    <link rel="stylesheet" href="css/datepicker.css"/>
    <script src="scripts/datepicker.js"></script>
    <div style="min-height:430px !important;max-height:1500px !important;">
        <div class="container"> 


            <div class="panel panel-primary" style="margin-top:36px !important;">  
                <div class="panel-heading">Upload IT-GK/SP Passbook</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> --> 
                    <form name="frmupdexamresult" id="frmupdexamresult" class="form-inline" role="form" enctype="multipart/form-data" >    
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>

                        </div>
                        <div class="container" id="coursemode">
                            <div class="col-sm-3 form-group"> 
                                <label for="course">Add Payment Description<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-4 form-group"> 
                          
                                <input type="text" id="txtPayDes" name="txtPayDes" class="form-control">
                            
                            <input type="button" id="btnPayDes" value="Save"/>
                            </div> 
                            <div class="col-sm-3 form-group"> 
                                <label for="course">Already Added Payment Description</label>

                            </div> 
                            <div class="col-sm-3 form-group"> 
                                <select id="ddlPayDes" name="ddlPayDes" class="form-control">  
                                                                  
                                </select>
                               
                            </div> 
                        </div>                      
                        <div class="container" id="coursemode">
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Select User Type<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-6 form-group"> 
                                <select id="ddlUserType" name="ddlUserType" class="form-control">  
                                    <option value="0">Select</option>

                                    <option value="7">IT-GK</option>
                                   <!-- <option value="14">SP</option>                                -->                          
                                </select>
                                <input type="text" id="txtfilename" name="txtfilename" class="form-control" style="display: none;">
                            </div> 
                        </div>

                        <div class="container" id="upddiv"> 

                            <div class="col-sm-4 form-group">  
                                <label>Download Sample File:</label>
                                <a href="upload/sample_file_final_acc_cr.csv"> <input type="button" value="Download File" name="download">  </a>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="ecl">Upload File:<span class="star">*</span></label>
                                <input type="file" class="form-control" id="_file" name="_file" accept=".csv"/>
                                <span style="font-size:10px;">Note : .CSV Allowed Max Size =50MB</span>
                                <span class="btn submit" id="fileuploadbutton"> <input type="button" id="btnUpload" value="Upload File"/></span> 
                                <span class='btn submit'> <input type='button' id='btnReset' name='btnReset' value='Reset' /> </span>
                            </div>


                        </div>
                        <div class="container" >
                            <div class="col-md-10">       
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Validate Uploaded File" style="display: none;" />
                                <input type="button" name="btnTransferResult" id="btnTransferResult" class="btn btn-warning" value="Reflect Changes" style="display: none;"/>
                                <input type="button" name="btnUpdAgain" id="btnUpdAgain" class="btn btn-primary" value="Upload CSV Again" style="display: none;"/>
                            </div>

                        </div> 
                        <div id="grid" style="margin-top:35px;"> </div>                  
                    </form>
                </div>
            </div>
            </form>
        </div>
    </div>
    </body>

    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>


    <script type="text/javascript">
        $('#result_date').datepicker({
            format: "yyyy-mm-dd",
            orientation: "bottom auto",
            todayHighlight: true
        });
    </script>

    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

        $(document).ready(function () {
            //alert("hii");
            var uploadfilepath = "";
            var updeventid = "";
            function FillEvent() {
                //alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfEventMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        //alert(data);
                        $("#ddlExamEvent").html(data);
                        $("#ddlExamEvent1").html(data);
                        $("#ddlExamEvent2").html(data);
                        ddlEID.value = ddlExamEvent.value;
                    }
                });
            }
            //FillEvent();
            $("#ddlUserType").change(function () {
                var eidd = $(this).val();
                var conta = eidd.concat("_");
                var contb = conta.concat(filea);
                $('#txtfilename').val(contb);
            });
            $("#btnUpload").click(function () {

                var url = "common/cfITGKPassbook.php";
                //var forminput = $("#frmupdexamresult").serialize();


                var fd = new FormData();
                fd.append('resultfile', $("#_file").prop("files")[0]);
                fd.append('action', 'updfile');
                fd.append('ddlUserType', ddlUserType.value);
                fd.append('txtfilename', txtfilename.value);
                $.ajax({
                    url: url,
                    data: fd,
                    processData: false,
                    contentType: false,
                    type: 'POST',

                    success: function (data)
                    {

                        //var obj = JSON.parse(data);
                        obj = $.parseJSON(data);
    //alert(obj['resultfile']['status']);
                        if (obj['resultfile']['status'] == "1") {

                            uploadfilepath = obj['resultfile']['filename'];
                            alert("File Uploaded");
                            updeventid = obj['resultfile']['eventname'];
                            $('#btnSubmit').show(2000);
                            $('#btnTransferResult').hide();
                            $('#btnUpdAgain').hide();
                        }

                    }
                });


            });
            function fillDescp() {
               $('#ddlPayDes').html('');
                var url = "common/cfITGKPassbook.php";
                data = "action=fillDescp"; // serializes the form's 
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        $('#response').empty();
                        //alert(data);
                        $('#ddlPayDes').html(data);

                        //showData();
                    }
                });

                return false; // avoid to execute the actual submit of the form.
            }
            fillDescp();
            $("#btnPayDes").click(function () {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfITGKPassbook.php"; // the script where you handle the form input.
                var data;
                data = "action=saveDescp&txtPayDes=" + txtPayDes.value + ""; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        
                        if (data == 'Successfully Done')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span> Description Added</span></p>");
                            fillDescp();


                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Error while adding Description</span></p>");


                        }
                        //showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            $("#btnSubmit").click(function () {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfITGKPassbook.php"; // the script where you handle the form input.
                var data;
                data = "action=uploadexamresult&filepath=" + uploadfilepath + "&usertype=" + ddlUserType.value + ""; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        var obj = JSON.parse(data);
                        //alert(obj['status']);
                        if (obj['status'] == 'success')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + obj['rows'] + " Records Imported from CSV</span></p>");
                            fillgrid();
                            $('#btnTransferResult').show();
                            $('#btnUpdAgain').show();
                            $('#btnSubmit').hide();

                            $('#upddiv').hide();
                            $('#btnTransferResult').prop("disabled", false);
                            $('#btnUpdAgain').prop("disabled", false);

                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + obj['rows'] + "</span></p>");
                            $('#btnTransferResult').hide();


                        }
                        //showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function fillgrid() {
                $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
                setTimeout(function () {
                    $('#grid').load();
                }, 2000);
                var url = "common/cfITGKPassbook.php";
                data = "action=SHOWgrid"; // serializes the form's 
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        $('#response').empty();
                        $("#grid").html(data);
                        $('#example').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]
                        });


                        //showData();
                    }
                });

                return false; // avoid to execute the actual submit of the form.
            }
            $("#btnUpdAgain").click(function () {
                $('#upddiv').show();

                $('#btnTransferResult').prop("disabled", true);

            });
            $("#btnTransferResult").click(function () {
                var returnVal = confirm("Are you sure?");
                if (returnVal) {
                    $('#btnUpdAgain').prop("disabled", true);
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                    var url = "common/cfITGKPassbook.php"; // the script where you handle the form input.
                    var data;
                    data = "action=transferexamresult" + "&uploadfor=" + updeventid + ""; // serializes the form's elements.

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data)
                        {
                            var obj = JSON.parse(data);
                            //alert(obj['rows']);
                            if (obj['status'] == 'success')
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + obj['rows'] + "  Records Inserted</span></p>");
                                $('#btnTransferResult').prop("disabled", true);

                                $('#result_date').show(2000);

                            } else
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + obj['rows'] + "</span></p>");


                                $('#result_date').hide();
                                 $('#btnUpdAgain').show();
                                 $('#btnUpdAgain').prop("disabled", false);
                            }
                            //showData();


                        }
                    });
                } else {

                }


                return false; // avoid to execute the actual submit of the form.
            });


            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });
    </script>

                                        <!--    <script src="scripts/fileuploadfinalresult.js"></script>-->
    </html> 


    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>  