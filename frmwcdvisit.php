<?php
$title = "Set Visit for WCD Scheme";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var ItPeripheralsCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var DeviceCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading"> Set Visit for WCD Scheme

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                     

                    <div class="panel panel-success">
                        <div class="panel-heading"> Set Visit for WCD Scheme

                        </div>
                        <div class="panel-body">
                            <form name="form" id="frmsetvisit" class="form-inline" role="form" enctype="multipart/form-data">
                            <div id="response"></div>
                            <div class="col-sm-5"  id="non-printable"> 
                                <label for="edistrict">Select ITGK Code 
                                    :<span class="star">*</span></label>
                                <select id="ddlVisitCenter" name="ddlVisitCenter" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Visit Center')"
                                onchange="try {
                                            setCustomValidity('')
                                        } catch (e) {
                                        }"> </select>    
                            </div>
                            <div class="col-md-5">     
                                <label for="dateFrom">Visit Date:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="visitDate" id="visitDate" readonly="true" placeholder="DD-MM-YYYY"> 
                            </div>
                            <div class="col-md-2">   
                                <input type="button" name="setVisitBtn" id="setVisitBtn" class="btn btn-primary" value="Set Visit" style="margin-top:25px;" />
                            </div>
                           </form> 
                            
                        </div> 
                    </div>
                <div class="panel panel-success" id="aodetails" style="display:none;">
                        <div class="panel-heading"> ITGK details for Visit

                        </div>
                        <div class="panel-body">
                <div id="itgk-grid" name="itgk-grid" class="form-group" style="margin-top:35px;"> </div>
                        </div>
                </div>
                
                <div class="panel panel-success">
                        <div class="panel-heading"> Scheduled Visit

                        </div>
                        <div class="panel-body">
                <div id="gird" name="gird" class="form-group" style="margin-top:35px;"> </div>
                        </div>
                </div>
            </div>

        </div>   
    </div>

</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<style>
    #errorBox{
        color:#F00;
    }
    .aslink {
        cursor: pointer;
    }
</style>
<script type="text/javascript">
 var today = new Date();
 var startDate1 = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate() ;
    $('#visitDate').datepicker({
		
        format: "yyyy-mm-dd",
		orientation: "bottom auto",
	todayHighlight: true,
	autoclose: true,
	minDate: 0,
	startDate: startDate1,

    });
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        


        function FillCenterCode() {
            $.ajax({
                type: "post",
                url: "common/cfWcdVisit.php",
                data: "action=FILLAO",
                success: function (data) {
                    //alert(data);
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    $("#ddlVisitCenter").html(data);
                }
            });
        }
        FillCenterCode();
        
         $("#ddlVisitCenter").change(function () {
            var selAO = $(this).val();
            if (selAO == '') {
                alert("No Details Found. Please Select ITGK Code to Proceed");
                return false;
            } else {
                getITGKInfo(selAO);
            }
        });
        
        function getITGKInfo(itgkCode) {
        //alert(itgkCode);
        $('#itgk-grid').html('');
        if (itgkCode != '') {
            $.ajax({
                type: "post",
                url: "common/cfWcdVisit.php",
                data: "action=GETAODETAILS&itgk=" + itgkCode,
                success: function (data)
                {
                     $("#itgk-grid").html(data);
                    $('#example1').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $("#aodetails").show();
                }
            });
        }
    }
    
     function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfWcdVisitConfirm.php",
                data: "action=SHOW",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print']
                    });



                }
            });
        }
         showData();

        $("#setVisitBtn").click(function () {
            var AOCode = ddlVisitCenter.value;
            var Date = visitDate.value;
            
            if(AOCode == '' || Date == ''){
                alert("Please Select ALL Fields");
                return false;
            }
            else {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfWcdVisit.php"; // the script where you handle the form input.
                var data;
                data = "action=ADDVISIT&AOCode=" + AOCode + "&Date=" + Date + ""; // serializes the form's elements.
                
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {//alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmwcdvisit.php";
                           }, 1000);

                            Mode="Add";
                            resetForm("frmwcdvisit");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }


                    }
                });
                }
                return false; // avoid to execute the actual submit of the form.
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</html>
