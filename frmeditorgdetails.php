<?php
$title="Edit Organisation Details";
include ('header.php'); 
include ('root_menu.php'); 

    if (isset($_REQUEST['code'])) {
                echo "<script>var OrganizationCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var OrganizationCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:450px !important;max-height:1500px !important">
        <div class="container"> 
			 

            <div class="panel panel-primary" style="margin-top:46px !important;">

                <div class="panel-heading">Edit Organisation Details</div>
                <div class="panel-body">
                    
                    <form name="frmeditOrgDetails" id="frmeditOrgDetails" action="" class="form-inline">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Organization Name:<font color="red">*</font></label>
                               <input type="text" class="form-control" maxlength="500" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center"  >
                            </div>


							
							</div>
							
							

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="UPDATE" style='margin-left:10px;display:none;'/>    
                        </div>
						
						
						
						<div id="gird"></div>
                 </div>
            </div>   
        </div>


    </form>

	
	</div>



</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>


 <script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
						if (Mode == 'Delete')
							{
								if (confirm("Do You Want To Delete This Item ?"))
								{
		                   		 deleteRecord();
		               		}
		           		 }
		           else if (Mode == 'Edit')
		           {
		                fillForm();
		           }
				   
				   
					function deleteRecord()
					{
		               $('#response').empty();
		                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		               $.ajax({
	                    type: "post",
		                    url: "common/cfeditorgdetails.php",
		                   data: "action=DELETE&values=" + OrganizationCode + "",
		                   success: function (data) {
		                       //alert(data);
		                        if (data == SuccessfullyDelete)
		                        {
		                            $('#response').empty();
		                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
		                           window.setTimeout(function () {
		                               window.location.href="frmorgdetail.php";
		                          }, 1000);
		                            
		                            Mode="Add";
		                            resetForm("frmeditOrgDetails");
		                        }
		                        else
		                        {
		                            $('#response').empty();
		                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
		                        }
		                       showData();
		                    }
		               });
		            }
		
		
		            function fillForm()
		            {
		                $.ajax({
		                    type: "post",
		                    url: "common/cfeditorgdetails.php",
		                    data: "action=EDIT&values=" + OrganizationCode + "",
		                    success: function (data) {
		                        //alert($.parseJSON(data)[0]['Status']);
		                        data = $.parseJSON(data);
							   //alert(data);
		                        txtName1.value = data[0].Organization_Name;
		                        $("#gird").hide();
		                    }
		                });
		            }
					
				
		
		            function showData() {
						$('#response').empty();
		                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		                
		                $.ajax({
		                    type: "post",
		                    url: "common/cfeditorgdetails.php",
		                    data: "action=SHOW",
		                    success: function (data) {
								$('#response').empty();
		
		                        $("#gird").html(data);
								
								$('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
					$('#btnSubmit').show();
		
		                    }
		              });
		            }
		
		            showData();
		
				
							
			$("#btnSubmit").click(function () {
			
			if ($("#frmeditOrgDetails").valid())
            {
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfeditorgdetails.php"; // the script where you handle the form input.
				
                var data;
                var forminput=$("#frmeditOrgDetails").serialize();
                if (Mode == 'UPDATE')
                {
                    data = "action=UPDATE&birthproof=" + birthproof + "&" + forminput; // serializes the form's elements.
                }
                else
                {
					
					data = "action=UPDATE&code=" + OrganizationCode +"&" + forminput;
                    //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                }
				//alert(data);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
					
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                             window.setTimeout(function () {
                                window.location.href = "frmeditorgdetails.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmeditorgdetails");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();


                    }
                });
			 }
                return false; // avoid to execute the actual submit of the form.
            });

                            
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmeditorgdetails_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>