<?php
$title = "ICT Report";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";   
} else {
    echo "<script>var UserLoginID=0</script>";    
}

?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">ICT Report</div>
            <div class="panel-body">

                <form name="frmictreport" id="frmictreport" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course"> Course Name:<span class="star">*</span></label>
								  <input type="text" name="txtcoursename" id="txtcoursename" readonly="true" 
										class="form-control">
                             </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                    <!--                                <option value="0">All Batch</option>-->
                                </select>
                            </div> 
						
                        </div>

						<div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" 
										value="View Details"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>

<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

		function FillCourseName() {
            var CourseCode = '4';
            $.ajax({
                type: "post",
                url: "common/cfICTReport.php",
                data: "action=FillCourse&values=" + CourseCode + "",
                success: function (data) {
					txtcoursename.value = data;
                    //document.getElementById('txtcoursename').innerHTML = data;
                }
            });
        }
        FillCourseName();

        function FillBatch() {
            var selCourse = '4';
            $.ajax({
                type: "post",
                url: "common/cfICTReport.php",
                data: "action=FILLBatchName&values=" + selCourse + "",
                success: function (data) {
					//alert(data);
                    $("#ddlBatch").html(data);
                }
            });

        }
	FillBatch();

        function showData() {
			//$("#btnSubmit").hide();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfICTReport.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=GETDATA&course=4&batch=" + ddlBatch.value + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					//alert(data);
					if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please Select Batch." + "</span></div>");
                            
						}
					else {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}

                }
            });
        }
	

        $("#btnSubmit").click(function () {
            showData();
          return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<!-- <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmphotosignprocesscount.js" type="text/javascript"></script> -->
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
