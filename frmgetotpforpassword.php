<?php
$title = "Get OTP For Password";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '28') {
    ?>
    <div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
            <div class="panel panel-primary" style="margin-top:36px !important;">  
                <div class="panel-heading">Get OTP For Password</div>
                <div class="panel-body">
                    <div class="form-inline"> 
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            
                            <form name="frmgetotpforpassword" method="POST" id="frmgetotpforpassword" class="form-inline" role="form" enctype="multipart/form-data"> 
                                <div class="col-sm-4 form-group"> 
                                    <label for="ecl">User/Login ID<span class="star">*</span></label>
                                    <input type="text" class="form-control" id="txtloginid" name="txtloginid" />
                                </div>
                                <div class="col-sm-4 form-group" >   
                                    <label for="ecl"><span class="star"></span></label>
                                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" 
                                           value="Submit" style="margin-top: 24px;"/>
                                </div>                  
                            </form>
                            <div id="responsedetail" style="margin-top: 30px;"></div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
    </body>

    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>	
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

        $(document).ready(function () {
            $("#frmgetotpforpassword").on('submit', (function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append('action', 'GetOtpPassword');
                $.ajax({
                    url: "common/cfgetotpforpassword.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data)
                    {
                       $('#responsedetail').html(data);  
                    },
                    error: function (e)
                    {
                        $("#errmsg").html(e).fadeIn();
                    }
                });
            }));
        });
    </script>
    </html> 
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>	