<?php
$title = "Survey Master ";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_UserRoll'] == '1') {
    ?>



    <div class="container" > 


        <div class="panel panel-primary" style="margin-top:46px !important;" >

            <div class="panel-heading" id="non-printable">Add Question Answers</div>
            <div class="panel-body">


                <!-- <div class="jumbotron"> -->
                <form name="frmquestionanswers" id="frmquestionanswers" action="" class="form-inline">     


                    <div class="container" >

                        <div id="response"></div>	

                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group"> 
                            <label for="designation">Select Survey:<span class="star">*</span></label>
                            <select id="ddlSurvey" name="ddlSurvey" class="form-control">                                  
                            </select>
                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">  
                            <label for="fname">Enter Question:<span class="star">*</span></label>

                            <textarea class="form-control" rows="3" id="txtQuestion" name="txtQuestion" placeholder="Enter Question" ></textarea>

                        </div>
                    </div>






                    <div class="container">

                        <div class="col-sm-4 form-group">     
                            <label for="dob">option1<span class="star">*</span></label>								
                            <textarea class="form-control"  name="txtoption1" id="txtoption1"  placeholder="option1"></textarea>
                        </div>



                        <div class="col-sm-4 form-group">     
                            <label for="dob">option2<span class="star">*</span></label>								
                            <textarea class="form-control"    name="txtoption2" id="txtoption2"  placeholder="option2"></textarea>
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="dob">option3<span class="star">*</span></label>								
                            <textarea class="form-control"    name="txtoption3" id="txtoption3"  placeholder="option3"></textarea>
                        </div>




                    </div>







                    <div class="container" id="non-printable">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                    </div>
                    <div id='gird'></div>





                </form> 

            </div>

        </div>   
    </div>


    </body>	


    <?php include ('footer.php'); ?>				
    <?php include'common/message.php'; ?>


    <style>
        #errorBox{
            color:#F00;
        }
    </style>



    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            } else if (Mode == 'Edit')
            {
                fillForm();
            }

            function fillForm()
            {
                //alert("HII");
                $.ajax({
                    type: "post",
                    url: "common/cfquestionanswers.php",
                    data: "action=EDIT&values=" + Code + "",
                    success: function (data) {
                        //alert(data);
                        //alert($.parseJSON(data)[0]['Status']);
                        data = $.parseJSON(data);

                        ddlSurvey.value = data[0].Servey_Code;
                        txtQuestion.value = data[0].Servey_Question;
                        txtoption1.value = data[0].Servey_option1;
                        txtoption2.value = data[0].Servey_option2;
                        txtoption3.value = data[0].Servey_option3;


                    }
                });
            }





            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfquestionanswers.php",
                    data: "action=DELETE&values=" + Code + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmquestionanswers.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("frmquestionanswers");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }

            function showData() {

                $.ajax({
                    type: "post",
                    url: "common/cfquestionanswers.php",
                    data: "action=SHOWSURVEY",
                    success: function (data)
                    {

                        $('#response').empty();
                        $("#gird").html(data);
                        $('#example').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'print'
                            ],
                            scrollY: 400,
                            scrollCollapse: true,
                            paging: true

                        });

                    }
                });
            }
            showData();




            function FillSurvey() {
                $.ajax({
                    type: "post",
                    url: "common/cfquestionanswers.php",
                    data: "action=FILLSurvey",
                    success: function (data) {
                        $("#ddlSurvey").html(data);
                    }
                });
            }

            FillSurvey();




            $("#btnSubmit").click(function () {
                if ($("#frmquestionanswers").valid())
                {
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    var url = "common/cfquestionanswers.php"; // the script where you handle the form input.
                    var data;
                    var forminput = $("#frmquestionanswers").serialize();
                    if (Mode == 'Add')
                    {
                        data = "action=INSERT&" + forminput; // serializes the form's elements.
                    } else
                    {

                        data = "action=UPDATE&code=" + Code + "&" + forminput;
                        //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                    }
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data)
                        {
                            if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                window.setTimeout(function () {
                                    window.location.href = "frmquestionanswers.php";
                                }, 1000);
                                Mode = "Add";
                                resetForm("frmquestionanswers");
                            } else
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                            }
                            showData();


                        }
                    });
                }
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>


    <style>
        .error {
            color: #D95C5C!important;
        }
    </style>
    <script src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmquestionanswers_validation.js"></script>
    </html>
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>