<?php
$title = "Correction DD Payment Confirm";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_UserRoll'] == '9') {
?>

<div style="min-height:430px !important;max-height:auto !important;"> 
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">DD Payment Confirm</div>
        <div class="panel-body">
            <form name="frmddpaymentconfirm" id="frmddpaymentconfirm" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>


                    <div class="col-sm-4 form-group">     
						<label for="correction">Select Correction Name:</label>
						<select id="ddlCorrection" name="ddlCorrection" class="form-control">
						
						</select>
						<input type="hidden" name="txtapplicationfor" id="txtapplicationfor" class="form-control"/>
					</div> 
                </div>
               
                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Center Code:</label>
                        <select id="ddlcentercode" name="ddlcentercode" class="form-control">

                        </select>											
                    </div> 
                </div>

                <div id="menuList" name="menuList" style="margin-top:15px;"> </div> 

                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                </div>
        </div>
    </div>   
</div>
</form>
</div>

</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function FillCorrectionName() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=FILL",
                    success: function (data) {
						//alert(data);
                        $("#ddlCorrection").html(data);
						 
                    }
                });
            }
            FillCorrectionName();
         
		    $("#ddlCorrection").change(function () {
				GetApplicationName(ddlCorrection.value);				
        });
		
		function GetApplicationName(val) {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=GetApplicationName&for=" + val + "",
                    success: function (data) {
						//alert(data);
						txtapplicationfor.value = data;
						showCenterCode();
                        //$("#txtapplicationfor").html(data);
						 
                    }
                });
            }
			
        function showCenterCode() {
			//alert("hii");
            $.ajax({
                type: "post",
                url: "common/cfCorrectionFee.php",
                data: "action=GETCENTERCODE&code=" + txtapplicationfor.value + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlcentercode").html(data);
                }
            });
        }		

        $("#ddlcentercode").change(function () {
            showDdModeData(ddlcentercode.value);
        });


        function showDdModeData(val) {
            $.ajax({
                type: "post",
                url: "common/cfCorrectionFee.php",
                data: "action=GETCorrectionData&refno=" + val + "",
                success: function (data) {
                    //alert(data);
                    $("#menuList").html(data);
					$("#btnSubmit").show();
                }
            });
        }

     

        $("#btnSubmit").click(function () {
		$("#btnSubmit").hide();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfCorrectionFee.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmddpaymentconfirm").serialize();

            data = "action=UPDATEDDSTATUS&" + forminput; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmddcorrectionconfirm.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmddpaymentconfirm");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?> 
