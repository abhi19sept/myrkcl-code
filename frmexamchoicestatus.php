<?php
$title = "Exam choice Status";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
// echo $_SESSION['User_UserRoll'];
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '14' ) {	
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">ITGK Exam Choice Status

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
                <form name="frmexamchoicestatus" id="frmexamchoicestatus" class="form-inline" action=""> 

                    <br>

                    <div id="gird"></div>
                </form>

            </div>

        </div>   
    </div>

</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
            var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                    $(document).ready(function () {


                        function showData() {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                            $.ajax({
                                type: "post",                 url: "common/cfexamchoicestatus.php",
                                data: "action=SHOW",
                                success: function (data) {
                                    $('#response').empty();
                                    $("#gird").html(data);
                                    $('#example').DataTable({
                                        dom: 'Bfrtip',
                                        buttons: [
                                        'copy', 'csv', 'excel', 'pdf', 'print'                         ]
                                    });



                                }
                            });
                        }

                        showData();

                        function resetForm(formid) {
                            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                        }

    });
                    
</script>
</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>
