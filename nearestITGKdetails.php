<?php
header('Access-Control-Allow-Origin: *');  
/**
 * Description: This is file is gives the detail of nearestITGK detail of any district.
 * All user details are read from HTTP GET Request
 */
// Database Connection
require 'BAL/clsCenterlocation.php';
$emp = new clsOrgUpdate();
// array for JSON response
$response = array();
$res = array();

//

if (isset($_POST['District_Code']) && $_POST['District_Code']!='' 
        && isset($_POST['Tehsil_Code']) && $_POST['Tehsil_Code']!='' 
        && isset($_POST['areatype']) && $_POST['areatype']!='' 
        && ( 
                ( isset($_POST['Municipality_Raj_Code']) && $_POST['Municipality_Raj_Code']!='' )
                || ( isset($_POST['Block_Code']) && $_POST['Block_Code']!='' && isset($_POST['GP_Code']) && $_POST['GP_Code']!='' ) 
                )
        ) 
    {
    
     /** This part return in result the listing of detail of ITGK  according to the state, 
      * District code, Tehsil Code, area type  passes on the function */ 
            
        
        $res["ddlState"]=29;
        $res["ddlDistrict"]=$_POST['District_Code'];
        $res["ddlTehsil"]=$_POST['Tehsil_Code'];
        
        if($_POST['areatype']==1){
            $res["area"]="Urban";
            $res["ddlMunicipalName"]=$_POST['Municipality_Raj_Code'];
        }else{
            $res["area"]="Rural";
            $res["ddlPanchayat"]=$_POST['Block_Code'];
            $res["ddlGramPanchayat"]=$_POST['GP_Code'];
        }
        //echo $res["area"];die;
        
        
        
        $response = $emp->LearnercenterlistWebAPI($res);
     $_DataTable = array();
     if ($response[0] == 'Success') 
            {//echo 
                    
                            $_i = 0;
               while ($_Row = mysqli_fetch_array($response[2],true)) {
                   //print_r($_Row);
                         $_DataTable[$_i] = $_Row;
                                $_i = $_i + 1;
                }
                //print_r($_DataTable);
                echo json_encode($_DataTable);
            } 
        else 
            {
            echo json_encode($_DataTable);
//                echo json_encode(
//                        array("message" => "No Detail found.")
//                );
            }
    
    }
elseif (isset($_POST['District_Code']) && $_POST['District_Code']!='' 
        && isset($_POST['Tehsil_Code']) && $_POST['Tehsil_Code']!='' 
        && isset($_POST['areatype']) && $_POST['areatype']!='' 
        && isset($_POST['Block_Code']) && $_POST['Block_Code']!='' 
        )
    {
    
     /** This part return in result the listing of GramPanchayat according to the District code, Tehsil Code, area type and Block_Code passes on the function */ 
            
        
        $res["District_Code"]=$_POST['District_Code'];
        $res["Tehsil_Code"]=$_POST['Tehsil_Code'];
        $res["areatype"]=$_POST['areatype'];
        $res["Block_Code"]=$_POST['Block_Code'];
        
        $response = $emp->GetAllGramPanchayat($_POST['Block_Code']);
        $res["GramPanchayat"]=array();

        if ($response[0] == 'Success') 
            {
                while ($_Row = mysqli_fetch_array($response[2])) {
                    $product_item = array(
                        "GP_Code" => $_Row['GP_Code'],
                        "GP_Name" => $_Row['GP_Name']
                    );
                    array_push($res["GramPanchayat"], $product_item);
                }

                echo json_encode($res);
            } 
        else 
            {
                echo json_encode(
                        array("message" => "No Tehsil Found.")
                );
            }
    
    
    
    
    
    
    }
elseif (isset($_POST['District_Code']) && $_POST['District_Code']!='' 
        && isset($_POST['Tehsil_Code']) && $_POST['Tehsil_Code']!='' 
        && isset($_POST['areatype']) && $_POST['areatype']!='' 
        ) 
    {
    
        $res["District_Code"]=$_POST['District_Code'];
        $res["Tehsil_Code"]=$_POST['Tehsil_Code'];
        $res["areatype"]=$_POST['areatype'];
        if($_POST['areatype']==1)
        {
            /** This part return in result the listing of MunicipalName according to the District code, Tehsil Code and area type passes on the function */ 
            
                $response = $emp->FillMunicipalName($res["District_Code"]);
                $res["MunicipalName"]=array();

                if ($response[0] == 'Success') 
                {
                   while ($_Row = mysqli_fetch_array($response[2],true)) {
                            $product_item = array(
                            "Municipality_Raj_Code" => $_Row['Municipality_Raj_Code'],
                            "Municipality_Name" => $_Row['Municipality_Name']
                        );
                        array_push($res["MunicipalName"], $product_item);
                    }

                    echo json_encode($res);
                } 
                else 
                {
                echo json_encode(
                        array("message" => "No Municipal Name Found.")
                );
            }
          
        }        
        else{
            
            /** This part return in result the listing of PanchayatSamiti according to the District code, Tehsil Code and area type passes on the function */ 
            
                //$response = $emp->FillMunicipalName($res["District_Code"]);
                $response = $emp->GetAllPanchayat($res["District_Code"]);
                $res["PanchayatSamiti"]=array();

                if ($response[0] == 'Success') 
                {
                   while ($_Row = mysqli_fetch_array($response[2],true)) {
                            $product_item = array(
                            "Block_Code" => $_Row['Block_Code'],
                            "Block_Name" => $_Row['Block_Name']
                        );
                        array_push($res["PanchayatSamiti"], $product_item);
                    }
                    echo json_encode($res);
                } 
                else 
                {
                echo json_encode(
                        array("message" => "No Panchayat Samiti Name Found.")
                );
            }
            
            
        }
     
    
    }
elseif (isset($_POST['District_Code']) && $_POST['District_Code']!='' 
        && isset($_POST['Tehsil_Code']) && $_POST['Tehsil_Code']!='' 
        ) 
    {
    /** This part return in result the listing of area type according to the District code and Tehsil Code passes on the function */ 
    
        $res["District_Code"]=$_POST['District_Code'];
        $res["Tehsil_Code"]=$_POST['Tehsil_Code'];
        $res["areatype"]=array();
        $product_item = array(
                        "1" => "Urban",
                        "2" => "Rural"
                    );
        array_push($res["areatype"], $product_item);
        echo json_encode($res);
     
    
    }
elseif (isset($_POST['District_Code']) && $_POST['District_Code']!='' ) 
    {
    
     /** This part return in result the listing of Tehsil according to the District code passes on the function */ 
        
        $_actionvalue =$_POST['District_Code']; // rajasthan
        $response = $emp->FillTehsils($_actionvalue);
        //print_r($response);
        
        $res["District_Code"]=$_POST['District_Code'];
        $res["Tehsil"]=array();

        if ($response[0] == 'Success') 
            {
                while ($_Row = mysqli_fetch_array($response[2])) {
                    $product_item = array(
                        "Tehsil_Code" => $_Row['Tehsil_Code'],
                        "Tehsil_Name" => $_Row['Tehsil_Name']
                    );
                    array_push($res["Tehsil"], $product_item);
                }

                echo json_encode($res);
            } 
        else 
            {
                echo json_encode(
                        array("message" => "No Tehsil Found.")
                );
            }
    
    
    
    
    
    
    }
    
elseif (isset($_POST['ClientName']) && $_POST['ClientName']!='' 
        && isset($_POST['APIKey']) && $_POST['APIKey']!='' 
        && isset($_POST['ClientIP']) && $_POST['ClientIP']!='' 
        ) 

    {
    
    
    $RkclApiPermissions = $emp->getRkclApiPermissions($_POST['ClientName'], $_POST['APIKey'], $_POST['ClientIP']);
    
    if($RkclApiPermissions[0]=='Success')
        {
        /** This part return in result the listing of district of rajasthan. Here we are using default rajasthan state*/  
          $_actionvalue =29; // rajasthan
          $response = $emp->FillDistricts($_actionvalue);
          //print_r($response);

          $res["District"]=array();

          if ($response[0] == 'Success') 
              {
                  while ($_Row = mysqli_fetch_array($response[2])) {
                      $product_item = array(
                          "District_Code" => $_Row['District_Code'],
                          "District_Name" => $_Row['District_Name']
                      );
                      array_push($res["District"], $product_item);
                  }
  //echo "<pre>";print_r($res);
                  echo json_encode($res);
              } 
          else 
              {
                echo json_encode(
                        array("message" => "No District Name Found.")
                );
            }
    
        }
        else{
            echo json_encode(
                        array("message" => "Credentials You have provided, Does not matched.")
                );
        }
    
    }
    
?>