<?php
$title = "Close Visit";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var ItPeripheralsCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var DeviceCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}

echo "<script>var UserRoll='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading"> Select Purpose of Visit

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
                <form name="frmselectvisit" id="frmselectvisit" class="form-inline" action=""> 

                    <div class="col-md-2 form-group">     
                        <label for="visit"> Select Visit Purpose:<span class="star">*</span></label>
                    </div>
                    <div class="col-md-6 form-group"> 
                        <select id="ddlVisitType" name="ddlVisitType" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Visit Type')"
                                onchange="try {
                                            setCustomValidity('')
                                        } catch (e) {
                                        }"> </select>
                        <!--</div>-->
                    </div>

                    <div class="container">
                        <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Proceed"/> </a>
                    </div>
                </form>

            </div>

        </div>   
    </div>

</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillVisitType() {
            $.ajax({
                type: "post",
                url: "common/cfVisitPurposeMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlVisitType").html(data);
                }
            });
        }
        FillVisitType();

        $("#btnSubmit").click(function () {
            //alert(UserRoll);
            var selVisitType = ddlVisitType.value;
            if (selVisitType == 1) {
                if(UserRoll == '4' || UserRoll == '23' || UserRoll == '14'){
                window.setTimeout(function () {
                    window.location.href = "frmprocessvisitclose.php?visitcode=" + ddlVisitType.value;
                });
            } else if(UserRoll == '7'){
                alert("You are not authorized to Close Visit");
            }
            } else if(selVisitType == 2){
                 alert("NCR Visit Close will be available soon.");
                 return false; 
//                if(UserRoll == '4' || UserRoll == '23' || UserRoll == '14'){
//                 window.setTimeout(function () {
//                    window.location.href = "frmprocessvisitclose.php?visitcode=" + ddlVisitType.value;
//                });
//                } else if(UserRoll == '7' || UserRoll == '15'){
//                 alert("You are not authorized to Close Visit");
//            }
            } else {
                alert("Not Defined");
            }
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</html>
