<?php
$title = "Message Notification";
include ('header.php');
include ('root_menu.php');
include ('common/message.php');
?>
<div style="min-height:350px !important;max-height:1500px !important">	
    <div class="container" style=" margin-top:50px">
        <div class="row">
            <div class="col-md-12" >
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-bookmark"></span> Message Notification </h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12 bhoechie-tab-container">
                            <div class="col-md-10 bhoechie-tab" style="width: 100%;">
                                <div class="bhoechie-tab-content active">
                                    <input id="myInput" type="text" placeholder="Search.." >
                                    <center id='result'>
                                        
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModalfile" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h6>Message Attached Document</h6>
        </div>
        <div class="modal-body" style="text-align: center;">
            <div id="viewmessage"></div>
            <iframe id="viewphoto" src="" style="width:100%;height:450px"></iframe>
        </div>
    </div>
</div>
<!-- End Update Process Guide Popup -->
<script>
// Message Varibale...
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    var Success = "<?php echo Message::SuccessfullyFetch; ?>";
   
//View Data.....Show
    $(document).ready(function () {
         $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".containermessage").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
        showdata();
    function showdata() {
        $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
        setTimeout(function () {
            $('#grid').load();
        }, 2000);
        var url = "common/cfpushsmsnotification.php"; // the script where you handle the form input.
        var data;
        data = "action=SHOWMESSAGENOT"; // serializes the form's elements.				 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            { 
                if(data === "nomessage"){
                    $("#myInput").css("display","none");
                     $("#result").html("No message available.");
                }else{
                    $("#myInput").css("display","block");
                    $("#result").html(data);
                } 
            }
        });
    }
    
    $("#result").on("click", ".fun_view_file", function () {
        var mybtn = $(this).attr("id");
        var filename = $(this).attr("name");
        $("#viewphoto").css("display","block");
        $("#viewmessage").css("display","none");
        $("#viewphoto").attr("src","upload/message/"+filename);
        var modal = document.getElementById('myModalfile');
        var btn = document.getElementById(mybtn);
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        span.onclick = function () {
            modal.style.display = "none";
        }
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
        var url = "common/cfpushsmsnotification.php"; // the script where you handle the form input.
        var data;
        data = "action=READSTATUS&messageid=" + mybtn; // serializes the form's elements.				 
        $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   //alert(data);
                }
            });
    });
    $("#result").on("click", ".read_status_change", function () {
        var mybtn = $(this).attr("id");
        var filename = $(this).attr("name");
        $("#viewmessage").css("display","block");
        $("#viewphoto").css("display","none");
        $("#viewmessage").html(filename);
        var modal = document.getElementById('myModalfile');
        var btn = document.getElementById(mybtn);
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        span.onclick = function () {
            modal.style.display = "none";
        }
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
        var url = "common/cfpushsmsnotification.php"; // the script where you handle the form input.
        var data;
        data = "action=READSTATUS&messageid=" + mybtn; // serializes the form's elements.				 
        $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   //alert(data);
                }
            });
    });
    
    });
</script>
<style>
    .modal-content{
            width: 80%;
    margin: 0 0 0 145px;
    }
    .btnview{
        text-align: right;
    margin: 0 0 10px 0;
    }
    #result{
        text-align: left;
    }
.containermessage {
  border: 2px solid #dedede;
  background-color: #f1f1f1;
  border-radius: 5px;
  padding: 10px;
  margin: 10px 0;
}

.darker {
  border-color: #ccc;
  background-color: #ddd;
}

.containermessage::after {
  content: "";
  clear: both;
  display: table;
}

.containermessage img {
  float: left;
  max-width: 60px;
  width: 100%;
  margin-right: 20px;
  border-radius: 50%;
}

.containermessage img.right {
  float: right;
  margin-left: 20px;
  margin-right:0;
}

.time-right {
  float: right;
  color: black;
}

.time-left {
  float: left;
  color: black;
}
</style>
</body>
<?php include ('footer.php'); ?>
