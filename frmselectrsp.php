<?php
$title = "RSP Selection";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">RSP Selection</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Select RSP Name:<span class="star">*</span></label>
                            <select id="ddlRsp" name="ddlRsp" class="form-control" >

                            </select>    
                        </div>

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                        </div>
                    </div>


                    <div id="main-content" style="display:none;">
                        <div class="container">
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Organization/Center:</label>
                                <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Mobile No:</label>
                                <input type="text" class="form-control" readonly="true" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:</label>
                                <input type="text" class="form-control" readonly="true" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:</label>
                                <input type="text" class="form-control" readonly="true" name="txtType" id="txtType" placeholder="Type Of Organization">  
                            </div>
                        </div>

                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Tehsil:</label>
                                <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="address">Street:</label>
                                <input type="text" class="form-control" readonly="true" name="txtStreet" id="txtStreet" placeholder="Street">    
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="address">Road:</label>
                                <input type="text" class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road">

                            </div>
                        </div>
                    </div>
                    <div class="container">

                        <input type="button" name="btnshowotp" id="btnshowotp" class="btn btn-primary" value="Generate OTP" style="display:none;"/>
                    </div>
                    <div id="otpverify" style="display:none;">
                        <div class="container">
                            <fieldset style="border: 1px groove #ddd !important;" class="col-sm-11" id="verification">
                                <br>

                                <div class="col-sm-4 form-group"> 
                                    <label for="email">Enter Email:</label>
                                    <input type="text" class="form-control" maxlength="50" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="Mobile">Enter Mobile Number:</label>
                                    <input type="text" class="form-control" maxlength="10" name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <label for="otp">Enter OTP:</label>
                                    <input type="text" class="form-control" maxlength="6" name="txtOtp" id="txtOtp"  placeholder="Enter OTP">
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <input type="button" name="btnVerify" id="btnVerify" class="btn btn-primary" value="Verify" style="margin-top:25px"/>
                                </div>
                                <br>
                                <br>
                            </fieldset>
                        </div>
                    </div>
                    <br>
                    <div class="container" id="tnc" style="display:none">  
                        <div class="col-md-12"> 							
                            <label for="tnc">Terms and conditions :<span class="star">*</span></label></br>
                             <label for="tnc">1) I have read all the terms and conditions mentioned in the attached document and agree to abide by the same. :<span class="star">*</span></label></br> 
                              <label for="tnc">2) I agree to sign an agreement as per new terms and conditions with the Service Provider selected by me within next 15 days.<span class="star">*</span></label></br> 
                               <label for="tnc">3) I am selecting my Service Provider under full consciousness and I am not influenced by any kind of pressure from anyone.<span class="star">*</span></label></br> 
                            <label class="checkbox-inline"> <input type="checkbox" name="chk" id="chk" value="" required>
                                <a title="" style="text-decoration:none;" href="images/EOIhighlightsforITGK.pdf" target="_blank"><span id="fix">I Accept all Terms and Conditions(Click here for detailed Document)</span> </a>
                                
                            </label>								
                        </div>
                    </div>	

                    <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button class="close" type="button" data-dismiss="modal">×</button>
                                    <h3 class="modal-title">Terms and Conditions</h3>
                                </div>
                                <div class="modal-body">
                                    <img src="images/EOIhighlightsforITGK.pdf" />
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>
                    </div>
            </div>
        </div>
    </div>   
</div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function FillRsp() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfSelectRsp.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlRsp").html(data);
                }
            });
        }
        FillRsp();

        $("#ddlRsp").change(function () {
            $("#btnShow").show();
            $("#btnSubmit").hide();
            $("#btnshowotp").hide();
            $("#verification").hide();
            $("#tnc").hide();
            
        });

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }

        $("#btnShow").click(function () {
            //alert("OTP has been sent to your Registered Mobile Number. Please Enter OTP to Proceed");
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfSelectRsp.php",
                data: "action=DETAILS&values=" + ddlRsp.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select a RSP for Selection" + "</span></p>");
                    }
                    else {
                        data = $.parseJSON(data);
                        txtName1.value = data[0].orgname;
                        txtRegno.value = data[0].regno;
                        txtEstdate.value = data[0].fdate;
                        txtType.value = data[0].orgtype;

                        txtDistrict.value = data[0].district;
                        txtTehsil.value = data[0].tehsil;
                        txtStreet.value = data[0].street;
                        txtRoad.value = data[0].road;

                        $("#main-content").show();
                        $("#btnShow").hide();
                        $("#btnshowotp").show();

                    }

                }
            });
        });


        $("#btnshowotp").click(function () {
            alert("OTP has been sent to your Registered Mobile Number. Please Enter OTP to Proceed");
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfSelectRsp.php",
                data: "action=SENDOTP&values=" + ddlRsp.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate) {

                        $("#btnshowotp").hide();
                        $("#otpverify").show();
                        $("#verification").show();
                        
                    }
                    else {

                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   OTP already Generated, Please Try again." + "</span></p>");

                    }

                }
            });
        });

        $("#btnVerify").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfSelectRsp.php",
                data: "action=VERIFY&email=" + txtEmail.value + "&mobile=" + txtMobile.value + "&opt=" + txtOtp.value + "",
                success: function (data)
                {
                    //alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $("#btnVerify").hide();
                        $('#btnSubmit').show(3000);
                        $('#tnc').show(3000);
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                    }
                }
            });
        });


        $("#btnSubmit").click(function () {
            if ($("#form").valid())
            {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfSelectRsp.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#form").serialize();
            if (Mode == 'Add')
            {
                data = "action=ADD&" + forminput; //serializes the form's elements.
                //alert(data);
            }
            else
            {
                data = "action=UPDATE&code=" + StatusCode + "&name=" + txtStatusName.value + "&description=" + txtStatusDescription.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    //alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmselectrsp.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmbankaccount_validation.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>
 <?php 
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?> 
