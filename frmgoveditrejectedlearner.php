<?php
$title="Edit Government Entry Form";
include ('header.php'); 
include ('root_menu.php');

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
				
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
$lcode=$_REQUEST['code']; 
$msg = "";
$flag = 0;
//echo empty($_POST);
if(isset($_POST) && !empty($_POST)) {
	if (isset($_POST["action"]) && !empty($_POST["txtLCode"])) {
	
	$_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_payment/';
	$_UploadDirectory2 = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_birth/';
	
				$_LearnerCode = $_POST["txtLCode"];
				$_EmpName = $_POST["txtEmpName"];
				$_FatherName = $_POST["txtFaName"];
				$_EmpDistrict = $_POST["ddlempdistrict"];
				$_EmpMobile = $_POST["txtEmpMobile"];
				$_EmpEmail = $_POST["txtEmail"];
				$_EmpAddress = $_POST["txtEmpAddress"];
				$_EmpDeptName = $_POST["ddlDeptName"];
				$_EmpId = $_POST["txtEmpId"];
				$_EmpGPF = $_POST["txtGpfNo"];
				$_EmpDesignation = $_POST["ddlEmpDesignation"];
				$_EmpMarks = $_POST["txtEmpMarks"];
				$_EmpExamAttempt = $_POST["ddlExamattepmt"];
				$_EmpPan = $_POST["txtPanNo"];
				$_EmpBankAccount = $_POST["txtBankAccount"];
				$_EmpBankDistrict = $_POST["ddlBankDistrict"];
				$_EmpBankName = $_POST["ddlBankName"];
				$_EmpBranchName = $_POST["txtBranchName"];
				$_EmpIFSC = $_POST["txtIfscNo"];
				$_EmpMICR = $_POST["txtMicrNo"];
				//$_fileReceipt = $_POST["fileReceipt"];
				//$_fileBirth = $_POST["fileBirth"];
				$_EmpDobProof = $_POST["ddlDobProof"];
				$_GeneratedId = $_POST['txtGenerateId'];
	
	if($_FILES["p1"]["name"]!='') {					     
				$imag =  $_FILES["p1"]["name"];
				$newpicture = $_LearnerCode .'_payment'.'.jpg';
				$_FILES["p1"]["name"] = $newpicture;
	  	}
	else  {					   
			$newpicture = $_POST['txtpaymentreceipt'];
		}

	$flag=1;	
	if($_FILES["p1"]["name"]!='') {
		$imageinfo = pathinfo($_FILES["p1"]["name"]);		
		if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg") {
			$msg = "Payment Receipt must be in either PNG or JPG Format";
			$flag=10;
		 }
		else {	
			if (file_exists("$_UploadDirectory1/" .$_LearnerCode .'_payment'.'.jpg'))			 
				{ 
					unlink("$_UploadDirectory1/".$newpicture); 
				}
			if (file_exists($_UploadDirectory1)) {
				if (is_writable($_UploadDirectory1)) {
				  move_uploaded_file($_FILES["p1"]["tmp_name"], "$_UploadDirectory1/".$newpicture);							 
			 }
		  }
		}
	 }	
	 
	 
	 if($_FILES["p2"]["name"]!='') {					     
				$imag =  $_FILES["p2"]["name"];
				$newsign = $_LearnerCode .'_birth'.'.jpg';
				$_FILES["p2"]["name"] = $newsign;
	  	}
	else  {					   
			$newsign = $_POST['txtbirthproof'];
		}

	$flag=1;
	if($_FILES["p2"]["name"]!='') {
		$imageinfo = pathinfo($_FILES["p2"]["name"]);		
		if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg") {
			$msg = "Birth Proof must be in either PNG or JPG Format";
			$flag=10;
		 }
		else {	
			if (file_exists("$_UploadDirectory2/" .$_LearnerCode .'_birth'.'.jpg'))	{ 
					unlink("$_UploadDirectory2/".$newsign); 
				}
			if (file_exists($_UploadDirectory2)) {
				if (is_writable($_UploadDirectory2)) {
				  move_uploaded_file($_FILES["p2"]["tmp_name"], "$_UploadDirectory2/".$newsign);							 
			 }
		  }
		}
	 }	
	 
	 if($flag!=10) {

//include 'common/commonFunction.php';		    
require 'DAL/classconnection.php';
$_ObjConnection = new _Connection();
$_ObjConnection->Connect();

	$_ITGK_Code =   $_SESSION['User_LoginId'];
	$_User_Code =   $_SESSION['User_Code'];			
			
    $_UpdateQuery = "Update tbl_govempentryform set fname='" . $_EmpName . "', faname='" . $_FatherName . "', ldistrictname='" . $_EmpDistrict . "', lmobileno='" . $_EmpMobile . "',"
					. "lemailid='" . $_EmpEmail . "', officeaddress='" . $_EmpAddress . "', gdname='" . $_EmpDeptName . "', empid='" . $_EmpId . "',empgpfno='" . $_EmpGPF . "',"
                    . "designation='" . $_EmpDesignation . "', exammarks='" . $_EmpMarks . "', aattempt='" . $_EmpExamAttempt . "', panno='" . $_EmpPan . "', "
					. "empaccountno='" . $_EmpBankAccount . "', bankdistrict='" . $_EmpBankDistrict . "', gbankname='" . $_EmpBankName . "', gbranchname='" . $_EmpBranchName . "',"
					. "gifsccode='" . $_EmpIFSC . "', gmicrcode='" . $_EmpMICR . "', dobtype='" . $_EmpDobProof . "',"
					. "attach1='" . $newpicture . "', attach2='" . $newsign . "', trnpending='3'"
                    . " Where learnercode='" . $_LearnerCode . "' AND Govemp_ITGK_Code='" . $_ITGK_Code . "'";
    $_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
	$flag = 3;	
	$msg = "Successfully Updated";				
	}		
  }
}
 else {
		include'common/message.php';
	}
 ?>

<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			 
            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Modify Reimbursement Application  Form</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" method="post" action="" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
							<?php if($flag != 0) {?>
                                <div id="response"><?php
        								echo $msg;
										echo "<script>setTimeout(\"location.href = 'frmgovrejectedlearner.php';\",3000);</script>";
										?>
								</div>
							 <?php } ?>
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">LearnerCode:<span class="star">*</span></label>
                                <input type="text" class="form-control" readonly="true" maxlength="18" onkeypress="javascript:return allownumbers(event);" name="txtLCode" id="txtLCode" placeholder="LearnerCode">
								<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
								<input type="hidden" class="form-control" name="txtpaymentreceipt" id="txtpaymentreceipt"/>
								<input type="hidden" class="form-control"  name="txtbirthproof" id="txtbirthproof"/>
								<input type="hidden" class="form-control"  name="action" id="action" value="UPDATE"/>
								<input type="hidden" class="form-control"  name="LearnerCode" id="LearnerCode" value="<?php echo $lcode; ?>"/>
                            </div>					

							<div class="col-sm-4 form-group"> 
                                <label for="ename">Employee Name:<span class="star">*</span></label>
                                <input type="text" class="form-control text-uppercase" maxlength="50" name="txtEmpName" id="txtEmpName" placeholder="Employee Name">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Employee Father Name:<span class="star">*</span></label>
                                <input type="text" class="form-control text-uppercase" maxlength="50" name="txtFaName" id="txtFaName"  placeholder="Employee Father Name">
                            </div>
							
							 <div class="col-sm-4 form-group"> 
                                <label for="email">Employee EmailId:<span class="star">*</span></label>
                                <input type="email" class="form-control" name="txtEmail" id="txtEmail" placeholder="Emp EmailId">    
                            </div>
						</div>
						
						<div class="container">                              
							<div class="col-sm-4 form-group">     
                                <label for="emobile">Employee Mobileno:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="10" name="txtEmpMobile" id="txtEmpMobile" placeholder="Emp Mobileno" >
                            </div>
                       
							<div class="col-sm-4 form-group">     
                                <label for="address">Employee Office Address:<span class="star">*</span></label>
                                <textarea class="form-control" rows="3" id="txtEmpAddress" onkeypress="javascript:return validAddress(event);" name="txtEmpAddress" placeholder="Emp Office Address"></textarea>
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="gpfno">Employee GPF No:<span class="star">*</span></label>
                                <input type="text"  class="form-control" name="txtGpfNo" onkeypress="javascript:return allownumbers(event);" id="txtGpfNo" placeholder="Emp GPF No">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Employee District:<span class="star">*</span></label>
                                <select id="ddlempdistrict" name="ddlempdistrict" class="form-control" >								  
                                </select>    
                            </div>							
						</div>    

                        <div class="container">
                            <div class="col-sm-6 form-group"> 
                                <label for="deptname">Employee Department Name:<span class="star">*</span></label>
                                <select id="ddlDeptName" name="ddlDeptName" class="form-control">
								
                                </select>
                            </div>
                        
                            <div class="col-sm-4 form-group"> 
                                <label for="empid">Employee Id:<span class="star">*</span></label>
                                <input type="text" class="form-control"  name="txtEmpId" onkeypress="javascript:return allownumbers(event);"  id="txtEmpId" placeholder="Employee Id">
                            </div>                            

                            <div class="col-sm-4 form-group"> 
                                <label for="designation">Employee Designation:<span class="star">*</span></label>
                                <select id="ddlEmpDesignation" name="ddlEmpDesignation" class="form-control">
                                          
                                </select>
                            </div>     

                            <div class="col-sm-4 form-group"> 
                                <label for="marks">RS-CIT Exam Marks:<span class="star">*</span></label>
                                <input type="text" class="form-control" id="txtEmpMarks" name="txtEmpMarks" onkeypress="javascript:return allownumbers(event);"
									placeholder="RS-CIT Exam Marks">
                            </div>
						</div>
						
                        <div class="container">							
							<div class="col-sm-4 form-group"> 
                                <label for="attempt">Select RS-CIT Exam Attempt:<span class="star">*</span></label>
                                <select id="ddlExamattepmt" name="ddlExamattepmt" class="form-control">
                                    <option value="" selected="selected">Please Select</option>   
                                    <option value="1"> 1</option>>
                                    <option value="2"> 2</option>>
                                </select>
                            </div>
                        
     						<div class="col-sm-4 form-group"> 
                                <label for="pan">PAN No:<span class="star">*</span></label>
                                <input type="text" class="form-control text-uppercase" onkeypress="javascript:return validAddress(event);"  name="txtPanNo" id="txtPanNo"  placeholder="PAN No">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="bankaccount">Employee Bank Accountno:<span class="star">*</span></label>
                                <input type="text" class="form-control"  name="txtBankAccount" onkeypress="javascript:return allownumbers(event);"  id="txtBankAccount" placeholder="Emp Bank Accountno">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict">Employee Bank District:<span class="star">*</span></label>
                                <select id="ddlBankDistrict" name="ddlBankDistrict" class="form-control">
                                </select>
                            </div>
						</div>

                        <div class="container">   
							<div class="col-sm-4 form-group"> 
                                <label for="bankname">Employee Bank Name:<span class="star">*</span></label>
                                <select id="ddlBankName" name="ddlBankName" class="form-control">
                                    <option value="" selected="selected">Please Select</option>                
                                </select>                                                              
                            </div>  
                              
							<div class="col-sm-4 form-group"> 
                                <label for="branchname">Branch_Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" onkeypress="javascript:return allowchar(event);"  name="txtBranchName" id="txtBranchName"  placeholder="EMP Branch_Name">
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="ifsc">IFSC Code:<span class="star">*</span></label>
                                <input type="text" class="form-control text-uppercase" onkeypress="javascript:return validAddress(event);"  name="txtIfscNo"  id="txtIfscNo" placeholder="EMP Bank IFSC Code">
                            </div> 
                            
                              <div class="col-sm-4 form-group"> 
                                <label for="micr">MICR Code:<span class="star">*</span></label>
                                <input type="text" class="form-control" onkeypress="javascript:return allownumbers(event);"   name="txtMicrNo" id="txtMicrNo" placeholder="EMP Bank MICR Code">
                            </div>
						</div>
                        
                        <div class="container">											
							<div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Pre Payment Receipt:<span class="star">*</span></label>
								<img id="uploadPreview1" src="images/user icon big.png" id="uploadPreview1" name="filePhoto" width="150px" height="135px" onclick="javascript:document.getElementById('uploadImage1').click();">								  
                                <input type="file" class="form-control"  name="p1" id="uploadImage1" onchange="checkPhoto(this);PreviewImage(1)"/>
                            </div>
      
                            <div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Upload Birth Proof:<span class="star">*</span></label>
								<img id="uploadPreview2" src="images/sign.jpg" id="uploadPreview2" name="filePhoto" width="150px" height="135px" onclick="javascript:document.getElementById('uploadImage2').click();">							  
                                <input type="file" class="form-control" name="p2" id="uploadImage2" onchange="checkSign(this);PreviewImage(2)" />
                            </div> 
      
							<div class="col-sm-4 form-group"> 
                                <label for="dobprrof">Date of Birth Proof:<span class="star">*</span></label>
                                <select id="ddlDobProof" name="ddlDobProof" class="form-control">
                                                 
                                </select>
                            </div> 
						</div>

						<div class="container">  
							<div class="col-md-4 form-group"> 							
								<label for="learnercode">Terms and conditions :<span class="star">*</span></label></br>                              
								<label class="checkbox-inline"> <input type="checkbox" name="chk" id="chk" value="1" >
									<a title="" style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><span id="fix">click here</span> </a>
								</label>								
                            </div>
						</div>	
						
						<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button class="close" type="button" data-dismiss="modal">×</button>
									<h3 class="modal-title">Terms and Conditions</h3>
								</div>
								<div class="modal-body">
									<img src="images/1.jpg" />
								</div>
								<div class="modal-footer">
									<button class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
						    </div>
						  </div>
						</div>

                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
					</div>
                </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php //include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<style>
  .modal-dialog {width:700px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
<script type="text/javascript">  
  $(document).ready(function() {
		jQuery(".fix").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>


<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>

<script type="text/javascript">
	function PreviewImage(no) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
		oFReader.onload = function (oFREvent) {
			document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
		};
	};
</script>

<script language="javascript" type="text/javascript">
function checkPhoto(target) {
	var ext = $('#uploadImage1').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("uploadImage1").value = '';
			return false;
		}

    if(target.files[0].size > 50000) {
			
        //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
		alert("Image size should less or equal 50 KB");
		document.getElementById("uploadImage1").value = '';
        return false;
    }
	else if(target.files[0].size < 20000)
			{
				alert("Image size should be greater than 20 KB");
				document.getElementById("uploadImage1").value = '';
				return false;
			}
    document.getElementById("uploadImage1").innerHTML = "";
    return true;
}
</script>

<script language="javascript" type="text/javascript">
function checkSign(target) {
	var ext = $('#uploadImage2').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("uploadImage2").value = '';
			return false;
		}

    if(target.files[0].size > 50000) {
			
        //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
		alert("Image size should less or equal 50 KB");
		document.getElementById("uploadImage2").value = '';
        return false;
    }
	else if(target.files[0].size < 20000)
			{
				alert("Image size should be greater than 20 KB");
				document.getElementById("uploadImage2").value = '';
				return false;
			}
    document.getElementById("uploadImage2").innerHTML = "";
    return true;
}
</script>	

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillEmpDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlempdistrict").html(data);
                }
            });
        }
        FillEmpDistrict();
		
		function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();

        function FillEmpDepartment() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLEMPDEPARTMENT",
                success: function (data) {
                    $("#ddlDeptName").html(data);
                }
            });
        }
        FillEmpDepartment();

        function FillEmpDesignation() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLEMPDESIGNATION",
                success: function (data) {
                    $("#ddlEmpDesignation").html(data);
                }
            });
        }
        FillEmpDesignation();

        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        FillBankDistrict();

		$("#ddlBankDistrict").change(function(){
			FillBankName(this.value);
		});
        function FillBankName(districtid) {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKNAME&districtid=" + districtid,
                success: function (data) {
                    $("#ddlBankName").html(data);
                }
            });
        }        

        function FillDobProof() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FillDobProof",
                success: function (data) {
					//alert(data);
                    $("#ddlDobProof").html(data);
                }
            });
        }
        FillDobProof();

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }

        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=DELETE&values=" + StatusCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfGovtRejectedLearner.php",
                data: "action=EDIT&values=" + LearnerCode.value + "",
                success: function (data) {
					//alert(data);
                    data = $.parseJSON(data);
					txtLCode.value = data[0].LearnerCode;
                    txtEmpName.value = data[0].lname;
					txtFaName.value = data[0].fname;
					txtEmail.value = data[0].email;
					txtEmpMobile.value = data[0].mobile;
					txtGpfNo.value = data[0].gpf;
					txtEmpAddress.value = data[0].address;
					ddlempdistrict.value = data[0].district;
					ddlDeptName.value = data[0].department;
					txtEmpId.value = data[0].empid;
					ddlEmpDesignation.value = data[0].designation;
					txtEmpMarks.value = data[0].marks;
					ddlExamattepmt.value = data[0].attempt;
					txtPanNo.value = data[0].panno;
					txtBankAccount.value = data[0].accountno;
					ddlBankDistrict.value = data[0].bdistrict;
					ddlBankName.value = data[0].bankname;
					txtBranchName.value = data[0].branchname;
					txtIfscNo.value = data[0].ifsc;
					txtMicrNo.value = data[0].micr;
					ddlDobProof.value = data[0].dobproof;
						txtpaymentreceipt.value= data[0].receipt;
						txtbirthproof.value=data[0].birth;
						$("#uploadPreview1").attr('src',"upload/government_payment/" + data[0].receipt);
						$("#uploadPreview2").attr('src',"upload/government_birth/" + data[0].birth);
						
                    //alert($.parseJSON(data)[0]['StatusName']);
                }
            });
        }

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmgoventry_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>