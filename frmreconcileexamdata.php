<?php
$title="Reexam Payment Reconcile";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

 ?>

<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			 
            <div class="panel panel-primary" style="margin-top:20px !important;">

                <div class="panel-heading">Reconcile Reexam Payment Transaction</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group"> 
									<label for="edistrict">Merchant Transaction ID:</label>
									<input type="text" name="merchantTransactionIds" id="merchantTransactionIds" class="form-control"/>
								</div>
							
							<div class="col-sm-4 form-group">                                  
                               <!-- <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Validate Transaction Id" style="margin-top:25px"/> -->
							   <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Reconcile Re-Exam Fee Payment Transaction" style="margin-top:25px"/>
                            </div>
						</div>
						
					<!--	 <div id="grid" style="margin-top:25px; width:94%;"> </div>
						 
						 <div class="container">
								<div class="col-sm-4"> 
									<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Reconcile Learner Fee Payment Transaction"/>
								</div> -->
						</div>
					</div>
                </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
			
	/*	$("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");		
					$.ajax({
						type: "post",
                        url: "common/cfCheckAdmissionTxnStatus.php",
						data: "action=ValidateTxnId&values=" + merchantTransactionIds.value + "",
						success: function (data)
						{
							//alert(data);
							 $('#response').empty();
								if (data == "0") {
									$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please Enter valid Transaction Ref. No." + "</span></p>");
									}
							else {
									$("#grid").html(data);
									$('#example').DataTable({						
									dom: 'Bfrtip',
									buttons: [
										'copy', 'csv', 'excel', 'pdf', 'print'
										]
									});
									$('#merchantTransactionIds').attr('readonly', true);
									$("#btnShow").hide();
									$("#btnSubmit").show();
							}	
					    }
                });		
          }); */
		  
				$("#btnSubmit").click(function () {
					$("#btnSubmit").hide();
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfReconcileexamdata.php"; // the script where you handle the form input.
                var data;
                var forminput=$("#form").serialize();
                //alert(forminput);
				
                if (Mode == 'Add')
                {
                    data = "action=UpdateReexamDataStatus&" + forminput; // serializes the form's elements.
                }
                else
                {
                    //data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
						//alert(data);
                        $('#response').empty();
						if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmreconcileexamdata.php';
                            }, 3000);
                        }
						else if (data == 0){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please enter Merchant Transaction ID." + "</span></div>");
                            
						}
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + "   Merchant Transaction ID Status:              " +              data + "</span></div>");
                        }               
						  
                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
	 });

</script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>