<?php
$title="RKCL-Dashboard";
include ('header.php'); 
include ('root_menu.php');?>

		 <script src="scripts/highcharts.js"></script>
        <script src="scripts/exporting.js"></script>
		

		<div class="container" > 
			  
				
				<form  class="form-group" name="frmgraphmaster" id="frmgraphmaster" action="" >      
				<div class="panel panel-primary" style="margin-top:36px;">
					
                <div class="panel-heading">Project Report </div>
				
                <div class="panel-body">
				
				<div class="col-sm-12 form-group"> 
				                                <label for="edistrict">Select Years:</label>
                                <select id="ddlyear" name="ddlyear" class="form-control" >
								  
									<option value="1">
										Last 1 Year
									</option>
									<option value="2">
										Last 2 Year
									</option>
								<option value="3">
										Last 3 Year
									</option>
									<option value="4">
										Last 4 Year
									</option>
									<option value="4">
										Last 5 Year
									</option>
								
                                </select>    	

				
				<div id="container1" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
				</div>
				
				
				
				 <div class="panel-body">
				 
				
				<div class="col-sm-12 form-group"> 
					<select id="dd1date" name="dd1date" class="form-control"  >
								  
                  </select> 
				
			<div class="clearfix" style="height: 10px;clear: both;"></div>
				<select id="dd1Batch" name="dd1Batch" class="form-control"  >
								  
                  </select> 
				
				
			
			<div id="container2" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
	
			  
				</div>
				</div>
				
				</form>
					

						</div>
			  </div>

		</div>
		
	
	<script type="text/javascript">
		$(document).ready(function() {
			function Fill() {
					var date=1;
					
		               $.ajax({
		                   type: "post",
		                   url: "common/cfgraphmaster.php",
		                   data: "action=FILL&values=" + date + "",
	                           success: function (data) {
								   json1 = $.parseJSON(data);
								  //alert(json);
							options.xAxis.categories = json1[0]['data'];
				//alert(json[0]['data']);
	        	options.series[0] = json1[1];
	        	//alert(json[1]['data']);
		        chart = new Highcharts.Chart(options);
		                       
		                   }
		               });
		           }
				   Fill();
			
			
			
			
			var options= {
	            chart: {
	                renderTo: 'container1',
	                type: 'column',
	                marginRight: 130,
	                marginBottom: 25
	            },
	            title: {
	                text: 'No of Admission in year',
	                x: -20 //center
	            },
	            subtitle: {
	                text: '',
	                x: -20
	            },
	            xAxis: {
	                categories: []
	            },
	            yAxis: {
	                title: {
	                    text: 'No of Admission'
	                },
	                plotLines: [{
						
	                    value: 0,
	                    width: 2,
	                    color: '#808080'
	                }]
	            },
	            tooltip: {
	                formatter: function() {
	                        return '<b>'+ this.series.name +'</b><br/>'+
	                        this.x +': '+ this.y;
	                }
	            },
	            legend: {
	                layout: 'vertical',
	                align: 'right',
	                verticalAlign: 'top',
					
	                x: -10,
	                y: 100,
	                borderWidth: 0
	            },
	             plotOptions: {
	                column: {
	                    stacking: 'normal',
	                    dataLabels: {
	                        enabled: true,
	                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
	                    }
	                }
	            },
	            series: []
				//FillStatus();
	        }
			$("#ddlyear").change(function () {
				 var Batch = $(this).val();
				//alert(Batch);
                $.ajax({
                    url: 'common/cfgraphmaster.php',
                    type: "post",
                    data: "action=FILL&values=" + Batch + "",
                    success: function (data) {
					
								  // json2 = (data);
								  var json2=$.parseJSON(data);
						  //  alert(json2[0]['data']);
						//alert(json2[1]['data']);
							options.xAxis.categories = json2[0]['data'];
							options.series[0]=json2[1];
							chart = new Highcharts.Chart(options);
	        	//alert(json2[1]['temp']);
		        
                    }
                });
            });	
			
			 function FillStatus() {
		               $.ajax({
		                   type: "post",
		                   url: "common/cfgraphmaster.php",
		                   data: "action=FILL",
	                           success: function (data) {
								   json1 = $.parseJSON(data);
								  //alert(json);
							options.xAxis.categories = json1[0]['data'];
				//alert(json[0]['data']);
	        	options.series[0] = json1[1];
	        	//alert(json[1]['data']);
		        chart = new Highcharts.Chart(options);
		                       
		                   }
		               });
		           }
				   FillStatus();
				 
		
		});
	
		</script>
		
		
		
		
	<script type="text/javascript">
		$(document).ready(function() {
			
			function showData() {
		                
		                $.ajax({
		                    type: "post",
		                    url: "common/cfgraphmaster.php",
		                    data: "action=FILLYEAR",
		                    success: function (data) {
		
		                        $("#dd1date").html(data);
		
		                    }
		                });
		            }
					showData();
					
					
					 function showbatch() {
		                
		                $.ajax({
		                    type: "post",
		                    url: "common/cfBatchMaster.php",
		                    data: "action=FILLALLBatch",
		                    success: function (data) {
		
		                        $("#dd1Batch").html(data);
		
		                    }
		                });
		            }
					showbatch();
			function Fill() {
					var date=2015;
					var Batch=1;
		               $.ajax({
		                   type: "post",
		                   url: "common/cfgraphmaster.php",
		                   data: "action=FILLDISTICT&values="+date+"&Batch="+Batch+"",
	                           success: function (data) {
								   json1 = $.parseJSON(data);
								  //alert(json);
							options.xAxis.categories = json1[0]['data'];
				//alert(json[0]['data']);
	        	options.series[0] = json1[1];
	        	//alert(json[1]['data']);
		        chart = new Highcharts.Chart(options);
		                       
		                   }
		               });
		           }
				   Fill();	
			 
					
				
					
				
			var options= {
	            chart: {
	                renderTo: 'container2',
	                type: 'column',
	                marginRight: 130,
	                marginBottom: 25
	            },
	            title: {
	                text: 'No of Admission V/s District',
	                x: -20 //center
	            },
	            subtitle: {
	                text: '',
	                x: -20
	            },
	            xAxis: {
	                categories: []
	            },
	            yAxis: {
	                title: {
	                    text: 'No of Admission'
	                },
	                plotLines: [{
						
	                    value: 0,
	                    width: 2,
	                    color: '#808080'
	                }]
	            },
	            tooltip: {
	                formatter: function() {
	                        return '<b>'+ this.series.name +'</b><br/>'+
	                        this.x +': '+ this.y;
	                }
	            },
	            legend: {
	                layout: 'vertical',
	                align: 'right',
	                verticalAlign: 'top',
					
	                x: -10,
	                y: 100,
	                borderWidth: 0
	            },
	             plotOptions: {
	                column: {
	                    stacking: 'normal',
	                    dataLabels: {
	                        enabled: true,
	                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
	                    }
	                }
	            },
	            series: []
				//FillStatus();
	        }
			
			$("#dd1Batch").change(function () {
				 var Batch = $(this).val();
				 if(Batch=='NULL')
				 {
					 
					var Batch=1;
					 
				 }
				 else
				 {
					var Batch;
					 
				 }
				 //alert(Batch);
				var date = dd1date.value;
                //alert(Batch);
                $.ajax({
                    url: 'common/cfgraphmaster.php',
                    type: "post",
                    data: "action=FILLDISTICT&values="+date+"&Batch="+Batch+"",
					success: function (data) {
					
								  // json2 = (data);
								  var json2=$.parseJSON(data);
						  //  alert(json2[0]['data']);
						//alert(json2[1]['data']);
							options.xAxis.categories = json2[0]['data'];
							options.series[0]=json2[1];
							chart = new Highcharts.Chart(options);
	        	//alert(json2[1]['temp']);
		        
                    }
                });
            });	
			
			 
				   
		
			 
		
		});
	
	
		</script>
		
		
		
		
		
		
		

		
		
	</body>
<?php include ('footer.php'); ?>
</html>