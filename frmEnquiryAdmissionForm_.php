<?php
$title = "Admission Form";
include ('header.php');
include ('root_menu.php');

if (isset($_POST['AckCode'])) {
    echo "<script>var AckCode=" . $_POST['AckCode'] . "</script>";
    echo "<script>var CourseCode=" . $_POST['CourseCode'] . "</script>";
} else {
    echo "<script>var AckCode=0</script>";
    echo "<script>var CourseCode=0</script>";?>
    <script type="text/javascript">
        window.location.href = 'frmadmissionEnquiryreport.php';
        </script>
    <?php
}
?>
<script type="text/javascript">
    if (AckCode == '0') {
        window.location.href = 'frmadmissionEnquiryreport.php';
    }
</script>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div class="container"> 
    <div class="panel panel-primary" style="margin-top:36px !important;">


        <div class="panel-body">
            <!-- <div class="jumbotron"> -->

                            <select id="ddlBatch" name="ddlBatch" class="form-control" style="display: none;"> </select>

            <form name="frmEnquiryAdmissionForm" id="frmEnquiryAdmissionForm" class="form-inline" role="form" enctype="multipart/form-data"> 


                <div class="panel panel-primary" style="margin-top:36px !important;">
                    <div class="panel-heading" style="height:40px">
                        <div  class="col-sm-4"> Admission Form </div>

                        <div class="col-sm-4">
                            Course: <div style="display: inline" id="txtcoursename"> </div>
                        </div>

                        <div class="col-sm-4">
                            Batch: <div style="display: inline" id="txtbatchname"> </div>
                        </div>


                    </div>
                    <div class="panel-body">
                        <!-- <div class="jumbotron"> -->

                        <div class="container">
                            <div class="container">
                                <div class="col-sm-4 form-group" id="response"></div>
                            </div>        
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Learner Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtlname" id="txtlname" readonly="true" style="text-transform:uppercase" placeholder="Learner Name">
                                <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                                <input type="hidden" class="form-control" maxlength="50" name="txtAdmissionCode" id="txtAdmissionCode"/>

                                <input type="hidden" class="form-control" maxlength="50" name="txtCourseFee" id="txtCourseFee"/>
                                <input type="hidden" class="form-control" maxlength="50" name="txtInstallMode" id="txtInstallMode"/>
                                <input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
                                <input type="hidden" class="form-control"  name="txtsign" id="txtsign"/>
                                <input type="hidden" class="form-control"  name="txtLearnercode" id="txtLearnercode"/>
                                <input type="hidden" class="form-control"  name="txtAckNo" id="txtAckNo" />
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="fname">Father/Husband Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" style="text-transform:uppercase" maxlength="50" name="txtfname" id="txtfname" readonly="true" placeholder="Father Name">     
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="dob">Date of Birth:<span class="star">*</span></label>                              
                                <input type="text" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="YYYY-MM-DD">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="mtongue">Mother Tongue:</label>
                                <select id="ddlmotherTongue" name="ddlmotherTongue" class="form-control" >                                
                                    <option value="Hindi" selected>Hindi</option>
                                    <option value="English">English</option>
                                </select>    
                            </div>
                        </div>  

                        <div class="container">
                            <div class="col-sm-4 form-group">     
                                <label for="gender">Gender:</label> <br/>                               
                                <label class="radio-inline"> <input type="radio" id="genderMale" checked="checked" name="gender" /> Male </label>
                                <label class="radio-inline"> <input type="radio" id="genderFemale" name="gender" /> Female </label>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="email">Marital Status:</label> <br/>  
                                <label class="radio-inline"><input type="radio" id="mstatusSingle" checked="checked" name="mstatus" /> Single </label>
                                <label class="radio-inline"><input type="radio" id="mstatusMarried" name="mstatus" /> Married   </label>                            
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="address">Medium Of Study:</label> <br/> 

                                <label class="radio-inline"> 
                                    <input type="radio" id="mediumHindi" name="Medium" checked="checked" />Combined (Hindi + English)</label>

                            </div>

                            <div class="col-sm-6 form-group"> 
                                <label for="deptname">Physically Challenged:</label> <br/> 
                                <label class="radio-inline"> <input type="radio" id="physicallyChStYes" name="PH" /> Yes </label>
                                <label class="radio-inline"> <input type="radio" id="physicallyChStNo" name="PH" checked="checked"/> No </label>
                            </div>
                        </div>    

                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="empid">Proof Of Identity:<span class="star">*</span></label>
                                <select id="ddlidproof" name="ddlidproof" class="form-control">
                                    <option value="">Select</option>
                                    <option value="PAN Card">PAN Card</option>
                                    <option value="Voter ID Card">Voter ID Card</option>
                                    <option value="Driving License">Driving License</option>
                                    <option value="Passport">Passport</option>
                                    <option value="Employer ID card">Employer ID card</option>
                                    <option value="Government ID Card">Governments ID Card</option>
                                    <option value="College ID Card">College ID Card</option>
                                    <option value="School ID Card">School ID Card</option>
                                </select>
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="bankname">Email Id:</label>
                                <input type="text" placeholder="Email Id" class="form-control" id="txtemail" name="txtemail" maxlength="100" />
                            </div> 


                            <div class="col-sm-4 form-group"> 
                                <label for="designation">District:<span class="star">*</span></label>
                                <select id="ddlDistrict" name="ddlDistrict" class="form-control">                                  
                                </select>
                            </div>     

                            <div class="col-sm-4 form-group"> 
                                <label for="marks">Tehsil:<span class="star">*</span></label>
                                <select id="ddlTehsil" name="ddlTehsil" class="form-control">                                 
                                </select>
                            </div>
                        </div>

                        <div class="container">                          
                            <div class="col-sm-4 form-group"> 
                                <label for="attempt">Address:<span class="star">*</span></label>  
                                <textarea class="form-control" rows="3" style="text-transform:uppercase" id="txtAddress" name="txtAddress" placeholder="Address"onkeypress="return validAddress(event);" ></textarea>

                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="pan">PINCODE:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtPIN" id="txtPIN"  placeholder="PINCODE" onkeypress="javascript:return allownumbers(event);" maxlength="6"/>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="bankaccount">Mobile No:<span class="star">*</span></label>
                                <input type="text" class="form-control" readonly="true" name="txtmobile"  id="txtmobile" placeholder="Mobile No" onkeypress="javascript:return allownumbers(event);" maxlength="10"/>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="bankdistrict">Phone No:</label>
                                <input type="text" placeholder="Phone No" class="form-control" id="txtResiPh" name="txtResiPh" onkeypress="javascript:return allownumbers(event);" maxlength="11"/>
                            </div>   
                        </div>

                        <div class="container">


                            <div class="col-sm-4 form-group"> 
                                <label for="branchname">Qualification:<span class="star">*</span></label>
                                <select id="ddlQualification" name="ddlQualification" class="form-control">                                  
                                </select>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="ifsc">Type of Learner:<span class="star">*</span></label>
                                <select id="ddlLearnerType" name="ddlLearnerType" class="form-control" onChange="findselected()">                                 
                                </select>
                            </div> 


                            <div class="col-sm-4 form-group" style="display:none;" id="divEmpId"> 
                                <label for="micr">Employee ID:</label>
                                <input type="text" placeholder="GPF No" class="form-control" id="txtEmpId" name="txtEmpId" maxlength="20"/>
                            </div>

                        </div>

                        <fieldset style="border: 1px groove #ddd !important;" class="">
                            <br>
                            <div class="container">
                                <div class="col-sm-1"> 

                                </div>
                                <div class="col-sm-3" style="display: none"> 
                                    <label for="payreceipt">Upload Scan Application Form:<span class="star">*</span></label>
                                    <input type="file" class="form-control"  name="txtUploadScanDoc" id="txtUploadScanDoc" onchange="checkScanForm(this)"/>
                                </div>                                          

                                <div class="col-sm-3" > 
                                    <label for="photo">Attach Photo:<span class="star">*</span></label> </br>
                                    <img id="uploadPreview1" src="images/samplephoto.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">                                 

                                    <input style="margin-top:5px !important;" id="uploadImage1" type="file" name="p1" onchange="checkPhoto(this);
                                    PreviewImage(1)"/>                                    
                                </div>

                                <div class="col-sm-3"> 
                                    <label for="photo">Attach Signature:<span class="star">*</span></label> </br>
                                    <img id="uploadPreview2" src="images/samplesign.png" id="uploadPreview2" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage2').click();">                              
                                    <input id="uploadImage2" type="file" name="p2" onchange="checkSign(this);
                                    PreviewImage(2)"/>
                                </div>  
                                <div class="col-sm-3"> 
                                    <input style="margin-top:50px !important;" type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload Photo Sign"/>   
                                </div>
                                <div class="col-sm-1"> 

                                </div>

                            </div>
                            <br>
                        </fieldset>
                        <br><br>
                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                        </div>
                       

                    </div> 
                </div>
                 </form>
        </div>
    </div>

</div>

<link href="css/popup.css" rel="stylesheet" type="text/css">
<!-- Modal -->



</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<!-- <script src="scripts/imageupload.js"></script> -->

<style>
    #errorBox
    {   color:#F00;  } 
</style>

<script type="text/javascript">
    $('#dob').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true
    });
</script>

<script type="text/javascript">
    $('#txtEmpId').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
</script>

<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>
<script language="javascript" type="text/javascript">
    function validAddress(e) {
        var keyCode = e.keyCode || e.which;
        var lblError = document.getElementById("txtAddress");
        lblError.innerHTML = "";

        //Regex for Valid Characters i.e. Alphabets and Numbers.
        var regex = /^([a-zA-Z0-9 _-]+)$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            lblError.innerHTML = "Only Alphabets and Numbers allowed.";
        }

        return isValid;
    }
</script>

<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
    ;
</script>   

<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#txtUploadScanDoc').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }
        document.getElementById("txtUploadScanDoc").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkPhoto(target) {
        var ext = $('#uploadImage1').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage1").value = '';
            return false;
        }

        if (target.files[0].size > 5000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 5 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        } else if (target.files[0].size < 3000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        document.getElementById("uploadImage1").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkSign(target) {
        var ext = $('#uploadImage2').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage2").value = '';
            return false;
        }

        if (target.files[0].size > 3000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 3 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        } else if (target.files[0].size < 1000)
        {
            alert("Image size should be greater than 1 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        document.getElementById("uploadImage2").innerHTML = "";
        return true;
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {

        function fillForm()
        {

            $.ajax({
                type: "post",
                url: "common/cfAdmissionForm.php",
                data: "action=FILL&values=" + AckCode + "",
                success: function (data) {

                    data = $.parseJSON(data);
                    txtlname.value = data[0].lname;
                    txtfname.value = data[0].fname;
                    txtmobile.value = data[0].mobile;
                    txtAckNo.value = data[0].AckNo;

                }
            });
        }
        fillForm();


        
        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        //FillCourse();

       function FillBatch() {

        $('#ddlBatch').html('');
            var selCourse = CourseCode;
            if (selCourse == 1 || selCourse == 5 || selCourse == 20 || selCourse == 22) {
                $("#divEmpId").hide();
            }
            if (selCourse == 4) {
                $("#divEmpId").show();
            }
 
            else {
                $.ajax({
                    type: "post",
                    url: "common/cfAdmissionForm.php",
                    data: "action=FILLEventBatch&values=" + selCourse + "",
                    success: function (data) {

                        $("#ddlBatch").html(data);
                          document.getElementById('txtbatchname').innerHTML = $("#ddlBatch option:selected").text();

                    }
                });
            }
        }
        FillBatch();
        // $("#ddlBatch").change(function () {

        //     document.getElementById('txtbatchname').innerHTML = $("#ddlBatch option:selected").text();
          
        // });
        FillDistrict();



        function FillCourseName() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FillAdmissionCourse&values=" + CourseCode + "",
                success: function (data) {
            document.getElementById('txtcoursename').innerHTML = data;
                }
            });
            // document.getElementById('txtcoursename').innerHTML = $("#ddlCourse option:selected").text();
        }
        FillCourseName();

        function FillBatchName() {
            // $.ajax({
            //     type: "post",
            //     url: "common/cfBatchMaster.php",
            //     data: "action=FillAdmissionBatch&values=" + BatchCode + "",
            //     success: function (data) {
            // document.getElementById('txtbatchname').innerHTML = ddlBatch.value;
            //     }
            // });
            document.getElementById('txtbatchname').innerHTML = $("#ddlBatch option:selected").text();
        }
        //FillBatchName();

        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                }
            });
        }
        GenerateUploadId();


        function GenerateLearnercode()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmissionForm.php",
                data: "action=GENERATELEARNERCODE",
                success: function (data) {
                    txtLearnercode.value = data;
                }
            });
        }
        GenerateLearnercode();

        function AdmissionFee()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmissionForm.php",
                data: "action=Fee&codes=" + ddlBatch.value + "",
                success: function (data) {
                    txtCourseFee.value = data;
                }
            });
        }
         AdmissionFee();

        function InstallMode()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmissionForm.php",
                data: "action=Install&values=" + ddlBatch.value + "",
                success: function (data) {
                    txtInstallMode.value = data;
                }
            });
        }
        InstallMode();

        $("#txtEmpId").prop("disabled", true);



        $("input[type='image']").click(function () {
            $("input[id='my_file']").click();
        });

        function FillDistrict() {

            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        }


        function FillQualification() {
            $.ajax({
                type: "post",
                url: "common/cfQualificationMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlQualification").html(data);
                }
            });
        }
        FillQualification();

        function FillLearnerType() {
            $.ajax({
                type: "post",
                url: "common/cfLearnerTypeMaster.php",
                data: "action=FILL&coursecode=" + CourseCode + "",
                success: function (data) {
                    $("#ddlLearnerType").html(data);
                }
            });
        }
        FillLearnerType();

        function findselected() {
            var selMenu = document.getElementById('ddlLearnerType');
            var txtField = document.getElementById('txtEmpId');
            if (selMenu.value != '34')
                txtField.disabled = false
            else
                txtField.disabled = true
        }

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });

        $("#ddlLearnerType").change(function () {
            findselected();
        });




        $("#btnUpload").click(function () {
            $('#btnSubmit').show(1000);
        });
        $("#frmEnquiryAdmissionForm").submit(function () {
            // if ($("#frmAdmissionForm").valid())
            // {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfAdmissionForm.php"; // the script where you handle the form input.

            if (document.getElementById('genderMale').checked) //for radio button
            {
                var gender_type = 'Male';
            } else {
                gender_type = 'Female';
            }
            if (document.getElementById('mstatusSingle').checked) //for radio button
            {
                var marital_type = 'Single';
            } else {
                marital_type = 'Married';
            }
            if (document.getElementById('physicallyChStYes').checked) //for radio button
            {
                var physical_status = 'Yes';
            } else {
                physical_status = 'No';
            }
            var data;
            var forminput = $("#frmEnquiryAdmissionForm").serialize();
            data = forminput; // serializes the form's elements.
            var form_data = new FormData(this);
            form_data.append("action", "ADDFromEnquiry")
            form_data.append("data", data)
            form_data.append("medium_type", "HindiPlusEnglish")
            form_data.append("gender_type", gender_type)
            form_data.append("physical_status", physical_status)
            form_data.append("marital_type", marital_type)
            form_data.append("CourseCode", CourseCode)
            form_data.append("BatchCode", ddlBatch.value)
            form_data.append("ddlAadhar", "n")


            $.ajax({
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                url: url,
                data: form_data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                        window.setTimeout(function () {
                            Mode = "Add";
                            window.location.href = 'frmadmissionenquiryreport.php';
                        }, 3000);
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                    }


                }
            });
            //  }
            return false; // avoid to execute the actual submit of the form.
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        // statement block end for submit click
    });

</script>

<!--<script src="scripts/signupload.js"></script>-->
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmadmissionform_validation.js" type="text/javascript"></script>    
</html>