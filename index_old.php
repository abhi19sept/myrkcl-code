<?php include ('outer_header.php'); ?>	
  
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4"  style="margin-top:36px;">
            <div class="panel panel-default">
                <div class="panel-heading"> <strong class="">Login</strong>

                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form">
					 <div id="response"></div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="txtUserName" name="txtUserName" placeholder="Username" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="txtPassword" name="txtPassword" placeholder="Password" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <label class="option block">
									<a href="forgot_password.php">Forgot Password ?</a>            
                                </label>                                       
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" name="btnSubmit"  id="btnSubmit" class="btn btn-success btn-sm">Sign in</button>
                                <button type="reset" class="btn btn-default btn-sm">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">Not Registered? <a href="#" class="">Register here</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include ('./common/message.php');
	  include ('footer.php'); ?>
	  
<script type="text/javascript">
            var Success = "<?php echo Message::SuccessfullyFetch; ?>";
            $("#btnSubmit").click(function () {
                //alert('Button Click');
                //alert(txtUserName.value);
                //alert(txtPassword.value);
                $('#response').empty();
                $('#response').append("<p class='state-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Authenticating.....</span></p>");
                var url = "./common/cfLogin.php"; // the script where you handle the form input.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: "action=CheckLogin&username=" + txtUserName.value + "&password=" + txtPassword.value + "", // serializes the form's elements.
                    success: function (data)
                    {
                        //alert(data);
                        // show response from the php script.
                        if (data === Success)
                        {
                            //alert(data);
                            $('#response').empty();
                            $('#response').append("<p class='state-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function ()
                            {
                                SuccessfullyLogin();
                            }, 3000);

                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='state-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                            resetForm("frmlogin");
                        }

                    }

                });

                return false; // avoid to execute the actual submit of the form.
            });
            function SuccessfullyLogin()
            {
                
                window.location.href = 'dashboard.php';
            }
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }


        </script>