<header>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link href="css/ct-navbar.css" rel="stylesheet" />
    <script src="js/ct-navbar.js"></script>
    <link rel="stylesheet" href="css/searchbox.css">
    <div id="navbar-full">
    <div id="navbar">
        <nav class="navbar navbar-ct-blue navbar-fixed-top" role="navigation">	
            <?php
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
			
			if($_SESSION['User_UserRoll'] == 19 || $_SESSION['User_UserRoll'] == 33){
				if($_COOKIE['useridCook'] != $_SESSION['User_LearnerCode']){
					?>
					<script>
						window.location.href = "logout.php";
					</script>
					<?php
				}
			}
				else{
					if(strtolower($_COOKIE['useridCook']) != strtolower($_SESSION['User_LoginId'])){
						?>
					<script>
						window.location.href = "logout.php";
					</script>
					<?php
				}
			}
			
			
            if (isset($_SESSION['Login']) && !empty($_SESSION['Login'])) {
                //echo 'Set and not empty, and no undefined index error!';
            } else {
                ?>
                <script>
                    window.location.href = "logout.php";
                </script>
                <?php
            }
            if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 3600)) {
                header("Location:logout.php");
            }
            $_SESSION['LAST_ACTIVITY'] = time();
            if (isset($_SESSION['Login'])) {
                if ($_SESSION['Login'] === 1) {
                    ?>
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="logo">
                                <a href="dashboard.php">
                                    <img src="images/myrkcl_logo.png"  height="90" >
                                </a>
                            </div>
                        </div>
                        <!-- Added By Sunil : For learner login  -->
                        <?php
                        if ($_SESSION['User_UserRoll'] == 19 || $_SESSION['User_UserRoll'] == 33) {
                            ?>   

                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li>
                                        <a href="#" >
                                            <i class="fa fa-users" style="font-size:32px !important;"></i>
                                            <p>Logged in as <?php echo $_SESSION['UserRoll_Name']; ?>  (<?php echo $_SESSION['User_LearnerCode']; ?>)</p>
                                        </a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="pe-7s-user"></i>
                                            <p>Account</p>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="logout.php">Logout</a></li>
                                            <li class="divider"></li>
                                        </ul>
                                    </li>
                                </ul>
                                <form class="navbar-form navbar-right navbar-search-form sitemapnavbar" role="search" >                  
                                    <div class="form-group">
                                        <input type="text" value="" class="form-control" placeholder="Search...">
                                    </div> 
                                </form>
                            </div>
                        <?php
                        } else {
                            ?>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li>
                                        <a href="javascript:void(0);" data-toggle="search" class="hidden-xs">
                                            <i class="pe-7s-search"></i>
                                            <p>Search</p>
                                        </a>
                                    </li>  			
                                    <li>
                                        <a href="http://support.myrkcl.com/" target="_blanck">
                                            <i class="fa fa-support" style="font-size:32px !important;"> </i>
                                            <p>Support</p>
                                        </a>
                                    </li> 
                                    <li>
                                        <a href="http://ilearn.myrkcl.com" target="_blanck">
                                            <i class="pe-7s-culture pe-fw"> </i>
                                            <p>LMS</p>
                                        </a>
                                    </li> 
                                    <li>
                                        <a href="frmmessagenotification.php" class="" id="messagetag">
                                            <i class="pe-7s-mail">
                                                <span class="label" id=""></span>
                                                <span class="badge" id="messagecount"></span>
                                            </i>
                                            <p>Messages</p>
                                        </a>
                                    </li>  
                                    <style>
                                        .notification {
                                            background-color: #ece3e3;
                                            color: white;
                                            text-decoration: none;
                                            padding: 15px 26px;
                                            position: relative;
                                            display: inline-block;
                                            border-radius: 2px;
                                        }
                                        .notification:hover {
                                            background: red;
                                        }
                                        .notification .badge {
                                            position: absolute;
                                            top: -6px;
                                            right: -34px;
                                            padding: 5px 10px;
                                            border-radius: 50%;
                                            background-color: red;
                                            color: white;
                                        }
                                    </style>  
                                    <li>
                                        <a href="#" >
                                            <i class="fa fa-users" style="font-size:32px !important;"></i>
                                            <p>Logged in as <?php echo $_SESSION['UserRoll_Name']; ?>  (<?php echo $_SESSION['User_LoginId']; ?>)</p>
                                        </a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="pe-7s-user"></i>
                                            <p>Account</p>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="change_password.php">Change Password</a></li>
                                            <li><a href="logout.php">Logout</a></li>
                                            <li class="divider"></li>
                                        </ul>
                                    </li>
                                </ul>
                                <div class="container  navbar-form navbar-right navbar-search-form sitemapnavbar" >

                                    <div class="searchfield form-wrapper cf stemapformwrap">
                                        <input type="text"  name="currency" class="biginput" id="autocomplete"   onkeypress="javascript:return validAddress(event);" placeholder="Search...">
                                        <a href="frmsitemap.php" id="sitemaplink">Site Map</a>
                                    </div>
                                </div>
                            </div><!-- /.navbar-collapse -->
                        <?php } ?>
                        <!-- Added By Sunil : For learner login  -->
                    </div><!-- /.container-fluid -->
                </nav>
                <div class="">
                </div>
            </div><!--  end navbar -->
        </div> <!-- end menu-dropdown -->
    </header>
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <?php echo $_SESSION['Menu']; ?>
                </ul>
            </div>

        </div>
    </div>
        <?php
        if (isset($_SESSION['ITGK_Expire_Days'])) {
            echo '<marquee id="scroll_news" onMouseOver="this.stop();" onMouseOut="this.start();" style="font-family:Book Antiqua; color: #FF0000"> <div id="news">' . $_SESSION['ITGK_Expire_Days'] . '</div>   
                </marquee>';
        }
    }
} else {
    header("Location:logout.php");
}
?>
<script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>
<script type="text/javascript">
if (document.location.href.match(/[^\/]+$/)[0] === "dashboard.php") {
    $.ajax({
        type: "post",
        url: "common/cfpushsmsnotification.php",
        data: "action=COUNTMESSAGE",
        success: function (data) {
            if (data === '0') {
                $("#messagecount").css("display", "none");
                $("#messagetag").removeClass("notification");
            } else {
                $("#messagecount").css("display", "block");
                $("#messagetag").addClass("notification");
                $("#messagecount").html(data);
            }

        }
    });
}
$(document).ready(function () {
    /* For Searching Function  */
    $("#Search").keyup(function ()
    {
        var sel = $(this).val();
        $.ajax({
            url: 'common/cfsearch.php',
            type: "post",
            data: "action=FILL&values=" + sel + "",
            success: function (data) {
                $("#results").html("");
                $("#results").show();
                $("#results").append(data);
            }
        });
        if ($("#Search").val() == "") {
            $("#results").html("");
        }
    });
    var currencies = [];
    function FillSkills(sel) {
        $.ajax({
            url: 'common/cfsearch.php',
            type: "post",
            data: "action=FILL2&values=" + sel + "",
            success: function (data) {
                var parseData = $.parseJSON(data);
                $.each(parseData, function (i, v)
                {
                    currencies.push({
                        'value': v.Function_Name,
                        'data': v.Function_URL
                    });

                });
            }
        });
    }
    FillSkills();
    // setup autocomplete function pulling from currencies[] array
    $('#autocomplete').autocomplete({
        lookup: currencies,
        onSelect: function (suggestion) {
            window.location.href = suggestion.data;
        }
    });
    /*for Search key function */
    $("#autocomplete").keypress(function (event)
    {  
        if (!$(this).val()) {
            $("#sitemaplink").css("display", "block");
            $("#sitemaplink").css("margin-left", "120px");
        } else {
            $("#sitemaplink").css("display", "none");
        }
    });
    $("#autocomplete").bind("keypress keyup keydown", function (event) {
        var ekeyCode = event.keyCode;
        if (ekeyCode == '8')
        { 
            if (!$(this).val()) {
                $("#sitemaplink").css("display", "block");
                $("#sitemaplink").css("margin-left", "120px");
            } else {
                $("#sitemaplink").css("display", "none");
            }
        }
    });
});
</script>
<style>
    .sitemapnavbar {
        margin-bottom: 15px;
        margin-top: 5px !important;
        padding-left: 5px;
        padding-right: 5px;
    }

    .stemapformwrap input
    {

        border-radius: 5px 5px 5px 5px;
        border: 1px solid #34ACDC;


    }
    #sitemaplink{
        position: absolute;
        margin: 12px 0 0 -62px;

    }
    .stemapformwrap
    {
        width: 180px;
    }

    .stemapformwrap input {
        width: 180px;
    }
</style>