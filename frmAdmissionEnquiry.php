<?php
$title = "Admission Form";
include ('outer_header_admission.php');
if (isset($_POST['MobNo2'])) {
    echo "<script>var MobNo=" . $_POST['MobNo2'] . "</script>";
} else {
    echo "<script>var MobNo=0</script>";
}
?>
<script type="text/javascript">
    if(MobNo == '0'){
            window.location.href = 'frmlearneradmission.php';
        }
</script>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:1500px !important;">
<div class="container"> 
<div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading" style="height:auto !important">
                <div class="container">
                    <div  class="col-sm-4"> Application Form </div>

<!--                    <div class="col-sm-4">
                        Course: <div style="display: inline" id="txtcoursename"> </div>
                    </div>

                    <div class="col-sm-4">
                        Application Month: <div style="display: inline" id="txtbatchname"> </div>
                    </div>-->

                </div>


            </div>

            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmAdmissionEnquiry" id="frmAdmissionEnquiry" class="form-inline" role="form" enctype="multipart/form-data"> 
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                         <div class="col-sm-3 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }">  </select>
                        </div> 
                        <div class="col-sm-3 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"> </select>
                        </div>
                        <div class="col-sm-3 form-group"> 
                            <label for="designation">District:<span class="star">*</span></label>
                            <select id="ddlDistrict" name="ddlDistrict" class="form-control">                                  
                            </select>
                        </div>     

                        <div class="col-sm-3 form-group"> 
                            <label for="marks">Tehsil:<span class="star">*</span></label>
                            <select id="ddlTehsil" name="ddlTehsil" class="form-control">                                 
                            </select>
                        </div>
                        <div class="col-sm-9 abhi">     
                            <label for="batch"> Select IT-GK Code:</label>
                            <select id="ddlCenter" name="ddlCenter" class="form-control select2">

                            </select>                                   
                        </div>  

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" style="margin-top:25px; display:none;"/>    
                        </div>
                    </div>
                    <br>
                    <div id="CenterDetail" style="display:none;">
                        <div class="panel panel-info">
                            <div class="panel-heading">Your Selected IT-GK Detail</div>
                            <div class="panel-body">
                                <div class="container">
                                    <div class="col-sm-4 form-group">     
                                        <label for="learnercode">Name of Organization/Center:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="ename">Mobile No:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtMobile" id="txtMobile" placeholder="Registration No">     
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Email Id:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtEmail" id="txtEmail"  placeholder="District">   
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Address:</label>
                                        <textarea class="form-control" readonly="true" name="txtAddress" id="txtAddress"  placeholder="Address"></textarea>   
                                    </div>
                                </div>  
                                 <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">District:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Tehsil:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                                    </div>
                                </div> 

                            </div>
                        </div>

                    
                    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading" style="height:40px">
            <div  class="col-sm-4">Enquiry Form </div>

            <div class="col-sm-4">
                Course: <div style="display: inline" id="txtcoursename"> </div>
            </div>

            <div class="col-sm-4">
                Batch: <div style="display: inline" id="txtbatchname"> </div>
            </div>


        </div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            
                <div class="container">
                    <div class="container">
                        <div class="col-sm-4 form-group" id="response"></div>
                    </div>        
                    <div id="errorBox"></div>
                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Learner Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" name="txtlname" id="txtlname" style="text-transform:uppercase" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || event.charCode ==32" placeholder="Learner Name">
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        
                        <input type="hidden" class="form-control" name="ITGK_Code" id="ITGK_Code"/>
                        <input type="hidden" class="form-control"  name="User_Code" id="User_Code" value="2289" />
                        <input type="hidden" class="form-control"  name="User_Rsp" id="User_Rsp" value="4459" />
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="fname">Father/Husband Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" style="text-transform:uppercase" maxlength="50" name="txtfname" id="txtfname" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || event.charCode ==32" placeholder="Father Name">     
                    </div>
                    <div class="col-sm-4 form-group"> 
                        <label for="bankaccount">Mobile No:<span class="star">*</span></label>
                        <input type="text" class="form-control" readonly="true" name="txtmobile"  id="txtmobile" placeholder="Mobile No" onkeypress="javascript:return allownumbers(event);" maxlength="10"/>
                    </div>
                    <div class="col-sm-4 form-group" style="margin-top: 18px">
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit Enquiry" style="min-width: -webkit-fill-available;"/>    
                    </div>

                </div>  

                </form>
                </div>
            </div> 
                </div>
            </div>
        </div>
  
</div>
</div>
<link href="css/popup.css" rel="stylesheet" type="text/css">
<!-- Modal -->



</body>
<?php include'common/message.php'; ?>
<script src="js/bootstrap-multiselect.js"></script>     
<link rel="stylesheet" href="css/bootstrap-multiselect.css">

<style>

.form-inline .form-control {

    width: 90%!important;

}
    .abhi .filter input{
            height: 36px;
    }
    ul{
        max-width: 1000px;
    }
    .dropdown-menu.pull-right {
        right: auto;
        left: auto;
    }
    .selectBox {
        position: relative;
    }

    .selectBox select {
        width: 100%;

    }

    .overSelect {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
    }

    #checkboxes {
        display: none;
        border: 1px #dadada solid;
    }

    #checkboxes label {
        display: block;
    }

    #checkboxes label:hover {
        background-color: #1e90ff;
    }
</style>
<?php include ('footer.php'); ?>

<!-- <script src="scripts/imageupload.js"></script> -->

<style>
    #errorBox
    {	color:#F00;	 } 
</style>
<script>
    $('.select2').select2();
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {
        $("#txtmobile").val(MobNo);
        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfAdmissionEnquirySummary.php",
                data: "action=FILLAdmissionSummaryCourse",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
             FillCourseName();
            document.getElementById('txtcoursename').innerHTML = ddlCourse.options[ddlCourse.selectedIndex].text;
            var selCourse = $(this).val();
            if(selCourse == 1 || selCourse == 5 || selCourse == 20 || selCourse == 22){
                $("#divEmpId").hide();
            }
            if(selCourse == 4){
                $("#divEmpId").show();
            }
            if (selCourse == 3) {
                //alert("ok");
                $("#ddlCourse").val('');
                //$("#ddlBatch").val('');
                $('#ddlBatch').html(' ');
                return false;
            }
                else if (selCourse == 23) {
                window.location.href = "frmclickadmission.php";
            }
            else {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLEventBatch&values=" + selCourse + "",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });
            }
        });
         $("#ddlBatch").change(function () {
            document.getElementById('txtbatchname').innerHTML = ddlBatch.options[ddlBatch.selectedIndex].text;
             FillBatchName();
        });

        $("#ddlDistrict").change(function () {
            $("#btnShow").hide();
            $("#ddlTehsil").html('');
            $("#ddlCenter").html('');
            // $('#ddlCenter').multiselect('destroy');
            // $('#ddlCenter').multiselect('rebuild');
    //             $('#ddlCenter option:selected').each(function() {
    //     $(this).prop('selected', false);
    // })
    // $('#ddlCenter').multiselect('refresh');

        });

        $("#ddlTehsil").change(function () {
            $("#btnShow").hide();
             $("#ddlCenter").html('');
              //$('#ddlCenter').multiselect('rebuild');
        });

        $("#ddlCenter").change(function () {
            $("#ITGK_Code").val((ddlCenter.options[ddlCenter.selectedIndex].text).substring(0, 8));

            $("#btnShow").show();
        });



        function FillDistrict() {
            //alert("hi");
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlDistrict").html(data);
                }
            });
        }
        FillDistrict();
        
        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });
        
        $("#ddlTehsil").change(function () {
            $("#ddlCenter").html('');
            var selTehsil = $(this).val();
            var Course = ddlCourse.value;
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfLearnerAdmission.php",
                data: "action=FILLCenter&tehsil=" + selTehsil + "&course=" + Course + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlCenter").html(data);
                     //$('#ddlCenter').select2();
                        // $('#ddlCenter').multiselect({
                           
                        //     nonSelectedText: 'Select Batch',
                        //     includeResetOption: true,
                        //     resetText: "Reset all",
                        //     enableFiltering: true,
                        //     enableCaseInsensitiveFiltering: true,

                        //     minHeight: 200,
                        //     maxHeight: '200',
                        //     width: '33%',
                        //     buttonWidth: '100%',
                        //     dropRight: true,
                            

                        // });
                   

                }
            });
        });
        $("#btnShow").click(function () {
            var a = $('#ddlCenter').val();
             var b = $('#ddlCourse').val();
            var c = $('#ddlBatch').val();
            //alert(a);
            if (a == "0" || a == "" || b == "" || c == "")
            {
                alert("Please Select all to Proceed");
            } else {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfLearnerAdmission.php",
                    data: "action=CENTERDETAILS&values=" + ddlCenter.value + "",
                    success: function (data)
                    {
                        //alert(data);
                        $('#response').empty();
                        if (data == "") {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select Center To Proceed" + "</span></p>");
                        }
                        else {
                            data = $.parseJSON(data);
                            txtName1.value = data[0].orgname;
                            txtMobile.value = data[0].regno;
                            txtDistrict.value = data[0].district;
                            txtTehsil.value = data[0].tehsil;
                            txtEmail.value = data[0].emailid;
                            txtAddress.value = data[0].itgk_address;
                            $("#CenterDetail").show(3000);
                        }
                    }
                });
            }
        });

        function FillCourseName() {
            // $.ajax({
            //     type: "post",
            //     url: "common/cfCourseMaster.php",
            //     data: "action=FillAdmissionCourse&values=" + CourseCode + "",
            //     success: function (data) {
                   // document.getElementById('txtcoursename').innerHTML = ddlCourse.value;
            //     }
            // });
             document.getElementById('txtcoursename').innerHTML = ddlCourse.options[ddlCourse.selectedIndex].text;
        }
        // FillCourseName();

        function FillBatchName() {
            // $.ajax({
            //     type: "post",
            //     url: "common/cfBatchMaster.php",
            //     data: "action=FillAdmissionBatch&values=" + BatchCode + "",
            //     success: function (data) {
                   // document.getElementById('txtbatchname').innerHTML = ddlBatch.value;
            //     }
            // });
             document.getElementById('txtbatchname').innerHTML = ddlBatch.options[ddlBatch.selectedIndex].text;
        }
       // FillBatchName();

        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                }
            });
        }
        GenerateUploadId();



        function FillDistrict() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        }
        FillDistrict();

        function FillQualification() {
            $.ajax({
                type: "post",
                url: "common/cfQualificationMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlQualification").html(data);
                }
            });
        }
        FillQualification();



        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });

        $("#frmAdmissionEnquiry").submit(function () {
			// if ($("#frmAdmissionEnquiry").valid())
			// {
            var d = $('#txtlname').val();
            var e = $('#txtfname').val();
            var f = $('#txtmobile').val();
            //alert(a);
            if (d == "0" || d == "" || e == "" || f == "" || f=="0")
            {
                alert("Please enter all to Proceed");
            } else {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfAdmissionForm.php"; // the script where you handle the form input.


            var data;
            var forminput = $("#frmAdmissionEnquiry").serialize();
            data = forminput; // serializes the form's elements.
            var form_data = new FormData(this);
            form_data.append("action", "ADDEnquiry")
            form_data.append("data", data)

            form_data.append("CourseCode", ddlCourse.value)
            form_data.append("BatchCode", ddlBatch.value)


           
            $.ajax({
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                url: url,
                data: form_data,
                success: function (data)
                {                        
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='state-success'><span><img src=images/correct.gif width=10px /></span><span> Enquiry Submitted Successfully.</span></p>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmAdmissionEnquiry.php';
                            }, 3000);
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                        }


                }
            });
            }
          //  }
            return false; // avoid to execute the actual submit of the form.
        });

       function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        // statement block end for submit click
    });

</script>

<!--<script src="scripts/signupload.js"></script>-->
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<!-- <script src="bootcss/js/frmAdmissionEnquiry_validation.js" type="text/javascript"></script> -->	
</html>