<?php
    $title = "Address Update Request Center Detail";
    include('header.php');
    include('root_menu.php');
?>
<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container">

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Address Update Requests

            </div>
            <div class="panel-body">
                <input type="button" class="btn-btn-primary" id="download_file" name="download_file" value="Download Excel">
                <br>   <br> 
                <div id="response"></div>
                <div id="gird"></div>

            </div>

        </div>
    </div>

</div>

<?php
    include 'common/message.php';
    include 'footer.php';

?>

<script type="text/javascript">

    var User_Roll = '<?php echo $_SESSION['User_UserRoll'];?>';
    var User_Code = '<?php echo $_SESSION['User_Code']?>';

    $(document).ready(function () {

//        function showData() {
//            var url = "common/cfmodifyITGK.php";
//            var data;
//            data = "action=showAddressUpdateRequestTableITGK";
//            $.ajax({
//                type: "POST",
//                url: url,
//                data: data,
//                success: function (data) {
//                    showDatatable(data);
//                }
//            });
//        }
//
//        function showDatatable(data) {
//            $("#gird").html(data);
//            $("#example").DataTable({
//                dom: 'Bfrtip',
//                "bProcessing": true,
//                "serverSide": true,
//                "ajax": {
//                    url: "common/cfmodifyITGK.php",
//                    data: {
//                        "action": "showAddressUpdateRequestITGK",
//                        "flag":"address",
//                        "Code": User_Code
//                    },
//                    type: "POST"
//                },
//                buttons: [
//                    //'copy', 'csv', 'excel', 'pdf', 'print'
//                ],
//                columnDefs: [
//                    {
//                        targets: 1,
//                        render: function (data, type, row, meta) {
//                            //alert(row[8]);
//                            if (type === 'display' && User_Roll === '14' && row[8]==='New Process') {
//                                data = '<a href="viewDetail.php?Code=' + encryptString(data) + '">' + data + '</a>';
//                            }
//
//                            if (type === 'display' && (User_Roll === '10' || User_Roll === '4' || User_Roll === '1')) {
//                                data = '<a href="viewDetail_rkcl.php?Code=' + encryptString(data) + '">' + data + '</a>';
//                            }
//                            return data;
//                        }
//                    }
//                ]
//            });
//        }
//
//        showData();


                        function showData() {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                            $.ajax({
                                type: "post",                 
                                url: "common/cfmodifyITGK.php",
                                data: "action=showAddressUpdateRequestTableITGK&Code=" + User_Code + "",
                                success: function (data) {
                                    $('#response').empty();
                                    $("#gird").html(data);
                                    $('#example').DataTable({
                                        dom: 'Bfrtip',
                                        buttons: [
                                        'copy', 'csv', 'excel', 'pdf', 'print'                         ]
                                    });



                                }
                            });
                        }

                        showData();
        
        $("#download_file").click(function () {
                $('#response').empty();
                var url = "common/cfmodifyITGK.php"; // the script where you handle the form input.            
                var data= "action=DOWNLOAD&usercode=" + User_Code + ""; // serializes the form's elements.
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
 window.open(data, '_blank');

                    }
                });
            return false;
        });
    });

</script>
</html>