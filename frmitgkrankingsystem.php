<?php
$title="ITGK Ranking Management System";
include ('header.php'); 
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
if ($_SESSION['User_UserRoll'] == '1'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}

?>
<link rel="stylesheet" href="css/profile_style.css">

<!----  /* Show Table Count Detail*/for box css stat form here---->

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<style>
    

.block-header h2 {
    margin: 0 !important;
    color: #666 !important;
    font-weight: normal;
    font-size: 16px;
}
    /* Card ======================================== */
    .info-box .content .number {
    font-weight: normal;
    font-size: 26px;
    margin-top: -4px;
    color: #555 !important;
}
.bg-red {
  background-color: #F44336 !important;
  color: #fff; }
  .bg-red .content .text,
  .bg-red .content .number {
    color: #fff !important; }

.bg-pink {
  background-color: #E91E63 !important;
  color: #fff; }
  .bg-pink .content .text,
  .bg-pink .content .number {
    color: #fff !important; }

.bg-purple {
  background-color: #9C27B0 !important;
  color: #fff; }
  .bg-purple .content .text,
  .bg-purple .content .number {
    color: #fff !important; }

.bg-deep-purple {
  background-color: #673AB7 !important;
  color: #fff; }
  .bg-deep-purple .content .text,
  .bg-deep-purple .content .number {
    color: #fff !important; }

.bg-indigo {
  background-color: #3F51B5 !important;
  color: #fff; }
  .bg-indigo .content .text,
  .bg-indigo .content .number {
    color: #fff !important; }

.bg-blue {
  background-color: #2196F3 !important;
  color: #fff; }
  .bg-blue .content .text,
  .bg-blue .content .number {
    color: #fff !important; }

.bg-light-blue {
  background-color: #03A9F4 !important;
  color: #fff; }
  .bg-light-blue .content .text,
  .bg-light-blue .content .number {
    color: #fff !important; }

.bg-cyan {
  background-color: #00BCD4 !important;
  color: #fff; }
  .bg-cyan .content .text,
  .bg-cyan .content .number {
    color: #fff !important; }

.bg-teal {
  background-color: #009688 !important;
  color: #fff; }
  .bg-teal .content .text,
  .bg-teal .content .number {
    color: #fff !important; }

.bg-green {
  background-color: #4CAF50 !important;
  color: #fff; }
  .bg-green .content .text,
  .bg-green .content .number {
    color: #fff !important; }

.bg-light-green {
  background-color: #8BC34A !important;
  color: #fff; }
  .bg-light-green .content .text,
  .bg-light-green .content .number {
    color: #fff !important; }

.bg-lime {
  background-color: #CDDC39 !important;
  color: #fff; }
  .bg-lime .content .text,
  .bg-lime .content .number {
    color: #fff !important; }

.bg-yellow {
  background-color: #ffe821 !important;
  color: #fff; }
  .bg-yellow .content .text,
  .bg-yellow .content .number {
    color: #fff !important; }

.bg-amber {
  background-color: #FFC107 !important;
  color: #fff; }
  .bg-amber .content .text,
  .bg-amber .content .number {
    color: #fff !important; }

.bg-orange {
  background-color: #FF9800 !important;
  color: #fff; }
  .bg-orange .content .text,
  .bg-orange .content .number {
    color: #fff !important; }

.bg-deep-orange {
  background-color: #FF5722 !important;
  color: #fff; }
  .bg-deep-orange .content .text,
  .bg-deep-orange .content .number {
    color: #fff !important; }

.bg-brown {
  background-color: #795548 !important;
  color: #fff; }
  .bg-brown .content .text,
  .bg-brown .content .number {
    color: #fff !important; }

.bg-grey {
  background-color: #9E9E9E !important;
  color: #fff; }
  .bg-grey .content .text,
  .bg-grey .content .number {
    color: #fff !important; }

.bg-blue-grey {
  background-color: #607D8B !important;
  color: #fff; }
  .bg-blue-grey .content .text,
  .bg-blue-grey .content .number {
    color: #fff !important; }

.bg-black {
  background-color: #000000 !important;
  color: #fff; }
  .bg-black .content .text,
  .bg-black .content .number {
    color: #fff !important; }

.bg-white {
  background-color: #ffffff !important;
  color: #fff; }
  .bg-white .content .text,
  .bg-white .content .number {
    color: #fff !important; }

.col-red {
  color: #F44336 !important; }

.col-pink {
  color: #E91E63 !important; }

.col-purple {
  color: #9C27B0 !important; }

.col-deep-purple {
  color: #673AB7 !important; }

.col-indigo {
  color: #3F51B5 !important; }

.col-blue {
  color: #2196F3 !important; }

.col-light-blue {
  color: #03A9F4 !important; }

.col-cyan {
  color: #00BCD4 !important; }

.col-teal {
  color: #009688 !important; }

.col-green {
  color: #4CAF50 !important; }

.col-light-green {
  color: #8BC34A !important; }

.col-lime {
  color: #CDDC39 !important; }

.col-yellow {
  color: #ffe821 !important; }

.col-amber {
  color: #FFC107 !important; }

.col-orange {
  color: #FF9800 !important; }

.col-deep-orange {
  color: #FF5722 !important; }

.col-brown {
  color: #795548 !important; }

.col-grey {
  color: #9E9E9E !important; }

.col-blue-grey {
  color: #607D8B !important; }

.col-black {
  color: #000000 !important; }

.col-white {
  color: #ffffff !important; }
    /* Infobox ===================================== */
.info-box {
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
  height: 80px;
  display: flex;
  cursor: default;
  background-color: #fff;
  position: relative;
  overflow: hidden;
  margin-bottom: 30px; }
  .info-box .icon {
    display: inline-block;
    text-align: center;
    background-color: rgba(0, 0, 0, 0.12);
    width: 80px; }
    .info-box .icon i {
      color: #fff;
      font-size: 50px;
      line-height: 80px; }
    .info-box .icon .chart.chart-bar {
      height: 100%;
      line-height: 100px; }
      .info-box .icon .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box .icon .chart.chart-pie {
      height: 100%;
      line-height: 123px; }
      .info-box .icon .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box .icon .chart.chart-line {
      height: 100%;
      line-height: 115px; }
      .info-box .icon .chart.chart-line canvas {
        vertical-align: baseline !important; }
  .info-box .content {
    display: inline-block;
    padding: 7px 10px; }
    .info-box .content .text {
      font-size: 13px;
      margin-top: 3px;
      color: #555; }
    .info-box .content .number {
      font-weight: normal;
      font-size: 26px;
      margin-top: -4px;
      color: #555; }
	  
	.dropdown-menu{
    box-shadow: -7px 7px 20px 1px rgb(136, 136, 136);
}


.chromeframe {
    margin: 0.2em 0;
    background: #ccc;
    color: #000;
    padding: 0.2em 0;
}


#loader-wrapper {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1000;
    background: rgba(0, 0, 0, 0.4);
}
#loader {
    display: block;
    position: relative;
    left: 50%;
    top: 50%;
    width: 150px;
    height: 150px;
    margin: -75px 0 0 -75px;
    border-radius: 50%;
    border: 3px solid transparent;
    border-top-color: #3498db;

    -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
    animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */

    z-index: 1001;
}

#loader:before {
    content: "";
    position: absolute;
    top: 5px;
    left: 5px;
    right: 5px;
    bottom: 5px;
    border-radius: 50%;
    border: 3px solid transparent;
    border-top-color: #e74c3c;

    -webkit-animation: spin 3s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
    animation: spin 3s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
}

#loader:after {
    content: "";
    position: absolute;
    top: 15px;
    left: 15px;
    right: 15px;
    bottom: 15px;
    border-radius: 50%;
    border: 3px solid transparent;
    border-top-color: #f9c922;

    -webkit-animation: spin 2s linear infinite; /* Chrome, Opera 15+, Safari 5+ */
    animation: spin 2s linear infinite; /* Chrome, Firefox 16+, IE 10+, Opera */
}

@-webkit-keyframes spin {
    0%   {
        -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(0deg);  /* IE 9 */
        transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
    }
    100% {
        -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(360deg);  /* IE 9 */
        transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
    }
}
@keyframes spin {
    0%   {
        -webkit-transform: rotate(0deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(0deg);  /* IE 9 */
        transform: rotate(0deg);  /* Firefox 16+, IE 10+, Opera */
    }
    100% {
        -webkit-transform: rotate(360deg);  /* Chrome, Opera 15+, Safari 3.1+ */
        -ms-transform: rotate(360deg);  /* IE 9 */
        transform: rotate(360deg);  /* Firefox 16+, IE 10+, Opera */
    }
}

#loader-wrapper .loader-section {
    position: fixed;
    top: 0;
    width: 51%;
    height: 100%;
    background: #222222;
    z-index: 1000;
}

#loader-wrapper .loader-section.section-left {
    left: 0;
}

#loader-wrapper .loader-section.section-right {
    right: 0;
}

.loaded #loader {
    opacity: 0;
    -webkit-transition: all 1s ease-out;
    transition: all 1s ease-out;
}
.loaded #loader-wrapper {
    opacity: 0;
    visibility: hidden;
    -webkit-transition: all 2s;
    transition: all 2s;
}

/* JavaScript Turned Off */
.no-js #loader-wrapper {
    display: none;
}
.no-js h1 {
    color: #222222;
}





/* ==========================================================================
   Helper classes
   ========================================================================== */

/*
 * Image replacement
 */

.ir {
    background-color: transparent;
    border: 0;
    overflow: hidden;
    /* IE 6/7 fallback */
    *text-indent: -9999px;
}

.ir:before {
    content: "";
    display: block;
    width: 0;
    height: 150%;
}

/*
 * Hide from both screenreaders and browsers: h5bp.com/u
 */

.hidden {
    display: none !important;
    visibility: hidden;
}

/*
 * Hide only visually, but have it available for screenreaders: h5bp.com/v
 */

.visuallyhidden {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
}

/*
 * Extends the .visuallyhidden class to allow the element to be focusable
 * when navigated to via the keyboard: h5bp.com/p
 */

.visuallyhidden.focusable:active,
.visuallyhidden.focusable:focus {
    clip: auto;
    height: auto;
    margin: 0;
    overflow: visible;
    position: static;
    width: auto;
}

/*
 * Hide visually and from screenreaders, but maintain layout
 */

.invisible {
    visibility: hidden;
}

/*
 * Clearfix: contain floats
 *
 * For modern browsers
 * 1. The space content is one way to avoid an Opera bug when the
 *    `contenteditable` attribute is included anywhere else in the document.
 *    Otherwise it causes space to appear at the top and bottom of elements
 *    that receive the `clearfix` class.
 * 2. The use of `table` rather than `block` is only necessary if using
 *    `:before` to contain the top-margins of child elements.
 */

.clearfix:before,
.clearfix:after {
    content: " "; /* 1 */
    display: table; /* 2 */
}

.clearfix:after {
    clear: both;
}

/*
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */

.clearfix {
    *zoom: 1;
}

/* ==========================================================================
   EXAMPLE Media Queries for Responsive Design.
   These examples override the primary ('mobile first') styles.
   Modify as content requires.
   ========================================================================== */

@media only screen and (min-width: 35em) {
    /* Style adjustments for viewports that meet the condition */
}

@media print,
(-o-min-device-pixel-ratio: 5/4),
(-webkit-min-device-pixel-ratio: 1.25),
(min-resolution: 120dpi) {
    /* Style adjustments for high resolution devices */
}

/* ==========================================================================
   Print styles.
   Inlined to avoid required HTTP connection: h5bp.com/r
   ========================================================================== */

@media print {
    * {
        background: transparent !important;
        color: #000 !important; /* Black prints faster: h5bp.com/s */
        box-shadow: none !important;
        text-shadow: none !important;
    }

    a,
    a:visited {
        text-decoration: underline;
    }

    a[href]:after {
        content: " (" attr(href) ")";
    }

    abbr[title]:after {
        content: " (" attr(title) ")";
    }

    /*
     * Don't show links for images, or javascript/internal links
     */

    .ir a:after,
    a[href^="javascript:"]:after,
    a[href^="#"]:after {
        content: "";
    }

    pre,
    blockquote {
        border: 1px solid #999;
        page-break-inside: avoid;
    }

    thead {
        display: table-header-group; /* h5bp.com/t */
    }

    tr,
    img {
        page-break-inside: avoid;
    }

    img {
        max-width: 100% !important;
    }

    @page {
        margin: 0.5cm;
    }

    p,
    h2,
    h3 {
        orphans: 3;
        widows: 3;
    }

    h2,
    h3 {
        page-break-after: avoid;
    }
}

.ajax-load {
    padding: 10px 0;
    width: 100%;
}

.table-scrollable {
    width: 100%;
    overflow-x: auto;
    overflow-y: hidden;
    margin: 10px 0 !important;
}
.col-lg-3 {
    width: 23%;
}

.profile-user-img {
    margin: 0 auto;
    width: 150px;
    padding: 3px;
    border: 3px solid #d2d6de;
}
</style>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">ITGK Ranking System</div>
            <div class="panel-body">
                <form name="frmfeepayment" id="frmfeepayment" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
							<div id="errorBox"></div>
                        </div>        
                        
							<div class="col-sm-4 form-group">     
								<label for="batch"> Select Batch:</label>
								<select id="ddlBatch" name="ddlBatch" class="form-control">

								</select>									
							</div> 
                    
                        <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary"
								value="Initialize Batch" style="display:none; margin-top:25px;"/>    
                    </div>
			 <div class="container">		
				<div id="parameters" style="display:none;">	
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-teal hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">playlist_add_check</i>
                                </div>
								
                                <div class="content totaldetails" id="admission">
                                    <div style="margin-top:25px;" class="text">Number of Admissions</div>
                                   
                                </div>
                            </div>
                    </div>
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-purple hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">hourglass_empty</i>
                                </div>
                                <div class="content totaldetails" id="learnersenroll">
                                    <div style="margin-top:15px;" class="text">Biometric Learner Enrollment </div>
								</div>
                            </div>
                    </div>						
					
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-blue-grey hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">flight_takeoff</i>
                                </div>
                                <div class="content totaldetails" id="attendance">
                                    <div style="margin-top:18px;" class="text">Learners Unique Attendance</div>
								</div>
                            </div>
                    </div>
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-light-green hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">content_paste</i>
                                </div>
                                <div class="content totaldetails" id="blockunblock">
                                    <div style="margin-top:16px;" class="text">Number of times ITGK Blocked</div>
								</div>
                            </div>
                    </div>
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-orange hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content totaldetails" id="deregister">
                                    <div style="margin-top:16px;" class="text">Learner De-Register Request</div>
								</div>
                            </div>
                    </div>
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-lime hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content totaldetails" id="result">
                                    <div style="margin-top:16px;" class="text">RS-CIT Results</div>
								</div>
                            </div>
                    </div>
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-brown hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content totaldetails" id="infra">
                                    <div style="margin-top:16px;" class="text">Computing Resource</div>
								</div>
                            </div>
                    </div>
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-teal hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content totaldetails" id="dpofeedback">
                                    <div style="margin-top:16px;" class="text">DPO Visit Feedback</div>
								</div>
                            </div>
                    </div>
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-pink hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content totaldetails" id="learnerfeedback">
                                    <div style="margin-top:16px;" class="text">Learner Feedback</div>
								</div>
                            </div>
                    </div>
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-teal hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content totaldetails" id="ilearnmarks">
                                    <div style="margin-top:16px;" class="text">iLearn Marks</div>
								</div>
                            </div>
                    </div>
					
				<!--	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-deep-purple hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content totaldetails" id="digitalcertificate">
                                    <div style="margin-top:16px;" class="text">Digital Certificate</div>
								</div>
                            </div>
                    </div>
					
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-lime hover-zoom-effect">
                                <div class="icon">
                                    <i class="material-icons">assignment_turned_in</i>
                                </div>
                                <div class="content totaldetails" id="supporttickets">
                                    <div style="margin-top:16px;" class="text">Support Tickets</div>
								</div>
                            </div>
                    </div>	-->				
					
				</div>
            </div>
			<div class="container">
				 <input type="button" name="btnfinalSubmit" id="btnfinalSubmit" class="btn btn-primary"
								value="Final Update" style="display:none; margin-left:8px;"/> 
			</div>
			
			</div>
        </div>   
    </div>
</form>
</div>

	<div id="show_processing" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
          <div style="margin-top: 100px;"><center><img src="images/double-ring-spinner.gif"></center></div>
        </div>
    </div>
	
<!-- Modal -->
<div id="myModalimage" class="modal" style="padding-top:50px !important">
  <div class="modal-content" style="width: 90%;">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h6>Exam Event</h6>
    </div>
      <div class="modal-body" style="max-height: 400px; overflow-y: scroll; text-align: center;">
		<div class="container">
			<div id="responses"></div>
		</div>
        <div class="container" style="align:center;">
			<div class="col-sm-4 form-group"> 
				<label for="designation">Select Exam Event:<span class="star">*</span></label>
				<select id="ddlExamEvent" name="ddlExamEvent" class="form-control">                                  
				</select>
			</div>
		
			<div class="col-md-2 form-group" style="margin-top:25px;">
                    <input type="button" name="Submit" id="Submit" class="btn btn-primary" 
						value="Submit" required="true"/>  
					 <input type="button" name="cancel" id="cancel" class="btn btn-primary" value="Cancel"/>    
								
            </div> 
		</div>
		

    </div>
  </div>
</div>

<!-- Modal -->
<div id="myModalimages" class="modal" style="padding-top:50px !important">
  <div class="modal-content" style="width: 90%;">
    <div class="modal-header">
      <span class="close closed">&times;</span>
      <h6>Select Exam Event</h6>
    </div>
      <div class="modal-body" style="max-height: 400px; overflow-y: scroll; text-align: center;">
		<div class="container">
			<div id="responsess"></div>
		</div>
        <div class="container" style="align:center;">
			<div class="col-sm-4 form-group"> 
				<label for="designation">Select Exam Event:<span class="star">*</span></label>
				<select id="ddlExamEvents" name="ddlExamEvents" class="form-control">                                  
				</select>
			</div>
		
			<div class="col-md-2 form-group" style="margin-top:25px;">
                    <input type="button" name="DigitalSubmit" id="DigitalSubmit" class="btn btn-primary" 
						value="Submit" required="true"/>  
					 <input type="button" name="Digitalcancel" id="Digitalcancel" class="btn btn-primary" value="Cancel"/>    
								
            </div> 
		</div>
		

    </div>
  </div>
</div>
	  
</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<style>
    .error{
        color:#F00;
    }
</style>
	
<script type="text/javascript">
	var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
			
			$("#cancel").click(function (){
					var modal = document.getElementById('myModalimage');
					modal.style.display = "none";
				});
				
				$("#Digitalcancel").click(function (){
					var modal = document.getElementById('myModalimages');
					modal.style.display = "none";
				});
				
			function FillBatch() {            
				$.ajax({
					type: "post",
					url: "common/cfItgkRating.php",
					data: "action=FillBatch",
					success: function (data) {						
						$("#ddlBatch").html(data);
					}
				});
			}
			FillBatch();
			
			$("#ddlBatch").change(function () {
				var BatchCode = $('#ddlBatch').val();
					if(BatchCode=='0'){
						$("#btnSubmit").hide();
						$("#btnfinalSubmit").hide();
						$("#parameters").hide();
					}
					else{
						$("#btnSubmit").show();						
					}				
			});
			   
			$("#btnSubmit").click(function () {
				 $("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();					
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=CheckBatchstatus&batchcode=" + ddlBatch.value + "",
						success: function (data) {							
							$('div[id=loader-wrapper]').remove();
							$("#parameters").show();
							$("#btnfinalSubmit").show();
						}
					});	
			});
			
			$("#btnfinalSubmit").click(function () {
				 $("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnfinalSubmit').hide();					
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=UpdateFinalRating&batchcode=" + ddlBatch.value + "",
						success: function (data) {
							$('div[id=loader-wrapper]').remove();							
						}
					});	
			});
			
			$("#admission").click(function () {				
				$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=updateadmissionwiserank&batchcode=" + ddlBatch.value + "",
						success: function (data) {
							$('div[id=loader-wrapper]').remove();
							//$("#parameters").show();							
						}
					});	
			});	
			
			$("#learnersenroll").click(function () {				
				$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=updatelearnersenrollwiserank&batchcode=" + ddlBatch.value + "",
						success: function (data) {							
							$('div[id=loader-wrapper]').remove();
							//$("#parameters").show();							
						}
					});	
			});	
			
			$("#attendance").click(function () {				
				$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=updateattendancewiserank&batchcode=" + ddlBatch.value + "",
						success: function (data) {							
							$('div[id=loader-wrapper]').remove();
							//$("#parameters").show();							
						}
					});	
			});	
			
			$("#blockunblock").click(function () {				
				$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=updateblockunblockwiserank&batchcode=" + ddlBatch.value + "",
						success: function (data) {							
							$('div[id=loader-wrapper]').remove();
							//$("#parameters").show();							
						}
					});	
			});	
			
							
			$("#digitalcertificate").click(function () {				
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=FillExamEvent",
						success: function (data) {
							$("#ddlExamEvents").html(data);
						}
					});
					var modal = document.getElementById('myModalimages');
					var span = document.getElementsByClassName("closed")[0];
							modal.style.display = "block";
							span.onclick = function() { 
								modal.style.display = "none";
							}	
			});	
			
			$("#result").click(function () {
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=FillExamEvent",
						success: function (data) {
							$("#ddlExamEvent").html(data);
						}
					});
					var modal = document.getElementById('myModalimage');
					var span = document.getElementsByClassName("close")[0];
							modal.style.display = "block";
							span.onclick = function() { 
								modal.style.display = "none";
							}
				
			});	
			
			$("#Submit").click(function (){
				$('#responses').empty();
				$('#responses').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				var event=$( "#ddlExamEvent" ).val();
					if (event==''){
						alert("Please select Exam Event");
						$('#responses').empty();
					}
					else{
						$('#Submit').hide();
							$.ajax({
								type: "post",
								url: "common/cfItgkRating.php",
								data: "action=updateresultwiserank&batchcode=" + ddlBatch.value + "&examevent=" + event +"",
								success: function (data) {							
									$('#responses').empty();
									var modal = document.getElementById('myModalimage');
									modal.style.display = "none";							
								}
							});
					}					
			});
			
			$("#DigitalSubmit").click(function (){
				$('#responsess').empty();
				$('#responsess').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				var event=$( "#ddlExamEvents" ).val();
					if (event==''){
						alert("Please select Exam Event");
						$('#responsess').empty();
					}
					else{
						$('#DigitalSubmit').hide();
						$.ajax({
							type: "post",
							url: "common/cfItgkRating.php",
					data: "action=updatedigitalcertificatewiserank&batchcode=" + ddlBatch.value + "&examevent=" + event +"",
							success: function (data) {							
								$('#responsess').empty();
									var modal = document.getElementById('myModalimages');
									modal.style.display = "none";							
							}
						});	
							
					}					
			});
			
			$("#infra").click(function () {				
				$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=updateinfrawiserank&batchcode=" + ddlBatch.value + "",
						success: function (data) {							
							$('div[id=loader-wrapper]').remove();
							//$("#parameters").show();							
						}
					});	
			});	
			
			$("#dpofeedback").click(function () {				
				$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=updatedpofeedbackwiserank&batchcode=" + ddlBatch.value + "",
						success: function (data) {							
							$('div[id=loader-wrapper]').remove();
							//$("#parameters").show();							
						}
					});	
			});	
			
			$("#learnerfeedback").click(function () {				
				$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=updatelearnerfeedbackwiserank&batchcode=" + ddlBatch.value + "",
						success: function (data) {							
							$('div[id=loader-wrapper]').remove();
							//$("#parameters").show();							
						}
					});	
			});
			
			$("#ilearnmarks").click(function () {				
				$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=updateilearnmarkswiserank&batchcode=" + ddlBatch.value + "",
						success: function (data) {							
							$('div[id=loader-wrapper]').remove();
							//$("#parameters").show();							
						}
					});	
			});	
			
			$("#deregister").click(function () {				
				$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=updatederegisterwiserank&batchcode=" + ddlBatch.value + "",
						success: function (data) {							
							$('div[id=loader-wrapper]').remove();
							//$("#parameters").show();							
						}
					});	
			});	
			
			$("#supporttickets").click(function () {				
				$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
					$('#btnSubmit').hide();
					$.ajax({
						type: "post",
						url: "common/cfItgkRating.php",
						data: "action=updatesupportticketswiserank&batchcode=" + ddlBatch.value + "",
						success: function (data) {							
							$('div[id=loader-wrapper]').remove();
							//$("#parameters").show();							
						}
					});	
			});	
			
			
        });
    </script>
</html>