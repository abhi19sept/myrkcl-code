<?php
$title="Edit Bank Account Details For Share Disbursement";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var BankAccountCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var BankAccountCode=0</script>";
echo "<script>var Mode='UPDATE'</script>";
$_REQUEST['Mode']='UPDATE';
}
?>
	
<div style="min-height:430px !important;max-height:auto !important;">
	<div class="container"> 
		
        <div class="panel panel-primary" style="margin-top:36px !important;">  
			<div class="panel-heading">Edit Bank Account Details </div>
			<div class="panel-body">
				<form name="frmfillbankaccount" id="frmfillbankaccount" class="form-inline" role="form"  >
				
					<div class="container" style="width:1100px;">
						<div class="container">
							<div id="response"></div>
						</div>        
							<div id="errorBox"></div>				
							<div class="container">
							<?php if($_REQUEST['Mode'] == 'Edit') { ?>
								<input type="button" name="btnshowotp" id="btnshowotp" class="btn btn-primary" value="Generate OTP" />
							<?php } else { ?>
								
							<?php } ?>
								
                            </div>
							
							<div class="panel panel-info" id="otpverify" style="display:none;">
                        <div class="panel-heading">Verify Authentication Details</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="email">Enter Email:</label>
                                    <input type="text" class="form-control" maxlength="50" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="Mobile">Enter Mobile Number:</label>
                                    <input type="text" class="form-control" maxlength="10" name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <label for="otp">Enter OTP:</label>
                                    <input type="text" class="form-control" maxlength="6" name="txtOtp" id="txtOtp"  placeholder="Enter OTP">
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <input type="button" name="btnVerify" id="btnVerify" class="btn btn-primary" value="Verify" style="margin-top:25px"/>
                                    <!--<input type="button" name="btnVerified" id="btnVerified" class="btn btn-primary" value="Details Verified" style="margin-top:25px; display:none"/>-->
                                    <label for="tnc" id="btnVerified" style="margin-top:25px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Details Verified</b></label></br>
                                </div>
                            </div>
                        </div>
                    </div>
						
								
							</div>
							
							
							<div id="details" style="display:none">
							
							<div class="container">
							
							<div class="col-sm-4 form-group" >     
									<label for="order">IFSC Code:<font color="red">*</font></label>
									<input type="text" class="form-control" maxlength="11" name="txtIfscCode" id="txtIfscCode" placeholder="IFSC Code" onkeypress="javascript:return validAddress(event);">
									
									</div> 


							</div>
							
							<div class="container">
							
							
							
							<div class="col-sm-4 form-group">     
									<label for="dob">Account Holder Name:<font color="red">*</font></label>
									<input type="hidden" class="form-control" maxlength="100" name="txtGenerateId" id="txtGenerateId"/>	
									<input type="hidden" class="form-control" name="ifscstatus" id="ifscstatus"/>
									<input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
								<input type="hidden" class="form-control"  name="txtsign" id="txtsign"/>
									<input type="text" class="form-control" maxlength="100" name="txtaccountName" id="txtaccountName"  placeholder="Account Name" onkeypress="javascript:return allowchar(event);" style="text-transform:uppercase"/>
								</div>
								
								<div class="col-sm-4 form-group">     
									<label for="dob">Account Number:<font color="red">*</font></label>
									
									<input type="text" class="form-control" maxlength="20" name="txtaccountNumber" id="txtaccountNumber"  placeholder="Account Number" onpaste="return false;" onkeypress="javascript:return allownumbers(event);"/>
								</div>
								
								
								<div class="col-sm-4 form-group" >     
									<label for="dob">Confirm Account Number:<font color="red" >*</font></label>
									
									<input type="text" class="form-control" maxlength="20" name="txtConfirmaccountNumber" id="txtConfirmaccountNumber" onpaste="return false;"  placeholder="Account Number"  onkeypress="javascript:return allownumbers(event);"/>
									<span id='message'></span>
								</div>
								
								
								
								<div class="col-sm-5 form-group"> 
									<label for="status">Bank Name:</label>
									
									<input type="text" class="form-control" maxlength="500" name="ddlBankName" id="ddlBankName" placeholder="Bank Name" onpaste="return false;"  onkeypress="javascript:return validAddress(event);">
								</div>                            
							
							</div>
								
								
							
							<div class="container">
							   <div class="col-sm-4 form-group"> 
									<label for="status">Branch Name:</label>
									<input type="text" class="form-control" maxlength="500" name="ddlBranch" id="ddlBranch" placeholder="Branch Name" onpaste="return false;"  onkeypress="javascript:return validAddress(event);">
								</div>   
							
								<div class="col-sm-4 form-group">     
									<label for="order">MICR Code:</label>
									<input type="text" class="form-control" maxlength="14" name="txtMicrcode" id="txtMicrcode" placeholder="MICR Code" onpaste="return false;"  onkeypress="javascript:return validAddress(event);">
								</div>  
								
								<div class="col-sm-4 form-group">     
									<label for="gender">Account Type:<font color="red">*</font></label> <br/>                               
									<label class="radio-inline"> <input type="radio" id="rbtaccountType_saving" name="rbtaccountType_saving" checked="checked" value="Savings"/> Savings </label>
									<label class="radio-inline"> <input type="radio" id="rbtaccountType_saving" name="rbtaccountType_saving" value="Current"/> Current </label>									
								</div>
								
								<div class="col-sm-4 form-group"> 
									<label for="status">Pan Card No.:<font color="red">*</font></label>
									<input type="text" class="form-control" maxlength="11" name="txtpanno" id="txtpanno"
										placeholder="Pan Card No." onpaste="return false;" onkeypress="javascript:return validAddress(event);">
								</div>  

							</div>
							
							<div class="container">
								
								<div class="col-sm-4 form-group"> 
									<label for="status">Pan Card Holder Name:</label>
									<input type="text" class="form-control" maxlength="100" name="txtpanname" id="txtpanname"
										placeholder="Pan Card Holder Name" onkeypress="javascript:return allowchar(event);" style="text-transform:uppercase">
								</div>  
								
								<div class="col-sm-4 form-group"> 
								  <label for="photo">Attach Pan Card Copy:<span class="star">*</span></label>
								  <input type="file" class="form-control" id="pancard" name="pancard"  onchange="pancard(this)" >
								   <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =200KB</span>
								   
								   <img id="uploadPreview1" src="images/Pencard.jpg" id="uploadPreview1" name="filePhoto" width="50px"  onclick="javascript:document.getElementById('uploadImage1').click(); " onchange="checkPhoto(this);PreviewImage(1)">
								</div> 

								<div class="col-sm-4 form-group"> 
									<label for="edistrict">Select Bank Account ID Proof:<span class="star">*</span></label>
									<select id="ddlidproof" name="ddlidproof" class="form-control">
									    <option value=""> Please Select </option>
										<option value="cheque"> Cancelled Cheque Copy </option>
										<option value="welcome"> Welcome Letter of Bank </option>
										<option value="passbook"> Passbook Copy </option>
									</select>    
								</div>	
							  
							  <div class="col-sm-4 form-group"> 
								  <label for="photo">Attach Bank Id Proof:<span class="star">*</span></label>
								  <input type="file" class="form-control" id="chequeImage" name="chequeImage"   onchange="BankIdProof(this)">
								   <span style="font-size:10px;">Note : JPG,JPEG Allowed Max Size =200KB</span>
								  
								   
								   
								   
								   <img id="uploadPreview2" src="images/sbi.jpg" id="uploadPreview2" name="filePhoto" width="80px" height="35px" onclick="javascript:document.getElementById('uploadImage2').click();">			
								</div>   
     
     
</div>
							
							
							
							<div class="container"> 
								
							<div class="col-md-11"> 	
															
								<label for="learnercode" style="float:left;margin:left:15px" ><b>मैं यह स्वीकार करता हूँ कि मैं आई. टी. ज्ञान केंद्र  का Proprietor / Partner / Director / President / Chairman / Secretary / Karta / Trustee हूँ एवं मेरे द्वारा प्रवेशित Bank Account Details मेरी अथवा मेरी Firm / साझेदारी Firm / Company / Society / Trust / HUF / संस्था की ही है |  मैं यह भी स्वीकार करता हूँ कि RKCL अथवा RKCL-Escrow Account द्वारा आई. टी. ज्ञान केंद्र को किसी भी प्रकार का भुगतान उक्त बैंक खाते में किया जायेगा अतः प्रवेशित Bank Account Details में किसी भी तरह की गलती के लिए मैं स्वयं जिमेदार रहूँगा |</b></label></br>                              
								
                            </div>
							
					
						</div>	
						
						<div class="container"> 
								<div class="col-md-11" > 
								<label class="checkbox-inline" style="float:left;"> <span class="star"> </span><input type="checkbox" name="chk" id="chk" value="1" >
									I Accept 
								</label>
								</div>
							<br>
							<br>
							
					
						    </div>
						<div class="container" id="sub" style="">
							  <div class="col-md-1" style="width:100px;"> 							
								                              
								<a marked="1" title="" style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModal"><span id="fix"><input name="Help" id="Help" class="btn btn-primary" value="Help" style="margin-top:10px" type="button"></span></a>    					
                            </div>
							  
							  <div class="col-md-1" style="width:100px;">
							  <input name="validate" id="preview" class="btn btn-primary" value="Verify" style="margin-left:18px;margin-top:10px" type="submit"> 
							  </div>
                               
                        </div>
							
							<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
						  <div class="modal-content">
							<div class="modal-header">
								<button class="close" type="button" data-dismiss="modal">×</button>
								<h3 class="modal-title">Terms and conditions </h3>
							</div>
							<div class="modal-body">
								<img src="images/1.jpg" width="620" />
							</div>
							<div class="modal-footer">
								<button class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						   </div>
						  </div>
						</div>
							</div>
							
							
	
							  <div class="container" id="sub" style="display:none">
							  
							  
							  <input type="submit" name="validate" id="preview" class="btn btn-primary" value="Verify" style="margin-left:18px"/> 
                               
                        </div>
						
						
						
						<div class="container" id='prev' style="display:none">
						
						<div class='col-md-11 col-lg-11'>
						<table class='table table-user-information'>
						<tbody>
						<tr>
						<td class="col-md-2 col-lg-2">IFSC Code:</td>
						<td class="col-md-3 col-lg-3" id='ifsccode'></td>
						</tr>
						<tr>
						<td  class="col-md-2 col-lg-2">Account Holder Name:</td>
						<td class="col-md-3 col-lg-3" id='Accname'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Account Number:</td>
						<td class="col-md-3 col-lg-3" id='Accnumber'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Confirm Account Number:</td>
						<td class="col-md-3 col-lg-3" id='ConAccnumber'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Bank Name:</td>
					   <td class="col-md-3 col-lg-3" id='BankName'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">Branch Name:</td>
						<td class="col-md-3 col-lg-3" id='BranchName'></td>
						</tr>
						<tr>
						<td class="col-md-2 col-lg-2">MICR Code:</td>
						<td class="col-md-3 col-lg-3" id='Micrcode'></td>
						</tr>
						
						<tr>
						<td class="col-md-2 col-lg-2">Account Type:</td>
						<td class="col-md-3 col-lg-3" id='Accounttype'></td>
						</tr>
						
						
						<tr>
						<td class="col-md-2 col-lg-2">Pan Card No:</td>
						<td class="col-md-3 col-lg-3" id='Pencard'></td>
						</tr>
						
						
						<tr>
						<td class="col-md-2 col-lg-2">Pan Card Holder Name:</td>
						<td class="col-md-3 col-lg-3" id='Pancardholder'></td>
						</tr>
						
						
						<tr>
						<td class="col-md-2 col-lg-2">Bank Account ID Proof:</td>
						<td class="col-md-3 col-lg-3" id='bankid'></td>
						</tr>
						
						

						 
						</tbody>
					   </table>
					   <input type="button" name="back" id="back" class="btn btn-primary" value="back"  /> 
					   <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="margin-left:15px"/>
						</div>
						  
						
						</div>
						
						
					</div>
                </div>
            </div>   
        </div>
    </form>
</div>
</body>

<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
<script type="text/javascript">
	function PreviewImage(no) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
		oFReader.onload = function (oFREvent) {
			document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
		};
	};
</script>
<script>
$("#preview").click(function () {
								var accountno = $("#txtaccountNumber").val();
								var confirmaccountno = $("#txtConfirmaccountNumber").val();
								
								if (accountno != confirmaccountno) {
									alert("Account No Does not match.");
									return false;
								}
								if ($("#frmfillbankaccount").valid())
								{
									//alert(1);
								
								
								document.getElementById("ifsccode").innerHTML = $("#txtIfscCode").val();
								document.getElementById("Accname").innerHTML = $("#txtaccountName").val();
								document.getElementById("Accnumber").innerHTML= $("#txtaccountNumber").val();
								document.getElementById("ConAccnumber").innerHTML = $("#txtConfirmaccountNumber").val();
								document.getElementById("BankName").innerHTML = $("#ddlBankName").val();
								document.getElementById("BranchName").innerHTML= $("#ddlBranch").val();
								document.getElementById("Micrcode").innerHTML = $("#txtMicrcode").val();
								document.getElementById("Accounttype").innerHTML = $('input[name=rbtaccountType_saving]:checked').val();
								document.getElementById("Pencard").innerHTML = $("#txtpanno").val();
								document.getElementById("Pancardholder").innerHTML= $("#txtpanname").val();
								document.getElementById("bankid").innerHTML = $("#ddlidproof").val();
								
								
                                            $("#prev").show();
											$("#details").hide();
											$("#sub").hide();
											$("#gird").hide();
											$("#ifsc").hide();
											
									        		 

								} 
                               return false;  
                                });
								
								
								
</script>
<script src="scripts/uploadbankaccount.js"></script>


<style>
  .modal-dialog {width:700px;}
.thumbnail {margin-bottom:6px; width:800px;}
  </style>
<script type="text/javascript">
  
  $(document).ready(function() {
		jQuery(".fix").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>




<script>
$('#txtConfirmaccountNumber').on('keyup', function () {
    if ($(this).val() == $('#txtaccountNumber').val()) {
        $('#message').html('confirmed').css('color', 'green');
    } else $('#message').html('confirm account no. should be same as account no').css('color', 'red');
});
</script>



<script language="javascript" type="text/javascript">


function pancard(target) {
	var ext = $('#pancard').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("chequeImage").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			        
		alert("Image size should less or equal 200 KB");
		document.getElementById("chequeImage").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("chequeImage").value = '';
				return false;
	}    
    document.getElementById("chequeImage").innerHTML = "";
    return true;
}





function BankIdProof(target) {
	var ext = $('#chequeImage').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("chequeImage").value = '';
			return false;
		}

	if(target.files[0].size > 200000) {			        
		alert("Image size should less or equal 200 KB");
		document.getElementById("chequeImage").value = '';
        return false;
    }
	else if(target.files[0].size < 100000) {
				alert("Image size should be greater than 100 KB");
				document.getElementById("chequeImage").value = '';
				return false;
	}    
    document.getElementById("chequeImage").innerHTML = "";
    return true;
}





</script>
	
	<script type="text/javascript">
		var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
		var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
		var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
		var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
		$(document).ready(function () {
			
			
			
			
		
			
			
	      
			
			
			if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
			
			
			$("#btnshowotp").click(function () {
            alert("OTP has been sent to your Registered Mobile Number. Please Enter OTP to Proceed");
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfeditbankaccountdetails.php",
                data: "action=SENDOTP",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate) {

                        $("#btnshowotp").hide();
                       $("#otpverify").show(3000);
                       //$("#verification").show();

                    }
                    else {

                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   OTP already Generated, Please Try again." + "</span></p>");

                    }

                }
            });
        });
		
		
		
		
		
		$("#btnVerify").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfeditbankaccountdetails.php",
                data: "action=VERIFY&email=" + txtEmail.value + "&mobile=" + txtMobile.value + "&opt=" + txtOtp.value + "",
                success: function (data)
                {
                    //alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
						
						
                        $('#response').empty();
                        $("#btnVerify").hide();
                        $("#btnVerified").show();
						setTimeout(function(){ 
							$('#txtIfscCode').focus();
						}, 1000);
						 checkvarify();
                        
                       // $('#tnc').show(3000);
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                    }
                }
            });
        });
		
		
		
		
		
		
		
			
			function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfeditbankaccountdetails.php",
                    data: "action=EDIT&values=" + BankAccountCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Make']);
                        //alert(data);
                        data = $.parseJSON(data);
                       
                        txtaccountName.value = data[0].Bank_Account_Name;
                        txtaccountNumber.value = data[0].Bank_Account_Number;
                        rbtaccountType_saving.value = data[0].Bank_Account_Type;
                        txtIfscCode.value = data[0].Bank_Ifsc_code;
						ddlBankName.value = data[0].Bank_Name;
						txtMicrcode.value = data[0].Bank_Micr_Code;
						ddlBranch.value = data[0].Bank_Branch_Name;
						txtpanno.value = data[0].Pan_No;
						txtpanname.value = data[0].Pan_Name;
						//pancard.value = data[0].Pan_Document;
						//chequeImage.value = data[0].Bank_Document;
						txtphoto.value= data[0].photo;
						txtsign.value=data[0].sign;
						$("#uploadPreview1").attr('src',"upload/pancard/" + data[0].Pan_Document);
						$("#uploadPreview2").attr('src',"upload/bankdocs/" + data[0].Bank_Document);
						
						
						
                        
                    }
                });
            }
			
			
			
			
			$("#txtIfscCode").blur(function () {
								//alert(1);
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                               var url = "common/cfeditbankaccountdetails.php"; // the script where you handle the form input.
								var data;
								data = "action=IFSCCHEK&IFSC=" + txtIfscCode.value + ""; // serializes the form's elements.
								
                                    $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
										$('#response').empty();
                                            //alert(data);
											if(data==1)
											{
											    
											$("#ifscstatus").val(1);
												$('#response').empty();
											$('#response').append("<p class='state-success'><span><img src=images/correct.gif width=10px height=20px /></span><span><font color=green>" + "  Correct IFSC Code ." + "</font></span></p>");
											}
											else if(data==0)
											{
												$("#ifscstatus").val(0);
											$('#response').empty();
											$('#response').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + " Incorrect  IFSC Code ." + "</font></span></p>");
											
										    }
											else  
											{
												$("#ifscstatus").val(0);
												$('#response').empty();
											$('#response').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + "  Incorrect IFSC Code." + "</font></span></p>");
											
											}
                                    }
                                });
							
                                return false; // avoid to execute the actual submit of the form.
                            });
							///IFSCCHECK();
							
			
			
		
		    function FillStatus() {
		        $.ajax({
		            type: "post",
		            url: "common/cfeditbankaccountdetails.php",
		            data: "action=FILL",
		            success: function (data) {
		                $("#ddlBankName").html(data);
		            }
		        });
		    }
		
		    FillStatus();
			
			
			
			
			function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfeditbankaccountdetails.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						

                    }
                });
            }

            
			
			
			function GenerateUploadId()
                   {
					$.ajax({
						type: "post",
						url: "common/cfBlockUnblock.php",
						data: "action=GENERATEID",
						success: function (data) {                      
							txtGenerateId.value = data;					
						}
					});
                  }
                  GenerateUploadId();
			
			
			
			function FillBranch() {
		        $.ajax({
		            type: "post",
		            url: "common/cfeditbankaccountdetails.php",
		            data: "action=FILLBRANCH",
		            success: function (data) {
		                $("#ddlBranch").html(data);
		            }
		        });
		    }
		
		    FillBranch();
										
								
								
								$("#back").click(function () {
								
								
                                            $("#details").show();
											
											$("#prev").hide();
											
											$("#gird").show();
											$("#sub").show();
											$("#ifsc").show();
										
                                });
							
                                
			
			
			
			
			
				  function checkvarify()
				  {
					
							$('#response').empty();
							   
								$("#details").show();
								$("#btnShow").hide();
								$("#otpverify").hide();
								
								$("#sub").show();
								showData();		 

				  }
                               
			
			  $("#btnSubmit").click(function () {
				 
			var accountno = $("#txtaccountNumber").val();
            var confirmaccountno = $("#txtConfirmaccountNumber").val();
            
			if (accountno != confirmaccountno) {
                alert("Account No Does not match.");
                return false;
            }
            
			
			if ($("#frmfillbankaccount").valid())
            {
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfeditbankaccountdetails.php"; // the script where you handle the form input.
				var chequeimage=$('#chequeImage').val(); 
               var data;
                var forminput=$("#frmfillbankaccount").serialize();
				var a = document.getElementById('ifscstatus').value;
				//alert(a);
				if(a==0)
				{
					$('#response').empty();
											$('#response').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + " Incorrect  IFSC Code ." + "</font></span></p>");
					
				}
				else
			    {
					
					if (Mode == 'Add')
					{
						data = "action=ADD&chequeimage=" + chequeimage + "&" + forminput; // serializes the form's elements.
					}
					else
					{
						
						data = "action=UPDATE&code=" + BankAccountCode +"&" + forminput; // serializes the form's elements.
					}
					
					
					$.ajax({
						type: "POST",
						url: url,
						data: data,
						success: function (data)
						{
							if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
							{
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
								 window.setTimeout(function () {
									window.location.href = "frmeditbankaccountdetails.php";
								}, 1000);
								Mode = "Add";
								resetForm("frmeditbankaccountdetails");
							}
							else
							{
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
							}
							showData();


						}
					});
				}
			 }
                return false; // avoid to execute the actual submit of the form.
            });
		    function resetForm(formid) {
		        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
		    }
		
		});
		
	</script>
	
	<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmfillbankaccount_validation.js" type="text/javascript"></script>	
</html>