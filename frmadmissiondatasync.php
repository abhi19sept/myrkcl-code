<?php
$title="Modify Admission";
include ('header.php'); 
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
echo "<script>var Admission_Name=0</script>";
echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
	<div style="height:65% !important;">
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Admission Data Sync Process</div>
                <div class="panel-body">
                    <form name="frmprocess" id="frmprocess" class="form-inline" role="form" enctype="multipart/form-data">
						<div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
								
								
							<div class="col-sm-4 form-group">     
								<label for="course">Select Course:<span class="star">*</span></label>
								<select id="ddlCourse" name="ddlCourse" class="form-control">
								
								</select>
								
							</div> 
						</div>
						
						
							<div class="container">
								<div class="col-sm-4 form-group">     
								<label for="batch"> Select Batch:<span class="star">*</span></label>
								<select id="ddlBatch" name="ddlBatch" class="form-control">
										   
								</select>
                                  <input type="hidden" name="cname" id="cname" class="form-control" readonly="true"/>
								 <input type="hidden" name="bname" id="bname" class="form-control" readonly="true"/>								
							</div> 
						</div>
						
								<div class="container">
								   <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>
								</div>
                    </div>
            </div>   
        </div>
    </form>
	</div>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
         
<script type="text/javascript"> 
 $('#admission_date').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
	});  
	</script>
	
        <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {			
			
			
			function FillCourse() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfadmissiondatasync.php",
                    data: "action=FILLCourseName",
                    success: function (data) {
						//alert(data);
                        $("#ddlCourse").html(data);
                    }
                });
            }
            FillCourse();
			
			
			$("#ddlCourse").change(function(){
				var selCourse = $(this).val(); 
				//alert(selCourse);
				 $.ajax({
                    type: "post",
                    url: "common/cfadmissiondatasync.php",
                    data: "action=FILLBatchName&values=" + selCourse + "",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });				
           });
		   
		   
			
			function GetCourseName(val) {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfadmissiondatasync.php",
                    data: "action=FILLCourse&course="+ val +"",
                    success: function (data) {
						cname.value=data;						
                    }
                });
            }
			
			function GetBatchName(val) {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfadmissiondatasync.php",
                    data: "action=FILLBatch&batch="+ val +"",
                    success: function (data) {
						bname.value=data;						
                    }
                });
            }
                 
           
			 $("#ddlBatch").change(function(){
				 //FillBatchFee(this.value);
				 GetBatchName(this.value);
				 GetCourseName(ddlCourse.value);
            });
			
			
            $("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfadmissiondatasync.php"; // the script where you handle the form input.
                var data;
                var forminput=$("#frmprocess").serialize();
                //alert(forminput);
				
                if (Mode == 'Add')
                {
                    data = "action=ADD&" + forminput; // serializes the form's elements.
                }
                
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href="frmadmissiondatasync.php";
                            }, 1000);

                            Mode="Add";
                            resetForm("frmprocess");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
    </body>
    
</html>