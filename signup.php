<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Government Entry Form</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="bootcss/css/bootstrap.min.css">	
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="bootcss/js/bootstrap.min.js"></script>	
			
	<?php include ('header.php'); ?>	
    </head>
    <body>
        <?php
            
            
        if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
        ?>

        <div class="container"> 
			  
            <div class="panel panel-primary" style="margin-top:36px;">

                <div class="panel-heading">Sign up Application  Form</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
  <div class="row">
  	<div class="col-md-6">
    
          <form class="form-horizontal" action="" method="POST">
          <fieldset>
            <div id="legend">
              <legend class="">Register</legend>
            </div>
            <div class="control-group">
              <label class="control-label" for="username">Username</label>
              <div class="controls">
                <input id="username" name="username" placeholder="" class="form-control input-lg" type="text">
                <p class="help-block">Username can contain any letters or numbers, without spaces</p>
              </div>
            </div>
         
            <div class="control-group">
              <label class="control-label" for="email">E-mail</label>
              <div class="controls">
                <input id="email" name="email" placeholder="" class="form-control input-lg" type="email">
                <p class="help-block">Please provide your E-mail</p>
              </div>
            </div>
         
            <div class="control-group">
              <label class="control-label" for="password">Password</label>
              <div class="controls">
                <input id="password" name="password" placeholder="" class="form-control input-lg" type="password">
                <p class="help-block">Password should be at least 6 characters</p>
              </div>
            </div>
         
            <div class="control-group">
              <label class="control-label" for="password_confirm">Password (Confirm)</label>
              <div class="controls">
                <input id="password_confirm" name="password_confirm" placeholder="" class="form-control input-lg" type="password">
                <p class="help-block">Please confirm password</p>
              </div>
            </div>
         
            <div class="control-group">
              <!-- Button -->
              <div class="controls">
                <button class="btn btn-success">Register</button>
              </div>
            </div>
          </fieldset>
        </form>
    
    </div> 
  </div>
</div>

                       
                </div>
            </div>   
        </div>


    </form>




</body>
<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function FillEmpDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlempdistrict").html(data);
                }
            });
        }
        FillEmpDistrict();

        function FillEmpDepartment() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLEMPDEPARTMENT",
                success: function (data) {
                    $("#ddlDeptName").html(data);
                }
            });
        }
        FillEmpDepartment();

        function FillEmpDesignation() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLEMPDESIGNATION",
                success: function (data) {
                    $("#ddlEmpDesignation").html(data);
                }
            });
        }
        FillEmpDesignation();

        function FillBankDistrict() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKDISTRICT",
                success: function (data) {
                    $("#ddlBankDistrict").html(data);
                }
            });
        }
        FillBankDistrict();

		$("#ddlBankDistrict").change(function(){
			FillBankName(this.value);
		});
        function FillBankName(districtid) {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FILLBANKNAME&districtid=" + districtid,
                success: function (data) {
                    $("#ddlBankName").html(data);

                }
            });
        }
        

        function FillDobProof() {
            $.ajax({
                type: "post",
                url: "common/cfgoventryform.php",
                data: "action=FillDobProof",
                success: function (data) {
                    $("#ddlDobProof").html(data);

                }
            });
        }
        FillDobProof();



        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=DELETE&values=" + StatusCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=EDIT&values=" + StatusCode + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    txtStatusName.value = data[0].StatusName;
                    txtStatusDescription.value = data[0].StatusDescription;
                    //alert($.parseJSON(data)[0]['StatusName']);
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfStatusMaster.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);

                }
            });
        }

        //showData();		
		
        $("#btnSubmit").click(function () {
			
			$("#form").valid();
			$('input[type=file]').fileValidator({
			onInvalid:    function(type, file){ $(this).val(null); },
			type:        'image'
			});
     
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfgoventryform.php"; // the script where you handle the form input.
            var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&lcode=" + txtLCode.value + "&empname=" + txtEmpName.value + "&faname="+ txtFaName.value + 
					   "&empdistrict="+ ddlempdistrict.value + "&empmobile="+ txtEmpMobile.value + "&empemail="+ txtEmail.value + 
					   "&empaddress="+ txtEmpAddress.value + "&empdeptname="+ ddlDeptName.value + "&empid="+ txtEmpId.value + 
					   "&empgpfno="+ txtGpfNo.value + "&empdesignation="+ ddlEmpDesignation.value + "&empmarks="+ txtEmpMarks.value + 
					   "&empexamattempt="+ ddlExamattepmt.value + "&emppan="+ txtPanNo.value + "&empbankaccount="+ txtBankAccount.value + 
					   "&empbankdistrict="+ ddlBankDistrict.value + "&empbankname="+ ddlBankName.value + "&empbranchname="+ txtBranchName.value +
					   "&empifscno="+ txtIfscNo.value + "&empmicrno="+ txtMicrNo.value + "&filereceipt="+ fileReceipt.value + 
					   "&filebirth="+ fileBirth.value + "&empdobproof="+ ddlDobProof.value + ""; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + StatusCode + "&name=" + txtStatusName.value + "&description=" + txtStatusDescription.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmstatusmaster.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmStatusMaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmgoventry_validation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>
<?php include ('footer.php'); ?>
</html>