<?php
$title = "WCD Allocate Learner Batch Time Count Report";
include ('header.php');
include ('root_menu.php');
echo "<script>var Role='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">WCD Allocate Learner Batch Time Count Report</div>
            <div class="panel-body">

                <form name="frmwcdbatchcount" id="frmwcdbatchcount" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
								  <select id="ddlCourse" name="ddlCourse" class="form-control" required="true">
								   </select>
                             </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control" required="true">
                                </select>
                            </div> 
							
							<div class="col-md-4 form-group" id="dist" style="display:none;">     
                                <label for="dist"> Select District:<span class="star">*</span></label>
                                <select id="ddlDistrict" name="ddlDistrict" class="form-control" required="true">
                                </select>
                            </div> 
						
                        </div>

						<div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		
		
		function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfWcdBatchTimeCountReport.php",
                data: "action=FillCourse",
                success: function (data) {
					$("#ddlCourse").html(data);
                }
            });

        }
	FillCourse();

        $("#ddlCourse").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfWcdBatchTimeCountReport.php",
                data: "action=FILLBatchName&values=" + ddlCourse.value + "",
                success: function (data) {
					$("#ddlBatch").html(data);					
                }
            });

        });
		
		$("#ddlBatch").change(function () {
			if(Role=='1' || Role=='4' || Role=='16' || Role=='11'){
							$("#dist").show();
							filldistrict();
						}
						else{
							$("#dist").hide();
						}
        });
		
	function filldistrict() {
            $.ajax({
                type: "post",
                url: "common/cfWcdBatchTimeCountReport.php",
                data: "action=FillDistrict",
                success: function (data) {
					$("#ddlDistrict").html(data);
                }
            });

        }
	
        function showData() {
			if (ddlCourse.value == '') {
                alert('Please select a course.');
                return;
            }
			if (ddlBatch.value == '') {
                alert('Please select a batch.');
                return;
            }
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdBatchTimeCountReport.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=GetBatchTimeCount&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Batch." + "</span></div>");
                            
						}
					else {
						
                    $('#response').empty();
						if(Role=='14'){
							window.open(data, '_blank');
						}
						else{
							$("#grid").html(data);
							$('#example').DataTable({
								dom: 'Bfrtip',
								buttons: [
									'copy', 'csv', 'excel', 'pdf', 'print'
								]
							});
						}
					}

                }
            });
        }
		
		function showDataAdmin() {
			if (ddlCourse.value == '') {
                alert('Please select a course.');
                return;
            }
			if (ddlBatch.value == '') {
                alert('Please select a batch.');
                return;
            }
			if (ddlDistrict.value == '') {
                alert('Please select District.');
                return;
            }
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdBatchTimeCountReport.php"; // the script where you handle the form input.
            //var batchvalue = $("#ddlBatch").val();
            data = "action=GetBatchTimeCountForAdmin&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "&district=" + ddlDistrict.value + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Batch." + "</span></div>");
                            
						}
					else {
                    $('#response').empty();
                    window.open(data, '_blank');
				}

                }
            });
        }

        $("#btnSubmit").click(function () {
			if(Role=='1' || Role=='4' || Role=='16' || Role=='11'){
				showDataAdmin();
			}else{
				showData();
			}           
          return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
