<?php
$title = "Update Location Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Update'</script>";
}
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '8') {
    
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Update Location Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmUpdateLocationDetails" id="frmUpdateLocationDetails" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="cc">Enter Center Code:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="javascript:return allownumbers(event);" name="txtcc" id="txtcc" placeholder="Enter Center Code">
                            <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        </div>

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                        </div>
                    </div>


                    <div id="main-content" style="display:none">
                        <div class="panel panel-info" id="impreg">
                            <div class="panel-heading">Registerede IT-GK Details</div>
                            <div class="panel-body">

                                <div class="container">
                                    <div class="col-sm-8"> 
                                        <label for="edistrict">IT-GK Name:<span class="star">*</span></label>
                                        <input type="text" maxlength="10" class="form-control" name="txtCenterName" id="txtCenterName" readonly="true"/>

                                    </div>
                                </div>
                                <br>
                                <div class="container">
                                    <div class="col-sm-4"> 
                                        <label for="edistrict">Current Tehsil:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtTehsilName" id="txtTehsilName" readonly="true"/>

                                    </div>;
                                    <div class="col-sm-4"> 
                                        <label for="edistrict">Requested Tehsil:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtTehsilName_New" id="txtTehsilName_New" readonly="true"/>

                                    </div>
                                </div>
                                <br>
                                <div class="container">
                                    <div class="col-sm-4"> 
                                        <label for="edistrict">Current Area Type:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtAreaType" id="txtAreaType" readonly="true"/>
                                    </div>

                                    <div class="col-sm-4"> 
                                        <label for="edistrict">Requested Area Type:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtAreaType_New" id="txtAreaType_New" readonly="true"/>
                                    </div>
                                </div>
                                <br>
                                <div class="container">
                                    <div class="col-sm-4"> 
                                        <label for="edistrict">NcrAmount:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtNcrAmt" id="txtNcrAmt" readonly="true"/>

                                    </div>
                                    <div class="col-sm-4"> 
                                        <label for="edistrict">Eligiblity:<span class="star">*</span></label><br>
                                        <label for="yes" id="eligible" style=" display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Eligible for Amount Update</b></label>
                                        <label for="no" id="noteligible" style=" display:none; color:red; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Not Eligible for Amount Update</b></label></br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container" id="submitdiv" style="display:none;">

                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Click here to Update Details"/>    
                        </div>
                    </div>
                </form>
            </div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            if (txtcc.value == '') {
                BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>&nbsp; Please Enter Center Code to Proceed.</span>");
            }
            $.ajax({
                type: "post",
                url: "common/cfAddressChangeAmtUpdate.php",
                data: "action=DETAILS&values=" + txtcc.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Center Code" + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        txtCenterName.value = data[0].orgname;
                        txtTehsilName.value = data[0].tehsilname;
                        txtAreaType.value = data[0].areatype;

                        txtNcrAmt.value = data[0].ncramt;
                        txtAreaType_New.value = data[0].areatype_new;
                        txtTehsilName_New.value = data[0].tehsilname_new;
                        
                        if(txtNcrAmt.value < '38000'){
                            $("#eligible").show();
                            $("#submitdiv").show();
                        } else {
                            $("#noteligible").show();
                        }

                        $("#main-content").show();
                        $('#txtcc').attr('readonly', true);
                        $("#btnShow").hide();

                    }

                }
            });
        });

        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfAddressChangeAmtUpdate.php"; // the script where you handle the form input.
           
            //alert(filename);    

            var data;
            if (Mode == 'Add')
            {

                data = "action=ADD&&tehsil=" + ddlTehsil.value + "&orgcode=" + txtOrgCode.value +
                        "&areatype=" + area_type + ""; // serializes the form's elements.
                //alert(data);
            } else
            {
                data = "action=UPDATE&values=" + txtcc.value + ""; // serializes the form's elements.
            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Center Details Updated Successfully.</span>");
                        window.setTimeout(function () {
                            window.location.href = "frmaddresschangeamtupdate.php";
                        }, 2000);

                        Mode = "Add";
                        resetForm("frmaddresschangeamtupdate");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }

                }
            });


            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

</html>