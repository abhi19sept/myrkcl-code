<?php
$title = "Bio-Metric Registration Report";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='Show'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 
    

    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading">Bio-Metric Registration Report</div>
        <div class="panel-body">					
            <form name="frmbiometricregister" id="frmbiometricregister" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>
                    </div>        
                    <div id="errorBox"></div>

                    <div class="col-sm-4 form-group"> 
                        <label for="sdate">Select Status:</label>
                        <span class="star">*</span>
							<select id="ddlStatus" name="ddlStatus" class="form-control">                                  
                                <option value=""> Please Select </option>
								<option value="1"> Registered </option>
								<option value="2"> Not Registered </option>
							</select>     
                    </div>
				</div>  

                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View Report" style="margin-left:15px;"/>    
                </div>

               <div id="grid" style="margin-top:15px;"> </div>      
        </div>
    </div>   
</div>
</form>

</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

	$(document).ready(function () {
       

        $("#btnSubmit").click(function () {
            if ($("#frmbiometricregister").valid()) {
                $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
				setTimeout(function(){$('#grid').load();}, 2000); 
				
				var url = "common/cfBioMetricRegisterReport.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Show')
                {
                    data = "action=SHOW&status=" + ddlStatus.value + ""; // serializes the form's elements.				 
                }
                else
                {
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        
                            $("#grid").html(data);
                            $('#example').DataTable({
							dom: 'Bfrtip',
							buttons: [
								'copy', 'csv', 'excel', 'pdf', 'print'
							]
						});
                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>

	<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmbiometricregistrationreport_validation.js"></script>
</html>	