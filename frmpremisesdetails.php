<?php
$title="Premises Details";
include ('header.php'); 
include ('root_menu.php'); 

if (isset($_REQUEST['code'])) {
				    echo "<script>var PremisesCode=" . $_REQUEST['code'] . "</script>";
				    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
				} else {
				    echo "<script>var PremisesCode=0</script>";
				    echo "<script>var Mode='Add'</script>";
				}
				?>
         <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">Premises Details
				
				</div>
                <div class="panel-body">
					
                    <form  class="form-inline" name="frmpremisesdetail" class="form-inline" id="frmpremisesdetail" action="" >     
<div class="container">
                                <div id="response"></div>
                            </div>        
				
                        <div class="panel panel-info">
                        <div class="panel-heading">Premises Area Details.</div>
                        <div class="panel-body">

                            <div class="container">
<!--                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Current Total Area (Theory + LAB) in Sq.Ft.:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtarea" id="txtarea" placeholder="Total Area">
                                </div>-->
                                
                                <div class="col-sm-4 form-group">     
                                        <label for="area">Do you have Separate Lab and Theory Rooms:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="areaUrban" name="area" value="Urban" onChange="findselected()"/> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="areaRural" name="area" value="Rural"onChange="findselected1()"/> No </label>
                                    </div>

 <div class="container" id="Urban" style="display:none">
                                       

                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">Enter Theory Room Area in Sq.Ft.:</label>
                                    <input type="text" class="form-control" name="TheoryRoomArea" id="TheoryRoomArea" onkeyup="calc()" value="0" placeholder="Theory Room Area" onkeypress="javascript:return allownumbers(event);">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="faname">Enter Total LAB Room Area in Sq.Ft.:</label>
                                    <input type="text" class="form-control" name="LabArea" id="LabArea" onkeyup="calc()" value="0" placeholder="LAB Area" onkeypress="javascript:return allownumbers(event);">
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Total Area (Theory + LAB) in Sq.Ft.:</label>
                                    <input type="text" class="form-control" readonly="true" name="TotalArea" id="TotalArea" onkeyup="calc()" value="0" placeholder="Total Area" onkeypress="javascript:return allownumbers(event);">  
                                </div>
                                    </div>

                                    <div class="container" id="Rural" style="display:none">
                                       <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Enter Total Area (Theory + LAB) in Sq.Ft.:</label>
                                    <input type="text" class="form-control" name="TotalArea1" id="TotalArea1" placeholder="Total Area">  
                                </div>
                                    </div>

                            </div>
                            
                           
                        </div>
                    </div>
							<div class="container" style="border: 1px solid #428bca;width:1100px">
                            			<div id="errorBox"></div>
                                                        
                                                        
							
                            <div class="col-md-4 ">     
                                <label for="learnercode">Reception type:&nbsp;</label>
                              
								<label class="radio-inline"><input type="radio" name="receptionYesNo" id="receptionYesNo" value="Available" checked="checked">Available</label>
								<label class="radio-inline"><input type="radio" name="receptionYesNo" id="receptionYesNo" value="Not available">Not available</label> 
							  
							
                            </div>
							<div class="clearfix" style="height: 10px;clear: both;"></div>
							
                            <div class="col-md-4 form-group"> 
                                <label for="ename">Seating Capacity :<span class="star">*</span></label>
								
								
								<input type="text" class="form-control" name="txtreceptionSeatingCapacity" id="txtreceptionSeatingCapacity" placeholder="Seating Capacity " onkeypress="javascript:return allownumbers(event);"> 
                                  
                            </div>
							
							

                            <div class="col-md-4 form-group">     
                                <label for="faname"><span class="star">*</span>Area in Sq. Ft:</label>
                               
                           
						   
						   <input type="text" class="form-control" name="txtreceptionAreaInSqFt" id="txtreceptionAreaInSqFt" placeholder="Area in Sq. Ft" onkeypress="javascript:return allownumbers(event);"> 


						   </div>
						   
						   
						   <div class="col-md-4" style="padding:2px">     
                                <label for="address">Details:</label>
                                <textarea class="form-control" rows="0"  id="txtreceptionDetails" name="txtreceptionDetails" placeholder="Details"></textarea>

                            </div>
							
							
						
						   
						 </div>
							<div class="clearfix" style="height: 10px;clear: both;"></div>
							
							<div class="container" style="border: 1px solid #428bca;width:1100px">
							
							
							<div class="col-md-5">     
                                <label for="address">Parking Facility For Customers:<span class="star">*</span></label>
                              
								<label class="radio-inline"><input type="radio"  name="parkingYesNo"  id="parkingYesNo" value="Available" checked="checked">Available</label>
								<label class="radio-inline"><input type="radio" name="parkingYesNo" id="parkingYesNo" value="Not available">Not available</label>
							   
							   
							   
				
                            </div>
							
							
							<div class="clearfix" style="height: 20px;clear: both;"></div>
							
							
							<div class="col-md-5"> 
<label for="learnercode">If yes, Ownership of the Parking:</label>
                              
								<label class="radio-inline"><input type="radio" name="parkingType" id="parkingType" value="PRIVATE" checked="checked">Private &nbsp;&nbsp;</label>
								<label class="radio-inline"><input type="radio" name="parkingType" id="parkingType" value="PUBLIC">Public</label>


                            </div>
							
							
							</div>
							<div class="clearfix" style="height: 20px;clear: both;"></div>
							
							<div class="container" style="border: 1px solid #428bca;width:1100px"> 
							<div class="col-md-3 form-group">     
							
							<label for="learnercode">Parking For:<span class="star">*</span></label></br>
                              
								<label class="checkbox-inline"><input type="checkbox" name="parkingTW" id="parkingTW"  value="Two Wheeler" checked="checked">Two Wheeler</label>
								<label class="checkbox-inline"><input type="checkbox" name="parkingTW" id="parkingFW"  value="Four Wheeler">Four Wheeler</label>
							
							
						
							
                         
                            </div>
							
							
							<div class="col-md-3 form-group"> 
													
                                <label for="emobile">Four Wheeler Capacity:</label>
                                <input type="text" class="form-control" maxlength="12" name="txtfwCapacity" 
									id="txtfwCapacity" placeholder="Capacity" onkeypress="javascript:return allownumbers(event);">
                            </div>
						
							
							
							<div class="col-md-3 form-group">   
									
                                <label for="emobile">Two Wheeler Capacity:</label>
                                <input type="text" class="form-control" maxlength="12" name="txttwCapacity" 
									id="txttwCapacity" placeholder="Capacity" onkeypress="javascript:return allownumbers(event);">
                            </div>
							
							

							
														
						</div>	
						
						<div class="clearfix" style="height: 20px;clear: both;"></div>
						
						
						<div class="container" style="border: 1px solid #428bca;width:1100px">
							
							
							
							
							<div class="col-md-4">     
							<label for="learnercode">Toilet / Washroom:<span class="star">*</span></label>
                              
								<label class="radio-inline"><input type="radio" name="toiletYesNo" id="toiletYesNo" value="Available" checked="checked">Available</label>
								<label class="radio-inline"><input type="radio" name="toiletYesNo"  id="toiletYesNo" value="Not available">Not available</label>
							
							
                             
                            </div>
							
								<div class="col-md-4">

							<label for="learnercode">If yes, Ownership of the Toilet:</label>
                              
							<label class="radio-inline"><input type="radio" name="toiletOwnershipType" id="toiletOwnershipType"  value="PRIVATE" checked="checked">Private</label>
								<label class="radio-inline"><input type="radio" name="toiletOwnershipType" id="toiletOwnershipType"  value="PUBLIC">Public</label>
							
							
                         
                            </div>
							
								<div class="col-md-4">     
							
							<label for="learnercode">Type:(Gents, Ladies):</label>
                              
							<label class="radio-inline"><input type="radio" name="toiletType" id="toiletType" value="Separate" checked="checked">Separate
													&nbsp;&nbsp;</label>
								<label class="radio-inline"><input type="radio" name="toiletType" id="toiletType" value="COMMON">Common</label>
							
							
                           
                            </div>
							
							</div>
							
							<div class="clearfix" style="height: 10px;clear: both;"></div>
							
							
							
							
							
					<div class="clearfix" style="height: 10px;clear: both;"></div>
					
					
						<div class="container" style="border: 1px solid #428bca;width:1100px">	
							
							<div class="col-md-4">
							<label for="learnercode">Pantry / Kitchen:</label>
                              
							<label class="radio-inline"><input type="radio" name="pantryYesNo"  id="pantryYesNo"  value="yes" checked="checked">Yes &nbsp;&nbsp;
													&nbsp;&nbsp;</label>
								<label class="radio-inline"><input type="radio" name="pantryYesNo"  id="pantryYesNo" value="no">No</label>
							
							

                            </div>
							
							
							
							<div class="col-md-4 form-group">     
                                <label for="address">Capacity &nbsp;</label>
                          <input type="text" name="txtpantryDetails" id="txtpantryDetails" class="form-control" onkeypress="javascript:return allownumbers(event);">
                            </div>
							
							
							
						</div>	
							
							<div class="clearfix" style="height: 10px;clear: both;"></div>
							
							
						


						<div class="container" style="border: 1px solid #428bca;width:1100px">
							
							
						  <div class="col-md-4">   
								
							<label for="learnercode">Library:</label>
                              
							<label class="radio-inline"><input type="radio" name="libraryYesNo"  id="libraryYesNo"  value="yes" checked="checked">Yes &nbsp;&nbsp;
													&nbsp;&nbsp;</label>
								<label class="radio-inline"><input type="radio" name="libraryYesNo"  id="libraryYesNo" value="no">No</label>	
								
												
                          
                            </div>
							
							<div class="col-md-4 form-group">     
                                <label for="address">Capacity &nbsp;</label>
                        <input type="text" name="txtlibraryDetails" id="txtlibraryDetails" class="form-control" onkeypress="javascript:return allownumbers(event);">
                            </div>
							
							
					</div>
					<div class="clearfix" style="height: 10px;clear: both;"></div>
						<div class="container" style="border: 1px solid #428bca;width:1100px">	
							
								<div class="col-md-4">     
							<label for="learnercode">Staff Room:</label>
                              
							<label class="radio-inline"><input type="radio" name="staffRoomYesNo"  id="staffRoomYes" value="yes" checked="checked" >Yes &nbsp;&nbsp;
													&nbsp;&nbsp;</label>
								<label class="radio-inline"><input type="radio" name="staffRoomYesNo" id="staffRoomNo" value="no">No</label>		
								
                          
                            </div>
							
							
							
							
							<div class="col-md-4 form-group">     
                                <label for="address">Capacity  &nbsp;</label>
                      <input type="text" name="txtstaffRoomDetails" id="txtstaffRoomDetails" class="form-control" onkeypress="javascript:return allownumbers(event);">
                            </div>
							
							
							

                            
                        </div>
						
						
						<div class="clearfix" style="height: 10px;clear: both;"></div>



                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
					
						<div id="gird"></div>
						
						

					

					
  </div>
            </div>   
        </div>
    </form>



</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>

<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>
<script>
    function calc()
    {
        var TA = parseInt(document.getElementById('TheoryRoomArea').value);
        var LA = parseInt(document.getElementById('LabArea').value);
         var Total = TA + LA;
         
        if (Total)
        {
            document.getElementById("TotalArea").value = Total;
        }
        
    }
</script>

<style>
#errorBox{
 color:#F00;
 }
</style>
<style>
  .modal-dialog {width:600px;}
.thumbnail {margin-bottom:6px;}
  </style>
  
  
  <script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>
  <script>
  $(document).ready(function() {
$('.thumbnail').click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});
});
  </script>
<script type="text/javascript">
		var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
		var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
		var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
		var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
		$(document).ready(function () {
		
		  if (Mode == 'Delete')
		            {
		                if (confirm("Do You Want To Delete This Item ?"))
		                {
		                    deleteRecord();
		                }
		            }
		            else if (Mode == 'Edit')
		            {
		                fillForm();
		            }
		    
		
		          
		            
		            
		            function deleteRecord()
		            {
		                $('#response').empty();
		                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		                $.ajax({
		                    type: "post",
		                    url: "common/cfPremisesDetail.php",
		                    data: "action=DELETE&values=" + PremisesCode + "",
		                    success: function (data) {
		                        //alert(data);
		                        if (data == SuccessfullyDelete)
		                        {
		                            $('#response').empty();
		                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
		                            window.setTimeout(function () {
		                          window.location.href="frmpremisesdetails.php";
		                           }, 1000);
		                            
		                            Mode="Add";
		                            resetForm("frmpremisesdetails");
		                        }
		                        else
		                        {
		                            $('#response').empty();
		                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
		                        }
		                        showData();
		                    }
		                });
		            }
		 function fillForm()
                        {
							//alert("HII");
                            $.ajax({
                                type: "post",
                                url: "common/cfPremisesDetail.php",
                                data: "action=EDIT&values=" + PremisesCode + "",
                                success: function (data) {
									//alert(data);
                                    //alert($.parseJSON(data)[0]['Status']);
                                    data = $.parseJSON(data);
                                    
								
									receptionYesNo.value = data[0].Premises_Receptiontype;
									txtreceptionSeatingCapacity.value = data[0].Premises_seatingcapacity;
									txtreceptionAreaInSqFt.value = data[0].Premises_area;
									txtreceptionDetails.value = data[0].Premises_details;
									parkingYesNo.value = data[0].Premises_parkingfacility;
									parkingType.value = data[0].Premises_ownership;
									parkingTW.value = data[0].Premises_parkingfor;
									txtfwCapacity.value = data[0].Premises_fwcapacity;
									txttwCapacity.value = data[0].Premises_twcapacity;
									toiletYesNo.value = data[0].Premises_toiletavailable;
									toiletOwnershipType.value = data[0].Premises_ownershiptoilet;
									toiletType.value = data[0].Premises_toilet_type;
									pantryYesNo.value = data[0].Premises_panatry;
									txtpantryDetails.value = data[0].Premises_panatry_capacity;
									libraryYesNo.value = data[0].Premises_library_available;
									txtlibraryDetails.value = data[0].Premises_library_capacity;
									//staffRoomYesNo.value = data[0].Premises_Staff_available;
									txtstaffRoomDetails.value = data[0].Premises_Staff_capacity;
									
                                }
                            });
                        }
		       		 function showData() {
		                
		                $.ajax({
		                    type: "post",
		                    url: "common/cfPremisesDetail.php",
		                    data: "action=SHOW",
		                    success: function (data) {
		
		                        $("#gird").html(data);
								
								 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
		
		                    }
		                });
		            }
					showData();
                                        
                                        
 function findselected() {

            var UrbanDiv = document.getElementById("areaUrban");
            var category = document.getElementById("Urban");

            if (UrbanDiv) {
                Urban.style.display = areaUrban.checked ? "block" : "none";
                Rural.style.display = "none";
            } else
            {
                Urban.style.display = "none";
            }
        }
        function findselected1() {


            var RuralDiv = document.getElementById("areaRural");
            var category1 = document.getElementById("Rural");

            if (RuralDiv) {
                Rural.style.display = areaRural.checked ? "block" : "none";
                Urban.style.display = "none";
            } else
            {
                Rural.style.display = "none";
            }
        }
        $("#areaUrban").change(function () {
            findselected();
        });
        $("#areaRural").change(function () {
            findselected1();
        });
        
		$("#btnSubmit").click(function () {
			
			if ($("#frmpremisesdetail").valid())
            {
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfPremisesDetail.php"; // the script where you handle the form input.
				
                var forminput=$("#frmpremisesdetail").serialize();
                if (Mode == 'Add')
                {
                    data = "action=ADD&" +forminput; // serializes the form's elements.
                }
                else
                {
					
					data = "action=UPDATE&code=" + PremisesCode +"&" + forminput;
                    
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                             window.setTimeout(function () {
                                window.location.href = "frmpremisesdetails.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmpremisesdetails");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			 }
                return false; // avoid to execute the actual submit of the form.
            });
			
		    function resetForm(formid) {
		        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
		    }
		
		});
		
	</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmpremisesdetail.js"></script>
		<style>
		
.error {
	color: #D95C5C!important;
}
</style>

</html>