 <?php
    require_once 'DAL/classconnection.php';

    $_ObjConnection = new _Connection();
	$_ObjConnection->Connect();
	
	date_default_timezone_set("Asia/Kolkata");
	ini_set("memory_limit", "5000M");
    ini_set("max_execution_time", 0);
    set_time_limit(0);

	$batchId = 182;
	$minAge = 16;
	$maxAge = 40;
	$maxInTake = 10;

	//To restart merit logic just enable bellow function calls.
	resetPriorities($batchId);
	startProcess($batchId, $minAge, $maxAge, $maxInTake);

/**
*	Function to get database connection object.
*/
	function dbConnection() {
		global $_ObjConnection;

		return $_ObjConnection;
	}

	function resetPriorities($batchId) {
		$_ObjConnection = dbConnection();
		$updateQuery = "UPDATE tbl_oasis_admission SET 
			Oasis_Admission_Eligibility = '',
			Oasis_Admission_Eligiblity_Remark = '',
			Oasis_Admission_Stage_One_Priority = '0', 
			Oasis_Admission_Stage_Two_Priority = '0', 
			Oasis_Admission_Stage_Three_Priority = '0', 
			Oasis_Admission_Stage_Four_Priority = '0', 
			Oasis_Admission_Stage_Five_Priority = '0', 
			Oasis_Admission_Final_Priority = '0',
			Oasis_Admission_Final_Preference = '0' 
		WHERE Oasis_Admission_Batch = '" . $batchId . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

	function startProcess($batchId, $minAge, $maxAge, $maxInTake) {
		updateNullAge($batchId);
		markNotEligibileRecords($batchId, $minAge, $maxAge);
		markEligibileRecords($batchId);
		setFinalPreferences($batchId, $maxInTake);
		setFinalPreferencesByIntake($batchId);
		setPriorityOfEligibleRecords($batchId);
	}

	function updateNullAge($batchId) {
		$_ObjConnection = dbConnection();
		$updateQuery = "UPDATE tbl_oasis_admission SET Oasis_Admission_Age = ROUND((DATEDIFF('2017-07-01', Oasis_Admission_DOB) / 365.25), 2) WHERE Oasis_Admission_Batch = " . $batchId . " AND (Oasis_Admission_Age IS NULL OR Oasis_Admission_Age >= 40)";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

	function markNotEligibileRecords($batchId, $minAge, $maxAge) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Oasis_Admission_Code, 'Duplicate' AS remark, TRIM(REPLACE(Oasis_Admission_Name, ' ', '')) AS name, TRIM(REPLACE(Oasis_Admission_Fname, ' ', '')) AS fname FROM tbl_oasis_admission WHERE Oasis_Admission_Batch = $batchId GROUP BY name, fname, Oasis_Admission_DOB HAVING COUNT(*) > 1 UNION SELECT Oasis_Admission_Code, 'Invalid Age' AS remark, '' AS name, '' AS fname FROM tbl_oasis_admission WHERE Oasis_Admission_Batch = $batchId AND (Oasis_Admission_Age < $minAge OR Oasis_Admission_Age > $maxAge)";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2])) {
			while ($row = mysqli_fetch_array($response[2])) {
				$updateQuery = "UPDATE tbl_oasis_admission SET Oasis_Admission_Eligibility = 'Not Eligible', Oasis_Admission_Eligiblity_Remark = '" . $row['remark'] . "', Oasis_Admission_Final_Priority = '0' WHERE Oasis_Admission_Code = '" . $row['Oasis_Admission_Code'] . "'";
				$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
			}
		}
	}

	function markEligibileRecords($batchId) {
		$_ObjConnection = dbConnection();
		$updateQuery = "UPDATE tbl_oasis_admission SET Oasis_Admission_Eligibility = 'Eligible', Oasis_Admission_Final_Priority = '0' WHERE Oasis_Admission_Batch = '" . $batchId . "' AND (Oasis_Admission_Eligibility IS NULL OR Oasis_Admission_Eligibility = '')";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

	function setFinalPreferences($batchId, $maxInTake) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Oasis_Admission_ITGK_Code, COUNT(*) as n FROM tbl_oasis_admission WHERE Oasis_Admission_Batch = '" . $batchId . "' AND Oasis_Admission_Eligibility = 'Eligible' GROUP BY Oasis_Admission_ITGK_Code HAVING n >= " . $maxInTake;
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2])) {
			while ($row = mysqli_fetch_array($response[2])) {
				$updateQuery = "UPDATE tbl_oasis_admission SET Oasis_Admission_Final_Preference = Oasis_Admission_ITGK_Code WHERE Oasis_Admission_ITGK_Code = '" . $row['Oasis_Admission_ITGK_Code'] . "' AND Oasis_Admission_Eligibility = 'Eligible'";
				$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
			}
		}
	}

	function setFinalPreferencesByIntake($batchId) {
		$_ObjConnection = dbConnection();
		$updateQuery = "UPDATE tbl_oasis_admission SET Oasis_Admission_Final_Preference = Oasis_Admission_ITGK_Code2 WHERE Oasis_Admission_Batch = '" . $batchId . "' AND Oasis_Admission_Eligibility = 'Eligible' AND Oasis_Admission_Final_Preference = 0";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

	function setPriorityOfEligibleRecords($batchId) {
		$_ObjConnection = dbConnection();
		//IF(Oasis_Admission_Category = '2', '1', IF(Oasis_Admission_Category = '3', '2', '3')) AS stage4
		$query = "SELECT Oasis_Admission_Code, IF(Oasis_Admission_MaritalStatus NOT IN ('Single','Married'), '1', '2') AS stage1, IF(Oasis_Admission_SocialCategory = 1, '1', '2') AS stage2, IF(Oasis_Admission_Subcategory = 'Saathin', '1', IF(Oasis_Admission_Subcategory = 'Aaganwadi', '2', '3')) AS stage3, '1' AS stage4, IF(qm.Qualification_Name LIKE ('Doctorate'), '1', IF(qm.Qualification_Name LIKE ('Post Graduate'), '2', IF(qm.Qualification_Name LIKE ('Graduate'), '3', IF(qm.Qualification_Name LIKE ('HSC'), '4', '5')))) AS stage5, IF(qm.Qualification_Name IS NULL, 'SSC', qm.Qualification_Name) AS Qualification_Name, Oasis_Admission_Percentage, Oasis_Admission_MINPercentage, Oasis_Admission_Age FROM tbl_oasis_admission oa LEFT JOIN tbl_qualification_master qm ON oa.Oasis_Admission_Qualification = qm.Qualification_Code WHERE Oasis_Admission_Batch = $batchId AND Oasis_Admission_Eligibility = 'Eligible' ORDER BY stage1, stage2, stage3, stage5, Oasis_Admission_Percentage DESC, Oasis_Admission_MINPercentage DESC, Oasis_Admission_Age DESC";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2])) {
			$sn = 1;
			while ($row = mysqli_fetch_array($response[2])) {
				$updateQuery = "UPDATE tbl_oasis_admission SET 
					Oasis_Admission_Stage_One_Priority = '" . $row['stage1'] . "', 
					Oasis_Admission_Stage_Two_Priority = '" . $row['stage2'] . "', 
					Oasis_Admission_Stage_Three_Priority = '" . $row['stage3'] . "', 
					Oasis_Admission_Stage_Four_Priority = '" . $row['stage4'] . "', 
					Oasis_Admission_Stage_Five_Priority = '" . $row['stage5'] . "', 
					Oasis_Admission_Final_Priority = '" . $sn . "'
				WHERE Oasis_Admission_Code = '" . $row['Oasis_Admission_Code'] . "'";
				$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
				$sn++;
			}
		}
	}




	//Bellow SQLs are used to execute for mannual data corrections:

	//SELECT Oasis_Admission_DOB, Oasis_Admission_Age, Oasis_Admission_Eligibility, ROUND((DATEDIFF('2017-07-01', Oasis_Admission_DOB) / 365.25), 2)  AS age FROM tbl_oasis_admission HAVING age < 16 OR age > 40 OR Oasis_Admission_Age >= 40;

	//update `tbl_oasis_admission` set `Oasis_Admission_Eligibility`='Not Eligible',  `Oasis_Admission_Eligiblity_Remark`='Invalid Name' where `Oasis_Admission_Name`='D';

	//SELECT * FROM tbl_oasis_admission WHERE Oasis_Admission_Name REGEXP '[^a-zA-Z 0-9]'

	//SELECT * FROM tbl_oasis_admission WHERE Oasis_Admission_FName REGEXP '[^a-zA-Z 0-9]'

	//UPDATE tbl_oasis_admission SET Oasis_Admission_Name = REPLACE(Oasis_Admission_Name, 'KUM ','') WHERE Oasis_Admission_Batch=140
	//UPDATE tbl_oasis_admission SET Oasis_Admission_Name = REPLACE(Oasis_Admission_Name, 'Kum ','') WHERE Oasis_Admission_Batch=140
	//UPDATE tbl_oasis_admission SET Oasis_Admission_Name = REPLACE(Oasis_Admission_Name, 'kum ','') WHERE Oasis_Admission_Batch=140

	//SELECT Oasis_Admission_Code, Oasis_Admission_Name FROM tbl_oasis_admission WHERE (Oasis_Admission_Name LIKE('kum %') OR Oasis_Admission_Name LIKE('ku %') OR Oasis_Admission_Name LIKE('km %') OR Oasis_Admission_Name LIKE('kumari %') OR Oasis_Admission_Name LIKE('mrs %') OR Oasis_Admission_Name LIKE('ms %') OR Oasis_Admission_Name LIKE('mr %') OR Oasis_Admission_Name LIKE('shri %') OR Oasis_Admission_Name LIKE('late %'))

	//SELECT Oasis_Admission_Code, Oasis_Admission_FName FROM tbl_oasis_admission WHERE (Oasis_Admission_FName LIKE('kum %') OR Oasis_Admission_FName LIKE('ku %') OR Oasis_Admission_FName LIKE('km %') OR Oasis_Admission_FName LIKE('kumari %') OR Oasis_Admission_FName LIKE('mrs %') OR Oasis_Admission_FName LIKE('ms %') OR Oasis_Admission_FName LIKE('mr %') OR Oasis_Admission_FName LIKE('shri %') OR Oasis_Admission_FName LIKE('late %'))


	/**WCD registration issues found in DB which will need to fix:**/

	/**
		*1. Many values found incorrect in Center Choice 1 / Choice 2 fields as 'DB connection string stored in these fields'
		*2. 2nd choice found empty
		*3. Choice1 & Choice2 found same for few applications
		*4. 0000-00-00 DOB found in a application
		*5. Age calullations need to be more accurate.
		*6. Education Percentages are found without decimal / more than 100% or incorrect.
		*7. High edu code found empty in few records.
		
		*8. Special charectors found in Learner/Father names many be in other strings also.
		*9. Name prefixes are found frequently as MS., Late., Shri, Kum, Kumari, these may used for duplicate applications.
		*10. Single letter names are found in DB which are logically incorrect.
		
		*11.Field Values of Higher Qualifications are stored in DB even if 'No' Higher Education option was selected.
		
		*12.Anagwadi with HSC is incorrect.
		
		*13. Do not save hide field values if filled by user by changing the options. according to the related option either validate / do empty the value.
			a. hrEdu != 1 then empty Higher Edu Fields
			b. ddlCategory != 'ST' then TSP, ddlTspDistrict, ddlTspTehsil & Nontspval is empty
			c. TSP == 'TSP' then Nontspval is empty
			d. TSP == 'NonTSP' then ddlTspDistrict, ddlTspTehsil is empty 

		13.If all crireria found same in two diffrent records then final short will be as first+lastname.

	*/

	/**
		Possible efforts required for enhancements:
		1. Apply more JS validations
		2. apply checking to ensure all ajax results are properly processed.
		3. Minimize use of ajax processes
		4. Apply Server side validations on all storing values.
	*/


		/*
			select * from tbl_oasis_admission_10 where  Oasis_Admission_Final_Preference=0

	update tbl_oasis_admission_10 set Oasis_Admission_Final_Preference=0

	UPDATE tbl_oasis_admission_10 SET Oasis_Admission_Final_Preference = Oasis_Admission_ITGK_Code WHERE Oasis_Admission_Batch = '140' AND Oasis_Admission_Eligibility = 'Eligible' AND Oasis_Admission_ITGK_Code2=0

	INSERT INTO tbl_oasis_admission_names (SELECT Oasis_Admission_ITGK_Code, COUNT(*) as n FROM tbl_oasis_admission_10 WHERE Oasis_Admission_Batch = '140' AND Oasis_Admission_Eligibility = 'Eligible' GROUP BY Oasis_Admission_ITGK_Code HAVING n < 10)

	UPDATE tbl_oasis_admission_10 oa INNER JOIN tbl_oasis_admission_names an ON oa.Oasis_Admission_ITGK_Code = an.id SET oa.Oasis_Admission_Final_Preference = oa.Oasis_Admission_ITGK_Code2 where Oasis_Admission_Final_Preference=0

	UPDATE tbl_oasis_admission_10 SET Oasis_Admission_Final_Preference = Oasis_Admission_ITGK_Code WHERE Oasis_Admission_Batch = '140' AND Oasis_Admission_Eligibility = 'Eligible' AND Oasis_Admission_Final_Preference=0

	select Oasis_Admission_Final_Preference, Count(*) as n from tbl_oasis_admission_10  where Oasis_Admission_Eligibility = 'Eligible' and Oasis_Admission_Batch = '140' group by Oasis_Admission_Final_Preference order by n

		*/

	//WCD Merit generation SQLs:

/*
	CREATE TABLE `tbl_oasis_wcd_merit` (                          
       `id` int(11) NOT NULL AUTO_INCREMENT,                       
       `admission_code` int(11) DEFAULT NULL,                      
       `stage1` int(3) DEFAULT NULL,                               
       `stage2` int(3) DEFAULT NULL,                               
       `stage3` int(3) DEFAULT NULL,                               
       `stage4` int(3) DEFAULT NULL,                               
       `stage5` int(3) DEFAULT NULL,                               
       `Qualification_Name` varchar(100) DEFAULT NULL,             
       `HSC_percent` float(5,2) DEFAULT NULL,                      
       `SSC_percent` float(5,2) DEFAULT NULL,                      
       `age` float(5,2) DEFAULT NULL,                              
       PRIMARY KEY (`id`)                                          
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1

	*/

	//truncate table tbl_oasis_wcd_merit;

	//INSERT INTO tbl_oasis_wcd_merit (SELECT '', Oasis_Admission_Code, IF(Oasis_Admission_MaritalStatus NOT IN ('Single','Married'), '1', '2') AS stage1, IF(Oasis_Admission_SocialCategory = 1, '1', '2') AS stage2, IF(Oasis_Admission_Subcategory = 'Saathin', '1', IF(Oasis_Admission_Subcategory = 'Aaganwadi', '2', '3')) AS stage3, IF(Oasis_Admission_Category = '2', '1', IF(Oasis_Admission_Category = '3', '2', '3')) AS stage4, IF(qm.Qualification_Name LIKE ('Doctorate'), '1', IF(qm.Qualification_Name LIKE ('Post Graduate'), '2', IF(qm.Qualification_Name LIKE ('Graduate'), '3', IF(qm.Qualification_Name LIKE ('HSC'), '4', '5')))) AS stage5, IF(qm.Qualification_Name IS NULL, 'SSC', qm.Qualification_Name) AS Qualification_Name, Oasis_Admission_Percentage, Oasis_Admission_MINPercentage, Oasis_Admission_Age FROM tbl_oasis_admission_10 oa LEFT JOIN tbl_qualification_master qm ON oa.Oasis_Admission_Qualification = qm.Qualification_Code WHERE Oasis_Admission_Batch = 140 AND Oasis_Admission_Eligibility = 'Eligible' ORDER BY stage1, stage2, stage3, stage4, stage5, Oasis_Admission_Percentage DESC, Oasis_Admission_MINPercentage DESC, Oasis_Admission_Age DESC)

	//SELECT Oasis_Admission_LearnerCode, Oasis_Admission_ITGK_Code, Oasis_Admission_ITGK_Code2, Oasis_Admission_Eligibility, Oasis_Admission_Final_Priority, Oasis_Admission_Final_Preference, Oasis_Admission_MaritalStatus, IF(Oasis_Admission_SocialCategory = 1, 'Victim of Violence', '') AS SocialCategory, Oasis_Admission_Subcategory, IF(Oasis_Admission_Category = '2', 'SC', IF(Oasis_Admission_Category = '3', 'ST', IF(Oasis_Admission_Category = '1', 'General', 'OBC'))), qm.Qualification_Name, Oasis_Admission_Percentage, Oasis_Admission_MINPercentage, Oasis_Admission_Age, Oasis_Admission_DOB FROM tbl_oasis_admission_10 oa LEFT JOIN tbl_qualification_master qm ON oa.Oasis_Admission_Qualification = qm.Qualification_Code WHERE Oasis_Admission_Batch = 140 AND Oasis_Admission_Eligibility = 'Eligible' ORDER BY oa.Oasis_Admission_Final_Priority ASC

    //UPDATE tbl_oasis_admission_10 oa INNER JOIN tbl_oasis_wcd_merit wm ON oa.Oasis_Admission_Code = wm.admission_code SET oa.Oasis_Admission_Final_Priority = wm.id, oa.Oasis_Admission_Stage_One_Priority = wm.stage1, oa.Oasis_Admission_Stage_Two_Priority = wm.stage2, oa.Oasis_Admission_Stage_Three_Priority = wm.stage3, oa.Oasis_Admission_Stage_Four_Priority = wm.stage4, oa.Oasis_Admission_Stage_Five_Priority = wm.stage5, oa.Oasis_Admission_Age = wm.age

?>