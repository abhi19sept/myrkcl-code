<?php
    $title = "Name Update Request";
    include('header.php');
    include('root_menu.php');
    include "common/modals.php";

    if ($_SESSION["User_UserRoll"] <> 7) {
        echo "<script>$('#unauthorized').modal('show')</script>";
        die;
    }
?>
<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>
<style type="text/css">
    div.dt-buttons{
        margin-bottom: 10px;
    }
</style>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container">

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">View All Name Update Requests

            </div>
            <div class="panel-body">
                <div id="response"></div>
                <div id="gird"></div>

            </div>

        </div>
    </div>

</div>
<?php
    include 'common/message.php';
    include 'footer.php';
?>

<script type="text/javascript">

    var Code = '<?php echo $_SESSION['User_LoginId'];?>';

    $(document).ready(function () {

        function showData() {
            var url = "common/cfmodifyITGK.php";
            var data;
            data = "action=showNameUpdateRequestTableITGK&Code="+Code;
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    showDatatable(data);
                }
            });
        }

        function showDatatable(data) {
            $("#gird").html(data);
            $("#example").DataTable({
                dom: 'Bfrtip',
                "bProcessing": true,
                "serverSide": true,
                "ajax": {
                    url: "common/cfmodifyITGK.php",
                    data: {
                        "action": "showAddressUpdateRequestITGK",
                        "flag":"name",
                        "Code": Code
                    },
                    type: "POST"
                },
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columnDefs: [
                    {
                        targets: 1,
                        render: function (data, type, row, meta) {
                            if (type === 'display') {
                             data = '<a href="viewDetails_name_ITGK.php?Code=' + encryptString(data) + '">' + data + '</a>';
                             }
                            return data;
                        }
                    }
                ]
            });
        }
        showData();
    });

</script>
</html>