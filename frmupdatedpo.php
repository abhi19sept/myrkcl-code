<?php
$title = "Update DPO Details";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var ItPeripheralsCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var DeviceCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}

//echo "<script>var UserRoll='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading"> Update DPO Details

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
                <form name="frmselectvisit" id="frmselectvisit" class="form-inline" action=""> 

                    <div class="panel panel-info">
                        <div class="panel-heading">Select DPO to EDIT</div>
                        <div class="panel-body">
                            <div class="col-md-2 form-group">     
                                <label for="visit">Select DPO to EDIT:<span class="star">*</span></label>
                            </div>
                            <div class="col-md-6 form-group"> 
                                <select id="ddlDPO" name="ddlDPO" class="form-control" required="required" oninvalid="setCustomValidity('Please Select DPO to Update')"
                                        onchange="try {
                                                    setCustomValidity('')
                                                } catch (e) {
                                                }"> </select>
                                <!--</div>-->
                            </div>

                            <div class="container">
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Proceed"/> </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info" id="existing_details" style="display:none">
                        <div class="panel-heading">Update DPO Contact Details</div>
                        <div class="panel-body">
                            <div id="response_Update"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Login Id of DPO:</label>
                                <input type="text" class="form-control" name="LoginId" id="LoginId" placeholder="LoginId" readonly='readonly'>
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Mobile Number:</label>
                                <input type="text" class="form-control" minlength="10" maxlength="10" name="Mobile" id="Mobile">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Email:</label>
                                <input type="text" class="form-control" name="Email" id="Email">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label for="email">Click Here to Update Details:</label>
                                <input type="button" name="btnUpdateContact" id="btnUpdateContact" class="btn btn-primary" value="Update DPO"/>
                            </div>
                        </div></div>	
                    <div class="panel panel-info" id="existing_details1" style="display:none">
                        <div class="panel-heading">De-Register DPO Biometric Enrollment</div>
                        <div class="panel-body">
                            <div id="response_Deregister"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="faname">DPO Name:</label>
                                <input type="text" class="form-control" name="BioName" id="BioName" readonly='readonly'>
                            </div>
                            <div class="col-sm-4 form-group">     
                                <label for="SelectType">Mobile No:</label>
                                <input type="text" class="form-control"  minlength="10" maxlength="10" name="BioMobile" id="BioMobile" readonly='readonly'>

                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="email">Email:</label>
                                <input type="text" class="form-control" name="BioEmail" id="BioEmail" readonly='readonly'>
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="email">Click Here to De-Register:</label>
                                <label for="lblDeregister"></label>
                                <input type="button" name="btnDeregisterDPO" id="btnDeregisterDPO" class="btn btn-primary" value="De-Register"/>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>

    </div>

</div>   
</div>

</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillDPO() {
            $.ajax({
                type: "post",
                url: "common/cfUpdateDPO.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDPO").html(data);
                }
            });
        }
        FillDPO();

        $("#ddlDPO").change(function () {
            // var selOwnType = $(this).val();
            //alert(selOrgType);
            $('#existing_details').hide();
            $('#existing_details1').hide();


        });

        $("#btnSubmit").click(function () {
            fillForm();

        });

        function fillForm()
        {           //alert(val);    
            $.ajax({
                type: "post",
                url: "common/cfUpdateDPO.php",
                data: "action=APPROVE&value=" + ddlDPO.value + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " DPO Details Not Available." + "</span></p>");
                    } else if (data == "1") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select DPO to Proceed" + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        LoginId.value = data[0].username;
                        Mobile.value = data[0].usermobile;
                        Email.value = data[0].useremail;
                        if (data[0].fullname == 'NA' || data[0].biomobile == 'NA' || data[0].bioemail == 'NA') {
                            $('#btnDeregisterDPO').hide();
                            jQuery("label[for='lblDeregister']").html("Details not available to De-Register");
                        }
                        BioName.value = data[0].fullname;
                        BioMobile.value = data[0].biomobile;
                        BioEmail.value = data[0].bioemail;
                        $('#existing_details').show(2000);
                        $('#existing_details1').show(3000);

                    }

                }
            });
        }

        $("#btnUpdateContact").click(function () {
//		if ($("#frmupdateitgkinfo").valid())
//		{
            $('#response_Update').empty();
            $('#response_Update').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfUpdateDPO.php",
                data: "action=UpdateDPO&Code=" + ddlDPO.value + "&Mobile=" + Mobile.value + "&Email=" + Email.value + "",
                success: function (data)
                {
                    //alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {


                        $('#response_Update').empty();
                        $('#response_Update').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmupdatedpo.php";
                        }, 3000);
                    } else
                    {
                        $('#response_Update').empty();
                        $('#response_Update').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span> Contact details updated Successfully.</span></div>");

                    }
                }
            });
            //}
            return false;
        });

        $("#btnDeregisterDPO").click(function () {
//		if ($("#frmupdateitgkinfo").valid())
//		{
            $('#response_Deregister').empty();
            $('#response_Deregister').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfUpdateDPO.php",
                data: "action=DeRegister&Code=" + ddlDPO.value + "",
                success: function (data)
                {
                    alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate || data == 'Sucscessfully Deleted')
                    {


                        $('#response_Deregister').empty();
                        $('#response_Deregister').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmupdatedpo.php";
                        }, 3000);
                    } else
                    {
                        $('#response_Deregister').empty();
                        $('#response_Deregister').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");

                    }
                }
            });
            //}
            return false;
        });


        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</html>
