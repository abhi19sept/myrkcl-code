<?php

    $title = "Update Address Payment";
    include('header.php');
    include('root_menu.php');
    include "common/modals.php";

    $flag = 0;

    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var CenterCode='" . $_SESSION['User_LoginId'] . "'</script>";
    echo "<script>var PayType= 'Update Address Payment' </script>";

    if (!isset($_POST['txnid'])) {
        echo "<script>$('#tempered').modal('show');</script>";
        exit();
    }

    $txnid = (isset($_POST['txnid'])) ? $_POST['txnid'] : '';

    $MERCHANT_KEY = ($_SERVER["HTTP_HOST"] == "localhost") ? "gtKFFx" : "X2ZPKM";
    $SALT = ($_SERVER["HTTP_HOST"] == "localhost") ? "eCwWELxi" : "8IaBELXB";
    $PAYU_BASE_URL = ($_SERVER["HTTP_HOST"] == "localhost") ? "https://test.payu.in" : "https://secure.payu.in";

    $posted = array();
    if (!empty($_POST)) {
        foreach ($_POST as $key => $value) {
            $posted[$key] = htmlentities($value, ENT_QUOTES);
        }
    }

    // Hash Sequence
    $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|||||||";


    if (isset($_REQUEST["btnPay"]) && ($_REQUEST["btnPay"] == "Pay Now")) {


        $hash_string = '';
        $action = '';
        $formError = 0;

        if ($posted['hash'] == "") {

            /*echo "posted hash empty !";
            echo "<BR>";*/

            if (($posted['key'] == "") || ($posted['txnid'] == "") || ($posted['amount'] == "") || ($posted['firstname'] == "") || ($posted['email'] == "") || ($posted['phone'] == "") || ($posted['productinfo'] == "") || ($posted['surl'] == "") || ($posted['furl'] == "") || ($posted['pg'] == "")
            ) {

                /*echo "inside first if !";
                echo "<BR>";*/

                $formError = 1;
            } elseif (($posted['pg'] == 'NB') && ($posted['bankcode'] == "")) {

                /*echo "inside second if !";
                echo "<BR>";*/

                $formError = 1;

            } elseif ((($posted['pg'] == 'CC') || ($posted['pg'] == 'DC')) && ($posted['bankcode'] == "") || ($posted['ccnum'] == "") || ($posted['ccvv'] == "") || ($posted['ccexpmon'] == "") || ($posted['ccname'] == "") || ($posted['ccexpyr'] == "")) {

                /*echo "inside third if !";
                echo "<BR>";*/

                $formError = 1;

            } else {

                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach ($hashVarsSeq as $hash_var) {
                    $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
                    $hash_string .= '|';
                }
                $hash_string .= $SALT;
                $hash = strtolower(hash('sha512', $hash_string));
                $action = $PAYU_BASE_URL . '/_payment';

                $flag = 1;

                echo "<script>$('#payuForm').attr('action','" . $action . "');</script>";
                echo "<script>$('#payuForm').submit()</script>";

            }
        } elseif (!empty($posted['hash'])) {

            /*echo "hash === ".$posted["hash"];

            echo "inside second last else condition !";
            echo "<BR>";*/
            $hash = $posted['hash'];
            $action = $PAYU_BASE_URL . '/_payment';

            $flag = 1;


        } else {

            /*echo "inside last else condition !";
            echo "<BR>";

            echo "no hash";
            die;*/
        }
    } else {
        $hashVarsSeq = explode('|', $hashSequence);
        $hash_string = '';
        foreach ($hashVarsSeq as $hash_var) {
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
        }
        $hash_string .= $SALT;
        $hash = strtolower(hash('sha512', $hash_string));
        $action = $PAYU_BASE_URL . '/_payment';
    }

?>
<title>Update Address Payment</title>
<link rel="stylesheet" href="assets/footer-distributed-with-address-and-phones.css">
<script type="text/javascript" src="scripts/validate.js"></script>

<link rel="stylesheet" href="css/style.css">
<script src="scripts/Menu.js" type="text/javascript"></script>


<link href="bootcss/css/tab_style.css" rel="stylesheet" type="text/css"/>

<script>
    $(document).ready(function () {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    });
</script>

<style>
    #credityear, #debityear {
        width: 100% !important;
    }

    .form-inline .form-group {
        display: inline-block;
        max-width: 100% !important;
        vertical-align: middle;
    }

    .form-control {
        width: 85% !important;
    }
</style>

<body onload="submitPayuForm()">
    <div style="min-height:430px !important;max-height:auto !important;">
        <div class="container">
            <div class="panel panel-primary" style="margin-top:36px;">
                <div class="panel-heading">Payment Transaction Details for <?php echo $_POST['Command'] ?> Request</div>
                <div class="panel-body">
                    <form method="post" name="payuForm" id="payuForm" class="form-inline" role="form">
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>
                            <div id="errorBox"></div>

                            <div style="display: none">
                                <div class="col-sm-3 form-group">
                                    <label for="edistrict">EOI Name:</label>
                                    <input type="text" readonly="true" class="form-control" name="productinfoname" id="productinfoname"
                                           value="<?php echo (empty($posted['productinfoname'])) ? '' : $posted['productinfoname'] ?>"/>
                                </div>


                                <div class="col-sm-3 form-group">
                                    <label for="learnercode">Center Code:</label>
                                    <!--   <div class="form-control" maxlength="50"   name="txtCenterCodediv" id="txtCenterCodediv">

                                      </div> -->
                                    <input type="text" readonly="true" class="form-control" maxlength="50" name="udf1" id="udf1"
                                           value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>">
                                    <input type="hidden" readonly="true" class="form-control" maxlength="50" name="udf2" id="udf2"
                                           value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>">
                                    <input type="hidden" readonly="true" class="form-control" maxlength="50" name="udf3" id="udf3"
                                           value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>">
                                    <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>"/>
                                    <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
                                    <input type="hidden" name="txnid" value="<?php echo $txnid ?>"/>

                                    <input type="hidden" name="paymethod" id="paymethod" value="credit"/>
                                    <input type="hidden" name="cardtype" id="cardtype"/>
                                    <input type="hidden" name="cardcategory" id="cardcategory"/>

                                    <input type="hidden" name="surl" value="<?php echo $posted['surl']; ?>"/>
                                    <input type="hidden" name="furl" value="<?php echo $posted['furl']; ?>"/>
                                    <input type="hidden" name="pg" id="pg" class="form-control" value="<?php echo (empty($posted['pg'])) ? '' : $posted['pg']; ?>"/>
                                    <input type="hidden" class="form-control" name="bankcode" id="bankcode"
                                           value="<?php echo (empty($posted['bankcode'])) ? '' : $posted['bankcode']; ?>"/>
                                    <input type="hidden" class="form-control" maxlength="50" name="ccname" id="ccname" placeholder="CCNAME"
                                           value="<?php echo (empty($posted['ccname'])) ? '' : $posted['ccname']; ?>">
                                    <input type="hidden" class="form-control" maxlength="50" name="ccnum" id="ccnum" placeholder="CCNUM"
                                           value="<?php echo (empty($posted['ccnum'])) ? '' : $posted['ccnum']; ?>">
                                    <input type="hidden" class="form-control" name="ccvv" id="ccvv" value="<?php echo (empty($posted['ccvv'])) ? '' : $posted['ccvv']; ?>"/> <input
                                            type="hidden" class="form-control" maxlength="50" name="ccexpmon" id="ccexpmon" placeholder="CCEXPMON"
                                            value="<?php echo (empty($posted['ccexpmon'])) ? '' : $posted['ccexpmon']; ?>">
                                    <input type="hidden" class="form-control" maxlength="50" name="ccexpyr" id="ccexpyr"
                                           value="<?php echo (empty($posted['ccexpyr'])) ? '' : $posted['ccexpyr']; ?>">
                                </div>

                                <div class="col-sm-3 form-group">
                                    <label for="edistrict">Payment Type:</label>
                                    <input type="hidden" readonly="true" class="form-control" name="ddlEOI" id="ddlEOI"
                                           value="<?php echo (empty($posted['productinfoname'])) ? '' : $posted['productinfoname'] ?>"/>
                                    <input type="text" readonly="true" class="form-control" name="productinfo" id="productinfo"
                                           value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>"/>

                                </div>

                                <div class="col-sm-3 form-group">
                                    <label for="ename">Owner Name:</label>
                                    <!--   <div class="form-control" maxlength="50" name="firstnamediv" id="firstnamediv">

                                      </div> -->
                                    <input type="text" readonly="true" class="form-control text-uppercase" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>"
                                           maxlength="50" name="firstname" id="firstname" placeholder="Owner Name">
                                </div>
                            </div>


                        </div>
                        <div class="container">
                            <div class="col-sm-3 form-group">
                                <label for="faname">Mobile No:</label>
                                <!-- <div class="form-control" maxlength="50" name="phonediv" id="phonediv">

                                 </div> -->
                                <input type="text" readonly="true" class="form-control" maxlength="50" name="phone" id="phone"
                                       value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" placeholder="Owner Mobile">
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="faname">Email ID:</label>
                                <!--    <div class="form-control" maxlength="50" name="emaildiv" id="emaildiv">

                                   </div> -->
                                <input type="text" readonly="true" class="form-control" maxlength="50" name="email" id="email"
                                       value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" placeholder="Owner Email">
                            </div>

                            <div class="col-sm-3 form-group">
                                <label for="ename">Transaction Reference No:</label>
                                <!--  <div class="form-control" maxlength="50"  name="transactiondiv" id="transactiondiv">
                                     123456
                                 </div> -->
                                <input type="text" readonly="true" class="form-control" maxlength="50" name="transaction" id="transaction" placeholder="Tran. Ref. No:"
                                       value="<?php echo $txnid ?>">
                            </div>


                            <div class="col-sm-3 form-group">
                                <label for="faname">Amount:</label>
                                <!--   <div class="form-control" maxlength="50" name="amountdiv" id="amountdiv">

                                  </div> -->
                                <input type="text" readonly="true" class="form-control" maxlength="50" name="amount" id="amount"
                                       value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>" placeholder="Total Amount">
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab-container">
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 bhoechie-tab-menu">
                                        <div class="list-group">
                                            <a href="#" id="tab1" data-index="1" name="tab" class="list-group-item active"> <img src="images/credit_card.png" alt="#"/> &nbsp; Credit Card</a>
                                            <a href="#" id="tab2" data-index="2" name="tab" class="list-group-item"> <img src="images/net_banking.png" alt="#"/> &nbsp; Net Banking </a>
                                            <a href="#" id="tab3" data-index="3" name="tab" class="list-group-item"> <img src="images/credit_card.png" alt="#"/> &nbsp; Debit Cart </a>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 bhoechie-tab">
                                        <!-- CREDIT CARD FORM STARTS HERE -->
                                        <div class="bhoechie-tab-content active">

                                            <div class="panel panel-default credit-card-box">
                                                <div class="panel-heading display-table">
                                                    <div class="row display-tr">
                                                        <h3 class="panel-title display-td">Payment Details</h3>
                                                        <div class="display-td"><img class="img-responsive pull-right" src="images/accepted_c22e0.png"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <!-- <form role="form" id="payment-form" method="POST" action="javascript:void(0);"> -->
                                                    <div class="row">
                                                        <div class="col-xs-12 form-group">
                                                            <label for="ccnum">Card Number:</label>
                                                            <input type="text" class="form-control" maxlength="19" name="creditnum" id="creditnum" placeholder="Credit Card Number"
                                                                   onkeypress="mycreditvalidate();
								javascript:return allownumbers(event);" autocomplete="off">
                                                            <i class="fa fa-credit-card"></i>
                                                        </div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-4 col-md-4">
                                                            <div class="form-group" style="width:100%;">
                                                                <label for="ccexpmon"><span class="hidden-xs">Card Expiry Date</span></label>
                                                                <select name="creditmonth" id="creditmonth" class="selectbox form-control">
                                                                    <option value=''> Select Month</option>
                                                                    <option value='01'>January</option>
                                                                    <option value='02'>February</option>
                                                                    <option value='03'>March</option>
                                                                    <option value='04'>April</option>
                                                                    <option value='05'>May</option>
                                                                    <option value='06'>June</option>
                                                                    <option value='07'>July</option>
                                                                    <option value='08'>August</option>
                                                                    <option value='09'>September</option>
                                                                    <option value='10'>October</option>
                                                                    <option value='11'>November</option>
                                                                    <option value='12'>December</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5 col-md-5">
                                                            <div class="form-group">
                                                                <label for="ccexpyr"><span class="hidden-xs">&nbsp;</span></label>
                                                                <select name="credityear" id="credityear" class="selectbox form-control">

                                                                </select>

                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 col-md-3 pull-right">
                                                            <div class="form-group">
                                                                <label for="ccvv">Security Code</label>
                                                                <input type="text" class="form-control" maxlength="3" name="creditcvv" id="creditcvv" placeholder="CVV Code"
                                                                       onkeypress="javascript:return allownumbers(event);" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 form-group">
                                                            <label for="ccnum">Cardholder's Name:</label>
                                                            <input type="text" class="form-control" maxlength="50" name="creditname" id="creditname" placeholder="Name on Card"
                                                                   style="text-transform:uppercase" onkeypress="javascript:return allowchar(event);" autocomplete="off">

                                                        </div>
                                                    </div>
                                                    <!-- <div class="row">
                                                       <div class="col-xs-12">
                                                         <button class="subscribe btn btn-success btn-lg btn-block" type="button">Save and Pay</button>
                                                       </div>
                                                     </div> -->
                                                    <div class="row" style="display:none;">
                                                        <div class="col-xs-12">
                                                            <p class="payment-errors"></p>
                                                        </div>
                                                    </div>
                                                    <!-- </form> -->
                                                </div>
                                            </div>

                                        </div>
                                        <!-- CREDIT CARD FORM ENDS HERE -->
                                        <!-- NET BANKING BLOG START HERE -->
                                        <div class="bhoechie-tab-content">

                                            <div class="panel">

                                                <div class="panel-body">
                                                    <!-- <form role="form" id="payment-form" method="POST" action="javascript:void(0);"> -->
                                                    <div class="row">
                                                        <div class="col-xs-12 mb20">

                                                            <div class="select-net-bank-wr"><span class="all-banks-txt">All banks</span>
                                                                <select name="ddlbankname" id="ddlbankname" class="selectbox form-control">

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--  <div class="row">
                                                        <div class="col-xs-12">
                                                          <button class="subscribe btn btn-success btn-lg btn-block" type="button">CONTINUE</button>
                                                        </div>
                                                      </div>-->
                                                    <div class="row" style="display:none;">
                                                        <div class="col-xs-12">
                                                            <p class="payment-errors"></p>
                                                        </div>
                                                    </div>
                                                    <!-- </form> -->
                                                </div>
                                            </div>

                                        </div>
                                        <!-- NET BANKING BLOG END HERE -->

                                        <!-- CREDIT CARD FORM STARTS HERE -->
                                        <div class="bhoechie-tab-content">

                                            <div class="panel panel-default credit-card-box">
                                                <div class="panel-heading display-table">
                                                    <div class="row display-tr">
                                                        <h3 class="panel-title display-td">Payment Details</h3>
                                                        <div class="display-td"><img class="img-responsive pull-right" src="images/accepted_c22e0.png"></div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <!-- <form role="form" id="payment-form" method="POST" action="javascript:void(0);"> -->
                                                    <div class="row">
                                                        <div class="col-xs-12 form-group">
                                                            <label for="ccnum">Card Number:</label>
                                                            <input type="text" class="form-control" maxlength="19" name="debitnum" id="debitnum" placeholder="Debit Card Number"
                                                                   onkeypress="mydebitvalidate(); javascript:return allownumbers(event);" autocomplete="off">
                                                            <i class="fa fa-credit-card"></i>
                                                        </div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-4 col-md-4">
                                                            <div class="form-group" style="width:100%;">
                                                                <label for="ccexpmon"><span class="hidden-xs">Card Expiry Date</span></label>
                                                                <select name="debitmonth" id="debitmonth" class="selectbox form-control">
                                                                    <option value=''> Select Month</option>
                                                                    <option value='01'>January</option>
                                                                    <option value='02'>February</option>
                                                                    <option value='03'>March</option>
                                                                    <option value='04'>April</option>
                                                                    <option value='05'>May</option>
                                                                    <option value='06'>June</option>
                                                                    <option value='07'>July</option>
                                                                    <option value='08'>August</option>
                                                                    <option value='09'>September</option>
                                                                    <option value='10'>October</option>
                                                                    <option value='11'>November</option>
                                                                    <option value='12'>December</option>
                                                                </select>

                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5 col-md-5">
                                                            <div class="form-group">
                                                                <label for="ccexpyr">&nbsp;<span class="hidden-xs"></span></label>
                                                                <select name="debityear" id="debityear" class="selectbox form-control"></select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 col-md-3 pull-right">
                                                            <div class="form-group">
                                                                <label for="ccvv">Security Code</label>
                                                                <input type="text" class="form-control" maxlength="3" name="debitcvv" id="debitcvv" placeholder="CVV Code"
                                                                       onkeypress="javascript:return allownumbers(event);" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-xs-12 form-group">
                                                            <label for="ccnum">Cardholder's Name:</label>
                                                            <input type="text" class="form-control" maxlength="50" name="debitname" id="debitname" placeholder="Name on Card"
                                                                   style="text-transform:uppercase" onkeypress="javascript:return allowchar(event);" autocomplete="off">

                                                        </div>
                                                    </div>

                                                    <div class="row" style="display:none;">
                                                        <div class="col-xs-12">
                                                            <p class="payment-errors"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--  </form> -->
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!---->
                        </div>
                        <div class="container">
                            <div class="col-sm-4">
                                </br>
                                <input type="submit" name="btnPay" id="btnPay" class="btn btn-primary" value="Pay Now"/>
                            </div>

                        </div>
                        <div class="container">
                            <div class="col-sm-4">
                                <?php if (!isset($hash)) { ?>
                                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Pay Now" style="display:none;"/>
                                <?php } ?>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<?php include 'common/message.php'; ?>
<style>
    #errorBox {
        color: #F00;
    }
</style>
<script type="text/javascript" src="bootcss/js/secure.js"></script>
<script type="text/javascript">

    $("a[name=tab]").on("click", function () {
        var a = $(this).data("index");
        //alert(a);
        if (a == 1) {
            var cc = 'credit';
            paymethod.value = cc;
        }
        else if (a == 2) {
            //alert(22);
            var nb = 'netbank';
            paymethod.value = nb;
        }
        else if (a == 3) {
            //alert(33);
            var dc = 'debit';
            paymethod.value = dc;

        }
    });
</script>

<script type="text/javascript">

    function mycreditvalidate() {
        var newValue = $("#creditnum").val();
        if (newValue.length == 6) {
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=ValidateCard&values=" + newValue + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    if (data[0].cardCategory == 'CC') {
                        //alert("OK");
                        cardtype.value = data[0].cardType;
                        cardcategory.value = data[0].cardCategory;
                        //$("#creditnum").val('');
                    }
                    else {
                        alert("Please Enter Valid Card Details");
                        $("#creditnum").val('');
                    }
                }
            });
        }
    }
</script>

<script type="text/javascript">
    function mydebitvalidate() {
        //alert("You pressed a debit key inside the input field");

        var newValue = $("#debitnum").val();
        if (newValue.length == 6) {
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=ValidateCard&values=" + newValue + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    if (data[0].cardCategory == 'DC') {
                        //alert("OK");
                        cardtype.value = data[0].cardType;
                        cardcategory.value = data[0].cardCategory;
                    }
                    else {
                        alert("Please Enter Valid Card Details");
                        $("#debitnum").val('');
                    }
                }
            });
        }
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {

        var min = new Date().getFullYear(),
            max = min + 15,
            select = document.getElementById('credityear');

        var firstOption = document.createElement('option');
        firstOption.value = "";
        firstOption.innerHTML = "Select Year";
        select.appendChild(firstOption);

        for (var i = min; i <= max; i++) {
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            select.appendChild(opt);
        }

        var min = new Date().getFullYear(),
            max = min + 15,
            select = document.getElementById('debityear');

        var firstOption_d = document.createElement('option');
        firstOption_d.value = "";
        firstOption_d.innerHTML = "Select Year";
        select.appendChild(firstOption_d);

        for (var i = min; i <= max; i++) {
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            select.appendChild(opt);
        }

        function FillNetBankingName() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=FillNetBankingName",
                success: function (data) {
                    //alert(data);
                    $("#ddlbankname").html(data);
                }
            });
        }

        FillNetBankingName();

        $("#ddlbankname").change(function () {
            var selBankName = $(this).val();
            pg.value = 'NB';
            bankcode.value = selBankName;
        });


        $("#paytype").change(function () {
            var PayType = document.getElementById('paytype').value;
            document.getElementById('pg').value = PayType;
        });

        $("#btnPay").click(function () {
            var method = document.getElementById('paymethod').value;
            if (method == 'credit') {
                //alert("clickcredit");
                var creditnum = document.getElementById('creditnum').value;
                var creditmnth = document.getElementById('creditmonth').value;
                var credityear = document.getElementById('credityear').value;
                var creditcvv = document.getElementById('creditcvv').value;
                var creditname = document.getElementById('creditname').value;

                var creditpg = document.getElementById('cardcategory').value;
                var creditbankcode = document.getElementById('cardtype').value;

                pg.value = creditpg;
                bankcode.value = creditbankcode;

                ccnum.value = creditnum;
                ccexpmon.value = creditmnth;
                ccexpyr.value = credityear;
                ccvv.value = creditcvv;
                ccname.value = creditname;
            }
            else if (method == 'netbank') {
                //alert("clicknetbanking");
                var nb = 'NB';
                pg.value = nb;

            }
            else if (method == 'debit') {
                //alert("clickdebit");
                var creditnum = document.getElementById('debitnum').value;
                var creditmnth = document.getElementById('debitmonth').value;
                var credityear = document.getElementById('debityear').value;
                var creditcvv = document.getElementById('debitcvv').value;
                var creditname = document.getElementById('debitname').value;

                var creditpg = document.getElementById('cardcategory').value;
                var creditbankcode = document.getElementById('cardtype').value;

                pg.value = creditpg;
                bankcode.value = creditbankcode;

                ccnum.value = creditnum;
                ccexpmon.value = creditmnth;
                ccexpyr.value = credityear;
                ccvv.value = creditcvv;
                ccname.value = creditname;
            }
            else {
                //alert("defaultcredit");
                var creditnum = document.getElementById('creditnum').value;
                var creditmnth = document.getElementById('creditmonth').value;
                var credityear = document.getElementById('credityear').value;
                var creditcvv = document.getElementById('creditcvv').value;
                var creditname = document.getElementById('creditname').value;

                var creditpg = document.getElementById('cardcategory').value;
                var creditbankcode = document.getElementById('cardtype').value;

                pg.value = creditpg;
                bankcode.value = creditbankcode;

                ccnum.value = creditnum;
                ccexpmon.value = creditmnth;
                ccexpyr.value = credityear;
                ccvv.value = creditcvv;
                ccname.value = creditname;
            }


            verify();


        });

        function verify() {
            var txtamt = document.getElementById('amount').value;
            var txtamt1 = parseInt(txtamt);
            //if(txtamt1 == amount) {

            var method = document.getElementById('cardcategory').value;
            var methodmatch = document.getElementById('paymethod').value;
            var category = '';
            if (methodmatch == 'credit') {
                category = 'CC';
            }
            else if (methodmatch == 'debit') {
                category = 'DC';
            }
            else if (methodmatch == 'netbank') {
                category = 'netbank';
            }
            if (category == 'CC') {
                if ($("#payuForm").valid()) {
                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=FinalValidateCard&values=" + creditnum.value + "",

                        success: function (data) {
//alert(data);
                            if (data == 1) {
                                alert("Invalid Credit Card Details");
                            }
                            else if (data == 2) {
                                if (method == category) {
                                    $("#btnSubmit").click();
                                }
                            }
                            else {
                                alert("Invalid Credit Card Details.");
                            }
                        }
                    });
                }
            }
            else if (category == 'DC') {
                if ($("#payuForm").valid()) {
                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=FinalValidateCard&values=" + debitnum.value + "",
                        success: function (data) {
                            if (data == 1) {
                                alert("Invalid Debit Card Details");
                            }
                            else if (data == 2) {
                                if (method == category) {
                                    $("#btnSubmit").click();
                                }
                            }
                            else {
                                alert("Invalid Debit Card Details.");
                            }
                        }
                    });
                }
            }
            else if (category == 'netbank') {
                if ($("#payuForm").valid()) {
                    $("#btnSubmit").click();
                }
            }

        }

        //}

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });

</script>

<?php
    if ($flag == 1) {
        echo "<script>$('#payuForm').attr('action','" . $action . "');var payuForm = document.forms.payuForm;payuForm.submit();</script>";
    } else {
        ?>
        <script>
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=addTransaction&txnid=" + '<?php echo $txnid ?>' + "&fld_amount=" + '<?php echo $posted['amount'];?>' + "&fld_productInfo=" + '<?php echo
                $posted['productinfoname'];?>',
                success: function (data) {
                    if (data.search("Successfully Inserted") >= 0 || data.search("Successfully Updated") >= 0) {
                        //alert("successfully inserted into db");
                    }
                }
            });
        </script>
        <?php
    }

?>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmpaymentcardvalidate_validation.js"></script>
<script>
    var hash = '<?php echo (isset($posted['hash'])) ? $posted['hash'] : ""; ?>';
    if (hash !== "") {
        $('#please_wait').modal('show');
    }

    function submitPayuForm() {
        /*if (hash == '') {
            return;
        }
        var payuForm = document.forms.payuForm;
        payuForm.submit();*/

    }
</script>

<?php include('footer.php'); ?>
</html>