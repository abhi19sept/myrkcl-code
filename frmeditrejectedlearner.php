<?php
$title="Edit Rejected Learner Form";
include ('header.php'); 
include ('root_menu.php'); 

//print_r($_REQUEST);

if (isset($_REQUEST['code'])) {
echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
$Mode=$_REQUEST['Mode'];
} else {
echo "<script>var AdmissionCode=0</script>";
echo "<script>var Mode='Add'</script>";
$Mode="Add";
}

$msg = "";
$flag = 0;
//echo empty($_POST);

if (isset($_POST["txtlname"]) && !empty($_POST["txtlname"])) {
	//print_r($_POST);
	//print_r($_FILES);
	
	$_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_photo/';
	$_UploadDirectory2 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_sign/';
	
	$_LearnerName = $_POST["txtlname"];
	$_ParentName = $_POST["txtfname"];
	$_DOB = $_POST["dob"];                        
	$_Code = $_POST['txtAdmissionCode'];
	$_GeneratedId = $_POST['txtlcode'];		
	$_Mobile = $_POST["txtmobile"];
		
	if($_FILES["p1"]["name"]!='') {					     
				$imag =  $_FILES["p1"]["name"];
				$newpicture = $_GeneratedId .'_photo'.'.png';
				$_FILES["p1"]["name"] = $newpicture;
	  	}
	else  {					   
			$newpicture = $_POST['txtphoto'];
		}

	$flag=1;	
	if($_FILES["p1"]["name"]!='') {
		$imageinfo = pathinfo($_FILES["p1"]["name"]);		
		if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg") {
			$msg = "Image must be in either PNG or JPG Format";
			$flag=10;
		 }
		else {	
			if (file_exists("$_UploadDirectory1/" .$_GeneratedId .'_photo'.'.png'))			 
				{ 
					unlink("$_UploadDirectory1/".$newpicture); 
				}
			if (file_exists($_UploadDirectory1)) {
				if (is_writable($_UploadDirectory1)) {
				  move_uploaded_file($_FILES["p1"]["tmp_name"], "$_UploadDirectory1/".$newpicture);							 
			 }
		  }
		}
	 }	
	 
	 
	 if($_FILES["p2"]["name"]!='') {					     
				$imag =  $_FILES["p2"]["name"];
				$newsign = $_GeneratedId .'_sign'.'.png';
				$_FILES["p2"]["name"] = $newsign;
	  	}
	else  {					   
			$newsign = $_POST['txtsign'];
		}

	$flag=1;
	if($_FILES["p2"]["name"]!='') {
		$imageinfo = pathinfo($_FILES["p2"]["name"]);		
		if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg") {
			$msg = "Image must be in either PNG or JPG Format";
			$flag=10;
		 }
		else {	
			if (file_exists("$_UploadDirectory2/" .$_GeneratedId .'_sign'.'.png'))			 
				{ 
					unlink("$_UploadDirectory2/".$newsign); 
				}
			if (file_exists($_UploadDirectory2)) {
				if (is_writable($_UploadDirectory2)) {
				  move_uploaded_file($_FILES["p2"]["tmp_name"], "$_UploadDirectory2/".$newsign);							 
			 }
		  }
		}
	 }	
	 
	 if($flag!=10) {

//include 'common/commonFunction.php';		    
require 'DAL/classconnection.php';
$_ObjConnection = new _Connection();
$_ObjConnection->Connect();

	if(isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
		if(isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
			
				$_ITGK_Code =   $_SESSION['User_LoginId'];
				$_User_Code =   $_SESSION['User_Code'];	
				$lphotodoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_photo/' . $newpicture;
                $lsigndoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_sign/' . $newsign;
				
                if (file_exists($lphotodoc) && file_exists($lsigndoc)) {			
							$_UpdateQuery = "Update tbl_admission set Admission_PhotoProcessing_Status ='Reprocess', Admission_Photo='" . $newpicture . "', Admission_Sign='" . $newsign . "'"
							. " Where Admission_Code='" . $_Code . "'";
							$_Response=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
           
							$_Insert = "Insert Into tbl_admission_log(Admission_Log_Code,Admission_Log_LearnerCode,"
										. "Admission_Log_ITGK_Code,"                 
										. "Admission_Log_Photo,Admission_Log_Sign,Admission_Log_ProcessingStatus,Admission_Log_User_Code) "
										. "'" . $_Code . "' as Admission_Log_Code,"
										. "'" . $_GeneratedId . "' as Admission_Log_LearnerCode,'" .$_ITGK_Code  . "' as Admission_Log_ITGK_Code,"                                     
										. "'" .$newpicture. "' as Admission_Log_Photo,'" .$newsign. "' as Admission_Log_Sign, 'Reprocess' as Admission_Log_ProcessingStatus,"                   
										. "'" . $_User_Code . "' as Admission_Log_User_Code"
										. " From tbl_admission_log";
							$_Response1=$_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
							$flag = 3;	
							$msg = "Successfully Updated";	
				} else {
					$flag = 4;
                    $msg = "Photo and Sign not attached successfully ,Please Re-Upload Photo-Sign";                    
                }		  
		}
		else {
			  session_destroy();
				?>
				<script> window.location.href = "index.php"; </script> 
		<?php 
			}
	}
	else {
		  session_destroy();
			?>
			<script> window.location.href = "index.php"; </script> 
	<?php 
	  }
 }
}
 else
		  {
			  include'common/message.php';
		  }
 ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			
            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">Edit Rejected Learner Form </div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmadmission" method="post" action="" id="frmadmission" class="form-inline" role="form" enctype="multipart/form-data"> 
                        <div class="container">
                            <div class="container">
							 <?php if($flag != 0) {?>
                                <div id="response"><?php
        								echo "<span><img src=images/correct.gif width=10px /></span>";
										echo $msg;
										echo "<script>window.setTimeout(\"window.location.href = 'frmreuploadphotosign.php';\",2000);</script>"; ?>
								</div>
							 <?php } ?>
                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Learner Name:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control text-uppercase" maxlength="50" name="txtlname" id="txtlname" onkeypress="javascript:return allowchar(event);" placeholder="Learner Name">
								<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
								<input type="hidden" class="form-control" maxlength="50" name="txtAdmissionCode" id="txtAdmissionCode"/>								
								<input type="hidden" class="form-control" maxlength="50" name="txtlcode" id="txtlcode"/>
								<input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
								<input type="hidden" class="form-control"  name="txtsign" id="txtsign"/>
								<input type="hidden" class="form-control"  name="action" id="action" value="UPDATE"/>
								<input type="hidden" class="form-control" maxlength="50" name="txtmode" id="txtmode" value="<?php echo $Mode; ?>" />
								
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="fname">Father Name:<span class="star">*</span></label>
                                <input type="text" readonly="true" class="form-control text-uppercase" maxlength="50" name="txtfname" id="txtfname" onkeypress="javascript:return allowchar(event);" placeholder="Father Name">     
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="dob">Date of Birth:<span class="star">*</span></label>								
								<input type="text" readonly="true" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="YYYY-MM-DD">
							</div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="bankaccount">Mobile No:<span class="star">*</span></label>
                                <input type="text" class="form-control" readonly="true"  name="txtmobile"  id="txtmobile" placeholder="Mobile No" onkeypress="javascript:return allownumbers(event);" maxlength="10"/>
                            </div>
                        </div>                         
                        
                        <div class="container">                            
                            <div class="col-sm-4 form-group" > 
								  <label for="photo">Attach Photo:<span class="star">*</span></label> </br>
								  <img id="uploadPreview1" src="images/user icon big.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">								  
									<input id="uploadImage1" type="file" name="p1" onchange="checkPhoto(this);PreviewImage(1)"/>									  
								</div>
							
							<div class="col-sm-4 form-group"> 
							  <label for="photo">Attach Signature:<span class="star">*</span></label> </br>
							   <img id="uploadPreview2" src="images/sign.jpg" id="uploadPreview2" name="filePhoto" width="80px" height="35px" onclick="javascript:document.getElementById('uploadImage2').click();">							  
								 <input id="uploadImage2" type="file" name="p2" onchange="checkSign(this);PreviewImage(2)" />
							</div>  
						</div>

						<div class="container">
                            <input type="submit" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Submit"/>    
                        </div>
                </div>
            </div>   
        </div>
    </form>

</body>
  </div>
<?php include ('footer.php'); ?>
<style>
#errorBox
{	color:#F00;	 } 
</style>

<script type="text/javascript"> 
 $('#dob').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
	});  
	</script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>

<script type="text/javascript">
	function PreviewImage(no) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
		oFReader.onload = function (oFREvent) {
			document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
		};
	};
</script>

<script language="javascript" type="text/javascript">
function checkPhoto(target) {
	var ext = $('#uploadImage1').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("uploadImage1").value = '';
			return false;
		}

    if(target.files[0].size > 5000) {
			
        //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
		alert("Image size should less or equal 5 KB");
		document.getElementById("uploadImage1").value = '';
        return false;
    }
	else if(target.files[0].size < 3000)
			{
				alert("Image size should be greater than 3 KB");
				document.getElementById("uploadImage1").value = '';
				return false;
			}
    document.getElementById("uploadImage1").innerHTML = "";
    return true;
}
</script>

<script language="javascript" type="text/javascript">
function checkSign(target) {
	var ext = $('#uploadImage2').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
			alert('Image must be in either PNG or JPG Format');
			document.getElementById("uploadImage2").value = '';
			return false;
		}

    if(target.files[0].size > 3000) {
			
        //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
		alert("Image size should less or equal 3 KB");
		document.getElementById("uploadImage2").value = '';
        return false;
    }
	else if(target.files[0].size < 1000)
			{
				alert("Image size should be greater than 1 KB");
				document.getElementById("uploadImage2").value = '';
				return false;
			}
    document.getElementById("uploadImage2").innerHTML = "";
    return true;
}
</script>	
	
	<?php
			if(isset($_REQUEST['Mode']) == 'Edit')
			{ ?>
			   <script>
				//$( "#ddlmotherTongue" ).prop( "disabled", true );	
				//$("input[type=radio]").attr('disabled', true);
				//$( "#ddlidproof" ).prop( "disabled", true );
				//$( "#txtidno" ).prop( "disabled", true );
				//$( "#ddlDistrict" ).prop( "disabled", true );
				//$( "#ddlTehsil" ).prop( "disabled", true );
				//$( "#txtAddress" ).prop( "disabled", true );
				//$( "#txtPIN" ).prop( "disabled", true );
				//$( "#txtmobile" ).prop( "disabled", true );
				//$( "#txtResiPh" ).prop( "disabled", true );
				//$( "#txtemail" ).prop( "disabled", true );
				//$( "#ddlQualification" ).prop( "disabled", true );	
				//$( "#ddlLearnerType" ).prop( "disabled", true );
				//$( "#txtGPFno" ).prop( "disabled", true );	
				//$( "#txtUploadScanDoc" ).prop( "disabled", true );				
				</script>
	<?php } ?>    
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
		
        $(document).ready(function () {
			//alert(BatchCode);
			function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();
			
			
            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
				//alert(1);
                fillForm();
            }

            $("input[type='image']").click(function () {
                $("input[id='my_file']").click();
            });
		
		function fillForm()
            {               
				$.ajax({
                    type: "post",
                    url: "common/cfAdmission.php",
                    data: "action=EDIT&values=" + AdmissionCode + "",
                    success: function (data) {						
                        data = $.parseJSON(data);
					    txtlcode.value = data[0].lcode;
                        txtlname.value = data[0].lname;
                        txtfname.value = data[0].fname;
                        dob.value = data[0].dob;
						txtAdmissionCode.value = data[0].AdmissionCode;						
                        txtmobile.value = data[0].mobile;
						txtphoto.value= data[0].photo;
						txtsign.value=data[0].sign;
						$("#uploadPreview1").attr('src',"upload/admission_photo/" + data[0].photo);						
						$("#uploadPreview2").attr('src',"upload/admission_sign/" + data[0].sign);
                        //p1.value = data[0].photo;
                        //p2.value = data[0].sign; 
					}
                });
            }			
			
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }
            // statement block end for submit click
        });

    </script>
	
	<!--<script src="scripts/editadmissionimageupload.js"></script>
	<script src="scripts/signupload.js"></script>-->
    <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <!--<script src="bootcss/js/frmmodifyadmission_validation.js" type="text/javascript"></script>	-->
</html>