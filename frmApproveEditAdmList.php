<?php
$title = "Approve Learner Correction Before Final Exam Request";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['itgk'])) {

    echo "<script>var itgkcode='" . $_REQUEST['itgk'] . "'</script>";
    echo "<script>var coursevalue='" . $_REQUEST['course'] . "'</script>";
    echo "<script>var batchvalue='" . $_REQUEST['batch'] . "'</script>";
} else {
    echo "<script>var itgkcode='0'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Approve Learner Correction Before Final Exam Request</div>
            <div class="panel-body">

                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                        <div class="container">
                            <div id="grid" style="margin-top:25px; width:94%;"> </div>
                        </div>

                    </div>   
                </form>
                <div id="myModalupdate2" class="modal">

                    <div class="modal-content panel-danger" style=" margin-left: auto;width: 50%;  margin-right: auto; margin-top: 150px;">
                        <div class=" panel-heading">
                            <span class="close close2">&times;</span>
                            <h6>Warning</h6>
                        </div>
                        <div class="modal-body" style="max-height: 800px;" id='formhtml'>

                            <form name="frmCourseBatch1" id="frmCourseBatch1" class="form-horizontal" role="form"
                                  enctype="multipart/form-data" style="margin: 15px;">  
                                <div class="form-group" >

                                    <div class="col-sm-12" id="turmcandition">
                                        <p>Are you sure you want to Hold Application.</p>

                                    </div>
                                    <div class="col-sm-12" id="turmcandition">
                                        
                                        <label for="holdremark">Enter Remark for SMS to Learner:<span class="star">*</span></label>
                                        <input type="text" class="form-control text-uppercase" maxlength="500" name="holdremark" id="holdremark" placeholder="Remark">
                                    </div>

                                </div>
                                <div class="form-group last">
                                    <div class=" col-sm-6">
                                        <input type="button" name="btnOTPSubmitNo" id="btnOTPSubmitNo" class="btn-lg btn-warning" value="No"/>

                                    </div>
                                    <div class=" col-sm-6">
                                        <input type="button" name="btnOTPSubmit1"  id="btnOTPSubmit1" class="btn btn-lg btn-danger" value="Yes"/>

                                    </div>
                                </div>  
                            </form>

                        </div>
                    </div>
                </div>
                <div id="myModalupdate1" class="modal">

                    <div class="modal-content panel-danger" style=" margin-left: auto;width: 40%;  margin-right: auto; margin-top: 150px;">
                        <div class=" panel-heading">
                            <span class="close close2">&times;</span>
                            <h6>Warning</h6>
                        </div>
                        <div class="modal-body" style="max-height: 800px;" id='formhtml'>

                            <form name="frmCourseBatch1" id="frmCourseBatch1" class="form-horizontal" role="form"
                                  enctype="multipart/form-data" style="margin: 15px;">  
                                <div class="form-group" >

                                    <div class="col-sm-12" id="turmcandition">
                                        <p>Are you sure you want to Approve Application.</p>

                                    </div>
                                    

                                </div>
                                <div class="form-group last">
                                    <div class=" col-sm-6">
                                        <input type="button" name="btnOTPSubmitNo1" id="btnOTPSubmitNo1" class="btn-lg btn-warning" value="No"/>

                                    </div>
                                    <div class=" col-sm-6">
                                        <input type="button" name="btnOTPSubmitYes"  id="btnOTPSubmitYes" class="btn btn-lg btn-danger" value="Yes"/>

                                    </div>
                                </div>  
                            </form>

                        </div>
                    </div>
                </div>
                <div id="myModalupdate3" class="modal">

                    <div class="modal-content panel-danger" style=" margin-left: auto;width: 40%;  margin-right: auto; margin-top: 150px;">
                        <div class=" panel-heading">
                            <span class="close close2">&times;</span>
                            <h6>Warning</h6>
                        </div>
                        <div class="modal-body" style="max-height: 800px;" id='formhtml'>

                            <form name="frmCourseBatch1" id="frmCourseBatch1" class="form-horizontal" role="form"
                                  enctype="multipart/form-data" style="margin: 15px;">  
                                <div class="form-group" >

                                    <div class="col-sm-12" id="turmcandition">
                                        <p>Are you sure you want to Reject Application.</p>

                                    </div>
                                    <div class="col-sm-12" id="turmcandition">
                                        
                                        <label for="rejectrmk">Enter Remark for SMS to Learner:<span class="star">*</span></label>
                                        <input type="text" class="form-control text-uppercase" maxlength="500" name="rejectrmk" id="rejectrmk" placeholder="Remark">
                                    </div>
                                    

                                </div>
                                <div class="form-group last">
                                    <div class=" col-sm-6">
                                        <input type="button" name="btnOTPSubmitNo2" id="btnOTPSubmitNo2" class="btn-lg btn-warning" value="No"/>

                                    </div>
                                    <div class=" col-sm-6">
                                        <input type="button" name="btnOTPSubmitYes1"  id="btnOTPSubmitYes1" class="btn btn-lg btn-danger" value="Yes"/>

                                    </div>
                                </div>  
                            </form>

                        </div>
                    </div>
                </div>
                <div id="myModalupdateDoc" class="modal">

                    <div class="modal-content panel-danger" style=" margin-left: auto;width: 80%;  margin-right: auto; margin-top: 20px;">
                        <div class=" panel-heading">
                            <span class="close close2">&times;</span>
                            <h6>Doc Proof</h6>
                        </div>
                        <div class="modal-body" style="max-height: 550px;" id='formhtml'>

                            <iframe id="viewimagesrc" src="" style="width: 100%;height: 500px;border: none;"></iframe>



                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" data-dismiss="modal" id="modalcolsedoc">Close</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<style>
    .w3-tile {
        float: left;
        height: 30px;
        width: 110px;
        margin: 4px;
        text-align: center;
        padding-top: 5px;
        font-size: 12px;
    }
    .fancybox-slide--iframe .fancybox-content {
    width  : 800px;
    height : 800px;
    max-width  : 80%;
    max-height : 80%;
    margin: 0;
}
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            var url = "common/cfModifyAdmForAadhar.php"; // the script where you handle the form input.

            var data;

            //alert (role_type);
            data = "action=GETLEARNERLIST&itgkcode=" + itgkcode + "&batchcode=" + batchvalue + "&course=" + coursevalue + ""; //

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $('#btnSubmit').show();
                }
            });
        }
        showData();

        $("#grid").on("click",".view_photo",function(){
            
            var batchname = $(this).attr("batchname");
            var image = $(this).attr("image");
            var count = $(this).attr("count");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfModifyAdmForAadhar.php",
                data: "action=ViewPhoto&batchname=" + batchname + "&image=" + image + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    //$("#"+count+"_pic").show();
                    $("#"+count+"_photo").hide();
                    $("#Viewphoto_"+count).html(data);
                    $("#Viewphoto_"+count).show();

                }
            });
        });
        $("#grid").on("click",".view_photoafter",function(){
            
            var batchname = $(this).attr("batchname");
            var image = $(this).attr("image");
            var count = $(this).attr("count");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfModifyAdmForAadhar.php",
                data: "action=ViewPhotoAfter&batchname=" + batchname + "&image=" + image + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    //$("#"+count+"_pic").show();
                    $("#"+count+"_photoafter").hide();
                    $("#Viewphotoafter_"+count).html(data);
                    $("#Viewphotoafter_"+count).show();

                }
            });
        });        
        $("#grid").on("click",".view_photonoedit",function(){
            
            var batchname = $(this).attr("batchname");
            var image = $(this).attr("image");
            var count = $(this).attr("count");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfModifyAdmForAadhar.php",
                data: "action=ViewPhotonoedit&batchname=" + batchname + "&image=" + image + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    //$("#"+count+"_pic").show();
                    $("#"+count+"_photonoedit").hide();
                    $("#Viewphotonoedit_"+count).html(data);
                    $("#Viewphotonoedit_"+count).show();

                }
            });
        });
        $("#grid").on("click", ".approveadm", function () {

            var lcode1 = $(this).attr("id");
            var lcode = lcode1.substring(3);
            var itgkcode = $(this).attr("name");

            var modal3 = document.getElementById('myModalupdate1');
            modal3.style.display = "block";

            $("#btnOTPSubmitNo1").click(function () {
                var modal3 = document.getElementById('myModalupdate1');
                modal3.style.display = "none";
            });

            $("#btnOTPSubmitYes").click(function () {

            //alert(lcode);
            $('#' + lcode1).prop('disabled', true);
            $('#rej' + lcode).prop('disabled', true);
            //$('#'+lcode+"p").show();
            //$('#'+lcode).hide();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfModifyAdmForAadhar.php"; // the script where you handle the form input.            
            var data;

            data = "action=UpdateApproveLearner&lcode=" + lcode + "&itgkcode=" + itgkcode + ""; // serializes the form's elements.
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {    
                    var modal3 = document.getElementById('myModalupdate1');
                    modal3.style.display = "none";
                    //alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate || data == 'Successfully Inserted	' || data == 'Successfully Updated	')
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmWcdLearnerCountList.php?batch=" + batch + "&itgk=" + itgkcode + "&districtcode=" + districtcode + "&mode=one";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span>          <span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });
            });

        });



        $("#grid").on("click", ".rejectadm", function () {

            var lcode1 = $(this).attr("id");
            var lcode = lcode1.substring(3);
            var itgkcode = $(this).attr("name");

            var modal2 = document.getElementById('myModalupdate2');
            modal2.style.display = "block";

            $("#btnOTPSubmitNo").click(function () {
                var modal2 = document.getElementById('myModalupdate2');
                modal2.style.display = "none";
            });

            $("#btnOTPSubmit1").click(function () {
                var onholdrmk = ($("#holdremark").val()).trim();
                if(onholdrmk == ''){
                    alert("Remarks is Mandatory.");
                }
                else{
                    var modal2 = document.getElementById('myModalupdate2');
                    modal2.style.display = "none";
                    $('#' + lcode1).prop('disabled', true);
                    $('#app' + lcode).prop('disabled', true);
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    var url = "common/cfModifyAdmForAadhar.php"; // the script where you handle the form input.            
                    var data;

                    data = "action=UpdateRejectLearner&lcode=" + lcode + "&itgkcode=" + itgkcode + "&onholdrmk=" + onholdrmk + ""; // serializes the form's elements.
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data)
                        {
                            //alert(data);
                            if (data == SuccessfullyInsert || data == SuccessfullyUpdate || data == 'Successfully Inserted  ' || data == 'Successfully Updated  ')
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                window.setTimeout(function () {
                                    window.location.href = "frmWcdLearnerCountList.php?batch=" + batch + "&itgk=" + itgkcode + "&districtcode=" + districtcode + "&mode=one";
                                }, 1000);

                                Mode = "Add";
                                resetForm("form");
                            } else
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span>          <span>" + data + "</span></p>");
                            }
                            //showData();


                        }
                    });
                    
                }


            });



        });
        $("#grid").on("click", ".rejectapplication", function () {

            var lcode1 = $(this).attr("id");
            var lcode = lcode1.substring(6);
            var itgkcode = $(this).attr("name");

            var modal3 = document.getElementById('myModalupdate3');
            modal3.style.display = "block";

            $("#btnOTPSubmitNo2").click(function () {
                var modal3 = document.getElementById('myModalupdate3');
                modal3.style.display = "none";
            });

            $("#btnOTPSubmitYes1").click(function () {
                var rejectrmk = ($("#rejectrmk").val()).trim();
                if(rejectrmk == ''){
                    alert("Remarks is Mandatory.");
                }
                else{
            //alert(lcode);
            $('#' + lcode1).prop('disabled', true);
            $('#reject' + lcode).prop('disabled', true);
            //$('#'+lcode+"p").show();
            //$('#'+lcode).hide();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfModifyAdmForAadhar.php"; // the script where you handle the form input.            
            var data;
            data = "action=FinalRejectLearner&lcode=" + lcode + "&itgkcode=" + itgkcode + "&rejectrmk=" + rejectrmk + "";
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {    
                    var modal3 = document.getElementById('myModalupdate3');
                    modal3.style.display = "none";
                    //alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate || data == 'Successfully Inserted  ' || data == 'Successfully Updated  ')
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmWcdLearnerCountList.php?batch=" + batch + "&itgk=" + itgkcode + "&districtcode=" + districtcode + "&mode=one";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span>          <span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });
            }
            });

        });
        $("#grid").on("click", ".docadm", function () {

            var lcode = $(this).attr("id");
           
            var itgkcode = $(this).attr("name");

            var modal4 = document.getElementById('myModalupdateDoc');
            modal4.style.display = "block";

            $("#viewimagesrc").attr('src', "upload/AdmissionCorrectionAfterPayment/" + lcode + "_DocProof.pdf");

        });
        $("#modalcolsedoc").click(function () {
            var modal4 = document.getElementById('myModalupdateDoc');
            modal4.style.display = "none";
         });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</html>
