<?php
$title="Organisation Details";
include ('header.php'); 
include ('root_menu.php'); 

    if (isset($_REQUEST['code'])) {
                echo "<script>var OrgCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var OrgCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
        <div class="container"> 
			 

            <div class="panel panel-primary" style="margin-top:46px !important;">

                <div class="panel-heading">Organization  Details</div>
                <div class="panel-body">
                    
                    <form name="frmupdateorgdetails" id="frmupdateorgdetails" action="" class="form-inline">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Organization Name:<font color="red">*</font></label>
                                <input type="text" class="form-control" maxlength="50" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center" >
								<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                            </div>


                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Registration No:<font color="red">*</font></label>
                                <input type="text" class="form-control" maxlength="50"  name="txtRegno" id="txtRegno" placeholder="Registration No">     
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:<font color="red">*</font></label>
                                <input type="text" class="form-control" maxlength="50" name="txtEstdate" id="txtEstdate"  placeholder="DD-MM-YY" readonly='true'>
								
                            </div>
							
							 <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:<font color="red">*</font></label>
                                <select id="txtType" name="txtType" class="form-control" >
								  <option selected="true"value="">Select</option>
									<option value="1" >
										RS-CIT
									</option>
                                </select>    
                            </div>
							
							</div>
							
							
							
							
						<div class="container">

								
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Country:<font color="red">*</font></label>
                                <select id="ddlCountry" name="ddlCountry" class="form-control" >
								  <option selected="true"value="">Select</option>
									<option value="India" >
										India
									</option>
                                </select>    
                            </div>
							
								
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">State:<font color="red">*</font></label>
                                <select id="ddlState" name="ddlState" class="form-control" >
								 
                                </select>    
                            </div>
							
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Divison:<font color="red">*</font></label>
                                <select id="ddlRegion" name="ddlRegion" class="form-control" >
								 
                                </select>    
                            </div>
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:<font color="red">*</font></label>
                                <select id="ddlDistrict" name="ddlDistrict" class="form-control" >
								 
                                </select>    
                            </div>
							
							
						</div>	
							
							
						<div class="container">
							
						
							
							
						
								
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Tehsil:<font color="red">*</font></label>
                                <select id="ddlTehsil" name="ddlTehsil" class="form-control" >
								 
                                </select>    
                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address">House No:<font color="red">*</font></label>
                                <input type="text" class="form-control" maxlength="12" name="txtHouseno" 
									id="txtHouseno" placeholder="House No"  >
                            </div>
							
							
							 <div class="col-sm-4 form-group"> 
                                <label for="address">Street:<font color="red">*</font></label>
                                <input type="text" class="form-control" name="txtStreet" id="txtStreet" placeholder="Street" >    
                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address">Nearest Landmark:<font color="red">*</font></label>
                                <input type="text" class="form-control" maxlength="12" name="txtLandmark" 
									id="txtLandmark" placeholder="Nearest Landmark"  >

                            </div>
							
							
							 
					</div>
							
					<div class="container">	
							
						
							
					</div>		
					
					<div class="container">
					 <div class="col-sm-4 form-group">     
                                <label for="address">Road:<font color="red">*</font></label>
                                <textarea class="form-control" rows="4" id="txtRoad" name="txtRoad" placeholder="Road" ></textarea>

                            </div>
							
					<div class="col-sm-4 form-group">     
                                <label for="address">Address:<font color="red">*</font></label>
                                <textarea class="form-control" rows="4" id="txtAddress" name="txtAddress" placeholder="Address" ></textarea>

                            </div>
					</div>

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style='margin-left:10px'/>    
                        </div>
						
						
						
						<div id="gird"></div>
                 </div>
            </div>   
        </div>


    </form>





</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>


    
	<script type="text/javascript"> 
 $('#txtEstdate').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true,
		autoclose: true
	});  
	
	
	
	</script>
 <script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
						if (Mode == 'Delete')
							{
								if (confirm("Do You Want To Delete This Item ?"))
								{
		                   		 deleteRecord();
		               		}
		           		 }
		           else if (Mode == 'Edit')
		           {
		                fillForm();
		           }
				   
				  
				   
			 
				 /*   function FillDobdocs() {
            $.ajax({
                type: "post",
                url: "common/cfOrgDetail.php",
                data: "action=FillDobdocs",
                success: function (data) {
                    $("#ddlDobProof").html(data);
                }
            });
        }
        FillDobdocs(); */
				   
				   function GenerateUploadId()
                   {
					$.ajax({
						type: "post",
						url: "common/cfBlockUnblock.php",
						data: "action=GENERATEID",
						success: function (data) {                      
							txtGenerateId.value = data;					
						}
					});
                  }
                  GenerateUploadId();
					function deleteRecord()
					{
		               $('#response').empty();
		                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		               $.ajax({
	                    type: "post",
		                    url: "common/cfmodifyorgdetails.php",
		                   data: "action=DELETE&values=" + OrgCode + "",
		                   success: function (data) {
		                       //alert(data);
		                        if (data == SuccessfullyDelete)
		                        {
		                            $('#response').empty();
		                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
		                           window.setTimeout(function () {
		                               window.location.href="frmmodifyorgdetails.php";
		                          }, 1000);
		                            
		                            Mode="Add";
		                            resetForm("frmmodifyorgdetails");
		                        }
		                        else
		                        {
		                            $('#response').empty();
		                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
		                        }
		                       showData();
		                    }
		               });
		            }
		
		
		            function fillForm()
		            {
		                $.ajax({
		                    type: "post",
		                    url: "common/cfmodifyorgdetails.php",
		                    data: "action=EDIT&values=" + OrgCode + "",
		                    success: function (data) {
		                        //alert($.parseJSON(data)[0]['Status']);
		                       data = $.parseJSON(data);
							   //alert(data);
		                        txtName1.value = data[0].Organization_Name;
		                        txtRegno.value = data[0].Organization_RegistrationNo;
								txtEstdate.value = data[0].Organization_FoundedDate;
		                        txtType.value = data[0].Organization_Type;
		                        ddlState.value = data[0].Organization_State;
								ddlRegion.value = data[0].Organization_Region;
		                        ddlDistrict.value = data[0].Organization_District;
								ddlTehsil.value = data[0].Organization_Tehsil;
		                        
								txtRoad.value = data[0].Organization_Road;
								txtStreet.value = data[0].Organization_Street;
		                        txtHouseno.value = data[0].Organization_HouseNo;
								ddlCountry.value = data[0].Organization_Country;
		                        txtLandmark.value = data[0].Organization_Landmark;
								txtAddress.value = data[0].Organization_Address;
								ddlDobProof.value = data[0].Organization_DocType;
								 
		                        
		                    }
		                });
		            }
					
				
		
		            
		
		
						function FillCourse() {
						$.ajax({
							type: "post",
							url: "common/cfCourseMaster.php",
							data: "action=FILLCourseName",
							success: function (data) {
							$("#txtCourse").html(data);
							}
						});
						}

            FillCourse();	
		
		
		

                            $(function() {
                                $( "#txtEstdate" ).datepicker({
                                  changeMonth: true,
                                  changeYear: true
                                });
                              });
                              function FillOrgType() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfOrgTypeMaster.php",
                                    data: "action=FILL",
                                    success: function (data) {
                                        $("#txtType").html(data);
                                    }
                                });
                            }
                             FillOrgType();
							 
							  $("#txtType").change(function(){
				var selorgtype = $(this).val(); 
				//alert(selcountry);
				$.ajax({
			          url: 'common/cfOrgDetail.php',
			          type: "post",
			          data: "action=FILL",
			          success: function(data){
						//alert(data);
						$('#txtDoctype').html(data);
			          }
			        });
                            });
							 
							 
                              
                            function FillParent() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfCountryMaster.php",
                                    data: "action=FILL",
                                    success: function (data) {
                                        $("#ddlCountry").html(data);
                                    }
                                });
                            }
                             FillParent();
                             
                              $("#ddlCountry").change(function(){
				var selcountry = $(this).val(); 
				//alert(selcountry);
				$.ajax({
			          url: 'common/cfStateMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selcountry + "",
			          success: function(data){
						//alert(data);
						$('#ddlState').html(data);
			          }
			        });
                            });
            
           
                            $("#ddlState").change(function(){
				var selState = $(this).val(); 
				//alert(selState);
				$.ajax({
			          url: 'common/cfRegionMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selState + "",
			          success: function(data){
						//alert(data);
						$('#ddlRegion').html(data);
			          }
			        });
                             });
                            $("#ddlRegion").change(function(){
				var selregion = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfDistrictMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selregion + "",
			          success: function(data){
						//alert(data);
						$('#ddlDistrict').html(data);
			          }
			        });
                            });
                            $("#ddlDistrict").change(function(){
				var selDistrict = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfTehsilMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selDistrict + "",
			          success: function(data){
						//alert(data);
						$('#ddlTehsil').html(data);
			          }
			        });
                            });
							
							
							
			$("#btnSubmit").click(function () {
			
			if ($("#frmupdateorgdetails").valid())
            {
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfmodifyorgdetails.php"; // the script where you handle the form input.
				var birthproof=$('#txtGenerateId').val(); 
                var data;
                var forminput=$("#frmupdateorgdetails").serialize();
                if (Mode == 'Add')
                {
					//alert(1);
                    data = "action=UPDATE&birthproof=" + birthproof + "&" + forminput; // serializes the form's elements.
                }
                else
                {
				//alert(2);	
					data = "action=UPDATE&code=" + OrgCode +"&" + forminput;
                    //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                             window.setTimeout(function () {
                                window.location.href = "frmmodifyorgdetails.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmmodifyorgdetails");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			 }
                return false; // avoid to execute the actual submit of the form.
            });

                            
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmorgmastervalidation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>