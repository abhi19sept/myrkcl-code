<?php
$title = "Message Master";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var UserParentID=" . $_SESSION['User_ParentId'] . "</script>";
    echo "<script>var UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
    echo "<script>var MessageCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var UserParentID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var MessageCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:300px !important;min-height:500px !important">
    
<div class="container"> 

    <div class="panel panel-primary" style="margin-top:36px !important;">  
        <div class="panel-heading">Message Board</div>
        <div class="panel-body">

            <form name="frmmessagemaster" id="frmmessagemaster" class="form-inline" role="form" enctype="multipart/form-data" action="">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>
                    


                    

                   

                    
                    
                    <div class="container">
                        <div id="gird" style="margin-top:25px; width:94%;"> </div>
                    </div>
                    
                    <!--					<div class="container">
                                                                    <div class="container">
                                                                            <div id="response"></div>
                                                                    </div>        
                                                                            <div id="errorBox"></div>
                                                                                    <div bgcolor="CCCCCC" align="left" colspan="3">     
                                                                                            Learner List
                                                                                    </div>
                                                    
                                                                            <div id="gird" style="margin-top:25px;"> </div>	
                                                            </div>-->
                </div>   
            </form>
        </div>
    </div>
</div>
    </div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>



<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

//        if (Mode == 'Delete')
//        {
//            if (confirm("Do You Want To Delete This Item ?"))
//            {
//                deleteRecord();
//            }
//        }
//        else if (Mode == 'Edit')
//        {
//            fillForm();
//        }

//                        function FillStatus() {
//                            $.ajax({
//                                type: "post",
//                                url: "common/cfStatusMaster.php",
//                                data: "action=FILL",
//                                success: function (data) {
//                                    $("#ddlStatus").html(data);
//                                }
//                            });
//                        }
//
//                        FillStatus();




        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfMessageMaster.php",
                data: "action=DELETE&values=" + MessageCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmmessagemaster.php";
                        }, 1000);
                        Mode = "Add";
                        resetForm("frmmessagemaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        function fillForm()
        {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfMessageMaster.php",
                data: "action=EDIT&values=" + MessageCode + "",
                success: function (data) {
                    //alert(data);
                    //alert($.parseJSON(data)[0]['Status']);
                    data = $.parseJSON(data);
                    txtmessage.value = data[0].Name;
                    RoleITGK.value = data[0].Role;
                    txtstartdate.value = data[0].StartDate;
                    txtenddate.value = data[0].EndDate;
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfMessageMaster.php",
                data: "action=SHOWMSG",
                success: function (data) {
                    //alert(data);
                    $("#gird").html(data);
                    $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
            });

                }
            });
        }

        showData();


        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfMessageMaster.php"; // the script where you handle the form input.
            if (document.getElementById('RoleRM').checked) {
                role_type = '4';
            }
            if (document.getElementById('RolePSA').checked) {
                role_type = '5';
            }
            else if (document.getElementById('RoleDLC').checked) {
                role_type = '6';
            }
            else if (document.getElementById('RoleITGK').checked) {
                role_type = '7';
            }
            var startdate = $('#txtstartdate').val();
            var enddate = $('#txtenddate').val();
            var data;
            
            if (Mode == 'Add')
            {
                alert("hi");
                data = "action=ADD&name=" + txtmessage.value + "&role=" + role_type + "&startdate=" + startdate + "&enddate=" + enddate + ""; // serializes the form's elements.
                alert(data);
    }
            else
            {
                data = "action=UPDATE&code=" + MessageCode + "&name=" + txtmessage.value + "&role=" + role_type + "&startdate=" + startdate + "&enddate=" + enddate + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        //alert(data);
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmmessagemaster.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmmessagemaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });
</script>


<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
