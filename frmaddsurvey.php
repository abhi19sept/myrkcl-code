<?php
$title="Survey Master ";
include ('header.php'); 
include ('root_menu.php');  

 if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
			if ($_SESSION['User_Code'] == '1') {	
            ?>
			
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

        <div class="container" > 
			  

            <div class="panel panel-primary" style="margin-top:46px !important;" >

                <div class="panel-heading" id="non-printable">Create Survey</div>
                <div class="panel-body">
				
			 
                    <!-- <div class="jumbotron"> -->
                    <form name="frmaddsurvey" id="frmaddsurvey" action="" class="form-inline">     

                        
                            <div class="container" >
							
							<div id="response"></div>	
							
							<div id="errorBox"></div>
							   <div class="col-sm-4 form-group"> 
                                <label for="fname">Survey Name:<span class="star">*</span></label>
                              
<textarea class="form-control" rows="3" id="txtSurvey" name="txtSurvey" placeholder="Survey Name" ></textarea>
																
                            </div>
						</div>
							
							<div class="container">
							
							 <div class="col-sm-4 form-group">     
                                <label for="dob">Survey Start Date:<span class="star">*</span></label>								
										<input type="text" class="form-control" readonly="true" maxlength="50" name="txtStartdate" id="txtStartdate"  placeholder="YYYY-MM-DD">
							</div>
							
							</div>
							
							
							
							
							<div class="container">
							
							 <div class="col-sm-4 form-group">     
                                <label for="dob">Survey End Date:<span class="star">*</span></label>								
										<input type="text" class="form-control" readonly="true" maxlength="50" name="txtEnddate" id="txtEnddate"  placeholder="YYYY-MM-DD">
							</div>
							
							</div>
							
							
								

 

                        <div class="container" id="non-printable">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						<div id='gird'></div>
						
						
						
						
					
					 </form> 
				 
				</div>
					
            </div>   
        </div>
  
				
	</body>	

	
<?php include ('footer.php'); ?>				
<?php include'common/message.php';?>


<style>
#errorBox{
 color:#F00;
 }
</style>


<script type="text/javascript"> 
 $('#txtStartdate').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true,
		autoclose: true
	});  
	
	
	 $('#txtEnddate').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true,
		autoclose: true
	}); 
	</script>
<script type="text/javascript">
		var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
		var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
		var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
		var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
		$(document).ready(function () {
		 if (Mode == 'Delete')
		            {
		                if (confirm("Do You Want To Delete This Item ?"))
		                {
		                    deleteRecord();
		                }
		            }
		            else if (Mode == 'Edit')
		            {
		                fillForm();
		            }
					
					
					
					$("#txtEnddate").change(function () {
    var startDate = document.getElementById("txtStartdate").value;
    var endDate = document.getElementById("txtEnddate").value;
 
    if ((Date.parse(startDate) >= Date.parse(endDate))) {
        alert("End date should be greater than Start date");
        document.getElementById("datepicker1").value = "";
    }
});
					
					
					
					
					 function deleteRecord()
		            {
		                $('#response').empty();
		                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		                $.ajax({
		                    type: "post",
		                    url: "common/cfaddsurvey.php",
		                    data: "action=DELETE&values=" + Code + "",
		                    success: function (data) {
		                        //alert(data);
		                        if (data == SuccessfullyDelete)
		                        {
		                            $('#response').empty();
		                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
		                            window.setTimeout(function () {
		                          window.location.href="frmaddsurvey.php";
		                           }, 1000);
		                            
		                            Mode="Add";
		                            resetForm("frmaddsurvey");
		                        }
		                        else
		                        {
		                            $('#response').empty();
		                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
		                        }
		                        showData();
		                    }
		                });
		            }
		
		function showData() {
		                
		                $.ajax({
		                    type: "post",
		                    url: "common/cfaddsurvey.php",
		                    data: "action=SHOW",
		                    success: function (data) {
		
		                        $("#gird").html(data);
		
		                    }
		                });
		            }
					showData();
		

		     $("#btnSubmit").click(function () {
			 if ($("#frmaddsurvey").valid())
                {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfaddsurvey.php"; // the script where you handle the form input.
               var data;
                var forminput=$("#frmaddsurvey").serialize();
                if (Mode == 'Add')
                {
                    data = "action=ADD&" + forminput; // serializes the form's elements.
                }
                else
                {
					
					data = "action=UPDATE&code=" + Examcode + "&" + forminput;
                    //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                             window.setTimeout(function () {
                                window.location.href = "frmaddsurvey.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmaddsurvey");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
		   }
                return false; // avoid to execute the actual submit of the form.
            });
		    function resetForm(formid) {
		        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
		    }
		
		});
		
	</script>

		
		<style>
.error {
	color: #D95C5C!important;
}
</style>
 <script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmaddsurvey_validation.js"></script>
</html>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>