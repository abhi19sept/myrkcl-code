<?php
$title = "OutGoing Centre Report - SP Switching";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var ItPeripheralsCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var DeviceCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading"> OutGoing Approved Centre Report - SP Switching

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
                <form name="frmSwitchReport" id="frmSwitchReport" class="form-inline" action=""> 

                    <br>

                    <div id="gird"></div>
                </form>

            </div>

        </div>   
    </div>

</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
            var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                    $(document).ready(function () {


                        function showData() {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                            $.ajax({
                                type: "post",                 url: "common/cfSwitchReport.php",
                                data: "action=SHOW",
                                success: function (data) {
                                    $('#response').empty();
                                    $("#gird").html(data);
                                    $('#example').DataTable({
                                        dom: 'Bfrtip',
                                        buttons: [
                                        'copy', 'csv', 'excel', 'pdf', 'print'                         ]
                                    });



                                }
                            });
                        }

                        showData();

                        function resetForm(formid) {
                            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                        }

    });
                    
</script>
</html>