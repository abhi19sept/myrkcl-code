  <?php
  $txnid = (isset($_POST['txnid'])) ? trim($_POST['txnid']) : '';
  $len = strlen($txnid);
  if (empty($txnid) || $len != 20) {
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
  } else {
    echo "<script>var REFERER = '" . $_SERVER['HTTP_REFERER'] . "'</script>";
    echo "<script>var txnid = '" . $txnid . "'</script>";
  }
  ?>
  <html>
    <head>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <link rel="shortcut icon" href="../favicon.ico">          
      <link rel="stylesheet" href="bootcss/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="bootcss/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="assets/header-search.css">
      <link rel="stylesheet" href="assets/footer-distributed-with-address-and-phones.css">
      <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
      <script src="bootcss/js/jquery.dataTables.min.js"></script>
      <script src="bootcss/js/dataTables.bootstrap.min.js"></script>
      <link rel="stylesheet" href="bootcss/css/dataTables.bootstrap.min.css">
      <script type="text/javascript" src="scripts/validate.js"></script>
      <link rel="stylesheet" href="css/style.css">
      <script src="scripts/Menu.js" type="text/javascript"></script>
      <link href="bootcss/css/tab_style.css" rel="stylesheet" type="text/css" />
      <link href="css/extra.css" rel="stylesheet" type="text/css" />
      <script>
        $(document).ready(function() {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });
    </script>
    <style>
      .form-inline .form-group {
          display: inline-block;
          max-width:100% !important;
          vertical-align: middle;
      }
      .form-control {
       
          width: 85% !important;
      }
      #errorBox{
          color:#F00;
      }
    </style>
    </head>
    <body>
      <div id="loader-wrapper">
          <div id="loader"></div>
      </div>
      <?php include ('root_menu.php'); ?>
      <div style="min-height:430px !important;max-height:auto !important;">
        <div class="container">
          <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Payment Transaction Details</div>
              <div class="panel-body">
                <form action="" method="post" name="payuForm" id="payuForm" class="form-inline" role="form">
                  <div class="container">
                    <div id="response"></div>
                  </div>
                  <div id="errorBox"></div>
                  <div class="container">
                    <div class="col-sm-3 form-group">     
                      <label for="learnercode">ITGK Code:</label>
                        <input type="text" readonly="true" class="form-control" maxlength="50" name="udf1" id="udf1">
                    </div>
                    <div class="col-sm-3 form-group"> 
                      <label for="ename">ITGK Name:</label>
                      <input type="text" readonly="true" class="form-control text-uppercase" maxlength="255" name="firstname" id="firstname" placeholder="Owner Name">
                    </div>
                    <div class="col-sm-3 form-group">     
                      <label for="faname">Mobile No:</label>
                      <input type="text" readonly="true" class="form-control" maxlength="50" name="phone" id="phone" placeholder="Owner Mobile">
                    </div>
                    <div class="col-sm-3 form-group">     
                      <label for="faname">Email ID:</label>
                      <input type="text" readonly="true" class="form-control" maxlength="50" name="email" id="email" placeholder="Owner Email">
                    </div>
                  </div>
                  <div class="container">
                    <div class="col-sm-3 form-group">     
                      <label for="learnercode">Payment Type:</label>
                        <input type="text" readonly="true" class="form-control" name="productinfo" id="productinfo"/>
                    </div>
                    <div class="col-sm-3 form-group"> 
                      <label for="ename">Transaction Reference No:</label>
                      <input type="text" readonly="true" class="form-control" maxlength="50" name="transaction" id="transaction" placeholder="Tran. Ref. No:">
                      <input type="hidden" name="udf2" id="udf2"/>
                    </div>
                    <div class="col-sm-3 form-group">     
                      <label for="faname">Amount:</label>
                      <input type="text" readonly="true" class="form-control" maxlength="50" name="amount" id="amount" placeholder="Total Amount">
                    </div>
                  </div>
                   <div class="container">
                    <div class="row">
                      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab-container">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 bhoechie-tab-menu">
                          <div class="list-group">
                            <a href="#" id="tab1" data-index="1" name="tab" class="list-group-item active"> <img src="images/credit_card.png" alt="#" /> &nbsp; Credit Card</a> 
                            <a href="#" id="tab2" data-index="2" name="tab" class="list-group-item"> <img src="images/net_banking.png" alt="#" /> &nbsp; Net Banking </a>
                            <a href="#" id="tab3" data-index="3" name="tab" class="list-group-item"> <img src="images/credit_card.png" alt="#" /> &nbsp; Debit Cart </a>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-8 bhoechie-tab">
                        <!-- CREDIT CARD FORM STARTS HERE -->
                        <div class="bhoechie-tab-content active">
                          <div class="panel panel-default credit-card-box">
                            <div class="panel-heading display-table" >
                              <div class="row display-tr" >
                                <h3 class="panel-title display-td" >Payment Details</h3>
                                <div class="display-td" > <img class="img-responsive pull-right" src="images/accepted_c22e0.png"> </div>
                              </div>
                            </div>
                            <div class="panel-body">
                              <div class="row">
                                <div class="col-xs-12 form-group">     
                                  <label for="ccnum">Card Number:</label>
                                  <input type="text" class="form-control" maxlength="19" name="creditnum" id="creditnum" placeholder="CCNUM" onkeypress="mycreditvalidate(); javascript:return allownumbers(event);">
                                  <i class="fa fa-credit-card"></i>
                                </div>    
                              </div>
                    <div class="row">
                      <div class="col-xs-6 col-md-6">
                        <div class="form-group" style="width:100%;">
                          <label for="ccexpmon"><span class="hidden-xs">EXPIRATION Month</span></label>
                                            <select name="creditmonth" id="creditmonth" class="selectbox form-control">
                                                  <option value=''> Select Month </option>
                                                  <option value='01'>Janaury</option>
                                                  <option value='02'>February</option>
                                                  <option value='03'>March</option>
                                                  <option value='04'>April</option>
                                                  <option value='05'>May</option>
                                                  <option value='06'>June</option>
                                                  <option value='07'>July</option>
                                                  <option value='08'>August</option>
                                                  <option value='09'>September</option>
                                                  <option value='10'>October</option>
                                                  <option value='11'>November</option>
                                                  <option value='12'>December</option>
                                            </select>                                  
                        </div>
                      </div>
                                <div class="col-xs-3 col-md-3">
                        <div class="form-group">
                          <label for="ccexpyr"><span class="hidden-xs">EXPIRATION Year</span></label>
                                            <select name="credityear" id="credityear" class="selectbox form-control">
                           
                                            </select>
                           
                        </div>
                      </div>
                      <div class="col-xs-3 col-md-3 pull-right">
                        <div class="form-group">
                          <label for="ccvv">CVV CODE</label>
                           <input type="text" class="form-control" maxlength="3" name="creditcvv" id="creditcvv" placeholder="CVV Code" onkeypress="javascript:return allownumbers(event);"/>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12 form-group">     
                                  <label for="ccnum">Card Name:</label>
                                  <input type="text" class="form-control" maxlength="50" name="creditname" id="creditname" placeholder="Card Name" style="text-transform:uppercase" onkeypress="javascript:return allowchar(event);">

                              </div>
                    </div>
                    <div class="row" style="display:none;">
                      <div class="col-xs-12">
                        <p class="payment-errors"></p>
                      </div>
                    </div>
                </div>
              </div>
            
          </div>
          <!-- CREDIT CARD FORM ENDS HERE -->
              <!-- NET BANKING BLOG START HERE -->
          <div class="bhoechie-tab-content">
            
              <div class="popubankblog">
                    <div class="panel panel-default credit-card-box">
              <div class="panel-heading display-table" >
                  <div class="row display-tr" >
                    <h3 class="panel-title display-td" >Popular Banks</h3>
                                <div class="display-td" > <img class="img-responsive pull-right" src="images/banklogo1.png"> </div>
                  </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                      <div class="col-xs-12 mb20">
                        <div class="select-net-bank-wr"> <span class="all-banks-txt">All banks</span>
                          <select name="ddlbankname" id="ddlbankname" class="selectbox form-control">
                           
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row" style="display:none;">
                      <div class="col-xs-12">
                        <p class="payment-errors"></p>
                      </div>
                    </div>
                </div>
              </div>
           </div>
          </div>
              <!-- NET BANKING BLOG END HERE -->
         
            <!-- CREDIT CARD FORM STARTS HERE -->
            <div class="bhoechie-tab-content">
            
              <div class="panel panel-default credit-card-box">
                <div class="panel-heading display-table" >
                  <div class="row display-tr" >
                    <h3 class="panel-title display-td" >Payment Details</h3>
                    <div class="display-td" > <img class="img-responsive pull-right" src="images/accepted_c22e0.png"> </div>
                  </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                            <div class="col-xs-12 form-group">     
                                  <label for="ccnum">Card Number:</label>
                                  <input type="text" class="form-control" maxlength="19" name="debitnum" id="debitnum" placeholder="CCNUM" onkeypress="mydebitvalidate(); javascript:return allownumbers(event);">
                                                  <i class="fa fa-credit-card"></i>
                              </div>
                                            
                                            
                    </div>
                    <div class="row">
                      <div class="col-xs-6 col-md-6">
                        <div class="form-group" style="width:100%;">
                          <label for="ccexpmon"><span class="hidden-xs">EXPIRATION Month</span></label>
                                      <select name="debitmonth" id="debitmonth" class="selectbox form-control">
                                                  <option value=''> Select Month </option>
                                                  <option value='01'>Janaury</option>
                                                  <option value='02'>February</option>
                                                  <option value='03'>March</option>
                                                  <option value='04'>April</option>
                                                  <option value='05'>May</option>
                                                  <option value='06'>June</option>
                                                  <option value='07'>July</option>
                                                  <option value='08'>August</option>
                                                  <option value='09'>September</option>
                                                  <option value='10'>October</option>
                                                  <option value='11'>November</option>
                                                  <option value='12'>December</option>
                                            </select> 
                            
                        </div>
                      </div>
                                <div class="col-xs-3 col-md-3">
                        <div class="form-group">
                          <label for="ccexpyr"><span class="hidden-xs">EXPIRATION Year</span></label>
                                      <select name="debityear" id="debityear" class="selectbox form-control">
                           
                                            </select>
                           
                        </div>
                      </div>
                      <div class="col-xs-3 col-md-3 pull-right">
                        <div class="form-group">
                          <label for="ccvv">CVV CODE</label>
                           <input type="text" class="form-control" maxlength="3" name="debitcvv" id="debitcvv" placeholder="CVV Code" onkeypress="javascript:return allownumbers(event);"/>
                        </div>
                      </div>
                      </div>
                   
                            
                    <div class="row">
                      <div class="col-xs-12 form-group">     
                                  <label for="ccnum">Card Name:</label>
                                  <input type="text" class="form-control" maxlength="50" name="debitname" id="debitname" placeholder="Card Name" style="text-transform:uppercase" onkeypress="javascript:return allowchar(event);">

                              </div>
                    </div>
                   
                    <div class="row" style="display:none;">
                      <div class="col-xs-12">
                        <p class="payment-errors"></p>
                      </div>
                    </div>
                             </div>
                </div>
              </div>
            
          </div>
        </div>
      </div>
    </div>
                  <div class="container">
                    <p><center>
                      <input type="button" name="btnPay" id="btnPay" class="btn btn-primary" value="Pay Now"/> 
                    </center></p>
                    <input type="hidden" name="paymethod" id="paymethod" value="credit"/>
                    <input type="hidden" name="cardtype" id="cardtype"/>
                    <input type="hidden" name="cardcategory" id="cardcategory" />
                    <input type="hidden" name="pg" id="pg" />
                    <input type="hidden" name="bankcode" id="bankcode"/>
                    <input type="hidden" name="ccname" id="ccname"/>
                    <input type="hidden" name="ccnum" id="ccnum"/>
                    <input type="hidden" name="ccvv" id="ccvv"/>
                    <input type="hidden" name="ccexpmon" id="ccexpmon"/> 
                    <input type="hidden" name="ccexpyr" id="ccexpyr"/>
                    <input type="hidden" name="txnid" id="txnid" />
                    <input type="hidden" name="udf3" id="udf3" />
                    <input type="hidden" name="udf4" id="udf4" />
                    <div id="gateway"></div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="show_processing" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
          <div style="margin-top: 100px;"><center><img src="images/double-ring-spinner.gif"></center></div>
        </div>
      </div>
    </body>
    <?php include'common/message.php'; ?>

<script type="text/javascript">
  $(document).ready(function () {
    $("a[name=tab]").on("click", function () {
      var a = $(this).data("index");
      if (a == 1) {
        var cc = 'credit';
        paymethod.value = cc;
      } else if (a == 2) {
        var nb = 'netbank';
        paymethod.value = nb;
      } else if (a == 3) {
        var dc = 'debit';
        paymethod.value = dc;
      }
    });

    var min = new Date().getFullYear(),
    max = min + 30,
    select = document.getElementById('credityear');
    for (var i = min; i<=max; i++) {
      var opt = document.createElement('option');
      opt.value = i;
      opt.innerHTML = i;
      select.appendChild(opt);
    }

    select = document.getElementById('debityear');
    for (var i = min; i<=max; i++) {
      var opt = document.createElement('option');
      opt.value = i;
      opt.innerHTML = i;
      select.appendChild(opt);
    }

    showData();
    FillNetBankingName();

    $("#ddlbankname").change(function () {
      var selBankName = $(this).val();
      pg.value = 'NB';
      bankcode.value = selBankName;
    });         
        
    $("#paytype").change(function () {
      var PayType = document.getElementById('paytype').value;
      document.getElementById('pg').value = PayType;
    });

    $("#btnPay").click(function () {
      var method = $('#paymethod').val();
      if(method == 'credit') {
        var creditnum = document.getElementById('creditnum').value;
        var creditmnth = document.getElementById('creditmonth').value;
        var credityear = document.getElementById('credityear').value;
        var creditcvv = document.getElementById('creditcvv').value;
        var creditname = document.getElementById('creditname').value;
        var creditpg = document.getElementById('cardcategory').value;
        var creditbankcode = document.getElementById('cardtype').value;

        pg.value = creditpg;
        bankcode.value = creditbankcode;        
        ccnum.value = creditnum;
        ccexpmon.value = creditmnth;
        ccexpyr.value = credityear;
        ccvv.value = creditcvv;
        ccname.value = creditname;    
      } else if(method == 'netbank') {
        var nb = 'NB';
        pg.value = nb;
      } else if (method == 'debit') {
        var creditnum = document.getElementById('debitnum').value;
        var creditmnth = document.getElementById('debitmonth').value;
        var credityear = document.getElementById('debityear').value;
        var creditcvv = document.getElementById('debitcvv').value;
        var creditname = document.getElementById('debitname').value;
        var creditpg = document.getElementById('cardcategory').value;
        var creditbankcode = document.getElementById('cardtype').value;
        
        pg.value = creditpg;
        bankcode.value = creditbankcode;
        ccnum.value = creditnum;
        ccexpmon.value = creditmnth;
        ccexpyr.value = credityear;
        ccvv.value = creditcvv;
        ccname.value = creditname;
      } else {
        var creditnum = document.getElementById('creditnum').value;
        var creditmnth = document.getElementById('creditmonth').value;
        var credityear = document.getElementById('credityear').value;
        var creditcvv = document.getElementById('creditcvv').value;
        var creditname = document.getElementById('creditname').value;
        var creditpg = document.getElementById('cardcategory').value;
        var creditbankcode = document.getElementById('cardtype').value;

        pg.value = creditpg;
        bankcode.value = creditbankcode;            
        ccnum.value = creditnum;
        ccexpmon.value = creditmnth;
        ccexpyr.value = credityear;
        ccvv.value = creditcvv;
        ccname.value = creditname;
      }
      verify();               
    });
  });

  function showData() {
    //$("#show_processing").modal("show");
    $('#txnid').val(txnid);
    $.ajax({
      type: "post",
      url: "common/cfPayment.php",
      data: "action=showPayData&values=" + txnid + "",
      success: function (data) {
          data = $.parseJSON(data);
          //udf3 & udf4 are missing.
          $('#productinfo').val(data[0].Pay_Tran_ProdInfo);
          $('#transaction').val(data[0].Pay_Tran_PG_Trnid);
          $('#udf1').val(data[0].Pay_Tran_ITGK);
          $('#udf2').val(data[0].Pay_Tran_RKCL_Trnid);
          $('#udf3').val(data[0].Pay_Tran_Course);
          $('#udf4').val(data[0].Pay_Tran_Batch);
          $('#amount').val(data[0].Pay_Tran_Amount);
          $('#firstname').val(data[0].UserProfile_FirstName);
          $('#phone').val(data[0].UserProfile_Mobile);
          $('#email').val(data[0].UserProfile_Email);
          //$("#show_processing").modal("hide");
          $('body').addClass('loaded');
      }
    });
  }

  function verify() {
    var txtamt = document.getElementById('amount').value;
    var txtamt1 = parseInt(txtamt);                       
    if(txtamt1 == txtamt) {
      var method = document.getElementById('cardcategory').value;
      var methodmatch = document.getElementById('paymethod').value;
      var category='';
      if(methodmatch == 'credit') {
            category='CC';
      } else if (methodmatch == 'debit') {
            category='DC';
      } else if (methodmatch == 'netbank') {
            category='netbank';
      }
      if (category == 'CC') {
        //Shift functiond to common cf files.
        if ($("#payuForm").valid()) {
          $.ajax({
            type: "post",
            url: "common/cfPayment.php",
            data: "action=FinalValidateCard&values=" + creditnum.value + "",
            success: function (data) {
              if (data == 1) {
                alert("Invalid Credit Card Details");
              } else if (data == 2) {
                if (method == category) {
                  generateHash();
                }
              } else {
                alert("Invalid Credit Card Details.");
              }
            }
          });   
        }
      } else if (category == 'DC') {
          if ($("#payuForm").valid()) {
            $.ajax({
              type: "post",
              url: "common/cfPayment.php",
              data: "action=FinalValidateCard&values=" + debitnum.value + "",
              success: function (data) {
                if (data == 1) {
                  alert("Invalid Debit Card Details");
                } else if(data == 2) {                                 
                  if(method == category) {
                    generateHash();
                  }
                } else {
                  alert("Invalid Debit Card Details.");
                }
              }
          });
        }
      } else if(category == 'netbank') {
        if ($("#payuForm").valid()) {
          generateHash();
        }
      }
    }
  }

  function generateHash() {
    //$("#show_processing").modal("show");
    $('body').removeClass('loaded');
    $("#gateway").empty();
    var forminput = $("#payuForm").serialize();
    $.ajax({
        type: "post",
        url: "common/cfPayment.php",
        data: "action=generatehash&" + forminput,
        success: function (data) {
          if (data != '') {
            if (data != '1' && data != '2' && data != '3' && data != '4') {
              $("#gateway").html(data);
              $('#payuForm').attr('action', $('#action').val());
            }
            $('#payuForm').submit();
          }
        }
    });
  }



  function FillNetBankingName() {
    $.ajax({
        type: "post",
        url: "common/cfPayment.php",
        data: "action=FillNetBankingName",
        success: function (data) {
            $("#ddlbankname").html(data);
        }
    });
  }

  function mycreditvalidate() {
    var newValue = $("#creditnum").val();
    if (newValue.length == 6) {
      $.ajax({
        type: "post",
        url: "common/cfPayment.php",
        data: "action=ValidateCard&values=" + newValue + "",
        success: function (data) {
          data = $.parseJSON(data);
          if(data[0].cardCategory == 'CC') {
            cardtype.value = data[0].cardType;
            cardcategory.value = data[0].cardCategory;
          } else {
            alert("Please Enter Valid Card Details");
            $("#creditnum").val('');
          }
        }
      });
    }
  }

  function mydebitvalidate() {
    var newValue = $("#debitnum").val();
    if (newValue.length == 6) {
      $.ajax({
        type: "post",
        url: "common/cfPayment.php",
        data: "action=ValidateCard&values=" + newValue + "",
        success: function (data) {
          data = $.parseJSON(data);
          if (data[0].cardCategory == 'DC') {
            cardtype.value = data[0].cardType;
            cardcategory.value = data[0].cardCategory;
          } else {
            alert("Please Enter Valid Card Details");
            $("#debitnum").val('');
          }
        }
      });
    }
  }
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmpaymentcardvalidate_validation.js"></script>
<?php include ('footer.php'); ?>
</html>
