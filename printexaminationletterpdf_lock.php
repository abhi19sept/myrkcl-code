<?php
$title="Exam Event ";
include ('header.php'); 

//include ('root_menu.php');  
//include 'common/commonFunction.php';
require_once("common/cfexaminationpdf.php");

//session_start();
if (isset($_REQUEST['code']))
	{

	$data = array();
	$_REQUEST["code"];
    $data = getdata($_REQUEST["code"]);
    //$row=  mysqli_fetch_array($_SESSION['data']);
   //print_r($_SESSION['data']);   
$_Count = 1;
while($row= mysqli_fetch_array($data))
{mysqli_set_charset('utf8');
	//print_r($row);
$html1.='<center>
<DIV id="page_1"  style="background-image: url(images/422900248x1.jpg);height:890px;width:700px !important;border:0px solid #333;padding:10px;margin-top:20px;">
<DIV id="dimg1">
</DIV>

<P align="center" style="line-height:0.5px; font-size:15px"><b>VARDHAMAN MAHAVEER OPEN UNIVERSITY, KOTA</b></P>
<P align="center"  style="line-height:0.5px;font-size:15px"><NOBR><b>RS-CIT</b></NOBR> <b>EXAMINATION, FEBRUARY- 2016</b></P>
<P align="center" style="line-height:0.5px;font-size:15px"><NOBR><b>PERMISSION LETTER</b></P>
<P style="margin-top:15px;height:20px;width:1000px"><IMG src="images/notice.png" >  </P>

<TABLE cellpadding=0 cellspacing=0 class="t0" border="1" style="width:700px;">
<TR>
	<TD class="tr0 td0" width="350"><P class="p4 ft1" style="font-size:17px"><b>Learner Code : </b>'.$row['learnercode'].' </P></TD>
	<TD class="tr0 td1" width="350"><P class="p5 ft2" style="font-size:17px"><b>Exam Center / Venue</b></P></TD>
</TR>

<TR>
	<TD rowspan=2 width="350" class="tr2 td4" style="border:none !important;border:none;font-size:15px"><P class="p7 ft4"><b>Date of Examination: </b>28th February 2016 , Sunday </P></TD>
	<TD class="tr3 td5" width="350" style="border:none !important;font-size:15px"><P class="p8 ft5">Center Code: '.$row['examcode'].'</P></TD>
</TR>
<TR>
	<TD class="tr4 td5" style="border:none !important;font-size:15px"><P class="p8 ft6">Center Name: '.$row['examcentername'].'</P></TD>
</TR>
<TR>
	<TD class="tr5 td4" width="350" style="border:none;"><P class="p6 ft7">&nbsp;</P></TD>
	<TD class="tr5 td5" width="350" style="border:none !important;font-size:15px"><P class="p8 ft8">Address: '.$row['examcenteraddress'].'</P></TD>
</TR>
<TR>
	<TD class="tr6 td4" width="350" style="border:none; !important;font-size:15px;border:none;"><P class="p7 ft2"><b>Time of Examination</b>: 12:00 NOON to 01:00 PM</P></TD>
	<TD class="tr6 td5"  width="350" style="border:none !important;font-size:15px;"><P class="p8 ft6">District:  '.$row['District_Name'].', Tehsil :  '.$row['Tehsil_Name'].'</P></TD>
</TR>
<TR>
	<TD class="tr7 td2" width="350" style="border:none; !important;"><P class="p6 ft9">&nbsp;</P></TD>
	<TD class="tr7 td3"  width="350" style="border:none !important;"><P class="p6 ft9">&nbsp;</P></TD>
</TR>
</TABLE>
<TABLE cellpadding=0 cellspacing=0 class="t1" style="width:700px !important">

<TR>
	<TD colspan="1" width="200"></TD>
	<TD colspan="2" width="350"><P >&nbsp;</P></TD>
	<TD colspan="3"><P style="font-size:15px !important;">Candidate Photo</p></TD>
</TR>

<TR>
	
	<TD colspan="1" width="200"><P style="font-size:15px !important;" > 1.)<b>ROLL NO</b></P></TD>
	
	<TD colspan="2" width="350"><P style="font-size:15px !important;">:'.$row['rollno'].'</P></TD>
	
	
	<TD colspan="3" rowspan="20" style="margin"><IMG src="upload/admission_photo/'.$row['learnercode'].'_Photo.jpeg"   style="margin-left:2px;width:129px;height:150px;margin-top:10px;border:1px solid  "> </TD>
	
</TR>
<TR>
	
	<TD colspan="1"><P class="p6 ft11" style="font-size:15px !important;" >2)<b>Name of the Candidate</b></P></TD>
	<TD colspan="2"  ><P class="p4 ft10" style="font-size:15px !important;" >:'.$row['learnername'].' </P></TD>
	<TD colspan="3"></TD>
</TR>

<TR>
	
	<TD colspan="1"><P class="p6 ft11" style="font-size:15px !important;" >3)<b>Father /Husband Name</b></P></TD>
	<TD colspan="2"><P class="p4 ft10" style="font-size:15px !important;" >: '.$row['fathername'].'</P></TD>
	<TD colspan="3"></TD>
</TR>
<TR>
	
	<TD colspan="1"><P class="p6 ft4" style="font-size:15px !important;">4)<b>Date of Birth</b></P></TD>
	<TD colspan="2"><P class="p4 ft10" style="font-size:15px !important;">:'.$row['dob'].'</P></TD>
	<TD colspan="3"></TD>
	
	
</TR>

<TR>
	<TD colspan="1" width="200"></TD>
	<TD colspan="2" width="350"><P >&nbsp;</P></TD>
	<TD colspan="3"><P style="font-size:15px !important;"></p></TD>
</TR>
<TR>
	
	<TD colspan="1" width="350" align="center"><IMG src="images/sign.png" style="border:1px solid #333; height:40px; width:140px;"></TD>
	<TD colspan="2" width="250" align="right"><img src="upload/admission_sign/'.$row['learnercode'].'_Sign.jpeg" style="border:1px solid #333; height:40px; width:140px;margin-right:5px"></TD>
	<TD colspan="3"></TD>
	
</TR>

<TR>
	
	<TD colspan="1" width="350" align="center"><P class="p6 ft4" style="font-size:15px !important;">Controller of Examination</b></P></TD>
	<TD colspan="2" width="200" align="right" ><P class="p6 ft4" style="font-size:15px !important;margin-left:50px !important;">Candidate`s Signature</b></P></TD>
	<TD colspan="3"></TD>
	
</TR>


<TR>
	
	<TD colspan="1" width="350"><P class="p6 ft4" style="font-size:15px !important;">VARDHAMAN MAHAVEER OPEN UNIVERSITY</b></P></TD>
	<TD colspan="2"><P class="p6 ft4" style="font-size:15px !important;"></b></P></TD>
	<TD colspan="3"></TD>
	
</TR>
</TABLE>

<IMG src="images/instru.jpg" style="width:100% !important;height:400px;margin-top:20px">
</DIV> 
</center>';
 
$_Count++;

}
}

include("mpdf/mpdf.php");
$mpdf = new mPDF('win-1252', 'A4', '', '', 15, 10,30, 10, 10, 10); //A4 page in portrait for landscape add -L.
//$mpdf->SetHeader('||');
//$mpdf->setFooter('{PAGENO}'); // Giving page number to your footer.
$mpdf->useOnlyCoreFonts = true;    // false is default
$mpdf->SetDisplayMode('fullpage');
// Buffer the following html with PHP so we can store it to a variable later
ob_start();

//$html1 = 'visitdetails.php';
//ob_end_clean();
// send the captured HTML from the output buffer to the mPDF class for processing
$mpdf->WriteHTML($html1);






//$mpdf->SetProtection(array(), 'user', 'password'); uncomment to protect your pdf page with password.
echo $temp=$mpdf->Output();

$mpdf->Output('printexaminationletterpdf.pdf','F');
$files = array('printexaminationletterpdf.pdf','printexaminationletterpdf.pdf');

    # create new zip opbject
    $zip = new ZipArchive();

    # create a temp file & open it
    $tmp_file = tempnam('.','');
    $zip->open($tmp_file, ZipArchive::CREATE);

    # loop through each file
    foreach($files as $file){

        # download file
        $download_file = file_get_contents($file);

        #add it to the zip
        $zip->addFromString(basename($file),$download_file);

    }

    # close zip
    $zip->close();

    # send the file to the browser as a download
    header('Content-disposition: attachment; filename=Permissionletter.zip');
    header('Content-type: application/zip');
    readfile($tmp_file);
?>
