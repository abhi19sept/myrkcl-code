<?php
$title = "Biometric Attendance Count Summary";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '17' || $_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '8') {
    ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
    <div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 


            <div class="panel panel-primary" style="margin-top:20px !important;">  
                <div class="panel-heading">Biometric Attendance Count Summary</div>
                <div class="panel-body">

                    <form name="frmbiometricattendancesummary" id="frmbiometricattendancesummary" class="form-inline" role="form" enctype="multipart/form-data">
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
                            <div id="errorBox"></div>
                            <div class="container">
                            <div class="col-md-6 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">
                                    <option value="0" selected>Select Course</option>
                                    <option value="3">RS-CIT Women</option>
                                    <option value="24">RS-CFA Women</option>
                                </select>
                            </div> 
                                <div class="col-md-6 form-group">     
                                    <label for="batch"> Select Batch:<span class="star">*</span></label>
                                    <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                            onchange="try { setCustomValidity('') } catch (e) { }"> 
                                    </select>
                                </div>
                            </div>
                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="sdate">Start Date:</label>
                                    <span class="star">*</span>
                                    <input type="text" class="form-control" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="DD-MM-YYYY">     
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <label for="edate">End Date:</label>    
                                    <span class="star">*</span>
                                    <input type="text" class="form-control" readonly="true" name="txtenddate" id="txtenddate"  placeholder="DD-MM-YYYY" value=" <?php echo date("Y-m-d"); ?>">
                                </div>
                            </div>
                            <div class="container">
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                            </div>

                            <div id="grid" style="margin-top:5px; width:94%;"> </div>

                        </div>   
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php include ('footer.php'); ?>
    <?php include'common/message.php'; ?>
    </body>
<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        // autoclose: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script> 
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
        $("#ddlCourse").change(function () {

            var selCourse = $(this).val();
            FillBatch(selCourse);


        });
            function FillBatch(selCourse) {
                $.ajax({
                    type: "post",
                    url: "common/cfBiometricAttendanceReport.php",
                    data: "action=FILLBatch&coursecode="+selCourse+"",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });
            }
    

            $("#frmbiometricattendancesummary").submit(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBiometricAttendanceReport.php"; // the script where you handle the form input.   
                var startdate = $('#txtstartdate').val();
                var enddate = $('#txtenddate').val();        
                data = "action=GETDETAILS&batchcode=" + ddlBatch.value + "&coursecode=" + ddlCourse.value + "&sdate=" + startdate + "&edate=" + enddate + ""; //            
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) {
                        //alert(data);
                        $('#response').empty();
                        $("#grid").html(data);
                        $('#example').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]
                        });
                        $(".approvalLetter").click();
                    }
                });
                return false;
            });
            
            

            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
    <script src="rkcltheme/js/jquery.validate.min.js"></script>
    <style>
        .error {
            color: #D95C5C!important;
        }
        .clicklist_action{
            color: #fff;
            font-weight: bold;
            padding: 6px 10px;
            display:block;
            width: auto;
            text-align: center;
            border: none;
            min-width : 95px;
            background: #C6400C;
        }
        .clicklist_action1{
            background: green;
            margin-bottom: 5px;
        }
        .clicklist_action2{
            background: red;
        }
        #click_itgk {
            font-weight: bold;
            color: blue;
        }
        #Click_school {
            font-weight: bold;
        }
    </style>

    </html>
    <?php
} else {
    session_destroy();
    ?>
    <script>
    window.location.href = "logout.php";
    </script>
    <?php
}
?>
