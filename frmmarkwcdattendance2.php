<?php
$title = "Mark WCD Batch Learners Attendance";
include ('header.php');
include ('root_menu.php');
if (isset($_SESSION)) {
    //echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var Role='" . $_SESSION['User_UserRoll'] . "'</script>";
} else {

    echo "<script>var Mode='Add'</script>";
    echo "<script>var Role='0'</script>";
}
?>		
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Mark WCD Batch Learners Attendance</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmmarkwcdattendance" id="frmmarkwcdattendance" class="form-inline" role="form" method="post">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
<!-- <div id="loadingDiv" class="loader"></div> -->
<div id="loadingDiv" class="overlay">
    <div class="overlay__inner">
        <div class="overlay__content"><span class="spinner"></span></div>
    </div>
</div>
                        </div>        
                        <div id="errorBox"></div>

                    </div>


                    <div id="detailsdiv">				


                        <div class="container"> 
                            <div class="col-md-6 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">
                                    <option value="0" selected>Select Course</option>
                                    <option value="3">RS-CIT Women</option>
                                    <option value="24">RS-CFA Women</option>
                                </select>
                            </div> 

                            <div class="col-md-6 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">

                                </select>
                            </div> 

                            <div class="col-sm-4 form-group">     
                                <label for="dob">Select Date:<span class="star">*</span></label>                              
                                <input type="text" class="form-control" disabled="true" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="YYYY-MM-DD" required="required">
                                <input type="hidden" id="txtcid" name="txtcid">
                              
                            </div>


                        </div>	
                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="sdate">Enter Online Class URL:<span class="star">*</span></label>


                                <textarea class="form-control" cols="40" id="txtclassurl" name="txtclassurl"  rows="5" required="required" oninvalid="setCustomValidity('Please Enter Class URL')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"></textarea>
                               
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="sdate">Upload Online Class Photo:<span class="star">*</span></label>
                                <div id="uploadPreview9">
                                    
                                </div>
                                <img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="fileCasteCertificate" width="80px" height="107px">
                                  <input type="file" class="form-control" id="filePhoto" name="filePhoto" onchange="ValidateSingleInput(this);">

                                  <span style="font-size:10px;">Note : JPG,JPEG Allowed Size =100KB to 200KB</span>
                            </div>
                            <div class="col-sm-4 form-group">     
                                <label for="dob">Duration of Online Class :<span class="star">*</span></label>                              
                                <input type="text" class="form-control" maxlength="50" name="classdur" id="classdur"  placeholder="In Minutes" required="required" onkeypress="javascript:return allownumbers(event);">
                            <input type="hidden" class="form-control" maxlength="50" name="action" id="action" value="ADD"/>
                            </div>
                        </div>	
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Upload Online Class Details"/>    
                            </div>
                        </div>
                        <div id="grid" name="grid" style="margin-top:35px;"> </div> 
                    </div>

            </div>
        </div>   
    </div>
</form>
</div>
<div class="modal fade" id="previewModal" role="dialog">
    <div>
        <div class="modal-content" style="margin: 100px auto; width: 500px;" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-left">Warning!!</h4>
            </div>
            <div class="modal-body" id="rec_type">
                <h4 id="rec_type_h3"></h4>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>
            <div class="modal-footer">
                <button class="btn btn-danger" id="btnFinalSubmit">Yes</button>
                 <button class="btn btn-primary" id="btnFinalCancle">No</button>
                                         
            </div>
        </div>
    </div>
</div> 
<div class="modal fade" id="previewModal2" role="dialog">
    <div>
        <div class="modal-content" style="margin: 100px auto; width: 500px;" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-left">Warning!!</h4>
            </div>
            <div class="modal-body" id="rec_type_2">
                <h4 id="rec_type_h4"></h4>
            </div>
            <div class="clearfix" style="height: 10px;clear: both;"></div>

        </div>
    </div>
</div> 
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<style type="text/css">
.overlay {
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    position: fixed;
    background: #222;
    z-index: 999;
    opacity: 0.2;
}

.overlay__inner {
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    position: absolute;
}

.overlay__content {
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
}

.spinner {
  
    display: inline-block;
    border: 16px solid #f3f3f3; /* Light grey */
      border-top: 16px solid #006fba; /* Blue */
      border-radius: 50%;
      width: 120px;
      height: 120px;
      animation: spin 2s linear infinite;
}

@keyframes spin {
  100% {
    transform: rotate(360deg);
  }
}
a {
  color: #FFFFFF;
  text-decoration: none;
}

.primary-btn {
  background-image: -moz-linear-gradient(0deg, #235ee7 0%, #4ae7fa 100%);
  background-image: -webkit-linear-gradient(0deg, #235ee7 0%, #4ae7fa 100%);
  background-image: -ms-linear-gradient(0deg, #235ee7 0%, #4ae7fa 100%);
}

.primary-btn {
  line-height: 36px;
  padding-left: 30px;
  padding-right: 30px;
  border-radius: 25px;
  border: none;
  color: #fff;
  display: inline-block;
  font-weight: 500;
  position: relative;
  -webkit-transition: all 0.3s ease 0s;
  -moz-transition: all 0.3s ease 0s;
  -o-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
  cursor: pointer;
  text-transform: uppercase;
  position: relative;
}

.primary-btn {
  color: #fff;
  border: 1px solid #fff;
  -webkit-transition: all 0.3s ease 0s;
  -moz-transition: all 0.3s ease 0s;
  -o-transition: all 0.3s ease 0s;
  transition: all 0.3s ease 0s;
}

.primary-btn:hover {
  background: transparent;
  color: #235ee7;
  border-color: #235ee7;
}
</style>
</body>
<script type="text/javascript">
    var $loading = $('#loadingDiv').hide();
$(document)
  .ajaxStart(function () {
    $loading.show();
  })
  .ajaxStop(function () {
    $loading.hide();
  });
var d = new Date();

var yesterday = new Date(d);
yesterday.setDate(yesterday.getDate() - 1);
var yesterdayDate = formatDate(yesterday);

var daybeforeyesterday = new Date(d);
daybeforeyesterday.setDate(daybeforeyesterday.getDate() - 2);
var daybeforeyesterdayDate = formatDate(daybeforeyesterday);

var daybeforedaybeforeyesterday = new Date(d);
daybeforedaybeforeyesterday.setDate(daybeforedaybeforeyesterday.getDate() - 3);
var daybeforedaybeforeyesterdayDate = formatDate(daybeforedaybeforeyesterday);

var enableDays =  yesterdayDate+ ', '+daybeforeyesterdayDate+ ', '+daybeforedaybeforeyesterdayDate+", 25-03-2021, 26-03-2021, 27-03-2021, 28-03-2021, 30-03-2021, 31-03-2021, 01-04-2021, 02-04-2021, 03-04-2021, 04-04-2021, 05-04-2021".split(', ');

function formatDate(d) {
  var day = String(d.getDate())
  //add leading zero if day is is single digit
  if (day.length == 1)
    day = '0' + day
  var month = String((d.getMonth()+1))
  //add leading zero if month is is single digit
  if (month.length == 1)
    month = '0' + month
  return day + "-" + month + "-" + d.getFullYear()
}
$(function () {
  $("#dob").datepicker({ 
        maxViewMode: 2,
        weekStart: 1,
        beforeShowDay: function(date){
          if (enableDays.indexOf(formatDate(date)) < 0)
            return {
              enabled: false
            }
          else
            return {
              enabled: true
            }
        },

        format: "yyyy-mm-dd",
        clearBtn: true,
        autoclose: true,
    
  }).change(dateChanged);
});
function dateChanged(ev) {
    $(this).datepicker('hide');

    chkClass($(this).val(),$("#ddlCourse").val(),$("#ddlBatch").val());

}
    function chkClass(classdate,course,batch) {

        
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        var url = "common/cfMarkWCDAttendance2.php"; // the script where 

        var data;
        data = "action=CHKclass&course=" + course + "&batch=" + batch + "&classdate=" + classdate + ""; 

        $.ajax({
            type: "post",
            url: url,
            data: data,
            success: function (data) {
                    data = $.parseJSON(data);
                    classphoto = data[0].classphoto;
                    classurl = data[0].classurl;
                    classduration = data[0].classduration;
               
                if(classurl == '0'){
                    $('#response').empty();
                    
                    $("#txtclassurl").val("");
                    $("#classdur").val("");
                    $("#uploadPreview8").attr("src","images/sampleproof.png");
                    $("#txtclassurl").prop('disabled', false);
                      $("#classdur").prop('disabled', false);
                       $("#uploadPreview8").show();
                       $("#filePhoto").show();
                       $("#btnSubmit").show();
                        $("#uploadPreview9").html("");
                        $("#uploadPreview9").hide();
                }
                else{
                    $('#response').empty();
                    $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>You Have Already Entered Class Details on this Date</span></div>");
                    
                    $("#txtclassurl").val(classurl);
                    $("#classdur").val(classduration);

                    $("#uploadPreview9").html(classphoto);
                    $("#uploadPreview9").show();
                     $("#txtclassurl").prop('disabled', true);
                      $("#classdur").prop('disabled', true);
                       $("#uploadPreview8").hide();
                       $("#filePhoto").hide();
                       $("#btnSubmit").hide();
                }
               

            }
        });
        
            // document.getElementById("txtenddate").value = "";
        
    }


</script>
<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>
<script language="javascript" type="text/javascript">
function checkPhoto(target) {
    var ext = $('#filePhoto').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("filePhoto").value = '';
            return false;
        }

    if(target.files[0].size > 100000) {                  
        alert("Image size should less or equal 100 KB");
        document.getElementById("filePhoto").value = '';
        return false;
    }
    else if(target.files[0].size < 200000) {
                alert("Image size should be greater than 200 KB");
                document.getElementById("filePhoto").value = '';
                return false;
            }
    document.getElementById("filePhoto").innerHTML = "";
    return true;
}
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

    var cid='';
    var atype='';
    var checkattenlcode='';
    var checkattenatype='';
    var checkattenatime='';

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
                $.ajax({
                    type: "post",
                    url: "common/cfMarkWCDAttendance2.php",
                    data: "action=FILLEventBatch&values=" + selCourse + "",
                    success: function (data) {

                        $("#ddlBatch").html(data);
                    }
                });


        });

        function showAllData(val, val1) {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfMarkWCDAttendance2.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "",
                success: function (data)
                {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $('#btnSubmit').show();

                    //showData();
                }
            });
        }


        $("#ddlBatch").change(function () {
            $("#dob").prop('disabled', false);
             $("#grid").html("");
            $('#response').empty();
             $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var selBatch = $(this).val();
            var selCourse = $("#ddlCourse").val();
            $.ajax({
                type: "post",
                url: "common/cfMarkWCDAttendance2.php",
                data: "action=CheckCaptureEvent&batch=" + selBatch + "&course=" + selCourse + "",
                success: function (data)
                {
               
                    if(data == 1){
                        showAllData(selBatch, selCourse);
                    }
                    else{
                        $('#response').empty();
                        $('#response').append("<p class='error'><span>Attendance of this batch is closed.</span></p>");
                        $("#ddlBatch").val("");
                    }
                }
            });
            // showAllData(this.value, ddlCourse.value);
        });
        function isUrl(str) {
          regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
          return regexp.test(str);

        }

    $("#frmmarkwcdattendance").on('submit',(function(e) {
        if ($("#frmmarkwcdattendance").valid()) {
            $('#responses').empty();
            $('#responses').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
            if(classdur.value==0){
                alert("Enter Valid Class Duration!");
                return false;  
            }
            else{ 
            if(isUrl(txtclassurl.value)){     
                e.preventDefault();
                $.ajax({ 
                    url: "common/cfMarkWCDAttendance2.php",
                    type: "POST",
                    data:  new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    { 

                            //alert(data);
                        if(data == "duplicatslot"){
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>You Have Already Entered Class Details in this Time Slot</span></div>");
                        }
                        else{
                                $('#response').empty();
                                $('#response').append("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>"+ data +", Now Mark Learners Attendance.</span></div>");
                            }

                        }
                    });

                }
                else{
                    $('#response').empty();
                    alert("Class URL is Invalid");
                    location.reload();

                }
                }               

            }
            return false; // avoid to execute the actual submit of the form.
        }));
        $("#grid").on('click', '.Present',function(){

            var attendate = $('#dob').val();

            if(attendate == ""){
                alert("Please Select Date from Calendar");
            }
            else{
            cid = $(this).attr('id');
            $('#txtcid').val(cid);
            atype ="P";
            var batch=$("#ddlBatch").val();
            $.ajax({
                type: "post",
                url: "common/cfMarkWCDAttendance2.php",
                data: "action=CheckAttendance&lcode=" + cid + "&batch=" + batch + "&attendate=" + attendate + "",
                success: function (data) {

                    data = $.parseJSON(data);
                    checkattenlcode = data[0].lcode;
                    checkattenatype = data[0].atype;
                    checkattenatime = data[0].atime;

                    if(checkattenlcode == '0'){
                        $('#rec_type_h3').html("Do You want to Mark Present for this learner?");
                        $('#txtcid').val(cid);
                        $('#previewModal').modal({show: true});
                    }
                    else if(checkattenlcode == '1'){
                        $('#errorBox').html("<p style='color:red'>Please Submit Online Class Details First!</p>");
                    }
                    else{
                        if(checkattenatype == "P"){
                            $('#rec_type_h4').html("You have already Marked Present for this learner ");
                        }else{
                            $('#rec_type_h4').html("You have already Marked Absent for this learner ");
                        }

                        $('#previewModal2').modal({show: true});
                    }

                }
            });
            // checkattendance(cid,batch);

             $(this).prop("disabled", true);

            
            }
        }); 
        $("#grid").on('click', '.Absent',function(){

            var attendate = $('#dob').val();

            if(attendate == ""){
                alert("Please Select Date from Calendar");
            }
            else{
            cid = $(this).attr('id');
            $('#txtcid').val(cid);
            atype ="A";
            var batch=$("#ddlBatch").val();
            $.ajax({
                type: "post",
                url: "common/cfMarkWCDAttendance2.php",
                data: "action=CheckAttendance&lcode=" + cid + "&batch=" + batch + "&attendate=" + attendate + "",
                success: function (data) {

                    data = $.parseJSON(data);
                    checkattenlcode = data[0].lcode;
                    checkattenatype = data[0].atype;
                    checkattenatime = data[0].atime;

                    if(checkattenlcode == '0'){
                        $('#rec_type_h3').html("Do You want to Mark Absent for this learner?");
                        $('#txtcid').val(cid);
                        $('#previewModal').modal({show: true});
                    }
                    else{
                        if(checkattenatype == "P"){
                            $('#rec_type_h4').html("You have already Marked Present for this learner ");
                        }else{
                            $('#rec_type_h4').html("You have already Marked Absent for this learner ");
                        }
                        $('#previewModal2').modal({show: true});
                    }

                }
            });
            

             $(this).prop("disabled", true);

            }
    
        }); 

        $("#btnFinalCancle").click(function () {
             $("#"+cid).prop("disabled", false);
             $('#previewModal').modal('hide');
             cid='';
             $('#txtcid').val('');
          });
        $("#btnFinalSubmit").click(function () {
             $.ajax({
                type: "post",
                url: "common/cfMarkWCDAttendance2.php",
                data: "action=MarkWCDAttendance&lcode=" + txtcid.value + "&ccode=" + ddlCourse.value + "&bcode=" + ddlBatch.value + "&dob=" + dob.value + "&atype=" + atype + "",
                success: function (data) {
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>"+data+"</span></p>");
                    $('#txtcid').val('');
                    $('#previewModal').modal('hide');
                    window.setTimeout(function () {
                        $('#response').empty();
                    }, 3000);
                      
                }
            });
        });
    });
</script>
</html>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmmarkwcdattendance.js" type="text/javascript"></script>