<?php
$title="Photo Sign Process";
include ('header.php'); 
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
echo "<script>var Admission_Name=0</script>";
echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
?>

<div style="min-height:430px !important;max-height:1500px !important;">
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Photo Sign Process</div>
                <div class="panel-body">
                    <form name="frmprocess" id="frmprocess" class="form-inline" role="form" enctype="multipart/form-data">
						<div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
								
								
									<div class="col-sm-4 form-group">     
										<label for="course">Select Course:<span class="star">*</span></label>
										<select id="ddlCourse" name="ddlCourse" class="form-control">
										
										</select>
										
									</div> 
								</div>
								
								<div class="container">
									<div class="col-sm-4 form-group">     
										<label for="batch"> Select Batch:<span class="star">*</span></label>
										<select id="ddlBatch" name="ddlBatch" class="form-control">
												   
										</select>									
									</div> 
								</div>
							
								<div id="menuList" name="menuList" style="margin-top:35px;"> </div> 
								
								<div class="container">
								<!--	<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    -->
								</div>
                    </div>
            </div>   
        </div>
    </form>
	</div>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
                        
        <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {			
			
			
			function FillCourse() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfphotosign.php",
                    data: "action=FILLCourseName",
                    success: function (data) {
						//alert(data);
                        $("#ddlCourse").html(data);
                    }
                });
            }
            FillCourse();
			
			
			$("#ddlCourse").change(function(){
				var selCourse = $(this).val(); 
				//alert(selCourse);
				 $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLPhotoSignProcessEventBatch&values=" + selCourse + "",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });				
           });
		

       
           
            function showAllData(val,val1) {
              //alert(val);
			  //alert(val1);
                $.ajax({
                    type: "post",
                    url: "common/cfphotosign.php",
                    data: "action=SHOWALL&batch="+ val +"&course="+ val1 +"",
                    success: function (data) {
						//alert(data);
                        $("#menuList").html(data);
						$('#example').DataTable();
                    }
                });
            }
		
			 $("#ddlBatch").change(function(){
                showAllData(this.value,ddlCourse.value);
            });
			
			if (Mode === 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            
			
			function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfAdmissionModify.php",
                    data: "action=DELETE&values=" + Admission_Name + "&batchcode=" + BatchCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmModifyAdmission.php";
                           }, 1000);
                            
                            Mode="Add";
                            resetForm("frmmodifyadmission");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();
                    }
                });
            }
        });

    </script>
    </body>
    
</html>