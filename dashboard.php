<?php
$title = "RKCL-Dashboard";
include ('header.php');
include ('root_menu.php');
?>
<script src="bootcss/js/watch.js" type="text/javascript"></script>
<div style="min-height:430px !important;max-height:auto !important">	
    <div class="container">
        <button type="button" id="btnSubmit" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header btn-primary ">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title ">Important Information</h4>
                    </div>
                    <div class="modal-body">
                        <p id='pop'></p>
                    </div>
                    <div class="modal-footer btn-primary ">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style=" margin-top:50px">
        <div class="row">
            <div class="col-md-12" >
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-bookmark"></span> Quick Shortcuts</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div style="display: None" id="location"><?php echo $url->cityName; ?></div>
                            <?php if ($_SESSION['User_UserRoll'] != 19) { ?>
                                <div class="col-md-6 col-md-6" style="margin-top:20px">
                                    <a href="#" class="btn btn-danger btn-lg" role="button"><span class="glyphicon glyphicon-list-alt"></span> <br/>Apps</a>
                                    <a href="frmprocessguide.php" class="btn btn-warning btn-lg" role="button"><span class="glyphicon glyphicon-bookmark"></span> <br/>MyRKCL Guide</a>
                                    <a href="frmgallery.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-camera"></span> <br/>Gallery</a>
                                    <a href="frmPressrelease.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-comment"></span> <br/>PressRelease</a>
                                </div>
                                <div class="col-md-6 col-md-6" style="margin-top:20px">
                                    <a href="frmrscfaupdates.php" class="btn btn-success btn-lg" role="button" id="rscfamessagetag">
                                        <span class="glyphicon glyphicon-comment" ></span> 
                                        <span class="label"></span> <br/>RS-CFA Updates
                                        <span class="badge" id="rscfamessagecount"></span>        
                                    </a>		  
                                    <a href="frmsurvey.php" class="btn btn-info btn-lg" role="button"><span class="glyphicon glyphicon-star-empty"></span> <br/>Survey</a>
                                    <a href="frmdisplaythoughts.php" class="btn btn-info btn-lg" role="button"><span class="glyphicon glyphicon-eye-open"></span> <br/>Thought of Day</a>
                                    <a href="frmsoftwaredownload.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-tag"></span> <br/>Softwares</a>
                                </div>
                            <?php } ?>
                            <div class="col-md-12 col-md-12">
                                <div id="Date"></div>
                                <ul class="watch">
                                    <li id="hours" class="watch_main"> </li>
                                    <li id="point" class="watch_main">:</li>
                                    <li id="min" class="watch_main"> </li>
                                    <li id="point"class="watch_main">:</li>
                                    <li id="sec"  class="watch_main"> </li>
                                </ul>
                            </div>
                        </div>
                        <a href="http://www.rkcl.in" class="btn btn-success btn-lg btn-block" role="button" style="margin-top:10px;"><span class="glyphicon glyphicon-globe"></span> Website</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
     .rscfanotification {
        background-color: #ece3e3;
        color: white;
        text-decoration: none;
        position: relative;
        display: inline-block;
    }
    .rscfanotification:hover {
        background: red;
    }
    .rscfanotification .badge {
        position: absolute;
        top: -6px;
        right: 60px;
        padding: 5px 10px;
        border-radius: 50%;
        background-color: red;
        color: white;
    }
    html
    {
        overflow-y:hidden !important;
    }
    .btn-lg {    
        font-size: 17px;    
    }
</style>	
<link rel="stylesheet" href="css/watch.css">
<script>
if (document.location.href.match(/[^\/]+$/)[0] === "dashboard.php") {
    $.ajax({
        type: "post",
        url: "common/cfrscfanotification.php",
        data: "action=COUNTMESSAGE",
        success: function (data) {
            if (data === '0') {
                $("#rscfamessagecount").css("display", "none");
                $("#rscfamessagetag").removeClass("rscfanotification");
            } else {
                $("#rscfamessagecount").css("display", "block");
                $("#rscfamessagetag").addClass("rscfanotification");
                $("#rscfamessagecount").html(data);
            }
        }
    });
}
</script>
</body>
<?php include ('footer.php'); ?>
</html>