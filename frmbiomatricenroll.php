<?php
    $title = "Bio-Matric";
    include('header.php');
    include('root_menu.php');
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">First time capture may take time, so wait after click the button "Click To
                Capture"
            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmCourseBatch" id="frmCourseBatch" class="form-inline" role="form" method="post">

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>
                        <div id="errorBox"></div>

                    </div>


                    <div id="detailsdiv" style="display:none;">
                        <div style="display:none;">
                            <div class="container">
                                <div class="col-md-4 form-group">
                                    <label for="batch"> Serial No:<span class="star">*</span></label>
                                    <input type="text" name="tdSerial" id="tdSerial" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Make:<span class="star">*</span></label>
                                    <input type="text" name="tdMake" id="tdMake" class="form-control" readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Model:<span class="star">*</span></label>
                                    <input type="text" name="tdModel" id="tdModel" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Width:<span class="star">*</span></label>
                                    <input type="text" name="tdWidth" id="tdWidth" class="form-control"
                                           readonly="true"/>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-md-4 form-group">
                                    <label for="batch"> Height:<span class="star">*</span></label>
                                    <input type="text" name="tdHeight" id="tdHeight" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Local MAC:<span class="star">*</span></label>
                                    <input type="text" name="tdLocalMac" id="tdLocalMac" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Local IP:<span class="star">*</span></label>
                                    <input type="text" name="tdLocalIP" id="tdLocalIP" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> Status:<span class="star">*</span></label>
                                    <input type="text" name="txtStatus" id="txtStatus" class="form-control"
                                           readonly="true"/>
                                </div>
                            </div>

                        </div>

                        <div class="container">
                            <div class="col-md-6 form-group">
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">

                                </select>
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">

                                </select>
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="deviceType">Select Device Make<span class="star">*</span></label>
                                <select id="deviceType" name="deviceType" class="form-control"
                                        onchange="javascript:callAPI_initialize(this.value)">
                                    <option value="n">- - - Please Select - - -</option>
                                </select>
                            </div>

                        </div>
                        <div style="display:none;">
                            <div class="container">
                                <div class="col-md-4 form-group">
                                    <label for="batch"> Quality:<span class="star">*</span></label>
                                    <input type="text" name="txtQuality" id="txtQuality" value="" class="form-control"
                                           readonly="true"/>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="batch"> NFIQ:<span class="star">*</span></label>
                                    <input type="text" name="txtNFIQ" id="txtNFIQ" value="" class="form-control"
                                           readonly="true"/>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-md-10 ">
                                    <label for="batch"> Base64Encoded ISO Template:<span class="star">*</span></label>
                                    <textarea id="txtIsoTemplate" rows="4" cols="12" name="txtIsoTemplate"
                                              class="form-control" readonly="true"></textarea>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-md-10">
                                    <label for="batch"> Base64Encoded ISO Image:<span class="star">*</span></label>
                                    <textarea id="txtIsoImage" rows="4" cols="12" name="txtIsoImage"
                                              class="form-control" readonly="true"></textarea>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-md-10">
                                    <label for="batch"> Base64Encoded Raw Data:<span class="star">*</span></label>
                                    <textarea id="txtRawData" rows="4" cols="12" name="txtRawData" class="form-control"
                                              readonly="true"></textarea>
                                </div>
                            </div>

                            <div class="container">
                                <div class="col-md-10">
                                    <label for="batch"> Base64Encoded Wsq Image Data:<span class="star">*</span></label>
                                    <textarea id="txtWsqData" rows="4" cols="12" name="txtWsqData" class="form-control"
                                              readonly="true"></textarea>
                                </div>
                            </div>
                        </div>

                        <div id="menuList" name="menuList" style="margin-top:15px;"></div>

                        <!--  <div class="container" style="margin-top:20px; margin-left:15px;">
                               <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>
                         </div>		-->

                    </div>
            </div>
        </div>
    </div>
    </form>
</div>
<?php
    include('footer.php');
    include 'common/message.php';
    include 'common/modals.php';
    $mac = explode(" ", exec('getmac'));
?>
</body>
<script src="js/mfs100.js"></script>

<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.js"></script>

<script type="text/javascript">
    var localIP = "<?php echo getHostByName(getHostName());?>";
    var localMAC = "<?php echo str_ireplace('-', '', $mac[0]);?>";
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function GetRegisterDeviceInfo() {
            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=GetRegisterDeviceInfo&serial=" + tdSerial.value + "",
                success: function (data) {
                    if (data == "") {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=20px /></span><b><font color=red><span>" + "     Please Register your device First OR Enroll With Valid BioMetric Machine. <a href='frmbiomatricdeviceregistration.php'> Click Here </a>" + "</span></font></b></p>");
                    }
                    else {
                        $("#detailsdiv").show();
                    }
                }
            });
        }

        GetRegisterDeviceInfo();

        function FillMachines() {
            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=FillMachines",
                success: function (data) {
                    //alert(data);
                    $("#deviceType").html(data);
                }
            });
        }

        FillMachines();

        /*function GetInfo() {
         //alert("hii");
         document.getElementById('tdSerial').value = "";
         document.getElementById('tdMake').value = "";
         document.getElementById('tdModel').value = "";
         document.getElementById('tdWidth').value = "";
         document.getElementById('tdHeight').value = "";
         document.getElementById('tdLocalMac').value = "";
         document.getElementById('tdLocalIP').value = "";

         var res = GetMFS100Info();
         if (res.httpStaus) {

         document.getElementById('txtStatus').value = res.data.ErrorDescription;

         if (res.data.ErrorCode == "0") {
         document.getElementById('tdSerial').value = res.data.DeviceInfo.SerialNo;
         document.getElementById('tdMake').value = res.data.DeviceInfo.Make;
         document.getElementById('tdModel').value = res.data.DeviceInfo.Model;
         document.getElementById('tdWidth').value = res.data.DeviceInfo.Width;
         document.getElementById('tdHeight').value = res.data.DeviceInfo.Height;
         document.getElementById('tdLocalMac').value = res.data.DeviceInfo.LocalMac;
         document.getElementById('tdLocalIP').value = res.data.DeviceInfo.LocalIP;
         }

         }
         else {
         alert(res.err);
         }
         return false;
         }*/

        //GetInfo();


        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=FILLCourse",
                success: function (data) {
                    $("#ddlCourse").html(data);
                    if ($("#ddlCourse").val() != 0) {
                        $("#ddlCourse").trigger("change");
                    }
                }
            });
        }

        FillCourse();


        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=FILLBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                    if ($("#ddlBatch").val() != 0) {
                        $("#ddlBatch").trigger("change");
                    }
                }
            });

        });

        function showAllData(val, val1) {
            //alert(val);
            //alert(val1);
            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#menuList").html(data);
                    $('#example').DataTable();

                    //Capture();

                }
            });
        }

        $("#ddlBatch").change(function () {
            showAllData(this.value, ddlCourse.value);
        });

    });

    function PopupCenter(url, title, w, h) {


        if ($("#deviceType").val() == "n") {
            $("#errorText").html("Please select biometric device");
            $("#errorModal_Custom").modal("show");
        } else {
            // Fixes dual-screen position                         Most browsers      Firefox
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 2) - (h / 2)) + dualScreenTop;
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

            // Puts focus on the newWindow
            if (window.focus) {
                newWindow.focus();
            }
        }
    }

    function callAPI_initialize(x) {
        if (x != "n") {
            switch (x) {
                case "m":
                    $.ajax({
                        type: "post",
                        url: "common/cfbiomatricenroll.php",
                        data: "action=setMachine&value=" + x + "",
                        success: function (data) {
                            $("#machine_set").modal("show");
                        }
                    });
                    break;

                case "s":
                    $.ajax({
                        type: "post",
                        url: "common/cfbiomatricenroll.php",
                        data: "action=setMachine&value=" + x + "",
                        success: function (data) {
                            $("#machine_set").modal("show");
                        }
                    });
                    break;
					
                 case "t":
                            $.ajax({
                                type: "post",
                                url: "common/cfbiomatricenroll.php",
                                data: "action=setMachine&value=" + x + "",
                                success: function (data) {
                                    $("#machine_set").modal("show");
                                }
                            });
                            break;   
							
                case "c":

                    /*** INITIALIZE COGENT DEVICE HERE ***/
                    var strMessage = "Initialize";
                    $.ajax({
                        type: "GET",
                        url: "https://localhost:62016/MMMCSD200Service/ActionServlet?action=" + strMessage,
                        dataType: 'jsonp',
                        jsonp: 'callback',
                        jsonpCallback: 'myCallback',
                        success: function (jsonObj) {
                            console.log(jsonObj.Message);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        }
                    });


                    $.ajax({
                        type: "post",
                        url: "common/cfbiomatricenroll.php",
                        data: "action=setMachine&value=" + x + "",
                        success: function (data) {
                            $("#machine_set").modal("show");
                        }
                    });
                    break;

                default:
                    break;
            }
        }
    }


</script>
</html>