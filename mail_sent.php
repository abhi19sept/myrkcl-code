<?php
$message='';
$body='';
require_once 'PHPMailer/class.phpmailer.php';		
define('GUSER', 'testing.rkcl@gmail.com'); // Gmail username  
define('GPWD', 'testing@123'); // Gmail password 

	function smtpmailer($to, $from, $subject, $body)
	{		
		global $error;
		$mail = new PHPMailer();
		$mail->isSMTP();		
		$mail->IsHTML();
		$mail->SMTPDebug = 0;
		$mail->SMTPAuth = true;
		$mail->Debugoutput = 'html';
		$mail->SMTPSecure = 'ssl';
		$mail->Host = 'smtp.gmail.com';		
		$mail->Port = 465;		
		$mail->Username = GUSER;		
		$mail->Password = GPWD;	
        $mail->SetFrom($from);		
		$mail->addReplyTo('no-reply@rkcl.in', '');		
		$mail->addAddress($to);
			
		$mail->Subject = $subject;
		$mail->Body = $body;		
		$mail->AltBody = '';
		
		if (!$mail->send()) {		   
		   $error = 'Mail error: '.$mail->ErrorInfo;   
            return false;
		} else {		    
			$error = 'Message sent';  
            return true;			
		}
	}
	
	function mail_forgot_password($data)
	{
		//print_r($data);
	   	$subject = "RKCL - Forgot Password";		
		
		$message = '<html xmlns="http://www.w3.org/1999/xhtml">';
		$message .= '<head><meta http-equiv="Content-Type" content="html; charset=iso-8859-1" /></head><body>';
		$message .= 'Hi '.$data['email'].',';
		//$message .= 'Hi';		
		$message .= '<p>This is a notification email generated to inform you that your login password has been successfully changed.</p>';		
		$message .= '<p>Your new password is - '.$data["password"].'</p>';
		//$message .= '';
		$message .= '<p>You can change your password after logging into your account.</p>';
		
		$message .= "<br/>Regards,<br />";
		$message .= "RKCL Team<br/>";
		$message .= "www.rkcl.in<br/>";
		$message .= "<br/><b>Do Not Reply:</b> This is an auto generated email from RKCL and any replies to this email won't reach us. If you wish to contact us please visit: http://www.rkcl.in <br />";
		$message .= "</body></html>";
		//echo $message;
		//$from = $FromEmail;
		$from = 'rkcl_supportadmin@gmail.com';
		smtpmailer($data['email'], $from, $subject, $message);
	}

	
?>