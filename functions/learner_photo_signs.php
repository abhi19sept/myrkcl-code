<?php
    function processFetchedLearnerPics($itgks, $response, $path) {
        if (isset($response[2]) && mysql_num_rows($response[2])) {
            while ($row = mysql_fetch_array($response[2])) {
                $itgks = processPic($itgks, $row, 'photo', $path);
                $itgks = processPic($itgks, $row, 'sign', $path);
            }
        }

        return $itgks;
    }

    function processPic($itgks, $row, $type, $path) {
       global $general;
       $source = isFileExists($row['Admission_LearnerCode'], $type, $path);
        if ($source) {
            $targetDir = '/upload/admission' . $type . '/';
            
            $targetDir .= getbatchname($row) . '/';
            $absTarget = $general->getDocRootDir() . $targetDir;
            dirMake($absTarget);
            
            $targetPaths = isFileExists($row['Admission_LearnerCode'], $type, $targetDir);
            $targetPath = ($targetPaths) ? $targetPaths : $absTarget . $row['Admission_LearnerCode'] . '_' . $type . '.png';
            $itgks = copyPics($itgks, $row['Admission_ITGK_Code'], $targetPath, $source);
        }

        return $itgks;
    }

    function copyPics($itgks, $itgkCode, $target, $source) {
        $target = str_ireplace('.jpg', '.png', $target);
        if (copy($source, $target)) {
            $itgks[] = $itgkCode;
            $target = str_ireplace('.png', '.jpg', $target);
            if (file_exists($target)) {
                unlink($target);
            }
            $updated = str_ireplace('missingphotosigns', 'missingphotosigns/updated', $source);
            if (@copy($source, $updated)) {
                unlink($source);
            }
        }

        return array_unique($itgks);
    }

    function isFileExists($learnerCode, $type, $path) {
        global $general;
        $pngImgBatchPath = $general->getDocRootDir() . $path;
        dirMake($pngImgBatchPath);
        $sourcePngPhoto = $learnerCode . '_' . $type . '.png';
        $pngImgBatchPath .= $sourcePngPhoto;
        $jpgImgBatchPath = str_replace('.png', '.jpg', $pngImgBatchPath);
        $path = '';
        if (file_exists($pngImgBatchPath)) {
            $pngImgBatchPath = str_ireplace('.jpg', '.png', $pngImgBatchPath);
            $path = $pngImgBatchPath;
        } elseif (file_exists($jpgImgBatchPath)) {
            $path = $jpgImgBatchPath;
        }

        return $path;
    }

    function getbatchname($row) {
        $batchname = trim(ucwords($row['Batch_Name']));
        $coursename = $row['Course_Name'];
        $isGovtEmp = stripos($coursename, 'gov');
        if ($isGovtEmp) {
            $batchname = $batchname . '_Gov';
        } else {
            $isWoman = stripos($coursename, 'women');
            if ($isWoman) {
                $batchname = $batchname . '_Women';
            } else {
                $isMadarsa = stripos($coursename, 'madar');
                if ($isMadarsa) {
                    $batchname = $batchname . '_Madarsa';
                }
            }
        }
        $batchname = str_replace(' ', '_', $batchname);

        return $batchname;
    }

    function getPhotoSigns() {
        global $general;
        $path = '/upload/missingphotosigns/';
        $dir = $general->getDocRootDir() . $path;
        $pngfiles = glob($dir . '*.png');
        $jpgfiles = glob($dir . '*.jpg');
        $files = array_merge($pngfiles, $jpgfiles);
        $names = [];
        $learnerCodes = [];
        foreach ($files as $file) {
            $basename = basename($file);
            $names[] = $basename;
            $part = explode('_', $basename);
            $type = explode('.', $part[1]);
            $learnerCodes[$part[0]][$type[0]] = $type[1];
        }
        sort($names);

        return [$path, $names, $learnerCodes];
    }

    function dirMake($path) {
         return is_dir($path) || mkdir($path);
    }
