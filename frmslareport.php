<?php
$title = "IT-GK SLA Report";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}

if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8' || $_SESSION['User_UserRoll'] == '11' || $_SESSION['User_UserRoll'] == '14') {	
?>

<style>
    #myProgress {
        width: 100%;
        background-color: #ddd;
    }

    #myBar {
        width: 1%;
        height: 30px;
        background-color: #4CAF50;
        text-align: center;
        line-height: 30px;
        color: white;
    }
</style>

<div style="min-height:430px !important;max-height:auto !important">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">IT-GK SLA Report</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">


                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4">     
                            <label for="cname">SLA Report as on TODAY</label>
                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Generate SLA Report" style='margin-left:10px'/>    

                        </div>	
                    </div>
                    <br>
                    <div id="myProgress" style="display:none;">
                        <div id="myBar">Loading - 0%</div>
                    </div>
                    <br>
                    <!--<div id="response"></div>-->
                    <div class="panel panel-success" style="display:none;" id="report">
                        <div class="panel-heading">SLA Report as on TODAY</div>
                        <div class="panel-body">
                            <div id="grid" name="grid" style="margin-top:35px;"> </div> 
                        </div>
                    </div>

                </form>
            </div>
        </div>   
    </div>
</div>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function move() {
            var elem = document.getElementById("myBar");
            var width = 0;
            var id = setInterval(frame, 500);
            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                    elem.innerHTML = width * 1 + '%';
                }
            }
        }

        $("#btnSubmit").click(function () {
            $('#report').hide();
            $('#myProgress').show();
            move();
            FillItgkList();

            function FillItgkList() {
                //alert("gi");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfSlaReport.php",
                    data: "action=DETAILS",
                    success: function (data)
                    {
                        $('#myProgress').hide();
                        $('#report').show(3000);
                        //alert(data);
                        $('#response').empty();
                        $("#grid").html(data);
                        $('#example').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]
                        });
                    }
                });
            }
        });





        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>