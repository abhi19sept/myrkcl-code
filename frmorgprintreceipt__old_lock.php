<?php
$title = "Organization Detail";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}

//print_r($_SESSION);
    ?>
    <div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 			  
            <div class="panel panel-primary" style="margin-top:36px;">
                <div class="panel-heading">Organization Detail</div>
                <div class="panel-body">
                    <form name="frmorgprintreceipt" id="frmorgprintreceipt" class="form-inline" role="form" enctype="multipart/form-data">
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
                            <div id="errorBox"></div>	

                            <div class="col-sm-4 form-group">     
                                <label for="batch"> Select Applicant:</label>
                                <select id="ddlrole" name="ddlrole" class="form-control">
                                    <option selected="true" value="">Select</option>

                                    <option value="15" >IT-GK</option>
                                </select>									
                            </div> 
                            <div class="col-sm-4 form-group">     
                                <label for="batch"> Select Status:</label>
                                <select id="ddlstatus" name="ddlstatus" class="form-control">

                                </select>									
                            </div> 
                        </div>                

                        <div class="container">
                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show"/> 
                        </div>

                        <div id="menuList" name="menuList" style="margin-top:35px;"> </div> 
                </div>
            </div>   
        </div>
    </form>
    </div>
    </body>
    <?php include ('footer.php'); ?>
    <?php include'common/message.php'; ?>

    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            function showAllOrgDetail(val, val1) {
//                if ($("#frmcorrectionapproved").valid())
//                {
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    $.ajax({
                        type: "post",
                        url: "common/cfOrgPrintReceipt.php",
                        data: "action=ShowDetails&role=" + val + "&status=" + val1 + "",
                        success: function (data) {
                            $('#response').empty();
                            $("#menuList").html(data);
                            $('#example').DataTable();
                            //$("#btnSubmit").hide();
                            $('#txtCentercode').attr('readonly', true);
                        }
                    });
//                }
                return false;
            }

            function FillOrgStatus() {
                //alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfOrgMaster.php",
                    data: "action=FILLStatus",
                    success: function (data) {
                        //alert(data);
                        $("#ddlstatus").html(data);

                    }
                });
            }
            FillOrgStatus();

            $("#btnSubmit").click(function () {
                //var lcode = $('#txtLearnercode').val();
                //var ccode = $('#txtCentercode').val();			
                showAllOrgDetail(ddlrole.value, ddlstatus.value);
            });

        });

    </script>

    <script src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmcorrectionapproval_validation.js"></script>

    </body>

    </html>

