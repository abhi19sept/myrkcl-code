<?php
$title = "Re-Exam Payment";
include ('header.php');
include ('root_menu.php');
require("razorpay/checkout/manual.php");
if (isset($_REQUEST['code'])) {
    echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var FunctionCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
echo "<script>var CenterCode='" . $_SESSION['User_LoginId'] . "'</script>";
//echo "<script>var amount= " . $_REQUEST['amount'] . " </script>";
echo "<script>var PayType= 'Re Exam Fee Payment' </script>";

$payutxnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
echo "<script>var PayUTranID= '" . $payutxnid . "' </script>";

$random = (mt_rand(1000, 9999));
$random.= date("y");
$random.= date("m");
$random.= date("d");
$random.= date("H");
$random.= date("i");
$random.= date("s");
//echo $random;

echo "<script>var RKCLTxnId= '" . $random . "' </script>";
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Re-Exam Payment</div>
            <div class="panel-body">
                <form name="frmreexampayment" id="frmreexampayment" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">

                            </select>
                        </div> 
                    </div>  
                    <div class="container">
                        <div class="col-md-6 form-group">     
                            <label for="batch"> Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div> 

                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Payment Mode:</label>
                            <select id="paymode" name="paymode" class="form-control" onchange="toggle_visibility1('online_mode');">

                            </select>									
                        </div> 
                    </div>

                   <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="gender">Choose Payment Gateway:</label> <br />
                            <label class="radio-inline"> <input type="radio" id="gatewayPayu" checked="checked" class="gateway" name="gateway" value="payu" onchange="toggle_visibility1('online_mode');"/> Payu </label>
                            <label class="radio-inline"> <input type="radio" id="gatewayRazor" class="gateway" name="gateway" value="razorpay" onchange="toggle_visibility1('online_mode');"/> Razorpay </label>
                        </div>
                    </div>




                    <div>
                        <input type="hidden" name="amounts" id="amounts"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId" value="<?php echo $random; ?>"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtGeneratePayUId" id="txtGeneratePayUId" value="<?php echo $payutxnid; ?>"/>
                    </div>
                    <div id="menuList" name="menuList" style="margin-top:35px;"> 


                    </div> 
                    <div class="container">
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                    </div>                  


            </div>
        </div>   
    </div>
</form>

<form id="frmpostvalue" name="frmpostvalue" action="frmreexamfeepayment.php" method="post">
    <input type="hidden" id="code" name="code">
    <input type="hidden" id="paytypename" name="paytypename"> 
    <input type="hidden" id="paytypecode" name="paytypecode">    
    <input type="hidden" id="RKCLtranid" name="RKCLtranid"/>
    <input type="hidden" id="amount" name="amount">	
    <input type="hidden" id="txnid" name="txnid">	 
</form>

<form id="frmddpostvalue" name="frmddpostvalue" action="frmddreexampayment.php" method="post">
    <input type="hidden" id="ddcode" name="ddcode">
    <input type="hidden" id="ddpaytype" name="ddpaytype">                
    <input type="hidden" id="ddamount" name="ddamount">
    <input type="hidden" id="ddRKCLtranid" name="ddRKCLtranid"/>
    <input type="hidden" id="ddtxnid" name="ddtxnid">
</form>

<form id="frmwithoutfeepostvalue" name="frmwithoutfeepostvalue" action="frmwithoutreexampayment.php" method="post">
    <input type="hidden" id="withoutfeecode" name="withoutfeecode">
    <input type="hidden" id="withoutfeepaytype" name="withoutfeepaytype">                
    <input type="hidden" id="withoutfeeamount" name="withoutfeeamount">				
</form>

</body>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<script type="text/javascript">
<!--

    function toggle_visibility1(id) {
        var e = document.getElementById(id);
        var f = document.getElementById('paymode').value;
        if (f == "1")
        {
            e.style.display = 'block';
        }
        else {
            e.style.display = 'none';
        }
    }
//-->
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillCourse() {

            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLCOURSEBLOCKCENTER",
                success: function (data) {

                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {

            var selCourse = $(this).val();

            $.ajax({
                type: "post",
                url: "common/cfReexamPayment_Ver1.php",
                data: "action=FILLEXAMEVENT&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlExamEvent").html(data);
                }
            });

        });

        $("#ddlExamEvent").change(function () {

            showpaymentmode(this.value, ddlCourse.value);
        });

        function showpaymentmode(val, val1) {
            $.ajax({
                type: "post",
                url: "common/cfEvents.php",
                data: "action=SHOWReexamPay&examevent=" + val + "&course=" + val1 + "",
                success: function (data) {

                    $("#paymode").html(data);
                }
            });
        }

        $("#paymode").change(function () {
            var gateway = $(".gateway").val();
            if (gateway != "") {
                showAllData(paymode.value, ddlCourse.value, ddlExamEvent.value);
                getfee(paymode.value, ddlCourse.value, ddlExamEvent.value);
            }


        });
		
		$(".gateway").click(function () {
            if (this.value == "") {
                showAllData(this.value, ddlCourse.value, ddlExamEvent.value);
                getfee(this.value, ddlCourse.value, ddlExamEvent.value);
            } else {
                showAllData(paymode.value, ddlCourse.value, ddlExamEvent.value);
                getfee(paymode.value, ddlCourse.value, ddlExamEvent.value);
            }
        });
		
        function showAllData(val, val1, val2) {
            $.ajax({
                type: "post",
                url: "common/cfReexamPayment_Ver1.php",
                data: "action=SHOWALL&paymode=" + val + "&course=" + val1 + "&event=" + val2 + "",
                success: function (data) {

                    $("#menuList").html(data);
                    $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                    $('#btnSubmit').show();
                }
            });
        }
        function getfee(val, val1, val2)
        {
            $.ajax({
                type: "post",
                url: "common/cfReexamPayment_Ver1.php",
                data: "action=Fee&paymode=" + val + "&course=" + val1 + "&event=" + val2 + "",
                success: function (data) {

                    amounts.value = data;
                }
            });
        }


        $("#btnSubmit").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $('#btnSubmit').hide();
            var url = "common/cfReexamPayment_Ver1.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmreexampayment").serialize();
            ;

            if (Mode == 'Add')
            {
                data = "action=ADD&" + forminput; // serializes the form's elements.
            }
            else
            {
                //data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {

                    $('#response').empty();
                    if (data == 0) {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please Checked Atleast one checkbox." + "</span></p>");
                    }
                    else {
                       data = $.parseJSON(data);
                        if (data.payby == 'payu') {
                        var paymode2 = $('#paymode').val();

                        $('#code').val(CenterCode);
                        $('#paytype').val(PayType);

                        $('#ddcode').val(CenterCode);
                        $('#ddpaytype').val(PayType);

                        var payname = $("#ddlExamEvent option:selected").text();


                        $('#paytypecode').val(ddlExamEvent.value);
                        $('#paytypename').val(payname);

                        $('#RKCLtranid').val(RKCLTxnId);
                        $('#ddRKCLtranid').val(RKCLTxnId);					

                        $('#txnid').val(PayUTranID);
                        $('#ddtxnid').val(PayUTranID);

                        //$('#ddmode') = $('#paymode').val();
                        amount.value = data.amount;
                            ddamount.value = data.amount;

                        //paytype.value = PayType;

                        if (paymode2 == "2") {

                            $('#frmddpostvalue').submit();
                        }
                        else if (paymode2 == "4") {
                            $('#frmwithoutfeepostvalue').submit();
                        }
                        else {

                            $('#frmpostvalue').submit();
                        }
						} else {
                            var options = data;
                            <?php include("razorpay/razorpay.js"); ?>
                        }
                    }

                }
            });

            return false; // avoid to execute the actual submit of the form.

        });
		 $('#menuList').on('click', '#checkuncheckall', function (){
            checkuncheckall(this.checked);
        });
    });
  function checkuncheckall(checked) {
        var aa = document.getElementById('frmreexampayment');
        for (var i =0; i < aa.elements.length; i++) {
            aa.elements[i].checked = checked;
        }
    }
</script>
</body>

</html>