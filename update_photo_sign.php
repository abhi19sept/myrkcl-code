<?php
$title = 'Upload Photo / Signs';
include ('header.php');
include ('root_menu.php');
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Process to <?php echo $title; ?></div>
            <div class="panel-body">
            	<form name="frmexameventmaster" id="frmexameventmaster" action="common/cflearnerdetails.php" method="post" class="form-inline" enctype="multipart/form-data">     
            			<div class="container">
            			<div id="response" class="small"></div>
            		</div>
                        <div class="container">
							<div id="errorBox"></div>
							<div class="col-sm-6 form-group"  id="non-printable"> 
                                <label for="edistrict">Upload File: </label>
                                <input type="file" name="images[]" id="images" onchange="checkFiles()" multiple >
                                <div id="psize1">
                                    <span  style="font-size:10px;">Note: .jpg, .png</span>
                                </div>
                            </div>
                            <div class="col-sm-6 form-group"  id="non-printable">
                                <p>&nbsp;</p>
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Upload" />
                                <input type="hidden" name="action" value="uploadPics" />
                            </div>
                    </div>
                </form>
                <div class="container small" id="non-printable">
                    <p>&nbsp;</p>
                    <p>Note:</p>
                    <ul>
                        <li>You are allow to upload <b>.png / .jpg</b> format files only.</li>
                        <li>Name format of photo should be exactly as: <b>learnercode_photo.png</b> or <b>.jpg</b></li>
                        <li>Name format of sign should be exactly as: <b>learnercode_sign.png</b> or <b>.jpg</b></li>
                        <li>*Images without proper name format are simply ignore to process.</li>
                        <li>Maximum <b>20 files</b> are allow to upload at a time.</li>
                        <li>Only those images will update, who's learner code is exists in eligible learners records.</li>
                    </ul>
                </div>
                
                <div id="gird"></div> 
            </div>
        </div>
    </div>
</div>


</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript" src="scripts/jquery.form.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
    	$("#btnSubmit").click(function () {
            if (checkFiles()) {
                uploadFiles();
            }
        });
        $('#gird').on('click', '#btnUpdate', function () {
            updatePhotoSigns();
        });
        displayList();
    });

    function checkFiles() {
        $('#response').empty();
        var target = document.getElementById('images');
        var totalFiles = target.files.length;
        if(totalFiles === 0){
        alert("Please select any file to upload.");
            return false;
        } else if(totalFiles > 20) {
            alert("You can upload maximum 20 files at a time.");
            return false;
        }
        var invalid = '';
        for (var i=0; i<totalFiles; i++) {
            var fname = target.files[i].name;
            var ext = fname.split('.').pop().toLowerCase();
            if ($.inArray(ext, ['png', 'jpg']) == -1) {
                invalid = invalid + fname + '<br /> ';
            } else {
                var nameParts = fname.split('_');
                if (nameParts.length == 2) {
                    var subpart = nameParts[1].split('.');
                    if (subpart[0] != 'photo' && subpart[0] != 'sign') {
                        invalid = invalid + fname + '<br /> ';
                    }
                } else {
                    invalid = invalid + fname + '<br /> ';
                }
            }
        }
        if (invalid != '') {
            setresponse('Invalid naming convention found in: <br />' + invalid);
            target.value = '';
            return false;
        }
        
        return true;
    }

    function setresponse(msg) {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span>&nbsp;<span>" + msg +"</span></p>");
    }

    function setsuccessresponse(msg) {
        $('#response').empty();
        $('#response').append("<p class='success'><span><img src=images/correct.gif width=10px /></span>&nbsp;<span>" + msg +"</span></p>");
    }

    function uploadFiles() {
        var target = document.getElementById('images');
        $('#frmexameventmaster').ajaxForm({
            //display the uploaded images
            target:'#response',
            beforeSubmit:function(e){
                setresponse('Uploading images...');
            },
            success:function(e){
                $('.response').empty();
                target.value = '';
                displayList();
            },
            error:function(e){
            }
        }).submit();
    }

    function displayList() {
        setresponse('Getting uploaded images.....');
        var url = "common/cflearnerdetails.php"; // the script where you handle the form input.
        var data = "action=getPics"; // serializes the form's elements.
        $.ajax({
            type: "post",
            url: url,
            data: data,
            success: function (data) {
                $('#footer').hide();
                $("#gird").html(data);
                $('#response').empty();
            }
        });
    }

    function updatePhotoSigns() {
        setresponse('Update Photo / Signs.....');
        var url = "common/cflearnerdetails.php"; // the script where you handle the form input.
        var data = "action=updatePhotoSigns"; // serializes the form's elements.
        $.ajax({
            type: "post",
            url: url,
            data: data,
            success: function (data) {
                setsuccessresponse('Photo / Sign are uploaded for related learners.');
                $("#gird").html(data);
            }
        });
    }

    

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>