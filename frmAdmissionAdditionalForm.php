<?php
$title = "RS-CIT Additional Fields for Shyama Prasad Scheme";
include ('header.php');
include ('root_menu.php');
if (isset($_SESSION)) {
    //echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var Role='" . $_SESSION['User_UserRoll'] . "'</script>";
} else {

    echo "<script>var Mode='Add'</script>";
    echo "<script>var Role='0'</script>";
}
?>		

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Fill Additional Form entries for RS-CIT Shyama Prasad Scheme Learner</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmAdmissionAdditionalForm" id="frmAdmissionAdditionalForm" class="form-inline" role="form" method="post">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                    </div>


                    <div id="detailsdiv">				


                        <div class="container"> 
                            <div class="col-md-6 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">
                                    <option value="0" selected>Select Course</option>
                                    <option value="26">RS-CIT SPMRM</option>
                                </select>
                            </div> 

                            <div class="col-md-6 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">

                                </select>
                            </div> 

                        </div>					
                        <div id="grid" name="grid" style="margin-top:35px;"> </div> 
                    </div>

            </div>
        </div>   
    </div>
</form>
</div>
<div id="LearnerDetails" class="modal" style="padding-top:50px !important">            
    <div class="modal-content" style="width: 90%;">
        <div class="modal-header">
            <span class="close mm">&times;</span>
                <h6>Fill Additional Form entries for RS-CIT Shyama Prasad Scheme Learner</h6>
        </div>
        <div class="modal-body" style="max-height: 1000px;">
    <form name="frmprocess" action="common/cfAdmissionAdditionalForm.php" id="frmprocess" class="form-inline"
            role="form" enctype="multipart/form-data">
        <div id="responses"></div>
            <div class="container">
                <div class="col-sm-4 form-group">     
                  <label for="learnercode">Learner Code:<span class="star">*</span></label>
                  <input type="text" class="form-control" maxlength="20" name="txtLearnerCode" id="txtLearnerCode"
                                placeholder="Learner Code" readonly='true'>                          
                </div>
            
                            
                <div class="col-sm-4 form-group"> 
                    <label for="learnername">Learner Name:<span class="star">*</span></label>
                        <input type="text" class="form-control text-uppercase" maxlength="50" name="txtLCorrectName"
                        id="txtLCorrectName" readonly='true'>                 
                </div>
                    
               <div class="col-sm-4 form-group">     
                    <label for="faname">Father/Husband Correct Name:<span class="star">*</span></label>
                        <input type="text" class="form-control text-uppercase" id="txtFCorrectName" name="txtFCorrectName" readonly='true'>
                            <input type="hidden" class="form-control" name="txtlcode" id="txtlcode"/>
                            <input type="hidden" class="form-control" name="txtacode" id="txtacode"/>
                            <input type="hidden" class="form-control" maxlength="50" name="action" id="action" value="Update"/>
                            <input type="hidden" class="form-control" maxlength="50" name="txtcid" id="txtcid"/>
               </div>  
           </div>
            <div class="container">
                  <div class="col-sm-4 form-group" id="photodiv"> 
                      <label for="photo">Highest Qualification Document:<span class="star">*</span></label>
                      </br>
                    <img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="fileCasteCertificate" width="80px" height="107px">
                      <input type="file" class="form-control" id="filePhoto" name="filePhoto" onchange="ValidateSingleInput(this);">

                      <span style="font-size:10px;">Note : JPG,JPEG Allowed Size =50KB to 100KB</span>
                  </div> 
    

                    <div class="col-sm-4 form-group"> 
                        <label for="bankdistrict">Select Caste Category:</label><span id="pon" style="font-size:10px;"></span>
                        <select id="ddlCategory" name="ddlCategory" class="proofdoc form-control"></select>
                        
                    </div>                  
              

                <div class="col-sm-4 form-group"  style="display: none;" style=" margin-bottom: 50px;" id="castdiv"> 
                    <label for="photo">Attach Caste Proof Certificate<span id="casttype"></span>:<span class="star">*</span></label> </br>
                    <img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="fileCasteCertificate" width="80px" height="107px">
                    <input type="file" class="form-control" id="fileCasteProof" name="fileCasteProof" onchange="ValidateSingleInput(this);">
                    <span style="font-size:10px;">Note : JPG,JPEG Allowed Size =50KB to 100KB</span>
                </div> 

                <div class="col-sm-4 form-group"  style=" margin-bottom: 50px;"> 
                    <label for="photo">Class X Marksheet:<span id="casttype"></span>:<span class="star">*</span></label> </br>
                    <img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="fileCasteCertificate" width="80px" height="107px">
                    <input type="file" class="form-control" id="fileSSCcert" name="fileSSCcert" onchange="ValidateSingleInput(this);">
                    <span style="font-size:10px;">Note : JPG,JPEG Allowed Size =50KB to 100KB</span>
                </div> 

                <div class="col-sm-4 form-group"> 
                    <label for="learnername">Special Preference Category:<span class="star">*</span></label>
                     <select id="ddlSpecialPref" name="ddlSpecialPref" class="proofdoc form-control">
                         <option value="NA">No Preference</option>
                         <option value="widow">Widow</option>
                         <option value="divorcee">Divorcee</option>
                         <option value="palanhar">Palanhar (Orphan) Yojna</option>
                     </select>                 
                </div>
                <div class="col-sm-4 form-group"  style="display: none;" id="prefdiv"> 
                    <label for="learnername">Special Preference Document:<span class="star">*</span></label>                    
                    <img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="fileCasteCertificate" width="80px" height="107px"> 
                    <input type="file" class="form-control" id="fileSpecialPref" name="fileSpecialPref" onchange="ValidateSingleInput(this);">
                    <span style="font-size:10px;">Note : JPG,JPEG Allowed Size =50KB to 100KB</span>             
                </div>   
                <div class="col-sm-4 form-group"> 
                    <label for="learnername">Physically Challenged Status :<span class="star">*</span></label>
                     <select id="ddlPHstatus" name="ddlPHstatus" class="proofdoc form-control">
                         <option value="0" selected="">No</option>
                         <option value="1">Yes</option>
                     </select>                 
                </div>

               
            </div>
            <div class="container">
                <div class="col-sm-4 form-group"> 
                    <label for="learnername">Scanned Aadhar Card :<span class="star">*</span></label>
                    </br>
                    <img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="fileCasteCertificate" width="80px" height="107px">
                    <input type="file" class="form-control" id="fileAadhar" name="fileAadhar" onchange="ValidateSingleInput(this);">
                    <span style="font-size:10px;">Note : JPG,JPEG Allowed Size =50KB to 100KB</span>                
                </div>
                <div class="col-sm-4 form-group"> 
                    <label for="learnername">Jan Aadhar Number :<span class="star">*</span></label>

                    <input type="text" class="form-control" maxlength="10" name="JanAadhar" id="JanAadhar"  placeholder="JanAadhar Number" required="required" onkeypress="javascript:return allownumbers(event);">
                </div>
                <div class="col-sm-4 form-group"> 
                    <label for="learnername">Scanned Jan Aadhar :<span class="star">*</span></label>
                    </br>
                    <img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="fileCasteCertificate" width="80px" height="107px">
                    <input type="file" class="form-control" id="fileJanAadhar" name="fileJanAadhar" onchange="ValidateSingleInput(this);">
                    <span style="font-size:10px;">Note : JPG,JPEG Allowed Size =50KB to 100KB</span>                
                </div>
                <div class="col-sm-4 form-group"> 
                    <label for="learnername">Ration Number :</label>

                    <input type="text" class="form-control" maxlength="12" name="Ration" id="Ration"  placeholder="Ration Number" onkeypress="javascript:return allownumbers(event);">
                </div>
                <div class="col-sm-4 form-group" id="divrationdoc" style="display: none"> 
                    <label for="learnername">Scanned Ration :<span class="star">*</span></label>
                    </br>
                    <img id="uploadPreview8" src="images/sampleproof.png" id="uploadPreview8" name="fileCasteCertificate" width="80px" height="107px">
                    <input type="file" class="form-control" id="fileRation" name="fileRation" onchange="ValidateSingleInput(this);">
                    <span style="font-size:10px;">Note : JPG,JPEG Allowed Size =50KB to 100KB</span>                
                </div>                    
            </div>
            <div class="container">
                <div class="col-sm-3"  style="float:left;"> 
                    <input type="submit" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Update"/> 
                </div>                  
            </div>
        </form>     
                
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script language="javascript" type="text/javascript">
    var _validFileExtensions = ['png','jpg','jpeg'];
    function ValidateSingleInput(oInput) {
        if (oInput.type == "file") {
            var sFileName = oInput.value;

            var iFileSize = oInput.files[0].size;

            if (sFileName.length > 0) {
                if (iFileSize > 50000 && iFileSize < 100000) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }

                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }

                } else {
                    alert("File Size should be between 50KB to 100KB.");
                    oInput.value = "";
                }
            }
        }
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
function checkPhoto(target) {
    var ext = $('#filePhoto').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("filePhoto").value = '';
            return false;
        }

    if(target.files[0].size > 50000) {                  
        alert("Image size should less or equal 50 KB");
        document.getElementById("filePhoto").value = '';
        return false;
    }
    else if(target.files[0].size < 100000) {
                alert("Image size should be greater than 100 KB");
                document.getElementById("filePhoto").value = '';
                return false;
            }
    document.getElementById("filePhoto").innerHTML = "";
    return true;
}
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        jQuery('#Ration').on('input propertychange paste', function() {
            if($("#Ration").val() == ""){
                $("#divrationdoc").hide();
            }
            else{
                $("#divrationdoc").show();
            }
        });
    $("#ddlCategory").change(function () {
        var cast = $(this).val();
       
        if(cast == 1){
           $("#castdiv").hide();
        }
        else{
            $("#castdiv").show();
        }
    });
    
    $("#ddlSpecialPref").change(function () {
        var pref = $(this).val();
       
        if(pref == "NA"){
           $("#prefdiv").hide();
        }
        else{
            $("#prefdiv").show();
        }
    });
        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            if (Role == '7')
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                    success: function (data) {

                        $("#ddlBatch").html(data);
                    }
                });
            } else
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILL&values=" + selCourse + "",
                    success: function (data) {

                        $("#ddlBatch").html(data);
                    }
                });
            }

        });

        function showAllData(val, val1) {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfAdmissionAdditionalForm.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "",
                success: function (data)
                {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $('#btnSubmit').show();

                    //showData();
                }
            });
        }


        $("#ddlBatch").change(function () {
            showAllData(this.value, ddlCourse.value);
        });

        function FillLearnerType() {
            $.ajax({
                type: "post",
                url: "common/cfCategoryMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCategory").html(data);
                }
            });
        }
        
        $("#grid").on('click', '.Update',function(){
            
            //$("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
            var cid = $(this).attr('id');
            $('#txtcid').val(cid);
            
            var url = "common/cfAdmissionAdditionalForm.php"; // the script where you handle the form input.           
            var data;            
            data = "action=GetExistingDetails&cid=" + txtcid.value + ""; //

            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {  
                  
                    //$('div[id=loader-wrapper]').remove();
                    data = $.parseJSON(data);
                    txtLCorrectName.value = data[0].cfname;
                    txtFCorrectName.value = data[0].cfaname;
                    txtacode.value = data[0].acode;
                    txtlcode.value = data[0].lcode;
                    txtLearnerCode.value = data[0].lcode;
                    
                    var modal = document.getElementById('LearnerDetails');
                    var span = document.getElementsByClassName("close")[0];
                    modal.style.display = "block";
                    span.onclick = function() { 
                        modal.style.display = "none";
                    }
                    FillLearnerType();
                }
            });
    
        });
        $("#frmprocess").on('submit',(function(e) {
           

        if ($("#frmprocess").valid()) {
            $('#responses').empty();
            $('#responses').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");     
            e.preventDefault();
              $.ajax({ 
                url: "common/cfAdmissionAdditionalForm.php",
                type: "POST",
                data:  new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                success: function(data)
                  { 
//                      alert(data);
                     if (data.trim() == "Successfully Done")
                            {
                                 $('#btnSubmit1').prop('disabled', true); 
                                $('#responses').empty();
                                $('#responses').append("<p class='error'><span>" + data + "</span></p>");
                                 window.setTimeout(function () {
                                     window.location.href = "frmAdmissionAdditionalForm.php";
                                 }, 2000);

                                Mode = "Add";
                                resetForm("form");
                            }
                            else
                            {
                                $('#responses').empty();
                                $('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                            }
                  },
                    error: function(e) 
                    {
                       $("#errmsg").html(e).fadeIn();
                    }           
            });
        }
    return false;
}));  
        
    });
</script>
</html>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>