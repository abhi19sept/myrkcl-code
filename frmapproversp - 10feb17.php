<?php
$title = "Change RSP Application Process";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['Code'])) {
    echo "<script>var Code='" . $_REQUEST['Code'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<div style="min-height:430px !important;max-height:auto !important;">

    <!-- <div class="jumbotron"> -->
    <form name="form" id="form" action="" class="form-inline">     


        <div class="container">
            <div class="container">
                <div id="response"></div>

            </div>        
            <div id="errorBox"></div>
        </div>
        <br>
        <div id="main-content" style="display:none;">
            <div class="container">			 
                <div class="panel panel-primary" style="margin-top:20px !important;">
                    <div class="panel-heading">Applicant IT-GK Details</div>
                    <div class="panel-body">
                        <div class="container">
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Name Of Center:</label>
                                <textarea class="form-control" readonly="true" name="txtCName" id="txtCName"  placeholder="Center Name"></textarea>   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Mobile:</label>
                                <input type="text" class="form-control" readonly="true" name="txtCMobile" id="txtCMobile"  placeholder="Center Mobile">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="address">District:</label>
                                <input type="text" class="form-control" readonly="true" name="txtCDistrict" id="txtCDistrict" placeholder="Center District">    
                            </div>
                            <div class="col-sm-4 form-group">     
                                <label for="address">Tehsil:</label>
                                <input type="text" class="form-control" readonly="true" id="txtCTehsil" name="txtCTehsil" placeholder="Center Tehsil">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">			 
                <div class="panel panel-primary" style="margin-top:20px !important;">
                    <div class="panel-heading">Current RSP Details</div>
                    <div class="panel-body">
                        <div class="container">
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Name of Current RSP:</label>
                                <textarea class="form-control" readonly="true" name="txtRName" id="txtRName"  placeholder="Name"></textarea>   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Date of Establishment:</label>
                                <input type="text" class="form-control" readonly="true" name="txtRMobile" id="txtRMobile"  placeholder="Mobile">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="address">District:</label>
                                <input type="text" class="form-control" readonly="true" name="txtRDistrict" id="txtRDistrict" placeholder="District">    
                            </div>
                            <div class="col-sm-4 form-group">     
                                <label for="address">Tehsil:</label>
                                <input type="text" class="form-control" readonly="true" id="txtRTehsil" name="txtRTehsil" placeholder="Tehsil">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">			 
                <div class="panel panel-primary" style="margin-top:20px !important;">
                    <div class="panel-heading">Selected New RSP Details</div>
                    <div class="panel-body">
                        <div class="container">
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Selected RSP:</label>
                                <textarea class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center"></textarea>
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Mobile No:</label>
                                <input type="text" class="form-control" readonly="true" name="txtRegno" id="txtRegno" placeholder="Registration No"> 
                                <input type="hidden" class="form-control" readonly="true" name="txtRspCode" id="txtRspCode" placeholder="RSP Code">                                 
                            </div>
                            <div class="col-sm-4 form-group">     
                                <label for="faname">Date of Establishment:</label>
                                <input type="text" class="form-control" readonly="true" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Type of Organization:</label>
                                <input type="text" class="form-control" readonly="true" name="txtType" id="txtType" placeholder="Type Of Organization">  
                            </div>
                        </div>

                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:</label>
                                <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Tehsil:</label>
                                <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="address">Street:</label>
                                <input type="text" class="form-control" readonly="true" name="txtStreet" id="txtStreet" placeholder="Street">    
                            </div>
                            <div class="col-sm-4 form-group">     
                                <label for="address">Road:</label>
                                <input type="text" class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road">
                            </div>
                        </div>	

                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="address">Selected Reason:</label>
                                <input type="text" class="form-control" readonly name="ddlReason" id="ddlReason" placeholder="Street">    
                            </div>


                            <div class="col-sm-4 form-group">     
                                <label for="address">Reason Details:</label>
                                <textarea class="form-control" readonly id="txtReason" name="txtReason" placeholder="Road"></textarea>

                            </div>
                        </div>	


                        <div class="container">	
                            <div class="col-sm-4 form-group"> 
                                <label for="status">Action:<span class="star">*</span></label>
                                <select id="ddlstatus" name="ddlstatus" class="form-control" onchange="toggle_visibility1('remark')">
                                    <option selected="true" value="">Select</option>
                                    <option value="Approved" >Approve</option>
                                    <option value="Rejected" >Reject</option>
                                </select>    
                            </div>
                            <div class="col-sm-4 form-group" id="remark" style="display:none;"> 
                                <label for="pan">Remark:<span class="star">*</span></label>
                                <input type="text" class="form-control"  name="txtRemark" id="txtRemark"  placeholder="Remark">
                            </div>


                        </div>		

                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>  
                    </div>
                </div>
            </div> 

            <div class="container">			 
                <div class="panel panel-primary" style="margin-top:20px !important;">
                    <div class="panel-heading">Change Service Provider Details - History</div>
                    <div class="panel-body">
                        <div id="gird"></div>  
                    </div>
                </div>
            </div>
        </div>
    </form> 

</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>
<script type="text/javascript">
    function toggle_visibility1(id) {
        var e = document.getElementById(id);

        //alert(e);
        var f = document.getElementById('ddlstatus').value;
        //alert(f);
        if (f == "Rejected")
        {
            e.style.display = 'block';
        }
        else {
            e.style.display = 'none';
        }

    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Add')
        {
            //alert(1);
            fillForm();
            fillCenterDetails();
            fillCurrentRSP();
        }

        function fillForm()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfChangeRspApproval.php",
                data: "action=APPROVE&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    txtName1.value = data[0].orgname;
                    txtRspCode.value = data[0].rspcode;
                    txtRegno.value = data[0].regno;
                    txtEstdate.value = data[0].fdate;
                    txtType.value = data[0].orgtype;

                    txtDistrict.value = data[0].district;
                    txtTehsil.value = data[0].tehsil;
                    txtStreet.value = data[0].street;
                    ddlReason.value = data[0].selectreason;
                    txtReason.value = data[0].reason;
                    txtRoad.value = data[0].road;
                    $('#main-content').show(3000);
                }
            });
        }

        function fillCenterDetails()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfChangeRspApproval.php",
                data: "action=CDetails&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    txtCName.value = data[0].cname;
                    txtCMobile.value = data[0].cmobile;

                    txtCDistrict.value = data[0].cdistrict;
                    txtCTehsil.value = data[0].ctehsil;
                }
            });
        }

        function fillCurrentRSP()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfChangeRspApproval.php",
                data: "action=RSPDetails&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    txtRName.value = data[0].rname;
                    txtRMobile.value = data[0].rmobile;

                    txtRDistrict.value = data[0].rdistrict;
                    txtRTehsil.value = data[0].rtehsil;
                }
            });
        }

        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post", url: "common/cfChangeRspApproval.php",
                data: "action=SHOWHISTORY&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print']
                    });



                }
            });
        }

        showData();

        $("#btnSubmit").click(function () {
            if ($("#form").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfChangeRspApproval.php"; // the script where you handle the form input.            
                var data;
                if (Mode == 'Add')
                {
                    data = "action=UPDATE&centercode=" + Code + "&status=" + ddlstatus.value + "&remark=" + txtRemark.value + "&rspcode=" + txtRspCode.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&centercode=" + Code + "&status=" + ddlstatus.value + "&remark=" + txtRemark.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmchangerspapproval.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("form");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();


                    }
                });
            }
            return false;
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmcorrectionprocess_validation.js"></script>
</html>