<?php
//$title="Forgot Password";
include ('outer_header.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Forgot'</script>";
}

$itgk = '';
if (isset($_REQUEST['itgk'])) {
    $itgk = $_REQUEST['itgk'];
} else {
    $itgk = '';
}
?>

<div style="min-height:391px !important;max-height:1500px !important">	
    <div class="forgotblog" id="finalc">   
        <div class="panel" style="margin-top:50px; box-shadow: 3px 3px 20px 5px #d0d0d0; border-radius: 10px;">
            <div class="panel-body">
                <form class="form-inline" role="form" name="forgot_password_form" id="forgot_password_form">
                    <div class="container1">


                        <div id="userfields" >
                            <div class="locksmall"><img src="images/lock_icon_small.png"></div>
                            <div class="panelforgot">Security Validate</div>
                            <div id="response"></div>
                            <div style="margin-left:23px; width: 80%;float: left;"><b>Security Question</b></div>

                            <div style="clear: both;"></div>

                            <div style="float:left; margin-left:23px; margin-top:15px;  display: none;" id="sqexits2">
                                <span> 
                                    Please select your security question to verify your identity. 
                                </span>
                                <p></p>
                            </div>
                            <div id="response1" style="width: 100%;float: left;margin-left: 20px;"></div>
                            <div class="col-lg-12 col-md-12" style="padding-bottom: 10px; margin-left: 8px;">
                                <label for="psw">Please Choose a security question.</label>
                                <select id="SQ_ID" name="SQ_ID" class="form-control  text-uppercase" style="width: 460px;">								  
                                </select>  

                            </div>
                            <div class="col-lg-12 col-md-12" style="padding-bottom: 2px;">
                                <label for="psw" style="margin-left: 10px;">Answer the question</label>
                                <input class="inputsearch" type="text" id="securityqueans" name="securityqueans" required>  
                                <input value=" <?php echo $itgk;?>" type="hidden"  id="txtCenter" name="txtCenter" class="inputsearch"/>
                            </div>
                        </div>
                        
                                               

                    </div>
                    <div class="submitbtn" id="btnfinal" style="text-align: center;">

                        <input type="button" name="btnValidateOTP" id="btnValidateOTP" class="btn subbtn2"
                               value="VALIDATE OTP" style="display: none;"/>

                        <input type="button" name="btnseqcurityq" id="btnseqcurityq" class="btn subbtn3"
                               value="SUBMIT" style="text-align: left;"/>


                        <div id="resendbtn" class="resendotp" style="display: none;"><a href="#">Resend OTP</a></div>
                    </div>											
                </form>
            </div>
        </div>
    </div>
</div>
</body>	
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<script>


    


    function validAddress(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[A-Za-z,-/&0-9]")

        if (key == 8 || key == 0 || key == 32) {
            keychar = "a";
        }


        return reg.test(keychar);
    }

    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")

        if (key == 8 || key == 0) {
            keychar = 8;
        }

        return reg.test(keychar);
    }


    
</script>


<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {

        var showmobile;

        $("#resendbtn").click(function () {

            setProcessing();
            var url = "common/cfforgotpassword.php"; // the script where you handle the form input.
            var data = "action=RESENDOTPLOGIN&Center=" + ccode.value + ""; // serializes the form's elements.
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    $('#response').empty();
                    if (data != 0) {
                        data = $.parseJSON(data);
                            $('#response').append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span style='color: green;'>" + data.msg + "</span></p>");
                            $('#userfields').html(data.frm);
                            $("#btnForgotpass").hide(1000);
                            $("#btnValidateOTP").show(1000);
                            $("#btnseqcurityq").hide(1000);
                            $("#resendbtn").show(1000);

                    } else {
                        $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Invalid Username. Please try again." + "</span></p>");
                    }
                }
            });

        });

       

        function setProcessing() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        }

        $("#btnValidateOTP").click(function () {
            setProcessing();
            var url = "common/cfLogin.php";
            var data = "action=VerifyOTPSecurityAns&otp=" + txtOTP.value + "&oid=" + oid.value + "&ccode=" + ccode.value + "";
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    $('#response').empty();
                    //alert(data);
                    $("#response").css({"margin-top": "20px"});
                    if (data == 0)
                    {
                        $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Invalid OTP. Please try again." + "</span></p>");
                    } else if (data == 'OTPBLANCK') {
                        $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Please enter valid OTP." + "</span></p>");
                    } else if (data == 'EXPIRE') {
                        $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Your OTP has Expired. Please try again." + "</span></p>");
                    } else if (data == 'NORECORD') {
                        $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Your OTP has Expired or Invalid. Please try again." + "</span></p>");
                    } else {
                        $('#response').empty();
                        $('#response').append("<p class='state-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                       
                         setTimeout(function () {
                                   window.location.href = 'dashboard.php';
                               }, 1000);
                    }
                }
            });
        });


        function FillSecurityQuestion() {
            $.ajax({
                type: "post",
                url: "common/cfforgotpassword.php",
                data: "action=FillSecurityQuestion",
                success: function (data) {
                    $("#SQ_ID").html(data);
                }
            });
        }
        FillSecurityQuestion();

        $("#btnseqcurityq").click(function () {
//setProcessing();
            var SQ_ID = $("#SQ_ID").val();
            var securityqueans = $("#securityqueans").val();
            if (SQ_ID == 0) {
                $('#response1').empty();
                $('#response1').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Please Choose a security question." + "</span></p>");
            } else if (securityqueans == '') {
                $('#response1').empty();
                $('#response1').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Please Answer the question." + "</span></p>");
            } else
            {
                var url = "common/cfforgotpassword.php";
                var data = "action=SecurityAns&SQ_ID=" + SQ_ID + "&securityqueans=" + securityqueans + "&ccode=" + txtCenter.value + "";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#response1').empty();
                        if (data == 'ANSBLANCK')
                        {
                            $('#response1').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span style='color: green;'>  Please Answer the question.</span></p>");
                        } 
                        else if (data == 'NORECORD') 
                        {
                            $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span>" + "   Your security question's answer is not matched with our record. Please try again or contact to RKCL." + "</span></p>");
                        } 
                        else 
                        {
                            data = $.parseJSON(data);
                            $('#userfields').html(data.frm);
                            $("#btnForgotpass").hide(1000);
                            $("#btnValidateOTP").show(1000);
                            $("#btnseqcurityq").hide(1000);
                            $("#resendbtn").show(1000);

                        }
                    }
                });
            }

        });

        

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<style>
    .error {
        color: #D95C5C!important;
    }
</style>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/forgotpasswordvalidation.js" type="text/javascript"></script>
</html>