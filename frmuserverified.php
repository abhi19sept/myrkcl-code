<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Welcome To RKCL Sign Up</title>

    </head>

    <body>
        <div class="wrapper">

            <?php
            include './include/header.html';

            include './include/menu.php';
            ?>
            <div class="main">

                <form id="frmsignup" name="frmsignup" action="">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="field">
                        <tr>
                            <td style="width: 100%;height: 400px;" align="center" valign="middle" >
                                <table border="0" cellpadding="0" cellspacing="10" width="400px" class="frmtable" >
                                    <tr>
                                        <td colspan="3">
                                            <h1> User Verification Successful </h1>
                                        </td>
                                    </tr>
                                    
                                    <tr><td>
                                    <p>Your Verification with is Successful with RKCL. We have sent you an Email with Username and Password. 
                                    Please login in your Account for New Center Registration Process. </p>
                                            </td></tr>
                                    
                                    
                                </table>
                            </td>
                        </tr>                     
                    </table>
                </form>

            </div>
            <?php include './include/footer.html'; ?>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
            $("#btnSubmit").click(function() {
		           var url = "common/cfSignUp.php"; // the script where you handle the form input.
		           $.ajax({
		                  type: "POST",
		                  url: url,
		                  data:"action=ADD&pagename=signup&mobile="+txtMobile.value+"&emailid="+txtEmailId.value+"", // serializes the form's elements.
		                  success: function(data)
		                  {
		                      $('#response').empty();
		                      $('#response').append("<span class='error'>"+data+"</span>"); // show response from the php script.
		                      window.setTimeout(function ()
                                        {
                                            SuccessfullySignUp();
                                        }, 3000);
						   
		                  }
		                });
                                return false; // avoid to execute the actual submit of the form.
		   });
                   
                   function SuccessfullySignUp()
            {
                
                window.location.href = 'frmsignupsuccess.php';
            }
            
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }


        </script>
    </body>

</html>