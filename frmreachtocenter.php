<?php

$title="Center Dashboard ";
include ('header.php'); 
include ('root_menu.php');  
 //echo $_SESSION['User_UserRoll'];//die;
 //echo "<pre>";print_r($_SESSION);
    if($_SESSION['User_UserRoll']==1 || $_SESSION['User_UserRoll']==7 || $_SESSION['User_UserRoll']==14){
     // you are in
    }else{
           header('Location: index.php');
        exit;
    }

include ('DAL/sendsms.php');

?>

	
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container learner_nearcenter"> 
        <div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">
                <span  style="float: right;">
                    <a href="frmcenterdashboard.php" style=" color: white; font-weight: bold;" >Back To Dashboard</a>
                </span> 
                <span style="font-weight: bold;">Search Center Location</span> </div>
           
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
					<form name="frmcenter_learner" id="frmcenter_learner" class="form-inline" role="form" enctype="multipart/form-data">     

						<div class="container">
							<div class="container">
								<div id="response"></div>

							</div>        
							<div id="errorBox"></div>
						</div>

                                            <input type="hidden" id="itgkcode" name="itgkcode" value="<?php  echo $_REQUEST['itgk'];?>"
					</form>
					<div id="marker_count" style="text-align: right;" ></div>
					<div id="google_map" style="width: 100%;margin-top: 20px;"></div>
					<div class="container map-direction">
						<div class="col-sm-4" id="directions_1">
							<h2></h2>
							<div id="right-panel_1" class="right-panel"></div>
						</div>
						<div class="col-sm-4" id="directions_2">
							<h2></h2>
							<div id="right-panel_2" class="right-panel"></div>
						</div>
						<div class="col-sm-4" id="directions_3">
							<h2></h2>
							<div id="right-panel_3" class="right-panel"></div>
						</div>
					</div>
					
					
				</div>
			
			</div>   
		</div>
	</div>
</div>
<input type="hidden" value="" id="otpval_locmodal">
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>



<!--<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3LDfJa6bQsDLslWSt7MdkTD45zSyw4Ac&libraries=places"></script>
<!--<script type="text/javascript" src="bootcss/js/learner_google_places.js"></script>-->

<script>

$(document).ready(function() {
	var mapCenter = new google.maps.LatLng(26.960514208305806, 74.76696749999996); //Google map Coordinates
	var geocoder = new google.maps.Geocoder;
	var map;
	var directionsDisplay;
        var directionsService;
	
	map_initialize(); // initialize google map
	
	//############### Google Map Initialize ##############
	function map_initialize()
	{
			var googleMapOptions = 
			{ 
				center: mapCenter, // map center
				zoom: 7, //zoom level, 0 = earth view to higher value
				maxZoom: 20,
				minZoom: 5,
				zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL //zoom control size
			},
				scaleControl: true, // enable scale control
				mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
			};
		
		   	map = new google.maps.Map(document.getElementById("google_map"), googleMapOptions);		
			
			
			//Right Click to Drop a New Marker
			google.maps.event.addListener(map, 'rightclick', function(event){
				//Edit form to be displayed with new marker
				var EditForm = '<p><div class="marker-edit">'+'<form action="ajax-save.php" method="POST" name="SaveMarker" id="SaveMarker">'+			'<label for="pDesc"><span>Address :</span><textarea id="fld_address" name="pDesc" class="save-desc" placeholder="Enter Address" maxlength="150"></textarea></label>'+'</form>'+'</div></p><button name="save-marker" class="save-marker">Save Marker Details</button>';
				
				

				//Drop a new Marker with our Edit Form
				create_marker(event.latLng, 'New Marker', EditForm, true, true, true, "http://localhost/maps/icons/pin_green.png");
				
				var $address = document.getElementById("fld_address");
				
				geocoder.geocode({'location': event.latLng}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                $address.value = results[1].formatted_address;
                            } else {
                                BootstrapDialog.alert('No results found');
                            }
                        } else {
                            BootstrapDialog.alert('Geocoder failed due to: ' + status);
                        }
                    });
			});
										
	}
	
	//############### Create Marker Function ##############
	function create_marker(MapPos, MapTitle, MapDesc,  InfoOpenDefault, DragAble, Removable, iconPath)
	{	  	  		  
		//new marker
		var marker = new google.maps.Marker({
			position: MapPos,
			map: map,
			draggable:DragAble,
			animation: google.maps.Animation.DROP,
			title:"Address",
			icon: iconPath
		});
		
		//Content structure of info Window for the Markers
		var contentString = $('<div class="marker-info-win">'+'<div class="marker-inner-win"><span class="info-content"><h1 class="marker-heading">'+MapTitle+'</h1>'+MapDesc+'</span></div></div>');	

		
		//Create an infoWindow
		var infowindow = new google.maps.InfoWindow();
		//set the content of infoWindow
		infowindow.setContent(contentString[0]);

		//Find remove button in infoWindow

		var saveBtn = contentString.find('button.save-marker')[0];


		
		if(typeof saveBtn !== 'undefined') //continue only when save button is present
		{
			//add click listner to save marker button
			google.maps.event.addDomListener(saveBtn, "click", function(event) {
				var mReplace = contentString.find('span.info-content'); //html to be replaced after success
				//var mName = contentString.find('input.save-name')[0].value; //name input field value
				var mDesc  = contentString.find('textarea.save-desc')[0].value; //description input field value
				//var mType = contentString.find('select.save-type')[0].value; //type of marker
				
				if( mDesc =='')
				{
					BootstrapDialog.alert("Please enter Name and Description!");
				}else{
					save_marker(marker, mDesc, mReplace); //call save marker function
				}
			});
		}
		
		//add click listner to save marker button		 
		google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker); // click on marker opens info window 
	    });
		  
		if(InfoOpenDefault) //whether info window should be open by default
		{
		  infowindow.open(map,marker);
		}
	}
	
	//############### Remove Marker Function ##############
	function remove_marker(Marker)
	{
		
		/* determine whether marker is draggable 
		new markers are draggable and saved markers are fixed */
		if(Marker.getDraggable()) 
		{
			Marker.setMap(null); //just remove new marker
		}
		else
		{
			//Remove saved marker from DB and map using jQuery Ajax
			var mLatLang = Marker.getPosition().toUrlValue(); //get marker position
			var myData = {del : 'true', latlang : mLatLang}; //post variables
			$.ajax({
				type: "POST",
				url: "map_process.php",
				data: myData,
				success:function(data){
					Marker.setMap(null); 
					BootstrapDialog.alert(data);
				},
				error:function (xhr, ajaxOptions, thrownError){
					BootstrapDialog.alert(thrownError); //throw any errors
				}
			});
		}

	}
	
	//############### Save Marker Function ##############
	function save_marker(Marker, mAddress, replaceWin)
	{
		//Save new marker using jQuery Ajax
		var mLatLang = Marker.getPosition().toUrlValue(); //get marker position
		var myData = {address : mAddress, latlang : mLatLang}; //post variables
		console.log(replaceWin);		
		$.ajax({
		  type: "POST",
		  url: "map_process.php",
		  data: myData,
		  success:function(data){
				replaceWin.html(data); //replace info window with new html
				Marker.setDraggable(true); //set marker to fixed
				Marker.setIcon('http://localhost/maps/icons/pin_blue.png'); //replace icon
            },
            error:function (xhr, ajaxOptions, thrownError){
                BootstrapDialog.alert(thrownError); //throw any errors
            }
		});
	}

	function calculateAndDisplayRoute(directionsService, directionsDisplay, start, end) {
        directionsService.route({
			origin: start,
			destination: end,
			travelMode: 'DRIVING'
        }, function(response, status) {
			if (status === 'OK') {
				directionsDisplay.setDirections(response);
			} else {
				window.alert('Directions request failed due to ' + status);
			}
        });
	}
	
	$("#frmcenter_learner").validate({
		errorPlacement: function(error, element) {
            if (element.attr("name") == "area") {
                error.insertAfter("#areaErrorMessageBox");
               // error.html($("#areaErrorMessageBox"));
            } else {
                error.insertAfter(element);
            }
        },
        rules: {
			ddlContact: { required: true },
			ddlDistrict: { required: true },
			ddlTehsil: { required: true },
			area: { required: true },
            ddlMunicipalType: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Urban"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			},
            ddlMunicipalName: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Urban"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			},
            ddlWardno: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Urban"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			},
            ddlPanchayat: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Rural"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			},
            ddlGramPanchayat: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Rural"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			},
            ddlVillage: { required: {
				depends: function(element) {
                     if ($("#frmcenter_learner input[name='area']:checked").val() == "Rural"){
                       return true;}
                     else{
                       return false;}
                   }
				} 
			}
			
						
        },			
        messages: {	
			ddlContact: { required: '<span style="color:red; font-size: 12px;">Please fill your contact no.</span>' },
			ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Select your District</span>' },
			ddlTehsil: { required: '<span style="color:red; font-size: 12px;">Select your Tehsil</span>' },
			area: { required: '<span style="color:red; font-size: 12px;">Select your Area Type</span>' },
			ddlMunicipalType: { required: '<span style="color:red; font-size: 12px;">Select your Municipality Type.</span>' },
			ddlMunicipalName: { required: '<span style="color:red; font-size: 12px;">Select your Municipality Name:</span>' },
			ddlWardno: { required: '<span style="color:red; font-size: 12px;">Select your Ward No.</span>' },
			ddlPanchayat: { required: '<span style="color:red; font-size: 12px;">Select your Panchayat Samiti/Block.</span>' },
			ddlGramPanchayat: { required: '<span style="color:red; font-size: 12px;">Select your Gram Panchayat.</span>' },
			ddlVillage: { required: '<span style="color:red; font-size: 12px;">Select your Village.</span>' },
        },
	});
	
	//$("#btnSubmit").click(function(e){
		                var itgkcode = $("#itgkcode").val();
                                //alert(itgkcode);
                                //alert("sunil");
                                var input_data = "action=Learnerloc&itgk="+itgkcode;
			
				var googleMapOptions = 
					{ 
						center: mapCenter, // map center
						zoom: 7, //zoom level, 0 = earth view to higher value
						maxZoom: 20,
						minZoom: 2,
						zoomControlOptions: {
						style: google.maps.ZoomControlStyle.SMALL //zoom control size
					},
						scaleControl: true, // enable scale control
						mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
					};
					
				map = new google.maps.Map(document.getElementById("google_map"), googleMapOptions);	
				
				
				var bounds = new google.maps.LatLngBounds();
				
					
				$.ajax({
					url: 'common/cfcenterdashboard.php',
					type: "post",
					data: input_data,
					success: function (data) {
						if(data == "fail"){
							BootstrapDialog.alert("Address not found by map.");
						}
						else{ 
							var learner_latLng = data.replace('--', ',');
                                                        var new_address = learner_latLng.split('--');
							latLng_start = learner_latLng.split(',');
							start_lat = latLng_start[0];
							start_lng = latLng_start[1];
                                                        
                                                        var learner_caddress = new_address[1];
                                                        
							//cname = learner_caddress;
                                                        
                                                        var name = "Your Location";
                                                        var address 	= learner_caddress;
                                                        var point 	= new google.maps.LatLng(parseFloat(start_lat),parseFloat(start_lng));
                                                        create_marker(point, name, address, true, false, false, "images/icons/pin_green.png");
                                                        bounds.extend(point);
							
							
							

						}
					}
				});
			
			
	
                
	//});
	
	
});


</script>


<script src="rkcltheme/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="bootcss/css/learner-location.css">


</html>