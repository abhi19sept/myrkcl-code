<?php

$path =  $_REQUEST['path'];
$filename = 'VisitFeedback.pdf';
header('Content-Transfer-Encoding: binary');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
header('Accept-Ranges: bytes');
header('Content-Length: ' . filesize($path));
header('Content-Encoding: none');
header('Content-Type: application/pdf');
header('Content-Disposition: attachment; filename=' . $filename);
readfile($path);