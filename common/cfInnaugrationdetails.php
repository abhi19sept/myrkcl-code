<?php

/* 
 * Created by Yogendra Soni

 */

include './commonFunction.php';
require 'BAL/clsInnaugrationdetails.php';

$response = array();
$emp = new clsInnaugrationdetails();

//print_r($_POST);
/* for Add function */
if ($_action == "ADD")
{
        if (isset($_POST["txtName"]) && !empty($_POST["txtName"])) 
		{
			if (isset($_POST["txtdesig"]) && !empty($_POST["txtdesig"])) 
			{
				if (isset($_POST["txtarea"]) && !empty($_POST["txtarea"])) 
				{
					if (isset($_POST["txtenddate"]) && !empty($_POST["txtenddate"])) 
					{
						if (isset($_POST["ddlBatch"]) && !empty($_POST["ddlBatch"])) 
						{
							if (isset($_POST["ddlCourse"]) && !empty($_POST["ddlCourse"])) 
							{
                        
											$_Name = $_POST["txtName"];
											$_desig = $_POST["txtdesig"];
											$_area = $_POST["txtarea"];
											$_enddate = $_POST["txtenddate"];
											$_otherofficer= $_POST["txtotherofficer"];
											$_batchcode= $_POST["ddlBatch"];
											$_coursecode= $_POST["ddlCourse"];
											$response = $emp->Add($_Name,$_desig,$_area,$_enddate,$_otherofficer,$_batchcode,$_coursecode);
											echo $response[0];
											
							} 		
							else 
							{
								echo "Please Select Course.";
							}													
                        } 		
						else 
						{
							echo "Please Select Batch.";
						}
					} 
					else
				    {
						echo "Please Select Date.";
					}
				} 
				else 
				{
					echo "Please Enter Area.";
				}
			} 
			else 
			{
				echo "Please Enter Designation.";
			}
		
        } 
		else 
		{
			echo "Please Enter Representative Name.";
		}                                                   
									
}

/* for Designation function */
if ($_action == "FILLDESIGNATION")
{
    $response = $emp->Getdesignation($_actionvalue);
    echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Designation'] . ">" . $_Row['Designation'] . "</option>";
    }
}

/* for show function */

if ($_action == "SHOW") 
{

   
    $response = $emp->GetAll();

    $_DataTable = "";

   echo "<div class='table-responsive' style='margin-top:10px'>";
   
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
 
	 echo "<th style='35%'>Name of Public Representative</th>";
	 echo "<th style='35%'>Designation</th>";
	 echo "<th style='35%'>Centercode</th>";
	
	 echo "<th style='35%'>Area Details Of Public Representative </th>";
	 echo "<th style='35%'>Inauguration  / Closure Date </th>";
	 echo "<th style='35%'>Other Present Officers </th>";
	 echo "<th style='35%'>Batch Name</th>";
	 echo "<th style='35%'>Course Name</th>";
	 echo "<th style='35%'>Upload Photo1</th>";
	 echo "<th style='35%'>Upload Photo2</th>";
	 
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
		 $centercode=$_Row['centercode'];
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
         echo "<td>" .strtoupper( $_Row['Innaug_janpratinidhi_name']) . "</td>";
		  echo "<td>" . strtoupper($_Row['Innaug_designation'] ). "</td>";
		  echo "<td>" . strtoupper($_Row['centercode'] ). "</td>";
		 echo "<td>" . strtoupper($_Row['Innaug_area_details']) . "</td>";
		  echo "<td>" . $_Row['Innaug_end_date'] . "</td>";
		   echo "<td>" . strtoupper($_Row['Innaug_other_officer']) . "</td>";
		   echo "<td>" . strtoupper($_Row['Batch_Name']) . "</td>";
		    echo "<td>" . strtoupper($_Row['Course_Name']) . "</td>";
		  
		  echo "<td>";
		 $l=1;
		 $l++;
		 
		 echo '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#'.$centercode.$_Count.$l.'">Click Here</button>';
		 
		 
		/*   */
		 echo  "</td>";
		 
		 
		  
		 echo "<td>";
		  $j=2;
		  $j++;
		echo '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#'.$centercode.$_Count.$j.'">Click Here</button>';
      
	  
	     /* echo '<a href="upload/Inauguration/'.$_Row['Innaug_image1'] .'" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Click here </a>'; */
		 
		 /* <img class="img-squre img-responsive" width="50"  style="paddadding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/Inauguration/'.$_Row['Innaug_image2'].'"/> */
		 echo  "</td>";
    
    

        echo "</tr>";
		
		
					echo '<div id="'.$centercode.$_Count.$l.'" class="modal fade" tabindex="-1" role="dialog">';
					echo '<div class="modal-dialog">';
					echo '<div class="modal-content">';
					echo '<div class="modal-body">';
					
					echo '<img class="img-squre img-responsive"   src="upload/Inauguration/'.$_Row['Innaug_image1'].'" class="img-responsive"/>';
					
					
					echo '</div>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
					
					
					echo '<div id="'.$centercode.$_Count.$j.'" class="modal fade" tabindex="-1" role="dialog">';
					echo '<div class="modal-dialog">';
					echo '<div class="modal-content">';
					echo '<div class="modal-body">';
					
					echo '<img class="img-squre img-responsive"   src="upload/Inauguration/'.$_Row['Innaug_image2'].'" class="img-responsive"/>';
					
					
					echo '</div>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}


/* For get Details */

if ($_action == "FILLDETAILS") 
{
	$response = $emp->GetDetails($_POST["ddlBatch"],$_POST["ddlCourse"]);
	//print_r($response[0]);
	if($response[0] == 'Success') 
	{
		echo "0";
	}
	
	else 
	{
		echo "1";
	}	
    
}

/* For Get Course */

if ($_action == "FILLCOURSE") 
{
    $response = $emp->Getcourse($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}


/* For Get Batch */

if ($_action == "FILLBATCH") 
{
    $response = $emp->Getbatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

