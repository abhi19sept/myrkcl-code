<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsOrgUpdate.php';

$response = array();
$emp = new clsOrgUpdate();



if ($_action == "DETAILS") {
    $response = $emp->GetDatabyCode();
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                "district" => $_Row['District_Name'],
                "tehsil" => $_Row['Tehsil_Name'],
                "districtcode" => $_Row['Organization_District'],
                "tehsilcode" => $_Row['Organization_Tehsil']);
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "";
    }
}

if ($_action == "FillMunicipalName") {
    $response = $emp->FillMunicipalName($_actionvalue);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Municipality_Raj_Code'] . "'>" . $_Row['Municipality_Name'] . "</option>";
    }
}

if ($_action == "FILLWardno") {
    $response = $emp->FILLWardno($_actionvalue);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Ward_Code'] . "'>" . $_Row['Ward_Name'] . "</option>";
    }
}

if ($_action == "FillMunicipalType") {
    $response = $emp->FillMunicipalType();
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Municipality_Type_Code'] . "'>" . $_Row['Municipality_Type_Name'] . "</option>";
    }
}

if ($_action == "UPDATE") {
		
				$_Areatype = $_POST["areatype"];
				$_WardNo = $_POST["wardno"];	                       
				$_village = $_POST["village"];	                       
				$_GramPanch = $_POST["gram"];	                       
				$_Panchayat = $_POST["panchayat"];	                       
				$_MName = $_POST["municipalname"];	                       
				$_MType = $_POST["municipaltype"];	                       
									   
		
				$response = $emp->UpdateOrg($_Areatype, $_WardNo, $_village, $_GramPanch, $_Panchayat, $_MName, $_MType);
				echo $response[0]; 
		
}