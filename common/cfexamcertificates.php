<?php

/* 
 * Created by yogi

 */

include 'commonFunction.php';
require 'BAL/clsexamcertificates.php';
$response = array();
$emp = new clsexamcertificates();

if ($_action == "FILLRESULTEVENTS") {
    $response = $emp->GetResultEvents();
    echo "<option value='' > Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "FillByDistrict") {
    $response = $emp->GetcenterByDistrict($_POST['values']);
    	
    if (mysqli_num_rows($response[2])) {
	 echo "<option value='All' selected='selected'>All ITGK</option>";
	}
    while ($_Row = mysqli_fetch_array($response[2])) {
		
        echo "<option value=" . $_Row['centercode'] . ">" . $_Row['centercode'] . "</option>";
    }
}

if ($_action == "PDF") {
    $generateBy = $_POST['generateBy'];

    $ddlCenter1 = isset($_POST['ddlCenter']) ? $_POST['ddlCenter'] : [] ;
    $center2 = @implode(",", $ddlCenter1);
    //print_r($center2);
    $CenterCode3 = rtrim($center2, ",");
    $center3 = @explode(",",$CenterCode3);
    $district = $_POST['ddlDistrict'];
    //print_r($ExamCenter3);
    $learnerCode = isset($_POST['learnerCode']) ? $_POST['learnerCode'] : '' ;

    $job = isset($_POST['job']) ? $_POST['job'] : '' ;
    
    $_DataTable = "";
    
    $error = 'Unable to process due to insufficient details provided.';
    switch ($generateBy) {
        case 'district' :
            if (!empty($district)) {
                $error = '';
            }
            break;
        case 'itgk' :
            if (!empty($CenterCode3) && !empty($district)) {
                $error = '';
            }
            break;
        case 'learners' :
            if (!empty($learnerCode)) {
                $error = '';
            }
            break;
        case 'job' :
            if (!empty($job)) {
                $error = '';
            }
            break;
    }

    if (empty($error)) {
        echo "<a class='downloadbutton' target='_blank' href='generatecertificatepdf.php?code=" . $_POST["code"] . "&centercode=" . $CenterCode3 . "&district=" . $district . "&learnerCode=" . $learnerCode . "&job=" . $job . "&Mode=Edit'><img src='images/Download-zip.png' alt='Edit' width='100px' style='margin-top:20px;margin-left:40px;' /></a>";
    } else {
        echo "<p class='error'><span></span> " . $error . " <span></span></p>";
    }
}
?>


