<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsGovLearnerStatusReport.php';

$response = array();
$emp = new clsGovLearnerStatusReport();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll($_POST['eid']);

    $_DataTable = "";
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>Center Code</th>";
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";
    echo "<th style='20%'>Father/Husband Name</th>";
    echo "<th style='20%'>Department</th>";
    echo "<th style='20%'>Designation</th>";
    echo "<th style='5%'>Office Address</th>";
	echo "<th style='5%'>Status</th>";
	echo "<th style='5%'>Lot</th>";
    //echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	   $_Total = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Govemp_ITGK_Code'] . "</td>";
        echo "<td>" . $_Row['learnercode'] . "</td>";		 
			$fname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['fname']))));
        echo "<td>" . $fname . "</td>";
			$faname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['faname']))));
        echo "<td>" . $faname . "</td>";
        echo "<td>" . $_Row['gdname'] . "</td>";
        echo "<td>" . $_Row['designation'] . "</td>";
		echo "<td>" . $_Row['officeaddress'] . "</td>";			
        echo "<td>" . $_Row['cstatus'] . "</td>";
       echo "<td>" . $_Row['lotname'] . "</td>";
        echo "</tr>";
		
        $_Count++;
    }
    echo "</tbody>";
	 
    echo "</table>";
	 echo "</div>";
}


?>