<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCorrectionManualApproved.php';

$response = array();
$emp = new clsCorrectionManualApproved();

	if ($_action == "ShowDetails")
	{
        $response = $emp->GetAll($_POST['code'],$_POST['lcode'],$_POST['status']);
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>S No.</th>";
		echo "<th>Learner Code</th>";
		echo "<th>Learner Name</th>";
		echo "<th>Father Name</th>";
		echo "<th>D.O.B</th>";
		echo "<th>Exam Event</th>";
		echo "<th>Payment Status</th>";
		echo "<th>Marks</th>";				
		echo "<th>Action</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		
			$_Count = 1;	
			while ($_Row = mysqli_fetch_array($response[2])) {
			echo "<tr>";
			echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['lcode'] . "</td>";
			echo "<td>" . $_Row['cfname'] . "</td>";
			echo "<td>" . $_Row['cfaname'] . "</td>";
			echo "<td>" . $_Row['dob'] . "</td>"; 
			echo "<td>" . $_Row['exameventname'] . "</td>"; 
			  if($_Row['Correction_Payment_Status'] == '0'){
				  $status = 'Pending';
				  echo "<td>" . $status . "</td>";
			  }
			  else {
				  $status = 'Confirmed';
				  echo "<td>" . $status . "</td>";
			  }
			 
			echo "<td>" . $_Row['totalmarks'] . "</td>";					
			
			if($_Row['dispatchstatus'] == '3')
				{
					echo "<td> <a href='frmcorrectionmanualprocess.php?code=" . $_Row['lcode'] . "&cid=" . $_Row['cid'] . "&Mode=Edit'>"
						. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='Edit'/></a>"						
						. "</td>";
				}
			else if($_Row['dispatchstatus'] == '4')
				{
					echo "<td> <a href='frmcorrectionmanualprocess.php?code=" . $_Row['lcode'] . "&cid=" . $_Row['cid'] . "&Mode=Reject'>"
						. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='Edit'/></a>"						
						. "</td>";
				}
			else if($_Row['dispatchstatus'] == '2')
				{
					echo "<td>"
					. "<input type='button' name='Delivered' id='Delivered' class='btn btn-primary' value='Delivered to DLC'/>"
					. "</td>";
				}
		    else if($_Row['dispatchstatus'] == '0') 
				{
					echo "<td>"
					. "<input type='button' name='Processed' id='Processed' class='btn btn-primary' value='Processed by RKCL'/>"
					. "</td>";
				}
			else
				{
					echo "<td>"
					. "<input type='button' name='Sent' id='Sent' class='btn btn-primary' value='Sent to VMOU for printing'/>"
					. "</td>";
				}
		
			echo "</tr>";
			$_Count++;
		}
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}


if ($_action == "FILLStatus") {
    $response = $emp->FILLStatus();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}

if ($_action == "FILLAPPROVEDSTATUS") {
    $response = $emp->FILLAPPROVEDSTATUS();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}

if ($_action == "GETALLLOT") {
    $response = $emp->GETALLLOT();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['lotid'] . ">" . $_Row['lotname'] . "</option>";
    }
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("LearnerCode" => $_Row['lcode'],
            "lname" => $_Row['cfname'],
            "fname" => $_Row['cfaname'],
            "applicationfor" => $_Row['applicationfor'],
            "exameventname" => $_Row['exameventname'],            
            "txnid" => $_Row['Correction_TranRefNo'],            
			"marks" => $_Row['totalmarks'],
			"photo" => $_Row['photo'],
			"certificate" => $_Row['attach2'],
			"marksheet" => $_Row['attach1'],
			"provisional" => $_Row['attach4']);        
        $_i = $_i + 1;
		
    }
	
    echo json_encode($_DataTable);
}

if ($_action == "GetOldValues") {
    $response = $emp->GetOldValues($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("AdmissionCode" => $_Row['Admission_Code'],
            "lname" => $_Row['Admission_Name'],
            "fname" => $_Row['Admission_Fname']);        
        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}

if ($_action == "UPDATE") {	
	$_Cid = $_POST["cid"];
	$_ProcessStatus = $_POST["status"];
	$_LOT = $_POST["lot"];                        
	$_Remark = $_POST["remark"];
	$_EventName = $_POST["eventname"];
	$_Marks = $_POST['marks'];
	$_Txnid = $_POST['txnid'];
	if($_EventName == "") {
		$response = $emp->UpdateToProcess($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_Marks, $_Txnid);
		echo $response[0];	
	}
	else {	
		$response = $emp->UpdateToProcessWithEvent($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_EventName, $_Marks, $_Txnid);
		echo $response[0];
	}
}


if ($_action == "UpdateRejectedLearner") {	
	$_Cid = $_POST["cid"];
	$_ProcessStatus = $_POST["status"];
	$_LOT = $_POST["lot"];                        
	$_Remark = $_POST["remark"];
	$_EventName = $_POST["eventname"];
	$_Marks = $_POST['marks'];
	
	$_LName = $_POST["lname"];
	$_Fname = $_POST['fname'];
	
	if($_EventName == "") {
		$response = $emp->UpdateToProcessRejected($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_Marks, $_LName, $_Fname);
		echo $response[0];	
	}
	else {	
		$response = $emp->UpdateToProcessRejectedWithEvent($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_EventName, $_Marks, $_LName, $_Fname);
		echo $response[0];
	}
}