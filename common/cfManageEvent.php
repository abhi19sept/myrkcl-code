<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsManageEvent.php';

$response = array();
$emp = new clsManageEvent();

if ($_action == "ADD") {
    // print_r($_POST);
    if (isset($_POST["category"])) {
        $_EventCat = $_POST["category"];
        $_EventName = $_POST["eventname"];
        if (isset($_POST["paymode"]) && !empty($_POST["paymode"])) {
            $_PayMode = $_POST["paymode"];
        } else {
            $_PayMode = '0';
        }
        if (isset($_POST["course"]) && !empty($_POST["course"])) {
            $_Course = $_POST["course"];
        } else {
            $_Course = '0';
        }
        if (isset($_POST["courseeoi"]) && !empty($_POST["courseeoi"])) {
            $_CourseEOI = $_POST["courseeoi"];
        } else {
            $_CourseEOI = '0';
        }
        if (isset($_POST["batch"]) && !empty($_POST["batch"])) {
            $_BatchName = $_POST["batch"];
        } else {
            $_BatchName = '0';
        }
        if (isset($_POST["reexemevent"]) && !empty($_POST["reexemevent"])) {
            $_ReexamEvent = $_POST["reexemevent"];
        } else {
            $_ReexamEvent = '0';
        }
        if (isset($_POST["correctionevent"]) && !empty($_POST["correctionevent"])) {
            $_CorrectionEvent = $_POST["correctionevent"];
        } else {
            $_CorrectionEvent = '0';
        }
        if (isset($_POST["learnerattendanceevent"]) && !empty($_POST["learnerattendanceevent"])) {
            $_Event_LearnerAttendance = $_POST["learnerattendanceevent"];
        } else {
            $_Event_LearnerAttendance = '0';
        }
        if (isset($_POST["coreectionbfrexamevent"]) && !empty($_POST["coreectionbfrexamevent"])) {
            $_Event_Coreectionbfrexamevent = $_POST["coreectionbfrexamevent"];
        } else {
            $_Event_Coreectionbfrexamevent = '0';
        }
        if (isset($_POST["rscfacertificatepayment"]) && !empty($_POST["rscfacertificatepayment"])) {
            $_Event_Rscfacertificatepayment = $_POST["rscfacertificatepayment"];
        } else {
            $_Event_Rscfacertificatepayment = '0';
        }
        $_Sdate = $_POST["startdate"];
        $_Edate = $_POST["enddate"];
        $response = $emp->Add($_EventCat, $_EventName, $_PayMode, $_Course, $_CourseEOI, $_BatchName, $_ReexamEvent, $_CorrectionEvent, $_Event_LearnerAttendance,$_Event_Coreectionbfrexamevent,$_Event_Rscfacertificatepayment, $_Sdate, $_Edate);
        echo $response[0];
    }
}
if ($_action == "FILLCategory") {
    $response = $emp->FILLCategory();
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Category_ID'] . ">" . $_Row['Event_Category_Name'] . "</option>";
    }
}

if ($_action == "FILLEvent") {
    $response = $emp->FILLEvent($_POST['category']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Type_ID'] . ">" . $_Row['Event_Type_Name'] . "</option>";
    }
}

if ($_action == "FILLEventDDmode") {
    $response = $emp->FILLEventDDmode();
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Type_ID'] . ">" . $_Row['Event_Type_Name'] . "</option>";
    }
}

if ($_action == "FILLEventIDddmode") {
    $response = $emp->FILLEventIDddmode($_POST['course'], $_POST['batch'], $_POST['event']);

    $_Row = mysqli_fetch_array($response[2]);
    echo $_Row['Event_ID'];
}

if ($_action == "ADDddmodeitgk") {
    //print_r($_POST["eventid"]);
    if (isset($_POST["eventid"])) {
        $_eventid = $_POST["eventid"];



        if (isset($_SESSION['DataArray'])) {

            for ($i = 1; $i <= count($_SESSION['DataArray']); $i++) {
                $response = $emp->Addlistddmodeitgk($_eventid, $_SESSION['DataArray'][$i][0]);
            }
        }
        echo $response[0];
    }
}


if ($_action == "FILLPaymentMode") {
    $response = $emp->FILLPaymentMode();
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['payment_id'] . ">" . $_Row['payment_mode'] . "</option>";
    }
}

if ($_action == "SHOWDATA") {

    $_EventCat = $_POST["category"];
    $_EventName = $_POST["eventname"];
    $_PayMode = $_POST["paymode"];
    $_Course = $_POST["course"];
//    $_CourseEoi = ($_POST["eoi"] == '' ? '0' : $_POST["eoi"]);
    $_CourseEoi = $_POST["eoi"];
    $_BatchName = $_POST["batch"];
    $_ReexamEvent = ($_POST["reexemevent"] == '' ? '0' : $_POST["reexemevent"]);
    $_CorrectionEvent = ($_POST["correctionevent"] == '' ? '0' : $_POST["correctionevent"]);
    $_NameAddressEvent = $_POST["nameaddressevent"];
    $_NcrPaymentEvent = $_POST["ncrpaymentevent"];
    $_OwnerChangeEvent = $_POST["ownerchangeevent"];
    $_RenewalPenaltyEvent = $_POST["renewalpenaltyevent"];
    $_NameAadharUpdEvent = $_POST["nameaadharupdevent"];

    $_RscfaCertificateEvent = $_POST["rscfacertificatepayment"];

    $response = $emp->GetAll($_EventCat, $_EventName, $_PayMode, $_Course, $_CourseEoi, $_BatchName, $_ReexamEvent, $_CorrectionEvent, $_NameAddressEvent, $_NcrPaymentEvent, $_OwnerChangeEvent, $_RenewalPenaltyEvent, $_NameAadharUpdEvent, $_RscfaCertificateEvent);
    // print_r($response[2]);    
//$_DataTable = "";
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("StartDate" => $_Row['Event_Startdate'],
            "EndDate" => $_Row['Event_Enddate'],
            "EventID" => $_Row['Event_ID']);

        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);


//    echo "<div class='table-responsive'>";
//    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
//    echo "<thead>";
//    echo "<tr>";
//    echo "<th style='10%'>S No.</th>";
//    echo "<th style='10%'>Event Catagory</th>";
//    echo "<th style='8%'>Event Name</th>";
//    echo "<th style='12%'>Start Date</th>";
//    echo "<th style='10%'>End Date</th>";
//    echo "<th style='10%'>Edit</th>";
//
//    echo "</tr>";
//    echo "</thead>";
//    echo "<tbody>";
//    $_Count = 1;
//    $co = mysqli_num_rows($response[2]);
//    if ($co) {
//        while ($_Row = mysqli_fetch_array($response[2])) {
//            echo "<tr>";
//            echo "<td>" . $_Count . "</td>";
//            echo "<td>" . $_Row['Event_Category'] . "</td>";
//            echo "<td>" . $_Row['Event_Name'] . "</td>";
//            echo "<td>" . $_Row['Event_Startdate'] . "</td>";
//            echo "<td>" . $_Row['Event_Enddate'] . "</td>";
//            echo "<td><a href='frmsetdatetime.php?code=" . $_Row['Event_ID'] . "&Mode=Edit' target='_blank'>"
//                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a></td>";
//
//            echo "</tr>";
//            $_Count++;
//        }
//        echo "</tbody>";
//        echo "</div>";
//    } else {
//        echo "No Record Found";
//    }
}


if ($_action == "SHOWEVENT") {

    $_EventCat = $_POST["category"];
    $_EventName = $_POST["eventname"];
//    $_PayMode = $_POST["paymode"];
//    $_Course = $_POST["course"];
//    $_BatchName = $_POST["batch"];
//    $_ReexamEvent = $_POST["reexemevent"];
//    $_CorrectionEvent = $_POST["correctionevent"];

    $response = $emp->GetEvents($_EventCat, $_EventName);
    //$_DataTable = "";
    $_DataTable = array();
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Event Catagory</th>";
    echo "<th style='8%'>Event Name</th>";
    echo "<th style='8%'>Payment</th>";
    echo "<th style='8%'>Course</th>";
    echo "<th style='8%'>Batch</th>";
    echo "<th style='8%'>ReexamEvent</th>";
    echo "<th style='8%'>CorrectionEvent</th>";
    echo "<th style='12%'>Start Date</th>";
    echo "<th style='10%'>End Date</th>";
    // echo "<th style='10%'>Edit</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['EventCat'] . "</td>";
            echo "<td>" . $_Row['EventName'] . "</td>";
            echo "<td>" . $_Row['Event_Payment'] . "</td>";
            echo "<td>" . $_Row['Event_Course'] . "</td>";
            echo "<td>" . $_Row['Event_Batch'] . "</td>";
            echo "<td>" . $_Row['Event_ReexamEvent'] . "</td>";
            echo "<td>" . $_Row['Event_CorrectionEvent'] . "</td>";
            echo "<td>" . $_Row['Event_Startdate'] . "</td>";
            echo "<td>" . $_Row['Event_Enddate'] . "</td>";
//            echo "<td><a href='frmsetdatetime.php?code=" . $_Row['Event_ID'] . "&Mode=Edit' target='_blank'>"
//                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a></td>";

            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }


//    echo "<div class='table-responsive'>";
//    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
//    echo "<thead>";
//    echo "<tr>";
//    echo "<th style='10%'>S No.</th>";
//    echo "<th style='10%'>Event Catagory</th>";
//    echo "<th style='8%'>Event Name</th>";
//    echo "<th style='12%'>Start Date</th>";
//    echo "<th style='10%'>End Date</th>";
//    echo "<th style='10%'>Edit</th>";
//
//    echo "</tr>";
//    echo "</thead>";
//    echo "<tbody>";
//    $_Count = 1;
//    $co = mysqli_num_rows($response[2]);
//    if ($co) {
//        while ($_Row = mysqli_fetch_array($response[2])) {
//            echo "<tr>";
//            echo "<td>" . $_Count . "</td>";
//            echo "<td>" . $_Row['Event_Category'] . "</td>";
//            echo "<td>" . $_Row['Event_Name'] . "</td>";
//            echo "<td>" . $_Row['Event_Startdate'] . "</td>";
//            echo "<td>" . $_Row['Event_Enddate'] . "</td>";
//            echo "<td><a href='frmsetdatetime.php?code=" . $_Row['Event_ID'] . "&Mode=Edit' target='_blank'>"
//                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a></td>";
//
//            echo "</tr>";
//            $_Count++;
//        }
//        echo "</tbody>";
//        echo "</div>";
//    } else {
//        echo "No Record Found";
//    }
}


if ($_action == "UPDATE") {
    if (isset($_POST["eventid"])) {
        $_txtEventID = $_POST["eventid"];
        $_txtEndDate = $_POST["enddate"];

        $response = $emp->Update($_txtEventID, $_txtEndDate);
        echo $response[0];
    }
}


if ($_action == "DELETE") {
    //print_r($_POST);
    $value = $_POST['values'];
    $batch = $_POST['batchcode'];

    $response = $emp->DeleteRecord($value, $batch);

    echo $response[0];
}


if ($_action == "SHOWALLEVENT") {


    $response = $emp->GetAllEvents();
    //$_DataTable = "";
    $_DataTable = array();
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Event Catagory</th>";
    echo "<th style='8%'>Event Name</th>";
    echo "<th style='8%'>Course</th>";
    echo "<th style='8%'>Batch</th>";
    echo "<th style='8%'>ExamEvent Name</th>";
    echo "<th style='8%'>Eoi Name</th>";
    echo "<th style='8%'>AttendanceEvent Status</th>";
    echo "<th style='12%'>Start Date</th>";
    echo "<th style='10%'>End Date</th>";
    echo "<th style='10%'>Event Created Date</th>";
    // echo "<th style='10%'>Edit</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['EventCat'] . "</td>";
            echo "<td>" . $_Row['EventName'] . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            echo "<td>" . $_Row['ExamEvent'] . "</td>";
            echo "<td>" . $_Row['EOI_Name'] . "</td>";
            echo "<td>" . $_Row['Event_LearnerAttendance'] . "</td>";
            echo "<td>" . $_Row['Event_Startdate'] . "</td>";
            echo "<td>" . $_Row['Event_Enddate'] . "</td>";
            echo "<td>" . $_Row['Timestamp'] . "</td>";
//            echo "<td><a href='frmsetdatetime.php?code=" . $_Row['Event_ID'] . "&Mode=Edit' target='_blank'>"
//                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a></td>";

            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }


//    echo "<div class='table-responsive'>";
//    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
//    echo "<thead>";
//    echo "<tr>";
//    echo "<th style='10%'>S No.</th>";
//    echo "<th style='10%'>Event Catagory</th>";
//    echo "<th style='8%'>Event Name</th>";
//    echo "<th style='12%'>Start Date</th>";
//    echo "<th style='10%'>End Date</th>";
//    echo "<th style='10%'>Edit</th>";
//
//    echo "</tr>";
//    echo "</thead>";
//    echo "<tbody>";
//    $_Count = 1;
//    $co = mysqli_num_rows($response[2]);
//    if ($co) {
//        while ($_Row = mysqli_fetch_array($response[2])) {
//            echo "<tr>";
//            echo "<td>" . $_Count . "</td>";
//            echo "<td>" . $_Row['Event_Category'] . "</td>";
//            echo "<td>" . $_Row['Event_Name'] . "</td>";
//            echo "<td>" . $_Row['Event_Startdate'] . "</td>";
//            echo "<td>" . $_Row['Event_Enddate'] . "</td>";
//            echo "<td><a href='frmsetdatetime.php?code=" . $_Row['Event_ID'] . "&Mode=Edit' target='_blank'>"
//                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a></td>";
//
//            echo "</tr>";
//            $_Count++;
//        }
//        echo "</tbody>";
//        echo "</div>";
//    } else {
//        echo "No Record Found";
//    }
}

    