<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsPaymentSummary.php';

$response = array();
$emp = new clsPaymentSummary();

if ($_action == "SHOW") {
    if (isset($_POST["startdate"])) {
        if (isset($_POST["enddate"])) {
            $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
            $response = $emp->Show($sdate, $edate);
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>Date From : </th>";
            echo "<th>" . $sdate . "</th>";

            echo "<th>Date To : </th>";
            echo "<th>" . $edate . "</th>";

            echo "</tr>";
            echo "<tr>";
            echo "<th> S No.</th>";
            echo "<th> Head Name </th>";
            echo "<th> Total Online Amount </th>";
            echo "<th> Total DD Amount </th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;
            $row = mysqli_fetch_array($response[2]);

            //$Tdate = date("d-m-Y", strtotime($row['Admission_Transaction_timestamp']));
            echo "<tr class='odd gradeX'>";
            echo "<td>1.</td>";

            echo "<td>Admission Fee Payment</td>";
            if ($row['Total_Online_Learner_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Learner_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=Admission_Payment&mode=ShowOnline' target='_blank'>"
                . "" . $row['Total_Online_Learner_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_Learner_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Learner_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=Admission_Payment&mode=ShowDD' target='_blank'>"
                . "" . $row['Total_DD_Learner_Payment'] . "</a></td>";
            }

            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>2.</td>";

            echo "<td>EOI Fee Payment</td>";
            if ($row['Total_Online_EOI_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                ///echo "<td>" . strtoupper($row['Total_EOI_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=EOI_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_Online_EOI_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_EOI_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                ///echo "<td>" . strtoupper($row['Total_EOI_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=EOI_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_DD_EOI_Payment'] . "</a></td>";
            }
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>3.</td>";

            echo "<td>Correction Fee Payment</td>";
            if ($row['Total_Online_Correction_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                echo "<td>" . strtoupper($row['Total_Online_Correction_Payment']) . "</td>";
//                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=Correction_Payment&mode=Show' target='_blank'>"
//                . "" . $row['Total_Correction_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_Correction_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                echo "<td>" . strtoupper($row['Total_DD_Correction_Payment']) . "</td>";
//                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=Correction_Payment&mode=Show' target='_blank'>"
//                . "" . $row['Total_Correction_Payment'] . "</a></td>";
            }
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>4.</td>";

            echo "<td>Re-Exam Fee Payment</td>";
            if ($row['Total_Online_Reexam_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=Reexam_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_Online_Reexam_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_Reexam_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=Reexam_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_DD_Reexam_Payment'] . "</a></td>";
            }
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>5.</td>";

            echo "<td>NCR Fee Payment</td>";
            if ($row['Total_Online_Ncr_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=NCR_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_Online_Ncr_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_Ncr_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=NCR_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_DD_Ncr_Payment'] . "</a></td>";
            }
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>6.</td>";

            echo "<td>Name Address Change Fee Payment</td>";
            if ($row['Total_Online_address_name_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=address_name_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_Online_address_name_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_address_name_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td>" . $row['Total_DD_address_name_Payment'] . " </td>";
            }
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>7.</td>";
            echo "<td>Renewal Penalty Fee Payment</td>";

            if ($row['Total_Online_RP_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=renewal_penalty_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_Online_RP_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_RP_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td>" . $row['Total_DD_RP_Payment'] . " </td>";
            }
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>8.</td>";
            echo "<td>Ownership Change Fee Payment</td>";
            if ($row['Total_Online_OC_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=owner_change_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_Online_OC_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_OC_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td>" . $row['Total_DD_OC_Payment'] . " </td>";
            }
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>9.</td>";
            echo "<td>Admission Correction Before Exam Fee Payment</td>";
            if ($row['Total_Online_Aadhar_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=adm_correction_aadhar_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_Online_Aadhar_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_Aadhar_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td>" . $row['Total_DD_Aadhar_Payment'] . " </td>";
            }
            echo "</tr>";
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>10.</td>";
            echo "<td>Red Hat Experience Center Payment</td>";
            if ($row['Total_Online_RHExpCenter_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=RHExpCenter_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_Online_RHExpCenter_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_RHExpCenter_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td>" . $row['Total_DD_RHExpCenter_Payment'] . " </td>";
            }
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>11.</td>";
            echo "<td>RS-CFA Certificate Payment</td>";
            if ($row['Total_Online_RSCFACert_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=RSCFACert_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_Online_RSCFACert_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_RSCFACert_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td>" . $row['Total_DD_RSCFACert_Payment'] . " </td>";
            }
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>12.</td>";
            echo "<td>RS-CFA Women Re-Exam Payment</td>";
            if ($row['Total_Online_RSCFAreexam_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td><a href='frmPaySumCategory.php?sdate=" . $_POST["startdate"] . "&edate=" . $_POST["enddate"] . "&headname=RSCFAReexam_Payment&mode=Show' target='_blank'>"
                . "" . $row['Total_Online_RSCFAreexam_Payment'] . "</a></td>";
            }
            if ($row['Total_DD_RSCFAreexam_Payment'] == "") {
                echo "<td>0</td>";
            } else {
                //echo "<td>" . strtoupper($row['Total_Reexam_Payment']) . "</td>";
                echo "<td>" . $row['Total_DD_RSCFAreexam_Payment'] . " </td>";
            }
            echo "</tr>";
            echo "<tr class='odd gradeX'>";
            echo "<td>13.</td>";

            echo "<td><b>Total Amount</b></td>";

            $tonlineamount = $row['Total_Online_Learner_Payment'] + $row['Total_Online_EOI_Payment'] +
                    $row['Total_Online_Correction_Payment'] + $row['Total_Online_Reexam_Payment'] +
                    $row['Total_Online_Ncr_Payment'] + $row['Total_Online_address_name_Payment'] +
                    $row['Total_Online_RP_Payment'] + $row['Total_Online_OC_Payment'] + $row['Total_Online_Aadhar_Payment'] + $row['Total_Online_RHExpCenter_Payment'] + $row['Total_Online_RSCFACert_Payment'] + $row['Total_Online_RSCFAreexam_Payment'];
            $tddamount = $row['Total_DD_Learner_Payment'] + $row['Total_DD_EOI_Payment'] +
                    $row['Total_DD_Correction_Payment'] + $row['Total_DD_Reexam_Payment'] +
                    $row['Total_DD_Ncr_Payment'] + $row['Total_DD_RP_Payment'] + $row['Total_DD_OC_Payment'] + $row['Total_DD_Aadhar_Payment'] + $row['Total_DD_RHExpCenter_Payment']  + $row['Total_DD_RSCFACert_Payment'] + $row['Total_DD_RSCFAreexam_Payment'];
            echo "<td><b>" . $tonlineamount . "</b></td>";
            echo "<td><b>" . $tddamount . "</b></td>";

            echo "</tr>";
            //$_Count++;

            echo "</tbody>";
            echo "</table>";
            echo "</div>";
            //echo $html;
        }
    }
}

if ($_action == "SHOWcategory") {
    if (isset($_POST["startdate"])) {
        if (isset($_POST["enddate"])) {
            $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
            $headname = $_POST["headname"];
            $mode = $_POST["Mode"];
            $response = $emp->ShowCategory($sdate, $edate, $headname, $mode);
            // print_r($response);
            //$html = "";
            //$_centerdetail=  mysqli_fetch_array($response[2]);
            //$response = $emp->GetCenterWiseReport($_POST['CenterCode']);
            if ($headname == "Admission_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
                echo "<th></th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th>Date To : </th>";
                echo "<th>" . $edate . "</th>";
                echo "<th></th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>Admission Fee Payment</th>";
                echo "<th></th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['coursename'] . "</td>";
                        echo "<td>" . $_Row['batchname'] . "</td>";
                        echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } elseif ($headname == "EOI_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th>Date To : </th>";
                echo "<th>" . $edate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>EOI Fee Payment (EOI Name)</th>";
//                echo "<th></th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['EOI_Name'] . "</td>";
                        echo "<td>" . $_Row['totaleoiamount'] . "</td>";
                        //echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } elseif ($headname == "Reexam_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th>Date To : </th>";
                echo "<th>" . $edate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>Re-Exam Fee Payment (Exam Event Name)</th>";
//                echo "<th></th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['Reexam_Transaction_ProdInfo'] . "</td>";
                        echo "<td>" . $_Row['totalreexamamount'] . "</td>";
                        //echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } elseif ($headname == "Correction_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
                echo "<th></th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th>Date To : </th>";
                echo "<th>" . $edate . "</th>";
                echo "<th></th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>Admission Fee Payment</th>";
                echo "<th></th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        echo "<td>" . $_Count . "</td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        echo "<td></td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } elseif ($headname == "NCR_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th>Date To : </th>";
                echo "<th>" . $edate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>NCR Fee Payment</th>";
//                echo "<th></th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['Ncr_Transaction_CenterCode'] . "</td>";
                        echo "<td>" . $_Row['totalncramount'] . "</td>";
                        //echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } elseif ($headname == "address_name_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th>Date To : </th>";
                echo "<th>" . $edate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>Name-Address Change Fee Payment </th>";
//                echo "<th></th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['fld_ITGK_Code'] . "</td>";
                        echo "<td>" . $_Row['totaladdress_name_Payment'] . "</td>";
                        //echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } elseif ($headname == "renewal_penalty_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th>Date To : </th>";
                echo "<th>" . $edate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>Renewal Penalty Fee Payment </th>";
//                echo "<th></th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['RP_Transaction_CenterCode'] . "</td>";
                        echo "<td>" . $_Row['totalamount'] . "</td>";
                        //echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } elseif ($headname == "owner_change_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th>Date To : </th>";
                echo "<th>" . $edate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>Ownership Change Fee Payment </th>";
//                echo "<th></th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['Ownership_Transaction_CenterCode'] . "</td>";
                        echo "<td>" . $_Row['totalamount'] . "</td>";
                        //echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } elseif ($headname == "adm_correction_aadhar_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th>Date To : </th>";
                echo "<th>" . $edate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>Admission Correction Before Exam Fee Payment </th>";
//                echo "<th></th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['Aadhar_Transaction_CenterCode'] . "</td>";
                        echo "<td>" . $_Row['totalamount'] . "</td>";
                        //echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } elseif ($headname == "RHExpCenter_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th colspan='5'>Red Hat Experience Center Payment</th>";
                 echo "</tr>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
              
                echo "<th>Date To : </th>";
                echo "<th >" . $edate . "</th>";
                echo "<th></th>";
                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>Center Code</th>";
                echo "<th>Payment For</th>";
                echo "<th>User Code</th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['exp_center_code'] . "</td>";
                        if ($_Row['exp_center_sub_type'] == '1') {
                            echo "<td>For Own Center</td>";
                            echo "<td>" . $_Row['exp_center_code'] . "</td>";
                        } else {
                            echo "<td>For Learner</td>";
                            echo "<td>" . $_Row['exp_center_lcode'] . " " . strtoupper($_Row['exp_center_lname']) . "</td>";
                        }
                        echo "<td>" . $_Row['ExpCenter_Transaction_Amount'] . "</td>";

                        //echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            }
            elseif ($headname == "RSCFACert_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th colspan='5'>RS-CFA Certificate Payment</th>";
                 echo "</tr>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
              
                echo "<th>Date To : </th>";
                echo "<th >" . $edate . "</th>";

                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>Center Code</th>";
                echo "<th>Tran ID</th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['Rscfa_Certificate_Transaction_CenterCode'] . "</td>";                        
                        echo "<td>" . $_Row['Rscfa_Certificate_Transaction_Txtid'] . "</td>";
                        echo "<td>" . $_Row['Rscfa_Certificate_Transaction_Amount'] . "</td>";

                        //echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            }
            elseif ($headname == "RSCFAReexam_Payment") {
                echo "<div class='table-responsive'>";
                echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th colspan='5'>  RS-CFA Women Re-Exam Payment</th>";
                 echo "</tr>";
                echo "<tr>";
                echo "<th>Date From : </th>";
                echo "<th>" . $sdate . "</th>";
              
                echo "<th>Date To : </th>";
                echo "<th >" . $edate . "</th>";

                echo "</tr>";
                echo "<tr>";
                echo "<th> S No.</th>";
                echo "<th>Center Code</th>";
                echo "<th>Tran ID</th>";
                echo "<th> Total Amount </th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                $_Count = 1;
                if ($response) {
                    while ($_Row = mysqli_fetch_array($response[2])) {
                        echo "<tr>";
                        
                        echo "<td>" . $_Count . "</td>";
                        echo "<td>" . $_Row['Rscfa_ReExam_Transaction_CenterCode'] . "</td>";

                        echo "<td>" . $_Row['Rscfa_ReExam_Transaction_Txtid'] . "</td>";

                        echo "<td>" . $_Row['Rscfa_ReExam_Transaction_Amount'] . "</td>";

                        //echo "<td>" . $_Row['totalamount'] . "</td>";
                        ///echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                        echo "</tr>";
                        $_Count++;
                    }
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            }

            //echo $html;
        }
    }
}