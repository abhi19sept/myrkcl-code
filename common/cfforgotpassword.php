<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsforgotpassword.php';
require 'DAL/sendsms.php';

    $response = array();
    $emp = new clsforgotpassword();
    
    if ($_action == "RESENDOTP") {
        if (isset($_POST["Center"]) && !empty($_POST["Center"])) {
		$_Center = $_POST["Center"];
		$response = $emp->ResendOTP($_Center);
                //echo "<pre>"; print_r($response);die;
            if ($response[0] != Message::NoRecordFound) {
                $_row = mysqli_fetch_array($response[2]);
                $_Mobile =  "XXXXXX". substr($_row['AO_Mobile'], 6);
                $_OTP = $_row["AO_OTP"];
                $_OTP_StartOn = $_row["OTP_StartOn"];
                $_OTP_EndOn = $_row["OTP_EndOn"];
                $_SMS = "Dear MYRKCL User ". $_Center .", Your verification OTP is " . $_OTP ." for forgot Password / Reset Password. Valid till ".$_OTP_EndOn."."; 
                SendSMS($_row['AO_Mobile'], $_SMS);
                $result = [
                    //'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
                    'frm' => $emp->getVeriFyOTPForm($_row["AO_Code"], $_Center, $_Mobile),
                ];

                echo json_encode($result);
            } else {
                echo '0';
            }
        }
    }
    
    if ($_action == "FORGOT") {
        if (isset($_POST["Center"]) && !empty($_POST["Center"])) {
		$_Center = trim($_POST["Center"]);
		$response = $emp->Forgot($_Center);
                //echo "<pre>"; print_r($response);die;
            if ($response[0] != Message::NoRecordFound) {
                $_row = mysqli_fetch_array($response[2]);
                $_Mobile =  "XXXXXX". substr($_row['AO_Mobile'], 6);
                $_OTP = $_row["AO_OTP"];
                $_OTP_StartOn = $_row["OTP_StartOn"];
                $_OTP_EndOn = $_row["OTP_EndOn"];
                $_SMS = "Dear MYRKCL User ". $_Center .", Your verification OTP is " . $_OTP ." for forgot Password / Reset Password. Valid till ".$_OTP_EndOn."."; 
                SendSMS($_row['AO_Mobile'], $_SMS);
                $result = [
                    //'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
                    'frm' => $emp->getVeriFyOTPForm($_row["AO_Code"], $_Center, $_Mobile),
                ];

                echo json_encode($result);
            } else {
                echo '0';
            }
        }
    }
    
    if ($_action == "VerifyOTP" && isset($_POST["oid"]) && !empty($_POST["oid"])) {
        
        if($_POST["otp"]=='')
        {
          echo 'OTPBLANCK';  
        }
        else{
            $response = $emp->VerifyOTPNEW($_POST["oid"], $_POST["otp"]);
            //echo "<pre>"; print_r($response);die;
            if($response[0]=='EXPIRE')
            {
               echo 'EXPIRE';

            }
            else if($response[0]=='NORECORD')
            {
               echo 'NORECORD';

            }

            else{
            if($response[0]=='Success'){
                    if (mysqli_num_rows($response[2])) 
                        {
                        $responseQuestionITGK = $emp->GetSecurityQuestionITGK($_POST["ccode"]);
                        //echo "<pre>"; print_r($responseQuestionITGK);
                        if($responseQuestionITGK[0]=='Success')
                        {
                         $sqexits=1;   
                        }else{
                         $sqexits=0;  
                        }
                         $result = [
                            'msg' => 'Please register your new passowrd.',
                            'oid' => $_POST["oid"],
                            'ccode' => $_POST["ccode"],
                            'sqexits' => $sqexits,
                        ];

                        echo json_encode($result);
                        } 
                }                
                else {
                    echo '0';
                }
        }
        }
    }

    if ($_action == "setNewPassword" && isset($_POST["pass"]) && !empty($_POST["pass"]) && isset($_POST["CnPass"]) && !empty($_POST["CnPass"]) && !empty($_POST['oid']) && !empty($_POST['ccode'])) 
        {
        //echo "<pre>"; print_r($_POST);die;
        
        if ($_POST["pass"] == $_POST["CnPass"]) {//echo "<pre>"; print_r($_POST);die;
            $responceNew = $emp->updateUserPasswordNew($_POST["pass"], $_POST['ccode'], $_POST['oid']);
            //echo "<pre>"; print_r($responceNew);die;
            if ($responceNew[0]=='Successfully Inserted') 
            {
                echo '1';
            } 
            else if ($responceNew[0]=='MORE3') 
            {
                echo 'MORE3';
            } 
            else {
                echo '0';
            }
        } else {
            echo 'PaasNOTMatch';
        }
    }
    
    
    if ($_action == "FillSecurityQuestion") {
        $response = $emp->GetSecurityQuestion();
        echo "<option value='0'>Select Security Question</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['SQ_ID'] . ">" . $_Row['Security_Question'] . "</option>";            
        }
    }
    
    
    if ($_action == "setSecurityQueAns" && isset($_POST["ccode"]) && !empty($_POST["ccode"])) {
        
        if($_POST["securityqueans"]=='')
        {
          echo 'ANSBLANCK';  
        }
        else
        {
            $response = $emp->setSecurityQueAns( $_POST["SQ_ID"], trim($_POST["securityqueans"]), $_POST["ccode"]);
            //echo "<pre>"; print_r($response);
            
            if($response[0]=='Successfully Inserted')
                {
                    if (mysqli_num_rows($response[2])) 
                        {
                         $result = [
                            'msg' => 'Please register your new passowrd.',
                            'oid' => $_POST["oid"],
                            'ccode' => $_POST["ccode"],
                        ];

                        echo json_encode($result);
                        } 
                }
            else if ($response[0]=='Success')
                {
                
                    if (mysqli_num_rows($response[2])) 
                        {
                         $result = [
                            'msg' => 'Please register your new passowrd.',
                            'oid' => $_POST["oid"],
                            'ccode' => $_POST["ccode"],
                        ];

                        echo json_encode($result);
                        } 
                }                
            else {
                    echo 'NORECORD';  
                }
        
        }
    }
    
    
    // theser part is used for login security questions//
    
    if ($_action == "SecurityAns" && isset($_POST["ccode"]) && !empty($_POST["ccode"])) {
        
        if($_POST["securityqueans"]=='')
        {
          echo 'ANSBLANCK';  
        }
        else
        {
            $response = $emp->setSecurityQueAns( $_POST["SQ_ID"], $_POST["securityqueans"], $_POST["ccode"]);
            //echo "<pre>"; print_r($response);
            
             if ($response[0]=='Success')
                {
                
                    if (mysqli_num_rows($response[2])) 
                        {
                        
                        $_Center = trim($_POST["ccode"]);
                        $response = $emp->Forgot($_Center);
                        //echo "<pre>"; print_r($response);die;
                        if ($response[0] != Message::NoRecordFound) 
                            {
                        $_row = mysqli_fetch_array($response[2]);
                        $_Mobile =  "XXXXXX". substr($_row['AO_Mobile'], 6);
                        $_OTP = $_row["AO_OTP"];
                        $_OTP_StartOn = $_row["OTP_StartOn"];
                        $_OTP_EndOn = $_row["OTP_EndOn"];
                        $_SMS = "Dear MYRKCL User ". $_Center .", Your verification OTP is " . $_OTP ." for login in MYRKCL. Valid till ".$_OTP_EndOn."."; 
                        SendSMS($_row['AO_Mobile'], $_SMS);
                        $result = [
                            //'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
                            'frm' => $emp->getVeriFyOTPForm2($_row["AO_Code"], $_Center, $_Mobile),
                        ];

                        echo json_encode($result);
                    } 
                        else 
                            {
                            echo '0';
                            }
                        } 
                }                
            else {
                    echo 'NORECORD';  
                }
        
        }
    }
    
    if ($_action == "VerifyOTPSecurityAns" && isset($_POST["oid"]) && !empty($_POST["oid"])) {
        
        if($_POST["otp"]=='')
        {
          echo 'OTPBLANCK';  
        }
        else{
            $response = $emp->VerifyOTPNEW($_POST["oid"], $_POST["otp"]);
            //echo "<pre>"; print_r($response);die;
            if($response[0]=='EXPIRE')
            {
               echo 'EXPIRE';

            }
            else if($response[0]=='NORECORD')
            {
               echo 'NORECORD';

            }

            else
            {
                if($response[0]=='Success')
                {
                   $_Center = trim($_POST["ccode"]);
                   $response = $emp->GetLoginDetailNew($_Center);
                   $_Row = $response[2];
                   if($_Row!=0)
                        {
                            $_SESSION['Login'] = 1;
                            $_SESSION['User_LoginId'] = $_Row['User_LoginId'];
                            $_SESSION['User_UserRoll'] = $_Row['User_UserRoll'];
                            $_SESSION['User_ParentId'] = $_Row['User_ParentId'];
                            $_SESSION['User_Status'] = $_Row['User_Status'];
                            $_SESSION['User_Code'] = $_Row['User_Code'];
                            $_SESSION['User_EmailId'] = $_Row['User_EmailId'];
                            $_SESSION['User_Rsp'] = $_Row['User_Rsp'];

                            $_SESSION['UserRoll_Name'] = $_Row['UserRoll_Name'];

                            $_SESSION['Organization_Name'] = $_Row['Organization_Name'];
                            $_SESSION['Organization_District'] = $_Row['Organization_District'];

                            $_SESSION['LAST_ACTIVITY'] = time();
                            $_SESSION['Menu'] = CreateMenubyUserRole($_SESSION['User_UserRoll']);

                            $response1 = $emp->SaveSession($_Row['User_LoginId'], $_Row['User_UserRoll'], $_Row['User_ParentId'], $_Row['User_Status'], $_Row['User_Code'], $_Row['User_EmailId'], $_Row['UserRoll_Name'], $_Row['Organization_Name']);

                            if ($_Row['User_UserRoll'] == 7) {
                                $checkProfileExpire = $emp->CheckITGKExpireDate($_Row['User_LoginId']);
                                if ($checkProfileExpire) {
                                    $_SESSION['ITGK_Expire_Days'] = $checkProfileExpire;
                                    $iscfa = stripos($checkProfileExpire, 'CFA');
                                    if ($iscfa) {
                                        $_SESSION['ITGK_Expire_For'] = 'For RS-CFA';
                                    } else {
                                        $_SESSION['ITGK_Expire_For'] = 'RS-CIT Center';
                                    }
                                }
                            }
                            echo $response[0];
                        }
                        
                }                
                else {
                    echo '0';
                }
            }
        }
    }
    
    if ($_action == "RESENDOTPLOGIN") {
        if (isset($_POST["Center"]) && !empty($_POST["Center"])) {
		$_Center = $_POST["Center"];
		$response = $emp->ResendOTP($_Center);
                //echo "<pre>"; print_r($response);die;
            if ($response[0] != Message::NoRecordFound) {
                $_row = mysqli_fetch_array($response[2]);
                $_Mobile =  "XXXXXX". substr($_row['AO_Mobile'], 6);
                $_OTP = $_row["AO_OTP"];
                $_OTP_StartOn = $_row["OTP_StartOn"];
                $_OTP_EndOn = $_row["OTP_EndOn"];
                //$_SMS = "Dear MYRKCL User ". $_Center .", Your verification OTP is " . $_OTP ." for forgot Password / Reset Password. Valid till ".$_OTP_EndOn."."; 
                $_SMS = "Dear MYRKCL User ". $_Center .", Your verification OTP is " . $_OTP ." for login in MYRKCL. Valid till ".$_OTP_EndOn."."; 
                SendSMS($_row['AO_Mobile'], $_SMS);
                $result = [
                    //'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
                    'frm' => $emp->getVeriFyOTPForm2($_row["AO_Code"], $_Center, $_Mobile),
                ];

                echo json_encode($result);
            } else {
                echo '0';
            }
        }
    }
    

     // theser part is used for login security questions//
    
    
    /// for sequerty question  start form here 9-1-2019////
    
    
    if ($_action == "FORGOTSEQUERTY") {
        if (isset($_POST["Center"]) && !empty($_POST["Center"])) {
		$_Center = trim($_POST["Center"]);
		$response = $emp->Forgot($_Center);
                //echo "<pre>"; print_r($response);die;
            if ($response[0] != Message::NoRecordFound) {
                $_row = mysqli_fetch_array($response[2]);
                $_Mobile =  "XXXXXX". substr($_row['AO_Mobile'], 6);
                $_OTP = $_row["AO_OTP"];
                $_OTP_StartOn = $_row["OTP_StartOn"];
                $_OTP_EndOn = $_row["OTP_EndOn"];
                $_SMS = "Dear MYRKCL User ". $_Center .", Your verification OTP is " . $_OTP ." for Reset Security Question. Valid till ".$_OTP_EndOn."."; 
                SendSMS($_row['AO_Mobile'], $_SMS);
                $result = [
                    //'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
                    'frm' => $emp->getVeriFyOTPFormSecurity($_row["AO_Code"], $_Center, $_Mobile),
                ];

                echo json_encode($result);
            } else {
                echo '0';
            }
        }
    }
    
    if ($_action == "VerifyOTPSecurity" && isset($_POST["oid"]) && !empty($_POST["oid"])) {
        
        if($_POST["otp"]=='')
        {
          echo 'OTPBLANCK';  
        }
        else{
            $response = $emp->VerifyOTPNEW($_POST["oid"], $_POST["otp"]);
            //echo "<pre>"; print_r($response);die;
            if($response[0]=='EXPIRE')
            {
               echo 'EXPIRE';

            }
            else if($response[0]=='NORECORD')
            {
               echo 'NORECORD';

            }

            else{
            if($response[0]=='Success'){
                $result = [
                            'msg' => 'Please register your new passowrd.',
                            'oid' => $_POST["oid"],
                            'ccode' => $_POST["ccode"],
                            'sqexits' => 0,
                        ];
                    echo json_encode($result);   
                }                
                else {
                    echo '0';
                }
        }
        }
    }
    
    if ($_action == "RESENDOTPSEQUERTY") {
        if (isset($_POST["Center"]) && !empty($_POST["Center"])) {
		$_Center = $_POST["Center"];
		$response = $emp->ResendOTP($_Center);
                //echo "<pre>"; print_r($response);die;
            if ($response[0] != Message::NoRecordFound) {
                $_row = mysqli_fetch_array($response[2]);
                $_Mobile =  "XXXXXX". substr($_row['AO_Mobile'], 6);
                $_OTP = $_row["AO_OTP"];
                $_OTP_StartOn = $_row["OTP_StartOn"];
                $_OTP_EndOn = $_row["OTP_EndOn"];
                $_SMS = "Dear MYRKCL User ". $_Center .", Your verification OTP is " . $_OTP ." for Reset Security Question. Valid till ".$_OTP_EndOn."."; 
                SendSMS($_row['AO_Mobile'], $_SMS);
                $result = [
                    //'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
                    'frm' => $emp->getVeriFyOTPFormSecurity($_row["AO_Code"], $_Center, $_Mobile),
                ];

                echo json_encode($result);
            } else {
                echo '0';
            }
        }
    }
    
    if ($_action == "setSecurityQueAnsADD" && isset($_POST["ccode"]) && !empty($_POST["ccode"])) {
        
        if($_POST["securityqueans"]=='')
        {
          echo 'ANSBLANCK';  
        }
        else
        {
            //echo "<pre>";print_r($_POST);die;
            $response = $emp->setSecurityQueAnsUpdate( $_POST["SQ_ID"], trim($_POST["securityqueans"]), $_POST["ccode"]);
           // echo "<pre>"; print_r($response);
            
            if($response[0]=='Successfully Inserted')
                {
                    if (mysqli_num_rows($response[2])) 
                        {
                         $result = [
                            'msg' => 'Please register your new passowrd.',
                            'oid' => $_POST["oid"],
                            'ccode' => $_POST["ccode"],
                        ];

                        echo json_encode($result);
                        } 
                }
            else if ($response[0]=='Success')
                {
                
                    if (mysqli_num_rows($response[2])) 
                        {
                         $result = [
                            'msg' => 'Please register your new passowrd.',
                            'oid' => $_POST["oid"],
                            'ccode' => $_POST["ccode"],
                        ];

                        echo json_encode($result);
                        } 
                }                
            else {
                    echo 'NORECORD';  
                }
        
        }
    }

?>
