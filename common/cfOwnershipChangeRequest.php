<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsOwnershipChangeRequest.php';
require '../DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();

$response = array();
$emp = new clsOwnershipChangeRequest();


if ($_action == "ADD") {
    global $_ObjFTPConnection;
//    print_r($_FILES);
//    die;
    if (isset($_POST["txtName1"]) && !empty($_POST["txtName1"])) {
        if (isset($_POST["txtType"]) && !empty($_POST["txtType"])) {
            if (isset($_POST["txtRegno"]) && !empty($_POST["txtRegno"])) {
                if (isset($_POST["txtIfscCode"]) && !empty($_POST["txtIfscCode"])) {
                    if (isset($_POST["txtaccountName"]) && !empty($_POST["txtaccountName"])) {
                        if (isset($_POST["txtaccountNumber"]) && !empty($_POST["txtaccountNumber"])) {
                            if (isset($_POST["ddlBankName"]) && !empty($_POST["ddlBankName"])) {
                                if (isset($_POST["ddlBranch"]) && !empty($_POST["ddlBranch"])) {
                                    if (isset($_POST["txtMicrcode"]) && !empty($_POST["txtMicrcode"])) {
                                        if (isset($_POST["ddlidproof"]) && !empty($_POST["ddlidproof"])) {
                                            if (isset($_POST["txtAADHARNo"]) && !empty($_POST["txtAADHARNo"])) {
                                                if (isset($_POST["txtDoctype"]) && !empty($_POST["txtDoctype"])) {
                                                    if (isset($_POST["txtpanno1"]) && !empty($_POST["txtpanno1"])) {
                                                        if (isset($_POST["txtpanname"]) && !empty($_POST["txtpanname"])) {


                                                            $_OrgAOType = $_POST["ddlOrgType"];
                                                            $_OrgEmail = $_POST["txtEmail"];
                                                            $_OrgMobile = $_POST["txtMobile"];

                                                            $_OrgName = trim($_POST["txtName1"]);
                                                            $_OrgRegno = trim($_POST["txtRegno"]);
                                                            $_OrgEstDate = $_POST["txtEstdate"];
                                                            $_OrgType = $_POST["txtType"];


                                                            $_doctype = $_POST["txtDoctype"];
                                                            $_PanNo = $_POST["txtPanNo"];
                                                            $_AADHARNo = $_POST["txtAADHARNo"];

                                                            $_IFSC_Code = $_POST["txtIfscCode"];
                                                            $_AccountName = $_POST["txtaccountName"];
                                                            $_AccountNumber = $_POST["txtaccountNumber"];
                                                            $_BankName = $_POST["ddlBankName"];
                                                            $_BranchName = $_POST["ddlBranch"];
                                                            $_MicrCode = $_POST["txtMicrcode"];
                                                            $_AccountType = $_POST["rbtaccountType_saving"];
                                                            $_PanNo1 = $_POST["txtpanno1"];
                                                            $_PanName = $_POST["txtpanname"];
                                                            $_IdProof = $_POST["ddlidproof"];

                                                            $_GeneratedId = $_POST["txtGenerateId"];
                                                            
                                                            
                                                            $_UploadDirectory = "/ORGTYPEDOC";
                                                                            

                                                            $bankpanimg = $_FILES['pancard']['name'];
                                                            $bankpantmp = $_FILES['pancard']['tmp_name'];
                                                            $bankpantemp = explode(".", $bankpanimg);
                                                            //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
                                                            $BankPANfilename = $_GeneratedId . '_bankpancard.' . end($bankpantemp);
                                                            $bankpanfilestorepath = "../upload/Bankdocs/" . $BankPANfilename;
                                                            $bankpanimageFileType = pathinfo($bankpanfilestorepath, PATHINFO_EXTENSION);
                                                            $bankpanfilestorepathftp = "/Bankdocs";
                                                            $bankpan_response_ftp=ftpUploadFile($bankpanfilestorepathftp,$BankPANfilename,$bankpantmp);

                                                            $bankdocimg = $_FILES['chequeImage']['name'];
                                                            $bankdoctmp = $_FILES['chequeImage']['tmp_name'];
                                                            $bankdoctemp = explode(".", $bankdocimg);
                                                            //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
                                                            $bankdocfilename = $_GeneratedId . '_bankdoc.' . end($bankdoctemp);
                                                            $bankdocfilestorepath = "../upload/Bankdocs/" . $bankdocfilename;
                                                            $bankdocimageFileType = pathinfo($bankdocfilestorepath, PATHINFO_EXTENSION);
                                                            $bankdocfilestorepathftp = "/Bankdocs";
                                                            $bankdoc_response_ftp=ftpUploadFile($bankdocfilestorepathftp,$bankdocfilename,$bankdoctmp);


                                                            $panimg = $_FILES['panno']['name'];
                                                            $pantmp = $_FILES['panno']['tmp_name'];
                                                            $pantemp = explode(".", $panimg);
                                                            //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
                                                            $PANfilename = $_GeneratedId . '_pancard.' . end($pantemp);
                                                            $panfilestorepath = "../upload/NCRPAN/" . $PANfilename;
                                                            $panimageFileType = pathinfo($panfilestorepath, PATHINFO_EXTENSION);
                                                            $panfilestorepathftp = "/NCRPAN";
                                                            $ncrpan_response_ftp=ftpUploadFile($panfilestorepathftp,$PANfilename,$pantmp);

                                                            $uidimg = $_FILES['aadharno']['name'];
                                                            $uidtmp = $_FILES['aadharno']['tmp_name'];
                                                            $uidtemp = explode(".", $uidimg);
                                                            $UIDfilename = $_GeneratedId . '_uid.' . end($pantemp);
                                                            $uidfilestorepath = "../upload/NCRUID/" . $UIDfilename;
                                                            $uidimageFileType1 = pathinfo($uidfilestorepath, PATHINFO_EXTENSION);
                                                            $uidfilestorepathftp = "/NCRUID";
                                                            $ncruid_response_ftp=ftpUploadFile($uidfilestorepathftp,$UIDfilename,$uidtmp);

                                                            $addproofimg = $_FILES['addproof']['name'];
                                                            $addprooftmp = $_FILES['addproof']['tmp_name'];
                                                            $addprooftemp = explode(".", $addproofimg);
                                                            $addprooffilename = $_GeneratedId . '_addproof.' . end($pantemp);
                                                            $addprooffilestorepath = "../upload/NCRAddProof/" . $addprooffilename;
                                                            $addproofimageFileType = pathinfo($addprooffilestorepath, PATHINFO_EXTENSION);
                                                            $addprooffilestorepathftp = "/NCRAddProof";
                                                            $addproof_response_ftp=ftpUploadFile($addprooffilestorepathftp,$addprooffilename,$addprooftmp);

                                                            $appformimg = $_FILES['appform']['name'];
                                                            $appformtmp = $_FILES['appform']['tmp_name'];
                                                            $appformtemp = explode(".", $appformimg);
                                                            $appformfilename = $_GeneratedId . '_appform.' . end($pantemp);
                                                            $appformfilestorepath = "../upload/NCRAppForm/" . $appformfilename;
                                                            $appformimageFileType = pathinfo($appformfilestorepath, PATHINFO_EXTENSION);
                                                            $appformfilestorepathftp = "/NCRAppForm";
                                                            $appform_response_ftp=ftpUploadFile($appformfilestorepathftp,$appformfilename,$appformtmp);

                                                            //}
                                                            if ($_OrgType == '1') {
                                                                $filename = "Individual";
                                                                $_OwnType = $_POST["ddlowntype"];
                                                                //print_r($_OwnType);
                                                                if ($_OwnType == 'Rented' || $_OwnType == 'NOC') {
                                                                    $roimg = $_FILES['utilitybill']['name'];
                                                                    $rotmp = $_FILES['utilitybill']['tmp_name'];
                                                                    $rotemp = explode(".", $roimg);
                                                                    //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
                                                                    $rofilename = $_GeneratedId . '_' . $filename . '_utilitybill.' . end($rotemp);
                                                                    $rofilestorepath = "../upload/ORGTYPEDOC/" . $rofilename;
                                                                    $roimageFileType = pathinfo($rofilestorepath, PATHINFO_EXTENSION);
                                                                    if (($_FILES["utilitybill"]["size"] < 2000000 && $_FILES["utilitybill"]["size"] > 100000)) {
                                                                        if ($roimageFileType == "pdf") {
                                                                        $response_ftp=ftpUploadFile($_UploadDirectory,$rofilename,$_FILES["utilitybill"]["tmp_name"]);
                                                                            if(trim($response_ftp) == "SuccessfullyUploaded"){
                                                                            //if (move_uploaded_file($rotmp, $rofilestorepath)) {
                                                                            } else {
                                                                                echo "Utility Bill not uploaded. Please try again.";
                                                                                return FALSE;
                                                                            }
                                                                        } else {
                                                                            echo "Sorry, File Not Valid";
                                                                            return FALSE;
                                                                        }
                                                                    } else {
                                                                        echo "File Size should be between 100KB to 2MB.";
                                                                        return FALSE;
                                                                    }
                                                                }

                                                                $owntypeimg = $_FILES['owntype']['name'];
                                                                $owntypetmp = $_FILES['owntype']['tmp_name'];
                                                                $owntypetemp = explode(".", $owntypeimg);
                                                                //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
                                                                $owntypefilename = $_GeneratedId . '_' . $filename . '_owntype.' . end($owntypetemp);
                                                                $owntypefilestorepath = "../upload/ORGTYPEDOC/" . $owntypefilename;
                                                                $owntypeimageFileType = pathinfo($owntypefilestorepath, PATHINFO_EXTENSION);
                                                                $owntype_response_ftp=ftpUploadFile($_UploadDirectory,$owntypefilename,$owntypetmp);

                                                                $salpiimg = $_FILES['salpi']['name'];
                                                                $salpitmp = $_FILES['salpi']['tmp_name'];
                                                                $salpitemp = explode(".", $salpiimg);
                                                                $salpifilename = $_GeneratedId . '_' . $filename . '_salpi.' . end($salpitemp);
                                                                $salpifilestorepath = "../upload/ORGTYPEDOC/" . $salpifilename;
                                                                $salpiimageFileType = pathinfo($salpifilestorepath, PATHINFO_EXTENSION);
                                                                $salpi_response_ftp=ftpUploadFile($_UploadDirectory,$salpifilename,$salpitmp);

                                                                $panpiimg = $_FILES['panpi']['name'];
                                                                $panpitmp = $_FILES['panpi']['tmp_name'];
                                                                $panpitemp = explode(".", $panpiimg);
                                                                $panpifilename = $_GeneratedId . '_' . $filename . '_panpi.' . end($panpitemp);
                                                                $panpifilestorepath = "../upload/ORGTYPEDOC/" . $panpifilename;
                                                                $panpiimageFileType = pathinfo($panpifilestorepath, PATHINFO_EXTENSION);
                                                                $panpi_response_ftp=ftpUploadFile($_UploadDirectory,$panpifilename,$panpitmp);

                                                                $ccpiimg = $_FILES['ccpi']['name'];
                                                                $ccpitmp = $_FILES['ccpi']['tmp_name'];
                                                                $ccpitemp = explode(".", $ccpiimg);
                                                                $ccpifilename = $_GeneratedId . '_' . $filename . '_ccpi.' . end($ccpitemp);
                                                                $ccpifilestorepath = "../upload/ORGTYPEDOC/" . $ccpifilename;
                                                                $ccpiimageFileType = pathinfo($ccpifilestorepath, PATHINFO_EXTENSION);
                                                                $ccpi_response_ftp=ftpUploadFile($_UploadDirectory,$ccpifilename,$ccpitmp);

                                                                if (($_FILES["owntype"]["size"] < 2000000 && $_FILES["owntype"]["size"] > 100000) || ($_FILES["salpi"]["size"] < 200000 && $_FILES["salpi"]["size"] > 100000) || ($_FILES["panpi"]["size"] < 200000 && $_FILES["panpi"]["size"] > 100000) || ($_FILES["ccpi"]["size"] < 200000 && $_FILES["ccpi"]["size"] > 100000)) {

                                                                    if ($owntypeimageFileType == "pdf" && $salpiimageFileType == "pdf" && $panpiimageFileType == "pdf" && $ccpiimageFileType == "pdf") {
                                                                        if(trim($owntype_response_ftp) == "SuccessfullyUploaded" && trim($salpi_response_ftp) == "SuccessfullyUploaded" && trim($panpi_response_ftp) == "SuccessfullyUploaded" && trim($ccpi_response_ftp) == "SuccessfullyUploaded"){
                                                                        //if (move_uploaded_file($owntypetmp, $owntypefilestorepath) && move_uploaded_file($salpitmp, $salpifilestorepath) && move_uploaded_file($panpitmp, $panpifilestorepath) && move_uploaded_file($ccpitmp, $ccpifilestorepath)) {
                                                                            if(trim($ncrpan_response_ftp) == "SuccessfullyUploaded" && trim($ncruid_response_ftp) == "SuccessfullyUploaded" && trim($addproof_response_ftp) == "SuccessfullyUploaded" && trim($appform_response_ftp) == "SuccessfullyUploaded" && trim($bankpan_response_ftp) == "SuccessfullyUploaded" && trim($bankdoc_response_ftp) == "SuccessfullyUploaded"){
                                                                            //if (move_uploaded_file($pantmp, $panfilestorepath) && move_uploaded_file($uidtmp, $uidfilestorepath) && move_uploaded_file($addprooftmp, $addprooffilestorepath) && move_uploaded_file($appformtmp, $appformfilestorepath) && move_uploaded_file($bankpantmp, $bankpanfilestorepath) && move_uploaded_file($bankdoctmp, $bankdocfilestorepath)) {
                                                                                $response = $emp->Add($_GeneratedId, $_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_OrgEstDate, $PANfilename, $UIDfilename, $addprooffilename, $appformfilename, $_doctype, $_PanNo, $_AADHARNo, $_IFSC_Code, $_AccountName, $_AccountNumber, $_BankName, $_BranchName, $_MicrCode, $_AccountType, $_PanNo1, $_PanName, $_IdProof, $BankPANfilename, $bankdocfilename);


                                                                                echo $response[0];
                                                                            } else {
                                                                                echo "Orgnization Owner Documents not upload. Please try again.";
                                                                            }
                                                                        } else {
                                                                            echo "Orgnization Type Documents not upload. Please try again.";
                                                                        }
                                                                    } else {
                                                                        echo "Sorry, File Not Valid";
                                                                    }
                                                                } else {
                                                                    echo "File Size should be between 100KB to 2MB.";
                                                                }
                                                            }
                                                            if ($_OrgType == '2') {
                                                                $filename = "Partnership";
                                                                $copdimg = $_FILES['copd']['name'];
                                                                $copdtmp = $_FILES['copd']['tmp_name'];
                                                                $copdtemp = explode(".", $copdimg);
                                                                //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
                                                                $copdfilename = $_GeneratedId . '_' . $filename . '_copd.' . end($copdtemp);
                                                                $copdfilestorepath = "../upload/ORGTYPEDOC/" . $copdfilename;
                                                                $copdimageFileType = pathinfo($copdfilestorepath, PATHINFO_EXTENSION);
                                                                $copd_response_ftp=ftpUploadFile($_UploadDirectory,$copdfilename,$copdtmp);

                                                                $alimg = $_FILES['al']['name'];
                                                                $altmp = $_FILES['al']['tmp_name'];
                                                                $altemp = explode(".", $alimg);
                                                                $alfilename = $_GeneratedId . '_' . $filename . '_al.' . end($altemp);
                                                                $alfilestorepath = "../upload/ORGTYPEDOC/" . $alfilename;
                                                                $alimageFileType = pathinfo($alfilestorepath, PATHINFO_EXTENSION);
                                                                $al_response_ftp=ftpUploadFile($_UploadDirectory,$alfilename,$altmp);

                                                                $ppfimg = $_FILES['ppf']['name'];
                                                                $ppftmp = $_FILES['ppf']['tmp_name'];
                                                                $ppftemp = explode(".", $ppfimg);
                                                                $ppffilename = $_GeneratedId . '_' . $filename . '_ppf.' . end($ppftemp);
                                                                $ppffilestorepath = "../upload/ORGTYPEDOC/" . $ppffilename;
                                                                $ppfimageFileType = pathinfo($ppffilestorepath, PATHINFO_EXTENSION);
                                                                $ppf_response_ftp=ftpUploadFile($_UploadDirectory,$ppffilename,$ppftmp);


                                                                $ccpfimg = $_FILES['ccpf']['name'];
                                                                $ccpftmp = $_FILES['ccpf']['tmp_name'];
                                                                $ccpftemp = explode(".", $ccpfimg);
                                                                $ccpffilename = $_GeneratedId . '_' . $filename . '_ccpf.' . end($ccpftemp);
                                                                $ccpffilestorepath = "../upload/ORGTYPEDOC/" . $ccpffilename;
                                                                $ccpfimageFileType = pathinfo($ccpffilestorepath, PATHINFO_EXTENSION);
                                                                $ccpf_response_ftp=ftpUploadFile($_UploadDirectory,$ccpffilename,$ccpftmp);

                                                                $apdpfimg = $_FILES['apdpf']['name'];
                                                                $apdpftmp = $_FILES['apdpf']['tmp_name'];
                                                                $apdpftemp = explode(".", $apdpfimg);
                                                                $apdpffilename = $_GeneratedId . '_' . $filename . '_apdpf.' . end($apdpftemp);
                                                                $apdpffilestorepath = "../upload/ORGTYPEDOC/" . $apdpffilename;
                                                                $apdpfimageFileType = pathinfo($apdpffilestorepath, PATHINFO_EXTENSION);
                                                                $apdpf_response_ftp=ftpUploadFile($_UploadDirectory,$apdpffilename,$apdpftmp);

                                                                $rcrfimg = $_FILES['rcrf']['name'];
                                                                $rcrftmp = $_FILES['rcrf']['tmp_name'];
                                                                $rcrftemp = explode(".", $rcrfimg);
                                                                $rcrffilename = $_GeneratedId . '_' . $filename . '_rcrf.' . end($rcrftemp);
                                                                $rcrffilestorepath = "../upload/ORGTYPEDOC/" . $rcrffilename;
                                                                $rcrfimageFileType = pathinfo($rcrffilestorepath, PATHINFO_EXTENSION);
                                                                $rcrf_response_ftp=ftpUploadFile($_UploadDirectory,$rcrffilename,$rcrftmp);

                                                                $sarpfimg = $_FILES['sarpf']['name'];
                                                                $sarpftmp = $_FILES['sarpf']['tmp_name'];
                                                                $sarpftemp = explode(".", $sarpfimg);
                                                                $sarpffilename = $_GeneratedId . '_' . $filename . '_sarpf.' . end($sarpftemp);
                                                                $sarpffilestorepath = "../upload/ORGTYPEDOC/" . $sarpffilename;
                                                                $sarpfimageFileType = pathinfo($sarpffilestorepath, PATHINFO_EXTENSION);
                                                                $sarpf_response_ftp=ftpUploadFile($_UploadDirectory,$sarpffilename,$sarpftmp);

                                                                if (($_FILES["copd"]["size"] < 2000000 && $_FILES["copd"]["size"] > 100000) || ($_FILES["al"]["size"] < 200000 && $_FILES["al"]["size"] > 100000) || ($_FILES["ppf"]["size"] < 200000 && $_FILES["ppf"]["size"] > 100000) || ($_FILES["ccpf"]["size"] < 200000 && $_FILES["ccpf"]["size"] > 100000) || ($_FILES["apdpf"]["size"] < 200000 && $_FILES["apdpf"]["size"] > 100000) || ($_FILES["rcrf"]["size"] < 200000 && $_FILES["rcrf"]["size"] > 100000) || ($_FILES["sarpf"]["size"] < 200000 && $_FILES["sarpf"]["size"] > 100000)) {

                                                                    if ($copdimageFileType == "pdf" && $alimageFileType == "pdf" && $ppfimageFileType == "pdf" && $ccpfimageFileType == "pdf" && $apdpfimageFileType == "pdf" && $rcrfimageFileType == "pdf" && $sarpfimageFileType == "pdf") {
                                                                        
                                                                        if(trim($copd_response_ftp) == "SuccessfullyUploaded" && trim($al_response_ftp) == "SuccessfullyUploaded" && trim($ppf_response_ftp) == "SuccessfullyUploaded" && trim($ccpf_response_ftp) == "SuccessfullyUploaded" && trim($apdpf_response_ftp) == "SuccessfullyUploaded"  && trim($rcrf_response_ftp) == "SuccessfullyUploaded"  && trim($sarpf_response_ftp) == "SuccessfullyUploaded"){
                                                                        //if (move_uploaded_file($copdtmp, $copdfilestorepath) && move_uploaded_file($altmp, $alfilestorepath) && move_uploaded_file($ppftmp, $ppffilestorepath) && move_uploaded_file($ccpftmp, $ccpffilestorepath) && move_uploaded_file($apdpftmp, $apdpffilestorepath) && move_uploaded_file($rcrftmp, $rcrffilestorepath) && move_uploaded_file($sarpftmp, $sarpffilestorepath)) {
                                                                            if(trim($ncrpan_response_ftp) == "SuccessfullyUploaded" && trim($ncruid_response_ftp) == "SuccessfullyUploaded" && trim($addproof_response_ftp) == "SuccessfullyUploaded" && trim($appform_response_ftp) == "SuccessfullyUploaded" && trim($bankpan_response_ftp) == "SuccessfullyUploaded" && trim($bankdoc_response_ftp) == "SuccessfullyUploaded"){
                                                                            //if (move_uploaded_file($pantmp, $panfilestorepath) && move_uploaded_file($uidtmp, $uidfilestorepath) && move_uploaded_file($addprooftmp, $addprooffilestorepath) && move_uploaded_file($appformtmp, $appformfilestorepath) && move_uploaded_file($bankpantmp, $bankpanfilestorepath) && move_uploaded_file($bankdoctmp, $bankdocfilestorepath)) {
                                                                                $response = $emp->Add($_GeneratedId, $_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_OrgEstDate, $PANfilename, $UIDfilename, $addprooffilename, $appformfilename, $_doctype, $_PanNo, $_AADHARNo, $_IFSC_Code, $_AccountName, $_AccountNumber, $_BankName, $_BranchName, $_MicrCode, $_AccountType, $_PanNo1, $_PanName, $_IdProof, $BankPANfilename, $bankdocfilename);

                                                                                echo $response[0];
                                                                            } else {
                                                                                echo "Orgnization Owner Documents not upload. Please try again.";
                                                                            }
                                                                        } else {
                                                                            echo "Orgnization Type Documents not upload. Please try again.";
                                                                        }
                                                                    } else {
                                                                        echo "Sorry, File Not Valid";
                                                                    }
                                                                } else {
                                                                    echo "File Size should be between 100KB to 2MB.";
                                                                }
                                                            }
                                                            if ($_OrgType == '3' || $_OrgType == '4') {
                                                                $filename = "Company";
                                                                $coiimg = $_FILES['coi']['name'];
                                                                $coitmp = $_FILES['coi']['tmp_name'];
                                                                $coitemp = explode(".", $coiimg);
                                                                //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
                                                                $coifilename = $_GeneratedId . '_' . $filename . '_coi.' . end($coitemp);
                                                                $coifilestorepath = "../upload/ORGTYPEDOC/" . $coifilename;
                                                                $coiimageFileType = pathinfo($coifilestorepath, PATHINFO_EXTENSION);
                                                                $coi_response_ftp=ftpUploadFile($_UploadDirectory,$coifilename,$coitmp);

                                                                $lodimg = $_FILES['lod']['name'];
                                                                $lodtmp = $_FILES['lod']['tmp_name'];
                                                                $lodtemp = explode(".", $lodimg);
                                                                $lodfilename = $_GeneratedId . '_' . $filename . '_lod.' . end($lodtemp);
                                                                $lodfilestorepath = "../upload/ORGTYPEDOC/" . $lodfilename;
                                                                $lodimageFileType = pathinfo($lodfilestorepath, PATHINFO_EXTENSION);
                                                                $lod_response_ftp=ftpUploadFile($_UploadDirectory,$lodfilename,$lodtmp);

                                                                $copanimg = $_FILES['copan']['name'];
                                                                $copantmp = $_FILES['copan']['tmp_name'];
                                                                $copantemp = explode(".", $copanimg);
                                                                $copanfilename = $_GeneratedId . '_' . $filename . '_copan.' . end($copantemp);
                                                                $copanfilestorepath = "../upload/ORGTYPEDOC/" . $copanfilename;
                                                                $copanimageFileType = pathinfo($copanfilestorepath, PATHINFO_EXTENSION);
                                                                $copan_response_ftp=ftpUploadFile($_UploadDirectory,$copanfilename,$copantmp);

                                                                $cccimg = $_FILES['ccc']['name'];
                                                                $ccctmp = $_FILES['ccc']['tmp_name'];
                                                                $ccctemp = explode(".", $cccimg);
                                                                $cccfilename = $_GeneratedId . '_' . $filename . '_ccc.' . end($ccctemp);
                                                                $cccfilestorepath = "../upload/ORGTYPEDOC/" . $cccfilename;
                                                                $cccimageFileType = pathinfo($cccfilestorepath, PATHINFO_EXTENSION);
                                                                $ccc_response_ftp=ftpUploadFile($_UploadDirectory,$cccfilename,$ccctmp);


                                                                $cobrimg = $_FILES['cobr']['name'];
                                                                $cobrtmp = $_FILES['cobr']['tmp_name'];
                                                                $cobrtemp = explode(".", $cobrimg);
                                                                $cobrfilename = $_GeneratedId . '_' . $filename . '_cobr.' . end($cobrtemp);
                                                                $cobrfilestorepath = "../upload/ORGTYPEDOC/" . $cobrfilename;
                                                                $cobrimageFileType = pathinfo($cobrfilestorepath, PATHINFO_EXTENSION);
                                                                $cobr_response_ftp=ftpUploadFile($_UploadDirectory,$cobrfilename,$cobrtmp);

                                                                $apdimg = $_FILES['apd']['name'];
                                                                $apdtmp = $_FILES['apd']['tmp_name'];
                                                                $apdtemp = explode(".", $apdimg);
                                                                $apdfilename = $_GeneratedId . '_' . $filename . '_apd.' . end($apdtemp);
                                                                $apdfilestorepath = "../upload/ORGTYPEDOC/" . $apdfilename;
                                                                $apdimageFileType = pathinfo($apdfilestorepath, PATHINFO_EXTENSION);
                                                                $apd_response_ftp=ftpUploadFile($_UploadDirectory,$apdfilename,$apdtmp);

                                                                $csarimg = $_FILES['csar']['name'];
                                                                $csartmp = $_FILES['csar']['tmp_name'];
                                                                $csartemp = explode(".", $csarimg);
                                                                $csarfilename = $_GeneratedId . '_' . $filename . '_csar.' . end($csartemp);
                                                                $csarfilestorepath = "../upload/ORGTYPEDOC/" . $csarfilename;
                                                                $csarimageFileType = pathinfo($csarfilestorepath, PATHINFO_EXTENSION);
                                                                $csar_response_ftp=ftpUploadFile($_UploadDirectory,$csarfilename,$csartmp);

                                                                if (($_FILES["coi"]["size"] < 2000000 && $_FILES["coi"]["size"] > 100000) || ($_FILES["lod"]["size"] < 200000 && $_FILES["lod"]["size"] > 100000) || ($_FILES["copan"]["size"] < 200000 && $_FILES["copan"]["size"] > 100000) || ($_FILES["ccc"]["size"] < 200000 && $_FILES["ccc"]["size"] > 100000) || ($_FILES["cobr"]["size"] < 200000 && $_FILES["cobr"]["size"] > 100000) || ($_FILES["apd"]["size"] < 200000 && $_FILES["apd"]["size"] > 100000) || ($_FILES["csar"]["size"] < 200000 && $_FILES["csar"]["size"] > 100000)) {

                                                                    if ($coiimageFileType == "pdf" && $lodimageFileType == "pdf" && $copanimageFileType == "pdf" && $cccimageFileType == "pdf" && $cobrimageFileType == "pdf" && $apdimageFileType == "pdf" && $csarimageFileType == "pdf") {
                                                                        
                                                                        if(trim($coi_response_ftp) == "SuccessfullyUploaded" && trim($lod_response_ftp) == "SuccessfullyUploaded" && trim($copan_response_ftp) == "SuccessfullyUploaded" && trim($ccc_response_ftp) == "SuccessfullyUploaded" && trim($cobr_response_ftp) == "SuccessfullyUploaded"  && trim($apd_response_ftp) == "SuccessfullyUploaded"  && trim($csar_response_ftp) == "SuccessfullyUploaded"){
                                                                        //if (move_uploaded_file($coitmp, $coifilestorepath) && move_uploaded_file($lodtmp, $lodfilestorepath) && move_uploaded_file($copantmp, $copanfilestorepath) && move_uploaded_file($ccctmp, $cccfilestorepath) && move_uploaded_file($cobrtmp, $cobrfilestorepath) && move_uploaded_file($apdtmp, $apdfilestorepath) && move_uploaded_file($csartmp, $csarfilestorepath)) {
                                                                            if(trim($ncrpan_response_ftp) == "SuccessfullyUploaded" && trim($ncruid_response_ftp) == "SuccessfullyUploaded" && trim($addproof_response_ftp) == "SuccessfullyUploaded" && trim($appform_response_ftp) == "SuccessfullyUploaded" && trim($bankpan_response_ftp) == "SuccessfullyUploaded" && trim($bankdoc_response_ftp) == "SuccessfullyUploaded"){
                                                                            //if (move_uploaded_file($pantmp, $panfilestorepath) && move_uploaded_file($uidtmp, $uidfilestorepath) && move_uploaded_file($addprooftmp, $addprooffilestorepath) && move_uploaded_file($appformtmp, $appformfilestorepath && move_uploaded_file($bankpantmp, $bankpanfilestorepath) && move_uploaded_file($bankdoctmp, $bankdocfilestorepath))) {
                                                                                $response = $emp->Add($_GeneratedId, $_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_OrgEstDate, $PANfilename, $UIDfilename, $addprooffilename, $appformfilename, $_doctype, $_PanNo, $_AADHARNo, $_IFSC_Code, $_AccountName, $_AccountNumber, $_BankName, $_BranchName, $_MicrCode, $_AccountType, $_PanNo1, $_PanName, $_IdProof, $BankPANfilename, $bankdocfilename);

                                                                                echo $response[0];
                                                                            } else {
                                                                                echo "Orgnization Owner Documents not upload. Please try again.";
                                                                            }
                                                                        } else {
                                                                            echo "Orgnization Type Documents not upload. Please try again.";
                                                                        }
                                                                    } else {
                                                                        echo "Sorry, File Not Valid";
                                                                    }
                                                                } else {
                                                                    echo "File Size should be between 100KB to 2MB.";
                                                                }
                                                            }
                                                            if ($_OrgType == '8') {
                                                                $filename = "LLP";
                                                                $coillpimg = $_FILES['coillp']['name'];
                                                                $coillptmp = $_FILES['coillp']['tmp_name'];
                                                                $coillptemp = explode(".", $coillpimg);
                                                                //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
                                                                $coillpfilename = $_GeneratedId . '_' . $filename . '_coillp.' . end($coillptemp);
                                                                $coillpfilestorepath = "../upload/ORGTYPEDOC/" . $coillpfilename;
                                                                $coillpimageFileType = pathinfo($coillpfilestorepath, PATHINFO_EXTENSION);
                                                                $coillp_response_ftp=ftpUploadFile($_UploadDirectory,$coillpfilename,$coillptmp);

                                                                $lopimg = $_FILES['lop']['name'];
                                                                $loptmp = $_FILES['lop']['tmp_name'];
                                                                $loptemp = explode(".", $lopimg);
                                                                $lopfilename = $_GeneratedId . '_' . $filename . '_lop.' . end($loptemp);
                                                                $lopfilestorepath = "../upload/ORGTYPEDOC/" . $lopfilename;
                                                                $lopimageFileType = pathinfo($lopfilestorepath, PATHINFO_EXTENSION);
                                                                $lop_response_ftp=ftpUploadFile($_UploadDirectory,$lopfilename,$loptmp);

                                                                $panllpimg = $_FILES['panllp']['name'];
                                                                $panllptmp = $_FILES['panllp']['tmp_name'];
                                                                $panllptemp = explode(".", $panllpimg);
                                                                $panllpfilename = $_GeneratedId . '_' . $filename . '_panllp.' . end($panllptemp);
                                                                $panllpfilestorepath = "../upload/ORGTYPEDOC/" . $panllpfilename;
                                                                $panllpimageFileType = pathinfo($panllpfilestorepath, PATHINFO_EXTENSION);
                                                                $panllp_response_ftp=ftpUploadFile($_UploadDirectory,$panllpfilename,$panllptmp);

                                                                $ccllpimg = $_FILES['ccllp']['name'];
                                                                $ccllptmp = $_FILES['ccllp']['tmp_name'];
                                                                $ccllptemp = explode(".", $ccllpimg);
                                                                $ccllpfilename = $_GeneratedId . '_' . $filename . '_ccllp.' . end($ccllptemp);
                                                                $ccllpfilestorepath = "../upload/ORGTYPEDOC/" . $ccllpfilename;
                                                                $ccllpimageFileType = pathinfo($ccllpfilestorepath, PATHINFO_EXTENSION);
                                                                $ccllp_response_ftp=ftpUploadFile($_UploadDirectory,$ccllpfilename,$ccllptmp);


                                                                $cobrllpimg = $_FILES['cobrllp']['name'];
                                                                $cobrllptmp = $_FILES['cobrllp']['tmp_name'];
                                                                $cobrllptemp = explode(".", $cobrllpimg);
                                                                $cobrllpfilename = $_GeneratedId . '_' . $filename . '_cobrllp.' . end($cobrllptemp);
                                                                $cobrllpfilestorepath = "../upload/ORGTYPEDOC/" . $cobrllpfilename;
                                                                $cobrllpimageFileType = pathinfo($cobrllpfilestorepath, PATHINFO_EXTENSION);
                                                                $cobrllp_response_ftp=ftpUploadFile($_UploadDirectory,$cobrllpfilename,$cobrllptmp);

                                                                $apdllpimg = $_FILES['apdllp']['name'];
                                                                $apdllptmp = $_FILES['apdllp']['tmp_name'];
                                                                $apdllptemp = explode(".", $apdllpimg);
                                                                $apdllpfilename = $_GeneratedId . '_' . $filename . '_apdllp.' . end($apdllptemp);
                                                                $apdllpfilestorepath = "../upload/ORGTYPEDOC/" . $apdllpfilename;
                                                                $apdllpimageFileType = pathinfo($apdllpfilestorepath, PATHINFO_EXTENSION);
                                                                $apdllp_response_ftp=ftpUploadFile($_UploadDirectory,$apdllpfilename,$apdllptmp);

                                                                $csarllpimg = $_FILES['csarllp']['name'];
                                                                $csarllptmp = $_FILES['csarllp']['tmp_name'];
                                                                $csarllptemp = explode(".", $csarllpimg);
                                                                $csarllpfilename = $_GeneratedId . '_' . $filename . '_csarllp.' . end($csarllptemp);
                                                                $csarllpfilestorepath = "../upload/ORGTYPEDOC/" . $csarllpfilename;
                                                                $csarllpimageFileType = pathinfo($csarllpfilestorepath, PATHINFO_EXTENSION);
                                                                $csarllp_response_ftp=ftpUploadFile($_UploadDirectory,$csarllpfilename,$csarllptmp);

                                                                if (($_FILES["coillp"]["size"] < 2000000 && $_FILES["coillp"]["size"] > 100000) || ($_FILES["lop"]["size"] < 200000 && $_FILES["lop"]["size"] > 100000) || ($_FILES["panllp"]["size"] < 200000 && $_FILES["panllp"]["size"] > 100000) || ($_FILES["ccllp"]["size"] < 200000 && $_FILES["ccllp"]["size"] > 100000) || ($_FILES["cobrllp"]["size"] < 200000 && $_FILES["cobrllp"]["size"] > 100000) || ($_FILES["apdllp"]["size"] < 200000 && $_FILES["apdllp"]["size"] > 100000) || ($_FILES["csarllp"]["size"] < 200000 && $_FILES["csarllp"]["size"] > 100000)) {

                                                                    if ($coillpimageFileType == "pdf" && $lopimageFileType == "pdf" && $panllpimageFileType == "pdf" && $ccllpimageFileType == "pdf" && $cobrllpimageFileType == "pdf" && $apdllpimageFileType == "pdf" && $csarllpimageFileType == "pdf") {

                                                                        if(trim($coillp_response_ftp) == "SuccessfullyUploaded" && trim($lop_response_ftp) == "SuccessfullyUploaded" && trim($panllp_response_ftp) == "SuccessfullyUploaded" && trim($ccllp_response_ftp) == "SuccessfullyUploaded" && trim($cobrllp_response_ftp) == "SuccessfullyUploaded"  && trim($apdllp_response_ftp) == "SuccessfullyUploaded"  && trim($csarllp_response_ftp) == "SuccessfullyUploaded"){
                                                                        //if (move_uploaded_file($coillptmp, $coillpfilestorepath) && move_uploaded_file($loptmp, $lopfilestorepath) && move_uploaded_file($panllptmp, $panllpfilestorepath) && move_uploaded_file($ccllptmp, $ccllpfilestorepath) && move_uploaded_file($cobrllptmp, $cobrllpfilestorepath) && move_uploaded_file($apdllptmp, $apdllpfilestorepath) && move_uploaded_file($csarllptmp, $csarllpfilestorepath)) {
                                                                            if(trim($ncrpan_response_ftp) == "SuccessfullyUploaded" && trim($ncruid_response_ftp) == "SuccessfullyUploaded" && trim($addproof_response_ftp) == "SuccessfullyUploaded" && trim($appform_response_ftp) == "SuccessfullyUploaded" && trim($bankpan_response_ftp) == "SuccessfullyUploaded" && trim($bankdoc_response_ftp) == "SuccessfullyUploaded"){
                                                                            //if (move_uploaded_file($pantmp, $panfilestorepath) && move_uploaded_file($uidtmp, $uidfilestorepath) && move_uploaded_file($addprooftmp, $addprooffilestorepath) && move_uploaded_file($appformtmp, $appformfilestorepath) && move_uploaded_file($bankpantmp, $bankpanfilestorepath) && move_uploaded_file($bankdoctmp, $bankdocfilestorepath)) {
                                                                                $response = $emp->Add($_GeneratedId, $_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_OrgEstDate, $PANfilename, $UIDfilename, $addprooffilename, $appformfilename, $_doctype, $_PanNo, $_AADHARNo, $_IFSC_Code, $_AccountName, $_AccountNumber, $_BankName, $_BranchName, $_MicrCode, $_AccountType, $_PanNo1, $_PanName, $_IdProof, $BankPANfilename, $bankdocfilename);

                                                                                echo $response[0];
                                                                            } else {
                                                                                echo "Orgnization Owner Documents not upload. Please try again.";
                                                                            }
                                                                        } else {
                                                                            echo "Orgnization Type Documents not upload. Please try again.";
                                                                        }
                                                                    } else {
                                                                        echo "Sorry, File Not Valid";
                                                                    }
                                                                } else {
                                                                    echo "File Size should be between 100KB to 2MB.";
                                                                }
                                                            }
                                                            if ($_OrgType == '5' || $_OrgType == '6' || $_OrgType == '7' || $_OrgType == '9') {
                                                                $filename = "Other";
                                                                $corimg = $_FILES['cor']['name'];
                                                                $cortmp = $_FILES['cor']['tmp_name'];
                                                                $cortemp = explode(".", $corimg);
                                                                //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
                                                                $corfilename = $_GeneratedId . '_' . $filename . '_cor.' . end($cortemp);
                                                                $corfilestorepath = "../upload/ORGTYPEDOC/" . $corfilename;
                                                                $corimageFileType = pathinfo($corfilestorepath, PATHINFO_EXTENSION);
                                                                $cor_response_ftp=ftpUploadFile($_UploadDirectory,$corfilename,$cortmp);

                                                                $panothimg = $_FILES['panoth']['name'];
                                                                $panothtmp = $_FILES['panoth']['tmp_name'];
                                                                $panothtemp = explode(".", $panothimg);
                                                                $panothfilename = $_GeneratedId . '_' . $filename . '_panoth.' . end($panothtemp);
                                                                $panothfilestorepath = "../upload/ORGTYPEDOC/" . $panothfilename;
                                                                $panothimageFileType = pathinfo($panothfilestorepath, PATHINFO_EXTENSION);
                                                                $panoth_response_ftp=ftpUploadFile($_UploadDirectory,$panothfilename,$panothtmp);

                                                                $ccothimg = $_FILES['ccoth']['name'];
                                                                $ccothtmp = $_FILES['ccoth']['tmp_name'];
                                                                $ccothtemp = explode(".", $ccothimg);
                                                                $ccothfilename = $_GeneratedId . '_' . $filename . '_ccoth.' . end($ccothtemp);
                                                                $ccothfilestorepath = "../upload/ORGTYPEDOC/" . $ccothfilename;
                                                                $ccothimageFileType = pathinfo($ccothfilestorepath, PATHINFO_EXTENSION);
                                                                $ccoth_response_ftp=ftpUploadFile($_UploadDirectory,$ccothfilename,$ccothtmp);

                                                                $lebimg = $_FILES['leb']['name'];
                                                                $lebtmp = $_FILES['leb']['tmp_name'];
                                                                $lebtemp = explode(".", $lebimg);
                                                                $lebfilename = $_GeneratedId . '_' . $filename . '_leb.' . end($lebtemp);
                                                                $lebfilestorepath = "../upload/ORGTYPEDOC/" . $lebfilename;
                                                                $lebimageFileType = pathinfo($lebfilestorepath, PATHINFO_EXTENSION);
                                                                $leb_response_ftp=ftpUploadFile($_UploadDirectory,$lebfilename,$lebtmp);


                                                                $cbrimg = $_FILES['cbr']['name'];
                                                                $cbrtmp = $_FILES['cbr']['tmp_name'];
                                                                $cbrtemp = explode(".", $cbrimg);
                                                                $cbrfilename = $_GeneratedId . '_' . $filename . '_cbr.' . end($cbrtemp);
                                                                $cbrfilestorepath = "../upload/ORGTYPEDOC/" . $cbrfilename;
                                                                $cbrimageFileType = pathinfo($cbrfilestorepath, PATHINFO_EXTENSION);
                                                                $cbr_response_ftp=ftpUploadFile($_UploadDirectory,$cbrfilename,$cbrtmp);

                                                                $adpothimg = $_FILES['adpoth']['name'];
                                                                $adpothtmp = $_FILES['adpoth']['tmp_name'];
                                                                $adpothtemp = explode(".", $adpothimg);
                                                                $adpothfilename = $_GeneratedId . '_' . $filename . '_adpoth.' . end($adpothtemp);
                                                                $adpothfilestorepath = "../upload/ORGTYPEDOC/" . $adpothfilename;
                                                                $adpothimageFileType = pathinfo($adpothfilestorepath, PATHINFO_EXTENSION);
                                                                $adpoth_response_ftp=ftpUploadFile($_UploadDirectory,$adpothfilename,$adpothtmp);

                                                                if (($_FILES["cor"]["size"] < 2000000 && $_FILES["cor"]["size"] > 100000) || ($_FILES["panoth"]["size"] < 200000 && $_FILES["panoth"]["size"] > 100000) || ($_FILES["ccoth"]["size"] < 200000 && $_FILES["ccoth"]["size"] > 100000) || ($_FILES["leb"]["size"] < 200000 && $_FILES["leb"]["size"] > 100000) || ($_FILES["cbr"]["size"] < 200000 && $_FILES["cbr"]["size"] > 100000) || ($_FILES["adpoth"]["size"] < 200000 && $_FILES["adpoth"]["size"] > 100000)) {

                                                                    if ($corimageFileType == "pdf" && $panothimageFileType == "pdf" && $ccothimageFileType == "pdf" && $lebimageFileType == "pdf" && $cbrimageFileType == "pdf" && $adpothimageFileType == "pdf") {
                                                                        
                                                                        if(trim($cor_response_ftp) == "SuccessfullyUploaded" && trim($panoth_response_ftp) == "SuccessfullyUploaded" && trim($ccoth_response_ftp) == "SuccessfullyUploaded" && trim($leb_response_ftp) == "SuccessfullyUploaded" && trim($cbr_response_ftp) == "SuccessfullyUploaded"  && trim($adpoth_response_ftp) == "SuccessfullyUploaded"){
                                                                        //if (move_uploaded_file($cortmp, $corfilestorepath) && move_uploaded_file($panothtmp, $panothfilestorepath) && move_uploaded_file($ccothtmp, $ccothfilestorepath) && move_uploaded_file($lebtmp, $lebfilestorepath) && move_uploaded_file($cbrtmp, $cbrfilestorepath) && move_uploaded_file($adpothtmp, $adpothfilestorepath)) {
                                                                            if(trim($ncrpan_response_ftp) == "SuccessfullyUploaded" && trim($ncruid_response_ftp) == "SuccessfullyUploaded" && trim($addproof_response_ftp) == "SuccessfullyUploaded" && trim($appform_response_ftp) == "SuccessfullyUploaded" && trim($bankpan_response_ftp) == "SuccessfullyUploaded" && trim($bankdoc_response_ftp) == "SuccessfullyUploaded"){
                                                                            //if (move_uploaded_file($pantmp, $panfilestorepath) && move_uploaded_file($uidtmp, $uidfilestorepath) && move_uploaded_file($addprooftmp, $addprooffilestorepath) && move_uploaded_file($appformtmp, $appformfilestorepath) && move_uploaded_file($bankpantmp, $bankpanfilestorepath) && move_uploaded_file($bankdoctmp, $bankdocfilestorepath)) {
                                                                                $response = $emp->Add($_GeneratedId, $_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_OrgEstDate, $PANfilename, $UIDfilename, $addprooffilename, $appformfilename, $_doctype, $_PanNo, $_AADHARNo, $_IFSC_Code, $_AccountName, $_AccountNumber, $_BankName, $_BranchName, $_MicrCode, $_AccountType, $_PanNo1, $_PanName, $_IdProof, $BankPANfilename, $bankdocfilename);

                                                                                echo $response[0];
                                                                            } else {
                                                                                echo "Orgnization Owner Documents not upload. Please try again.";
                                                                            }
                                                                        } else {
                                                                            echo "Orgnization Type Documents not upload. Please try again.";
                                                                        }
                                                                    } else {
                                                                        echo "Sorry, File Not Valid";
                                                                    }
                                                                } else {
                                                                    echo "File Size should be between 100KB to 2MB.";
                                                                }
                                                            }
                                                        } else {
                                                            echo "Inavalid Entry1";
                                                        }
                                                    } else {
                                                        echo "Inavalid Entry2";
                                                    }
                                                } else {
                                                    echo "Inavalid Entry3";
                                                }
                                            } else {
                                                echo "Inavalid Entry4";
                                            }
                                        } else {
                                            echo "Inavalid Entry5";
                                        }
                                    } else {
                                        echo "Inavalid Entry6";
                                    }
                                } else {
                                    echo "Inavalid Entry7";
                                }
                            } else {
                                echo "Inavalid Entry8";
                            }
                        } else {
                            echo "Inavalid Entry9";
                        }
                    } else {
                        echo "Inavalid Entry11";
                    }
                } else {
                    echo "Inavalid Entry12";
                }
            } else {
                echo "Inavalid Entry13";
            }
        } else {
            echo "Inavalid Entry14";
        }
    } else {
        echo "Inavalid Entry15";
    }
}

if ($_action == "GETDETAILS") {
    $response = $emp->GetITGK_Details();
    
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        
        
    if ($_Row['Organization_AreaType'] == 'Urban') {
        $_AT1 = ($_Row['Municipality_Type_Name'] ? $_Row['Municipality_Type_Name'] : 'NA');
        $_AT2 = ($_Row['Municipality_Name'] ? $_Row['Municipality_Name'] : 'NA');
        $_AT3 = ($_Row['Ward_Name'] ? $_Row['Ward_Name'] : 'NA');
    } elseif ($_Row['Organization_AreaType'] == 'Rural') {
        $_AT1 = ($_Row['Block_Name'] ? $_Row['Block_Name'] : 'NA');
        $_AT2 = ($_Row['GP_Name'] ? $_Row['GP_Name'] : 'NA');
        $_AT3 = ($_Row['Village_Name'] ? $_Row['Village_Name'] : 'NA');
    } else {
        $_AT1 = 'NA';
        $_AT2 = 'NA';
        $_AT3 = 'NA';
    }
           

        $_DataTable[$_i] = array( "itgkname" => $_Row['ITGK_Name'],
            "itgktype" => $_Row['Org_Type_Name'],
            "email" => $_Row['User_EmailId'],
            "mobile" => $_Row['User_MobileNo'],
            "address" => $_Row['Organization_Address'],
            "spname" => $_Row['RSP_Name'],
            "district" => $_Row['District_Name'],
            "tehsil" => $_Row['Tehsil_Name'],
            "areatype" => $_Row['Organization_AreaType'],
            "ownertype" => $_Row['Organization_OwnershipType'],
            "at1" => $_AT1,
            "at2" => $_AT2,
            "at3" => $_AT3);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "GenerateOTP") {
    //print_r($_POST);
    if (isset($_POST["emailid"]) && isset($_POST["mobno"])) {
        if (!empty($_POST["emailid"]) && !empty($_POST["mobno"])) {
            $_EmailId = $_POST["emailid"];
            $_MobNo = $_POST["mobno"];

            $response = $emp->GenerateOTP($_EmailId, $_MobNo);
            echo $response[0];
        } else {
            echo "Please Enter Email ID and Mobile Number to Proceed";
        }
    } else {
        echo "Please Enter Email ID and Mobile Number to Proceed";
    }
}

if ($_action == "VERIFY") {
    //print_r($_POST);
    if (isset($_POST["otpemail"]) && isset($_POST["otpmobile"])) {
        $_OTPEmail = $_POST["otpemail"];
        $_OTPMobile = $_POST["otpmobile"];

        $response = $emp->Verify($_OTPEmail, $_OTPMobile);
        echo $response[0];
    } else {
        echo "Please Enter OTP received on Email ID and Mobile Number to Proceed";
    }
}

if ($_action == "GetNcrAmt") {
    // print_r($_POST);
    $response = $emp->GetNcrAmt($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Ncr_Fee_Amount'];
}


if ($_action == "FILLDETAILS") {


    $response = $emp->GetDetails($_POST["txtIfscCode"]);
	 $_Row = mysqli_fetch_array($response[2]);
    $_DataTable = array();
    $_i = 0;
        $_Datatable[$_i] = array("bankname" => $_Row['bankname'],
            "micrcode" => $_Row['micrcode'],
            "branchname" => $_Row['branchname']);
     
    echo json_encode($_Datatable);
}


