<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsPinCodeReport.php';

$response = array();
$emp = new clsPinCodeReport();


if ($_action == "DETAILS") {
    //print_r($_POST);die;

    //echo "Show";
    $response = $emp->GetAll($_POST['values']);
if($count = mysqli_num_rows($response[2])){
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='10%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='40%'>IT-GK Mob No</th>";
    echo "<th style='10%'>IT-GK Email</th>";
    
    echo "<th style='40%'>IT-GK District</th>";
    echo "<th style='10%'>IT-GK Tehsil</th>";
    echo "<th style='10%'>IT-GK Address</th>";
     echo "<th style='10%'>IT-GK PinCode</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['User_LoginId'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['User_MobileNo'] . "</td>";
            echo "<td>" . $_Row['User_EmailId'] . "</td>";
            
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Address'] . "</td>";
            echo "<td>" . $_Row['Organization_PinCode'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
        
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
    } else {
            echo '0';
        }
}


