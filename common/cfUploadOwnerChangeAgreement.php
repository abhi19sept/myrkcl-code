<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsUploadOwnerChangeAgreement.php';
require '../DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();

$response = array();
$emp = new clsUploadOwnerChangeAgreement();


if ($_action == "APPROVE") {
    $response = $emp->GetDatabyCode();
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "orgcourse" => $_Row['Org_Application_Type'],
            "doctype" => $_Row['Organization_DocType'],
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            "ack" => $_Row['Org_Ack'],
            "orgdoc" => $_Row['SPCenter_Agreement'],
            "Orgstatus" => $_Row['Org_Final_Approval_Status']);
        $_i = $_i + 1;
    }

        echo json_encode($_DataTable);
    } else {
        echo "";
    }
}

if ($_action == "ADD") {
    global $_ObjFTPConnection;
//    print_r(($_POST));
//    print_r(($_POST));
//    die;
    if (isset($_POST["txtGenerateId"]) && !empty($_POST["txtGenerateId"])) {
        $_GeneratedId = $_POST["txtGenerateId"];
        $_Ack = $_POST["txtAck"];

        $panimg = $_FILES['uploadImage7']['name'];
        $pantmp = $_FILES['uploadImage7']['tmp_name'];
        $pantemp = explode(".", $panimg);
        //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
        $PANfilename = $_GeneratedId . '_agreement.' . end($pantemp);
        $panfilestorepath = "../upload/SPCENTERAGREEMENT/" . $PANfilename;
        $panimageFileType = pathinfo($panfilestorepath, PATHINFO_EXTENSION);
        $appformfilestorepathftp = "/SPCENTERAGREEMENT";
        $appform_response_ftp=ftpUploadFile($appformfilestorepathftp,$PANfilename,$pantmp);
        
        if (($_FILES["uploadImage7"]["size"] < 5000000 && $_FILES["uploadImage7"]["size"] > 100000)){

            if ($panimageFileType == "pdf") {

                //if (move_uploaded_file($pantmp, $panfilestorepath)) {
                if (trim($appform_response_ftp) == "SuccessfullyUploaded") {
                    $response = $emp->Add($PANfilename,$_Ack);

                    echo $response[0];
                } else {
                    echo "Agreement Not Uploded. Please try again.";
                }
            } else {
                echo "Sorry, File Not Valid";
            }
        } else {
            echo "File Size should be between 100KB to 5MB.";
        }
    } else {
        echo "Inavalid Entry15";
    }
}

if ($_action == "UPDATE") {
    global $_ObjFTPConnection;
//    print_r(($_POST));
//    die;
    if (isset($_POST["txtfilename"]) && !empty($_POST["txtfilename"])) {
        $_FilePath = "../" . $_POST["txtfilename"];
          $_Ack = $_POST["txtAckUpdate"];
        //print_r(($_FilePath));
        $panimg = $_FILES['uploadImage72']['name'];
        $pantmp = $_FILES['uploadImage72']['tmp_name'];
        //$pantemp = explode(".", $panimg);
        //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
        //$PANfilename = $_GeneratedId . '_agreement.' . end($pantemp);
        //$panfilestorepath = "../upload/SPCENTERAGREEMENT/" . $PANfilename;
        $panimageFileType = pathinfo($_FilePath, PATHINFO_EXTENSION);
        $PANfilename = $_POST["txtfilename"];
        $appformfilestorepathftp = "/SPCENTERAGREEMENT";
        $appform_response_ftp=ftpUploadFile($appformfilestorepathftp,$PANfilename,$pantmp);

        if (($_FILES["uploadImage72"]["size"] < 5000000 && $_FILES["uploadImage72"]["size"] > 100000)) {

            if ($panimageFileType == "pdf") {
//                if (file_exists($_FilePath)) {
//                    unlink($_FilePath);
//                    $response = $emp->Update($_Ack);
//                }
                //if (move_uploaded_file($pantmp, $_FilePath)) {
                if (trim($appform_response_ftp) == "SuccessfullyUploaded") {
                    $response = $emp->Update($_Ack);
                    echo "Agreement Updated Successfully.";
                } else {
                    echo "Agreement Not Updated. Please try again.";
                }
            } else {
                echo "Agreement Should be in PDF Format.";
            }
        } else {
            echo "Agreement Size should be between 100KB to 5MB.";
        }
    } else {
        echo "Inavalid Entry15";
    }
}