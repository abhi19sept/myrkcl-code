<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsWcdlearnerpdfreport.php';

$response = array();
$emp = new clsWcdlearnerpdfreport();

if ($_action == "GETDETAILS") {

    $response = $emp->GETDETAILS();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";   
    echo "<th>Organization Name</th>";    
    echo "<th >Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";			
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
			
            $_UploadDirectory3 = 'upload/wcdpdf/'.$_SESSION['User_LoginId'].'.pdf';
			
			echo "<td> <a href='".$_UploadDirectory3."' type='application/octet-stream' download='".$_SESSION['User_LoginId']."'.'pdf'>"
					. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='Download'/></a>"						
					. "</td>";
			
           
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
