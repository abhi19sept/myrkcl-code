<?php


include './commonFunction.php';
require 'BAL/clsNcrFeedbackUpdate.php';

$response = array();
$emp = new clsNcrFeedbackUpdate();


if ($_action == "ADD") {
    if (isset($_POST["reason"]) && !empty($_POST["reason"])) {
        $_Reason = $_POST["reason"];
        $_CenterCode=$_POST["cc"];
        $_VisitId=$_POST["id"];
        $_TheoryArea = $_POST["theory"];
        $_LabArea=$_POST["lab"];
        $_TotalArea=$_POST["total"];
        $response = $emp->Add($_Reason,$_CenterCode,$_VisitId,$_TheoryArea,$_LabArea,$_TotalArea);
        echo $response[0];
    }
}

if ($_action == "FILL") {
    $response = $emp->GetITGKCodes($_POST['values']);
    echo "<option value='' >Select Center Code</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['NCRFeedback_AOCode'] . ">" . $_Row['NCRFeedback_AOCode'] . "</option>";
    }
}

if ($_action == "FILLREASON") {
    $response = $emp->GetVisitCloseReason();
    echo "<option value='' >Select Reason</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['VisitClose_Reason'] . "'>" . $_Row['VisitClose_Reason'] . "</option>";
    }
}


if ($_action == "DETAILS") {
    $response = $emp->GetDatabyCode($_POST['cc']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {

        $_DataTable[$_i] = array("itgkcode" => $_Row['NCRFeedback_AOCode'],
            "feedbackid" => $_Row['NCRFeedback_Code'],
            "itgkname" => strtoupper($_Row['NCRFeedback_AOName']),
            "visitorid" => $_Row['NCRFeedback_User'],
            "spname" => strtoupper($_Row['NCRFeedback_SPName']),
            "areaseparate" => $_Row['NCRFeedback_Separate'],
            "throeyarea" => $_Row['NCRFeedback_TheoryArea'],
            "labarea" => $_Row['NCRFeedback_LABArea'],
            "totalarea" => $_Row['NCRFeedback_TotalArea']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}




if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetDetails();

    $_DataTable = "";

    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'>ITGK Code</th>";
    echo "<th style='30%'>Penalty Amount</th>";
    echo "<th style='20%'>Ref No</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['AddressPenalty_Center_Code'] . "</td>";
         echo "<td>" . $_Row['AddressPenalty_Amount'] . "</td>";
         echo "<td>" . $_Row['AddressPenalty_RefNo'] . "</td>";
//        echo "<td><a href='frmcountrymaster.php?code=" . $_Row['Country_Code'] . "&Mode=Edit'>"
//                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
//        . "<a href='frmcountrymaster.php?code=" . $_Row['Country_Code'] . "&Mode=Delete'>"
//                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
