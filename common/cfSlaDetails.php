<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsSlaDetails.php';

$response = array();
$emp = new clsSlaDetails();

    if ($_action == "DETAILS") {
		
	$response = $emp->GetITGK_SLA_Details($_POST['ExpDate']);
        $response2 = $emp->GetRenewalPenaltyAmount();
        $_Row2 = mysqli_fetch_array($response2[2]);
        //$_Row2['Renewal_Penalty_Amount']
	$_DataTable = "";
		
	echo "<div class='table-responsive'>";
	echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
	echo "<thead>";
	echo "<tr>";
	echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>ITGK-CODE</th>";
        echo "<th style='25%'>ITGK Name</th>";
//	echo "<th style='15%'>ITGK Creation Amount</th>";
        echo "<th style='15%'>ITGK SP Name</th>";
         echo "<th style='15%'>ITGK Area Type</th>";
         echo "<th style='15%'>ITGK Panchayat Samiti</th>";
         echo "<th style='15%'>ITGK Gram Panchayat</th>";
        echo "<th style='15%'>ITGK Created Date</th>";
        echo "<th style='5%'>ITGK Renewal Date</th>";
        echo "<th style='5%'>Renewal Expire Period</th>";
        echo "<th style='5%'>Renewal Eligiblity</th>";
	echo "<th style='5%'>Admission Count</th>";
        echo "<th style='5%'>Shortfall admission count as per area condition</th>";
        echo "<th style='5%'>Penalty  amount as per area condition</th>";
//        echo "<th style='5%'>Shortfall admission as per NCR Creation  Amount</th>";
//        echo "<th style='5%'>Penalty amount as per NCR creation amount</th>";
//	
//	
//	
//	echo "<th style='5%'>RSP Name</th>";
//	echo "<th style='10%'>Login Created Date</th>";
//        
//        echo "<th style='10%'>Final Approval Date</th>";
//        echo "<th style='10%'>Course Allocation Date</th>";
//	echo "<th style='10%'>ITGK Expire Date</th>";
//        echo "<th style='10%'>ITGK Address</th>";
//	echo "<th style='10%'>ITGK District</th>";
//	echo "<th style='10%'>ITGK Tehsil</th>";
//        
//        echo "<th style='10%'>IT-GK Area Type</th>";
//    
//        echo "<th style='10%'>Municipality Type: (Urban)/Panchayat Samiti (Rural)</th>";
//        echo "<th style='10%'>Municipality Name: (Urban)/Gram Panchayat (Rural)</th>";
//        echo "<th style='10%'>Ward No.: (Urban)/Village (Rural)</th>";
//    
//        echo "<th style='10%'>ITGK Pincode</th>";
//        echo "<th style='10%'>ITGK Type</th>";
//        echo "<th style='10%'>ITGK PaperLess</th>";
//        echo "<th style='10%'>Amount Paid</th>";
	echo "</tr>";
	echo "</thead>";
	echo "<tbody>";
	if($response[0] == 'Success') {
	$_Count = 1;
	while ($_Row = mysqli_fetch_array($response[2])) {
            
            $_CenterCode = $_Row['ITGKCODE'];
            //$_StartDate = $_Row['CourseITGK_UserCreatedDate'];
            $_EndDate = $_Row['CourseITGK_ExpireDate'];
            
          $_StartDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($_EndDate)) . " -13 months"));
          //$_StartDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($_StartDate1)) . " -30 days"));
            //die;
    
            $response1 = $emp->GetITGK_Admission_Details($_CenterCode, $_StartDate, $_EndDate);
            $_Row1 = mysqli_fetch_array($response1[2]);
//            if($_Row['Ncr_Transaction_Amount'] == '' || $_Row['Ncr_Transaction_Amount'] == '51000' || $_Row['Ncr_Transaction_Amount'] == '38000')
//            {
//            $_Shortfall = '25' - $_Row1['Admission_Count'];
//            } if ($_Row['Ncr_Transaction_Amount'] == '24000') {
//             $_Shortfall = '25' - $_Row1['Admission_Count'];    
//            }
//            else{
//              $_Shortfall = 'Amount not Available';   
//            }
//            $_Penalty = $_Shortfall * $_Row2['Renewal_Penalty_Amount'];
	echo "<tr class='odd gradeX'>";
	echo "<td>" . $_Count . "</td>";
	echo "<td>" . $_Row['ITGKCODE'] . "</td>";
        echo "<td>" . strtoupper($_Row['ITGK_Name'] ? $_Row['ITGK_Name'] : 'NA') . "</td>";
//	echo "<td>" . ($_Row['Ncr_Transaction_Amount'] ? $_Row['Ncr_Transaction_Amount'] : '51000') . "</td>";
        echo "<td>" . strtoupper($_Row['RSP_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_AreaType'] ? $_Row['Organization_AreaType'] : 'Area Type Not Available') . "</td>";
        
        if($_Row['CourseITGK_ExpireDate'] > '2019-08-01'){
        $_AdmissionRequired = '25';
        $_ShortfallAreaType = $_AdmissionRequired - $_Row1['Admission_Count'];
        if($_Row['Organization_AreaType'] == 'Urban'){
         echo "<td>NA</td>";
          echo "<td>NA</td>"; 
        }elseif ($_Row['Organization_AreaType'] == 'Rural') {
           echo "<td>" . strtoupper($_Row['Block_Name']) . "</td>";
          echo "<td>" . strtoupper($_Row['GP_Name']) . "</td>";
            } else{
              echo "<td>Area Type Not Available</td>";
          echo "<td>Area Type Not Available</td>";  
            }
        
    } else if ($_Row['CourseITGK_ExpireDate'] < '2019-08-01'){
        if($_Row['Organization_AreaType'] == 'Urban'){
         echo "<td>NA</td>";
          echo "<td>NA</td>"; 
          $_ShortfallAreaType = '50' - $_Row1['Admission_Count'];
        }elseif ($_Row['Organization_AreaType'] == 'Rural') {
           echo "<td>" . strtoupper($_Row['Block_Name']) . "</td>";
          echo "<td>" . strtoupper($_Row['GP_Name']) . "</td>";
          
          $panchayat = trim(str_replace(' ', '', $_Row['Block_Name']));
        $panchayat = preg_replace('/\s+/', ' ',$panchayat);
        $panchayat = str_replace('.', '', $panchayat);

        $gram = trim(str_replace(' ', '', $_Row['GP_Name']));
        $gram = preg_replace('/\s+/', ' ',$gram);
        $gram = str_replace('.', '', $gram);
          if($panchayat === $gram){
          $_ShortfallAreaType = '50' - $_Row1['Admission_Count'];
          }else{
          $_ShortfallAreaType = '25' - $_Row1['Admission_Count'];    
          }
            } else{
              echo "<td>Area Type Not Available</td>";
          echo "<td>Area Type Not Available</td>";  
          $_ShortfallAreaType = "Area Type Not Available";
            }
    }
        
         
        echo "<td>" . ($_Row['CourseITGK_UserCreatedDate'] ? $_Row['CourseITGK_UserCreatedDate'] : 'NA'). "</td>";
         echo "<td>" . ($_Row['CourseITGK_ExpireDate'] ? $_Row['CourseITGK_ExpireDate'] : 'NA') . "</td>";
         
         date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d h:i:s");
            $datetime1 = new DateTime($_Date);
$datetime2 = new DateTime($_Row['CourseITGK_ExpireDate']);
$interval = $datetime1->diff($datetime2);
//echo $interval->format('%y years %m months and %d days');

         echo "<td>" . ($interval->format('%y years %m months and %d days')) . "</td>";
//         if($_Row['CourseITGK_ExpireDate'] >= '2018-09-05'){
//             echo "<td>" . ((($interval->m)>'10' || ($interval->y)>'0') ? '<h6 style="color:red">Not Eligible for Renewal</h6>' : '<h6 style="color:green">Eligible for Renewal</h6>') . "</td>";
//        
//    } else if ($_Row['CourseITGK_ExpireDate'] < '2018-09-05'){
//        echo "<td>" . ((($interval->m)>'9' || ($interval->y)>'0') ? '<h6 style="color:red">Not Eligible for Renewal</h6>' : '<h6 style="color:green">Eligible for Renewal</h6>') . "</td>";
//        } 
         echo "<td>" . ((($interval->m)>'10' || ($interval->y)>'0') ? '<h6 style="color:red">Not Eligible for Renewal</h6>' : '<h6 style="color:green">Eligible for Renewal</h6>') . "</td>";
       echo "<td>" . $_Row1['Admission_Count'] . "</td>";  
        echo "<td>" . ($_ShortfallAreaType>'0' ? $_ShortfallAreaType : 'NA') . "</td>";
        $_PenaltyAreaType = $_ShortfallAreaType * $_Row2['Renewal_Penalty_Amount'];
        echo "<td>" . ($_PenaltyAreaType>'0' ? $_PenaltyAreaType : 'NA')  . "</td>";
//       echo "<td>" . ($_Shortfall>'0' ? $_Shortfall : 'NA') . "</td>"; 
//	echo "<td>" . ($_Penalty>'0' ? $_Penalty : 'NA')  . "</td>"; 
//	                    
//	echo "<td>" . strtoupper($_Row['RSP_Name']) . "</td>";            
//	echo "<td>" . date_format(date_create($_Row['CourseITGK_UserCreatedDate']), "d-m-Y") . "</td>"; 
//        if($_Row['User_PaperLess']=='Yes') {
//		 echo "<td>" . ($_Row['User_SPFinalApprovalDate'] ? date('d-m-Y', strtotime($_Row['User_SPFinalApprovalDate'])) : 'NA') . "</td>"; 
//		 }
//		 else {
//                 echo "<td>" . ($_Row['User_FinalApprovalDate'] ? date('d-m-Y', strtotime($_Row['User_FinalApprovalDate'])) : 'NA') . "</td>";
//		 }
//        if($_Row['User_PaperLess']=='Yes') {
//		 echo "<td>" . ($_Row['User_FinalApprovalDate'] ? date('d-m-Y', strtotime($_Row['User_FinalApprovalDate'])) : 'NA') . "</td>"; 
//		 }
//		 else {
//                 echo "<td>" . ($_Row['User_FinalApprovalDate'] ? date('d-m-Y', strtotime($_Row['User_FinalApprovalDate'])) : 'NA') . "</td>";
//		 }
//        
//        
//	echo "<td>" . date_format(date_create($_Row['CourseITGK_ExpireDate']), "d-m-Y") . "</td>";  
//        echo "<td>" . strtoupper($_Row['Organization_Address']) . "</td>";
//	echo "<td>" . strtoupper($_Row['District_Name']) . "</td>";   
//	echo "<td>" . strtoupper($_Row['Tehsil_Name']) . "</td>";
//        
//        if ($_Row['Organization_AreaType'] == '') {
//                echo "<td>" . "NA" . "</td>";
//            } else {
//                echo "<td>" . $_Row['Organization_AreaType'] . "</td>";
//            }
//        if ($_Row['Organization_AreaType'] == 'Urban') {
//                echo "<td>" . ($_Row['Municipality_Type_Name'] ? $_Row['Municipality_Type_Name'] : 'NA') . "</td>";
//                echo "<td>" . ($_Row['Municipality_Name'] ? $_Row['Municipality_Name'] : 'NA') . "</td>";
//                echo "<td>" . ($_Row['Ward_Name'] ? $_Row['Ward_Name'] : 'NA') . "</td>";
//            } elseif ($_Row['Organization_AreaType'] == 'Rural') {
//                echo "<td>" . ($_Row['Block_Name'] ? $_Row['Block_Name'] : 'NA') . "</td>";
//                echo "<td>" . ($_Row['GP_Name'] ? $_Row['GP_Name'] : 'NA') . "</td>";
//                echo "<td>" . ($_Row['Village_Name'] ? $_Row['Village_Name'] : 'NA') . "</td>";
//            } else {
//                echo "<td>" . "NA" . "</td>";
//                echo "<td>" . "NA" . "</td>";
//                echo "<td>" . "NA" . "</td>";
//            }
//        echo "<td>" . ($_Row['Organization_PinCode'] ? $_Row['Organization_PinCode'] : 'NA') . "</td>";    
//        echo "<td>" . strtoupper($_Row['User_Type']) . "</td>";
//        echo "<td>" . strtoupper($_Row['User_PaperLess']) . "</td>";
//        echo "<td>" . ($_Row['User_Type'] == 'New' ? $_Row['Ncr_Transaction_Amount'] : '51000') . "</td>";
	echo "</tr>";
	$_Count++;
	}
	echo "</tbody>";
	echo "</table>";
	echo "</div>";
	}
    }
//if ($_action == "ADD") {
////    print_r($_POST);
////    die;
//    $_UserPermissionArray = array();
//    $_Count = 0;
//    $_Counts = 0;
//    foreach ($_POST as $key => $value) {
//        if (substr($key, 0, 3) == 'chk') {
//            $_Count++;
//        }
//        $_UserPermissionArray[$_Counts] = array(
//            "Function" => (substr($key, 3)));
//        $_Counts++;
//        //print_r($_UserPermissionArray);
//    }
//
//    //$response=$emp->Update($_Count);
//    if ($_UserPermissionArray == "") {
//        echo "NoDetails";
//        return;
//    } else {
//        $response1 = $emp->Upload_SLA($_UserPermissionArray);
//    }
//
//    echo $response1[0];
//}

if ($_action == "ADD") {
//    print_r($_POST);
//    die;
    $_UserPermissionArray = array();
    $_Count = 0;
    $_Counts = 0;
    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 3) == 'chk') {
            $_Count++;
        }
        $_UserPermissionArray[$_Counts] = array(
            "Function" => (substr($key, 3, 8)),
            "yr" => (substr($key, 17, 4)));
        $_Counts++;
        //print_r($_UserPermissionArray);
        //$previous_year = $_POST['year'];
    }
//    print_r($_UserPermissionArray);
//    die;
    //$response=$emp->Update($_Count);
    if ($_UserPermissionArray == "") {
        echo "NoDetails";
        return;
    } else {
        $response1 = $emp->Upload_SLA($_UserPermissionArray);
    }

    echo $response1[0];
}
?>