<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsAdvanceFeePayment.php';
$response = array();
$emp = new clsAdvanceFeePayment();

if ($_action == "FILLAdvanceCourse") {
    $response = $emp->GetAdvanceCourse();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Courseitgk_Course'] . "</option>";
    }
}

if ($_action == "FILLAdvanceCourseName") {
    $response = $emp->GetAdvanceCourseName($_POST['values']);	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Ad_Course_Id'] . ">" . $_Row['Ad_Course_Name'] . "</option>";
    }
}

if ($_action == "FILLCourseCategory") {
    $response = $emp->GetAdvanceCourseCategory($_POST['values']);	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Ad_Category_Code'] . ">" . $_Row['Ad_Category_Name'] . "</option>";
    }
}

if ($_action == "ADD") { 
    if (empty($_POST["amounts"]) || !isset($_POST["AdmissionCodes"])) {
        echo "0";
    } else {
        $productinfo = 'RedHatFeePayment';
        $trnxId = $emp->AddPayTran($_POST, $productinfo);
        if ($_POST['gateway'] == 'razorpay' && !empty($trnxId) && $trnxId != 'TimeCapErr') {
            require("razorpay.php");
        } else {
            echo $trnxId;
        }
    }
}

if ($_action == "SHOWALL") {
    $response = $emp->GetAll($_POST['batch'], $_POST['course'], $_POST['paymode'], $_POST['coursepkg']);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father/Husband Name</th>";
    echo "<th style='12%'>D.O.B</th>";
    echo "<th style='10%'>Photo</th>";
    echo "<th style='10%'>Sign</th>";
    echo "<th style='10%'>Amount</th>";   
    echo "<th style='8%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $response1 = $emp->GetAdmissionFee($_POST['batch']);
    $_row1 = mysqli_fetch_array($response1[2]);
    $fee = ($_row1['RKCL_Share'] + $_row1['VMOU_Share']);
   
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {

            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";

            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="80" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="80" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="80" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="80" src="images/no_image.png"/>' . "</td>";
            }

            echo "<td>" . $fee . "</td>";

            if ($_Row['Admission_Payment_Status'] == '0') {
                echo "<td><input type='checkbox' id='chk" . $_Row['Admission_Code'] .
                "' name='AdmissionCodes[]' value='" . $_Row['Admission_Code'] . "'></input></td>";
            } elseif ($_Row['Admission_Payment_Status'] == '8') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='DD in process'>"
                . "</td>";
            } elseif ($_Row['Admission_Payment_Status'] == '1') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='Payment Confirmed'>"
                . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    } else {        
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "Fee") {    
    $response = $emp->GetAdmissionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);
    $RKCLshare = $_row['RKCL_Share'];
    $VMOUShare = $_row['VMOU_Share'];
    $totalshare = $RKCLshare + $VMOUShare;
    echo $totalshare;
}

if ($_action == 'showPayData') {
    $response = $emp->GetDatabyTrnxid($_actionvalue);
    if (!empty($response)) {
        $_DataTable = array();
        $_DataTable[] = mysqli_fetch_array($response[2]);
        echo json_encode($_DataTable);
    }
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
    //print_r($response);
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
	
	$key = "X2ZPKM";
	$salt = "8IaBELXB";
	
	//$salt = "eCwWELxi";
	//$key = 'gtKFFx';
	$command1 = "check_isDomestic";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	
	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);		
	$_DataTable = array();
    $_i = 0;
		$_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
				echo json_encode($_DataTable);		
 }

 if ($_action == "FinalValidateCard") {	
	$key = "X2ZPKM";
	$salt = "8IaBELXB";
			//$salt = "eCwWELxi";
	    	//$key = 'gtKFFx';
	$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	
	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);	 
	if($response == 'Invalid'){
		echo "1";
	}
	else if($response == 'valid'){
		echo "2";
	}
 }
 
function curlCall($wsUrl, $qs, $true){
   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
		if (curl_errno($c)) {
		  $c_error = curl_error($c);
		  if (empty($c_error)) {
			  $c_error = 'Some server error';
			}
			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);
		return $arr;
}

if ($_action == 'generatehash') {
    $posted = $_POST;

    if ($payment::PAY_MODE == 'Live') {
           
			$MERCHANT_KEY = "X2ZPKM";
			$SALT = "8IaBELXB";              
       
        $PAYU_BASE_URL = "https://secure.payu.in";
        $surl = 'https://myrkcl.com/frmadvancepaysuccess.php';
        $furl = 'https://myrkcl.com/frmadvancepayfailure.php';
    } else {
        $PAYU_BASE_URL = "https://test.payu.in";
        $SALT = "eCwWELxi"; // For Test Mode
        $MERCHANT_KEY = 'gtKFFx'; // For Test Mode
        $surl = 'http://localhost/myrkcl/frmadvancepaysuccess.php';
        $furl = 'http://localhost/myrkcl/frmadvancepayfailure.php';
    }

    $action = $PAYU_BASE_URL . '/_payment';

    $hash = '';
    $formError = '';
    $response = $emp->GetDatabyTrnxid($posted['txnid']);
    if (empty($response) || !isset($response[2]) || empty($posted['transaction']) || empty($posted['txnid']) || $posted['txnid'] != $posted['transaction']) {
        $formError = 1;
    } elseif (empty($posted['transaction']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['phone']) || empty($posted['productinfo']) || empty($posted['pg'])) {
            $formError = 2;
    } elseif (($posted['pg']=='NB') && empty($posted['bankcode'])) {
         $formError = 3; 
    } elseif ((($posted['pg'] == 'CC') || ($posted['pg'] == 'DC')) && (empty($posted['bankcode']) || empty($posted['ccnum']) || empty($posted['ccvv']) || empty($posted['ccexpmon']) || empty($posted['ccname']) || empty($posted['ccexpyr']))) {
         $formError = 4;
    } else {
        $trnxData = mysqli_fetch_array($response[2]);
        //print_r($trnxData);  
        //Sequence : "key|transaction|amount|productinfo|firstname|email|udf1|udf2|||||||||SALT";
        if ($trnxData['Pay_Tran_PG_Trnid'] == $posted['txnid'] && $trnxData['Pay_Tran_Amount'] == $posted['amount'] && $trnxData['Pay_Tran_ITGK'] == $posted['udf1']) {
            $_Response = $emp->updateTransactionStatus($posted['txnid']);
            if ($_Response[0] == Message::SuccessfullyUpdate) {
                $hash_string = $MERCHANT_KEY . '|' . $trnxData['Pay_Tran_PG_Trnid'] . '|' . $trnxData['Pay_Tran_Amount'] . '|' . $trnxData['Pay_Tran_ProdInfo'] . '|' . $trnxData['UserProfile_FirstName'] . '|' . $trnxData['UserProfile_Email'] . '|' . $trnxData['Pay_Tran_ITGK'] . '|' . $trnxData['Pay_Tran_RKCL_Trnid'] . '|' . $trnxData['Pay_Tran_Course'] . '|' . $trnxData['Pay_Tran_Batch'] . '|' . $trnxData['Pay_Tran_CoursePkg'] . '||||||' . $SALT;
                $hash = strtolower(hash('sha512', $hash_string));
                echo '<input type="hidden" name="key" value="' . $MERCHANT_KEY . '" />
                    <input type="hidden" name="hash" value="' . $hash . '"/>
                    <input type="hidden" name="surl" value="' . $surl . '"/>
                    <input type="hidden" name="furl" value="' . $furl . '"/>
                    <input type="hidden" name="action" id="action" value="' . $action . '"/>';
            }
        } else {
            $formError = 1;
        }
    }
    echo $formError;
}
?>	