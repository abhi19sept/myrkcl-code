<?php

/*
 * Created by SUNIL KUMAR BAINDARA
 * Date: 24-02-2020
 */


 /***
     * @function generateOTP : function to generate Adhaar OTP
     * @param int $aadhaar_no : The adhaar number of User
     * @param int $pinCode : Area pin code
     * @return array : Returns an array containing the status and transaction id
     ***/

    function generateOTP($aadhaar_no,$Learner_Code){
        /*** DO NOT MODIFY THE VALUES BELOW ***/
        //$licence_key = "MEmVkcpNLahCE-9skCRMK36S_ufQGPaCiNFAZ33o_ICd01JIE6IBLpU";
        //$licence_key = "MPjsZ77ep-Mu0gTYs5_1mOfkfozxJ-3Q2AGHyhhjkTOeg7JSFAolRqI";
		// $licence_key = "MMa5z-ryHS_tvB5t9Hmxyjbte7icRdddVAbDRL_WN1DXyUBLz8bB2-M";
		 //$licence_key ="MJqg1YjQL8GkseVac8FM2g1BUzLmiKN_I6gjETFFD27i6RGUYysHeKo"; //updaated on 30 August 2018//
		 $licence_key ="MAlCZADE4gHodMG6QJ5GRY7-NlR-7A18QUrh2zo-WspGIibbrm0YgOc"; //updaated on 27 August 2019//
		
        $tid = "public";
        //$subaua = "STGDOIT006";
        //$subaua = "STGDOIT238";
		 $subaua = "PRKCL22857"; // 27Aug2019 2.5
        $udc = "RKCL123";
        $ip = "127.0.0.1";
        $fdc = "NA";
        $idc = "NA";
        $macadd = "";
        $lot = "P";
        $rc = "Y";
        $pinCode='NA';

        /*** GENERATE AUTH XML BLOCK HERE (DO NOT MODIFY) ***/
        $auth_xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
		<authrequest uid="' . $aadhaar_no . '" subaua="' . $subaua . '" ip="' . $ip . '" fdc="' . $fdc . '" idc="' . $idc . '"  macadd="' . $macadd . '" lot="' . $lot . '" lov="'.$pinCode.'" lk="' . $licence_key . '" rc="' . $rc . '" ver="2.5"> 
			<otp />
		</authrequest>';

        /*** INITIATE CURL REQUEST (DO NOT MODIFY) ***/
        //$ch = curl_init("http://103.203.138.120/api/aua/otp/request/encr");
       // $ch = curl_init("https://api.sewadwaar.rajasthan.gov.in/app/live/api/aua/otp/request?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69");
	   $ch = curl_init("https://api.sewadwaar.rajasthan.gov.in/app/live/was/otp/request/prod?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $auth_xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Accept: application/xml",
            "Content-Type: application/xml",
            "appname: MYRKCL"
        ]);

        /*** EXECUTE CURL HERE ***/

        $result = curl_exec($ch);
        if ( ! $result) {
                print curl_errno($ch) .': '. curl_error($ch);
            }
        /*** CLOSING CURL HERE ***/

        curl_close($ch);

        /*** STORE CURL RESPONSE INTO XML ***/

        $xml = @simplexml_load_string($result);

        /*** CONVERT XML INTO JSON FORMAT ***/

        $json = json_encode($xml);

        /*** CONVERT JSON INTO ARRAY FORMAT ***/

        $array = json_decode($json,true);

        /*** RETURNING RESPONSE AND TRANSACTION ***/
      //echo "<pre>"; print_r($array);die;
       if($array["auth"]["@attributes"]["status"]=='y')
        {
           /*** ENCRIPTION THE VERSION 2 RESPONSE ***/
            $referenceID=$array["@attributes"]["UUID"];
            $Encrver2aadhaar = generateEncryptionversion2aadhaar($aadhaar_no,$referenceID);          
           //echo "<pre>"; print_r($Encrver2aadhaar);die;
            if( $Encrver2aadhaar['status']=='Y')//y
                {
                //$emp = new clsLinkAadhar();
                $empAadhar = new clsEncrAadhar();
                $EncAadharNumber=$Encrver2aadhaar['encdata'];
                $response = $empAadhar->InsertAadharEncrData($Learner_Code,$referenceID,$EncAadharNumber);
                //echo "<pre>"; print_r($response);die;
                if($response[0]=='Successfully Inserted')
                    {   /*/ here we again sening same status and txn number after successfully insert the encryption aadhar number in the table./*/
                        $responseArray = [
                                       "txn" => $array["auth"]["@attributes"]["txn"],
                                       "msg" => 'Successfully encrypt aadhar number',
                                       "status" => $array["auth"]["@attributes"]["status"]
                                   ];
                    }
                    else{
                        $responseArray = [
                                       "txn" => $array["auth"]["@attributes"]["txn"],
                                       "msg" => 'Allready exit encryption aadhar number',
                                       "status" => $array["auth"]["@attributes"]["status"]
                                   ];
                    }
                }
            else
                {
                //echo "<pre>"; print_r($Encrver2aadhaar);die;
                    $responseArray = [
                        //"error" => $Encrver2aadhaar["error"],
                        "error" => 'If you can not see the OTP box then you may click on submit button again.',
                        "status" => $Encrver2aadhaar["status"]
                    ];
                }

       }else{
           $responseArray = [
                "txn" => $array["auth"]["@attributes"]["txn"],
                "status" => $array["auth"]["@attributes"]["status"]
            ];
       }
        return $responseArray;
    }
 
   /***
     * @function generateOTP : function to generate Adhaar OTP
     * @param int $aadhaar_no : The adhaar number of User
     * @param int $pinCode : Area pin code
     * @return array : Returns an array containing the status and transaction id
     ***/

    function authenticateOTPbyEXC($aadhaar_no, $txnID, $otp){
        
         //$URL="http://192.168.49.45/UIDAIAPI/uidai/otp/auth/".$aadhaar_no."/".$otp."/".$txnID;
	//$URL="http://192.168.164.202/UIDAIAPI/uidai/otp/auth/".$aadhaar_no."/".$otp."/".$txnID;
	$URL="http://lms.rkcl.co.in/UIDAIAPI/uidai/otp/auth/".$aadhaar_no."/".$otp."/".$txnID;
        $ch = curl_init();  
        curl_setopt($ch,CURLOPT_URL,$URL);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $output=curl_exec($ch);
        curl_close($ch);       
        $array = json_decode($output,true);
        if($array["authresponse"]["auth"]["@status"]=='Y')
        {
            $responseArray = [
            "status" => $array["authresponse"]["auth"]["@status"],
            "Aadharno" => $array["authresponse"]["UidData"]["@UUID"],
            "LName" => $array["authresponse"]["UidData"]["pi"]["@name"],
            "Fname" => $array["authresponse"]["UidData"]["pa"]["@co"],
            "Gender" => $array["authresponse"]["UidData"]["pi"]["@gender"],
            "LDOB" => $array["authresponse"]["UidData"]["pi"]["@dob"],
            "District" => $array["authresponse"]["UidData"]["pa"]["@dist"],
            "Tehsil" => $array["authresponse"]["UidData"]["pa"]["@loc"],
            "Pincode" => $array["authresponse"]["UidData"]["pa"]["@pc"]
            ];
        }
        else{
            $responseArray = [
               "status" => $array["authresponse"]["auth"]["@status"],
               "message" => $array["authresponse"]["message"]
            ];   
        } 
        return $responseArray;       
        
    }   
    
    
    /***
     * @function generateEncryptionversion2aadhaar : function to ecryption the version 2 Adhaar OTP
     * @param int $aadhaar_no : The adhaar number of User
     * @param int $referenceID : Reference code that is coming at the time of otp genrate
     * @return array : Returns an array containing the status and transaction id
     ***/

    function generateEncryptionversion2aadhaar($aadhaar_no,$referenceID){
        
        /*** DO NOT MODIFY THE VALUES BELOW ***/
       	$subaua = "PRKCL22857"; // 27Aug2019 2.5
        
        /*** GENERATE AUTH XML BLOCK HERE (DO NOT MODIFY) ***/
        $auth_xml = '<AuthRequest uid="' . $aadhaar_no . '" flagType="A" subaua="' . $subaua . '" ver="2.5" UUID="'.$referenceID.'">
</AuthRequest>
';

        /*** INITIATE CURL REQUEST (DO NOT MODIFY) ***/
        $ch = curl_init("https://apitest.sewadwaar.rajasthan.gov.in/app/live/Aadhaar/Staging/encV2/doitAadhaar/encDec/demo/hsm/auth?client_id=7d547d4c-b546-4261-8b67-d1a5a292e2f2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $auth_xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Accept: application/xml",
            "Content-Type: application/xml",
            "appname: MYRKCL"
        ]);

        /*** EXECUTE CURL HERE ***/

        $result = curl_exec($ch);
        if ( ! $result) {
                print curl_errno($ch) .': '. curl_error($ch);
            }
        /*** CLOSING CURL HERE ***/

        curl_close($ch);

        /*** STORE CURL RESPONSE INTO XML ***/

        $xml = @simplexml_load_string($result);

        /*** CONVERT XML INTO JSON FORMAT ***/

        $json = json_encode($xml);

        /*** CONVERT JSON INTO ARRAY FORMAT ***/

        $array = json_decode($json,true);

        /*** RETURNING RESPONSE AND TRANSACTION ***/
        //echo "<pre>"; print_r($array);die;
//        $responseArray = [
//            "txn" => $array["auth"]["@attributes"]["txn"],
//            "status" => $array["auth"]["@attributes"]["status"]
//        ];
        if(array_key_exists('@attributes', $array)){
            return $array["@attributes"];
        }else{
            return $array['httpMessage'];
        }
        
    }  
    
     /***
     * @function generateEncryptionversion2aadhaar : function to ecryption the version 2 Adhaar OTP
     * @param int $encaadhaar_no : The encrypted aadhaar number of User
     * @param int $referenceID : Reference code that is coming at the time of otp genrate
     * @return array : Returns an array containing the status and transaction id
     ***/

    function Decryptionversion2aadhaar($encaadhaar_no,$referenceID){
        
        /*** DO NOT MODIFY THE VALUES BELOW ***/
       	$subaua = "PRKCL22857"; // 27Aug2019 2.5
        
        /*** GENERATE AUTH XML BLOCK HERE (DO NOT MODIFY) ***/
        $auth_xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<AuthRequest uid="' . $encaadhaar_no . '"  flagType="A" subaua="' . $subaua . '" ver="2.5" UUID="'.$referenceID.'" ></AuthRequest>';

        /*** INITIATE CURL REQUEST (DO NOT MODIFY) ***/
        $ch = curl_init("https://apitest.sewadwaar.rajasthan.gov.in/app/live/Aadhaar/Staging/decV2/doitAadhaar/encDec/demo/hsm/auth?client_id=7d547d4c-b546-4261-8b67-d1a5a292e2f2");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $auth_xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Accept: application/xml",
            "Content-Type: application/xml",
            "appname: MYRKCL"
        ]);

        /*** EXECUTE CURL HERE ***/

        $result = curl_exec($ch);
        if ( ! $result) {
                print curl_errno($ch) .': '. curl_error($ch);
            }
        /*** CLOSING CURL HERE ***/

        curl_close($ch);

        /*** STORE CURL RESPONSE INTO XML ***/

        $xml = @simplexml_load_string($result);

        /*** CONVERT XML INTO JSON FORMAT ***/

        $json = json_encode($xml);

        /*** CONVERT JSON INTO ARRAY FORMAT ***/

        $array = json_decode($json,true);

        /*** RETURNING RESPONSE AND TRANSACTION ***/
        //echo "<pre>"; print_r($array);die;
//        $responseArray = [
//            "txn" => $array["auth"]["@attributes"]["txn"],
//            "status" => $array["auth"]["@attributes"]["status"]
//        ];
        return $array["@attributes"];
    } 
    
    
     /***
     * @function EncrAadharDataByLCode : function to ecryption the Adhaar by learner code
     * @param int $learner_code : The encrypted aadhaar number of User
     * @return array : Returns an array containing the status and transaction id
     ***/

    function EncrAadharDataByLCode($lcode){
        
            $empAadhar = new clsEncrAadhar();
            $response = $empAadhar->GetEncrAadharDataByLCode($lcode);// get encr aadhar detail
            if($response[0]=='Success')
                {
                    $EncrAadharData = mysqli_fetch_assoc($response[2]);
                    $referenceID=$EncrAadharData['UUIDReferenceID'];
                    $encaadhaar_no=$EncrAadharData['EncAadharNumber'];                    
                    $Decryptionversion2aadhaar=Decryptionversion2aadhaar($encaadhaar_no,$referenceID);
                    if($Decryptionversion2aadhaar['status']=='Y')
                    {
                       $learner_aadhar=$Decryptionversion2aadhaar['uid'];                       
                    }
                    else
                    {
                        $learner_aadhar='Error In Decryption';                        
                    }              
                }
            else
                {
                    $learner_aadhar='No Record Found';
                }
         return $learner_aadhar;
    }    
    
  