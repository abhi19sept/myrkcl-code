<?php

/*
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsApplyLuckyDrawApplicationReport.php';

$response = array();
$emp = new clsApplyLuckyDrawApplicationReport();

if ($_action == "GETDATA") {

    $response = $emp->GetDataAll($_POST['course'], $_POST['batch']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    //echo "<th>CenterCode</th>";
    // echo "<th>Course</th>";
    // echo "<th>Batch</th>";
    echo "<th>IT-GK Code</th>";
	echo "<th >Total Admission Count</th>";
    echo "<th >Lucky Draw Application Count</th>";
    // echo "<th>Count</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;
	$_Totaleconfirm = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            // echo "<td>" . $_Row['Course'] . "</td>";
            //  echo "<td>" . $_Row['Batch'] . "</td>";
            echo "<td>" . $_Row['RoleName'] . "</td>";
			//echo "<td><a href='frmAdmissionList.php?course=" . $_Row['Course'] . "&batch=" . $_Row['Batch'] . "&rolecode=" . $_Row['RoleCode'] . "&mode=ShowUpload' target='_blank'>"
           // . "" . $_Row['uploadcount'] . "</a></td>";
            echo "<td><button type='button' class='updcount' course='".$_Row['Course']."' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowUpload' value='" . $_Row['uploadcount'] . "'>" . $_Row['uploadcount'] . "</button></td>";
             echo "<td><button type='button' class='updcount' course='".$_Row['Course']."' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowConfirm' value='" . $_Row['applycount'] . "'>" . $_Row['applycount'] . "</button></td>";
           // echo "<td><a href='frmAdmissionList.php?course=" . $_Row['Course'] . "&batch=" . $_Row['Batch'] . "&rolecode=" . $_Row['RoleCode'] . "&mode=ShowConfirm' target='_blank'>"
           // . "" . $_Row['confirmcount'] . "</a></td>";
            echo "</tr>";
            $_Total = $_Total + $_Row['uploadcount'];
			$_Totaleconfirm = $_Totaleconfirm + $_Row['applycount'];
            $_Count++;
        }

        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
        echo "<th >TotalCount </th>";
        if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 4){
                echo "<th>";
                 echo  $_Total;
                // echo "<button type='button' class='totalupdcount' course='".$_POST['course']."' batch='" . $_POST['batch'] . "' rolecode='" . $_SESSION['User_UserRoll'] . "' mode='ShowUpload' >" . $_Total . "</button>";
                echo "</th>";
            }else{
                echo "<th>";
               echo "$_Total";
                echo "</th>";
            }
        if($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 8 || $_SESSION['User_UserRoll'] == 4){
                echo "<th>";
                echo "<button type='button' class='totalupdcount' course='".$_POST['course']."' batch='" . $_POST['batch'] . "' rolecode='" . $_SESSION['User_UserRoll'] . "' mode='ShowConfirm' >" . $_Totaleconfirm . "</button>";
                echo "</th>";
            }else{
                echo "<th>";
                echo "$_Totaleconfirm";
                echo "</th>";
            }
		
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
if ($_action == "GETLEARNERLISTTOTAL") {

    $response = $emp->GetLearnerListSuperadmin($_POST['course'], $_POST['batch'], $_POST['rolecode'], $_POST['mode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
	echo "<th >Mobile NO</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
         echo "<th>Show Document</th>";
          echo "<th>SP Code</th>";
         echo "<th>SP Name</th>";
         echo "<th>ITGK District</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
			echo "<td>" . $_Row['Admission_Mobile'] . "</td>";

            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }
            echo "<td align='center'><a href='/upload/admission_fee_waiver_scheme/" . $_Row['admitcardfilename'] . "' target='_blank'>"
        . "<img src='images/dwnpdf.png' alt='Edit' width='35px' height='35px'  align='center'/></a> </td> ";
            echo "<td>" . $_Row['RSP_Code'] . "</td>";
            echo "<td>" . strtoupper($_Row['RSP_Name']) . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetLearnerList($_POST['course'], $_POST['batch'], $_POST['rolecode'], $_POST['mode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
	echo "<th >Mobile NO</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
     echo "<th>Show Document</th>";
             echo "<th>SP Code</th>";
        echo "<th>SP Name</th>";
        echo "<th>ITGK District</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
			echo "<td>" . $_Row['Admission_Mobile'] . "</td>";

            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }
            echo "<td align='center'><a href='/upload/admission_fee_waiver_scheme/" . $_Row['admitcardfilename'] . "' target='_blank'>"
        . "<img src='images/dwnpdf.png' alt='Edit' width='35px' height='35px'  align='center'/></a> </td> ";
            echo "<td>" . $_Row['RSP_Code'] . "</td>";
            echo "<td>" . strtoupper($_Row['RSP_Name']) . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETDATAITGK") {
    $response = $emp->GetLearnerListITGK($_POST['course'], $_POST['batch'], $_POST['rolecode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father/Husband Name</th>";
    echo "<th>Learner Mobile</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
	echo "<th>Payment Status</th>";
         echo "<th>Show Document</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";

            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }
			 if ($_Row['Admission_Payment_Status'] == "1") {
               
                echo "<td> Payment Confirmed </td>";
            } elseif ($_Row['Admission_Payment_Status'] == "0")  {
                echo "<td> Payment Not Confirmed </td>";
            }
			elseif ($_Row['Admission_Payment_Status'] == "8")  {
                echo "<td> Confirmation Pending at RKCL </td>";
            }
            echo "<td align='center'><a href='/upload/admission_fee_waiver_scheme/" . $_Row['admitcardfilename'] . "' target='_blank'>"
        . "<img src='images/dwnpdf.png' alt='Edit' width='35px' height='35px'  align='center'/></a> </td> ";

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
