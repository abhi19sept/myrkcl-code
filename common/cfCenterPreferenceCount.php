<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsCenterPreferenceCount.php';

$response = array();
$emp = new clsCenterPreferenceCount();

if ($_action == "GETDATA") {
    //echo "Show";
		$_pref = $_POST['pref'];
		if($_POST['course'] !=''){
		if($_POST['batch'] !='') {
		  if($_pref == '1') {
			  $response = $emp->GetAllPref1($_POST['course'],$_POST['batch']);
			  
			   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
				echo "<th>Applicant Id</th>";
				echo "<th>Center Preference Count</th>";
				
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;
					$_Total = 0;
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr class='odd gradeX'>";
					echo "<td>" . $_Count . "</td>";						
					echo "<td>" . $_Row['OasisAdmissionITGKCode'] . "</td>";
					if($_SESSION['User_UserRoll']=='14' || $_SESSION['User_UserRoll']=='7'){
							echo "<td>" . $_Row['pref'] . "</td>";
						}
						else{						
					echo "<td><a href='frmLearnerPreferenceList.php?batch=" . $_POST['batch'] . "&itgkcode=" . $_Row['OasisAdmissionITGKCode'] . "&mode=one' target='_blank'>"
							. "" . $_Row['pref'] . "</a></td>";
				}
						echo "</tr>";
						$_Total = $_Total + $_Row['pref'];
					$_Count++;				
				}
				 echo "</tbody>";
				echo "<tfoot>";
						echo "<tr>";
						echo "<th >  </th>";
						echo "<th >Total Count </th>";
						echo "<th>";
						echo "$_Total";
						echo "</th>";
						
						echo "</tr>";
						echo "</tfoot>";
				 echo "</table>";
				 echo "</div>";
		  }
		   else if($_pref == '2') {
			  $response = $emp->GetAllPref2($_POST['course'],$_POST['batch']);
			   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
				echo "<th>Applicant Id</th>";
				echo "<th>Center Preference Count</th>";
				
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;
					$_Total = 0;
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr class='odd gradeX'>";
					echo "<td>" . $_Count . "</td>";
						if($_Row['OasisAdmissionITGKCode'] == '0') {
					echo "<td>" . 'No Preference' . "</td>";
						}
						else {
					echo "<td>" . $_Row['OasisAdmissionITGKCode'] . "</td>";
						}
						if($_SESSION['User_UserRoll']=='14' || $_SESSION['User_UserRoll']=='7'){
							echo "<td>" . $_Row['pref'] . "</td>";
						}
						else{					
					echo "<td><a href='frmLearnerPreferenceList.php?batch=" . $_POST['batch'] . "&itgkcode=" . $_Row['OasisAdmissionITGKCode'] . "&mode=two' target='_blank'>"
							. "" . $_Row['pref'] . "</a></td>";
					}
					echo "</tr>";
					$_Total = $_Total + $_Row['pref'];
					$_Count++;					
				}
				echo "</tbody>";
					echo "<tfoot>";
						echo "<tr>";
						echo "<th >  </th>";
						echo "<th >Total Count </th>";
						echo "<th>";
						echo "$_Total";
						echo "</th>";
						
						echo "</tr>";
						echo "</tfoot>";
				echo "</table>";
				echo "</div>";
		  }		 
		   else {
			   echo "0";			   
		   }
		   }
		  else {
			  echo "1";
		  }
		}
		  else {
			  echo "2";
		  }
   
}


if ($_action == "FillAdmissionCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetLearnerList($_POST['mode'], $_POST['batch'], $_POST['rolecode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
	echo "<th >D.O.B.</th>";
	echo "<th >Mobile No.</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_POST['rolecode'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_Fname'] . "</td>";
			echo "<td>" . $_Row['Oasis_Admission_DOB'] . "</td>";
			echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
           if ($_Row['Oasis_Admission_Photo'] != "") {
                $image = $_Row['Oasis_Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="40" src="upload/oasis_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="40" src="images/user.png"/>' . "</td>";
            }
            if ($_Row['Oasis_Admission_Sign'] != "") {
                $sign = $_Row['Oasis_Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/oasis_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/sign.jpg"/>' . "</td>";
            } 

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}



