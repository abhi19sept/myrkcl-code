<?php

/*
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsCenterActiveTerminate.php';

$response = array();
$emp = new clsCenterActiveTerminate();



if ($_action == "DETAILS") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("orgname" => $_Row['ITGK_Name'],
                "itgkcode" => $_Row['ITGKCODE'],
                "districtname" => $_Row['District_Name'],
                "tehsilname" => $_Row['Tehsil_Name'],
                "txtSPname" => $_Row['RSP_Name'],
                "txtActiveStatus" => $_Row['User_Active_Status'],
                "txtActiveDate" => $_Row['User_Active_Timestamp']);
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "You are not Auhorized to use this functionality on MYRKCL.";
    }
}

if ($_action == "UPDATE") {
    //print_r($_POST);
    if (isset($_POST["ddlActiveStatus"]) && !empty($_POST["ddlActiveStatus"])) {
        if (isset($_POST["itgkcode"]) && !empty($_POST["itgkcode"])) {

            $_ddlActiveStatus = $_POST["ddlActiveStatus"];
            $_ItgkCode = $_POST["itgkcode"];

            $response = $emp->Update($_ddlActiveStatus, $_ItgkCode);
            echo $response[0];
        } else {
            echo "Sommething Went Wrong. Reload the page  and try again.";
        }
    } else {
        echo "Please select Tehsil";
    }
}
