<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsEventMaster.php';

$response = array();
$emp = new clsEventMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        $_EventName = $_POST["name"];
       
        $_Status=$_POST["status"];

        $response = $emp->Add($_EventName,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_EventName = $_POST["name"];
       
        $_Status=$_POST["status"];
        $_Code=$_POST['code'];
        $response = $emp->Update($_Code,$_EventName,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("Event_Id" => $_Row['Event_Id'],
            "Event_Name" => $_Row['Event_Name'],
            
            "Event_Status"=>$_Row['Event_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();   

      echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='30%'>Name</th>";
    echo "<th style='55%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Event_Name'] . "</td>";
        echo "<td>" . $_Row['Event_Status'] . "</td>";
        echo "<td><a href='frmEventMaster.php?code=" . $_Row['Event_Id'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmEventMaster.php?code=" . $_Row['Event_Id'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	 echo "</div>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}