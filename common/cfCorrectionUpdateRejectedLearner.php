<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCorrectionUpdateRejectedLearner.php';

$response = array();
$emp = new clsCorrectionUpdateRejectedLearner();

	if ($_action == "ShowDetails")
	{
        $response = $emp->GetAll();
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>S No.</th>";
		echo "<th>ITGK Code</th>";
		echo "<th>Learner Code</th>";
		echo "<th>Learner Name</th>";
		echo "<th>Father Name</th>";
		echo "<th>D.O.B</th>";
		echo "<th>Correction Id</th>";
		echo "<th>Application For</th>";
		echo "<th>Exam Event</th>";
		echo "<th>Rejection Date</th>";
		echo "<th>Reason</th>";
		echo "<th>Marks</th>";						
		echo "<th>Action</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		
			$_Count = 1;	
			while ($_Row = mysqli_fetch_array($response[2])) {
			echo "<tr>";
			echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['Correction_ITGK_Code'] . "</td>";
			echo "<td>" . $_Row['lcode'] . "</td>";
			echo "<td>" . strtoupper($_Row['cfname']) . "</td>";
			echo "<td>" . strtoupper($_Row['cfaname']) . "</td>";
			echo "<td>" . $_Row['dob'] . "</td>"; 
			echo "<td>" . $_Row['cid'] . "</td>"; 
			echo "<td>" . strtoupper($_Row['applicationfor']) . "</td>"; 
			echo "<td>" . strtoupper($_Row['exameventname']) . "</td>"; 
			echo "<td>" . $_Row['form_process_date'] . "</td>"; 
			echo "<td>" . strtoupper($_Row['remarks']) . "</td>"; 
			  
			 
			echo "<td>" . $_Row['totalmarks'] . "</td>";
			
			echo "<td> ". "<input type='button' data-toggle='modal' data-target='#Update' id='".$_Row['cid']."' 
						name='Edit' id='Edit' class='btn btn-primary Update' value='Edit'/>"
				. "</td>";					
			
		
		
			echo "</tr>";
			$_Count++;
		}
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "UpdateLearnerStatus") {
		$_Cid = $_POST["cid"];		
		$_Reason = $_POST["reason"];		
		$_Delv_Date = $_POST["delvdate"];		
		$response = $emp->UpdateLearnerProcessStatus($_Cid, $_Reason, $_Delv_Date);
		echo $response[0];
	
}

if ($_action == "GetExistingDetails") {
	global $_ObjFTPConnection;
    $response = $emp->GetExistingLearnerDetails($_POST['cid']);
    $_DataTable = array();
    $_i = 0;
    if (mysqli_num_rows($response[2])) {
        while ($_Row = mysqli_fetch_array($response[2])) {
			$details= $_ObjFTPConnection->ftpdetails();
			
			$photopath = '/correction_photo/';
			$photoid = $_Row['photo'];
			
			
			
			$ftpaddress = $_ObjFTPConnection->ftpPathIp();			
					$photodoc = $ftpaddress.'correction_photo/' . '/' . $photoid;
					
					$ch = 	curl_init($photodoc);
						curl_setopt($ch, CURLOPT_NOBODY, true);
						curl_setopt($ch,CURLOPT_TIMEOUT,5000);
						curl_exec($ch);
						 $responseCodePhoto = curl_getinfo($ch, CURLINFO_HTTP_CODE);
						curl_close($ch);
					if ($responseCodePhoto == 200){
						$photoimage =  file_get_contents($details.$photopath.$photoid);
			$imageData = base64_encode($photoimage);
			$showimage =  "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='80' height='110'/>";
					}
					else{
						$showimage='Photo Not Available';
					}
            $_DataTable[$_i] = array("lcode" => $_Row['lcode'],
                "cfname" => $_Row['cfname'],
                "cfaname" => $_Row['cfaname'],
                "mobile" => $_Row['mobile'],
				"photo" => $showimage,
                "photoad" => $_Row['photo']);
            $_i = $_i + 1;
        }
        echo json_encode($_DataTable);
    }
}

if ($_action == "Update") {
	if (isset($_POST["txtLCorrectName"]) && !empty($_POST["txtLCorrectName"]) && 
			!empty($_POST["txtFCorrectName"])){
				$_LearnerName = $_POST["txtLCorrectName"];				  
				$_FatherName = $_POST["txtFCorrectName"];
				$_LearnerCode = $_POST["txtLCode"];
				$parentid = $_POST["txtLCode"];
				$_Mobile = $_POST["txtMobile"];
				$_Cid = $_POST["txtcid"];
				
				date_default_timezone_set("Asia/Kolkata");
				$dtstamp = date("Ymd_His");
				
				if (!empty($_FILES['filePhoto']['name'])){
					$_UploadDirectory1 = '/correction_photo';
					
						if ($_FILES["filePhoto"]["name"] != '') {
									$imag = $_FILES["filePhoto"]["name"];
									$imageinfo = pathinfo($_FILES["filePhoto"]["name"]);
									if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
										$error_msg = "Image must be in either PNG or JPG or JPEG Format";
										$flag = 0;
									} else {
										if (file_exists("$_UploadDirectory1/" . $parentid . '_photo' . '_' . $dtstamp . '.' . substr($imag, -3))) {
											$_FILES["filePhoto"]["name"] . "already exists";
										} else {
											ftpUploadFile($_UploadDirectory1,$parentid . '_photo' . '_' . $dtstamp . '.jpg',$_FILES["filePhoto"]["tmp_name"]);
											$_SESSION['CorrectionPhoto'] = $parentid . '_photo' . '_' . $dtstamp . '.jpg';
										
											}
									}
							}
					$response = $emp->update($_Cid, $_LearnerCode, $_LearnerName, $_FatherName, $_Mobile, $dtstamp);
					echo $response[0];
							
				}
				else{
						echo "Please upload learner photo.";
					}
			}
}
