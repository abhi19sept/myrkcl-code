<?php

/*
 * author yogendra

 */
include './commonFunction.php';
require 'BAL/clssurveyresultapp.php';

$response = array();
$emp = new clssurveyresultapp();


if ($_action == "FILLSurvey") {
    $response = $emp->GetSurvey($_actionvalue);
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Survey_id'] . ">" . $_Row['Survey_Name'] . "</option>";
    }
}


if ($_action == "RESULT") {


    $response = $emp->GetResult($_actionvalue);
    //echo $_actionvalue;

    $_DataTable = "";
    echo "<div id='responsive'>";
    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%' style='margin-top:30px'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'> Survey Centercode</th>";
	echo "<th style='35%'> Organisation Name</th>";
	
    echo "<th style='35%'> Survey Question</th>";
	echo "<th style='30%'>Survey Answers</th>";
   
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['survey_centercode'] . "</td>";
		echo "<td>" . $_Row['Organization_Name'] . "</td>";
       
        echo "<td>" . $_Row['survey_question'] . "</td>";
        echo "<td>" . $_Row['survey_answers'] . "</td>";

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div >";
}
?>

