<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsLotteryResultReport.php';

$response = array();
$emp = new clsLotteryResultReport();

if ($_action == "FillAdmissionCourse") {
    $response = $emp->GetAdmissionCourse();	
    echo "<option value=''>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLAdmissionBatchcode") {
    $response = $emp->GetAdmissionBatchcode($_POST['values']);
    echo "<option value='' selected='selected'>Select Batch</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "GETDATA") {
    $response = $emp->GetDataAll($_POST['course'], $_POST['batch']);
    $_DataTable = "";

		echo "<div class='table-responsive' style='margin-top:10px'>";
   
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0'
			class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th style='15%'>S No.</th>";
		echo "<th style='15%'>ITGK-Code</th>";
		echo "<th style='15%'>LearnerCode</th>";
		echo "<th style='15%'>Learner Name</th>";
		echo "<th style='15%'>Father/Husband Name</th>";
		echo "<th style='15%'>D.O.B.</th>";
		echo "<th style='15%'>Discount Category</th>";
		//echo "<th style='15%'>Discount Type </th>";
		echo "<th style='15%'>IT-GK District</th>";
		//echo "<th style='15%'>IT-GK Name</th>";
		echo "<th style='15%'>SP Name</th>";
		echo "<th style='15%'>SP Code</th>";
                if($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_Code'] == '4257'
					|| $_SESSION['User_Code'] == '4258' || $_SESSION['User_Code'] == '9063'){
                    echo "<th style='15%'>Payment Status</th>";
                    echo "<th style='15%'>Remark</th>";
                    echo "<th style='15%'>Document</th>";
                }
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$_Count = 1;
		$co = mysqli_num_rows($response[2]);
		if ($co) {
			while ($_Row = mysqli_fetch_array($response[2])){
				echo "<tr class='odd gradeX'>";
				echo "<td>" . $_Count . "</td>";
				echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
				echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
				echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
				echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
				echo "<td>" . $_Row['Admission_DOB'] . "</td>";
				echo "<td>" . $_Row['Discount'] . "</td>";
				//echo "<td>" . $_Row['lottery_scheme_reservation_type'] . "</td>";
				echo "<td>" . $_Row['District_Name'] . "</td>";				
				//echo "<td>" . $_Row['ITGK_Name'] . "</td>";				
				echo "<td>" . $_Row['RSP_Name'] . "</td>";				
				echo "<td>" . $_Row['RSP_Code'] . "</td>";
					if($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_Code'] == '4257'
						|| $_SESSION['User_Code'] == '4258' || $_SESSION['User_Code'] == '9063'){
						$status ="";
						$updremark ="";
						if($_Row['adm_lcode']==""){
							$status = "Admission Deleted";
						}
						else{
								if($_Row['Admission_Payment_Status'] == '1')
								{ 
									$status = "Confirm"; 
								}
								else
								{ 
									$status = "Not Confirm";
								}
						}
					
						echo "<td>" . $status . "</td>";
							if($_Row['remark']==""){
								$updremark="NA";
							}else{
								$updremark=$_Row['remark'];
							}
						echo "<td>" . $updremark . "</td>";				
						echo "<td><button type='button' id='".$_Row['Admission_LearnerCode']."' class='btn btn-primary Fun_ViewDocument'>View</button></td>";				
					}
				echo "</tr>";
				$_Count++;
			}
			echo "</tbody>";
			echo "</table>";
			echo "</div>";
		}
}

if ($_POST["action"] == "ADDREMARK") {
    $lcode = $_POST["lcode"];
    $remarktext = $_POST["remarktext"];
    $response = $emp->AddRemark($lcode,$remarktext);
    echo $response[0]; 
}
if ($_POST["action"] == "CheckRemark") {
    $lcode = $_POST["lcode"];
    $response = $emp->CheckRemark($lcode);
    if($response[0] == "Success"){
        $_Row = mysqli_fetch_array($response[2]);
        $remark = $_Row["remark"];
        if( $remark == "" || $remark == null ){
            echo "1";
        }else{
            echo "0";
        }
    }else{
        echo "0";
    } 
}