<?php

include './commonFunction.php';
require 'BAL/clsfinalresultmanage.php';

$response = array();
$emp = new clsDigitalCertificateStatus();


if ($_action == "FILLEvent") {
    $response = $emp->GetEvent();
    echo "<option value=''>-- Select Event --</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['eventname'] . "</option>";
    }
}

if ($_action == "ViewEventData") {
    $eventid = $_POST["event"];
    $response = $emp->ViewEventData($eventid);
    $eventbtn = "";
    while ($_Row = mysqli_fetch_array($response[2])) {
        $eventbtn = $_Row['exameventnameID'];
        echo "<div class='container'>
                <div class='container'>
                    <div class='col-md-2 form-group'> 
                        <label for='course'>Exam Event ID<span class='star'>*</span></label>
                        <input type='text' class='form-control' disabled='disabled' value='".$_Row['exameventnameID']."'>
                    </div> 
                    <div class='col-md-3 form-group'> 
                        <label for='course'>Exam Event Name<span class='star'>*</span></label>
                        <input type='text' class='form-control' disabled='disabled' value='".$_Row['exam_event_name']."'>
                    </div> 
                    <div class='col-md-2 form-group'> 
                        <label for='course'>Exam Date<span class='star'>*</span></label>
                        <input type='text' class='form-control' disabled='disabled' value='".$_Row['exam_date']."'>
                        
                    </div> 
                    <div class='col-md-2 form-group'> 
                        <label for='course'>Exam Held Date<span class='star'>*</span></label>
                        <input type='text' class='form-control' disabled='disabled' value='".$_Row['exam_held_date']."'>
                        
                    </div> 
                    <div class='col-md-2 form-group'> 
                        <label for='course'>Result Date<span class='star'>*</span></label>
                        <input type='text' class='form-control' disabled='disabled' value='".$_Row['result_date']."'>
                        
                    </div> 
                </div>
            </div>";
    }
     echo "<div class='container' id='hidearea'>
                <div class='col-md-4 form-group'>     
                    <input type='button' name='btnupdatefinaldata' id='".$eventbtn."' class='btn btn-primary btnupdatefinaldata' value='Update'/>    
                </div>
            </div>";
}
if ($_action == "GETDATAEDIT") {
    $eventid = $_POST["event_id"];
    $response = $emp->ViewEventData($eventid);
    $_Row = mysqli_fetch_array($response[2]);
    echo "<div class='container'>
                <div class='container'>
                    <div class='col-md-2 form-group'> 
                        <label for='course'>Exam Event ID<span class='star'>*</span></label>
                        <input type='text' id='txtexameventnameID' name='txtexameventnameID' class='form-control' disabled='disabled' value='".$_Row['exameventnameID']."'>
                    </div> 
                    <div class='col-md-3 form-group'> 
                        <label for='course'>Exam Event Name<span class='star'>*</span></label>
                        <input type='text' id='txtexam_event_name' name='txtexam_event_name' class='form-control' disabled='disabled' value='".$_Row['exam_event_name']."'>
                    </div> 
                    <div class='col-md-2 form-group'> 
                        <label for='course'>Exam Date<span class='star'>*</span></label>
                        <input type='text' id='txtexam_date' name='txtexam_date' class='form-control' value='".$_Row['exam_date']."' required='required'>
                        <label for='course' style='color:red;' id='errorexamdate'><span class='star'></span></label>
                    </div> 
                    <div class='col-md-2 form-group'> 
                        <label for='course'>Exam Held Date<span class='star'>*</span></label>
                        <input type='text' id='txtexam_held_date' name='txtexam_held_date' class='form-control' value='".$_Row['exam_held_date']."' required='required'>
                        <label for='course' style='color:red;' id='errorhelddate'> <span class='star'></span></label>
                    </div> 
                    <div class='col-md-2 form-group'> 
                        <label for='course'>Result Date<span class='star'>*</span></label>
                        <input type='text' id='txtresult_date' name='txtresult_date' class='form-control' value='".$_Row['result_date']."' required='required'>
                        <label for='course' style='color:red;' id='errorresultdate'><span class='star'></span></label>
                    </div> 
                </div>
            </div>";
    echo "<div class='container'>
                <div class='col-md-4 form-group'>     
                    <input type='submit' name='btnSubmitUpdate' id='btnSubmitUpdate' class='btn btn-primary' value='Update'/>    
                </div>
            </div>";
}

if ($_action == "UPDATEDATES") {
    $event = $_POST["event"];
    $txtexam_date = $_POST["txtexam_date"];
    $txtexam_held_date = $_POST["txtexam_held_date"];
    $txtresult_date = $_POST["txtresult_date"];
    $response = $emp->UpdateFinalDates($event, $txtexam_date, $txtexam_held_date, $txtresult_date);
    if ($response[0] == "Successfully Updated") {
        echo "success";
    }else{
        echo "error";
    } 
}