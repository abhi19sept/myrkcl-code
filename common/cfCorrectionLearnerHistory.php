<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCorrectionLearnerHistory.php';

$response = array();
$emp = new clsCorrectionLearnerHistory();

	if ($_action == "ShowDetails")
	{
        $response = $emp->GetAll($_POST['lcode']);
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>S No.</th>";
		echo "<th>Learner Code</th>";
		echo "<th>ITGK Code</th>";
		echo "<th>Learner Name</th>";
		echo "<th>Father Name</th>";
		echo "<th>Correction ID</th>";
		echo "<th>D.O.B</th>";
		echo "<th>Exam Event</th>";
		echo "<th>Marks</th>";
		echo "<th>Payment Status</th>";
		echo "<th>Application For</th>";
		echo "<th>Application Date</th>";		
		echo "<th>Form Process Date</th>";		
		echo "<th>Form Delivered Date</th>";		
		echo "<th>Application Status</th>";
		echo "<th>Lot</th>";				
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		
			$_Count = 1;	
			while ($_Row = mysqli_fetch_array($response[2])) {
			echo "<tr>";
			echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['lcode'] . "</td>";
			echo "<td>" . $_Row['Correction_ITGK_Code'] . "</td>";
			echo "<td>" . $_Row['name'] . "</td>";
			echo "<td>" . $_Row['fname'] . "</td>";
			echo "<td>" . $_Row['cid'] . "</td>";
			echo "<td>" . $_Row['dob'] . "</td>"; 
			echo "<td>" . $_Row['exameventname'] . "</td>";
			echo "<td>" . $_Row['totalmarks'] . "</td>";
			$status ="";
			  if($_Row['Correction_Payment_Status'] == '0'){
				  $status = 'Pending';				  
			  }
			  else {
				  $status = 'Confirmed';				  
			  }
			echo "<td>" . $status . "</td>";
			echo "<td>" . strtoupper($_Row['applicationfor']) . "</td>";
			echo "<td>" . date("d-m-Y",strtotime($_Row['applicationdate'])) . "</td>";
				if($_Row['form_process_date']==''){
					echo "<td>NA</td>";
				}
				else{
					echo "<td>" . date("d-m-Y",strtotime($_Row['form_process_date'])) . "</td>";
				}
				if($_Row['form_delivered_date']==''){
					echo "<td>NA</td>";
				}else{
					echo "<td>" . date("d-m-Y",strtotime($_Row['form_delivered_date'])) . "</td>";
				}
									
			echo "<td>" . strtoupper($_Row['cstatus']) . "</td>";
				if($_Row['dispatchstatus'] !=3){
						$responses = $emp->GetLotName($_Row['cid']);
							$_Rows = mysqli_fetch_array($responses[2]);
							$lot=strtoupper($_Rows['lotname']);
					}
					else{
						$lot='NA';
					}
				echo "<td>" . $lot . "</td>";
		
			echo "</tr>";
			$_Count++;
		}
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}