<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsRscfaCertificatePayment.php';
$response = array();
$emp = new clsRscfaCertificatePayment();

if ($_action == "FillCourse") {
    $response = $emp->GetCourse();
		if($response[0]=="Success"){
			echo "<option value='' selected='selected'>Select Course</option>";
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
				}
		}
		else{
			echo "1";
		}   
}

if ($_action == "FillBatch") {
    $response = $emp->FILLBatchName($_POST['values']);
		if($_POST['values']=='5'){
			echo "<option value='' selected='selected'>Select Batch </option>";
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
			}
		}    
}

if ($_action == "ADD") {    
    if (empty($_POST["amounts"]) || !isset($_POST["AdmissionCodes"])) {
        echo "0";
    } else {
        $productinfo = 'RscfaCertificatePayment';
        $trnxId = $emp->AddPayTran($_POST, $productinfo);
        if ($_POST['gateway'] == 'razorpay' && !empty($trnxId) && $trnxId != 'TimeCapErr') {
            require("razorpay.php");
        } else {
            echo $trnxId;
        }
    }
}

if ($_action == "SHOWALL") {
    $response = $emp->GetAll($_POST['batch'], $_POST['course'], $_POST['paymode']);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    echo "<th style='12%'>D.O.B</th>";
    echo "<th style='10%'>Amount</th>";   
    echo "<th style='8%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $response1 = $emp->GetAdmissionFee($_POST['batch']);
    $_row1 = mysqli_fetch_array($response1[2]);
	
    $fee = $_row1['RscfaCertificate_Fee_Amount'];
   
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {

            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['apply_rscfa_cert_lname'] . "</td>";
            echo "<td>" . $_Row['apply_rscfa_cert_fname'] . "</td>";
            echo "<td>" . $_Row['apply_rscfa_cert_dob'] . "</td>";

            echo "<td>" . $fee . "</td>";

            if ($_Row['apply_rscfa_cert_payment_status'] == '0') {
                echo "<td><input type='checkbox' id='chk" . $_Row['apply_rscfa_cert_id'] .
                "' name='AdmissionCodes[]' value='" . $_Row['apply_rscfa_cert_id'] . "'></input></td>";
            } 
			elseif ($_Row['apply_rscfa_cert_payment_status'] == '1') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='Payment Confirmed'>"
                . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    } else {        
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "Fee") {    
    $response = $emp->GetAdmissionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);
    $totalshare = $_row['RscfaCertificate_Fee_Amount'];
    echo $totalshare;
}


if ($_action == "ShowRscfaCertificateEvent") {
    $response1 = $emp->ShowRscfaCertificateEvent($_POST['batch'],$_POST['course']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response1[2])) {
        echo "<option value=" . $_Row['Event_Payment'] . ">" . $_Row['payment_mode'] . "</option>";
    }
    
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();	
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
	
	$key = "X2ZPKM";
    $salt = "8IaBELXB";

    //For Test Mode
   // $key = 'gtKFFx';
   // $salt = "eCwWELxi";
	$command1 = "check_isDomestic";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	
	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);		
	$_DataTable = array();
    $_i = 0;
		$_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
				echo json_encode($_DataTable);		
 }

 if ($_action == "FinalValidateCard") {	
	$key = "X2ZPKM";
    $salt = "8IaBELXB";

    //For Test Mode
    //$key = 'gtKFFx';
    //$salt = "eCwWELxi";
	$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	
	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
   //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);	 
	if($response == 'Invalid'){
		echo "1";
	}
	else if($response == 'valid'){
		echo "2";
	}
 }
 
function curlCall($wsUrl, $qs, $true){
   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
		if (curl_errno($c)) {
		  $c_error = curl_error($c);
		  if (empty($c_error)) {
			  $c_error = 'Some server error';
			}
			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);
		return $arr;
}
?>	