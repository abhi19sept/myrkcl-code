<?php

/*
 * author VIVEK

 */
include './commonFunction.php';
require 'BAL/clsRSPTargetDeactive.php';

$response = array();
$emp = new clsRSPTargetDeactive();

if ($_action == "ShowDetails") {

    //echo "Show";
    $response = $emp->GetAll($_POST['user']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>District Name</th>";
    echo "<th style='40%'>Status</th>";
    echo "<th style='40%'>Select to Deactive</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
        echo "<td>" . $_Row['Rsptarget_Status'] . "</td>";
        
        $response1 = $emp->ValidateDistrict($_Row['User_Code'],$_Row['Rsptarget_District']);
        $_Row1 = mysqli_fetch_array($response1[2]);
        if($_Row1['UserCount'] != '0'){
          echo "<td><b>" . $_Row1['UserCount'] . " </b> IT-GK(s) Available</td>";  
        }
        else{
          echo "<td><input type='checkbox' id=chk" . $_Row['Rsptarget_Code'] .
        " name=chk" . $_Row['Rsptarget_Code'] . "></input></td>"; 
        }        
        echo "</tr>";
        $_Count++;       
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "FILLRSP") {
    $response = $emp->GetAllRSP();
    echo "<option value='0' selected='selected'>Select RSP</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Rsptarget_User'] . ">" . $_Row['Organization_Name'] . ' ' . "(" . $_Row['Rsptarget_User'] .")" . "</option>";
    }
}


if ($_action == "ADD") {
    // print_r($_POST);	
    $_UserPermissionArray = array();
    $_RSPCode = array();
    $_i = 0;
    $_Count = 0;
    $l = "";
    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 3) == 'chk') {
            $_UserPermissionArray[$_Count] = array(
                "Function" => substr($key, 3),
                "Attribute" => 3,
                "Status" => 1);
            $_Count++;
            $l .= substr($key, 3) . ",";
        }
        $_RSPCode = rtrim($l, ",");
        //print_r($_RSPCode);
        $count = count($_UserPermissionArray);
    }
    if ($count == '0') {
        echo "0";
    } else {
        $response = $emp->UpdateRSPStatus($_RSPCode);
        echo $response[0];
    }
}
