<?php
require 'commonFunction.php';
require 'BAL/clsrscfanotification.php';
$emp = new clsPushSMSRSCFA();
if ($_POST['action'] == "AddMessage") {
    if (isset($_POST["ddlroll"]) && !empty($_POST["ddlroll"]) && isset($_POST["txtMessageDes"]) && !empty($_POST["txtMessageDes"])
    ) {
        if(empty($_FILES["messagefile"]["name"])){
            $selectedddlroll = $_POST['selectedddlroll'];
            $txtMessageDes = $_POST['txtMessageDes'];
            $Status = $_POST['Status'];
            $arrayroll = explode(",", $selectedddlroll);
            $rollcount = count($arrayroll);
            $newfilename = "";
            for($i=0;$i<$rollcount;$i++){
                $response = $emp->PushMessageNew($arrayroll[$i], $txtMessageDes, $Status, $newfilename);
            }
            echo $response[0];
        }else{
            $selectedddlroll = $_POST['selectedddlroll'];
            $txtMessageDes = $_POST['txtMessageDes'];
            $Status = $_POST['Status'];
            $img = $_FILES['messagefile']['name'];
            $tmp = $_FILES['messagefile']['tmp_name'];
            $temp = explode(".", $img);
            $newfilename = round(microtime(true)) . '.' . end($temp);
            $filestorepath = "../upload/rscfaupdate/" . $newfilename;
            $imageFileType = pathinfo($filestorepath, PATHINFO_EXTENSION);
            if ($_FILES["messagefile"]["size"] > 1000000) {
                echo "Sorry, your file is too large.";
            }else{
                if ($imageFileType != "pdf") {
                    echo "Sorry, File Not Valid";
                } else {
                    if (move_uploaded_file($tmp, $filestorepath)) {
                        $arrayroll = explode(",", $selectedddlroll);
                        $rollcount = count($arrayroll);
                        for($i=0;$i<$rollcount;$i++){
                            $response = $emp->PushMessageNew($arrayroll[$i], $txtMessageDes, $Status, $newfilename);
                        }
                        echo $response[0]; 
                    } else {
                        echo "Invalid Details";
                    }
                }
            }
        }
    }
}

//Show Data Or View Details...
if ($_action == "SHOW") {
    if ($_POST['status'] == '1') {
        $response = $emp->ShowMessageList($_POST['status']);
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S. No.</th>";
        echo "<th>Send To</th>";
        echo "<th>Send By</th>";
        echo "<th>Message Description</th>";
        echo "<th>Attached File</th>";
        echo "<th>Date</th>";
        echo "<th>Status</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        if ($response[0] == 'Success') {
            while ($row = mysqli_fetch_array($response[2])) {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $row['UserRoll_Name'] . "</td>";
                echo "<td>" . $row['Message_addbyuser'] . "</td>";
                echo "<td>" . $row['Message_description'] . "</td>";
                if($row['Message_file']==""){
                    echo "<td> File not attached</td>";
                }else{
                   echo "<td><a target='_blanck' href='upload/rscfaupdate/" . $row['Message_file'] . "'><img class='thumbnail img-responsive' src='images/dwnpdf.png'  width='30'></a></td>"; 
                }
                echo "<td>" . date("F d, Y H:i:s", strtotime($row['Message_datetime'])) . "</td>";
                if($row['Message_status'] == '1'){$status = "Active";}else{$status = "Deactive";}
                echo "<td> " . $status . " <span id='" . $row['Message_id'] . "' name='".$row['Message_status']."' class='fun_change_status glyphicon glyphicon-pencil' "
                        . "style='cursor: pointer;margin: 0 0 0 5px;font-size: medium;'></span>"
                        . "<span id='" . $row['Message_id'] . "' name='".$row['Message_file']."' class='fun_delete_message glyphicon glyphicon-trash' "
                        . "style='cursor: pointer;margin: 0 0 0 10px;font-size: medium;'></span></td>";
                echo "</tr>";
                $_Count++;
            }
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
}

//Delete Slider Images....
if ($_action == "ChangeStatus") {
    if (isset($_POST["messageid"])) {
        $messageid = $_POST["messageid"];
        $messagestatus = $_POST["status"];
        if($messagestatus == '0'){$messagestatus = "1";}elseif($messagestatus == '1'){$messagestatus = "0";}
        $response = $emp->Changestatus($messageid,$messagestatus);
        echo $response[0];
    }
}
if ($_action == "DeleteMessage") {
    if (isset($_POST["messageid"])) {
        $messageid = $_POST["messageid"];
        $file = "../upload/rscfaupdate/".$_POST["status"];
        $response = $emp->DeleteMessage($messageid);
        echo $response[0];
        if(file_exists($file)){
            unlink($file);
        }
    }
}
if ($_action == "FILLROLL") {
    $response = $emp->GetRoll();
    //echo "<option> Select Roll </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        if($_Row['UserRoll_Code']==7){
           $rollname = "IT Gyan Kendra";
       }
       else if($_Row['UserRoll_Code']==14){
           $rollname = "Service Provider";
       }
       else if($_Row['UserRoll_Code']==15){
           $rollname = "AO";
       }
       else if($_Row['UserRoll_Code']==23){
           $rollname = "District Project Officer";
       }
        echo "<option value='" . $_Row['UserRoll_Code'] . "'>" . $rollname . "</option>";
    }
}

if ($_action == "COUNTMESSAGE") {
     $response = $emp->CountMessage();
     $_Row = mysqli_fetch_array($response[2]);
     if ($response[0] == 'Success') {
         echo $_Row['messagecount'];
     }else{
         echo "0";
     }
     
}
if ($_action == "READSTATUS") {
     $id = $_POST["messageid"];
     $response = $emp->ReadStatus($id);
     echo   $response[0];   
}
if ($_action == "SHOWMESSAGENOT") {
    
    $response = $emp->ShowMessageListNotification();
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($row = mysqli_fetch_array($response[2])) {
                $status = "";
                $responseCheck = $emp->ShowRead($row['Message_id']);
                 if ($responseCheck[0] == 'Success') {
                    $row1 = mysqli_fetch_array($responseCheck[2]);
                    $status = "Read";
                    
                 }else{
                     $status = "Unread";
                 }
            if ($_Count % 2 == 0) {
                echo "<div class='containermessage'>
                    <p><b>Message :- </b>" . $row['Message_description'] . "</p>
                    <div class='btnview'>";
                    if($row['Message_file']==""){
                        echo "<button type='submit' name='" . $row['Message_description'] . "'  id='" . $row['Message_id'] . "' class='btn btn-warning read_status_change'>View</button>";
                    }else{
                       echo "<button type='submit' name='" . $row['Message_file'] . "'  id='" . $row['Message_id'] . "' class='btn btn-warning fun_view_file'>View / Download</button>"; 
                    }  
                   echo "</div>
                    <span class='time-right'>Sent date:- " . date("F d, Y H:i:s", strtotime($row['Message_datetime'])) . "</span>
                         <span class='time-right' style='margin-right: 10px;'><b> " . $status . "</b></span>
                    <span class='time-left'>Sent by:- RKCL</span>
                   
                  </div>";
              }else{
                  echo "<div class='containermessage darker'>
                    <p><b>Message :- </b>" . $row['Message_description'] . "</p>
                    <div class='btnview'>
                       ";
                    if($row['Message_file']==""){
                        echo "<button type='submit' name='" . $row['Message_description'] . "'  id='" . $row['Message_id'] . "' class='btn btn-warning read_status_change'>View</button>";
                    }else{
                       echo "<button type='submit' name='" . $row['Message_file'] . "'  id='" . $row['Message_id'] . "' class='btn btn-warning fun_view_file'>View / Download</button>"; 
                    }  
                   echo "
                    </div>
                    <span class='time-right'>Sent date:- " . date("F d, Y H:i:s", strtotime($row['Message_datetime'])) . "</span>
                        <span class='time-right' style='margin-right: 10px;'><b> " . $status . "</b></span>
                    <span class='time-left'>Sent by:- RKCL</span>
                        
                    
                  </div>";
              }
            $_Count++;
        }
    }else{
        echo "nomessage";
    }
}

?>