<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsExamMaster.php';

$response = array();
$emp = new clsExamMaster();


if ($_action == "ADD") {
	
	
	//print_r($_POST);
	
	
    if (isset($_POST["ddlAffilate"]) ) {
        $_Affilate = $_POST["ddlAffilate"];
        $_Event=$_POST["ddlEvent"];
        $ddCourse=$_POST["ddlCourse"];
		$_Course = implode(",", $ddCourse);
		$_Assessment=$_POST["ddlAssessment"];
        $_fee=$_POST["ddlFee"];
		
		
		
		$_start=$_POST["txtAstart"];
		$_end=$_POST["txtEnd"];
		
        $_description=$_POST["txtDescription"];
		
		
		if(isset($_POST['ddlFreshBaches']))
		{
		$ddlFreshBaches = $_POST['ddlFreshBaches'];
		$freshexam = implode(",", $ddlFreshBaches);
		}
		else
		{
			$freshexam=0;
		}
		
		
		
		
		//$freshexam;
	
		$ddlReexamBatches = $_POST['ddlReexamBatches'];
		$reexam = implode(",", $ddlReexamBatches);
	
	
		//$ddlEvents = $_POST['ddlEvents'];
		if(isset($_POST["ddlEvents"]))
		{
		$ddlEvents= $_POST['ddlEvents'];
		}
		else
		{
			$ddlEvents=0;
		}
		
		
		
		if(isset($_POST["ddlBaches"]))
		{
		$ddlBaches= $_POST['ddlBaches'];
		}
		else
		{
			$ddlBaches=0;
		}
		$EventAstart=$_POST["txtEventAstart"];
		$EventAEnd=$_POST["txtEventAEnd"];
		
		
        $_ddlStatus=$_POST["ddlStatus"];
		
        $response = $emp->Add($_Affilate,$_Event,$_Course,$_Assessment,$_fee,$_start,$_end,$_description,$freshexam,$reexam,$ddlEvents,$ddlBaches,$EventAstart,$EventAEnd,$_ddlStatus);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
	//print_r($_POST);
    if (isset($_POST["txtDescription"]) ) {
        $_Affilate = $_POST["ddlAffilate"];
        $_Event=$_POST["ddlEvent"];
        $_Course=$_POST["ddlCourse"];
		$_Assessment=$_POST["ddlAssessment"];
        $_fee=$_POST["ddlFee"];
		$_start=$_POST["txtAstart"];
		$_end=$_POST["txtEnd"];
		
        $_description=$_POST["txtDescription"];
		$ddlFreshBaches = $_POST['ddlFreshBaches'];
		$freshexam = implode(",", $ddlFreshBaches);
		$freshexam;
	
		$ddlReexamBatches = $_POST['ddlReexamBatches'];
		$reexam = implode(",", $ddlReexamBatches);
	
	
		$ddlEvents = $_POST['ddlEvents'];
		
		$ddlBaches= $_POST['ddlBaches'];
		
		$EventAstart=$_POST["txtEventAstart"];
		$EventAEnd=$_POST["txtEventAEnd"];
		
		
        $_ddlStatus=$_POST["ddlStatus"];
		 $_Code=$_POST["code"];
        $response = $emp->Update($_Code,$_Affilate,$_Event,$_Course,$_Assessment,$_fee,$_start,$_end,$_description,$freshexam,$reexam,$ddlEvents,$ddlBaches,$EventAstart,$EventAEnd,$_ddlStatus);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("Affilate_authority" => $_Row['Affilate_authority'],
            "Affilate_Event" => $_Row['Affilate_Event'],
            "Course_Code" => $_Row['Course_Code'],
			"Affilate_Assessment" => $_Row['Affilate_Assessment'],
            "Affilate_Fee" => $_Row['Affilate_Fee'],
			"Affilate_Astart" => $_Row['Affilate_Astart'],
			"Affilate_End" => $_Row['Affilate_End'],
			"Affilate_Description" => $_Row['Affilate_Description'],
			 "Affilate_FreshBaches" => $_Row['Affilate_FreshBaches'],
            "Affilate_ReexamBatches" => $_Row['Affilate_ReexamBatches'],
			"Affilate_cases" => $_Row['Affilate_cases'],
            "Affilate_baches" => $_Row['Affilate_baches'],
			"Event_Start" => $_Row['Event_Start'],
			"Event_End" => $_Row['Event_End'],
			"Event_Status" => $_Row['Event_Status']
			);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>Exam Authority</th>";
    echo "<th style='20%'>Exam Event</th>";
   
    echo "<th style='15%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Affiliate_Name'] . "</td>";
        echo "<td>" . $_Row['Event_Name'] . "</td>";
      
        echo "<td><a href='frmexamMaster.php?code=" . $_Row['Affilate_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmexamMaster.php?code=" . $_Row['Affilate_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}


if ($_action == "FILLEVENT") {
    $response = $emp->GetEvent($_actionvalue);
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}


if ($_action == "FILLPUBLISHEVENT") {
    $response = $emp->GetpublishEvent();
   echo "<optgroup label='Select multiple'>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}


if ($_action == "FILLEXAMID") {
    $response = $emp->GetEventid($_actionvalue);
    $_Row = mysqli_fetch_array($response[2]);
   echo "<label for='address'>Examid:</label>";
        echo "<input type='text' class='form-control'  name='txtexamid' 
									id='txtexamid' readonly='true'   value='" . $_Row['Affilate_Event'] . "' >";
    
}
if ($_action == "FILLDATE") {
    $response = $emp->GetpublishDate($_actionvalue);
	$_Row = mysqli_fetch_array($response[2]);
	echo "<label for='dob'>Exam Date:</label>";
    echo "<input type='text' class='form-control' readonly='true'  name='txtdate' id='txtdate'  value=" . $_Row['Event_Start'] . " >";
}

if ($_action == "FILLEXAMFEE") {
    $response = $emp->GetexamFee($_actionvalue);
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['ReExam_Id'] . ">" . $_Row['ReExam_fee'] . "</option>";
    }
}
if ($_action == "FILLEXCEPTION") {
    $response = $emp->Getexception($_actionvalue);
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "FILLEXAMBATCH") {
    $response = $emp->Getexambatch($_actionvalue);
		$_pipe = mysqli_fetch_array($response[2]);
		print_r($_pipe);
		$_batch_pipe = explode("|",$_pipe[0]);
		print_r($_batch_pipe);
	$response = $emp->Getexambatchname($_batch_pipe[0]);		
    echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}


if ($_action == "FILLExamAdmissionBatchcode") {
    $response = $emp->FILLAdmissionBatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] ."_" . $_Row['Course_Name'] ."</option>";
    }
}

