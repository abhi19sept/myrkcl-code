<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsRedhatExperienceCenterFee.php';

$response = array();
$emp = new clsRedhatExperienceCenterFee();

if ($_action == "ADD") {
   if (empty($_POST["amounts"])) {
        echo "0";
    } 	
	else {
			$response = $emp->checkpaystatus($_POST['ddltype']);
				if($response[0]=='Success'){
					echo "done";
				}
				else {					
						$productinfo = 'ExperienceCenterFeePayment';
							if($_POST['ddltype']=='1'){
								$centerarray[]=$_SESSION['User_LoginId'];
								
								$trnxId = $emp->AddPayTran($_POST, $productinfo,$centerarray);
								if ($_POST['gateway'] == 'razorpay' && !empty($trnxId) && $trnxId != 'TimeCapErr') {
									require("razorpay.php");
								} else {
									echo $trnxId;
								}
							}
							else{
									if (!empty($_POST["txtLearnercode"]) && !empty($_POST["txtlname"]) && 
											!empty($_POST["txtfname"]) && !empty($_POST["dob"])) {
										$centerarray=array($_POST['txtLearnercode'],$_POST['txtlname'],$_POST['txtfname'],$_POST['dob']);
										
										$trnxId = $emp->AddPayTran($_POST, $productinfo,$centerarray);
											if ($_POST['gateway'] == 'razorpay' && !empty($trnxId) && $trnxId != 'TimeCapErr') {
												require("razorpay.php");
											} else {
												echo $trnxId;
											}
									}
									else{
										echo "empty";
									}
									
								}						
				}		
    }
}

if ($_action == "GENERATELEARNERCODE") {
	   $random="";
		 $random.= (mt_rand(100, 999));
         //$random.= date("y");
         //$random.= date("m");
         //$random.= date("d");
         //$random.= date("h");
         $random.= date("i");
         $random.= date("s");
         $random.= '8';
		 
    echo $random;
}

if ($_action == "checkauthorization") {
    $response = $emp->checkauthorization();
		if($response[0]=='Success'){
			echo "1";
		}
		else {
			echo "0";
		}
}

if ($_action == "GetAuthorizeType") {  
    $response = $emp->GetAuthorizeType();
    echo "<option value='' selected='selected'>Please Select</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['redhat_sub_id'] . ">" . $_Row['redhat_sub_name'] . "</option>";
    }
}

if ($_action == "Fee") { 
    $response = $emp->GetExperienceCenterFee();
    $row = mysqli_fetch_array($response[2]);
		$amt=$row["redhat_expcenter_fee"];
		echo $amt;
}

if ($_action == "ShowPayMode") {
    $response = $emp->ShowPayMode();
		if($response[0]=='Success'){
			echo "<option value='' selected='selected'>Please Select</option>";
			while ($_Row = mysqli_fetch_array($response[2])) {
				echo "<option value=" . $_Row['Event_Payment'] . ">" . $_Row['payment_mode'] . "</option>";
			}
		}
		else {
			echo "0";
		}
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
	//print_r($response);
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
	//values
	$key = "X2ZPKM";
	$salt = "8IaBELXB";
	$command1 = "check_isDomestic";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	//$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
	//$var3 = "500";//  Amount to be used in case of refund

	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);
	//echo "<pre>"; print_r($response); echo "</pre>"; 
	$_DataTable = array();
    $_i = 0;
		$_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
				echo json_encode($_DataTable);
		//echo $cardType = $response['cardType'];
		//echo $cardCategory = $response['cardCategory'];
 }

 if ($_action == "FinalValidateCard") {
	//values
	$key = "X2ZPKM";
	$salt = "8IaBELXB";
	$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	//$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
	//$var3 = "500";//  Amount to be used in case of refund

	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);
	//echo "<pre>"; print_r($response); echo "</pre>"; 
	if($response == 'Invalid'){
		echo "1";
	}
	else if($response == 'valid'){
		echo "2";
	}
 }
 
function curlCall($wsUrl, $qs, $true){
   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
		if (curl_errno($c)) {
		  $c_error = curl_error($c);
		  if (empty($c_error)) {
			  $c_error = 'Some server error';
			}
			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);
		return $arr;
}

?>
