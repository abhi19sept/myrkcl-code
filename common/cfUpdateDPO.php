<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsUpdateDPO.php';

$response = array();
$emp = new clsUpdateDPO();


if ($_action == "UpdateDPO") {
    if (isset($_POST["Code"])&& !empty($_POST["Code"]) && $_POST["Code"] !='0') {
        $_DPOCode = $_POST["Code"];
        $_Mobile=$_POST["Mobile"];
        $_Email=$_POST["Email"];
        $response = $emp->UpdateDPO($_DPOCode,$_Mobile,$_Email);
        echo $response[0];
    } else {
        echo "1";
        return;
    }
}

if ($_action == "DeRegister") {
    if (isset($_POST["Code"]) && !empty($_POST["Code"]) && $_POST["Code"] !='0') {
        $_Code = $_POST["Code"];
        $response = $emp->DeRegister($_Code);
        echo $response[0];
    } else {
        echo "1";
        return;
    }
}


if ($_action == "APPROVE") {
    //print_r($_POST);
    if (isset($_POST["value"]) && !empty($_POST["value"]) && $_POST["value"] !='0') {
    $response = $emp->GetDatabyCode($_POST['value']);
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if($co){
    while ($_Row = mysqli_fetch_array($response[2])) {
       
        $_DataTable[$_i] = array("username" => $_Row['User_LoginId'],
            "usermobile" => $_Row['User_MobileNo'],
            "useremail" => $_Row['User_EmailId'],
            "fullname" => ($_Row['fullname']? $_Row['fullname'] : 'NA'),
            "biomobile" => ($_Row['mobile']? $_Row['mobile'] : 'NA'),
            "bioemail" => ($_Row['email_id']? $_Row['email_id'] : 'NA'));
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
} else {
        echo "";
    }
    } else {
        echo "1";
        return;
    }
}

if ($_action == "FILL") {
    $response = $emp->GetAllDPO();
    echo "<option value='0' >Select DPO</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_Code'] . ">" . $_Row['User_LoginId'] . "</option>";
    }
}