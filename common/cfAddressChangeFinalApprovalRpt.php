<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsAddressChangeFinalApprovalRpt.php';

$response = array();
$emp = new clsAddressChangeFinalApprovalRpt();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='10%'>IT-GK  AreaType Old</th>";
    echo "<th style='40%'>IT-GK Address Old</th>"; 
    echo "<th style='40%'>IT-GK AreaType New</th>";
    echo "<th style='40%'>IT-GK Address New</th>";
    echo "<th style='40%'>Application Date</th>";
    echo "<th style='10%'>SP Approval Date</th>";
    echo "<th style='10%'>Remarks By SP</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['fld_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_AreaType_old'] . "</td>";
            echo "<td>" . $_Row['Organization_Address_old'] . "</td>";
            echo "<td>" . $_Row['Organization_AreaType'] . "</td>";
            echo "<td>" . $_Row['Organization_Address'] . "</td>";
            echo "<td>" . $_Row['submission_date'] . "</td>";
            echo "<td>" . $_Row['submission_date_sp'] . "</td>";
            echo "<td>" . $_Row['fld_remarks'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

