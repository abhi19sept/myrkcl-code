<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsPremisesAreaRpt.php';

$response = array();
$emp = new clsPremisesAreaRpt();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
        
    echo "<th style='40%'>IT-GK District</th>";
    echo "<th style='40%'>IT-GK Tehsil</th>";
    echo "<th style='40%'>Service Provider</th>";
    
    echo "<th style='40%'>Premises Seperate</th>";    
    echo "<th style='40%'>Theory Area in Sq.Ft.</th>";
    echo "<th style='10%'>Lab Area in Sq.Ft.</th>";
    echo "<th style='10%'>Total Area in Sq.Ft.</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2],MYSQLI_ASSOC)) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['ITGKCODE'] . "</td>";
            echo "<td>" . $_Row['ITGK_Name'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['RSP_Name'] . "</td>";
            
            echo "<td>" . ($_Row['Permises_Separate'] ? $_Row['Permises_Separate'] : 'NA' ) . "</td>";
            echo "<td>" . ($_Row['Premises_Theory_area'] ? $_Row['Premises_Theory_area'] : '0' ) . "</td>";
            echo "<td>" . ($_Row['Premises_Lab_area'] ? $_Row['Premises_Lab_area'] : '0' ) . "</td>";
            
            echo "<td>" . ($_Row['Premises_Total_area'] ? $_Row['Premises_Total_area'] : '0' ) . "</td>";
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

