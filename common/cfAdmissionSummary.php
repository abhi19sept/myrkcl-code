<?php

/*
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsAdmissionSummary.php';
require '../DAL/upload_ftp_doc.php';
$response = array();
$emp = new clsAdmissionSummary();
$_ObjFTPConnection = new ftpConnection();

if ($_action == "GETDATA") {

    $response = $emp->GetDataAll($_POST['role'], $_POST['course'], $_POST['batch']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    //echo "<th>CenterCode</th>";
    // echo "<th>Course</th>";
    // echo "<th>Batch</th>";
    echo "<th>Code</th>";
	echo "<th >Uploaded Admission Count</th>";
    echo "<th >Confirm Admission Count</th>";
    // echo "<th>Count</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;
	$_Totaleconfirm = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            // echo "<td>" . $_Row['Course'] . "</td>";
            //  echo "<td>" . $_Row['Batch'] . "</td>";
            echo "<td>" . $_Row['RoleName'] . "</td>";
			//echo "<td><a href='frmAdmissionList.php?course=" . $_Row['Course'] . "&batch=" . $_Row['Batch'] . "&rolecode=" . $_Row['RoleCode'] . "&mode=ShowUpload' target='_blank'>"
           // . "" . $_Row['uploadcount'] . "</a></td>";
          //  echo "<td><input type='button' class='updcount' course='".$_Row['Course']."' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowUpload' value='" . $_Row['uploadcount'] . "'/></td>";
           //  echo "<td><input type='button' class='updcount' course='".$_Row['Course']."' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowConfirm' value='" . $_Row['confirmcount'] . "'/></td>";
           // echo "<td><a href='frmAdmissionList.php?course=" . $_Row['Course'] . "&batch=" . $_Row['Batch'] . "&rolecode=" . $_Row['RoleCode'] . "&mode=ShowConfirm' target='_blank'>"
           // . "" . $_Row['confirmcount'] . "</a></td>";
		   
		     echo "<td><button type='button' class='updcount' course='".$_Row['Course']."' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowUpload' value='" . $_Row['uploadcount'] . "'>" . $_Row['uploadcount'] . "</button></td>";

             echo "<td><button type='button' class='updcount' course='".$_Row['Course']."' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowConfirm' value='" . $_Row['confirmcount'] . "'>" . $_Row['confirmcount'] . "</button></td>";
            echo "</tr>";
            $_Total = $_Total + $_Row['uploadcount'];
			$_Totaleconfirm = $_Totaleconfirm + $_Row['confirmcount'];
            $_Count++;
        }

        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
        echo "<th >TotalCount </th>";
        echo "<th>";
        echo "$_Total";
        echo "</th>";
		echo "<th>";
        echo "$_Totaleconfirm";
        echo "</th>";
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetLearnerList($_POST['course'], $_POST['batch'], $_POST['rolecode'], $_POST['mode']);

    $_DataTable = "";
global  $_ObjFTPConnection;
    echo "<div class='table-responsive'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
	echo "<th >Mobile NO</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
		$ftpaddress = $_ObjFTPConnection->ftpPathIp();
	$_Course = $_POST['course'];
	$_LearnerBatchNameFTP="";

                if ($_Course == 5) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP;                    

                } elseif ($_Course == 4) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

                }
                elseif ($_Course == 3) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }               
                
                elseif ($_Course == 23) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

                }   

                 elseif ($_Course == 25) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

                }
                elseif ($_Course == 28) {


                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

                }
                elseif ($_Course == 29) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

                }
                elseif ($_Course == 30) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

                }
                elseif ($_Course == 31) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

                }
                elseif ($_Course == 32) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

                }
				
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
			echo "<td>" . $_Row['Admission_Mobile'] . "</td>";

            $_LearnerBatchNameFTP2 = str_replace(' ', '_', $_Row['Batch_Name'].$_LearnerBatchNameFTP);
			
            if($_Row['Admission_Photo']!="")
				{
					$image = $_Row['Admission_Photo'];
					echo "<td >"
				. "<input type='button' name='Edit' class='btn btn-primary view_photo' batchname='".$_LearnerBatchNameFTP2."'
			image='".$image."' id='".$_Count.'_photo'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphoto_".$_Count."'> </div>"
			."</td>";
					}
			else
				{
					
					echo "<td id='".$_Count.'_photo'."'>" . '<img  alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' 
							.   "</td>";
				}
			//echo "<div id='".$_Count.'_pic'."' style='display:none;'>" . "<div id='Viewphoto_".$_Count."'> </div>" . "</div>";	
			
			
			
		if($_Row['Admission_Sign']!="")
			{
				$sign = $_Row['Admission_Sign'];
				echo "<td>"
				. "<input type='button' name='Edit' class='btn btn-primary view_sign' batchname='".$_LearnerBatchNameFTP2."'
			image='".$sign."' count='".$_Count."' id='".$_Count.'_sign'."' value='View Sign'/>". "<div style='display:none;' id='Viewsign_".$_Count."'> </div>".
			"</td>";
			}
		else
				{
					echo "<td id='".$_Count.'_sign'."'>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
				}
		//echo "<div id='".$_Count.'_signature'."' style='display:none;'>" . "<div id='Viewsign_".$_Count."'> </div>" . "</div>";	
		

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETDATAITGK") {
    $response = $emp->GetLearnerListITGK($_POST['course'], $_POST['batch'], $_POST['rolecode']);

    $_DataTable = "";
global  $_ObjFTPConnection;
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father/Husband Name</th>";
    echo "<th>Learner Mobile</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
	echo "<th>Payment Status</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
		$ftpaddress = $_ObjFTPConnection->ftpPathIp();
	$_Course = $_POST['course'];
	$_LearnerBatchNameFTP="";

                if ($_Course == 5) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP;                    

                } elseif ($_Course == 4) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

                }
                elseif ($_Course == 3) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }               
                
                elseif ($_Course == 23) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

                }   

                 elseif ($_Course == 25) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

                }
                elseif ($_Course == 28) {


                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

                }
                elseif ($_Course == 29) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

                }
                elseif ($_Course == 30) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

                }
                elseif ($_Course == 31) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

                }
                elseif ($_Course == 32) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

                }
				
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";

            $_LearnerBatchNameFTP2 = str_replace(' ', '_', $_Row['Batch_Name'].$_LearnerBatchNameFTP);
			
            if($_Row['Admission_Photo']!="")
				{
					$image = $_Row['Admission_Photo'];
					echo "<td >"
				. "<input type='button' name='Edit' class='btn btn-primary view_photo' batchname='".$_LearnerBatchNameFTP2."'
			image='".$image."' id='".$_Count.'_photo'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphoto_".$_Count."'> </div>"
			."</td>";
					}
			else
				{
					
					echo "<td id='".$_Count.'_photo'."'>" . '<img  alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' 
							.   "</td>";
				}
			//echo "<div id='".$_Count.'_pic'."' style='display:none;'>" . "<div id='Viewphoto_".$_Count."'> </div>" . "</div>";	
			
			
			
		if($_Row['Admission_Sign']!="")
			{
				$sign = $_Row['Admission_Sign'];
				echo "<td>"
				. "<input type='button' name='Edit' class='btn btn-primary view_sign' batchname='".$_LearnerBatchNameFTP2."'
			image='".$sign."' count='".$_Count."' id='".$_Count.'_sign'."' value='View Sign'/>". "<div style='display:none;' id='Viewsign_".$_Count."'> </div>".
			"</td>";
			}
		else
				{
					echo "<td id='".$_Count.'_sign'."'>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
				}
		//echo "<div id='".$_Count.'_signature'."' style='display:none;'>" . "<div id='Viewsign_".$_Count."'> </div>" . "</div>";	
		
			 if ($_Row['Admission_Payment_Status'] == "1") {
               
                echo "<td> Payment Confirmed </td>";
            } elseif ($_Row['Admission_Payment_Status'] == "0")  {
                echo "<td> Payment Not Confirmed </td>";
            }
			elseif ($_Row['Admission_Payment_Status'] == "8")  {
                echo "<td> Confirmation Pending at RKCL </td>";
            }


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
if ($_action == "DETAILRPTITGK") {
    $response = $emp->DetailedListITGK($_POST['course'], $_POST['batch'], $_POST['rolecode']);

    $_DataTable = "";
global  $_ObjFTPConnection;
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >DOB</th>";
    echo "<th>Gender</th>";
    echo "<th >Course</th>";
    echo "<th>Batch</th>";
    echo "<th >Medium</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "<th>Mother Tongue</th>";
    echo "<th>Marital Status</th>";
    echo "<th>UID</th>";
    echo "<th >PH Status</th>";
    echo "<th >District</th>";
    echo "<th>Tehsil</th>";
    echo "<th>Address</th>";
    echo "<th>PIN code</th>";
    echo "<th>Mobile</th>";
    echo "<th>Phone</th>";
    echo "<th>Email</th>";
    echo "<th >Qualification</th>";
    echo "<th >Learner Type</th>";
    echo "<th >GPFNO</th>";
    echo "<th>BioMetric Status</th>";
    echo "<th>Admission Date</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
		$ftpaddress = $_ObjFTPConnection->ftpPathIp();
	$_Course = $_POST['course'];
	$_LearnerBatchNameFTP="";

                if ($_Course == 5) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP;                    

                } elseif ($_Course == 4) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

                }
                elseif ($_Course == 3) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }               
                
                elseif ($_Course == 23) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

                }   

                 elseif ($_Course == 25) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

                }
                elseif ($_Course == 28) {


                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

                }
                elseif ($_Course == 29) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

                }
                elseif ($_Course == 30) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

                }
                elseif ($_Course == 31) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

                }
                elseif ($_Course == 32) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

                }
				
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";
            echo "<td>" . $_Row['Admission_Gender'] . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Medium'] . "</td>";
            $_LearnerBatchNameFTP2 = str_replace(' ', '_', $_Row['Batch_Name'].$_LearnerBatchNameFTP);
			
            if($_Row['Admission_Photo']!="")
				{
					$image = $_Row['Admission_Photo'];
					echo "<td >"
				. "<input type='button' name='Edit' class='btn btn-primary view_photo' batchname='".$_LearnerBatchNameFTP2."'
			image='".$image."' id='".$_Count.'_photo'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphoto_".$_Count."'> </div>"
			."</td>";
					}
			else
				{
					
					echo "<td id='".$_Count.'_photo'."'>" . '<img  alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' 
							.   "</td>";
				}
			//echo "<div id='".$_Count.'_pic'."' style='display:none;'>" . "<div id='Viewphoto_".$_Count."'> </div>" . "</div>";	
			
			
			
		if($_Row['Admission_Sign']!="")
			{
				$sign = $_Row['Admission_Sign'];
				echo "<td>"
				. "<input type='button' name='Edit' class='btn btn-primary view_sign' batchname='".$_LearnerBatchNameFTP2."'
			image='".$sign."' count='".$_Count."' id='".$_Count.'_sign'."' value='View Sign'/>". "<div style='display:none;' id='Viewsign_".$_Count."'> </div>".
			"</td>";
			}
		else
				{
					echo "<td id='".$_Count.'_sign'."'>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
				}
		//echo "<div id='".$_Count.'_signature'."' style='display:none;'>" . "<div id='Viewsign_".$_Count."'> </div>" . "</div>";	
		
			
            echo "<td>" . $_Row['Admission_MTongue'] . "</td>";
            echo "<td>" . $_Row['Admission_MaritalStatus'] . "</td>";
            echo "<td>" . $_Row['Admission_UID'] . "</td>";
            echo "<td>" . $_Row['Admission_PH'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Address'] . "</td>";
            echo "<td>" . $_Row['Admission_PIN'] . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            echo "<td>" . $_Row['Admission_Phone'] . "</td>";
            echo "<td>" . $_Row['Admission_Email'] . "</td>";
            echo "<td>" . $_Row['Qualification_Name'] . "</td>";
            echo "<td>" . $_Row['LearnerType_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_GPFNO'] . "</td>";
            if($_Row['BioMatric_Status']=='1'){
                 echo "<td>Learner Enrolled</td>";
            }
            else{
                 echo "<td>Learner Not Enrolled</td>";
            }
            //echo "<td>" . $_Row['BioMatric_Status'] . "</td>";
            echo "<td>" . date("d-m-Y", strtotime($_Row['Timestamp'])) . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_Name'] . "</td>";
//            echo "<td>" . $_Row['Admission_Fname'] . "</td>";


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
    if ($_action == "DETAILRPT") {
    $response = $emp->DetailedList($_POST['course'], $_POST['batch'], $_POST['role']);

    $_DataTable = "";
global  $_ObjFTPConnection;
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >DOB</th>";
    echo "<th>Gender</th>";
    echo "<th >Course</th>";
    echo "<th>Batch</th>";
    echo "<th >Medium</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "<th>Mother Tongue</th>";
    echo "<th>Marital Status</th>";
    echo "<th>UID</th>";
    echo "<th >PH Status</th>";
    echo "<th >District</th>";
    echo "<th>Tehsil</th>";
    echo "<th>Address</th>";
    echo "<th>PIN code</th>";
    echo "<th>Mobile</th>";
    echo "<th>Phone</th>";
    echo "<th>Email</th>";
    echo "<th >Qualification</th>";
    echo "<th >Learner Type</th>";
    echo "<th >GPFNO</th>";
    echo "<th>BioMetric Status</th>";
    echo "<th>Admission Date</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
		$ftpaddress = $_ObjFTPConnection->ftpPathIp();
	$_Course = $_POST['course'];
	$_LearnerBatchNameFTP="";

                if ($_Course == 5) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP;                    

                } elseif ($_Course == 4) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

                }
                elseif ($_Course == 3) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }               
                
                elseif ($_Course == 23) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

                }   

                 elseif ($_Course == 25) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

                }
                elseif ($_Course == 28) {


                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

                }
                elseif ($_Course == 29) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

                }
                elseif ($_Course == 30) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

                }
                elseif ($_Course == 31) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

                }
                elseif ($_Course == 32) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

                }
				
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";
            echo "<td>" . $_Row['Admission_Gender'] . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Medium'] . "</td>";
            $_LearnerBatchNameFTP2 = str_replace(' ', '_', $_Row['Batch_Name'].$_LearnerBatchNameFTP);
			
            if($_Row['Admission_Photo']!="")
				{
					$image = $_Row['Admission_Photo'];
					echo "<td >"
				. "<input type='button' name='Edit' class='btn btn-primary view_photo' batchname='".$_LearnerBatchNameFTP2."'
			image='".$image."' id='".$_Count.'_photo'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphoto_".$_Count."'> </div>"
			."</td>";
					}
			else
				{
					
					echo "<td id='".$_Count.'_photo'."'>" . '<img  alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' 
							.   "</td>";
				}
			//echo "<div id='".$_Count.'_pic'."' style='display:none;'>" . "<div id='Viewphoto_".$_Count."'> </div>" . "</div>";	
			
			
			
		if($_Row['Admission_Sign']!="")
			{
				$sign = $_Row['Admission_Sign'];
				echo "<td>"
				. "<input type='button' name='Edit' class='btn btn-primary view_sign' batchname='".$_LearnerBatchNameFTP2."'
			image='".$sign."' count='".$_Count."' id='".$_Count.'_sign'."' value='View Sign'/>". "<div style='display:none;' id='Viewsign_".$_Count."'> </div>".
			"</td>";
			}
		else
				{
					echo "<td id='".$_Count.'_sign'."'>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
				}
		//echo "<div id='".$_Count.'_signature'."' style='display:none;'>" . "<div id='Viewsign_".$_Count."'> </div>" . "</div>";	
		
			
			
            echo "<td>" . $_Row['Admission_MTongue'] . "</td>";
            echo "<td>" . $_Row['Admission_MaritalStatus'] . "</td>";
            echo "<td>" . $_Row['Admission_UID'] . "</td>";
            echo "<td>" . $_Row['Admission_PH'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Address'] . "</td>";
            echo "<td>" . $_Row['Admission_PIN'] . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            echo "<td>" . $_Row['Admission_Phone'] . "</td>";
            echo "<td>" . $_Row['Admission_Email'] . "</td>";
            echo "<td>" . $_Row['Qualification_Name'] . "</td>";
            echo "<td>" . $_Row['LearnerType_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_GPFNO'] . "</td>";
            if($_Row['BioMatric_Status']=='1'){
                 echo "<td>Learner Enrolled</td>";
            }
            else{
                 echo "<td>Learner Not Enrolled</td>";
            }
            //echo "<td>" . $_Row['BioMatric_Status'] . "</td>";
            echo "<td>" . date("d-m-Y", strtotime($_Row['Timestamp'])) . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_Name'] . "</td>";
//            echo "<td>" . $_Row['Admission_Fname'] . "</td>";


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "ViewPhoto") {
    //print_r($_POST); die;
    $batchname = $_POST['batchname'];
	$photopath = '/admission_photo/'.$batchname.'/';
    $image = $_POST['image'];
	
    global $_ObjFTPConnection;
	
	$details= $_ObjFTPConnection->ftpdetails();
	
    $photoimage =  file_get_contents($details.$photopath.$image);
$imageData = base64_encode($photoimage);
echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='80' height='110'/>";

    
}

if ($_action == "ViewSign") {
    //print_r($_POST); die;
    $batchname = $_POST['batchname'];
	$photopath = '/admission_sign/'.$batchname.'/';
    $image = $_POST['image'];
	
    global $_ObjFTPConnection;
	
	$details= $_ObjFTPConnection->ftpdetails();
	
    $photoimage =  file_get_contents($details.$photopath.$image);
$imageData = base64_encode($photoimage);
echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='50' height='80'/>";

    
}
