<?php
    require 'BAL/clsmodifyITGK.php';

    $emp = new clsmodifyITGK();

    /*** UPDATE DATABASE AND SEND EMAIL TO ALL USERS ***/

    $_PostResponse = [];
    $_PostResponse["Code"] = $postArray['udf1'];
    $_PostResponse["fld_transactionID"] = $postArray['txnid'];
    $_PostResponse["fld_bankTransactionID"] = $postArray['bank_ref_num'];
    $_PostResponse["fld_amount"] = $postArray['amount'];
    $_PostResponse["fld_ref_no"] = $postArray['udf1'];
    $_PostResponse["flag"] = $flag;
    $_PostResponse["fld_paymentTitle"] = $postArray['productinfo'];

    $response2 = $response3 = [];

    try {
        global $_ObjConnection;
        $_ObjConnection->Connect();
        $_SelectQuery = "UPDATE tbl_change_address_ITGK SET fld_status = '5', fld_updatedBy = '" . $_SESSION['UserRoll_Name'] . "' WHERE fld_ref_no = '" . $_PostResponse['Code'] . "'  ";
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::UpdateStatement);
    } catch (Exception $_ex) {
        $_Response[0] = $_ex->getLine() . $_ex->getTrace();
        $_Response[1] = Message::Error;
    }

    $response = $_Response;

    if ($response[0] == "Successfully Updated") {
        $response1 = $emp->GetAddressData($_PostResponse['Code']);
        if ($response1[1] == 1) {
            $response2 = mysqli_fetch_assoc($response1[2]);
            $response3 = $emp->commitChangeAddressFinal($response2, $_PostResponse['flag']);
        }
    }

    if ($response3[0] == "Successfully Updated") {
        $response4 = $emp->commitChangeAddressFinal2($_PostResponse);
    }


    /*** SEND EMAIL/SMS TO ITGK ***/
    $response4 = $emp->getCenterDetails($_PostResponse['Code']);
    $row = mysqli_fetch_assoc($response4[2]);
    $row["type"] = "paid";
    $emp->sendSMSEmail($row);

    /*** SEND EMAIL/SMS TO SERVICE PROVIDER ***/
    $response5 = $emp->getSPDetails($row['fld_rspCode']);
    $row5 = mysqli_fetch_assoc($response5[2]);
    $response6 = $emp->sendEmailtoSPAccept3($row5['username'], $row5['email'], $row5['mobile'], $row['ITGK_Name'], $_PostResponse['Code'], $_PostResponse['flag']);

    /*** SEND EMAIL/SMS TO RKCL ***/
    $rkcl_UserCode = '4257';
    $response6 = $emp->getRKCLDetails($rkcl_UserCode);
    $row6 = mysqli_fetch_assoc($response6[2]);
    $response7 = $emp->sendEmailtoRKCLAccept3($row5['username'], $row['ITGK_Name'], $_PostResponse['Code'], $_PostResponse['flag'], $row6);

    /*** GENERATE GST INVOICE ***/
    $response_gst = $emp->getInvoiceMasterDetails('Location Change');
    $row_gst = mysqli_fetch_assoc($response_gst[2]);
    $response1_gst = $emp->generateInvoice($_PostResponse, $row_gst);