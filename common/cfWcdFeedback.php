<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsWcdFeedback.php';

$response = array();
$emp = new clsWcdFeedback();


if ($_action == "SubmitFeedback") {
//    print_r(($_POST));
//   die;
    $_CenterCode = $_POST["ITGKCode"];
    $_VisitCode = $_POST["txtVisitCode"];
    if (isset($_POST["desktop"]) && !empty($_POST["desktop"]) && $_POST["desktop"] !='0') {
    $_Desktop = $_POST["desktop"];
        } else {
        echo "Please Enter Desktop Count.";
        return;
    }
    if (isset($_POST["toilet"]) && !empty($_POST["toilet"]) && $_POST["toilet"] !='0') {
    $_Toilet = $_POST['toilet'];
        } else {
        echo "Please specify toilet availablity";
        return;
    }
    if($_POST["toilet"] == 'Yes'){
    if (isset($_POST["Stoilet"]) && !empty($_POST["Stoilet"]) && $_POST["Stoilet"] !='0') {
    $_Stoilet = $_POST["Stoilet"];
        } else {
        echo "Please specify whether toilet are eparate.";
        return;
    }
    }
    elseif ($_POST["toilet"] == 'No') {
//      if (isset($_POST["Stoilet"]) && !empty($_POST["Stoilet"]) && $_POST["Stoilet"] !='0') {
    $_Stoilet = "NA";
//        } else {
//        echo "Please specify whether toilet are eparate.";
//        return;
//    }  
    }
    

     if (isset($_POST["water"]) && !empty($_POST["water"]) && $_POST["water"] !='0') {
    $_Water = $_POST["water"];
        } else {
        echo "Please specify water availablity.";
        return;
    }
     if (isset($_POST["inaguarated"]) && !empty($_POST["inaguarated"]) && $_POST["inaguarated"] !='0') {
    $_Inagurated = $_POST["inaguarated"];
        } else {
        echo "Please specify whether Ingurated by Public Representative.";
        return;
    }
    if($_POST["inaguarated"] == "Yes"){
    if (isset($_POST["upload"]) && !empty($_POST["upload"]) && $_POST["upload"] !='0') {
    $_Upload= $_POST["upload"];
    $_PlanDate = "NA";
        } else {
        echo "Please specify whether uploaded the details on MYRKCL for both the courses (RS-CIT/RS-CFA) separately.";
        return;
    }
    } elseif ($_POST["inaguarated"] == "No") {
     if (isset($_POST["txtEstdate"]) && !empty($_POST["txtEstdate"])) {
    $_PlanDate = $_POST["txtEstdate"];
    $_Upload= "NA";
        } else {
        echo "Please Select Date";
        return;
    }
}
    
      if (isset($_POST["informed"]) && !empty($_POST["informed"])) {
    $_Informed= $_POST["informed"];
        } else {
        echo "Please Select whether informed about compulsory Biometric Attendance for WCD Scheme";
        return;
    }
	if($_POST["RSCFACheck"] == '1'){
    if (isset($_POST["FacultyName"]) && !empty($_POST["FacultyName"])) {
    $_FacultyName = $_POST["FacultyName"];
        } else {
        echo "Please Enter FacultyName";
        return;
    }
    
    if (isset($_POST["ddlQualification"]) && !empty($_POST["ddlQualification"]) && $_POST["ddlQualification"] !='0') {
        $_Qualification = $_POST["ddlQualification"];
        } else {
        echo "Please Select Qualification";
        return;
        }
	} else {
	$_FacultyName = "NA";	
	$_Qualification = "NA";
	}	
		
		
    if (isset($_POST["Learner"]) && !empty($_POST["Learner"]) && $_POST["Learner"] !='0') {
		if($_POST["Learner"] == "Yes"){
			 $_UserPermissionArray = array();
				$_Count = 0;
				$_Counts = 0;
				$List = '';
				foreach ($_POST as $key => $value) {
					if (substr($key, 0, 3) == 'chk') {
					$_UserPermissionArray[$_Counts] = array(
						"Function" => (substr($key, 3)));
					$List .= $_UserPermissionArray[$_Counts]['Function'].',';
					$_Counts++;
				}
				}
				if($List == ''){
					echo "Please Select present learners";
					return;
				} else {
				$_AdmissionCodes = rtrim($List, ',');
				}
		}
		elseif ($_POST["Learner"] == "No") {
			$_AdmissionCodes = "NA";
		}
	} else {
        echo "Please Select Learner Availablity";
        return;
    }
   
        $response = $emp->AddFeedback($_VisitCode,$_CenterCode,$_Desktop,$_Toilet,$_Stoilet,$_Water,$_Inagurated,$_Upload,$_PlanDate,$_Informed,$_FacultyName,$_Qualification,$_AdmissionCodes);

        echo $response[0];
}


if ($_action == "GETDETAILS") {
    $_CenterCode = $_POST["cc"];
    $response = $emp->GetITGK_Details($_CenterCode);
    
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        
        $_DataTable[$_i] = array("itgkcode" => $_Row['ITGKCODE'],
            "itgkname" => $_Row['ITGK_Name'],
            "itgkmobile" => $_Row['ITGKMOBILE'],
            "itgkemail" => $_Row['ITGKEMAIL'],
            "district" => $_Row['District_Name'],
            "tehsil" => $_Row['Tehsil_Name'],
            
            "spcode" => $_Row['RSP_Code'],
            "spname" => $_Row['RSP_Name']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "CheckEligible") {
    $response = $emp->CheckEligible($_POST['cc']);
    echo $response[0];
}

if ($_action == "Check") {
    $response = $emp->CheckRSCFA($_POST['cc']);
    echo $response[0];
}

if ($_action == "FillCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "GetLearners") {
    //print_r($_POST);
    $response = $emp->GetDatabyCode($_POST["values"], $_POST["CenterCode"], $_POST["batch"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Learner Code</th>";
    echo "<th style='20%'>Learner Name</th>";
    echo "<th style='10%'>Father Name</th>";
    echo "<th style='10%'>DOB</th>";
    echo "<th style='10%'>Present Learners</th>";
   
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	 echo "</div>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
         echo "<td>" . $_Row['Admission_Name'] . "</td>";
         echo "<td>" . $_Row['Admission_Fname'] . "</td>";
         echo "<td>" . $_Row['Admission_DOB'] . "</td>";
         echo "<td><input type='checkbox' id=chk" . $_Row['Admission_Code'] .
            " name=chk" . $_Row['Admission_Code'] . "></input>"
            . "</td>";
        
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}

