<?php

/* clsCopyDbTables.php
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCopyDbTables.php';

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$response = array();
$emp = new clsCopyDbTables();

if ($_action == "GetTables") {
    $response = $emp->GetTables();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['cpy_id'] . ">" . $_Row['cpy_name'] . "</option>";
    }
}

if ($_action == "GetCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Courseitgk_Course'] . "</option>";
    }
}

if ($_action == "GetBatch") {
    $response = $emp->GetBatch($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "GetGovtStatus") {
    $response = $emp->FILLSTATUSFORUPDATE();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}

if ($_action == "GetEOICode") {
    $response = $emp->FILLEoi();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['EOI_Code'] . ">" . $_Row['EOI_Name'] . "</option>";
    }
}

if ($_action == "UpdateTable") {	

// ------------------ Admission Table Start ------------------------------------------------------------------------------
		if($_POST['ddltables']=='1'){
			$response = $emp->UpdateAdmission($_POST['ddltables'],$_POST['ddlCourse'],$_POST['ddlBatch']);
		}
// ------------------ Admission Table End ---------------------------------------------------------------------------------	




// ------------------ Admission Transaction Table Start --------------------------------------------------------------------	
		if($_POST['ddltables']=='2'){
			$response = $emp->UpdateAdmissionTransaction($_POST['ddltables'],$_POST['ddlCourse'],$_POST['ddlBatch']);
		}
// ------------------ Admission Transaction Table End ------------------------------------------------------------------------

		

		
// ------------------ Batch Master Table Start ---------------------------------------------------------------------------------		
		else if($_POST['ddltables']=='3'){
			$response = $emp->UpdateBatchMaster($_POST['ddltables'],$_POST['txtstartdate'],$_POST['txtenddate']);
		}
// ------------------ Batch Master Table End ---------------------------------------------------------------------------------			
		
	
	
// ------------------ Biometric Register Table Start --------------------------------------------------------------------			
		else if($_POST['ddltables']=='4'){
			if($_POST['ddlRange']=='1'){
				$srange='1';
				$erange='5000';
			}
			else if($_POST['ddlRange']=='2'){
				$srange='5001';
				$erange='10000';
			}
			else if($_POST['ddlRange']=='3'){
				$srange='10001';
				$erange='15000';
			}
			else if($_POST['ddlRange']=='4'){
				$srange='15001';
				$erange='20000';
			}
			else if($_POST['ddlRange']==''){
				echo "0";
			}
			else {
				echo "0";
			}
			$response = $emp->UpdateBiometricRegister($srange,$erange);		
		}
// ------------------ Biometric Register Table End --------------------------------------------------------------------			

		
		
		
// ------------------ Correction Copy Table Start --------------------------------------------------------------------				
		else if($_POST['ddltables']=='7'){
			$response = $emp->UpdateCorrectionCopy($_POST['ddltables'],$_POST['txtstartdate'],$_POST['txtenddate']);			
		}		
// ------------------ Correction Copy Table End ------------------------------------------------------------------------





// ------------------ Course ITGK Mapping Table Start --------------------------------------------------------------------
		else if($_POST['ddltables']=='10') {
			$response = $emp->UpdateCourseITGKMapping($_POST['ddltables'],$_POST['ddlMappingCourse']);
		}
// ------------------ Course ITGK Mapping Table End --------------------------------------------------------------------		





// ------------------ Eoi Center List Table Start ------------------------------------------------------------------------
		else if($_POST['ddltables']=='11') {
			$response = $emp->UpdateEoiCenterList($_POST['ddltables'],$_POST['ddlEoi']);
		}
// ------------------ Eoi Center List Table End --------------------------------------------------------------------------		
	



// ------------------ Govt. Epmloyee Table Start ---------------------------------------------------------------------------
	else if($_POST['ddltables']=='12') {
			$response = $emp->UpdateGovtEmp($_POST['ddltables'],$_POST['ddlGovtEmpStatus']);
		}		
// ------------------ Govt. Epmloyee Table End -------------------------------------------------------------------------------




// ------------------ Organization Detail Table Start -------------------------------------------------------------------------		
		else if($_POST['ddltables']=='13'){
			if($_POST['ddlRange']=='1'){
				$srange='1';
				$erange='5000';
			}
			else if($_POST['ddlRange']=='2'){
				$srange='5001';
				$erange='10000';
			}
			else if($_POST['ddlRange']=='3'){
				$srange='10001';
				$erange='15000';
			}
			else if($_POST['ddlRange']=='4'){
				$srange='15001';
				$erange='20000';
			}
			else if($_POST['ddlRange']==''){
				echo "0";
			}
			else {
				echo "0";
			}
			$response = $emp->UpdateOrganizationDetail($srange,$erange);		
		}
// ------------------ Organization Detail Table End --------------------------------------------------------------------			



// ------------------ User Master Table Start --------------------------------------------------------------------------	
		else if($_POST['ddltables']=='14'){
			if($_POST['ddlRange']=='1'){
				$srange='2';
				$erange='5000';
			}
			else if($_POST['ddlRange']=='2'){
				$srange='5001';
				$erange='10000';
			}
			else if($_POST['ddlRange']=='3'){
				$srange='10001';
				$erange='15000';
			}
			else if($_POST['ddlRange']=='4'){
				$srange='15001';
				$erange='20000';
			}
			else if($_POST['ddlRange']==''){
				echo "0";
			}
			else {
				echo "0";
			}
			$response = $emp->UpdateUserMaster($srange,$erange);		
		}
// ------------------ User Master Table End --------------------------------------------------------------------			
		
}


?>