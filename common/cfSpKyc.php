<?php

/* 
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsSpKyc.php';

$response = array();
$emp = new clsSpKyc();
$_ObjFTPConnection = new ftpConnection();

if ($_action == "CheckExistence") {
    $response = $emp->CheckExistence($_POST['SelKycType']);
    echo $response;
}

if ($_action == "FillCompType") {
    $response = $emp->GetCompType();
    echo "<option value='' selected='selected'>Select Company Type</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Org_Type_Code'] . ">" . $_Row['Org_Type_Name'] . "</option>";
    }
}

if ($_action == "GENERATEOTP") {
    $mobile = $_POST["mobile"];
    $response = $emp->GenerateOTP($mobile);
    echo $response[0];
}
if ($_action == "VERIFYOTPFUN") {
    $otp = $_POST["otp"];
    $mobile = $_POST["mobile"];
    $response = $emp->VerifyOTP($otp, $mobile);
    echo $response[0];
}

if ($_action == "GENERATEEMAILOTP") {
    $email = $_POST["email"];
    $response = $emp->GenerateEmailOTP($email);
    echo $response[0];
}
if ($_action == "VERIFYOTPEmail") {
    $otp = $_POST["otp"];
    $email = $_POST["email"];
    $response = $emp->VerifyEmailOTP($otp, $email);
    echo $response[0];
}

if ($_action == "insertform1") {
    $userid = "";
    $countuser = $_POST["countuser"];
    $flag = 0;
    $status = "";
	$txtType = $_POST["txtType"];
    for ($i = 1; $i <= $countuser; $i++) {
        $phoneNumber = 'txtfrm1mobile' . $i;
        $veriy = 'verfiyMobileSet' . $i;
        $veriyemail = 'verfiyEmailSet' . $i;
		$Name = 'txtfrm1name' . $i;
		
		if(!isset($_POST[$Name])){
				   
		}
		else{
			if(isset($_POST[$veriy])) {
				if($_POST[$veriy] != "varified"){
					$flag++;
					$status = "Please Verify Mobile Number Before Submitting Details " . $i; 
				}
			}
			if(isset($_POST[$veriyemail])) {
				if($_POST[$veriyemail] != "varified"){
					$flag++;
					$status = "Please Verify Email Id Before Submitting Details " . $i; 
				}
			}
		}        
    }
    if ($flag == 0) {
        for ($i = 1; $i <= $countuser; $i++) {
            $Name = 'txtfrm1name' . $i;
            $Email = 'txtfrm1email' . $i;
            $Mobile = 'txtfrm1mobile' . $i;
            $Designation = 'txtfrm1designation' . $i;
            $Address = 'txtfrm1address' . $i;
            $Appointment = 'txtfrm1doa' . $i;
            $PAN = 'txtfrm1pan' . $i;
            $ShareHolding = 'txtfrm1Shareholding' . $i;
			$txtType = 'txtType';
               if(!isset($_POST[$Name])){
				   
			   }else{
                $response = $emp->AddDirectorDetails($_POST[$Name], $_POST[$Email], $_POST[$Mobile], $_POST[$Designation],
			   $_POST[$Address], $_POST[$Appointment], $_POST[$PAN], $_POST[$ShareHolding], $_POST[$txtType]);}
        }
    }
    if (isset($response[0])) {
        if ($response[0] == 'Successfully Inserted') {
            $status = $response[0];
        }
		else{
			$status = $response[0];
		}
    }
    echo $status;    
}

if ($_action == "insertform2") {
	$userid = "";
    $countuser = $_POST["countuser2"];
    $flag = 0;
    $status = "";
	
   /* for ($i = 1; $i <= $countuser; $i++) {
        $companyName = 'companyName' . $i;
        $emailId = 'emailId' . $i;
        $phoneNumber = 'phoneNumber' . $i;
        $veriy = 'verfiyMobileSet' . $i;
        if(isset($_POST[$veriy])) {
            if($_POST[$veriy] != "varified"){
                $flag++;
                $status = "Please Verify Phone Number Before Submiting Student Information " . $i; 
            }
        } else {
            if (isset($companyName) && empty($_POST[$companyName])) {
                $flag++;
                $status = "Please Open Student Information And Check All Field.";
            }
        }
    }*/
    if ($flag == 0) {
        for ($i = 1; $i <= $countuser; $i++) {
            $Name = 'txtfrm2name' . $i;
            $Relation = 'txtfrm2relation' . $i;
            $Interested = 'frm2IW' . $i;
            $Interested_Name = 'txtfrm2iwname' . $i;
            $Nature = 'txtfrm2nature' . $i;
            $Remark = 'txtfrm2remark' . $i;
				if(!isset($_POST[$Name])){
				   
			   }else{
			
                $response = $emp->AddDirectorInterest($_POST[$Name], $_POST[$Relation], $_POST[$Interested], $_POST[$Interested_Name],
				$_POST[$Nature], $_POST[$Remark]);
			   }
        }
    }
    if (isset($response[0])) {
        if ($response[0] == 'Successfully Inserted') {
            $status = $response[0];
        }
    }
    echo $status;    
}

if ($_action == "insertform3") {
    $userid = "";
    $countuser = $_POST["countuser3"];
    $flag = 0;
    $status = "";

    if ($flag == 0) {
        for ($i = 1; $i <= $countuser; $i++) {
            $ITGK_Code = 'txtfrm3itgkcode' . $i;
            $ITGK_Name = 'txtfrm3itgkname' . $i;
            $Remark = 'txtfrm3remark' . $i;
				if(!isset($_POST[$ITGK_Code])){
				   
			   }else{
                $response = $emp->AddOwnedITGK($_POST[$ITGK_Code], $_POST[$ITGK_Name], $_POST[$Remark]);
			   }
        }
    }
    if (isset($response[0])) {
        if ($response[0] == 'Successfully Inserted') {
            $status = $response[0];
        }
    }
    echo $status;    
}

if ($_action == "insertform4") {
	
	
	$userid = "";
    $countuser = $_POST["countuser4"];
    $flag = 0;
    $status = "";
	
	for ($i = 1; $i <= $countuser; $i++) {
        $phoneNumber = 'txtfrm4mobile' . $i;
        $veriy = 'verfiyfrm4MobileSet' . $i;
		$Name = 'txtfrm4name' . $i;
		
		if(!isset($_POST[$Name])){
				   
		}
		else{
			if(isset($_POST[$veriy])) {
				if($_POST[$veriy] != "varified"){
					$flag++;
					$status = "Please Verify Mobile Number Before Submitting Details " . $i; 
				}
			}
		}		
    }
	if ($flag == 0) {
        for ($i = 1; $i <= $countuser; $i++) {
			$Name = 'txtfrm4name' . $i;
			if(isset($_POST[$Name])){
				   $Selected_District = 'ddlfrm4District' . $i;
			
			$Mobile = 'txtfrm4mobile' . $i;
			$Designation = 'txtfrm4designation' . $i;
			$Appointment = 'txtfrm4doa' . $i;
			$District = implode(',', $_POST[$Selected_District]);
			$StationAt = 'ddlfrm4stationat' . $i;
				
				$response = $emp->AddAuthorizePerson($_POST[$Name], $_POST[$Mobile], $_POST[$Designation],
					$_POST[$Appointment], $District, $_POST[$StationAt]);
			}
			  
        }
    }
    if (isset($response[0])) {
        if ($response[0] == 'Successfully Inserted') {
            $status = $response[0];
        }
    }
    echo $status;    
}

if ($_action == "insertform5") {
//	print_r($_POST);
//        echo "<br/>";
//        print_r($_FILES);
//        die;
		$flag = 0;	
		if(!isset($_POST['txtfrm5name1'])){
				   
		}
		else{
			if(isset($_POST['verfiyfrm5MobileSet1'])) {
				if($_POST['verfiyfrm5MobileSet1'] != "varified"){
					$flag++;
					$status = "Please Verify Mobile Number Before Submitting Details"; 
				}
			}
			if(isset($_POST['verfiyfrm5EmailSet1'])) {
				if($_POST['verfiyfrm5EmailSet1'] != "varified"){
					$flag++;
					$status = "Please Verify Email Id Before Submitting Details"; 
				}
			}
                        if (empty($_FILES['txtfrm5file1']['name'])) {				
					$flag++;
					$status = "Please Upload Front office with Name and Address Board Image"; 
				
			}
                          if (empty($_FILES['txtfrm5file2']['name'])) {				
					$flag++;
					$status = "Please Upload Reception or Interior Office Image"; 
				
			}
                          if (empty($_FILES['txtfrm5file3']['name'])) {				
					$flag++;
					$status = "Please Upload Attach Memorandum & Articles of Association / Partnership Deed / Society bylaws / Trust deed Image"; 
				
			}
		}        
		//echo $flag;print_r($_POST);die;

            
		if ($flag == 0) {
				$_UploadDirectory1 = '/SP_Front_Office';
                $_UploadDirectory2 = '/SP_Interior_office';
                $_UploadDirectory3 = '/SP_Deed';
                
                $_SP_Code = $_SESSION['User_LoginId'];
                date_default_timezone_set("Asia/Kolkata");
                $dtstamp = date("Ymd_His");
                
                if ($_FILES["txtfrm5file1"]["name"] != '') {
                    $imag = $_FILES["txtfrm5file1"]["name"];
                    $imageinfo = pathinfo($_FILES["txtfrm5file1"]["name"]);
                    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                            $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                            $flag = 0;
                    } else {
						if (file_exists("$_UploadDirectory1/" . $_SP_Code . '_' . $dtstamp . '.' . substr($imag, -3))) {
											$_FILES["txtfrm5file1"]["name"] . "already exists";
						}
                        else {                                   
                                ftpUploadFile($_UploadDirectory1,$_SP_Code . '_' . $dtstamp.'.jpg',$_FILES["txtfrm5file1"]["tmp_name"]);
							}
                    }
				}
            
				if ($_FILES["txtfrm5file2"]["name"] != '') {
                    $imag = $_FILES["txtfrm5file2"]["name"];
                    $imageinfo = pathinfo($_FILES["txtfrm5file2"]["name"]);
                    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                            $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                            $flag = 0;
                    } else {
                            if (file_exists("$_UploadDirectory2/" . $_SP_Code . '_' . $dtstamp . '.' . substr($imag, -3))) {
                                    $_FILES["txtfrm5file2"]["name"] . "already exists";
                            } else {
								ftpUploadFile($_UploadDirectory2,$_SP_Code . '_' . $dtstamp.'.jpg',$_FILES["txtfrm5file2"]["tmp_name"]);                                    
                            }
                    }
				}
            
				if ($_FILES["txtfrm5file3"]["name"] != '') {
                    $imag = $_FILES["txtfrm5file3"]["name"];
                    $imageinfo = pathinfo($_FILES["txtfrm5file3"]["name"]);
                    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                            $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                            $flag = 0;
                    } else {
                            if (file_exists("$_UploadDirectory3/" . $_SP_Code . '_' . $dtstamp . '.' . substr($imag, -3))) {
                                    $_FILES["txtfrm5file3"]["name"] . "already exists";
                            } else {
								ftpUploadFile($_UploadDirectory3,$_SP_Code . '_' . $dtstamp.'.jpg',$_FILES["txtfrm5file3"]["tmp_name"]);                                    
                            }
                    }
				}
            
            $img_name=$_SP_Code . '_' . $dtstamp . '.jpg';
                    
			$response = $emp->AddOfficeDetails($_POST['txtfrm5name1'], $_POST['txtfrm5email1'], $_POST['txtfrm5mobile1'],
						$_POST['txtfrm5designation1'],$_POST['txtfrm5address1'], $_POST['txtfrm5cp1'],$img_name);
		}		
   
		if (isset($response[0])) {
			if ($response[0] == 'Successfully Inserted') {
				$status = $response[0];
			}
		}
		echo $status;    
}