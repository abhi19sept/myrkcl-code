<?php

/* 
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsClassTypeMaster.php';

$response = array();
$emp = new clsClassTypeMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"])) {
        $_ClassTypeName = $_POST["name"];
        $_Status=$_POST["status"];

        $response = $emp->Add($_ClassTypeName,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_ClassTypeName = $_POST["name"];
        $_Status=$_POST["status"];
        $_Code=$_POST['code'];
        $response = $emp->Update($_Code,$_ClassTypeName,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("ClassTypeCode" => $_Row['ClassType_Code'],
            "ClassTypeName" => $_Row['ClassType_Name'],
            "Status"=>$_Row['ClassType_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%' style='margin-top:20px ;'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Class Type Name</th>";
    echo "<th style='40%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['ClassType_Name'] . "</td>";
         echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmclasstypemaster.php?code=" . $_Row['ClassType_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmclasstypemaster.php?code=" . $_Row['ClassType_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Status</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['ClassType_Code'] . ">" . $_Row['ClassType_Name'] . "</option>";
    }
}