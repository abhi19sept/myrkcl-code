<?php

/* 
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsHRDetail.php';

$response = array();
$emp = new clsHRDetail();


if ($_action == "ADD") {
    if (isset($_POST["name"])) {

        $_StaffName = $_POST["name"];
        $_StaffDesignation = $_POST["designation"];
        $_StaffGender = $_POST["gender"];
        $_StaffDob = $_POST["dob"];
        $_StaffMobile = $_POST["mobile"];
        $_StaffEmail = $_POST["email"];
        $_StaffQuali1 = $_POST["qualification1"];
        $_Experience = $_POST["experience"];
        $_StaffQuali2 = $_POST["qualification2"];
        $_StaffQuali3 = $_POST["qualification3"];
       
        //echo $_StatusName;


        $response = $emp->Add($_StaffName,$_StaffDesignation,$_StaffGender,$_StaffDob,$_StaffMobile,$_StaffEmail,$_StaffQuali1,$_Experience,$_StaffQuali2,$_StaffQuali3);
        echo $response[0];
    }
}



if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        
       $_StaffName = $_POST["name"];
        $_StaffDesignation = $_POST["designation"];
        $_StaffGender = $_POST["gender"];
        $_StaffDob = $_POST["dob"];
        $_StaffMobile = $_POST["mobile"];
        $_StaffEmail = $_POST["email"];
        $_StaffQuali1 = $_POST["qualification1"];
        $_Experience = $_POST["experience"];
        $_StaffQuali2 = $_POST["qualification2"];
        $_StaffQuali3 = $_POST["qualification3"];
       
        $_Code=$_POST["code"];
        $response = $emp->Update($_Code,$_StaffName,$_StaffDesignation,$_StaffGender,$_StaffDob,$_StaffMobile,$_StaffEmail,$_StaffQuali1,$_Experience,$_StaffQuali2,$_StaffQuali3);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("Staff_Designation" => $_Row['Staff_Designation'],
            "Staff_Name" => $_Row['Staff_Name'],
            "Staff_Dob" => $_Row['Staff_Dob'],
            "staff_Gender" => $_Row['staff_Gender'],
            "Staff_Mobile" => $_Row['Staff_Mobile'],
            "Staff_Experience" => $_Row['Staff_Experience'],
            "Staff_Email_Id" => $_Row['Staff_Email_Id'],
			"Staff_Qualification2" => $_Row['Staff_Qualification2'],
            "Staff_Qualification3" => $_Row['Staff_Qualification3']
			
			);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    //echo "<th style='20%'>Staff User</th>";
    echo "<th style='10%'>Staff Name</th>";
     echo "<th style='10%'>Staff Designation</th>";
    echo "<th style='20%'>Staff Mobile</th>";
    echo "<th style='20%'>Staff Experience (in Years)</th>";
    echo "<th style='10%'>Staff Email Id</th>";
    echo "<th style='10%'>Staff ITGK code</th>";
   // echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
       
        // echo "<td>" . $_Row['Staff_User'] . "</td>";
         echo "<td>" . $_Row['Staff_Name'] . "</td>";
         echo "<td>" . $_Row['Designation_Name'] . "</td>";
         echo "<td>" . $_Row['Staff_Mobile'] . "</td>";
         echo "<td>" . $_Row['Staff_Experience'] . "</td>";
         echo "<td>" . $_Row['Staff_Email_Id'] . "</td>";
		 echo "<td>" . $_Row['Staff_User'] . "</td>";
//        echo "<td><a href='frmhrdetails.php?code=" . $_Row['Staff_Code'] . "&Mode=Edit'>"
//                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a></td>  ";
     
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "FILL") {
    $response = $emp->Getdesigantion();
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['code'] . ">" . $_Row['Designation'] . "</option>";
    }
}
