<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsCourseAuthorization.php';
require '../DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();

$response = array();
$emp = new clsCourseAuthorization();


if ($_action == "ShowDetails") {
    $response = $emp->GetAll();
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Organization Name</th>";
    echo "<th>Organization Ack No</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th>Organization Address</th>";
    echo "<th>Rural/Urban</th>";
    echo "<th>Organization Pincode</th>";
    echo "<th>Organization District</th>";
    echo "<th>Organization Tehsil</th>";
    echo "<th>Organization Email</th>";
    echo "<th>Organization Mobile</th>";
    echo "<th>SP Code</th>";
    echo "<th>SP Name</th>";
    echo "<th>Faculty Obtained Marks</th>";
    echo "<th>Login Approval Date</th>";
    echo "<th>Final Approval Date</th>";
    echo "<th>Course Authorization Date</th>";
    echo "<th>IT-GK Docs And Details</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Ack']) . "</td>";
        echo "<td>" . strtoupper($_Row['User_LoginId']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Road']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_AreaType']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_PinCode']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_District']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Tehsil']) . "</td>";
        echo "<td>" . $_Row['Org_Email'] . "</td>";
        echo "<td>" . $_Row['Org_Mobile'] . "</td>";
        echo "<td>" . $_Row['Org_RspLoginId'] . "</td>";
        echo "<td>" . $_Row['RSP_Name'] . "</td>";
        echo "<td>" . $_Row['Marks'] . "</td>";
        echo "<td>" . date('d-m-Y', strtotime($_Row['Org_NCR_Login_Approval_Date'])) . "</td>";
        echo "<td>" . ($_Row['Org_NCR_Final_Approval_Date'] ? date('d-m-Y', strtotime($_Row['Org_NCR_Final_Approval_Date'])) : 'Not Available') . "</td>";
        echo "<td>" . ($_Row['Org_NCR_Course_Allocation_Date'] ? date('d-m-Y', strtotime($_Row['Org_NCR_Course_Allocation_Date'])) : 'Not Available') . "</td>";
         echo "<td> <a href='frmshowncrdetails.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit&cc=" . $_Row['User_LoginId'] ."&ack=" . $_Row['Org_Ack'] ."'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='View Details'/></a>"
            . "</td>";
        if ($_Row['Org_Course_Allocation_Status'] == 'Pending') {
        if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11  || $_SESSION['User_UserRoll'] == '4') {
            if($_Row['Marks'] >='30'){
            echo "<td> <a href='frmcourseallocationprocess.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit&cc=" . $_Row['User_LoginId'] ."&ack=" . $_Row['Org_Ack'] ."'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-danger' value='Process'/></a>"
            . "</td>";
            } else {
            echo "<td color='Red'>"
            . "Faculty Marks Not Sufficient for Approval"
            . "</td>";    
            }
        } else if ($_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '9' || $_SESSION['User_UserRoll'] == '23' || $_SESSION['User_UserRoll'] == '8') {
           if($_Row['Marks'] >='30'){
            echo "<td>"
            . "Pending for Course Authoization Approval"
            . "</td>";
            } else {
            echo "<td color='Red'>"
            . "Faculty Marks Not Sufficient for Approval"
            . "</td>";    
            }
        } else {
            echo "<td>"
            . "Rejected"
            . "</td>";
        }
        } else if ($_Row['Org_Course_Allocation_Status'] == 'Approved') {
            echo "<td>"
            . "Approved"
            . "</td>";
        } else {
            echo "<td>"
            . "Rejected"
            . "</td>";
        }
        

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "PROCESS") {
    $response = $emp->GetOrgDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
            $_role = 'IT-GK';
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "doctype" => $_Row['Organization_DocType'],
            "role" => $_role,
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            "district" => $_Row['Organization_District_Name'],
            "districtcode" => $_Row['Organization_District'],
            "tehsil" => $_Row['Organization_Tehsil'],
            "street" => $_Row['Organization_Street'],
            "road" => $_Row['Organization_Road'],
            "orgdoc" => $_Row['Organization_ScanDoc'],
            "orguid" => $_Row['Organization_UID'],
            "orgaddproof" => $_Row['Organization_AddProof'],
            "orgappform" => $_Row['Organization_AppForm'],
            "orgtypedoc1" => $_Row['Organization_TypeDocId'],);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "CourseAllocation") {
    
    $_Ack = $_POST["ack"];
    $_CenterCode = $_POST["centercode"];
    $_Mobile = $_POST["mobile"];
    $response = $emp->CourseAllocation($_Ack, $_CenterCode, $_Mobile);
    echo $response[0];
    
}

if ($_action == "REJECT") {
    
    $_Ack = $_POST["ack"];
    $_CenterCode = $_POST["centercode"];
    $_Mobile = $_POST["mobile"];
    $response = $emp->Reject($_Ack, $_CenterCode, $_Mobile);
    echo $response[0];
    
}

if ($_action == "SendSMS") {
    
    $_SMS = $_POST["sms"];
    $_CenterCode = $_POST["centercode"];
    $_Mobile = $_POST["mobile"];
    $response = $emp->SendMessage($_SMS, $_CenterCode, $_Mobile);
    echo $response[0];
    
}

if ($_action == "PAYMENTDETAIL") {

    //echo "Show";
    $response = $emp->SHOWPAYENTDETAILS($_POST["centercode"]);

    $_DataTable = "";

   echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='examplepay' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>IT-GK Code</th>";
    echo "<th style='40%'>IT-GK Name</th>";
    echo "<th style='10%'>NCR Amount</th>";
	echo "<th style='10%'>Payment Status</th>";
	echo "<th style='10%'>Product Info</th>";
	echo "<th style='10%'>Date and Time</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Pay_Tran_ITGK'] . "</td>";
         echo "<td>" . strtoupper($_Row['Pay_Tran_Fname']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Pay_Tran_Amount']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Pay_Tran_Status']) . "</td>";
		 echo "<td>" . $_Row['Pay_Tran_ProdInfo'] . "</td>";
		 echo "<td>" . $_Row['timestamp'] . "</td>";
		 
        echo "</tr>";
        $_Count++;
    }
   echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "NCRVISIT") {
    global  $_ObjFTPConnection;
    $details= $_ObjFTPConnection->ftpdetails();
    $photopath = '/NCRVISIT/'; 
    $response = $emp->GetNcrPhotoData($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        
        $TheoryRoom =  file_get_contents($details.$photopath.$_Row["NCRVisit_TheoryRoom"]);
        $TheoryRoomEn = base64_encode($TheoryRoom);
        $showTheoryRoom =  "<iframe id='ftpimg' src='data:image/png;base64, ".$TheoryRoomEn."' alt='Red dot' width='100%' height='500px'></iframe>";
        
        $Reception =  file_get_contents($details.$photopath.$_Row["NCRVisit_Reception"]);
        $ReceptionEn = base64_encode($Reception);
        $showReception =  "<iframe id='ftpimg' src='data:image/png;base64, ".$ReceptionEn."' alt='Red dot' width='100%' height='500px'></iframe>";
        
        $Exterior =  file_get_contents($details.$photopath.$_Row["NCRVisit_Exterior"]);
        $ExteriorEn = base64_encode($Exterior);
        $showExterior =  "<iframe id='ftpimg' src='data:image/png;base64, ".$ExteriorEn."' alt='Red dot' width='100%' height='500px'></iframe>";
        
        $Other =  file_get_contents($details.$photopath.$_Row["NCRVisit_Other"]);
        $OtherEn = base64_encode($Other);
        $showOther =  "<iframe id='ftpimg' src='data:image/png;base64, ".$OtherEn."' alt='Red dot' width='100%' height='500px'></iframe>";
        
        $_DataTable[$_i] = array("NCRTP" => $showTheoryRoom,
            "NCRRP" => $showReception,
            "NCREP" => $showExterior,
            "NCROP" => $showOther,);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "SPCenterAgreement") {
    $response = $emp->GetSPCenteragreement($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("SPCEN" => $_Row['SPCenter_Agreement'],);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "GETREGRSP") {
    $response = $emp->GetRspDetails($_POST["centercode"]);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {

        $_DataTable[$_i] = array("rspname" => $_Row['Rspitgk_Rspname'],
            "mobno" => $_Row['Rspitgk_Rsproad'],
            "date" => $_Row['Rspitgk_Rspestdate'],
            "rsptype" => $_Row['Rspitgk_Rsporgtype']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}