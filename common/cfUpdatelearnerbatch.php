<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsUpdatelearnerbatch.php';

$response = array();
$emp = new clsUpdatelearnerbatch();

/* Action for Course Loading */
if ($_action == "FILLCourse") {
    $response = $emp->GetAllCourse();
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}
/* Action for Viw Learner Last Batch */
if ($_action == "VIEWREPORT") {

    $response = $emp->GetPrevBatch($_POST["ddlCourse"]);
    if ($response[0] == 'Success') {
        $_Row = mysqli_fetch_array($response[2]);
        $prev_batch = $_Row['Batch_Code'];
        $responseLAM = $emp->GetPrevBatchLearners($prev_batch);
        $num_rowsLAM = mysqli_num_rows($responseLAM[2]);
        if ($num_rowsLAM == 0) {
            echo "NoDetails";
        } else {
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>S No.</th>";
            echo "<th>Learner Code </th>";
            echo "<th>Learner Name </th>";
            echo "<th>Father Name</th>";
            echo "<th>D.O.B.</th>";
            echo "<th>Course Name</th>";
            echo "<th>Batch Name</th>";
            echo "<th><input type='checkbox' id='checkuncheckall' name='checkuncheckall' value=''> Select All</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;

            if ($responseLAM[0] == 'Success') {
                while ($row = mysqli_fetch_array($responseLAM[2])) {
                    $dob = date("d-m-Y", strtotime($row['Admission_DOB']));
                    echo "<tr class='odd gradeX'>";
                    echo "<td>" . $_Count . "</td>";
                    echo "<td>" . $row['Admission_LearnerCode'] . "</td>";
                    echo "<td>" . strtoupper($row['Admission_Name']) . "</td>";
                    echo "<td>" . strtoupper($row['Admission_Fname']) . "</td>";
                    echo "<td>" . $dob . "</td>";
                    echo "<td>" . $row['Course_Name'] . "</td>";
                    echo "<td>" . $row['Batch_Name'] . "</td>";
                    //  echo "<td><input type='button' name=" . $row['Admission_Code'] . " id=" . $row['Admission_Code'] . " class='approvalLetter btn btn-primary' value='Show Result'/></td>";
                    echo "<td><input type='checkbox' class='viewdata' id=chk" . $row['Admission_Code'] .
                    " name=chk" . $row['Admission_Code'] . "></input></td>";
                    echo "</tr>";

                    $_Count++;
                }
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        }
    }
}

/* Action for Update Batch to current of unconfirmed learners */
if ($_action == "UpdateBatch") {

    $_UserPermissionArray = array();
    $_i = 0;
    $_Count = 0;
    $l = "";

    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 3) == 'chk') {
            $l .= substr($key, 3) . ",";
        }
        $_AdmissionArray = rtrim($l, ",");
    }
    if ($_AdmissionArray == "") {
        echo "NoDetails";
        return;
    } else {    //$response=$emp->Update($_Count);
        $response = $emp->GetCurBatch($_POST["ddlCourse"]);
        if ($response[0] == 'Success') {
            $_Row = mysqli_fetch_array($response[2]);
            $response1 = $emp->UpdateBatch($_AdmissionArray, $_POST['ddlCourse'], $_Row["Batch_Code"]);
        }
    }



    echo $response1[0];
}