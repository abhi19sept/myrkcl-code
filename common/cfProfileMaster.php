<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsProfileMaster.php';

    $response = array();
    $emp = new clsProfileMaster();
	//print_r($_POST);
    if ($_action == "ADD") {
		//print_r($_POST);
        if (isset($_POST["txtFirstName"]))
			 {  
			$_txtFirstName=$_POST["txtFirstName"];
            $_txtHouseNo = $_POST["txtHouseNo"];
            $_txtDOB = $_POST["txtDOB"];
			$_ddlMTongue = $_POST["ddlMTongue"];
            $_txtAddress = $_POST["txtAddress"];
			$_ddlGender = $_POST["ddlGender"];
            $_txtFatherName = $_POST["txtFatherName"];
			$_ddlQualification = $_POST["ddlQualification"];
			$_txtOccupation = $_POST["txtOccupation"];
			$_ddlChallenged = $_POST["ddlChallenged"];
			$_AadharNo = $_POST["txtAadharNo"];
			$_txtSTDCode = $_POST["txtSTDCode"];
           
			$_txtMobile = $_POST["txtMobile"];
			$_txtEmail = $_POST["txtEmail"];
			$_txtFaceBookId = $_POST["txtFaceBookId"];
			$_txtTwitterId = $_POST["txtTwitterId"];
           
			$response = $emp->Add($_txtFirstName,$_txtHouseNo,$_txtDOB,$_ddlMTongue,$_txtAddress,$_ddlGender,$_txtFatherName,$_ddlQualification,$_txtOccupation,
			$_ddlChallenged,$_AadharNo,$_txtSTDCode,$_txtMobile,$_txtEmail,$_txtFaceBookId,$_txtTwitterId);
            echo $response[0];
        }
    }
    
    if ($_action == "UPDATE") {
        if (isset($_POST["code"])) {
			//print_r($_POST);
			$_code = $_POST["code"];
			
            $_txtFirstName=$_POST["txtFirstName"];
            $_txtHouseNo = $_POST["txtHouseNo"];
            $_txtDOB = $_POST["txtDOB"];
			$_ddlMTongue = $_POST["ddlMTongue"];
            $_txtAddress = $_POST["txtAddress"];
			$_ddlGender = $_POST["ddlGender"];
            $_txtFatherName = $_POST["txtFatherName"];
			$_ddlQualification = $_POST["ddlQualification"];
			$_txtOccupation = $_POST["txtOccupation"];
			$_ddlChallenged = $_POST["ddlChallenged"];
			$_AadharNo = $_POST["txtAadharNo"];
			$_txtSTDCode = $_POST["txtSTDCode"];
           
			$_txtMobile = $_POST["txtMobile"];
			$_txtEmail = $_POST["txtEmail"];
			$_txtFaceBookId = $_POST["txtFaceBookId"];
			$_txtTwitterId = $_POST["txtTwitterId"];
           
           
			
            $response = $emp->Update($_code,$_txtFirstName,$_txtHouseNo,$_txtDOB,$_ddlMTongue,$_txtAddress,$_ddlGender,$_txtFatherName,$_ddlQualification,$_txtOccupation,
			$_ddlChallenged,$_AadharNo,$_txtSTDCode,$_txtMobile,$_txtEmail,$_txtFaceBookId,$_txtTwitterId);
            echo $response[0];
        }
    }
    
    
    if ($_action == "EDIT") {


        $response = $emp->GetDatabyCode($_actionvalue);

        $_DataTable = array();
        $_i=0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("UserProfile_FirstName" => $_Row['UserProfile_FirstName'],
                "UserProfile_HouseNo"=>$_Row['UserProfile_HouseNo'],
                "UserProfile_DOB"=>$_Row['UserProfile_DOB'],
				 "UserProfile_MTongue"=>$_Row['UserProfile_MTongue'],
				 "UserProfile_Address"=>$_Row['UserProfile_Address'],
                "UserProfile_Gender"=>$_Row['UserProfile_Gender'],
				 "UserProfile_FatherName"=>$_Row['UserProfile_FatherName'],
				 "UserProfile_Qualification"=>$_Row['UserProfile_Qualification'],
				  "UserProfile_Occupation"=>$_Row['UserProfile_Occupation'],
                "UserProfile_PhysicallyChallenged"=>$_Row['UserProfile_PhysicallyChallenged'],
				 "UserProfile_AadharNo"=>$_Row['UserProfile_AadharNo'],
				 "UserProfile_STDCode"=>$_Row['UserProfile_STDCode'],
                "UserProfile_Mobile"=>$_Row['UserProfile_Mobile'],
				 "UserProfile_Email"=>$_Row['UserProfile_Email'],
				 "UserProfile_FaceBookId"=>$_Row['UserProfile_FaceBookId'],
				 "UserProfile_TwitterId"=>$_Row['UserProfile_TwitterId']
                );
            $_i=$_i+1;
        }
        echo json_encode($_Datatable);
    }
    
    
    if ($_action == "DELETE") {


        $response = $emp->DeleteRecord($_actionvalue);

        echo $response[0];
    }


    if ($_action == "SHOW") {


        $response = $emp->GetAll();

        $_DataTable = "";
		
		$_Row = mysqli_fetch_array($response[2]);
		echo "<div align='center' class='col-md-3 col-lg-3'>"; 
		
		echo "<img class='img-circle img-responsive' src='".$_Row['UserProfile_Image']."' style='width:200px'>";
		echo "</div>";
                
               
                echo "<div class='col-md-9 col-lg-9'>"; 

        echo "<table class='table table-user-information'>";
        echo "<tbody>";
		
        echo "<tr>";
        echo "<td style='5%'>Name.</td>";
        
		
		echo "<td style='30%'>". $_Row['UserProfile_FirstName'] . "</td>";
		echo "</tr>";
		
		
		
		 echo "<tr>";
        echo "<td style='5%'>Father Name</td>";
        
		
		echo "<td style='30%'>". $_Row['UserProfile_FatherName'] . "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td style='5%'>Date of Birth</td>";
		echo "<td style='5%'>". $_Row['UserProfile_DOB'] ."</td>";
		echo "</tr>";
	
				echo "<tr>";
	echo "<td style='5%'>Mobile</td>";
        echo "<td style='10%'>" . $_Row['UserProfile_Mobile'] . "</td>";
        echo "</tr>";
		
	

echo "<tr>";
	echo "<td style='5%'>Email</td>";
        echo "<td style='10%'>" . $_Row['UserProfile_Email'] . "</td>";
        echo "</tr>";		
		
		
        echo "</tbody>";
       
       
        echo "</table>";
		echo "<a class='btn btn-primary' href='frmprofile.php?code=" . $_Row['UserProfile_Code'] . "&Mode=Edit'   style='margin-left: 200px !important;'  >Edit My Profile</a>";
         
		 echo "</div>";
		 		 
		
		
//echo $_DataTable;
    }
    
    
 
?>
