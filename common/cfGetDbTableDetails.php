<?php

include './commonFunction.php';
require 'BAL/clsGetDbTableDetails.php';

require '../DAL/upload_ftp_doc.php';

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
date_default_timezone_set("Asia/Kolkata");
$response = array();
$emp = new clsGetDbTableDetails();

if ($_action == "GetTables") {
    $response = $emp->GetTables();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['cpy_id'] . ">" . $_Row['cpy_name'] . "</option>";
    }
}

if ($_action == "UpdateTable") {
    if ($_POST['ddltables'] == '17') {
		
		$_ObjFTPConnection = new ftpConnection();
		$abc = $_ObjFTPConnection->ftpdetails();
   
        $file = $_FILES['Eoi_file']['name'];
        $tmp = $_FILES['Eoi_file']['tmp_name'];
        $temp = explode(".", $file);
		$fileTimestamp = date("YmdHis");
         $newfilename = 'EOI_Center_List_'.$fileTimestamp.'.'.end($temp);
		
        $filestorepath = "/eoi_tnc/" . $newfilename;
        $FileType = pathinfo($filestorepath, PATHINFO_EXTENSION);
        /* if ($_FILES["Eoi_file"]["size"] > 500000) 
          {
          echo "Sorry, your file is too large.";
          } */
        if ($FileType != "xlsx" && $FileType != "csv" && $FileType != "XLSX" && $FileType != "CSV") {
            echo "Sorry, File Not Valid";
        } else {
			
			$_UploadDirectory = '/eoi_tnc/';
			
			$response_ftp = ftpUploadFile($_UploadDirectory,$newfilename,$_FILES["Eoi_file"]["tmp_name"]);
			if(trim($response_ftp) == "SuccessfullyUploaded"){
				$_fname= $newfilename;
			$_UploadDirectory1= $abc.'/eoi_tnc/'.$_fname;
			
		$csvData = file_get_contents($_UploadDirectory1);
        $lines1 = array_filter(explode(PHP_EOL, $csvData));
       
        $lines = array_slice($lines1, 1);
		$valuestring = "";
        $j = 0;
        foreach ($lines as $key => $value) {
             $temp1 = explode(",", $value);
            $result = "'" . implode ( "', '", $temp1) . "'";
           $valuestring1 = "" . ($result) . "";
            $valuestring .= trim("(" . $valuestring1 . ")");
            $j++;
            $valuestring .= (count($lines) != $j) ? "," : "";
            
        }
		$response = $emp->UpdateEoiCenterList($valuestring);
                echo $response[0];
			}
			else {
                echo "Invalid File";
            }
			
        }
    }
}
?>