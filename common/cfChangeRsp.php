<?php

/*
 * author Viveks

 */


include './commonFunction.php';
require 'BAL/clsChangeRsp.php';

$response = array();
$emp = new clsChangeRsp();
if ($_action == "CHECKITGK") {
    //print_r($_POST);
        $response = $emp->CheckITGK();
        echo $response[0];
}

if ($_action == "ADD") {
    //print_r($_POST);
    if (isset($_POST["ddlRsp"])) {
        $_Rspcode = $_POST["ddlRsp"];
        $_Rspname = $_POST["txtName1"];
        $_Rspregno = $_POST["txtRegno"];
        $_Rspestdate = $_POST["txtEstdate"];
        $_Rsptype = $_POST["txtType"];
        $_Rspdistrict = $_POST["txtDistrict"];
        $_Rsptehsil = $_POST["txtTehsil"];
        $_Rspstreet = $_POST["txtStreet"];
        $_Rsproad = $_POST["txtRoad"];
        $_RspSelectReason = $_POST["ddlReason"];
        $_RspReason = $_POST["txtReason"];


        $response = $emp->Add($_Rspcode, $_Rspname, $_Rspregno, $_Rspestdate, $_Rsptype, $_Rspdistrict, $_Rsptehsil, $_Rspstreet, $_Rsproad, $_RspSelectReason, $_RspReason);
        echo $response[0];
    }
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='60%'>RSP Name</th>";
    echo "<th style='10%'>RSP District</th>";
    echo "<th style='40%'>RSP Tehsil</th>";
    echo "<th style='40%'>RSP Application date</th>";
    echo "<th style='10%'>RSP Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Rspitgk_ItgkCode'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspname'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspdistrict'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rsptehsil'] . "</td>";            
            echo "<td>" . $_Row['Rspitgk_Date'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Status'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "GETRSP") {
    $response = $emp->GetDatabyCode();
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {

        $_DataTable[$_i] = array("rspname" => $_Row['Organization_Name'],
            "mobno" => $_Row['User_MobileNo'],
            "date" => $_Row['Organization_FoundedDate'],
            "rsptype" => $_Row['Organization_Type']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

