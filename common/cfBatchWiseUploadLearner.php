<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsBatchWiseUploadLearner.php';

$response = array();
$emp = new clsBatchWiseUploadLearner();

if ($_action == "FillAdmissionCourse") {
    $response = $emp->GetAdmissionCourse();	
    echo "<option value=''>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLAdmissionBatchcode") {
    $response = $emp->GetAdmissionBatchcode($_POST['values']);
    echo "<option value='' selected='selected'>Select Batch</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "GETDATA") {

    $response = $emp->GetLearnerData($_POST['course'], $_POST['batch']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >D.O.B</th>";
	echo "<th >Mobile NO</th>";
	echo "<th >Payment Status</th>";
	echo "<th >IT-GK Name</th>";
	echo "<th >SP Code</th>";
	echo "<th >SP Name</th>";
    
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Learner_Name'] . "</td>";
            echo "<td>" . $_Row['Father_Name'] . "</td>";
			echo "<td>" . $_Row['Learner_DOB'] . "</td>";
			echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
				if($_Row['Admission_Payment_Status']=='1'){
					$status="Payment Confirm";
				}
				else{
					$status="Not Confirmed";
				}
            echo "<td>$status</td>";
			echo "<td>" . $_Row['ITGK_Name'] . "</td>";
			echo "<td>" . $_Row['RSP_Code'] . "</td>";
			echo "<td>" . $_Row['RSP_Name'] . "</td>";
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";

}


