<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsOrgDDModify.php';

$response = array();
$emp = new clsOrgDDModify();



if ($_action == "DETAILS") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("ddno" => $_Row['dd_no'],
                "dddate" => $_Row['dd_date'],
                "ddbranch" => $_Row['dd_branch'],
                "ddamount" => $_Row['dd_amount'],
            "ddtxnid" => $_Row['dd_Transaction_Txtid']);
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "";
    }
}

if ($_action == "Update") {
    //print_r($_POST);
    if (isset($_POST["ackno"])) {
        $_AckCode = filter_var($_POST["ackno"], FILTER_SANITIZE_STRING);
        $_Code = filter_var($_POST["txnid"], FILTER_SANITIZE_STRING);
        $_TranRefNo = filter_var($_POST["txtGenerateId"], FILTER_SANITIZE_STRING);

       
        
        $_ddno = filter_var($_POST["ddno"], FILTER_SANITIZE_STRING);
        $_dddate = filter_var($_POST["dddate"], FILTER_SANITIZE_STRING);

        
        $_ddlBankDistrict = filter_var($_POST["ddlBankDistrict"], FILTER_SANITIZE_STRING);
        $_ddlBankName = filter_var($_POST["ddlBankName"], FILTER_SANITIZE_STRING);
        $_txtBranchName = filter_var($_POST["txtBranchName"], FILTER_SANITIZE_STRING);

        $response = $emp->Update($_AckCode, $_Code, $_TranRefNo, $_ddno, $_dddate, $_ddlBankDistrict, $_ddlBankName, $_txtBranchName);
        echo $response[0];
    }
}
