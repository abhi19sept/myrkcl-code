<?php

/*
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsAdmissionSummarySPMRM.php';

$response = array();
$emp = new clsAdmissionSummarySPMRM();

if ($_action == "GETDATA") {

    $response = $emp->GetDataAll($_POST['role'], $_POST['course'], $_POST['batch']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>CenterCode</th>";
    echo "<th >Uploaded Admission Count</th>";
    echo "<th >Confirm Admission Count</th>";
    ;
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;
    $_Totaleconfirm = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";

            echo "<td>" . $_Row['RoleName'] . "</td>";
            echo "<td><button type='button' class='updcount' course='" . $_Row['Course'] . "' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowUpload' value='" . $_Row['uploadcount'] . "'>" . $_Row['uploadcount'] . "</button></td>";
            echo "<td><button type='button' class='updcount' course='" . $_Row['Course'] . "' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowConfirm' value='" . $_Row['confirmcount'] . "'>" . $_Row['confirmcount'] . "</button></td>";

            echo "</tr>";
             $_Total = $_Total + $_Row['uploadcount'];
            $_Totaleconfirm = $_Totaleconfirm + $_Row['confirmcount'];
            $_Count++;
        }

        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
        echo "<th >TotalCount </th>";
        echo "<th>";
        echo "$_Total";
        echo "</th>";
        echo "<th>";
        echo "$_Totaleconfirm";
        echo "</th>";
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetLearnerList($_POST['course'], $_POST['batch'], $_POST['rolecode'], $_POST['mode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >Mobile NO</th>";
    echo "<th>Highest Qualification Document</th>";
    echo "<th> Caste Category</th>";
    echo "<th>Class X Marksheet</th>";
    echo "<th>Special Preference Category</th>";
    echo "<th>Physically Challenged Status</th>";
    echo "<th>Aadhar Details</th>";
    echo "<th>Jan Adhaar Details</th>";
    echo "<th>Ration Card Details</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            if ($_Row['bAdmission_Code'] == '') {
                echo "<td>Details Not Found</td>";
                echo "<td>Details Not Found</td>";
                echo "<td>Details Not Found</td>";
                echo "<td>Details Not Found</td>";
                echo "<td>Details Not Found</td>";
                echo "<td>Details Not Found</td>";
                echo "<td>Details Not Found</td>";
                echo "<td>Details Not Found</td>";
            } else {
                echo "<td> <input type='hidden' id='file_id" . $_Row['Admission_LearnerCode'] . "' 
                                value='" . getFilePath($_Row['Admission_LearnerCode'], 'high_quali_proof') . "'> "
                . "<button type='button' id='" . $_Row['Admission_LearnerCode'] . "' value='" . getFilePath($_Row['Admission_LearnerCode'], 'high_quali_proof') . "' 
                                   class='viewimage btn-Info btn-xs'>Show</button></td>";

                if ($_Row['Admission_Cast'] == '1') {
                    echo "<td>" . $_Row['Category_Name'] . "</td>";
                } else {
                    echo "<td> <input type='hidden' id='file_id" . $_Row['Admission_LearnerCode'] . "' 
                                value='" . getFilePath($_Row['Admission_LearnerCode'], 'Caste_Proof') . "'> <button type='button' id='" . $_Row['Admission_LearnerCode'] . "' 
                                    value='" . getFilePath($_Row['Admission_LearnerCode'], 'Caste_Proof') . "' class='viewimage btn-Info btn-xs'>" . $_Row['Category_Name'] . "</button></td>";
                }
                echo "<td> <input type='hidden' id='file_id" . $_Row['Admission_LearnerCode'] . "' 
                                value='" . getFilePath($_Row['Admission_LearnerCode'], 'class_x_proof') . "'> <button type='button' id='" . $_Row['Admission_LearnerCode'] . "' 
                                    value='" . getFilePath($_Row['Admission_LearnerCode'], 'class_x_proof') . "' class='viewimage btn-Info btn-xs'>Show</button></td>";
                if ($_Row['Admission_SPC'] == 'NA') {
                    echo "<td>" . $_Row['Admission_SPC'] . "</td>";
                } else {
                    echo "<td> <input type='hidden' id='file_id" . $_Row['Admission_LearnerCode'] . "' 
                                value='" . getFilePath($_Row['Admission_LearnerCode'], 'special_pref_proof') . "'> <button type='button' id='" . $_Row['Admission_LearnerCode'] . "' 
                                    value='" . getFilePath($_Row['Admission_LearnerCode'], 'special_pref_proof') . "' class='viewimage btn-Info btn-xs'>" . $_Row['Admission_SPC'] . "</button></td>";
                }


                echo ($_Row['Admission_Ph'] == '1') ? "<td>yes</td>" : "<td>No</td>";

                if ($_Row['Admission_AadharDoc'] == '') {
                    echo "<td>Not Uploaded</td>";
                } 
                else{
                echo "<td> <input type='hidden' id='file_id" . $_Row['Admission_LearnerCode'] . "' 
                                value='" . getFilePath($_Row['Admission_LearnerCode'], 'aadhar_proof') . "'> <button type='button' id='" . $_Row['Admission_LearnerCode'] . "' 
                                    value='" . getFilePath($_Row['Admission_LearnerCode'], 'aadhar_proof') . "' class='viewimage btn-Info btn-xs'>Show</button></td>";
                }             
                if ($_Row['Admission_JanAadhar'] == 'NA') {
                    echo "<td>" . $_Row['Admission_JanAadhar'] . "</td>";
                }
                elseif ($_Row['Admission_JanAadhar'] == '') {
                    echo "<td>Not Uploaded</td>";
                } else {
                    echo "<td> <input type='hidden' id='file_id" . $_Row['Admission_LearnerCode'] . "' 
                                value='" . getFilePath($_Row['Admission_LearnerCode'], 'janaadhar_proof') . "'> <button type='button' id='" . $_Row['Admission_LearnerCode'] . "' 
                                    value='" . getFilePath($_Row['Admission_LearnerCode'], 'janaadhar_proof') . "' class='viewimage btn-Info btn-xs'>" . $_Row['Admission_JanAadhar'] . "</button></td>";
                }                
                if ($_Row['Admission_Ration'] == 'NA') {
                    echo "<td>" . $_Row['Admission_Ration'] . "</td>";
                } 
                elseif ($_Row['Admission_Ration'] == '') {
                    echo "<td>Not Uploaded</td>";
                }
                else {
                    echo "<td> <input type='hidden' id='file_id" . $_Row['Admission_LearnerCode'] . "' 
                                value='" . getFilePath($_Row['Admission_LearnerCode'], 'ration_pref_proof') . "'> <button type='button' id='" . $_Row['Admission_LearnerCode'] . "' 
                                   value='" . getFilePath($_Row['Admission_LearnerCode'], 'ration_pref_proof') . "' class='viewimage btn-Info btn-xs'>" . $_Row['Admission_Ration'] . "</button></td>";
                }

            }
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETDATAITGK") {
    $response = $emp->GetLearnerListITGK($_POST['course'], $_POST['batch'], $_POST['rolecode']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>CenterCode</th>";

    echo "<th >Admission Count</th>";
    ;
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    $_Totaleconfirm = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";

            echo "<td>" . $_Row['RoleName'] . "</td>";

            echo "<td><button type='button' class='updcount' course='" . $_Row['Course'] . "' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowConfirm' value='" . $_Row['confirmcount'] . "'>" . $_Row['confirmcount'] . "</button></td>";

            echo "</tr>";
            $_Totaleconfirm = $_Totaleconfirm + $_Row['confirmcount'];
            $_Count++;
        }

        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
        echo "<th >TotalCount </th>";

        echo "<th>";
        echo "$_Totaleconfirm";
        echo "</th>";
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

function getFilePath($learnerCode, $type) {
    $fileName = '';
    switch ($type) {
        case 'high_quali_proof' : $dir = 'ShyamaPrasadSchemeDocs';
            $name = '_HighQulification';
            break;
        case 'Caste_Proof' : $dir = 'ShyamaPrasadSchemeDocs';
            $name = '_CasteProof';
            break;
        case 'class_x_proof' : $dir = 'ShyamaPrasadSchemeDocs';
            $name = '_ClassXCert';
            break;
        case 'special_pref_proof' : $dir = 'ShyamaPrasadSchemeDocs';
            $name = '_SpecialPref';
            break;
        case 'aadhar_proof' : $dir = 'ShyamaPrasadSchemeDocs';
            $name = '_AadharDoc';
            break;
        case 'janaadhar_proof' : $dir = 'ShyamaPrasadSchemeDocs';
            $name = '_JanAadharDoc';
            break;
        case 'rationu_proof' : $dir = 'ShyamaPrasadSchemeDocs';
            $name = '_RationDoc';
            break;
    }

    $filePath = '../upload/' . $dir . '/' . $learnerCode . $name . '.jpg';
    $fileInfo = pathinfo($filePath);
    $fileName = $fileInfo['basename'];

    return $fileName;
    //     $OwnershipDoc = $learnerCode . $name . '.jpg';
    // $photopath = '/ShyamaPrasadSchemeDocs/'
    // $_ObjFTPConnection = new ftpConnection(); 
    // $details= $_ObjFTPConnection->ftpdetails();
    // $OwnershipImg =  file_get_contents($details.$photopath.$OwnershipDoc);
    // $OwnershipData = base64_encode($OwnershipImg);
    // $showOwnership =  "<iframe id='ftpimg' src='data:image/png;base64, ".$OwnershipData."' alt='Red dot' width='100%' height='500px'></iframe>";
    // echo $showOwnership ;
}
