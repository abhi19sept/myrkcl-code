<?php

/* 
 *  author Viveks

 */

include './commonFunction.php';
require 'BAL/clsAdmission.php';

$response = array();
$emp = new clsAdmission();


if ($_action == "ADD") {
    if (isset($_POST["lname"])) {

        $_LearnerTitle = $_POST["learnertitle"];
        $_LearnerName = $_POST["lname"];
        $_ParentTitle = $_POST["parenttitle"];
        $_ParentName = $_POST["parent"];
        $_IdProof = $_POST["idproof"];
        $_IdNo = $_POST["idno"];
        $_DOB = $_POST["dob"];
        $_MotherTongue = $_POST["mothertongue"];
        $_Medium = $_POST["medium"];
        $_Gender = $_POST["gender"];
        $_MaritalStatus = $_POST["marital_type"];
        $_Area = $_POST["area"];
        $_HouseNo = $_POST["houseno"];
        $_StreetNo = $_POST["streetno"];
        $_Road = $_POST["road"];
        $_ResiPh = $_POST["resiph"];
        $_Mobile = $_POST["mobile"];
        
        $_Qualification = $_POST["qualification"];
        $_LearnerType = $_POST["learnertype"];
        $_PhysicallyChallenged = $_POST["physicallychallenged"];
        
        $_Email = $_POST["email"];
        $_LearnerPhoto = $_POST["lphoto"];
        $_LearnerSign = $_POST["lsign"];
        

        $response = $emp->Add($_LearnerTitle,$_LearnerName,$_ParentTitle,$_ParentName,$_IdProof,$_IdNo,$_DOB,$_MotherTongue,$_Medium,$_Gender,$_MaritalStatus,
                $_Area,$_HouseNo,$_StreetNo,$_Road,$_ResiPh,$_Mobile,$_Qualification,$_LearnerType,$_PhysicallyChallenged,$_Email,$_LearnerPhoto,$_LearnerSign);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["lname"])) {

        $_LearnerTitle = $_POST["learnertitle"];
        $_LearnerName = $_POST["lname"];
        $_ParentTitle = $_POST["parenttitle"];
        $_ParentName = $_POST["parent"];
        $_IdProof = $_POST["idproof"];
        $_IdNo = $_POST["idno"];
        $_DOB = $_POST["dob"];
        $_MotherTongue = $_POST["mothertongue"];
        $_Medium = $_POST["medium"];
        $_Gender = $_POST["gender"];
        $_MaritalStatus = $_POST["marital_type"];
        $_Area = $_POST["area"];
        $_HouseNo = $_POST["houseno"];
        $_StreetNo = $_POST["streetno"];
        $_Road = $_POST["road"];
        $_ResiPh = $_POST["resiph"];
        $_Mobile = $_POST["mobile"];
        
        $_Qualification = $_POST["qualification"];
        $_LearnerType = $_POST["learnertype"];
        $_PhysicallyChallenged = $_POST["physicallychallenged"];
        
        $_Email = $_POST["email"];
        $_LearnerPhoto = $_POST["lphoto"];
        $_LearnerSign = $_POST["lsign"];
        
        $_Code=$_POST['code'];
        

        $response = $emp->Update($_Code,$_LearnerTitle,$_LearnerName,$_ParentTitle,$_ParentName,$_IdProof,$_IdNo,$_DOB,$_MotherTongue,$_Medium,$_Gender,$_MaritalStatus,
                $_Area,$_HouseNo,$_StreetNo,$_Road,$_ResiPh,$_Mobile,$_Qualification,$_LearnerType,$_PhysicallyChallenged,$_Email,$_LearnerPhoto,$_LearnerSign);
        echo $response[0];
    }
}

if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("AdmissionCode" => $_Row['UserProfile_Code'],
//            "learnertitle" => $_Row['UserProfile_Initial'],
            "lname" => $_Row['UserProfile_FirstName'],
//            "parenttitle" => $_Row['UserProfile_ParentTitle'],
            "fname" => $_Row['UserProfile_ParentName'],
//            "idproof" => $_Row['UserProfile_IdProoof'],
            "dob" => $_Row['UserProfile_DOB'],
//            "ddlmotherTongue" => $_Row['UserProfile_MTongue'],
//            "medium_type" => $_Row['UserProfile_Medium'],
//            "gender_type" => $_Row['UserProfile_Gender'],
//            "marital_type" => $_Row['UserProfile_Marital'],
//            "DeviceName" => $_Row['UserProfile_Area'],
            "Houseno" => $_Row['UserProfile_HouseNo'],
            "Street" => $_Row['UserProfile_Street'],
            "Road" => $_Row['UserProfile_Road'],
            "ResiPh" => $_Row['UserProfile_PhoneNo'],
            "mobile" => $_Row['UserProfile_Mobile']);
//            "DeviceName" => $_Row['UserProfile_Qualification'],
//            "DeviceName" => $_Row['UserProfile_LearnerType'],
 //          "physical_status" => $_Row['UserProfile_PhysicallyChallenged']);
//            "DeviceName" => $_Row['UserProfile_Image'],
//            "DeviceName" => $_Row['UserProfile_Sign']);
        
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}

