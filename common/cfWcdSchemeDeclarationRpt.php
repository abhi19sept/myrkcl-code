<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsWcdSchemeDeclarationRpt.php';

$response = array();
$emp = new clsWcdSchemeDeclarationRpt();



if ($_action == "GETDATA") {
		$response = $emp->GetDetails();
		
		$_DataTable = "";
		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
		echo "<thead>";
			echo "<tr>";
				echo "<th>S No.</th>";
				echo "<th>IT-GK Code</th>";
				echo "<th >IT-GK Name </th>";
				echo "<th >District</th>";
				echo "<th >Tehsil </th>";
				echo "<th >RSP Code</th>";
				echo "<th >RSP Name</th>";
				echo "<th >Drinking Water Facility (Yes/No)</th>";
				echo "<th >Toilet Facility (Yes/No)</th>";
				echo "<th >Applied Date</th>";

			echo "</tr>";
		echo "</thead>";
			echo "<tbody>";
			$_Count = 1;
			$co = mysqli_num_rows($response[2]);
				if ($co) {
					while ($_Row = mysqli_fetch_array($response[2])) {
						echo "<tr class='odd gradeX'>";
							echo "<td>" . $_Count . "</td>";
							echo "<td>" . $_Row['wcd_scheme_Itgk_code'] . "</td>";
							echo "<td>" . $_Row['ITGK_Name'] . "</td>";
							echo "<td>" . $_Row['District_Name'] . "</td>";
							echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
							echo "<td>" . $_Row['RSP_Code'] . "</td>";
							echo "<td>" . $_Row['RSP_Name'] . "</td>";
							echo "<td>" . $_Row['wcd_scheme_Ques_1'] . "</td>";
							echo "<td>" . $_Row['wcd_scheme_Ques_2'] . "</td>";
							echo "<td>" . date('Y-m-d',strtotime($_Row['datetime'])) . "</td>";							
						echo "</tr>";
			$_Count++;
					}

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } 
}