<?php

/* 
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsRspTarget.php';

$response = array();
$emp = new clsRspTarget();


if ($_action == "ADD") {
    if (isset($_POST["ddlDistrict"])) {
        $_District = $_POST["ddlDistrict"];
        $_TwoMonthRural=$_POST["txtTwoMonthRural"];
        $_TwoMonthUrban = $_POST["txtTwoMonthUrban"];
        $_FourMonthRural=$_POST["txtFourMonthRural"];
        $_FourMonthUrban = $_POST["txtFourMonthUrban"];
        $_SixMonthRural=$_POST["txtSixMonthRural"];
        $_SixMonthUrban = $_POST["txtSixMonthUrban"];
        $_Total=$_POST["Total"];

        $response = $emp->Add($_District,$_TwoMonthRural,$_TwoMonthUrban,$_FourMonthRural,$_FourMonthUrban,$_SixMonthRural,$_SixMonthUrban,$_Total);
        echo $response[0];
    }
}

//if ($_action == "DELETE") {
//
//
//    $response = $emp->DeleteRecord($_actionvalue);
//
//    echo $response[0];
//}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>District Name</th>";
    echo "<th style='40%'>First Quarter Rural</th>";
    echo "<th style='10%'>First Quarter Urban</th>";
    echo "<th style='40%'>Second Quarter Rural</th>";
    echo "<th style='10%'>Second Quarter Urban</th>";
    echo "<th style='40%'>Next 6 Month Rural</th>";
    echo "<th style='10%'>Next 6 Month Urban</th>";
    echo "<th style='40%'>Total ITGKs in 12 months</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_2MonthRural'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_2MonthUrban'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_4MonthRural'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_4MonthUrban'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_6MonthRural'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_6MonthUrban'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_Total'] . "</td>";
//        echo "<td><a href='frmBankMaster.php?code=" . $_Row['Bank_Code'] . "&Mode=Edit'>"
//                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
//        . "<a href='frmBankMaster.php?code=" . $_Row['Bank_Code'] . "&Mode=Delete'>"
//                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
     echo "</div>";
}

//$_POST["action"]
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Bank Name</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Bank_Code'] . ">" . $_Row['Bank_Name'] . "</option>";
    }
}