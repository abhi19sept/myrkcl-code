<?php

/*
 * author Viveks

 */
include 'commonFunction.php';
require 'BAL/clsEoiConfig.php';

$response = array();
$emp = new clsEoiConfig();


if ($_action == "ADD") {
    if (isset($_POST["txtEoiName"])) {
        $_EoiName = $_POST["txtEoiName"];
        $_Course = $_POST["ddlCourse"];
        $_SDate = $_POST["txtstartdate"];
        $_EDate = $_POST["txtenddate"];
        $_PFees = $_POST["txtPFees"];
        $_CFees = $_POST["txtCFees"];
        $_TncDoc=$_POST["txtGenerateId"];
        $filepng = $_FILES['tncdoc']['name'];
        $tmppng = $_FILES['tncdoc']['tmp_name'];
        $temppng = explode(".", $filepng);
        $newfilenamepng = $_TncDoc.'_tnc.'. end($temppng);
        $filestorepathpng = "../upload/eoi_tnc/" . $newfilenamepng;
        $FileTypepng = pathinfo($filestorepathpng, PATHINFO_EXTENSION);
        if ($FileTypepng != "png" && $FileTypepng != "PNG" && $FileTypepng != "jpg" && $FileTypepng != "JPG" && $FileTypepng != "pdf" && $FileTypepng != "PDF") {
            echo "Sorry, Terms and Condition File Not Valid";
        } else {
            if (move_uploaded_file($tmppng, $filestorepathpng)) {
                $file = $_FILES['_file']['name'];
                $tmp = $_FILES['_file']['tmp_name'];
                $temp = explode(".", $file);
                $newfilename = $_TncDoc.'.' . end($temp);
                $filestorepath = "../upload/eoi_tnc/" . $newfilename;
                $FileType = pathinfo($filestorepath, PATHINFO_EXTENSION);
                /* if ($_FILES["Eoi_file"]["size"] > 500000) 
                  {
                  echo "Sorry, your file is too large.";
                  } */
                if ($FileType != "xlsx" && $FileType != "csv" && $FileType != "XLSX" && $FileType != "CSV" && $FileType != "xls" && $FileType != "XLS") {
                    echo "Sorry, CSV File Not Valid";
                } else {
                    if (move_uploaded_file($tmp, $filestorepath)) {
                        
                        $response = $emp->Add($_EoiName, $_Course, $_SDate, $_EDate, $_PFees, $_CFees, $_TncDoc, $newfilenamepng);
                        if ($response[0] != Message::DuplicateRecord) {
                            $responses = $emp->GetMaxCode();
                            $_Code = mysqli_fetch_array($responses[2]);
                            
                            $upfile = fopen($filestorepath, "r");
                            $rows = "";
                            
                            while (($line = fgetcsv($upfile, 1000, ",")) !== FALSE) {
                                $rows .= "('".$_Code["Code"]."','" . $line[0] . "','" . $line[1] . "','" . $line[2] . "','" . $line[3] . "','" . $line[4] . "','" . $line[5] . "','" . $line[6] . "'),";
                            }
                             $rows = rtrim($rows, ",");
                            
                            $response = $emp->Addlist($rows);
                            echo $response[0];
                           
                        }else{
                            echo "Duplicate Record Available";
                        }
                        
                        
                    } else {
                        echo "Invalid File";
                    }
                }
            }
        }
         
         
        //$_ECL = $_POST["eclist"];
         //$_TncDoc=$_POST["txtGenerateId"];


//        $_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/eoi_tnc/';
//
//        $parentid = $_POST['txtEoiName'];
//        if ($_FILES["tncdoc"]["name"] != '') {
//            $imag = $_FILES["tncdoc"]["name"];
//            $imageinfo = pathinfo($_FILES["tncdoc"]["name"]);
//            if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg") {
//                $error_msg = "Image must be in either PNG or JPG Format";
//                $flag = 0;
//            } else {
//                if (file_exists("$_UploadDirectory1/" . $parentid . '_tnc' . '.' . substr($imag, -3))) {
//                    $_FILES["tncdoc"]["name"] . "already exists";
//                } else {
//                    if (file_exists($_UploadDirectory1)) {
//                        if (is_writable($_UploadDirectory1)) {
//                            move_uploaded_file($_FILES["tncdoc"]["tmp_name"], "$_UploadDirectory1/" . $parentid . '_tnc' . '.jpg');
//                            //session_start();
//                            // $_SESSION['PaymentReceiptFile'] = $parentid . '_tnc' . '.jpg';
//                        } else {
//                            outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
//                        }
//                    } else {
//                        outputJSON("<span class='error'>Upload Folder Not Available</span>");
//                    }
//                }
//            }
//        }




//        if (isset($_SESSION['DataArray'])) {
//            $response = $emp->Add($_EoiName, $_Course, $_SDate, $_EDate, $_PFees, $_CFees, $_TncDoc);
//            if ($response[0] != Message::DuplicateRecord) {
//                $response = $emp->GetMaxCode();
//
//                $_Code = mysqli_fetch_array($response[2]);
//                for ($i = 1; $i <= count($_SESSION['DataArray']); $i++) {
//                    //echo $_SESSION['DataArray'][$i][0];
//                    //$response = $emp->Add('UnBlock', '2', $_SESSION['DataArray'][$i][0], $_BlockDate, $_BlockCategory, $_SESSION['DataArray'][$i][2], $_UploadId, $_Duration, $_DurationType);
//                    $response = $emp->Addlist($_Code['Code'],$_SESSION['DataArray'][$i][0]);
//                    //echo $response[0] . " Row : - " . $i . "<br>";
//                    //echo $response[0];
//                }
//            }
//        }
//        echo $response[0];
    }
    //$response = $emp->Add($_EoiName,$_Course,$_SDate,$_EDate,$_PFees,$_CFees,$_ECL,$_TncDoc);
    //echo $response[0];   
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_EoiName = $_POST["name"];
        $_Course = $_POST["course"];
        $_SDate = $_POST["startdate"];
        $_EDate = $_POST["enddate"];
        $_PFees = $_POST["pfees"];
        $_CFees = $_POST["cfees"];
        $_ECL = $_POST["eclist"];
        $_TncDoc = $_POST["tncdoc"];
        $_Code = $_POST['code'];
        $response = $emp->Update($_Code, $_EoiName, $_Course, $_SDate, $_EDate, $_PFees, $_CFees, $_ECL, $_TncDoc);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("EOICode" => $_Row['EOI_Name'],
            "SDate" => $_Row['EOI_StartDate'],
            "EDate" => $_Row['EOI_EndDate'],
            "PFees" => $_Row['EOI_ProcessingFee'],
            "CFees" => $_Row['EOI_CourseFee'],
            "EOI_TNC" => $_Row['EOI_TNC'],
            "CName" => $_Row['Course_Name'],
            "CCode" => $_Row['EOI_Course']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<table border='0' cellpedding='0' cellspacing='0' width='100%' class='gridview'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Error Name</th>";
    echo "<th style='40%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Error_Name'] . "</td>";
        echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmErrorCodemaster.php?code=" . $_Row['Error_Code'] . "&Mode=Edit'>"
        . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmErrorCodemaster.php?code=" . $_Row['Error_Code'] . "&Mode=Delete'>"
        . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select EOI</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['EOI_Code'] . ">" . $_Row['EOI_Name'] . "</option>";
    }
}