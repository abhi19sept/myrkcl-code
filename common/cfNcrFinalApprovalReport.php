<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsNcrFinalApprovalReport.php';

$response = array();
$emp = new clsNcrFinalApprovalReport();


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>AO Code</th>";
    echo "<th style='20%'>IT-GK Code</th>";
	echo "<th style='20%'>IT-GK Password</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='10%'>IT-GK Type</th>";
    echo "<th style='40%'>IT-GK Mob No</th>"; 
    echo "<th style='40%'>IT-GK Email Id</th>";
    echo "<th style='40%'>IT-GK Address</th>";
   
    echo "<th style='40%'>IT-GK District</th>";
    echo "<th style='10%'>IT-GK Tehsil</th>";
    echo "<th style='10%'>IT-GK Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['User_Ack'] . "</td>";
            echo "<td>" . $_Row['User_LoginId'] . "</td>";
			echo "<td>" . $_Row['User_Password'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Type'] . "</td>";
            echo "<td>" . $_Row['User_MobileNo'] . "</td>";
            echo "<td>" . $_Row['User_EmailId'] . "</td>";
            echo "<td>" . $_Row['Organization_Address'] . "</td>";
            echo "<td>" . $_Row['Organization_District_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";
            
            if ($_Row['User_UserRoll'] == '15' && ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11)) {
                echo "<td>" . 'Pending for Final Approval' . "</td>";
            }elseif ($_Row['User_UserRoll'] == '15' && $_SESSION['User_UserRoll'] == 14 ) {
                echo "<td>" . 'Pending for Final Approval' . "</td>";
            } 
            elseif ($_Row['User_UserRoll'] == '7' ) {
                echo "<td>" . 'Approved' . "</td>";
            } else {
                echo "<td>" . 'NA' . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}