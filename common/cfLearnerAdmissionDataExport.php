<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
include './commonFunction.php';
require 'BAL/clsLearnerAdmissionDataExport.php';

$response = array();
$emp = new clsLearnerAdmissionDataExport();

if ($_action == "FillCourse") {
    $response = $emp->GetAllCourse();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FillBatch") {
    $response = $emp->GetAllBatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "SHOW") 
{
	if($_POST['course']==''){
			echo "c";
	}
	else if($_POST['batch']==''){
			echo "b";
	}
	else{
    //echo "Show";
	
    $response = $emp->GetAll($_POST['course'],$_POST['batch']);
		   $filename = "AdmissionData_" . $_POST['batch'] . ".csv";
	   $fileNamePath = "../upload/admissionlearnerdata/" . $filename;
		
    $myFile = fopen($fileNamePath, 'w');

    fputs($myFile, '"' . implode('","', array("Admission Code", "Admission LearnerCode", "ITGK Code", "BioMatric Status",
 "Aadhar Status", "Admission Date", "Admission Date LastModified", "Admission Date Payment", "Course Code",
"Batch Code","Admission Advance CourseCode","Admission Course Category","Admission Fee","Installation Mode",
"PhotoUpload Status","SignUpload Status","PhotoProcessing Status","rejection reason",
"rejection type","Payment Status","ReceiptPrint Status","Admission TranRefNo","Admission RKCL Trnid",
"Learner Name","Learner Fname","Learner DOB","Learner MTongue","Admission Scan","Admission Photo","Admission Sign",
"Learner Gender","Learner MaritalStatus","Admission Medium","Admission PH","Admission PID","Admission UID",
"Learner District","Learner Tehsil","Learner Address","Learner PIN","Learner Mobile","Learner Phone",
"Learner Email","Learner Qualification","Admission Ltype","Learner GPFNO","User Code",
"Admission RspCode","Timestamp","Admission yearid","IsNewRecord","User device id", "Course Name", "Batch Name" )) . '"' . "\n");

        while ($row1 = mysqli_fetch_row($response[2])) { 

            fputcsv($myFile, array($row1['0'], "'".$row1['1'],$row1['2'], $row1['3'], $row1['4'], $row1['5'], $row1['6'], $row1['7'],
			$row1['8'], $row1['9'], $row1['10'], $row1['11'], $row1['12'], $row1['13'], $row1['14'], $row1['15'], $row1['16'],
			$row1['17'], $row1['18'], $row1['19'], $row1['20'], $row1['21'], $row1['22'], $row1['23'], $row1['24'], $row1['25'],
			$row1['26'], $row1['27'], $row1['28'], $row1['29'], $row1['30'], $row1['31'], $row1['32'], $row1['33'], $row1['34'],
			"'".$row1['35'], $row1['36'], $row1['37'], $row1['38'], $row1['39'], $row1['40'], "'".$row1['41'], $row1['42'], $row1['43'],
			$row1['44'], $row1['45'], $row1['46'], $row1['47'], $row1['48'], $row1['49'], $row1['50'], $row1['51'], $row1['52'],
			$row1['53']), ',', '"');
        }
        fclose($myFile);
        echo "upload/admissionlearnerdata/" . $filename;
		
   
	}
}

?>