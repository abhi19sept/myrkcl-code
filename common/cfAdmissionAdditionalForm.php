<?php

/*
 * author Abhishek

 */
include './commonFunction.php';
require 'BAL/clsAdmissionAdditionalForm.php';
require 'DAL/upload_ftp_doc.php';
$response = array();
$emp = new clsAdmissionAdditionalForm();
$category = array();

if ($_action == "SHOWALL") {
    $response = $emp->GetAllLearner($_POST['batch'], $_POST['course']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";

    echo "<th style='10%'>Learner Code</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    if ($_SESSION['User_UserRoll'] != 7) {
        echo "<th style='10%'>Learner Gender</th>";
        echo "<th style='10%'>Learner Mobile</th>";
        echo "<th style='10%'>IT-GK Code</th>";
    }

    echo "<th style='8%'>Select Madarsa</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_CountGrid = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        if ($_SESSION['User_UserRoll'] != 7) {
            echo "<td>" . $_Row['Admission_Gender'] . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
        }
        // echo $_Row['abc'];
        // if(isset($_Row['abc']) && !empty($_Row['abc'])){
        if ($_Row['abc'] == "") {
            echo "<td><input type='button' data-toggle='modal' data-target='#Update' id='" . $_Row['Admission_Code'] . "' name='Edit' id='Edit' class='btn btn-primary Update' value='Fill Additional Details'/></td>";
        } else {
            echo "<td>Additional Details Already Filled</td>";
        }


        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
if ($_action == "GetExistingDetails") {
    $response = $emp->GetExistingLearnerDetails($_POST['cid']);
    $_DataTable = array();

    if (mysqli_num_rows($response[2])) {
        $_Row = mysqli_fetch_array($response[2]);
        $_DataTable[] = array("lcode" => $_Row['Admission_LearnerCode'],
            "acode" => $_Row['Admission_Code'],
            "cfname" => $_Row['Admission_Name'],
            "cfaname" => $_Row['Admission_Fname']);
    }
    echo json_encode($_DataTable);
}

if ($_action == "Update") {
//    print_r($_POST);
//    print_r($_FILES);
//    die;
    $filePhotoupd = $fileCasteProofupd = $fileSSCcertupd = $fileSpecialPrefupd = 0;
    if (isset($_POST["ddlCategory"]) && !empty($_POST["ddlCategory"]) && isset($_POST["ddlSpecialPref"]) && !empty($_POST["ddlSpecialPref"]) && isset($_POST["txtacode"]) && !empty($_POST["txtacode"]) && isset($_POST["txtlcode"]) && !empty($_POST["txtlcode"])) {
        $_ddlCategory = $_POST["ddlCategory"];
        $_ddlSpecialPref = $_POST["ddlSpecialPref"];
        $_LearnerCode = $_POST["txtlcode"];
        $_AdmCode = $_POST["txtacode"];
        $_PHstatus = $_POST["ddlPHstatus"];
        $_JanAadhar = $_POST["JanAadhar"];
        $_Ration = $_POST["Ration"];
//                $_UploadDirectory1 = '../upload/ShyamaPrasadSchemeDocs';
        $_UploadDirectory1 = '/ShyamaPrasadSchemeDocs';
//        if (file_exists($_UploadDirectory1)) {
            if (!empty($_FILES['filePhoto']['name'])) {


                if ($_FILES["filePhoto"]["name"] != '') {
                    $imag = $_FILES["filePhoto"]["name"];
                    $imageinfo = pathinfo($_FILES["filePhoto"]["name"]);
                    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                        $flag = 0;
                    } else {
                        $response_ftp1=ftpUploadFile($_UploadDirectory1,$_LearnerCode . '_HighQulification.jpg',$_FILES["filePhoto"]["tmp_name"]);
                        if(trim($response_ftp1) == "SuccessfullyUploaded"){
//                        if (move_uploaded_file($_FILES["filePhoto"]["tmp_name"], "$_UploadDirectory1/" . $_LearnerCode . '_HighQulification.jpg')) {
                            $filePhotoupd = '1';
                        } else {
                            $filePhotoupd = '0';
                            echo "Image not upload. Please try again.";
                        }
                    }
                }
            } else {
                echo "Please upload Highest Qualification Document";
            }

            if ($_ddlCategory == '1') {
                $fileCasteProofupd = '1';
            } else {
                if (!empty($_FILES['fileCasteProof']['name'])) {


                    if ($_FILES["fileCasteProof"]["name"] != '') {
                        $imag = $_FILES["fileCasteProof"]["name"];
                        $imageinfo = pathinfo($_FILES["fileCasteProof"]["name"]);
                        if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                            $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                            $flag = 0;
                        } else {
                        $response_ftp2=ftpUploadFile($_UploadDirectory1,$_LearnerCode . '_CasteProof.jpg',$_FILES["fileCasteProof"]["tmp_name"]);
                            if(trim($response_ftp2) == "SuccessfullyUploaded"){
//                            if (move_uploaded_file($_FILES["fileCasteProof"]["tmp_name"], "$_UploadDirectory1/" . $_LearnerCode . '_CasteProof.jpg')) {
                                $fileCasteProofupd = '1';
                            } else {
                                $fileCasteProofupd = '0';
                                echo "Image not upload. Please try again.";
                            }
                        }
                    }
                } else {
                    echo "Please upload Caste Proof Certificate";
                }
            }
            if (!empty($_FILES['fileSSCcert']['name'])) {


                if ($_FILES["fileSSCcert"]["name"] != '') {
                    $imag = $_FILES["fileSSCcert"]["name"];
                    $imageinfo = pathinfo($_FILES["fileSSCcert"]["name"]);
                    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                        $flag = 0;
                    } else {
                        $response_ftp3=ftpUploadFile($_UploadDirectory1,$_LearnerCode . '_ClassXCert.jpg',$_FILES["fileSSCcert"]["tmp_name"]);
                        if(trim($response_ftp3) == "SuccessfullyUploaded"){
//                        if (move_uploaded_file($_FILES["fileSSCcert"]["tmp_name"], "$_UploadDirectory1/" . $_LearnerCode . '_ClassXCert.jpg')) {
                            $fileSSCcertupd = '1';
                        } else {
                            $fileSSCcertupd = '0';
                            echo "Image not upload. Please try again.";
                        }
                    }
                }
            } else {
                echo "Please upload Class X Marksheet";
            }
            if ($_ddlSpecialPref == 'NA') {
                $fileSpecialPrefupd = '1';
            } else {
                if (!empty($_FILES['fileSpecialPref']['name']) && $_ddlSpecialPref != '1') {

                    if ($_FILES["fileSpecialPref"]["name"] != '') {
                        $imag = $_FILES["fileSpecialPref"]["name"];
                        $imageinfo = pathinfo($_FILES["fileSpecialPref"]["name"]);
                        if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                            $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                            $flag = 0;
                        } else {
                            $response_ftp4=ftpUploadFile($_UploadDirectory1,$_LearnerCode . '_SpecialPref.jpg',$_FILES["fileSpecialPref"]["tmp_name"]);
                            if(trim($response_ftp4) == "SuccessfullyUploaded"){
//                            if (move_uploaded_file($_FILES["fileSpecialPref"]["tmp_name"], "$_UploadDirectory1/" . $_LearnerCode . '_SpecialPref.jpg')) {
                                $fileSpecialPrefupd = '1';
                            } else {
                                $fileSpecialPrefupd = '0';
                                echo "Image not upload. Please try again.";
                            }
                        }
                    }
                } else {
                    echo "Please upload Special Preference Document";
                }
            }
            if (!empty($_FILES['fileAadhar']['name'])) {


                if ($_FILES["fileAadhar"]["name"] != '') {
                    $imag = $_FILES["fileAadhar"]["name"];
                    $imageinfo = pathinfo($_FILES["fileAadhar"]["name"]);
                    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                        $flag = 0;
                    } else {
                        $response_ftp5=ftpUploadFile($_UploadDirectory1,$_LearnerCode . '_Aadhar.jpg',$_FILES["fileAadhar"]["tmp_name"]);
                        if(trim($response_ftp5) == "SuccessfullyUploaded"){
//                        if (move_uploaded_file($_FILES["fileAadhar"]["tmp_name"], "$_UploadDirectory1/" . $_LearnerCode . '_Aadhar.jpg')) {
                            $fileAadharupd = '1';
                        } else {
                            $fileAadharupd = '0';
                            echo "Image not upload. Please try again.";
                        }
                    }
                }
            } else {
                echo "Please upload Aadhar Document.";
            }
            if (!empty($_FILES['fileJanAadhar']['name'])) {


                if ($_FILES["fileJanAadhar"]["name"] != '') {
                    $imag = $_FILES["fileJanAadhar"]["name"];
                    $imageinfo = pathinfo($_FILES["fileJanAadhar"]["name"]);
                    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                        $flag = 0;
                    } else {
                        $response_ftp6=ftpUploadFile($_UploadDirectory1,$_LearnerCode . '_JanAadhar.jpg',$_FILES["fileJanAadhar"]["tmp_name"]);
                        if(trim($response_ftp6) == "SuccessfullyUploaded"){
//                        if (move_uploaded_file($_FILES["fileJanAadhar"]["tmp_name"], "$_UploadDirectory1/" . $_LearnerCode . '_JanAadhar.jpg')) {
                            $JanAadharupd = '1';
                        } else {
                            $JanAadharupd = '0';
                            echo "Image not upload. Please try again.";
                        }
                    }
                }
            } else {
                
                echo "Please upload Jan Aadhar Document.";
            }
            if (isset($_Ration) && !empty($_Ration)) {
                
                if (!empty($_FILES['fileRation']['name']) && $_ddlSpecialPref != '1') {

                    if ($_FILES["fileRation"]["name"] != '') {
                        $imag = $_FILES["fileRation"]["name"];
                        $imageinfo = pathinfo($_FILES["fileRation"]["name"]);
                        if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                            $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                            $flag = 0;
                        } else {
                            $response_ftp7=ftpUploadFile($_UploadDirectory1,$_LearnerCode . '_Ration.jpg',$_FILES["fileRation"]["tmp_name"]);
                            if(trim($response_ftp7) == "SuccessfullyUploaded"){
//                            if (move_uploaded_file($_FILES["fileRation"]["tmp_name"], "$_UploadDirectory1/" . $_LearnerCode . '_Ration.jpg')) {
                                $fileRation = '1';
                            } else {
                                $fileRation = '0';
                                echo "Image not upload. Please try again.";
                            }
                        }
                    }
                } else {
                    echo "Please upload Special Preference Document";
                }
            } else {
                $_Ration="NA";
                $fileRation = '1';
            }            
            if ($filePhotoupd && $fileCasteProofupd && $fileSSCcertupd && $fileSpecialPrefupd && $fileRation && $JanAadharupd && $fileAadharupd) {
                $response = $emp->update($_ddlCategory, $_ddlSpecialPref, $_LearnerCode, $_AdmCode, $_PHstatus,$fileRation ,$JanAadharupd, $fileAadharupd, $_JanAadhar, $_Ration);
                echo $response[0];
            } else {
                $response[0] = "Details have No Updated";
            }
//        } else {
//            echo "Upload Folder Not Available";
//        }
    }
}