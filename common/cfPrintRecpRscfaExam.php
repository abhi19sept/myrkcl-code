<?php

/*clsPrintRecpRscfaExam
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsPrintRecpRscfaExam.php';
require 'DAL/upload_ftp_doc.php';
$response = array();
$emp = new clsPrintRecpRscfaExam();

if ($_action == "SHOW") {
    if (isset($_POST["startdate"])) {
        if (isset($_POST["enddate"])) {
            if(isset($_POST["type"])) {
            $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';

            
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>Sr. No.</th>";
            echo "<th> Learner Code </th>";
            echo "<th> Learner Name </th>";
            echo "<th> Father Name </th>";
            echo "<th> TransactionID </th>";
            echo "<th> Payment Date</th>";
            echo "<th>Invoice Detail</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            if($_POST["type"]=='1'){
                $response = $emp->Show($sdate, $edate);
                $_Count = 1;
                while ($row = mysqli_fetch_array($response[2])) {
                $Tdate = date("d-m-Y", strtotime($row['apply_rscfa_cert_payment_date']));

                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $row['apply_rscfa_cert_lcode'] . "</td>";
                echo "<td>" . strtoupper($row['apply_rscfa_cert_lname']) . "</td>";

                echo "<td>" . strtoupper($row['apply_rscfa_cert_fname']) . "</td>";

                echo "<td>" . $row['apply_rscfa_cert_txnid'] . "</td>";

                echo "<td>" . $Tdate . "</td>";

                if ($row['apply_rscfa_cert_txnid'] >= '2017-07-01 00:00:00') {
                    if($row['apply_rscfa_cert_payment_status']=='1'){
                        echo "<td class='org'><button type='button' data-toggle='modal' data-target='#approvalLetter' class='btn btn-primary approvalLetter' id='" . $row['apply_rscfa_cert_id'] . "' >Download Invoice</button></td>";
                    }
                    else{
                        echo "<td>Payment Not Confirmed</td>";
                    }
                } else {
                    echo "<td>Invoice Not Available</td>";
                }
                echo "</tr>";
            }
                $_Count ++;
            }
            else{
                $response = $emp->Showreexam($sdate, $edate);
                $_Count = 1;
                while ($row = mysqli_fetch_array($response[2])) {
                $Tdate = date("d-m-Y", strtotime($row['rscfa_reexam_paydate']));

                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $row['rscfa_reexam_lcode'] . "</td>";
                echo "<td>" . strtoupper($row['rscfa_reexam_lname']) . "</td>";

                echo "<td>" . strtoupper($row['rscfa_reexam_fname']) . "</td>";

                echo "<td>" . $row['rscfa_reexam_TranRefNo'] . "</td>";

                echo "<td>" . $Tdate . "</td>";

                if ($row['rscfa_reexam_paydate'] >= '2017-07-01 00:00:00') {
                    if($row['rscfa_reexam_paymentstatus']=='1'){
                        echo "<td class='org'><button type='button' data-toggle='modal' data-target='#approvalLetter' class='btn btn-primary approvalLetter2' id='" . $row['rscfa_reexam_code'] . "' >Download Invoice</button></td>";
                    }
                    else{
                        echo "<td>Payment Not Confirmed</td>";
                    }
                } else {
                    echo "<td>Invoice Not Available</td>";
                }
                echo "</tr>";
            }
                $_Count ++;
            }


            echo "</tbody>";
            echo "<tfoot>";

            echo "</tfoot>";
            echo "</table>";
            echo "</div>";
            //echo $html;
            }

        }
    }
}



if ($_action == "DwnldPrintRecp") {
    $path = '../upload/RscfaExamPrintRecpt/';
    $learnerCode = $_POST['cid'];
    $file = $_POST['cid'] . '.fdf';
    $filePath = $path . $file;
    $allreadyfoundAkcRecipt = $path . $learnerCode . '_RscfaExamInvoice.pdf';

    $response = $emp->GetAllPrintRecpDwnld($_POST['cid']);
    
    $co = mysqli_num_rows($response[2]);
    $res = '';
    if ($co) {
        $_Row1 = mysqli_fetch_array($response[2], true);

        $_Row1["invoice_no"] = "ADM/CFA/EXAM/" . $_Row1['invoice_no'];
        $_Row1["invoice_date"] = date("d-m-Y", strtotime($_Row1['addtime']));
        $_Row1["Lname"] = strtoupper($_Row1['apply_rscfa_cert_lname']);
        $_Row1['Fname'] = strtoupper($_Row1['apply_rscfa_cert_fname']);
        $_Row1["Lcode"] = $_Row1['apply_rscfa_cert_lcode'];
        $_Row1["Laddress"] = strtoupper($_Row1['District_Name']);
        $_Row1["Ldob"] = date("d-m-Y", strtotime($_Row1['apply_rscfa_cert_dob']));
        $_Row1["Lmobile"] = $_Row1['apply_rscfa_cert_lmobile'];

        $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
        $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
        $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
        $_Row1["Enrollmentfee"] = '250.00';
        $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'];
        $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'].'.00';
        $_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
        $_Row1["totalrkclplus"] =  $_Row1['Gst_Invoce_TutionFee'].'.00';
        $_Row1["amounttxt"] = $emp->convert_number_to_words($_Row1['Gst_Invoce_TutionFee']) . " Only";

        $_Row1["Lbatch"] = $_Row1['apply_rscfa_cert_batch'];
        $_Row1["Litgk"] = $_Row1['apply_rscfa_cert_itgk'];
        $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
        $_Row1["cgstvalue"] = "9";
        $_Row1["sgstvalue"] = "9";




        $fdfContent = '%FDF-1.2
			%Ã¢Ã£Ã?Ã“
			1 0 obj 
			<<
			/FDF 
			<<
			/Fields [';
        foreach ($_Row1 as $key => $val) {
            $fdfContent .= '
					<<
					/V (' . $val . ')
					/T (' . $key . ')
					>> 
					';
        }
        $fdfContent .= ']
			>>
			>>
			endobj 
			trailer
			<<
			/Root 1 0 R
			>>
			%%EOF';
        file_put_contents($filePath, $fdfContent);

        $fdfFilePath = $filePath;
        $fdfPath = str_replace('/', '//', $fdfFilePath);
        $resultFile = '';
        //$defaulPermissionLetterFilePath = getPermissionLetterPath() . '_ITGK_Approval_Letter.pdf';
        // echo "sunil";


        $defaulPermissionLetterFilePath = '../upload/RscfaExamPrintRecpt/_RscfaExamInvoice.pdf'; //die;


        if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
            $defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);
            $resultFile = $learnerCode . '_RscfaExamInvoice.pdf';

            $newURL = $path . $resultFile;

            //$resultPath = str_replace('/', '//', getPermitionLetterFilePath($learnerCode));
            $resultPath = str_replace('/', '//', $newURL);
            $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            // $pdftkPath = ($_SERVER['HTTP_HOST'] == "click.rkcl.in") ? '"C://inetpub//vhosts//rkcl.in//click.rkcl.in//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            //$pdftkPath = ($_SERVER['HTTP_HOST']=="10.1.1.20") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';

            $command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
            exec($command);


            // unlink($fdfFilePath);
            // $filepath5 = 'upload/RscfaExamPrintRecpt/' . $resultFile;
            // echo $filepath5;
            $filepath5 = 'common/showpdfftp.php?src=RscfaReExamPrintRecpt/' . $resultFile;
            unlink($fdfFilePath);
            $directoy = '/RscfaReExamPrintRecpt/';
            $file = $resultFile;
            $response_ftp = ftpUploadFile($directoy, $file, $newURL);
            if(trim($response_ftp) == "SuccessfullyUploaded"){
                
                echo $filepath5;
            }

            $res = "DONE";
        }
    } else {
        echo "";
    }

    function getPermissionLetterPath() {
        $path = '../upload/RscfaExamPrintRecpt/';
        makeDir($path);

        return $path;
    }

    function getPermitionLetterFilePath($code) {
        $resultFile = $code . '_RscfaExamInvoice.pdf';
        // $resultPath = getPermissionLetterPath() . $resultFile;
        $resultPath = '../upload/RscfaExamPrintRecpt/' . $resultFile;

        return $resultPath;
    }

    function makeDir($path) {
        return is_dir($path) || mkdir($path);
    }

}
if ($_action == "DwnldPrintRecpReexam") {
    $path = '../upload/RscfaReExamPrintRecpt/';
    $learnerCode = $_POST['cid'];
    $file = $_POST['cid'] . '.fdf';
    $filePath = $path . $file;
    $allreadyfoundAkcRecipt = $path . $learnerCode . '_RscfaReExamInvoice.pdf';

    $response = $emp->GetAllPrintRecpDwnldReexam($_POST['cid']);
    
    $co = mysqli_num_rows($response[2]);
    $res = '';
    if ($co) {
        $_Row1 = mysqli_fetch_array($response[2], true);

        $_Row1["invoice_no"] = "ADM/CFA/RE-EXAM/" . $_Row1['invoice_no'];
        $_Row1["invoice_date"] = date("d-m-Y", strtotime($_Row1['addtime']));
        $_Row1["Lname"] = strtoupper($_Row1['rscfa_reexam_lname']);
        $_Row1['Fname'] = strtoupper($_Row1['rscfa_reexam_fname']);
        $_Row1["Lcode"] = $_Row1['rscfa_reexam_lcode'];
        $_Row1["Laddress"] = strtoupper($_Row1['District_Name']);
        $_Row1["Ldob"] = date("d-m-Y", strtotime($_Row1['rscfa_reexam_lcode']));
        $_Row1["Lmobile"] = $_Row1['rscfa_reexam_mobile'];

        $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
        $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
        $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
        $_Row1["Enrollmentfee"] = '250.00';
        $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'];
        $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'].'.00';
        $_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
        $_Row1["totalrkclplus"] =  $_Row1['Gst_Invoce_TutionFee'].'.00';
        $_Row1["amounttxt"] = $emp->convert_number_to_words($_Row1['Gst_Invoce_TutionFee']) . " Only";

        $_Row1["Lbatch"] = $_Row1['batchname'];
        $_Row1["Litgk"] = $_Row1['itgkcode'];
        $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
        $_Row1["cgstvalue"] = "9";
        $_Row1["sgstvalue"] = "9";




        $fdfContent = '%FDF-1.2
            %Ã¢Ã£Ã?Ã“
            1 0 obj 
            <<
            /FDF 
            <<
            /Fields [';
        foreach ($_Row1 as $key => $val) {
            $fdfContent .= '
                    <<
                    /V (' . $val . ')
                    /T (' . $key . ')
                    >> 
                    ';
        }
        $fdfContent .= ']
            >>
            >>
            endobj 
            trailer
            <<
            /Root 1 0 R
            >>
            %%EOF';
        file_put_contents($filePath, $fdfContent);

        $fdfFilePath = $filePath;
        $fdfPath = str_replace('/', '//', $fdfFilePath);
        $resultFile = '';
        //$defaulPermissionLetterFilePath = getPermissionLetterPath() . '_ITGK_Approval_Letter.pdf';
        // echo "sunil";


        $defaulPermissionLetterFilePath = '../upload/RscfaReExamPrintRecpt/_RscfaReExamInvoice.pdf'; //die;


        if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
            $defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);
            $resultFile = $learnerCode . '_RscfaReExamInvoice.pdf';

            $newURL = $path . $resultFile;

            //$resultPath = str_replace('/', '//', getPermitionLetterFilePath($learnerCode));
            $resultPath = str_replace('/', '//', $newURL);
            $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            // $pdftkPath = ($_SERVER['HTTP_HOST'] == "click.rkcl.in") ? '"C://inetpub//vhosts//rkcl.in//click.rkcl.in//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            //$pdftkPath = ($_SERVER['HTTP_HOST']=="10.1.1.20") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';

            $command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
            exec($command);


            // unlink($fdfFilePath);
            // $filepath5 = 'upload/RscfaReExamPrintRecpt/' . $resultFile;
            // echo $filepath5;

            $filepath5 = 'common/showpdfftp.php?src=RscfaReExamPrintRecpt/' . $resultFile;
            unlink($fdfFilePath);
            $directoy = '/RscfaReExamPrintRecpt/';
            $file = $resultFile;
            $response_ftp = ftpUploadFile($directoy, $file, $newURL);
            if(trim($response_ftp) == "SuccessfullyUploaded"){
                
                echo $filepath5;
            }


            $res = "DONE";
        }
    } else {
        echo "";
    }

    function getPermissionLetterPath() {
        $path = '../upload/RscfaReExamPrintRecpt/';
        makeDir($path);

        return $path;
    }

    function getPermitionLetterFilePath($code) {
        $resultFile = $code . '_RscfaReExamInvoice.pdf';
        // $resultPath = getPermissionLetterPath() . $resultFile;
        $resultPath = '../upload/RscfaReExamPrintRecpt/' . $resultFile;

        return $resultPath;
    }

    function makeDir($path) {
        return is_dir($path) || mkdir($path);
    }

}
if ($_action == "delgeninvoice") {

    $path = $_SERVER['DOCUMENT_ROOT'] . '/' . $_POST['values'];
    unlink($path);
}
