<?php

include './commonFunction.php';
require 'BAL/clsCloseVisitReport.php';

$response = array();
$emp = new clsCloseVisitReport();

if ($_action == "ShowDetails") {

    //echo "Show";
    $response = $emp->GetDetails();

    $_DataTable = "";

    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'>ITGK Code</th>";
    echo "<th style='30%'>ITGK Name</th>";
     echo "<th style='35%'>ITGK District</th>";
    echo "<th style='30%'>ITGK Tehsil</th>";
    echo "<th style='20%'>SP Code</th>";
    echo "<th style='20%'>SP Name</th>";
    echo "<th style='20%'>Visit Schedule Date</th>";
    echo "<th style='20%'>Visit Close Date</th>";
    echo "<th style='20%'>Visit Close Reason</th>";
    echo "<th style='20%'>SP Employee</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['ITGKCODE'] . "</td>";
         echo "<td>" . strtoupper($_Row['ITGK_Name']) . "</td>";
         echo "<td>" . $_Row['District_Name'] . "</td>";
         echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
         echo "<td>" . $_Row['RSP_Code'] . "</td>";
         echo "<td>" . strtoupper($_Row['RSP_Name']) . "</td>";
            echo "<td>" . $_Row['visit_date'] . "</td>";
         echo "<td>" . ($_Row['close_date'] ? $_Row['close_date']  : 'NA') . "</td>";
         echo "<td>" . strtoupper($_Row['close_reason']) . "</td>";
         echo "<td>" . ($_Row['fullname'] ? strtoupper($_Row['fullname']) : 'NA'). "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}