<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsDisplaySms.php';

$response = array();
$emp = new clsDisplaySms();


//if ($_REQUEST['mode'] == "ShowConfirm") {
if ($_action == "DwldSmsLogList"){
     $response = $emp->DwldSmsLogList($_POST['smstype'],$_POST['senddate'],$_POST['rolecode']);
     $date = new DateTime();
        $fileName = "sentsmsreportbycount" . $date->getTimestamp() . ".csv";

        $fileNamePath = "../upload/gupshupmsg/" . $fileName;

        $myFile = fopen($fileNamePath, 'w');
header('Content-Encoding: UTF-8');
header('Content-type: text/csv; charset=UTF-8');
echo "\xEF\xBB\xBF"; // UTF-8 BOM
        fputs($myFile, '"' . implode('","', array('S No', 'SMS User','MyRKCL Login ID', 'Mobile No.','S No', 'SMS Type', 'SMS Text', 'Sent Date')) . '"' . "\n");
           $user_arr = array();
             $csv_row ='';
             $r = "";
             $i = 0;
 while ($row = mysqli_fetch_array($response[2])) {
    $dob = date("d-m-Y", strtotime($row['SMS_Log_SentDate']));
                    $csv_row .= '"' . $row['UserRoll_Name'] . '",';
                    $csv_row .= '"' . strtoupper($row['SMS_Log_UserLoginId']). '",';
                     $csv_row .= '"' . strtoupper($row['SMS_Log_Mobile']) . '",';
                    $csv_row .= '"' . strtoupper($row['SMS_Log_SmsTpye']). '",';
                    $csv_row .= '"' . mb_convert_encoding($row['msg'], "UTF-8"). '",';
                    $csv_row .= '"' . $dob. '",';
                    $csv_row .= "\n";
                    $i++;
}

 fwrite($myFile, $csv_row);

     $filetozippath =  realpath($fileNamePath);
    $zip = new ZipArchive;
    $zipname = rtrim($filetozippath,'.csv');
    $pieces = explode("/", $fileNamePath);
   
    $res = $zip->open($zipname.'.zip', ZipArchive::CREATE);
    if ($res === TRUE) {
       $zip->addFile($fileNamePath, $pieces[3]);
       //$zip->addFile('/path/to/index.txt', 'newname.txt');
       // $zip->addFile($pieces[3],  $filetozippath);
        $zip->close();
        $zipname = rtrim($pieces[3],'.csv').'.zip';
    }

$file_name = $zipname;
$file_url = "../upload/gupshupmsg/" . $file_name;

echo $file_url;





}

if ($_action == "FILLCAT") {
    $response = $emp->ShowCat();
    echo "<option value=''>Select Category</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['SMS_Log_SmsTpye'] . ">" . $_Row['SMS_Log_SmsTpye'] . "</option>";
    }
}

if ($_action == "SHOWMSG") {
$ddlCategory = $_POST['cat'];
    $response = $emp->GetData($ddlCategory);
    //print_r($response);
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>SMS Type</th>";
    echo "<th>SMS Text</th>";
    echo "<th>MyRKCL Login ID</th>";
    echo "<th>Mobile No.</th>";
    echo "<th>Sent Date</th>";


    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    if ($response[0] == 'Success') {
   
        $_Count = 1;

        while ($row = mysqli_fetch_array($response[2])) {
            $dob = date("d-m-Y", strtotime($row['SMS_Log_SentDate']));
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $row['SMS_Log_SmsTpye'] . "</td>";
            echo "<td>" . mb_convert_encoding($row['SMS_Log_SmsText'], "UTF-8") . "</td>";
            echo "<td>" . strtoupper($row['SMS_Log_UserLoginId']) . "</td>";
            echo "<td>" . $row['SMS_Log_Mobile'] . "</td>";
            echo "<td>" . $dob . "</td>";

            echo "</tr>";

            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "NoDetails";
    }
}
if ($_action == "SHOWrpt") {
    $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
           $date = new DateTime();
     $response = $emp->SHOWrpt($sdate, $edate);

$fileName = "sentsmsreport" . $date->getTimestamp() . ".csv";

        $fileNamePath = "../upload/gupshupmsg/" . $fileName;

        $myFile = fopen($fileNamePath, 'w');
header('Content-Encoding: UTF-8');
header('Content-type: text/csv; charset=UTF-8');
echo "\xEF\xBB\xBF"; // UTF-8 BOM
        fputs($myFile, '"' . implode('","', array('S No', 'SMS User','MyRKCL Login ID', 'Mobile No.','S No', 'SMS Type', 'SMS Text', 'Sent Date')) . '"' . "\n");
           $user_arr = array();
             $csv_row ='';
             $r = "";
             $i = 0;
 while ($row = mysqli_fetch_array($response[2])) {
    $dob = date("d-m-Y", strtotime($row['SMS_Log_SentDate']));
                    $csv_row .= '"' . $row['UserRoll_Name'] . '",';
                    $csv_row .= '"' . strtoupper($row['SMS_Log_UserLoginId']). '",';
                     $csv_row .= '"' . strtoupper($row['SMS_Log_Mobile']) . '",';
                    $csv_row .= '"' . strtoupper($row['SMS_Log_SmsTpye']). '",';
                    $csv_row .= '"' . mb_convert_encoding($row['msg'], "UTF-8"). '",';
                    $csv_row .= '"' . $dob. '",';
                    $csv_row .= "\n";
                    $i++;
}

 fwrite($myFile, $csv_row);

     $filetozippath =  realpath($fileNamePath);
    $zip = new ZipArchive;
    $zipname = rtrim($filetozippath,'.csv');
    $pieces = explode("/", $fileNamePath);
   
    $res = $zip->open($zipname.'.zip', ZipArchive::CREATE);
    if ($res === TRUE) {
       $zip->addFile($fileNamePath, $pieces[3]);
       //$zip->addFile('/path/to/index.txt', 'newname.txt');
       // $zip->addFile($pieces[3],  $filetozippath);
        $zip->close();
        $zipname = rtrim($pieces[3],'.csv').'.zip';
    }

$file_name = $zipname;
$file_url = "../upload/gupshupmsg/" . $file_name;
//header('Content-Type: application/octet-stream');
//  header("Content-Type: application/zip");
// header("Content-Transfer-Encoding: Binary"); 
// header("Content-disposition: attachment; filename=\"".$file_name."\""); 
// readfile($file_url);
// exit;
echo $file_url;

    //print_r($response);
    // echo "<div class='table-responsive'>";
    // echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    // echo "<thead>";
    // echo "<tr>";
    // echo "<th>S No.</th>";
    // echo "<th>SMS User</th>";
    // echo "<th>MyRKCL Login ID</th>";
    // echo "<th>Mobile No.</th>";
    // echo "<th>SMS Type</th>";
    // echo "<th>SMS Text</th>";

    // echo "<th>Sent Date</th>";


    // echo "</tr>";
    // echo "</thead>";
    // echo "<tbody>";
    // if ($response[0] == 'Success') {
   
    //     $_Count = 1;

    //     while ($row = mysqli_fetch_array($response[2])) {
    //         $dob = date("d-m-Y", strtotime($row['SMS_Log_SentDate']));
    //         echo "<tr class='odd gradeX'>";
    //         echo "<td>" . $_Count . "</td>";
    //         echo "<td>" . $row['UserRoll_Name'] . "</td>";
    //         echo "<td>" . strtoupper($row['SMS_Log_UserLoginId']) . "</td>";
    //         echo "<td>" . strtoupper($row['SMS_Log_Mobile']) . "</td>";
    //         echo "<td>" . strtoupper($row['SMS_Log_SmsTpye']) . "</td>";
    //         echo "<td>" . strtoupper($row['msg']) . "</td>";
          

    //         echo "<td>" . $dob . "</td>";

    //         echo "</tr>";

    //         $_Count++;
    //     }

    //     echo "</tbody>";
    //     echo "</table>";
    //     echo "</div>";
    // } else {
    //     echo "NoDetails";
    // }



}
if ($_action == "SHOWrpt2") {
    $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
           $date = new DateTime();
     $response = $emp->SHOWrpt1($sdate, $edate);
    //print_r($response);
    echo "<div class='table-responsive'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>SMS_Log_SmsTpye</th>";
    echo "<th>Count</th>";
     echo "<th>SMS Text</th>";
     echo "<th>Sent Date</th>";

    echo "<th>User Type</th>";
    


    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    if ($response[0] == 'Success') {
   
        $_Count = 1;

        while ($row = mysqli_fetch_array($response[2])) {
            $dob = date("d-m-Y", strtotime($row['SMS_Log_SentDate']));
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . strtoupper($row['SMS_Log_SmsTpye']) . "</td>";
            //echo "<td>" . strtoupper($row['cnt']) . "</td>";
            //echo "<td><a href='common/cfDisplaySms.php?smstype=" . $row['SMS_Log_SmsTpye'] . "&senddate=" . $row['SMS_Log_SentDate'] . "&rolecode=" . $row['SMS_Log_UserRole'] . "&mode=ShowConfirm'>"
            //. "" . $row['cnt'] . "</td>";
            echo "<td><input type='button' mode='" . $row['SMS_Log_UserRole'] . "' name='" . $row['SMS_Log_SmsTpye'] . "' id='" . $row['SMS_Log_SentDate'] . "' class='approvalLetter btn btn-primary btn-xs' value='" . $row['cnt'] . "'/></td>";
            echo "<td>" . mb_convert_encoding($row['msg'], "UTF-8") . "</td>";

            echo "<td>" . $dob . "</td>";  
            echo "<td>" . strtoupper($row['UserType']) . "</td>";

            echo "</tr>";

            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "NoDetails";
    }
}
