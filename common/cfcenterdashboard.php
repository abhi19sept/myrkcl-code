<?php

/* 
 * Created By Sunil Kuamr Baindara 
 * Dated: 19-7-2017
 */

include './commonFunction.php';
require 'BAL/clscenterdashboard.php';

$response = array();
$response1 = array();
$emp = new clscenterdashboard();

if ($_action == "FillRSP") {
    $response = $emp->GetAllRSP();
    echo "<option value='' selected='selected'>Please Select RSP</option>";
    $i=1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_Code'] . ">(" .$i .") ". $_Row['User_LoginId'] . " ( ".strtoupper($_Row['Organization_Name'])." )</option>";
        $i++;
    }
}

if ($_action == "FILLITGK") {
    //print_r($_POST);die;
    
    if(isset($_POST['RSP']) && $_POST['RSP']!='')
        {
             $User_Code=$_POST['RSP'];    
        }
       else
       {
            $User_Code=$_SESSION['User_Code'];
       }
    $response = $emp->GetAllITGK($User_Code);
    
    //if($_SESSION['User_UserRoll']==14){
    echo "<option value='' selected='selected'>Please Select ITGK</option>";
   // }
    $i=1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_LoginId'] . ">(" .$i .") ".$_Row['User_LoginId']. " ( ".strtoupper($_Row['Organization_Name'])." )</option>";
        $i++;
    }
}

        
        
        #####Start making small ajax function for all#########

if ($_action == "CENTERDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];
                 if($_SESSION['User_UserRoll']=='14')
                    {
                      
                      //print_r($_POST);//die; 
                       $response = $emp->checkITGK($User_LoginId);
                       //print_r($response);
                       if($response[0]!='Success'){
                         //echo "This is not your ITGK Center.";
                            echo "NOSP";                         
                         $User_LoginId='';
                       }
                    }
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
            
        $response = $emp->CenterDetails($User_LoginId);
        $num_rows = mysqli_num_rows($response[2]);
        if($num_rows==0)
	{
           //echo "NoData";
         
	}
	else
	{
            $_RowC = mysqli_fetch_array($response[2],true);
            //echo "<pre>";print_r($_RowC);die;
           /* $_RowC['Organization_Name']=strtoupper( ($_RowC['Organization_Name']=='') ? 'Not Available' : $_RowC['Organization_Name'].'sunil' );
            $_RowC['User_LoginId']=strtoupper( ($_RowC['User_LoginId']=='')?'Not Available' : $_RowC['User_LoginId']);
            $_RowC['User_EmailId']=strtoupper( ($_RowC['User_EmailId']=='') ? 'Not Available' : $_RowC['User_EmailId'] );
            $_RowC['User_MobileNo']=strtoupper( ($_RowC['User_MobileNo']=='') ? 'Not Available' : $_RowC['User_MobileNo'] );
            $_RowC['Organization_Address']=strtoupper( ($_RowC['Organization_Address']=='') ? 'Not Available' : $_RowC['Organization_Address'] );
            
            echo json_encode($_RowC);// final
           */
            
            echo "<div class='persdetablog bdrtop'>
                        <div class='persdetacont'>
                             <span>Center Name:</span>
                             <p>". strtoupper( ($_RowC['Organization_Name']=='')?'Not Available' : $_RowC['Organization_Name'] ) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Center Code:</span>
                             <p>". strtoupper( ($_RowC['User_LoginId']=='')?'Not Available' : $_RowC['User_LoginId']) . "</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Mobile:</span>
                             <p>". strtoupper( ($_RowC['User_MobileNo']=='')?'Not Available' : $_RowC['User_MobileNo']) ."</p>
                        </div>
<div class='persdetacont'>
                             <span>Center Address:</span>
                             <p  >". strtoupper( ($_RowC['Organization_Address']=='')?'Not Available' : $_RowC['Organization_Address']) ."</p>
                        </div>                        


                    </div>
                    <div class='persdetablog'>
                        
                        <div class='persdetacont peremail'>
                             <span>Email:</span>
                             <p>" . strtoupper( ($_RowC['User_EmailId']=='')?'Not Available' : $_RowC['User_EmailId']) . "</p>
                        </div>
                        
                    </div>";
	}	 
	
	
}

if ($_action == "SPDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $response = $emp->CenterDetails($User_LoginId);
        $num_rows = mysqli_num_rows($response[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
            
            
            $_RowC = mysqli_fetch_array($response[2],true);
            $responsesSP = $emp->SPDetails($_RowC["User_Rsp"]);
            
            $responseSPCount = $emp->GetAllITGK($_RowC["User_Rsp"]);
            $num_rowsSP = mysqli_num_rows($responseSPCount[2]);
            
            $responseSPName = $emp->GetRSPNAME($_RowC["User_Rsp"]);
            $_RowSPName = mysqli_fetch_array($responseSPName[2],true);
            
            $_RowSP = mysqli_fetch_array($responsesSP[2]); 
            //$_RowSP['Addresp']=strtoupper($_RowSP['Organization_HouseNo']." ".$_RowSP['Organization_Street']." ".$_RowSP['Organization_Road']." ".$_RowSP['Organization_Landmark']." ".$_RowSP['Org_formatted_address']) ; 
            //$_RowSP['Organization_FoundedDate'] = ($_RowSP['Organization_FoundedDate']=='')?'Not Available' : date("d M Y", strtotime($_RowSP['Organization_FoundedDate']));
            //$_RowSP['Organization_Name']= strtoupper( ($_RowSP['Organization_Name']=='')?'Not Available' : $_RowSP['Organization_Name']);
            //$_RowSP['Organization_RegistrationNo']= strtoupper( ($_RowSP['Organization_RegistrationNo']=='')?'Not Available' : $_RowSP['Organization_RegistrationNo']);
            //$_RowSP['noofcenter']=$num_rowsSP; 
            //$_RowSP['spname']=strtoupper( ($_RowSPName['User_LoginId']=='')?'Not Available' : $_RowSPName['User_LoginId']);
            
            //echo json_encode($_RowSP);// final
            
            
                                
                  $Addresp=strtoupper($_RowSP['Organization_HouseNo']." ".$_RowSP['Organization_Street']." ".$_RowSP['Organization_Road']." ".$_RowSP['Organization_Landmark']." ".$_RowSP['Org_formatted_address']) ; 
                    echo "<div class='persdetablog bdrtop'>
                        <div class='persdetacont'>
                             <span>SP Name:</span>
                             <p>". strtoupper( ($_RowSP['Organization_Name']=='')?'Not Available' : $_RowSP['Organization_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>No. Of Centers:</span>
                             <p>". $num_rowsSP . "</p>
                        </div>
                        </div>
                    
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>SP Code:</span>
                             <p>". strtoupper( ($_RowSPName['User_LoginId']=='')?'Not Available' : $_RowSPName['User_LoginId']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>SP Address :</span>
                             <p  >".strtoupper($Addresp). "</p>
                        </div>
                    </div>";
        	 
	}	 
	
	
}

if ($_action == "BANKDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $response = $emp->BankDetails($User_LoginId);
        $num_rows = mysqli_num_rows($response[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
            $_RowC = mysqli_fetch_array($response[2],true);
            $_RowC['Bank_Account_Name']=strtoupper( ($_RowC['Bank_Account_Name']=='')?'Not Available' : $_RowC['Bank_Account_Name']);
            $_RowC['Bank_Account_Number']=strtoupper( ($_RowC['Bank_Account_Number']=='')?'Not Available' : $_RowC['Bank_Account_Number']);
            $_RowC['Bank_Account_Type']=strtoupper( ($_RowC['Bank_Account_Type']=='')?'Not Available' : $_RowC['Bank_Account_Type']);
            $_RowC['Bank_Ifsc_code']=strtoupper( ($_RowC['Bank_Ifsc_code']=='')?'Not Available' : $_RowC['Bank_Ifsc_code']);
            $_RowC['Bank_Name']=strtoupper( ($_RowC['Bank_Name']=='')?'Not Available' : $_RowC['Bank_Name']);
            $_RowC['Bank_Micr_Code']=strtoupper( ($_RowC['Bank_Micr_Code']=='')?'Not Available' : $_RowC['Bank_Micr_Code']);
            $_RowC['Bank_Branch_Name']=strtoupper( ($_RowC['Bank_Branch_Name']=='')?'Not Available' : $_RowC['Bank_Branch_Name']);
            $_RowC['Bank_Id_Proof']=strtoupper( ($_RowC['Bank_Id_Proof']=='')?'Not Available' : $_RowC['Bank_Id_Proof']);
            $_RowC['Pan_No']=strtoupper( ($_RowC['Pan_No']=='')?'Not Available' : $_RowC['Pan_No']);
            $_RowC['Pan_Name']=strtoupper( ($_RowC['Pan_Name']=='')?'Not Available' : $_RowC['Pan_Name']);
            $_RowC['Pan_Document']= ($_RowC['Pan_Document']=='')?'noimagefound.jpg' : $_RowC['Pan_Document'];
            $_RowC['Bank_Document']= ($_RowC['Bank_Document']=='')?'noimagefound.jpg' : $_RowC['Bank_Document'];
            
            
            echo json_encode($_RowC);// final
        	 
	}	 
	
	
}

if ($_action == "LOCATIONDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $response = $emp->LocationDetails($User_LoginId);
        $num_rows = mysqli_num_rows($response[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
            $_RowC = mysqli_fetch_array($response[2],true);
            //echo "<pre>";print_r($_RowC);die;
          
               echo " <div class='persdetablog bdrtop'>
                        <div class='persdetacont'>
                             <span>State:</span>
                             <p>" . strtoupper( ($_RowC['State_Name']=='')?'Not Available' : $_RowC['State_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Division:</span>
                             <p  >". strtoupper( ($_RowC['Region_Name']=='')?'Not Available' : $_RowC['Region_Name'] ) ."</p>
                        </div>
                      </div> ";
                echo " <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>District:</span>
                             <p>" . strtoupper( ($_RowC['District_Name']=='')?'Not Available' : $_RowC['District_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Tehsil:</span>
                             <p  >". strtoupper( ($_RowC['Tehsil_Name']=='')?'Not Available' : $_RowC['Tehsil_Name']) ."</p>
                        </div>
                      </div> ";
                
                
                if($_RowC['Organization_AreaType']=='Urban' && $_RowC['Organization_AreaType']!=''){
               
 
               
                echo " <div class='persdetablog'>
                         <div class='persdetacont'>
                             <span>Area Type:</span>
                             <p  >". strtoupper( ($_RowC['Organization_AreaType']=='')?'Not Available' : $_RowC['Organization_AreaType']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Ward Number:</span>
                             <p  >". strtoupper( ($_RowC['Ward_Name']=='')?'Not Available' : $_RowC['Ward_Name']) ."</p>
                        </div>
                      </div> ";
                
                echo " <div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Municipal Type:</span>
                             <p  >". strtoupper( ($_RowC['Municipality_Type_Name']=='')?'Not Available' : $_RowC['Municipality_Type_Name']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Municipal Name:</span>
                             <p>" . strtoupper( ($_RowC['Municipality_Name']=='')?'Not Available' : $_RowC['Municipality_Name']) . "</p>
                        </div>
                      </div> ";
                
               }else{

                
                echo " <div class='persdetablog'>
                       
                        <div class='persdetacont'>
                             <span>Area Type:</span>
                             <p  >". strtoupper( ($_RowC['Organization_AreaType']=='')?'Not Available' : $_RowC['Organization_AreaType']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Panchayat Samiti:</span>
                             <p  >". strtoupper( ($_RowC['Block_Name']=='')?'Not Available' : $_RowC['Block_Name']) ."</p>
                        </div>
                         
                      </div> ";
                echo " <div class='persdetablog'>
                       <div class='persdetacont'>
                             <span>Gram Panchayat:</span>
                             <p  >". strtoupper( ($_RowC['GP_Name']=='')?'Not Available' : $_RowC['GP_Name']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Village:</span>
                             <p  >". strtoupper( ($_RowC['Village_Name']=='')?'Not Available' : $_RowC['Village_Name']) ."</p>
                        </div>
                      </div> ";
               }
                
                
                
              // echo "  </div>";
        	 
	}	 
	
	
}

if ($_action == "BLOCKUNBLOCKDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $response = $emp->GetCenterDetailReport($User_LoginId);
        $num_rows = mysqli_num_rows($response[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
            $_RowC = mysqli_fetch_array($response[2],true);
            //echo "<pre>";print_r($_RowC);die;
          
               echo " <div class='persdetablog bdrtop'>
                        <div class='persdetacont persdetacontN'>
                             <span>Center Code:</span>
                             <p>" . strtoupper( ($_RowC['CenterCode']=='')?'Not Available' : $_RowC['CenterCode']) . "</p>
                        </div>
                        <div class='persdetacont persdetacontN'>
                             <span>Center Name:</span>
                             <p  >". strtoupper( ($_RowC['CenterName']=='')?'Not Available' : $_RowC['CenterName'] ) ."</p>
                        </div>
                      </div> ";
                echo " <div class='persdetablog'>
                        <div class='persdetacont persdetacontN'>
                             <span>District:</span>
                             <p>" . strtoupper( ($_RowC['District_Name']=='')?'Not Available' : $_RowC['District_Name']) . "</p>
                        </div>
                        <div class='persdetacont persdetacontN'>
                             <span>Tehsil:</span>
                             <p  >". strtoupper( ($_RowC['Tehsil_Name']=='')?'Not Available' : $_RowC['Tehsil_Name']) ."</p>
                        </div>
                      </div> ";
                
//                echo " <div class='persdetablog'>
//                         <div class='persdetacont persdetacontN'>
//                             <span>DLC:</span>
//                             <p  >". strtoupper( ($_RowC['DLCName']=='')?'Not Available' : $_RowC['DLCName']) ."</p>
//                        </div>
//                        <div class='persdetacont persdetacontN'>
//                             <span>PSA:</span>
//                             <p  >". strtoupper( ($_RowC['PSAName']=='')?'Not Available' : $_RowC['PSAName']) ."</p>
//                        </div>
//                      </div> ";
                
                echo " <div class='persdetablog'>
                         
                        <div class='persdetacont persdetacontN'>
                             <span>Mobile:</span>
                             <p  >". strtoupper( ($_RowC['User_MobileNo']=='')?'Not Available' : $_RowC['User_MobileNo']) ."</p>
                        </div>
                        <div class='persdetacont persdetacontN'>
                             <span>Email:</span>
                             <p>" . strtoupper( ($_RowC['User_EmailId']=='')?'Not Available' : $_RowC['User_EmailId']) . "</p>
                        </div>
                      </div> ";
                
              
                
                
              // echo "  </div>";
                
                
        $responsesCD = $emp->GetCenterWiseReport($User_LoginId);
        $num_rows = mysqli_num_rows($responsesCD[2]);
        if($num_rows==0)
	{
            echo "<div class='persdetablog'>
                       <div class='division_heading'> Detail Report: </div> 
                        
                            <div>No Detail Available</div>
                        
                  </div>";
         
	}
	else
	{
                   echo "<div class='persdetablog'>
                       <div class='division_heading'> Detail Report: </div> 
                        <div class='exampannelNewHeading' style='width: 4%;'>
                            <div class='examname'>S. No.</div>
                        </div>
                        <div class='exampannelNewHeading' style='width: 13%;'>
                            <div class='examname'>Event Date</div>
                        </div>
                        <div class='exampannelNewHeading'   style='width: 13%;'>
                            <div class='examname'>Block / Unblock Date</div>
                        </div>
                        <div class='exampannelNewHeading'  style='width: 13%;'>
                            <div class='examname'>Block Type</div>
                        </div>
                        <div class='exampannelNewHeading'   style='width: 10%;'>
                            <div class='examname'>Activity</div>
                        </div>
                        <div class='exampannelNewHeading'   style='width: 10%;'>
                            <div class='examname'>Reason</div>
                        </div>
                        <div class='exampannelNewHeading'>
                            <div class='examname'>Remark</div>
                        </div>
                        <div class='exampannelNewHeading'>
                            <div class='examname'>Duration</div>
                        </div>
                       
                       
                     </div>";
                $i=1;
                while($_RowSP = mysqli_fetch_array($responsesCD[2],true))
                     {
                   
                   echo " <div class=' persdetablog1'>
                        
                        <div class='exampannelNewBU' style='width:3%'> 
                           <div class='marks font11'>". $i ."</div>
                        </div>
                        <div class='exampannelNewBU'> 
                           <div class='marks font11'>". strtoupper( ($_RowSP['eventdate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['eventdate']))) ."</div>
                        </div>
                         <div class='exampannelNewBU'>
                           <div class='marks font11'>". strtoupper( ($_RowSP['activitydate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['activitydate']))) ."</div>
                        </div>
                         <div class='exampannelNewBU'>
                           <div class='marks font11'>". strtoupper( ($_RowSP['blocktype']=='')? 'Not Available' : $_RowSP['blocktype']) ."</div>
                        </div>
                         <div class='exampannelNewBU' style='width: 9%;'>
                           <div class='marks font11'>". strtoupper( ($_RowSP['activity']=='')? 'Not Available' : $_RowSP['activity']) ."</div>
                        </div> 
                        <div class='exampannelNewBU'  style='width: 15%'>
                           <div class='marks font11'>". strtoupper( ($_RowSP['activityreason']=='')? 'Not Available' : $_RowSP['activityreason']) ."</div>
                        </div> 
                        <div class='exampannelNewBU'  style='width: 18%'>
                           <div class='marks font11'>". strtoupper( ($_RowSP['remark']=='')? 'Not Available' : $_RowSP['remark']) ."</div>
                        </div> 
                        <div class='exampannelNewBU'>
                           <div class='marks font11'>". strtoupper( $_RowSP['duration'] . " " . $_RowSP['durationtype'] ) ."</div>
                        </div> 
                       

                    </div>";
                   $i++;
                }
             
                
        	 
	}	 
        	 
	}	 
	
	
}

if ($_action == "COURSEDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $responsesCD = $emp->CourseDetails($User_LoginId);
        $num_rows = mysqli_num_rows($responsesCD[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
                   echo "<div class='persdetablog bdrtop'>
                       <div class='exampannelNew1'>
                            <div class='examname'>S. NO.</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Course Name</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Center Login Creation Date</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Final Approval Date</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Course Renewal Date</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Course EOI Applied Date</div>
                        </div>
                       
                     </div>";
                $i=1;
                while($_RowSP = mysqli_fetch_array($responsesCD[2],true))
                     {
                   $RenewalDate =  ($_RowSP['CourseITGK_ExpireDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['CourseITGK_ExpireDate'] ." +13 month"));
                   echo " <div class=' persdetablog1'>
                        <div class='exampannelNew1'>
                           <div class='marks'>". $i ."</div>
                        </div>
                        <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['Courseitgk_Course']=='')? 'Not Available' : $_RowSP['Courseitgk_Course']) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['User_CreatedDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['User_CreatedDate']))) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['CourseITGK_UserFinalApprovalDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['CourseITGK_UserFinalApprovalDate']))) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['CourseITGK_ExpireDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['CourseITGK_ExpireDate']))) ."</div>
                        </div> 
                        <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['EOI_StartDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['EOI_StartDate']))) ."</div>
                        </div> 
                       

                    </div>";
                   $i++;
                }
             
                
        	 
	}	 
	
	
}

if ($_action == "PREMISESDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $responsesPre = $emp->PremisesDetails($User_LoginId);
        $num_rows = mysqli_num_rows($responsesPre[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
                 $_RowPre = mysqli_fetch_array($responsesPre[2]); 
                 
                    echo "<div class='persdetablog bdrtop'>
                        <div class='division_heading'> Reception Type: ". strtoupper( ($_RowPre['Premises_Receptiontype']=='')?'Not Available' : $_RowPre['Premises_Receptiontype']) . "</div>
                        
                        <div class='persdetacont'>
                             <span>Seating Capacity:</span>
                             <p>". strtoupper( ($_RowPre['Premises_seatingcapacity']=='')?'Not Available' : $_RowPre['Premises_seatingcapacity']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Area in Sq. Ft:</span>
                             <p>". strtoupper( ($_RowPre['Premises_area']=='')?'Not Available' : $_RowPre['Premises_area']) . "</p>
                        </div>
                        </div>";
                    echo "<div class='persdetablog'>
                        
                        
                        <div class='persdetacont'>
                             <span>Details:</span>
                             <p>". strtoupper( ($_RowPre['Premises_details']=='')?'Not Available' : $_RowPre['Premises_details']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        <div class='division_heading'> Parking Facility: </div> 
                        <div class='persdetacont'>
                             <span>Parking Facility For Customers:</span>
                             <p>". strtoupper( ($_RowPre['Premises_parkingfacility']=='')?'Not Available' : $_RowPre['Premises_parkingfacility']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Ownership of the Parking:</span>
                             <p>". strtoupper( ($_RowPre['Premises_ownership']=='')?'Not Available' : $_RowPre['Premises_ownership']) . "</p>
                        </div>
                        
                        </div>";
                     echo "<div class='persdetablog'>
                        
                        
                        <div class='persdetacont'>
                             <span>Parking For:</span>
                             <p>". strtoupper( ($_RowPre['Premises_parkingfor']=='')?'Not Available' : $_RowPre['Premises_parkingfor']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        <div class='division_heading'> Toilet / Washroom: </div> 
                        
                        <div class='persdetacont'>
                             <span>Toilet Available:</span>
                             <p>". strtoupper( ($_RowPre['Premises_toiletavailable']=='')?'Not Available' : $_RowPre['Premises_toiletavailable']) . "</p>
                        </div>
                         <div class='persdetacont'>
                             <span>Ownership Toilet:</span>
                             <p>". strtoupper( ($_RowPre['Premises_ownershiptoilet']=='')?'Not Available' : $_RowPre['Premises_ownershiptoilet']) . "</p>
                        </div>
                        </div>";
                    echo "<div class='persdetablog'>
                        
                       
                        <div class='persdetacont'>
                             <span>Gents / Ladies Toilet:</span>
                             <p>". strtoupper( ($_RowPre['Premises_toilet_type']=='')?'Not Available' : $_RowPre['Premises_toilet_type']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'>
                        <div class='division_heading'> Other Details: </div>
                        <div class='persdetacont'>
                             <span>Premises Panatry:</span>
                             <p>". strtoupper( ($_RowPre['Premises_panatry']=='')?'Not Available' : $_RowPre['Premises_panatry']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Panatry Capacity:</span>
                             <p>". strtoupper( ($_RowPre['Premises_panatry_capacity']=='')?'Not Available' : $_RowPre['Premises_panatry_capacity']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Library Available:</span>
                             <p>". strtoupper( ($_RowPre['Premises_library_available']=='')?'Not Available' : $_RowPre['Premises_library_available']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Library Capacity:</span>
                             <p>". strtoupper( ($_RowPre['Premises_library_capacity']=='')?'Not Available' : $_RowPre['Premises_library_capacity']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Staff Available:</span>
                             <p>". strtoupper( ($_RowPre['Premises_Staff_available']=='')?'Not Available' : $_RowPre['Premises_Staff_available']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Staff Capacity:</span>
                             <p>". strtoupper( ($_RowPre['Premises_Staff_capacity']=='')?'Not Available' : $_RowPre['Premises_Staff_capacity']) . "</p>
                        </div>
                        
                        </div>";
                       
                    
                 
                       
             
                
        	 
	}	 
	
	
}

if ($_action == "COMPUTERRECOURSEDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $responsesCR = $emp->ComputerRecourseDetails($User_LoginId);
        $num_rows = mysqli_num_rows($responsesCR[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
                 echo "<div class='persdetablog bdrtop' style='padding-left:80px'>
                        <div class='exampannelNew1'>
                            <div class='examname'>S. NO.</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>System Type</div>
                        </div>
                        
                        <div class='exampannelNew1'>
                            <div class='examname'>System RAM</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>System HDD</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>System N-Computing</div>
                        </div>
                        
                     </div>";
             $i=1;
                while($_RowSP = mysqli_fetch_array($responsesCR[2],true))
                     {
                   //$RenewalDate =  ($_RowSP['CourseITGK_ExpireDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['CourseITGK_ExpireDate'] ." +13 month"));
                   echo " <div class=' persdetablog1'  style='padding-left:80px'>
                       
                        <div class='exampannelNew1'>
                           <div class='marks'>". $i ."</div>
                        </div>
                        <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['System_Type']=='')? 'Not Available' : $_RowSP['System_Type']) ."</div>
                        </div>
                        
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['System_RAM']=='')? 'Not Available' : $_RowSP['System_RAM']) ." GB</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['System_HDD']=='')? 'Not Available' : $_RowSP['System_HDD']) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['System_N-Computing']=='')? 'Not Available' : $_RowSP['System_N-Computing']) ."</div>
                        </div> 
                       
                        
                        
                        

                    </div>";
                   $i++;
                }
                       
                    
                 
                       
             
                
        	 
	}	 
	
	
}

//EmailMobileUpdateDetails
if ($_action == "EMUDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $responsesCR = $emp->EmailMobileUpdateDetails($User_LoginId);
        $num_rows = mysqli_num_rows($responsesCR[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
                 echo "<div class='persdetablog bdrtop' style='padding-left:80px'>
                        <div class='exampannelNew1'>
                            <div class='emailmobileupdate'>S. NO.</div>
                        </div>
                        <div class='exampannelNew1' style='width: 33%;'>
                            <div class='emailmobileupdate'>Email Id</div>
                        </div>
                        
                        <div class='exampannelNew1'>
                            <div class='emailmobileupdate'>Mobile Number</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='emailmobileupdate'>Time</div>
                        </div>
                       
                        
                     </div>";
             $i=1;
                while($_RowSP = mysqli_fetch_array($responsesCR[2],true))
                     {
                   //$RenewalDate =  ($_RowSP['CourseITGK_ExpireDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['CourseITGK_ExpireDate'] ." +13 month"));
                   echo " <div class=' persdetablog1'  style='padding-left:80px'>
                       
                        <div class='exampannelNew1'>
                           <div class='marks'>". $i ."</div>
                        </div>
                        <div class='exampannelNew1'  style='width: 33%;'>
                           <div class='marks'>". strtoupper( ($_RowSP['User_EmailId']=='')? 'Not Available' : $_RowSP['User_EmailId']) ."</div>
                        </div>
                        
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['User_MobileNo']=='')? 'Not Available' : $_RowSP['User_MobileNo']) ."</div>
                        </div>
                         <div class='exampannelNew1'  style='width: 22%;'>
                           <div class='marks'>". strtoupper( ($_RowSP['Timestamp']=='')? 'Not Available' : $_RowSP['Timestamp']) ."</div>
                        </div>
                        
                        

                    </div>";
                   $i++;
                }
                       
                    
                 
                       
             
                
        	 
	}	 
	
	
}

if ($_action == "HRDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $responsesHRC = $emp->HRDetailsCount($User_LoginId);
        $num_rows = mysqli_num_rows($responsesHRC[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
                //echo count($num_rows);
           
                        
            $Cou_rsp='';
            $Fac_rsp='';
            $Coo_rsp='';
            $style1='';
            $style2='';
            $style3='';
            
            $act='';
            $active1='';
            $active2='';
            $active3='';
            echo " <div class='tab'>";
            while($_RowANC = mysqli_fetch_array($responsesHRC[2],true))
                     {       $div_name=$_RowANC['Designation_Name'];
                      
                            if($num_rows==1)
                            {
                                if($div_name=='Counselor'){
                                    $Cou_rsp='Counselor';
                                    $style1="style='display: block;'";
                                    echo " <button class='tablinks active' onclick=\"openCity(event, 'Counselor')\">Counselor</button>";
                                }

                                if($div_name=='Faculty'){
                                    $Fac_rsp='Faculty';
                                    $style2="style='display: block;'";
                                    echo "<button class='tablinks active' onclick=\"openCity(event, 'Faculty')\">Faculty</button>";
                                }
                                
                                if($div_name=='Co-ordinator'){
                                    $Coo_rsp='Co-ordinator';
                                    $style3="style='display: block;'";
                                    echo "<button class='tablinks active' onclick=\"openCity(event, 'Co-ordinator')\">Co-ordinator</button>";
                                }
                            }
                            else{
                                
                                
                                if($div_name=='Counselor'){
                                    $Cou_rsp='Counselor';
                                    
                                    
                                    if($act==1){
                                      $act=1;
                                    }else{
                                       $act=1;
                                       $active1=' active';
                                       $style1="style='display: block;'";
                                    }
                                    echo " <button class='tablinks ".$active1."' onclick=\"openCity(event, 'Counselor')\">Counselor</button>";
                                
                                   
                                    }
 //echo $act;
                                else if($div_name=='Faculty'){
                                    //echo $act;
                                    $Fac_rsp='Faculty';
                                   
                                     if($act==1){
                                        $act=1;
                                     }else{
                                        $act=1;
                                        $active2=' active';
                                        $style2="style='display: block;'";
                                     }
                                    echo "<button class='tablinks ".$active2."' onclick=\"openCity(event, 'Faculty')\">Faculty</button>";
                                
                                    //echo $act;
                                     }
                                //echo $act;
                                
                                else if($div_name=='Co-ordinator'){
                                    $Coo_rsp='Co-ordinator';
                                    
                                    //echo $act;
                                    if($act==1){
                                        $act=1;
                                    }
                                    else{
                                        $act=1;
                                        $active3=' active'; 
                                        $style3="style='display: block;'";
                                     }
                                    echo "<button class='tablinks ".$active3."' onclick=\"openCity(event, 'Co-ordinator')\">Co-ordinator</button>";
                                }
                                else{
                                    
                                }   
                            }
                            
                            
                     
                     }
            echo " </div>";
            
            
             if($Cou_rsp=='Counselor'){
                      $id='Counselor'; $name='Counselor ';                  
                 echo "<div id='".$id."' class='tabcontent'   $style1>";  
                 $responsesHR = $emp->HRDetails($User_LoginId, 3);
                 $i=1; 
                 while($_RowHR = mysqli_fetch_array($responsesHR[2],true))
                  {
                     
                     echo "<div class='persdetablog bdrtop'>
                        
                        
                        <div class='persdetacont'>
                             <span>".$name." Name:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Name']=='')?'Not Available' : $_RowHR['Staff_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Designation:</span>
                             <p>". strtoupper( ($_RowHR['Designation_Name']=='')?'Not Available' : $_RowHR['Designation_Name']) . "</p>
                        </div>
                        </div>";
                    $Staff_Dob =  ($_RowHR['Staff_Dob']=='') ? 'Not Available' : date("d M Y", strtotime($_RowHR['Staff_Dob']));
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>DOB:</span>
                             <p>". strtoupper($Staff_Dob) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Gender:</span>
                             <p>". strtoupper( ($_RowHR['staff_Gender']=='') ? 'Not Available' : $_RowHR['staff_Gender']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Max. Qualification:</span>
                             <p>". strtoupper( ($_RowHR['Qualification_Name']=='') ? 'Not Available' : $_RowHR['Qualification_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Certification Details:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification2']=='') ? 'Not Available' : $_RowHR['Staff_Qualification2']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Other Qualifications:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification3']=='')?'Not Available' : $_RowHR['Staff_Qualification3']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Mobile:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Mobile']=='')?'Not Available' : $_RowHR['Staff_Mobile']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog' style='padding-bottom: 20px;'>
                        
                        <div class='persdetacont'>
                             <span> Total Work Experience:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Experience']=='')?'Not Available' : $_RowHR['Staff_Experience']) . " Year(s)</p>
                        </div>
                        <div class='persdetacont'>
                             <span style='width: 18%'>Email Id:</span>
                             <p style='width: 71%'>". strtoupper( ($_RowHR['Staff_Email_Id']=='')?'Not Available' : $_RowHR['Staff_Email_Id']) . "</p>
                        </div>
                        
                        </div>";
                    
                    
                    
                    $i++;
                     }
                 echo "</div>";  
                       
             } 
             
             if($Fac_rsp=='Faculty'){
                      $id='Faculty'; $name='Faculty ';
                     
                 echo "<div id='".$id."' class='tabcontent'   $style2>";  
                 $responsesHR = $emp->HRDetails($User_LoginId, 2);
                 $i=1; 
                 while($_RowHR = mysqli_fetch_array($responsesHR[2],true))
                  {
                     
                     echo "<div class='persdetablog bdrtop'>
                        
                        
                        <div class='persdetacont'>
                             <span>".$name." Name:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Name']=='')?'Not Available' : $_RowHR['Staff_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Designation:</span>
                             <p>". strtoupper( ($_RowHR['Designation_Name']=='')?'Not Available' : $_RowHR['Designation_Name']) . "</p>
                        </div>
                        </div>";
                    $Staff_Dob =  ($_RowHR['Staff_Dob']=='') ? 'Not Available' : date("d M Y", strtotime($_RowHR['Staff_Dob']));
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>DOB:</span>
                             <p>". strtoupper($Staff_Dob) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Gender:</span>
                             <p>". strtoupper( ($_RowHR['staff_Gender']=='') ? 'Not Available' : $_RowHR['staff_Gender']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Max. Qualification:</span>
                             <p>". strtoupper( ($_RowHR['Qualification_Name']=='') ? 'Not Available' : $_RowHR['Qualification_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Certification Details:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification2']=='') ? 'Not Available' : $_RowHR['Staff_Qualification2']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Other Qualifications:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification3']=='')?'Not Available' : $_RowHR['Staff_Qualification3']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Mobile:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Mobile']=='')?'Not Available' : $_RowHR['Staff_Mobile']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'  style='padding-bottom: 20px;'>
                        
                        <div class='persdetacont'>
                             <span> Total Work Experience:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Experience']=='')?'Not Available' : $_RowHR['Staff_Experience']) . " Year(s)</p>
                        </div>
                        <div class='persdetacont'>
                             <span style='width: 18%'>Email Id:</span>
                             <p style='width: 71%'>". strtoupper( ($_RowHR['Staff_Email_Id']=='')?'Not Available' : $_RowHR['Staff_Email_Id']) . "</p>
                        </div>
                        
                        </div>";
                    
                    
                    
                    $i++;
                     }
                 echo "</div>";  
                       
             }   
             
             if($Coo_rsp=='Co-ordinator'){
                      $id='Co-ordinator'; $name='Co-ordinator ';                      
                 echo "<div id='".$id."' class='tabcontent'  $style3 >";  
                 $responsesHR = $emp->HRDetails($User_LoginId, 1);
                 $i=1; 
                 while($_RowHR = mysqli_fetch_array($responsesHR[2],true))
                  {
                     
                     echo "<div class='persdetablog bdrtop'>
                        
                        
                        <div class='persdetacont'>
                             <span>".$name." Name:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Name']=='')?'Not Available' : $_RowHR['Staff_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Designation:</span>
                             <p>". strtoupper( ($_RowHR['Designation_Name']=='')?'Not Available' : $_RowHR['Designation_Name']) . "</p>
                        </div>
                        </div>";
                    $Staff_Dob =  ($_RowHR['Staff_Dob']=='') ? 'Not Available' : date("d M Y", strtotime($_RowHR['Staff_Dob']));
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>DOB:</span>
                             <p>". strtoupper($Staff_Dob) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Gender:</span>
                             <p>". strtoupper( ($_RowHR['staff_Gender']=='') ? 'Not Available' : $_RowHR['staff_Gender']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Max. Qualification:</span>
                             <p>". strtoupper( ($_RowHR['Qualification_Name']=='') ? 'Not Available' : $_RowHR['Qualification_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Certification Details:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification2']=='') ? 'Not Available' : $_RowHR['Staff_Qualification2']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Other Qualifications:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification3']=='')?'Not Available' : $_RowHR['Staff_Qualification3']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Mobile:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Mobile']=='')?'Not Available' : $_RowHR['Staff_Mobile']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'  style='padding-bottom: 20px;'>
                        
                        <div class='persdetacont'>
                             <span> Total Work Experience:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Experience']=='')?'Not Available' : $_RowHR['Staff_Experience']) . " Year(s)</p>
                        </div>
                        <div class='persdetacont'>
                             <span style='width: 18%'>Email Id:</span>
                             <p style='width: 71%'>". strtoupper( ($_RowHR['Staff_Email_Id']=='')?'Not Available' : $_RowHR['Staff_Email_Id']) . "</p>
                        </div>
                        
                        </div>";
                    
                    
                    
                    $i++;
                     }
                 echo "</div>";  
                       
             }    
                    
                 
                       
             
                
        	 
	}	 
	
	
}

if ($_action == "Learnerloc") {
    //echo "<pre>"; print_r($_POST);die;
    
        $User_LoginId=$_POST['itgk'];
        $response = $emp->CenterDetails($User_LoginId);
        $_RowC = mysqli_fetch_array($response[2],true);
        $cname=$_RowC['Organization_Name'];
        $address=urlencode($_RowC['Organization_Address']);
        $url1 = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyB3LDfJa6bQsDLslWSt7MdkTD45zSyw4Ac';
        $location1 = file_get_contents($url1);
        //echo "<pre>"; print_r($location1);die;
	$location1 = json_decode($location1);
        
	if($location1->status == 'OK'){
		echo $lat = $location1->results[0]->geometry->location->lat;
		echo "--";
		echo $long = $location1->results[0]->geometry->location->lng;
                echo "***";
                if(isset($location1->results[0]->formatted_address)){ 
                    echo $cname = $location1->results[0]->formatted_address;
                }else{
                   echo urldecode($address);
                }
		exit;
	}
        else{
            
        $response = $emp->LocationDetails($User_LoginId);
        $_RowC = mysqli_fetch_array($response[2],true);    
            
        if($_RowC['Organization_AreaType'] == "Urban"){
		$final_address = $_RowC['Ward_Name']." ".$_RowC['Tehsil_Name']." ".$_RowC['District_Name']." Rajasthan";
	}
	else{
		$final_address = $_RowC['Block_Name']." ".$_RowC['Tehsil_Name']." ".$_RowC['District_Name']." Rajasthan";
	}
	$final_address = implode(' ',array_unique(explode(' ', $final_address)));
	$final_address = urlencode($final_address);
	$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$final_address.'&key=AIzaSyB3LDfJa6bQsDLslWSt7MdkTD45zSyw4Ac';

	$location = file_get_contents($url);
	$location = json_decode($location);
	if($location->status == 'OK'){
		echo $lat = $location->results[0]->geometry->location->lat;
		echo "--";
		echo $long = $location->results[0]->geometry->location->lng;
                echo "***";
                //
                if(isset($location1->results[0]->formatted_address)){ 
                    echo $cname = $location1->results[0]->formatted_address;
                }else{
                   echo urldecode($final_address);
                }
		exit;
	}
	else{
		$final_address = $_RowC['Tehsil_Name']." ".$_RowC['District_Name']." Rajasthan";
		$final_address = implode(' ',array_unique(explode(' ', $final_address)));
		$final_address = urlencode($final_address);
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$final_address.'&key=AIzaSyB3LDfJa6bQsDLslWSt7MdkTD45zSyw4Ac';

		$location = file_get_contents($url);
		$location = json_decode($location);
		if($location->status == 'OK'){
			echo $lat = $location->results[0]->geometry->location->lat;
			echo "--";
			echo $long = $location->results[0]->geometry->location->lng;
                        echo "***";
                        if(isset($location1->results[0]->formatted_address)){ 
                            echo $cname = $location1->results[0]->formatted_address;
                        }else{
                           echo urldecode($final_address);
                        }
		}
		else{
			echo "fail";
		}
	}
            
            
            
            
            
            
            
        }
	
	
	
}

if ($_action == "Nearestcenter") {
	$_actionvalue = $_REQUEST['value'];
	
	$latlng = explode("--",$_actionvalue);
    $response = $emp->Nearestcenter($_actionvalue);
	$dom = new DOMDocument("1.0");
    $node = $dom->createElement("markers"); //Create new element node
    $parnode = $dom->appendChild($node); //make the node show up
    while ($_Row = mysqli_fetch_array($response[2])) {
        $node = $dom->createElement("marker");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("address", $_Row['Org_formatted_address']);
        $newnode->setAttribute("lat", $_Row['Organization_lat']);
        $newnode->setAttribute("lng", $_Row['Organization_long']);
        $newnode->setAttribute("name", $_Row['Organization_Name']);
        $newnode->setAttribute("distance", $_Row['distance']);
        $newnode->setAttribute("orgadd", $_Row['Organization_Address']);
		$sms = "IT-GK \n".$_Row['Organization_Name']."\nAddress:\n".$_Row['Organization_Address']."\n".$_Row['Org_Mobile']."\n http://maps.google.com/?q=".$_Row['Organization_lat'].",".$_Row['Organization_long'];
        $newnode->setAttribute("sms", $sms);
		// 
		// sleep(1);
    }
	echo $dom->saveXML();
	
}

if ($_action == "SLADETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $responsesCD = $emp->SLADetails($User_LoginId);
        $num_rows = mysqli_num_rows($responsesCD[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
                   echo "<div class='persdetablog bdrtop'>
                        <div class='exampannelNew1' >
                            <div class='examname'>Course Name</div>
                        </div>
                       
                        <div class='exampannelNew1' style='padding-left: 15px;'>
                            <div class='examname'>Event Name</div>
                        </div>
                        <div class='exampannelNew1' style='margin-left: 30px; width: 13%;'>
                            <div class='examname'>Total Learner Appeared</div>
                        </div>
                        <div class='exampannelNew1' style='width: 10%;'>
                            <div class='examname'>PASS</div>
                        </div>
                        <div class='exampannelNew1'  style='width: 11%;'>
                            <div class='examname'>FAIL</div>
                        </div>
                        <div class='exampannelNew1'  style='width: 11%;'>
                            <div class='examname'>ABSENTE</div>
                        </div>
                        <div class='exampannelNew1'  style='width: 12%;'>
                            <div class='examname'>% Result</div>
                        </div>
                        
                     </div>";
                $sum='';
                while($_RowSP = mysqli_fetch_array($responsesCD[2],true))
                     {
                   
                   echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew1'  style=' margin-left: 15px;margin-right: 0px; '>
                           <div class='marks'>". strtoupper( ($_RowSP['coursename']=='')? 'Not Available' : $_RowSP['coursename']) ."</div>
                        </div>
                         
                         <div class='exampannelNew1'  style=' margin-left: 15px;margin-right: 0px; '>
                           <div class='marks'>". strtoupper( ($_RowSP['eventname']=='')? 'Not Available' : $_RowSP['eventname']) ."</div>
                        </div>";
                   
                        $responsesSAL_PASS = $emp->SLADetailsResult($User_LoginId,$_RowSP['examid'],'PASS');
                        $_RowSLA_PASS= mysqli_fetch_array($responsesSAL_PASS[2],true);
                        $PASS=$_RowSLA_PASS['Allresult'];
                        
                        $responsesSAL_FAIL = $emp->SLADetailsResult($User_LoginId,$_RowSP['examid'],'FAIL');
                        $_RowSLA_FAIL= mysqli_fetch_array($responsesSAL_FAIL[2],true);
                        $FAIL=$_RowSLA_FAIL['Allresult'];
                        
                        $responsesSAL_ABSENTE = $emp->SLADetailsResult($User_LoginId,$_RowSP['examid'],'ABSENTE');
                        $_RowSLA_ABSENTE= mysqli_fetch_array($responsesSAL_ABSENTE[2],true);
                        $ABSENTE=$_RowSLA_ABSENTE['Allresult'];
                        
                        if($PASS==0 && $FAIL==0 && $ABSENTE==0)
                        {
                            echo "<div class='exampannelNew1'  style='width: 60%;margin-left: 15px;margin-right: 0px; '>
                                             <div class='marks'>No Data Available </div>
                                     </div> ";
                        
                                
                        }else{ 
                            $PASS=($PASS=='')? '0' : $PASS ;
                            $FAIL=($FAIL=='')? '0' : $FAIL ;
                            $ABSENTE=($ABSENTE=='')? '0' : $ABSENTE ;
                            $total=$PASS + $FAIL;
                            echo " <div class='exampannelNew1' style='width: 10%; margin-left: 15px;margin-right: 0px; '>
                                <div class='marks'>". $total ."</div>
                             </div> 
                             "; 
                            echo " <div class='exampannelNew1' style='width: 10%; margin-left: 15px;margin-right: 0px; '>
                                <div class='marks'>". $PASS ."</div>
                             </div> 
                             "; 
                            echo " <div class='exampannelNew1' style='width: 10%; margin-left: 15px;margin-right: 0px; '>
                                <div class='marks'>". $FAIL ."</div>
                             </div> 
                             "; 
                            echo " <div class='exampannelNew1' style='width: 10%; margin-left: 15px;margin-right: 0px; '>
                                <div class='marks'>". $ABSENTE ."</div>
                             </div> 
                             "; 
                            
                        $totoalAppeared= $PASS + $FAIL;
                        $resul_final=number_format(($PASS/$totoalAppeared)*100, 2);
                                   
                        echo "  <div class='exampannelNew1'  style='width: 13%;'>
                                <div class='marks_number'>".$resul_final." % </div>
                                </div> ";
                         

                        }

                    echo "</div>";
                }
             

	}	 
	
	
}

if ($_action == "SLAUDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $responsesCD = $emp->SLAUDetails($User_LoginId);
        $num_rows = mysqli_num_rows($responsesCD[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
                   echo "<div class='persdetablog bdrtop'>
                        <div class='exampannelNew1' >
                            <div class='examname'>Course Name</div>
                        </div>
                       
                        <div class='exampannelNew1' style='padding-left: 15px;'>
                            <div class='examname'>Event Name</div>
                        </div>
                        <div class='exampannelNew1' style='margin-left: 30px;'>
                            <div class='examname'>Total Learner Appeared</div>
                        </div>
                        <div class='exampannelNew1' style='width: 10%;'>
                            <div class='examname'>PASS</div>
                        </div>
                        <div class='exampannelNew1'  style='width: 10%;'>
                            <div class='examname'>FAIL</div>
                        </div>
                        <div class='exampannelNew1'  style='width: 10%;'>
                            <div class='examname'>ABSENTE</div>
                        </div>
                        <div class='exampannelNew1'  style='width: 10%;'>
                            <div class='examname'>% Result</div>
                        </div>
                        
                     </div>";
                $sum2='';
                while($_RowSP = mysqli_fetch_array($responsesCD[2],true))
                     {
                   
                   echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew1'  style=' margin-left: 15px;margin-right: 0px; '>
                           <div class='marks'>". strtoupper( ($_RowSP['coursename']=='')? 'Not Available' : $_RowSP['coursename']) ."</div>
                        </div>
                         
                         <div class='exampannelNew1'  style=' margin-left: 15px;margin-right: 0px; '>
                           <div class='marks'>". strtoupper( ($_RowSP['eventname']=='')? 'Not Available' : $_RowSP['eventname']) ."</div>
                        </div>
                         <div class='exampannelNew1' style='width: 12%; margin-left: 15px;margin-right: 0px; '>
                           <div class='marks'>". strtoupper( ($_RowSP['confirmcount']=='')? 'Not Available' : $_RowSP['confirmcount']) ."</div>
                        </div> 
                        "; 
                            $responsesSALresult = $emp->SLAUDetailsResult($User_LoginId,$_RowSP['examid']);
                            $num_rowsLA = mysqli_num_rows($responsesSALresult[2]);
                            if($num_rowsLA==0)
                            {
                                echo "<div class='exampannelNew1'  style='width: 47%;margin-left: 15px;margin-right: 0px; '>
                                             <div class='marks'>No Data Available </div>
                                     </div> ";

                            }
                            else
                            {
                                $i=1;
                                $pass='';
                                $totoalAppeared='';
                            while($_RowSLA= mysqli_fetch_array($responsesSALresult[2],true))
                                   {
                                        if($i==1){
                                           $pass= $_RowSLA['Allresult'];
                                        }
                                        if($i<=2){
                                           $totoalAppeared+= $_RowSLA['Allresult'];
                                        }
                                        echo "<div class='exampannelNew1' style='width: 10%;'>
                                                 <div class='marks_number'>";
                                                  echo strtoupper( ($_RowSLA['Allresult']=='')? 'Not Available' : $_RowSLA['Allresult']);
                                        echo " </div></div> ";
                                        $i++;
                                   }
                                   //$resul_final=number_format(($pass/$_RowSP['confirmcount'])*100, 2);;
                                   $resul_final=number_format(($pass/$totoalAppeared)*100, 2);
                                echo "  <div class='exampannelNew1'  style='width: 15%;'>
                                        <div class='marks_number'>".$resul_final." % </div>
                                        </div> ";
                                
                            }
                       
                        
                       $sum2+=$_RowSP['confirmcount']; 
                        

                    echo "</div>";
                }
             
                echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew2'>
                           <div class='marks' style='font-weight: bold;'>Total Count:</div>
                        </div>
                         <div class='exampannelNew2'>
                           <div class='marks'  style='font-weight: bold;'>". $sum2 ."</div>
                        </div>
                       
                    </div>";
        	 
	}	 
	
	
}

if ($_action == "ADCOUNT"){//echo "<pre>"; print_r($_POST);

     if(isset($_POST['itgk']) && $_POST['itgk']!='')
            {
                 $User_LoginId=$_POST['itgk'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
         $year=$_POST['txtYear'];
         $typeyear=$_POST['fyear'];
         $responsesAddate= $emp->SLAAdmisiionDate($User_LoginId);
         $_RowADDate = mysqli_fetch_array($responsesAddate[2],true);
         $AdmissionDate=$_RowADDate['CourseITGK_ExpireDate'];//echo "<br>"; // Actual Year
         $_EndDate = $_RowADDate['CourseITGK_ExpireDate'];            
         $_StartDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($AdmissionDate)) . " -13 months"));
         
         if($typeyear=='SLA')
          {
            //$year='2017';
            //$typeyear=$_POST['fyear'];
            //$responsesAddate= $emp->SLAAdmisiionDate($User_LoginId);
            //$_RowADDate = mysqli_fetch_array($responsesAddate[2],true);
            //$AdmissionDate=$_RowADDate['CourseITGK_ExpireDate'];//echo "<br>"; // Actual Year
            //echo $AdmissionDate='2018-07-05';//echo "<br>"; // Actual Year
            $someDay = strtotime($AdmissionDate);
            $OneYearBefore = strtotime('-1 years', $someDay);
            $OneYearBeforeMonthYear=date('F Y', $OneYearBefore);
            $OneYearBeforeMonth=date('m', $OneYearBefore);
            $OneYearBeforeYear=date('Y', $OneYearBefore);
            $currentMonthYear=date('F Y');
            $currentMonth=date('m');// current month
            $currentYear=date('Y');// current year
           
            
            $myArray = array(1 => "January ", 2 => "February ", 3 => "March ", 4 => "April ", 5 => "May ", 6 => "June ", 7 => "July ", 8 => "August ", 9 => "September ", 10 => "October ", 11 => "November ", 12 => "December ");
            $startpos = $OneYearBefore-2;
            $data1= array();
            $data2= array();
            $data= array();
            if($OneYearBeforeYear==$currentYear)
            {//echo "1";
                if($OneYearBeforeMonthYear==$currentMonthYear)
                   {
                
                        $data=date('F Y');
                
                    }
               else{//echo "<br>";
                    $newmonth=$currentMonth-$OneYearBeforeMonth;
                    $output1=array_slice($myArray,$OneYearBeforeMonth-1,$newmonth+1);

                    for($i=0;$i<count($output1); $i++){
                         $data1[$i] = isset($output1[$i]) ? "'". $output1[$i] .$currentYear ."'" : null;
                    //echo $output[$i];
                     }
                        $data=$data1;
                }
                
            }
            elseif($OneYearBeforeYear<$currentYear-1)
            {//echo "2";
                
                echo "This ITGK has not renewed.";
                
            }
            else {//echo "3";
                $output1=array_slice($myArray, $OneYearBeforeMonth-2, 12);
                //echo "<pre>"; print_r($output1);
                for($i=1;$i<count($output1); $i++){
                     $data1[$i] = isset($output1[$i]) ? "'". $output1[$i] .$OneYearBeforeYear ."'" : null;
               // echo $output[$i];
                 }
                $output2=array_slice($myArray, 0, $currentMonth);
                $coma='';
                for($i=0;$i<count($output2); $i++){
                    if($i<count($output2)){
                        $coma='$';
                    }
                     $data2[$i] = isset($output2[$i]) ? "'". $output2[$i].$currentYear ."'" : null;
               // echo $output[$i];
                 }
                    $data = array_merge($data1, $data2);
            }
            //print_r($data);
            
                $responsesAD = $emp->GetITGK_Admission_Details($User_LoginId, $_StartDate, $_EndDate);
                //$responsesAD= $emp->AdmissionDetails($User_LoginId,$year,$typeyear,$data); 
         }
         
         else
         {
                $responsesAD= $emp->AdmissionDetails($User_LoginId,$year,$typeyear,$data='');
         }
         
        $num_rows = mysqli_num_rows($responsesAD[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
        {  
            
            echo " <div class='division_heading' style='padding-top: 40px;margin-bottom:0px;'> SLA Admission Duration: </div> ";
            echo "<div class='persdetablog '>
                        <div class='exampannelNew2'>
                            <div class='examname'>Start Date: </div>
                        </div>
                        <div class='exampannelNew2'>
                            <div class='examname'>End Date:</div>
                        </div>
                     </div>";
            echo " <div class=' persdetablog1'>                       
                        <div class='exampannelNew2'>
                           <div class='marks'>".  date("d-F-Y", strtotime(date("Y-m-d", strtotime($_StartDate)) . "  +1 day")) ."</div>
                        </div>
                         <div class='exampannelNew2'>
                           <div class='marks'>". date('d-F-Y', strtotime($_EndDate)) ."</div>
                        </div>
                    </div>"; 
            
            
        if($typeyear=='Cal'){
           $nowyear='Calendar Year';
           $rosla=" <div class='division_heading' style='padding-top: 40px;margin-bottom:0px;width: 100%;float: left;'> Confirm Count in ".$nowyear." (".$year."): </div> ";
         }
         else if($typeyear=='SLA'){
             $nowyear='SLA Admission'; 
             $rosla=" <div class='division_heading' style='padding-top: 40px;margin-bottom:0px;width: 100%;float: left;'> Confirm Count in ".$nowyear." : </div> ";
         }else{
             $nowyear='Financial Year';
             $rosla=" <div class='division_heading' style='padding-top: 40px;margin-bottom:0px;width: 100%;float: left;'> Confirm Count in ".$nowyear." (".$year."): </div> ";
         }
            echo $rosla;
            echo "<div class='persdetablog '>
                        <div class='exampannelNew2'>
                            <div class='examname'>Course Name</div>
                        </div>
                        <div class='exampannelNew2'>
                            <div class='examname'>Confirm Count</div>
                        </div>
                        
                     </div>";
                $sum='';
                while($_RowSP = mysqli_fetch_array($responsesAD[2],true))
                     {
                   
                   echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew2'>
                           <div class='marks'>".  $_RowSP['Course_Name'] ."</div>
                        </div>
                         <div class='exampannelNew2'>
                           <div class='marks'>". $_RowSP['confirmcount'] ."</div>
                        </div>
                        
                        

                    </div>";
                    $sum+=$_RowSP['confirmcount'];
                    }
                    
                    echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew2'>
                           <div class='marks' style='font-weight: bold;'>Total Count:</div>
                        </div>
                         <div class='exampannelNew2'>
                           <div class='marks'  style='font-weight: bold;'>". $sum ."</div>
                        </div>
                       
                    </div>";
                    
                    echo " <div class='division_heading' style='padding-top: 80px;margin-bottom:0px;'> Month Wise Confirm Count in ".$nowyear." (".$year."): </div> ";
                    if($typeyear=='SLA')
                    {
                        //$responsesADMonth= $emp->AdmissionDetailsInMonths($User_LoginId,$year,$typeyear,$data);
                        $_EndDate = $_RowADDate['CourseITGK_ExpireDate'];            
                        $_StartDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($AdmissionDate)) . " -13 months"));
                        $responsesADMonth= $emp->GetITGK_Admission_DetailsInMonths($User_LoginId,$_StartDate,$_EndDate);
                        
                    }else{
                        $responsesADMonth= $emp->AdmissionDetailsInMonths($User_LoginId,$year,$typeyear,$data='');
                    }
                echo "<div class='persdetablog '>
                           <div class='exampannelNew2'>
                               <div class='examname'>Course Name</div>
                           </div>
                           <div class='exampannelNew2'>
                               <div class='examname'>Confirm Count</div>
                           </div>
                           <div class='exampannelNew2'>
                               <div class='examname'>Batch Name</div>
                           </div>

                        </div>";
                   $sum2='';
                   
                   while($_RowMonth = mysqli_fetch_array($responsesADMonth[2],true))
                     {
                   
                   echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew2'>
                           <div class='marks'>".  $_RowMonth['Course_Name'] ."</div>
                        </div>
                         <div class='exampannelNew2'>
                           <div class='marks'>". $_RowMonth['confirmcount'] ."</div>
                        </div>
                         <div class='exampannelNew2'>
                           <div class='marks'>". $_RowMonth['Batch_Name'] ."</div>
                        </div>
                        
                        

                    </div>";
                    $sum2+=$_RowMonth['confirmcount'];
                    }
                    
                     echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew2'>
                           <div class='marks' style='font-weight: bold;'>Total Count:</div>
                        </div>
                         <div class='exampannelNew2'>
                           <div class='marks'  style='font-weight: bold;'>". $sum2 ."</div>
                        </div>
                       
                    </div>";
            
        }
}

// Address Change Deatil
if ($_action == "ANCDETAILS") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        elseif(isset($_POST['itgk_code']) && $_POST['itgk_code']!='')
            {
                 $User_LoginId=$_POST['itgk_code'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $responsesANC = $emp->ANCDetailCount($User_LoginId);
        $num_rows = mysqli_num_rows($responsesANC[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
                //echo count($num_rows);
            $Add_rsp='';
            $Name_rsp='';
            $style1='';
            $style2='';
            echo " <div class='tab'>";
            while($_RowANC = mysqli_fetch_array($responsesANC[2],true))
                     {       $div_name=$_RowANC['fld_requestChangeType'];
                      
                            if($num_rows==1)
                            {
                                if($div_name=='address'){
                                    $Add_rsp='address';
                                    $style1="style='display: block;'";
                                    echo " <button class='tablinks active' onclick=\"openCity(event, 'Address')\">Address</button>";
                                }

                                if($div_name=='name'){
                                    $Name_rsp='name';
                                    $style2="style='display: block;'";
                                    echo "<button class='tablinks active' onclick=\"openCity(event, 'Name')\">Name</button>";
                                }
                            }
                            else{
                                $style1="style='display: block;'";
                                if($div_name=='address'){
                                         $Add_rsp='address';
                                         echo "<button class='tablinks active' onclick=\"openCity(event, 'Address')\">Address</button>";
                                    }
                                else if($div_name=='name'){
                                         $Name_rsp='name';
                                         echo "<button class='tablinks ' onclick=\"openCity(event, 'Name')\">Name</button>";
                                    }
                                    else{
                                    
                                    }
                                    
                            }
                            
                            
                     
                     }
            echo " </div>";
              
             
             
            if($Add_rsp=='address'){
                      $id='Address'; $name='Address '; //$style="style='display: block;'";
                  
                echo "<div id='".$id."' class='tabcontent' $style1>"; 
                
                  echo "<div class='persdetablog'>
                        <div class='exampannelNew1' style='width: 5%;'>
                            <div class='examname'>S.No.</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>ITGK - Request </div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>SP - Action </div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>RKCL - Action</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>ITGK - Payment</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Status</div>
                        </div>
                       
                     </div>";
                $responsesHR = $emp->ANCDetail($User_LoginId, $Add_rsp);
                $i=1;
                while($_RowHR = mysqli_fetch_array($responsesHR[2],true))
                     {
                    $fld_addedOn =  ($_RowHR['fld_addedOn']=='' || $_RowHR['fld_addedOn']=='0000-00-00 00:00:00') ? 'Not Available' : date("d M Y", strtotime($_RowHR['fld_addedOn']));
                    $fld_spActionDate =  ($_RowHR['fld_spActionDate']=='' || $_RowHR['fld_spActionDate']=='0000-00-00 00:00:00') ? 'Not Available' : date("d M Y", strtotime($_RowHR['fld_spActionDate']));
                    $fld_rkclActionDate =  ($_RowHR['fld_rkclActionDate']=='' || $_RowHR['fld_rkclActionDate']=='0000-00-00 00:00:00') ? 'Not Available' : date("d M Y", strtotime($_RowHR['fld_rkclActionDate']));
                    $fld_paymentDate =  ($_RowHR['fld_paymentDate']=='' || $_RowHR['fld_paymentDate']=='0000-00-00 00:00:00') ? 'Not Available' : date("d M Y", strtotime($_RowHR['fld_paymentDate']));
                    
                    echo " <div class=' persdetablog' >
                        <div class='exampannelNew1' style='width: 4%;'>
                           <div class='marks'>". $i ."</div>
                        </div>
                        <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( $fld_addedOn ) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( $fld_spActionDate ) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( $fld_rkclActionDate ) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( $fld_paymentDate ) ."</div>
                        </div> 
                        <div class='exampannelNew1' style='width: 25%;'>
                           <div class='marks'>". strtoupper( ($_RowHR['fld_status']=='')?'Not Available' : $_RowHR['fld_status']) ."</div>
                        </div> 
                       

                    </div>";
                   $i++;
                }      
                       
                 echo "</div>";   
                 
             }  
             
            if($Name_rsp=='name'){
                      $id='Name'; $name='Name '; //$style="style='display: block;'";
                  
                echo "<div id='".$id."' class='tabcontent' $style2 >"; 
                
                  echo "<div class='persdetablog'>
                        <div class='exampannelNew1' style='width: 5%;'>
                            <div class='examname'>S.No.</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>ITGK - Request </div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>SP - Action </div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>RKCL - Action</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>ITGK - Payment</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Status</div>
                        </div>
                       
                     </div>";
                $responsesHR = $emp->ANCDetail($User_LoginId, $Name_rsp);
                $i=1;
                while($_RowHR = mysqli_fetch_array($responsesHR[2],true))
                     {
                    $fld_addedOn =  ($_RowHR['fld_addedOn']=='' || $_RowHR['fld_addedOn']=='0000-00-00 00:00:00') ? 'Not Available' : date("d M Y", strtotime($_RowHR['fld_addedOn']));
                    $fld_spActionDate =  ($_RowHR['fld_spActionDate']=='' || $_RowHR['fld_spActionDate']=='0000-00-00 00:00:00') ? 'Not Available' : date("d M Y", strtotime($_RowHR['fld_spActionDate']));
                    $fld_rkclActionDate =  ($_RowHR['fld_rkclActionDate']=='' || $_RowHR['fld_rkclActionDate']=='0000-00-00 00:00:00') ? 'Not Available' : date("d M Y", strtotime($_RowHR['fld_rkclActionDate']));
                    $fld_paymentDate =  ($_RowHR['fld_paymentDate']=='' || $_RowHR['fld_paymentDate']=='0000-00-00 00:00:00') ? 'Not Available' : date("d M Y", strtotime($_RowHR['fld_paymentDate']));
                    
                    echo " <div class=' persdetablog' >
                        <div class='exampannelNew1' style='width: 4%;'>
                           <div class='marks'>". $i ."</div>
                        </div>
                        <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( $fld_addedOn ) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( $fld_spActionDate ) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( $fld_rkclActionDate ) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( $fld_paymentDate ) ."</div>
                        </div> 
                        <div class='exampannelNew1' style='width: 25%;'>
                           <div class='marks'>". strtoupper( ($_RowHR['fld_status']=='')?'Not Available' : $_RowHR['fld_status']) ."</div>
                        </div> 
                       

                    </div>";
                   $i++;
                }      
                       
                 echo "</div>";   
                 
             }         
             
                
        	 
	}	 
	
	
}



########################################################        
if ($_action == "DETAILS") {

	//print_r($_POST);die;
        
        $_DataTable = "";
        if(isset($_POST['selITGK']) && $_POST['selITGK']!=''){
             $User_LoginId=$_POST['selITGK'];    
        }
        else{
             $User_LoginId=$_SESSION['User_LoginId'];
        }
        $response = $emp->CenterDetails($User_LoginId);
        $_RowC = mysqli_fetch_array($response[2]);
        //echo "<pre>";print_r($_RowC);die;
	$num_rows = mysqli_num_rows($response[2]);
        
	$_LoginRole = $_SESSION['User_UserRoll'];
	if($num_rows==0)
	{
           echo "<div class='error'> You are not Registered/Enrolled In Your Center/ITGK/SP.</div>";
               
            
          
	}
	else
	{
	
        //print_r($_RowC);//die;
        
                echo "<div class='container'>
                        <div class='row'>
                            <div class='personalblog' style='width: 77%;'>";
               
                /* New Tab*/ 
                   
                    echo "<div class='col-lg-12 col-md-12 ' id='tabs-1' style='display: block;' aria-hidden='false'>";
                    echo "<div class='persdeta'>
                             <span>ITGK (Center) Details</span>
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Center Name:</span>
                             <p>". strtoupper( ($_RowC['Organization_Name']=='')?'Not Available' : $_RowC['Organization_Name'] ) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Center Code :</span>
                             <p>". strtoupper( ($_RowC['User_LoginId']=='')?'Not Available' : $_RowC['User_LoginId']) . "</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Email :</span>
                             <p style=' width: 59%;'>" . strtoupper( ($_RowC['User_EmailId']=='')?'Not Available' : $_RowC['User_EmailId']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Mobile :</span>
                             <p>". strtoupper( ($_RowC['User_MobileNo']=='')?'Not Available' : $_RowC['User_MobileNo']) ."</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Center Address :</span>
                             <p  >". strtoupper( ($_RowC['Organization_Address']=='')?'Not Available' : $_RowC['Organization_Address']) ."</p>
                        </div>
                        
                        
                    </div>
                  </div>";

                    
                    
                $responsesSP = $emp->SPDetails($_RowC["User_Rsp"]);
                $_RowSP = mysqli_fetch_array($responsesSP[2]); 
                    
                  $Addresp=$_RowSP['Organization_HouseNo']." ".$_RowSP['Organization_Street']." ".$_RowSP['Organization_Road']." ".$_RowSP['Organization_Landmark']." ".$_RowSP['Org_formatted_address'] ; 
                    echo "<div class='col-lg-12 col-md-12  tabcontent'  id='tabs-2' aria-hidden='true' style='display: none;'>
                    <div class='persdeta'>
                        <span>Service Provider Details</span>
                    </div>";
                    $Organization_FoundedDate = ($_RowSP['Organization_FoundedDate']=='')?'Not Available' : date("d M Y", strtotime($_RowSP['Organization_FoundedDate']));
                    echo "<div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>SP Name:</span>
                             <p>". strtoupper( ($_RowSP['Organization_Name']=='')?'Not Available' : $_RowSP['Organization_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>SP Registration No :</span>
                             <p>". strtoupper( ($_RowSP['Organization_RegistrationNo']=='')?'Not Available' : $_RowSP['Organization_RegistrationNo']) . "</p>
                        </div>
                        </div>
                    
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>SP Founded Date:</span>
                             <p>". strtoupper($Organization_FoundedDate) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>SP Address :</span>
                             <p  >".strtoupper($Addresp). "</p>
                        </div>
                    </div>
                    
                    
                   
               </div>";
                    
                  /* New Tab*/ 
                    
                    
                    
                    
                   echo "<div class='col-lg-12 col-md-12 ' id='tabs-3' aria-hidden='true' style='display: none;'>
                    <div class='persdeta'>
                        <span>Bank Account Details</span>
                    </div>";

                   echo " <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Account Name :</span>
                             <p>" . strtoupper( ($_RowC['Bank_Account_Name']=='')?'Not Available' : $_RowC['Bank_Account_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Account Number :</span>
                             <p  >". strtoupper( ($_RowC['Bank_Account_Number']=='')?'Not Available' : $_RowC['Bank_Account_Number']) ."</p>
                        </div>
                      </div> ";
                   echo " <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Account Type :</span>
                             <p>" . strtoupper( ($_RowC['Bank_Account_Type']=='')?'Not Available' : $_RowC['Bank_Account_Type']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Ifsc Code :</span>
                             <p  >". strtoupper( ($_RowC['Bank_Ifsc_code']=='')?'Not Available' : $_RowC['Bank_Ifsc_code']) ."</p>
                        </div>
                      </div> ";
                   echo " <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Bank Name :</span>
                             <p>" . strtoupper( ($_RowC['Bank_Name']=='')?'Not Available' : $_RowC['Bank_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Micr Code :</span>
                             <p  >". strtoupper( ($_RowC['Bank_Micr_Code']=='')?'Not Available' : $_RowC['Bank_Micr_Code']) ."</p>
                        </div>
                      </div> ";
                   echo " <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Branch Name :</span>
                             <p>" . strtoupper( ($_RowC['Bank_Branch_Name']=='')?'Not Available' : $_RowC['Bank_Branch_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Id Proof :</span>
                             <p  >". strtoupper( ($_RowC['Bank_Id_Proof']=='')?'Not Available' : $_RowC['Bank_Id_Proof']) ."</p>
                        </div>
                      </div> ";
                   echo " <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Pan No :</span>
                             <p>" . strtoupper( ($_RowC['Pan_No']=='')?'Not Available' : $_RowC['Pan_No']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Pan Name :</span>
                             <p  >". strtoupper( ($_RowC['Pan_Name']=='')?'Not Available' : $_RowC['Pan_Name']) ."</p>
                        </div>
                      </div> ";
                   echo " <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Pan Document :</span>
                             <p><img src='../myrkcl/upload/Bankdocs/".$_RowC['Pan_Document']."' height='100' width='100'></p>
                        </div>
                        <div class='persdetacont'>
                             <span>Bank Document :</span>
                              <p><img src='../myrkcl/upload/pancard/".$_RowC['Bank_Document']."'  height='100' width='100'></p>
                        </div>
                      </div> ";
                   
              echo "  </div>";
                    
              
              
              echo "<div class='col-lg-12 col-md-12 '  id='tabs-4' aria-hidden='true' style='display: none;'>
                    <div class='persdeta'>
                        <span>Location Details</span>
                    </div>";
               echo " <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>State :</span>
                             <p>" . strtoupper( ($_RowC['State_Name']=='')?'Not Available' : $_RowC['State_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Division :</span>
                             <p  >". strtoupper( ($_RowC['Region_Name']=='')?'Not Available' : $_RowC['Region_Name'] ) ."</p>
                        </div>
                      </div> ";
                echo " <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>District :</span>
                             <p>" . strtoupper( ($_RowC['District_Name']=='')?'Not Available' : $_RowC['District_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Tehsil :</span>
                             <p  >". strtoupper( ($_RowC['Tehsil_Name']=='')?'Not Available' : $_RowC['Tehsil_Name']) ."</p>
                        </div>
                      </div> ";
                
                
                if($_RowC['Organization_AreaType']=='Urban' && $_RowC['Organization_AreaType']!=''){
               
               //echo "<div class='col-lg-12 col-md-12 bdrblue'  id='tabs-6'>
//                   echo " <div class='persdeta'>
//                        <span>Area Type ( " . strtoupper($_RowC['Organization_AreaType']) . " )</span>
//                    </div>";
               
                echo " <div class='persdetablog'>
                         <div class='persdetacont'>
                             <span>Area Type :</span>
                             <p  >". strtoupper( ($_RowC['Organization_AreaType']=='')?'Not Available' : $_RowC['Organization_AreaType']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Ward Number :</span>
                             <p  >". strtoupper( ($_RowC['Ward_Name']=='')?'Not Available' : $_RowC['Ward_Name']) ."</p>
                        </div>
                      </div> ";
                
                echo " <div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Municipal Type :</span>
                             <p  >". strtoupper( ($_RowC['Municipality_Type_Name']=='')?'Not Available' : $_RowC['Municipality_Type_Name']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Municipal Name :</span>
                             <p>" . strtoupper( ($_RowC['Municipality_Name']=='')?'Not Available' : $_RowC['Municipality_Name']) . "</p>
                        </div>
                      </div> ";
                
               }else{
                //echo "<div class='col-lg-12 col-md-12 bdrblue'>
//                    echo "<div class='persdeta'>
//                        <span>Area Type ( " . strtoupper($_RowC['Organization_AreaType']) . " )</span>
//                    </div>";
                
                echo " <div class='persdetablog'>
                       
                        <div class='persdetacont'>
                             <span>Area Type :</span>
                             <p  >". strtoupper( ($_RowC['Organization_AreaType']=='')?'Not Available' : $_RowC['Organization_AreaType']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Panchayat Samiti :</span>
                             <p  >". strtoupper( ($_RowC['Block_Name']=='')?'Not Available' : $_RowC['Block_Name']) ."</p>
                        </div>
                         
                      </div> ";
                echo " <div class='persdetablog'>
                       <div class='persdetacont'>
                             <span>Gram Panchayat :</span>
                             <p  >". strtoupper( ($_RowC['GP_Name']=='')?'Not Available' : $_RowC['GP_Name']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Village :</span>
                             <p  >". strtoupper( ($_RowC['Village_Name']=='')?'Not Available' : $_RowC['Village_Name']) ."</p>
                        </div>
                      </div> ";
               }
                
                
                
               echo "  </div>";
               
               
               
               
//              echo "<div class='col-lg-12 col-md-12 '  id='tabs-5' aria-hidden='true' style='display: none;'>
//                    <div class='persdeta'>
//                        <span>Area Details</span>
//                    </div>";
//               echo " <div class='persdetablog'>
//                        <div class='persdetacont'>
//                             <span>House Number :</span>
//                             <p>" . strtoupper($_RowC['Organization_HouseNo']) . "</p>
//                        </div>
//                        <div class='persdetacont'>
//                             <span>Street:</span>
//                             <p  >". strtoupper($_RowC['Organization_Street']) ."</p>
//                        </div>
//                      </div> ";
//                echo " <div class='persdetablog'>
//                        <div class='persdetacont'>
//                             <span>Road :</span>
//                             <p>" . strtoupper($_RowC['Organization_Road']) . "</p>
//                        </div>
//                        <div class='persdetacont'>
//                             <span>Landmark:</span>
//                             <p  >". strtoupper($_RowC['Organization_Landmark']) ."</p>
//                        </div>
//                      </div> ";
//                echo " <div class='persdetablog'>
//                        <div class='persdetacont'>
//                             <span>Area :</span>
//                             <p>" . strtoupper($_RowC['Organization_Area']) . "</p>
//                        </div>
//                        <div class='persdetacont'>
//                             <span>PinCode:</span>
//                             <p  >". strtoupper($_RowC['Organization_PinCode']) ."</p>
//                        </div>
//                      </div> ";
//                echo " <div class='persdetablog'>
//                        <div class='persdetacont'>
//                             <span>Formatted Address:</span>
//                             <p  >". strtoupper($_RowC['Org_formatted_address']) ."</p>
//                        </div>
//                      </div> ";
//               echo "  </div>";
               
               
               
               
               
                $responsesCD = $emp->CourseDetails($User_LoginId);
                $num_rows_CD = mysqli_num_rows($responsesCD[2]); 
               
               echo "<div class='col-lg-12 col-md-12'  id='tabs-5' aria-hidden='true' style='display: none;'>
                    <div class='persdeta'>
                        <span>Course Details</span>
                    </div>";
                 if($num_rows_CD > 0)
                     {   
                     echo "<div class='persdetablog'>
                        <div class='exampannelNew1'>
                            <div class='examname'>Course Name</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Center Login Creation Date</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Final Approval Date</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Course Expire Date</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Course EOI Applied Date</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Course Renewal Date</div>
                        </div>
                     </div>";
               //$Batch_StartDate = date("d M Y", strtotime($_RowSP['User_CreatedDate']));
                while($_RowSP = mysqli_fetch_array($responsesCD[2],true))
                     {
                   $RenewalDate =  ($_RowSP['CourseITGK_ExpireDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['CourseITGK_ExpireDate'] ." +13 month"));
                   echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['Courseitgk_Course']=='')? 'Not Available' : $_RowSP['Courseitgk_Course']) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['User_CreatedDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['User_CreatedDate']))) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['CourseITGK_UserFinalApprovalDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['CourseITGK_UserFinalApprovalDate']))) ."</div>
                        </div>
                         <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['CourseITGK_ExpireDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['CourseITGK_ExpireDate']))) ."</div>
                        </div> 
                        <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper( ($_RowSP['EOI_StartDate']=='')? 'Not Available' : date("d M Y", strtotime($_RowSP['EOI_StartDate']))) ."</div>
                        </div> 
                        <div class='exampannelNew1'>
                           <div class='marks'>". strtoupper($RenewalDate)."</div>
                        </div>
                        
                        
                        

                    </div>";
                }
             
                 }
                 else
                   {
                            echo "<div class='persdetablog'>

                            <div class='error'> No details are available.</div>

                           </div>";
                   } 
                 echo" </div>";
                 
                 
                 
                 $responsesPre = $emp->PremisesDetails($_RowC['User_LoginId']);
                 $_RowPre = mysqli_fetch_array($responsesPre[2]); 
                 $num_rows_Pre = mysqli_num_rows($responsesPre[2]);  
                  //$Addresp=$_RowSP['Organization_HouseNo']." ".$_RowSP['Organization_Street']." ".$_RowSP['Organization_Road']." ".$_RowSP['Organization_Landmark']." ".$_RowSP['Org_formatted_address'] ; 
                    echo "<div class='col-lg-12 col-md-12  tabcontent'  id='tabs-6' aria-hidden='true' style='display: none;'>
                    <div class='persdeta'>
                        <span>Premises Details</span>
                    </div>";
                    if($num_rows_Pre > 0)
                    {
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Reception :</span>
                             <p>". strtoupper( ($_RowPre['Premises_Receptiontype']=='')?'Not Available' : $_RowPre['Premises_Receptiontype']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Seating Capacity :</span>
                             <p>". strtoupper( ($_RowPre['Premises_seatingcapacity']=='')?'Not Available' : $_RowPre['Premises_seatingcapacity']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Area :</span>
                             <p>". strtoupper( ($_RowPre['Premises_area']=='')?'Not Available' : $_RowPre['Premises_area']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Details :</span>
                             <p>". strtoupper( ($_RowPre['Premises_details']=='')?'Not Available' : $_RowPre['Premises_details']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Parking Facility :</span>
                             <p>". strtoupper( ($_RowPre['Premises_parkingfacility']=='')?'Not Available' : $_RowPre['Premises_parkingfacility']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Ownership :</span>
                             <p>". strtoupper( ($_RowPre['Premises_ownership']=='')?'Not Available' : $_RowPre['Premises_ownership']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Parking For :</span>
                             <p>". strtoupper( ($_RowPre['Premises_parkingfor']=='')?'Not Available' : $_RowPre['Premises_parkingfor']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Toilet Available :</span>
                             <p>". strtoupper( ($_RowPre['Premises_toiletavailable']=='')?'Not Available' : $_RowPre['Premises_toiletavailable']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Ownership Toilet :</span>
                             <p>". strtoupper( ($_RowPre['Premises_ownershiptoilet']=='')?'Not Available' : $_RowPre['Premises_ownershiptoilet']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Toilet Type :</span>
                             <p>". strtoupper( ($_RowPre['Premises_toilet_type']=='')?'Not Available' : $_RowPre['Premises_toilet_type']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Premises Panatry :</span>
                             <p>". strtoupper( ($_RowPre['Premises_panatry']=='')?'Not Available' : $_RowPre['Premises_panatry']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Panatry Capacity :</span>
                             <p>". strtoupper( ($_RowPre['Premises_panatry_capacity']=='')?'Not Available' : $_RowPre['Premises_panatry_capacity']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Library Available:</span>
                             <p>". strtoupper( ($_RowPre['Premises_library_available']=='')?'Not Available' : $_RowPre['Premises_library_available']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Library Capacity :</span>
                             <p>". strtoupper( ($_RowPre['Premises_library_capacity']=='')?'Not Available' : $_RowPre['Premises_library_capacity']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Staff Available:</span>
                             <p>". strtoupper( ($_RowPre['Premises_Staff_available']=='')?'Not Available' : $_RowPre['Premises_Staff_available']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Staff Capacity :</span>
                             <p>". strtoupper( ($_RowPre['Premises_Staff_capacity']=='')?'Not Available' : $_RowPre['Premises_Staff_capacity']) . "</p>
                        </div>
                        
                        </div>";
                        }
                    else
                        {
                            echo "<div class='persdetablog'>

                            <div class='error'> No details are available.</div>

                           </div>";
                        } 
                    
                  echo "</div>";  /// this is main div's last div
                  
                  
                  
                  $responsesHR = $emp->HRDetails($_RowC['User_LoginId']);
                  $_RowHR = mysqli_fetch_array($responsesHR[2]); 
                  $num_rows_HR = mysqli_num_rows($responsesHR[2]);
                 
                  //$Addresp=$_RowSP['Organization_HouseNo']." ".$_RowSP['Organization_Street']." ".$_RowSP['Organization_Road']." ".$_RowSP['Organization_Landmark']." ".$_RowSP['Org_formatted_address'] ; 
                    echo "<div class='col-lg-12 col-md-12  tabcontent'  id='tabs-11' aria-hidden='true' style='display: none;'>
                    <div class='persdeta'>
                        <span>HR Details</span>
                    </div>";
                    
                     if($num_rows_HR > 0)
                       {
                       
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Designation :</span>
                             <p>". strtoupper( ($_RowHR['Designation']=='')?'Not Available' : $_RowHR['Designation']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Staff Name :</span>
                             <p>". strtoupper( ($_RowHR['Staff_Name']=='')?'Not Available' : $_RowHR['Staff_Name']) . "</p>
                        </div>
                        
                        </div>";
                    $Staff_Dob =  ($_RowHR['Staff_Dob']=='') ? 'Not Available' : date("d M Y", strtotime($_RowHR['Staff_Dob']));
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>DOB :</span>
                             <p>". strtoupper($Staff_Dob) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Gender :</span>
                             <p>". strtoupper( ($_RowHR['staff_Gender']=='') ? 'Not Available' : $_RowHR['staff_Gender']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Qualification 1 :</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification1']=='') ? 'Not Available' : $_RowHR['Staff_Qualification1']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Qualification 2 :</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification2']=='') ? 'Not Available' : $_RowHR['Staff_Qualification2']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Qualification 3 :</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification3']=='')?'Not Available' : $_RowHR['Staff_Qualification3']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Mobile :</span>
                             <p>". strtoupper( ($_RowHR['Staff_Mobile']=='')?'Not Available' : $_RowHR['Staff_Mobile']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Experience :</span>
                             <p>". strtoupper( ($_RowHR['Staff_Experience']=='')?'Not Available' : $_RowHR['Staff_Experience']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Email Id :</span>
                             <p>". strtoupper( ($_RowHR['Staff_Email_Id']=='')?'Not Available' : $_RowHR['Staff_Email_Id']) . "</p>
                        </div>
                        
                        </div>";
                    
                     }
                     else
                        {
                            echo "<div class='persdetablog'>

                            <div class='error'> No details are available.</div>

                           </div>";
                        } 
                  
                    
                  echo "</div>";  /// this is main div's last div
                 
                  
                    
            #======= last==========###
         echo" </div>
                    </div>
                        </div>";
        
        
		 
	}	 
	//echo $response[0];
	
}

if ($_action == "SLADETAILS_old") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $responsesCD = $emp->SLADetails($User_LoginId);
        $num_rows = mysqli_num_rows($responsesCD[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
                   echo "<div class='persdetablog bdrtop'>
                        <div class='exampannelNew1' >
                            <div class='examname'>Course Name</div>
                        </div>
                       
                        <div class='exampannelNew1' style='padding-left: 15px;'>
                            <div class='examname'>Event Name</div>
                        </div>
                        <div class='exampannelNew1' style='margin-left: 30px;'>
                            <div class='examname'>Total Learner Appeared</div>
                        </div>
                        <div class='exampannelNew1' style='width: 10%;'>
                            <div class='examname'>PASS</div>
                        </div>
                        <div class='exampannelNew1'  style='width: 10%;'>
                            <div class='examname'>FAIL</div>
                        </div>
                        <div class='exampannelNew1'  style='width: 10%;'>
                            <div class='examname'>ABSENTE</div>
                        </div>
                        <div class='exampannelNew1'  style='width: 10%;'>
                            <div class='examname'>% Result</div>
                        </div>
                        
                     </div>";
                $sum='';
                while($_RowSP = mysqli_fetch_array($responsesCD[2],true))
                     {
                   
                   echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew1'  style=' margin-left: 15px;margin-right: 0px; '>
                           <div class='marks'>". strtoupper( ($_RowSP['coursename']=='')? 'Not Available' : $_RowSP['coursename']) ."</div>
                        </div>
                         
                         <div class='exampannelNew1'  style=' margin-left: 15px;margin-right: 0px; '>
                           <div class='marks'>". strtoupper( ($_RowSP['eventname']=='')? 'Not Available' : $_RowSP['eventname']) ."</div>
                        </div>";
                   
                   
//                   echo " <div class='exampannelNew1' style='width: 12%; margin-left: 15px;margin-right: 0px; '>
//                           <div class='marks'>". strtoupper( ($_RowSP['confirmcount']=='')? 'Not Available' : $_RowSP['confirmcount']) ."</div>
//                        </div> 
//                        "; 
                            $responsesSALresult2 = $emp->SLADetailsResult($User_LoginId,$_RowSP['examid']);
                            $num_rowsLA2 = mysqli_num_rows($responsesSALresult2[2]);
                             if($num_rowsLA2>0)
                                     { $totoalAppeared2='';$i=1;
                                 while($_RowSLA2= mysqli_fetch_array($responsesSALresult2[2],true))
                                            {
                                            if($i<=2)
                                                {
                                                 $totoalAppeared2+= $_RowSLA2['Allresult'];
                                                 
                                                }
                                                $i++;
                                            }
                            echo " <div class='exampannelNew1' style='width: 12%; margin-left: 15px;margin-right: 0px; '>
                                    <div class='marks'>". $totoalAppeared2 ."</div>
                                 </div> 
                                 "; 
                                     }
                            
                            $responsesSALresult = $emp->SLADetailsResult($User_LoginId,$_RowSP['examid']);
                            $num_rowsLA = mysqli_num_rows($responsesSALresult[2]);
                            if($num_rowsLA==0)
                            {
                               echo "<div class='exampannelNew1'  style='width: 60%;margin-left: 15px;margin-right: 0px; '>
                                             <div class='marks'>No Data Available </div>
                                     </div> ";

                            }
                            else
                            {
                                $i=1;
                                $pass='';
                                $totoalAppeared='';
                            while($_RowSLA= mysqli_fetch_array($responsesSALresult[2],true))
                                   {
                                    if($i==1){
                                       $pass= $_RowSLA['Allresult'];
                                    }
                                    if($i<=2){
                                       $totoalAppeared+= $_RowSLA['Allresult'];
                                    }
                                    echo "<div class='exampannelNew1' style='width: 10%;'>
                                             <div class='marks_number'>";
                                              echo strtoupper( ($_RowSLA['Allresult']=='')? 'Not Available' : $_RowSLA['Allresult']);
                                    echo " </div></div> ";
                                    $i++;
                                   }
                                  // echo $totoalAppeared;
                                   //$resul_final=number_format(($pass/$_RowSP['confirmcount'])*100, 2);
                                   $resul_final=number_format(($pass/$totoalAppeared)*100, 2);
                                   
                                echo "  <div class='exampannelNew1'  style='width: 15%;'>
                                        <div class='marks_number'>".$resul_final." % </div>
                                        </div> ";
                                
                            }
                       
                        $sum+=$_RowSP['confirmcount']; 
                        
                        

                    echo "</div>";
                }
             
//                 echo " <div class=' persdetablog1'>
//                       
//                        <div class='exampannelNew2' style='width:36%'>
//                           <div class='marks' style='font-weight: bold;'>Total Count:</div>
//                        </div>
//                         <div class='exampannelNew2' style='width:12%'>
//                           <div class='marks'  style='font-weight: bold;'>". $sum ."</div>
//                        </div>
//                       
//                    </div>";
//        	 
	}	 
	
	
}

if ($_action == "HRDETAILS_OLD") {

	//print_r($_POST);die;
        
        if(isset($_POST['selITGK']) && $_POST['selITGK']!='')
            {
                 $User_LoginId=$_POST['selITGK'];    
            }
        else
            {
                 $User_LoginId=$_SESSION['User_LoginId'];
            }
        $responsesHR = $emp->HRDetails($User_LoginId);
        $num_rows = mysqli_num_rows($responsesHR[2]);
        if($num_rows==0)
	{
           echo "NoData";
         
	}
	else
	{
                //echo count($num_rows);
            if($num_rows==1){
                echo " <div class='tab'>
                                      <button class='tablinks active' onclick=\"openCity(event, 'Faculty')\">Faculty</button>
                        </div>";
            }
            if($num_rows==2){
                echo " <div class='tab'>
                                      <button class='tablinks active' onclick=\"openCity(event, 'Faculty')\">Faculty</button>
                                      <button class='tablinks' onclick=\"openCity(event, 'Coordinator')\">Coordinator</button>
                        </div>";
            }
            if($num_rows==3){
                echo " <div class='tab'>
                                      <button class='tablinks active' onclick=\"openCity(event, 'Faculty')\">Faculty</button>
                                      <button class='tablinks' onclick=\"openCity(event, 'Coordinator')\">Coordinator</button>
                                      <button class='tablinks' onclick=\"openCity(event, 'Ownername')\">Counselor</button>
                        </div>";
            }
                 $i=1; 
                 while($_RowHR = mysqli_fetch_array($responsesHR[2],true))
                  {
                     if($i==1){ $id='Faculty'; $name='Faculty '; $style="style='display: block;'";}
                     if($i==2){ $id='Coordinator'; $name='Coordinator '; $style='';}
                     if($i==3){ $id='Ownername'; $name='Counselor '; $style='';}
                     
                     echo "<div id='".$id."' class='tabcontent' $style >";  
                     echo "<div class='persdetablog bdrtop'>
                        
                        
                        <div class='persdetacont'>
                             <span>".$name." Name:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Name']=='')?'Not Available' : $_RowHR['Staff_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Designation:</span>
                             <p>". strtoupper( ($_RowHR['Designation_Name']=='')?'Not Available' : $_RowHR['Designation_Name']) . "</p>
                        </div>
                        </div>";
                    $Staff_Dob =  ($_RowHR['Staff_Dob']=='') ? 'Not Available' : date("d M Y", strtotime($_RowHR['Staff_Dob']));
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>DOB:</span>
                             <p>". strtoupper($Staff_Dob) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Gender:</span>
                             <p>". strtoupper( ($_RowHR['staff_Gender']=='') ? 'Not Available' : $_RowHR['staff_Gender']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Max. Qualification:</span>
                             <p>". strtoupper( ($_RowHR['Qualification_Name']=='') ? 'Not Available' : $_RowHR['Qualification_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Certification Details:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification2']=='') ? 'Not Available' : $_RowHR['Staff_Qualification2']) . "</p>
                        </div>
                        
                        </div>";
                    
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span>Other Qualifications:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Qualification3']=='')?'Not Available' : $_RowHR['Staff_Qualification3']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Mobile:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Mobile']=='')?'Not Available' : $_RowHR['Staff_Mobile']) . "</p>
                        </div>
                        
                        </div>";
                    echo "<div class='persdetablog'>
                        
                        <div class='persdetacont'>
                             <span> Total Work Experience:</span>
                             <p>". strtoupper( ($_RowHR['Staff_Experience']=='')?'Not Available' : $_RowHR['Staff_Experience']) . " Year(s)</p>
                        </div>
                        <div class='persdetacont'>
                             <span style='width: 18%'>Email Id:</span>
                             <p style='width: 71%'>". strtoupper( ($_RowHR['Staff_Email_Id']=='')?'Not Available' : $_RowHR['Staff_Email_Id']) . "</p>
                        </div>
                        
                        </div>";
                    
                    
                    echo "</div>";
                    $i++;
                     }
                        
                       
                       
                    
                 
                       
             
                
        	 
	}	 
	
	
}








