<?php

/* 
 * Created by Yogendra soni

 */

include 'commonFunction.php';
require 'BAL/clsschoolfeedbackreport.php';

$response = array();
$emp = new clsschoolfeedbackreport();


if ($_action == "SHOW") 
{

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='10%'>School Name</th>";
    echo "<th style='20%'>Board </th>";
    echo "<th style='20%'>Full Address</th>";
	echo "<th style='10%'>State </th>";
	echo "<th style='10%'>District </th>";
	echo "<th style='10%'> School Website </th>";
    echo "<th style='10%'> Total Student Count </th>";
    echo "<th style='10%'>  Contact Person Name</th>";
    echo "<th style='10%'> Contact Person Designation</th>";
	echo "<th style='10%'>Contact Person Mobile</th>";
    echo "<th style='10%'>Contact Person Email </th>";
    echo "<th style='10%'>Preferred Date for Demo</th>";
    echo "<th style='10%'>Preferred Time for Demo</th>";
	 echo "<th style='10%'>Date / Timestamp</th>";
    
  
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
         echo "<td>" . $_Row['schoolname'] . "</td>";
         echo "<td>" . $_Row['board'] . "</td>";
         echo "<td>" . $_Row['address'] . "</td>";
		  echo "<td>" . $_Row['state'] . "</td>";
		 echo "<td>" . $_Row['district'] . "</td>";
         echo "<td>" . $_Row['schoolweb'] . "</td>";
		  echo "<td>" . $_Row['count'] . "</td>";
		  echo "<td>" . $_Row['contact'] . "</td>";
		   echo "<td>" . $_Row['designation'] . "</td>";
		    echo "<td>" . $_Row['mobile'] . "</td>";
		  echo "<td>" . $_Row['email'] . "</td>";
		   echo "<td>" . $_Row['date'] . "</td>";
		    echo "<td>" . $_Row['time'] . "</td>";
			echo "<td>" . $_Row['timestamp'] . "</td>";
       
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}








