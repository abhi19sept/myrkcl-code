<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsgoventryform.php';

    $response = array();
    $emp = new clsgoventryform();
    if ($_action == "ADD") {
		
        if (isset($_POST["lcode"]) && !empty($_POST["lcode"]) && !empty($_POST["empname"]) && !empty($_POST["faname"]) && !empty($_POST["empdistrict"])
			 && !empty($_POST["empmobile"]) && !empty($_POST["empemail"]) && !empty($_POST["empaddress"]) && !empty($_POST["empdeptname"])
			 && !empty($_POST["empid"]) && !empty($_POST["empgpfno"]) && !empty($_POST["empdesignation"]) && !empty($_POST["empmarks"])
			 && !empty($_POST["empexamattempt"]) && !empty($_POST["emppan"]) && !empty($_POST["empbankaccount"]) && !empty($_POST["empbankdistrict"])
			 && !empty($_POST["empbankname"]) && !empty($_POST["empbranchname"]) && !empty($_POST["empifscno"]) && !empty($_POST["empmicrno"]))
			 {
				$_LearnerCode = $_POST["lcode"];
				$_EmpName = $_POST["empname"];
				$_FatherName = $_POST["faname"];
				$_EmpDistrict = $_POST["empdistrict"];
				$_EmpMobile = $_POST["empmobile"];
				$_EmpEmail = $_POST["empemail"];
				$_EmpAddress = $_POST["empaddress"];
				$_EmpDeptName = $_POST["empdeptname"];
				$_EmpId = $_POST["empid"];
				$_EmpGPF = $_POST["empgpfno"];
				$_EmpDesignation = $_POST["empdesignation"];
				$_EmpMarks = $_POST["empmarks"];
				$_EmpExamAttempt = $_POST["empexamattempt"];
				$_EmpPan = $_POST["emppan"];
				$_EmpBankAccount = $_POST["empbankaccount"];
				$_EmpBankDistrict = $_POST["empbankdistrict"];
				$_EmpBankName = $_POST["empbankname"];
				$_EmpBranchName = $_POST["empbranchname"];
				$_EmpIFSC = $_POST["empifscno"];
				$_EmpMICR = $_POST["empmicrno"];
				$_fileReceipt = $_POST["fileReceipt"];
				$_fileBirth = $_POST["fileBirth"];
				$_EmpDobProof = $_POST["empdobproof"];
			
            $response = $emp->Add($_LearnerCode, $_EmpName, $_FatherName, $_EmpDistrict, $_EmpMobile, $_EmpEmail, $_EmpAddress, $_EmpDeptName, 
								  $_EmpId , $_EmpGPF, $_EmpDesignation, $_EmpMarks, $_EmpExamAttempt, $_EmpPan, $_EmpBankAccount, $_EmpBankDistrict,
								  $_EmpBankName, $_EmpBranchName, $_EmpIFSC, $_EmpMICR, $_fileReceipt, $_fileBirth, $_EmpDobProof );
            echo $response[0];
        }
    }
    
    if ($_action == "UPDATE") {
        if (isset($_POST["name"])) {
            $_StatusName = $_POST["name"];
            $_StatusDescription = $_POST["description"];
            $_Status_Code=$_POST['code'];
            $response = $emp->Update($_Status_Code,$_StatusName, $_StatusDescription);
            echo $response[0];
        }
    }
    
    
    if ($_action == "EDIT") {


        $response = $emp->GetDatabyCode($_actionvalue);

        $_DataTable = array();
        $_i=0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("StatusCode" => $_Row['Status_Code'],
                "StatusName"=>$_Row['Status_Name'],
                "StatusDescription"=>$_Row['Status_Description']);
            $_i=$_i+1;
        }
        echo json_encode($_Datatable);
    }
    
    
    if ($_action == "DELETE") {


        $response = $emp->DeleteRecord($_actionvalue);

        echo $response[0];
    }


    if ($_action == "SHOW") {


        $response = $emp->GetAll();

        $_DataTable = "";

         echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='5%'>S No.</th>";
        echo "<th style='30%'>Name</th>";
        echo "<th style='55%'>Description</th>";
        echo "<th style='10%'>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Status_Name'] . "</td>";
            echo "<td>" . $_Row['Status_Description'] . "</td>";
            echo "<td><a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a>  <a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
		echo "</div>";
//echo $_DataTable;
    }
    if ($_action == "FILL") {
        $response = $emp->GetAll();
        echo "<option value=''>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['District_Name'] . ">" . $_Row['District_Name'] . "</option>";
             
     
        }
    }
    
     if ($_action == "FILLEMPDEPARTMENT") {
        $response = $emp->GetEmpdepartment();
        echo "<option value=''>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['gdname'] . ">" . $_Row['gdname'] . "</option>";
            
        }
    }
    
    
     if ($_action == "FILLEMPDESIGNATION") {
        $response = $emp->GetEmpdesignation();
        echo "<option value=''>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['designationname'] . ">" . $_Row['designationname'] . "</option>";
            
        }
    }
    
    
     if ($_action == "FILLBANKDISTRICT") {
        $response = $emp->GetBankdistrict();
        echo "<option value=''>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value='" . $_Row['District_Name'] . "'>" . $_Row['District_Name'] . "</option>";
            
        }
    }
    
    
     if ($_action == "FILLBANKNAME") {
        $response = $emp->GetBankname($_POST['districtid']);
        echo "<option value=''>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value='" . $_Row['bankname'] . "'>" . $_Row['bankname'] . "</option>";
            
        }
    }
    
    
     if ($_action == "FillDobProof") {
        $response = $emp->GetDobProof();
        echo "<option value=''>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['cdocname'] . ">" . $_Row['cdocname'] . "</option>";            
        }
    }
	
	if ($_action == "DETAILS") {
        $response = $emp->GetAllDETAILS($_POST['values']);
		//print_r($response[2]);
        $_DataTable = array();
        $_i=0;
		$co = mysqli_num_rows($response[2]);
		if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("Admission_Name" => $_Row['Admission_Name'],
                "Admission_Fname"=>$_Row['Admission_Fname'],
                "Admission_Mobile"=>$_Row['Admission_Mobile'],
				"Admission_Email"=>$_Row['Admission_Email'],
				"Admission_Address"=>$_Row['Admission_Address'],
				"Admission_GPFNO"=>$_Row['Admission_GPFNO']);
            $_i=$_i+1;
        }
         echo json_encode($_Datatable);
		}
		else {
        echo "";
    }
    }
?>
