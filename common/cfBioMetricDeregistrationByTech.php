<?php

/*
 * Created by : SUNIL KUMAR BAINDARA
 * DATED: 26-09-2018
 * Updated by : Abhishek
 * DATED: 15-05-2019

 */

include './commonFunction.php';
require 'BAL/clsBioMetricDeregistrationByTech.php';
require 'cflog.php';
$response = array();
$emp = new clsBioMetricDeregistrationByTech();
$LD_ITGK_Code = '';
if ($_action == "FILLCurrentOpenBatch") {
    $response = $emp->FILLCurrentOpenBatch($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "GETCURRENTBATCHLEARNER") {
    $response = $emp->GetCurrentBatchLearnerList();
//echo "<pre>"; print_r($response);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>LMS Score</th>";
    echo "<th>LMS Attendance</th>";
    echo "<th >Course Name</th>";
    echo "<th>Batch Name</th>";
    echo "<th>Reason</th>";
    echo "<th>Apply Date</th>";
    echo "<th>Deregister Count</th>";
    echo "<th><input type='checkbox' name='select_all' value='1' id='example-select-all'></th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Bio_Dereg_Apply_ITGK'] . "</td>";
            echo "<td>" . $_Row['Bio_Dereg_Apply_Lcode'] . "</td>";
            echo "<td>" . $_Row['Bio_Dereg_Apply_LMSscore'] . "</td>";
            echo "<td>" . $_Row['Bio_Dereg_Apply_Attendance'] . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            echo "<td>" . $_Row['Bio_Dereg_Apply_Reason'] . "</td>";

            echo "<td>" . $_Row['Bio_Dereg_Apply_Datetime'] . "</td>";
            echo "<td>" . $_Row['cnt'] . "</td>";
            echo "<td><input type='checkbox' name='id[]' value='" . $_Row['Bio_Dereg_Apply_Admcode'] . "'/></td>";

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "AddDeregisterLearnerPreview") {
    //echo "<pre>"; print_r($_POST);die;
    if (isset($_POST["id"])) {

        $LD_LearnerCode = implode(", ", $_POST["id"]);
        $response = $emp->GetCurrentBatchLearnerListPreview($LD_LearnerCode);
        //echo "<pre>"; print_r($response);
        echo "<div class='table-responsive'>";
        echo "<table class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th>Center Code</th>";
        echo "<th>Learner Code</th>";
        echo "<th>LMS Score</th>";
        echo "<th>LMS Attendance</th>";
        echo "<th >Course Name</th>";
        echo "<th>Batch Name</th>";
        echo "<th>Reason</th>";
        echo "<th>Apply Date</th>";
        echo "<th>Deregister Count</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";

        $_Count = 1;
        if ($response) {
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $_Row['Bio_Dereg_Apply_ITGK'] . "</td>";
                echo "<td>" . $_Row['Bio_Dereg_Apply_Lcode'] . "</td>";
                echo "<td>" . $_Row['Bio_Dereg_Apply_LMSscore'] . "</td>";
                echo "<td>" . $_Row['Bio_Dereg_Apply_Attendance'] . "</td>";
                echo "<td>" . $_Row['Course_Name'] . "</td>";
                echo "<td>" . $_Row['Batch_Name'] . "</td>";
                echo "<td>" . $_Row['Bio_Dereg_Apply_Reason'] . "</td>";

                echo "<td>" . $_Row['Bio_Dereg_Apply_Datetime'] . "</td>";
                echo "<td>" . $_Row['cnt'] . "</td>";
                echo "</tr>";
                $_Count++;
            }

            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }

        die;
    } else {
        echo 'IDEMPTY';
    }
}

if ($_action == "AddDeregisterLearner") {
    //echo "<pre>"; print_r($_POST);die;
    if (isset($_POST["id"])) {
        $LD_LearnerCode1 = implode(", ", $_POST["id"]);
        $response = $emp->GetCurrentBatchLearnerListPreview($LD_LearnerCode1);
        //echo "<pre>"; print_r($response);
        $yes = 0;
        global $LD_ITGK_Code;
        while ($_Row = mysqli_fetch_array($response[2])) {

            // getting the BioMatric ISO Template
            $LD_AdmCode = $_Row['Bio_Dereg_Apply_Admcode'];
            $LD_LearnerCode = $_Row['Bio_Dereg_Apply_Lcode'];
            $LD_ITGK_Code = $_Row['Bio_Dereg_Apply_ITGK'];

            $BioMatric_ISO_Template = $_Row['Bio_Dereg_Apply_Oldisotemplate'];
            $BioMatric_Reason = $_Row['Bio_Dereg_Apply_Reason'];
            $BioMatric_LMS = $_Row['Bio_Dereg_Apply_LMSscore'];
            $BioMatric_Attnd = $_Row['Bio_Dereg_Apply_Attendance'];
            $Bio_Dereg_Apply_Course = $_Row['Bio_Dereg_Apply_Course'];
            $Bio_Dereg_Apply_Batch = $_Row['Bio_Dereg_Apply_Batch'];
            // getting the BioMatric ISO Template
            //#### inserting the BioMatric ISO Template into tbl_adm_data_log table
            $User_LoginId = $_SESSION['User_LoginId'];
            $AddAdmAataLog = $emp->AddAdmAataLog($LD_AdmCode, $LD_LearnerCode, $LD_ITGK_Code, $User_LoginId, $BioMatric_ISO_Template, $BioMatric_Reason, $BioMatric_LMS, $BioMatric_Attnd, $Bio_Dereg_Apply_Course, $Bio_Dereg_Apply_Batch);
            // inserting the BioMatric ISO Template into tbl_adm_data_log table
            //#### deleting the BioMatric entry in tbl_biomatric_registration table                
            if ($AddAdmAataLog[0] = 'Successfully Inserted') {
                $AddAdmAataLog2 = $emp->delete_biomatric_registration_entry($LD_LearnerCode, $LD_ITGK_Code);
                m_log("delete_biomatric_registration_entry " . $AddAdmAataLog2[0] . date('Y-m-d h:i:s'));
                $finaladmission = $emp->update_tbl_admission_table($LD_LearnerCode, $LD_ITGK_Code);
                m_log("update_tbl_admission_table " . $finaladmission[0] . date('Y-m-d h:i:s'));
                $finallearner_deregister = $emp->update_learner_deregister_apply_table($LD_LearnerCode, $LD_ITGK_Code);
                m_log("update_learner_deregister_apply_table " . $finallearner_deregister[0] . date('Y-m-d h:i:s'));
                $yes = 1;
                $response_SMS = $emp->SendSMSTOITGK($LD_ITGK_Code, $LD_LearnerCode);
            }
            // deleting the BioMatric entry in tbl_biomatric_registration table
            //
               //die;
        }
        //echo $yes;
        //$response_SMS = $emp->SendSMSTOITGK($LD_ITGK_Code, count($_POST["id"]));
        //$response_SMS = $emp->SendSMSTOITGK($_SESSION['User_LoginId'],count($_POST["id"]));
        if ($yes == 1) {
            echo "DONE";
        }
        //echo $response[0];
    } else {
        echo 'IDEMPTY';
    }
}
if ($_action == "RejectDeregisterLearner") {
    //echo "<pre>"; print_r($_POST);die;
    if (isset($_POST["id"])) {
        $LD_LearnerCode1 = implode(", ", $_POST["id"]);
        $response = $emp->GetCurrentBatchLearnerListPreview($LD_LearnerCode1);
        //echo "<pre>"; print_r($response);
        $yes = 0;
        global $LD_ITGK_Code;
        while ($_Row = mysqli_fetch_array($response[2])) {

            // getting the BioMatric ISO Template
            $LD_AdmCode = $_Row['Bio_Dereg_Apply_Admcode'];
            $LD_LearnerCode = $_Row['Bio_Dereg_Apply_Lcode'];
            $LD_ITGK_Code = $_Row['Bio_Dereg_Apply_ITGK'];

            $BioMatric_ISO_Template = $_Row['Bio_Dereg_Apply_Oldisotemplate'];
            $BioMatric_Reason = $_Row['Bio_Dereg_Apply_Reason'];
            $BioMatric_LMS = $_Row['Bio_Dereg_Apply_LMSscore'];
            $BioMatric_Attnd = $_Row['Bio_Dereg_Apply_Attendance'];
            $Bio_Dereg_Apply_Course = $_Row['Bio_Dereg_Apply_Course'];
            $Bio_Dereg_Apply_Batch = $_Row['Bio_Dereg_Apply_Batch'];
            // getting the BioMatric ISO Template
            //#### inserting the BioMatric ISO Template into tbl_adm_data_log table
            $User_LoginId = $_SESSION['User_LoginId'];             

    
                $finallearner_deregister = $emp->update_learner_deregister_apply_table_reject($LD_LearnerCode, $LD_ITGK_Code);
                m_log("update_learner_deregister_apply_table_reject " . $finallearner_deregister[0] . date('Y-m-d h:i:s'));
                $yes = 1;
       
            // deleting the BioMatric entry in tbl_biomatric_registration table
            //
               //die;
            $response_SMS = $emp->SendSMSTOITGK_reject($LD_ITGK_Code, $LD_LearnerCode);
        }
        //echo $yes;
       // $response_SMS = $emp->SendSMSTOITGK_reject($LD_ITGK_Code, count($_POST["id"]));
        //$response_SMS = $emp->SendSMSTOITGK($_SESSION['User_LoginId'],count($_POST["id"]));
        if ($yes == 1) {
            echo "DONE";
        }
        //echo $response[0];
    } else {
        echo 'IDEMPTY';
    }
}
if ($_action == "Filltotalreq") {
    $response = $emp->Filltotalreq();
    $_Row = mysqli_fetch_array($response[2]);
    echo $_Row['cnt'];
    
}


