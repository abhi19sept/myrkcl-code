<?php

/* 
 * author Abhishek

 */
include './commonFunction.php';
require 'BAL/clsddReexamPayment.php';

$response = array();
$emp = new clsddReexamPayment();


if ($_action == "Update") {
	//print_r($_POST);
    if (isset($_POST["txtCenterCode"])) {
        $_Code = filter_var($_POST["txtCenterCode"],FILTER_SANITIZE_STRING);
        $_PayTypeCode = filter_var($_POST["productinfo"],FILTER_SANITIZE_STRING);		
        $_TranRefNo = filter_var($_POST["txtGenerateId"],FILTER_SANITIZE_STRING);
		
		$_firstname = filter_var($_POST["firstname"],FILTER_SANITIZE_STRING);
        $_phone = filter_var($_POST["phone"],FILTER_SANITIZE_STRING);		
        $_email = filter_var($_POST["email"],FILTER_SANITIZE_STRING);
		
		$_amount = filter_var($_POST["amount"],FILTER_SANITIZE_STRING);
        $_ddno = filter_var($_POST["ddno"],FILTER_SANITIZE_STRING);		
        $_dddate = filter_var($_POST["dddate"],FILTER_SANITIZE_STRING);
		
		$_txtMicrNo = filter_var($_POST["txtMicrNo"],FILTER_SANITIZE_STRING);
        $_ddlBankDistrict = filter_var($_POST["ddlBankDistrict"],FILTER_SANITIZE_STRING);		
        $_ddlBankName = filter_var($_POST["ddlBankName"],FILTER_SANITIZE_STRING);
		$_txtBranchName = filter_var($_POST["txtBranchName"],FILTER_SANITIZE_STRING);
        
		$response = $emp->Update($_Code,$_PayTypeCode,$_TranRefNo,$_firstname,$_phone,$_email,$_amount,$_ddno,$_dddate,$_txtMicrNo,$_ddlBankDistrict,$_ddlBankName,$_txtBranchName);
        echo $response[0];
    }
}

if ($_action == "GETCENTERCODE") {  
    $response = $emp->GetCenterCode($_POST['batch'],$_POST['course']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Admission_TranRefNo'] . ">" . $_Row['Admission_ITGK_Code'] . "</option>";
    }
}

if ($_action == "GETDDMODEDATA") {
    $response = $emp->GetAllDdData($_POST['refno']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";    
    echo "<th>DD No.</th>";
    echo "<th >DD Date</th>";  
    echo "<th >DD Amount</th>"; 
    echo "<th >DD Image</th>"; 
    echo "<th >Action</th>"; 	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['dd_no'] . "</td>";          
            echo "<td>" . $_Row['dd_date'] . "</td>";
			echo "<td>" . $_Row['dd_amount'] . "</td>";
			echo "<td>".'<a href="upload/admission_dd_payment/'.$_Row['dd_Transaction_Txtid'].'_ddpayment.png'.'" target="_blank">' . '<img alt="No Image Found" width="40" height="15" src="upload/admission_dd_payment/'.$_Row['dd_Transaction_Txtid'].'_ddpayment.png'.'"/>' . "</a></td>";
			//echo "<td><a href='upload/admission_dd_payment/".$_Row['dd_Transaction_Txtid']."_ddpayment.png'>". "</a></td>";
                        echo "<td><input type='checkbox' id=chk" . $_Row['dd_Transaction_Txtid'].
			" name=chk" . $_Row['dd_Transaction_Txtid'].">" . $_Row['dd_Transaction_Txtid'] . "</input>"                
                . "</td>";
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
		echo "</div>";
    } else {
        echo "No Record Found";
    }
}


if ($_action == "ADD") {           
	$_UserPermissionArray=array();
    $_AdmissionCode = array();
	$_i = 0;
    $_Count=0;
    $l="";
        foreach ($_POST as $key => $value)
            {
               if(substr($key, 0,3)=='chk')
               {                				  
				   $l .= substr($key, 3) . ",";				 
               }			   
				$_AdmissionCode = rtrim($l, ",");											
			}
                $_SESSION['LearnerAdmissionCodes'] = $_AdmissionCode;                 				
                $response=$emp->UpdateDDPaymentStatus();               		
				echo $response[0];  	   
	}
