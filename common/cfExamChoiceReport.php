<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsExamChoiceReport.php';

$response = array();
$emp = new clsExamChoiceReport();

if ($_action == "SHOW") 
{

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>Center Code</th>";
	echo "<th style='5%'>Choice1 District</th>";
    echo "<th style='20%'>Choice1 Tehsil</th>";
    echo "<th style='20%'>District Code</th>";
    echo "<th style='20%'>Choice1</th>";
	
	echo "<th style='5%'>Choice2 District</th>";
    echo "<th style='20%'>Choice2 Tehsil</th>";
    echo "<th style='20%'>District Code</th>";
    echo "<th style='20%'>Choice2</th>";
	
	echo "<th style='5%'>Choice3 District</th>";
    echo "<th style='20%'>Choice3 Tehsil</th>";
    echo "<th style='20%'>District Code</th>";
    echo "<th style='20%'>Choice3</th>";
	
	echo "<th style='5%'>Choice4 District</th>";
    echo "<th style='20%'>Choice4 Tehsil</th>";
    echo "<th style='20%'>District Code</th>";
    echo "<th style='20%'>Choice4</th>";
	
	echo "<th style='5%'>Choice5 District</th>";
    echo "<th style='20%'>Choice5 Tehsil</th>";
    echo "<th style='20%'>District Code</th>";
    echo "<th style='20%'>Choice5</th>";
    

	echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['centercode']."</td>";
		echo "<td>" . $_Row['Choice1_Dist'] . "</td>";
		echo "<td>" . $_Row['Choice1_Thesil'] . "</td>";
        echo "<td>" . $_Row['district'] . "</td>";
        echo "<td>" . $_Row['choice1'] . "</td>";
		
		echo "<td>" . $_Row['Choice2_Dist'] . "</td>";
		echo "<td>" . $_Row['Choice2_Thesil'] . "</td>";
        echo "<td>" . $_Row['district'] . "</td>";
        echo "<td>" . $_Row['choice2'] . "</td>";
		
		echo "<td>" . $_Row['Choice3_Dist'] . "</td>";
		echo "<td>" . $_Row['Choice3_Thesil'] . "</td>";
        echo "<td>" . $_Row['district'] . "</td>";
        echo "<td>" . $_Row['choice3'] . "</td>";
		
		echo "<td>" . $_Row['Choice4_Dist'] . "</td>";
		echo "<td>" . $_Row['Choice4_Thesil'] . "</td>";
        echo "<td>" . $_Row['district'] . "</td>";
        echo "<td>" . $_Row['choice4'] . "</td>";
		
		echo "<td>" . $_Row['Choice5_Dist'] . "</td>";
		echo "<td>" . $_Row['Choice5_Thesil'] . "</td>";
        echo "<td>" . $_Row['district'] . "</td>";
        echo "<td>" . $_Row['choice5'] . "</td>";
				
        echo "</tr>";		
        $_Count++;
    }
		echo "</tbody>";
		echo "<tfoot>";        
        echo "</tfoot>";
		echo "</table>";
		echo "</div>";
}
?>