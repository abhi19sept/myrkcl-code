<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsAllItgkDetails.php';

    $response = array();
    $emp = new clsAllItgkDetails();

    if ($_action == "DETAILS") {
		
	$response = $emp->GetITGKDetails($_POST['values']);
	$_DataTable = "";	
		
	echo "<div class='table-responsive'>";
	echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
	echo "<thead>";
	echo "<tr>";
	echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>ITGK-CODE</th>";
        echo "<th style='5%'>Ack No.</th>";
	echo "<th style='15%'>ITGK Name</th>";
        echo "<th style='15%'>ITGK Mobile</th>";
		echo "<th style='15%'>ITGK Mobile</th>";	
	if($_POST['values']=='RS-CFA') {
	echo "<th style='10%'>Center Type</th>";
	}
	
	echo "<th style='5%'>RSP Code</th>";
	echo "<th style='5%'>RSP Name</th>";
	echo "<th style='10%'>Login Created Date</th>";
        
        echo "<th style='10%'>Final Approval Date</th>";
        echo "<th style='10%'>Course Allocation Date</th>";
	echo "<th style='10%'>ITGK Expire Date</th>";
        echo "<th style='10%'>ITGK Address</th>";
	echo "<th style='10%'>ITGK District</th>";
	echo "<th style='10%'>ITGK Tehsil</th>";
        
        echo "<th style='10%'>IT-GK Area Type</th>";
    
        echo "<th style='10%'>Municipality Type: (Urban)/Panchayat Samiti (Rural)</th>";
        echo "<th style='10%'>Municipality Name: (Urban)/Gram Panchayat (Rural)</th>";
        echo "<th style='10%'>Ward No.: (Urban)/Village (Rural)</th>";
    
        echo "<th style='10%'>ITGK Pincode</th>";
        echo "<th style='10%'>ITGK Type</th>";
        echo "<th style='10%'>ITGK PaperLess</th>";
        echo "<th style='10%'>Amount Paid</th>";
	echo "</tr>";
	echo "</thead>";
	echo "<tbody>";
	if($response[0] == 'Success') {
	$_Count = 1;
	while ($_Row = mysqli_fetch_array($response[2])) {
	echo "<tr class='odd gradeX'>";
	echo "<td>" . $_Count . "</td>";
	echo "<td>" . $_Row['Courseitgk_ITGK'] . "</td>";
        echo "<td>" . ($_Row['User_Ack'] ? $_Row['User_Ack'] : 'NA') . "</td>";
	echo "<td>" . strtoupper($_Row['ITGK_Name']) . "</td>";
        echo "<td>" . $_Row['User_MobileNo'] . "</td>";
		echo "<td>" . $_Row['User_EmailId'] . "</td>";
	 if($_POST['values']=='RS-CFA') {
		 if($_Row['Courseitgk_EOI']=='12') {
		 echo "<td>" . 'New' . "</td>"; 
		 }
		 else {
                 echo "<td>" . 'Renewed' . "</td>";
		 }
	 }
			
	echo "<td>" . $_Row['RSP_Code'] . "</td>";            
	echo "<td>" . strtoupper($_Row['RSP_Name']) . "</td>";            
	echo "<td>" . date_format(date_create($_Row['CourseITGK_UserCreatedDate']), "d-m-Y") . "</td>"; 
        if($_Row['User_PaperLess']=='Yes') {
		 echo "<td>" . ($_Row['User_SPFinalApprovalDate'] ? date('d-m-Y', strtotime($_Row['User_SPFinalApprovalDate'])) : 'NA') . "</td>"; 
		 }
		 else {
                 echo "<td>" . ($_Row['User_FinalApprovalDate'] ? date('d-m-Y', strtotime($_Row['User_FinalApprovalDate'])) : 'NA') . "</td>";
		 }
        if($_Row['User_PaperLess']=='Yes') {
		 echo "<td>" . ($_Row['User_FinalApprovalDate'] ? date('d-m-Y', strtotime($_Row['User_FinalApprovalDate'])) : 'NA') . "</td>"; 
		 }
		 else {
                 echo "<td>" . ($_Row['User_FinalApprovalDate'] ? date('d-m-Y', strtotime($_Row['User_FinalApprovalDate'])) : 'NA') . "</td>";
		 }
        
        
	echo "<td>" . date_format(date_create($_Row['CourseITGK_ExpireDate']), "d-m-Y") . "</td>";  
        echo "<td>" . strtoupper($_Row['Organization_Address']) . "</td>";
	echo "<td>" . strtoupper($_Row['District_Name']) . "</td>";   
	echo "<td>" . strtoupper($_Row['Tehsil_Name']) . "</td>";
        
        if ($_Row['Organization_AreaType'] == '') {
                echo "<td>" . "NA" . "</td>";
            } else {
                echo "<td>" . $_Row['Organization_AreaType'] . "</td>";
            }
        if ($_Row['Organization_AreaType'] == 'Urban') {
                echo "<td>" . ($_Row['Municipality_Type_Name'] ? $_Row['Municipality_Type_Name'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Municipality_Name'] ? $_Row['Municipality_Name'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Ward_Name'] ? $_Row['Ward_Name'] : 'NA') . "</td>";
            } elseif ($_Row['Organization_AreaType'] == 'Rural') {
                echo "<td>" . ($_Row['Block_Name'] ? $_Row['Block_Name'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['GP_Name'] ? $_Row['GP_Name'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Village_Name'] ? $_Row['Village_Name'] : 'NA') . "</td>";
            } else {
                echo "<td>" . "NA" . "</td>";
                echo "<td>" . "NA" . "</td>";
                echo "<td>" . "NA" . "</td>";
            }
        echo "<td>" . ($_Row['Organization_PinCode'] ? $_Row['Organization_PinCode'] : 'NA') . "</td>";    
        echo "<td>" . strtoupper($_Row['User_Type']) . "</td>";
        echo "<td>" . strtoupper($_Row['User_PaperLess']) . "</td>";
        echo "<td>" . ($_Row['User_Type'] == 'New' ? $_Row['Ncr_Transaction_Amount'] : '51000') . "</td>";
	echo "</tr>";
	$_Count++;
	}
	echo "</tbody>";
	echo "</table>";
	echo "</div>";
	}
    }
	 
if ($_action == "FillCourse") {
    $response = $emp->GetAllCourse();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Courseitgk_Course'] . "'>" . $_Row['Courseitgk_Course'] . "</option>";
    }
}
?>