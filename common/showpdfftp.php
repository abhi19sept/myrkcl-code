<?php
require '../DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection(); 
$details= $_ObjFTPConnection->ftpdetails();
$docprf =  $details.$_REQUEST['src'];

$filename=explode("/",$_REQUEST['src']);

$content = file_get_contents($docprf);
 $ext = trim(pathinfo($_REQUEST['src'], PATHINFO_EXTENSION));

if($ext == 'csv'){
	header('Content-Type: application/csv');
header('Content-Disposition: attachment; filename='.$filename[1]);
header('Pragma: no-cache');
die($content);
}
elseif ($ext == 'jpg') {

	$imageData = base64_encode($content);
	// Output the image
	echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot'/>";
}
else{
	
	header('Content-Type: application/pdf');
	header('Content-Length: ' . strlen($content));
	header('Content-Disposition: inline; filename='.$filename[1]);
	header('Cache-Control: private, max-age=0, must-revalidate');
	header('Pragma: public');
	ini_set('zlib.output_compression','0');
	die($content);
}

?>