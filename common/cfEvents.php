<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsEvents.php';

$response = array();
$emp = new clsEvents();




if ($_action == "FILLEvent") {
    $response = $emp->FILLEvent($_POST['category']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Type_ID'] . ">" . $_Row['Event_Type_Name'] . "</option>";
    }
}

if ($_action == "SHOWAdmissionPay") {
    $response1 = $emp->SHOWAdmissionPay($_POST['batch'],$_POST['course']);
    $response2 = $emp->SHOWAdmissionPayDD($_POST['batch'],$_POST['course']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response1[2])) {
        echo "<option value=" . $_Row['Event_Payment'] . ">" . $_Row['payment_mode'] . "</option>";
    }
    while ($_Row = mysqli_fetch_array($response2[2])) {
        echo "<option value=" . $_Row['Event_Payment'] . ">" . $_Row['payment_mode'] . "</option>";
    }
}

if ($_action == "SHOWEOIPay") {
    //echo $_POST['course'];
    $response = $emp->SHOWEOIPay($_POST['courseeoi']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Payment'] . ">" . $_Row['payment_mode'] . "</option>";
    }
}

if ($_action == "SHOWReexamPay") {
    $response = $emp->SHOWReexamPay($_POST['examevent'],$_POST['course']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Payment'] . ">" . $_Row['payment_mode'] . "</option>";
    }
}

if ($_action == "SHOWCorrectionPay") {
    $response = $emp->SHOWCorrectionPay($_POST['code']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Payment'] . ">" . $_Row['payment_mode'] . "</option>";
    }
}