<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsWcdFeedbackRpt.php';

$response = array();
$emp = new clsWcdFeedbackRpt();

if ($_action == "GETDATA") 
{
    $course = $_POST['course'];
    $batch = $_POST['batch'];
    $_LoginUserRole = $_SESSION['User_UserRoll'];
    if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' || $_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || $_LoginUserRole == '10' || $_LoginUserRole == '11' || $_LoginUserRole == '28') {
        $response = $emp->GetDatafirst($course, $batch);
    }
    if ($_LoginUserRole == '17') {
        $response = $emp->GetDataPO($course, $batch);
    }
    if ($_LoginUserRole == '13' || $_LoginUserRole == '14') {

        $response = $emp->GetDataSP($course, $batch);
    }
   
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th >बहुत अच्छा </th>";
    echo "<th >अच्छा </th>";
    echo "<th >सामान्य </th>";
    echo "<th >सुधार की आवश्यकता  </th>";
    echo "<th >Total Feedbacks by Learner</th>";



    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            $total = ($_Row['adhik'] + $_Row['atyadhik'] + $_Row['samanya'] + $_Row['kam']);
            $exclnt = ($_Row['atyadhik'] / $total) * 100;
            $vgud = ($_Row['adhik'] / $total) * 100;
            $gud = ($_Row['samanya'] / $total) * 100;
            $avg = ($_Row['kam'] / $total) * 100;

            echo "<td>" . $_Row['Wcd_Feedback_Itgk_Code'] . "</td>";
            echo "<td>" . $exclnt . "% </td>";
            echo "<td>" . $vgud . "% </td>";
            echo "<td>" . $gud . "% </td>";
            echo "<td>" . $avg . "% </td>";
            echo "<td>" . $_Row['lcount'] . "</td>";


            echo "</tr>";

            $_Count++;
        }

        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}






if ($_action == "GETCUSTOM") 
{
    $course = $_POST['course'];
    $batch = $_POST['batch'];
    $_LoginUserRole = $_SESSION['User_UserRoll'];
	
    if ($_LoginUserRole == '1' || $_LoginUserRole == '2' || $_LoginUserRole == '3' ||
	$_LoginUserRole == '4' || $_LoginUserRole == '8' || $_LoginUserRole == '9' || 
	$_LoginUserRole == '10' || $_LoginUserRole == '11' || $_LoginUserRole == '28') {
        $response = $emp->GetCustomDatafirst($course, $batch);
	}
	else if($_LoginUserRole == '14'){
		$response = $emp->GetCustomDatafirstforSP($course, $batch);
	}
	else if($_LoginUserRole == '7'){
		$response = $emp->GetCustomDatafirstforITGK($course, $batch);
	}
		//print_r($response);
		$_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th colspan='4'>आपके द्वारा ऑनलाइन प्रशिक्षण/क्लास हेतु किस DEVICE का प्रयोग किया गया </th>";
    echo "<th colspan='7'>ऑनलाइन क्लास हेतु आपके द्वारा कौन सा TOOL का उपयोग किया गया </th>";
	echo "<th colspan='4'>प्रशिक्षण पूर्ण करने हेतु ली गया ऑनलाइन क्लास/मार्गदर्शन की संख्या </th>";
	echo "<th colspan='4'>Faculty द्वारा पढ़ाने की क्षमता, ज्ञान व प्रतिबद्धता   </th>";
	echo "<th colspan='2'>ऑनलाइन प्रशिक्षण पूर्ण किया गया </th>";
	echo "<th colspan='4'>ऑनलाइन प्रशिक्षण से आप कितने संतुष्ट है  </th>";
	echo "<th> फीडबैक देने वाले प्रशिक्षुओं की संख्या  </th>";
	
    

    echo "</tr>";
	echo "<tr>";
    echo "<th colspan='2'></th>";
    echo "<th>Desktop </th>";
    echo "<th>Laptop</th>";
    echo "<th>Smartphone </th>";
    echo "<th> संशाधन उपलब्ध नहीं  </th>";
	
	
	echo "<th> Microsoft Team </th>";
    echo "<th>Zoom App </th>";
	echo "<th >Google </th>";
	echo "<th >Webex</th>";
	echo "<th>WhatsApp </th>";
	echo "<th >Telegram  </th>";
	echo "<th >कोई अन्य टूल </th>";
	
	
	echo "<th >अत्यधिक</th>";
	echo "<th>उचित </th>";
	echo "<th >कम  </th>";
	echo "<th > बहुत कम </th>";
	
	
	echo "<th >बहुत अच्छा</th>";
	echo "<th>सामान्य </th>";
	echo "<th >संतोषप्रद  </th>";
	echo "<th > असंतुष्ट  </th>";
	
	
	
	echo "<th >हाँ</th>";
	echo "<th>नहीं </th>";
	
	
	
	echo "<th> अत्यधिक</th>";
	echo "<th>सामान्य  </th>";
	echo "<th> कम </th>";
	echo "<th> बिल्कुल नहीं  </th>";

    echo "<th colspan='2'></th>";


    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	//print_r($response);
    $co = mysqli_num_rows($response[2]);
    //print_r($co);
    if ($co) 
    {
        while ($_Row = mysqli_fetch_array($response[2])) 
        {
            //print_r($_Row);
			if($_Row['lcount']=='0')
            {
				
			}
			else
            {
			echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
			
            
			 $total1 = ($_Row['Desktop1'] + $_Row['Laptop1'] + $_Row['Smartphone1'] + $_Row['notavilable1']);
            
			 $total2 = ($_Row['Microsoft2'] + $_Row['Zoom2'] + $_Row['Google2'] + $_Row['Webex2']+ $_Row['WhatsApp2'] + $_Row['Telegram2'] + $_Row['othertool2']);
			 
			 $total3 = ($_Row['atyadhik3'] + $_Row['uchit3'] + $_Row['kam3'] + $_Row['bahutkam3']);
            
			 $total4 = ($_Row['bahutaccha4'] + $_Row['samanya4'] + $_Row['santoshpad4'] + $_Row['asantusth4']);
            
			 $total5 = ($_Row['ha5'] + $_Row['nahi5']);
            
			 $total6 = ($_Row['ataydhik6'] + $_Row['samanya6'] + $_Row['kam6'] + $_Row['BilkulNahi6']);
            
            
				//$exclnt = ($_Row['atyadhik'] / $total) * 100;
				
				//$exclnt = ($_Row['atyadhik'] / $total) * 100;
			$Desktop1 = round(($_Row['Desktop1'] / $total1) * 100,2);
			$Laptop1 = round(($_Row['Laptop1'] / $total1) * 100,2);
			$Smartphone1 =round(($_Row['Smartphone1'] / $total1) * 100,2);
			$notavilable1 =round(($_Row['notavilable1'] / $total1) * 100,2);
			$Microsoft2 = round(($_Row['Microsoft2'] / $total2) * 100,2);
			$Zoom2 =round(($_Row['Zoom2'] / $total2) * 100,2);
			$Google2 =round(($_Row['Google2'] / $total2) * 100,2);
			$Webex2 = round(($_Row['Webex2'] / $total2) * 100,2);
			$WhatsApp2 = round(($_Row['WhatsApp2'] / $total1) * 100,2);
			$Telegram2 = round(($_Row['Telegram2'] / $total2) * 100,2);
			$othertool2 = round(($_Row['othertool2'] / $total2) * 100,2);
			$atyadhik3 = round(($_Row['atyadhik3'] / $total3) * 100,2);
			$uchit3 = round(($_Row['uchit3'] / $total3) * 100,2);
			$kam3 = round(($_Row['kam3'] / $total3) * 100,2);
			$bahutkam3 = round(($_Row['bahutkam3'] / $total3) * 100,2);
			$bahutaccha4 =round(($_Row['bahutaccha4'] / $total4) * 100,2);
			$samanya4 = round(($_Row['samanya4'] / $total4) * 100,2);
			$santoshpad4 = round(($_Row['santoshpad4'] / $total4) * 100,2);
			$asantusth4 = round(($_Row['asantusth4'] / $total4) * 100,2);
			$ha5 = round(($_Row['ha5'] / $total5) * 100,2);
			$nahi5 =round(($_Row['nahi5'] / $total5) * 100,2);
			$ataydhik6 = round(($_Row['ataydhik6'] / $total6) * 100,2);
			$samanya6 = round(($_Row['samanya6'] / $total6) * 100,2);
			$kam6 = round(($_Row['kam6'] / $total6) * 100,2);
			$BilkulNahi6 =round(($_Row['BilkulNahi6'] / $total6) * 100,2);

            echo "<td>" . $_Row['Wcd_Feedback_Itgk_Code'] . "</td>";

            echo "<td>" . $Desktop1 . "% </td>";
            echo "<td>" . $Laptop1 . "% </td>";
            echo "<td>" . $Smartphone1 . "% </td>";
            echo "<td>" . $notavilable1 . "% </td>";
			
			echo "<td>" . $Microsoft2 . "% </td>";
            echo "<td>" . $Zoom2 . "% </td>";
            echo "<td>" . $Google2 . "% </td>";
            echo "<td>" . $Webex2 . "% </td>";
			echo "<td>" . $WhatsApp2 . "% </td>";
            echo "<td>" . $Telegram2 . "% </td>";
			echo "<td>" . $othertool2 . "% </td>";
			
			
			echo "<td>" . $atyadhik3 . "% </td>";
            echo "<td>" . $uchit3 . "% </td>";
            echo "<td>" . $kam3 . "% </td>";
            echo "<td>" . $bahutkam3 . "% </td>";
			
			echo "<td>" . $bahutaccha4 . "% </td>";
            echo "<td>" . $samanya4 . "% </td>";
            echo "<td>" . $santoshpad4 . "% </td>";
            echo "<td>" . $asantusth4 . "% </td>";
			
			echo "<td>" . $ha5 . "% </td>";
            echo "<td>" . $nahi5 . "% </td>";
            
			
			echo "<td>" . $ataydhik6 . "% </td>";
            echo "<td>" . $samanya6 . "% </td>";
            echo "<td>" . $kam6 . "% </td>";
            echo "<td>" . $BilkulNahi6 . "% </td>";
			
			
			//echo "<td>" . $_Row['tcount']. " </td>";
            echo "<td>" . $_Row['lcount'] . " </td>";
			

            echo "</tr>";

            $_Count++;
			}
            
        }

        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    } 
    else 
    {
        echo "No Record Found";
    }
		
    
  /*  else if ($_LoginUserRole == '17')
		{
        $response = $emp->GetCustomDataPO($course,$batch);
		$_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th colspan='2'>ऑनलाइन प्रशिक्षण पूर्ण किया गया </th>";
    echo "<th colspan='4'>ऑनलाइन प्रशिक्षण से आप कितने संतुष्ट है </th>";
   echo "<th >कुल आवंटित सीटों की संख्या</th>";
    echo "<th >फीडबैक देने वाले प्रशिक्षुओं की संख्या </th>";
	
    

    echo "</tr>";
	
	
	 echo "<tr>";
    echo "<th colspan='2'></th>";
    
    echo "<th >हाँ </th>";
    echo "<th >नहीं</th>";
    echo "<th >अत्यधिक </th>";
    echo "<th >सामान्य </th>";
    echo "<th >कम</th>";
	echo "<th >बिल्कुल नहीं</th>";
	echo "<th colspan='2'></th>";



    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
			if($_Row['lcount']=='0'){
				
			}
			else{
				echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
			
             $total1 = ($_Row['Ha'] + $_Row['Nahi']);
			$total2 = ($_Row['Ataydhik'] + $_Row['Samanya'] + $_Row['Kam'] + $_Row['BilkulNahi']);;
            $ha = round(($_Row['Ha'] / $total1) * 100,2);
            $nahi = round(($_Row['Nahi'] / $total1) * 100,2);
            $Ataydhik = round(($_Row['Ataydhik'] / $total2) * 100,2);
			$Samanya = round(($_Row['Samanya'] / $total2) * 100,2);
            $Kam = round(($_Row['Kam'] / $total2) * 100,2);
            $BilkulNahi = round(($_Row['BilkulNahi'] / $total2) * 100,2);
           

            echo "<td>" . $_Row['ITGK'] . "</td>";
            echo "<td>" . $ha . "% </td>";
            echo "<td>" . $nahi . "% </td>";
            echo "<td>" . $Ataydhik . "% </td>";
            echo "<td>" . $Samanya . "% </td>";
			echo "<td>" . $Kam . "% </td>";
            echo "<td>" . $BilkulNahi . "% </td>";
			echo "<td>" . $_Row['tcount'] . "</td>";
            echo "<td>" . $_Row['lcount'] . "</td>";
            
			
			

            echo "</tr>";

            $_Count++;
			}
            
        }

        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
		
    }
    else if ($_LoginUserRole == '13' || $_LoginUserRole == '14') {

        $response = $emp->GetCustomDataSP($course, $batch);
		$_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th colspan='2'>ऑनलाइन प्रशिक्षण पूर्ण किया गया </th>";
    echo "<th colspan='4'>ऑनलाइन प्रशिक्षण से आप कितने संतुष्ट है </th>";
   
    echo "<th >फीडबैक देने वाले प्रशिक्षुओं की संख्या </th>";
	
    

    echo "</tr>";
	
	
	 echo "<tr>";
    echo "<th colspan='2'></th>";
    
    echo "<th >हाँ </th>";
    echo "<th >नहीं</th>";
    echo "<th >अत्यधिक </th>";
    echo "<th >सामान्य </th>";
    echo "<th >कम</th>";
	echo "<th >बिल्कुल नहीं</th>";
	echo "<th colspan='2'></th>";



    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
			if($_Row['lcount']=='0'){
				
			}
			else{
				echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
			
             $total1 = ($_Row['Ha'] + $_Row['Nahi']);
			$total2 = ($_Row['Ataydhik'] + $_Row['Samanya'] + $_Row['Kam'] + $_Row['BilkulNahi']);;
            $ha = round(($_Row['Ha'] / $total1) * 100,2);
            $nahi = round(($_Row['Nahi'] / $total1) * 100,2);
            $Ataydhik = round(($_Row['Ataydhik'] / $total2) * 100,2);
			$Samanya = round(($_Row['Samanya'] / $total2) * 100,2);
            $Kam = round(($_Row['Kam'] / $total2) * 100,2);
            $BilkulNahi = round(($_Row['BilkulNahi'] / $total2) * 100,2);
           

            echo "<td>" . $_Row['Wcd_Feedback_Itgk_Code'] . "</td>";
            echo "<td>" . $ha . "% </td>";
            echo "<td>" . $nahi . "% </td>";
            echo "<td>" . $Ataydhik . "% </td>";
            echo "<td>" . $Samanya . "% </td>";
			echo "<td>" . $Kam . "% </td>";
            echo "<td>" . $BilkulNahi . "% </td>";
            echo "<td>" . $_Row['lcount'] . "</td>";
			

            echo "</tr>";

            $_Count++;
			}
            
        }

        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
		
    }*/
   
    
}
/*if ($_action == "GETDATALearner") 
{
    $course = $_POST['course'];
    $batch = $_POST['batch'];

    $response = $emp->GetDatafirstLearner($course, $batch);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th colspan='5' style='font-size:14px;'>Learner Details</th>";
    echo "<th  colspan='4' style='font-size:14px;'><b>Theoretical Feedback</b></th>";
    echo "<th  colspan='3' style='font-size:14px;'><b>Practical session</b></th>";
    echo "<th  colspan='3' style='font-size:14px;'><b>Faculty Feedback</b></th>";
    echo "<th  colspan='2' style='font-size:14px;'><b>Others Feedback</b></th>";
    echo "<th style='font-size:14px;'><b>Over All Feedback</b></th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th>District</th>";
    echo "<th >Learner Code</th>";
    echo "<th >Learner Name</th>";
    echo "<th ><p class='question' style='font-size:8px;'>प्रशिक्षण पाठ्यक्रमानुसार पूर्ण किया गया  </p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>अतिरिक्त संबंधित जानकारी प्रदान की गई    </p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>मुख्य विषयों को दिया गया समय</p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>समयबद्धता व नियमित्ता</p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>समस्त आवश्यक विषयों पर प्रायोगिक सत्र  </p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>प्रायोगिक सत्र के दौरान कम्प्यूटर/संबंधित संसाधन सही कार्य कर रहे थे </p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>कठनाईया दूर की गई  </p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>पढ़ाने/समझाने की क्षमता </p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>विषय का ज्ञान </p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>कठनाईया दूर की गई  </p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>आवश्यकतानुसार प्रोजेक्टर व अन्य उपकरणों का उपयोग</p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>चाही गई भाषा में पाठ्य सामग्री उपलब्ध कराई गई </p></th>";
    echo "<th ><p class='question' style='font-size:8px;'>स्वयं के विकास में आपको यह पाठ्यक्रम कितनी सहायता प्रदान कर सकता है </p></th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";

            echo "<td>" . $_Row['Wcd_Feedback_Itgk_Code'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>'" . $_Row['Wcd_Feedback_lcode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Wcd_Feedback_Lname']) . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_1'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_2'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_3'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_4'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_5'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_6'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_7'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_8'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_9'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_10'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_11'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_12'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback_Ques_13'] . "</td>";

            echo "</tr>";

            $_Count++;
        }

        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}*/


if ($_action == "GETDATALearner") 
{
    $course = $_POST['course'];
    $batch = $_POST['batch'];

    $response = $emp->GetDatafirstLearner($course, $batch);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th>District Name</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    
    echo "<th>आपके द्वारा ऑनलाइन प्रशिक्षण/क्लास हेतु किस DEVICE का प्रयोग किया गया </th>";
    echo "<th>ऑनलाइन क्लास हेतु आपके द्वारा कौन सा TOOL का उपयोग किया गया </th>";
	echo "<th>प्रशिक्षण पूर्ण करने हेतु ली गया ऑनलाइन क्लास/मार्गदर्शन की संख्या </th>";
	echo "<th>Faculty द्वारा पढ़ाने की क्षमता, ज्ञान व प्रतिबद्धता   </th>";
	echo "<th>ऑनलाइन प्रशिक्षण पूर्ण किया गया </th>";
	echo "<th>ऑनलाइन प्रशिक्षण से आप कितने संतुष्ट है  </th>";
	
    echo "</tr>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";

            echo "<td>" . $_Row['Wcd_Feedback_Itgk_Code'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>'" . $_Row['Wcd_Feedback_lcode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Wcd_Feedback_Lname']) . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback1_Ques_1'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback1_Ques_2'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback1_Ques_3'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback1_Ques_4'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback1_Ques_5'] . "</td>";
            echo "<td>" . $_Row['Wcd_Feedback1_Ques_6'] . "</td>";

            echo "</tr>";

            $_Count++;
        }

        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

