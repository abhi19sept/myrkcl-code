<?php

/*
 *  author Viveks
 */
ob_start();
require 'commonFunction.php';
require 'BAL/clsReconcilationReport.php';

$response = array();
$emp = new clsReconcilationReport();
//print_r($_POST);

ini_set("memory_limit", "5000M");
//ini_set("max_execution_time", 0);
set_time_limit(0);
//ini_set("upload_max_filesize", "855M");
//ini_set("post_max_size", "1000M");
ini_set("max_execution_time", "30000000000");
ini_set("max_input_time", "30000000000");


if ($_action == "reconcilation") {
	
	$startdate = $_POST["txtstartdate"]." 00:00:00";
	$enddate = $_POST["txtenddate"]." 23:59:59";
	$reporttype = $_POST["reportType"];    
	$dir = "reconcilation";// file name
	if($_SERVER['HTTP_HOST']=="localhost")
    {
            $path = $_SERVER['DOCUMENT_ROOT'] . '/myrkcl/upload/';
            $serverpath = $_SERVER['HTTP_HOST'] . '/myrkcl/upload/';
    }else{
            $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/';
            $serverpath = $_SERVER['HTTP_HOST'] . '/upload/';
    }
	if( !is_dir($path.$dir))
	{
		
			mkdir($path.$dir, 0700);
	}
	
    //$_DataTable = "";
    $_DataTable = array();
    
    $_Count = 1;
/* Type 1 */
	if($reporttype == "learner_fee"){
		$report = $_POST['report1'];
		if($report == "report_1"){
			$response = $emp->reconcilation_type1_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		
		if($report == "report_2"){
			$response = $emp->reconcilation_type1_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			
			if ($co) {
				
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				
				//readfile($serverpath .$dir."/" .$filename);
	
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
																				
		if($report == "report_3"){
			$response = $emp->reconcilation_type1_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
																	
		if($report == "report_4"){
			
			$response = $emp->reconcilation_type1_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
																				
		if($report == "report_5"){
			
			$response = $emp->reconcilation_type1_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
																				
	}
/* Type 2 */	
	if($reporttype == "reexam_fee"){
		$report = $_POST['report2'];
		if($report == "report_1"){
			$response = $emp->reconcilation_type2_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_2"){
			$response = $emp->reconcilation_type2_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_3"){
			$response = $emp->reconcilation_type2_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_4"){
			$response = $emp->reconcilation_type2_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_5"){
			$response = $emp->reconcilation_type2_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
	}
/* Type 3 */	
	if($reporttype == "NCR_fee"){
		$report = $_POST['report3'];
		if($report == "report_1"){
			$response = $emp->reconcilation_type3_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_2"){
			$response = $emp->reconcilation_type3_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
			
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_3"){
			$response = $emp->reconcilation_type3_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_4"){
			$response = $emp->reconcilation_type3_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_5"){
			$response = $emp->reconcilation_type3_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
	}
/* Type 4 */
	if($reporttype == "eoi_fee"){
		$report = $_POST['report4'];
		if($report == "report_1"){
			$response = $emp->reconcilation_type4_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_2"){
			$response = $emp->reconcilation_type4_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_3"){
			$response = $emp->reconcilation_type4_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_4"){
			$response = $emp->reconcilation_type4_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_5"){
			$response = $emp->reconcilation_type4_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
	}
/* Type 5 */
	if($reporttype == "correction_fee"){
		$report = $_POST['report5'];
		if($report == "report_1"){
			$response = $emp->reconcilation_type5_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_2"){
			$response = $emp->reconcilation_type5_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_3"){
			$response = $emp->reconcilation_type5_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_4"){
			$response = $emp->reconcilation_type5_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_5"){
			$response = $emp->reconcilation_type5_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
	}
/* Type 6 */
    if($reporttype == "name_fee"){
		$report = $_POST['report6'];
		if($report == "report_1"){
			$response = $emp->reconcilation_type6_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_2"){
			$response = $emp->reconcilation_type6_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_3"){
			$response = $emp->reconcilation_type6_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_4"){
			$response = $emp->reconcilation_type6_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_5"){
			$response = $emp->reconcilation_type6_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				$row21 = mysqli_fetch_row( $response[2],true );
				$fields = mysqli_num_fields ( $response[2] );
				$header='';
				for ( $i = 0; $i < $fields; $i++ )
				{
					$header .= mysqli_field_name( $response[2] , $i ) . "\t";
				}
				$data='';
				while( $row = mysqli_fetch_row( $response[2],true ) )
				{
					$line = '';
					foreach( $row as $value )
					{                                          
						if ( ( !isset( $value ) ) || ( $value == "" ) )
						{
							$value = "\t";
						}
						else
						{
							$value = str_replace( '"' , '""' , $value );
							if(isset($row['Admission_LearnerCode'])){
									if($value == $row['Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									}
									elseif ($value == $row['Admission_TranRefNo'])
									{
										$t="'";
										$value=$t.$row['Admission_TranRefNo'];
										$value = '"' . $value . '"' . "\t";
									}
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}
							elseif(isset($row['Oasis_Admission_LearnerCode'])){
							  if($value == $row['Oasis_Admission_LearnerCode'])
									{
										$t="'";
										$value=$t.$row['Oasis_Admission_LearnerCode'];
										$value = '"' . $value . '"' . "\t";
									} 
									else{
										$value = '"' . $value . '"' . "\t";
									}
							}else{
									$value = '"' . $value . '"' . "\t";
							}
						}

						$line .= $value;
					}
					$data .= trim( $line ) . "\n";
				}
				$data = str_replace( "\r" , "" , $data );

				if ( $data == "" )
				{
					$data = "\n(0) Records Found!\n";                        
				}

				//$filename = "myFile.xls";
				$filename = $_REQUEST['fileName'].".xls";
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=".$filename);
				header("Pragma: no-cache");
				header("Expires: 0");
				//print "$header\n$data";

				file_put_contents($path .$dir."/" . $filename, "$header\n$data");
				//echo $filename;
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					echo "/myrkcl/upload/reconcilation/" .$filename;
				}else{
					echo "/upload/reconcilation/" .$filename;
				}
				
			} 
			else {
				echo "No Record Found";
			}
		}
	}
}



