<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsReexamPayment_Ver1.php';
$response = array();
$emp = new clsReexamPayment();

if ($_action == "FILLEXAMEVENT") {
    $response = $emp->GetExamEvent($_POST['values']);
    echo "<option value=''>Select Exam Event</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Affilate_Event'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "FILLEXAMEVENTDDConfirm") {
    $response = $emp->GetExamEventDDConfirm($_POST['values']);
    echo "<option value=''>Select Exam Event</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Affilate_Event'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "GETCENTERREEXAM") {
    $response = $emp->GetCenterCodeRexamdd($_POST['examevent'], $_POST['course']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['reexxam_TranRefNo'] . ">" . $_Row['itgkcode'] . "</option>";
    }
}

//if ($_action == "FILLREEXAMBATCH") {
//    $response = $emp->GetReexamBatch($_POST['values']);
//    echo "<option value=''>Select Batch</option>";
//    while ($_Row = mysqli_fetch_array($response[2])) {
//        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
//    }
//}

if ($_action == "ADD") {
    //print_r($_POST);
    if (empty($_POST["amounts"]) || !isset($_POST["examCode"])) {
        echo "0";
    } else {
        $amount = $emp->AddPayTran($_POST['ddlExamEvent'], $_POST['ddlCourse'], $_POST["amounts"], $_POST["examCode"], $_POST['txtGenerateId'], $_POST['txtGeneratePayUId']);
        if ($_POST['gateway'] == 'razorpay') {
            $itgk = $general->getTransactionItgkDetails($_POST['txtGeneratePayUId']);
            $details = [
                "pay_for" => $itgk['payfor'],
                "name"    => "ITGK(" . $itgk['Pay_Tran_ITGK'] . ")",
                "email"   => $itgk['User_EmailId'],
                "phone"   => $itgk['User_MobileNo'],
                "address" => $itgk['Organization_Address'],
                "orderId" => $itgk['Pay_Tran_Code'],
                "firstname"=> $itgk['Pay_Tran_Fname'],
                "itgkcode"=> $itgk['Pay_Tran_ITGK'],
                "txnid"   => $itgk['Pay_Tran_PG_Trnid'],
                "batch"   => $itgk['Pay_Tran_Batch'],
                "course"  => $itgk['Pay_Tran_Course'],
                "amount"  => $itgk['Pay_Tran_Amount'],
            ];
            require("razorpay.php");
        } else {
            $result = [];
            $result['payby'] = "payu";
            $result['amount'] = $amount;
            echo json_encode($result);
        }
    }
}

if ($_action == "SHOWALL") {
    $response = $emp->GetAll($_POST['paymode'], $_POST['course'], $_POST['event']);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th>Father Name</th>";
    echo "<th>D.O.B</th>";
    echo "<th>Amount</th>";
    echo "<th>Batch</th>";
    echo "<th><input type='checkbox' id='checkuncheckall' name='checkuncheckall' value=''> Select All</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['learnercode'] . "</td>";
            echo "<td>" . ucwords($_Row['learnername']) . "</td>";
            echo "<td>" . ucwords($_Row['fathername']) . "</td>";
            echo "<td>" . $_Row['dob'] . "</td>";
            echo "<td>" . $_Row['fee'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            if ($_Row['paymentstatus'] == '0') {
                echo "<td><input type='checkbox' id=chk" . $_Row['examdata_code'] .
                " name='examCode[]' value = '" . $_Row['examdata_code'] . "'></input></td>";
            }
            echo "</tr>";
            $_Count++;
        }
    } else {
        //echo "invaliddata";
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}



if ($_action == "Fee") {
    //print_r($_POST);
    $response = $emp->GetAll($_POST['paymode'], $_POST['course'], $_POST['event']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['fee'];
}

if ($_action == "GETEXAMEVENTNAME") {
    // print_r($_POST);
    $response = $emp->GetEventName($_POST['examevent']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Event_Name'];
}

if ($_action == "GETDDMODEDATAREEXAM") {
    $response = $emp->GetAllDdDataReexam($_POST['refno']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>DD No.</th>";
    echo "<th >DD Date</th>";
    echo "<th >DD Amount</th>";
    echo "<th >DD Image</th>";
    echo "<th >Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['dd_no'] . "</td>";
            echo "<td>" . $_Row['dd_date'] . "</td>";
            echo "<td>" . $_Row['dd_amount'] . "</td>";
            echo "<td>" . '<a href="upload/admission_dd_payment/' . $_Row['dd_Transaction_Txtid'] . '_ddpayment.png' . '" target="_blank">' . '<img alt="No Image Found" width="40" height="15" src="upload/admission_dd_payment/' . $_Row['dd_Transaction_Txtid'] . '_ddpayment.png' . '"/>' . "</a></td>";
            //echo "<td><a href='upload/admission_dd_payment/".$_Row['dd_Transaction_Txtid']."_ddpayment.png'>". "</a></td>";
            echo "<td><input type='checkbox' id=chk" . $_Row['dd_Transaction_Txtid'] .
            " name=chk" . $_Row['dd_Transaction_Txtid'] . ">" . $_Row['dd_Transaction_Txtid'] . "</input>"
            . "</td>";
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "ADDddconfirm") {
    $_UserPermissionArray = array();
    $_AdmissionCode = array();
    $_MobileNo = array();
    $_i = 0;
    $_Count = 0;
    $l = "";
    //print_r($_POST);
    $course = $_POST['ddlCourse'];
    $examevent = $_POST['eventname'];
    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 3) == 'chk') {
            $l .= substr($key, 3) . ",";
        }
        $_AdmissionCode = rtrim($l, ",");
    }
    $_SESSION['LearnerAdmission'] = $_AdmissionCode;
    $_MobileNo = $emp->GetLearnerMobile($_AdmissionCode);
    $_i = 0;
    $MobileNo = '';
    while ($_Row = mysqli_fetch_array($_MobileNo[2])) {

        $MobileNo.=$_Row['Admission_Mobile'] . ",";
        $_i = $_i + 1;
    }
    $MobileNo3 = rtrim($MobileNo, ",");
    $response = $emp->UpdateReexamDDPaymentConfirm($course, $examevent, $MobileNo3);
    echo $response[0];
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
	//print_r($response);
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
	//values
	$key = "X2ZPKM";
	$salt = "8IaBELXB";

    //For Test Mode
    //$key = 'gtKFFx';
    //$salt = "eCwWELxi";
    
	$command1 = "check_isDomestic";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	//$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
	//$var3 = "500";//  Amount to be used in case of refund

	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);	 
	$_DataTable = array();
    $_i = 0;
		$_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
				echo json_encode($_DataTable);		
 }

 if ($_action == "FinalValidateCard") {
	//values
	//For Live Mode
    $key = "X2ZPKM";
	$salt = "8IaBELXB";

    //For Test Mode
    //$key = 'gtKFFx';
    //$salt = "eCwWELxi";

	$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	//$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
	//$var3 = "500";//  Amount to be used in case of refund

	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);	
	if($response == 'Invalid'){
		echo "1";
	}
	else if($response == 'valid'){
		echo "2";
	}
 }
 
function curlCall($wsUrl, $qs, $true){
   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
		if (curl_errno($c)) {
		  $c_error = curl_error($c);
		  if (empty($c_error)) {
			  $c_error = 'Some server error';
			}
			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);
		return $arr;
}
?>
