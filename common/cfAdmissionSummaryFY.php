<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsAdmissionSummaryFY.php';

$response = array();
$emp = new clsAdmissionSummaryFY();


if ($_action == "FILLFY") {
    $response = $emp->FILLFY();
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Financial_Year'] . ">" . $_Row['Financial_Year'] . "</option>";
    }
}


//if ($_action == "FILLCC") {
//    $response = $emp->FILLCC($_POST['values']);
//    echo "<option value='0' selected='selected'>Select </option>";
//    while ($_Row = mysqli_fetch_array($response[2])) {
//        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Financial_Year'] . "</option>";
//    }
//}


if ($_action == "GETDATA") {

    $response = $emp->GetDataAll($_POST['fy'], $_POST['cc']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Financial Year</th>";
    echo "<th >Course Name</th>";
    echo "<th >Admission Count</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;
    $_Totaleconfirm = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_POST['fy'] . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
            // echo "<td>" . $_Row['admcount'] . "</td>";
            echo "<td><a href='frmAdmissionSymmaryFYdetail.php?course=" . $_Row['Course_Code'] . "&fy=" . $_POST['fy'] . "&mode=Show'  target='_blank'>"
            . "" . $_Row['admcount'] . "</a></td>";

            echo "</tr>";
            $_Total = $_Total + $_Row['admcount'];
            $_Count++;
        }

        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
        echo "<th >Total Admission Count </th>";
        echo "<th >  </th>";
        echo "<th>";
        echo "$_Total";
        echo "</th>";
        echo "</tfoot>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETAdmissionSymmaryFYdetail") {
    $batcharray = array();

    $i = 0;

    $response3 = $emp->GetBatchFY($_POST['cc'], $_POST['fy']);
    while ($_Row1 = mysqli_fetch_array($response3[2])) {


        $batcharray[$i]['Batch_Code'] = $_Row1['Batch_Code'];
        $batcharray[$i]['Batch_Name'] = $_Row1['Batch_Name'];
        $batcharray[$i]['Course_Name'] = $_Row1['Course_Name'];
        $i++;
    }
    if ($i < 12) {
        // print_r($i);
        for ($j = $i; $j <= 12; $j++) {
            $batcharray[$j]['Batch_Code'] = "0";
            $batcharray[$j]['Batch_Name'] = "NoBatch";
        }
    }
    $response = $emp->GetAdmissionSymmaryFYdetail($batcharray);
    $_DataTable = "";


    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condenced'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th colspan='2'>Financial year</th>";
    echo "<th colspan='2'>" . $_POST['fy'] . "</th>";
    echo "<th colspan='2'>Course Name</th>";

    echo "<th colspan='2'>" . $batcharray[1]['Course_Name'] . "</th>";
    echo "<th colspan='9'></th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>ITGK Code</th>";
  //  echo "<th>Center Name</th>";
  //  echo "<th>District</th>";
   // echo "<th >Service Provider</th>";
    echo "<th>" . ($batcharray[0]['Batch_Name'] ? $batcharray[0]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[1]['Batch_Name'] ? $batcharray[1]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[2]['Batch_Name'] ? $batcharray[2]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[3]['Batch_Name'] ? $batcharray[3]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[4]['Batch_Name'] ? $batcharray[4]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[5]['Batch_Name'] ? $batcharray[5]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[6]['Batch_Name'] ? $batcharray[6]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[7]['Batch_Name'] ? $batcharray[7]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[8]['Batch_Name'] ? $batcharray[8]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[9]['Batch_Name'] ? $batcharray[9]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[10]['Batch_Name'] ? $batcharray[10]['Batch_Name'] : 'NA') . "</th>";
    echo "<th>" . ($batcharray[11]['Batch_Name'] ? $batcharray[11]['Batch_Name'] : 'NA') . "</th>";

    //echo "<th>April</th>";
//    echo "<th>May</th>";
//    echo "<th>June</th>";
//    echo "<th>July</th>";
//    echo "<th>August</th>";
//    echo "<th>September</th>";
//    echo "<th>October</th>";
//    echo "<th>November</th>";
//    echo "<th>December</th>";
//    echo "<th>Jenuary</th>";
//    echo "<th>Februry</th>";
//    echo "<th>March</th>";
    echo "<th>Total Admission</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    while ($_Row = mysqli_fetch_array($response[2])) {
       // $response1 = $emp->GetCenterdetail($_Row['Admission_ITGK_Code']);
       // if ($response1[0] == "Success") {

            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            //$response1 = $emp->GetCenterdetail($_Row['Admission_ITGK_Code']);
          //  $_Row2 = mysqli_fetch_array($response1[2]);

          //  echo "<td>" . strtoupper($_Row2['ITGK_Name']) . "</td>";
          //  echo "<td>" . strtoupper($_Row2['District_Name']) . "</td>";
          //  echo "<td>" . strtoupper($_Row2['RSP_Name']) . "</td>";
            // if ($_Row['BioMatric_Status'] == '0') {
            echo "<td>" . $_Row['batch0'] . "</td>";
            echo "<td>" . $_Row['batch1'] . "</td>";
            echo "<td>" . $_Row['batch2'] . "</td>";
            echo "<td>" . $_Row['batch3'] . "</td>";
            echo "<td>" . $_Row['batch4'] . "</td>";
            echo "<td>" . $_Row['batch5'] . "</td>";
            echo "<td>" . $_Row['batch6'] . "</td>";
            echo "<td>" . $_Row['batch7'] . "</td>";
            echo "<td>" . $_Row['batch8'] . "</td>";
            echo "<td>" . $_Row['batch9'] . "</td>";
            echo "<td>" . $_Row['batch10'] . "</td>";
            echo "<td>" . $_Row['batch11'] . "</td>";
            $totaladm = ( $_Row['batch0'] + $_Row['batch1'] + $_Row['batch2'] + $_Row['batch3'] + $_Row['batch4'] + $_Row['batch5'] +
                    $_Row['batch6'] + $_Row['batch7'] + $_Row['batch8'] + $_Row['batch9'] + $_Row['batch10'] + $_Row['batch11']);
            echo "<td>" . $totaladm . "</td>";
            echo "</tr>";
            $_Count++;
        //}
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}