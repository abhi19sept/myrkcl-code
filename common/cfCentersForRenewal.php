<?php

/* 
 * Created by yogi
 * Update By Sunil Kuamr Baindara Dated: 9-2-1017

 */

include './commonFunction.php';
require 'BAL/clsCentersForRenewal.php';

$response = array();
$response1 = array();
$emp = new clsCentersForRenewal();

//print_r($_POST['ddlCenter']);



if ($_action == "GETDATA") {

	// $date=date_create($_POST["date"]);
	// $checkdate = date_format($date,"Y/m/d H:i:s");
    $response = $emp->GetAll();
    

	$_DataTable = "";

	echo "<div class='table-responsive' style='margin-top:10px'>";
   
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='25%'>ITGK Details</th>";
	echo "<th style='25%'>SP Details</th>";
	echo "<th style='10%'>District</th>";
	echo "<th style='10%'>Tehsil</th>";
	echo "<th style='10%'>Center Creation Date </th>";
	echo "<th style='10%'>Renewal Date </th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	
    while ($_Row = mysqli_fetch_array($response[2])) {
		$date= date_create($_Row['optional_expire_date']);
		$checkdate = date_format($date,"d-m-Y");
		$date2=date_create($_Row['User_CreatedDate']);
		$checkdate2 = date_format($date2,"d-m-Y");
        echo "<tr class='odd gradeX' id='clist_".$_Row['User_LoginId']."'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td><table><tr><th>Code:</th><td>" . $_Row['User_LoginId'] . "</td></tr><tr><th>Name:</th><td>".strtoupper($_Row['Organization_Name'])."</td></tr><tr><th>Phone:</th><td>".strtoupper($_Row['User_MobileNo'])."</td></tr><tr><th>Email:</th><td>".$_Row['User_EmailId']."</td></tr></table></td>";
        echo "<td><table style='background:transparent;'><tr><th>Code:</th><td>" . $_Row['SP_Code'] . "</td></tr><tr><th>Name:</th><td>".strtoupper($_Row['SP_Name'])."</td></tr><tr><th>Phone:</th><td>".strtoupper($_Row['SP_Phone'])."</td></tr><tr><th>Email:</th><td>".$_Row['SP_Email']."</td></tr></table></td>";
		echo "<td>" . strtoupper($_Row['District_Name'] ). "</td>";
		echo "<td>" . strtoupper($_Row['Tehsil_Name']) . "</td>";
		echo "<td>" . $checkdate2 . "</td>";
		//echo "<td>" . $checkdate . "</td>";
		echo "<td>" . ($_Row['optional_expire_date'] ? date('d-m-Y', strtotime($_Row['optional_expire_date'])) : 'NA') . "</td>"; 
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
	
	
}
if ($_action == "GETADMINDATA") {

	// $date=date_create($_POST["date"]);
	// $checkdate = date_format($date,"Y/m/d H:i:s");
    $response = $emp->GetAll();
    

	$_DataTable = "";

	echo "<div class='table-responsive' style='margin-top:10px'>";
   
    echo "<table id='example_admin' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='6%'>S No.</th>";
    echo "<th style='6%'>ITGK Code</th>";
    echo "<th style='15%'>ITGK Name</th>";
	echo "<th style='6%'>SP Code</th>";
	echo "<th style='15%'>SP Name</th>";
	echo "<th style='6%'>District</th>";
	echo "<th style='6%'>Tehsil</th>";
	echo "<th style='10%'>Phone</th>";
	echo "<th style='10%'>Email</th>";
	echo "<th style='10%'>Center Creation Date </th>";
	echo "<th style='10%'>Renewal Date </th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
		$date= date_create($_Row['optional_expire_date']);
		$checkdate = date_format($date,"d-m-Y");
		$date2=date_create($_Row['User_CreatedDate']);
		$checkdate2 = date_format($date2,"d-m-Y");

        echo "<tr class='odd gradeX' id='clist_".$_Row['User_LoginId']."'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['User_LoginId']  . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . $_Row['SP_Code'] . "</td>";
        echo "<td>" . $_Row['SP_Name'] . "</td>";
        echo "<td>" . strtoupper($_Row['District_Name'] ) . "</td>";
        echo "<td>" . strtoupper($_Row['Tehsil_Name']) . "</td>";
        echo "<td>" . $_Row['User_MobileNo'] . "</td>";
        echo "<td>" . $_Row['User_EmailId'] . "</td>";
        echo "<td>" . $checkdate2 . "</td>";
        //echo "<td>" . $checkdate . "</td>";
		echo "<td>" . ($_Row['optional_expire_date'] ? date('d-m-Y', strtotime($_Row['optional_expire_date'])) : 'NA') . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
	
	
}

