<?php

ini_set("memory_limit", "-1");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
//error_reporting(0);
require 'commonFunction.php';
require 'BAL/clsmyrkcltostagingFileCreate.php';

$emp = new clsmyrkcltostagingFileCreate();

if ($_REQUEST['action'] == 'myrkcltostagingFileCreate') {

    $data = json_decode(file_get_contents('php://input'), TRUE);

    $course= $data['course'];
    $batch= $data['batch'];
    $requesttype = $data['requesttype'];
    if($requesttype == 'photo')
    {
        $response = $emp->gentxtfilephoto($course,$batch);
        if($response[0] == 'Success') 
            {
                $photolist = $_SERVER["DOCUMENT_ROOT"].'/upload/admission_photosign_download/admission_photo/photonames_'.$batch.'.txt';
                $myfile = fopen($photolist, "w") or die("Unable to open file!");
                $_Row = mysqli_fetch_all($response[2]);

                $txt="";
                 foreach ($_Row as $key => $value) {
                    $txt .= $value[0].",";
                 }
                fwrite($myfile, $txt);
                fclose($myfile);
                echo 1;
            }
    }
    else{
        $response = $emp->gentxtfilesign($course,$batch);
        if($response[0] == 'Success') 
            {
                $photolist = $_SERVER["DOCUMENT_ROOT"].'/upload/admission_photosign_download/admission_sign/signnames_'.$batch.'.txt';
                $myfile = fopen($photolist, "w") or die("Unable to open file!");
                $_Row = mysqli_fetch_all($response[2]);

                $txt="";
                 foreach ($_Row as $key => $value) {
                    $txt .= $value[0].",";
                 }
                fwrite($myfile, $txt);
                fclose($myfile);
                echo 1;
            }
    }

}

if ($_REQUEST['action'] == 'UploadFolderFileCreate') {

    $data = json_decode(file_get_contents('php://input'), TRUE);

    $dirname= $data['dirname'];
    $sdate= $data['sdate'];
    $date = date_create($sdate);
    $vardate = date_format($date,"m/d/Y");
   // $vardate = '01/28/2019';
    $realpath = realpath ($_SERVER['DOCUMENT_ROOT']."/");
    $realpath = str_replace ("/", "\\", $realpath);
    $varpath = $realpath.'\upload\\'.$dirname;
    $var1 = 'forfiles /p ' . $varpath . ' /d +'.$vardate;
    $var1 .= ' /C "cmd /Q /C for %I in (@relpath) do echo %~I"';

    $output = shell_exec($var1);
    $abc = strstr($output, '.\\');
    $txt = str_replace('.\\', ",", $abc);

    $photolist = $_SERVER["DOCUMENT_ROOT"].'/upload/myrkcltostagingcopylog/fileslist_'.$dirname.'.txt';
    $myfile = fopen($photolist, "w") or die("Unable to open file!");
    $txt = ltrim($txt,',');
     $string = trim(preg_replace('/\s+/', '', $txt));
    fwrite($myfile, $string);
    fclose($myfile);
    echo 1;

}