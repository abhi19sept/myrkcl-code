<?php

/*
 * Created by Abhishek

 */
ini_set("memory_limit", "-1");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);

include './commonFunction.php';
require 'BAL/clsUploadOnlineClassurl.php';

$response = array();
$emp = new clsUploadOnlineClassurl();

if ($_action == "FILLCourse") {
    $response = $emp->GetAdmissionCourse(); 
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLBatch") {
    $response = $emp->FILLAdmissionBatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_POST['action'] == "ADD") {

    if (isset($_POST["course"]) && !empty($_POST["course"]) && isset($_POST["batch"]) && !empty($_POST["batch"]) && isset($_POST["slottime"]) && !empty($_POST["slottime"]) && isset($_POST["classurl"]) && !empty($_POST["classurl"]))
     {
        $course = $_POST['course'];
        $batch = $_POST['batch'];

        $slottime = $_POST['slottime'];
        $classurl = $_POST['classurl'];
        $date=date_create($slottime);
        $newslottime = date_format($date,"Y-m-d H:i:s");
        
        $response = $emp->Add($course,$batch,$newslottime,$classurl);
        echo $response[0];
            
        
    }
    else {
        echo "Invalid Details";
    }
}


if ($_POST['action'] == "CHKSLOTTIME") {

    if (isset($_POST["course"]) && !empty($_POST["course"]) && isset($_POST["batch"]) && !empty($_POST["batch"]) && isset($_POST["slottime"]) && !empty($_POST["slottime"]))
     {
        $course = $_POST['course'];
        $batch = $_POST['batch'];

        $slottime = $_POST['slottime'];
        
        $response = $emp->Chkslottime($course,$batch,$slottime);
        $co = mysqli_num_rows($response[2]);
        if ($co) {
            echo "1";
        }
        else{
            echo "0";
        } 
        
    }
    else {
        echo "Invalid Details";
    }
}


if ($_action == "GETDATAITGK") {
    $response = $emp->GetListITGK($_POST['course'], $_POST['batch']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Course</th>";
    echo "<th>Batch</th>";
    echo "<th>Slot Date Time</th>";
    echo "<th >Class URL</th>";
    
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            echo "<td>" . $_Row['Slote_Time'] . "</td>";
            echo "<td>" . $_Row['Class_Url'] . "</td>";

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "FILLMONTH") {
    $dataArray = [];
    for ($i = 0; $i <= 11; $i++) {
        $dataArray[$i]["Month_Name"] = date("F Y", strtotime( date( 'Y-m-01' )." -$i months"));
        $dataArray[$i]["Month"] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
    }
    echo json_encode($dataArray);
}
   
if ($_action == "EmployeeDetailForOneViewAttendence") {
    $mmyy = $_POST["month"];
    $course = $_POST["course"];
    $batch = $_POST["batch"];
    $dataArray = [];

    $response = $emp->getAllEmployeeForOneViewAttence($course,$batch,$mmyy);
    $daya = explode("-",$mmyy);
    $totalday = cal_days_in_month(CAL_GREGORIAN,$daya[1],$daya[0]);
    $day = "1";

    echo "<div class='table-responsive'>
            <table id='example' class='table table-bordered'>
              <thead>
                    <tr>
                      <th>S. No.</th>
                      <th>IT-GK Code</th>";
                      while($day <= $totalday) {
                        echo "<th>".$day."</th>";
                        $day++;
                      } 
                   echo "</tr>
              </thead>
              <tbody>";
    $_Count = 1;
    
    if ($response[0] == 'Success') {
        // for ($i = 0; $i < count($_Row[2]); $i++) {
        $i = 0;
            while ($_Row = mysqli_fetch_array($response[2])) {
                // print_r($_Row);
            $dayy = "1";

            echo "<tr style='font-size: 12px;'>
                      <td>" . $_Count . ".</td>
                      <td>" . $_Row['ITGKCode'] . "</td>";
                while($dayy <= $totalday) {
                     $date = $mmyy."-".$dayy;
                     $dayName = date("l", strtotime($date)); 
                     $_Attendance = $emp->AttendanceView($date,$_Row['ITGKCode'],$course,$batch);
                     if ($_Attendance[0] == 'Success') {
                     $autoid=""; 
                        while($_Row1 = mysqli_fetch_array($_Attendance[2])){
                            $autoid .= $_Row1['ClassUrlId'].',';

                        }
                        $autoid = rtrim($autoid,',');
                            // echo "<td style='color:blue;'>view</td>";
                            echo "<td><input type='button' class='btn-primary btn-md totalupdcount btn-block' autoid='".$autoid."' value='View'/></td>";
                       
                    }else{
                            echo "<td style='color:white;'></td>"; 
                            }
                  $dayy++;
                } 
                 echo "</tr>";
            $_Count++;
            $i++;
        }
    } else {
        echo "<tr><td colspan='6' style='text-align: center'> - No data -</td></tr>";
    }
    echo "</tbody></table></div></div>";

}
if ($_action == "GETURL") {

    $response = $emp->GetURL($_POST['autoid']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Slot Date </th>";
    echo "<th>URL</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['ITGKCode'] . "</td>";
            echo "<td>'" . $_Row['Slote_Time'] . "</td>";
            echo "<td>" . $_Row['Class_Url'] . "</td>";

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

