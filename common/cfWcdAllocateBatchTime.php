<?php

/*
 * Created by Mayank
cfWcdAllocateBatchTime
 */

include './commonFunction.php';
require 'BAL/clsWcdAllocateBatchTime.php';

$response = array();
$emp = new clsWcdAllocateBatchTime();

if ($_action == "GETDATA") {    
	if($_POST['course'] !=''){
		if($_POST['batch'] !='') {		  
		   $response = $emp->GetAllLearner($_POST['course'],$_POST['batch']);
			  
			   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
				echo "<th>Learner Code</th>";
				echo "<th>Learner Name</th>";
				echo "<th>Father Name</th>";
				//echo "<th>D.O.B</th>";
				echo "<th>Mobile No.</th>";				
				echo "<th>Allocate Batch Start Time</th>";
					if($_POST['course']=='24'){
				echo "<th>Email ID</th>";		
					}
				echo "<th>Action</th>";				
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;
								
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr class='odd gradeX'>";
					echo "<td>" . $_Count . "</td>";						
					echo "<td>" . $_Row['Oasis_Admission_LearnerCode'] . "</td>";
					echo "<td>" . $_Row['Oasis_Admission_Name'] . "</td>";
					echo "<td>" . $_Row['Oasis_Admission_Fname'] . "</td>";
					//echo "<td>" . $_Row['Oasis_Admission_DOB'] . "</td>";
					echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
						if($_Row['Batch_Time_lcode']==''){
							echo "<td> <select id='ddltime$_Count' name='ddltime' class='form-control'>
									<option value=''>Select Time</option>
									<option value='10 AM'>10 AM</option>
									<option value='11 AM'>11 AM</option>
									<option value='12 PM'>12 PM</option>
									<option value='01 PM'>01 PM</option>
									<option value='02 PM'>02 PM</option>
									<option value='03 PM'>03 PM</option>
									<option value='04 PM'>04 PM</option>
								</select>
							</td>";
							if($_POST['course']=='24'){
								echo "<td><input type='text' class='updemail$_Count' name='".$_Count."'
									id='" . $_Row['Oasis_Admission_LearnerCode'] . "' value=''/></td>";		
							}
							echo "<td><input type='button' class='updtime' name='".$_Count."'
										id='" . $_Row['Oasis_Admission_LearnerCode'] . "' value='Update'/></td>";
						}
						else{
							echo "<td>" . $_Row['Batch_Time_time'] . "</td>";
								if($_POST['course']=='24'){
									echo "<td>" . $_Row['Batch_Time_email'] . "</td>";
								}
							echo "<td><input type='button' class='updedtime' name='updedtime' value='Updated'/></td>";
						}
					echo "</tr>";
						
					$_Count++;				
				}
				echo "</tbody>";
				
				 echo "</table>";
				 echo "</div>";			
		}
			else {
			  echo "1";
			}
	}
	  else {
		  echo "2";
	  }   
}


if ($_action == "FillAdmissionCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "Updaterscitbatchtime") {
    if (isset($_POST['lcode']) && !empty($_POST['lcode'])) {      
        $_appid= $_POST["lcode"];
        $_allottime= $_POST["allottime"];        
        $_batch= $_POST["batch"];        
        $response = $emp->updaterscitbatchtime($_appid,$_allottime,$_batch);
        echo $response[0];
    }
	else{
		echo "0";
	}
}

if ($_action == "Updaterscfabatchtime") {
    if (isset($_POST['lcode']) && !empty($_POST['lcode'])) {      
        $_appid= $_POST["lcode"];
        $_allottime= $_POST["allottime"];        
        $_batch= $_POST["batch"];        
        $_email= $_POST["email"];        
        $response = $emp->Updaterscfabatchtime($_appid,$_allottime,$_batch,$_email);
        //echo $response[0];
		if($response[0]=='Successfully Inserted')
			{
				echo $response[0];
			}
		else if($response[0]=='No Record Found')
		{
			echo "00";
		}
		else
		{
			echo "1";
		}
    }
	else{
		echo "0";
	}
}

if ($_action == "Veifyemailotp") {
    if (isset($_POST["otp"])) {       
        $_Otp = $_POST["otp"];
        $_Email = $_POST["email"];
        $_Batch = $_POST["batch"];
        $_Lcode = $_POST["lcode"];
        $_AllotTime = $_POST["allottime"];
        $response = $emp->Veifyemailotp($_Otp,$_Email,$_Batch,$_Lcode,$_AllotTime);
        echo $response[0];
    }
}