<?php

/* 
 * author Mayank

 */
include './commonFunction.php';
require 'BAL/clsCheckCorrectionTxnStatus.php';

$response = array();
$emp = new clsCheckCorrectionTxnStatus();

if ($_action == "ValidateCorrectionTxnId") {
		$response = $emp->ValidateTxnId($_POST['values']);
		if ($response[0]=='Success') {	
		
		$_DataTable = "";
		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>S No.</th>";
		echo "<th>CenterCode</th>";
		echo "<th>Amount</th>";
    	echo "<th>Transaction Id</th>";	
		echo "<th>Payment Type</th>";
		echo "<th>Date-Time</th>";
		
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;
    $co = mysqli_num_rows($response[2]);
    //if ($response[0]=='Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['itgk'] . "</td>";
			echo "<td>" . $_Row['amount'] . "</td>";
            echo "<td>" . $_POST['values'] . "</td>";
			echo "<td>" . $_Row['prodinfo'] . "</td>";
            echo "<td>" . $_Row['dtime'] . "</td>";
            echo "</tr>";
            //$_Total = $_Total + $_Row['admissioncount'];
            $_Count++;
        }   
			echo "</tbody>";        
	echo "</table>";
	echo "</div>";
    } 
		else{
				echo "0";
		}
	
}


if ($_action == "ChkCorrectionTxnStatus") {
		//print_r($_POST);
		if($_POST['merchantTransactionIds']==""){
			echo "0";
		}
		else {
		$merchantTransactionIds = $_POST['merchantTransactionIds'];		
		$PayuMoney_BASE_URL = "https://www.payumoney.com/payment/payment/chkMerchantTxnStatus?";
		$action = '';
		$posted = array();
		$MERCHANT_KEY = '7yoZu9Bn';//PUT YOUR MERCHANT KEY HERE

		$msg = "";
		$flag = 0;
  
		foreach($_POST as $key => $value) {     
			$posted[$key] = htmlentities($value, ENT_QUOTES);
		}
			$postData = array();
  $postData['merchantKey']= $MERCHANT_KEY;
  unset($posted['merchantKey']);
  $postData['merchantTransactionIds']=$posted['merchantTransactionIds'];
  unset($posted['merchantTransactionIds']);
  $postNow = http_build_query($postData);

  $response = curlCall($PayuMoney_BASE_URL.$postNow,TRUE); 
  
  //echo "<pre>"; print_r($response); echo "</pre>";  
   //echo "<pre>"; print_r($response['message']); echo "</pre>";
	
	if($response['result']) {
	foreach($response['result'] as $val) {      
			if($val['status'] == "Money Settled" || $val['status'] == "Settlement in process") {				
				$txnid = $val['merchantTransactionId'];	
				$amount= $val['amount'];
				    $response = $emp->CorrectionMoneySettled($txnid,$amount);
					echo $response[0];
				}				
			else {
						$txnid = $val['merchantTransactionId'];
						$response = $val['status'];
						echo $response;
			}
		}
	}
}	
$formError = 0;
}

function curlCall($postUrl, $toSend) {   
  $ch = curl_init();
 curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $toSend);
  curl_setopt($ch, CURLOPT_URL, $postUrl);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $header = array(
     'Authorization: Iirn6nqrD0zG6EahWNQGooLY1cGQm+IM+UEkHVlpB00='
  );
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
   
  $out = curl_exec($ch);
  //if got error
  if (curl_errno($ch)) {
    $c_error = curl_error($ch);
    if (empty($c_error)) {
      $c_error = 'Some server error';
    }
    return array('curl_status' => 'FAILURE', 'error' => $c_error);
  }
  $out = trim($out);
  $arr = json_decode($out,true);
	return $arr;
  //return array('curl_status' => 'SUCCESS', 'result' => $out);
}