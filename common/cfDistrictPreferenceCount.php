<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsDistrictPreferenceCount.php';

$response = array();
$emp = new clsDistrictPreferenceCount();

if ($_action == "GETDATA") {
    //echo "Show";
		
		if($_POST['batch'] !='') {
		 
			  $response = $emp->GetAllPref($_POST['course'],$_POST['batch']);
			  
			   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
				echo "<th>District Name</th>";
				echo "<th>Center Preference Count</th>";
				
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;				
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr class='odd gradeX'>";
					echo "<td>" . $_Count . "</td>";		
					echo "<td>" . $_Row['District_Name'] . "</td>";
					echo "<td><a href='frmLearnerDistrictPreferenceList.php?batch=" . $_POST['batch'] . "&districtcode=" . $_Row['District_Code'] . "&mode=one' target='_blank'>"
							. "" . $_Row['pref'] . "</a></td>";
					//echo "<td>" . $_Row['pref'] . "</td>";
						echo "</tr>";
					$_Count++;				
				}
				 echo "</tbody>";			
				 echo "</table>";
				 echo "</div>";
		 
		}
		  else {
			  echo "1";
		  }
   
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}


if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetLearnerList($_POST['mode'], $_POST['batch'], $_POST['rolecode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";   
    echo "<th>Applicant Id</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
	 echo "<th>Center Preference-1</th>";
	echo "<th>Center Preference-2</th>";
	echo "<th >D.O.B.</th>";
	echo "<th >Mobile No.</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";			
            echo "<td>" . $_Row['Oasis_Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_Fname'] . "</td>";
			echo "<td>" . $_Row['Oasis_Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_ITGK_Code2'] . "</td>";
			echo "<td>" . $_Row['Oasis_Admission_DOB'] . "</td>";
			echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
           if ($_Row['Oasis_Admission_Photo'] != "") {
                $image = $_Row['Oasis_Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="40" src="upload/oasis_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="40" src="images/user.png"/>' . "</td>";
            }
            if ($_Row['Oasis_Admission_Sign'] != "") {
                $sign = $_Row['Oasis_Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/oasis_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/sign.jpg"/>' . "</td>";
            } 

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
