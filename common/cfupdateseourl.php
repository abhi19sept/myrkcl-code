<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsupdateseourl.php';

$response = array();
$emp = new clsupdateseourl();


if ($_action == "UPDATE") 
{
    if (isset($_POST["SEO"])) 
	{
            $_Code = $_POST["code"];
            $_SeoURL = $_POST["SEO"];
            $_FunURL = $_POST["FUNURL"];
            $response = $emp->Update($_Code,$_SeoURL);
            if($response[0] == 'Successfully Updated'){
                $xmlFile = "../web.config";
                $xmlString = file_get_contents($xmlFile);
                $dom = new DomDocument;
                $dom->preserveWhiteSpace = FALSE;
                $dom->loadXML($xmlString);
                $rules = $dom->getElementsByTagName('rules');
                $gallery = $dom->getElementsByTagName('rule')->item(0);
                foreach ($rules as $rul) {
                    $rule1 = $dom->createElement('rule');
                    $rul->insertBefore($rule1,$gallery);

                    $text0 = $dom->createAttribute('name');
                    $text0->value = 'Imported Rule ' . uniqid();
                    $rule1->appendChild($text0);

                    $text0 = $dom->createAttribute('stopProcessing');
                    $text0->value = 'true';
                    $rule1->appendChild($text0);

                    $match1 = $dom->createElement('match');
                    $rule1->appendChild($match1);

                    $text0 = $dom->createAttribute('url');
                    $text0->value = '^'.$_SeoURL.'?$';
                    $match1->appendChild($text0);

                    $text0 = $dom->createAttribute('ignoreCase');
                    $text0->value = 'false';
                    $match1->appendChild($text0);

                    $conditions = $dom->createElement('conditions');
                    $rule1->appendChild($conditions);

                    $add1 = $dom->createElement('add');
                    $conditions->appendChild($add1);

                    $add2 = $dom->createElement('add');
                    $conditions->appendChild($add2);

                    $text0 = $dom->createAttribute('input');
                    $text0->value = '{REQUEST_FILENAME}';
                    $add1->appendChild($text0);

                    $text0 = $dom->createAttribute('matchType');
                    $text0->value = 'IsFile';
                    $add1->appendChild($text0);

                    $text0 = $dom->createAttribute('ignoreCase');
                    $text0->value = 'false';
                    $add1->appendChild($text0);

                    $text0 = $dom->createAttribute('negate');
                    $text0->value = 'true';
                    $add1->appendChild($text0);

                    $text0 = $dom->createAttribute('input');
                    $text0->value = '{REQUEST_FILENAME}';
                    $add2->appendChild($text0);

                    $text0 = $dom->createAttribute('matchType');
                    $text0->value = 'IsDirectory';
                    $add2->appendChild($text0);

                    $text0 = $dom->createAttribute('ignoreCase');
                    $text0->value = 'false';
                    $add2->appendChild($text0);

                    $text0 = $dom->createAttribute('negate');
                    $text0->value = 'true';
                    $add2->appendChild($text0);

                    $action1 = $dom->createElement('action');
                    $rule1->appendChild($action1);

                    $text0 = $dom->createAttribute('type');
                    $text0->value = 'Rewrite';
                    $action1->appendChild($text0);

                    $text0 = $dom->createAttribute('url');
                    $text0->value = $_FunURL;
                    $action1->appendChild($text0);


                    $rule = $dom->createElement('rule');
                    $rul->insertBefore($rule,$gallery);

                    $text = $dom->createAttribute('name');
                    $text->value = 'Imported Rule ' . uniqid();
                    $rule->appendChild($text);

                    $text = $dom->createAttribute('stopProcessing');
                    $text->value = 'true';
                    $rule->appendChild($text);

                    $match = $dom->createElement('match');
                    $rule->appendChild($match);

                    $text2 = $dom->createAttribute('url');
                    $text2->value = $_FunURL;
                    $match->appendChild($text2);

                    $text2 = $dom->createAttribute('ignoreCase');
                    $text2->value = 'false';
                    $match->appendChild($text2);

                    $action = $dom->createElement('action');
                    $rule->appendChild($action);

                    $text3 = $dom->createAttribute('type');
                    $text3->value = 'Redirect';
                    $action->appendChild($text3);

                    $text3 = $dom->createAttribute('redirectType');
                    $text3->value = 'Permanent';
                    $action->appendChild($text3);

                    $text3 = $dom->createAttribute('url');
                    $text3->value = $_SeoURL;
                    $action->appendChild($text3);
                }
                $dom->formatOutput = true;
                $dom->save("../web.config");

                echo $response[0];
            }
    }
}



if ($_action == "SHOW") 
{

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";
	echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>Name</th>";
    echo "<th style='20%'>URL</th>";
	echo "<th style='20%'>SEO URL</th>";
    echo "<th style='20%'>Parent</th>";
    echo "<th style='20%'>Root</th>";
	echo "<th style='5%'>Display</th>";
    echo "<th style='5%'>Status</th>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Function_Name'] . "</td>";
        echo "<td>" . $_Row['Function_URL'] . "</td>";
		echo "<td>" . $_Row['Function_SEO'] . "</td>";
        echo "<td>" . $_Row['Parent_Function_Name'] . "</td>";
        echo "<td>" . $_Row['Root_Menu_Name'] . "</td>";
        echo "<td>" . $_Row['Function_Display'] . "</td>";
        echo "<td>" . $_Row['Status_Name'] . "</td>";
        
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}
if ($_action == "FILLCHILD") 
{
    $response = $emp->GetChild($_POST["values"]);
    echo "<option value='0' selected='selected'>Select Status</option>";
    while ($_Row = mysqli_fetch_array($response[2])) 
	{
        echo "<option value=" . $_Row['FunctionCode'] . ">" . $_Row['FunctionName'] . "</option>";
    }
}




if ($_action == "FILLFUNCTIONURL") {
    $response = $emp->Getfunctionurl($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) 
	{
        while ($_Row = mysqli_fetch_array($response[2])) 
		{

            $_DataTable[$_i] = array("FunctionSEO" => $_Row['FunctionSEO'],
                "FunctionURL" => $_Row['FunctionURL']);
            $_i = $_i + 1;
        }
		echo json_encode($_DataTable);
    }
	else 
	{
        echo "You are not Auhorized.";
    }
}
?>
