<?php

/* 
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsCourseMaster.php';

$response = array();
$emp = new clsCourseMaster();

if ($_action == "ADD") {
        if (isset($_POST["txtName"] ) && !empty($_POST["txtName"]))
			 {
				$_txtName = $_POST["txtName"];
				$_ddlCourseType = $_POST["ddlCourseType"];
				$_txtDuration = $_POST["txtDuration"];
				$_Duration = $_POST["Duration"];
				$_ddlMedium = $_POST["ddlMedium"];
				//$_ddlFeeType = $_POST["ddlFeeType"];
				
				
				$_ddlExamination = $_POST["ddlExamination"];
				$_ddlClasstype= $_POST["ddlClasstype"];
				$_ddlAffiliate = $_POST["ddlAffiliate"];
				$_ddlStatus = $_POST["ddlStatus"];
				
			
            $response = $emp->Add($_txtName,$_ddlCourseType,$_txtDuration,$_Duration,$_ddlMedium ,$_ddlExamination,$_ddlClasstype,$_ddlAffiliate,$_ddlStatus );
            echo $response[0];
        }
    }
    
	if ($_action == "UPDATE")
	{
			if (isset($_POST["ddlMedium"]))
			 {
				$_txtName = $_POST["txtName"];
				$_ddlCourseType = $_POST["ddlCourseType"];
				$_txtDuration = $_POST["txtDuration"];
				$_Duration = $_POST["Duration"];
				$_ddlMedium = $_POST["ddlMedium"];
				$_ddlExamination = $_POST["ddlExamination"];
				$_ddlClasstype= $_POST["ddlClasstype"];
				$_ddlAffiliate = $_POST["ddlAffiliate"];
				$_ddlStatus = $_POST["ddlStatus"];
            $_Status_Code=$_POST['code'];
            $response = $emp->Update($_Status_Code,$_txtName,$_ddlCourseType,$_txtDuration,$_Duration,$_ddlMedium ,$_ddlExamination,$_ddlClasstype,$_ddlAffiliate,$_ddlStatus );
            echo $response[0];
        }
    
    }
    
    if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("Course_Name" => $_Row['Course_Name'],
		"Course_Type" => $_Row['Course_Type'],
		"Course_Duration" => $_Row['Course_Duration'],
		"Course_DurationType" => $_Row['Course_DurationType'],
		"Course_Medium" => $_Row['Course_Medium'],		
		"Course_ExaminationType" => $_Row['Course_ExaminationType'],
		"Course_ClassType" => $_Row['Course_ClassType'],
		"Course_Affiliate" => $_Row['Course_Affiliate'],
		"Course_Status" => $_Row['Course_Status'] );
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}
    
    if ($_action == "DELETE") {


        $response = $emp->DeleteRecord($_actionvalue);

        echo $response[0];
    }


    if ($_action == "SHOW") {


        $response = $emp->GetAll();

        $_DataTable = "";

        echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='5%'>S No.</th>";
        echo "<th style='30%'>Name</th>";
       
        echo "<th style='10%'>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
           
            echo "<td><a href='frmcoursemaster.php?code=" . $_Row['Course_Code'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a>  <a href='frmdisplaycourses.php?code=" . $_Row['Course_Code'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
//echo $_DataTable;
    }

if ($_action == "FILL") {
    $response = $emp->GetAllFill();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Courseitgk_Course'] . "</option>";
    }
}

if ($_action == "FILLCOURSEBLOCKCENTER") {
    $response = $emp->GetAllFillBlockCenter();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}
if ($_action == "FILLAdmissionSummaryCourse") {
    $response = $emp->GetAdmissionCourse();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Courseitgk_Course'] . "</option>";
    }
}

if ($_action == "FILLAdmissionCourseDD") {
    $response = $emp->GetAdmissionCourseDD();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Courseitgk_Course'] . "</option>";
    }
}

if ($_action == "FILLCourseName") {
    $response = $emp->GetAllCourse();	
    echo "<option value='0'>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}


if ($_action == "FILLCourseCode") {
    $response = $emp->GetCourseCode($_POST['values']);	
    //echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . " selected='selected'>" . $_Row['Course_Code'] . "</option>";
    }
}


if ($_action == "MODIFYFILL") {
    $response = $emp->GetAllMODIFY();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Admission_Course'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}


if ($_action == "FillAdmissionCourse") {
    $response = $emp->GetAdmissionCourseName($_POST['values']);	    
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo $_Row['Course_Name'];
    }
}