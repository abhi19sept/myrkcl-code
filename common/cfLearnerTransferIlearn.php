<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsLearnerTransferIlearn.php';
require 'cflog.php';

    $response = array();
    $emp = new clsLearnerTransferIlearn();
    //  $BASE_ILEARN = "http://localhost/toc/";
        $BASE_ILEARN = "http://49.50.65.36/";
        $ILEEARN_FILE_PATH = "uploads/jsonfile/"; // path for ilearn server
        $BASE_SERVER_FILE = "../upload/jsonfile/"; // path for same/myrkcl server

    if ($_action == "DETAILS") {
        
    $response = $emp->GetITGK_NCR_Details($_POST['values'], $_POST['batch']);
       
    $_DataTable = "";
        
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
        echo "<th style='5%'>IT-GK Code</th>";
        echo "<th style='5%'>Ack Code</th>";
        echo "<th style='5%'>Learner Name</th>";
        echo "<th style='5%'>Father Name</th>";
          echo "<th style='5%'>Learner Mobile</th>";
          echo "<th style='5%'>Admission Date</th>";
        echo "<th style='5%'>Payment Status</th>";
         echo "<th style='5%'>Action</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    if($response[0] == 'Success') {
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {

    echo "<tr class='odd gradeX'>";
    echo "<td>" . $_Count . "</td>";
    echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
        echo "<td>" . ($_Row['Admission_LearnerCode']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
    echo "<td>" . ($_Row['Admission_Mobile']) . "</td>";
        $date=date_create($_Row['Timestamp']);
        echo "<td>" . date_format($date,"d-M-Y") . "</td>";
      if($_Row['Admission_Payment_Status'] == '1'){
          echo "<td>Payment Confirmed</td>";
        }
        else{
            echo "<td>Payment Not Confirmed</td>";
        }
        if($_Row['IsNewRecord'] == 'Y'){
        	if($_Row['Admission_Payment_Status'] == '1'){
            	echo "<td><input type='button' class='updcount btn btn-primary btn-sm' id='".$_Row['Admission_LearnerCode']."' AckCode='".$_Row['Admission_LearnerCode']."' CourseCode='" . $_Row['Admission_Course'] . "' BatchCode='" . $_Row['Admission_Batch'] . "' mode='ShowUpload' value='Activate Learning'/></td>";
     		}
     		else{
 				echo "<td>Payment Not Confirmed</td>";
     		}
        }
        else{
            echo "<td>Transfered to Ilearn</td>";
        }

        echo "</tr>";
    $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
    }
    }
     
if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetDatabyCode($_POST["itgkcode"], $_POST["learnercode"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Learner Code</th>";
    echo "<th style='20%'>Learner Name</th>";
    echo "<th style='10%'>Father Name</th>";
    echo "<th style='10%'>DOB</th>";
   
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
     echo "</div>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
         echo "<td>" . $_Row['Admission_Name'] . "</td>";
         echo "<td>" . $_Row['Admission_Fname'] . "</td>";
         echo "<td>" . $_Row['Admission_DOB'] . "</td>";
        
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILLAdmissionSummaryCourse") {
    $response = $emp->GetAllCourse(); 
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}
if ($_action == "FILLAdmissionBatchcode") {
    $response = $emp->FILLAdmissionBatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
   } 

if ($_action == "TransferLearner") {
        $lcode  = $_POST['lcode'];
        $batche = $_POST['bcode'];
        $course = $_POST['ccode'];
        $result = $emp->myrkcltoilearnFileCreate($lcode, $batche, $course);
        if ($result['status'] == '200') {
        m_log("sen request to i learn".date('Y-m-d h:i:s'));
            $insert = $emp->InsertiLearnTbl($result['result']);
          // print_r($insert); die;
            if ($insert['status'] == '200') {
                if($insert['success'] == "success"){
                   
                    $emp->TblStatusUpdtoN($lcode, $batche, $course);
                     m_log("change status to N".date('Y-m-d h:i:s'));
                 echo json_encode($insert);    
                } else {
                    m_log("curl request success not data not insert".date('Y-m-d h:i:s'));
                   // echo $insert['error'] ;
                    echo json_encode($insert); 
                }
                
            } else {

                echo json_encode($insert);
            }
        } else {
            echo json_encode($result);
        }
    }   

?>