<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsLinkAadhar.php';
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
$response = array();
$emp = new clsLinkAadhar();


// Staging Testing URL For Ecertificate
//$addServiceURL = "https://apitest.sewadwaar.rajasthan.gov.in/app/live/RajeVault/UAT/RBSE/addDocument?client_id=9cb343e2-791b-4eb6-8a36-f4a146e28db4";
//$updateServiceURL = "https://apitest.sewadwaar.rajasthan.gov.in/app/live/RajeVault/uat/RBSE/UpdateDocument?client_id=9cb343e2-791b-4eb6-8a36-f4a146e28db4";
//$getServiceURL = "https://apitest.sewadwaar.rajasthan.gov.in/app/live/RajeVault/Uat/RBSE/GetDocument?client_id=9cb343e2-791b-4eb6-8a36-f4a146e28db4";


// Production URL For Ecertificate
$addServiceURL = "https://api.sewadwaar.rajasthan.gov.in/app/live/RajeVault/Prod/RSCIT/Add/document?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69";
$updateServiceURL = "https://api.sewadwaar.rajasthan.gov.in/app/live/RajeVault/Prod/RSCIT/docupdate?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69";
$getServiceURL = "https://api.sewadwaar.rajasthan.gov.in/app/live/RajeVault/Prod/RSCIT/Get/document?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69";


// Signature Authority Aadhar Number
//$signedAdhaar = "434098406796"; //Test
//$signedAdhaar = "753593019725"; //VMOU Registar Aadhar Number till 1 Nov 2019
//$signedNameReg = "Mahendra Meena"; //VMOU Registrar Name

$signedAdhaar = "917608274451"; //VMOU Registar Aadhar Number After 2 Nov 2019 Registrar Name -  Aadhar No. - 
$signedNameReg = "Shambhu Dayal"; //VMOU Registrar Name



// File Store path for get service in MYRKCL
$filestorepath = "../upload/singedEcertificate/";
$filedownloadlinkpath = "../upload/singedEcertificate/";

//Generate OTP for Aadhar Verification
if ($_action == "generateOTP") 
        {         
         $aadhaar_no=$_POST['txtACno'];        
         $generateOTP = generateOTP($aadhaar_no);         
         echo json_encode($generateOTP);        
        }        

//Authenticate OTP for Aadhar Verification		
if ($_action == "authenticateOTP") 
        {        
         $aadhaar_no=$_POST['txtACno'];
         $txnID=$_POST['txtTxn'];
         $otp=$_POST['txtotpTxt'];
         $txtCourse=$_POST['txtCourse'];
         $txtBatch=$_POST['txtBatch'];
         $txtLcode=$_POST['txtLcode'];
         $authenticateOTP = authenticateOTPbyEXC($aadhaar_no, $txnID, $otp);
          
         if($authenticateOTP['status']=='Y'){
             $response = $emp->GetVerificationAadhar($txtLcode,$txtBatch,$txtCourse);
             if($response[0]=='Success'){
                $_Row = mysqli_fetch_array($response[2]);
                $name = trim($_Row["Admission_Name"]);
                $fname = trim($_Row["Admission_Fname"]);
                $dob = trim($_Row["Admission_DOB"]);
                $namew = trim($authenticateOTP['LName']);
                $fnamew = trim($authenticateOTP['Fname']);
                $dobw = trim($authenticateOTP['LDOB']);
                $newDOB = date("Y-m-d", strtotime($dobw));
                $Aadharno = $authenticateOTP['Aadharno'];
                $Gender = $authenticateOTP['Gender'];
                $District = $authenticateOTP['District'];
                $Tehsil = $authenticateOTP['Tehsil'];
                $Pincode = $authenticateOTP['Pincode'];
                if(strcasecmp($name, $namew)==0){
					if(strcasecmp($dob, $newDOB)==0){
						$response = $emp->InsertAadharData($txtLcode,$txtBatch,$txtCourse,$namew,$fnamew,$newDOB,$aadhaar_no,$Gender,
						$District,$Tehsil,$Pincode);
						echo $response[0];
                        }else{
                            echo "DOB Not Match";
                        }
                }else{
                    echo "Name Not Match";
                }
             }
         }
         else{          
             echo $authenticateOTP['message'];            
         }      
        }
  
  /***
     * @function generateOTP : function to generate Adhaar OTP
     * @param int $aadhaar_no : The adhaar number of User
     * @param int $pinCode : Area pin code
     * @return array : Returns an array containing the status and transaction id
     ***/

    function generateOTP($aadhaar_no){
        /*** DO NOT MODIFY THE VALUES BELOW ***/
        //$licence_key = "MEmVkcpNLahCE-9skCRMK36S_ufQGPaCiNFAZ33o_ICd01JIE6IBLpU";
        //$licence_key = "MPjsZ77ep-Mu0gTYs5_1mOfkfozxJ-3Q2AGHyhhjkTOeg7JSFAolRqI";
		// $licence_key = "MMa5z-ryHS_tvB5t9Hmxyjbte7icRdddVAbDRL_WN1DXyUBLz8bB2-M";
		 //$licence_key ="MJqg1YjQL8GkseVac8FM2g1BUzLmiKN_I6gjETFFD27i6RGUYysHeKo"; //updaated on 30 August 2018//
		 $licence_key ="MAlCZADE4gHodMG6QJ5GRY7-NlR-7A18QUrh2zo-WspGIibbrm0YgOc"; //updaated on 27 August 2019//
		
        $tid = "public";
        //$subaua = "STGDOIT006";
        $subaua = "STGDOIT238";
		// $subaua = "PRKCL22857"; // 27Aug2019 2.5
        $udc = "RKCL123";
        $ip = "127.0.0.1";
        $fdc = "NA";
        $idc = "NA";
        $macadd = "";
        $lot = "P";
        $rc = "Y";
        $pinCode='NA';

        /*** GENERATE AUTH XML BLOCK HERE (DO NOT MODIFY) ***/
        $auth_xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<authrequest uid="' . $aadhaar_no . '" tid="' . $tid . '" subaua="' . $subaua . '" ip="' . $ip . '" fdc="' . $fdc . '" idc="' . $idc . '" udc="' . $udc . '" macadd="' . $macadd . '" lot="' . $lot . '" lov="'.$pinCode.'" lk="' . $licence_key . '" rc="' . $rc . '">
    <otp channel="00"/>
</authrequest>';

        /*** INITIATE CURL REQUEST (DO NOT MODIFY) ***/
        //$ch = curl_init("http://103.203.138.120/api/aua/otp/request/encr");
        $ch = curl_init("https://api.sewadwaar.rajasthan.gov.in/app/live/api/aua/otp/request?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $auth_xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Accept: application/xml",
            "Content-Type: application/xml",
            "appname: MYRKCL"
        ]);

        /*** EXECUTE CURL HERE ***/

        $result = curl_exec($ch);
        if ( ! $result) {
                print curl_errno($ch) .': '. curl_error($ch);
            }
        /*** CLOSING CURL HERE ***/

        curl_close($ch);

        /*** STORE CURL RESPONSE INTO XML ***/

        $xml = @simplexml_load_string($result);

        /*** CONVERT XML INTO JSON FORMAT ***/

        $json = json_encode($xml);

        /*** CONVERT JSON INTO ARRAY FORMAT ***/

        $array = json_decode($json,true);

        /*** RETURNING RESPONSE AND TRANSACTION ***/

        $responseArray = [
            "txn" => $array["auth"]["@attributes"]["txn"],
            "status" => $array["auth"]["@attributes"]["status"]
        ];
        return $responseArray;
    }    
    
  /***
     * @function generateOTP : function to generate Adhaar OTP
     * @param int $aadhaar_no : The adhaar number of User
     * @param int $pinCode : Area pin code
     * @return array : Returns an array containing the status and transaction id
     ***/

    function authenticateOTP($aadhaar_no, $txnID, $otp){
        /*** DO NOT MODIFY THE VALUES BELOW ***/
        /*** INITIATE CURL REQUEST (DO NOT MODIFY) ***/
        $URL="http://server2/UIDAI_RDAPI/uidai/otp/auth/".$aadhaar_no."/".$otp."/".$txnID;
        //die;
        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL,$URL);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //  curl_setopt($ch,CURLOPT_HEADER, false); 
        $output=curl_exec($ch);

        curl_close($ch);       
        $array = json_decode($output,true);     
        if($array["authresponse"]["auth"]["@status"]=='Y')
        {
            $responseArray = [
            "status" => $array["authresponse"]["auth"]["@status"],
            "Aadharno" => $array["authresponse"]["UidData"]["@UUID"],
            "LName" => $array["authresponse"]["UidData"]["pi"]["@name"],
            "Fname" => $array["authresponse"]["UidData"]["pa"]["@co"],
            "Gender" => $array["authresponse"]["UidData"]["pi"]["@gender"],
            "LDOB" => $array["authresponse"]["UidData"]["pi"]["@dob"],
            "District" => $array["authresponse"]["UidData"]["pa"]["@dist"],
            "Tehsil" => $array["authresponse"]["UidData"]["pa"]["@loc"],
            "Pincode" => $array["authresponse"]["UidData"]["pa"]["@pc"]
            ];
        }
        else{
            $responseArray = [
               "status" => $array["authresponse"]["auth"]["@status"],
               "message" => $array["authresponse"]["message"]
            ];   
        }        
        return $responseArray; 
    }
    
if ($_action == "FILLLinkAadharCourse") {
    $response = $emp->GetAllLinkAadharCourse();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLAdmissionBatch") {
    $response = $emp->GetAllAdmissionBatch($_POST['values']);	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "EDITFILLAADHAR") {
    $lcode = $_POST['lcode'];
    $response = $emp->ShowDetailForEdit($lcode);
    $_DataTable = array();
    $_i = 0;
    if($response[0]=='Success'){
        while ($row = mysqli_fetch_array($response[2])) {
         $_Datatable[$_i] = array("Admission_UID" => $row['Admission_UID'],
             "Admission_PIN" => $row['Admission_PIN'],
             "Admission_Batch" => $row['Admission_Batch'],
             "Admission_Course" => $row['Admission_Course'],
             "Admission_LearnerCode" => $row['Admission_LearnerCode']);
         $_i = $_i + 1;
        }
    }
    echo json_encode($_Datatable);
}

/**
     * @param string  $date
     * @param boolean $time
     * @return string
     */
    function changeDateFormatStandered($date)
    {
        $timestamp = strtotime($date);
        return $new_date_format = date('d-M-Y', $timestamp);
    }


if ($_action == "GETDATAITGKWISE") {
    
    if($_SESSION['User_UserRoll']=='28' || $_SESSION['User_UserRoll']=='1'){
       $response = $emp->GetLearnerListTech($_POST['course'], $_POST['batch'], $_POST['txtlcode']); 
    }else{
       $response = $emp->GetLearnerListITGK($_POST['course'], $_POST['batch']); 
    }
    

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th>D.O.B</th>";
    echo "<th>Mobile</th>";
	echo "<th>Aadhar No.</th>";
	echo "<th>Action</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
            echo "<td>" . changeDateFormatStandered($_Row['Admission_DOB']) . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            echo "<td>" . $_Row['Admission_UID'] . "</td>";
            
            if($_Row['Admission_Aadhar_Status']=='0'){
                 echo "<td><input type='button' name='editaadhar' id='" . $_Row['Admission_LearnerCode'] . "'
						value='Update Aadhar No.' class='btn btn-primary link_aadhar'></td>";              
            }
			else if($_Row['Admission_Aadhar_Status']=='1'){            
				 echo "<td><input type='button' name='generatecertificate' id='" . $_Row['Admission_LearnerCode'] . "'
						value='Generate RS-CIT E-Certificate' class='btn btn-primary generate_certificate'></td>";
            }
			else if($_Row['Admission_Aadhar_Status']=='2'){            
				 echo "<td><input type='button' name='downloadcertificate' id='downloadcertificate'
						value='RS-CIT E-Certificate Generated' class='btn btn-primary'></td>";
            }
			else if($_Row['Admission_Aadhar_Status']=='3'){             
				 echo "<td><input type='button' name='updatecertificate' id='" . $_Row['Admission_LearnerCode'] . "'
						value='Update RS-CIT E-Certificate' class='btn btn-primary update_certificate'></td>";
            }
            else if($_Row['Admission_Aadhar_Status']=='4'){           
				 echo "<td><input type='button' name='downloadcertificate' id='downloadcertificate'
						value='RS-CIT E-Certificate Generated' class='btn btn-primary'></td>";
            }
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

  /***
     * @function generateOTP : function to generate Adhaar OTP
     * @param int $aadhaar_no : The adhaar number of User
     * @param int $pinCode : Area pin code
     * @return array : Returns an array containing the status and transaction id
     ***/

    function authenticateOTPbyEXC($aadhaar_no, $txnID, $otp){
        
    //    $URL="http://192.168.49.45/UIDAIAPI/uidai/otp/auth/".$aadhaar_no."/".$otp."/".$txnID;
		  $URL="http://192.168.164.202/UIDAIAPI/uidai/otp/auth/".$aadhaar_no."/".$otp."/".$txnID;
        $ch = curl_init();  
        curl_setopt($ch,CURLOPT_URL,$URL);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $output=curl_exec($ch);
        curl_close($ch);       
        $array = json_decode($output,true);
        if($array["authresponse"]["auth"]["@status"]=='Y')
        {
            $responseArray = [
            "status" => $array["authresponse"]["auth"]["@status"],
            "Aadharno" => $array["authresponse"]["UidData"]["@UUID"],
            "LName" => $array["authresponse"]["UidData"]["pi"]["@name"],
            "Fname" => $array["authresponse"]["UidData"]["pa"]["@co"],
            "Gender" => $array["authresponse"]["UidData"]["pi"]["@gender"],
            "LDOB" => $array["authresponse"]["UidData"]["pi"]["@dob"],
            "District" => $array["authresponse"]["UidData"]["pa"]["@dist"],
            "Tehsil" => $array["authresponse"]["UidData"]["pa"]["@loc"],
            "Pincode" => $array["authresponse"]["UidData"]["pa"]["@pc"]
            ];
        }
        else{
            $responseArray = [
               "status" => $array["authresponse"]["auth"]["@status"],
               "message" => $array["authresponse"]["message"]
            ];   
        } 
        return $responseArray;       
        
    }
	
	if ($_action == "generateandaddcertificate") {
			$lcode = $_POST['lcode'];							
			$path = '../upload/certificates';
			makeDir($path);

			$qrpath = $path . '/qrcodes/';
			makeDir($qrpath);
			
			$cropperpath = $path . '/cropper';
			makeDir($cropperpath);

			$masterPdf = $path . '/certificate_verified.pdf';
			//$masterPdf = $path . '/certificate_not_verified.pdf';

			if (!file_exists($masterPdf)) {
				die('Main pdf not found.');
			}

			$results = $emp->getLearnerResults($lcode,$signedNameReg);
			if(! mysqli_num_rows($results[2])) {
				die('No Record Found!!');
			}

			$pathBkp = '../upload/certificates_' . date("d-m-Y_h_i_s_a") . '_bkp';
			//rename($path, $pathBkp);

			$centerRow = mysqli_fetch_assoc($results[2]); 
	if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$centerRow['result_date'])) {

    $centerRow['result_date'] = date("d-m-Y", strtotime($centerRow['result_date']));
} 
			$filepath = '../upload/certificates/' . $centerRow['learnercode'] . '_certificate.pdf';
			if (file_exists($filepath)){
				unlink($filepath);
			} 
                        $evente = date("F Y", strtotime($centerRow['exam_date']));
			$learnerBatch = $emp->getData($centerRow['learnercode']);
			if ($learnerBatch) {
				$centerRow = array_merge($centerRow, $learnerBatch);
				$exam_date = explode('-', $centerRow['exam_date']);
				$centerRow['exam_year'] = $exam_date[2];
                                $centerRow['eventname'] = $evente;
			}
			
		//print_r($centerRow); die;

			$photo = getPath('photo', $centerRow);
			$fdfPath = generateFDF($centerRow, $path);
			if ($fdfPath) {
				generateCertificate($centerRow, $path, $fdfPath, $masterPdf, $photo);
					if (file_exists($filepath)) {
						/*   Add Certificate to Rajevault  */
							$response = $emp->ShowLearnerEcertificate($lcode);
							$successCount = "0";
							$FailCount = "0";
							$Count = "0";
							$statusmessage = "no record found in result";
								if ($response) {
									$_Row = mysqli_fetch_array($response[2]); 
									$Count = $Count+1;
									$filepath = "../upload/certificates/".$_Row['scol_no']."_certificate.pdf";
									$date= date_create($_Row['result_date']);
									$result_date = date_format($date,"d/m/Y"); 
									$datedob= date_create($_Row['dob']);
									$dob = date_format($datedob,"m/d/Y"); 
									$documentname = "".$_Row['scol_no']."_RSCIT";
                                                                        $exam_held_name = date("F Y", strtotime($_Row['exam_date']));
									$b64Doc = base64_encode(file_get_contents($filepath));
										$data = [
												'metaData' => [
														'FileExtension' => 'pdf',
														'Attributes' => [
																[
																		'Value' => $_Row['name'],
																		'SymbolicName' => 'LearnersName',
																],
																[
																		'Value' => $_Row['fname'],
																		'SymbolicName' => 'LearnersFathersHusbandsName',
																],
																[
																		'Value' => $_Row['scol_no'],
																		'SymbolicName' => 'LearnerCode',
																],
																[
																		'Value' => $_Row['study_cen'],
																		'SymbolicName' => 'CenterCode',
																],
																[
																		'Value' => $_Row['batch_name'],
																		'SymbolicName' => 'AdmissionBatch',
																],
																[
																		'Value' => $_Row['tot_marks'],
																		'SymbolicName' => 'Marks',
																],
																[
																		'Value' => $result_date,
																		'SymbolicName' => 'ResultDate',
																],
																[
																		'Value' => $exam_held_name,
																		'SymbolicName' => 'ExamEventName',
																],
																[
																		'Value' => $dob,
																		'SymbolicName' => 'DOB',
																],
																[
																		'Value' => $documentname,
																		'SymbolicName' => 'NameOfDocument',
																],
																[
																		'Value' => $documentname,
																		'SymbolicName' => 'DocumentTitle',
																],
														],
														'ClassName' => 'RSCITeCertificates',
														'MimeType' => 'application/pdf',
														'RepoDocID' => $_Row['scol_no'],
														'File' => $b64Doc,
												],
												'llx' =>  '218',
												'lly' =>  '95',
												'location' => 'Kota (Rajasthan)',
												'positionX' =>  '62.5',
												'positionY' =>  '90',
												'aadharId' => $_Row['learner_aadhar'],
												'fullname' => $_Row['name'],
												'SigningAuthorityAadhaar' => $signedAdhaar,
											];
										$json = json_encode($data);            
                                        
										if($_Row['scol_no']=='576180719110118789'){
                                                                                    print_r($json);
                                                                                }
										
									/*** INITIATE CURL REQUEST (DO NOT MODIFY) ***/
									$ch = curl_init($addServiceURL);
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
									curl_setopt($ch, CURLOPT_POST, 1);
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);		
									curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
									curl_setopt($ch, CURLOPT_HTTPHEADER, [
										"Accept: application/json",
										"Content-Type: application/json"
									]);

									/*** EXECUTE CURL HERE ***/
									$result = curl_exec($ch);
									if($errno = curl_errno($ch)) {
										$error_message = curl_strerror($errno);
										echo "cURL error ({$errno}):\n {$error_message}";
									}
            
									$jsonresultArray = json_decode($result, true);
                                                                        if(isset($jsonresultArray["status"])){
                                                                            $status = $jsonresultArray["status"];
                                                                            $message = $jsonresultArray["Message"];
                                                                            if($jsonresultArray['status']=='1'){
                                                                                    $response1 = $emp->UpdateEcertificateStatus($_Row['scol_no'],$_Row['exameventnameID'],$_Row['study_cen']);
                                                                                    $statusmessage = "<img src='images/righttick.gif' style='height: 115px;' /><div style='color: green;margin: 15px 0 15px 0;'>RS-CIT eCertificate has been uploaded to Raj eVault website. RS-CIT e certificate can be downloaded from <a href='https://evault.rajasthan.gov.in' target='_blank'>https://evault.rajasthan.gov.in</a></div>";										 
                                                                            } else{
                                                                                $statusmessage = "<img src='images/wrongtick.gif' style='height: 115px;' /><div style='color: red;margin: 15px 0 15px 0;'>Failed to upload RS-CIT eCertificate to Raj eVault website." . $message . "</div>";										 
                                                                            }            
                                                                            curl_close($ch); 
                                                                        }else{
                                                                            $statusmessage = "<img src='images/wrongtick.gif' style='height: 115px;' /><div style='color: red;margin: 15px 0 15px 0;'>Failed to upload RS-CIT eCertificate to Raj eVault website.</div>";										 
                                                                        }
									           
        
									echo "<div style='font-size: 20px;margin-top: 45px;'>".$statusmessage ."</div>";
						} else {
							echo "No Record Found";
						}
				}			
		}	
}	
	
	
	if ($_action == "generateandupdatecertificate") {
			$lcode = $_POST['lcode'];							
			$path = '../upload/certificates';
			makeDir($path);

			$qrpath = $path . '/qrcodes/';
			makeDir($qrpath);
			
			$cropperpath = $path . '/cropper';
			makeDir($cropperpath);

			$masterPdf = $path . '/certificate_verified.pdf';
	
			if (!file_exists($masterPdf)) {
				die('Main pdf not found.');
			}

			$results = $emp->getLearnerResults($lcode,$signedNameReg);
			if(! mysqli_num_rows($results[2])) {
				die('No Record Found!!');
			}

			$pathBkp = '../upload/certificates_' . date("d-m-Y_h_i_s_a") . '_bkp';
			//rename($path, $pathBkp);

			$centerRow = mysqli_fetch_assoc($results[2]); 
			 if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$centerRow['result_date'])) {

    $centerRow['result_date'] = date("d-m-Y", strtotime($centerRow['result_date']));
} 
			$filepath = '../upload/certificates/' . $centerRow['learnercode'] . '_certificate.pdf';
			if (file_exists($filepath)){
				unlink($filepath);
			} 
                        $evente = date("F Y", strtotime($centerRow['exam_date']));
			$learnerBatch = $emp->getData($centerRow['learnercode']);
			if ($learnerBatch) {
				$centerRow = array_merge($centerRow, $learnerBatch);
				$exam_date = explode('-', $centerRow['exam_date']);
				$centerRow['exam_year'] = $exam_date[2];
                                $centerRow['eventname'] = $evente;
			}
		
			$photo = getPath('photo', $centerRow);
			$fdfPath = generateFDF($centerRow, $path);
				if ($fdfPath) {
					generateCertificate($centerRow, $path, $fdfPath, $masterPdf, $photo);
						if (file_exists($filepath)) {
						/*   Update Certificate to Rajevault  */
							$response = $emp->ShowLearnerEcertificateUpdate($lcode);
							$successCount = "0";
							$FailCount = "0";
							$Count = "0";
							$statusmessage = "no record found in result";
								if ($response) {
									$_Row = mysqli_fetch_array($response[2]); 
									$Count = $Count+1;
									$filepath = "../upload/certificates/".$_Row['scol_no']."_certificate.pdf";
									$date= date_create($_Row['result_date']);
									$result_date = date_format($date,"d/m/Y"); 
									$datedob= date_create($_Row['dob']);
									$dob = date_format($datedob,"m/d/Y"); 
									$documentname = "".$_Row['scol_no']."_RSCIT";
                                                                        $exam_held_name = date("F Y", strtotime($_Row['exam_date']));
									$b64Doc = base64_encode(file_get_contents($filepath));
										 $data = [
										'metaData' => 
											[
											   'FileExtension' =>  'pdf',
											   'Attributes' =>  [[
																'Value' =>  $_Row['name'],
																'SymbolicName' =>  'LearnersName'
														], [
																'Value' =>  $_Row['fname'],
																'SymbolicName' =>  'LearnersFathersHusbandsName'
														],[
																'Value' =>  $_Row['scol_no'],
																'SymbolicName' =>  'LearnerCode'
														],[
																'Value' =>  $_Row['study_cen'],
																'SymbolicName' =>  'CenterCode'
														],[
																'Value' =>  $_Row['batch_name'],
																'SymbolicName' =>  'AdmissionBatch'
														],[
																'Value' =>  $_Row['tot_marks'],
																'SymbolicName' =>  'Marks'
														],[
																'Value' =>  $result_date,
																'SymbolicName' =>  'ResultDate'
														],[
																'Value' =>  $exam_held_name,
																'SymbolicName' =>  'ExamEventName'
														],[
																'Value' =>  $dob,
																'SymbolicName' =>  'DOB '
														],
														[
																'Value' => $documentname,
																'SymbolicName' => 'NameOfDocument',
														],
														[
																'Value' =>  $documentname,
																'SymbolicName' =>  'DocumentTitle'
												]]
											],
											'aadharId' =>  $_Row['learner_aadhar'],
											'docId' =>  $_Row['scol_no'],
											'classname' => 'RSCITeCertificates',
											'fileContent' =>  $b64Doc,
											'llx' =>  '218',
											'lly' =>  '95',
											'location' => 'Kota (Rajasthan)',
											'positionX' =>  '62.5',
											'positionY' =>  '90',
											'SigningAuthorityAadhaar'  =>  $signedAdhaar
								  ];

									$json = json_encode($data);   
if($_Row['scol_no']=='954171123110209789'){
                                                                    print_r($json);
                                                                }									
//print_r($json);
									/*** INITIATE CURL REQUEST (DO NOT MODIFY) ***/
									$ch = curl_init($updateServiceURL);
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
									curl_setopt($ch, CURLOPT_POST, 1);
									curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
									curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);		
									curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
									curl_setopt($ch, CURLOPT_HTTPHEADER, [
										"Accept: application/json",
										"Content-Type: application/json"
									]);

									/*** EXECUTE CURL HERE ***/
									$result = curl_exec($ch);
									if($errno = curl_errno($ch)) {
										$error_message = curl_strerror($errno);
										echo "cURL error ({$errno}):\n {$error_message}";
									}
            
								$jsonresultArray = json_decode($result, true);
                                                                if(isset($jsonresultArray["status"])){
                                                                    $status = $jsonresultArray["status"];
                                                                    $message = $jsonresultArray["Message"];
                                                                            if($jsonresultArray['status']=='1'){
                                                                                    $response1 = $emp->UpdateEcertificateStatusForUpdateCertificate($_Row['scol_no'],
                                                                                                                            $_Row['exameventnameID'],$_Row['study_cen']);
                                                                                    $statusmessage = "<img src='images/righttick.gif' style='height: 115px;' /><div style='color: green;margin: 15px 0 15px 0;'>RS-CIT eCertificate has been uploaded to Raj eVault website. RS-CIT e certificate can be downloaded from <a href='https://evault.rajasthan.gov.in' target='_blank'>https://evault.rajasthan.gov.in</a></div>";										 
                                                                            }else{
                                                                                    $statusmessage = "<img src='images/wrongtick.gif' style='height: 115px;' /><div style='color: red;margin: 15px 0 15px 0;'>Failed to upload RS-CIT eCertificate to Raj eVault website." . $message . "</div>";										 
                                                                            }							 
                                                                    curl_close($ch);
                                                                }else{
                                                                        $statusmessage = "<img src='images/wrongtick.gif' style='height: 115px;' /><div style='color: red;margin: 15px 0 15px 0;'>Failed to upload RS-CIT eCertificate to Raj eVault website.</div>";										 
                                                                }
														
							
							echo "<div style='font-size: 20px;margin-top: 45px;'>".$statusmessage ."</div>";
						} else {
							echo "No Record Found";
						}
				}			
		}	
}
	
	/*Generate PDF Functions*/
	function getPath($type, $row) {
	global $general;
	$batchname = trim(ucwords($row['batchname']));
    $coursename = $row['coursename'];
    $isGovtEmp = stripos($coursename, 'gov');
    if ($isGovtEmp) {
        $batchname = $batchname . '_Gov';
    } else {
        $isWoman = stripos($coursename, 'women');
        if ($isWoman) {
            $batchname = $batchname . '_Women';
        } else {
            $isMadarsa = stripos($coursename, 'madar');
            if ($isMadarsa) {
                $batchname = $batchname . '_Madarsa';
            }
        }
    }

    $batchname = str_replace(' ', '_', $batchname);
    $dirPath = '../upload/admission' . $type . '/' . $row['learnercode'] . '_' . $type . '.png';

    $imgPath = $dirPath;
	$imgPath = str_replace('//', '/', $imgPath);

    $dirPathJpg = str_replace('.png', '.jpg', $dirPath);
    $imgPathJpg = str_replace('.png', '.jpg', $imgPath);

    $imgBatchPath = '../upload/admission' . $type . '/' . $batchname . '/' . $row['learnercode'] . '_' . $type . '.png';
	$imgBatchPath = str_replace('//', '/', $imgBatchPath);
    $imgBatchPathJpg = str_replace('.png', '.jpg', $imgBatchPath);

    $path = '';
    if (file_exists($imgBatchPath)) {
    	$path = $imgBatchPath;
    } elseif (file_exists($imgBatchPathJpg)) {
    	$path = $imgBatchPathJpg;
    } elseif (file_exists($imgPath)) {
    	$path = $imgPath;
    } elseif (file_exists($imgPathJpg)) {
    	$path = $imgPathJpg;
    } else {
    	/*$imgPath = 'http://' . $_SERVER['HTTP_HOST'] . $dirPath;
    	if(@getimagesize($imgPath)) {
    		$path = $imgPath;
    	} else {
    		$path = 'http://' . $_SERVER['HTTP_HOST'] . $dirPathJpg;
    	}*/
    }

    return $path;
}

function makeDir($path) {
    return is_dir($path) || mkdir($path);
}

function generateFDF($data, $path) {
	$fdfContent = '%FDF-1.2
		%Ã¢Ã£Ã?Ã“
		1 0 obj 
		<<
		/FDF 
		<<
		/Fields [';
    foreach ($data as $key => $val) {
        $fdfContent .= '
				<<
				/V (' . $val . ')
				/T (' . $key . ')
				>> 
				';
    }
    $fdfContent .= ']
		>>
		>>
		endobj 
		trailer
		<<
		/Root 1 0 R
		>>
		%%EOF';

	$filePath = $path . '/' . $data['learnercode'] . '.fdf';
    file_put_contents($filePath, $fdfContent);
    $return = '';
    if (file_exists($filePath)) {
    	$return = $filePath;
    }

    return $return;
}

function generateCertificate($centerRow, $path, $fdfPath, $masterPdf, $photo) {
	$resultPath = $path . '/' . $centerRow['learnercode'] . '_certificate.pdf';

	$pdftkPath = ($_SERVER['HTTP_HOST']=="myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : (($_SERVER['HTTP_HOST']=="staging.rkcl.co.in") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"') ;
	
	$command = $pdftkPath . '  ' . $masterPdf . ' fill_form   ' . $fdfPath . ' output  '. $resultPath . ' flatten';
	exec($command);
	unlink($fdfPath);
	addPhotoInPDF($centerRow['learnercode'], $photo, $path, $resultPath);
}

function addPhotoInPDF($learnercode, $Imagepath, $path, $pdfPath) {

    require_once('../fpdi/fpdf/fpdf.php');
    require_once('../fpdi/fpdi.php');
    require_once('../fpdi/fpdf_tpl.php');
    require_once("../cropper.php");
	
	global $general;
	global $cropperpath;
	
	//echo $Imagepath; die;
    
    //$qrpath = generateQRCode($learnercode, $path);
    //$qrResource = imagecreatefromstring(file_get_contents($qrpath));
    $picResource = imagecreatefromstring(file_get_contents($Imagepath));

    $pdf = new FPDI();
    $pdf->AddPage();
    $pdf->setSourceFile($pdfPath);
    $template = $pdf->importPage(1);
    $pdf->useTemplate($template);
    

    try {
    	if(!empty($Imagepath) && $picResource) $pdf->Image($Imagepath, 92.8, 79.5, 29, 32.8);
	} catch (Exception $_ex) {
		$pdf->Output($pdfPath, "F");

		if (!empty($Imagepath) && file_exists($Imagepath)) {
			$cropper = new Cropper($cropperpath);
			$args = array("size" => array(30,30), "image" => $Imagepath, "placeholder" => 2);
			$newImgPath = $cropper->crop($args);

			if (file_exists($newImgPath)) {
				addPhotoInPDF($learnercode, $newImgPath, $path, $pdfPath);
			}
		}
		return;
    }

    $pdf->Output($pdfPath, "F");
}

function generateQRCode($learnercode, $path) {
	$return = '';
    $qrpath = $path . '/qrcodes/' . $learnercode . '.png';
    if (file_exists($qrpath)) return $qrpath;

    try {
    	require_once ('qr_sat/qr_img.php');
    	$id='471170616094512789';
		$data='?id='.$id;
		$image = new Qrcode();
		$image->setdata($data);
		$image->calculate();
		$image->save($qrpath);
    } catch (Exception $_ex) {
		print_r($_ex->getTrace());
	}

	if (file_exists($qrpath)) {
		$return = $qrpath;
	}

	return $return;
}

function generateBarCode($learnercode, $path) {
	require_once('phpbarcode/code128.class.php');

	$return = '';
    $barpath = $path . '/barcodes/' . $learnercode . '.png';
    if (file_exists($barpath)) return $barpath;
	try {
	$barcode = new phpCode128($learnercode, 90, $path . '\verdana.ttf', 10);
	$barcode->setEanStyle(false);
	$barcode->setShowText(true);
	$barcode->setPixelWidth(2);
	$barcode->setBorderWidth(0);
	$barcode->saveBarcode($barpath);
	} catch (Exception $_ex) {
		print_r($_ex->getTrace());
	}

	if (file_exists($barpath)) {
		$return = $barpath;
	}

	return $return;
}

if ($_action == "GETDATAEcertificateRajevolt") 
{ 
    $aadhar = $_POST["aadhar"];
    $lcode = $_POST["lcode"];
    $filestorepath = "../upload/singedEcertificate/";
    //$filedownloadlinkpath = "http://localhost/myrkcl_git/upload/singedEcertificate/";
    $filedownloadlinkpath = "https://myrkcl.com//upload/singedEcertificate/";

    $data = [
                "aadharId" => $aadhar,
                "docId" => $lcode,
                "classname" => "RSCITeCertificates"	

            ];

    echo $json = json_encode($data);
  
    $ch = curl_init($getServiceURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);		
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Accept: application/json",
            "Content-Type: application/json"
    ]);
    
    $result = curl_exec($ch);
    if($errno = curl_errno($ch)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    //print_r($result);
    $jsonresultArray = json_decode($result, true);
    $status = $jsonresultArray["status"];
    if($jsonresultArray['status']=='1'){
        $file = $jsonresultArray["file"];
        $DocumentTitle = $jsonresultArray["DocumentTitle"];
        $pdf_decoded = base64_decode($file);
        $genfile = $filestorepath.$DocumentTitle.'.pdf';
        $pdf = fopen ($genfile,'w');
        fwrite ($pdf,$pdf_decoded);
        fclose ($pdf);
        if(file_exists($genfile)) {
               echo "<td><a href='$filedownloadlinkpath$DocumentTitle.pdf' target='_blank'><button class='btn btn-primary downloadEcertificate' type='button'>View or Download</button></a></td>";
        }
    }else{
        $message = $jsonresultArray["Message"];
        echo "<div  style='background-color: darkgray;text-align: center;padding: 10px;border-radius: 10px;font-size: 18px;'><span style='font-weight: bold;'>".$message."</span></div>";
    }
    curl_close($ch);
}
