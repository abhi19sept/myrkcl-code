<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsAdmissionDistrictWiseCount.php';

$response = array();
$emp = new clsAdmissionDistrictWiseCount();

if ($_action == "GETDATA") {

    $response = $emp->GetDataAll($_POST['course'], $_POST['batch']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>District Name</th>";    
    echo "<th>Total Uploaded Learner Count</th>";
    echo "<th>Total Confirmed Learner Count</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;
   
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";       
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Total_Learner'] . "</td>";
            echo "<td>" . $_Row['Confirmed_Learner'] . "</td>";
            $_Count++;
        }

        echo "</tbody>";        
        echo "</table>";
        echo "</div>";
   
}

