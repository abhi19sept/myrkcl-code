<?php

/* 
 * Created by yogendra

 */

include './commonFunction.php';
require 'BAL/clsexamchooicemaster.php';

$response = array();
$emp = new clsexamchooicemaster();

if ($_action == "FILL") {
    $response = $emp->GetThesils($_actionvalue);
    echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Tehsil_Code'] . ">" . $_Row['Tehsil_Name'] . "</option>";
    }
}

//Print_r($_POST);
if ($_action == "ADD") 
{
    if (isset($_POST["txtregion"]) && !empty($_POST["ddlDistrict1"])  && !empty($_POST["ddlChooice1"])  && !empty($_POST["ddlChooice2"])  && !empty($_POST["ddlChooice3"])) 
	{
       
		$_Region = $_POST["txtregion"];
		
		
      
		$_District = $_POST["ddlDistrict1"];
        $_Chooice1 = $_POST["ddlChooice1"];
        $_Chooice2 = $_POST["ddlChooice2"];
        $_Chooice3 = $_POST["ddlChooice3"];
		
		if(($_Chooice1==$_Chooice2) OR ($_Chooice1== $_Chooice3) )
		{
			echo "Duplicate Choice1";
		}
		else if(($_Chooice2== $_Chooice3) )
	    {
			echo "Duplicate Choice2";
		}
		else
		{
			$response = $emp->Add($_Region,$_District,$_Chooice1,$_Chooice2,$_Chooice3);
			echo $response[0];
		}
       
       
    }
	else
	{
		echo "Please fill All Tehsil";
	}
	
}





if ($_action == "DELETE") 
{


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") 
{

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

   echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'>CenterCode</th>";
	 
	   echo "<th style='35%'>District</th>";
	    
		 echo "<th style='35%'>1st<br>Preference </th>";
		  echo "<th style='35%'>2st<br>Preference </th>";
		   echo "<th style='35%'>3st<br>Preference</th>";
		   
		   
    echo "<th style='20%'>Action</th>";
	echo "<th style='20%'>Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['centercode'] . "</td>";
		  
		   echo "<td>" . $_Row['District_Name'] . "</td>";
		   
		    echo "<td>" . $_Row['choice1'] . "</td>";
			 echo "<td>" . $_Row['choice2'] . "</td>";
			  echo "<td>" . $_Row['choice3'] . "</td>";
			 
			  echo "<td nowrap>";
			  if($_Row['status']=='Approve')
			  {
					echo "<span  >Approved</span>";
				
			  }
			  else
			  {
				echo "<a href='frmexamchoicemaster.php?code=" . $_Row['id'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>";
			  }
			  echo "</td>";
			  
			  echo "<td nowrap>";
			  if($_Row['status']=='Approve')
			  {
			   echo "<span  >Approved</span>";
			  }
			  else if($_Row['status']=='Disapprove')
			  {
				echo "<span >Denied</span> <br>(" . $_Row['Reasion'] . ")";
			  }
			  else 
			  {
				echo "<span >Pending</td>";
			  }
			  echo "</td>"; 
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
	
}



if ($_action == "FILLDISTRICT") 
{
    $response = $emp->GetAllDistrict($_actionvalue);
	$response1 = $emp->GetcenterDistrict();
	$_Row1 = mysqli_fetch_array($response1[2]);
    echo "<option value=" . $_Row1['District_Code'] . " selected='selected'   >" . $_Row1['District_Name'] . " </option>";
	
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['District_Code'] . ">" . $_Row['District_Name'] . "</option>";
    }
}







if ($_action == "EDIT") 
{


    $response = $emp->GetDatabyCode($_POST['values']);

    $_DataTable = array();
    $_i = 0;
    $_Row = mysqli_fetch_array($response[2]); 
        $_Datatable[$_i] = array("district" => $_Row['district'],
           
            "choice1" => $_Row['choice1'],
            "choice2" => $_Row['choice2'],
            "choice3" => $_Row['choice3']
            );
        $_i = $_i + 1;
    
    echo json_encode($_Datatable);
}


if ($_action == "UPDATE") {
    if (isset($_POST["code"]) && isset($_POST["txtregion"]) && !empty($_POST["ddlDistrict1"])  && !empty($_POST["ddlChooice1"])  && !empty($_POST["ddlChooice2"])  && !empty($_POST["ddlChooice3"])) 
	{
       
		$_Region = $_POST["txtregion"];
		$_Code = $_POST["code"];
		
		
      
		$_District = $_POST["ddlDistrict1"];
        $_Chooice1 = $_POST["ddlChooice1"];
        $_Chooice2 = $_POST["ddlChooice2"];
        $_Chooice3 = $_POST["ddlChooice3"];
		
		if(($_Chooice1==$_Chooice2) OR ($_Chooice1== $_Chooice3) )
		{
			echo "Duplicate Choice1";
		}
		else if(($_Chooice2== $_Chooice3) )
	    {
			echo "Duplicate Choice2";
		}
		else
		{
			$response = $emp->UPDATE($_Code,$_District,$_Chooice1,$_Chooice2,$_Chooice3);
			echo $response[0];
		}
       
       
    }
	else
	{
		echo "Please fill All Tehsil";
	}
	
}



if ($_action == "SENDOTP") {
    if (isset($_POST["txtregion"]) && !empty($_POST["ddlDistrict1"])  && !empty($_POST["ddlChooice1"])  && !empty($_POST["ddlChooice2"])  && !empty($_POST["ddlChooice3"])) 
	{
       
		$_Region = $_POST["txtregion"];
		
		
      
		$_District = $_POST["ddlDistrict1"];
        $_Chooice1 = $_POST["ddlChooice1"];
        $_Chooice2 = $_POST["ddlChooice2"];
        $_Chooice3 = $_POST["ddlChooice3"];
		
		if(($_Chooice1==$_Chooice2) OR ($_Chooice1== $_Chooice3) )
		{
			echo "Duplicate Choice1";
		}
		else if(($_Chooice2== $_Chooice3) )
	    {
			echo "Duplicate Choice2";
		}
		
		else
		{
			$response = $emp->SendOTP();
			echo $response[0];
		}
       
       
    }
	else
	{
		echo "Please fill All Tehsil";
	}
	
}


if ($_action == "VERIFY") 
{
    //print_r($_POST);
    if (isset($_POST["otp"])) {
        
        $_Otp = $_POST["otp"];

        $response = $emp->Verify($_Otp);
        echo $response[0];
    }
}





if ($_action == "FILLMOBILENO")
{
    $response = $emp->Getmobile();
	$_Row = mysqli_fetch_array($response[2]);
       $_Mobile = $_Row['User_MobileNo'];
        $_Mobile1 =  substr($_Mobile,6);
        $_Mobile2 = "XXXXXX".$_Mobile1;
		echo $_Mobile2;
}


if ($_action == "FILLEdittehshil") {
    $response = $emp->GetAllTeh();
    echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Tehsil_Code'] . ">" . $_Row['Tehsil_Name'] . "</option>";
    }
}














