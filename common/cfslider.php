<?php
include('./commonFunction.php');
include('BAL/clsslider.php');
$response = array();
$emp = new clsSliderForm();

//Add New Slider Images.....
if($_POST['action'] == "Addslider") 
{
    if(isset($_POST["imageTittle"]) && !empty($_POST["imageTittle"]) 
         && isset($_POST["sliderStatus"]) && !empty($_POST["sliderStatus"])
         && isset($_FILES["silederImage"]) && !empty($_FILES["silederImage"]) && !empty($_POST["link"]) )
       {
            $imageTittle = $_POST['imageTittle'];
            $sliderStatus = $_POST['sliderStatus'];
			$link = $_POST['link'];
            $img = $_FILES['silederImage']['name'];
            $tmp = $_FILES['silederImage']['tmp_name'];
            $temp = explode(".",$img);
            $newfilename = round(microtime(true)) . '.' . end($temp);
            $filestorepath = "../upload/slider/".$newfilename;
            $imageFileType = pathinfo($filestorepath,PATHINFO_EXTENSION);
            if ($_FILES["silederImage"]["size"] > 50000000) 
            {
                echo "Sorry, your file is too large.";
            }
           else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") 
            {
                echo "Sorry, File Not Valid"; 
            }
            else
            {
                if(move_uploaded_file($tmp,$filestorepath))
                { 
                    $response = $emp->ImageSliderNew($imageTittle,$sliderStatus,$newfilename,$link);
                    echo $response[0];
                }
                else
                {
                    echo "Invalid Details";
                }
            }
        }
}

//Show Data Or View Details...
if ($_action == "SHOW") {
    if($_POST['status']=='1') {
            $response = $emp->ShowSlideImages($_POST['status']);
            echo "<div class='table-responsive'>";
            echo  "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>S No.</th>";
            echo "<th>Image Tittle</th>";
			 echo "<th>Image Link</th>";
            echo "<th>Add date</th>";
            echo "<th>Status</th>";
            echo "<th>Photo</th>";
            echo "<th>Edit</th>";
            echo "<th>Delete</th>";          
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;
            if($response[0]=='Success'){
                while ($row = mysqli_fetch_array($response[2])) {
                    echo "<tr class='odd gradeX'>";
                    echo "<td>" .$_Count. "</td>";
                    echo "<td>" .$row['imagetittle']. "</td>";
					echo "<td>" .$row['link']. "</td>";
                    echo "<td>" .date("F d, Y H:i:s", strtotime($row['time'])). "</td>";
                    echo "<td>" .$row['status']. "</td>";
                    echo "<td><button id='" .$row['id']. "' class='viewimage'>View</button></td>";
                    echo "<td><button id='" .$row['id']. "' class='fun_update_slider'>Update</button></td>";
                    echo "<td><input type='hidden' id='file_id_".$row['id']."' value='".$row['photo']."'><button id='" .$row['id']. "' class='fun_delete_slider'>Delete</button></td>";
                    echo "</tr>";		
                    $_Count++;
                }
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";     
	   }
}

//Delete Slider Images....
if ($_action == "DELETE") 
    {
        if (isset($_POST["deleteid"])) 
          {   
              $deleteid = $_POST["deleteid"];
              $response = $emp->DeleteSliderImage($deleteid);
              $delfie = $_POST["fileid"];
              unlink("../upload/slider/".$delfie);
            }
    }
  
//Edit Or Fill Data for Slider Image
if ($_action == "EDIT") {
    $editid = $_POST['editid'];
    $_POST['fileid'];
    $response = $emp->ShowSlideImagesForEdit($editid);
    $_DataTable = array();
    $_i = 0;
    if($response[0]=='Success'){
        while ($row = mysqli_fetch_array($response[2])) {
         $_Datatable[$_i] = array("sliderid" => $row['id'],
             "imagetittle" => $row['imagetittle'],
			 "link" => $row['link'],
             "status" => $row['status'],
             "photo" => $row['photo'],);
         $_i = $_i + 1;
        }
    }
    echo json_encode($_Datatable);
}

//Update Slider Image And Data....
if ($_POST['action'] == "Updateslider") { 
    if(isset($_POST["imageTittleupdate"]) && !empty($_POST["imageTittleupdate"]) 
         && isset($_POST["sliderStatusupdate"]) && !empty($_POST["sliderStatusupdate"])
         && isset($_FILES["silederImageupdate"]) && !empty($_FILES["silederImageupdate"])
         && isset($_POST["updatelink"]) && !empty($_POST["updatelink"])
         && isset($_POST["sliderid"]) && !empty($_POST["sliderid"])
         && isset($_POST["oldimagefile"]) && !empty($_POST["oldimagefile"])
       ){
            $imageTittleupdate = $_POST['imageTittleupdate'];
			$link = $_POST['updatelink'];
            $sliderStatusupdate = $_POST['sliderStatusupdate'];  
            $silederImageupdate = $_FILES['silederImageupdate'];  
            $sliderid = $_POST['sliderid'];  
            $oldimagefile = $_POST['oldimagefile'];  
            if(empty($_FILES['silederImageupdate']['name']))
            {
                $newfilename = $_POST["oldimagefile"];
                $response = $emp->SliderImageUpdate($sliderid,$imageTittleupdate,$sliderStatusupdate,$newfilename,$link);
                       
            } 
            else
            {
                $img = $_FILES['silederImageupdate']['name'];
                $tmp = $_FILES['silederImageupdate']['tmp_name'];
                $temp = explode(".",$img);
                $newfilename = round(microtime(true)) . '.' . end($temp);
                $filestorepath = "../upload/slider/".$newfilename;
                $imageFileType = pathinfo($filestorepath,PATHINFO_EXTENSION);
                if ($_FILES["silederImageupdate"]["size"] > 50000000) 
                {
                    echo "Sorry, your file is too large.";
                }
                else if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                            && $imageFileType != "gif") 
                {
                    echo "Sorry, File Not Valid"; 
                }
                else
                {
                    if(move_uploaded_file($tmp,$filestorepath))
                    { 
                        $delfie = $_POST["oldimagefile"];
                        unlink("../upload/slider/".$delfie);
                        $response = $emp->SliderImageUpdate($sliderid,$imageTittleupdate,$sliderStatusupdate,$newfilename,$link);
						echo $response[0];
                    }
                    else
                    {
                        echo "Invalid details";
                    }
                }
            }
        }
}

?>