<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsAddressChangeAmtUpdate.php';

$response = array();
$emp = new clsAddressChangeAmtUpdate();



if ($_action == "DETAILS") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                "tehsilname" => ($_Row['Tehsil_Old'] ? $_Row['Tehsil_Old'] : 'Not Available'),
                "areatype" => ($_Row['Organization_AreaType_old'] ? $_Row['Organization_AreaType_old'] : 'Not Available'),
                "ncramt" => $_Row['NCRamount_Amount'],
                "tehsilname_new" => ($_Row['Tehsil_New'] ? $_Row['Tehsil_New'] : 'Not Available'),
                "areatype_new" => ($_Row['Organization_AreaType'] ? $_Row['Organization_AreaType'] : 'Not Available'));
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "You are not Auhorized to use this functionality on MYRKCL.";
    }
}

if ($_action == "UPDATE") {
//    print_r($_POST);
//    die;
            if (isset($_POST["values"]) && !empty($_POST["values"])) {
            
            $response = $emp->Update($_POST["values"]);
            echo $response[0];
            } else {
                echo "Sommething Went Wrong. Reload the page  and try again.";
                   }
        
    
}
