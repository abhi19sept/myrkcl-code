<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsRatingTempDataGenerate.php';

$response = array();
$emp = new clsRatingTempDataGenerate();

if ($_action == "FillRatingParameters") {
    $response = $emp->GetRatingParameters();	
    echo "<option value=''>Select Parameter</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['ranking_weightage_id'] . ">" . $_Row['ranking_weightage_parameter'] . "</option>";
    }
}

if ($_action == "FillBatchforRating") {
    $response = $emp->GetBatchforRating();
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FillExamEvent") {
        $response = $emp->GetExamEvent();
        echo "<option value='' selected='selected'>Please Select</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name_VMOU'] . "</option>";    
        }
}

if ($_action == "ADD") { 
	if (isset($_POST["parameter"]) && !empty($_POST["parameter"])) {
		if (isset($_POST["batch"]) && !empty($_POST["batch"])) {							
											
							$_Batch = trim($_POST["batch"]);
							$_Parameter = trim($_POST["parameter"]);
							$_Event = trim($_POST["event"]);					

						$response = $emp->Add($_Batch, $_Parameter, $_Event);
						echo $response[0];				  
			
		} else {
			echo "something2";
		}
	} 	else {
			echo "something3";
		}

}

