<?php

/* 
 * Created by yogi

 */

include './commonFunction.php';
require 'BAL/clsexaminationeventmaster.php';
$response = array();
$emp = new clsexaminationeventmaster();

if ($_action == "PDF") 
{
		$generateBy = $_POST['generateBy'];
	
		$ddlCenter1 = isset($_POST['ddlCenter']) ? $_POST['ddlCenter'] : [] ;
		$center2 = @implode(",", $ddlCenter1);
		//print_r($center2);
		$CenterCode3 = rtrim($center2, ",");
		$center3 = @explode(",",$CenterCode3);

		$ddlCenter1 = isset($_POST['ddlExamCenter']) ? $_POST['ddlExamCenter'] : [] ;
		$center2 = @implode(",", $ddlCenter1);
		//print_r($center2);
		$examCenterCodes = rtrim($center2, ",");
		$ExamCenter3 = @explode(",",$examCenterCodes);
		$district = $_POST['ddlDistrict'];
		//print_r($ExamCenter3);
		$learnerCode = isset($_POST['learnerCode']) ? $_POST['learnerCode'] : '' ;

        $job = isset($_POST['job']) ? $_POST['job'] : '' ;

		
	 $_DataTable = "";
    
    $error = 'Unable to process due to insufficient details provided.';
    switch ($generateBy) {
    	case 'district' :
    		if (!empty($district)) {
    			$error = '';
    		}
    		break;
    	case 'itgk' :
    		if (!empty($CenterCode3) && !empty($district)) {
    			$error = '';
    		}
    		break;
    	case 'examcenter' :
    		if (!empty($examCenterCodes) && !empty($district)) {
    			$error = '';
    		}
    		break;
    	case 'learners' :
    		if (!empty($learnerCode)) {
    			$error = '';
    		}
    		break;
        case 'byupdatepics' :
                if (!empty($_POST["code"])) {
                    $error = '';
                }
            break;
        case 'job' :
        case 'miss' :
        case 'copy' :
            if (!empty($job)) {
                $error = '';
            }
            break;
    }

    if (empty($error)) {
        if ($generateBy == 'miss' || $generateBy == 'copy') {
            $copy = ($generateBy == 'copy') ? 1 : 0;
            echo "<a class='downloadbutton' target='_blank' href='process_learner_photo_signs.php?code=" . $_POST["code"] . "&job=" . $job . "&copy=" . $copy . "'><img src='images/Download-zip.png' alt='Edit' width='100px' style='margin-top:20px;margin-left:40px;' /></a>";
        } elseif ($generateBy == 'byupdatepics') {
            echo "<a class='downloadbutton' target='_blank' href='printexaminationletterpdf.php?code=" . $_POST["code"] . "&act=" . $generateBy . "&Mode=Edit'><img src='images/Download-zip.png' alt='Edit' width='100px' style='margin-top:20px;margin-left:40px;' /></a>";
        } else {
		  echo "<a class='downloadbutton' target='_blank' href='printexaminationletterpdf.php?code=" . $_POST["code"] . "&centercode=" . $CenterCode3 . "&examCenterCode=" . $examCenterCodes . "&district=" . $district . "&learnerCode=" . $learnerCode . "&job=" . $job . "&Mode=Edit'><img src='images/Download-zip.png' alt='Edit' width='100px' style='margin-top:20px;margin-left:40px;' /></a>";
        }
	} else {
		echo "<p class='error'><span></span> " . $error . " <span></span></p>";
	}
}


if ($_action == "FILL") {
    $response = $emp->Getcenter($_POST['values']);
    
	 echo "<option value='All' selected='selected'>Select All</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
		
        echo "<option value=" . $_Row['centercode'] . ">" . $_Row['centercode'] . "</option>";
    }
}
if ($_action == "FillByDistrict") {
    $response = $emp->GetcenterByDistrict($_POST['values']);
    	
    if (mysqli_num_rows($response[2])) {
	 echo "<option value='All' selected='selected'>All ITGK</option>";
	}
    while ($_Row = mysqli_fetch_array($response[2])) {
		
        echo "<option value=" . $_Row['centercode'] . ">" . $_Row['centercode'] . "</option>";
    }
}
?>


