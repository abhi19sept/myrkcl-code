<?php

/*
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clstransectionauditreport.php';
//require '../frmPayuTransactionResponse.php';

$response = array();
$emp = new clstransectionauditreport();


if ($_action == "GETDATA") {

    //echo "Show";
    $response = $emp->GetAll($_POST['transaction_type'], $_POST['dateFrom'], $_POST['dateTo']);
    $report = $response[0];
    
    $_DataTable = "";
	 echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
	 echo "<th> </th>";
    echo "<th> </th>";
	echo "<th> </th>";
    echo "<th> </th>";
    echo "<th> </th>";
    echo "</tr>";
	
	switch ($_POST['transaction_type']) {
		case 'LearnerFeePayment':
			$transactionTable = 'Admission Transaction';
			$mainTable = 'Admission';
			$invoiceTable = 'tbl_admission_invoice';
			break;
		case 'ReexamPayment':
			$transactionTable = 'ReExam Transaction';
			$mainTable = 'ExamData';
			$invoiceTable = 'tbl_reexam_invoice';
			break;
		case 'Correction Certificate':
		case 'Duplicate Certificate':
			$transactionTable = 'Correction Transaction';
			$mainTable = 'Correction_Copy';
			$invoiceTable = 'tbl_correction_invoice';
			break;
		case 'NcrFeePayment':
		case 'Ncr Fee Payment':
			$transactionTable = 'NCR Transaction';
			$mainTable = 'tbl_org_master';
			$invoiceTable = 'tbl_ncr_invoice';
			break;
		case 'EOI Fee Payment':
			$transactionTable = 'EOI Transaction';
			$mainTable = 'tbl_courseitgk_mapping';
			$invoiceTable = 'tbl_eoi_invoice';
			break;
		case 'NameAddressFeePayment':
			$transactionTable = 'tbl_address_name_transaction';
			$mainTable = 'tbl_change_address_itgk';
			$invoiceTable = 'tbl_nlc_itgk_gstinvoice';
			break;
	}
	
	echo "<tr>";
	 echo "<td> Status </td>";
    echo "<td> Payment Transaction </td>";
	echo "<td> " . $transactionTable . " </td>";
    echo "<td> " . $mainTable . "</td>";
    echo "<td> " . $invoiceTable . "</td>";
    echo "<td> Refund</td>";
	echo "<td >Total</td>";
	echo "<td >Amount</td>";
    echo "</tr>";
	echo "</thead>";
    echo "<tbody>";
   
    $transactionsTotal = $table1Total = $table2Total = $refundsTotal = $rkclAmountTotal = 0;
    $payuTotals = [];
	foreach ($report as $type => $transactions) {
        if ($type == 'RKCL_Total' || $type == 'PayU') continue;
		echo "<tr class='odd gradeX'>";
		echo "<td> " . $type . "</td>";
		 echo "<td> " . $transactions['Payment_Transactions']['count'] . "</td>";
		echo "<td> " . $transactions['table1_transactions']['count'] . "</td>";
		echo "<td> " . $transactions['table2_transactions']['count'] . " (" . $transactions['table2_transactions']['LearnersCount'] . ") " . ((isset($transactions['table2_transactions']['missings'])) ? $transactions['table2_transactions']['missings'] : '' ) . "</td>";
		echo "<td> " . $transactions['table3_transaction_invoices']['count'] . "</td>";
        echo "<td> " . $transactions['Refunds']['count'] . "</td>";

		echo "<td> " . $transactions['Payment_Transactions']['count'] . "</td>"; 
      	echo "<td> " . $transactions['Payment_Transactions']['Amount'] . "</td>";
        echo "</tr>";

        $transactionsTotal = $transactionsTotal + $transactions['Payment_Transactions']['count'];
        $table1Total = $table1Total + $transactions['table1_transactions']['count'];
        $table2Total = $table2Total + $transactions['table2_transactions']['count'];
        $table3Total = $table2Total + $transactions['table3_transaction_invoices']['count'];
        $refundsTotal = $refundsTotal + $transactions['Refunds']['count'];
        $rkclAmountTotal = $rkclAmountTotal + $transactions['Payment_Transactions']['Amount'];
		
	}

    echo "</tbody>";
	 echo "<tfoot>";
        echo "<tr>";
        echo "<th > Total: </th>";
		        echo "<th >" . $transactionsTotal . "</th>";
		echo "<th >" . $table1Total . "</th>";
		 echo "<th >" . $table2Total . "</th>";
		 echo "<th >" . $table3Total . "</th>";
		   echo "<th >" . $refundsTotal . "</th>";

		    echo "<th >" . $transactionsTotal . "</th>";
			echo "<th >" . $rkclAmountTotal . "</th>";
			
        echo "</tr>";
        echo "</tfoot>";
    echo "</table>";
	 echo "</div>";
}

if ($_action == "Optimization" || $_REQUEST["action"] == "Optimization") {

	$emp->optimizeTransactionsRecords($_POST);

	echo "<p class='text-success small'><span><img src='images/correct.gif' width=20px /></span><span>Optimization Completed.</span></p>";
}


function performReconcilation() {
	while ($response = getResponseByPaymentTransaction(0)) {
		$response = getTransactionStatusByPayU($response);
		processResponse($response);
	}
}