<?php
include './commonFunction.php';
require 'BAL/clssitemap.php';
$response = array();
$emp = new clssitemap();
if ($_action == "SHOW") {
    global $emp;
    global $response;
    $_Menu = "";
    $response = $emp->GetRootMenu();
    if ($response[0] == Message::SuccessfullyFetch) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Menu .= "<ul>";
            $_Menu .= "<h5 class='weight-500'>Level 1</h5>";
            $_Menu .= "<li><a href='#' >" . $_Row['RootName'] . "</a>";
            $parentmenu = $emp->GetParentMenu($_Row['RootCode']);

            if ($parentmenu[0] == Message::SuccessfullyFetch) {
                $_Menu .= "<li class='child'>";
                $_Menu .= "<h5 class='weight-500'>Level 2</h5>";
                $_Menu .= "<ul>";

                while ($_Row1 = mysqli_fetch_array($parentmenu[2])) {
                    $_Menu .= "<li>" . $_Row1['ParentName'] . "</li>";
                    $childmenu = $emp->GetChildMenu($_Row1['ParentCode']);
                    $_Menu .= "<li class='child'>";
                    $_Menu .= "<h5 class='weight-500'>Level 3</h5>";
                    $_Menu .= "<ul>";
                    if ($childmenu[0] == Message::SuccessfullyFetch) {
                        while ($_ChildRow = mysqli_fetch_array($childmenu[2])) {
                            $_Menu .= "
							<li><a href='" . $_ChildRow['FunctionURL'] . "'>" . $_ChildRow['FunctionName'] . "</a></li>
							
							";
                        }
                    }
                    $_Menu .= "</ul></li>";
                }
                $_Menu .= " </ul></li>";
            }
            $_Menu .= " </li></ul>";
        }
    }
    echo $_Menu;
    return $_Menu;
}




    