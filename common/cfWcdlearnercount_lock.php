<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsWcdlearnercount.php';

$response = array();
$emp = new clsWcdlearnercount();

if ($_action == "GETDATA") {
    //echo "Show";

    if ($_POST['batch'] != '') {

        $response = $emp->GetAllPref($_POST['course'], $_POST['batch'], $_POST['cat']);

        $_DataTable = "";
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        $_LoginUserRole = $_SESSION['User_UserRoll'];
        if ($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4') {
            echo "<th>District Name</th>";
        } else {
            echo "<th>ITGK Code</th>";
        }

        echo "<th>Eligible Applicant Count</th>";
        if ($_LoginUserRole == '17' && $_POST['cat'] == '0') {
            echo "<th>Reserved Applicants</th>";
            echo "<th>Vacant Seat Approval</th>";
        }

        echo "<th>Approved Applicant Count</th>";

        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        $_Total = 0;
        $_ATotal = 0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            if($_POST['cat'] ==0 && $_Row['pref'] > 4){
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                $_LoginUserRole = $_SESSION['User_UserRoll'];
                if ($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4') {
                    echo "<td>" . $_Row['District_Name'] . "</td>";
                    echo "<td>" . $_Row['pref'] . "</td>";
                    echo "<td>" . $_Row['Confirmed_Learner'] . "</td>";
                } else {
                    echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";
                    echo "<td><a href='frmWcdLearnerCountList.php?batch=" . $_POST['batch'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&category=" . $_POST['cat'] . "&mode=one' target='_blank'>"
                    . "" . $_Row['pref'] . "</a></td>";
                    if ($_LoginUserRole == '17' && $_POST['cat'] == '0') {
                        $stLink = $scLink = '';
                        if ($_Row['Wcd_Intake_Available_SC'] == -1) {
                            $scLink = 'SC Applicants Approved';
                        } elseif ($_Row['Wcd_Intake_Consumed'] > 0) {
                            $scRows = $emp->GetLearnerList('one', $_POST['batch'], $_Row['District_Code'], $_Row['Oasis_Admission_Final_Preference'], $_POST['cat'], 2);
                            if (mysqli_num_rows($scRows[2])) {
                                $scLink = "<a href='frmWcdReservedLearnerCountList.php?batch=" . $_POST['batch'] . "&category=" . $_POST['cat'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=one&cat=2' target='_blank'>Approve SC Applicants</a>";
                            } else {
                                $scLink = 'No SC Applicants Found';
                            }
                        }

                        if ($_Row['Wcd_Intake_Available_ST'] == -1) {
                            $stLink = 'ST Applicants Approved';
                        } elseif ($_Row['Wcd_Intake_Consumed'] > 0) {
                            $stRows = $emp->GetLearnerList('one', $_POST['batch'], $_Row['District_Code'], $_Row['Oasis_Admission_Final_Preference'], $_POST['cat'], 3);
                            if (mysqli_num_rows($stRows[2])) {
                                $stLink = "<a href='frmWcdReservedLearnerCountList.php?batch=" . $_POST['batch'] . "&category=" . $_POST['cat'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=one&cat=3' target='_blank'>Approve ST Applicants</a>";
                            } else {
                                $stLink = 'No ST Applicants Found';
                            }
                        }

                        $rervLink = ($_Row['Wcd_Intake_Consumed'] > 0) ? $scLink . " | " . $stLink : "";
                        echo "<td nowrap>$rervLink</td>";
                    }
                    if ($_LoginUserRole == '17' && $_POST['cat'] == '0') {
                        $wlLink = '';
                        if ($_Row['Confirmed_Learner'] < 9) {
                            if($_Row['apgen'] < 9 && $_Row['apgen'] > 0){
                                if(($_Row['psc'] > 0 && $_Row['Wcd_Intake_Available_SC'] != -1)){
                                      $wlLink = '';
                                }
                                else{
                                    if($_Row['pst'] > 0 && $_Row['Wcd_Intake_Available_ST'] != -1){
                                        $wlLink = '';
                                    }
                                    else {
                                        $wlLink = "<a href='frmWcdWLLearnerCountList.php?batch=" . $_POST['batch'] . "&category=" . $_POST['cat'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=three&cat=2' target='_blank'>Approve WL Applicants</a>";
                                        $wlRows = $emp->GetLearnerList('three', $_POST['batch'], $_Row['District_Code'], $_Row['Oasis_Admission_Final_Preference'], $_POST['cat'], 2);
                                        if (mysqli_num_rows($wlRows[2])) {

                                             $wlLink = "<a href='frmWcdWLLearnerCountList.php?batch=" . $_POST['batch'] . "&category=" . $_POST['cat'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=three&cat=2' target='_blank'>Approve WL Applicants</a>";
                                        }
                                    }
                                   
                                }
                                
                            }

                        } else {
                            $wlLink = 'No WL Applicants Found';
                        }

                        $rervLink = $wlLink;
                        echo "<td nowrap>$rervLink</td>";
                    }
                    if ($_Row['Confirmed_Learner'] > 0) {
                        echo "<td><a href='frmWcdLearnerCountList.php?batch=" . $_POST['batch'] . "&category=" . $_POST['cat'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=two' target='_blank'>"
                        . "" . $_Row['Confirmed_Learner'] . "</a></td>";
                    } else {
                        echo "<td>" . $_Row['Confirmed_Learner'] . "</td>";
                    }
                }

                echo "</tr>";
                $_Total = $_Total + $_Row['pref'];
                $_ATotal = $_ATotal + $_Row['Confirmed_Learner'];
                $_Count++;
            
            }
            else {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                $_LoginUserRole = $_SESSION['User_UserRoll'];
                if ($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4') {
                    echo "<td>" . $_Row['District_Name'] . "</td>";
                    echo "<td>" . $_Row['pref'] . "</td>";
                    echo "<td>" . $_Row['Confirmed_Learner'] . "</td>";
                } else {
                    echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";
                    echo "<td><a href='frmWcdLearnerCountList.php?batch=" . $_POST['batch'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&category=" . $_POST['cat'] . "&mode=one' target='_blank'>"
                    . "" . $_Row['pref'] . "</a></td>";
                    if ($_LoginUserRole == '17' && $_POST['cat'] == '0') {
                        $stLink = $scLink = '';
                        if ($_Row['Wcd_Intake_Available_SC'] == -1) {
                            $scLink = 'SC Applicants Approved';
                        } elseif ($_Row['Wcd_Intake_Consumed'] > 0) {
                            $scRows = $emp->GetLearnerList('one', $_POST['batch'], $_Row['District_Code'], $_Row['Oasis_Admission_Final_Preference'], $_POST['cat'], 2);
                            if (mysqli_num_rows($scRows[2])) {
                                $scLink = "<a href='frmWcdReservedLearnerCountList.php?batch=" . $_POST['batch'] . "&category=" . $_POST['cat'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=one&cat=2' target='_blank'>Approve SC Applicants</a>";
                            } else {
                                $scLink = 'No SC Applicants Found';
                            }
                        }

                        if ($_Row['Wcd_Intake_Available_ST'] == -1) {
                            $stLink = 'ST Applicants Approved';
                        } elseif ($_Row['Wcd_Intake_Consumed'] > 0) {
                            $stRows = $emp->GetLearnerList('one', $_POST['batch'], $_Row['District_Code'], $_Row['Oasis_Admission_Final_Preference'], $_POST['cat'], 3);
                            if (mysqli_num_rows($stRows[2])) {
                                $stLink = "<a href='frmWcdReservedLearnerCountList.php?batch=" . $_POST['batch'] . "&category=" . $_POST['cat'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=one&cat=3' target='_blank'>Approve ST Applicants</a>";
                            } else {
                                $stLink = 'No ST Applicants Found';
                            }
                        }

                        $rervLink = ($_Row['Wcd_Intake_Consumed'] > 0) ? $scLink . " | " . $stLink : "";
                        echo "<td nowrap>$rervLink</td>";
                    }
                    if ($_LoginUserRole == '17' && $_POST['cat'] == '0') {
                        $wlLink = '';
                        if ($_Row['Confirmed_Learner'] < 9) {
                            if($_Row['apgen'] < 9 && $_Row['apgen'] > 0){
                                if(($_Row['psc'] > 0 && $_Row['Wcd_Intake_Available_SC'] != -1)){
                                      $wlLink = '';
                                }
                                else{
                                    if($_Row['pst'] > 0 && $_Row['Wcd_Intake_Available_ST'] != -1){
                                        $wlLink = '';
                                    }
                                    else {
                                        $wlLink = "<a href='frmWcdWLLearnerCountList.php?batch=" . $_POST['batch'] . "&category=" . $_POST['cat'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=three&cat=2' target='_blank'>Approve WL Applicants</a>";
                                        $wlRows = $emp->GetLearnerList('three', $_POST['batch'], $_Row['District_Code'], $_Row['Oasis_Admission_Final_Preference'], $_POST['cat'], 2);
                                        if (mysqli_num_rows($wlRows[2])) {

                                             $wlLink = "<a href='frmWcdWLLearnerCountList.php?batch=" . $_POST['batch'] . "&category=" . $_POST['cat'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=three&cat=2' target='_blank'>Approve WL Applicants</a>";
                                        }
                                    }
                                   
                                }
                                
                            }

                        } else {
                            $wlLink = 'No WL Applicants Found';
                        }

                        $rervLink = $wlLink;
                        echo "<td nowrap>$rervLink</td>";
                    }
                    if ($_Row['Confirmed_Learner'] > 0) {
                        echo "<td><a href='frmWcdLearnerCountList.php?batch=" . $_POST['batch'] . "&category=" . $_POST['cat'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=two' target='_blank'>"
                        . "" . $_Row['Confirmed_Learner'] . "</a></td>";
                    } else {
                        echo "<td>" . $_Row['Confirmed_Learner'] . "</td>";
                    }
                }

                echo "</tr>";
                $_Total = $_Total + $_Row['pref'];
                $_ATotal = $_ATotal + $_Row['Confirmed_Learner'];
                $_Count++;
            }
        }
        echo "</tbody>";

        echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
        echo "<th >Total Count </th>";
        echo "<th>";
        echo "$_Total";
        echo "</th>";
        if ($_LoginUserRole == '17' && $_POST['cat'] == '0') {
            echo "<th >  </th>";
            echo "<th >  </th>";
        }
        echo "<th >";
        echo "$_ATotal";
        echo "</th>";
        echo "</tr>";
        echo "</tfoot>";

        echo "</table>";
        echo "</div>";
    } else {
        echo "1";
    }
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}


if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetLearnerList($_POST['mode'], $_POST['batch'], $_POST['districtcode'], $_POST['itgkcode'], $_POST['spcategory']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Applicant Id</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >Marital Status</th>";
    echo "<th >Violence Victim</th>";
    echo "<th >Age</th>";
    echo "<th >Female Category</th>";
    echo "<th >Caste Category</th>";
    echo "<th >Qualification</th>";
    echo "<th >School Type</th>";
    echo "<th >Min. Qual. PER.</th>";
    echo "<th >Highest PER.</th>";
    echo "<th >Mobile No.</th>";
    echo "<th >Details</th>";
    echo "<th >Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Oasis_Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Oasis_Admission_Fname']) . "</td>";
            if ($_Row['Oasis_Admission_MaritalStatus'] == 'Saperated' || $_Row['Oasis_Admission_MaritalStatus'] == 'Divorcee' ||
                    $_Row['Oasis_Admission_MaritalStatus'] == 'Widow') {
                $fileName = getFilePath($_Row['Oasis_Admission_meritalStatusProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_marital_status_proof');
                echo "<td> <input type='hidden' id='file_id_" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='viewimage btn-Info btn-xs'>
							" . $_Row['Oasis_Admission_MaritalStatus'] . "</button>
				</td>";
            } else {
                echo "<td>" . $_Row['Oasis_Admission_MaritalStatus'] . "</td>";
            }

            if ($_Row['Oasis_Admission_SocialCategory'] == '1') {
                $fileName = getFilePath($_Row['Oasis_Admission_violanceProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_victim_of_violence_proof');
                echo "<td> <input type='hidden' id='file_id_viol" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='viewimage btn-Info btn-xs'>Yes</button>
				</td>";
            } else {
                echo "<td>No</td>";
            }

            echo "<td>" . $_Row['Oasis_Admission_Age'] . "</td>";

            if ($_Row['Oasis_Admission_Subcategory'] == 'Saathin' || $_Row['Oasis_Admission_Subcategory'] == 'Aaganwadi') {
                $fileName = getFilePath($_Row['Oasis_Admission_subCategoryProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_sub_category_proof');
                echo "<td> <input type='hidden' id='file_id_sub" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='sub_category btn-Info btn-xs'>
							" . $_Row['Oasis_Admission_Subcategory'] . "</button>
				</td>";
            } else {
                echo "<td>" . $_Row['Oasis_Admission_Subcategory'] . "</td>";
            }


            if ($_Row['Category_Name'] == 'SC' || $_Row['Category_Name'] == 'ST') {
                $fileName = getFilePath($_Row['Oasis_Admission_CasteProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_caste_proof');
                echo "<td> <input type='hidden' id='file_id_cat" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='caste btn-Info btn-xs'>
							" . $_Row['Category_Name'] . "</button>
				</td>";
            } else {
                echo "<td>" . $_Row['Category_Name'] . "</td>";
            }

            echo "<td>" . $_Row['Qualification_Name'] . "</td>";

            //	echo "<td>" . $_Row['Oasis_Admission_MINPercentage'] . "</td>";
            if ($_Row['Oasis_Admission_MinSchoolType'] == '1') {
                echo "<td>Government School</td>";
            } else {
                echo "<td>Private / Other School</td>";
            }

            if ($_Row['Oasis_Admission_Course'] == '3') {
                echo "<td>" . $_Row['Oasis_Admission_MINPercentage'] . "</td>";
            } else {
                echo "<td>" . $_Row['Oasis_Admission_HSCPercentage'] . "</td>";
            }


            echo "<td>" . $_Row['Oasis_Admission_Percentage'] . "</td>";

            echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
            echo "<td> <a href='frmwcdlearnerdetails.php?code=" . $_Row['Oasis_Admission_LearnerCode'] . "&month=" . $_Row['Oasis_Admission_Batch'] . "&category=" . $_POST['spcategory'] . "&Mode=Reject' target='_Blank'>"
            . "<input type='button' name='Edit' id='Edit' class='btn btn-primary btn-xs' value='View Details' /></a>"
            . "</td>";

            if ($_POST['spcategory'] == '0') {
                $intake70Per = (empty($_Row['Wcd_Intake_Available_General'])) ? round($_Row['Wcd_Intake_Available'] * .7) : $_Row['Wcd_Intake_Available_General'];
                if ($_Count > $intake70Per && $_Row['Oasis_Admission_LearnerStatus'] == 'Pending') {
                    echo "<td>"
                    . "Waiting List"
                    . "</td>";
                } else if ($_Row['Oasis_Admission_LearnerStatus'] == 'Approved') {
                    echo "<td> Final Approved </td>";
                } else if ($_Row['Oasis_Admission_LearnerStatus'] == 'Rejected') {
                    echo "<td> Rejected </td>";
                } else {
                    echo "<td> <a href='frmwcdlearnerdetails.php?code=" . $_Row['Oasis_Admission_LearnerCode'] . "&category=" . $_POST['spcategory'] . "&month=" . $_Row['Oasis_Admission_Batch'] . "&Mode=Rejected'>"
                    . "<input type='button' name='Edit' id='Edit' class='btn btn-primary btn-xs' value='Reject'/></a>"
                    . "</td>";
                }
            } else {
                if ($_Row['Oasis_Admission_LearnerStatus'] == 'Approved') {
                    echo "<td> Final Approved </td>";
                } else if ($_Row['Oasis_Admission_LearnerStatus'] == 'Rejected') {
                    echo "<td> Rejected </td>";
                } else {
                    echo "<td> <a href='frmwcdlearnerdetails.php?code=" . $_Row['Oasis_Admission_LearnerCode'] . "&category=" . $_POST['spcategory'] . "&month=" . $_Row['Oasis_Admission_Batch'] . "&Mode=Rejected'>"
                    . "<input type='button' name='Edit' id='Edit' class='btn btn-primary btn-xs' value='Reject'/></a>"
                    . "</td>";
                }
            }


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETREVLEARNERLIST") {

    $response = $emp->GetLearnerList($_POST['mode'], $_POST['batch'], $_POST['districtcode'], $_POST['itgkcode'], $_POST['spcategory'], $_POST['category']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Applicant Id</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >Marital Status</th>";
    echo "<th >Violence Victim</th>";
    echo "<th >Age</th>";
    echo "<th >Female Category</th>";
    echo "<th >Caste Category</th>";
    echo "<th >Qualification</th>";
    echo "<th >School Type</th>";
    echo "<th >Min. Qual. PER</th>";
    echo "<th >Highest PER.</th>";
    echo "<th >Mobile No.</th>";
    echo "<th >Details</th>";
    echo "<th >Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $showSC = $showST = 0;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            if ($_Row['Oasis_Admission_Category'] != 2 && $_Row['Oasis_Admission_Category'] != 3) {
                continue;
            }
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Oasis_Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Oasis_Admission_Fname']) . "</td>";
            if ($_Row['Oasis_Admission_MaritalStatus'] == 'Saperated' || $_Row['Oasis_Admission_MaritalStatus'] == 'Divorcee' ||
                    $_Row['Oasis_Admission_MaritalStatus'] == 'Widow') {
                $fileName = getFilePath($_Row['Oasis_Admission_meritalStatusProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_marital_status_proof');
                echo "<td> <input type='hidden' id='file_id_" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='viewimage btn-Info btn-xs'>
							" . $_Row['Oasis_Admission_MaritalStatus'] . "</button>
				</td>";
            } else {
                echo "<td>" . $_Row['Oasis_Admission_MaritalStatus'] . "</td>";
            }

            if ($_Row['Oasis_Admission_SocialCategory'] == '1') {
                $fileName = getFilePath($_Row['Oasis_Admission_violanceProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_victim_of_violence_proof');
                echo "<td> <input type='hidden' id='file_id_viol" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='viewimage btn-Info btn-xs'>Yes</button>
				</td>";
            } else {
                echo "<td>No</td>";
            }

            echo "<td>" . $_Row['Oasis_Admission_Age'] . "</td>";


            if ($_Row['Oasis_Admission_Subcategory'] == 'Saathin' || $_Row['Oasis_Admission_Subcategory'] == 'Aaganwadi') {
                $fileName = getFilePath($_Row['Oasis_Admission_subCategoryProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_sub_category_proof');
                echo "<td> <input type='hidden' id='file_id_sub" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='sub_category btn-Info btn-xs'>
							" . $_Row['Oasis_Admission_Subcategory'] . "</button>
				</td>";
            } else {
                echo "<td>" . $_Row['Oasis_Admission_Subcategory'] . "</td>";
            }


            if ($_Row['Category_Name'] == 'SC' || $_Row['Category_Name'] == 'ST') {
                $fileName = getFilePath($_Row['Oasis_Admission_CasteProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_caste_proof');
                echo "<td> <input type='hidden' id='file_id_cat" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='caste btn-Info btn-xs'>
							" . $_Row['Category_Name'] . "</button>
				</td>";
            } else {
                echo "<td>" . $_Row['Category_Name'] . "</td>";
            }

            echo "<td>" . $_Row['Qualification_Name'] . "</td>";
            if ($_Row['Oasis_Admission_MinSchoolType'] == '1') {
                echo "<td>Government School</td>";
            } else {
                echo "<td>Private / Other School</td>";
            }
            //		echo "<td>" . $_Row['Oasis_Admission_MINPercentage'] . "</td>";

            if ($_Row['Oasis_Admission_Course'] == '3') {
                echo "<td>" . $_Row['Oasis_Admission_MINPercentage'] . "</td>";
            } else {
                echo "<td>" . $_Row['Oasis_Admission_HSCPercentage'] . "</td>";
            }
            echo "<td>" . $_Row['Oasis_Admission_Percentage'] . "</td>";

            echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
            echo "<td> <a href='frmwcdlearnerdetails.php?code=" . $_Row['Oasis_Admission_LearnerCode'] . "&month=" . $_Row['Oasis_Admission_Batch'] . "&category=" . $_POST['spcategory'] . "&Mode=Reject'>"
            . "<input type='button' name='Edit' id='Edit' class='btn btn-primary btn-xs' value='View Details' /></a>"
            . "</td>";
            $intakeRev = $_Row['Wcd_Intake_Available_SC'] + $_Row['Wcd_Intake_Available_ST'];
            if ($_Row['Oasis_Admission_LearnerStatus'] == 'Approved') {
                echo "<td> Final Approved </td>";
            } elseif (($_Row['Category_Name'] == 'SC' && $showSC >= $_Row['Wcd_Intake_Available_SC']) || ($_Row['Category_Name'] == 'ST' && $showST >= $_Row['Wcd_Intake_Available_ST'])) {
                echo "<td>"
                . "Waiting List"
                . "</td>";
            } elseif ($_Row['Wcd_Intake_Consumed'] > 0 && $_Row['Wcd_Intake_Consumed'] >= $_Row['Wcd_Intake_Available_General']) {
                if ($_Row['Category_Name'] == 'SC')
                    $showSC++;
                if ($_Row['Category_Name'] == 'ST')
                    $showST++;
                echo "<td> <a href='frmwcdlearnerdetails.php?code=" . $_Row['Oasis_Admission_LearnerCode'] . "&month=" . $_Row['Oasis_Admission_Batch'] . "&category=" . $_POST['spcategory'] . "&Mode=Rejected&Type=Reserved'>"
                . "<input type='button' name='Edit' id='Edit' class='btn btn-primary btn-xs' value='Reject'/></a>"
                . "<input type='hidden' name='shortlisted[]' value='" . $_Row['Oasis_Admission_Code'] . "'/></td>";
            } else {
                echo "<td></td>";
            }

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "<input type='hidden' value='" . $_POST['category'] . "' name='category'>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
if ($_action == "GETWLLEARNERLIST") {


    $response = $emp->GetLearnerList($_POST['mode'], $_POST['batch'], $_POST['districtcode'], $_POST['itgkcode'], $_POST['spcategory']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Applicant Id</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >Marital Status</th>";
    echo "<th >Violence Victim</th>";
    echo "<th >Age</th>";
    echo "<th >Female Category</th>";
    echo "<th >Caste Category</th>";
    echo "<th >Qualification</th>";
    echo "<th >School Type</th>";
    echo "<th >Min. Qual. PER.</th>";
    echo "<th >Highest PER.</th>";
    echo "<th >Mobile No.</th>";
    echo "<th >Details</th>";
    echo "<th >Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Oasis_Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Oasis_Admission_Fname']) . "</td>";
            if ($_Row['Oasis_Admission_MaritalStatus'] == 'Saperated' || $_Row['Oasis_Admission_MaritalStatus'] == 'Divorcee' ||
                    $_Row['Oasis_Admission_MaritalStatus'] == 'Widow') {
                $fileName = getFilePath($_Row['Oasis_Admission_meritalStatusProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_marital_status_proof');
                echo "<td> <input type='hidden' id='file_id_" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='viewimage btn-Info btn-xs'>
							" . $_Row['Oasis_Admission_MaritalStatus'] . "</button>
				</td>";
            } else {
                echo "<td>" . $_Row['Oasis_Admission_MaritalStatus'] . "</td>";
            }

            if ($_Row['Oasis_Admission_SocialCategory'] == '1') {
                $fileName = getFilePath($_Row['Oasis_Admission_violanceProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_victim_of_violence_proof');
                echo "<td> <input type='hidden' id='file_id_viol" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='viewimage btn-Info btn-xs'>Yes</button>
				</td>";
            } else {
                echo "<td>No</td>";
            }

            echo "<td>" . $_Row['Oasis_Admission_Age'] . "</td>";

            if ($_Row['Oasis_Admission_Subcategory'] == 'Saathin' || $_Row['Oasis_Admission_Subcategory'] == 'Aaganwadi') {
                $fileName = getFilePath($_Row['Oasis_Admission_subCategoryProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_sub_category_proof');
                echo "<td> <input type='hidden' id='file_id_sub" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='sub_category btn-Info btn-xs'>
							" . $_Row['Oasis_Admission_Subcategory'] . "</button>
				</td>";
            } else {
                echo "<td>" . $_Row['Oasis_Admission_Subcategory'] . "</td>";
            }


            if ($_Row['Category_Name'] == 'SC' || $_Row['Category_Name'] == 'ST') {
                $fileName = getFilePath($_Row['Oasis_Admission_CasteProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_caste_proof');
                echo "<td> <input type='hidden' id='file_id_cat" . $_Row['Oasis_Admission_LearnerCode'] . "' 
						value='" . $fileName . "'> 
						<button type='button' id='" . $_Row['Oasis_Admission_LearnerCode'] . "' class='caste btn-Info btn-xs'>
							" . $_Row['Category_Name'] . "</button>
				</td>";
            } else {
                echo "<td>" . $_Row['Category_Name'] . "</td>";
            }

            echo "<td>" . $_Row['Qualification_Name'] . "</td>";
            if ($_Row['Oasis_Admission_MinSchoolType'] == '1') {
                echo "<td>Government School</td>";
            } else {
                echo "<td>Private / Other School</td>";
            }

            //	echo "<td>" . $_Row['Oasis_Admission_MINPercentage'] . "</td>";


            if ($_Row['Oasis_Admission_Course'] == '3') {
                echo "<td>" . $_Row['Oasis_Admission_MINPercentage'] . "</td>";
            } else {
                echo "<td>" . $_Row['Oasis_Admission_HSCPercentage'] . "</td>";
            }

            echo "<td>" . $_Row['Oasis_Admission_Percentage'] . "</td>";

            echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
            echo "<td> <a href='frmwcdlearnerdetails.php?code=" . $_Row['Oasis_Admission_LearnerCode'] . "&month=" . $_Row['Oasis_Admission_Batch'] . "&category=" . $_POST['spcategory'] . "&Mode=Reject' target='_Blank'>"
            . "<input type='button' name='Edit' id='Edit' class='btn btn-primary btn-xs' value='View Details' /></a>"
            . "</td>";

            if ($_POST['spcategory'] == '0') {
// $intake70Per = (empty($_Row['Wcd_Intake_Available_General'])) ? round($_Row['Wcd_Intake_Available'] * .7) : $_Row['Wcd_Intake_Available_General'];
                if ($_Count > ($_Row['Wcd_Intake_Available'] - $_Row['TotalConsumed']) && $_Row['Oasis_Admission_LearnerStatus'] == 'Pending') {
//                if ($_Row['TotalConsumed'] < $_Row['Wcd_Intake_Available']) {
                    echo "<td>"
                    . "Waiting List"
                    . "</td>";
                } else {
                    echo "<td> <a href='frmwcdlearnerdetails.php?code=" . $_Row['Oasis_Admission_LearnerCode'] . "&category=" . $_POST['spcategory'] . "&month=" . $_Row['Oasis_Admission_Batch'] . "&Mode=Rejected&Redir=WL'>"
                    . "<input type='button' name='Edit' id='Edit' class='btn btn-primary btn-xs' value='Reject'/></a>"
                    . "</td>";
                }
            }
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
/** if ($_action == "EDIT") {
  $response = $emp->GetDatabyCode($_POST['batch'], $_POST['lcode']);
  $_DataTable = array();
  $_i = 0;
  while ($_Row = mysqli_fetch_array($response[2])) {
  $_DataTable[$_i] = array("LearnerCode" => $_Row['Oasis_Admission_LearnerCode'],
  "lname" => $_Row['Oasis_Admission_Name'],
  "fname" => $_Row['Oasis_Admission_Fname'],
  "dob" => $_Row['Oasis_Admission_DOB'],
  "itgk" => $_Row['Oasis_Admission_Final_Preference'],
  "district" => $_Row['Oasis_Admission_District'],
  "minper" => $_Row['Oasis_Admission_MINPercentage'],
  "highper" => $_Row['Oasis_Admission_Percentage'],
  "mstatus" => $_Row['Oasis_Admission_MaritalStatus'],
  "categoryname" => $_Row['Category_Name'],
  "category" => $_Row['Oasis_Admission_Category'],
  "mobile" => $_Row['Oasis_Admission_Mobile'],
  "qualification" => $_Row['Qualification_Name'],
  "caste" => $_Row['Oasis_Admission_Caste'],
  "address" => $_Row['Oasis_Admission_Address'],
  "photo" => getFilePath($_Row['Oasis_Admission_Photo'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_photo'),
  "sign" => getFilePath($_Row['Oasis_Admission_Sign'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_sign'),
  "mincert" => getFilePath($_Row['Oasis_Admission_MinCertificate'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_minimum_certificate'),
  "highcert" => getFilePath($_Row['Oasis_Admission_HighCertificate'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_highest_certificate'),
  "ageproof" => getFilePath($_Row['Oasis_Admission_AgeProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_age_proof'),
  "casteproof" => getFilePath($_Row['Oasis_Admission_CasteProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_caste_proof'));
  $_i = $_i + 1;

  }

  echo json_encode($_DataTable);
  }

 * */
if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_POST['batch'], $_POST['lcode']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("LearnerCode" => $_Row['Oasis_Admission_LearnerCode'],
            "lname" => $_Row['Oasis_Admission_Name'],
            "fname" => $_Row['Oasis_Admission_Fname'],
            "dob" => $_Row['Oasis_Admission_DOB'],
            "itgk" => $_Row['Oasis_Admission_Final_Preference'],
            "district" => $_Row['Oasis_Admission_District'],
            "minper" => $_Row['Oasis_Admission_MINPercentage'],
            "hscper" => $_Row['Oasis_Admission_HSCPercentage'],
            "highper" => $_Row['Oasis_Admission_Percentage'],
            "mstatus" => $_Row['Oasis_Admission_MaritalStatus'],
            "categoryname" => $_Row['Category_Name'],
            "category" => $_Row['Oasis_Admission_Category'],
            "mobile" => $_Row['Oasis_Admission_Mobile'],
            "qualification" => $_Row['Qualification_Name'],
            "caste" => $_Row['Oasis_Admission_Caste'],
            "address" => $_Row['Oasis_Admission_Address'],
            "pid" => $_Row['Oasis_Admission_PID'],
            "course" => $_Row['Oasis_Admission_Course'],
            "photo" => getFilePath($_Row['Oasis_Admission_Photo'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_photo'),
            "sign" => getFilePath($_Row['Oasis_Admission_Sign'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_sign'),
            "mincert" => getFilePath($_Row['Oasis_Admission_MinCertificate'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_minimum_certificate'),
            "highcert" => getFilePath($_Row['Oasis_Admission_HighCertificate'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_highest_certificate'),
            "hsccert" => getFilePath($_Row['oasis_Admission_HSC_certificate'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_HSC_certificate'),
            "ageproof" => getFilePath($_Row['Oasis_Admission_AgeProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_age_proof'),
            "aadharproof" => getFilePath($_Row['Oasis_Admission_idProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_identity_proof'),
            "casteproof" => getFilePath($_Row['Oasis_Admission_CasteProof'], $_Row['Oasis_Admission_LearnerCode'], 'oasis_caste_proof'));
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "UpdateRejectingLearner") {
    $_LCode = $_POST["lcode"];
    $_ProcessStatus = $_POST["status"];
    $_Remark = $_POST["remark"];

    $response = $emp->UpdateWcdRejected($_LCode, $_ProcessStatus, $_Remark);
    echo $response[0];
}

if ($_action == "UpdateApproveLearner") {
    $_Batch = $_POST["batch"];
    $_ITGK = $_POST["itgkcode"];
    $_SpCategory = $_POST["spcategory"];

    $response = $emp->UpdateWcdAppoved($_Batch, $_ITGK, $_SpCategory);
    echo $response[0];
}

if ($_action == "UpdateApproveReserveLearner") {
    $response = $emp->UpdateWcdReservedAppoved($_POST);
    echo $response[0];
}
if ($_action == "UpdateApproveLearnerWL") {
    $_Batch = $_POST["batch"];
    $_ITGK = $_POST["itgkcode"];
    $_SpCategory = $_POST["spcategory"];

    $response = $emp->UpdateWcdAppovedWL($_Batch, $_ITGK, $_SpCategory);
    echo $response[0];
}

if ($_action == "resetMerit" || $_action == "setEligibility" || $_action == "setFinalPreference" || $_action == "generateMerit") {
    $batchId = $_POST["batch"];

    echo $emp->processWCDMerit($batchId, $_action);
}

if ($_action == "checkBeforeGenerateMerit") {
    echo '1';
}

function getFilePath($filePath, $learnerCode, $type) {
    $fileName = '';
    switch ($type) {
        case 'oasis_photo' : $dir = 'oasis_photo';
            $name = '_photo';
            break;
        case 'oasis_sign' : $dir = 'oasis_sign';
            $name = '_sign';
            break;
        case 'oasis_age_proof' : $dir = 'oasis_age_proof';
            $name = '_ageproof';
            break;

        case 'oasis_identity_proof' : $dir = 'oasis_identity_proof';
            $name = '_identityproof';
            break;
        case 'oasis_minimum_certificate' : $dir = 'oasis_minimum_certificate';
            $name = '_mincertificate';
            break;
        case 'oasis_caste_proof' : $dir = 'oasis_caste_proof';
            $name = '_casteproof';
            break;
        case 'oasis_sub_category_proof' : $dir = 'oasis_sub_category_proof';
            $name = '_subcategoryproof';
            break;
        case 'oasis_highest_certificate' : $dir = 'oasis_highest_certificate';
            $name = '_highcertificate';
            break;
        case 'oasis_physical_disability_proof' : $dir = 'oasis_physical_disability_proof';
            $name = '_physicaldisabilityproof';
            break;
        case 'oasis_victim_of_violence_proof' : $dir = 'oasis_victim_of_violence_proof';
            $name = '_victimofviolenceproof';
            break;
        case 'oasis_marital_status_proof' : $dir = 'oasis_marital_status_proof';
            $name = '_maritalstatusproof';
            break;
    }
    if (!empty($filePath) && $filePath != 'NA') {
        $filePath = (!file_exists($filePath)) ? '../upload/' . $dir . '/' . $learnerCode . $name . '.jpg' : $filePath;
        $fileInfo = pathinfo($filePath);
        $fileName = $fileInfo['basename'];
    } else
        $fileInfo = 'NA';

    return $fileName;
}

if ($_action == "FillCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}