<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsreexamdatacount.php';

$response = array();
$emp = new clsreexamdatacount();

if ($_action == "GETDATA") {
//print_r($_POST);
    $response = $emp->GetDataAll($_POST['examevent']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";    
    echo "<th>Total ReExam Learner Count</th>";
    echo "<th>Total  Re Exam Confirmed Learner Count</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;
	$_PTotal = 0;
   
        while ($_Row = mysqli_fetch_array($response[2])) {
			//print_r($_Row);
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";       
            echo "<td>" . $_Row['itgkcode'] . "</td>";
            echo "<td>" . $_Row['Total_Learner'] . "</td>";
			
            echo "<td>" . $_Row['Confirmed_Learner'] . "</td>";
            $_Count++;
			 $_Total = $_Total + $_Row['Total_Learner'];
			 $_PTotal = $_PTotal + $_Row['Confirmed_Learner'];
        }

        echo "</tbody>";    
		
///$_Count++;
	 echo "<tfoot>";
        echo "<tr>";
		 echo "<th >  </th>";
        echo "<th > Grand Total </th>";
		echo "<th >";
		 echo "$_Total";
		 echo "</th>";
		 echo "<th >";
		 echo "$_PTotal";
		 echo "</th>";
		 
       
        echo "</tr>";
        echo "</tfoot>";		
        echo "</table>";
        echo "</div>";
   
}

