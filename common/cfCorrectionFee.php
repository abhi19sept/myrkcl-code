<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsCorrectionFee.php';

$response = array();
$emp = new clsCorrectionFee();

if ($_action == "ADD") {
   if (empty($_POST["amounts"]) || !isset($_POST["codes"])) {
        echo "0";
    } else {
        echo $emp->AddCorrectionPayTran($_POST);
    }
}

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll($_POST['application'], $_POST['correctioncode'], $_POST['paymode']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='20%'>Correction Id</th>";
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";
    echo "<th style='20%'>Father Name</th>";
    echo "<th style='20%'>Marks</th>";
    echo "<th style='5%'>Email</th>";
    echo "<th style='5%'>Mobile</th>";
    echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	
	if($response[0]=='Success'){
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['cid'] . "</td>";
        echo "<td>" . $_Row['lcode'] . "</td>";
        echo "<td>" . $_Row['cfname'] . "</td>";
        echo "<td>" . $_Row['cfaname'] . "</td>";
        echo "<td>" . $_Row['totalmarks'] . "</td>";
        echo "<td>" . $_Row['emailid'] . "</td>";
        echo "<td>" . $_Row['mobile'] . "</td>";
		
		if($_Row['Correction_Payment_Status'] == '0')
		{
			echo "<td><input type='checkbox' class='chk' id='chk" .  $_Row['cid'] . "' name='codes[]' value='" . $_Row['cid']."'></input></td>";
		}
		
        echo "</tr>";
        $_Count++;
    }
   }
	else{
		//echo "invaliddata";
	}
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}
if ($_action == "FILL") {
    $response = $emp->GetCorrectionName();
    echo "<option value='' selected='selected'>Select Name</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['docid'] . ">" . $_Row['applicationfor'] . "</option>";
    }
}

if ($_action == "Fee") {
   // print_r($_POST);
    $response = $emp->GetCorrectionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['RKCL_Share'];
}

if ($_action == "GetApplicationName") {
   // print_r($_POST);
    $response = $emp->GetApplicationName($_POST['for']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['applicationfor'];
}


if ($_action == "Update") {
    //print_r($_POST);
    if (isset($_POST["txtCenterCode"])) {
        $_Code = filter_var($_POST["txtCenterCode"], FILTER_SANITIZE_STRING);
        $_PayTypeCode = filter_var($_POST["productinfo"], FILTER_SANITIZE_STRING);
        //$_PayType = filter_var($_POST["productinfocode"], FILTER_SANITIZE_STRING);
        $_TranRefNo = filter_var($_POST["txtGenerateId"], FILTER_SANITIZE_STRING);

        $_firstname = filter_var($_POST["firstname"], FILTER_SANITIZE_STRING);
        $_phone = filter_var($_POST["phone"], FILTER_SANITIZE_STRING);
        $_email = filter_var($_POST["email"], FILTER_SANITIZE_STRING);

        $_amount = filter_var($_POST["amount"], FILTER_SANITIZE_STRING);
        $_ddno = filter_var($_POST["ddno"], FILTER_SANITIZE_STRING);
        $_dddate = filter_var($_POST["dddate"], FILTER_SANITIZE_STRING);

        $_txtMicrNo = filter_var($_POST["txtMicrNo"], FILTER_SANITIZE_STRING);
        $_ddlBankDistrict = filter_var($_POST["ddlBankDistrict"], FILTER_SANITIZE_STRING);
        $_ddlBankName = filter_var($_POST["ddlBankName"], FILTER_SANITIZE_STRING);
        $_txtBranchName = filter_var($_POST["txtBranchName"], FILTER_SANITIZE_STRING);

        $response = $emp->Update($_Code, $_PayTypeCode, $_TranRefNo, $_firstname, $_phone, $_email, $_amount, $_ddno, $_dddate, $_txtMicrNo, $_ddlBankDistrict, $_ddlBankName, $_txtBranchName);
        echo $response[0];
    }
}

if ($_action == "GETCENTERCODE") {  
    $response = $emp->GetCenterCode($_POST['code']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Correction_TranRefNo'] . ">" . $_Row['Correction_ITGK_Code'] . "</option>";
    }
}


if ($_action == "GETCorrectionData") {
    $response = $emp->GETCorrectionData($_POST['refno']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";    
    echo "<th>DD No.</th>";
    echo "<th >DD Date</th>";  
    echo "<th >DD Amount</th>"; 
    echo "<th >DD Image</th>"; 
    echo "<th >Action</th>"; 	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['dd_no'] . "</td>";          
            echo "<td>" . $_Row['dd_date'] . "</td>";
			echo "<td>" . $_Row['dd_amount'] . "</td>";
			echo "<td>".'<a href="upload/correction_dd_payment/'.$_Row['dd_Transaction_Txtid'].'_ddpayment.png'.'" target="_blank">' . '<img alt="No Image Found" width="50" height="35" src="upload/correction_dd_payment/'.$_Row['dd_Transaction_Txtid'].'_ddpayment.png'.'"/>' . "</a></td>";
			//echo "<td><a href='upload/admission_dd_payment/".$_Row['dd_Transaction_Txtid']."_ddpayment.png'>". "</a></td>";
            echo "<td><input type='checkbox' id=chk" . $_Row['dd_Transaction_Txtid'].
			" name=chk" . $_Row['dd_Transaction_Txtid']."> </input>"                
                . "</td>";
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
		echo "</div>";
    } else {
        echo "No Record Found";
    }
}


if ($_action == "UPDATEDDSTATUS") {           
	$_UserPermissionArray=array();
    $_AdmissionCode = array();
	$_i = 0;
    $_Count=0;
    $l="";
        foreach ($_POST as $key => $value)
            {
               if(substr($key, 0,3)=='chk')
               {                				  
				   $l .= substr($key, 3) . ",";				 
               }			   
				$_AdmissionCode = rtrim($l, ",");											
			}
                $_SESSION['LearnerCorrectionCodes'] = $_AdmissionCode;                 				
                $response=$emp->UpdateDDPaymentStatus($_AdmissionCode);               		
				echo $response[0];  	   
	}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
	//print_r($response);
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
	//values
	$key = "X2ZPKM";
	$salt = "8IaBELXB";
	$command1 = "check_isDomestic";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	//$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
	//$var3 = "500";//  Amount to be used in case of refund

	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);
	//echo "<pre>"; print_r($response); echo "</pre>"; 
	$_DataTable = array();
    $_i = 0;
		$_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
				echo json_encode($_DataTable);
		//echo $cardType = $response['cardType'];
		//echo $cardCategory = $response['cardCategory'];
 }

 if ($_action == "FinalValidateCard") {
	//values
	$key = "X2ZPKM";
	$salt = "8IaBELXB";
	$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	//$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
	//$var3 = "500";//  Amount to be used in case of refund

	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);
	//echo "<pre>"; print_r($response); echo "</pre>"; 
	if($response == 'Invalid'){
		echo "1";
	}
	else if($response == 'valid'){
		echo "2";
	}
 }
 
function curlCall($wsUrl, $qs, $true){
   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
		if (curl_errno($c)) {
		  $c_error = curl_error($c);
		  if (empty($c_error)) {
			  $c_error = 'Some server error';
			}
			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);
		return $arr;
}

?>
