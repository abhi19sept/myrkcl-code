<?php

include './commonFunction.php';
require 'BAL/clsSpaddressupdate.php';

$response = array();
$emp = new clsSpaddressupdate();

if ($_action == "Getaddress") {

    $response = $emp->Getaddress();
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Rsp Code</th>";
    echo "<th>Organization Name</th>";
    echo "<th>Organization Address</th>";
    echo "<th>State Name</th>";
    echo "<th>District Name</th>";
    echo "<th>Tehsil Name</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['User_LoginId'] . "</td>";
        echo "<td>" . $_Row['Organization_Name'] . "</td>";
        echo "<td>" . $_Row['Org_formatted_address'] . "</td>";
        echo "<td>" . $_Row['State_Name'] . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
        echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
        echo "<td><button id='" . $_Row['Organization_Code'] . "' class='fun_update_details'>Update</button></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
if ($_action == "EDIT") 
{
    $editid = $_POST['editid'];
    $response = $emp->ShowDetailForEdit($editid);
    $_DataTable = array();
    $_i = 0;
    if($response[0]=='Success'){
        while ($row = mysqli_fetch_array($response[2])) {
         $_Datatable[$_i] = array("Organization_Code" => $row['Organization_Code'],
             "Organization_State" => $row['Organization_State'],
             "District_Code" => $row['District_Code'],
             "Tehsil_Code" => $row['Tehsil_Code'],
             "Org_formatted_address" => $row['Org_formatted_address']
            );
         $_i = $_i + 1;
        }
    }
    echo json_encode($_Datatable);
}

if ($_action == "FILLSTATE") {
    $response = $emp->GetAllState();
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['State_Code'] . ">" . $_Row['State_Name'] . "</option>";
    }
}
if ($_action == "FILLDISTRICT") {
    if(isset($_POST["values"])){
        $state = $_POST["values"];
        $dist = $_POST["dist"];
    }else{
        $state = "";
    }
    $response = $emp->GetAllDistrict($state);
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        if($dist == $_Row['District_Code']){
            echo "<option value=" . $_Row['District_Code'] . " selected='selected'>" . $_Row['District_Name'] . "</option>";
        }else{
            echo "<option value=" . $_Row['District_Code'] . ">" . $_Row['District_Name'] . "</option>";
        }
        
    }
}
if ($_action == "FILLTEHSHIL") {
    if(isset($_POST["values"])){
        $district = $_POST["values"];
        $tehsil = $_POST["tehsil"];
    }else{
        $district = "";
    }
    $response = $emp->GetAllTehshil($district);
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        if($tehsil == $_Row['Tehsil_Code']){
            echo "<option value=" . $_Row['Tehsil_Code'] . " selected='selected'>" . $_Row['Tehsil_Name'] . "</option>";
        }else{
            echo "<option value=" . $_Row['Tehsil_Code'] . ">" . $_Row['Tehsil_Name'] . "</option>";
        }
        
    }
}
if ($_action == "UPDATE") {
    
     if(isset($_POST["txtaddress"]) && !empty($_POST["txtaddress"]) 
         && isset($_POST["ddlState"]) && !empty($_POST["ddlState"])
         && isset($_POST["ddlDistrict"]) && !empty($_POST["ddlDistrict"])
         && isset($_POST["ddlTehsil"]) && !empty($_POST["ddlTehsil"])
		 && isset($_POST["code"]) && !empty($_POST["code"])
       ){
        $_address = $_POST["txtaddress"];
		$_State = $_POST["ddlState"];
		$_district = $_POST["ddlDistrict"];
		$_tehsil = $_POST["ddlTehsil"];
        $_code = $_POST["code"];
        $response = $emp->updateaddress($_code,$_State,$_address,$_district,$_tehsil);
        echo $response[0];
    }else{
        echo "empty";
    }
    
}
