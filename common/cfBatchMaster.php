<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsBatchMaster.php';

$response = array();
$emp = new clsBatchMaster();

if ($_action == "ADD") {

    if (isset($_POST["name"])) {
        $_BatchName = $_POST["name"];
        $_Course = $_POST["course"];
        $_Status = $_POST["status"];
        $_Sdate = $_POST["startdate"];
        $_Edate = $_POST["enddate"];
        $_Install = $_POST["installmode"];
        $_Certif = $_POST["certcode"];
        $_Finance = $_POST["finance"];
        $_Year = $_POST["year"];
        $_Fees = $_POST["cfees"];
        $_Share = $_POST["share"];
        $_VMOUShare = $_POST["vmoushare"];

        $response = $emp->Add($_BatchName, $_Course, $_Status, $_Sdate, $_Edate, $_Install, $_Certif, $_Finance, $_Year, $_Fees, $_Share, $_VMOUShare);
        if ($response[0] != Message::DuplicateRecord && $_Course == "1") {
            $response = $emp->GetMaxBatchCode($_Course);
            $_BatchCode = mysqli_fetch_array($response[2]);

            $response1 = $emp->GetMaxIntakeBatch($_Course);
            $_IntakeCode = mysqli_fetch_array($response1[2]);

            $response = $emp->Addintake($_Course, $_BatchCode['Code'], $_IntakeCode['IntakeCode']);
        }
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    //print_r($_POST);
    if (isset($_POST["ddlCourse"])) {

        $_BatchName = $_POST["ddlBatch"];
        $_Course = $_POST["ddlCourse"];
        $_Sdate = $_POST["txtstartdate"];
        $_Edate = $_POST["txtenddate"];
        $_Fees = $_POST["txtCourseFee"];
        $_Share = $_POST["txtShare"];
        $response = $emp->Update($_BatchName, $_Course, $_Sdate, $_Edate, $_Fees, $_Share);

        echo $response[0];
    }
}
if ($_action == "FILL") {
    $response = $emp->Batch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FILLALLBatch") {
    $response = $emp->ALLBatch();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "SHOW") {

    //print_r($_POST);
    $response = $emp->GetBatchDetail($_POST['batch'], $_POST['course']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Course Name</th>";
    echo "<th style='45%'>Batch Name</th>";
    echo "<th style='40%'>End Date</th>";
    echo "<th style='10%'>Course Fee</th>";
    echo "<th style='10%'>RKCL Share</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Course_Name'] . "</td>";
        echo "<td>" . $_Row['Batch_Name'] . "</td>";
        echo "<td>" . $_Row['Batch_EndDate'] . "</td>";
        echo "<td>" . $_Row['Course_Fee'] . "</td>";
        echo "<td>" . $_Row['RKCL_Share'] . "</td>";
        echo "<td><a href='frmmodifybatch.php?code=" . $_Row['Batch_Code'] . "&Mode=Edit'>"
        . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmmodifybatch.php?code=" . $_Row['Batch_Code'] . "&Mode=Delete'>"
        . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "EDIT") {


    $response = $emp->GetBatchDetail($_POST['batch'], $_POST['course']);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("Batch_Code" => $_Row['Batch_Code'],
            "Batch_Name" => $_Row['Batch_Name'],
            "Batch_StartDate" => $_Row['Batch_StartDate'],
            "Batch_EndDate" => $_Row['Batch_EndDate'],
            "Batch_Status" => $_Row['Batch_Status'],
            "Course_Name" => $_Row['Course_Name'],
            "Admission_Installation_Mode" => $_Row['Admission_Installation_Mode'],
            "Certificate_Code" => $_Row['Certificate_Code'],
            "Financial_Year" => $_Row['Financial_Year'],
            "Year" => $_Row['Year'],
            "Course_Fee" => $_Row['Course_Fee'],
            "RKCL_Share" => $_Row['RKCL_Share']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}

if ($_action == "FILLAdmissionBatchcode") {
    $response = $emp->FILLAdmissionBatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FILLBatch") {
    $response = $emp->ModifyAdmissionBatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Admission_Batch'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}
if ($_action == "FILLEventBatch") {
    $response = $emp->FILLEventBatch($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Batch'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FILLModifyEventBatch") {
    $response = $emp->FILLModifyEventBatch($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Batch'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FillAdmissionBatch") {
    $response = $emp->GetAdmissionBatchName($_POST['values']);
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo $_Row['Batch_Name'];
    }
}

if ($_action == "FILLPhotoSignProcessEventBatch") {
    $response = $emp->FILLPhotoSignProcessEventBatch($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Batch'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FILLPhotoSignReuploadEventBatch") {
    $response = $emp->FILLPhotoSignReuploadEventBatch($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Batch'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FILLBatchCode") {
    $response = $emp->FILLEventBatch($_POST['values']);
    //echo "<option value=''>Select Courses</option>";
    $_Row = mysqli_fetch_array($response[2]);
    echo $_Row['Event_Batch'];
}

if ($_action == "FILLAdmissionBatchCourse") {
    $response = $emp->FILLAdmissionBatchCourse($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] ." (". $_Row['Course_Name'] .")</option>";
    }
}
if ($_action == "FILLModifyForAadhar") {
    $response = $emp->FILLModifyForAadhar($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Batch'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}
