<?php

/* 
 * author Yogendra

 */
include './commonFunction.php';
require 'BAL/clseditbankaccountdetails.php';

$response = array();
$emp = new clseditbankaccountdetails();



if ($_action == "UPDATE") {
	//print_r($_POST);
    if (isset($_POST["code"])) {
	 $_code = $_POST["code"];
        
		$_AccountName = $_POST["txtaccountName"];
	    $_AccountNumber = $_POST["txtaccountNumber"];
		
		
		$_ConfirmaccountNumber = $_POST["txtConfirmaccountNumber"];
		
	    $_AccountType = $_POST["rbtaccountType_saving"];
        $_IfscCode = $_POST["txtIfscCode"];
	    $_BankName = $_POST["ddlBankName"];
	    $_BranchName = $_POST["ddlBranch"];
	    $_MicrCode = $_POST["txtMicrcode"];
	    $_PanNo = $_POST["txtpanno"];
		$_PanName = $_POST["txtpanname"];
		$_IdProof = $_POST["ddlidproof"];
		$_Genid = $_POST["txtGenerateId"];
		
		
		
		
        $response = $emp->Update($_code,$_AccountName,$_AccountNumber,$_AccountType,$_IfscCode,$_BankName,$_BranchName,$_MicrCode,$_PanNo,$_PanName,$_IdProof,$_Genid);
        echo $response[0];
    }
}





if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->SHOWDATA();

    $_DataTable = "";

   echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Bank Account Name</th>";
    echo "<th style='40%'> 	Bank Account Number</th>";
    echo "<th style='10%'> 	Bank IFSC Code</th>";
	echo "<th style='10%'> 	Bank Name</th>";
	echo "<th style='10%'> 	Bank Micr Code</th>";
	echo "<th style='10%'>  Bank Branch Name</th>";
	echo "<th style='10%'>  Account Type</th>";
	echo "<th style='10%'>  Pan Card No</th>";
	echo "<th style='10%'>  Pan Card Holder Name</th>";
	echo "<th style='10%'>  EDIT</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Bank_Account_Name'] . "</td>";
         echo "<td>'" . $_Row['Bank_Account_Number'] . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
		 echo "<td>" . $_Row['Bank_Micr_Code'] . "</td>";
		 echo "<td>" . $_Row['Bank_Branch_Name'] . "</td>";
		 echo "<td>" . $_Row['Bank_Account_Type'] . "</td>";
		 echo "<td>" . strtoupper($_Row['Pan_No']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Pan_Name']) . "</td>";
		  echo "<td><a href='frmbankaccount.php?code=" . $_Row['Bank_Account_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a></td>  ";
		 
        echo "</tr>";
        $_Count++;
    }
   echo "</tbody>";
    echo "</table>";
	echo "</div>";
}


if ($_action == "SHOWEDIT") {

    //echo "Show";
    $response = $emp->SHOWDATA();

    $_DataTable = "";

   echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Bank Account Name</th>";
    echo "<th style='40%'> 	Bank Account Number</th>";
    echo "<th style='10%'> 	Bank IFSC Code</th>";
	echo "<th style='10%'> 	Bank Name</th>";
	echo "<th style='10%'> 	Bank Micr Code</th>";
	echo "<th style='10%'>  Bank Branch Name</th>";
	echo "<th style='10%'>  Account Type</th>";
	echo "<th style='10%'>  Pan Card No</th>";
	echo "<th style='10%'>  Pan Card Holder Name</th>";
	echo "<th style='10%'>  Bank Document</th>";
	echo "<th style='10%'>  Pan Document</th>";
	echo "<th style='10%'>  ITGK Code</th>";
	
	echo "<th style='10%'>  Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
         echo "<td>'" . $_Row['Bank_Account_Number'] . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
		 echo "<td>" . $_Row['Bank_Micr_Code'] . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Branch_Name']) . "</td>";
		 echo "<td>" . $_Row['Bank_Account_Type'] . "</td>";
		 echo "<td>" . strtoupper($_Row['Pan_No']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Pan_Name']) . "</td>";
		  
		 
		 echo "<td>";
		 echo '<a href="upload/Bankdocs/'.$_Row['Bank_Document'].'" target="_blank"><img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/Bankdocs/'.$_Row['Bank_Document'].'"/></a>';
		 echo  "</td>";
		  echo "<td>";
		  echo '<a href="upload/pancard/'.$_Row['Pan_Document'].'" target="_blank"><img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/pancard/'.$_Row['Pan_Document'].'"/></a>';
		  echo "</td>";
		 

		echo "<td>" . $_Row['Bank_User_Code'] . "</td>";
		
		
       echo "<td><a href='frmfillbankaccount.php?code=" . $_Row['Bank_Account_Code'] . "&Mode=Edit'>"
              . "<img src='images/editicon.png' alt='Edit' width='30px' /></a> </td>";
        echo "</tr>";
        $_Count++;
    }
   echo "</tbody>";
    echo "</table>";
	
	
	
	
	echo "</div>";
}












if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='' >Select Banks</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['bankname'] . ">" . $_Row['bankname'] . "</option>";
    }
}

if ($_action == "FILLBRANCH") {
    $response = $emp->GetBranch();
    echo "<option value='' >Select Branch</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['branchname'] . ">" . $_Row['branchname'] . "</option>";
    }
}

if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_POST["values"]);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array(
            "Bank_Account_Name" => $_Row['Bank_Account_Name'],
            "Bank_Account_Number" => $_Row['Bank_Account_Number'],
            "Bank_Account_Type" => $_Row['Bank_Account_Type'],
            "Bank_Ifsc_code" => $_Row['Bank_Ifsc_code'],
            "Bank_Name" => $_Row['Bank_Name'],
            "Bank_Micr_Code" => $_Row['Bank_Micr_Code'],
			"Bank_Branch_Name" => $_Row['Bank_Branch_Name'],
			"Pan_No" => $_Row['Pan_No'],
			"Pan_Name" => $_Row['Pan_Name'],
			"Bank_Id_Proof" => $_Row['Bank_Id_Proof'],
			"Bank_Document" => $_Row['Bank_Document'],
			"Pan_Document" => $_Row['Pan_Document']
			
			);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}



if ($_action == "SHOWGRID") {

    //echo "Show";
    $response = $emp->SHOWDATA();

    $_DataTable = "";

   echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Bank Account Name</th>";
    echo "<th style='40%'> 	Bank Account Number</th>";
    echo "<th style='10%'> 	Bank IFSC Code</th>";
	echo "<th style='10%'> 	Bank Name</th>";
	echo "<th style='10%'> 	Bank Micr Code</th>";
	echo "<th style='10%'>  Bank Branch Name</th>";
	echo "<th style='10%'>  Account Type</th>";
	echo "<th style='10%'>  Pan Card No</th>";
	echo "<th style='10%'>  Pan Card Holder Name</th>";
	echo "<th style='10%'>  Bank Document</th>";
	echo "<th style='10%'>  PAN  Document</th>";
	echo "<th style='10%'>  ITGK Code</th>";
	
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
         echo "<td>'" . strtoupper($_Row['Bank_Account_Number']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
		 echo "<td>" . $_Row['Bank_Micr_Code'] . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Branch_Name']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Account_Type']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Pan_No']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Pan_Name']) . "</td>";
		  
		 
		 echo "<td>";
		 echo '<a href="upload/Bankdocs/'.$_Row['Bank_Document'].'" target="_blank"><img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/Bankdocs/'.$_Row['Bank_Document'].'"/></a>';
		 echo  "</td>";
		  echo "<td>";
		  echo '<a href="upload/pancard/'.$_Row['Pan_Document'].'" target="_blank"><img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/pancard/'.$_Row['Pan_Document'].'"/></a>';
		  echo "</td>";
		 

		echo "<td>" . $_Row['Bank_User_Code'] . "</td>";
		
		
     
        echo "</tr>";
        $_Count++;
    }
   echo "</tbody>";
    echo "</table>";
	
	
	
	
	echo "</div>";
}




if ($_action == "SENDOTP") {
    $response = $emp->SendOTP();
    echo $response[0];
}



if ($_action == "VERIFY") {
    //print_r($_POST);
    if (isset($_POST["email"])) {
        $_Email = $_POST["email"];
        $_Mobile = $_POST["mobile"];
        $_Otp = $_POST["opt"];

        $response = $emp->Verify($_Email, $_Mobile, $_Otp);
        echo $response[0];
    }
}


if ($_action == "IFSCCHEK") {
    //print_r($_POST);
    if (isset($_POST["IFSC"])) {
        $_IFSC = $_POST["IFSC"];
        

        $response = $emp->checkifsccode($_IFSC);
        if($response[0] == 'Success') 
		{
			 echo "1";
		}
		
		else
		{
					  echo "0";
		}
    }
}




