<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsNcrVisit.php';

$response = array();
$emp = new clsNcrVisit();

if ($_action == 'FILLAO') {
    $response = $emp->GetAOList();
    echo "<option value='' >Select AO Code</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_LoginId'] . ">" . $_Row['User_LoginId'] . "</option>";
    }
}

if ($_action == "GETAODETAILS") {

    $response = $emp->GetDatabyCode($_POST["itgk"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example1' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>AO Code</th>";
    echo "<th style='20%'>AO Name</th>";
    echo "<th style='20%'>AO Ack No</th>";
    echo "<th style='10%'>AO Email</th>";
    echo "<th style='10%'>AO Mobile</th>";
    echo "<th style='10%'>AO Address</th>";
    echo "<th style='10%'>AO District</th>";
    echo "<th style='10%'>Ao Tehsil.</th>";
   
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	 echo "</div>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['User_LoginId'] . "</td>";
         echo "<td>" . $_Row['Organization_Name'] . "</td>";
         echo "<td>" . $_Row['User_Ack'] . "</td>";
         echo "<td>" . $_Row['User_EmailId'] . "</td>";
         echo "<td>" . $_Row['User_MobileNo'] . "</td>";
         echo "<td>" . $_Row['Organization_Address'] . "</td>";
         echo "<td>" . $_Row['Organization_District_Name'] . "</td>";
         echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";
        
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}

if ($_action == "ADDVISIT") {
    if (isset($_POST["AOCode"]) && !empty($_POST["Date"])) {
        $_AOCode = $_POST["AOCode"];
        $_Date=$_POST["Date"];

        $response = $emp->AddVisit($_AOCode,$_Date);
        echo $response[0];
    }
}

