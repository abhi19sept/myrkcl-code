<?php

/* 
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsPremisesDetail.php';

$response = array();
$emp = new clsPremisesDetail();


if ($_action == "ADD") {    
       // print_r($_POST);
         if (isset($_POST["receptionYesNo"]))
		{
			$_Reception = $_POST["receptionYesNo"];
			$_ReceptionCapacity = $_POST["txtreceptionSeatingCapacity"];
			$_receptionAreaInSqFt = $_POST["txtreceptionAreaInSqFt"];
			$_receptionDetails = $_POST["txtreceptionDetails"];
			$_parkingYesNo = $_POST["parkingYesNo"];
			$_parkingType = $_POST["parkingType"];
			$_parkingTW = $_POST["parkingTW"];
			$_fwCapacity= $_POST["txtfwCapacity"];
			$_twCapacity = $_POST["txttwCapacity"];
			$_toiletYes = $_POST["toiletYesNo"];
			$_OwnershipType = $_POST["toiletOwnershipType"];
			$_toiletType = $_POST["toiletType"];
			$_pantryYesNo = $_POST["pantryYesNo"];
			$_pantryDetails = $_POST["txtpantryDetails"];
			$_libraryYes = $_POST["libraryYesNo"];
			$_libraryDetails = $_POST["txtlibraryDetails"];
			$_staffRoomYesNo = $_POST["staffRoomYesNo"];
			$_staffRoomDetails =  $_POST["txtstaffRoomDetails"];
			
                          if ($_POST["area"] == "Urban") {
            $_AreaType = 'Yes';
         $_TheoryArea = $_POST["TheoryRoomArea"];
         $_LabArea = $_POST["LabArea"];
         $_TotalArea = $_POST["TotalArea"];
        }
        if ($_POST["area"] == "Rural") {
            $_AreaType = 'No';
         $_TheoryArea = '0';
         $_LabArea = '0';
         $_TotalArea = $_POST["TotalArea1"];
        }
				
			//echo $_StatusName;
			$response = $emp->Add($_Reception,$_ReceptionCapacity,$_receptionAreaInSqFt,$_receptionDetails,
                                $_parkingYesNo,$_parkingType,$_parkingTW,$_fwCapacity,$_twCapacity,$_toiletYes,
                                $_OwnershipType,$_toiletType,$_pantryYesNo,$_pantryDetails,$_libraryYes,$_libraryDetails,
                                $_staffRoomYesNo,$_staffRoomDetails, $_AreaType, $_TheoryArea, $_LabArea, $_TotalArea);
            echo $response[0];
    }
}




if ($_action == "SHOW") {

	
    $response = $emp->GetAll();

    $_DataTable = "";
    echo "<div class='table-responsive' style='margin-top:10px'>";
   
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'> 	Premises User</th>";
    echo "<th style='30%'> Reception Type</th>";
	echo "<th style='30%'> Seating Capacity</th>";
	echo "<th style='30%'> Area</th>";
	echo "<th style='30%'> Details</th>";
	echo "<th style='30%'> Parking Facility</th>";
	echo "<th style='30%'> Ownership</th>";
	echo "<th style='30%'> Parking For</th>";
	echo "<th style='30%'> Toilet Available</th>";
	echo "<th style='30%'> Toilet Ownership </th>";
	echo "<th style='30%'>Toilet Type</th>";
	echo "<th style='30%'>Panatry</th>";
	echo "<th style='30%'>Panatry Capacity</th>";
	echo "<th style='30%'>Library Available</th>";
	echo "<th style='30%'>Library capacity</th>";
	echo "<th style='30%'>Staff Available</th>";
	echo "<th style='30%'>Staff Capacity</th>";
    echo "<th style='20%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
       echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Premises_User'] . "</td>";
		 echo "<td>" . $_Row['Premises_Receptiontype'] . "</td>";
         echo "<td>" . $_Row['Premises_seatingcapacity'] . "</td>";
		 echo "<td>" . $_Row['Premises_area'] . "</td>";
		  echo "<td>" . $_Row['Premises_details'] . "</td>";
		  echo "<td>" . $_Row['Premises_parkingfacility'] . "</td>";
		   echo "<td>" . $_Row['Premises_ownership'] . "</td>";
		   echo "<td>" . $_Row['Premises_parkingfor'] . "</td>";
		   echo "<td>" . $_Row['Premises_toiletavailable'] . "</td>";
		    echo "<td>" . $_Row['Premises_ownershiptoilet'] . "</td>";
			echo "<td>" . $_Row['Premises_toilet_type'] . "</td>";
			
			echo "<td>" . $_Row['Premises_panatry'] . "</td>";
			echo "<td>" . $_Row['Premises_panatry_capacity'] . "</td>";
			echo "<td>" . $_Row['Premises_library_available'] . "</td>";
			echo "<td>" . $_Row['Premises_library_capacity'] . "</td>";
			echo "<td>" . $_Row['Premises_Staff_available'] . "</td>";
			echo "<td>" . $_Row['Premises_Staff_capacity'] . "</td>";
	if ($_SESSION['User_UserRoll'] == 15) {		
        echo "<td><a href='frmpremisesdetails.php?code=" . $_Row['Premises_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a> </td> ";
        } else{
            echo "<td> Details Submitted </td>";
        }
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}
if ($_action == "UPDATE") {
     if(isset($_POST["receptionYesNo"]))
		{
			$_Reception = $_POST["receptionYesNo"];
			$_ReceptionCapacity = $_POST["txtreceptionSeatingCapacity"];
			$_receptionAreaInSqFt = $_POST["txtreceptionAreaInSqFt"];
			$_receptionDetails = $_POST["txtreceptionDetails"];
			$_parkingYesNo = $_POST["parkingYesNo"];
			$_parkingType = $_POST["parkingType"];
			$_parkingTW = $_POST["parkingTW"];
			$_fwCapacity= $_POST["txtfwCapacity"];
			$_twCapacity = $_POST["txttwCapacity"];
			$_toiletYes = $_POST["toiletYesNo"];
			$_OwnershipType = $_POST["toiletOwnershipType"];
			$_toiletType = $_POST["toiletType"];
			$_pantryYesNo = $_POST["pantryYesNo"];
			$_pantryDetails = $_POST["txtpantryDetails"];
			$_libraryYes = $_POST["libraryYesNo"];
			$_libraryDetails = $_POST["txtlibraryDetails"];
			$_staffRoomYesNo = $_POST["staffRoomYesNo"];
			$_staffRoomDetails =  $_POST["txtstaffRoomDetails"];
			$_Code=$_POST['code'];
        $response = $emp->Update($_Code,$_Reception,$_ReceptionCapacity,$_receptionAreaInSqFt,$_receptionDetails,$_parkingYesNo,$_parkingType,$_parkingTW,$_fwCapacity,$_twCapacity,$_toiletYes,$_OwnershipType,$_toiletType,$_pantryYesNo,$_pantryDetails,$_libraryYes,$_libraryDetails,$_staffRoomYesNo,$_staffRoomDetails);
        echo $response[0];
    }
}



if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_POST["values"]);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("Premises_Receptiontype" => $_Row['Premises_Receptiontype'],
		"Premises_seatingcapacity" => $_Row['Premises_seatingcapacity'],
		"Premises_area" => $_Row['Premises_area'],
		"Premises_details" => $_Row['Premises_details'],
		"Premises_parkingfacility" => $_Row['Premises_parkingfacility'],
		"Premises_ownership" => $_Row['Premises_ownership'],
		"Premises_parkingfor" => $_Row['Premises_parkingfor'],
		"Premises_fwcapacity" => $_Row['Premises_fwcapacity'],
		"Premises_twcapacity" => $_Row['Premises_twcapacity'],
		"Premises_toiletavailable" => $_Row['Premises_toiletavailable'],
		"Premises_ownershiptoilet" => $_Row['Premises_ownershiptoilet'],
		"Premises_toilet_type" => $_Row['Premises_toilet_type'],
		"Premises_panatry" => $_Row['Premises_panatry'],
		"Premises_panatry_capacity" => $_Row['Premises_panatry_capacity'],
		"Premises_library_available" => $_Row['Premises_library_available'],
		"Premises_library_capacity" => $_Row['Premises_library_capacity'],
		"Premises_Staff_available" => $_Row['Premises_Staff_available'],
		"Premises_Staff_capacity" => $_Row['Premises_Staff_capacity']);
		
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}

if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


?>
