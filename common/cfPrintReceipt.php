<?php

/*
 * @author Abhi

 */
include 'commonFunction.php';
require 'BAL/clsPrintReceipt.php';

$response = array();
$emp = new clsPrintReceipt();

if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_Datatable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("LearnerCode" => $_Row['Admission_LearnerCode'],
            "LearnerName" => $_Row['Admission_Name'],
            "FatherName" => $_Row['Admission_Fname'],
            "LearnerPhoto" => $_Row['Admission_Photo'],
            "LearnerSign" => $_Row['Admission_Sign'],
            //        "Mobile" => $_Row['UserProfile_Mobile'],
            "CenterCode" => $_Row['Admission_ITGK_Code'],
            "AdmissionDate" => $_Row['Admission_Date'],
            "CourseName" => $_Row['Course_Name'],
            "CenterName" => $_Row['Organization_Name'],
            "Batch" => $_Row['Batch_Name'],
            "BatchStartDate" => $_Row['Batch_StartDate'],
            "Medium" => $_Row['Admission_Medium'],
            "District" => $_Row['District_Name'],
            "CenterCoordinator" => $_Row['Staff_Name'],
            "CourseFee" => $_Row['Admission_Fee'],
            "DOB" => $_Row['Admission_DOB']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}

if ($_action == "PRINT") {

//     $response = $emp->GetDatabyCode($_actionvalue);
//
//    $_Datatable = array();
//    $_i = 0;
//    while ($_Row = mysqli_fetch_array($response[2])) {
//        $_Datatable[$_i] = array("LearnerCode" => $_Row['Admission_LearnerCode'],
//            "LearnerName" => $_Row['Admission_Name'],
//            "FatherName" => $_Row['Admission_Fname'],
//            "LearnerPhoto" => $_Row['Admission_Photo'],
//            "LearnerSign" => $_Row['Admission_Sign'],
//            //        "Mobile" => $_Row['UserProfile_Mobile'],
//            "CenterCode" => $_Row['Admission_ITGK_Code'],
//            "AdmissionDate" => $_Row['Admission_Date'],
//            "CourseName" => $_Row['Course_Name'],
//            "CenterName" => $_Row['Organization_Name'],
//            "Batch" => $_Row['Batch_Name'],
//            "BatchStartDate" => $_Row['Batch_StartDate'],
//            "Medium" => $_Row['Admission_Medium'],
//            "District" => $_Row['District_Name'],
//            "CenterCoordinator" => $_Row['Staff_Name'],
//            "CourseFee" => $_Row['Admission_Fee'],
//            "DOB" => $_Row['Admission_DOB']);
//        $_i = $_i + 1;
//    }
//    echo json_encode($_Datatable);
/////////////////////////////////////////////////////////////
    $response = $emp->GetDatabyCode($_actionvalue);
    //  $_Row = mysqli_fetch_array($response[2]);
    $_row = mysqli_fetch_array($response[2]);
//    $_CourseName = $_Row["Course_Name"];
//    $_LearnerCode = $_Row['Admission_LearnerCode'];
//    $_LearnerName = $_Row['Admission_Name'];
//    $_FatherName = $_Row['Admission_Fname'];
//    $_LearnerPhoto = $_Row['Admission_Photo'];
//    $_LearnerSign = $_Row['Admission_Sign'];
//    $_CenterCode = $_Row['Admission_ITGK_Code'];
//    $_AdmissionDate = $_Row['Admission_Date'];
//    $_CourseName = $_Row['Course_Name'];
//    $_CenterName = $_Row['Organization_Name'];
//    $_Batch = $_Row['Batch_Name'];
//    $_BatchStartDate = $_Row['Batch_StartDate'];
//    $_Medium = $_Row['Admission_Medium'];
//    $_District = $_Row['District_Name'];
//    $_CenterCoordinator = $_Row['Staff_Name'];
//    $_CourseFee = $_Row['Admission_Fee'];
//    $_DOB = $_Row['Admission_DOB'];
//    //////////////////////////////////////////////////////////////////////
//    
//    $_Datatable = array();
//    $_i = 0;
//    while ($_Row = mysqli_fetch_array($response[2])) {
//        $_Datatable[$_i] = array("LearnerCode" => $_Row['Admission_LearnerCode'],
//            "LearnerName" => $_Row['Admission_Name'],
//            "FatherName" => $_Row['Admission_Fname'],
//            "LearnerPhoto" => $_Row['Admission_Photo'],
//            "LearnerSign"=>$_Row['Admission_Sign'],
//    //        "Mobile" => $_Row['UserProfile_Mobile'],
//            "CenterCode" => $_Row['Admission_ITGK_Code'],
//            "AdmissionDate" => $_Row['Admission_Date'],
//            "CourseName" => $_Row['Course_Name'],
//            "CenterName" => $_Row['Organization_Name'],
//            "Batch" => $_Row['Batch_Name'],
//            "BatchStartDate" => $_Row['Batch_StartDate'],
//            "Medium" => $_Row['Admission_Medium'],
//            "District" => $_Row['District_Name'],
//            "CenterCoordinator" => $_Row['Staff_Name'],
//            "CourseFee"=>$_Row['Admission_Fee'],
//            "DOB"=>$_Row['Admission_DOB']);
//        $_i = $_i + 1;
//    }
//    ///////////////////////////////////////////////////////////
//    
//    
//    $html = '<center>
//                    <DIV style="width:1000px;" >
//                        <div align="center">
//                            <p style="text-align:center ">';
//    $html.= $_CourseName;
//    $html.=' FEE RECEIPT (ITGK Copy) - Original</p>
//                            <p><div style="display: inline">
//                                <img id="lphoto" width="80" height="107" alt="photo" src="/myrkcladmin/upload/admission_photo/'.$_LearnerPhoto.'"/>
//                            </div> </p>
//                            <p>
//                                <img id="lsign" width="80" height="35" alt="sign" src="/myrkcladmin/upload/admission_sign/'.$_LearnerSign.'"/>
//                            </p>
//                        </div>
//
//
//                        <div style="float:left ">
//                            ITGK Name: ';
//    $html.= $_CenterName;
//    $html.= '(ITGK Code: ';
//    $html.= $_CenterCode;
//    $html.= ')<br>
//                        <span style="margin-right:116px;">	Receipt No: 4788 </span>
//                        </div>
//                        <div style="float:right">
//                            Form No: 1506280047
//                        </div>
//                        </BR>
//                        </BR>
//
//                        <div style="text-align:justify; margin-top:25px;">
//
//                            We have received ';
//    $html.= $_CourseFee;
//    $html.= ' /-(Rs. ';
//    $html.= convert_number_to_words($_CourseFee);
//    $html.= ' Only in cash from the applicant ';
//    $html.= $_LearnerName;
//    $html.= 'Father/Husband`s Name: ';
//    $html.= $_FatherName;
//    $html.= ' with DOB: ';
//    $html.= $_DOB;
//    $html.= ' and Learner ID: ';
//    $html.= $_LearnerCode;
//    $html.= ' towards tuition fee, internal assessment, learning facilitation, course material charges, examination and enrolment fee of ';
//    $html.= $_CourseName;
//    $html.= ' in Regular with single instalment Mode for the batch commencing on ';
//    $html.= $_BatchStartDate;
//    $html.= ' in ';
//    $html.= $_Medium;
//    $html.= ' medium. This fee has been received by us on behalf of Rajasthan Knowledge Corporation Ltd. (RKCL), Vardhaman Mahaveer Open University and ourselves and we undertake to remit the respective shares to RKCL and VMOU. We also undertake to issue the course material to you at the beginning of the course, cost of which is included in this fee. In case we fail to remit the above mentioned fee received from you to RKCL and VMOU before the due date for any reason as a result of which there is a deficiency in provision of services, we undertake the full responsibility for the damage and loss to you and shall be duty bound to refund the fee received and in lump sum to you immediately. We have also received your photograph and signature. We have attested them against the proof of identity submitted by you as attached to your application form. We undertake to upload the same and scan copy of application form against your learner ID ';
//    $html.= $_LearnerCode;
//    $html.= ' on RKCL`s website on or before the last date stipulated by RKCL.
//                        </div>
//                        <div style="float:left; margin-top:25px;">
//                            Seal and Signature of ITGK: <br>
//                            <span style="margin-right:80px;"> Date: ';
//    $html.= date("d/m/Y");
//    $html.= '</span>
//                        </div>
//                        <div style="float:right; margin-top:25px;">
//                            Place: ';
//    $html.= $_District;
//    $html.= '<BR>
//                            Center Coordinator: ';
//    $html.= $_CenterCoordinator;
//    $html.= '             </div>
//                    </DIV>
//                    <!--   /////////////////////////////////////////////////////////////////////// -->
//
//
//                    <DIV style="width:1000px; margin-top:120px;" >
//                        <hr>';
//    $html.= '<div align="center">
//                            <p style="text-align:center ">';
//    $html.= $_CourseName;
//    $html.=' FEE RECEIPT (Applicant Copy) - Original</p>
//                             <p><div style="display: inline">
//                                <img id="lphoto" width="80" height="107" alt="photo" src="/myrkcladmin/upload/admission_photo/'.$_LearnerPhoto.'"/>
//                            </div> </p>
//                            <p>
//                                <img id="lsign" width="80" height="35" alt="sign" src="/myrkcladmin/upload/admission_sign/'.$_LearnerSign.'"/>
//                            </p>
//                        </div>
//
//
//                        <div style="float:left ">
//                            ITGK Name: ';
//    $html.= $_CenterName;
//    $html.= '(ITGK Code: ';
//    $html.= $_CenterCode;
//    $html.= ')<br>
//                        <span style="margin-right:116px;">	Receipt No: 4788 </span>
//                        </div>
//                        <div style="float:right">
//                            Form No: 1506280047
//                        </div>
//                        </BR>
//                        </BR>
//
//                        <div style="text-align:justify; margin-top:25px;">
//
//                            We have received ';
//    $html.= $_CourseFee;
//    $html.= ' /-(Rs. ';
//    $html.= convert_number_to_words($_CourseFee);
//    $html.= ' Only in cash from the applicant ';
//    $html.= $_LearnerName;
//    $html.= 'Father/Husband`s Name: ';
//    $html.= $_FatherName;
//    $html.= ' with DOB: ';
//    $html.= $_DOB;
//    $html.= ' and Learner ID: ';
//    $html.= $_LearnerCode;
//    $html.= ' towards tuition fee, internal assessment, learning facilitation, course material charges, examination and enrolment fee of ';
//    $html.= $_CourseName;
//    $html.= ' in Regular with single instalment Mode for the batch commencing on ';
//    $html.= $_BatchStartDate;
//    $html.= ' in ';
//    $html.= $_Medium;
//    $html.= ' medium. This fee has been received by us on behalf of Rajasthan Knowledge Corporation Ltd. (RKCL), Vardhaman Mahaveer Open University and ourselves and we undertake to remit the respective shares to RKCL and VMOU. We also undertake to issue the course material to you at the beginning of the course, cost of which is included in this fee. In case we fail to remit the above mentioned fee received from you to RKCL and VMOU before the due date for any reason as a result of which there is a deficiency in provision of services, we undertake the full responsibility for the damage and loss to you and shall be duty bound to refund the fee received and in lump sum to you immediately. We have also received your photograph and signature. We have attested them against the proof of identity submitted by you as attached to your application form. We undertake to upload the same and scan copy of application form against your learner ID ';
//    $html.= $_LearnerCode;
//    $html.= ' on RKCL`s website on or before the last date stipulated by RKCL.
//                        </div>
//                        <div style="float:left; margin-top:25px;">
//                            Seal and Signature of ITGK: <br>
//                            <span style="margin-right:80px;"> Date: ';
//    $html.= date("d/m/Y");
//    $html.= '</span>
//                        </div>
//                        <div style="float:right; margin-top:25px;">
//                            Place: ';
//    $html.= $_District;
//    $html.= '<BR>
//                            Center Coordinator: ';
//    $html.= $_CenterCoordinator;
//    $html.= '             </div>
//                    </DIV>
//                    <!--   /////////////////////////////////////////////////////////////////////// -->
//                    <DIV style="width:1000px; margin-top:120px;" >
//                        <hr>
//                        <p>Please contact feedback@rkcl.in for any feedback, grievances or any difficulties during RS-CIT training. You can also send an SMS</p>
//                        <p> to 9220092200 by typing RKCL {Center Code} {Query}.</p>
//                    </div>
//                </center>';
    
    $html1 = '<div id="page_1" style="position: relative;overflow: hidden;margin: -50px 0 18px 35px !important;padding: 0;border: none;width: 760px">
                            
<div id="id_1" style="margin: 0 0 0 0;padding: 0;border: 0px solid #333;width: 760px;overflow: hidden">
<table cellpadding="0" cellspacing="0" class="t0" style="width: 611px;font: 9px &quot;Arial&quot;">

</table>
<p class="p2 ft1" style="font: 14px bold &quot;Times New Roman&quot;;text-decoration: underline;line-height: 16px;text-align: left;padding-left: 340px;margin-top: 5px;margin-bottom: 0">' . $_row['Course_Name'] . '</p>
<p class="p3 ft2" style="font: 10px &quot;Times New Roman&quot;;text-decoration: underline;line-height: 12px;text-align: left;padding-left: 240px;margin-top: 1px;margin-bottom: 2px"><span class="ft1" style="font: 14px bold &quot;Times New Roman&quot;;text-decoration: underline;line-height: 14px">FEE RECEIPT </span>(NOT VALID WITHOUT LEARNERID)</p>
<div align="center">
    <img id="lphoto" width="80" height="107" alt="photo" src="/myrkcllive2/upload/admission_photo/' . $_row['Admission_Photo'] . '"/>
</div>
<div align="center">
    <img id="lsign" width="80" height="35" alt="sign" src="/myrkcllive2/upload/admission_sign/' . $_row['Admission_Sign'] . '"/>
</div>
<table cellpadding="0" cellspacing="0" class="t1" width="500" style="width:500px !important;margin-left:70px;margin-top: 0px;font: italic 13px &quot;Times New Roman&quot;">
<tr>
	<td width="300"><p style="font-weight: bold">ITGK</p></td>
	<td width="250"><p style="font-weight: bold">Receipt No. <span class="ft4" style="font: 13px &quot;Times New Roman&quot;line-height: 15px">7827</span></p></td>
</tr>
<tr>
	<td class="tr0 td2" style="width: 600px;vertical-align: bottom;height: 16px"><p class="p4 ft3" style="font: italic bold 13px &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">Name : ' . $_row['Organization_Name'] . '</p></td>
	<td class="tr0 td3" style="width: 100px;vertical-align: bottom;height: 16px"><p class="p5 ft4" style="font: 13px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: right;margin-top: 0;margin-bottom: 0;white-space: nowrap">Form No. <span class="ft3" style="font: italic 13px &quot;Times New Roman&quot;;line-height: 15px"> 1512080002</span></p></td>
</tr>
<tr>
	<td class="tr2 td2" style="width: 600px;vertical-align: bottom;height: 18px"><p class="p4 ft3" style="font: italic 13px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">ITGK Code : <span class="ft4" style="font: 13px &quot;Times New Roman&quot;;line-height: 15px">' . $_row['Admission_ITGK_Code'] . '</span></p></td>
	<td class="tr2 td3" style="width: 100px;vertical-align: bottom;height: 18px"><p class="p5 ft4" style="font: 15px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: right;margin-top: 0;margin-bottom: 0;white-space: nowrap">ITGK COPY</p></td>
</tr>
</table>
<p class="p6 ft6" style="font: italic 9px &quot;Times New Roman&quot;;line-height: 12px;text-align: justify;padding-left: 43px;padding-right: 77px;margin-top: 4px;margin-bottom: 0;text-indent: 26px"><span class="ft5" style="font: 9px &quot;Times New Roman&quot;;line-height: 12px">We have received ' . $_row['Admission_Fee'] . ' <b>INR</b>( </span>' . convert_number_to_words($_row['Admission_Fee']) . ' <b>INR</b>) in cash from the applicant <b>' . $_row['Admission_Name'] . '</b> Father/Husband’s Name: <b>' . $_row['Admission_Fname'] . '</b> with DOB:<b>' . $_row['Admission_DOB'] . '</b> and Learner ID: <b>' . $_row['Admission_LearnerCode'] . '</b> towards course fees comprising tuition fee, internal assessment, learning facilitation, course material charges, examination and enrollment fee of ' . $_row['Course_Name'] . ' for the batch commencing on ' . $_row['Batch_StartDate'] . ' in ' . $_row['Admission_Medium'] . ' medium. This fee has been received on behalf of Rajasthan Knowledge Corporation Ltd. (RKCL). Vardhaman Mahaveer Open University and ourselves and we undertake to remit the respective shares to RKCL/VMOU. We also undertake to issue the study material to you at the beginning of the course, cost of which is included in this fee. In case we fail to remit the above mentioned fee received from you to RKCL /VMOU before the due date for any reason, we undertake the full responsibility for the damage and loss to you and shall be duty bound to refund the fee received to you immediately. We have also received your photograph and signature. We have attested them against the proof of identity submitted by you as attached to your application form. We undertake to upload the same against your Learner ID <b>' . $_row['Admission_LearnerCode'] . '</b> on RKCLs website on or before the last date stipulated by RKCL.</p>
<table cellpadding="0" cellspacing="0" class="t2" style="width: 614px;margin-left: 43px;margin-top: 5px;font: 13px &quot;Times New Roman&quot;">
<tr>
	<td class="tr1 td4" style="padding: 0;margin: 0;width: 387px;vertical-align: bottom;height: 21px"><p class="p4 ft4" style="font: 12px &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">&nbsp;&nbsp;Seal of ITGK:</p></td>
	<td class="tr1 td5" style="padding: 0;margin: 0;width: 227px;vertical-align: bottom;height: 21px"><p class="p4 ft7" style="font: 1px &quot;Times New Roman&quot;;line-height: 1px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap"> </p></td>
</tr>
<tr>
	<td colspan="1" style="width:150px !important;margin-left:50px !important"><p>&nbsp;&nbsp;Place: ' . $_row['District_Name'] . '</p></td>
	<td colspan="1" style="width:100px !important;"><p >Signature:</p></td>
</tr>
<tr>
	<td colspan="1" style="width:250px !important;margin-left:50px !important"><p>&nbsp;&nbsp;Date: ' . date("d/m/Y") . '</p></td>
	<td colspan="1" style="width:200px !important;"><p>Center Co­ordinator: ' . $_row['Staff_Name'] . '</p></td>
</tr>
</table>
<hr>
<p class="p7 ft9" style="font: 14px bold &quot;Times New Roman&quot;;text-decoration: underline;line-height: 16px;text-align: left;padding-left: 340px;margin-top: 10px;margin-bottom: 0">' . $_row['Course_Name'] . '</p>
<p class="p8 ft10" style="font: 10px &quot;Times New Roman&quot;;text-decoration: underline;line-height: 12px;text-align: left;padding-left: 240px;margin-top: 2px;margin-bottom: 2px"><span class="ft9" style="font: 14px bold &quot;Times New Roman&quot;;text-decoration: underline;line-height: 16px">FEE RECEIPT </span>(NOT VALID WITHOUT LEARNERID)</p>
<div align="center">
    <img id="lphoto" width="80" height="107" alt="photo" src="/myrkcllive2/upload/admission_photo/' . $_row['Admission_Photo'] . '"/>
</div>
<div align="center">
    <img id="lsign" width="80" height="35" alt="sign" src="/myrkcllive2/upload/admission_sign/' . $_row['Admission_Sign'] . '"/>
</div>
<table cellpadding="0" cellspacing="0" class="t1" width="500" style="width:500px !important;margin-left:70px;margin-top: 0px;font: italic 13px &quot;Times New Roman&quot;">
<tr>
	<td width="300"><p style="font-weight: bold">ITGK</p></td>
	<td width="250"><p style="font-weight: bold">Receipt No. <span class="ft4" style="font: 13px &quot;Times New Roman&quot;line-height: 15px">7827</span></p></td>
</tr>
<tr>
	<td class="tr0 td2" style="width: 600px;vertical-align: bottom;height: 16px"><p class="p4 ft3" style="font: italic bold 13px &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">Name : ' . $_row['Organization_Name'] . '</p></td>
	<td class="tr0 td3" style="width: 100px;vertical-align: bottom;height: 16px"><p class="p5 ft4" style="font: 13px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: right;margin-top: 0;margin-bottom: 0;white-space: nowrap">Form No. <span class="ft3" style="font: italic 13px &quot;Times New Roman&quot;;line-height: 15px"> 1512080002</span></p></td>
</tr>
<tr>
	<td class="tr2 td2" style="width: 600px;vertical-align: bottom;height: 18px"><p class="p4 ft3" style="font: italic 13px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">ITGK Code : <span class="ft4" style="font: 13px &quot;Times New Roman&quot;;line-height: 15px">' . $_row['Admission_ITGK_Code'] . '</span></p></td>
	<td class="tr2 td3" style="width: 100px;vertical-align: bottom;height: 18px"><p class="p5 ft4" style="font: 15px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: right;margin-top: 0;margin-bottom: 0;white-space: nowrap">ITGK COPY</p></td>
</tr>
</table>
<p class="p6 ft6" style="font: italic 9px &quot;Times New Roman&quot;;line-height: 12px;text-align: justify;padding-left: 43px;padding-right: 77px;margin-top: 4px;margin-bottom: 0;text-indent: 26px"><span class="ft5" style="font: 9px &quot;Times New Roman&quot;;line-height: 12px">We have received ' . $_row['Admission_Fee'] . ' <b>INR</b>( </span>' . convert_number_to_words($_row['Admission_Fee']) . ' <b>INR</b>) in cash from the applicant <b>' . $_row['Admission_Name'] . '</b> Father/Husband’s Name: <b>' . $_row['Admission_Fname'] . '</b> with DOB:<b>' . $_row['Admission_DOB'] . '</b> and Learner ID: <b>' . $_row['Admission_LearnerCode'] . '</b> towards course fees comprising tuition fee, internal assessment, learning facilitation, course material charges, examination and enrollment fee of ' . $_row['Course_Name'] . ' for the batch commencing on ' . $_row['Batch_StartDate'] . ' in ' . $_row['Admission_Medium'] . ' medium. This fee has been received on behalf of Rajasthan Knowledge Corporation Ltd. (RKCL). Vardhaman Mahaveer Open University and ourselves and we undertake to remit the respective shares to RKCL/VMOU. We also undertake to issue the study material to you at the beginning of the course, cost of which is included in this fee. In case we fail to remit the above mentioned fee received from you to RKCL /VMOU before the due date for any reason, we undertake the full responsibility for the damage and loss to you and shall be duty bound to refund the fee received to you immediately. We have also received your photograph and signature. We have attested them against the proof of identity submitted by you as attached to your application form. We undertake to upload the same against your Learner ID <b>' . $_row['Admission_LearnerCode'] . '</b> on RKCLs website on or before the last date stipulated by RKCL.</p>
<table cellpadding="0" cellspacing="0" class="t2" style="width: 614px;margin-left: 43px;margin-top: 5px;font: 13px &quot;Times New Roman&quot;">
<tr>
	<td class="tr1 td4" style="padding: 0;margin: 0;width: 387px;vertical-align: bottom;height: 21px"><p class="p4 ft4" style="font: 12px &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">&nbsp;&nbsp;&nbsp;&nbsp;Seal of ITGK:</p></td>
	<td class="tr1 td5" style="padding: 0;margin: 0;width: 227px;vertical-align: bottom;height: 21px"><p class="p4 ft7" style="font: 1px &quot;Times New Roman&quot;;line-height: 1px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap"> </p></td>
</tr>
<tr>
	<td colspan="1" style="width:200px !important;margin-left:50px !important"><p>&nbsp;&nbsp;&nbsp;&nbsp;Place: ' . $_row['District_Name'] . '</p></td>
	<td colspan="1" style="width:150px !important;"><p >Signature:</p></td>
</tr>
<tr>
	<td colspan="1" style="width:300px !important;margin-left:50px !important"><p>&nbsp;&nbsp;&nbsp;&nbsp;Date: ' . date("d/m/Y") . '</p></td>
	<td colspan="1" style="width:200px !important;"><p>Center Co­ordinator: ' . $_row['Staff_Name'] . '</p></td>
</tr>
</table>
<hr>
<p class="p9 ft11" style="font: italic 9px &quot;Times New Roman&quot;;line-height: 13px;text-align: left;padding-left: 43px;padding-right: 87px;margin-top: 15px;margin-bottom: 0">Please contact the following in case of any difficulty during the course or for redressal of your complaints and grievances.(RKCL : info@rkcl.in) You can also send an SMS to 9220092200 by typing RKCL {Center Code} {Query}.</p>
<table cellpadding="0" cellspacing="0" class="t3" style="width: 599px;margin-left: 43px;font: 13px &quot;Times New Roman&quot;">
<tr>
	<td class="tr3 td6" style="padding: 0;margin: 0;width: 0;vertical-align: bottom;height: 17px"/>
	<td rowspan="2" class="tr5 td7" style="padding: 0;margin: 0;width: 409px;vertical-align: bottom;height: 25px"><p class="p4 ft4" style="font: 9px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">1. DLC: ' . $_row['DLCName'] . '</p></td>
	<td class="tr3 td8" style="padding: 0;margin: 0;width: 190px;vertical-align: bottom;height: 17px"><p class="p4 ft4" style="font: 9px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">2. PSA: ' . $_row['PSAName'] . '</p></td>
</tr>

<tr>
	<td class="tr8 td6" style="padding: 0;margin: 0;width: 0;vertical-align: bottom;height: 7px"/>
	<td class="tr8 td7" style="padding: 0;margin: 0;width: 409px;vertical-align: bottom;height: 7px"><p class="p4 ft12" style="font: 1px &quot;Times New Roman&quot;;line-height: 7px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap"> </p></td>
</tr>
<tr>
	<td class="tr3 td6" style="padding: 0;margin: 0;width: 0;vertical-align: bottom;height: 10px"/>
	<td class="tr3 td7" style="padding: 0;margin: 0;width: 409px;vertical-align: bottom;height: 10px"><p class="p4 ft4" style="font: 9px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">3. ITGK : ' . $_row['Organization_Name'] . '(Code: ' . $_row['Admission_ITGK_Code'] . ' ) (Mobile</p></td>
	<td class="tr3 td8" style="padding: 0;margin: 0;width: 190px;vertical-align: bottom;height: 10px"><p class="p4 ft4" style="font: 9px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">4. RKCL</p></td>
</tr>
<tr>
	<td class="tr3 td6" style="padding: 0;margin: 0;width: 0;vertical-align: bottom;height: 10px"/>
	<td class="tr3 td7" style="padding: 0;margin: 0;width: 409px;vertical-align: bottom;height: 10px"><p class="p4 ft4" style="font: 9px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">No.:9829445469 )</p></td>
	<td class="tr3 td8" style="padding: 0;margin: 0;width: 190px;vertical-align: bottom;height: 10px"><p class="p4 ft4" style="font: 9px bold &quot;Times New Roman&quot;;line-height: 15px;text-align: left;margin-top: 0;margin-bottom: 0;white-space: nowrap">5. VMOU</p></td>
</tr>
</table>
</div>

</div>';
    echo $html1;
}

function convert_number_to_words($number) {

    $hyphen = '-';
    $conjunction = ' and ';
    $separator = ', ';
    $negative = 'negative ';
    $decimal = ' point ';
    $dictionary = array(
        0 => 'Zero',
        1 => 'One',
        2 => 'Two',
        3 => 'Three',
        4 => 'Four',
        5 => 'Five',
        6 => 'Six',
        7 => 'Seven',
        8 => 'Eight',
        9 => 'Nine',
        10 => 'Ten',
        11 => 'Eleven',
        12 => 'Twelve',
        13 => 'Thirteen',
        14 => 'Fourteen',
        15 => 'Fifteen',
        16 => 'Sixteen',
        17 => 'Seventeen',
        18 => 'Eighteen',
        19 => 'Nineteen',
        20 => 'Twenty',
        30 => 'Thirty',
        40 => 'Fourty',
        50 => 'Fifty',
        60 => 'Sixty',
        70 => 'Seventy',
        80 => 'Eighty',
        90 => 'Ninety',
        100 => 'Hundred',
        1000 => 'Thousand',
        1000000 => 'Million',
        1000000000 => 'Billion',
        1000000000000 => 'Trillion',
        1000000000000000 => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens = ((int) ($number / 10)) * 10;
            $units = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}

function getdata($code) {
    $response = $emp->GetDatabyCode($_actionvalue);

    $_Datatable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("LearnerCode" => $_Row['Admission_LearnerCode'],
            "LearnerName" => $_Row['Admission_Name'],
            "FatherName" => $_Row['Admission_Fname'],
            "LearnerPhoto" => $_Row['Admission_Photo'],
            "LearnerSign" => $_Row['Admission_Sign'],
            //        "Mobile" => $_Row['UserProfile_Mobile'],
            "CenterCode" => $_Row['Admission_ITGK_Code'],
            "AdmissionDate" => $_Row['Admission_Date'],
            "CourseName" => $_Row['Course_Name'],
            "CenterName" => $_Row['Organization_Name'],
            "Batch" => $_Row['Batch_Name'],
            "BatchStartDate" => $_Row['Batch_StartDate'],
            "Medium" => $_Row['Admission_Medium'],
            "District" => $_Row['District_Name'],
            "CenterCoordinator" => $_Row['Staff_Name'],
            "CourseFee" => $_Row['Admission_Fee'],
            "DOB" => $_Row['Admission_DOB']);
        $_i = $_i + 1;
    }
    return $_Datatable;
}
