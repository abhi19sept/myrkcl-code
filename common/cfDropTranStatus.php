<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsDropTranStatus.php';

$response = array();
$emp = new clsDropTranStatus();



if ($_action == "DETAILS") {
    $response = $emp->getdetails($_POST['values']);
    
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Transaction ID</th>";
    echo "<th>Payment Status</th>";
    echo "<th >Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th>Father/Husband Name</th>";
    echo "<th>DOB</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if($response[0] == 'Success')
    {
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_POST['values'] . "</td>";
        echo "<td>" . 'Payment In Process' . "</td>";
        echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        echo "<td>" . $_Row['Admission_DOB'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    }
    echo "</tbody>";
     
    echo "</table>";
   
    echo "</div>";
}
?>