<?php

//session_start
/*
 *  author MAYANK
 */
include 'commonFunction.php';
require 'BAL/clsWcdRscfaAdmission.php';
header('Content-Type: text/html; charset=utf-8');

$response = array();
$emp = new clsWcdRscfaAdmission();

if ($_action == "ADD") {
    $response = $emp->Add(serialize($_POST));
    echo $response[0];
}

if ($_action == 'ValidateAgeName') {
    $_DOB = $_POST["dob"];
    $dateDigits = explode('-', $_DOB);
    if ($dateDigits[0] == 0 || $dateDigits[1] == 0 || $dateDigits[2] == 0) {
        die('ageInvalid');
    }

   // $compareBy = '01-01-' . date('Y');
    
	$compareBy = '01-01-2021';
	$diff = $emp->calculateAge($_DOB, $compareBy);
    $years = $diff->y;
    $months = $diff->m;
    $days = $diff->d;
    $result = ($years < 16 || $years > 40 || ($years == 40 && ($months > 0 || $days > 0))) ? 0 : 1;
    if (!$result) {
        echo 'ageInvalid';
    } else {
        $insertValues = $emp->getValidatedValues($_POST, 0);
        //print_r($insertValues); die;
        if (empty($insertValues) || !is_array($insertValues)) {
            echo $insertValues;
        } else {
            $_Response = $emp->getAccountExistance($_POST['lname'], $_POST['parent'], $_DOB, $_POST['aadhaar'], $_POST['batch']);
            if (mysqli_num_rows($_Response[2])) {
                $result = mysqli_fetch_array($_Response[2]);
                if ($result['Oasis_Admission_UID'] == $_POST['aadhaar']) {
                    echo 'aadhaarExists';
                } elseif ($result['Oasis_Admission_LearnerStatus'] == 'Pending') {
                    echo 'accPending';
                } else {
                    echo 'accInvalid';
                }
            } else {
                echo '1';
            }
        }
    }
}

if ($_action == 'downloadApp') {
    $filePath = $emp->downloadApplicationFormById($_POST);
    if ($filePath != 'No Record Found') {
        echo "<script>window.location.href = '" . str_replace('../','',$filePath) . "';</script>";
    } else echo $filePath;
}
if ($_action == "UPDATE") {
	//print_r($_POST);
	//print_r($_FILES);
	
	$_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_photo/';
	$_UploadDirectory2 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_sign/';
	
	$_LearnerName = $_POST["txtlname"];
	$_ParentName = $_POST["txtfname"];
	$_DOB = $_POST["dob"];                        
	$_Code = $_POST['txtAdmissionCode'];
	$_GeneratedId = $_POST['txtGenerateId'];
	
	if($_FILES["p1"]["name"]!='') {					     
				$imag =  $_FILES["p1"]["name"];
				$newpicture = $_GeneratedId .'_photo'.'.png';
				$_FILES["p1"]["name"] = $newpicture;
	  	}
	else  {					   
			$newpicture = $_POST['txtphoto'];
		}

	if($_FILES["p1"]["name"]!='') {
		$imageinfo = pathinfo($_FILES["p1"]["name"]);		
		if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg") {
			$error_msg = "Image must be in either PNG or JPG Format";			
		 }
		else {	
			if (file_exists("$_UploadDirectory1/" .$_GeneratedId .'_photo'.'.png'))			 
				{ 
					unlink("$_UploadDirectory1/".$newpicture); 
				}
			if (file_exists($_UploadDirectory1)) {
				if (is_writable($_UploadDirectory1)) {
				  move_uploaded_file($_FILES["p1"]["tmp_name"], "$_UploadDirectory1/".$newpicture);							 
			 }
		  }
		}
	 }	
	 
	 
	 if($_FILES["p2"]["name"]!='') {					     
				$imag =  $_FILES["p2"]["name"];
				$newsign = $_GeneratedId .'_sign'.'.png';
				$_FILES["p2"]["name"] = $newsign;
	  	}
	else  {					   
			$newsign = $_POST['txtsign'];
		}

	if($_FILES["p2"]["name"]!='') {
		$imageinfo = pathinfo($_FILES["p2"]["name"]);		
		if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg") {
			$error_msg = "Image must be in either PNG or JPG Format";			
		 }
		else {	
			if (file_exists("$_UploadDirectory2/" .$_GeneratedId .'_sign'.'.png'))			 
				{ 
					unlink("$_UploadDirectory2/".$newsign); 
				}
			if (file_exists($_UploadDirectory2)) {
				if (is_writable($_UploadDirectory2)) {
				  move_uploaded_file($_FILES["p2"]["tmp_name"], "$_UploadDirectory2/".$newsign);							 
			 }
		  }
		}
	 }	
					
	$response = $emp->Update($_LearnerName, $_ParentName, $_DOB, $newpicture, $newsign, $_Code);
	echo $response[0];
	
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("AdmissionCode" => $_Row['Admission_Code'],
            "lname" => $_Row['Admission_Name'],
            "fname" => $_Row['Admission_Fname'],
            "dob" => $_Row['Admission_DOB'],
            "photo" => $_Row['Admission_Photo'],
            "sign" => $_Row['Admission_Sign']);
        //$_SESSION['ImageFileEdit'] = $_Row['Admission_Photo'];
        //$_SESSION['SignFileEdit'] = $_Row['Admission_Sign'];

        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}

if ($_action == "Fee") {
    // print_r($_POST);
    $response = $emp->GetAdmissionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Course_Fee'];
}


if ($_action == "Install") {
    //echo "call";
    $response = $emp->GetAdmissionInstall($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Admission_Installation_Mode'];
}

if ($_action == "FILLIntake") {
    //echo "call";
    $response = $emp->GetIntakeAvailable($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Intake_Available'];
}

if ($_action == "FILLCenterOasispref2") {
    $response = $emp->FILLCenterOasis($_POST['values']);
    echo "<option value='0' selected='selected'>Select IT-GK</option>";
	$_rowcount= mysqli_num_rows($response[2]);
    while ($_Row = mysqli_fetch_array($response[2])) {
        $Center = $_Row['Courseitgk_ITGK'] . "(" . $_Row['Organization_Name'] . ")";
        echo "<option value=" . $_Row['Organization_User'] . ">" . $Center . "</option>";
		 if ($_rowcount == '1') {
			 echo "<option value='4332'>No Preference</option>";
		 }
    }
}

if ($_action == "FILLCenterOasispref1") {
    $response = $emp->FILLCenterOasis($_POST['values']);
    echo "<option value='0' selected='selected'>Select IT-GK</option>";
	
    while ($_Row = mysqli_fetch_array($response[2])) {
        $Center = $_Row['Courseitgk_ITGK'] . "(" . $_Row['Organization_Name'] . ")";
        echo "<option value=" . $_Row['Organization_User'] . ">" . $Center . "</option>";
    }
}
/*
if ($_action == "CENTERDETAILS") {
    $response = $emp->GetCenterDetails($_POST['values']);
    $batchcode = $_POST['batchcode'];
    $_DataTable = array();
    $_i = 0;
    //$applications = $emp->getCenterApplications($_POST['values'], $batchcode);
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array(
            "orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['User_MobileNo'],           
            "itgk_address" => $_Row['Organization_Address'],
            "applications" => '', // '<span class="small">Applications received: <b>' . $applications . '</b></span>',
        );
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}
*/

if ($_action == "CENTERDETAILS") {
    $response = $emp->GetCenterDetails($_POST['values']);
    $batchcode = $_POST['batchcode'];
    $_DataTable = array();
    $_i = 0;
    //$applications = $emp->getCenterApplications($_POST['values'], $batchcode);
    while ($_Row = mysqli_fetch_array($response[2])) {
                //print_r($_Row);
                htmlspecialchars($_Row['Organization_Address'], ENT_NOQUOTES, "UTF-8");

                $address =  preg_replace('/[[:^print:]]/', "", $_Row['Organization_Address']);
//echo trim($c);

 $itgk_address=preg_replace('/\s/', ' ', $address);
        $_DataTable[$_i] = array(
            "orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['User_MobileNo'],           
                        "itgk_address" => $itgk_address,
            "applications" => '', // '<span class="small">Applications received: <b>' . $applications . '</b></span>',
        );
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "FILLTSPDistrict") {
    $response = $emp->FILLTSPDistrict();
    echo "<option value='' selected='selected'>Select T.S.P District</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {       
        echo "<option value=" . $_Row['TSP_District_Code'] . ">" . $_Row['TSP_District_Name'] . "</option>";
    }
}

if ($_action == "FILLTSPTehsil") {
    $response = $emp->FILLTSPTehsil($_POST['values']);
    echo "<option value='' selected='selected'>Select T.S.P Tehsil</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {       
        echo "<option value=" . $_Row['TSP_Tehsil_Code'] . ">" . $_Row['TSP_Tehsil_Name'] . "</option>";
    }
}

if ($_action == "FILLHigher") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Qualification_Code'] . ">" . $_Row['Qualification_Name'] . "</option>";
    }
}

