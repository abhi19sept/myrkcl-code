<?php
/* 
 * Created by Abhishek
 */
 
include './commonFunction.php';
require 'BAL/clsUpdateResultDate.php';

$response = array();
$emp = new clsUpdateResultDate();

if ($_action == "ADD") {
    if (isset($_POST["examid"]) && !empty($_POST["examid"])) {
        $_ExamID = $_POST["examid"];
        $_ResultDate=$_POST["resultdate"];

        $response = $emp->Add($_ExamID,$_ResultDate);
        echo $response[0];
    }
}

if ($_action == "Update") {
    if (isset($_POST["examidmodal"]) && !empty($_POST["examidmodal"])) {
        $_ExamIdModal = $_POST["examidmodal"];
        $_ResultDateModal=$_POST["resultdatemodal"];

        $response = $emp->Update($_ExamIdModal,$_ResultDateModal);
        echo $response[0];
    }
}

if ($_action == "SHOW") {
    //echo "Show";
    $response = $emp->GetAllDetails();

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='15%'>Event ID</th>";
    echo "<th style='35%'>Event Name</th>";
    echo "<th style='30%'>Event Name VMOU</th>";
    echo "<th style='30%'>Result Date</th>";
	if ($_SESSION['User_Code'] == '1'){
		echo "<th style='20%'>Action</th>";
	}
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Event_Id'] . "</td>";
        echo "<td>" . $_Row['Event_Name'] . "</td>";
        echo "<td>" . $_Row['Event_Name_VMOU'] . "</td>";
        echo "<td>" . $_Row['Result_Date'] . "</td>";
		if ($_SESSION['User_Code'] == '1'){
			echo "<td><img class='rsltdate' id='".$_Row['Event_Id']."' name='" . $_Row['Event_Name'] . "' src='images/editicon.png' alt='Edit' width='30px' /></td>";
		}
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	 echo "</div>";
}

if ($_action == "FillExamEvent") {
    $response = $emp->GetExamEvent();
    echo "<option value='' >Select Exam Event</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}