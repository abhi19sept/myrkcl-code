<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCourseWiseBlockRpt.php';

$response = array();
$emp = new clsCourseWiseBlockRpt();



if ($_action == "FILL") {
    $response = $emp->GetCourse();
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Courseitgk_Course'] . "'>" . $_Row['Courseitgk_Course'] . "</option>";
    }
}



if ($_action == "COURSEWISE") {

  //print_r($_POST);
    $response = $emp->GetCourseWise($_POST['Course']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Center Code</th>";
    echo "<th style='45%'>Centre Name</th>";
    echo "<th style='45%'>Event Date</th>";
    echo "<th style='40%'>Email ID</th>";
    echo "<th style='10%'>Mobile No</th>";
    echo "<th style='10%'>Tehsil</th>";
    echo "<th style='10%'>District</th>";
    echo "<th style='10%'>SP Name</th>";
    echo "<th style='10%'>Remark</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['CenterCode'] . "</td>";
         echo "<td>" . $_Row['CenterName'] . "</td>";
         echo "<td>" . date("d M Y", strtotime($_Row['activitydate'])) . "</td>";
         echo "<td>" . $_Row['User_EmailId'] . "</td>";
         echo "<td>" . $_Row['User_MobileNo'] . "</td>";
         echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
         echo "<td>" . $_Row['District_Name'] . "</td>";
         echo "<td>" . $_Row['RSPName'] . "</td>";
         echo "<td>" . $_Row['remark'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

