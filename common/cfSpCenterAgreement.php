<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsSpCenterAgreement.php';
require '../DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();

$response = array();
$emp = new clsSpCenterAgreement();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>AO Code</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='10%'>IT-GK Type</th>";
    echo "<th style='40%'>IT-GK Mob No</th>";
    echo "<th style='40%'>IT-GK Email Id</th>";
    echo "<th style='40%'>IT-GK Address</th>";

    echo "<th style='40%'>IT-GK District</th>";
    echo "<th style='10%'>IT-GK Tehsil</th>";
    echo "<th style='10%'>Upload Agreement</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['User_Ack'] . "</td>";
            echo "<td>" . $_Row['User_LoginId'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Type'] . "</td>";
            echo "<td>" . $_Row['User_MobileNo'] . "</td>";
            echo "<td>" . $_Row['User_EmailId'] . "</td>";
            echo "<td>" . $_Row['Organization_Road'] . "</td>";
            echo "<td>" . $_Row['Organization_District_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";

            $response1 = $emp->GetUploadStatus($_Row['User_LoginId']);
            if ($response1[0] == 'Success') {
                if ($_SESSION['User_UserRoll'] == '14' && $_Row['User_UserRoll'] == '7' && $_Row['Org_Course_Allocation_Status'] == 'Pending') {
                    echo "<td> <a href='frmuploadspcenteragreement.php?code=" . $_Row['User_LoginId'] . "&Mode=Add&ack=" . $_Row['User_Ack'] . "'>"
                    . "<input type='button' name='Approve' id='Approve' class='btn btn-danger' value='Update Agreement'/></a>"
                    . "</td>";
                } elseif ($_Row['Org_Course_Allocation_Status'] == 'Approved') {
                    echo "<td>" . 'Approved' . "</td>";
                } else {
                    echo "<td>" . 'NA' . "</td>";
                }
            } else {

                if ($_SESSION['User_UserRoll'] == '14' && $_Row['User_UserRoll'] == '7' && $_Row['Org_Course_Allocation_Status'] == 'Pending') {
                    echo "<td> <a href='frmuploadspcenteragreement.php?code=" . $_Row['User_LoginId'] . "&Mode=Add&ack=" . $_Row['User_Ack'] . "'>"
                    . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='Upload Agreement'/></a>"
                    . "</td>";
                } elseif ($_Row['Org_Course_Allocation_Status'] == 'Approved') {
                    echo "<td>" . 'Approved' . "</td>";
                } else {
                    echo "<td>" . 'NA' . "</td>";
                }
            }
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "APPROVE") {
    $response = $emp->GetDatabyCode($_POST['values']);
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                "regno" => $_Row['Organization_RegistrationNo'],
                "fdate" => $_Row['Organization_FoundedDate'],
                "orgtype" => $_Row['Organization_Type'],
                "doctype" => $_Row['Organization_DocType'],
                "district" => $_Row['Organization_District_Name'],
                "districtcode" => $_Row['Organization_District'],
                "tehsil" => $_Row['Organization_Tehsil'],
                "orgdoc" => $_Row['SP_Agreement'],
                "road" => $_Row['Organization_Address']);
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "";
    }
}

if ($_action == "ADD") {
//    print_r(($_POST));
//    die;
    global $_ObjFTPConnection;
    if (isset($_POST["txtGenerateId"]) && !empty($_POST["txtGenerateId"])) {
        $_GeneratedId = $_POST["txtGenerateId"];
        $_CenterCode = $_POST["txtCenterCode"];
        $_UploadDirectory = "/SPCENTERAGREEMENT";

        $panimg = $_FILES['uploadImage7']['name'];
        $pantmp = $_FILES['uploadImage7']['tmp_name'];
        $pantemp = explode(".", $panimg);
        //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
        $PANfilename = $_GeneratedId . '_agreement.' . end($pantemp);
        $panfilestorepath = "../upload/SPCENTERAGREEMENT/" . $PANfilename;
        $panimageFileType = pathinfo($panfilestorepath, PATHINFO_EXTENSION);
        
        if (($_FILES["uploadImage7"]["size"] < 5000000 && $_FILES["uploadImage7"]["size"] > 100000)) {

            if ($panimageFileType == "pdf") {
        $Agreement_response_ftp=ftpUploadFile($_UploadDirectory,$PANfilename,$pantmp);

                //if (move_uploaded_file($pantmp, $panfilestorepath)) {
                if(trim($Agreement_response_ftp) == "SuccessfullyUploaded"){
                    $response = $emp->Add($_CenterCode, $PANfilename);

                    echo $response[0];
                } else {
                    echo "Agreement Not Uploded. Please try again.";
                }
            } else {
                echo "Sorry, File Not Valid";
            }
        } else {
            echo "File Size should be between 100KB to 5MB.";
        }
    } else {
        echo "Inavalid Entry15";
    }
}

if ($_action == "UPDATE") {
//    print_r(($_POST));
//    die;
    global $_ObjFTPConnection;
    if (isset($_POST["txtfilename"]) && !empty($_POST["txtfilename"])) {
        $_FilePath = "../" . $_POST["txtfilename"];
        $FileName = explode('/', $_POST["txtfilename"]);
        //print_r(($_FilePath));
        $panimg = $_FILES['uploadImage72']['name'];
        $pantmp = $_FILES['uploadImage72']['tmp_name'];
        //$pantemp = explode(".", $panimg);
        //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
        //$PANfilename = $_GeneratedId . '_agreement.' . end($pantemp);
        //$panfilestorepath = "../upload/SPCENTERAGREEMENT/" . $PANfilename;
        $panimageFileType = pathinfo($_FilePath, PATHINFO_EXTENSION);
        $PANfilename = $_POST["txtfilename"];
        $appformfilestorepathftp = "/SPCENTERAGREEMENT";
        $appform_response_ftp=ftpUploadFile('/'.$FileName[1],$FileName[2],$pantmp);

        if (($_FILES["uploadImage72"]["size"] < 5000000 && $_FILES["uploadImage72"]["size"] > 100000)) {

            if ($panimageFileType == "pdf") {
//                if (file_exists($_FilePath)) {
//                    unlink($_FilePath);
//                }
                //if (move_uploaded_file($pantmp, $_FilePath)) {
                if (trim($appform_response_ftp) == "SuccessfullyUploaded") {
                    echo "Agreement Updated Successfully.";
                } else {
                    echo "Agreement Not Updated. Please try again.";
                }
            } else {
                echo "Agreement Should be in PDF Format.";
            }
        } else {
            echo "Agreement Size should be between 100KB to 5MB.";
        }
    } else {
        echo "Inavalid Entry15";
    }
}