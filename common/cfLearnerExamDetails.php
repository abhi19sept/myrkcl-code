<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsLearnerExamDetails.php';

$response = array();
$emp = new clsLearnerExamDetails();

if ($_action == "DETAILS") {

    if ($_POST["searchtype"] == "1") {
        $responseLAM = $emp->GetLearnerAllMarks($_POST["txtLearnerCode"]);
        echo "<div class='container'>";
        echo "<div class='row'>";
        echo "<div class='personalblog scrollmenu' style='width: 125% !important; margin-top: 10px; !important'>";


//$_Row1 = mysqli_fetch_array($responseLAM[2]);
//print_r($_Row);//die;
        $num_rowsLAM = mysqli_num_rows($responseLAM[2]);
        if ($num_rowsLAM == 0) {
            ?><div class="error">
            <?php echo "<b>No Detail Found.</b>"; ?>
            </div>
            <?php
        } else {
            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Final Exam Marks Detail</span>
                    </div>";
            echo "<div class='persdetablog'>
                        <div class='exampannelNew'>
                            <div class='examname'>Learner Code</div>
                        </div>
                        <div class='exampannelNew' style='width: 18.9%;'>
                            <div class='examname'>Learner Name</div>
                        </div>
                        <div class='exampannelNew' style='width: 8.9%;'>
                            <div class='examname'>DOB</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Final Score</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Event Name</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Exam Date</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Result</div>
                        </div>
                    </div>";

            while ($_Row1 = mysqli_fetch_array($responseLAM[2], true)) {
                $Tdate = date("d-m-Y", strtotime($_Row1['exam_date']));
                $dd = date("d-m-Y", strtotime($_Row1['dob']));
                echo " <div class=' persdetablog1'>
                        <div class='exampannelNew'>
                            
                         <div class='marks'>" . strtoupper($_Row1['scol_no']) . " </div>
                        </div>                        
                       <div class='exampannelNew' style='width: 18.9%;'>
                            
                            <div class='marks'>" . strtoupper($_Row1['name']) . " </div>
                        </div>                        
                        <div class='exampannelNew' style='width: 8.9%;'>
                            
                            <div class='marks'>" . $dd . " </div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($_Row1['tot_marks']) . " Marks</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($_Row1['Event_Name']) . "</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($Tdate) . "</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($_Row1['result']) . "</div>
                         </div>
                    </div>";
            }
        }

        echo "</div>";
        echo "</div>";
        echo "</div>";
        echo "</div>";
    } elseif ($_POST["searchtype"] == "2") {
        $responseLAM = $emp->GetLearnerAllMarksbyrollno($_POST["txtLearnerRollNo"], $_POST["ddlExamEvent"]);
        echo "<div class='container'>";
        echo "<div class='row'>";
        echo "<div class='personalblog scrollmenu' style='width: 125% !important; margin-top: 0px; !important'>";


//$_Row1 = mysqli_fetch_array($responseLAM[2]);
//print_r($_Row);//die;
        $num_rowsLAM = mysqli_num_rows($responseLAM[2]);
        if ($num_rowsLAM == 0) {
            ?><div class="error">
            <?php echo "<b>No Detail Found.</b>"; ?>
            </div>
            <?php
        } else {
            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Final Exam Marks Detail</span>
                    </div>";
            echo "<div class='persdetablog'>
                        <div class='exampannelNew'>
                            <div class='examname'>Learner Code</div>
                        </div>
                        <div class='exampannelNew' style='width: 18.9%;'>
                            <div class='examname'>Learner Name</div>
                        </div>
                        <div class='exampannelNew' style='width: 8.9%;'>
                            <div class='examname'>DOB</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Final Score</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Event Name</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Exam Date</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Result</div>
                        </div>
                    </div>";

            while ($_Row1 = mysqli_fetch_array($responseLAM[2], true)) {
                $Tdate = date("d-m-Y", strtotime($_Row1['exam_date']));
                $dd = date("d-m-Y", strtotime($_Row1['dob']));
                echo " <div class=' persdetablog1'>
                        <div class='exampannelNew'>
                            
                         <div class='marks'>" . strtoupper($_Row1['scol_no']) . " </div>
                        </div>                        
                        <div class='exampannelNew' style='width: 18.9%;'>
                            
                            <div class='marks'>" . strtoupper($_Row1['name']) . " </div>
                        </div>                        
                        <div class='exampannelNew' style='width: 8.9%;'>
                            
                            <div class='marks'>" . $dd . " </div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($_Row1['tot_marks']) . " Marks</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($_Row1['Event_Name']) . "</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($Tdate) . "</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($_Row1['result']) . "</div>
                         </div>
                    </div>";
            }
        }

        echo "</div>";
        echo "</div>";
        echo "</div>";
        echo "</div>";
    } elseif ($_POST["searchtype"] == "3") {

        if ($_POST["txtLdob"]) {
            $Tdob = date("Y-m-d", strtotime($_POST["txtLdob"]));
        } else {
            $Tdob = '';
        }

        //$Tdob = date("Y-m-d", strtotime($_POST["txtLdob"]));
        $responseLAM = $emp->Getbyall($_POST["txtLearnerName"], $_POST["txtCcode"], $Tdob);
        $num_rowsLAM = mysqli_num_rows($responseLAM[2]);
        if ($num_rowsLAM == 0) {
            ?><div class="error">
            <?php echo "<b>No Detail Found.</b>"; ?>
            </div>
            <?php
        } else {
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>S No.</th>";
            echo "<th> Learner Name </th>";
            echo "<th> Father Name</th>";
            echo "<th> Learner Code </th>";
            echo "<th> DOB</th>";
            echo "<th> ITGK Code </th>";
            echo "<th>Roll No</th>";
            echo "<th>Exam Event Name</th>";
            echo "<th>Show Details</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;

            if ($responseLAM[0] == 'Success') {
                while ($row = mysqli_fetch_array($responseLAM[2])) {
                    $dd = date("d-m-Y", strtotime($row['dob']));
                    echo "<tr class='odd gradeX'>";
                    echo "<td>" . $_Count . "</td>";
                    echo "<td>" . strtoupper($row['name']) . "</td>";
                    echo "<td>" . strtoupper($row['fname']) . "</td>";
                    echo "<td>" . $row['scol_no'] . "</td>";
                    echo "<td>" . $dd . "</td>";
                    echo "<td>" . $row['study_cen'] . "</td>";
                    echo "<td>" . $row['rollno'] . "</td>";
                    echo "<td>" . $row['Event_Name'] . "</td>";
                    echo "<td><input type='button' name=" . $row['study_cen'] . " id=" . $row['scol_no'] . " class='approvalLetter btn btn-primary' value='Show Result'/></td>";
                    echo "</tr>";

                    $_Count++;
                }
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        }
    }
}
if ($_POST['action'] == "getbysearch") {
//    print_r($_POST);
//    die;
    $study_cen = $_POST['study_cen'];
    $scol_no = $_POST['scol_no'];
    $responseLAM = $emp->Getbysearch($study_cen, $scol_no);
    echo "<div class='container'>";
    echo "<div class='row'>";
    echo "<div class='personalblog' style='width: 89% !important; margin-top: 0px; !important'>";


//$_Row1 = mysqli_fetch_array($responseLAM[2]);
//print_r($_Row);//die;
    $num_rowsLAM = mysqli_num_rows($responseLAM[2]);
    if ($num_rowsLAM == 0) {
        ?><div class="error">
        <?php echo "<b>No Detail Found.</b>"; ?>
        </div>
        <?php
    } else {
        echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Final Exam Marks Detail</span>
                    </div>";
        echo "<div class='persdetablog'>
                        <div class='exampannelNew'>
                            <div class='examname'>Final Score</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Event Name</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Exam Date</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Result</div>
                        </div>
                    </div>";

        while ($_Row1 = mysqli_fetch_array($responseLAM[2], true)) {
            $Tdate = date("d-m-Y", strtotime($_Row1['exam_date']));
            echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($_Row1['tot_marks']) . " Marks</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($_Row1['exam_event_name']) . "</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($Tdate) . "</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>" . strtoupper($_Row1['result']) . "</div>
                         </div>
                    </div>";
        }
    }

    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
}
if ($_action == "ReExamDETAILS") {
    if ($_POST["searchtype"] == "1") {
        $responseLAM = $emp->GetLearnerReexam($_POST["txtLearnerCode"]);
        echo "<div class='container'>";
        echo "<div class='row'>";
        echo "<div class='personalblog scrollmenu' style='width: 125% !important; margin-top: 10px; !important'>";


//$_Row1 = mysqli_fetch_array($responseLAM[2]);
//print_r($_Row);//die;
        $num_rowsLAM = mysqli_num_rows($responseLAM[2]);
        if ($num_rowsLAM == 0) {
            ?><div class="error">
            <?php echo "<b>No Detail Found.</b>"; ?>
            </div>
            <?php
        } else {
            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Re-Exam Detail</span>
                    </div>";
            echo "<div class='persdetablog'>
                        <div class='exampannelNew1'>
                            <div class='examname'>Learner Code</div>
                        </div>
                        <div class='exampannelNew1' style='width: 18.9%;'>
                            <div class='examname'>Learner Name</div>
                        </div>
                        <div class='exampannelNew1' style='width: 8.9%;'>
                            <div class='examname'>DOB</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>IT-GK Code</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Event Name</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Exam Date</div>
                        </div>
                
                    </div>";

            while ($_Row1 = mysqli_fetch_array($responseLAM[2], true)) {
                $Tdate = date("d-m-Y", strtotime($_Row1['examdate']));
                $dd = date("d-m-Y", strtotime($_Row1['dob']));
                echo " <div class=' persdetablog1'>
                        <div class='exampannelNew1'>
                            
                         <div class='marks'>" . strtoupper($_Row1['learnercode']) . " </div>
                        </div>                        
                       <div class='exampannelNew1' style='width: 18.9%;'>
                            
                            <div class='marks'>" . strtoupper($_Row1['learnername']) . " </div>
                        </div>                        
                        <div class='exampannelNew1' style='width: 8.9%;'>
                            
                            <div class='marks'>" . $dd . " </div>
                        </div>
                        <div class='exampannelNew1'>
                            
                            <div class='marks'>" . strtoupper($_Row1['itgkcode']) . " Marks</div>
                        </div>
                        <div class='exampannelNew1'>
                            
                            <div class='marks'>" . strtoupper($_Row1['Event_Name']) . "</div>
                        </div>
                        <div class='exampannelNew1'>
                            
                            <div class='marks'>" . strtoupper($Tdate) . "</div>
                        </div>
                      
                    </div>";
            }
        }

        echo "</div>";
        echo "</div>";
        echo "</div>";
        echo "</div>";
    } elseif ($_POST["searchtype"] == "3") {
        if ($_POST["txtLdob"]) {
            $Tdob = date("Y-m-d", strtotime($_POST["txtLdob"]));
        } else {
            $Tdob = '';
        }
        // $Tdob = date("Y-m-d", strtotime($_POST["txtLdob"]));
        $responseLAM = $emp->Getbyallreexam($_POST["txtLearnerName"], $_POST["txtCcode"], $Tdob);
        $num_rowsLAM = mysqli_num_rows($responseLAM[2]);
        if ($num_rowsLAM == 0) {
            ?><div class="error">
            <?php echo "<b>No Detail Found.</b>"; ?>
            </div>
            <?php
        } else {
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>S No.</th>";
            echo "<th> Learner Name </th>";
            echo "<th> Father Name</th>";
            echo "<th> Learner Code </th>";
            echo "<th> ITGK Code </th>";
            echo "<th>DOB</th>";
            //echo "<th>Exam Event Name</th>";
            echo "<th>Show Details</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;

            if ($responseLAM[0] == 'Success') {
                while ($row = mysqli_fetch_array($responseLAM[2])) {
                    $dd = date("d-m-Y", strtotime($row['dob']));
                    echo "<tr class='odd gradeX'>";
                    echo "<td>" . $_Count . "</td>";
                    echo "<td>" . strtoupper($row['learnername']) . "</td>";
                    echo "<td>" . strtoupper($row['fathername']) . "</td>";
                    echo "<td>" . $row['learnercode'] . "</td>";
                    echo "<td>" . $row['itgkcode'] . "</td>";
                    echo "<td>" . $dd . "</td>";
                    //  echo "<td>" . $row['Event_Name'] . "</td>";
                    echo "<td><input type='button' name=" . $row['itgkcode'] . " id=" . $row['learnercode'] . " class='approvalLetter btn btn-primary' value='Show Details'/></td>";
                    echo "</tr>";

                    $_Count++;
                }
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        }
    }
}
if ($_POST['action'] == "getbysearchreexam") {
//    print_r($_POST);
//    die;
    $study_cen = $_POST['study_cen'];
    $scol_no = $_POST['scol_no'];
    $responseLAM = $emp->Getbysearchreexam($study_cen, $scol_no);

    echo "<div class='container'>";
    echo "<div class='row'>";
    echo "<div class='personalblog scrollmenu' style='width: 125% !important; margin-top: 10px; !important ;padding-right:15px;'>";


//$_Row1 = mysqli_fetch_array($responseLAM[2]);
//print_r($_Row);//die;
    $num_rowsLAM = mysqli_num_rows($responseLAM[2]);
    if ($num_rowsLAM == 0) {
        ?><div class="error">
            <?php echo "<b>No Detail Found.</b>"; ?>
        </div>
        <?php
    } else {
        echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Re-Exam Detail</span>
                    </div>";
        echo "<div class='persdetablog'>
                        <div class='exampannelNew1'>
                            <div class='examname'>Learner Code</div>
                        </div>
                        <div class='exampannelNew1' style='width: 18.9%;'>
                            <div class='examname'>Learner Name</div>
                        </div>
                        <div class='exampannelNew1' style='width: 8.9%;'>
                            <div class='examname'>DOB</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>IT-GK Code</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Event Name</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Exam Date</div>
                        </div>
                
                    </div>";

        while ($_Row1 = mysqli_fetch_array($responseLAM[2], true)) {
            $Tdate = date("d-m-Y", strtotime($_Row1['examdate']));
            $dd = date("d-m-Y", strtotime($_Row1['dob']));
            echo " <div class=' persdetablog1'>
                        <div class='exampannelNew1'>
                            
                         <div class='marks'>" . strtoupper($_Row1['learnercode']) . " </div>
                        </div>                        
                       <div class='exampannelNew1' style='width: 18.9%;'>
                            
                            <div class='marks'>" . strtoupper($_Row1['learnername']) . " </div>
                        </div>                        
                        <div class='exampannelNew1' style='width: 8.9%;'>
                            
                            <div class='marks'>" . $dd . " </div>
                        </div>
                        <div class='exampannelNew1'>
                            
                            <div class='marks'>" . strtoupper($_Row1['itgkcode']) . "</div>
                        </div>
                        <div class='exampannelNew1'>
                            
                            <div class='marks'>" . strtoupper($_Row1['Event_Name']) . "</div>
                        </div>
                        <div class='exampannelNew1'>
                            
                            <div class='marks'>" . strtoupper($Tdate) . "</div>
                        </div>
                      
                    </div>";
        }
    }

    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
}