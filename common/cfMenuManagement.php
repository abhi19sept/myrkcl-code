<?php

include './commonFunction.php';
require 'BAL/clsMenuManagement.php';

$response = array();
$emp = new clsMenuManagement();

if ($_POST['action'] == "MenuManagementAction") {
    $response = $emp->GetRootMenu();
    if ($response[0] == 'Success') {
        echo "<div class='our-faq-wrapper'>
        <div class=''>
          <div class='row'><div class='col-md-12'>
            <div class='col-lg-12'>
              <div class='accordion' id='accordion'><div class='card'>
                  <div class='card-header' id='heading'>
                    <h5 class='mb-0' style='margin-bottom: -1px;'>
                      <div class='btn btn-link collapsed rootmenuclass' style='cursor: auto;'> <span class='clickManageRootMenu' id='AddRootMenu' style='cursor: pointer;'>Add New Root Menu</span> <input class='searchrootbtn' id='myInputRoot' type='text' placeholder='Search Root Menu...'></div>
                      
                    </h5>
                  </div>
                </div>
            </div><div id='SearchRoot'>
            ";
        $i = 1;
        $j = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            if ($_Row['root_menu_status'] == 1) {
                $color = "color: green;";
            } else {
                $color = "color: red;";
            }
            echo "<div class='col-lg-6 rootmenubtnfilter'>
              <div class='accordion' id='accordion'><div class='card'>
                  <div class='card-header' id='heading" . $i . "'>
                    <h5 class='mb-0' style='margin-bottom: -1px;'>
                      <div class='btn btn-link rootmenubtn' style='cursor: auto;' type='button'> <span style='cursor: pointer;' class='collapsed' data-toggle='collapse' data-target='#collapse" . $i . "' aria-expanded='false' aria-controls='collapse'>" . $_Row['root_menu_name'] . "</span><span id='" . $_Row['root_menu_code'] . "' class='glyphicon glyphicon-edit fun_edit_root' style='cursor: pointer;margin-right: 60px;margin-top: 2px;float: right;'></span><span id='" . $_Row['root_menu_code'] . "' class='glyphicon glyphicon-trash fun_delete_root' style='margin-right: 20px;margin-top: 2px;float: right;cursor:pointer;'></span><span class='glyphicon glyphicon-certificate' style='float: right;margin: 2px 15px 0 0;" . $color . "'></span> </div>
                    </h5>
                  </div>
                  <div id='collapse" . $i . "' class='collapse' aria-labelledby='heading" . $i . "' data-parent='#accordion'>
                    <div class='card-body'>";
            $parentmenu = $emp->GetParentMenu($_Row['root_menu_code']);
            echo "<h5 class='mb-0' style='margin-bottom: -1px;'>
                                <div class='btn btn-link collapsed parentclass' id='" . $_Row['root_menu_code'] . "' type='button' style='margin-left: 12px !important;width: 95%;background: #3366CC;'> <span class='clickManageParentMenu' id='" . $_Row['root_menu_code'] . "'>Add New Parent Menu</span> <input class='searchrootbtn myInputParent' id='" . $i . "' type='text' placeholder='Search Parent Menu...' style='margin-right: 55px;width: 170px;'></div>
                            </h5>";
            if ($parentmenu[0] == 'Success') {
                echo "<div id='SearchParent_" . $i . "'>";
                while ($_Row1 = mysqli_fetch_array($parentmenu[2])) {
                    if ($_Row1['Parent_Function_Status'] == 1) {
                        $colorp = "color: green;";
                    } else {
                        $colorp = "color: red;";
                    }
                    echo "<div class='panel panel-default parentmenubtnfilter_" . $i . "' style='border-color: #ffffff;'>
                                          <div class='panel-heading' role='tab' id='headingTwo' style='width: 80%;padding: 8px 15px;margin: 5px 0 -15px 35px;color: #242c42;background-color: #3366CC;border-color: #3366CC;'>
                                              <h4 class='panel-title'>
                                                  <a role='button' style='color: #ffffff;font-size: 16px; font-weight: 600;'>
                                                      
                                                     <span class='parentmenubtn_" . $i . "' data-toggle='collapse' data-parent='#accordion' href='#Parent_" . $j . "' aria-expanded='true' aria-controls='collapseOne' >" . $_Row1['Parent_Function_Name'] . "</span>
                                                  <span class='glyphicon glyphicon-edit fun_edit_parent' id='" . $_Row1['Parent_Function_Code'] . "' style='margin-right: 5px;margin-top: 2px;float: right;'></span><span class='glyphicon glyphicon-trash fun_delete_parent' id='" . $_Row1['Parent_Function_Code'] . "'  style='margin-right: 20px;margin-top: 2px;float: right;cursor:pointer;'></span></a>
                                                      <span class='glyphicon glyphicon-certificate' style='float: right;margin: 2px 15px 0 0;" . $colorp . "'></span>
                                                </h4>
                                          </div>
                                          <div id='Parent_" . $j . "' class='panel-collapse collapse' role='tabpanel' aria-labelledby='headingTwo' style='width: 80%;margin: 15px 0 0 35px;border-bottom: solid 1px rgb(0, 153, 255);border-left: solid 1px rgb(0, 153, 255);border-right: solid 1px rgb(0, 153, 255);'>
                                              <div class='panel-body' style='background-color: #f1f0f0;'>";
                    $childmenu = $emp->GetChildMenu($_Row1['Parent_Function_Code']);
                    echo "<h5 class='mb-0' style='margin-bottom: -1px;'>
                                                <div class='btn btn-link collapsed childslacc' type='button' style='margin-top: -18px;margin-left: 0 !important;width: 100%;background: #0099FF;'> <span class='clickManageChildMenu' id='" . $_Row1['Parent_Function_Code'] . "' name='" . $_Row['root_menu_code'] . "'>Add New Child Menu</span> <input class='searchrootbtn myInputChild' id='" . $j . "' type='text' placeholder='Search Child...' style='margin-right: 40px;width: 125px;'></div>
                                            </h5>";
                    if ($childmenu[0] == 'Success') {
                        echo "<div id='SearchChild_" . $j . "'>";
                        while ($_ChildRow = mysqli_fetch_array($childmenu[2])) {
                            if ($_ChildRow['Function_Status'] == 1) {
                                $colorc = "color: green;";
                            } else {
                                $colorc = "color: red;";
                            }
                            echo "<div style='margin: 5px;border: solid 1px #0099ff;background-color: #0099ff;' class='childmenubtnfilter_" . $j . "'><div style='margin: 5px;font-size: 15px;font-weight: 600;'><div style='    background-color: #3366cc;color: white;border-radius: 3px;'><div style='padding:5px;' class='childmenubtn_" . $j . "'>" . $_ChildRow['Function_Name'] . " <span class='glyphicon glyphicon-edit fun_edit_child' id='" . $_ChildRow['Function_Code'] . "' name='" . $_Row['root_menu_code'] . "' style='cursor:pointer;margin-right: 5px;margin-top: 2px;float: right;'></span><span class='glyphicon glyphicon-trash fun_delete_child' id='" . $_ChildRow['Function_Code'] . "' style='cursor:pointer;margin-right: 20px;margin-top: 2px;float: right;'></span><span class='glyphicon glyphicon-certificate' style='float: right;margin: 2px 15px 0 0;" . $colorc . "'></span></div></div></div>";
                            echo "<div style='margin: 5px;font-size: 15px;font-weight: 600;'><div style='    background-color: #3366cc;color: white;border-radius: 3px;'><div style='padding:5px;'>" . $_ChildRow['Function_URL'] . "</div></div></div></div>";
                        }
                        echo"</div>";
                    }
                    echo "</div>
                                          </div>
                                      </div>";
                    $j++;
                }
                echo "</div>";
            }
            echo "</div>
                  </div>
                </div></div>
            </div>";
            $i++;
        }
        echo "</div></div>
          </div>
        </div>
      </div>";
    }
}
if ($_action == "FILLSTATUS") {
    $select = $_POST['select'];
    if ($select == '0') {
        $selected = "selected='selected'";
    } else {
        $selected = "";
    }
    $response = $emp->GetStatus();
    echo "<option value='' " . $selected . ">-- Select Status --</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        if ($select == $_Row['Status_Code']) {
            $selected = "selected='selected'";
        } else {
            $selected = "";
        }
        echo "<option value=" . $_Row['Status_Code'] . " " . $selected . ">" . $_Row['Status_Name'] . "</option>";
    }
}
if ($_action == "FILLROOTFORPARENT") {
    $select = $_POST['RootCode'];
    if ($select == '0') {
        $selected = "selected='selected'";
    } else {
        $selected = "";
    }
    $response = $emp->FillRootForMenu();
    // echo "<option value='' " . $selected . ">-- Select Root Menu --</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        if ($select == $_Row['root_menu_code']) {
            $selected = "selected='selected'";
            echo "<option value=" . $_Row['root_menu_code'] . " " . $selected . ">" . $_Row['root_menu_name'] . "</option>";
        } else {
            $selected = "";
        }
    }
}
if ($_action == "FILLPARENT") {
    $root = $_POST['values'];
    $select = $_POST['values'];
    if ($select == '0') {
        $selected = "selected='selected'";
    } else {
        $selected = "";
    }
    $response = $emp->FillParent($root);
    while ($_Row = mysqli_fetch_array($response[2])) {
        if ($select == $_Row['Parent_Function_Code']) {
            $selected = "selected='selected'";
        } else {
            $selected = "";
        }
        echo "<option value=" . $_Row['Parent_Function_Code'] . " " . $selected . ">" . $_Row['Parent_Function_Name'] . "</option>";
    }
}
if ($_action == "FILLDISPLAYORDER") {
    $response = $emp->GetDisplayOrder();
    $_Row = mysqli_fetch_array($response[2]);
    $order = ($_Row["displayorder"] + 1);
    echo $order;
}
if ($_action == "FILLDISPLAYORDERPARENT") {
    $_Root = $_POST["RootCode"];
    $response = $emp->GetDisplayOrderParent($_Root);
    if ($response[0] == 'Success') {
        $_Row = mysqli_fetch_array($response[2]);
        $order = ($_Row["displayorder"] + 1);
        echo $order;
    } else {
        echo "1";
    }
}
if ($_action == "FILLDISPLAYORDERCHILD") {
    $_Parent = $_POST["ParentCode"];
    $response = $emp->GetDisplayOrderChild($_Parent);
    if ($response[0] == 'Success') {
        $_Row = mysqli_fetch_array($response[2]);
        $order = ($_Row["displayorder"] + 1);
        echo $order;
    } else {
        echo "1";
    }
}
if ($_action == "AddRootMenu") {
    if (isset($_POST["txtRootMenu"])) {
        $_RootName = strtoupper($_POST["txtRootMenu"]);
        $_Status = $_POST["ddlStatus"];
        $_DisplayOrder = $_POST['txtDisplayOrder'];
        $response = $emp->AddRootMenu($_RootName, $_Status, $_DisplayOrder);
        if ($response[0] == "Successfully Inserted") {
            echo "success";
        } else if ($response[0] == "Already Exists") {
            echo "duplicate";
        } else {
            echo "error";
        }
    }
}
if ($_action == "AddParentMenu") {
    if (isset($_POST["txtParentFunctionName"])) {
        $_RootCode = $_POST["ddlRootParent"];
        $_ParentName = strtoupper($_POST["txtParentFunctionName"]);
        $_DisplayOrder = $_POST["txtDisplayOrderParent"];
        $_Status = $_POST["ddlStatusParent"];
        $response = $emp->AddParentMenu($_RootCode, $_ParentName, $_DisplayOrder, $_Status);
        if ($response[0] == "Successfully Inserted") {
            echo "success";
        } else if ($response[0] == "Already Exists") {
            echo "duplicate";
        } else {
            echo "error";
        }
    }
}
if ($_action == "AddChildMenu") {

    if (isset($_POST["txtFunctionName"])) {
        $_ChildName = $_POST["txtFunctionName"];
        //$_RootCode = $_POST["ddlRootFunction"];
        $_ParentCode = $_POST["ddlParentFunction"];
        $_FunctionURL = $_POST["txtFunctionURL"];
        $_DisplayOrder = $_POST["txtDisplayOrderFunction"];
        $_Status = $_POST["ddlStatusFunction"];
        $response = $emp->AddChildMenu($_ChildName, $_ParentCode, $_FunctionURL, $_DisplayOrder, $_Status);
        if ($response[0] == "Successfully Inserted") {
            echo "success";
        } else if ($response[0] == "Already Exists") {
            echo "duplicate";
        } else {
            echo "error";
        }
    }
}
if ($_action == "DELETEROOT") {
    if (isset($_POST["deleteid"])) {
        $deleteid = $_POST["deleteid"];
        $response = $emp->DeleteRoot($deleteid);
    }
}
if ($_action == "DELETEPARENT") {
    if (isset($_POST["deleteid"])) {
        $deleteid = $_POST["deleteid"];
        $response = $emp->DeleteParent($deleteid);
    }
}
if ($_action == "DELETECHILD") {
    if (isset($_POST["deleteid"])) {
        $deleteid = $_POST["deleteid"];
        $response = $emp->DeleteChild($deleteid);
    }
}
if ($_POST['action'] == "FillRootEditData") {
    $editid = $_POST['editid'];
    $response = $emp->FillRootEditData($editid);
    $_DataTable = array();
    $_i = 0;
    if ($response[0] == 'Success') {
        while ($row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("root_menu_code" => $row['root_menu_code'],
                "root_menu_name" => $row['root_menu_name'],
                "root_menu_display" => $row['root_menu_display'],
                "action" => "UpdateRootMenu",
                "root_menu_status" => $row['root_menu_status']);
            $_i = $_i + 1;
        }
    }
    echo json_encode($_Datatable);
}

if ($_POST['action'] == "FillParentEditData") {
    $editid = $_POST['editid'];
    $response = $emp->FillParentEditData($editid);
    $_DataTable = array();
    $_i = 0;
    if ($response[0] == 'Success') {
        while ($row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("Parent_Function_Code" => $row['Parent_Function_Code'],
                "Parent_Function_Name" => $row['Parent_Function_Name'],
                "Parent_Function_Display" => $row['Parent_Function_Display'],
                "Parent_Function_Status" => $row["Parent_Function_Status"],
                "action" => "UpdateParentMenu",
                "Parent_Function_Root" => $row['Parent_Function_Root']);
            $_i = $_i + 1;
        }
    }
    echo json_encode($_Datatable);
}

if ($_POST['action'] == "FillChildEditData") {
    $editid = $_POST['editid'];
    $response = $emp->FillChildEditData($editid);
    $_DataTable = array();
    $_i = 0;
    if ($response[0] == 'Success') {
        while ($row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("Function_Code" => $row['Function_Code'],
                "Function_Name" => $row['Function_Name'],
                "Function_URL" => $row['Function_URL'],
                "Function_Parent" => $row["Function_Parent"],
                "Function_Display" => $row["Function_Display"],
                "action" => "UpdateChildMenu",
                "Function_Status" => $row['Function_Status']);
            $_i = $_i + 1;
        }
    }
    echo json_encode($_Datatable);
}
if ($_action == "UpdateRootMenu") {
    if (isset($_POST["txtRootMenu"])) {
        $_RootName = strtoupper($_POST["txtRootMenu"]);
        $_Status = $_POST["ddlStatus"];
        $_DisplayOrder = $_POST['txtDisplayOrder'];
        $_Code = $_POST['txtRootCode'];
        $response = $emp->UpdateRootMenu($_RootName, $_Status, $_DisplayOrder, $_Code);
        if ($response[0] == "Successfully Updated") {
            echo "update";
        } else if ($response[0] == "Already Exists") {
            echo "duplicate";
        } else {
            echo "error";
        }
    }
}
if ($_action == "UpdateParentMenu") {
    if (isset($_POST["txtParentFunctionName"])) {
        $_RootCode = $_POST["ddlRootParent"];
        $_ParentName = strtoupper($_POST["txtParentFunctionName"]);
        $_DisplayOrder = $_POST["txtDisplayOrderParent"];
        $_Status = $_POST["ddlStatusParent"];
        $_ParentCode = $_POST["txtParentCode"];
        $response = $emp->UpdateParentMenu($_RootCode, $_ParentName, $_DisplayOrder, $_Status, $_ParentCode);
        if ($response[0] == "Successfully Updated") {
            echo "update";
        } else if ($response[0] == "Already Exists") {
            echo "duplicate";
        } else {
            echo "error";
        }
    }
}
if ($_action == "UpdateChildMenu") {

    if (isset($_POST["txtFunctionName"])) {
        $_ChildName = $_POST["txtFunctionName"];
        //$_RootCode = $_POST["ddlRootFunction"];
        $_ParentCode = $_POST["ddlParentFunction"];
        $_FunctionURL = $_POST["txtFunctionURL"];
        $_DisplayOrder = $_POST["txtDisplayOrderFunction"];
        $_Status = $_POST["ddlStatusFunction"];
        $_ChildCode = $_POST["txtChildCode"];
        $response = $emp->UpdateChildMenu($_ChildName, $_ParentCode, $_FunctionURL, $_DisplayOrder, $_Status, $_ChildCode);
        if ($response[0] == "Successfully Updated") {
            echo "success";
        } else if ($response[0] == "Already Exists") {
            echo "duplicate";
        } else {
            echo "error";
        }
    }
}
?> 
