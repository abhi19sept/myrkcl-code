<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsOwnershipFeePayment.php';

$response = array();
$emp = new clsOwnershipFeePayment();


if ($_action == "APPROVE") {
    $response = $emp->GetDatabyCode();
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "orgcourse" => $_Row['Org_Application_Type'],
            "doctype" => $_Row['Organization_DocType'],
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            "ack" => $_Row['Org_Ack']);
        $_i = $_i + 1;
    }

        echo json_encode($_DataTable);
    } else {
        echo "";
    }
}

if ($_action == "CheckPayStatus") {
    // print_r($_POST);
    $response = $emp->CheckPayStatus($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Org_PayStatus'];
}

if ($_action == "ChkPayEvent") {

    $response = $emp->ChkPayEvent();

    $num_rows = mysqli_num_rows($response[2]);

    if ($response[0] == "Success") {
        echo "1";
    } else {
        echo "0";
    }
}

if ($_action == "ADD") {
    $responses = $emp->CheckPayStatus($_POST['txtAck']);
    $_row = mysqli_fetch_array($responses[2]);
    $paystatus = $_row['Org_PayStatus'];

    $response = $emp->GetOwnershipAmt();
    $_rows = mysqli_fetch_array($response[2]);
    $amt = $_rows['Ownership_Fee_Amount'];
    
//echo $amt;
    if (! $amt || $paystatus == '1') {
        echo "0";
    } else {
        //echo $response = $emp->AddOwnershipPayTran($amt, $_POST);
        $productinfo = 'OwnershipFeePayment';
        $trnxId = $emp->AddOwnershipPayTran($amt, $_POST, $productinfo);
        if ($_POST['gateway'] == 'razorpay' && !empty($trnxId) && $trnxId != 'TimeCapErr') {
            require("razorpay.php");
        } else {
            echo $trnxId;
        }
    }
}
