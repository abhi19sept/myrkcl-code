<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsRenewedItgkList.php';

    $response = array();
    $emp = new clsRenewedItgkList();

    if ($_action == "DETAILS") {
		
			$response = $emp->GetITGKDetails($_POST['values']);
			$_DataTable = "";	
		
			echo "<div class='table-responsive'>";
			echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
			echo "<thead>";
			echo "<tr>";
			echo "<th style='5%'>S No.</th>";
			echo "<th style='5%'>ITGK-CODE</th>";
			echo "<th style='15%'>ITGK Name</th>";
			
			if($_POST['values']=='RS-CFA') {
				echo "<th style='10%'>Center Type</th>";
			}
			
			echo "<th style='5%'>RSP Code</th>";
			echo "<th style='5%'>RSP Name</th>";
			echo "<th style='10%'>ITGK Created Date</th>";
			echo "<th style='10%'>ITGK Expire Date</th>";
			echo "<th style='10%'>ITGK District</th>";
			echo "<th style='10%'>ITGK Tehsil</th>";
			echo "<th style='10%'>ITGK Mobile</th>";
			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			if($response[0] == 'Success') {
			$_Count = 1;
			while ($_Row = mysqli_fetch_array($response[2])) {
				echo "<tr class='odd gradeX'>";
				echo "<td>" . $_Count . "</td>";
				echo "<td>" . $_Row['Courseitgk_ITGK'] . "</td>";
				echo "<td>" . strtoupper($_Row['ITGK_Name']) . "</td>";
				 if($_POST['values']=='RS-CFA') {
					 if($_Row['Courseitgk_EOI']=='12') {
						 echo "<td>" . 'New' . "</td>"; 
					 }
					 else {
						  echo "<td>" . 'Renewed' . "</td>";
					 }
				 }
				
				echo "<td>" . $_Row['RSP_Code'] . "</td>";            
				echo "<td>" . strtoupper($_Row['RSP_Name']) . "</td>";            
				echo "<td>" . date_format(date_create($_Row['CourseITGK_UserCreatedDate']), "d-m-Y") . "</td>";            
				echo "<td>" . date_format(date_create($_Row['CourseITGK_ExpireDate']), "d-m-Y") . "</td>";   
				echo "<td>" . strtoupper($_Row['District_Name']) . "</td>";   
				echo "<td>" . strtoupper($_Row['Tehsil_Name']) . "</td>";   
				echo "<td>" . strtoupper($_Row['ITGK_Mobile']) . "</td>";   
				echo "</tr>";
				$_Count++;
			}
			echo "</tbody>";
			echo "</table>";
			echo "</div>";
		}
	 }
	 
if ($_action == "FillCourse") {
    $response = $emp->GetAllCourse();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Courseitgk_Course'] . "'>" . $_Row['Courseitgk_Course'] . "</option>";
    }
}
?>