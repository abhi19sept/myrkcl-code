<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsAddressChangeVisitRpt.php';

$response = array();
$emp = new clsAddressChangeVisitRpt();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th colspan='3'>IT-GK Details</th>";
    echo "<th colspan='6'>IT-GK Existing Details</th>";
    echo "<th colspan='6'>IT-GK Requested Details</th>";
    echo "<th colspan='3'>Dates and Remarks</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='40%'>Existing IT-GK Address</th>"; 
    echo "<th style='40%'>Existing IT-GK Tehsil</th>"; 
    echo "<th style='10%'>Existing IT-GK  AreaType</th>";
    
    echo "<th style='10%'>Existing Municipality Type: (Urban)/Existing Panchayat Samiti(Rural)</th>";
    echo "<th style='10%'>Existing Municipality Name: (Urban)/Existing Gram Panchayat(Rural)</th>";
    echo "<th style='10%'>Existing Ward No: (Urban)/Existing Village(Rural)</th>";
        
    echo "<th style='40%'>Requested IT-GK Address</th>";
    echo "<th style='40%'>Requested IT-GK Tehsil</th>";
    echo "<th style='40%'>Requested IT-GK AreaType</th>";
    
    echo "<th style='10%'>Requested Municipality Type: (Urban)/Requested Panchayat Samiti(Rural)</th>";
    echo "<th style='10%'>Requested Municipality Name: (Urban)/Requested Gram Panchayat(Rural)</th>";
    echo "<th style='10%'>Requested Ward No.: (Urban)/Requested Village(Rural)</th>";
        
    echo "<th style='40%'>Application Date</th>";
    echo "<th style='10%'>SP Approval Date</th>";
    echo "<th style='10%'>Remarks By SP</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['fld_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Address_old'] . "</td>";
            echo "<td>" . $_Row['Organization_Tehsil_old'] . "</td>";
            echo "<td>" . $_Row['Organization_AreaType_old'] . "</td>";
            if($_Row['Organization_AreaType_old'] == 'Urban'){
                echo "<td>" . ($_Row['Organization_Municipal_Type_old'] ? $_Row['Organization_Municipal_Type_old'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Organization_Municipal_old'] ? $_Row['Organization_Municipal_old'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Organization_WardNo_old'] ? $_Row['Organization_WardNo_old'] : 'NA') . "</td>";
            } elseif ($_Row['Organization_AreaType_old'] == 'Rural'){
                echo "<td>" . ($_Row['Organization_Panchayat_old'] ? $_Row['Organization_Panchayat_old'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Organization_Gram_old'] ? $_Row['Organization_Gram_old'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Organization_Village_old'] ? $_Row['Organization_Village_old'] : 'NA') . "</td>";
            }else {
                echo "<td>" . "NA" . "</td>";
                echo "<td>" . "NA" . "</td>";
                echo "<td>" . "NA" . "</td>";
            }
            echo "<td>" . $_Row['Organization_Address'] . "</td>";
            echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";
            echo "<td>" . $_Row['Organization_AreaType'] . "</td>";
            
            if($_Row['Organization_AreaType'] == 'Urban'){
                echo "<td>" . ($_Row['Organization_Municipal_Type'] ? $_Row['Organization_Municipal_Type'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Organization_Municipal'] ? $_Row['Organization_Municipal'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Organization_WardNo'] ? $_Row['Organization_WardNo'] : 'NA') . "</td>";
            } elseif ($_Row['Organization_AreaType'] == 'Rural'){
                echo "<td>" . ($_Row['Organization_Panchayat'] ? $_Row['Organization_Panchayat'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Organization_Gram'] ? $_Row['Organization_Gram'] : 'NA') . "</td>";
                echo "<td>" . ($_Row['Organization_Village'] ? $_Row['Organization_Village'] : 'NA') . "</td>";
            }else {
                echo "<td>" . "NA" . "</td>";
                echo "<td>" . "NA" . "</td>";
                echo "<td>" . "NA" . "</td>";
            }
            
            echo "<td>" . $_Row['submission_date'] . "</td>";
            echo "<td>" . $_Row['submission_date_sp'] . "</td>";
            echo "<td>" . $_Row['fld_remarks'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

