<?php

include 'commonFunction.php';
require 'BAL/clsvisitmaster.php';
//require 'cflog_Bio_enrollment.php';
require '../DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);

date_default_timezone_set("Asia/Kolkata");

set_time_limit(0);
//mysqli_set_charset('utf8');

$response = array();
$emp = new clsvisitmaster();

$superUsers = [1, 4];

if ($_action == "enrollas") {
    echo '<option value="">Select Type</option>';
    if ($_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == '15') {
        echo '<option value="Faculty">Faculty</option>';
        echo '<option value="Coordinator">Coordinator</option>';
        echo '<option value="Counsellor">Counsellor</option>';
    }
    if ($_SESSION['User_UserRoll'] == '4') {
        echo '<option value="RM">RM</option>';
    }
    if ($_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == 1) {
        echo '<option value="Employee">Employee</option>';
        echo '<option value="Owner">Owner</option>';
    }
    if (in_array($_SESSION['User_UserRoll'], $superUsers)) {
        echo '<option value="DPO">DPO</option>';
    }
}

//if ($_action == "listvisits") {
//    print_r($_POST);
//    if (!$_POST['dateFrom'] && !$_POST['dateTo']) {
//        exit;
//    }
//    $status = (in_array($_SESSION['User_UserRoll'], $superUsers) || !empty($_POST['VisitCode'])) ? 1 : 0;
//    $response = $emp->getVisitList($_POST['VisitCode'], $_POST, $status);
//    $_DataTable = "";
//    if ($response) {
//        echo "<div class='table-responsive'>";
//        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
//        echo "<thead>";
//        echo "<tr>";
//        if (!$_POST['VisitCode']) {
//            echo "<th>SNo.</th>";
//        }
//        echo "<th nowrap>Request Code </th>";
//        echo "<th nowrap>Visit Date</th>";
//        echo "<th nowrap>Confirm Date</th>";
//        if (isset($response[0]['User_Code']) && $_SESSION['User_Code'] != $response[0]['User_Code'])
//            echo "<th nowrap>Organization Name</th>";
//        echo "<th nowrap>ITGK Code </th>";
//        echo "<th nowrap>ITGK Name</th>";
//        echo "<th >District</th>";
//        echo "<th >Tehsil</th>";
//        echo "<th nowrap>Visit Status</th>";
//        if (in_array($_SESSION['User_UserRoll'], $superUsers)) {
//            echo "<th nowrap>Overall Feedback</th>";
//        }
//        if (!$_POST['VisitCode']) {
//            echo "<th>Action</th>";
//        }
//        echo "</tr>";
//        echo "</thead>";
//        echo "<tbody>";
//        $_Count = 1;
//        foreach ($response as $_Row) {
//            $status = ($_Row['status']) ? (($_Row['feedbackdata']) ? 'Feedback Completed' : 'Feedback Pending') : 'Not Completed';
//            $action = (!$_Row['status']) ? '<img title="Delete" class="aslink" id="del-' . $_Row['id'] . '" src="./images/settingsDelete.gif"/>' : (($_Row['feedbackdata']) ? '<img title="Download Feedback" class="aslink" height="30" id="dfb-' . $_Row['id'] . '" src="./images/dwnpdf.png"/>' : ((!in_array($_SESSION['User_UserRoll'], $superUsers)) ? '<img title="Feedback" class="aslink" id="fb-' . $_Row['id'] . '" src="./images/settingsEdit.gif"/>' : ''));
//            
//            echo "<tr class='odd gradeX'>";
//            if (!$_POST['VisitCode']) {
//                echo "<td>" . $_Count . "</td>";
//            }
//            echo "<td>" . $_Row['id'] . "</td>";
//            echo "<td>" . $_Row['visit_date'] . "</td>";
//            echo "<td>" . (($_Row['confirm_date']) ? date("d-m-Y", $_Row['confirm_date']) : '-') . "</td>";
//            if ($_SESSION['User_Code'] != $_Row['User_Code'])
//                echo "<td>" . $_Row['Organization_Name'] . "</td>";
//            echo "<td>" . $_Row['itgk_code'] . "</td>";
//            echo "<td>" . $_Row['ITGK_NAME'] . "</td>";
//            echo "<td>" . $_Row['District_Name'] . "</td>";
//            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
//            echo "<td>" . $status . "</td>";
//            if (in_array($_SESSION['User_UserRoll'], $superUsers)) {
//                $feedback = ($_Row['feedbackdata']) ? unserialize($_Row['feedbackdata']) : ['txtOverAllRemark' => ''];
//                echo "<td>" . $feedback['txtOverAllRemark'] . "</td>";
//            }
//            if (!$_POST['VisitCode']) {
//                echo "<td>" . $action . "</td>";
//            }
//            echo "</tr>";
//            $_Count++;
//        }
//        echo "</tbody>";
//        echo "</table>";
//        echo "</div>";
//    } else {
//        //echo "<center><b>No visit request found.</b></center>";
//    }
//}

if ($_action == "listvisits") {
//	if (in_array($_SESSION['User_UserRoll'], $superUsers) && ! $_POST['dateFrom'] && ! $_POST['dateTo']) {
//    	exit;
//    }
    $status = (in_array($_SESSION['User_UserRoll'], $superUsers) || !empty($_POST['VisitCode'])) ? 1 : 0;
    $response = $emp->getVisitList($_POST['VisitCode'], $_POST, $status);
    $_DataTable = "";
    if ($response) {
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    if (!$_POST['VisitCode']) {
    	echo "<th>SNo.</th>";
	}
    echo "<th nowrap>Request Code </th>";
    echo "<th nowrap>Visit Date</th>";
    echo "<th nowrap>Confirm/Close Date</th>";
    //if (isset($response[0]['User_Code']) && $_SESSION['User_Code'] != $response[0]['User_Code']) echo "<th nowrap>Visitor User Name</th>";
    echo "<th nowrap>Visitor User Name</th>";
    echo "<th nowrap>Visited SP/ITGK Code </th>";
    echo "<th nowrap>ITGK Name</th>";
	echo "<th >District</th>";
	echo "<th >Tehsil</th>";
	echo "<th nowrap>Visit Status</th>";
        echo "<th nowrap>Overall Feedback</th>";
        echo "<th nowrap>Visit Photos</th>";
//	if (in_array($_SESSION['User_UserRoll'], $superUsers)) {
//		echo "<th nowrap>Overall Feedback</th>";	
//	}
	if (!$_POST['VisitCode']) {
		echo "<th>Action</th>";
	}
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    foreach ($response as $_Row) {
    	//$status = ($_Row['status']) ? (($_Row['feedbackdata']) ? 'Feedback Completed' : 'Feedback Pending') : 'Not Completed';
        if($_Row['status'] == '1'){
            if($_Row['feedbackdata']){
              $status = 'Feedback Completed';
            } else {
               $status = 'Feedback Pending'; 
            }
        } elseif($_Row['status'] =='0') {
            $status = 'Not Completed';
        } elseif($_Row['status'] =='2') {
            $status = 'Visit Closed';
        }
        if($_Row['status'] == '1'){
           $actionvalue = '1'; 
        } elseif($_Row['status'] =='0'  || $_Row['status'] =='2') {
            $actionvalue = '0';
        }
    	$action = (!$actionvalue) ? '<img title="Delete" class="aslink" id="del-' . $_Row['id'] . '" src="./images/settingsDelete.gif"/>' : (($_Row['feedbackdata']) ? '<img title="Download Feedback" class="aslink" height="30" id="dfb-' . $_Row['id'] . '" src="./images/dwnpdf.png"/>' : ((!in_array($_SESSION['User_UserRoll'], $superUsers)) ? '<img title="Feedback" class="aslink" id="fb-' . $_Row['id'] . '" src="./images/settingsEdit.gif"/>' : '<img title="Feedback" class="aslink" id="fb-' . $_Row['id'] . '" src="./images/settingsEdit.gif"/>'));
    //    $action = (!$_Row['status']) ? '<img title="Delete" class="aslink" id="del-' . $_Row['id'] . '" src="./images/settingsDelete.gif"/>' : (($_Row['feedbackdata']) ? '<img title="Download Feedback" class="aslink" height="30" id="dfb-' . $_Row['id'] . '" src="./images/dwnpdf.png"/>' : '<img title="Feedback" class="aslink" id="fb-' . $_Row['id'] . '" src="./images/settingsEdit.gif"/>');
        echo "<tr class='odd gradeX'>";
        if (!$_POST['VisitCode']) {
        	echo "<td>" . $_Count . "</td>";
    	}
        echo "<td>" . $_Row['id'] . "</td>";
        echo "<td>" . $_Row['visit_date'] . "</td>";
        if($_Row['status'] =='1' || $_Row['status'] =='0'){
        echo "<td>" . (($_Row['confirm_date']) ? date("d-m-Y", $_Row['confirm_date']) : '-') . "</td>";
        } else {
         echo "<td>" . (($_Row['close_date']) ?  $_Row['close_date'] : '-') . "</td>";   
        }
       // if ($_SESSION['User_Code'] != $_Row['User_Code']) echo "<td>" . $_Row['Organization_Name'] . "</td>";
        echo "<td>" . $_Row['Organization_Name'] . "</td>";
        echo "<td>" . $_Row['itgk_code'] . "</td>";
        echo "<td>" . $_Row['ITGK_NAME'] . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
        echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
		echo "<td>" . $status . "</td>";
                if ($status == 'Feedback Completed'){ 
                echo "<td>" . ($_Row['overall_feedback'] ? $_Row['overall_feedback'] : 'NA') . "</td>";
                } elseif($status == 'Visit Closed') {
                    echo "<td> Visit Closed </td>";  
                } else {
                echo "<td> Feedback Pending </td>";    
                }
                if ($status == 'Feedback Completed'){
                echo "<td class='details-control'><button id='itgkAddmission_".$_Row['id']."' type='button' class='clicklist_action' data-toggle='modal' data-target='#itgkAddmission_list'>View Photos</button>";    
                } elseif($status == 'Visit Closed') {
                    echo "<td> Visit Closed </td>";  
                }else{
                echo "<td> Feedback Pending </td>";    
                }
//		if (in_array($_SESSION['User_UserRoll'], $superUsers)) {
////			$feedback = ($_Row['feedbackdata']) ? unserialize($_Row['feedbackdata']) : ['txtOverAllRemark'=>''];
////			echo "<td>" . $feedback['txtOverAllRemark'] . "</td>";
//                    echo "<td>" . (($_Row['feedbackdata']) ? unserialize($_Row['feedbackdata']) : '') . "</td>";
//		}
		if (!$_POST['VisitCode']) {
			echo "<td>" . $action . "</td>";
//                     if($_Row['status'] =='0'){
//	echo "<img title='Delete' class='aslink' id='del-" . $_Row['id'] . " src='./images/settingsDelete.gif'/>" ;  
// } elseif ($_Row['status'] =='1' && $_Row['feedbackdata'] == ''){
//	echo "<img title='Feedback' class='aslink' id='fb-" . $_Row['id'] . " src='./images/settingsEdit.gif'/>";
//	 
// } elseif ($_Row['status'] =='1' && $_Row['feedbackdata'] != '' && $_SESSION['User_UserRoll']!='7'){
//echo "<img title='Download Feedback' class='aslink' height='30' id='dfb-" . $_Row['id'] . " src='./images/dwnpdf.png'/>"; 
// }  elseif ($_Row['status'] =='1' && $_Row['feedbackdata'] != '' && $_SESSION['User_UserRoll']='7'){
//echo	'Feedback Completed'; 
// }
		}
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
	} else {
		//echo "<center><b>No visit request found.</b></center>";
	}
}

if ($_action == 'showDataITGKpopup') {
    global  $_ObjFTPConnection;
    $details= $_ObjFTPConnection->ftpdetails();
    $response = $emp->getVisitPhotos($_POST['itgk']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $photopath = '/VISITPHOTO/';
        $ExteriorPhoto = $_Row["exterior_photo"];
        $ExteriorImg =  file_get_contents($details.$photopath.$ExteriorPhoto);
        $ExtData = base64_encode($ExteriorImg);
        $showExt =  "<iframe id='ftpimg' src='data:image/png;base64, ".$ExtData."' alt='Red dot' width='100%' height='500px'></iframe>";
        
        $ReceptionPhoto = $_Row["reception_photo"];
        $ReceptionImg =  file_get_contents($details.$photopath.$ReceptionPhoto);
        $ReceptionData = base64_encode($ReceptionImg);
        $showReception =  "<iframe id='ftpimg' src='data:image/png;base64, ".$ReceptionData."' alt='Red dot' width='100%' height='500px'></iframe>";
        
        $ClassPhoto = $_Row["classroom_photo"];
        $ClassImg =  file_get_contents($details.$photopath.$ClassPhoto);
        $ClassData = base64_encode($ClassImg);
        $showClass =  "<iframe id='ftpimg' src='data:image/png;base64, ".$ClassData."' alt='Red dot' width='100%' height='500px'></iframe>";
        
        $LabPhoto = $_Row["lab_photo"];
        $LabImg =  file_get_contents($details.$photopath.$LabPhoto);
        $LabData = base64_encode($LabImg);
        $showLab =  "<iframe id='ftpimg' src='data:image/png;base64, ".$LabData."' alt='Red dot' width='100%' height='500px'></iframe>";
        
        $_DataTable[$_i] = array("extphoto" => $showExt,
            "recphoto" => $showReception,
            "classphoto" => $showClass,
            "labphoto" => $showLab);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
    
}

if ($_action == 'listitgk') {
    $response = $emp->getITGKList();
    echo '<option value="">Select ITGK</option>';
    if ($response) {
        foreach ($response as $itgkCode => $row) {
            echo '<option value="' . $itgkCode . '">' . $itgkCode . '</option>';
        }
    }
}

if ($_action == 'rspitgk') {
    $response = $emp->getITGKList('', $_POST['rsp_code']);
    echo '<option value="">Select ITGK</option>';
    if ($response) {
        foreach ($response as $itgkCode => $row) {
            echo '<option value="' . $itgkCode . '">' . $itgkCode . '</option>';
        }
    }
}

if ($_action == 'getITGKInfo') {
    $visitInfo = $emp->getVisitList($_POST['VisitCode']);
    if ($visitInfo) {
        $visit = $visitInfo[0];
        $itgkInfo = $emp->getITGKList($visit['itgk_code']);
        if ($itgkInfo) {
            $infoArray = $itgkInfo[$visit['itgk_code']];
            $itgkStaffInfo = $emp->getITGKStaffInfo($visit['itgk_code']);
            if ($itgkStaffInfo) {
                $infoArray = array_merge($infoArray, $itgkStaffInfo);
            }
            $itgkInfra = $emp->getITGKInfraDetails($visit['itgk_code']);
            if ($itgkInfra) {
                $infoArray = array_merge($infoArray, $itgkInfra);
            }
            $itgkSystems = $emp->getITGKSyatemDetails($visit['itgk_code']);
            if ($itgkSystems) {
                $infoArray = array_merge($infoArray, $itgkSystems);
            }

            echo json_encode($infoArray);
        }
    }
}

if ($_action == 'itgkinfo') {
    $response = $emp->getITGKList1($_POST['itgk']);
    if ($response) {
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>ITGK Name</th>";
        echo "<th >District</th>";
        echo "<th >Tehsil</th>";
        echo "<th>Email </th>";
        echo "<th>Address </th>";
        echo "<th >Mobile</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        foreach ($response as $itgkCode => $_Row) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['User_EmailId'] . "</td>";
            echo "<td>" . $_Row['Organization_Address'] . "</td>";
            echo "<td>" . $_Row['User_MobileNo'] . "</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
}

if ($_action == 'setvisit') {
    $itgkCode = $_POST['itgk'];
    if (empty($itgkCode) || empty($_POST['vdate'])) {
        echo "Invalid input values !!";
        exit;
    }
    $vDate = explode('-', $_POST['vdate']);
    $curdate = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
    $visitDate = mktime(0, 0, 0, $vDate[1], $vDate[0], $vDate[2]);
    if ($visitDate < $curdate) {
        echo "New visit date should not be in past !!";
        exit;
    }
    if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
        echo $response = $emp->setNewVisit($_POST['itgk'], $_POST['vdate'], $_SESSION['User_LoginId']);
    } else {
        echo "Oops ! Something went wrong, Please re-try.";
    }
}

if ($_action == "doAction") {
    
    $act = explode('-', $_POST['id']);
    if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
        if ($act[0] == 'del') {
            echo $response = $emp->deleteVisit($act[1], $_SESSION['User_LoginId']);
        } elseif ($act[0] == 'dfb') {
            $filePath = 'upload/visitFeedbacks/visit_feedback_' . $act[1] . '.pdf';
            $baseFilePath = $_SERVER['DOCUMENT_ROOT'] . '/myrkcl/' . $filePath;
            if (file_exists($baseFilePath)) {
                echo $filePath;
            } else {
                 echo $emp->generateFeedbackPdf($act[1]); 
            }
        }
    }
}


if ($_action == "listcentervisits") {
    $response = $emp->getVisitListForITGK($_POST['visitId']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Request Code </th>";
    echo "<th>Visit Date</th>";
    echo "<th>Schedule By</th>";
    echo "<th>Email</th>";
    echo "<th>Contact</th>";
    echo "<th nowrap>Visit Status</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    foreach ($response as $_Row) {
        $status = ($_Row['status']) ? 'Confirmed' : 'Pending';
        $action = (!$_Row['status'] && empty($_POST['visitId'])) ? '<img title="Confirm" class="aslink" id="confirm-' . $_Row['id'] . '" src="./images/settingsEdit.gif"/>' : (($_Row['status'] && $_Row['feedbackdata']) ? 'Feedback Completed' : '');
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['id'] . "</td>";
        echo "<td>" . $_Row['visit_date'] . "</td>";
        echo "<td nowrap>" . $_Row['Organization_Name'] . " (" . $_Row['User_LoginId'] . ")" . "</td>";
        echo "<td>" . $_Row['User_EmailId'] . "</td>";
        echo "<td>" . $_Row['User_MobileNo'] . "</td>";
        echo "<td>" . $status . "</td>";
        echo "<td>" . $action . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == 'biodevices') {
    $result = '<option value="">- - - Please Select - - -</option>';
    if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
        $response = $emp->getBioDevices($_SESSION['User_LoginId']);
        if ($response) {
            foreach ($response as $row) {
                $result .= '<option value="' . strtolower($row['BioMatric_Make']) . '">' . $row['BioMatric_Make'] . ' (Model: ' . $row['BioMatric_Model'] . ')</option>';
            }
        }
    }

    echo $result;
}

if ($_action == 'verifybiodevice') {
    $result = 0;
    if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
        $response = $emp->getBioDevices($_SESSION['User_LoginId'], $_POST['serial']);
        if ($response)
            $result = 1;
    }

    echo $result;
}

if ($_action == 'getvisitorBioPics' || $_action == 'getitgkBioPics') {
    $response = $emp->getVisitListForITGK($_POST['visitId']);
     //m_log("getvisitorBioPics : getVisitListForITGK ".json_encode($response) . " " . date('Y-m-d h:i:s'));
    $result = [];
    if ($response && isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
         //m_log("action ".$_action . " " . date('Y-m-d h:i:s'));
        $userCode = ($_action == 'getitgkBioPics') ? $emp->getUserCodeOfITGK($response[0]['itgk_code']) : $response[0]['request_by'];
        $result = $emp->getBioEnrolledList($userCode, 1);
          //m_log("getvisitorBioPics : getBioEnrolledList ".json_encode($result) . " " . date('Y-m-d h:i:s'));
        //print_r($result);
        if ($result) {
            echo json_encode($result);
        } else {
            echo '0';
        }
    } else {
        echo '0';
    }
}

if ($_action == 'bioenrolledlist') {
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Name</th>";
    echo "<th>Enrolled As</th>";
    echo "<th>Mobile</th>";
    echo "<th>Email</th>";
    echo "<th>Status</th>";
    if ($_SESSION['User_UserRoll'] != 4) {
        echo "<th>Action</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
        $response = $emp->getBioEnrolledList2($_SESSION['User_Code']);
        //m_log("bioenrolledlist : getBioEnrolledList ".json_encode($response) . " " . date('Y-m-d h:i:s'));
        $_Count = 1;
        foreach ($response as $_Row) {
            $status = ($_Row['status']) ? 'Active' : 'Inactive';
            if ($_SESSION['User_UserRoll'] != 4) {
                $action = (!$_Row['status']) ? '<img title="Activate" class="aslink" id="Activate-' . $_Row['id'] . '" src="./images/arrowicon.gif"/>' : '<img title="Deactivate" class="aslink" id="Deactivate-' . $_Row['id'] . '" src="./images/drop.png"/>';
            }
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['fullname'] . "</td>";
            echo "<td>" . $_Row['employee_type'] . "</td>";
            echo "<td>" . $_Row['mobile'] . "</td>";
            echo "<td>" . $_Row['email_id'] . "</td>";
            echo "<td><span id='biostatus_" . $_Row['id'] . "'>" . $status . "</span></td>";
            if ($_SESSION['User_UserRoll'] != 4) {
                echo "<td>" . $action . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == 'bioenrolledpics') {
    $result = [];
    if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
        $result = $emp->getBioEnrolledList1($_SESSION['User_Code'], 0, 'all');
        //m_log("bioenrolledpics : getBioEnrolledList ".json_encode($result) . " " . date('Y-m-d h:i:s'));
        if ($result) {
            echo json_encode($result);
        } else {
            echo '1';
        }
    } else {
        echo '0';
    }
}

if ($_action == 'bioRegister') {
    if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
        $userCode = (isset($_POST['dpouser']) && !empty($_POST['dpouser'])) ? $_POST['dpouser'] : ((isset($_POST['itgkuser']) && !empty($_POST['itgkuser'])) ? $_POST['itgkuser'] : ((isset($_POST['rspuser']) && !empty($_POST['rspuser'])) ? $_POST['rspuser'] : $_SESSION['User_Code']));
        if (isset($_POST['dpouser']) && !empty($_POST['dpouser'])) {
            $addedBy = $_POST['dpouser'];
        } else {
            $addedBy = $_SESSION['User_Code'];
        }

        echo $response = $emp->processBioEnrollment($_POST, $userCode, $addedBy);
        //m_log("bioRegister : processBioEnrollment ".json_encode($response) . " " . date('Y-m-d h:i:s'));
    } else {
        echo "Unable to register, please re-try!!";
    }
}

if ($_action == 'changeEnrollStatus') {
    if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
        $act = explode('-', $_POST['elemId']);
        $emp->changeEnrollStatus($act[1], $act[0], $_SESSION['User_Code']);
    }
}

if ($_action == 'GetDetails') {
    $response = $emp->GetDetails($_POST['visitId']);
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("itgk" => $_Row['itgkid'],
                "visitor" => $_Row['visitorid']);
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "You are not Auhorized to use this functionality on MYRKCL.";
    }
}


//if ($_action == 'changeVisitStatus') {
//    $verifyId = $emp->validateVisitWithConfirmingBy($_POST['visitId'], $_POST['mtigk'], $_POST['mvisitor']);
//    if ($verifyId && isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
//        echo $emp->setVisitConfirmed($verifyId, $_SESSION['User_LoginId'], $_POST['mvisitor'], $_POST['mtigk']);
//    } else {
//        echo 'Oops!! Something went worng, please re-try to confirm.';
//    }
//}

if ($_action == 'changeVisitStatus') {
    if ((isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) && (isset($_POST['visitId']) && !empty($_POST['visitId']))) {
        echo $emp->setVisitConfirmed($_POST['visitId'], $_SESSION['User_LoginId'], $_POST['mvisitor'], $_POST['mtigk']);
    } else {
        echo 'Oops!! Something went worng, please re-try to confirm.';
    }
}

if ($_action == "savefeedback") {
    global $_ObjFTPConnection;

    $response = $emp->getVisitList($_POST['VisitCode']);
    $_DataTable = "";
    if ($response) {
        $_GeneratedId = $_POST["txtGenerateId"];
        $_OverallFeedback = $_POST["txtOverAllRemark"];
        
        $owntypeimg = $_FILES['uploadImage7']['name'];
        $owntypetmp = $_FILES['uploadImage7']['tmp_name'];
        $owntypetemp = explode(".", $owntypeimg);
        //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
        $owntypefilename = $_GeneratedId . '_exterior.' . end($owntypetemp);
        $owntypefilestorepath = "../upload/VISITPHOTO/" . $owntypefilename;
        $owntypeimageFileType = pathinfo($owntypefilestorepath, PATHINFO_EXTENSION);
        $Extfilestorepathftp = "/VISITPHOTO";
        $Ext_response_ftp=ftpUploadFile($Extfilestorepathftp,$owntypefilename,$owntypetmp);
        

        $salpiimg = $_FILES['uploadImage8']['name'];
        $salpitmp = $_FILES['uploadImage8']['tmp_name'];
        $salpitemp = explode(".", $salpiimg);
        $salpifilename = $_GeneratedId . '_reception.' . end($salpitemp);
        $salpifilestorepath = "../upload/VISITPHOTO/" . $salpifilename;
        $salpiimageFileType = pathinfo($salpifilestorepath, PATHINFO_EXTENSION);
        $Receptionfilestorepathftp = "/VISITPHOTO";
        $Reception_response_ftp=ftpUploadFile($Receptionfilestorepathftp,$salpifilename,$salpitmp);

        $panpiimg = $_FILES['uploadImage9']['name'];
        $panpitmp = $_FILES['uploadImage9']['tmp_name'];
        $panpitemp = explode(".", $panpiimg);
        $panpifilename = $_GeneratedId . '_classroom.' . end($panpitemp);
        $panpifilestorepath = "../upload/VISITPHOTO/" . $panpifilename;
        $panpiimageFileType = pathinfo($panpifilestorepath, PATHINFO_EXTENSION);
        $Classroomfilestorepathftp = "/VISITPHOTO";
        $Classroom_response_ftp=ftpUploadFile($Classroomfilestorepathftp,$panpifilename,$panpitmp);

        $ccpiimg = $_FILES['uploadImage6']['name'];
        $ccpitmp = $_FILES['uploadImage6']['tmp_name'];
        $ccpitemp = explode(".", $ccpiimg);
        $ccpifilename = $_GeneratedId . '_lab.' . end($ccpitemp);
        $ccpifilestorepath = "../upload/VISITPHOTO/" . $ccpifilename;
        $ccpiimageFileType = pathinfo($ccpifilestorepath, PATHINFO_EXTENSION);
        $Labfilestorepathftp = "/VISITPHOTO";
        $Lab_response_ftp=ftpUploadFile($Labfilestorepathftp,$ccpifilename,$ccpitmp);

        if (($_FILES["uploadImage7"]["size"] < 200000 && $_FILES["uploadImage7"]["size"] > 100000) || ($_FILES["uploadImage8"]["size"] < 200000 && $_FILES["uploadImage8"]["size"] > 100000) || ($_FILES["uploadImage9"]["size"] < 200000 && $_FILES["uploadImage9"]["size"] > 100000) || ($_FILES["uploadImage6"]["size"] < 200000 && $_FILES["uploadImage6"]["size"] > 100000)) {

            if ($owntypeimageFileType == "jpg" && $salpiimageFileType == "jpg" && $panpiimageFileType == "jpg" && $ccpiimageFileType == "jpg") {

                //if (move_uploaded_file($owntypetmp, $owntypefilestorepath) && move_uploaded_file($salpitmp, $salpifilestorepath) && move_uploaded_file($panpitmp, $panpifilestorepath) && move_uploaded_file($ccpitmp, $ccpifilestorepath)) {
                if(trim($Ext_response_ftp) == "SuccessfullyUploaded" && trim($Reception_response_ftp) == "SuccessfullyUploaded" && trim($Classroom_response_ftp) == "SuccessfullyUploaded" && trim($Lab_response_ftp) == "SuccessfullyUploaded"){    
                    echo $response = $emp->saveVisitFeedback($_POST, $owntypefilename, $salpifilename, $panpifilename, $ccpifilename, $_OverallFeedback);
                } else {
                    echo "Visit Photo not upload. Please try again.";
                }
            } else {
                echo "Sorry, File Not Valid";
            }
        } else {
            echo "File Size should be between 100KB to 200KB.";
        }
    } else {
        echo "<p class='error'><span>Invalid visit Id.</span></p>";
    }
}

if ($_action == "getvisitfeedback") {
    $response = $emp->getVisitList($_POST['VisitCode']);
    $_DataTable = "";
    if ($response) {
        $response = $emp->getVisitFeedback($_POST['VisitCode'], 0);
        if ($response) {
            echo json_encode(unserialize($response));
        }
    }
}

if ($_action == 'fetchCenterBatches') {
    $response = $emp->getVisitList($_POST['VisitCode']);
    $_DataTable = "";
    if ($response) {
        $response = $emp->fetchCenterBatches($response[0]['itgk_code']);
        if ($response) {
            $total = count($response);
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
            echo "<tbody>";
            echo "<tr>";
            echo "<td><b>Batches</b></td>";
            $j = 1;
            for ($i = 0; $i < $total; $i++) {
                echo "<td><b>" . $response[$i]['Batch_Name'] . "</b> <input type='hidden' id='batch" . $j . "' name='batch" . $j . "' value='" . $response[$i]['Batch_Name'] . "' /></td>";
                $j++;
            }
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>RS-CIT Learners</b></td>";
            $j = 1;
            for ($i = 0; $i < $total; $i++) {
                echo "<td><b>" . $response[$i]['n'] . "</b> <input type='hidden' id='lCount" . $j . "' name='lCount" . $j . "' value='" . $response[$i]['n'] . "' /></td>";
                $j++;
            }
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Learner Presence</b></td>";
            $j = 1;
            for ($i = 0; $i < $total; $i++) {
                echo "<td><b>" . $response[$i]['n'] . "</b> <input type='hidden' id='lPresence" . $j . "' name='lPresence" . $j . "' value='" . $response[$i]['n'] . "' /></td>";
                $j++;
            }
            echo "</tr>";
            echo "<tr>";
            echo "<td><b>Learners Feedback Collected</b></td>";
            $j = 1;
            for ($i = 0; $i < $total; $i++) {
                echo "<td><label class='radio-inline'> <input type='radio' id='LearnerFeedback" . $j . "' value='Yes' checked='checked' name='LearnerFeedback" . $j . "' /><b>Yes </b></label> <label class='radio-inline'> <input type='radio' id='LearnerFeedbackNo" . $j . "' value='No' name='LearnerFeedback" . $j . "' /><b>No</b></label></td>";
                $j++;
            }
            echo "</tr>";
            echo "</tbody></table></div>";
        }
    }
}

if ($_action == 'dpolist') {
    if (in_array($_SESSION['User_UserRoll'], $superUsers)) {
        $response = $emp->getUserList('dpo');
        if ($response) {
            echo '<select class="form-control" name="dpouser" id="dpouser">';
            foreach ($response as $row) {
                echo '<option value="' . $row['User_Code'] . '">' . $row['User_LoginId'] . '</option>';
            }
            echo '</select>';
        }
    }
}

if ($_action == 'itgklist') {
    if ($_SESSION['User_UserRoll'] == 1) {
        $response = $emp->getUserList('itgk');
        if ($response) {
            echo '<label for="edistrict">ITGK:<span class="star">*</span><select class="form-control" name="itgkuser" id="itgkuser">';
            foreach ($response as $row) {
                echo '<option value="' . $row['User_Code'] . '">' . $row['User_LoginId'] . '</option>';
            }
            echo '</select>';
        }
    }
}

if ($_action == 'rsplist') {
    if ($_SESSION['User_UserRoll'] == 1) {
        $response = $emp->getUserList('rsp');
        if ($response) {
            echo '<label for="edistrict">SP:<span class="star">*</span><select class="form-control" name="rspuser" id="rspuser">';
            foreach ($response as $row) {
                echo '<option value="' . $row['User_Code'] . '">' . $row['User_LoginId'] . '</option>';
            }
            echo '</select>';
        }
    }
}

if ($_action == 'getrsplist') {
    echo '<option value="">Select RSP</option>';
    if (in_array($_SESSION['User_UserRoll'], $superUsers)) {
        $response = $emp->getUserList('rsp');
        if ($response) {
            foreach ($response as $row) {
                echo '<option value="' . $row['User_Code'] . '">' . $row['User_LoginId'] . '</option>';
            }
        }
    }
}

if ($_action == 'setmobileotp') {
    if (in_array($_SESSION['User_UserRoll'], $superUsers)) {
        echo 'nootp';
    } else {
        echo $emp->setOTP(trim($_POST['mobile']), trim($_POST['email']));
    }
}
?>