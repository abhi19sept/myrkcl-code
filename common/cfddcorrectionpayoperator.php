<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsddcorrectionpayoperator.php';

$response = array();
$emp = new clsddcorrectionpayoperator();

if ($_action == "VERIFYDETAILS") {
        $response = $emp->GetAllDETAILS($_POST['values'],$_POST['correction']);
		//print_r($response[2]);
        $_DataTable = array();
        $_i=0;
		$co = mysqli_num_rows($response[2]);
		if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("Admission_Name" => $_Row['cfname'],
                "Admission_Fname"=>$_Row['cfaname'],
                "Admission_Mobile"=>$_Row['mobile'],
				"Admission_Email"=>$_Row['emailid'],
				"Correction_Id"=>$_Row['cid'],
				"ITGK" =>$_Row['Correction_ITGK_Code']);
            $_i=$_i+1;
        }
         echo json_encode($_Datatable);
		}
		else {
			echo "";
		}
    }
	
	if ($_action == "Update") {  
    //print_r($_POST);
    if (isset($_POST["txtCenterCode"])) {
        $_Code = filter_var($_POST["txtCenterCode"], FILTER_SANITIZE_STRING);
		$_LearnerCode = filter_var($_POST["txtLCode"], FILTER_SANITIZE_STRING);
        $_PayTypeCode = filter_var($_POST["productinfo"], FILTER_SANITIZE_STRING);
        //$_PayType = filter_var($_POST["productinfocode"], FILTER_SANITIZE_STRING);
        $_TranRefNo = filter_var($_POST["txtGenerateId"], FILTER_SANITIZE_STRING);

        $_firstname = filter_var($_POST["firstname"], FILTER_SANITIZE_STRING);
        $_phone = filter_var($_POST["phone"], FILTER_SANITIZE_STRING);
        $_email = filter_var($_POST["email"], FILTER_SANITIZE_STRING);
		$_cid = filter_var($_POST["txtcid"], FILTER_SANITIZE_STRING);

        $_amount = filter_var($_POST["amount"], FILTER_SANITIZE_STRING);
        $_ddno = filter_var($_POST["ddno"], FILTER_SANITIZE_STRING);
        $_dddate = filter_var($_POST["dddate"], FILTER_SANITIZE_STRING);

        $_txtMicrNo = filter_var($_POST["txtMicrNo"], FILTER_SANITIZE_STRING);
        $_ddlBankDistrict = filter_var($_POST["ddlBankDistrict"], FILTER_SANITIZE_STRING);
        $_ddlBankName = filter_var($_POST["ddlBankName"], FILTER_SANITIZE_STRING);
        $_txtBranchName = filter_var($_POST["txtBranchName"], FILTER_SANITIZE_STRING);

        $response = $emp->Update($_Code, $_LearnerCode, $_PayTypeCode, $_TranRefNo, $_firstname, $_phone, $_email, $_amount, $_ddno, $_dddate, $_txtMicrNo, $_ddlBankDistrict, $_ddlBankName, $_txtBranchName, $_cid);
        echo $response[0];
    }
}

?>
