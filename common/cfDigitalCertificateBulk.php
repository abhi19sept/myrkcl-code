<?php

include './commonFunction.php';
require 'BAL/clsDigitalCertificateBulk.php';

$response = array();
$emp = new clsDigitalCertificate();


function changeDateFormatStandered($date) {
    $timestamp = strtotime($date);
    return $new_date_format = date('d-M-Y', $timestamp);
}

if ($_action == "FILLLinkAadharCourse") {
    $response = $emp->GetCourseCertificate();
    echo "<option value=''>Select Exam Event</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "GETDATAITGKWISE") {
    $response = $emp->GetLearnerListITGK($_POST['event']);
    $_Row = mysqli_fetch_array($response[2]);
    $_DataTable = array();
    $_DataTable[] = array("noofcertificatepending" => $_Row['noofcertificatepending'],
		    "noofcertificategenterated" => $_Row['noofcertificategenterated'],
                    "signedcertificate" => $_Row['signedcertificate']);
    echo json_encode($_DataTable);
}
if ($_action == "SIGNCERTIFICATES") {
    $designation = "Registrar";
    $department = "Examination";
    $vmouregistrar = $emp->getSignAuthoriser($designation, $department);
    if($vmouregistrar[0] == 'Success'){
        $event = $_POST['event'];
        $regstrar = mysqli_fetch_array($vmouregistrar[2]);
        $signedNameReg = $regstrar['Sign_Name'];
        $signeId = $regstrar['Sign_Sno'];
        $response = $emp->ShowLearnerEcertificate($event);
           while($_Row = mysqli_fetch_array($response[2])){
                $filepath = "../upload/digitalEcertificate/".$event."/" . $_Row['scol_no'] . "_certificate.pdf";
                $date = date_create($_Row['result_date']);
                $result_date = date_format($date, "d/m/Y");
                $datedob = date_create($_Row['dob']);
                $dob = date_format($datedob, "d/m/Y");
                $documentname = "" . $_Row['scol_no'] . "_RSCIT";
                $b64Doc = base64_encode(file_get_contents($filepath));
               echo $request ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sign="http://sign.api.emSigner/">
                            <soapenv:Header/>
                            <soapenv:Body>
                               <sign:signPdf>
                                  <!--Optional:-->
                                  <arg0>' . $documentname . '</arg0>
                                  <!--Optional:-->
                                  <arg1>' . $signeId . '</arg1>
                                  <!--Optional:-->
                                  <arg2>' . $b64Doc . '</arg2>
                                  <!--Optional:-->
                                  <arg3></arg3>
                                  <!--Optional:-->
                                  <arg4>218,100,355,145</arg4>
                                  <!--Optional:-->
                                  <arg5>all</arg5>
                                  <!--Optional:-->
                                  <arg6>Document Approve</arg6>
                                  <!--Optional:-->
                                  <arg7>Kota (Rajasthan)</arg7>
                                  <!--Optional:-->
                                  <arg8>true</arg8>
                               </sign:signPdf>
                            </soapenv:Body>
                         </soapenv:Envelope> 
                         ';
                $header = array(
                    "Content-type: text/xml;charset=\"utf-8\"",
                    "Accept: text/xml",
                    "Cache-Control: no-cache",
                    "Pragma: no-cache",
                    "SOAPAction: \"run\"",
                    "Content-length: " . strlen($request),
                );

                $soap_do = curl_init();
                //print_r($soap_do);
                curl_setopt($soap_do, CURLOPT_URL, "http://staging.rkcl.co.in:7080/emSigner/services/dsverifyWS?wsdl");
                //curl_setopt($soap_do, CURLOPT_URL, "http://10.1.1.158:7080/emSigner/services/dsverifyWS?wsdl");
                curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
                curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
                curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($soap_do, CURLOPT_POST, true);
                curl_setopt($soap_do, CURLOPT_POSTFIELDS, $request);
                curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);

                $result = curl_exec($soap_do);
                print_r($result);
                $doc = new DOMDocument('1.0', 'utf-8');
                $doc->loadXML($result);
                $XMLresults = $doc->getElementsByTagName("return");
                $output = $XMLresults->item(0)->nodeValue;
                if (isset($output)) {
                    if ($output == '' || empty($output)) {
                       echo "Response Blanck" ;
                    }else if (strpos ($output, "failure") !== false ) {
                       echo "Response Failed" ;
                    }else{
                         $path1 = '../upload/singedDecertificate/'.$event;
                        if(!file_exists($path1)) {
                            makeDir($path1);
                        }
                        $filestorepath = "../upload/singedDecertificate/".$event."/";
                        $DocumentTitle = $documentname;
                       // $file = $Res[2];
                        $file = $output;
                        $pdf_decoded = base64_decode($file);
                        $genfile = $filestorepath . $DocumentTitle . '.pdf';
                        $pdf = fopen($genfile, 'w');
                        fwrite($pdf, $pdf_decoded);
                        fclose($pdf);
                        if (file_exists($genfile)) {
                            $response1 = $emp->UpdateEcertificateStatus($_Row['scol_no'], $_Row['exameventnameID'], $_Row['study_cen']);
                        }
                    }
                    curl_close($soap_do);
                } 
           }
    }
}
if ($_action == "GENRATECERTIFICATES") {
    $designation = "Registrar";
    $department = "Examination";
    $vmouregistrar = $emp->getSignAuthoriser($designation, $department);
    if($vmouregistrar[0] == 'Success'){
        $event = $_POST['event'];
        $path = '../upload/digitalEcertificate/';
        $qrpath = $path . '/qrcodes/';
        makeDir($qrpath);
        $cropperpath = $path . '/cropper';
        makeDir($cropperpath);
        $masterPdf = $path . '/certificate_verified.pdf';
        if (!file_exists($masterPdf)) {
            die('Main pdf not found.');
        }
        $regstrar = mysqli_fetch_array($vmouregistrar[2]);
        $signedNameReg = $regstrar['Sign_Name'];
        $signeId = $regstrar['Sign_Sno'];
        $results = $emp->getLearnerResults($event, $signedNameReg);
        if (!mysqli_num_rows($results[2])) {
            die('No Record Found!!');
        }
        $path1 = '../upload/digitalEcertificate/'.$event;
        if(!file_exists($path1)) {
            makeDir($path1);
        }
        while($centerRow = mysqli_fetch_assoc($results[2])){
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $centerRow['result_date'])) {
                $centerRow['result_date'] = date("d-m-Y", strtotime($centerRow['result_date']));
            }
            $filepath = '../upload/digitalEcertificate/'.$event.'/' . $centerRow['learnercode'] . '_certificate.pdf';
            if (file_exists($filepath)) {
                unlink($filepath);
            }
            $photo = getPath('photo', $centerRow);
            $fdfPath = generateFDF($centerRow, $path);
            if ($fdfPath) {
                generateCertificate($centerRow, $path, $fdfPath, $masterPdf, $photo, $event);
                if (file_exists($filepath)) {
                    /*   Add Certificate to Rajevault  */
                    $status = $emp->UpdateStatus($event, $centerRow['learnercode']);
                }
            }
        }
    }
    
}

/* Generate PDF Functions */

function getPath($type, $row) {
    global $general;
    $batchname = trim(ucwords($row['batchname']));
    $coursename = $row['coursename'];
    $isGovtEmp = stripos($coursename, 'gov');
    if ($isGovtEmp) {
        $batchname = $batchname . '_Gov';
    } else {
        $isWoman = stripos($coursename, 'women');
        if ($isWoman) {
            $batchname = $batchname . '_Women';
        } else {
            $isMadarsa = stripos($coursename, 'madar');
            if ($isMadarsa) {
                $batchname = $batchname . '_Madarsa';
            }
        }
    }

    $batchname = str_replace(' ', '_', $batchname);
    $dirPath = '../upload/admission_' . $type . '/' . $row['learnercode'] . '_' . $type . '.png'; 

    $imgPath = $dirPath;
    $imgPath = str_replace('//', '/', $imgPath);

    $dirPathJpg = str_replace('.png', '.jpg', $dirPath);
    $imgPathJpg = str_replace('.png', '.jpg', $imgPath);

    $imgBatchPath = '../upload/admission_' . $type . '/' . $batchname . '/' . $row['learnercode'] . '_' . $type . '.png';
    $imgBatchPath = str_replace('//', '/', $imgBatchPath);
    $imgBatchPathJpg = str_replace('.png', '.jpg', $imgBatchPath);

    $path = '';
    if (file_exists($imgBatchPath)) {
        $path = $imgBatchPath;
    } elseif (file_exists($imgBatchPathJpg)) {
        $path = $imgBatchPathJpg;
    } elseif (file_exists($imgPath)) {
        $path = $imgPath;
    } elseif (file_exists($imgPathJpg)) {
        $path = $imgPathJpg;
    } else {
        
    }

    return $path;
}

function makeDir($path) {
    return is_dir($path) || mkdir($path);
}

function generateFDF($data, $path) {
    $fdfContent = '%FDF-1.2
		%ÃƒÂ¢ÃƒÂ£Ãƒ?Ãƒâ€œ
		1 0 obj 
		<<
		/FDF 
		<<
		/Fields [';
    foreach ($data as $key => $val) {
        $fdfContent .= '
				<<
				/V (' . $val . ')
				/T (' . $key . ')
				>> 
				';
    }
    $fdfContent .= ']
		>>
		>>
		endobj 
		trailer
		<<
		/Root 1 0 R
		>>
		%%EOF';

    $filePath = $path . '/' . $data['learnercode'] . '.fdf';
    file_put_contents($filePath, $fdfContent);
    $return = '';
    if (file_exists($filePath)) {
        $return = $filePath;
    }

    return $return;
}

function generateCertificate($centerRow, $path, $fdfPath, $masterPdf, $photo, $event) {
    $resultPath = $path .'/'.$event.'/' . $centerRow['learnercode'] . '_certificate.pdf';

    $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : (($_SERVER['HTTP_HOST'] == "staging.rkcl.co.in") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"');

    $command = $pdftkPath . '  ' . $masterPdf . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
    exec($command);
    unlink($fdfPath);
    addPhotoInPDF($centerRow, $photo, $path, $resultPath, $event);
}

function addPhotoInPDF($learnercode, $Imagepath, $path, $pdfPath, $event) {

    require_once('fpdi/fpdf/fpdf.php');
    require_once('fpdi/fpdi.php');
    require_once('fpdi/fpdf_tpl.php');
    require_once("cropper.php");

    global $general;
    global $cropperpath;
    $qrResource = $picResource = 0;
    $qrpath = '';
    $qrpath = generateQRCode($learnercode, $path, $event);
    if (!empty($qrpath) && file_exists($qrpath)) {
        $qrResource = imagecreatefromstring(file_get_contents($qrpath));
    }
    if (!empty($Imagepath) && file_exists($Imagepath)) {
        $picResource = imagecreatefromstring(file_get_contents($Imagepath));
    }

    $pdf = new FPDI();
    $pdf->AddPage();
    $pdf->setSourceFile($pdfPath);
    $template = $pdf->importPage(1);
    $pdf->useTemplate($template);
    try {
        if (!empty($qrpath) && $qrResource) {
            $pdf->Image($qrpath, 25, 238, 25, 25);
        }
    } catch (Exception $_ex) {
        
    }

    try {
        if (!empty($Imagepath) && $picResource)
            $pdf->Image($Imagepath, 92.8, 79.5, 29, 32.8);
    } catch (Exception $_ex) {
        $pdf->Output($pdfPath, "F");

        if (!empty($Imagepath) && file_exists($Imagepath)) {
            $cropper = new Cropper($cropperpath);
            $args = array("size" => array(30, 30), "image" => $Imagepath, "placeholder" => 2);
            $newImgPath = $cropper->crop($args);

            if (file_exists($newImgPath)) {
                addPhotoInPDF($learnercode['learnercode'], $newImgPath, $path, $pdfPath, $event);
            }
        }
        return;
    }

    $pdf->Output($pdfPath, "F");
}

function generateQRCode($learnercode, $path, $event) {
    $qrdir = $path.'/qrcodes/'.$event;
    if(!file_exists($qrdir)){
       makeDir($qrdir); 
    }
    if (is_array($learnercode) || is_object($learnercode)) {
        $json_string = encrypt_decrypt('encrypt', json_encode($learnercode));
        $secure = base64_encode($json_string);
        //$contents = file_get_contents('http://localhost/myrkcl_git/QRCodeGenerater.php?data=' . $secure . '');
       $contents = file_get_contents('https://myrkcl.com/QRCodeGenerater.php?data=' . $secure . '');
       $qrpath = $path.'/qrcodes/'.$event.'/' . $learnercode['learnercode'] . '.png';
       $file = file_put_contents($qrpath, $contents);
        if (file_exists($qrpath)) {
            $qrpath;
            return $qrpath;
        }
        else {
            echo 'An error occurred.';
        }
        return $qrpath; 
    }
}

if ($_action == "ReadQrCode") {
//    echo $data = $_POST['data'] . "<br>";
//    echo $str = encrypt_decrypt('decrypt', $data);
}

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-128-CBC";
    $secret_key = '1212121211121212';
    $secret_iv = 'axwepghrtycgarvt'; 
    $key = substr(hash('sha256', $secret_key), 0, 16);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, OPENSSL_RAW_DATA, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, OPENSSL_RAW_DATA, $iv);
    }
    return $output;
}

?>