<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

$_UploadDirectory = $_SERVER['DOCUMENT_ROOT'] . '/upload/';

//$_UploadDirectory = '/files';
// Check for errors
if ($_FILES['SelectedFile']['error'] > 0) {
    outputJSON("<span class='error'>An error ocurred when uploading.</span>");
}

/* if(!getimagesize($_FILES['SelectedFile']['tmp_name'])){
  outputJSON('Please ensure you are uploading an image.');
  } */

// Check filetype
/* if($_FILES['SelectedFile']['type'] != 'image/png'){
  outputJSON('Unsupported filetype uploaded.');
  } */

// Check filesize
if ($_FILES['SelectedFile']['size'] > 20000000) {
    outputJSON("<span class='error'>" . $_FILES['SelectedFile']['size'] . "File uploaded exceeds maximum upload size.</span>");
}

$ext = pathinfo($_FILES['SelectedFile']['name'], PATHINFO_EXTENSION);
//$ext="xls";
// Check if the file exists
if ($ext == "xls") {
   /* if (file_exists($_UploadDirectory . $_POST['UploadId'] . "." . $ext)) {
        //outputJSON("<span class='error'>File with that name already exists.</span>");
        unlink($_UploadDirectory . $_POST['UploadId'] . "." . $ext);
    }*/

// Upload file

    if (file_exists($_UploadDirectory)) {
        if (is_writable($_UploadDirectory)) {
			date_default_timezone_set('Asia/Calcutta');
			 $login = date("Y-m-d_H-i-s");
            if (!copy($_FILES['SelectedFile']['tmp_name'], $_UploadDirectory . $_POST['UploadId'] . "_" . $login . "." . $ext)) {
                outputJSON("<span class='error'>Error uploading file - check destination is writeable.</span>");
            }
        } else {
            outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
        }
    } else {
        outputJSON("<span class='error'>Upload Folder Not Available</span>");
    }

// Success!
//outputJSON('File uploaded successfully to "' . 'upload/' . $_POST['UploadId'] . "." . $ext . '".', 'success');

    ini_set("display_errors", 1);
    require_once 'excel_reader2.php';
//require_once 'db.php';

   $data = new Spreadsheet_Excel_Reader($_UploadDirectory . $_POST['UploadId'] . "_" . $login . "." . $ext);

	//echo "Total Sheets in this xls file: ".count($data->sheets)."<br /><br />";

    $ColumnHeader = array();
    $DataArray = array();
    $html = "";
    $CenterCodeList = "";
    //$html .= "<span class='error'>" . count($data->sheets[0]['cells']) . "</span>";
    if (count($data->sheets[0]['cells']) >= 2) { // checking sheet not empty
        //$html .= "<span class='error'>" . $data->sheets[0]['cells'][1][1] . $data->sheets[0]['cells'][1][2] . $data->sheets[0]['cells'][1][3] . "</span>";
        if ($data->sheets[0]['cells'][1][1] == "CenterCode" && $data->sheets[0]['cells'][1][2] == "Remark") {
            $html .= "<table border='0' cellpedding='0' cellspacing='0' width='100%' class='gridview'>";
            //echo "Sheet $i:<br /><br />Total rows in sheet $i  ".count($data->sheets[$i][cells])."<br />";
            $i = 0;
            for ($j = 1; $j <= count($data->sheets[0]['cells']); $j++) { // loop used to get each row of the sheet
                if ($j == 1) {
                    $html.="<thead>";

                    $html.="<tr>";
                    for ($k = 1; $k <= count($data->sheets[0]['cells'][$j]); $k++) { // This loop is created to get data in a table format.
                        $html.="<th>";
                        $html.=$data->sheets[0]['cells'][$j][$k];
                        $html.="</th>";
                    }
                    $html.="</tr>";
                    $html.="</thead>";
                    $html.="<tbody>";
                } else {
                    $html.="<tr class='odd gradeX'>";

                    for ($k = 1; $k <= count($data->sheets[0]['cells'][$j]); $k++) { // This loop is created to get data in a table format.
                        
                            $html.="<td>";
                            $html.=$data->sheets[0]['cells'][$j][$k];
                            $html.="</td>";
                            $DataArray[$i][$k - 1] = $data->sheets[0]['cells'][$j][$k];                       
                    }
                    $html.="</tr>";
                }
                $i++;
            }
            $html.="</tbody>";
            $html.="</table>";
        } else {
            $html .= "<span class='error'>Pleaseeeee Select Pre defined (.xls) File Format</span>";
        }
    } 
	
	else {
        $html .= "<span class='error'>Please HI Select Pre defined (.xls) File Format</span>";
    }

    session_start();
    $_SESSION['DataArray'] = $DataArray;
//print_r($DataArray);
    outputJSON($html, 'success');
} 

else {
    outputJSON("<span class='error'>Invalid File Formet. Please Upload .xls Files</span>");
}