<?php

/*
 * @author Abhi

 */
include 'commonFunction.php';
require 'BAL/clsApplyLuckyDrawApplication.php';

$response = array();
$emp = new clsApplyLuckyDrawApplication();

if ($_action == "FILLCourse") {
    $response = $emp->GetAdmissionCourse(); 
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLBatch") {
    $response = $emp->FILLAdmissionBatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "SHOWALL") {
    $response = $emp->GetAll($_POST['batch'], $_POST['course']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th>Father Name</th>";
    echo "<th>D.O.B</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "<th>Action</th>";
    //echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        echo "<td>" . $_Row['Admission_DOB'] . "</td>";

        if ($_Row['Admission_Photo'] != "") {
            $image = $_Row['Admission_Photo'];
            echo "<td align='center'>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
        } else {
            echo "<td align='center'>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
        }
        if ($_Row['Admission_Sign'] != "") {
            $sign = $_Row['Admission_Sign'];
            echo "<td align='center'>" . '<img alt="No Image Found" width="80" height="35" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
        } else {
            echo "<td align='center'>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
        }
        if(isset($_Row["applystatus"]) && !empty($_Row["applystatus"])){
            echo "<td>Already Applied</td>";
        }
        else{
            echo "<td><input type='button' class='btn-primary btn-md approvalLetter btn-block' course='".$_Row['Admission_Course']."' batch='" . $_Row['Admission_Batch'] . "' mode='ShowConfirm' lcode='" . $_Row['Admission_LearnerCode'] . "' value='Apply for Scheme'/></td>";
        }
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
if ($_action == "GETLEARNERDATA") {
    $response = $emp->LcodeDetail($_POST['batch'], $_POST['course'], $_POST['lcode']);
    // print_r($response);
    // die;
    $_DataTable = array();
    $_i = 0;
    if (mysqli_num_rows($response[2])) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_DataTable[$_i] = array("AdmissionCode" => $_Row['Admission_Code'],
                "lcode" => $_Row['Admission_LearnerCode'],
                "lname" => strtoupper($_Row['Admission_Name']),
                "fname" => strtoupper($_Row['Admission_Fname']),
                "dob" => $_Row['Admission_DOB'],
                "itgkcode" => $_Row['Admission_ITGK_Code'],
                "district" => $_Row['District_Name'],
                "tehsil" => $_Row['Tehsil_Name'],
                "address" => strtoupper($_Row['Admission_Address']),
                "pin" => $_Row['Admission_PIN'],
                "mobile" => $_Row['Admission_Mobile'],
                "itgkname" => strtoupper($_Row['Organization_Name']),
                "itgkdistrict" => $_Row['Organization_District'],
                "itgktehsil" => $_Row['Organization_Tehsil'],
                "email" => $_Row['Admission_Email'],
                "photo" => $_Row['Admission_Photo'],
                "sign" => $_Row['Admission_Sign']);
            $_i = $_i + 1;
        }
        echo json_encode($_DataTable);
    }
}
if ($_action == "UpdLearner") {
    if (isset($_POST["lcodes"]) && !empty($_POST["lcodes"]) && isset($_POST["acode"]) && !empty($_POST["acode"])) {
        if ((isset($_POST["tenboardroll"]) && !empty($_POST["tenboardroll"])) || (isset($_POST["tweboardroll"]) && !empty($_POST["tweboardroll"]))) {

            if (isset($_POST["itgktehsil"]) && !empty($_POST["itgktehsil"]) && isset($_POST["itgkdistrict"]) && !empty($_POST["itgkdistrict"])) {
            if (!empty($_FILES['admitcard']['tmp_name'])) {

                $admitcardimg = $_FILES['admitcard']['name'];
                $admitcardtmp = $_FILES['admitcard']['tmp_name'];
                $admitcardtemp = explode(".", $admitcardimg);
                $admitcardfilename = $_POST["lcodes"] . '_admitcard.pdf';
                $admitcardfilestorepath = "../upload/admission_fee_waiver_scheme/" . $admitcardfilename;
                $admitcardimageFileType = pathinfo($admitcardfilestorepath, PATHINFO_EXTENSION);
            } else {

                $admitcardfilename = '';
            }
            if (!empty($_FILES['lsign']['tmp_name'])) {

                $pic = '1';
                $lsignimg = $_FILES['lsign']['name'];
                $lsigntmp = $_FILES['lsign']['tmp_name'];
                $lsigntemp = explode(".", $lsignimg);
                $lsignfilename = $_POST["lcodes"] . '_lsign.png';
                $lsignfilestorepath = "../upload/admission_fee_waiver_scheme/" . $lsignfilename;
                $lsignimageFileType = pathinfo($lsignfilestorepath, PATHINFO_EXTENSION);
            } else {

                $lsignfilename = '';
            }
            if (!empty($_FILES['itgksign']['tmp_name'])) {

                $pic = '1';
                $itgksignimg = $_FILES['itgksign']['name'];
                $itgksigntmp = $_FILES['itgksign']['tmp_name'];
                $itgksigntemp = explode(".", $itgksignimg);
                $itgksignfilename = $_POST["lcodes"] . '_itgksign.png';
                $itgksignfilestorepath = "../upload/admission_fee_waiver_scheme/" . $itgksignfilename;
                $itgksignimageFileType = pathinfo($itgksignfilestorepath, PATHINFO_EXTENSION);
            } else {

                $itgksignfilename = '';
            }
            if (($_FILES["admitcard"]["size"] < 2000000 && $_FILES["admitcard"]["size"] > 100000) || ($_FILES["itgksign"]["size"] < 5000 && $_FILES["itgksign"]["size"] > 1000) || ($_FILES["lsign"]["size"] < 5000 && $_FILES["lsign"]["size"] > 1000)){

                if ($admitcardimageFileType == "pdf" && $itgksignimageFileType == "png" && $lsignimageFileType == "png") {

                    if (move_uploaded_file($admitcardtmp, $admitcardfilestorepath) && move_uploaded_file($lsigntmp, $lsignfilestorepath) && move_uploaded_file($itgksigntmp, $itgksignfilestorepath)) {
                        
                                $responses = $emp->Add($_POST, $admitcardfilename, $itgksignfilename, $lsignfilename);
                                //print_r($responses); 
                                echo $responses[0];
                           
                      
                    } else {
                        // echo $lphotofilestorepath;
                        // echo $docprooffilestorepath;
                        echo "Documents not upload. Please try again.";
                    }
                } else {
                    echo "Sorry, File Not Valid";
                }
            } else {
                echo "Doc File Size Incorrect.";
            }
            } else {
            echo "Inavalid Entry";
            }
            
            } else {
            echo "Atleast one Acadamic Detail is Mandatory";
            }
        }
        else {
        echo "Inavalid Entry";
    }
}