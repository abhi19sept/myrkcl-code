<?php

/* 
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsPanchayatSamitiMaster.php';

$response = array();
$emp = new clsPanchayatSamitiMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        $_PanchayatName = $_POST["name"];
        $_Parent=$_POST["parent"];
        $_Status=$_POST["status"];

        $response = $emp->Add($_PanchayatName,$_Parent,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_PanchayatName = $_POST["name"];
        $_Parent=$_POST["parent"];
        $_Status=$_POST["status"];
        $_Code=$_POST['code'];
        $response = $emp->Update($_Code,$_PanchayatName,$_Parent,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("PanchayatCode" => $_Row['Block_Code'],
            "PanchayatName" => $_Row['Block_Name'],
            "District" => $_Row['Block_District'],
            "Status"=>$_Row['Block_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll($_actionvalue);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='25%'>Name</th>";
    echo "<th style='20%'>Parent</th>";
    echo "<th style='20%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Block_Name'] . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
         echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmPanchayatSamitiMaster.php?code=" . $_Row['Block_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmPanchayatSamitiMaster.php?code=" . $_Row['Block_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll($_actionvalue);
    echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Block_Code'] . ">" . $_Row['Block_Name'] . "</option>";
    }
}