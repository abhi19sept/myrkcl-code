<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsOrgPrintReceipt.php';

$response = array();
$emp = new clsOrgPrintReceipt();


if ($_action == "ShowDetails") {
    $response = $emp->GetAll($_POST['role'], $_POST['status']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Organization Name</th>";
    echo "<th>IT-GK Acknowledgement No</th>";
    echo "<th>Organization Type</th>";
    echo "<th>Organization District</th>";
    echo "<th>Organization Tehsil</th>";
    echo "<th>Organization Email</th>";
    echo "<th>Organization Mobile</th>";
    echo "<th>Payment Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Ack']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Type']) . "</td>";
        echo "<td>" . $_Row['Organization_District'] . "</td>";
        echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";
        echo "<td>" . $_Row['Org_Email'] . "</td>";
        echo "<td>" . $_Row['Org_Mobile'] . "</td>";

        if ($_Row['Org_Status'] == 'Pending' && $_Row['Org_PayStatus'] == '0') {
            echo "<td> <a href='frmncrddpayment.php?code=" . $_Row['Org_Ack'] . "&Mode=Edit'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary btn-xs' value='NCR Payment thru DD'/></a>"
            . "</td>";
        } else if ($_Row['Org_Status'] == 'Pending' && $_Row['Org_PayStatus'] == '8') {
            echo "<td>Payment Approval Pending at RKCL "
            //. "<input type='button' name='Approved' id='Approved' class='btn btn-primary' value='Payment Approval Pending at RKCL'/>"
            
            . "</td>";
        } else if ($_Row['Org_Status'] == 'Pending' && $_Row['Org_PayStatus'] == '1') {
            echo "<td>Payment Confirmed"
            //. "<input type='button' name='Approved' id='Approved' class='btn btn-primary' value='Payment Confirmed'/>"
            . "</td>";
        } else if ($_Row['Org_Status'] == 'Approved') {
            echo "<td>Payment Confirmed"
            //. "<input type='button' name='Approved' id='Approved' class='btn btn-primary' value='Payment Confirmed'/>"
            . "</td>";
        } else {
            echo "<td>"
            . "<input type='button' name='Rejected' id='Rejected' class='btn btn-primary' value='Rejected By RM'/>"
            . "</td>";
        }

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        if ($_Row['Org_Role'] == '14') {
            $_role = 'RSP';
        } elseif ($_Row['Org_Role'] == '15') {
            $_role = 'IT-GK';
        }
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "doctype" => $_Row['Organization_DocType'],
            "role" => $_role,
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            "district" => $_Row['Organization_District_Name'],
            "districtcode" => $_Row['Organization_District'],
            "tehsil" => $_Row['Organization_Tehsil'],
            "street" => $_Row['Organization_Street'],
            "road" => $_Row['Organization_Road'],
            "orgdoc" => $_Row['Organization_ScanDoc']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "UPDATE") {
    $_OrgCode = $_POST["orgcode"];
    $_Status = $_POST["status"];
    $_Remark = $_POST["remark"];
    $_DistrictCode = $_POST["districtcode"];
    if ($_Status == "Reject") {
        $response = $emp->UpdateOrgMaster($_OrgCode, $_Status, $_Remark);
        echo $response[0];
    } elseif ($_Status == "Approve") {
        $response = $emp->UpdateToCreateLogin($_OrgCode, $_Status, $_DistrictCode);
        echo $response[0];
    }
}