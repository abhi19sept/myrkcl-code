<?php
/* 
 * author Viveks
 */
include 'commonFunction.php';
require 'BAL/clsRootMenu.php';

$response = array();
$emp = new clsRootMenu();

if ($_action == "ADD") {
    if (isset($_POST["name"])) {
        $_RootName = $_POST["name"];
        $_Status=$_POST["status"];
        $_DisplayOrder=$_POST['display'];
        $response = $emp->Add($_RootName,$_Status,$_DisplayOrder);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_RootName = $_POST["name"];
        $_Status=$_POST["status"];
        $_Code=$_POST['code'];
        $_DisplayOrder=$_POST['display'];
        $response = $emp->Update($_Code,$_RootName,$_Status,$_DisplayOrder);
        echo $response[0];
    }
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("RootCode" => $_Row['Root_Menu_Code'],
            "RootName" => $_Row['Root_Menu_Name'],
            "Status"=>$_Row['Root_Menu_Status'],
            "Display"=>$_Row['Root_Menu_Display']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}

if ($_action == "DELETE") {
    $response = $emp->DeleteRecord($_actionvalue);
    echo $response[0];
}

if ($_action == "SHOW") {
    //echo "Show";
    $response = $emp->GetAll();
    $_DataTable = "";

    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Menu Name</th>";
    echo "<th style='10%'>Display Order</th>";
    echo "<th style='30%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Root_Menu_Name'] . "</td>";
         echo "<td>" . $_Row['Root_Menu_Display'] . "</td>";
         echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmrootmenu.php?code=" . $_Row['Root_Menu_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmrootmenu.php?code=" . $_Row['Root_Menu_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Root</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Root_Menu_Code'] . ">" . $_Row['Root_Menu_Name'] . "</option>";
    }
}