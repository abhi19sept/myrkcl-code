<?php

/*
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsAdmissionEnquirySummary.php';

$response = array();
$emp = new clsAdmissionEnquirySummary();

if ($_action == "GETDATA") {

    $response2 = $emp->EnquiryReport($_POST['course'], $_POST['batch']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th>Total Uploaded Enquiries</th>";
    echo "<th>Total Confirmed Enquiries</th>";
	// echo "<th >Uploaded Enquiry By Learner In Selected Batch</th>";
 //    echo "<th >Enquiry Converted to Admission(Without Payment) In Current Month</th>";
 //     echo "<th>Enquiry Payment Confirmed In Current Month</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;
    $_Totaleconvert= 0;
	$_Totaleconfirm = 0;
    $co = mysqli_num_rows($response2[2]);
    if ($co) {
        while ($_Row2 = mysqli_fetch_array($response2[2])) {
// print_r($_Row2);
// die;
            $response = $emp->GetDataAll($_Row2['RoleName'], $_POST['course'], $_POST['batch']);
            $_Row = mysqli_fetch_array($response[2]);

            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";

            echo "<td>" . $_Row2['RoleName'] . "</td>";
            // echo "<td><input type='button' class='updcount' course='".$_Row2['Course']."' batch='" . $_Row2['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowUpload' value='" . $_Row2['uploadcount'] . "'/></td>";

            //  echo "<td><input type='button' class='updcount' course='".$_Row['Course']."' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowConvert' value='" . $_Row['convertcount'] . "'/></td>";

            //  echo "<td><input type='button' class='updcount' course='".$_Row['Course']."' batch='" . $_Row['Batch'] . "' rolecode='" . $_Row['RoleCode'] . "' mode='ShowConfirm' value='" . $_Row['confirmcount'] . "'/></td>";
             echo "<td>" . $_Row2['uploadcount'] . "</td>";

             // echo "<td>" . $_Row['confirmcount'] . "</td>";
if(isset($_Row['confirmcount'])){
echo "<td>" . $_Row2['confirmcount'] . "</td>";

}
else{
    echo "<td>0</td>";

}
             
            echo "</tr>";
            $_Total = $_Total + $_Row2['uploadcount'];
            // $_Totaleconvert = $_Totaleconvert + $_Row['convertcount'];
			$_Totaleconfirm = $_Totaleconfirm + $_Row['confirmcount'];
            $_Count++;
        }

        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
        echo "<th >TotalCount </th>";
        echo "<th>";
        echo "$_Total";
        echo "</th>";
        // echo "<th>";
        // echo "$_Totaleconvert";
        // echo "</th>";
		echo "<th>";
        echo "$_Totaleconfirm";
        echo "</th>";
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetLearnerList($_POST['course'], $_POST['batch'], $_POST['rolecode'], $_POST['mode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
	echo "<th >Mobile NO</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
			echo "<td>" . $_Row['Admission_Mobile'] . "</td>";

            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETDATAITGK") {
    $response = $emp->GetLearnerListITGK($_POST['course'], $_POST['batch'], $_POST['rolecode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
	echo "<th>Payment Status</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";

            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }
			 if ($_Row['Admission_Payment_Status'] == "1") {
               
                echo "<td> Learner Confirmed </td>";
            } elseif ($_Row['Admission_Payment_Status'] == "0")  {
                echo "<td> Learner Not Confirmed </td>";
            }
			elseif ($_Row['Admission_Payment_Status'] == "8")  {
                echo "<td> Confirmation Pending at RKCL </td>";
            }


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
if ($_action == "DETAILRPTITGK") {
    $response = $emp->DetailedListITGK($_POST['course'], $_POST['batch'], $_POST['rolecode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >DOB</th>";
    echo "<th>Gender</th>";
    echo "<th >Course</th>";
    echo "<th>Batch</th>";
    echo "<th >Medium</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "<th>Mother Tongue</th>";
    echo "<th>Marital Status</th>";
    echo "<th>UID</th>";
    echo "<th >PH Status</th>";
    echo "<th >District</th>";
    echo "<th>Tehsil</th>";
    echo "<th>Address</th>";
    echo "<th>PIN code</th>";
    echo "<th>Mobile</th>";
    echo "<th>Phone</th>";
    echo "<th>Email</th>";
    echo "<th >Qualification</th>";
    echo "<th >Learner Type</th>";
    echo "<th >GPFNO</th>";
    echo "<th>BioMetric Status</th>";
    echo "<th>Admission Date</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";
            echo "<td>" . $_Row['Admission_Gender'] . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Medium'] . "</td>";
            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }
            echo "<td>" . $_Row['Admission_MTongue'] . "</td>";
            echo "<td>" . $_Row['Admission_MaritalStatus'] . "</td>";
            echo "<td>" . $_Row['Admission_UID'] . "</td>";
            echo "<td>" . $_Row['Admission_PH'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Address'] . "</td>";
            echo "<td>" . $_Row['Admission_PIN'] . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            echo "<td>" . $_Row['Admission_Phone'] . "</td>";
            echo "<td>" . $_Row['Admission_Email'] . "</td>";
            echo "<td>" . $_Row['Qualification_Name'] . "</td>";
            echo "<td>" . $_Row['LearnerType_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_GPFNO'] . "</td>";
            if($_Row['BioMatric_Status']=='1'){
                 echo "<td>Learner Enrolled</td>";
            }
            else{
                 echo "<td>Learner Not Enrolled</td>";
            }
            //echo "<td>" . $_Row['BioMatric_Status'] . "</td>";
            echo "<td>" . date("d-m-Y", strtotime($_Row['Timestamp'])) . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_Name'] . "</td>";
//            echo "<td>" . $_Row['Admission_Fname'] . "</td>";


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
    if ($_action == "DETAILRPT") {
    $response = $emp->DetailedList($_POST['course'], $_POST['batch'], $_POST['role']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >DOB</th>";
    echo "<th>Gender</th>";
    echo "<th >Course</th>";
    echo "<th>Batch</th>";
    echo "<th >Medium</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "<th>Mother Tongue</th>";
    echo "<th>Marital Status</th>";
    echo "<th>UID</th>";
    echo "<th >PH Status</th>";
    echo "<th >District</th>";
    echo "<th>Tehsil</th>";
    echo "<th>Address</th>";
    echo "<th>PIN code</th>";
    echo "<th>Mobile</th>";
    echo "<th>Phone</th>";
    echo "<th>Email</th>";
    echo "<th >Qualification</th>";
    echo "<th >Learner Type</th>";
    echo "<th >GPFNO</th>";
    echo "<th>BioMetric Status</th>";
    echo "<th>Admission Date</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";
            echo "<td>" . $_Row['Admission_Gender'] . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Medium'] . "</td>";
            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }
            echo "<td>" . $_Row['Admission_MTongue'] . "</td>";
            echo "<td>" . $_Row['Admission_MaritalStatus'] . "</td>";
            echo "<td>" . $_Row['Admission_UID'] . "</td>";
            echo "<td>" . $_Row['Admission_PH'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Address'] . "</td>";
            echo "<td>" . $_Row['Admission_PIN'] . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            echo "<td>" . $_Row['Admission_Phone'] . "</td>";
            echo "<td>" . $_Row['Admission_Email'] . "</td>";
            echo "<td>" . $_Row['Qualification_Name'] . "</td>";
            echo "<td>" . $_Row['LearnerType_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_GPFNO'] . "</td>";
            if($_Row['BioMatric_Status']=='1'){
                 echo "<td>Learner Enrolled</td>";
            }
            else{
                 echo "<td>Learner Not Enrolled</td>";
            }
            //echo "<td>" . $_Row['BioMatric_Status'] . "</td>";
            echo "<td>" . date("d-m-Y", strtotime($_Row['Timestamp'])) . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_Name'] . "</td>";
//            echo "<td>" . $_Row['Admission_Fname'] . "</td>";


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
if ($_action == "FILLAdmissionSummaryCourse") {
    $response = $emp->GetAllCourse(); 
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}
if ($_action == "FILLAdmissionBatchcode") {
    $response = $emp->FILLAdmissionBatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}