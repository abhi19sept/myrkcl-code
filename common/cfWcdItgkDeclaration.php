<?php

/*
 * Created by Mayank
cfWcdItgkDeclaration.php
 */

include './commonFunction.php';
require 'BAL/clsWcdItgkDeclaration.php';

$response = array();
$emp = new clsWcdItgkDeclaration();

if ($_action == "verifyDetails") {
    $response = $emp->verifyDetails($_POST['course'],$_POST['batch']);
		if($response[0]=="Success"){
			echo "y";
		}else{
			$responses = $emp->GetDetails($_POST['course'],$_POST['batch']);
				if($responses[0]=='Success'){
					$_DataTable = array();
						$_i = 0;						
							while ($_Row = mysqli_fetch_array($responses[2])) {
								$_DataTable[$_i] = array("ItgkCode" => $_Row['Wcd_Intake_Center'],
									"seat" => $_Row['Wcd_Base_Intake'],
									"itgkname" => $_Row['ITGK_Name']);
								$_i = $_i + 1;
							}
							echo json_encode($_DataTable);
						
				}
				else{
					echo "s";
				}
		}
}

if ($_action == "FillCourse") {
    $response = $emp->GetCourse();
		if($response[0]=="Success"){
			echo "<option value='' selected='selected'>Select </option>";
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
				}
		}
		else{
			echo "1";
		}   
}

if ($_action == "FillBatch") {
    $response = $emp->FILLBatchName($_POST['values']);
		if($_POST['values']=='3' || $_POST['values']=='24'){
			echo "<option value='' selected='selected'>Select </option>";
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
			}
		}
    
}

if ($_action == "AddDetails"){
	if (isset($_POST["course"]) && !empty($_POST["course"])){
		if (isset($_POST["batch"]) && !empty($_POST["batch"])){
			$response = $emp->ADD($_POST['course'],$_POST['batch']);
			echo $response[0];
		}else{
			echo "batch";
		}
	}else{
		echo "course";
	}
	
}