<?php

/*
 * @author yogi

 */
include './commonFunction.php';
require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();
date_default_timezone_set("Asia/Kolkata");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'BAL/clsIdentityInfo.php';
//echo "tri";
$response = array();

if ($_action == "ADD") {
	echo $clsIdInfo->Add(serialize($_POST));
}

if ($_action == "ADDTrainee") {
	echo $clsIdInfo->ADDTrainee(serialize($_POST));
}

if ($_action == "PerformAction") {
	if (isset($_POST['elemId']) && !empty($_POST['elemId'])) {
		$elem = explode('-', $_POST['elemId']);
		if (isset($elem[1]) && !empty($elem[1])) {
			if ($elem[0] == 'Delete') {
				$clsIdInfo->Remove($elem[1]);
			} elseif ($elem[0] == 'DeleteTrainee') {
				$clsIdInfo->RemoveTrainee($elem[1]);
			}
		}
	}
}

if ($_action == 'Info') {
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>eSakhi Name</th>";
    echo "<th>Father Name</th>";
	echo "<th>Bhamasha ID</th>";
	echo "<th>SSO ID</th>";
	echo "<th>App Appication ID</th>";
	echo "<th>eSakhi Mobile</th>";
	echo "<th>eSakhi Email</th>";
	echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	    if (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 7) {
	    $saveall = 0;
		$response = $clsIdInfo->getInfo();
		if (!empty($response['dbrow'])) {
		    $_Count = 1;
		    foreach ($response['dbrow'] as $key => $_Row) {

		    	$esakhi_mobile = (!empty($_Row['esakhi_mobile'])) ? $_Row['esakhi_mobile'] : '<input type="text" class="form-control" name="esakhi_mobile' . $_Row['id'] . '" id="esakhi_mobile' . $_Row['id'] . '" data-mask="ten-number" maxlength="10" placeholder="eSakhi Mobile">';

		    	$app_application_id = (!empty($_Row['app_application_id'])) ? $_Row['app_application_id'] : '<input maxlength="8" type="text" class="form-control" name="app_application_id' . $_Row['id'] . '" maxlength="150" id="app_application_id' . $_Row['id'] . '"  placeholder="App Application Id">';

		    	$esakhi_email = (!empty($_Row['esakhi_email'])) ? $_Row['esakhi_email'] : '<input type="text" class="form-control" name="esakhi_email' . $_Row['id'] . '" maxlength="150" id="esakhi_email' . $_Row['id'] . '"  placeholder="eSakhi Email ID">';

		    	if (empty($_Row['esakhi_mobile']) || empty($_Row['esakhi_email']) || empty($_Row['app_application_id'])) $saveall = 1;

		        echo "<tr class='odd gradeX'>";
		        echo "<td>" . $_Count . "</td>";
		        echo "<td>" . strtoupper($_Row['name']) . " <input type='hidden' id='esakhi_id' name='esakhi_id[]' value='" . $_Row['id'] . "'></td>";
		        echo "<td>" . strtoupper($_Row['fname']) . "</td>";
		        echo "<td>" . strtoupper($_Row['bhamasha_id']) . "</td>";
		        echo "<td>" . $_Row['sso_id'] . "</td>";
		        echo "<td>" . $app_application_id . "</td>";
		        echo "<td>" . $esakhi_mobile . "</td>";
		        echo "<td>" . $esakhi_email . "</td>";
		        echo '<td nowrap><span id="eSakhiAttendance"><a href="javascript:void(0)" class="aslink" id="addattendance-' . $_Row['id'] . '-' . ucwords($_Row['name']) . '"><b>Add Attendance</b></a></span> <b>|</b> <img title="Delete" class="aslink" id="Delete-' . $_Row['id'] . '" src="./images/drop.png"/></td>';
		        echo "</tr>";
		        $_Count++;
		    }

		    if ($saveall) echo '<tr><td colspan="10"><center><input type="button" name="submiteSakhi" id="submiteSakhi" class="btn btn-primary" value="Update Details" /><br /><span class="star">*</span><span class="small">(eSakhi mobile number and app application id should be unique)</span></center></tr></td>';
		} else {
			echo $response['isvalid'];
		}
	}
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == 'TraineeInfo') {
	echo "<div style='width:980px;'>";
    echo "<hr><p><center><b><u>List of eSakhi Trainees</u></b></center></p><table id='example3' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>ITGK Code</th>";
    echo "<th>eSakhi Name</th>";
    echo "<th>Trainee Name</th>";
	echo "<th>Mobile</th>";
	echo "<th>SSO ID</th>";
	echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	$response = $clsIdInfo->getTraineeInfo($_POST['esakhi']);
	if ($response) {
	    foreach ($response as $_Row) {
	        echo "<tr class='odd gradeX'>";
	        echo "<td>" . $_Row['itgkcode'] . "</td>";
	        echo "<td>" . $_Row['eSakhi_Name'] . "</td>";
	        echo "<td>" . $_Row['name'] . "</td>";
	        echo "<td>" . $_Row['mobile'] . "</td>";
	        echo "<td>" . $_Row['sso_id'] . "</td>";
	        echo '<td nowrap><span id="eSakhiTrainees"><img title="Delete" class="aslink" id="DeleteTrainee-' . $_Row['id'] . '" src="./images/drop.png"/></td>';
	        echo "</tr>";
	    }
	}
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == 'itgkreport') {
	if ($_POST['report_type'] == 'eSakhi_Reg') {
		geteSakhiRegList();	
	} elseif ($_POST['report_type'] == 'eSakhi_Trainee_Reg') {
		geteSakhiTraineeList();	
	} elseif ($_POST['report_type'] == 'ITGK_Trainer_Reg') {
		getITGKTrainerRegList();
	}
}

function geteSakhiRegList() {
	global $clsIdInfo;
	$path = getPath(0);
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>ITGK CODE</th>";
    echo "<th>Name</th>";
	echo "<th>Mobile</th>";
	echo "<th>Email</th>";
	echo "<th>District</th>";
	echo "<th>Thesil</th>";
	echo "<th nowrap>Registered eSakhi Count</th>";
	echo "<th nowrap>Photo Uploaded</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	$response = $clsIdInfo->getItgkReport();
	if ($response) {
	    $_Count = 1;
	    foreach ($response as $_Row) {
	    	$forItgk = ($_Row['itgkcode']) ? $_Row['itgkcode'] : 'all';
	        echo "<tr class='odd gradeX'>";
	        echo "<td>" . $_Count . "</td>";
	        echo "<td>" . $_Row['itgkcode'] . "</td>";
	        echo "<td>" . $_Row['itgkname'] . "</td>";
	        echo "<td>" . $_Row['mobile'] . "</td>";
	        echo "<td>" . $_Row['email'] . "</td>";
			echo "<td>" . $_Row['district'] . "</td>";
	        echo "<td>" . $_Row['thesil'] . "</td>";
	        echo '<td align="center" class="aslink" id="showlist-' . $forItgk . '"><b>' . $_Row['n'] . '</b></td>';
	        if ($_Row['images_count']) {
	        	echo '<td align="center" class="aslink" id="showfiles-' . $forItgk . '"><b>View <b>' . $_Row['images_count'] . '</b> pics</b></td>';
	        } else {
	        	echo "<td align='center'> No </td>";
	        }
	        echo "</tr>";
	        $_Count++;
	    }
	}
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

function geteSakhiTraineeList() {
	global $clsIdInfo;
	echo "<div>";
    echo "<hr><p><center><b><u>List of eSakhi Trainees</u></b></center></p><table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S.No.</th>";
    echo "<th>ITGK Code</th>";
    echo "<th>eSakhi Name</th>";
    echo "<th>Trainee Name</th>";
	echo "<th>Mobile</th>";
	echo "<th>SSO ID</th>";
	//echo "<th>&nbsp;</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	$response = $clsIdInfo->getTraineeInfo('all');
	if ($response) {
		$_Count = 1;
	    foreach ($response as $_Row) {
	        echo "<tr class='odd gradeX'>";
	        echo "<td>" . $_Count . "</td>";
	        echo "<td>" . $_Row['itgkcode'] . "</td>";
	        echo "<td>" . ucwords($_Row['eSakhi_Name']) . "</td>";
	        echo "<td>" . ucwords($_Row['name']) . "</td>";
	        echo "<td>" . $_Row['mobile'] . "</td>";
	        echo "<td>" . $_Row['sso_id'] . "</td>";
	        echo "</tr>";
	        $_Count++;
	    }
	}
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

function getITGKTrainerRegList() {
	global $clsIdInfo;
	echo "<div class='table-responsive' >";
	echo "<hr /><center><b><u>Registered Trainers with ITGK</u></b></center>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th nowrap>S.No.</th>";
    echo "<th nowrap>ITGK CODE</th>";
    echo "<th>Trainer Name</th>";
    echo "<th>Mobile</th>";
    echo "<th nowrap>SSO ID</th>";
	echo "<th>Email</th>";
	echo "<th nowrap>Status</th>";
	echo "<th nowrap>&nbsp;</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $saveall = 0;
    $itgkCode = 'all';
	$response = $clsIdInfo->getItgkeSakhiTrainers($itgkCode);
	if ($response) {
		$_Count = 1;
	    foreach ($response as $_Row) {
	    	$ssoinput = $mobileinput = $emailinput = '';
	    	$ssoinput = (!empty($_Row['ssoid'])) ? $_Row['ssoid'] : '';
	    	$mobileinput = (!empty($_Row['mobile'])) ? $_Row['mobile'] : '';
	    	$emailinput = (!empty($_Row['email'])) ? $_Row['email'] : '';

	    	$status = (!empty($_Row['ssoid'])) ? '<img title="SSOID Added" id="Saved-' . $_Row['id'] . '" src="./images/correct.gif" width="20px" />' : '<img title="SSOID Missing" id="Missed-' . $_Row['id'] . '" src="./images/drop.png" width="20px" />';

	        echo "<tr class='odd gradeX'>";
	        echo "<td>" . $_Count . "</td>";
	        echo "<td>" . $_Row['itgkcode'] . "</td>";
	        echo "<td nowrap>" . strtoupper($_Row['trainer_name']) . '<input type="hidden" id="trainer_id" name="trainer_id[]" value="' . $_Row['id'] . '">' . "</td>";
	        echo "<td>" . $mobileinput . "</td>";
	        echo "<td nowrap>" . $ssoinput . "</td>";
	        echo "<td>" . $emailinput . "</td>";
	        echo '<td nowrap>' . $status . '</td>';
	        echo "</tr>";
	        $_Count++;
	    }
	} else {
		echo '<tr><td colspan="6"><center>No record found.</center></tr></td>';
	}
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == 'showlist') {
	echo "<div class='table-responsive' >";
    echo "<table id='example1' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th class='list_itc'>ITGK CODE</th>";
    echo "<th>Name</th>";
    echo "<th>Father / Husband Name</th>";
	echo "<th>eSakhi Mobile</th>";
	echo "<th>eSakhi Email</th>";
	echo "<th>Bhamasha Id</th>";
	echo "<th>SSO Id</th>";
	echo "<th>App ID</th>";
	echo "<th>Start Date</th>";
	echo "<th>Training Completed</th>";
	echo "<th>Attendance</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $itgkCode = 0;
    if (isset($_POST['elemId']) && !empty($_POST['elemId'])) {
		$elem = explode('-', $_POST['elemId']);
		if (isset($elem[1]) && !empty($elem[1])) {
			$itgkCode = $elem[1];
		}
	}
	$itgkCode = ($itgkCode) ? $itgkCode : 'all';
	$response = $clsIdInfo->getItgkeSakhi($itgkCode);
	if ($response) {
	    foreach ($response as $_Row) {
	    	$attendance = ($_Row['attendance']) ? 'Day' . str_replace(',', ', Day', $_Row['attendance']) : '';
	    	$attendance = ((!empty($_Row['remark_attendance'])) ? $attendance . '<br />[ ' . trim($_Row['remark_attendance']) . ' ]' : (($attendance) ? $attendance : '--Nill--'));
	        echo "<tr class='odd gradeX'>";
	        echo "<td>" . $_Row['itgkcode'] . "</td>";
	        echo "<td nowrap>" . strtoupper($_Row['name']) . "</td>";
	        echo "<td nowrap>" . strtoupper($_Row['fname']) . "</td>";
	        echo "<td>" . $_Row['esakhi_mobile'] . "</td>";
	        echo "<td>" . $_Row['esakhi_email'] . "</td>";
			echo "<td>" . $_Row['bhamasha_id'] . "</td>";
	        echo "<td>" . $_Row['sso_id'] . "</td>";
	        echo "<td>" . $_Row['app_application_id'] . "</td>";
	        echo "<td nowrap>" . $_Row['training_start_date'] . "</td>";
	        echo "<td nowrap>" . (($_Row['training_completed']) ? 'Yes' : 'No') . "</td>";
	        echo "<td nowrap>" . $attendance . "</td>";
	        echo "</tr>";
	    }
	}
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == 'AddAttendance') {
	echo $clsIdInfo->addAttendance($_POST);
}

if ($_action == 'getAttendance') {
	$response = $clsIdInfo->getItgkeSakhi('all', $_POST['att_id']);
	if ($response) {
		echo $response[0]['training_completed'] . '|' . $response[0]['training_start_date'] . '|' . trim($response[0]['remark_attendance']) . '|' . $response[0]['attendance'];
	}
}

if ($_action == 'uploadPics') {
	if (!isset($_SESSION['User_LoginId'])) die('Unable to process!'); 
	$target_dir = getPath();
    $images_arr = array();
        //upload and stored images
    $target_file = $target_dir . $_FILES['file']['name'];
    if(move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) {
    	recursiveScan($target_dir, $_SESSION['User_LoginId']);
        print 'Photo uploaded successfully.';
    }
}

if ($_action == 'eSakhiPhotos') {
	$itgkCode = '';
	if (isset($_POST['elemId']) && !empty($_POST['elemId'])) {
		$elem = explode('-', $_POST['elemId']);
		if (isset($elem[1]) && !empty($elem[1])) {
			$itgkCode = $elem[1];
		}
	} else {
		$itgkCode = $_SESSION['User_LoginId'];
	}
	if (!$itgkCode) die('Unable to process!');

	$path = getPath(0);
	$path = ($itgkCode == 'all') ? $path : $path . $itgkCode . '/';
	//print_r($files);
	$content = '';
	if ($_SESSION['User_UserRoll'] == 14 && $itgkCode == 'all') {
		$getRSPItgkCodes  = $clsIdInfo->getRspItgkCodes();
		foreach ($getRSPItgkCodes as $itgkCode) {
			$itgkPath = $path . $itgkCode . '/';
			$content .= getUploadedPics($itgkPath, $itgkCode);
		}
	} else {
		$content .= getUploadedPics($path, $itgkCode);
	}
	if ($content == '') {
		$content = '<center class="small">No photo found.</center>';
	}

	echo $content;
}

if ($_action == 'trainersList') {
	echo "<div class='table-responsive' >";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small' style='width:1050px;'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th nowrap class='list_itc'>ITGK CODE</th>";
    echo "<th>Trainer Name</th>";
    echo "<th>Mobile</th>";
    echo "<th nowrap>SSO ID</th>";
	echo "<th>Email</th>";
	echo "<th nowrap>Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $saveall = 0;
	$response = $clsIdInfo->getItgkeSakhiTrainers($_SESSION['User_LoginId']);
	if ($response) {
	    foreach ($response as $_Row) {
	    	$ssoinput = $mobileinput = $emailinput = '';
	    	$ssoinput = (!empty($_Row['ssoid'])) ? $_Row['ssoid'] : '<input type="text" class="form-control" name="ssoid' . $_Row['id'] . '" id="ssoid' . $_Row['id'] . '" data-mask="ssoId" placeholder="SSO ID">';
	    	$mobileinput = (!empty($_Row['mobile'])) ? $_Row['mobile'] : '<input type="text" maxlength="10" class="form-control" name="mobile' . $_Row['id'] . '" id="mobile' . $_Row['id'] . '" placeholder="Mobile No" data-mask="ten-number">';
	    	$emailinput = (!empty($_Row['email'])) ? $_Row['email'] : '<input type="text" class="form-control" name="email' . $_Row['id'] . '" id="email' . $_Row['id'] . '" placeholder="Email Id">';
	    	
	    	if (empty($_Row['ssoid']) || empty($_Row['mobile']) || empty($_Row['email'])) $saveall = 1;

	    	$status = (!empty($_Row['ssoid'])) ? '<img title="SSOID Added" id="Saved-' . $_Row['id'] . '" src="./images/correct.gif" width="20px" />' : '<img title="SSOID Missing" id="Missed-' . $_Row['id'] . '" src="./images/drop.png" width="20px" />';

	        echo "<tr class='odd gradeX'>";
	        echo "<td>" . $_Row['itgkcode'] . "</td>";
	        echo "<td nowrap>" . strtoupper($_Row['trainer_name']) . '<input type="hidden" id="trainer_id" name="trainer_id[]" value="' . $_Row['id'] . '">' . "</td>";
	        echo "<td>" . $mobileinput . "</td>";
	        echo "<td nowrap>" . $ssoinput . "</td>";
	        echo "<td>" . $emailinput . "</td>";
	        echo '<td nowrap>' . $status . '</td>';
	        echo "</tr>";
	    }

	    if ($saveall) echo '<tr><td colspan="6"><center><input type="button" name="submitTrainer" id="submitTrainer" class="btn btn-primary" value="Submit" /></center></tr></td>';
	} else {
		echo '<tr><td colspan="6"><center>No record found.</center></tr></td>';
	}
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == 'updateeSakhiInfo') {
	echo $clsIdInfo->updateeSakhiInfo($_POST);
}

if ($_action == 'updateTrainerInfo') {
	$clsIdInfo->updateTrainerInfo($_POST);
}

if ($_action == 'FilleSakhi') {
	$response = $clsIdInfo->getItgkeSakhi($_SESSION['User_LoginId']);
	echo "<option value='' >Select </option>";
    if ($response) {
	    foreach ($response as $_Row) {
	    	echo "<option value=" . $_Row['id'] . ">" . $_Row['name'] . "</option>";
	    }
	}
}

	function getPath($generateITGKDir = 1) {
		$target_dir = '../upload/esakhi_photos/';
		dirMake($target_dir);
		if ($generateITGKDir) {
		    $target_dir = '../upload/esakhi_photos/' . $_SESSION['User_LoginId'] . '/';
		    dirMake($target_dir);
		}

	    return $target_dir;
	}

	function dirMake($path) {
         return is_dir($path) || mkdir($path);
    }

    function getUploadedPics($path, $itgkCode) {
    	$content = '';
    	if (is_dir($path)) {
			$files = scandir($path);
			if ($itgkCode == 'all') {
				foreach ($files as $fname) {
					if ($fname != '.' && $fname != '..') {
						if (is_dir($path . $fname)) {
							$content .= listUploadedPics($path . $fname, $fname);
							//recursiveScan($path . $fname, $fname);
						}
					}
				}
			} else {
				$content .= listUploadedPics($path, $itgkCode);
			}
		}

		return $content;
    }

    function listUploadedPics($path, $itgkCode) {
		$content = '<hr /><center>Uploaded by: <b>' . $itgkCode . '</b></center><hr /><div id="links">';
		$content .= '<div class="container">';
		$counter = 1;
		if (is_dir($path)) {
			$files = scandir($path);
			foreach ($files as $filename) {
				if ($filename != '.' && $filename != '..') {
					$imgpath = str_replace('../','',$path) . '/' . $filename;
					$content .= '<div class="col-sm-4 form-group"><center><a href="' . $imgpath . '" onclick="javascript:return false;" class="image-link">';
					$content .= '<img class="img-thumbnail" src="' . $imgpath . '" width="80px" />';
					$content .= '</a></center></div>';
					$counter++;
					if ($counter%5 == 0) $content .= '</div><div class="container">';
				}
			}
		}

		if ($counter == 1) {
			$content = '<center class="small">No photo found.</center>';
		}

		$content .= '</div></div>';

		return $content;
	}

    function recursiveScan($path, $itgk) {
		$files = scandir($path);
		if ($itgk == 'all') {
			foreach ($files as $name) {
				if ($name != '.' && $name != '..') {
					$newPath = $path . $name;
					if (is_dir($newPath)) {
						updatePicsCount($newPath, $name);
					}
				}
			}
		} else {
			updatePicsCount($path, $itgk);
		}
	}

	function updatePicsCount($path, $itgk) {
		global $clsIdInfo;
		$files = scandir($path);
		$totalFiles = count($files);
		if ($totalFiles < 3) {
			if(!rmdir($path)) {
			  echo ("Could not remove $path");
			}
		} else {
			$clsIdInfo->updateITGKUploadStatus($itgk, $totalFiles);
		}
	}