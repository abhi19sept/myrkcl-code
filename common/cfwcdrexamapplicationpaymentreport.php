<?php

/*
 * Created by Yogi

 */

include './commonFunction.php';
require 'BAL/clswcdrexamapplicationpaymentreport.php';

$response = array();
$emp = new clswcdrexamapplicationpaymentreport();

if ($_action == "FILLWcdrexamapplicationCourse") {
    $response = $emp->GetWcdrexamapplicationCourse();	
    echo "<option value=''>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLWcdrexamapplicationBatch") {
    $response = $emp->FILLWcdrexamapplicationBatch($_POST['values']);	
    echo "<option value=''>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "APPLICATION") 
{

		$sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
        $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
    $response = $emp->GetApplication($_POST['course'], $sdate, $edate);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";    
    echo "<th>Learner Code </th>";
    echo "<th>Learner Name</th>";
	echo "<th>Father Name </th>";
	echo "<th>ITGK Code</th>";
    echo "<th>ITGK District</th>";
    echo "<th>Batch Name</th>";
	echo "<th>SP Name </th>";
	echo "<th>Payment Date</th>";
	echo "<th>Payment Amount</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>'" . $_Row['rscfa_reexam_lcode'] . "</td>";
        echo "<td>" . $_Row['rscfa_reexam_lname'] . "</td>";
		echo "<td>" . $_Row['rscfa_reexam_fname'] . "</td>";
        echo "<td>" . $_Row['itgkcode'] . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
        echo "<td>" . $_Row['Batch_Name'] . "</td>";
		echo "<td>" . $_Row['RSP_Name'] . "</td>";
		echo "<td>" . $_Row['rscfa_reexam_paydate'] . "</td>";
		echo "<td>" . 700 . "</td>";
        $_Count++;
        
    }

    echo "</tbody>";
   
    echo "</table>";
    echo "</div>";
}



if ($_action == "CERTIFICATE") 
{
	$sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
        $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';

    $response = $emp->GetCertificate($_POST['course'], $sdate, $edate);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
	echo "<th>S No.</th>";    
    echo "<th>Learner Code </th>";
    echo "<th>Learner Name</th>";
	echo "<th>Father Name </th>";
	echo "<th>ITGK Code</th>";
    echo "<th>ITGK District</th>";
	echo "<th>Email</th>";
	 echo "<th>Batch Name</th>";
	echo "<th>SP Name </th>";
	echo "<th>Payment Date</th>";
	echo "<th>Payment Amount</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_TotalUploaded = 0;
    $_TotalConfirmed = 0;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>'" . $_Row['apply_rscfa_cert_lcode'] . "</td>";
        echo "<td>" . $_Row['apply_rscfa_cert_lname'] . "</td>";
		echo "<td>" . $_Row['apply_rscfa_cert_fname'] . "</td>";
        echo "<td>" . $_Row['apply_rscfa_cert_itgk'] . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
		echo "<td>" . $_Row['apply_rscfa_cert_email'] . "</td>";
		echo "<td>" . $_Row['Batch_Name'] . "</td>";
		echo "<td>" . $_Row['RSP_Name'] . "</td>";
		echo "<td>" . $_Row['apply_rscfa_cert_payment_date'] . "</td>";
		echo "<td>" . 1250 . "</td>";
		
        $_Count++;
        
    }

    echo "</tbody>";
    
    echo "</table>";
    echo "</div>";
}





   