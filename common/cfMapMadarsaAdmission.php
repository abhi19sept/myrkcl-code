<?php

/*
 * author Abhishek

 */
include './commonFunction.php';
require 'BAL/clsMapMadarsaAdmission.php';

$response = array();
$emp = new clsMapMadarsaAdmission();
$category = array();

if ($_action == "SHOWALL") {
    $response = $emp->GetAllLearner($_POST['batch'], $_POST['course']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";

    echo "<th style='10%'>Learner Code</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    if ($_SESSION['User_UserRoll'] != 7) {
        echo "<th style='10%'>Learner Gender</th>";
        echo "<th style='10%'>Learner Mobile</th>";
        echo "<th style='10%'>IT-GK Code</th>";
    }

    echo "<th style='8%'>Select Madarsa</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_CountGrid = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        if ($_SESSION['User_UserRoll'] != 7) {
            echo "<td>" . $_Row['Admission_Gender'] . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
        }



        $response2 = $emp->chkadmissionmadarsamap($_Row['Admission_LearnerCode']);
        $_numRow3 = mysqli_num_rows($response2[2]);
        $_Row3 = mysqli_fetch_array($response2[2]);

        if (!$_numRow3) {
            if ($_SESSION['User_UserRoll'] != 7) {
                echo "<td class='odd gradeX warning'><b> Learner Not Mapped to Madarsa </b></td>";
            } else {
                echo "<td><select id='ddlMadarsa' name='ddlMadarsa_" . $_CountGrid . "' class='form-control'><option value='0' >Select </option>";
                $response1 = $emp->FILLmadarsa();
                while ($_Row2 = mysqli_fetch_array($response1[2])) {
                    echo "<option value=" . $_Row['Admission_LearnerCode'] . "_" . $_Row2['Madarsa_Code'] . ">" . $_Row2['Madarsa_Regd_code'] . " - " . $_Row2['Madarsa_Name'] . "</option>";
                }
                echo " </select></td>";
                $_CountGrid++;
            }
        } else {
            echo "<td>" . $_Row3['Madarsa_Regd_code'] . " - " . $_Row3['Madarsa_Name'] . "</td>";
        }


        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "submitclick") {

    $flag = '0';
    $countarray = sizeof($_POST) - 3;
    for ($i = 1; $i <= $countarray; $i++) {
        $arindex = "ddlMadarsa_" . $i;
        $lcode = explode("_", $_POST[$arindex]);
        //  print_r($lcode);
        if ($lcode[0] != '0') {
            $response = $emp->Addtoadmissionmadarsamap($lcode[0], $lcode[1]);
            $flag = '1';
        }
    }
    if ($flag == '1') {
        echo $response[0];
    } else {
        echo "0";
    }
}