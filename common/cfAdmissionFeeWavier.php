<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsAdmissionFeeWavier.php';

$response = array();
$emp = new clsAdmissionFeeWavier();

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FillCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "CheckAlreadyGenerated") {
    //echo "Show";		
		if($_POST['batch'] !='') {		 
			  $response = $emp->CheckExistence($_POST['course'],$_POST['batch']);			  
				if($response[0]=='Success'){
					echo "yes";
				}
				else{
					echo "no";
				}
		}
		  else {
			  echo "1";
		  }   
}

if ($_action == "DistrictWiseLottery") {
    //echo "Show";		
		if($_POST['levelid'] !='') {
			$response = $emp->GenerateDistrictWiseLottery($_POST['levelid'],$_POST['batchcode']);
			echo $response[0];
		}
		  else {
			  echo "1";
		  }   
}

if ($_action == "RemainingSeatWiseLottery") {
    //echo "Show";		
		if($_POST['levelid'] !='') {
			$response = $emp->GenerateRemainingSeatWiseLottery($_POST['levelid'],$_POST['batchcode']);
			echo $response[0];
		}
		  else {
			  echo "1";
		  }   
}

if ($_action == "ResetGeneratedSeats") {
    //echo "Show";		
		if($_POST['levelid'] !='') {
			$response = $emp->ResetGeneratedSeats($_POST['levelid'],$_POST['batchcode']);
			echo $response[0];
		}
		  else {
			  echo "1";
		  }   
}

if ($_action == "LearnerWiseLotteryReport") {
    //echo "Show";		
		if($_POST['levelid'] !='') {
			$response = $emp->GetLearnerWiseLotteryReport($_POST['levelid'],$_POST['batchcode']);
			$_DataTable = "";

		echo "<div class='table-responsive' style='margin-top:10px'>";
   
		echo "<table id='example".$_POST['levelid']."' border='0' cellpedding='0' cellspacing='0'
			class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th style='15%'>S No.</th>";
		echo "<th style='15%'>ITGK-Code</th>";
		echo "<th style='15%'>LearnerCode</th>";
		echo "<th style='15%'>Learner Name</th>";
		echo "<th style='15%'>Father/Husband Name</th>";
		echo "<th style='15%'>D.O.B.</th>";
		echo "<th style='15%'>Discount Category</th>";
		echo "<th style='15%'>Discount Type </th>";
		echo "<th style='15%'>District Name</th>";
		//echo "<th style='15%'>IT-GK Name</th>";
		echo "<th style='15%'>SP Name</th>";
		echo "<th style='15%'>SP Code</th>";
		
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$_Count = 1;
		$co = mysqli_num_rows($response[2]);
		if ($co) {
			while ($_Row = mysqli_fetch_array($response[2])){
				echo "<tr class='odd gradeX'>";
				echo "<td>" . $_Count . "</td>";
				echo "<td>" . $_Row['lottery_scheme_itgk_code'] . "</td>";
				echo "<td>'" . $_Row['lottery_scheme_lcode'] . "</td>";
				echo "<td>" . strtoupper($_Row['lottery_scheme_lname']) . "</td>";
				echo "<td>" . strtoupper($_Row['lottery_scheme_fname']) . "</td>";
				echo "<td>" . $_Row['lottery_scheme_dob'] . "</td>";
				echo "<td>" . $_Row['Discount'] . "</td>";
				echo "<td>" . $_Row['lottery_scheme_reservation_type'] . "</td>";
				echo "<td>" . $_Row['District_Name'] . "</td>";
				//echo "<td>" . $_Row['ITGK_Name'] . "</td>";				
				echo "<td>" . $_Row['RSP_Name'] . "</td>";				
				echo "<td>" . $_Row['RSP_Code'] . "</td>";
		
				echo "</tr>";
				$_Count++;
			}
			echo "</tbody>";
			echo "</table>";
			echo "</div>";
		}
		}
		  else {
			  echo "1";
		  }   
}