<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsExamSchedule.php';

$response = array();
$emp = new clsExamSchedule();

if ($_action == "FILL") {
    $response = $emp->FillEvent();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "schedule") {
    $response = $emp->getschedule($_POST['examevent']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Learner Code</th>";
    echo "<th >Learner Name</th>";
    echo "<th>Father Name</th>";
    echo "<th>DOB</th>";
    echo "<th>TAC</th>";
    echo "<th>Course Name</th>";
    echo "<th>Batch Name</th>";
    echo "<th>Exam Center Code</th>";
    echo "<th>Exam Center Name</th>";
    echo "<th>Exam Center District</th>";
    echo "<th>Exam Center Tehsil</th>";
    echo "<th>Exam Center Address</th>";
    echo "<th>Learner Roll No</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['learnercode'] . "</td>";
        echo "<td>" . $_Row['learnername'] . "</td>";
        echo "<td>" . $_Row['fathername'] . "</td>";
        echo "<td>" . $_Row['dob'] . "</td>";
        echo "<td>" . $_Row['tac'] . "</td>";
        echo "<td>" . $_Row['coursename'] . "</td>";
        echo "<td>" . $_Row['batchname'] . "</td>";
        echo "<td>" . $_Row['examcentercode'] . "</td>";
        echo "<td>" . $_Row['examcentername'] . "</td>";
        echo "<td>" . $_Row['examcenterdistrict'] . "</td>";
        echo "<td>" . $_Row['examcentertehsil'] . "</td>";
        echo "<td>" . $_Row['examcenteraddress'] . "</td>";
        echo "<td>" . $_Row['rollno'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
?>