<?php

require '../DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();
session_start();

//$_SESSION['ImageFile'] = "";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

    global $_ObjFTPConnection;
//print_r($_POST);
//die();
//$_UploadDirectory1 = '../upload/Bankdocs/';
//$_UploadDirectory2 = '../upload/pancard/';
//$_UploadDirectory3 = '../upload/GST_document/';

$_UploadDirectory1 = '/Bankdocs';
$_UploadDirectory2 = '/bank_pancard';//remane folder from pancard to bank_pancard
$_UploadDirectory3 = '/GST_document';

//$name = explode(",", $_POST['UploadId']);
//$_time = $name[0];
//$_Proof = $name[1];

$parentid = $_SESSION['User_LoginId'] . '_' . $_POST['UploadId'];
$docname = $_SESSION['User_LoginId'] . '_' . $_POST['UploadId'] . '_' . $_POST['UploadDocName'];



//if (isset($_FILES["SelectedFile2"])) {
//    if ($_FILES["SelectedFile2"]["name"] != '') {
//        $imag = $_FILES["SelectedFile2"]["name"];
//        $imageinfo = pathinfo($_FILES["SelectedFile2"]["name"]);
//        if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
//            $error_msg = "Image must be in either PNG or JPG or JPEG Format";
//            $flag = 0;
//        } else {
//            if (file_exists("$_UploadDirectory2/" . $parentid . '_pancard' . '.' . substr($imag, -3))) {
//                $_FILES["SelectedFile2"]["name"] . "already exists";
//            } else {
//                if (file_exists($_UploadDirectory2)) {
//                    if (is_writable($_UploadDirectory2)) {
//                        move_uploaded_file($_FILES["SelectedFile2"]["tmp_name"], "$_UploadDirectory2/" . $parentid . '_pancard' . '.jpg');
//                        //session_start();
//                        //$_SESSION['SignFile'] =  $parentid .'_sign'.'.png';
//                    } else {
//                        outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
//                    }
//                } else {
//                    outputJSON("<span class='error'>Upload Folder Not Available</span>");
//                }
//            }
//        }
//    }
//}

if (isset($_FILES["SelectedFile2"])) {
    if ($_FILES["SelectedFile2"]["name"] != '') {
        $imag = $_FILES["SelectedFile2"]["name"];
        $imagtmp = $_FILES['SelectedFile2']['tmp_name'];
        $imageinfo = pathinfo($_FILES["SelectedFile2"]["name"]);
        if (strtolower($imageinfo['extension']) != "jpg") {
            $error_msg = "Image must be in JPG Format";
            $flag = 0;
        } else {
            //move_uploaded_file($_FILES["SelectedFile2"]["tmp_name"], "$_UploadDirectory2/" . $parentid . '_pancard' . '.jpg');
            $bankpan_response_ftp=ftpUploadFile($_UploadDirectory2,$imag,$imagtmp);       
        }
    }
}

//if ($_FILES["SelectedFile1"]["name"] != '') {
//    $imag = $_FILES["SelectedFile1"]["name"];
//    $imageinfo = pathinfo($_FILES["SelectedFile1"]["name"]);
//    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
//        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
//        $flag = 0;
//    } else {
//        if (file_exists("$_UploadDirectory1/" . $docname . '_bankdocs' . '.' . substr($imag, -3))) {
//            $_FILES["SelectedFile1"]["name"] . "already exists";
//        } else {
//            if (file_exists($_UploadDirectory1)) {
//                if (is_writable($_UploadDirectory1)) {
//                    move_uploaded_file($_FILES["SelectedFile1"]["tmp_name"], "$_UploadDirectory1/" . $docname . '.jpg');
//                    //session_start();
//                    //$_SESSION['PaymentReceiptFile'] =  $docname .'_bankdocs'.'.jpg';
//                } else {
//                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
//                }
//            } else {
//                outputJSON("<span class='error'>Upload Folder Not Available</span>");
//            }
//        }
//    }
//}

if ($_FILES["SelectedFile1"]["name"] != '') {
    $imag = $_FILES["SelectedFile1"]["name"];
    $imageinfo = pathinfo($_FILES["SelectedFile1"]["name"]);
    $imagtmp = $_FILES['SelectedFile1']['tmp_name'];
    if (strtolower($imageinfo['extension']) != "jpg") {
        $error_msg = "Image must be JPG Format";
        $flag = 0;
    } else {
        //move_uploaded_file($_FILES["SelectedFile1"]["tmp_name"], "$_UploadDirectory1/" . $docname . '.jpg');
        $bankpan_response_ftp=ftpUploadFile($_UploadDirectory1,$imag,$imagtmp);
             
    }
}

//if ($_FILES["gstinproof"]["name"] != '') {
//    $imag = $_FILES["gstinproof"]["name"];
//    $imageinfo = pathinfo($_FILES["gstinproof"]["name"]);
//    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
//        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
//        $flag = 0;
//    } else {
//        if (file_exists("$_UploadDirectory1/" . $parentid . '_gstin' . '.' . substr($imag, -3))) {
//            $_FILES["gstinproof"]["name"] . "already exists";
//        } else {
//            if (file_exists($_UploadDirectory1)) {
//                if (is_writable($_UploadDirectory1)) {
//                    move_uploaded_file($_FILES["gstinproof"]["tmp_name"], "$_UploadDirectory3/" . $parentid . '_gstin' . '.jpg');
//
//                    //session_start();
//                    //$_SESSION['PaymentReceiptFile'] =  $docname .'_bankdocs'.'.jpg';
//                } else {
//                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
//                }
//            } else {
//                outputJSON("<span class='error'>Upload Folder Not Available</span>");
//            }
//        }
//    }
//}

if ($_FILES["gstinproof"]["name"] != '') {
    $imag = $_FILES["gstinproof"]["name"];
    $imagtmp = $_FILES['gstinproof']['tmp_name'];
    $imageinfo = pathinfo($_FILES["gstinproof"]["name"]);
    if (strtolower($imageinfo['extension']) != "jpg") {
        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
        $flag = 0;
    } else {
        if (file_exists("$_UploadDirectory1/" . $parentid . '_gstin' . '.' . substr($imag, -3))) {
            $_FILES["gstinproof"]["name"] . "already exists";
        } else {
            //move_uploaded_file($_FILES["gstinproof"]["tmp_name"], "$_UploadDirectory3/" . $parentid . '_gstin' . '.jpg');
            $bankpan_response_ftp=ftpUploadFile($_UploadDirectory3,$imag,$imagtmp);
        }
    }
}
					 
				