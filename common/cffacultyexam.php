<?php

include('../BAL/clsfacultyexam.php');
include('commonFunction.php');
$emp = new clsFacultyPaperOnline();
	
date_default_timezone_set("Asia/Kolkata");

if ($_action == "SHOWITGKFACULTY") {
    $itgk = $_POST["itgkCode"];
    $response = $emp->ShowCenterData($itgk);
    if($response[0]=='Success'){
        $row = mysqli_fetch_array($response[2]);
        echo "<div class='col-md-6'><div class='form-group'>
                <label for='inputEmail3' class='col-sm-3 control-label'>Center Code</label>
                <div class='col-sm-8'>
                    <div class='form-control bxdesign'>".$row["ITGKCODE"]."</div>
                </div>
            </div>";
        echo "<div class='form-group'>
                <label for='inputEmail3' class='col-sm-3 control-label'>IT-GK Mobile</label>
                <div class='col-sm-8'>
                    <div class='form-control bxdesign'>".$row["ITGKMOBILE"]."</div>
                </div>
            </div>";
        echo "<div class='form-group'>
                <label for='inputEmail3' class='col-sm-3 control-label'>IT-GK Email</label>
                <div class='col-sm-8'>
                    <div class='form-control bxdesign'>".$row["ITGKEMAIL"]."</div>
                </div>
            </div>";
        echo "<div class='form-group'>
                <label for='inputEmail3' class='col-sm-3 control-label'>IT-GK Name</label>
                <div class='col-sm-8'>
                    <div class='form-control bxdesign'>".$row["ITGK_Name"]."</div>
                </div>
            </div></div>";
        echo "<div class='col-md-6'><div class='form-group'>
                <label for='inputEmail3' class='col-sm-3 control-label'>District Name</label>
                <div class='col-sm-8'>
                    <div class='form-control bxdesign'>".$row["District_Name"]."</div>
                </div>
            </div>";
        echo "<div class='form-group'>
                <label for='inputEmail3' class='col-sm-3 control-label'>Tehsil Name</label>
                <div class='col-sm-8'>
                    <div class='form-control bxdesign'>".$row["Tehsil_Name"]."</div>
                </div>
            </div>";
        echo "<div class='form-group'>
                <label for='inputEmail3' class='col-sm-3 control-label'>SP Name</label>
                <div class='col-sm-8'>
                    <div class='form-control bxdesign'>".$row["RSP_Name"]."</div>
                </div>
            </div></div>";
    $userid = $row["ITGKCODE"];
    $response = $emp->ShowFacultyData($userid);
        $_Count = 1;
        echo "<div class='row'><div class='col-md-12'><div class='table-users'>";
        echo "<div class='table-responsive tablescroll'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered' style='margin-bottom: 10px;'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='font-family: initial;text-align:center;'>S. No.</th>";
        echo "<th style='font-family: initial;text-align:center;'>IT-GK Code</th>";
        echo "<th style='font-family: initial;text-align:center;'>Staff Code</th>";
        echo "<th style='font-family: initial;text-align:center;'>Faculty Name</th>";
        echo "<th style='font-family: initial;text-align:center;'>Mobile Number</th>";
        echo "<th style='font-family: initial;text-align:center;'>Email Address</th>";
        echo "<th style='font-family: initial;text-align:center;'>Exam Attempt 1</th>";
        echo "<th style='font-family: initial;text-align:center;'>Exam Attempt 2</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        if($response[0]=='Success'){
            $disable = "0";
        while($row = mysqli_fetch_array($response[2])){
            echo "<tr class='odd gradeX'>";
            echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'>" .$_Count. "</td>";  
            echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'>" .$row['Staff_User']. "</td>";  
            echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'>" .$row['Staff_Code']. "</td>";  
            echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'>" .$row['Staff_Name']. "</td>";  
            echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'>" .$row['Staff_Mobile']. "</td>";  
            echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'>" .$row['Staff_Email_Id']. "</td>";  
            if(isset($row['marks'])){
                $ob_marks = explode(",", $row['marks']);
                $ob_attempts = explode(",", $row['attempts']);
                if(isset($ob_marks[0]) && $ob_attempts[0] == '1'){
                    $disable = "1";
                   //echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'><button class='btn btn-success' type='button'>Completed ( ". $ob_marks[0] ." / ". $row['ResExam_totalmarks'] ." )</button></td>"; 
                   echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'><button class='btn btn-success' type='button'>Completed</button></td>"; 
                }else{
                   echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'><button id='1' name='".$row['Staff_User']."_".$row['Staff_Code']."' class='fun_start_exam btn btn-success' type='button'>Start</button><div class='resloc".$row['Staff_User']."_".$row['Staff_Code']."'></div></td>";   
                }
                if(isset($ob_marks[1]) && $ob_attempts[1] == '2'){
                   //echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'><button class='btn btn-success' type='button'>Completed ( ". $ob_marks[1] ." / ". $row['ResExam_totalmarks'] ." )</button></td>"; 
                   echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'><button class='btn btn-success' type='button'>Completed</button></td>"; 
                }else{
                    if($disable == "1"){
                        echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'><button id='1' name='".$row['Staff_User']."_".$row['Staff_Code']."' class='fun_start_exam_2 btn btn-success' type='button'>Start</button><div class='resloc_2".$row['Staff_User']."_".$row['Staff_Code']."'></div></td>";   
                    }else{
                        echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'><button class='btn btn-success' type='button' disabled>Start</button></td>";  
                    }
                   
                }
            }else{
              echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'><button id='1' name='".$row['Staff_User']."_".$row['Staff_Code']."' class='fun_start_exam btn btn-success' type='button'>Start</button><div class='resloc".$row['Staff_User']."_".$row['Staff_Code']."'></div></td>";  
              echo "<td style='font-family: initial;vertical-align: middle;text-align: center;'><button class='btn btn-success' type='button' disabled>Start</button></td>";  
            }
            
            echo "</tr>";
            $_Count++;
        }
        }else{
            echo "<tr class='odd gradeX'>";
            echo "<td colspan='7' style='font-family: initial;vertical-align: middle;text-align: center;'>No Record Found</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
        echo "</div></div></div>";
        
    }else{
        echo "norecord";
    }
}

if ($_action == "EXAMSINST") {
    $examid = $_POST['examid'];
    $staff = $_POST['staff'];
    $attempt = $_POST['attempt'];
    $userid = $staff;
    $responseCheckr = $emp->CheckAlready($examid,$userid,$attempt);
    if ($responseCheckr[0] == 'Success') {
        echo "<div class='alert-success'><span><img src=images/warning.jpg width=10px /></span><span>You have alredy strated exam. Please conatct admin.</span></div>";
    }else{
        $response = $emp->Fillinsruction($examid,$userid);
        if ($response[0] == 'Success') {
            $row = mysqli_fetch_array($response[2]);
            $InstDesc = html_entity_decode($row['Inst_description']);
            echo " <div class='col-sm-6' style='border-right: 1px solid black;'>" . $InstDesc . "</div>";
            $InstDescHindi = html_entity_decode($row['Inst_descriptionhindi']);
            echo " <div class='col-sm-6'>" . $InstDescHindi . "</div>";
            echo "<input type='hidden' name='examId' id='examId' value='" . $row['Exam_id'] . "'>";
            echo "<input type='hidden' name='itgkcode' id='itgkcode' value='" . $row['User_LoginId'] . "'>";
            echo "<input type='hidden' name='rspcode' id='rspcode' value='" . $row['User_Rsp'] . "'>";
            echo "<input type='hidden' name='districtcode' id='districtcode' value='" . $row['Organization_District'] . "'>";
            echo "<input type='hidden' name='facultycode' id='facultycode' value='".$staff."'>";
            echo "<input type='hidden' name='realattempt' id='realattempt' value='" . $_POST['attempt'] . "'>";
            echo "<input type='hidden' name='examAttempt' id='examAttempt' value='" . $row['Exam_attempt'] . "'>";
            echo "<input type='hidden' name='chapterCode' id='chapterCode' value='" . $row['Exam_chaptercode'] . "'>";
            echo "<input type='hidden' name='selectedTopic' id='selectedTopic' value='" . $row['Exam_selectedtopic'] . "'>";
            echo "<div class='col-md-12' style='text-align: center;'>";
            echo "<input type='checkbox' value='0' name='agree' style='margin-top: 15px;'>  I Agree Terms & Instructions<br>";
            echo "<input type='button' style='margin-right: 50px;margin-top: 10px;' class='toplablel onlinesubmit' value='Start Exam' id='examidinst' name='examidinst' onclick='fullscreen()'>";
            echo "</div>";
        } else {
            echo "error to find exam";
        }  
    }
}
if ($_POST['action'] == "QuestionPaper") {
    $userid = $_POST['facultycode'];
    $userexamid = round(microtime(true)) . "_" . $userid;
    $_SESSION['username'] = $userexamid;
    $examidd = $_POST['examId'];
    $chaptercode = $_POST['chapterCode'];
    $attempt = $_POST['examAttempt'];
    $realattempt = $_POST['realattempt'];
    $itgkcode = $_POST['itgkcode'];
    $rspcode = $_POST['rspcode'];
    $districtcode = $_POST['districtcode'];
        $response = $emp->OneQuetion($examidd, $_POST['selectedTopic'], $userid, $userexamid, $chaptercode, $attempt, $realattempt, $itgkcode, $rspcode, $districtcode);
        $responseHeader = $emp->OneQuetionHeader($examidd, $_POST['selectedTopic'], $userid, $chaptercode, $attempt, $realattempt);
        $_RowUp = mysqli_fetch_array($responseHeader[2]);
        $responseTime = $emp->OneQuetionTimer($examidd, $userid, $realattempt);
        $_RowTimer = mysqli_fetch_array($responseTime[2]);
        echo "<div style='font-weight: bold;font-size: 18px; width: 20%;'>Exam Time :- " . $_RowUp['Exam_time_duration'] . " Min.</div>";
        echo "<div style='font-weight: bold;font-size: 18px;'>Exam Name :- " . $_RowUp['Exam_name'] . "</div>";
        if (isset($_RowTimer['timer'])) {
            $timer = $_RowTimer['timer'];
        } else {
            $timer = $_RowUp['Exam_time_duration'];
        }
        echo "<input id='duration' name='duration' type='hidden' value='" . $timer . "' />";
        echo "<input id='examid' name='examid' type='hidden' value='" . $_RowUp['Exam_id'] . "' />";
        echo "<input id='staffcode' name='staffcode' type='hidden' value='" . $_POST['facultycode'] . "' />";
        echo "<input id='realattemptforres' name='realattemptforres' type='hidden' value='" . $realattempt . "' />";
        echo "<input id='resExamname' name='resExamname' type='hidden' value='" . $_RowUp['Exam_name'] . "' />";
        echo "<hr class='style5'>";
         $num_rows = mysqli_num_rows($response[2]);
         $rocup = $_RowUp["Exam_attempt"];
        if ($num_rows == $rocup) {
            if ($response[0] == 'Success') {
                $_Count = 1;
                $inserttedRecord = 0;
                $queidfornumber = array();
                while ($_Row = mysqli_fetch_array($response[2])) {
                    $queidfornumber[] = $_Row['Que_id'];
                    $responseResultTable = $emp->AssessmentResult($examidd, $userid, $userexamid, $_Row['Que_id'], $_Row['Exam_Course'], $_Row['Exam_Batch'], $_Row['Exam_time_duration'], $realattempt);
                    if ($responseResultTable[0] == 'Successfully Inserted' || $responseResultTable[0] == 'Successfully Updated') {

                        echo "<div style='width:75%;float:left;' class='Quepost' data-cat='Fillter" . $_Count . "' class='Fillter" . $_Count . "'>";
                        $queDesc = html_entity_decode($_Row['Que_description']);
                        echo "<div id='queheading'><span class='queseries'>Q. " . $_Count . ") </span> " . $queDesc . "</div>";
                        echo "<input type='hidden' id='que_id" . $_Count . "' name='que_id" . $_Count . "' value='" . $_Row['Que_id'] . "'/>";
                        echo "<ul class='objectivq' style='width: 75%; float: left;'>";
                        $qidfc = $_Row['Que_id'];
                        $qifid = $_Row['Exam_id'];
                        if (isset($queDesc) || !empty($queDesc)) {
                            $inserttedRecord++;
                        }
                        $response2 = $emp->GetQuetionOption($_Row['Que_id'], $realattempt);
                        while ($_Row2 = mysqli_fetch_array($response2[2])) {
                            $response1 = $emp->OneQuetionCountgreen($qifid, $qidfc, $userid, $realattempt);
                            $row1 = mysqli_fetch_array($response1[2]);
                            $cans = $row1['Result_answer'];
                            echo "<li class='objectivop'>";
                            if ($cans == '1') {
                                echo "<input type='radio' class='objanimation' value='1' name='useranswer_" . $_Row['Que_id'] . "' checked='checked'>";
                            } else {
                                echo "<input type='radio' class='objanimation' value='1' name='useranswer_" . $_Row['Que_id'] . "'>";
                            }
                            echo "<label class='qlabel'>" . $_Row2['Option_1'] . "</label>";
                            echo "<div class='bullet'>";
                            echo "<div class='line zero'></div>";
                            echo "<div class='line one'></div>";
                            echo "<div class='line two'></div>";
                            echo "<div class='line three'></div>";
                            echo "<div class='line four'></div>";
                            echo "<div class='line five'></div>";
                            echo "<div class='line six'></div>";
                            echo "<div class='line seven'></div>";
                            echo "</div>";
                            echo "</li>";
                            echo "<li class='objectivop'>";
                            if ($cans == '2') {
                                echo "<input type='radio' class='objanimation' value='2' name='useranswer_" . $_Row['Que_id'] . "' checked='checked'>";
                            } else {
                                echo "<input type='radio' class='objanimation' value='2' name='useranswer_" . $_Row['Que_id'] . "'>";
                            }
                            //echo "<input type='radio' class='objanimation' value='2' name='useranswer'>";
                            echo "<label class='qlabel'>" . $_Row2['Option_2'] . "</label>";
                            echo "<div class='bullet'>";
                            echo "<div class='line zero'></div>";
                            echo "<div class='line one'></div>";
                            echo "<div class='line two'></div>";
                            echo "<div class='line three'></div>";
                            echo "<div class='line four'></div>";
                            echo "<div class='line five'></div>";
                            echo "<div class='line six'></div>";
                            echo "<div class='line seven'></div>";
                            echo "</div>";
                            echo "</li>";
                            if ($_Row2["Option_3"] != 'NA') {
                                echo "</li>";
                                echo "<li class='objectivop'>";
                                if ($cans == '3') {
                                    echo "<input type='radio' class='objanimation' value='3' name='useranswer_" . $_Row['Que_id'] . "' checked='checked'>";
                                } else {
                                    echo "<input type='radio' class='objanimation' value='3' name='useranswer_" . $_Row['Que_id'] . "'>";
                                }
                                //echo "<input type='radio' class='objanimation' value='3' name='useranswer'>";
                                echo "<label class='qlabel'>" . $_Row2['Option_3'] . "</label>";
                                echo "<div class='bullet'>";
                                echo "<div class='line zero'></div>";
                                echo "<div class='line one'></div>";
                                echo "<div class='line two'></div>";
                                echo "<div class='line three'></div>";
                                echo "<div class='line four'></div>";
                                echo "<div class='line five'></div>";
                                echo "<div class='line six'></div>";
                                echo "<div class='line seven'></div>";
                                echo "</div>";
                                echo "</li>";
                            }
                            if ($_Row2["Option_4"] != 'NA') {
                                echo "</li>";
                                echo "<li class='objectivop'>";
                                if ($cans == '4') {
                                    echo "<input type='radio' class='objanimation' value='4' name='useranswer_" . $_Row['Que_id'] . "' checked='checked'>";
                                } else {
                                    echo "<input type='radio' class='objanimation' value='4' name='useranswer_" . $_Row['Que_id'] . "'>";
                                }
                                //echo "<input type='radio' class='objanimation' value='4' name='useranswer'>";
                                echo "<label class='qlabel'>" . $_Row2['Option_4'] . "</label>";
                                echo "<div class='bullet'>";
                                echo "<div class='line zero'></div>";
                                echo "<div class='line one'></div>";
                                echo "<div class='line two'></div>";
                                echo "<div class='line three'></div>";
                                echo "<div class='line four'></div>";
                                echo "<div class='line five'></div>";
                                echo "<div class='line six'></div>";
                                echo "<div class='line seven'></div>";
                                echo "</div>";
                                echo "</li>";
                            }
                            if ($_Row2["Option_5"] != 'NA') {
                                echo "</li>";
                                echo "<li class='objectivop'>";
                                if ($cans == '5') {
                                    echo "<input type='radio' class='objanimation' value='5' name='useranswer_" . $_Row['Que_id'] . "' checked='checked'>";
                                } else {
                                    echo "<input type='radio' class='objanimation' value='5' name='useranswer_" . $_Row['Que_id'] . "'>";
                                }
                                //echo "<input type='radio' class='objanimation' value='5' name='useranswer'>";
                                echo "<label class='qlabel'>" . $_Row2['Option_5'] . "</label>";
                                echo "<div class='bullet'>";
                                echo "<div class='line zero'></div>";
                                echo "<div class='line one'></div>";
                                echo "<div class='line two'></div>";
                                echo "<div class='line three'></div>";
                                echo "<div class='line four'></div>";
                                echo "<div class='line five'></div>";
                                echo "<div class='line six'></div>";
                                echo "<div class='line seven'></div>";
                                echo "</div>";
                                echo "</li>";
                            }
                        }
                        echo "</ul>";
                        echo "<button class='toplablel onlinesubmitbtnans examonlinesubmit savenextubmit' data-filter='Fillter2' id='" . $_Row['Que_id'] . "' name='examonlinesubmit' type='button' title='Submit Question' style='float: left;width: 11%; z-index: 9999;position:absolute; left:344px; bottom:117px' ><span style='margin: 0 10px 0 0;' class='glyphicon glyphicon-ok'></span>Save & Next</button>";
                        echo "</div>";
                        $_Count++;
                    }
                }

                $_CountCir = 1;
                echo "<div style='min-height: 395px;width:23%;float:right; border: 1px solid #000;padding: 10px;'><div id='questioncoutactive'>";
                foreach ($queidfornumber as $QueId) {
                    $responseGreen = $emp->OneQuetionCountgreen($examidd, $QueId, $userid, $realattempt);
                    while ($rowGreen = mysqli_fetch_array($responseGreen[2])) {
                        if ($rowGreen['Result_answer'] == 1 || $rowGreen['Result_answer'] == 2 || $rowGreen['Result_answer'] == 3 || $rowGreen['Result_answer'] == 4 || $rowGreen['Result_answer'] == 5) {
                            echo "<div  class='round-button'><div class='round-button-circle' style='background-color: rgb(69, 202, 69) !important;' id='Green_" . $rowGreen['Result_queid'] . "'><a href='#' data-filter='Fillter" . $_CountCir . "' class='round-button numberquebtn QueNumberString' id='" . $_CountCir . "' name='" . $rowGreen['Result_queid'] . "'>" . $_CountCir . "</a></div></div>";
                        } else {
                            echo "<div  class='round-button'><div class='round-button-circle' id='Green_" . $rowGreen['Result_queid'] . "'><a href='#' data-filter='Fillter" . $_CountCir . "' class='round-button numberquebtn QueNumberString' id='" . $_CountCir . "' name='" . $rowGreen['Result_queid'] . "'>" . $_CountCir . "</a></div></div>";
                        }
                    }
                    $_CountCir++;
                }
                echo "<input type='hidden' name='disableCount' id='disableCount' value='" . $_CountCir . "'>";
                echo"</div></div>";

                echo "<div style='border: 1px solid #000; width: 23%; float: right; padding: 10px 0px; clear:both;'><button style='margin-right: 30px;' class='toplablel comsubmit' id='completeexamsubmit' name='completeexamsubmit' type='button' title='Submit Exam'><span style='margin: 0 10px 0 0;' class='glyphicon glyphicon-paste'></span>Submit Or Complete Exam</button></div>";
                echo "</div>
                  <div class='form-group last'>
                      <div class='col-sm-8 nextprebtn'>
                          <button class='toplablel onlinesubmit' data-filter='' id='presubmit' name='presubmit' type='button' title='Previous Question' ><span style='margin: 0 10px 0 0;' class='glyphicon glyphicon-circle-arrow-left'></span>Previous</button>
                          			
                          <button class='toplablel onlinesubmit' data-filter='Fillter2' id='nextubmit' name='nextubmit' type='button' title='Next Question'>Skip<span style='margin: 0 0 0 10px;' class='glyphicon glyphicon-circle-arrow-right'></span></button>
                          
                      </div>
                  </div>
                  <div class='warningsubmit'>For image based question please click on image to see in zoom mode</div>
                  <div class='warningsubmit'>*** Exam will be submitted automatically after completion of time. Do not turn off your computer or browser ***</div>
                  </div>";
                if ($inserttedRecord != $_RowUp["Exam_attempt"]) {
                    $realA = $realattempt;
                    $delexamid = $_RowUp['Exam_id'];
                    $dellearner = $userid;
                    $delresponse = $emp->DeleteRecordLearnerExam($delexamid, $dellearner, $realA);
                    echo 'Something went wrong. Please try again and leave the page.';
                    echo "<script>
                            alert('Something went wrong. Please try again and leave the page.');
                            window.setTimeout(function ()
                                    {
                                       window.onbeforeunload = null;
                                       window.location.href = 'frmfacultyexam.php';
                                    }, 1000);

                            </script>";
                }
            }
        } else {
            $realA = $realattempt;
            $delexamid = $_RowUp['Exam_id'];
            $dellearner = $userid;
            $delresponse = $emp->DeleteRecordLearnerExam($delexamid, $dellearner, $realA);
            if ($delresponse[0] == 'Successfully Deleted') {
                echo 'Something went wrong. Please try again and leave the page.';
                echo "<script>
                alert('Something went wrong. Please try again and leave the page.');
                window.setTimeout(function ()
                        {
                           window.onbeforeunload = null;
                           window.location.href = 'frmfacultyexam.php';
                        }, 3000);
               
                </script>";
            } else {
                echo 'Something went wrong. Please try again and leave the page.';
                echo "<script>
                    alert('Something went wrong. Please try again and leave the page.');
                    window.setTimeout(function ()
                            {
                               window.onbeforeunload = null;
                               window.location.href = 'frmfacultyexam.php';
                            }, 3000);

                    </script>";
            }
        }
}

if ($_POST['action'] == "SubmitQuestion") {
    $durationexit = $_POST['durationexit'];
    $staffcode = $_POST['staffcode'];
    $examid = $_POST['examid'];
    $queid = $_POST['queid'];
    $currect = "useranswer_" . $queid;
    if (isset($_POST[$currect])) {
        $answer = $_POST[$currect];
    } else {
        $answer = "0";
    }
    $answer;
    $realattempt = $_POST['realattemptforres'];
    $userid = $staffcode;

    $response = $emp->SubmitExamQue($examid, $queid, $answer, $userid, $durationexit, $realattempt);
    echo $response[0];
}

if ($_POST['action'] == "SubmitQuestionCompletion") {
    $examid = $_POST['examid'];
    $staffcode = $_POST['staffcode'];
    $realattempt = $_POST['realattempt'];
    $userid = $staffcode;
    $response = $emp->SubmitExamCompletion($examid, $userid, $realattempt);
    echo $response[0];
}
if ($_POST['action'] == "ShowLearnerResult") {
   $examid = $_POST['examid'];
   $staffcode = $_POST['staffcode'];
    $res = $emp->CheckExamId();
    $error = "";
    while ($exam = mysqli_fetch_array($res[2])) {
        if($exam['Exam_id'] == $examid){
            $userid = $staffcode;
            $realattempt = $_POST['realattempt'];
            $resExamname = $_POST['resExamname'];
            $itgkcode = $_POST['itgkcode'];
            $rspcode = $_POST['rspcode'];
            $lat = $_POST['lat'];
            $long = $_POST['long'];
            $districtcode = $_POST['districtcode'];
            $response = $emp->ShowLearnerResult($examid, $userid, $realattempt);
            $_Count = 1;
            $marks = '0';
//            echo "<div class='table-users'>";
//            echo "<div class='table-responsive tablescroll'>";
//            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered' style='margin-bottom: 10px;'>";
//            echo "<thead>";
//            echo "<tr>";
//            echo "<th >S No.</th>";
//            echo "<th >Exam Name</th>";
//            echo "<th >Maximum Marks</th>";
//            echo "<th >Obtained Marks</th>";
//            echo "<th >Total Que.</th>";
//            echo "<th >No of Attempted Que.</th>";
//            echo "<th >No of Currect Ans.</th>";
//            echo "</tr>";
//            echo "</thead>";
//            echo "<tbody>";
            $calmarks = $emp->Markscalculations($examid);
            $markrow = mysqli_fetch_array($calmarks[2]);
            $totalmarks = $markrow['Exam_totalmarks']; 
            $totalque = $markrow['Exam_attempt'];
            $singlequemarks = $totalmarks/$totalque;
            if ($response[0] == 'Success') {
                while ($row = mysqli_fetch_array($response[2])) {
                    $Canswer = '';
                    $CheckAns = '';
                    if ($row['Option_currect_ans1'] == '1') {
                        $Canswer = "Option_1";
                        $CheckAns = "1";
                    } else if ($row['Option_currect_ans2'] == '1') {
                        $Canswer = "Option_2";
                        $CheckAns = "2";
                    } else if ($row['Option_currect_ans3'] == '1') {
                        $Canswer = "Option_3";
                        $CheckAns = "3";
                    } else if ($row['Option_currect_ans4'] == '1') {
                        $Canswer = "Option_4";
                        $CheckAns = "4";
                    } else if ($row['Option_currect_ans5'] == '1') {
                        $Canswer = "Option_5";
                        $CheckAns = "5";
                    }

                    if ($CheckAns == $row['Result_answer']) {
                        $marks++;
                    }
                    $attemptRes = $_POST['realattempt'];
                    $batchRes = $row['Result_batch'];
                    $coursetRes = $row['Result_course'];
                    $_Count++;
                }

                $finalmarks = $marks*$singlequemarks;
//				if($finalmarks > 2){
//                    $finalmarks = 2;
//                }
//                echo "<tr class='odd gradeX'>";
//                echo "<td style='font-family: initial;vertical-align: middle;'> 1 </td>";
//                echo "<td style='font-family: initial;vertical-align: middle;'>" . $resExamname . "</td>";
//                echo "<td style='font-family: initial;vertical-align: middle;'>" . $totalmarks . "</td>";
//                echo "<td style='font-family: initial;vertical-align: middle;'>" . $finalmarks . "</td>";
//                echo "<td style='font-family: initial;vertical-align: middle;'>" . $totalque . "</td>";
                $responseAtt = $emp->ShowLearnerResultAtt($examid, $userid, $realattempt);
                $QueAtt = mysqli_fetch_array($responseAtt[2]);
//                echo "<td style='font-family: initial;vertical-align: middle;'>" . $QueAtt['QueAtt'] . "</td>";
//                echo "<td style='font-family: initial;vertical-align: middle;'>" . $marks . "</td>";
//                echo "</tr>";
//                echo "</tr>";
                $_Count++;
//                echo "<script>document.getElementById('panelresult').style.display = 'block';"
//                . "document.getElementById('getmarks').innerHTML = " . $finalmarks . ";"
//                . "document.getElementById('totalmarks').innerHTML = " . $totalmarks . ";</script>";
                $responseResultsubmit = $emp->Resultsubmit($examid, $userid, $finalmarks, $totalmarks ,$attemptRes, $batchRes, $coursetRes,$itgkcode,$rspcode,$districtcode,$lat,$long);
                // PutFacultyIlearnerScore($data);
                if ($responseResultsubmit[0] == 'Successfully Inserted') {
                    echo "Success";
                     //echo "<script>document.getElementById('resmassage').innerHTML = 'Exam has been submited successfully';</script>";
                }else{
                     echo "Error";
                    //echo "<script>document.getElementById('resmassage').innerHTML = 'Error to submit result';</script>";
                }
            }
//            echo "</tbody>";
//            echo "</table>";
//            echo "</div>";
//            echo "</div>";
//            echo "<style>#errmsg{display: none;}</style>";
    } 
    else{
        $error = "<div id='errmsg'>Error to complete assessment please try again....</div>";
    }
  }
  echo $error;
}
 
 
?>