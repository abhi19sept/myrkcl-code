<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsgovupdatebankdetails.php';

    $response = array();
    $emp = new clsgovupdatebankdetails();

	if ($_action == "EDIT") {
		if($_POST['editid'] !='') {
        $response = $emp->GetBankDetails($_POST['editid']);
		$_DataTable = array();
			$_i = 0;
			if($response[0]=='Success'){
				while ($_Row = mysqli_fetch_array($response[2])) {
				 $_Datatable[$_i] = array("empaccountno" => $_Row['empaccountno'],
					 "gifsccode" => $_Row['gifsccode'],
					 "learnercode" => $_Row['learnercode']);
				 $_i = $_i + 1;
				}
			}
			echo json_encode($_Datatable);
		}
    }
	
	if ($_action == "BANKDETAILSUPDATE") {
		if($_POST['accnumber'] !='' && $_POST['ifscnumber'] !='') {
			$lcode = $_POST['lcode'];
			$accnumber = $_POST['accnumber'];
			$ifsccode = $_POST['ifscnumber'];
			$response = $emp->UpdateBankDetails($lcode, $accnumber, $ifsccode);
			echo $response[0];
		}else{
			echo "0";
		}
    }
	
    if ($_action == "DETAILS") {
		if($_POST['values'] !='') {
        $response = $emp->GetBankDetails($_POST['values']);
        $_DataTable = "";
	
		
			echo "<div class='table-responsive'>";
			echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
			echo "<thead>";
			echo "<tr>";
			echo "<th style='5%'>S No.</th>";
			echo "<th style='30%'>ITGK-CODE</th>";
			echo "<th style='55%'>Employee Name</th>";
			echo "<th style='10%'>Father/Husband Name</th>";
			echo "<th style='10%'>Bank Name</th>";
			echo "<th style='10%'>Branch Name</th>";
			echo "<th style='10%'>Account No.</th>";
			echo "<th style='10%'>IFSC Code</th>";
			echo "<th style='10%'>Action</th>";
			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			if($response[0] == 'Success') {
			$_Count = 1;
			while ($_Row = mysqli_fetch_array($response[2])) {
				echo "<tr class='odd gradeX'>";
				echo "<td>" . $_Count . "</td>";
				echo "<td>" . $_Row['Govemp_ITGK_Code'] . "</td>";
				echo "<td>" . $_Row['fname'] . "</td>";
				echo "<td>" . $_Row['faname'] . "</td>";            
				echo "<td>" . $_Row['gbankname'] . "</td>";            
				echo "<td>" . $_Row['gbranchname'] . "</td>";            
				echo "<td>" . $_Row['empaccountno'] . "</td>";            
				echo "<td>" . $_Row['gifsccode'] . "</td>";   
				echo "<td>"
					. "<input type='button' name='Edit' id='" .$_Row['learnercode']. "' class='btn btn-primary fun_update_slider' value='EDIT'/>"
					. "</td>";  
				echo "</tr>";
				$_Count++;
			}
			echo "</tbody>";
			echo "</table>";
			echo "</div>";
		}
		       
	}
	  else {
		  echo "1";
	  }
    }
?>