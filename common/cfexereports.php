<?php

/*
 * @author yogi

 */
include './commonFunction.php';
require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();
date_default_timezone_set("Asia/Kolkata");

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

require_once 'BAL/clsexereports.php';

$clsObj = new clsexecutesqls();

$response = array();
if ($_action == "Add") {
	if (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == '1') {
		echo $clsObj->Add(serialize($_POST));
	}
}

if ($_action == "GETDATA") {
	$responce = $clsObj->getall();
	if ($responce) {
		echo "<div class='table-responsive'>";
	    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
	    echo "<thead>";
	    echo "<tr>";
	    echo "<th>SNo.</th>";
	    echo "<th>SQL Statements</th>";
	    echo "<th>Action</th>";
	    echo "</tr>";
	    echo "</thead>";
	    echo "<tbody>";
	    $_Count = 1;
		foreach ($responce as $row) {
			$action = ($row['status']) ? '<img title="Delete" class="aslink" id="del-' . $row['id'] . '" src="./images/settingsDelete.gif"/> &nbsp; <img title="Edit" class="aslink" id="edit-' . $row['id'] . '" src="./images/settingsEdit.gif"/>' : '';
			$execute = ($row['status']) ? '<input type="button" value="Execute" title="Execute" class="aslink execute" id="execute-' . $row['id'] . '" />' : '';
			echo "<tr>";
			echo "<td rowspan='2'>" . $_Count . "</td>";
			echo "<td valign='center'><b><span id='subject-" . $row['id'] . "'>" . stripcslashes($row['subject']) . "</span></b></td>";
			echo "<td align='center'><span id='act_del_" . $row['id'] . "'>" . $action . "</span></td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td valign='center'><span id='qry-" . $row['id'] . "'>" . stripcslashes($row['qry']) . "</span></td>";
			echo "<td align='center'><span id='act_execute_" . $row['id'] . "'>" . $execute . "</span></td>";
			echo "</tr>";
			$_Count++;
		}
		echo "</tbody>";
	    echo "</table>";
	    echo "</div>";
	}
}

if ($_action == 'exesqls') {
	if (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == '1') {
		$act = explode('-', $_POST['elemId']);
		if ($act[0] == 'del') {
			$clsObj->deleteSQl($act[1]);
		} elseif ($act[0] == 'execute') {
			$response = $clsObj->executeSQl($act[1]);
			if ($response) {
				processQueryResponse($response);
			}
		}
	}
}

function processQueryResponse($response) {
	echo "<div class='table-responsive'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    foreach ($response['cols'] as $field) {
    	echo "<th nowrap>" . $field . "</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    foreach ($response['data'] as $row) {
    	echo "<tr>";
    	foreach ($response['cols'] as $field) {
    		echo "<td>" . $row[$field] . "</td>";
    	}
    	echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
