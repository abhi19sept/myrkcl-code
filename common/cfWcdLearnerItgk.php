<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsWcdLearnerItgk.php';

$response = array();
$emp = new clsWcdLearnerItgk();


if ($_action == "showdata") {
	if($_POST['batch'] !='') {
    $response = $emp->getdatawcd($_POST['course'],$_POST['batch']);

   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
					$_LoginUserRole = $_SESSION['User_UserRoll'];
					if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4') {
				//echo "<th>District Name</th>";
					echo "<th>ITGK Code</th>";
					}
					else {
				echo "<th>ITGK Code</th>";		
					}
				
				echo "<th>Approved Applicant Count</th>";
				echo "<th>Reported Applicant Count</th>";
				echo "<th>Not Reported Applicant Count</th>";
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;
					$_Total = 0;
					$_ATotal = 0;
					$_NTotal = 0;
				while ($_Row = mysqli_fetch_array($response[2])) {
				//	if($_Row['pref'] > 9) {
							echo "<tr class='odd gradeX'>";
							echo "<td>" . $_Count . "</td>";
									$_LoginUserRole = $_SESSION['User_UserRoll'];
								if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4') {
										//echo "<td>" . $_Row['District_Name'] . "</td>";		
										echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";		
										echo "<td>" . $_Row['pref'] . "</td>";		
										echo "<td>" . $_Row['Reported_Learner'] . "</td>";											
										echo "<td>" . $_Row['NotReported_Learner'] . "</td>";
								}
								else {
										echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";	
										echo "<td><a href='frmWcdApprovedLearnerCountList.php?batch=" . $_POST['batch'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=one' target='_blank'>"
												. "" . $_Row['pref'] . "</a></td>";
											if($_Row['Reported_Learner'] > 0)	{
										echo "<td><a href='frmWcdApprovedLearnerCountList.php?batch=" . $_POST['batch'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=two' target='_blank'>"
										. "" . $_Row['Reported_Learner'] . "</a></td>";									
											}
											else {
										echo "<td>" . $_Row['Reported_Learner'] . "</td>";
											}
											
											if($_Row['NotReported_Learner'] > 0) {
												echo "<td><a href='frmWcdApprovedLearnerCountList.php?batch=" . $_POST['batch'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=three' target='_blank'>"
										. "" . $_Row['NotReported_Learner'] . "</a></td>";	
											}
											else {
												echo "<td>" . $_Row['NotReported_Learner'] . "</td>";
											}
										
								}						
							
								echo "</tr>";
								$_Total = $_Total + $_Row['pref'];
								$_ATotal = $_ATotal + $_Row['Reported_Learner'];
								$_NTotal = $_NTotal + $_Row['NotReported_Learner'];
							$_Count++;	
				//	}
								
				}
				 echo "</tbody>";
					
						echo "<tfoot>";
						echo "<tr>";
						echo "<th >  </th>";
						echo "<th >Total Count </th>";
						echo "<th>";
						echo "$_Total";
						echo "</th>";
						echo "<th >";
						echo "$_ATotal";
						echo "</th>";
						echo "<th >";
						echo "$_NTotal";
						echo "</th>";
						echo "</tr>";
						echo "</tfoot>";
		
				 echo "</table>";
				 echo "</div>";
		 
		}
		  else {
			  echo "1";
		  }
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetLearnerList($_POST['mode'], $_POST['batch'], $_POST['districtcode'], $_POST['itgkcode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";   
    echo "<th>Applicant Id</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";	
    echo "<th >Marital Status</th>";	
    echo "<th >Sub Category</th>";	
	echo "<th >Caste Category</th>";
	echo "<th >Qualification</th>";
	echo "<th >Mobile No.</th>";	
    echo "<th >Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";			
            echo "<td>" . $_Row['Oasis_Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_MaritalStatus'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_Subcategory'] . "</td>";
			
			echo "<td>" . $_Row['Category_Name'] . "</td>";
			echo "<td>" . $_Row['Qualification_Name'] . "</td>";
			
			echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
				if($_Row['Oasis_Admission_ReportedStatus'] == 'Reported') {
					echo "<td>" . 'Reported' . "</td>";
				}
				else if($_Row['Oasis_Admission_ReportedStatus'] == 'Not Reported') {
					echo "<td>" . 'Not Reported' . "</td>";
						
				}
				else {
					echo "<td> <a href='frmwcdupdatereportedstatus.php?code=" . $_Row['Oasis_Admission_LearnerCode'] . "&month=" . $_Row['Oasis_Admission_Batch'] . "&Mode=Reported'>"
					. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='Not Reported'/></a>"						
					. "</td>";
				}
           
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_POST['batch'], $_POST['lcode']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("LearnerCode" => $_Row['Oasis_Admission_LearnerCode'],
            "lname" => $_Row['Oasis_Admission_Name'],
            "fname" => $_Row['Oasis_Admission_Fname'],
            "dob" => $_Row['Oasis_Admission_DOB'],
			"itgk" => $_Row['Oasis_Admission_Final_Preference'],
			"district" => $_Row['Oasis_Admission_District'],			
			"minper" => $_Row['Oasis_Admission_MINPercentage'],			
			"highper" => $_Row['Oasis_Admission_Percentage'],			
            "mstatus" => $_Row['Oasis_Admission_MaritalStatus'],            
			"categoryname" => $_Row['Category_Name'],
			"category" => $_Row['Oasis_Admission_Category'],
			"mobile" => $_Row['Oasis_Admission_Mobile'],
			"qualification" => $_Row['Qualification_Name'],
			"caste" => $_Row['Oasis_Admission_Caste'],
			"address" => $_Row['Oasis_Admission_Address'],
			"photo" => $_Row['Oasis_Admission_Photo'],
			"sign" => $_Row['Oasis_Admission_Sign'],
			"mincert" => $_Row['Oasis_Admission_MinCertificate'],
			"highcert" => $_Row['Oasis_Admission_HighCertificate'],
			"ageproof" => $_Row['Oasis_Admission_AgeProof'],
			"casteproof" => $_Row['Oasis_Admission_CasteProof']);        
        $_i = $_i + 1;
		
    }
	
    echo json_encode($_DataTable);
}

if ($_action == "UpdateNotReportingLearner") {	
	$_LCode = $_POST["lcode"];
	$_ProcessStatus = $_POST["status"];	                       
	
	$response = $emp->UpdateWcdNotReported($_LCode, $_ProcessStatus);
	echo $response[0];	
	
}

if ($_action == "UpdateApproveReportedLearner") {	
	$_Batch = $_POST["batch"];
	$_ITGK = $_POST["itgkcode"];	                       
	
	$response = $emp->UpdateWcdReported($_Batch, $_ITGK);
	echo $response[0];	
	
}

if ($_action == "showdataitgk") {
	//print_r($_POST);
		if($_POST['batch'] !='') {
    $response = $emp->getdatawcditgk($_POST['course'],$_POST['batch']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
   // echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
	echo "<th >Mobile No.</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0]=='Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
           // echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Oasis_Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Oasis_Admission_Fname']) . "</td>";
			echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
             if ($_Row['Oasis_Admission_Photo'] != "") {
                $image = $_Row['Oasis_Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/oasis_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Oasis_Admission_Sign'] != "") {
                $sign = $_Row['Oasis_Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/oasis_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            } 
			 
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
	}
		  else {
			  echo "1";
		  }
}

if ($_action == "FillCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

?>