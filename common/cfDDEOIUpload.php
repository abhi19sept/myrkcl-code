<?php

session_start();

//$_SESSION['ImageFile'] = "";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

$_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/EOI_dd_payment/';

$parentid = $_POST['UploadId'];
if ($_FILES["SelectedFile1"]["name"] != '') {
    $imag = $_FILES["SelectedFile1"]["name"];
    $imageinfo = pathinfo($_FILES["SelectedFile1"]["name"]);
    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg") {
        $error_msg = "Image must be in either PNG or JPG Format";
        $flag = 0;
    } else {
        if (file_exists("$_UploadDirectory1/" . $parentid . '_payment' . '.' . substr($imag, -3))) {
            $_FILES["SelectedFile1"]["name"] . "already exists";
        } else {
            if (file_exists($_UploadDirectory1)) {
                if (is_writable($_UploadDirectory1)) {
                    move_uploaded_file($_FILES["SelectedFile1"]["tmp_name"], "$_UploadDirectory1/" . $parentid . '_ddpayment' . '.png');
                    //session_start();
                    $_SESSION['PaymentReceiptFile'] = $parentid . '_ddpayment' . '.png';
                } else {
                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
                }
            } else {
                outputJSON("<span class='error'>Upload Folder Not Available</span>");
            }
        }
    }
}
				