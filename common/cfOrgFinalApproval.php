<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsOrgFinalApproval.php';

$response = array();
$emp = new clsOrgFinalApproval();


if ($_action == "APPROVE") {
    $response = $emp->GetDatabyCode($_POST['values']);
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if($co){
    while ($_Row = mysqli_fetch_array($response[2])) {
       
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "doctype" => $_Row['Organization_DocType'],
           
            "district" => $_Row['Organization_District_Name'],
            "districtcode" => $_Row['Organization_District'],
            "tehsil" => $_Row['Organization_Tehsil'],
           
            "road" => $_Row['Organization_Address']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
} else {
        echo "";
    }
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>AO Code</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='10%'>IT-GK Type</th>";
    echo "<th style='40%'>IT-GK Mob No</th>"; 
    echo "<th style='40%'>IT-GK Email Id</th>";
    echo "<th style='40%'>IT-GK Address</th>";
   
    echo "<th style='40%'>IT-GK District</th>";
    echo "<th style='10%'>IT-GK Tehsil</th>";
    echo "<th style='10%'>IT-GK Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['User_Ack'] . "</td>";
            echo "<td>" . $_Row['User_LoginId'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Type'] . "</td>";
            echo "<td>" . $_Row['User_MobileNo'] . "</td>";
            echo "<td>" . $_Row['User_EmailId'] . "</td>";
            echo "<td>" . $_Row['Organization_Address'] . "</td>";
            echo "<td>" . $_Row['Organization_District_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";
            
            if ($_Row['User_UserRoll'] == '15' && ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11)) {
                echo "<td> <a href='frmorgfinalapproval.php?code=" . $_Row['User_LoginId'] . "&Mode=Add'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='Approve'/></a>"
            . "</td>";
            }elseif ($_Row['User_UserRoll'] == '15' && $_SESSION['User_UserRoll'] == 14 ) {
                echo "<td>" . 'Pending for Final Approval' . "</td>";
            } 
            elseif ($_Row['User_UserRoll'] == '7' ) {
                echo "<td>" . 'Approved' . "</td>";
            } else {
                echo "<td>" . 'NA' . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}





if ($_action == "ADD") {
    if (isset($_POST["centercode"])) {
        $_CenterCode = $_POST["centercode"];

        $response = $emp->Upgrade($_CenterCode);
        echo $response[0];
    }
}