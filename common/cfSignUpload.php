<?php
session_start();
//$_SESSION['SignFile'] = "";

// Output JSON

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

$_UploadDirectory = $_SERVER['DOCUMENT_ROOT'] . '/myrkcladmin/upload/admission_sign/';

$parentid = $_POST['UploadId'];
				if($_FILES["SelectedFile"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
						  if (file_exists("$_UploadDirectory/" .$parentid .'_sign'.'.'.substr($imag,-3)))
						   {
							 $_FILES["SelectedFile"]["name"]. "already exists";
						   }
						  else
						   {
								if (file_exists($_UploadDirectory)) {
									if (is_writable($_UploadDirectory)) {
								move_uploaded_file($_FILES["SelectedFile"]["tmp_name"], "$_UploadDirectory/".$parentid .'_sign'.'.png');
								$_SESSION['SignFile'] =  $parentid .'_sign'.'.png';
									} else {
            outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
        }
    } else {
        outputJSON("<span class='error'>Upload Folder Not Available</span>");
    }
						   }  				
						 }	
			       }