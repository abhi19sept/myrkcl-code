<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsLotMaster.php';

$response = array();
$emp = new clsLotMaster();

if ($_action == "ADD") {
	
    if (isset($_POST["txtLotName"])) {
        $_LotName = $_POST["txtLotName"];
        $_Sdate=$_POST["txtstartdate"];
        $_StatusField=$_POST["ddlStatus"];        
		$_Role=$_POST["ddlrole"];        
        $_Year=$_POST["ddlYear"];
		
		if($_StatusField == 1){
			$_Status = 'Active';
		}
		else{
			$_Status = 'Deactive';
		}
		
		if($_Role == 1){
			
			$response = $emp->AddGovRole($_LotName,$_Sdate,$_Status,$_Role,$_Year);
		}
		else{
			$response = $emp->AddCorrectionRole($_LotName,$_Sdate,$_Status,$_Role,$_Year);
		}
        echo $response[0];
    }
}