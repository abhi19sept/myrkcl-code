<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsdownloadreprocessdata.php';

$response = array();
$emp = new clsdownloadreprocessdata();


	if ($_action == "ShowDetailsToDownload") {
    //echo "Show";
    $response = $emp->ShowDetailsToDownload();

    $_DataTable = "";
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>Center Code</th>";
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";
	echo "<th style='20%'>Father Name</th>";	
    echo "<th style='20%'>D.O.B</th>";	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	   $_Total = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
        echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";		 
			$fname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($_Row['Admission_Name']))));
        echo "<td>" . $fname . "</td>";
			$faname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($_Row['Admission_Fname']))));
		echo "<td>" . $faname . "</td>";
        echo "<td>" . $_Row['Admission_DOB'] . "</td>";		
		
        echo "</tr>";
		 //$_Total = $_Total + $_Row['trnamount'];
         $_Count++;
    }
		echo "</tbody>";
		echo "</table>";
		echo "</div>";
}

	if ($_action == "DownloadSign")
	{
		$response = $emp->GetLearnerPhoto();
		$_Count=0;		
		$s="";		
		$ns="";
		
		while($_Row = mysqli_fetch_array($response[2])) {			
			$lsigndoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_sign/'.$_Row['Admission_Sign'];
				if (file_exists($lsigndoc)) {					
					$s .= "http://myrkcl.com/upload/admission_sign/".$_Row['Admission_Sign'] . ",";
				}
				else {					
					$ns .=$_Row['Admission_Sign']. ",";
				}			
		}		   
		   $_SignLearnerCode = rtrim($s, ",");		   
		   $_SignNotAvailable = rtrim($ns, ",");		   
		   $_SESSION['LearnerSign'] = $_SignLearnerCode;		   
		   $_SESSION['SignNotAvailable'] = $_SignNotAvailable;
		   echo "yes";
	}
	
	
	if ($_action == "Download")
	{
		$response = $emp->GetLearnerPhoto();
		$_Count=0;
		$l="";		
		$np="";		
		
		while($_Row = mysqli_fetch_array($response[2])) {
			$lphotodoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_photo/'.$_Row['Admission_Photo'];
			
				if (file_exists($lphotodoc)) {
					$l .= "http://myrkcl.com/upload/admission_photo/".$_Row['Admission_Photo'] . ",";					
				}
				else {
					$np .=$_Row['Admission_Photo']. ",";					
				}			
		}
		   $_PhotoLearnerCode = rtrim($l, ",");		   
		   $_PhotoNotAvailable = rtrim($np, ",");		  
		   $_SESSION['LearnerPhoto'] = $_PhotoLearnerCode;		   
		   $_SESSION['PhotoNotAvailable'] = $_PhotoNotAvailable;		  
		   echo "yes";
	}
?>