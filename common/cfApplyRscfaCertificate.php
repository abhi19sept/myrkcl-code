<?php

/*
 * Created by Mayank
  cfApplyRscfaCertificate.php
 */

include './commonFunction.php';
require 'BAL/clsApplyRscfaCertificate.php';

$response = array();
$emp = new clsApplyRscfaCertificate();

if ($_action == "FillCourse") {
    $response = $emp->GetCourse();
    if ($response[0] == "Success") {
        echo "<option value='' selected='selected'>Select Course</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
        }
    } else {
        echo "1";
    }
}

if ($_action == "FillBatch") {
    $response = $emp->FILLBatchName($_POST['values']);
    if ($_POST['values'] == '5') {
        echo "<option value='' selected='selected'>Select Batch </option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
        }
    }
}


if ($_action == "viewlearners") {
    $response = $emp->viewlearners($_POST['course'], $_POST['batch']);
    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";

    echo "<table id='example' border='0' cellpedding='0' cellspacing='0'
			class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='15%'>ITGK-Code</th>";
    echo "<th style='15%'>Learner Code</th>";
    echo "<th style='15%'>Learner Name</th>";
    echo "<th style='15%'>Father Name</th>";
    echo "<th style='15%'>D.O.B</th>";
    echo "<th style='5%'>Learner Mobile</th>";
    echo "<th style='5%'>Admission Date</th>";
    echo "<th style='15%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";
            echo "<td>" . ($_Row['Admission_Mobile']) . "</td>";
            $date = date_create($_Row['Timestamp']);
            echo "<td>" . date_format($date, "d-M-Y") . "</td>";
            if ($_Row['apply_rscfa_cert_lcode'] == '') {
                echo "<td><input type='button' class='updcount btn btn-primary btn-sm' id='" . $_Row['Admission_LearnerCode'] . "' AckCode='" . $_Row['Admission_LearnerCode'] . "' CourseCode='" . $_Row['Admission_Course'] . "' BatchCode='" . $_Row['Admission_Batch'] . "' EmailAdd='" . $_Row['Admission_Email'] . "' mode='ShowUpload' value='Apply for Certificate'/></td>";
            } else {
                echo "<td>Already Applied</td>";
            }
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
}

if ($_action == "ApplyForCertificate") {
    $lcode = $_POST['lcode'];
    $batch = $_POST['bcode'];
    $course = $_POST['ccode'];
    $EmailAdd = $_POST['EmailAdd'];
    if ((isset($lcode) && !empty($lcode)) && (isset($batch) && !empty($batch)) && (isset($course) && !empty($course)) && (isset($EmailAdd) && !empty($EmailAdd))) {
        if (!filter_var($EmailAdd, FILTER_VALIDATE_EMAIL)) {
            echo "Invalid email format";
        } else {
            $result = $emp->Applyforcertificate($lcode, $batch, $course, $EmailAdd);
            echo $result[0];
        }
    } else {
        echo "Field should not empty";
    }
}  


