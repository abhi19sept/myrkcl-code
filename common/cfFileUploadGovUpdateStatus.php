<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

include './commonFunction.php';
require 'BAL/clsFileUploadGovUpdateStatus.php';
require_once 'phpexcel/Classes/PHPExcel.php';

$response = array();
$emp = new clsFileUploadGovUpdateStatus();

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

if(isset($_FILES['SelectedFile']['name'])) {

$ext = pathinfo($_FILES['SelectedFile']['name'], PATHINFO_EXTENSION);
//$ext="xls";
// Check if the file exists
if ($ext == "xlsx") {

$_UploadDirectory = '../upload/govupdatestatus/';
if ($_FILES['SelectedFile']['error'] > 0) {
    outputJSON("<span class='error'>An error ocurred when uploading.</span>");
}


if ($_FILES['SelectedFile']['size'] > 20000000000000000000000000000000) {
    outputJSON("<span class='error'>" . $_FILES['SelectedFile']['size'] . "File uploaded exceeds maximum upload size.</span>");
}

    if (file_exists($_UploadDirectory)) {
        if (is_writable($_UploadDirectory)) {
            if (!copy($_FILES['SelectedFile']['tmp_name'], $_UploadDirectory . $_POST['UploadId']. "." . $ext)) {
                outputJSON("<span class='error'>Error uploading file - check destination is writeable.</span>");
            }
        } else {
            outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
        }
    } else {
        outputJSON("<span class='error'>Upload Folder Not Available</span>");
    }

}

}


if ($_action == "uploadgovdata") {
   // ini_set("display_errors", 1);
    $_UploadDirectory = '../upload/govupdatestatus/';
    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load($_UploadDirectory . $_POST['filename'].  ".xlsx");
    $objWorksheet = $objPHPExcel->getActiveSheet();

    $highestRow = $objWorksheet->getHighestRow();
    $highestColumn = $objWorksheet->getHighestColumn();

    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

    $rows = array();
    //print_r($_POST["status"]);
    if (isset($_POST["filename"])) {
        $_status = $_POST["status"];
		$_fname= $_POST['filename'].  ".xlsx";

        for ($row = 2; $row <= $highestRow; ++$row) {
            for ($col = 0; $col <= $highestColumnIndex; ++$col) {
                $rows[$col] = stripslashes(trim($objWorksheet->getCellByColumnAndRow($col, $row)->getValue()));
            }
            $response = $emp->Addgovstatusdata($rows[0], $rows[1], $rows[2], $_status, $_fname);
        }

        echo $response[0];
    }
}