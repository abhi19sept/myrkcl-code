<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsITGKPassbook.php';

$response = array();
$emp = new clsITGKPassbook();

if ($_action == "SHOW") {
    // print_r($_POST);
    //die;
    if($_POST["rpttype"] == "radio1"){
        if (isset($_POST["startdate"])) {
            if (isset($_POST["enddate"])) {
            $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
            $response = $emp->ShowByDate($sdate, $edate);
            }
        }
    }
    elseif ($_POST["rpttype"] == "radio2") {
        if (isset($_POST["year"])) {
            if (isset($_POST["month"])) {
                $response = $emp->ShowByMonth($_POST["month"], $_POST["year"]);
            }
        }
    }
    elseif ($_POST["rpttype"] == "radio3") {
        $response = $emp->ShowByLastSixM();
    }
    elseif ($_POST["rpttype"] == "radio4") {
       if (isset($_POST["financialyear"])) {
            
                $response = $emp->ShowByFyear($_POST["financialyear"]);
             //print_r($response);
        }
    }
            $response2 = $emp->SHOWBANKDATA();
            $co1 = mysqli_num_rows($response2[2]);
            if ($co1) {
            $row2 =  mysqli_fetch_array($response2[2],MYSQLI_ASSOC);

            // print_r($response);
            //$html = "";
            //$_centerdetail=  mysqli_fetch_array($response[2]);
            //$response = $emp->GetCenterWiseReport($_POST['CenterCode']);
            // $_DataTable = "";
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table  table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo '<th > </th>';
            echo '<th >ITGK Passbook </th>';
            echo '<th >' .$_SESSION['User_LoginId'].'</th>';
            echo '<th ></th>';
            echo '<th ></th>';
            echo '<th > </th>';
            echo '<th ></th>';
            echo '<th ></th>';
            echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
            echo "<tr>";
            echo '<td ></td>';
            echo "<th >Account Holder Name</td>";
            echo "<td >" . strtoupper($row2['Bank_Account_Name']) . "</td>";
             
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
           
            echo "</tr>";
                      echo "<tr>";
            echo '<td ></td>';
            echo "<th >Date</td>";
            echo "<td >" . date("l jS \of F Y") . "</td>";
             
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
           
            echo "</tr>";
     
            echo "<tr>";
             echo '<td ></td>';
            echo "<th >Account Number</td>";
            echo "<td >'" . $row2['Bank_Account_Number'] . "</td>";
             echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo "</tr>";
            echo "<tr>";
             echo '<td ></td>';
            echo "<th >Branch</td>";
            echo "<td >" . strtoupper($row2['Bank_Branch_Name']) . "</td>";
             echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo "</tr>";
            echo "<tr>";
             echo '<td ></td>';
            echo "<th>IFS Code</td>";
             echo "<td >'" . strtoupper($row2['Bank_Ifsc_code']) . "</td>";
              echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo "</tr>";
            echo "<tr>";
             echo '<td ></td>';
            echo "<th >MICR Code</td>";
             echo "<td >'" . strtoupper($row2['Bank_Micr_Code']) . "</td>";
              echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo "</tr>";
            echo "<tr>";
             echo '<td ></td>';
            echo "<th > Start Date</td>";
             echo "<td >" . $response[3] . "</td>";
              echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo "</tr>";
            echo "<tr>";
             echo '<td ></td>';
            echo "<th >End Date</td>";
             echo "<td >" . $response[4] . "</td>";
              echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo '<td >  </td>';
            echo "</tr>";  
            echo "<tr >";
           
            echo "<td> </td>";
            echo "<th> ITGK Code </th>";
            echo "<th> Transaction Date </th>";
            echo "<th> Description</th>";
            echo "<th>Reference/UTR No.</th>";
            echo "<th>Amount </th>";
            echo "<th>Type</th>";
            echo "<th>Remarks</th>";
            echo "</tr>";       
            //  echo "</thead>";
            // echo "<tbody>";    
            $_Count = 1;
            $_Total = 0;
            $co = mysqli_num_rows($response[2]);
            if ($co) {
            while ($row =  mysqli_fetch_array($response[2],MYSQLI_ASSOC)) {

                $Tdate = date("d-m-Y", strtotime($row['paydate']));
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";

                echo "<td>" . $row['Pay_Tran_ITGK'] . "</td>";

                echo "<td>" . $Tdate . "</td>";

                echo "<td>" . $row['Pay_Tran_ProdInfo'] . "</td>";
               // echo "<td>" . $row['Total_Refund_Amount'] . "</td>";

                //echo "<td>" . $row['Admission_Transaction_Amount'] . "</td>";

                echo "<td>" . $row['Pay_Tran_PG_Trnid'] . "</td>";

                echo "<td>" . $row['Pay_Tran_Amount'] . "</td>";


                echo "<td>" . $row['type'] . "</td>";
                echo "<td>" . $row['rmk'] . "</td>";

                echo "</tr>";

                $_Count++;
            }
            echo "</tbody>";
            // echo "<tfoot>";
            // echo "<tr>";
            // echo "<th >  </td>";
            // echo "<th >  </td>";
            // echo "<th >Total Count </td>";
            // echo "<td>";
            // echo "";
            // echo "</td>";
            // echo "<th >  </td>";
            // echo "<th >  </td>";
            // echo "<th >  </td>";            
            // echo "</tr>";
            // echo "</tfoot>";
            echo "</table>";
            echo "</div>";
            } else {
        echo "No Record Found";
    }
            //echo $html;
      }
}

if ($_action == "updfile") {
//    print_r($_POST);
//  print_r($_FILES);
//
//    die;

    $filename = $_POST['txtfilename'];
    $eventname = $_POST['ddlUserType'];
    $roimg = $_FILES['resultfile']['name'];
    $rotmp = $_FILES['resultfile']['tmp_name'];
    $rotemp = explode(".", $roimg);
    //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
    $rofilename = $filename . '_resultfile.' . end($rotemp);
    $rofilestorepath = "../upload/UserCreditAccounts/" . $rofilename;
    //echo $rofilestorepath;
    //die;
    $roimageFileType = pathinfo($rofilestorepath, PATHINFO_EXTENSION);
    if (($_FILES["resultfile"]["size"] < 2000000000000 && $_FILES["resultfile"]["size"] > 100)) {
        if ($roimageFileType == "csv") {
            if (move_uploaded_file($rotmp, $rofilestorepath)) {
                //echo "1";
                $response_return["resultfile"] = array("status" => "1", "filename" => $rofilestorepath, "eventname" => $eventname);
                echo json_encode($response_return);
            } else {
                echo "File not uploaded. Please try again.";
                return FALSE;
            }
        } else {
            echo "Sorry, File Not Valid";
            return FALSE;
        }
    } else {
        echo "File Size should be between 100KB to 2MB.";
        return FALSE;
    }
}

if ($_action == "uploadexamresult") {

    if (isset($_POST["filepath"])) {
        $_filepath = $_POST["filepath"];
        $_usertyperole = $_POST["usertype"];
        $csvData = file_get_contents($_filepath);
        $lines1 = array_filter(explode(PHP_EOL, $csvData));
       
        $lines = array_slice($lines1, 1);
     //    print_r($lines);
        $valuestring = "";
        $j = 0;
         $response1 = $emp->chkpaymentdesc();
         // print_r($response1);
         // die;
          while ($row = mysqli_fetch_array($response1[2],MYSQLI_ASSOC)) {
            $Description[] = $row['Pay_Des_Value'];

          }
        foreach ($lines as $key => $value) {
            # code...
 //$valuestring1 = "'" . implode("', '", $value)  . "'";
                     
            $temp1 = explode(",", $value);
             if (in_array($temp1[1],  $Description)){
                 $resflag = "valid";
             }
             else{
                 $l= $j;
                echo json_encode(['status' => 'fail', 'rows' => 'Invalid Description Entered in Record No:'.($l+1).' , Please Upload Correct CSV.']);
                    return;

                }

            // switch ($temp1[1]) {
            //     case 'ReexamPayment':
            //     case 'Re Exam Event Name':      
            //     case 'LearnerFeePayment':
            //     case 'Learner Fee Payment':                 
            //     case 'RedHatFeePayment':
            //     case 'Red Hat Fee Payment':
            //     case 'Correction Certificate':
            //     case 'Correction Fee Payment':
            //     case 'Duplicate Certificate':
            //     case 'NcrFeePayment':
            //     case 'Ncr Fee Payment':
            //     case 'Name Change Fee Payment':
            //     case 'Address Change Fee Payment':
            //     case 'NameAddressFeePayment':
            //     case 'EOI Fee Payment':
            //     case 'OwnershipFeePayment':
            //     case 'Ownership Fee Payment':                   
            //     case 'PenaltyFeePayment':                   
            //     case 'AadharUpdFeePayment':
            //     case 'ExperienceCenterFeePayment':
            //     $resflag = "valid";
            //     break;

            //     default:
            //     $l= $j;
            //     echo json_encode(['status' => 'fail', 'rows' => 'Invalid Description Entered in Record No:'.($l+1).' , Please Upload Correct CSV.']);
            //         return;

            //     }
            switch ($temp1[2]) {
                case '0':
                case '0.0': 
                case '0.00':      
                
                $m= $j;
                echo json_encode(['status' => 'fail', 'rows' => 'Amount Entered Zero in Record No:'.($m+1).' , Please Upload Correct CSV.']);
                    return;
                break;

                default:
                $resflag = "valid";

                }
                if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$temp1[3])) {
                    $resflag = "valid";
                } else {
                    //$m= $j;
                    //echo json_encode(['status' => 'fail', 'rows' => 'Invalid Date Format in Record No:'.($m+1).' , Please Use YYYY-MM-DD Format in CSV.']);
                    //return;
                    $temp1[3] = str_replace('/', '-', $temp1[3]);
                    $temp1[3] = date("Y-m-d", strtotime($temp1[3]));
                    $resflag = "valid";
                    if($temp1[3] == '1970-01-01'){
                        $m= $j;
                        echo json_encode(['status' => 'fail', 'rows' => 'Invalid Date Format in Record No:'.($m+1).' , Please Use YYYY-MM-DD Format in CSV.']);
                        return;
                    }
                }
            $result = "'" . implode ( "', '", $temp1) . "'";
           //  print_r($temp1);
            $valuestring1 = "" . ($result) . "";
           // $valuestring .= trim("(" . $valuestring1 . ", '" . $_SESSION['User_Code'] . "','" . $_usertyperole . "','1','credit','".uniqid("crd_",FALSE)."')");
             $valuestring .= trim("(" . $valuestring1 . ", '" . $_SESSION['User_Code'] . "','" . $_usertyperole . "','1','Credit')");
            $j++;
            $valuestring .= (count($lines) != $j) ? "," : "";
            
        }
        if($resflag == "valid"){
            
            $response = $emp->Addfinalexamresult($valuestring);
 //print_r($response);
// die;
            if($response['0']== "Successfully Done")
            {
                echo json_encode(['status' => 'success', 'rows' => $response['2']]);
            }
             else{
                echo json_encode(['status' => 'fail', 'rows' => '0']);
                   // return $response;
             }
        }
        else{
            echo json_encode(['status' => 'Invalid Description', 'rows' => '0']);
        }


    }
}

if ($_action == "SHOWgrid") {

 
            $response = $emp->ShowGrid();
//print_r($response);
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<td> S No.</td>";
            echo "<td> ITGK Code </td>";
            echo "<td> Transaction Date </td>";
            echo "<td> Description</td>";
            echo "<td>UTR No.</td>";
            // echo "<td>Debit Amount </td>";
            echo "<td>Credit Amount</td>";
            echo "<td>Remarks</td>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;
            $_Total = 0;
            $_Totalamount = 0;
            $co = mysqli_num_rows($response[2]);
            if ($co) {
            while ($row = mysqli_fetch_array($response[2],MYSQLI_ASSOC)) {

                $Tdate = date("d-m-Y", strtotime($row['User_Credit_Tran_Timestamp']));
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";

                echo "<td>" . $row['User_Credit_Tran_UserLoginId'] . "</td>";


                echo "<td>" . $Tdate . "</td>";

                echo "<td>" . $row['User_Credit_Tran_ProdInfo'] . "</td>";
                // ($row['User_Credit_Tran_ProdInfo'] = $row['User_Credit_Tran_ProdInfo']) ? $_Total++ : ;

                
               // echo "<td>" . $row['Total_Refund_Amount'] . "</td>";

                //echo "<td>" . $row['Admission_Transaction_Amount'] . "</td>";

                echo "<td>" . $row['User_Credit_Tran_PGid'] . "</td>";

                echo "<td>" . $row['User_Credit_Tran_Amount'] . "</td>";
                echo "<td>" . $row['User_Credit_Tran_Remark'] . "</td>";
$_Totalamount = $_Totalamount + $row['User_Credit_Tran_Amount'];
                echo "</tr>";

                $_Count++;
                $_Total = $row['cnt'];
            }
            echo "</tbody>";
            echo "<tfoot>";
            echo "<tr>";
            echo "<th >  </th>";
            echo "<th >Total IT-GK: " .$_Total."</th>";
            echo "<th ></th>";
            
            echo "<th >  </td>";
            echo "<th >  </td>";
            echo "<th >Total Amount: " .$_Totalamount."  </td>"; 
             echo "<th >  </td>";           
            echo "</tr>";
            echo "</tfoot>";
            echo "</table>";
            echo "</div>";
            } else {
        echo "No Record Found";
    }
            //echo $html;
  
}
if ($_action == "transferexamresult"){
  if (isset($_POST["uploadfor"])) {

     $response1 = $emp->chkduplicateutr($_POST["uploadfor"]);
      $response2 = $emp->chkduplicateutrfinal($_POST["uploadfor"]);
     // print_r($response1);
     // die;
     if($response1[2] == '0'){
        
// print_r($response);
// die;
         if($response2[2] == '0'){

            $response = $emp->transferexamresult($_POST["uploadfor"]);

            if($response['0']== "Successfully Done")
            {
            echo json_encode(['status' => 'success', 'rows' => $response['2']]);
            }
            else{
            echo json_encode(['status' => 'fail', 'rows' => '0']);
               // return $response;
            }
        }
         else{
        echo json_encode(['status' => 'fail', 'rows' => 'Some UTR No Already Uploaded, Please check data']);
        }
     }
     else{
        echo json_encode(['status' => 'fail', 'rows' => $response1[2].' Duplicate UTR No Found in CSV, Please check data']);
     }

}
}

if ($_action == "SHOWrpt") {
        if (isset($_POST["startdate"])) {
            if (isset($_POST["enddate"])) {
            $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
            if(!(empty($_POST['txtCenter'])))
                $response = $emp->SHOWrptCenter($_POST['txtCenter'],$sdate, $edate);
            else
             $response = $emp->SHOWrpt($sdate, $edate);
//print_r($response);
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<td> S No.</td>";
            echo "<td> ITGK Code </td>";
            echo "<td> Transaction Date </td>";
            echo "<td> Description</td>";
            echo "<td>UTR No.</td>";
            // echo "<td>Debit Amount </td>";
            echo "<td>Credit Amount</td>";
            echo "<td>Remarks</td>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;

            $co = mysqli_num_rows($response[2]);
            if ($co) {
            while ($row = mysqli_fetch_array($response[2],MYSQLI_ASSOC)) {

                $Tdate = date("d-m-Y", strtotime($row['User_Credit_Tran_Timestamp']));
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";

                echo "<td>" . $row['User_Credit_Tran_UserLoginId'] . "</td>";


                echo "<td>" . $Tdate . "</td>";

                echo "<td>" . $row['User_Credit_Tran_ProdInfo'] . "</td>";

                echo "<td>" . $row['User_Credit_Tran_PGid'] . "</td>";

                echo "<td>" . $row['User_Credit_Tran_Amount'] . "</td>";
                echo "<td>" . $row['User_Credit_Tran_Remark'] . "</td>";

                echo "</tr>";

                $_Count++;

            }
            echo "</tbody>";

            echo "</table>";
            echo "</div>";
            } else {
        echo "No Record Found";
    }
            }
        }
 
         
}
            //echo $html;
    if ($_action == "fillDescp") 

        {
    $response = $emp->chkpaymentdesc();

    while ($row = mysqli_fetch_array($response[2],MYSQLI_ASSOC)) {
        echo "<option value=" . $row['Pay_Des_Id'] . ">" . $row['Pay_Des_Value'] . "  </option>";
        
    
        
        }
    
    }
    if ($_action == "saveDescp") {
    if (isset($_POST["txtPayDes"]) && !empty($_POST["txtPayDes"])) {
       
        $_txtPayDes=trim($_POST["txtPayDes"]);

        $response = $emp->AddPayDes($_txtPayDes);
        echo $response[0];
    }
}