<?php

/*
 * @author Abhi

 */
include 'commonFunction.php';
require 'BAL/clsGenerateLearnerReceipt.php';

$response = array();
$emp = new clsGenerateLearnerReceipt();
if ($_action == "SHOWALL") {
    $response = $emp->GetDatabyCode($_POST['lcode']);
    $_DataTable = "";

    $co = mysqli_num_rows($response[2]);
    $res = '';
    if ($co) {

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th>Father/Husband Name</th>";
    echo "<th>D.O.B</th>";

    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        echo "<td>" . $_Row['Admission_DOB'] . "</td>";

        echo "<td><button type='button' class='updcount' value='" . $_Row['Admission_Code'] . "'>Download Receipt</button></td>";

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
    }
    else {
        echo "No Record Found.";
    }
}

if ($_action == "DwnldPrintRecp") {
    // print_r($_POST);
    // die;
    $path = '../upload/DownloadPrintRecpt/';
    $learnerCode = $_POST['lid'];
    $file = $_POST['lid'] . '.fdf';
    $filePath = $path . $file;



    $response = $emp->GetAllPrintRecpDwnld($_POST['lid']);
    $co = mysqli_num_rows($response[2]);
    $res = '';
    // echo $co;
    // die;
    if ($co) {
        $_Row1 = mysqli_fetch_array($response[2], true);
  
        $allreadyfoundAkcRecipt = $path . $learnerCode . '_Admission_Invoice.pdf';
        
        //echo "<pre>";print_r($_Row1);die;
        //$_DataTable = array();
        //$_Row1["todate"] = date("d-m-Y");
        $_Row1["invoice_no"] = "ADM/" . $_Row1['invoice_no'];
        $_Row1["invoice_date"] = date("d-m-Y", strtotime($_Row1['addtime']));
        $_Row1["Lname"] = strtoupper($_Row1['Admission_Name']);
        $_Row1['Fname'] = strtoupper($_Row1['Admission_Fname']);
        $_Row1["Lcode"] = $_Row1['Admission_LearnerCode'];
        $_Row1["Laddress"] = strtoupper($_Row1['District_Name']);
        $_Row1["Ldob"] = date("d-m-Y", strtotime($_Row1['Admission_DOB']));
        $_Row1["Lmobile"] = $_Row1['Admission_Mobile'];
        $_Row1["Lcourse"] = $_Row1['Course_Name'] . " Course";


        if ($_Row1['Admission_Course'] == '1' || $_Row1['Admission_Course'] == '4') {
            $_Row1["customtext1"] = "Total";
            $_Row1["customtext2"] = "Less - Tax Component borne by RKCL";
            $_Row1["customtext3"] = "Enrollment, Examination and Certification fee ***";
            $_Row1["customtext4"] = "RKCL ** and VMOU*** to confirm the admission.";
//            $_Row1["customtext5"] = "certificate, you should visit https://myrkcl.com and file your claim online for reimbursement by yourself or through your ITGK. No physical documents will be entertained by RKCL in this regard.  ";
            $_Row1["customtext6"] = "/ 3 months";
//            $_Row1["customtext7"] = "If you are a ";
//            $_Row1["customtext8"] = "Govt. Employee and eligible for fee reimbursement ";
//            $_Row1["customtext9"] = "then after getting your VMOU ";
            $_Row1["customtext5"] = "सरकारी कर्मचारियों हेतु शुल्क पुनर्भरण  मय प्रोत्साहन राशी (वर्तमान एवं लंबित) का भुगतान नियमानुसार राज्य कर्मी के पैतृक विभाग (मुख्यालय संबंधित विभागाध्यक्ष) के स्तर से किया जायेगा | अतः सरकारी कर्मचारी आवेदन करने एवं शुल्क पुनर्भरण हेतु अपने से सम्बंधित विभाग में ही संपर्क करें | ";
            $_Row1["customtext10"] = "Internal assessment, learning, facilitation and Course Material **";
            $_Row1["customcourse"] = "RS-CIT";
            $_Row1["coursedec"] = $_Row1['Course_Name'] . " (Digital Literacy Course)";
            $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
            $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
            $_Row1["TuitionFee"] = $_Row1['Gst_Invoce_TutionFee'] . ".00";
            $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
            $_Row1["Enrollmentfee"] = $_Row1['Gst_Invoce_VMOU'] . '.00';
            $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'] . '.00';
            $_Row1["Rkclshare2"] = $_Row1['Gst_Invoce_BaseFee'] . '.00';
            $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'] . '.00';
            $_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
            //$_Row1["totalrkclplus"] = ($_Row1["Gst_Invoce_BaseFee"] + $_Row1["cgst"] + $_Row1["cgst"]) . "0";
            $_Row1["totalrkclplus"] = ($_Row1["Gst_Invoce_BaseFee"] + $_Row1["cgst"] + $_Row1["cgst"]) . "0";
            //$_Row1["totaltaxless"] = ($_Row1["cgst"] + $_Row1["cgst"]) . "0";
            $_Row1["totaltaxless"] = ($_Row1["cgst"] + $_Row1["cgst"]) . "0";
            $_Row1["totalrkcl"] = ($_Row1["totalrkclplus"] - $_Row1["totaltaxless"]) . ".00";
            $_Row1["amounttxt"] = convert_number_to_words($_Row1['Gst_Invoce_BaseFee']) . " Only";
            $_Row1["CourseDuration"] = $_Row1['Course_Duration'] . $_Row1['Course_DurationType'];
            $_Row1["Lbatch"] = $_Row1['Batch_Name'];
            $_Row1["Litgk"] = $_Row1['Admission_ITGK_Code'];
            $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
            $_Row1["cgstvalue"] = "9";
            $_Row1["sgstvalue"] = "9";
        }


        $fdfContent = '%FDF-1.2
			%Ã¢Ã£Ã?Ã“
			1 0 obj 
			<<
			/FDF 
			<<
			/Fields [';
        foreach ($_Row1 as $key => $val) {
            $fdfContent .= '
					<<
					/V (' . $val . ')
					/T (' . $key . ')
					>> 
					';
        }
        $fdfContent .= ']
			>>
			>>
			endobj 
			trailer
			<<
			/Root 1 0 R
			>>
			%%EOF';
        file_put_contents($filePath, $fdfContent);

        $fdfFilePath = $filePath;
        $fdfPath = str_replace('/', '//', $fdfFilePath);
        $resultFile = '';

        $defaulPermissionLetterFilePath = '../upload/DownloadPrintRecpt/_Admission_Invoice.pdf'; //die;


        if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
            $defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);

            $resultFile = $learnerCode . '_Admission_Invoice.pdf';
            



            $newURL = $path . $resultFile;

            //$resultPath = str_replace('/', '//', getPermitionLetterFilePath($learnerCode));
            $resultPath = str_replace('/', '//', $newURL);
            $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            // $pdftkPath = ($_SERVER['HTTP_HOST'] == "click.rkcl.in") ? '"C://inetpub//vhosts//rkcl.in//click.rkcl.in//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            //$pdftkPath = ($_SERVER['HTTP_HOST']=="10.1.1.20") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';

            $command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
            exec($command);

            if ($_Row1['Admission_Course'] == '1' || $_Row1['Admission_Course'] == '4') {
                $emp->addPhotoInPDF($newURL, $_Row1['Admission_Course']);
                $emp->addNotePhotoInPDF($newURL, $_Row1['Admission_Course']);
            }
           
            unlink($fdfFilePath);
            $filepath5 = 'upload/DownloadPrintRecpt/' . $resultFile;
            echo $filepath5;


            $res = "DONE";
        }
    } else {
        echo 0;
    }

    function getPermissionLetterPath() {
        $path = '../upload/DownloadPrintRecpt/';
        makeDir($path);

        return $path;
    }

    function getPermitionLetterFilePath($code) {
        $resultFile = $code . '_Admission_Invoice.pdf';
        // $resultPath = getPermissionLetterPath() . $resultFile;
        $resultPath = '../upload/DownloadPrintRecpt/' . $resultFile;

        return $resultPath;
    }

    function makeDir($path) {
        return is_dir($path) || mkdir($path);
    }
}
    function convert_number_to_words($number) {

        $hyphen = ' ';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'Zero',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen',
            20 => 'Twenty',
            30 => 'Thirty',
            40 => 'Fourty',
            50 => 'Fifty',
            60 => 'Sixty',
            70 => 'Seventy',
            80 => 'Eighty',
            90 => 'Ninety',
            100 => 'Hundred',
            1000 => 'Thousand',
            1000000 => 'Million',
            1000000000 => 'Billion',
            1000000000000 => 'Trillion',
            1000000000000000 => 'Quadrillion',
            1000000000000000000 => 'Quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }



