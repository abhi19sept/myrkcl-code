<?php

/*
 * Created by SUNIL KUAMR BAINDARA 1-9-2019

 */

include './commonFunction.php';
require 'BAL/clsUpdateMobileNumber.php';

$response = array();
$emp = new clsUpdateMobileNumber();



if ($_action == "DETAILS") {
    $response = $emp->GetDatabyCode($_POST['values']);
    //echo "<pre>"; print_r($response);die;
    $_DataTable = array();
    $_i = 0;
    //$co = mysqlii_num_rows($response[2]);
    if ($response[0]=='Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                "orgcode" => $_Row['Organization_Code'],
                "address" => $_Row['Organization_HouseNo']." ".$_Row['Organization_Street']." ".$_Row['Organization_Road']." ".$_Row['Organization_Landmark']." ".$_Row['Org_formatted_address'],
               
                "districtname" => $_Row['District_Name'],
                "ITGK_MobileNo" => $_Row['User_MobileNo'],
                "tehsilname" => $_Row['Tehsil_Name'],
                "User_EmailId" => $_Row['User_EmailId']
                    );
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "NORECD";
    }
}

if ($_action == "UPDATEMOBILE") {
    //print_r($_POST);
    if (isset($_POST["ITGK_Code"]) && !empty($_POST["ITGK_Code"])) 
        {
        if (isset($_POST["User_MobileNo"]) && !empty($_POST["User_MobileNo"])) {


            
            $_ITGK_Code = $_POST["ITGK_Code"];
            $_User_MobileNo = $_POST["User_MobileNo"];
        if(strlen($_POST["User_MobileNo"]) < 10)
            {
                echo "Please enter at least 10 characters.";
            }
        else{
            $response = $emp->UpdateMobile($_ITGK_Code, $_User_MobileNo);
            echo $response[0];
            }
        } else {
            echo "Please Enter MobileNo";
        }
    } else {
        echo "Please Enter ITGK Code";
    }
}
