<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsLearnerCorrectionFee.php';

$response = array();
$emp = new clsLearnerCorrectionFee();


if ($_action == "ADD")
	{        
		$amount = $_POST["amounts"];
		$_UserPermissionArray=array();
		$_CorrectionId = array();
		$_i = 0;
        $_Count=0;
		$l="";
			foreach ($_POST as $key => $value)
			{
               if(substr($key, 0,3)=='chk')
               {
                   $_UserPermissionArray[$_Count]=array(
                       "Function" => substr($key, 3),
                       "Attribute"=>3,
                       "Status"=>1);
                   $_Count++;				   
					$l .= substr($key, 3) . ",";				   
               }
				$_CorrectionId = rtrim($l, ",");				
				$count = count($_UserPermissionArray);
				$amount1 = $count*$amount;					
			}
			     if($amount1 == '0') {
					 echo "Invalid";
				 }
				 else {					
						$response = $emp->AddLearnerCorrectionPayTran($_POST['txtapplicationfor'], $amount1, $_CorrectionId, $_POST['txtGenerateId'], $count, $_POST['txtGeneratePayUId']);
						echo $amount1; 
				 }				 	   
	}
	
if ($_action == "SHOW") {
	
    $response = $emp->GetAll($_POST['application'], $_POST['correctioncode'], $_POST['paymode']);

    $_DataTable = "";
    $html .= "<div class='table-responsive'>";
    $html .= "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    $html .=$html .= "<thead>";
    $html .= "<tr>";
    $html .= "<th style='5%'>S No.</th>";
	$html .= "<th style='20%'>Correction Id</th>";
    $html .= "<th style='20%'>Learner Code</th>";
    $html .= "<th style='20%'>Name</th>";
    $html .= "<th style='20%'>Father Name</th>";
    $html .= "<th style='20%'>Marks</th>";
    $html .= "<th style='5%'>Email</th>";
    $html .= "<th style='5%'>Mobile</th>";
    $html .= "<th style='5%'>Action</th>";
    $html .= "</tr>";
    $html .= "</thead>";
    $html .= "<tbody>";
    $_Count = 1;
	
	if($response[0]=='Success'){
    while ($_Row = mysqli_fetch_array($response[2])) {
        $html .= "<tr class='odd gradeX'>";
        $html .=  "<tr class='odd gradeX'>";
        $html .=  "<td>" . $_Count . "</td>";
        $html .=  "<td>" . $_Row['cid'] . "</td>";
        $html .=  "<td>" . $_Row['lcode'] . "</td>";
        $html .=  "<td>" . $_Row['cfname'] . "</td>";
        $html .=  "<td>" . $_Row['cfaname'] . "</td>";
        $html .=  "<td>" . $_Row['totalmarks'] . "</td>";
        $html .=  "<td>" . $_Row['emailid'] . "</td>";
        $html .=  "<td>" . $_Row['mobile'] . "</td>";


  //       echo "<tr class='odd gradeX'>";
  //       echo "<td>" . $_Count . "</td>";
		// echo "<td>" . $_Row['cid'] . "</td>";
  //       echo "<td>" . $_Row['lcode'] . "</td>";
  //       echo "<td>" . $_Row['cfname'] . "</td>";
  //       echo "<td>" . $_Row['cfaname'] . "</td>";
  //       echo "<td>" . $_Row['totalmarks'] . "</td>";
  //       echo "<td>" . $_Row['emailid'] . "</td>";
  //       echo "<td>" . $_Row['mobile'] . "</td>";
		
		if($_Row['Correction_Payment_Status'] == '0')
		{
			$html .=  "<td><input type='checkbox' class='chk' id='chk" .  $_Row['cid'].
			"' name='codes[]' value = '"  . $_Row['cid'] . "'></input></td>";
            // echo "<td><input type='checkbox' class='chk' id='chk" .  $_Row['cid'].
            // "' name='codes[]' value = '"  . $_Row['cid'] . "'></input></td>";
		}	

        $html .= "</tr>";
        $_Count++;
    }
   }
	else{
		//echo "invaliddata";
	}
    $html .= "</tbody>";
    $html .= "</table>";
	$html .= "</div>";
    echo json_encode($html);
}
if ($_action == "FILL") {
    $response = $emp->GetCorrectionName();
    echo "<option value='' selected='selected'>Select Name</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['docid'] . ">" . $_Row['applicationfor'] . "</option>";
    }
}

if ($_action == "Fee") {   
    $response = $emp->GetCorrectionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);    
    echo $_row['RKCL_Share'];
}

if ($_action == "GetApplicationName") {  
    $response = $emp->GetApplicationName($_POST['for']);
    $_row = mysqli_fetch_array($response[2]);  
    echo $_row['applicationfor'];
}


if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
	//print_r($response);
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
	//values
	$key = "X2ZPKM";
	$salt = "8IaBELXB";
	$command1 = "check_isDomestic";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	//$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
	//$var3 = "500";//  Amount to be used in case of refund

	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);
	//echo "<pre>"; print_r($response); echo "</pre>"; 
	$_DataTable = array();
    $_i = 0;
		$_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
				echo json_encode($_DataTable);
		//echo $cardType = $response['cardType'];
		//echo $cardCategory = $response['cardCategory'];
 }

 if ($_action == "FinalValidateCard") {
	//values
	$key = "X2ZPKM";
	$salt = "8IaBELXB";
	$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	//$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
	//$var3 = "500";//  Amount to be used in case of refund

	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);
	//echo "<pre>"; print_r($response); echo "</pre>"; 
	if($response == 'Invalid'){
		echo "1";
	}
	else if($response == 'valid'){
		echo "2";
	}
 }
 
function curlCall($wsUrl, $qs, $true){
   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
		if (curl_errno($c)) {
		  $c_error = curl_error($c);
		  if (empty($c_error)) {
			  $c_error = 'Some server error';
			}
			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);
		return $arr;
}

if ($_action == "ShowCenterDetails") {
    $response = $emp->GetDatabyCode();
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("OwnerName" => $_Row['UserProfile_FirstName'],
            "OwnerMobile" => $_Row['UserProfile_Mobile'],
			"OwnerEmail" => $_Row['UserProfile_Email']);

        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}

?>
