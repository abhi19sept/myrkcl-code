<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsAdmissionSummaryCount.php';

$response = array();
$emp = new clsAdmissionSummaryCount();

if ($_action == "GETDATA") {

    $response = $emp->GetDataAll($_POST['course'], $_POST['batch']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    //echo "<th>CenterCode</th>";    
    echo "<th>Total Uploaded Learner Count</th>";
    echo "<th>Total Confirmed Learner Count</th>";
		if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 ||
			$_SESSION['User_UserRoll'] == 4) {
				echo "<th>Total Pass Learner Count</th>";
			}
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;
   
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['Admission_Code'] . "</td>";
			echo "<td>" . $_Row['Confirmed_Learner'] . "</td>";
			if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 9 ||
				$_SESSION['User_UserRoll'] == 4) {
					echo "<td>" . $_Row['pass_count'] . "</td>";
			}
			$_Count++;
        }

        echo "</tbody>";        
        echo "</table>";
        echo "</div>";
   
}

