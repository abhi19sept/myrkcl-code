<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsOwnershipChangeFinalApproval.php';

$response = array();
$emp = new clsOwnershipChangeFinalApproval();


if ($_action == "ShowDetails") {
    $response = $emp->GetAll();
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>New IT-GK Name</th>";
    echo "<th>Ownership Change Ack No</th>";
    echo "<th>IT-GK Code</th>";
    
    echo "<th>IFSC Code</th>";
    echo "<th>Account Holder Name</th>";
    echo "<th>Account Number</th>";
    echo "<th>Bank Name</th>";
    echo "<th>Branch Name</th>";
    echo "<th>MICR Code</th>";
    echo "<th>Account Type</th>";
    echo "<th>Application Date</th>";
    echo "<th>SMS Sent to ITGK</th>";
    echo "<th>Agreement Status</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Ack']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Itgk_Code']) . "</td>";
        
        echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Number']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Branch_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Micr_Code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Type']) . "</td>";
        
        echo "<td>" . date('d-m-Y', strtotime($_Row['Org_App_Date'])) . "</td>";
         echo "<td>" . ($_Row['Org_Final_Message'] ? strtoupper($_Row['Org_Final_Message']): 'NA') . "</td>";
         echo "<td>" . strtoupper($_Row['Org_Agreement_Status']) . "</td>";
//         echo "<td> <a href='frmshowncrdetails.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit&cc=" . $_Row['User_LoginId'] ."&ack=" . $_Row['Org_Ack'] ."'>"
//            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='View Details'/></a>"
//            . "</td>";
        if ($_Row['Org_Final_Approval_Status'] == 'Pending') {
        if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 9) {
            echo "<td> <a href='frmownershipchangefinalprocess.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit&cc=" . $_Row['Org_Itgk_Code'] ."&ack=" . $_Row['Org_Ack'] ."'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-danger' value='Approve'/></a>"
            . "</td>";
        } else if ($_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8') {
            echo "<td>"
            . "Pending for Ownership Change Final Approval"
            . "</td>";
        } else {
            echo "<td>"
            . "Rejected"
            . "</td>";
        }
        } else if ($_Row['Org_Final_Approval_Status'] == 'Approved') {
            echo "<td style='color:green'>"
            . "Approved"
            . "</td>";
        } else if ($_Row['Org_Final_Approval_Status'] == 'OnHold') {
            echo "<td style='color:red'>"
            . "OnHold"
            . "</td>";
        } else {
            echo "<td>"
            . "Rejected"
            . "</td>";
        }
        

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "PROCESS") {
    header('Content-Type', 'pdf/*');
//    print($_POST['values']);
//    die;
    global $_ObjFTPConnection;
    $response = $emp->GetOrgDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $details= $_ObjFTPConnection->ftpdetails();
            $_role = 'IT-GK';
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "orgcourse" => $_Row['Org_Application_Type'],
            "doctype" => $_Row['Organization_DocType'],
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            "orgdoc" => $_Row['Organization_ScanDoc'],
            "orguid" => $_Row['Organization_UID'],
            "orgaddproof" => $_Row['Organization_AddProof'],
            "orgappform" => $_Row['Organization_AppForm'],
            "agreement" => $_Row['SPCenter_Agreement'],
            "orgtypedoc1" => $_Row['Organization_TypeDocId'],);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "SendSMS") {
    
    $_SMS = $_POST["sms"];
    $_CenterCode = $_POST["centercode"];
    $_MobileNew = $_POST["mobile"];
    $response = $emp->SendSMS($_SMS, $_CenterCode, $_MobileNew);
    echo $response[0];
    
}


if ($_action == "GenerateOTP") {
    //print_r($_POST);
    if (isset($_POST["Tech"]) || isset($_POST["Acc"]) || isset($_POST["Markt"])) {
        $_Technical = $_POST["Tech"];
        $_Accounts = $_POST["Acc"];
        $_Marketing = $_POST["Markt"];
        $_CenterCode = $_POST["centercode"];
                
        $response = $emp->GenerateOTP($_Technical,$_Accounts,$_Marketing,$_CenterCode);
        echo $response[0];
    }
    else {
        echo "Please select required details";
    }
}

if ($_action == "VERIFY") {
    //print_r($_POST);
    if (isset($_POST["otptech"]) || isset($_POST["otpacc"]) || isset($_POST["otpmrtk"])) {
        $_OTPETech = $_POST["otptech"];
        $_OTPAcc = $_POST["otpacc"];
        $_OTPMrkt = $_POST["otpmrtk"];

        $response = $emp->Verify($_OTPETech, $_OTPAcc, $_OTPMrkt);
        echo $response[0];
    }
    else {
        echo "Please Enter OTP received on Mobile Number to Proceed";
    }
}

//if ($_action == "ChangeOwner") {
//    print_r($_POST);
//    die;
//    if (isset($_POST["Code"])) {    
//    $_Code = $_POST["Code"];
//    $response = $emp->CourseAllocation($_Code);
//    echo $response[0];
//    }else {
//        echo "Something went wrong. Please try again.";
//    }
//}
if ($_action == "UpdateUserMaster") {
//    print_r($_POST);
//    die;
    if (isset($_POST["Code"]) && !empty($_POST["Code"])) { 
    $_Code = $_POST["Code"];
    $response = $emp->UpdateUserMaster($_Code);
    echo $response[0];
    }else {
        echo "Something went wrong. Please try again.";
    }
    
}
if ($_action == "UpdateOrgDetail") {
    if (isset($_POST["Code"]) && !empty($_POST["Code"])) { 
    $_Code = $_POST["Code"];
    $response = $emp->UpdateOrgDetail($_Code);
    echo $response[0];
    }else {
        echo "Something went wrong. Please try again.";
    }
    
}
if ($_action == "UpdateBankAcc") {
    if (isset($_POST["Code"]) && !empty($_POST["Code"])) { 
    $_Code = $_POST["Code"];
    $response = $emp->UpdateBankAcc($_Code);
    echo $response[0];
    }else {
        echo "Something went wrong. Please try again.";
    }
    
}
if ($_action == "UpdateAgreement") {
    if (isset($_POST["Code"]) && !empty($_POST["Code"])) { 
    $_Code = $_POST["Code"];
    $response = $emp->UpdateAgreement($_Code);
    echo $response[0];
    }else {
        echo "Something went wrong. Please try again.";
    }
    
}

//if ($_action == "PAYMENTDETAIL") {
//
//    //echo "Show";
//    $response = $emp->SHOWPAYENTDETAILS($_POST["centercode"]);
//
//    $_DataTable = "";
//
//   echo "<div class='table-responsive' style='margin-top:10px'>";
//    echo "<table id='examplepay' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
//    echo "<thead>";
//    echo "<tr>";
//    echo "<th style='5%'>S No.</th>";
//    echo "<th style='45%'>IT-GK Code</th>";
//    echo "<th style='40%'>IT-GK Name</th>";
//    echo "<th style='10%'>NCR Amount</th>";
//	echo "<th style='10%'>Payment Status</th>";
//	echo "<th style='10%'>Product Info</th>";
//	echo "<th style='10%'>Date and Time</th>";
//    echo "</tr>";
//    echo "</thead>";
//    echo "<tbody>";
//    $_Count = 1;
//    while ($_Row = mysqli_fetch_array($response[2])) {
//        echo "<tr class='odd gradeX'>";
//        echo "<td>" . $_Count . "</td>";
//        echo "<td>" . $_Row['Pay_Tran_ITGK'] . "</td>";
//         echo "<td>" . strtoupper($_Row['Pay_Tran_Fname']) . "</td>";
//		 echo "<td>" . strtoupper($_Row['Pay_Tran_Amount']) . "</td>";
//		 echo "<td>" . strtoupper($_Row['Pay_Tran_Status']) . "</td>";
//		 echo "<td>" . $_Row['Pay_Tran_ProdInfo'] . "</td>";
//		 echo "<td>" . $_Row['timestamp'] . "</td>";
//		 
//        echo "</tr>";
//        $_Count++;
//    }
//   echo "</tbody>";
//    echo "</table>";
//	echo "</div>";
//}

//if ($_action == "NCRVISIT") {
//    $response = $emp->GetNcrPhotoData($_POST['values']);
//    $_DataTable = array();
//    $_i = 0;
//    while ($_Row = mysqli_fetch_array($response[2])) {
//        $_DataTable[$_i] = array("NCRTP" => $_Row['NCRVisit_TheoryRoom'],
//            "NCRRP" => $_Row['NCRVisit_Reception'],
//            "NCREP" => $_Row['NCRVisit_Exterior'],
//            "NCROP" => $_Row['NCRVisit_Other'],);
//        $_i = $_i + 1;
//    }
//
//    echo json_encode($_DataTable);
//}
//
//if ($_action == "SPCenterAgreement") {
//    $response = $emp->GetSPCenteragreement($_POST['values']);
//    $_DataTable = array();
//    $_i = 0;
//    while ($_Row = mysqli_fetch_array($response[2])) {
//        $_DataTable[$_i] = array("SPCEN" => $_Row['SPCenter_Agreement'],);
//        $_i = $_i + 1;
//    }
//
//    echo json_encode($_DataTable);
//}

//if ($_action == "GETREGRSP") {
//    $response = $emp->GetRspDetails($_POST["centercode"]);
//    $_DataTable = array();
//    $_i = 0;
//    while ($_Row = mysqli_fetch_array($response[2])) {
//
//        $_DataTable[$_i] = array("rspname" => $_Row['Rspitgk_Rspname'],
//            "mobno" => $_Row['Rspitgk_Rsproad'],
//            "date" => $_Row['Rspitgk_Rspestdate'],
//            "rsptype" => $_Row['Rspitgk_Rsporgtype']);
//        $_i = $_i + 1;
//    }
//
//    echo json_encode($_DataTable);
//}