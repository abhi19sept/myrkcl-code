<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsPayRenewalPenalty.php';

$response = array();
$emp = new clsPayRenewalPenalty();

if ($_action == "GETPENALTYDETAILS") {

    $response = $emp->GetITGK_Penalty_Details();
    
    if(!mysqli_num_rows($response[2])) die;

    echo $response[0];
}

if ($_action == "GETDETAILS") {
    $response = $emp->GetITGK_SLA_Details();
    if(!mysqli_num_rows($response[2])) die;

    $response2 = $emp->GetRenewalPenaltyAmount();
    $_Row2 = mysqli_fetch_array($response2[2]);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
    
        date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d h:i:s");
            $datetime1 = new DateTime($_Date);
            $datetime2 = new DateTime($_Row['CourseITGK_ExpireDate']);
            $interval = $datetime1->diff($datetime2);
            
            //$_StartDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($_Row['CourseITGK_ExpireDate'])) . " - 1 year"));
        $_StartDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($_Row['CourseITGK_ExpireDate'])) . " -13 months"));
        
    $response1 = $emp->GetITGK_Admission_Details($_Row['ITGKCODE'], $_StartDate, $_Row['CourseITGK_ExpireDate']);
    $_Row1 = mysqli_fetch_array($response1[2]);
    
//            if($_Row['Ncr_Transaction_Amount'] == '' || $_Row['Ncr_Transaction_Amount'] == '51000' || $_Row['Ncr_Transaction_Amount'] == '38000')
//            {
//            $_AdmissionRequired = '50';
//            } if ($_Row['Ncr_Transaction_Amount'] == '24000') {
//             $_AdmissionRequired = '25';    
//            }
    if($_Row['CourseITGK_ExpireDate'] > '2019-08-01'){
        $_AdmissionRequired = '25';
        $_Shortfall = $_AdmissionRequired - $_Row1['Admission_Count'];
        
    } else if ($_Row['CourseITGK_ExpireDate'] < '2019-08-01'){
    
    
    if($_Row['Organization_AreaType'] == 'Urban'){
          $_AdmissionRequired = '50';
          $_Shortfall = $_AdmissionRequired - $_Row1['Admission_Count'];
        } 
        elseif ($_Row['Organization_AreaType'] == 'Rural') {
          
        $panchayat = trim(str_replace(' ', '', $_Row['Block_Name']));
        $panchayat = preg_replace('/\s+/', ' ',$panchayat);
        $panchayat = str_replace('.', '', $panchayat);

        $gram = trim(str_replace(' ', '', $_Row['GP_Name']));
        $gram = preg_replace('/\s+/', ' ',$gram);
        $gram = str_replace('.', '', $gram);

          if($panchayat === $gram){
          $_AdmissionRequired = '50';
          $_Shortfall = $_AdmissionRequired - $_Row1['Admission_Count'];
          }else{
          $_AdmissionRequired = '25';
          $_Shortfall = $_AdmissionRequired - $_Row1['Admission_Count'];
          }
            } else{
          $_Shortfall = "Area Type Not Available";
            }
    }    
         $_Penalty  = $_Shortfall * $_Row2['Renewal_Penalty_Amount'];
       
    
        $_DataTable[$_i] = array("cc" => $_Row['ITGKCODE'],
            "itgkname" => $_Row['ITGK_Name'],
            "email" => $_Row['ITGKEMAIL'],
            "mobile" => $_Row['ITGKMOBILE'],
            "address" => $_Row['Organization_Address'],
            "spname" => $_Row['RSP_Name'],
            "panchayat" => ($_Row['Block_Name'] ? $_Row['Block_Name'] : 'Area Type: Urban/NA'),
            "gram" => ($_Row['GP_Name'] ? $_Row['GP_Name'] : 'Area Type: Urban/NA'),
            "creationdate" => $_Row['CourseITGK_UserCreatedDate'],
            "lastrenewaldate" => $_StartDate,
            "expiredate" => $_Row['CourseITGK_ExpireDate'],
            "rep" => $interval->format('%y years %m months and %d days'),
			"days" => ($interval->d),
            "re" => ($interval->m),
            "year" => ($interval->y),
            "creationamount" => ($_Row['Organization_AreaType'] ? $_Row['Organization_AreaType'] : 'Location details not updated by center'),
            "arr" => $_AdmissionRequired,
            "naly" => $_Row1['Admission_Count'],
            "shortfall" => $_Shortfall,
            "pps" => $_Row2['Renewal_Penalty_Amount'],
            "tp" => $_Penalty);
        $_i = $_i + 1;
    }
    
    $response = $emp->GetITGK_SLA_Details();
    $_Row = mysqli_fetch_array($response[2]);
    
    if($_Penalty > 0 && ($interval->m) <11){
     $response5 = $emp->Renewal_Payment($_Row['ITGK_Name'], $_Row['Block_Name'], $_Row['GP_Name'], $_Row['CourseITGK_UserCreatedDate'], $_StartDate, $_Row['CourseITGK_ExpireDate'], $_Row['Organization_AreaType'], $_AdmissionRequired, $_Row1['Admission_Count'], $_Shortfall, $_Penalty);   
    }

    echo json_encode($_DataTable);
}

if ($_action == "ADD") {
    if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
        if (isset($_SESSION['User_Code']) && !empty($_SESSION['User_Code'])) {
            if (isset($_SESSION['User_Rsp']) && !empty($_SESSION['User_Rsp']) && $_SESSION['User_Rsp'] != '0') {
                $productinfo = 'PenaltyFeePayment';
                $trnxId = $emp->AddPayTran($productinfo);
                if ($_POST['gateway'] == 'razorpay' && !empty($trnxId) && $trnxId != 'TimeCapErr') {
                    require("razorpay.php");
                } else {
                    echo $trnxId;
                }
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script> 
                <?php
            }
        }  else {
            session_destroy();
            ?>
            <script> window.location.href = "index.php";</script> 
            <?php
        }
    }  else {
        session_destroy();
        ?>
        <script> window.location.href = "index.php";</script> 
        <?php
    }
}

if ($_action == "ShowPenaltyPay") {
    $response = $emp->ShowPenaltyPay();
		if($response[0]=='Success'){
			echo "<option value='' selected='selected'>Select </option>";
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<option value=" . $_Row['Event_Payment'] . ">" . $_Row['payment_mode'] . "</option>";
				}
		}
		else{
			echo "0";
		}
    
}