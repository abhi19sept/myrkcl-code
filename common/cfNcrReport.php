<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsNcrReport.php';

$response = array();
$emp = new clsNcrReport();


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>Ack Number</th>";
    echo "<th style='60%'>RSP Code</th>";
    echo "<th style='60%'>RSP Name</th>";
    echo "<th style='10%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='40%'>IT-GK Mob No</th>";
    echo "<th style='10%'>IT-GK Area Type</th>";
    
    echo "<th style='10%'>Mohalla (Urban)/Panchayat Samiti (Rural)</th>";
    echo "<th style='10%'>Ward No (Urban)/Gram Panchayat (Rural)</th>";
    echo "<th style='10%'>Municipal Town (Urban)/Village (Rural)</th>";
    
    echo "<th style='40%'>IT-GK District</th>";
    echo "<th style='10%'>IT-GK Tehsil</th>";
    echo "<th style='10%'>IT-GK Status</th>";
     echo "<th style='10%'>IT-GK Rejection Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Org_Ack'] . "</td>";
            echo "<td>" . $_Row['Org_RspLoginId'] . "</td>";
            echo "<td>" . $_Row['RSPNAME'] . "</td>";
            echo "<td>" . $_Row['CenterCode'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['Org_Mobile'] . "</td>";
            echo "<td>" . $_Row['Org_AreaType'] . "</td>";
            if ($_Row['Org_AreaType'] == 'Urban') {
                echo "<td>" . $_Row['Org_Mohalla'] . "</td>";
                echo "<td>" . $_Row['Org_WardNo'] . "</td>";
                echo "<td>" . $_Row['Org_Municipal'] . "</td>";
            } else {
                echo "<td>" . $_Row['Block_Name'] . "</td>";
                echo "<td>" . $_Row['GP_Name'] . "</td>";
                echo "<td>" . $_Row['Org_Village'] . "</td>";
            }
            
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['Org_Status'] . "</td>";
            echo "<td>" . $_Row['Org_Remark'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


