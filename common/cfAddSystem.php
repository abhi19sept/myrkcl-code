<?php

/* 
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsAddSystem.php';

$response = array();
$emp = new clsAddSystem();


if ($_action == "ADD") {
    //echo 'ADD';
        if (isset($_POST["type"])) {
            if (isset($_POST["processor"])) {
                if (isset($_POST["ram"])) {
                    if (isset($_POST["hdd"])) {
            
                        $_Type = $_POST["type"];
                        $_Processor = $_POST["processor"];
                        $_RAM=$_POST["ram"];
                        $_HDD=$_POST["hdd"];

        $response = $emp->Add($_Type,$_Processor,$_RAM,$_HDD);
        echo $response[0];
                    } else {
                            echo "Invalid Entry";
                        }
                    } else {
                        echo "Invalid Entry";
                    }
                } else {
                    echo "Invalid Entry";
                }
            } else {
                echo "Invalid Entry";
            }
        }
    


if ($_action == "UPDATE") {
    if (isset($_POST["type"])) {
         if (isset($_POST["processor"])) {
            if (isset($_POST["ram"])) {
                if (isset($_POST["hdd"])) {
                    
                        $_Type = $_POST["type"];
                        $_Processor = $_POST["processor"];
                        $_RAM=$_POST["ram"];
                        $_HDD=$_POST["hdd"];
                        $_Code = $_POST['code'];
        $response = $emp->Update($_Code,$_Type,$_Processor,$_RAM,$_HDD);
        echo $response[0];
    }               else {
                            echo "Invalid Entry";
                        }
                    } else {
                        echo "Invalid Entry";
                    }
                } else {
                    echo "Invalid Entry";
                }
            } else {
                echo "Invalid Entry";
            }
        }


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("SystemCode" => $_Row['System_Code'],
            "Processor" => $_Row['System_Processor'],
            "RAM" => $_Row['System_RAM'],
            "HDD"=>$_Row['System_HDD']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>System Type</th>";
    echo "<th style='20%'>Processor</th>";
    echo "<th style='20%'>RAM</th>";
    echo "<th style='10%'>HDD</th>";
    echo "<th style='10%'>Syatem Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	 echo "</div>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['System_Type'] . "</td>";
         echo "<td>" . $_Row['Processor_Name'] . "</td>";
         echo "<td>" . $_Row['System_RAM'] . "</td>";
         echo "<td>" . $_Row['System_HDD'] . "</td>";
		 echo "<td>" . $_Row['System_Status'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}


if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select System Type</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['System_Code'] . ">" . $_Row['System_Type'] . "</option>";
    }
}