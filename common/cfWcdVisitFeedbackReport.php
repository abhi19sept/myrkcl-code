<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsWcdVisitFeedbackReport.php';

    $response = array();
    $emp = new clsWcdVisitFeedbackReport();

    if ($_action == "DETAILS") {
		
	$response = $emp->GetITGK_NCR_Details();
       
	$_DataTable = "";
		
	echo "<div class='table-responsive'>";
	echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
	echo "<thead>";
	echo "<tr>";
	echo "<th style='5%'>S No.</th>";
        echo "<th style='5%'>Visitor Code</th>";
        echo "<th style='5%'>Visitor Name</th>";
	echo "<th style='5%'>ITGK-CODE</th>";
        echo "<th style='25%'>ITGK Name</th>";
          echo "<th style='25%'>ITGK Mobile</th>";
            echo "<th style='25%'>ITGK District</th>";
             echo "<th style='25%'>ITGK Tehsil</th>";
        echo "<th style='15%'>SP Code</th>";
        echo "<th style='15%'>SP Name</th>";
        
        echo "<th style='15%'>No of Systems</th>";
        echo "<th style='15%'>Toilet Availablity</th>";
        echo "<th style='15%'>Separate Toilet</th>";
        echo "<th style='15%'>Water Availablity</th>";
        echo "<th style='5%'>Inagurated</th>";
        echo "<th style='5%'>Details Uploaded</th>";
        echo "<th style='5%'>Inguration Plan Date</th>";
        
	echo "<th style='5%'>Biometric Informed</th>";
        echo "<th style='5%'>Faculty Name</th>";
        echo "<th style='5%'>Faculty Qualification</th>";
         echo "<th style='5%'>Learners Present</th>";
         echo "<th style='5%'>Visit Scheduled Date</th>";
         echo "<th style='5%'>Visit Confirm Date</th>";
        echo "<th style='5%'>Feedback Date</th>";

	echo "</tr>";
	echo "</thead>";
	echo "<tbody>";
	if($response[0] == 'Success') {
	$_Count = 1;
	while ($_Row = mysqli_fetch_array($response[2])) {

	echo "<tr class='odd gradeX'>";
	echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['WCDFeedback_User']) . "</td>";
        echo "<td>" . strtoupper($_Row['fullname']) . "</td>";
        echo "<td>" . ($_Row['ITGKCODE'] ? $_Row['ITGKCODE'] : 'Somthing Wrong') . "</td>";
	echo "<td>" . ($_Row['ITGK_Name'] ? strtoupper($_Row['ITGK_Name']) : 'NA') . "</td>";
        echo "<td>" . ($_Row['ITGKMOBILE'] ? $_Row['ITGKMOBILE'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['District_Name'] ? strtoupper($_Row['District_Name']) : 'NA') . "</td>";
        echo "<td>" . ($_Row['Tehsil_Name'] ? strtoupper($_Row['Tehsil_Name']) : 'NA') . "</td>";
        echo "<td>" . strtoupper($_Row['RSP_Code']) . "</td>";
        echo "<td>" . strtoupper($_Row['RSP_Name']) . "</td>";
        
        echo "<td>" . ($_Row['WCDFeedback_Desktop'] ? $_Row['WCDFeedback_Desktop'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['WCDFeedback_Toilet'] ? $_Row['WCDFeedback_Toilet'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['WCDFeedback_Toilet'] == 'Yes' ? $_Row['WCDFeedback_SToilet'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['WCDFeedback_Water'] ? $_Row['WCDFeedback_Water'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['WCDFeedback_Inagurated'] ? $_Row['WCDFeedback_Inagurated'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['WCDFeedback_Inagurated'] == 'Yes' ? $_Row['WCDFeedback_Upload'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['WCDFeedback_Inagurated'] == 'No' ? $_Row['WCDFeedback_PlaneDate'] : 'NA') . "</td>";
        
        echo "<td>" . ($_Row['WCDFeedback_Informed'] ? $_Row['WCDFeedback_Informed'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['WCDFeedback_FacultyName'] ? $_Row['WCDFeedback_FacultyName'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['WCDFeedback_Qualification'] ? $_Row['WCDFeedback_Qualification'] : 'NA') . "</td>";
       // echo "<td>" . ($_Row['Admission_Count'] ? $_Row['Admission_Count'] : 'NA') . "</td>";
	   if($_Row['WCDFeedback_AdmissionCodes'] =='NA'){
		   echo "<td>" . 'NA' . "</td>";
	   } else {
         echo "<td><input type='button' class='updcount btn btn-primary' itgkcode='".$_Row['ITGKCODE']."' learnercode='" . $_Row['WCDFeedback_AdmissionCodes'] . "' mode='ShowUpload' value='" . $_Row['Admission_Count'] . "'/></td>";
		}
        echo "<td>" . ($_Row['WCDVisit_Selected_Date'] ? $_Row['WCDVisit_Selected_Date'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['WCDVisit_Confirm_date'] ? $_Row['WCDVisit_Confirm_date'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['WCDFeedback_Date'] ? $_Row['WCDFeedback_Date'] : 'NA') . "</td>";
        
//        if($_Row['ITGKCODE']){
//        $response1 = $emp->Get_Faculty_Details($_Row['ITGKCODE']);
//        $_Row1 = mysql_fetch_array($response1[2]);
//        echo "<td>" . ($_Row1['Staff_Name'] ? $_Row1['Staff_Name'] : 'Faculty Details Not Available') . "</td>";
//        } else {
//        echo "<td>Faculty Details Not Available</td>";    
//        }
        echo "</tr>";
	$_Count++;
	}
	echo "</tbody>";
	echo "</table>";
	echo "</div>";
	}
    }
	 
if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetDatabyCode($_POST["itgkcode"], $_POST["learnercode"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Learner Code</th>";
    echo "<th style='20%'>Learner Name</th>";
    echo "<th style='10%'>Father Name</th>";
    echo "<th style='10%'>DOB</th>";
   
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	 echo "</div>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
         echo "<td>" . $_Row['Admission_Name'] . "</td>";
         echo "<td>" . $_Row['Admission_Fname'] . "</td>";
         echo "<td>" . $_Row['Admission_DOB'] . "</td>";
        
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
?>