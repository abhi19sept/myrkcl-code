<?php

/* 
 * author Sunil Kumar Baindara

 */
include './commonFunction.php';
require 'BAL/clswebservicescoresync.php';
//require 'include/siteconfig.php';

ini_set("display_errors", "on");
ini_set("error_reporting", E_ERROR);
require_once("../nusoap/nuSOAP/lib/nusoap.php");
//define("SERVER_WSDL_API", "http://localhost/toc/nusoap/toc.php?wsdl");
//define("SERVER_WSDL_API", "http://ilearn.myrkcl.com/nusoap/toc.php?wsdl");
define("SERVER_WSDL_API", "http://49.50.65.34/nusoap/toc.php?wsdl");
date_default_timezone_set('Asia/Kolkata'); 
 //echo date('d-m-Y H:i');
$response = array();
$emp = new clswebservicescoresync();



if ($_action == "FILLTableName") {
    $response = $emp->GetWebserviceTableName();	
    echo "<option value='0'>Select Table Name</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['WTableName'] . ">" . $_Row['WTableName'] . "</option>";
    }
}
/* Show Table Count Detail*/
if ($_action == "COUNTTABLE") {
     //if (isset($_POST["values"] ) && !empty($_POST["values"]))
	 //{
         $totalcount= array();
         $tablename = 'tbl_learner_score';
         $response = $emp->GetTableCount($tablename);
         $_Row = mysqli_fetch_array($response[2]);
         echo '<div class="block-header" style="padding-left: 15px;padding-bottom: 13px;">
                <h2><b>Table Name</b> : '.$tablename.'</h2>
            </div>';
         echo $counttotal=' <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">MYRKCL TOTAL COUNT</div>
                            <div id="clickcount1" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">'.$_Row['totalcount'].'</div>
                        </div>
                    </div>
                </div>';
         
}

if ($_action == "getTotalByBatchCourse") {
     //if (isset($_POST["values"] ) && !empty($_POST["values"]))
	 //{
    //echo "<pre>";print_r($_POST);die;
         $totalcount= array();
         $tablename = 'tbl_learner_score';
         $response = $emp->GetTableCountLearnerScore($_POST['ddlCourse'],$_POST['ddlBatch']);
         
         $_Row = mysqli_fetch_array($response[2]);
         //echo "<pre>";print_r($_Row);die;
         echo '<div class="block-header" style="padding-left: 15px;padding-bottom: 13px;">
                <h2><b>Course,Batch Name</b> : '.$_Row['Course_Name'].','.$_Row['Batch_Name'].'</h2>
            </div>';
         echo $counttotal=' <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">MYRKCL TOTAL COUNT</div>
                            <div id="clickcount1" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">'.$_Row['totalcount'].'</div>
                        </div>
                    </div>
                </div>';
                        
                    $key = "RKCL_ILEARN";
                    $server_ip = '49.50.79.170';
                    $cname = 'RKCLILEARN';
                    $course = $_POST['ddlCourse'];
                    $batch = $_POST['ddlBatch']; 
                    $client = new nusoap_client(SERVER_WSDL_API, true, false, false, false, false, 300, 300);
                    $client->soap_defencoding = 'UTF-8';
                    $client->decode_utf8 = false;
                    ini_set('default_socket_timeout', 5000);                    
                    $result = $client->call("getLearnerAssessementFinalResultCount",array("cname" => $cname, "hashkey" => $key, "clientip" => $server_ip, "batchname" => $batch, "coursename" => $course));
                    $res=json_decode(base64_decode($result), true);
                if($res>0){
                        $rescount=$res;
                }else{
                        $rescount=0;
                }                    
                    echo $counttotal=' <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">iLEARN TOTAL COUNT</div>
                            <div id="clickcount1" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">'.$rescount.'</div>
                        </div>
                    </div>
                </div>';
                    echo '<div id="iLEARNDiv"></div>';
         
}
/* Show Table Count Detail*/

if ($_action == "SINKTABLE") {
     
            //echo "<pre>";print_r($_POST);die;
                    $tablename = 'tbl_assessementfinalresult';
                    $key = "RKCL_ILEARN";
                    $server_ip = '49.50.79.170';
                    $cname = 'RKCLILEARN';
                    
                    
        //$columnname='Fresult_id'; 
        //$lastrecordid ='2';
            if(isset($_POST['course']) && $_POST['course']!='' && isset($_POST['batch']) && $_POST['batch']!='')
                {
                      $course = $_POST['course'];
                      $batch = $_POST['batch'];  
                    
                    $client = new nusoap_client(SERVER_WSDL_API, true, false, false, false, false, 300, 300);
                    $client->soap_defencoding = 'UTF-8';
                    $client->decode_utf8 = false;
                    ini_set('default_socket_timeout', 5000);                    
                    $result = $client->call("getiLearneTableInfoAll",array("cname" => $cname, "hashkey" => $key, "clientip" => $server_ip, "tablename" => $tablename, "batchname" => $batch, "coursename" => $course));
                    $res=json_decode(base64_decode($result), true);
                     //echo "<pre>";print_r($res);die;
                    if($res)
                        {
                        if($res=='1')
                            {
                            echo "NONEWRF";//'No New Record Found.';
                            }
                        elseif($res=='2') 
                            {
                             echo "CYHPDNM";//'Credentials You have provided, Does not matched.';
                            }
                            else
                            {
                   // sari entry kerke last record bejte h jo update hua h uski entery kerte h fir///    
                            foreach ($res as $key => $value) {
                                
                            $resAC = $emp->getAdmissionCode($value['Fresult_learner']);
                            $AC_row=mysqli_fetch_array($resAC[2],true);
                            $Admission_Code=$AC_row['Admission_Code'];
                            
                            $Learner_Score_Id=$value['Fresult_id'];                            
                            $Learner_Code=$value['Fresult_learner'];
                            $Score=$value['Fresult_obtainmarks'];
                            $Score_Per=($value['Fresult_obtainmarks']/30*100);
                            $ITGK_code=$value['Fresult_itgk'];
                            $AddEditTime=date("Y-m-d h:i:s");
                            $response = $emp->AddLearnerScore($Admission_Code, $Learner_Code, $Score, $Score_Per, $ITGK_code, $AddEditTime); 
                            //echo "sunil";echo "<pre>";print_r($response); echo "sunil";die;
                            if($response[0]=='Already Exists')
                                {

                                   $up_row=mysqli_fetch_array($response[2],true);
                                   $Learner_Score_Id_old=$up_row['Learner_Score_Id'];
                                   $responseUpdate = $emp->UpdateLearnerScore($Learner_Score_Id_old, $Admission_Code, $Learner_Code, $Score, $Score_Per, $ITGK_code, $AddEditTime); 

                                }
                           
                            }
                            
                            
                                $lastrecordid=$Learner_Score_Id;
                                $columnname='Fresult_id';
                                
                               //if($_SERVER['HTTP_HOST']=="localhost"){
                                    $key = "RKCL_ILEARN";
                                    $server_ip = '49.50.79.170';
                                    $cname = 'RKCLILEARN';
                                 //}else{ 
                                    //$key = "0019042017100_MyRkcl";
                                    //$server_ip = '49.50.79.170';
                                    //$cname = 'RkclMyRkcl';
                                 //}
                                $result2 = $client->call("updateiLearneTableInfoAll",array("cname" => $cname, "hashkey" => $key, "clientip" => $server_ip, "tablename" => $tablename, "columnname" => $columnname, "lastrecordid" => $lastrecordid, "batchname" => $batch, "coursename" => $course));
                                $res2=json_decode(base64_decode($result2), true);
                                //echo "sunil";echo "<pre>";print_r($res2); echo "sunil";
                                //;
                                //echo "DONE";
                                echo $counttotal=' <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box bg-amber  hover-expand-effect">
                                    <div class="icon">
                                        <i class="material-icons">playlist_add_check</i>
                                    </div>
                                    <div class="content">
                                        <div class="text">Sync Learner Count From iLEARN To MyRKCL</div>
                                        <div id="clickcount1" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20">'.count($res).'</div>
                                    </div>
                                </div>
                            </div>';
                                
                            }
                        }
                    else
                        {
                            echo "APIERR";
                        }
               }
            else
                {
                   echo "COUBAT";
                }
}

