<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsItgkRating.php';

$response = array();
$emp = new clsItgkRating();


if ($_action == "FillBatch") {
    $response = $emp->GetBatch();
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "CheckBatchstatus") {    
    $response = $emp->GetBatchstatus($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "updateadmissionwiserank") {    
    $response = $emp->AdmissionWiseRank($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "updatelearnersenrollwiserank") {    
    $response = $emp->LearnersEnrollWiseRank($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "updateattendancewiserank") {    
    $response = $emp->AttendanceWiseRank($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "updateblockunblockwiserank") {    
    $response = $emp->BlockUnBlockWiseRank($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "updatederegisterwiserank") {    
    $response = $emp->DeregisterWiseRank($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "updateresultwiserank") {    
    $response = $emp->ResultWiseRank($_POST['batchcode'],$_POST['examevent']);		
    echo $response[0];
}

if ($_action == "updateinfrawiserank") {    
    $response = $emp->InfraWiseRank($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "updatedpofeedbackwiserank") {    
    $response = $emp->DpoFeedbackkWiseRank($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "updatelearnerfeedbackwiserank") {    
    $response = $emp->LearnerFeedbackWiseRank($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "updateilearnmarkswiserank") {    
    $response = $emp->iLearnMarksWiseRank($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "updatedigitalcertificatewiserank") {    
    $response = $emp->DigitalCertificateWiseRank($_POST['batchcode'],$_POST['examevent']);		
    echo $response[0];
}

if ($_action == "updatesupportticketswiserank") {    
    $response = $emp->SupportTicketsWiseRank($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "UpdateFinalRating") {    
    $response = $emp->FinalRatingUpdate($_POST['batchcode']);		
    echo $response[0];
}

if ($_action == "FILLITGK") {
    $response = $emp->GetITGK();
    echo "<option value='0' selected='true'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_LoginId'] . ">" . $_Row['User_LoginId'] . "</option>";
    }
}

if ($_action == "GETItgkRating") {
		if($_POST['itgkcode']==''){
			echo "blank";
		}
		else{
		$response = $emp->GetDataForITGKCode($_POST['itgkcode']);
		$_DataTable = array();
		$_i = 0;
		$avgrate = 0;
		$mnth='';
		if ($response[0] == 'Success') {
			while ($_Row = mysqli_fetch_array($response[2])) {
					 if($_i == 0){ $mnth='January'; }   else if($_i == 1){ $mnth='February'; }
				else if($_i == 2){ $mnth='March'; } 	else if($_i == 3){ $mnth='April'; }
				else if($_i == 4){ $mnth='May'; } 		else if($_i == 5){ $mnth='June'; }
				else if($_i == 6){ $mnth='July'; } 		else if($_i == 7){ $mnth='August'; }
				else if($_i == 8){ $mnth='September'; } else if($_i == 9){ $mnth='October'; }
				else if($_i == 10){ $mnth='November'; } else if($_i == 11){ $mnth='December'; }
				
				echo "<div class='col-lg-3 col-md-3 col-sm-12 col-xs-12 details' id='mnth".$_i."'>
						   <div class='ratingblog'>
								<div class='ratingjan'><h3>".$mnth."</h3></div>
								<div class='ratingboxbg'><p>ITGK Rating</p>
							<div id='month0' class='ratingboxcont'>".$_Row['ITGK_Rating_Final_Rating']." </div>";
					echo "<div class='jstars".$_i."' data-value='" . $_Row['ITGK_Rating_Final_Rating'] . "'></div>";
									echo "<script>$('.jstars".$_i."').jstars();</script>
								</div>
							</div>
						</div>";
				
				$avgrate = ($avgrate + $_Row['ITGK_Rating_Final_Rating']);
				
				$_i = $_i + 1;
			}
					$finalavg=($avgrate/$_i); 	
					$finalavgrate=round($finalavg,1);
				echo "<div class='col-lg-3 col-md-3 col-sm-12 col-xs-12' id='allLoginAvgs'>
							   <div class='ratingblog'>
								   <div class='ratingboxbg'><p>Average ITGK Rating</p>
										<div id='monthlyavg' class='ratingboxcont'>".$finalavgrate."</div>";
								echo "<div class='jstarsavg' data-value='" . $finalavgrate . "'></div>";
								//echo "<script>$('.jstarsavg').jstars();</script>
									echo "</div>
								</div>
						</div>";
			}
		}
}

if ($_action == "GetDataForItgkRating") {		
		$response = $emp->GetDataForITGKRating();
		$_DataTable = array();
		$_i = 0;
		$avgrate = 0;
		$mnth='';
		if ($response[0] == 'Success') {
			while ($_Row = mysqli_fetch_array($response[2])) {
				if($_i == 0){ $mnth='January'; }   else if($_i == 1){ $mnth='February'; }
				else if($_i == 2){ $mnth='March'; } 	else if($_i == 3){ $mnth='April'; }
				else if($_i == 4){ $mnth='May'; } 		else if($_i == 5){ $mnth='June'; }
				else if($_i == 6){ $mnth='July'; } 		else if($_i == 7){ $mnth='August'; }
				else if($_i == 8){ $mnth='September'; } else if($_i == 9){ $mnth='October'; }
				else if($_i == 10){ $mnth='November'; } else if($_i == 11){ $mnth='December'; }
				echo "<div class='col-lg-3 col-md-3 col-sm-12 col-xs-12 details' id='mnth".$_i."'>
						   <div class='ratingblog'>
								<div class='ratingjan'><h3>".$mnth."</h3></div>
								<div class='ratingboxbg'><p>ITGK Rating</p>
							<div id='month0' class='ratingboxcont'>".$_Row['ITGK_Rating_Final_Rating']." </div>";
			echo "<div class='jstars".$_i."' data-value='" . $_Row['ITGK_Rating_Final_Rating'] . "'></div>";
									echo "<script>$('.jstars".$_i."').jstars();</script>
								</div>
							</div>
						</div>";
				
				$avgrate = ($avgrate + $_Row['ITGK_Rating_Final_Rating']);
				
				$_i = $_i + 1;
			}
					$finalavg=($avgrate/$_i); 	
					$finalavgrate=round($finalavg,1);
				echo "<div class='col-lg-3 col-md-3 col-sm-12 col-xs-12' id='ItgkLoginAvgs'>
							   <div class='ratingblog'>
								   <div class='ratingboxbg'><p>Average ITGK Rating</p>
										<div id='monthlyavg' class='ratingboxcont'>".$finalavgrate."</div>";
								echo "<div class='jstarsavg' data-value='" . $finalavgrate . "'></div>";
								//echo "<script>$('.jstarsavg').jstars();</script>
								echo "</div>
								</div>
						</div>";
		}		
}

if ($_action == "GetRatingDetails")
	{
        $response = $emp->GetAll($_POST['itgkcode'],$_POST['mnth']);
        $responses = $emp->GetAllParameters();
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered '>";
		echo "<thead>";
		echo "<tr>";
		
		echo "<th>S No.</th>";
		echo "<th>CRITERIA</th>";
		echo "<th>RATING RATIO</th>";
		echo "<th>STAR RATING</th>";
		echo "<th>NUMBER RATING</th>";
					
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		
			$_Count = 1;	
				$_Row = mysqli_fetch_array($response[2]); 
				while($_Rows = mysqli_fetch_array($responses[2])){
					$arr[] = $_Rows['ranking_weightage_parameter'];
				}
					$count_row=mysqli_num_fields($response[2]);
					$row_cnt=$count_row/2;
					$j=0;
					for($i=1;$i<=$row_cnt;$i++){
					echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $arr[$j] . "</td>";
						echo "<td>" . $_Row['a'.$_Count] . "</td>";
						echo "<td><div class='frate".$i."' data-value='" . $_Row['s'.$_Count] . "'></div></td>";
						echo "<script>$('.frate".$i."').jstars();</script>";
						echo "<td >" . $_Row['s'.$_Count] . "</td>";			  
					echo "</tr>";
					$j++;
			$_Count++;
					}
				
		
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "SHOW") {

    $response = $emp->Search($_POST["values"]);
    echo "<div class='table-responsive'>";
    echo "<table id='examples' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
           
            echo "<td id='fill_.'".$_Count."' class='clk'>" . $row['User_LoginId'] . "</td>";


            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "GetDataForSPRating") {		
		$response = $emp->GetDataForSPRating();
		$responses = $emp->GetAvgRatingForSP();
		$_DataTable = array();
		
		$_Rows = mysqli_fetch_array($responses[2]);
		$finalavgrate=$_Rows['final_avg'];
		
		echo "<div class='col-lg-3 col-md-3 col-sm-12 col-xs-12'  
							style='margin-right: 63px; position: absolute; right: 0px; top: 210px !important;   
									z-index: 999; width: 21.5%;'>
							   <div class='ratingblog'>
								   <div class='ratingboxbg'><p>Average SP Rating</p>
										<div id='monthlyavg' class='ratingboxcont'>".$finalavgrate."</div>";
								echo "<div class='jstarsavg' data-value='" . $finalavgrate . "'></div>";
								echo "<script>$('.jstarsavg').jstars();</script>
									</div>
								</div>
						</div>";
		
		echo "<div class='table-responsive'>";
		echo "<table id='spexample' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered '>";
		echo "<thead>";
		echo "<tr>";
		
		echo "<th>S No.</th>";
		echo "<th>ITGK-CODE</th>";
		echo "<th>STAR RATING</th>";
		echo "<th>NUMBER RATING</th>";			
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
			$i=1;
			$_Count = 1;			
				while($_Row = mysqli_fetch_array($response[2])){					
					echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $_Row['ITGK_Rating_ITGK_code'] . "</td>";
						echo "<td><div class='frates".$i." getdetails' data-value='" . $_Row['avg'] . "' 
									id='".$_Row['ITGK_Rating_ITGK_code']."'></div></td>";
						echo "<td>" . $_Row['avg'] . "</td>";					
					echo "</tr>";
					
			$_Count++;
					}
				echo "<script>$('.frates".$i."').jstars();</script>";
		
    echo "</tbody>";
	
    echo "</table>";
	echo "</div>";	
}

if ($_action == "GetSPRatingDetails") {		
		$response = $emp->GetDetailsForSPRating($_POST['itgkcode']);
		$_DataTable = array();
		$_i = 0;
		$mnth='';
		if ($response[0] == 'Success') {
			while ($_Row = mysqli_fetch_array($response[2])) {
				if($_i == 0){ $mnth='January'; }   else if($_i == 1){ $mnth='February'; }
				else if($_i == 2){ $mnth='March'; } 	else if($_i == 3){ $mnth='April'; }
				else if($_i == 4){ $mnth='May'; } 		else if($_i == 5){ $mnth='June'; }
				else if($_i == 6){ $mnth='July'; } 		else if($_i == 7){ $mnth='August'; }
				else if($_i == 8){ $mnth='September'; } else if($_i == 9){ $mnth='October'; }
				else if($_i == 10){ $mnth='November'; } else if($_i == 11){ $mnth='December'; }
				echo "<div class='col-lg-3 col-md-3 col-sm-12 col-xs-12 details' id='mnth".$_i."'>
						   <div class='ratingblog'>
								<div class='ratingjan'><h3>".$mnth."</h3></div>
								<div class='ratingboxbg'><p>ITGK Rating</p>
							<div id='month0' class='ratingboxcont'>".$_Row['ITGK_Rating_Final_Rating']." </div>";
			echo "<div class='jstars".$_i." param' data-value='" . $_Row['ITGK_Rating_Final_Rating'] . "'
						id='mnth".$_i."' name='".$_POST['itgkcode']."'></div>";
									echo "<script>$('.jstars".$_i."').jstars();</script>
								</div>
							</div>
						</div>";
				
				
				$_i = $_i + 1;
			}
					
		}		
}

if ($_action == "GetSPRatingparameters")
	{
        $response = $emp->GetAll($_POST['itgkcode'],$_POST['mnth']);
        $responses = $emp->GetAllParameters();
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='examplesp' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered '>";
		echo "<thead>";
		echo "<tr>";
		
		echo "<th>S No.</th>";
		echo "<th>CRITERIA</th>";
		echo "<th>RATING RATIO</th>";
		echo "<th>STAR RATING</th>";
		echo "<th>NUMBER RATING</th>";
					
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		
			$_Count = 1;	
				$_Row = mysqli_fetch_array($response[2]); 
				while($_Rows = mysqli_fetch_array($responses[2])){
					$arr[] = $_Rows['ranking_weightage_parameter'];
				}
					$count_row=mysqli_num_fields($response[2]);
					$row_cnt=$count_row/2;
					$j=0;
					for($i=1;$i<=$row_cnt;$i++){
					echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $arr[$j] . "</td>";
						echo "<td>" . $_Row['a'.$_Count] . "</td>";
						echo "<td><div class='frate".$i."' data-value='" . $_Row['s'.$_Count] . "'></div></td>";
						echo "<script>$('.frate".$i."').jstars();</script>";
						echo "<td >" . $_Row['s'.$_Count] . "</td>";			  
					echo "</tr>";
					$j++;
			$_Count++;
					}
				
		
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "GETAllSPRating") {		
		$response = $emp->GETAllSPRating();
		$_DataTable = array();		
		
		echo "<div class='table-responsive'>";
		echo "<table id='Allspexample' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered '>";
		echo "<thead>";
		echo "<tr>";
		
		echo "<th>S No.</th>";
		echo "<th>SP-CODE</th>";
		echo "<th>STAR RATING</th>";
		echo "<th>NUMBER RATING</th>";			
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
			$i=1;
			$_Count = 1;			
				while($_Row = mysqli_fetch_array($response[2])){					
					echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $_Row['User_LoginId'] . "</td>";
						echo "<td><div class='fratess".$i." getspdetails' data-value='" . $_Row['avg'] . "' 
									id='".$_Row['ITGK_Rating_Rsp_code']."'></div></td>";
						echo "<td>" . $_Row['avg'] . "</td>";					
					echo "</tr>";
					
			$_Count++;
					}
				echo "<script>$('.fratess".$i."').jstars();</script>";
		
    echo "</tbody>";
	
    echo "</table>";
	echo "</div>";	
}

if ($_action == "GetSpItgkWiseRatingDetails") {		
		$response = $emp->GetSpItgkWiseRatingDetails($_POST['spcode']);
		$_DataTable = array();	
		
		echo "<div class='table-responsive'>";
		echo "<table id='spitgkexample' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		
		echo "<th>S No.</th>";
		echo "<th>ITGK-CODE</th>";
		echo "<th>STAR RATING</th>";
		echo "<th>NUMBER RATING</th>";			
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
			$i=1;
			$_Count = 1;			
				while($_Row = mysqli_fetch_array($response[2])){					
					echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $_Row['ITGK_Rating_ITGK_code'] . "</td>";
						echo "<td><div class='frates".$i."' data-value='" . $_Row['avg'] . "' 
									id='".$_Row['ITGK_Rating_ITGK_code']."'></div></td>";
						echo "<td>" . $_Row['avg'] . "</td>";					
					echo "</tr>";
					
			$_Count++;
					}
				echo "<script>$('.frates".$i."').jstars();</script>";
		
    echo "</tbody>";
	
    echo "</table>";
	echo "</div>";	
}

if ($_action == "GetDistrictWiseData") {		
		$response = $emp->GetDistrictWiseData($_POST['districtcode']);
		$_DataTable = array();	
		
		echo "<div class='table-responsive'>";
		echo "<table id='Allexample' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		
		echo "<th>S No.</th>";
		echo "<th>ITGK-CODE</th>";
		echo "<th>STAR RATING</th>";
		echo "<th>NUMBER RATING</th>";
		echo "<th>Rank</th>";		
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
			$i=1;
			$_Count = 1;
			$rank = 0;
			$previous = 0;
				while($_Row = mysqli_fetch_array($response[2])){					
					echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $_Row['ITGK_Rating_ITGK_code'] . "</td>";						
						echo "<td><div class='fratesss".$i." getdistrictwise' data-value='" . $_Row['avg'] . "' 
									id='".$_Row['ITGK_Rating_ITGK_code']."'></div></td>";
						echo "<td>" . $_Row['avg'] . "</td>";
							$current = $_Row['avg'];
							if ($current == $previous){
								$rank=$rank;
							}
							else{
								$rank++;
							}
							$previous = $current;
						echo "<td>" . 'Rank - '.$rank . "</td>";
						
					echo "</tr>";
					
			$_Count++;
					}
				echo "<script>$('.fratesss".$i."').jstars();</script>";
		
    echo "</tbody>";
	
    echo "</table>";
	echo "</div>";	
}

if ($_action == "GetDistrictmonthwiseRating") {		
		$response = $emp->GetDetailsForSPRating($_POST['itgkcode']);
		$_DataTable = array();
		$_i = 0;
		$mnth='';
		if ($response[0] == 'Success') {
			while ($_Row = mysqli_fetch_array($response[2])) {
				if($_i == 0){ $mnth='January'; }   else if($_i == 1){ $mnth='February'; }
				else if($_i == 2){ $mnth='March'; } 	else if($_i == 3){ $mnth='April'; }
				else if($_i == 4){ $mnth='May'; } 		else if($_i == 5){ $mnth='June'; }
				else if($_i == 6){ $mnth='July'; } 		else if($_i == 7){ $mnth='August'; }
				else if($_i == 8){ $mnth='September'; } else if($_i == 9){ $mnth='October'; }
				else if($_i == 10){ $mnth='November'; } else if($_i == 11){ $mnth='December'; }
				echo "<div class='col-lg-3 col-md-3 col-sm-12 col-xs-12 detailss' id='mnth".$_i."'>
						   <div class='ratingblog'>
								<div class='ratingjan'><h3>".$mnth."</h3></div>
								<div class='ratingboxbg'><p>ITGK Rating</p>
							<div id='month0' class='ratingboxcont'>".$_Row['ITGK_Rating_Final_Rating']." </div>";
			echo "<div class='jstars".$_i." params' data-value='" . $_Row['ITGK_Rating_Final_Rating'] . "'
						id='mnth".$_i."' name='".$_POST['itgkcode']."'></div>";
									echo "<script>$('.jstars".$_i."').jstars();</script>
								</div>
							</div>
						</div>";
				
				
				$_i = $_i + 1;
			}
					
		}		
}

if ($_action == "GetDistrictItgkRatingparameters")
	{
        $response = $emp->GetAll($_POST['itgkcode'],$_POST['mnth']);
        $responses = $emp->GetAllParameters();
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='exampledist' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered '>";
		echo "<thead>";
		echo "<tr>";
		
		echo "<th>S No.</th>";
		echo "<th>CRITERIA</th>";
		echo "<th>RATING RATIO</th>";
		echo "<th>STAR RATING</th>";
		echo "<th>NUMBER RATING</th>";
		
					
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		
			$_Count = 1;	
				$_Row = mysqli_fetch_array($response[2]); 
				while($_Rows = mysqli_fetch_array($responses[2])){
					$arr[] = $_Rows['ranking_weightage_parameter'];
				}
					$count_row=mysqli_num_fields($response[2]);
					$row_cnt=$count_row/2;
					$j=0;
					for($i=1;$i<=$row_cnt;$i++){
					echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $arr[$j] . "</td>";
						echo "<td>" . $_Row['a'.$_Count] . "</td>";
						echo "<td><div class='distfrate".$i."' data-value='" . $_Row['s'.$_Count] . "'></div></td>";
						echo "<script>$('.distfrate".$i."').jstars();</script>";
						echo "<td >" . $_Row['s'.$_Count] . "</td>";
							
						
	
					echo "</tr>";
					$j++;
			$_Count++;
					}
				
		
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "FillExamEvent") {
        $response = $emp->GetExamEvent();
        echo "<option value='' selected='selected'>Please Select</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name_VMOU'] . "</option>";    
        }
    }
