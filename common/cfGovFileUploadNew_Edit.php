<?php
/*
 * Made By : SUNIL KUMAR BAINDARA
 * Dated: 15-3-2017
 */
session_start();

//$_SESSION['ImageFile'] = "";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

$_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_cancelcheck/';
$_UploadDirectory2 = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_dpletter/';
$_UploadDirectory3 = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_rscitcer/';

$parentid = $_POST['UploadId'];
if ($_FILES["SelectedFile1"]["name"] != '') {
    $imag = $_FILES["SelectedFile1"]["name"];
    $imageinfo = pathinfo($_FILES["SelectedFile1"]["name"]);
    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
        $flag = 0;
    } else {
        //if (file_exists("$_UploadDirectory1/" . $parentid . '_cancelcheck' . '.' . substr($imag, -3))) {
           // echo $_FILES["SelectedFile1"]["name"] . "already exists";die;
        //} else {
            if (file_exists($_UploadDirectory1)) {
                if (is_writable($_UploadDirectory1)) {
                    move_uploaded_file($_FILES["SelectedFile1"]["tmp_name"], "$_UploadDirectory1/" . $parentid . '_cancelcheck' . '.jpg');
                    //session_start();
                    $_SESSION['EmpCancelCheck'] = $parentid . '_cancelcheck' . '.jpg';
                } else {
                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
                }
            } else {
                outputJSON("<span class='error'>Upload Folder Not Available</span>");
            }
       // }
    }
}


if ($_FILES["SelectedFile2"]["name"] != '') {
    $imag = $_FILES["SelectedFile2"]["name"];
    $imageinfo = pathinfo($_FILES["SelectedFile2"]["name"]);
    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
        $flag = 0;
    } else {
        //if (file_exists("$_UploadDirectory2/" . $parentid . '_dpletter' . '.' . substr($imag, -3))) {
            //$_FILES["SelectedFile2"]["name"] . "already exists";
       // } else {
            if (file_exists($_UploadDirectory2)) {
                if (is_writable($_UploadDirectory2)) {
                    move_uploaded_file($_FILES["SelectedFile2"]["tmp_name"], "$_UploadDirectory2/" . $parentid . '_dpletter' . '.jpg');
                    //session_start();
                    $_SESSION['DepartmentPermissionLetter'] = $parentid . '_dpletter' . '.jpg';
                } else {
                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
                }
            } else {
                outputJSON("<span class='error'>Upload Folder Not Available</span>");
            }
       // }
    }
}


if ($_FILES["SelectedFile3"]["name"] != '') {
    $imag = $_FILES["SelectedFile3"]["name"];
    $imageinfo = pathinfo($_FILES["SelectedFile3"]["name"]);
    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
        $flag = 0;
    } else {
        //if (file_exists("$_UploadDirectory3/" . $parentid . '_rscitcer' . '.' . substr($imag, -3))) {
            //$_FILES["SelectedFile3"]["name"] . "already exists";
        //} else {
            if (file_exists($_UploadDirectory3)) {
                if (is_writable($_UploadDirectory3)) {
                    move_uploaded_file($_FILES["SelectedFile3"]["tmp_name"], "$_UploadDirectory3/" . $parentid . '_rscitcer' . '.jpg');
                    //session_start();
                    $_SESSION['RSCITFinalCertificate'] = $parentid . '_rscitcer' . '.jpg';
                } else {
                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
                }
            } else {
                outputJSON("<span class='error'>Upload Folder Not Available</span>");
            }
        //}
    }
}