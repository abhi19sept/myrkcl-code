<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsAuthorizationCertificate.php';

$response = array();
$emp = new clsAuthorizationCertificate();

if ($_action == "GetItgkDetails") {
		if($_POST['district'] == '') {
			echo "1";
		}
		else if($_POST['tehsil'] == '') {
			echo "2";
		}
		else {
			$response = $emp->GetItgkList($_POST['district'], $_POST['tehsil']);
			$_DataTable = "";
			echo "<div class='table-responsive'>";
			echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
			echo "<thead>";
			echo "<tr>";
			echo "<th>S No.</th>";
			echo "<th>Center Code</th>";
			echo "<th>Organization Name</th>";
			echo "<th>Organization Address</th>";
			echo "<th>View Certificate</th>";
			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$_Count = 1;
			
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['User_LoginId'] . "</td>";
					echo "<td>" . $_Row['Organization_Name'] . "</td>";
					echo "<td>" . $_Row['Organization_Address'] . "</td>";
				    $edate=$_Row['CourseITGK_ExpireDate'];
					$date = date("Y-m-d");
					if($edate>=$date)
					{
						echo "<td> <a href='frmPrintAutorizationCertificate.php?code=" . $_Row['User_LoginId'] . "' target='_blank'>"
						. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='View'/></a>"						
						. "</td>";
					}
					else{
						echo "<td>"
						. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='Please Renew Your Center'/>"						
						. "</td>";
					}
					
					echo "</tr>";
					$_Count++;
				}

				echo "</tbody>";
				echo "</table>";
				echo "</div>";
			
		}
}		
		
