<style>
    .modal-open {
        overflow: hidden;
    }
    .modal {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1050;
        display: none;
        overflow: hidden;
        -webkit-overflow-scrolling: touch;
        outline: 0;
    }
    .modal.fade .modal-dialog {
        -webkit-transition: -webkit-transform .3s ease-out;
        -o-transition:      -o-transform .3s ease-out;
        transition:         transform .3s ease-out;
        -webkit-transform: translate(0, -25%);
        -ms-transform: translate(0, -25%);
        -o-transform: translate(0, -25%);
        transform: translate(0, -25%);
    }
    .modal.in .modal-dialog {
        -webkit-transform: translate(0, 0);
        -ms-transform: translate(0, 0);
        -o-transform: translate(0, 0);
        transform: translate(0, 0);
    }
    .modal-open .modal {
        overflow-x: hidden;
        overflow-y: auto;
    }
    .modal-dialog {
        position: relative;
        width: auto;
        margin: 10px;
    }
    .modal-content {
        position: relative;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #999;
        border: 1px solid rgba(0, 0, 0, .2);
        width: 100%;
        border-radius: 6px;
        outline: 0;
        -webkit-box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
        box-shadow: 0 3px 9px rgba(0, 0, 0, .5);
    }
    .modal-backdrop {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1040;
        background-color: #000;
    }
    .modal-backdrop.fade {
        filter: alpha(opacity=0);
        opacity: 0;
    }
    .modal-backdrop.in {
        filter: alpha(opacity=50);
        opacity: .5;
    }
    .modal-header {
        min-height: 16.42857143px;
        padding: 15px;
        border-bottom: 1px solid #e5e5e5;
    }
    .modal-header .close {
        margin-top: -2px;
    }
    .modal-title {
        margin: 0;
        line-height: 1.42857143;
    }
    .modal-body {
        position: relative;
        padding: 15px;
    }
    .modal-footer {
        padding: 15px;
        text-align: right;
        border-top: 1px solid #e5e5e5;
        background: #f2f2f2;
    }
    .modal-footer .btn + .btn {
        margin-bottom: 0;
        margin-left: 5px;
    }
    .modal-footer .btn-group .btn + .btn {
        margin-left: -1px;
    }
    .modal-footer .btn-block + .btn-block {
        margin-left: 0;
    }
    .modal-scrollbar-measure {
        position: absolute;
        top: -9999px;
        width: 50px;
        height: 50px;
        overflow: scroll;
    }
    @media (min-width: 768px) {
        .modal-dialog {
            width: 600px;
            margin: 30px auto;
        }
        .modal-content {
            -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
            box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
        }
        .modal-sm {
            width: 300px;
        }
    }
    @media (min-width: 992px) {
        .modal-lg {
            width: 900px;
        }
    }
</style>
<!-- ALL MODALS WILL APPEAR HERE -->
<style>
    .modal-open .container-fluid, .modal-open .container {
        -webkit-filter: blur(5px) grayscale(50%);
    }
</style>
<div id="wait_modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please wait</h4>
            </div>
            <div class="modal-body">
                <p>Please wait while we are detecting the device</p>
            </div>
        </div>
    </div>
</div>

<div id="wait_modal_match" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please wait</h4>
            </div>
            <div class="modal-body">
                <p>Capturing fingerprint, please place your finger on scanner</p>
            </div>
        </div>
    </div>
</div>

<div id="error_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
                <p>Device Not Found</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="error_modal_timeout" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
                <p>Timeout, please try again</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="error_modal_nomatch" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
                <p>Fingerprint mismatch</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="duplicity_checked" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Fingerprint Duplicity Verified</h4>
            </div>
            <div class="modal-body">
                <p style="text-align: left !important;">Your fingerprint duplicity has been verified, Please click on
                    Submit button to register your fingerprint</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="already_enrolled" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Fingerprint Already Enrolled</h4>
            </div>
            <div class="modal-body">
                <p>This finger has already enrolled, please add another fingerprint</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="matching_records" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please wait...</h4>
            </div>
            <div class="modal-body">
                <p>Please wait while we are matching your fingerprint with our records </p>
            </div>
        </div>
    </div>
</div>

<div id="please_wait" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please wait...</h4>
            </div>
            <div class="modal-body">
                <p>Submitting values, please wait...</p>
            </div>
        </div>
    </div>
</div>

<div id="errorModal_Custom" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
                <p id="errorText"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="successfully_submitted" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Success</h4>
            </div>
            <div class="modal-body">
                <p>Successfully Submitted</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="machine_set" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Success</h4>
            </div>
            <div class="modal-body">
                <p>Biometric Device has been successfully set</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="please_wait_verify_duplicity" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please wait...</h4>
            </div>
            <div class="modal-body">
                <p>Verifying duplicity with our records</p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="otp_modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Update Mobile Number
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <div id="responseText" style="display: none;"></div>

                <div class="alert alert-success alert-dismissible" id="showSuccess" style="display: none;">
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <div id="successText"></div>
                </div>

                <div class="alert alert-danger alert-dismissible" id="showError" style="display: none;">
                    <h4><i class="fa fa-warning"></i> Alert!</h4>
                    <div id="errorTextOTP"></div>
                </div>

                <form role="form">
                    <div class="input-group" style="font-family: Verdana, Arial, sans-serif;">
                        <label for="newMobile">
                            Please enter new mobile number (Do not add +91)
                        </label>
                        <input class="form-control" type="text" autocomplete="Off" id="newMobile" placeholder="Please enter new mobile number">
                        <div class="input-group-btn" style="padding-top: 24px">
                            <button type="button" class="btn btn-danger" id="sendOTP">Send OTP</button>
                        </div>
                    </div>
                </form>

                <form role="form" id="verifyOTP" style="display: none;">
                    <div class="input-group" style="font-family: Verdana, Arial, sans-serif;">
                        <label for="newMobile">
                            Enter OTP you have received
                        </label>
                        <input class="form-control" type="text" autocomplete="Off" id="otp" placeholder="Please enter the otp you have received">
                        <div class="input-group-btn" style="padding-top: 24px">
                            <button type="button" class="btn btn-danger" id="verifyOTPbtn">Verify</button>
                        </div>
                    </div>
                </form>


            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="otp_modal_updateAddressITGK" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">
                    Verify your mobile number
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <div id="responseText_updateAddressITGK" style="display: none;"></div>

                <div class="alert alert-success alert-dismissible" id="showSuccess_updateAddressITGK" style="display: none;">
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <div id="successText_updateAddressITGK"></div>
                </div>

                <div class="alert alert-danger alert-dismissible" id="showError_updateAddressITGK" style="display: none;">
                    <h4><i class="fa fa-warning"></i> Alert!</h4>
                    <div id="errorTextOTP_updateAddressITGK"></div>
                </div>

                <form role="form">
                    <div class="input-group" style="font-family: Verdana, Arial, sans-serif;">
                        <label for="newMobile">
                            Please enter your mobile number we have(Do not add +91)
                        </label>
                        <input class="form-control" type="text" autocomplete="Off" id="newMobile_updateAddressITGK" placeholder="Please enter your mobile number">
                        <div class="input-group-btn" style="padding-top: 24px">
                            <button type="button" class="btn btn-danger" id="sendOTP_updateAddressITGK">Send OTP</button>
                        </div>
                    </div>
                </form>

                <form role="form" id="verifyOTP_updateAddressITGK" style="display: none;">
                    <div class="input-group" style="font-family: Verdana, Arial, sans-serif;">
                        <label for="newMobile">
                            Enter OTP you have received
                        </label>
                        <input class="form-control" type="text" autocomplete="Off" id="otp_updateAddressITGK" placeholder="Please enter the otp you have received">
                        <div class="input-group-btn" style="padding-top: 24px">
                            <button type="button" class="btn btn-danger" id="verifyOTPbtn_updateAddressITGK">Verify</button>
                        </div>
                    </div>
                </form>


            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.location.href='dashboard.php'">Take Me to Dashboard</button>
            </div>
        </div>
    </div>
</div>

<div id="errorModal_Custom_backdrop" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
                <p id="errorText_backdrop"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" id="revokeReference">Revoke this Reference Number</button>
                <button type="button" class="btn btn-default" onclick="window.location.href='dashboard.php'">Take Me to Dashboard</button>
            </div>
        </div>
    </div>
</div>

<div id="loading" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please wait...</h4>
            </div>
            <div class="modal-body">
                <p>Please wait while we are fetching the records</p>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                        Loading...
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="submitting" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please wait...</h4>
            </div>
            <div class="modal-body">
                <p>Please wait while we are submitting the values</p>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                        Submitting...
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="submitted" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-success">
                <h4 class="modal-title">Success</h4>
            </div>
            <div class="modal-body">
                <p>Successfully submitted, you will be notified via sms/email on update of this Request. - Thank you</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.location.href='frmprofileDetails.php'">Profile Dashboard</button>
            </div>
        </div>
    </div>
</div>

<div id="revoke_success" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please Wait...</h4>
            </div>
            <div class="modal-body">
                <p id="revokeText">Please wait while we are fetching the records</p>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                        Loading...
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="submitted_thanks" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-success">
                <h4 class="modal-title">Success</h4>
            </div>
            <div class="modal-body">
                <p>Successfully submitted - Thank you</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.history.back();">Go Back</button>
            </div>
        </div>
    </div>
</div>

<div id="submitted_rejected" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">Success</h4>
            </div>
            <div class="modal-body">
                <p>Request has been successfully rejected</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.history.back();">Go Back</button>
            </div>
        </div>
    </div>
</div>

<div id="submitted_revoked" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">Success</h4>
            </div>
            <div class="modal-body">
                <p>Request has been successfully revoked</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.history.back();">Go Back</button>
            </div>
        </div>
    </div>
</div>

<div id="unauthorized" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">Unauthorized Access</h4>
            </div>
            <div class="modal-body">
                <p>You are not authorized to access this page</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.location.href='dashboard.php'">Take Me to the Dashboard</button>
            </div>
        </div>
    </div>
</div>

<div id="tempered" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">Unauthorized Access</h4>
            </div>
            <div class="modal-body">
                <p>Unauthorized access to this page</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.location.href='dashboard.php'">Take Me to the Dashboard</button>
            </div>
        </div>
    </div>
</div>

<div id="view_document" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content model-lg">
            <div class="modal-header modal-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Uploaded Document</h4>
            </div>
            <div class="modal-body">
                <img id="view_image_large" class="img-responsive" alt="large image">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" id="download_document"><i class="fa fa-download" aria-hidden="true"></i>
                    &nbsp;&nbsp;Download this Document</button>
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
                    &nbsp;&nbsp;Close</button>
            </div>
        </div>
    </div>
</div>

<div id="tempered" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
                <p>Transaction ID has been tempered for this session, please try again</p>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-danger" id="setPath">
            </div>
        </div>
    </div>
</div>

<div id="editRequest" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-primary">
                <h4 class="modal-title">Previous Request Already Exist</h4>
            </div>
            <div class="modal-body">
                <p id="editRequest_backdrop"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" id="editRequest_button">Edit this Request</button>
                <button type="button" class="btn btn-default" onclick="window.location.href='frmprofileDetails.php'">Profile Dashboard</button>
            </div>
        </div>
    </div>
</div>

<div id="editRequest_block" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">Previous Request Already Exist</h4>
            </div>
            <div class="modal-body">
                <p id="editRequest_block_backdrop"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.location.href='frmprofileDetails.php'">Profile Dashboard</button>
            </div>
        </div>
    </div>
</div>

<div id="errorModal_Custom2" class="modal fade" data-backdrop="staic" role="dialog" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
                <p id="errorText2"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="window.location.href='dashboard.php'">Take Me to Dashboard</button>
            </div>
        </div>
    </div>
</div>

<div id="support_name" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">List of Supporting Documents for Proof of Identity</h4>
            </div>
            <div class="modal-body">
                <ol type="1">
                    <li>Passport</li>
                    <li>PAN Card</li>
                    <li>Ration/ PDS Photo Card</li>
                    <li>Voter ID</li>
                    <li>Driving License</li>
                    <li>Government Photo ID Cards/ service photo identity card issued by PSU</li>
                    <li>NREGS Job Card</li>
                    <li>Photo ID issued by Recognized Educational Institution</li>
                    <li>Arms License</li>
                    <li>Photo Bank ATM Card</li>
                    <li>Photo Credit Card</li>
                    <li>Pensioner Photo Card</li>
                    <li>Freedom Fighter Photo Card</li>
                    <li>Kissan Photo Passbook</li>
                    <li>CGHS / ECHS Photo Card</li>
                    <li>Address Card having Name and Photo issued by Department of Posts</li>
                    <li>Certificate of Identify having photo issued by Gazetted Officer or Tehsildar on letterhead</li>
                    <li>Disability ID Card/handicapped medical certificate issued by the respective State/UT Governments/Administrations</li>
                </ol>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
                    &nbsp;&nbsp;Close</button>
            </div>
        </div>
    </div>
</div>

<div id="support_address" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">List of Supporting Documents for Proof of Address</h4>
            </div>
            <div class="modal-body">
                <ol type="1">
                    <li>Passport</li>
                    <li>Bank Statement/ Passbook</li>
                    <li>Post Office Account Statement/Passbook</li>
                    <li>Ration Card</li>
                    <li>Voter ID</li>
                    <li>Driving License</li>
                    <li>Government Photo ID cards/ service photo identity card issued by PSU</li>
                    <li>Electricity Bill (not older than 3 months)</li>
                    <li>Water bill (not older than 3 months)</li>
                    <li>Telephone Landline Bill (not older than 3 months)</li>
                    <li>Property  Tax Receipt (not older than 1 year)</li>
                    <li>Credit Card Statement (not older than 3 months)</li>
                    <li>Insurance Policy</li>
                    <li>Signed  Letter  having  Photo from Bank on letterhead</li>
                    <li>Signed  Letter  having  Photo issued  by registered Company on letterhead</li>
                    <li>Signed  Letter  having  Photo  issued  by Recognized Educational Institutions on letterhead</li>
                    <li>NREGS Job Card</li>
                    <li>Arms License</li>
                    <li>Pensioner Card</li>
                    <li>Freedom Fighter Card</li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
                    &nbsp;&nbsp;Close</button>
            </div>
        </div>
    </div>
</div>

<div id="geo_tag_support" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-error">
                <h4 class="modal-title">About Geo-tagging</h4>
            </div>
            <div class="modal-body">


                <h3>What is Geo-tagged Photograph?</h3>
                <h5 style="text-align: justify">
                    A geo-tagged photograph is a photograph which is associated with a geographical location by geotagging. Usually this is done by assigning at least a latitude and longitude to the image, and optionally altitude, compass bearing and other fields may also be included.
                </h5>

                <h3>How Can I get Geo-tagged Photographs</h3>
                <h5>
                    <ul type="1">
                        <li>Turn on location settings or GPS on your device.</li>
                        <li>Take a clear photograph from your device.</li>
                        <li>Upload this photograph to MyRKCL Portal from this section.</li>
                        <li>If successfully updated, we will fetch the location details and update in our records.</li>
                    </ul>
                </h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
                    &nbsp;&nbsp;Close</button>
            </div>
        </div>
    </div>
</div>

<div id="view_document_ownershipDocument" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content model-lg">
            <div class="modal-header modal-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Uploaded Document</h4>
            </div>
            <div class="modal-body">
                <img id="view_image_large_ownershipDocument" class="img-responsive" alt="large image">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" id="download_document_own"><i class="fa fa-download" aria-hidden="true"></i>
                    &nbsp;&nbsp;Download this Document</button>
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
                    &nbsp;&nbsp;Close</button>
            </div>
        </div>
    </div>
</div>

<div id="view_document_utilityBill" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content model-lg">
            <div class="modal-header modal-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Uploaded Document</h4>
            </div>
            <div class="modal-body">
                <img id="view_image_large_utilityBill" class="img-responsive" alt="large image">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" id="download_document_utility"><i class="fa fa-download" aria-hidden="true"></i>
                    &nbsp;&nbsp;Download this Document</button>
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
                    &nbsp;&nbsp;Close</button>
            </div>
        </div>
    </div>
</div>

<div id="view_document_rentAgreement" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content model-lg">
            <div class="modal-header modal-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Uploaded Document</h4>
            </div>
            <div class="modal-body">
                <img id="view_image_large_rentAgreement" class="img-responsive" alt="large image">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" id="download_document_agree"><i class="fa fa-download" aria-hidden="true"></i>
                    &nbsp;&nbsp;Download this Document</button>
                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
                    &nbsp;&nbsp;Close</button>
            </div>
        </div>
    </div>
</div>