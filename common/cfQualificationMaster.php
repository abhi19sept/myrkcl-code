<?php

/* 
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsQualificationMaster.php';

$response = array();
$emp = new clsQualificationMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"])  && !empty($_POST["name"])) {
        $_QualificationName = $_POST["name"];
        $_Status=$_POST["status"];
        $response = $emp->Add($_QualificationName,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_QualificationName = $_POST["name"];
        $_Status=$_POST["status"];
        $_Code=$_POST["code"];
        $response = $emp->Update($_Code,$_QualificationName,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);
    //echo $_actionvalue;
    //print_r($response);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("QualificationCode" => $_Row['Qualification_Code'],
            "QualificationName" => $_Row['Qualification_Name'],
            "Status"=>$_Row['Qualification_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();
    print_r($response);
    $_DataTable = "";

    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'>Qualification</th>";
    echo "<th style='30%'>Status</th>";
    echo "<th style='20%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Qualification_Name'] . "</td>";
         echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmQualificationMaster.php?code=" . $_Row['Qualification_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmQualificationMaster.php?code=" . $_Row['Qualification_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Qualification_Code'] . ">" . $_Row['Qualification_Name'] . "</option>";
    }
}

// if ($_action == "FILLHigher") {
//     $forGraduateOnly = $_POST['forGraduateOnly'];
//     $response = $emp->GetAll();
//     echo "<option value='0' selected='selected'>Select</option>";
//     while ($_Row = mysqli_fetch_array($response[2])) {
//         if ($_Row['Qualification_Name'] == 'SSC' || $_Row['Qualification_Name'] == 'Below 10th' || $_Row['Qualification_Name'] == 'Other' || ($forGraduateOnly == 'true' && $_Row['Qualification_Name'] == 'HSC')) continue;
//             echo "<option value=" . $_Row['Qualification_Code'] . ">" . $_Row['Qualification_Name'] . "</option>";
//     }
// }

//As new marit requirement for Jan 2021 batch
if ($_action == "FILLHigher") {
    $forGraduateOnly = $_POST['forGraduateOnly'];
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        if ($_Row['Qualification_Name'] == 'Doctorate' || $_Row['Qualification_Name'] == 'HSC' || $_Row['Qualification_Name'] == 'Post Graduate' || $_Row['Qualification_Name'] == 'SSC' || $_Row['Qualification_Name'] == 'Below 10th' || $_Row['Qualification_Name'] == 'Other' || ($forGraduateOnly == 'true' && $_Row['Qualification_Name'] == 'HSC')) continue;
            echo "<option value=" . $_Row['Qualification_Code'] . ">" . $_Row['Qualification_Name'] . "</option>";
    }
}