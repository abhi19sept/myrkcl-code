<?php

/*
 * author Viveks

 */


include './commonFunction.php';
require 'BAL/clsSubmitRspDetails.php';

$response = array();
$emp = new clsSubmitRspDetails();

if ($_action == "GETREGRSP") {
    $response = $emp->GetRspDetails();
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {

        $_DataTable[$_i] = array("rspname" => $_Row['Organization_Name'],
            "mobno" => $_Row['User_MobileNo'],
            "date" => $_Row['Organization_FoundedDate'],
            "rsptype" => $_Row['Organization_Type']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "SubmitDetails") {
        $response = $emp->SubmitDetails();
        echo $response[0];
    
    
}

