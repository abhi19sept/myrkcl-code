<?php

/*
 *  author Mayank  
 */

require 'commonFunction.php';
require 'BAL/clsWcdFeedbackForm.php';

$response = array();
$emp = new clsWcdFeedbackForm();

if ($_action == "FillBatch") {
    $response = $emp->FILLBatchName();
	 $_Row=mysqli_fetch_array($response[2]);
	 $_DataTable = array();
	 $_i = 0;
	 
	 $_DataTable[$_i] = array("BatchId" => $_Row['Batch_Code'],
						"BatchName" => strtoupper($_Row['Batch_Name']));        
					$_i = $_i + 1;
				
				echo json_encode($_DataTable); 
		    
}

if ($_action == "FillDetails") {
    $response = $emp->GetDetails();
	 $_DataTable = array();
    $_i = 0;
	if($response[0]=='Success'){
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("lcode" => $_Row['Admission_LearnerCode'],
			"itgkcode"=>$_Row['Admission_ITGK_Code'],
            "lname"=>$_Row['Admission_Name'],
            "itgkname" => $_Row['Organization_Name']);
        $_i = $_i + 1;
		}
		echo json_encode($_Datatable);
	}    
}

if ($_action == "ADD") {
	//print_r($_POST);
	if (!empty($_POST["qt1"]) && !empty($_POST["qt2"]) && !empty($_POST["qt3"]) && !empty($_POST["qt4"]) && !empty($_POST["qps1"]) && !empty($_POST["qps2"]) && !empty($_POST["qps3"]) && !empty($_POST["qff1"]) && !empty($_POST["qff2"]) && !empty($_POST["qff3"]) && !empty($_POST["qo1"]) && !empty($_POST["qo2"]) && !empty($_POST["qe1"])) {
		if($_POST["qt1"]=='पूरा' || $_POST["qt1"]=='लगभग पूरा' || $_POST["qt1"]=='कम' || $_POST["qt1"]=='बहुत कम') {
			if($_POST["qt2"]=='अत्यधिक' || $_POST["qt2"]=='उचित' || $_POST["qt2"]=='कम' || $_POST["qt2"]=='बहुत कम') {
				if($_POST["qt3"]=='अत्यधिक' || $_POST["qt3"]=='उचित' || $_POST["qt3"]=='कम' || $_POST["qt3"]=='बहुत कम') {
					if($_POST["qt4"]=='सदैव' || $_POST["qt4"]=='अधिकतर' || $_POST["qt4"]=='कभी-कभी' || $_POST["qt4"]=='कभी नहीं'){
						if($_POST["qps1"]=='समस्त विषयों पर' || $_POST["qps1"]=='मुख्य विषयों पर' || $_POST["qps1"]=='कुछ विषयों पर' || $_POST["qps1"]=='कभी नहीं'){
							if($_POST["qps2"]=='सदैव' || $_POST["qps2"]=='अधिकतर' || $_POST["qps2"]=='कभी-कभी' || $_POST["qps2"]=='कभी नहीं'){
								if($_POST["qps3"]=='सदैव' || $_POST["qps3"]=='अधिकतर' || $_POST["qps3"]=='कभी-कभी' || $_POST["qps3"]=='नहीं'){
									if($_POST["qff1"]=='बहुत अच्छा' || $_POST["qff1"]=='अच्छा' || $_POST["qff1"]=='संतोषप्रद' || $_POST["qff1"]=='सामान्य'){
										if($_POST["qff2"]=='बहुत अच्छा' || $_POST["qff2"]=='अच्छा' || $_POST["qff2"]=='संतोषप्रद' || $_POST["qff2"]=='सामान्य'){
											if($_POST["qff3"]=='सदैव' || $_POST["qff3"]=='अधिकतर' || $_POST["qff3"]=='कभी-कभी' || $_POST["qff3"]=='कभी नहीं'){
												if($_POST["qo1"]=='सदैव' || $_POST["qo1"]=='अधिकतर' || $_POST["qo1"]=='कभी-कभी' || $_POST["qo1"]=='कभी नहीं'){
													if($_POST["qo2"]=='हा' || $_POST["qo2"]=='नहीं'){
														if($_POST["qe1"]=='अत्यधिक' || $_POST["qe1"]=='अधिक' || $_POST["qe1"]=='सामान्य' || $_POST["qe1"]=='कम'){
															
															$_LearnerName = trim($_POST["firstname"]);
															$_LearnerCode = trim($_POST["lcode"]);
															$_ITGKName = trim($_POST["itgkname"]);
															$_ITGKCode = trim($_POST["itgkcode"]);
															$_BatchCode = trim($_POST["bcode"]);
															
															$qt1 = trim($_POST["qt1"]);
															$qt2 = trim($_POST["qt2"]);
															$qt3 = trim($_POST["qt3"]);
															$qt4 = trim($_POST["qt4"]);
															
															$qps1 = trim($_POST["qps1"]);
															$qps2 = trim($_POST["qps2"]);
															$qps3 = trim($_POST["qps3"]);
															
															$qff1 = trim($_POST["qff1"]);
															$qff2 = trim($_POST["qff2"]);
															$qff3 = trim($_POST["qff3"]);
															
															$qo1 = trim($_POST["qo1"]);
															$qo2 = trim($_POST["qo2"]);
															$qe1 = trim($_POST["qe1"]);
															
															$response = $emp->Add($_LearnerName, $_LearnerCode, $_ITGKName, $_ITGKCode, $_BatchCode, $qt1, $qt2, $qt3, $qt4, $qps1, $qps2, $qps3, $qff1, $qff2, $qff3, $qo1, $qo2, $qe1);
															echo $response[0];
														}
														else {
															echo "1";
														}
													}
													else {
														echo "2";
													}
												}
												else {
													echo "3";
												}
											}
											else {
												echo "4";
											}
										}
										else {
											echo "5";
										}
									}
									else {
										echo "6";
									}
								}
								else {
									echo "7";
								}
							}
							else {
								echo "8";
							}
						}
						else {
							echo "9";
						}
					}
					else {
						echo "10";
					}
				}
				else {
					echo "11";
				}
			}
			else {
				echo "12";
			}
		}
		else {
			echo "13";
		}
	}
	else {
			echo "2";
		}
}





if ($_action == "ADD1") 
{
	//Print_r($_POST);
	//die;
	if (!empty($_POST["qt11"]) && !empty($_POST["qt12"]) && !empty($_POST["qt13"]) && !empty($_POST["qt14"]) && !empty($_POST["qt15"]) && !empty($_POST["qt16"])) {
		if($_POST["qt11"]=='Desktop' || $_POST["qt11"]=='Laptop' || $_POST["qt11"]=='Smartphone' || $_POST["qt11"]=='संशाधन उपलब्ध नहीं') {
			if($_POST["qt12"]=='Microsoft Team' || $_POST["qt12"]=='Zoom App' || $_POST["qt12"]=='Google' || $_POST["qt12"]=='Webex'  || $_POST["qt12"]=='WhatsApp' || $_POST["qt12"]=='Telegram' || $_POST["qt12"]=='कोई अन्य टूल') {
				if($_POST["qt13"]=='अत्यधिक' || $_POST["qt13"]=='उचित' || $_POST["qt13"]=='कम' || $_POST["qt13"]=='बहुत कम') {
					if($_POST["qt14"]=='बहुत अच्छा' || $_POST["qt14"]=='सामान्य' || $_POST["qt14"]=='संतोषप्रद' || $_POST["qt14"]=='असंतुष्ट' ){
						if($_POST["qt15"]=='हाँ' || $_POST["qt15"]=='नहीं' ){
							if($_POST["qt16"]=='अत्यधिक' || $_POST["qt16"]=='सामान्य' || $_POST["qt16"]=='कम' || $_POST["qt16"]=='बिल्कुल नहीं'){
								
															$_LearnerName = trim($_POST["firstname1"]);
															$_LearnerCode = trim($_POST["lcode1"]);
															$_ITGKName = trim($_POST["itgkname1"]);
															$_ITGKCode = trim($_POST["itgkcode1"]);
															$_BatchCode = trim($_POST["bcode1"]);
															
															$qt11 = trim($_POST["qt11"]);
															$qt12 = trim($_POST["qt12"]);
															$qt13 = trim($_POST["qt13"]);
															$qt14 = trim($_POST["qt14"]);
															$qt15 = trim($_POST["qt15"]);
															$qt16 = trim($_POST["qt16"]);
															
															
															
															$response = $emp->Add1($_LearnerName, $_LearnerCode, $_ITGKName, $_ITGKCode, $_BatchCode,$qt11,$qt12,$qt13,$qt14,$qt15,$qt16);
															echo $response[0];
										
									}
									else {
										echo "1";
									}
								}
								else {
									echo "2";
								}
							}
							else {
								echo "3";
							}
						}
						else {
							echo "4";
						}
					}
					else {
						echo "5";
					}
				}
				else {
					echo "6";
				}
			
	}
	else {
			echo "2";
		}
}