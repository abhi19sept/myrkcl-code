<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

include './commonFunction.php';
require 'BAL/clsFileUploadItgkRenewalData.php';

require '../DAL/upload_ftp_doc.php';

$response = array();
$emp = new clsFileUploadItgkRenewalData();

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

if(isset($_FILES['SelectedFile']['name'])) {

$ext = pathinfo($_FILES['SelectedFile']['name'], PATHINFO_EXTENSION);
//$ext="xls";
// Check if the file exists
if ($ext == "csv") {

$_UploadDirectory = '/ITGK_Renewal/';
if ($_FILES['SelectedFile']['error'] > 0) {
    outputJSON("<span class='error'>An error ocurred when uploading.</span>");
}


if ($_FILES['SelectedFile']['size'] > 20000000000000000000000000000000) {
    outputJSON("<span class='error'>" . $_FILES['SelectedFile']['size'] . "File uploaded exceeds maximum upload size.</span>");
}

ftpUploadFile($_UploadDirectory,$_POST['UploadId']. "." . $ext,$_FILES["SelectedFile"]["tmp_name"]);


}

}


if ($_action == "uploaditgkdata") {
    $_ObjFTPConnection = new ftpConnection();
   $abc = $_ObjFTPConnection->ftpdetails();
   $_fname= $_POST['filename'].  ".csv";
        $_UploadDirectory= $abc.'/ITGK_Renewal/'.$_fname;
            $csvData = file_get_contents($_UploadDirectory);
        $lines1 = array_filter(explode(PHP_EOL, $csvData));
       
        $lines = array_slice($lines1, 1);
        $valuestring = "";
        $j = 0;
        foreach ($lines as $key => $value) {
             $temp1 = explode(",", $value);
         $response = $emp->Additgkrenewaldata($temp1[0], $temp1[1], $_fname);
        }
	
		echo $response[0];
    
}