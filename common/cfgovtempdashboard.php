<?php
/* 
 * author Sunil Kumar Baindara
 */
include './commonFunction.php';
require 'BAL/clsgovtempdashboard.php';
//require 'include/siteconfig.php';

$response = array();
$emp = new clsGovtEmpDashboard();



if ($_action == "FillApproved_by_RKCL") {
    $response = $emp->GetApproved_by_RKCL();	
    $_Row = mysqli_fetch_array($response[2]);
    echo $_Row['countN'];    
}

if ($_action == "GETDetails") {
    $response = $emp->GetLearnerDetails($_POST['status']);
		$_DataTable = "";

        echo "<div class='table-responsive' style='margin-top:10px'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		
        echo "<thead>";
		
        echo "<tr>";
		
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Learner Code</th>";
			echo "<th style='10%'>ITGK-Code</th>";
			echo "<th style='10%'>Learner Name</th>";
			echo "<th style='8%'>Father / Husband Name</th>";
			echo "<th style='12%'>D.O.B</th>";   
			echo "<th style='12%'>Department</th>";   
			echo "<th style='12%'>Designation</th>";   
			echo "<th style='12%'>Marks</th>";   
			echo "<th style='12%'>Date</th>";   
			echo "</tr>";
		
		
			
			echo "</thead>";
			echo "<tbody>";
			$_Count = 1;
    
			if ($response[0] == 'Success') {
				while ($_Row = mysqli_fetch_array($response[2])) {
			
						echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $_Row['learnercode'] . "</td>";
						echo "<td>" . $_Row['Govemp_ITGK_Code'] . "</td>";
						echo "<td>" . strtoupper($_Row['fname']) . "</td>";
						echo "<td>" . strtoupper($_Row['faname']) . "</td>";
						echo "<td>" . $_Row['empdob'] . "</td>";
						echo "<td>" . strtoupper($_Row['gdname']) . "</td>";
						echo "<td>" . strtoupper($_Row['designation']) . "</td>";
						echo "<td>" . $_Row['exammarks'] . "</td>";
							$s = $_Row['applicationdate'];
							$dt = new DateTime($s);
							$date = $dt->format('Y-m-d');
							
						echo "<td>" . $date . "</td>";
            
						echo "</tr>";
						$_Count++;
										
        }
			echo "</tbody>";
            echo "</table>";
            echo "</div>";
    }    
    
}


if ($_action == "GETOfflineDetails") {
    $response = $emp->GetOfflineLearnerDetails($_POST['status']);
		$_DataTable = "";

        echo "<div class='table-responsive' style='margin-top:10px'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		
        echo "<thead>";
		
        echo "<tr>";
		
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Learner Code</th>";
			echo "<th style='10%'>ITGK-Code</th>";
			echo "<th style='10%'>Learner Name</th>";
			echo "<th style='8%'>Father / Husband Name</th>";
			echo "<th style='12%'>D.O.B</th>";   
			echo "<th style='12%'>Department</th>";   
			echo "<th style='12%'>Designation</th>";   
			echo "<th style='12%'>Marks</th>";   
			echo "<th style='12%'>Date</th>";   
			echo "</tr>";
		
		
			
			echo "</thead>";
			echo "<tbody>";
			$_Count = 1;
    
			if ($response[0] == 'Success') {
				while ($_Row = mysqli_fetch_array($response[2])) {
			
						echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $_Row['learnercode'] . "</td>";
						echo "<td>" . $_Row['Govemp_ITGK_Code'] . "</td>";
						echo "<td>" . strtoupper($_Row['fname']) . "</td>";
						echo "<td>" . strtoupper($_Row['faname']) . "</td>";
						echo "<td>" . $_Row['empdob'] . "</td>";
						echo "<td>" . strtoupper($_Row['gdname']) . "</td>";
						echo "<td>" . strtoupper($_Row['designation']) . "</td>";
						echo "<td>" . $_Row['exammarks'] . "</td>";
							$s = $_Row['applicationdate'];
							$dt = new DateTime($s);
							$date = $dt->format('Y-m-d');
							
						echo "<td>" . $date . "</td>";
            
						echo "</tr>";
						$_Count++;
										
        }
			echo "</tbody>";
            echo "</table>";
            echo "</div>";
    }    
    
}


if ($_action == "GETPaperlessDetails") {
    $response = $emp->GetpaperlessLearnerDetails($_POST['status']);
		$_DataTable = "";

        echo "<div class='table-responsive' style='margin-top:10px'>";
        echo "<table id='example1' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		
        echo "<thead>";
		
        echo "<tr>";
	
			if($_POST['status']==15){
				echo "<th style='10%'>S No.</th>";
				echo "<th style='10%'>DEO Login ID</th>";
				echo "<th style='10%'>UserName</th>";
				echo "<th style='10%'>Form Processed Count</th>";
			}
			else {
				echo "<th style='10%'>S No.</th>";
				echo "<th style='10%'>Learner Code</th>";
				echo "<th style='10%'>ITGK-Code</th>";
				echo "<th style='10%'>Learner Name</th>";
				echo "<th style='8%'>Father / Husband Name</th>";
				echo "<th style='12%'>D.O.B</th>";   
				echo "<th style='12%'>Department</th>";   
				echo "<th style='12%'>Designation</th>";   
				echo "<th style='12%'>Marks</th>";   
				echo "<th style='12%'>Date</th>";   
				echo "</tr>";
			}
			echo "</thead>";
			echo "<tbody>";
			$_Count = 1;    
	
			if ($response[0] == 'Success') {
				while ($_Row = mysqli_fetch_array($response[2])) {
					if($_POST['status']==15){
						echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . strtoupper($_Row['deo']) . "</td>";
							if($_Row['deo']=='deo1'){
								echo "<td>" . 'SANJAY AMERIA' . "</td>";
							}
							if($_Row['deo']=='deo2'){
								echo "<td>" . 'Yogesh Kumar' . "</td>";
							}
							if($_Row['deo']=='deo3'){
								echo "<td>" . 'Jatin Sharma' . "</td>";
							}
							if($_Row['deo']=='deo4'){
								echo "<td>" . 'Bhupendra Singh' . "</td>";
							}
							if($_Row['deo']=='deo5'){
								echo "<td>" . 'Pankaj Kumar' . "</td>";
							}	
							echo "<td> <button id='" . $_Row['deo'] . "' name='" . $_Row['deo'] . "' 
									class='deodetails btn btn-success' type='button'>" . $_Row['deocnt'] . "</button></td>";
						//echo "<td>" . $_Row['deocnt'] . "</td>";
						echo "</tr>";
						$_Count++;
					}
					else {
						echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $_Row['learnercode'] . "</td>";
						echo "<td>" . $_Row['Govemp_ITGK_Code'] . "</td>";
						echo "<td>" . strtoupper($_Row['fname']) . "</td>";
						echo "<td>" . strtoupper($_Row['faname']) . "</td>";
						echo "<td>" . $_Row['empdob'] . "</td>";
						echo "<td>" . strtoupper($_Row['gdname']) . "</td>";
						echo "<td>" . strtoupper($_Row['designation']) . "</td>";
						echo "<td>" . $_Row['exammarks'] . "</td>";
							$s = $_Row['applicationdate'];
							$dt = new DateTime($s);
							$date = $dt->format('Y-m-d');
							
						echo "<td>" . $date . "</td>";
						
						echo "</tr>";
						$_Count++;
					}		   
           
				}
					echo "</tbody>";
					echo "</table>";
					echo "</div>";
			}    
}

if ($_action == "GETDeoDetails") {
    $response = $emp->GETDeoDetails($_POST['status']);
		$_DataTable = "";

        echo "<div class='table-responsive' style='margin-top:10px'>";
        echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		
        echo "<thead>";
		
        echo "<tr>";
		
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Form Processed Date</th>";
			echo "<th style='10%'>Form Processed Count</th>";			   
			echo "</tr>";			
			echo "</thead>";
			echo "<tbody>";
			$_Total = 0;
			$_Count = 1;
    
			if ($response[0] == 'Success') {
				while ($_Row = mysqli_fetch_array($response[2])) {
			
						echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						
							$s = $_Row['applicationdate'];
							$dt = new DateTime($s);
							$date = $dt->format('Y-m-d');
							
						echo "<td>" . $date . "</td>";
						echo "<td><input type='button' name='".$_POST['status']."' id='" . $date . "'
						value='" . $_Row['deocnt'] . "' class='btn btn-primary learner_details'></td>";						
						echo "</tr>";
								$_Total = $_Total + $_Row['deocnt'];
								
						$_Count++;
										
        }
			echo "</tbody>";
			
			echo "<tfoot>";
						echo "<tr>";
						echo "<th >  </th>";
						echo "<th >Total Count </th>";
						echo "<th>";
						echo "$_Total";
						echo "</th>";						
						echo "</tr>";
						echo "</tfoot>";
						
            echo "</table>";
            echo "</div>";
    }    
    
}


if ($_action == "Fill_Pending_for_Processing") {
    $response = $emp->GetPending_for_Processing();
    if ($response[0] == 'Success') 
        {
			$_Row = mysqli_fetch_array($response[2]);
			echo $_Row['countN'];
        }
    else{
			echo "error";
        }    
}

if ($_action == "Fill_Rejected_by_RKCL") {
    $response = $emp->GetRejected_by_RKCL();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN'];
        }
    else{
            echo "error";
        }     
}

if ($_action == "Fill_Receipt_Not_Submited") {
    $response = $emp->GetReceipt_Not_Submited();	
    
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }
}

if ($_action == "Fill_Sent_to_DoIT") {
    $response = $emp->GetSent_to_DoIT();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }   
}

if ($_action == "Fill_Amount_Reimbursed") {
    $response = $emp->GetAmount_Reimbursed();	
   if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }   
}

if ($_action == "Fill_Amount_Bounced") {
    $response = $emp->GetAmount_Bounced();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Awaiting_Budget_Approval") {
    $response = $emp->GetAwaiting_Budget_Approval();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Pending_With_DoIT") {
    $response = $emp->GetPending_With_DoIT();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }   
}

if ($_action == "Fill_Rejected_by_DoIT") {
    $response = $emp->GetRejected_by_DoIT();	
   if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }   
}

if ($_action == "Fill_No_Status_Found") {
    $response = $emp->GetNo_Status_Found();   
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }   
}


//===========Paper Less Detail============//

if ($_action == "Fill_Online_Start_Process_Date") {
    $response = $emp->GetOnline_Start_Process_Date();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            $timestamp = strtotime($_Row['Govemp_Datetime']);
            echo $new_date_format = date('d M Y', $timestamp);
        }
    else{
            echo "error";
        } 
}

if ($_action == "Fill_Total_Applied_Paper_Less") {
    $response = $emp->GetTotal_Applied_Paper_Less();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_num_rows($response[2]);
            echo $_Row;
        }
    else{
            echo "error";
        }      
}

if ($_action == "Fill_Applied_From_Last_30_Days") {
    $d2 = date('Y-m-d 00:00:00', strtotime('-30 days'));
    $response = $emp->GetApplied_From_Last_30_Days($d2);	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN'];
        }
    else{
            echo "error";
        } 
}

if ($_action == "Fill_Today_Applied_Application") {
    $d2 = date('Y-m-d');
    $response = $emp->GetFill_Today_Applied_Application($d2);	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN'];
        }
    else{
            echo "error";
        }    
}


if ($_action == "Fill_Approved_by_RKCL_PL") {
    $response = $emp->GetApproved_by_RKCL_PL();	
    if ($response[0] == 'Success') 
        {
             $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Pending_for_Processing_PL") {
    $response = $emp->GetPending_for_Processing_PL();	
    if ($response[0] == 'Success') 
        {
           $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Rejected_by_RKCL_PL") {
    $response = $emp->GetRejected_by_RKCL_PL();	
    if ($response[0] == 'Success') 
        {
			$_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Receipt_Not_Submited_PL") {
    $response = $emp->GetReceipt_Not_Submited_PL();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }
}

if ($_action == "Fill_Sent_to_DoIT_PL") {
    $response = $emp->GetSent_to_DoIT_PL();	
    if ($response[0] == 'Success') 
        {
			$_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Amount_Reimbursed_PL") {
    $response = $emp->GetAmount_Reimbursed_PL();	
    if ($response[0] == 'Success') 
        {
			$_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Amount_Bounced_PL") {
    $response = $emp->GetAmount_Bounced_PL();
		 if ($response[0] == 'Success') 
        {
			$_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN'];
		}
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Awaiting_Budget_Approval_PL") {
    $response = $emp->GetAwaiting_Budget_Approval_PL();	
    if ($response[0] == 'Success') 
        {
			$_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Pending_With_DoIT_PL") {
    $response = $emp->GetPending_With_DoIT_PL();	
    if ($response[0] == 'Success') 
        {
			$_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Rejected_by_DoIT_PL") {
    $response = $emp->GetRejected_by_DoIT_PL();	
    if ($response[0] == 'Success') 
        {
			$_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_No_Status_Found_PL") {
    $response = $emp->GetNo_Status_Found_PL();	
    if ($response[0] == 'Success') 
        {
			$_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Process_deo_PL") {
    $d2 = date('Y-m-d 00:00:00', strtotime('-30 days'));
    $response = $emp->GetProcess_deo_Last_30_Days($d2);	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        } 
}

//===========Offline Less Detail============//

if ($_action == "FillApproved_by_RKCL_Offline") {
    $response = $emp->GetApproved_by_RKCL_Offline();	
    $_Row = mysqli_fetch_array($response[2]);
    echo $_Row['countN'];    
}


if ($_action == "Fill_Pending_for_Processing_Offline") {
    $response = $emp->GetPending_for_Processing_Offline();
    if ($response[0] == 'Success') 
        {
			$_Row = mysqli_fetch_array($response[2]);
			echo $_Row['countN'];
        }
    else{
			echo "error";
        }    
}

if ($_action == "Fill_Rejected_by_RKCL_Offline") {
    $response = $emp->GetRejected_by_RKCL_Offline();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN'];
        }
    else{
            echo "error";
        }     
}

if ($_action == "Fill_Receipt_Not_Submited_Offline") {
    $response = $emp->GetReceipt_Not_Submited_Offline();	
    
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }
}

if ($_action == "Fill_Sent_to_DoIT_Offline") {
    $response = $emp->GetSent_to_DoIT_Offline();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }   
}

if ($_action == "Fill_Amount_Reimbursed_Offline") {
    $response = $emp->GetAmount_Reimbursed_Offline();	
   if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }   
}

if ($_action == "Fill_Amount_Bounced_Offline") {
    $response = $emp->GetAmount_Bounced_Offline();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Awaiting_Budget_Approval_Offline") {
    $response = $emp->GetAwaiting_Budget_Approval_Offline();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }  
}

if ($_action == "Fill_Pending_With_DoIT_Offline") {
    $response = $emp->GetPending_With_DoIT_Offline();	
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }   
}

if ($_action == "Fill_Rejected_by_DoIT_Offline") {
    $response = $emp->GetRejected_by_DoIT_Offline();	
   if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }   
}

if ($_action == "Fill_No_Status_Found_Offline") {
    $response = $emp->GetNo_Status_Found_Offline();   
    if ($response[0] == 'Success') 
        {
            $_Row = mysqli_fetch_array($response[2]);
            echo $_Row['countN']; 
        }
    else{
            echo "error";
        }   
}

if ($_action == "GETLearnerDeoDetails") {
    $response = $emp->GETLearnerDeoDetails($_POST['ldate'],$_POST['deologin']);
		$_DataTable = "";

        echo "<div class='table-responsive' style='margin-top:10px'>";
        echo "<table id='examplelearner' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		
        echo "<thead>";
		
        echo "<tr>";
		
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Learner Code</th>";
			echo "<th style='10%'>ITGK-Code</th>";
			echo "<th style='10%'>Learner Name</th>";
			echo "<th style='8%'>Father / Husband Name</th>";
			echo "<th style='12%'>D.O.B</th>";   
			echo "<th style='12%'>Department</th>";   
			echo "<th style='12%'>Process Status</th>";   
			echo "<th style='12%'>Marks</th>";   
			echo "<th style='12%'>Date</th>";   
			echo "</tr>";
		
		
			
			echo "</thead>";
			echo "<tbody>";
			$_Count = 1;
    
			if ($response[0] == 'Success') {
				while ($_Row = mysqli_fetch_array($response[2])) {
			
						echo "<tr>";
						echo "<td>" . $_Count . "</td>";
						echo "<td>" . $_Row['learnercode'] . "</td>";
						echo "<td>" . $_Row['Govemp_ITGK_Code'] . "</td>";
						echo "<td>" . strtoupper($_Row['fname']) . "</td>";
						echo "<td>" . strtoupper($_Row['faname']) . "</td>";
						echo "<td>" . $_Row['empdob'] . "</td>";
						echo "<td>" . strtoupper($_Row['gdname']) . "</td>";
							if($_Row['Govemp_Process_Status']=='0'){
								$status='Approved by RKCL';
							}
							else {
								$status='Rejected by RKCL';
							}
						echo "<td>" . $status . "</td>";
						echo "<td>" . $_Row['exammarks'] . "</td>";
							$s = $_Row['applicationdate'];
							$dt = new DateTime($s);
							$date = $dt->format('Y-m-d');
							
						echo "<td>" . $date . "</td>";
            
						echo "</tr>";
						$_Count++;
										
        }
			echo "</tbody>";
            echo "</table>";
            echo "</div>";
    }    
    
}