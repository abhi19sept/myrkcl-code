<?php

/* 
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsreqsms.php';

$response = array();
$arrM = array();
$emp = new clsreqsms();


if ($_action == "ADD") 
{
	
     if (isset($_POST["ddPackage"])) 
	 {
		 $_package = $_POST["ddPackage"];
		 $_price = $_POST["txtprice"]; 
         $response = $emp->ADD($_package,$_price);
		 echo $response[0];
				
		  													
    }
}



if ($_action == "FILL") 
{
	$_ITGK_Code =   $_SESSION['User_LoginId'];
    $response = $emp->GetAll($_ITGK_Code);
    $_Row = mysqli_fetch_row($response[2]);
    $tot=100;
	$arrM[] =$_Row[0];
	$let =$tot-$_Row[0];
	//print_r($arrM);
	$arr = array ('response'=>$_Row[0],'comment'=>$let);
	echo json_encode($arr);
	
}



if ($_action == "FILLPACKAGE") {
    $response = $emp->Getpackage();
    echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['package'] . ">" . $_Row['package'] . "@" . $_Row['lable'] . "@".$_Row['Price']."</option>";
    }
}

if ($_action == "FILLPRICE") {
    $response = $emp->Getprice($_POST['values']);
  
    $_Row = mysqli_fetch_array($response[2]);
        echo $_Row['Price'];
    
}

if ($_action == "FillPkgPrice") {
    $response = $emp->GetPkgPrice();
	if($response[0] == 'Success') {
		$_Row = mysqli_fetch_array($response[2]);
        echo $_Row['price'];
	}
	  else {
		  echo "0";
	  }
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='10%'>Package</th>";
    echo "<th style='20%'>Price</th>";
    echo "<th style='20%'>Center Code</th>";
	echo "<th style='20%'>Action</th>";
   
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
       
        // echo "<td>" . $_Row['Staff_User'] . "</td>";
         echo "<td>" . $_Row['package'] . "</td>";
         echo "<td>" . $_Row['price'] . "</td>";
         echo "<td>" . $_Row['centercode'] . "</td>";
         if ($_Row['Payment_Status'] == '0') 
			{
                echo "<td><input type='checkbox' id=chk" . $_Row['id'] .
                " name=chk" . $_Row['id'] . "></input></td>";
            }
		else{
			echo "<td><input type='button' name='button' id='button' value='Payment Confirmed'></input></td>";
		}
     
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}


if ($_action == "ADDPkgDetails") {
   
    //$amount = $_POST["txtpkgprice"];  
	
	$_UserPermissionArray = array();
    $_smspackageid = array();
    $_i = 0;
    $_Count = 0;
    $l = "";
	$total=0;
    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 3) == 'chk') {
            $_UserPermissionArray[$_Count] = array(
                "Function" => substr($key, 3),
                "Attribute" => 3,
                "Status" => 1);
            $_Count++;

            $l .= substr($key, 3) . ",";
           
        }
        $_smspackageid = rtrim($l, ",");
       
        $count = count($_UserPermissionArray);		       
    }
	$responses = $emp->GetSMSPkgAmt($_smspackageid);
	  if($responses[0] == 'Success') {
		  while($_Row2 = mysqli_fetch_array($responses[2])){
			$total = $total + $_Row2['price'];
		} 	
	  }
		
    if ($total == '0') {
        echo "0";
    } else {
        $response = $emp->AddPayTran($total, $_POST['txtGenerateId'], $_POST['txtGeneratePayUId'],$_smspackageid);
        echo $total;
    }
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
	//print_r($response);
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
	//values
	$key = "gtKFFx";
	$salt = "eCwWELxi";
	$command1 = "check_isDomestic";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	//$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
	//$var3 = "500";//  Amount to be used in case of refund

	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    $wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    //$wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);	 
	$_DataTable = array();
    $_i = 0;
		$_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
				echo json_encode($_DataTable);		
 }

 if ($_action == "FinalValidateCard") {
	//values
	$key = "gtKFFx";
	$salt = "eCwWELxi";
	$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	//$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
	//$var3 = "500";//  Amount to be used in case of refund

	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    $wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    //$wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);	
	if($response == 'Invalid'){
		echo "1";
	}
	else if($response == 'valid'){
		echo "2";
	}
 }
 
function curlCall($wsUrl, $qs, $true){
   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
		if (curl_errno($c)) {
		  $c_error = curl_error($c);
		  if (empty($c_error)) {
			  $c_error = 'Some server error';
			}
			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);
		return $arr;
}
?>