<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsexamdetails.php';

$response = array();
$emp = new clsexamdetails();

if ($_action == "FILL") {
    $response = ($_SESSION['User_UserRoll'] == 1) ? $emp->FillExamEvent()  : $emp->FillEvent();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['eventid'] . ">" . $_Row['eventname'] . "</option>";
    }
}

if ($_action == "schedule") {
    $response = $emp->getschedule($_POST['id']);
    $_DataTable = "";
    if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '2' || $_SESSION['User_UserRoll'] == '3' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8' || $_SESSION['User_UserRoll'] == '9' || $_SESSION['User_UserRoll'] == '10' || $_SESSION['User_UserRoll'] == '11') {
        if ($_SESSION['User_UserRoll'] == '1') {
            echo "<div class='text-right'><p><span id='syncingScore'><label for='syncscores'>Sync Scores:</label><select id='syncscores' name='syncscores'>
                    <option value='' selected='selected'>Select </option>
                    <option value='freshbatch'>Fresh Batches</option>
                    <option value='reexam'>Reexam Learners</option>
                    <option value='previous'>Previous Batches</option>
                            </select></span> | <span id='syncingRecords'><a href='javascript:void(0)' id='syncrecords'><b>Sync Eligible Learner Records</b></a></span> | <span id='liveExamSchedule'><a href='javascript:void(0)' id='liveschedule'><b>Make this exam schudule live</b></a></span></p></div>";
        }

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>SNo.</th>";
        echo "<th nowrap>Remark</th>";
        echo "<th nowrap>Status</th>";
        echo "<th nowrap>Count</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td nowrap>" . $_Row['remark'] . "</td>";
            echo "<td>" . $_Row['status'] . "</td>";
            echo "<td nowrap>" . $_Row['n'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
    } else {
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>SNo.</th>";
        echo "<th nowrap>Exam Event Name</th>";
        echo "<th>ExamDate</th>";
        echo "<th nowrap>Preffered Exam District By ITGK</th>";
        echo "<th nowrap>Preffered Exam Tehsil By ITGK</th>";
        echo "<th>Status</th>";
        echo "<th>Reason</th>";
        echo "<th nowrap>Learner Code</th>";
        echo "<th nowrap>Learner Name</th>";
        echo "<th>Remark</th>";
        echo "<th>Course Name</th>";
        echo "<th>Batch Name</th>";
        echo "<th nowrap>Father Name</th>";
        echo "<th>Dob</th>";
        echo "<th>ITGK Code</th>";
        echo "<th>ITGK Name</th>";
        echo "<th>ITGK District</th>";
        echo "<th>ITGK Tehsil</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td nowrap>" . ucwords($_Row['eventname']) . "</td>";
            echo "<td>" . $_Row['examdate'] . "</td>";
            echo "<td nowrap>" . ucwords($_Row['District_Name']) . "</td>";
            echo "<td nowrap>" . ucwords($_Row['Tehsil_Name']) . "</td>";
            echo "<td nowrap>" . $_Row['status'] . "</td>";
            echo "<td>" . $_Row['Reason'] . "</td>";
            echo "<td>" . $_Row['learnercode'] . "</td>";
            echo "<td nowrap>" . ucwords($_Row['learnername']) . "</td>";
            echo "<td>" . $_Row['remark'] . "</td>";
            echo "<td nowrap>" . ucwords($_Row['coursename']) . "</td>";
            echo "<td nowrap>" . ucwords($_Row['batchname']) . "</td>";
            echo "<td nowrap>" . ucwords($_Row['fathername']) . "</td>";
            echo "<td>" . $_Row['dob'] . "</td>";
            echo "<td>" . $_Row['itgkcode'] . "</td>";
            echo "<td nowrap>" . ucwords($_Row['itgkname']) . "</td>";
            echo "<td>" . $_Row['itgkdistrict'] . "</td>";
            echo "<td>" . $_Row['itgktehsil'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "syncEligibleLearners") {
    echo $emp->syncRecordsForEligibleLearners($_REQUEST['examid']);

    exit;
}

if ($_action == "liveschedule") {
    echo $emp->liveSelectedExamSchedule($_REQUEST['examid']);
    exit;
}


if ($_action == "syncScores") {
    $emp->syncLearnerScores($_REQUEST['examid']);
    print 'Scores Synced!!';
    exit;
}

if ($_action == "FILLBatch") {
    $response = $emp->GetAllBatch();
    echo "<option value=''>Select Batch</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "syncBatchScores") {
    if ($_REQUEST['batch']) {
        $emp->syncBatchScores($_REQUEST['batch']);
        print 'Scores Synced!!';
    } else {
        echo "Please select batch to sync.";
    }
    exit;
}
?>