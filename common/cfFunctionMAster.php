<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsFunctionMaster.php';

$response = array();
$emp = new clsFunctionMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"])) {
        $_FunctionName = $_POST["name"];
        $_FunctionURL = $_POST["URL"];
        $_Parent=$_POST["parent"];
        $_Status=$_POST["status"];
        $_DisplayOrder=$_POST['display'];
        $response = $emp->Add($_FunctionName, $_FunctionURL,$_Parent,$_Status,$_DisplayOrder);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_FunctionName = $_POST["name"];
        $_FunctionURL = $_POST["URL"];
        $_Parent=$_POST["parent"];
        $_Status=$_POST["status"];
        $_Code=$_POST['code'];
        $_DisplayOrder=$_POST['display'];
        $response = $emp->Update($_Code,$_FunctionName, $_FunctionURL,$_Parent,$_Status,$_DisplayOrder);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("FunctionCode" => $_Row['Function_Code'],
            "FunctionName" => $_Row['Function_Name'],
            "Parent" => $_Row['Function_Parent'],
            "URL"=>$_Row['Function_URL'],
            "Status"=>$_Row['Function_Status'],
            "Display"=>$_Row['Function_Display'],
            "Root"=>$_Row['Root_Menu_Code']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>Name</th>";
    echo "<th style='20%'>URL</th>";
    echo "<th style='20%'>Parent</th>";
     echo "<th style='20%'>Root</th>";
    echo "<th style='5%'>Display</th>";
    echo "<th style='5%'>Status</th>";
    echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Function_Name'] . "</td>";
        echo "<td>" . $_Row['Function_URL'] . "</td>";
        echo "<td>" . $_Row['Parent_Function_Name'] . "</td>";
        echo "<td>" . $_Row['Root_Menu_Name'] . "</td>";
        echo "<td>" . $_Row['Function_Display'] . "</td>";
        echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmfunctionmaster.php?code=" . $_Row['Function_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmfunctionmaster.php?code=" . $_Row['Function_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Status</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Function_Code'] . ">" . $_Row['Function_Name'] . "</option>";
    }
}
?>
