<?php

/* 
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsLearnerTypeMaster.php';

$response = array();
$emp = new clsLearnerTypeMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"])) {
        $_LearnerTypeName = $_POST["name"];
        $_Status=$_POST["status"];

        $response = $emp->Add($_LearnerTypeName,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_LearnerTypeName = $_POST["name"];
        $_Status=$_POST["status"];
        $_Code=$_POST['code'];
        $response = $emp->Update($_Code,$_LearnerTypeName,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("LearnerTypeCode" => $_Row['LearnerType_Code'],
            "LearnerTypeName" => $_Row['LearnerType_Name'],
            "Status"=>$_Row['LearnerType_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<table border='0' cellpedding='0' cellspacing='0' width='100%' class='gridview'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Learner Type Name</th>";
    echo "<th style='40%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['LearnerType_Name'] . "</td>";
         echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmLearnerTypeMaster.php?code=" . $_Row['LearnerType_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmLearnerTypeMaster.php?code=" . $_Row['LearnerType_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll($_POST['coursecode']);
    echo "<option value=''>Select LearnerType Name</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['LearnerType_Code'] . ">" . $_Row['LearnerType_Name'] . "</option>";
    }
}