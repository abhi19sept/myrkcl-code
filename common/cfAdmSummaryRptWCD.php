<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsAdmSummaryRptWCD.php';

$response = array();
$emp = new clsAdmSummaryRptWCD();

/* For Get Course */

if ($_action == "FILLCOURSE") 
{
    $response = $emp->Getcourse($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}


/* For Get Batch */

if ($_action == "FILLBATCH") 
{
    $response = $emp->Getbatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "GETDATA") {
    $batchcode = $_POST['batchcode'];
    $response = $emp->GetDatafirst($batchcode);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th >IT-GK Name </th>";
    echo "<th >IT-GK District</th>";
    echo "<th >Reported SC Learners Count</th>";
    echo "<th >Reported ST Learners Count</th>";
    echo "<th >Reported Other Learners Count</th>";
    echo "<th >Total Reported Learners Count</th>";



    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            $total = ($_Row['SCcount'] + $_Row['STcount'] + $_Row['Othcount']);
            echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";
            echo "<td>" . $_Row['ITGK_Name'] . " </td>";
            echo "<td>" . $_Row['District_Name'] . " </td>";
            echo "<td>" . $_Row['SCcount'] . " </td>";
            echo "<td>" . $_Row['STcount'] . " </td>";
            echo "<td>" . $_Row['Othcount'] . "</td>";
            echo "<td>" . $total . "</td>";


            echo "</tr>";

            $_Count++;
        }

        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETDATAPO") {
    $batchcode = $_POST['batchcode'];
    $response = $emp->GetDataPO($batchcode);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th >IT-GK Name </th>";
    echo "<th >IT-GK District</th>";
    echo "<th >Reported SC Learners Count</th>";
    echo "<th >Reported ST Learners Count</th>";
    echo "<th >Reported Other Learners Count</th>";
    echo "<th >Total Reported Learners Count</th>";



    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            $total = ($_Row['SCcount'] + $_Row['STcount'] + $_Row['Othcount']);
            echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";
            echo "<td>" . $_Row['ITGK_Name'] . " </td>";
            echo "<td>" . $_Row['District_Name'] . " </td>";
            echo "<td>" . $_Row['SCcount'] . " </td>";
            echo "<td>" . $_Row['STcount'] . " </td>";
            echo "<td>" . $_Row['Othcount'] . "</td>";
            echo "<td>" . $total . "</td>";


            echo "</tr>";

            $_Count++;
        }

        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
