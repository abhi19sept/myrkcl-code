<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsNCRCenterVisit.php';
require '../DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();

$response = array();
$emp = new clsNCRCenterVisit();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>AO Code</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='10%'>IT-GK Type</th>";
    echo "<th style='40%'>IT-GK Mob No</th>";
    echo "<th style='40%'>IT-GK Email Id</th>";
    echo "<th style='40%'>IT-GK Address</th>";

    echo "<th style='40%'>IT-GK District</th>";
    echo "<th style='10%'>IT-GK Tehsil</th>";
    echo "<th style='10%'>Upload Visit Photos</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['User_Ack'] . "</td>";
            echo "<td>" . $_Row['User_LoginId'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Type'] . "</td>";
            echo "<td>" . $_Row['User_MobileNo'] . "</td>";
            echo "<td>" . $_Row['User_EmailId'] . "</td>";
            echo "<td>" . $_Row['Organization_Address'] . "</td>";
            echo "<td>" . $_Row['Organization_District_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";

            if ($_Row['User_UserRoll'] == '15' && $_SESSION['User_UserRoll'] == 14) {
                echo "<td> <a href='frmuploadncrvisitphoto.php?code=" . $_Row['User_LoginId'] . "&Mode=Add&ack=" . $_Row['User_Ack'] . "'>"
                . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='Upload Photos'/></a>"
                . "</td>";
            } elseif ($_Row['User_UserRoll'] == '7') {
                echo "<td>" . 'Approved' . "</td>";
            } else {
                echo "<td>" . 'NA' . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "ADD") {
//    print_r(($_POST));
//    die;
    global $_ObjFTPConnection;
    if (isset($_POST["txtGenerateId"]) && !empty($_POST["txtGenerateId"])) {
        $_GeneratedId = $_POST["txtGenerateId"];
        $_CenterCode = $_POST["txtCenterCode"];
        $_UploadDirectory = "/NCRVISIT";
        
        $panimg = $_FILES['uploadImage7']['name'];
        $pantmp = $_FILES['uploadImage7']['tmp_name'];
        $pantemp = explode(".", $panimg);
        //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
        $PANfilename = $_GeneratedId . '_theoryroom.' . end($pantemp);
        $panfilestorepath = "../upload/NCRVISIT/" . $PANfilename;
        $panimageFileType = pathinfo($panfilestorepath, PATHINFO_EXTENSION);
        $TheoryRoom_response_ftp=ftpUploadFile($_UploadDirectory,$PANfilename,$pantmp);

        $uidimg = $_FILES['uploadImage8']['name'];
        $uidtmp = $_FILES['uploadImage8']['tmp_name'];
        $uidtemp = explode(".", $uidimg);
        $UIDfilename = $_GeneratedId . '_reception.' . end($pantemp);
        $uidfilestorepath = "../upload/NCRVISIT/" . $UIDfilename;
        $uidimageFileType1 = pathinfo($uidfilestorepath, PATHINFO_EXTENSION);
        $Reception_response_ftp=ftpUploadFile($_UploadDirectory,$UIDfilename,$uidtmp);

        $addproofimg = $_FILES['uploadImage9']['name'];
        $addprooftmp = $_FILES['uploadImage9']['tmp_name'];
        $addprooftemp = explode(".", $addproofimg);
        $addprooffilename = $_GeneratedId . '_exterior.' . end($pantemp);
        $addprooffilestorepath = "../upload/NCRVISIT/" . $addprooffilename;
        $addproofimageFileType = pathinfo($addprooffilestorepath, PATHINFO_EXTENSION);
        $Ext_response_ftp=ftpUploadFile($_UploadDirectory,$addprooffilename,$addprooftmp);

        $appformimg = $_FILES['uploadImage6']['name'];
        $appformtmp = $_FILES['uploadImage6']['tmp_name'];
        $appformtemp = explode(".", $appformimg);
        $appformfilename = $_GeneratedId . '_other.' . end($pantemp);
        $appformfilestorepath = "../upload/NCRVISIT/" . $appformfilename;
        $appformimageFileType = pathinfo($appformfilestorepath, PATHINFO_EXTENSION);
        $Other_response_ftp=ftpUploadFile($_UploadDirectory,$appformfilename,$appformtmp);


        if (($_FILES["uploadImage7"]["size"] < 200000 && $_FILES["uploadImage7"]["size"] > 100000) || ($_FILES["uploadImage8"]["size"] < 200000 && $_FILES["uploadImage8"]["size"] > 100000) || ($_FILES["uploadImage9"]["size"] < 200000 && $_FILES["uploadImage9"]["size"] > 100000) || ($_FILES["uploadImage6"]["size"] < 200000 && $_FILES["uploadImage6"]["size"] > 100000)) {

            if ($panimageFileType == "jpg" && $uidimageFileType1 == "jpg" && $addproofimageFileType == "jpg" && $appformimageFileType == "jpg") {

                //if (move_uploaded_file($pantmp, $panfilestorepath) && move_uploaded_file($uidtmp, $uidfilestorepath) && move_uploaded_file($addprooftmp, $addprooffilestorepath) && move_uploaded_file($appformtmp, $appformfilestorepath)) {
                  if(trim($TheoryRoom_response_ftp) == "SuccessfullyUploaded" && trim($Reception_response_ftp) == "SuccessfullyUploaded" && trim($Ext_response_ftp) == "SuccessfullyUploaded" && trim($Other_response_ftp) == "SuccessfullyUploaded"){  
                    $response = $emp->Add($_CenterCode, $PANfilename, $UIDfilename, $addprooffilename, $appformfilename);

                    echo $response[0];
                } else {
                    echo "Visit Photos Not Uploaded. Please try again.";
                }
            } else {
                echo "Sorry, File Not Valid";
            }
        } else {
            echo "File Size should be between 100KB to 200KB.";
        }
    } else {
        echo "Inavalid Entry15";
    }
}