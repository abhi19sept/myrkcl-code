<?php

/* 
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clseditorgdetails.php';

$response = array();
$emp = new clseditorgdetails();


if ($_action == "EDIT") {
$response = $emp->GetDatabyCode($_actionvalue);
    //echo $_actionvalue;
   // print_r($response);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("Organization_Code" => $_Row['Organization_Code'],
            "Organization_Name" => $_Row['Organization_Name'],
			"Organization_RegistrationNo" => $_Row['Organization_RegistrationNo'],
			"Organization_FoundedDate" => $_Row['Organization_FoundedDate'],
			"Organization_Type" => $_Row['Organization_Type'],
			
            "Organization_Landmark"=>$_Row['Organization_Landmark'],
			"Organization_Road" => $_Row['Organization_Road'],
            "Organization_Street"=>$_Row['Organization_Street'],
			"Organization_HouseNo" => $_Row['Organization_HouseNo'],
			"Organization_DocType" => $_Row['Organization_DocType'],
            
			"Organization_Address" => $_Row['Organization_Address']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}






if ($_action == "UPDATE") {
	//print_r($_POST);
    if (isset($_POST["txtName1"]) ) {
        
		 $_Code=$_POST["code"];
		 $_OrgName = $_POST["txtName1"];
		 if (isset($_Code) && !empty($_Code)) {
        $response = $emp->UPDATE($_Code,$_OrgName);
        echo $response[0];
		} else {
                                   echo "Please Select Record";
                                                                                    }
    }
}




if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

   echo "<div class='table-responsive' style='margin-top:10px'>";
   
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
   echo "<th style='35%'>Center Code</th>";
	 echo "<th style='35%'>Organization Name</th>";
	 echo "<th style='35%'>Organization Registration No</th>";
	 echo "<th style='35%'>Organization Founded Date</th>";
	 //echo "<th style='35%'>Organization Country</th>";
	 echo "<th style='35%'>Organization Road</th>";
	 echo "<th style='35%'>Organization Street</th>";
	 echo "<th style='35%'>Organization HouseNo</th>";
	 
	 echo "<th style='35%'>Organization Addresss</th>";
	 echo "<th style='35%'>Organization ScanDoc</th>";
	 
    
    echo "<th style='20%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
          echo "<td>" . $_Row['User_LoginId'] . "</td>";
         echo "<td>" . $_Row['Organization_Name'] . "</td>";
		 echo "<td>" . $_Row['Organization_RegistrationNo'] . "</td>";
		 echo "<td>" . $_Row['Organization_FoundedDate'] . "</td>";
		// echo "<td>" . $_Row['Country_Name'] . "</td>";
		 echo "<td>" . $_Row['Organization_Road'] . "</td>";
		   echo "<td>" . $_Row['Organization_Street'] . "</td>";
		    echo "<td>" . $_Row['Organization_HouseNo'] . "</td>";
		
		  echo "<td>" . $_Row['Organization_Address'] . "</td>";
		 
		 
		 echo "<td>";
		 echo '<a href="upload/orgdetails_birth/'.$_Row['Organization_ScanDoc'].'" target="_blank"><img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/orgdetails_birth/'.$_Row['Organization_ScanDoc'].'"/></a>';
		 echo  "</td>";
        echo "<td><a href='frmeditorgdetails.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a></td>  ";

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}





if ($_action == "SHOWALL") {


        $response = $emp->GetAll();

        $_DataTable = "";
		
		$_Row = mysqli_fetch_array($response[2]);
		
                
               
                echo "<div class='col-md-12 col-lg-12'>"; 

        echo "<table class='table table-user-information'>";
        echo "<tbody>";
		
        echo "<tr>";
        echo "<td width='50%'>Organization_Name :</td>";
        
		
		echo "<td width='50%'>". $_Row['Organization_Name'] . "</td>";
		echo "</tr>";
		
		
		
		 echo "<tr>";
        echo "<td style='5%'>Organization RegistrationNo :	</td>";
        
		
		echo "<td style='30%'>". $_Row['Organization_RegistrationNo'] . "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td style='5%'>Organization FoundedDate :</td>";
		echo "<td style='5%'>". $_Row['Organization_FoundedDate'] ."</td>";
		echo "</tr>";
	
				echo "<tr>";
	echo "<td style='5%'> 	Organization Type :</td>";
        echo "<td style='10%'>" . $_Row['Org_Type_Name'] . "</td>";
        echo "</tr>";
		
		
			echo "<tr>";
	echo "<td style='5%'> 	Organization State :</td>";
        echo "<td style='10%'>" . $_Row['State_Name'] . "</td>";
        echo "</tr>";
		
	

echo "<tr>";
	echo "<td style='5%'>Organization Region :</td>";
        echo "<td style='10%'>" . $_Row['Region_Name'] . "</td>";
        echo "</tr>";		
		
		
		
		
echo "<tr>";
	echo "<td style='5%'>Organization District :</td>";
        echo "<td style='10%'>" . $_Row['District_Name'] . "</td>";
        echo "</tr>";	
		
		
				
echo "<tr>";
	echo "<td style='5%'>Organization Tehsil :</td>";
        echo "<td style='10%'>" . $_Row['Tehsil_Name'] . "</td>";
        echo "</tr>";	
		
		
		
						
echo "<tr>";
	echo "<td style='5%'> 	Organization Area :</td>";
        echo "<td style='10%'>" . $_Row['Area_Name'] . "</td>";
        echo "</tr>";



						
echo "<tr>";
	echo "<td style='5%'> 	Organization Landmark</td>";
        echo "<td style='10%'>" . $_Row['Organization_Landmark'] . "</td>";
        echo "</tr>";


echo "<tr>";
	echo "<td style='5%'> 	Organization Road :</td>";
        echo "<td style='10%'>" . $_Row['Organization_Road'] . "</td>";
        echo "</tr>";	



echo "<tr>";
	echo "<td style='5%'> 	Organization Street :</td>";
        echo "<td style='10%'>" . $_Row['Organization_Street'] . "</td>";
        echo "</tr>";	




echo "<tr>";
	echo "<td style='5%'> 	Organization HouseNo :</td>";
        echo "<td style='10%'>" . $_Row['Organization_HouseNo'] . "</td>";
        echo "</tr>";


echo "<tr>";
	echo "<td style='5%'> 	Organization Country :</td>";
        echo "<td style='10%'>" . $_Row['Country_Name'] . "</td>";
        echo "</tr>";	


echo "<tr>";
	echo "<td style='5%'> Organization Address :</td>";
        echo "<td style='10%'>" . $_Row['Organization_Address'] . "</td>";
        echo "</tr>";		
        echo "</tbody>";
       
       
        echo "</table>";
		echo "<a class='btn btn-primary' href='frmorgdetail.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit'   style='margin-left: 200px !important;'  >Edit My Profile</a>";
         
		 echo "</div>";
		 		 
		
		
//echo $_DataTable;
    }






