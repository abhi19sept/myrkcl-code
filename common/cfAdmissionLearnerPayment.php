<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsAdmissionLearnerPayment.php';
$response = array();
$emp = new clsAdmissionLearnerPayment();

if ($_action == "verifydetails") {
	if (empty($_POST["lcode"]) || !isset($_POST["lcode"])) {
        echo trim("0");
    }
	else{
		$response = $emp->LearnerVerify($_POST["lcode"]);
			if($response[0]=='1'){
				echo "1";
			}
			else{
					if($response[0]=='Success'){
						$_DataTable = array();
						$_i = 0;
						while ($_Row = mysqli_fetch_array($response[2])) {
							$_Datatable[$_i] = array("Admission_Course" => $_Row['Admission_Course'],
								"Admission_Batch" => $_Row['Admission_Batch'],
								"Admission_ITGK_Code" => $_Row['Admission_ITGK_Code'],
								"RKCL_Share" => $_Row['RKCL_Share']								
							   );
							$_i = $_i + 1;
						}
						echo json_encode($_Datatable);
					}
					else{
						echo "2";
					}
			}    
	}    
}

if ($_action == "SHOWALL") {
    $response = $emp->GetAll($_POST['batch'], $_POST['course'], $_POST['paymode'], $_POST['lcode']);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    echo "<th style='12%'>D.O.B</th>";
    echo "<th style='12%'>IT-GK Code</th>";
    echo "<th style='12%'>Course Name</th>";
    echo "<th style='10%'>Photo</th>";
    echo "<th style='10%'>Sign</th>";
    echo "<th style='10%'>Amount</th>";   
    echo "<th style='8%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {

            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";
			echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
             if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="80" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="80" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="80" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="80" src="images/no_image.png"/>' . "</td>";
            }

           echo "<td>" . $_Row['RKCL_Share'] . "</td>";
           

            if ($_Row['Admission_Payment_Status'] == '0') {
                echo "<td><input type='checkbox' id='chk" . $_Row['Admission_Code'] .
                "' name='AdmissionCodes[]' value='" . $_Row['Admission_Code'] . "'></input></td>";
            } elseif ($_Row['Admission_Payment_Status'] == '8') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='DD in process'>"
                . "</td>";
            } elseif ($_Row['Admission_Payment_Status'] == '1') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='Payment Confirmed'>"
                . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    } else {        
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "ADD") {   
    if (empty($_POST["amounts"]) || !isset($_POST["AdmissionCodes"])) {
        echo "0";
    } else {
        $productinfo = 'LearnerEnquiryPayment';
        $trnxId = $emp->AddPayTran($_POST, $productinfo);
        if ($_POST['gateway'] == 'razorpay' && !empty($trnxId) && $trnxId != 'TimeCapErr') {
            require("razorpay.php");
        } else {
            echo $trnxId;
        }
    }
}


if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();	
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysql_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
	
	$key = "Es3Ozb";
	$salt = "o9aZmTfx";
	$command1 = "check_isDomestic";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	
	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);		
	$_DataTable = array();
    $_i = 0;
		$_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
				echo json_encode($_DataTable);		
 }

 if ($_action == "FinalValidateCard") {	
	$key = "Es3Ozb";
	$salt = "o9aZmTfx";
	$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	
	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
   // $wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);	 
	if($response == 'Invalid'){
		echo "1";
	}
	else if($response == 'valid'){
		echo "2";
	}
 }
 
function curlCall($wsUrl, $qs, $true){
   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
		if (curl_errno($c)) {
		  $c_error = curl_error($c);
		  if (empty($c_error)) {
			  $c_error = 'Some server error';
			}
			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);
		return $arr;
}
?>	