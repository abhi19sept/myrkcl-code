<?php

/* 
 * author Abhishek

 */
include './commonFunction.php';
require 'BAL/clsPayment.php';

$response = array();
$emp = new clsPayment();


if ($_action == "Update") {
    if (isset($_POST["code"])) {
        $_Code = filter_var($_POST["code"],FILTER_SANITIZE_STRING);
        $_PayTypeCode = filter_var($_POST["paytypecode"],FILTER_SANITIZE_STRING);
		//$_PayType = filter_var($_POST["paytype"],FILTER_SANITIZE_STRING);
        $_TranRefNo = filter_var($_POST["TranRefNo"],FILTER_SANITIZE_STRING);
        $response = $emp->Update($_Code,$_PayTypeCode,$_TranRefNo);
        echo $response[0];
    }
}

if ($_action == "SHOW") {
    $response = $emp->GetDatabyCode($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("OwnerName" => $_Row['UserProfile_FirstName'],
            "OwnerMobile" => $_Row['UserProfile_Mobile'],
            "OwnerEmail" => $_Row['UserProfile_Email']
            );

        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}

if ($_action == 'showPayData') {
    $response = $emp->GetDatabyTrnxid($_actionvalue);
    if (!empty($response)) {
        $_DataTable = array();
        $_DataTable[] = mysqli_fetch_array($response[2]);
        echo json_encode($_DataTable);
    }
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
    //print_r($response);
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
    //values
    $key = "X2ZPKM";
    $salt = "8IaBELXB";

    //For Test Mode
    //$key = 'gtKFFx';
    //$salt = "eCwWELxi";
    
    $command1 = "check_isDomestic";   //validatecardnumber   luhn algo
    //$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
    $var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
    //$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
    //$var3 = "500";//  Amount to be used in case of refund

    $hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
    $response = curlCall($wsUrl,$qs,TRUE);   
    $_DataTable = array();
    $_i = 0;
        $_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
                echo json_encode($_DataTable);      
 }

if ($_action == "FinalValidateCard") {
    //values
    //For Live Mode
    $key = "X2ZPKM";
    $salt = "8IaBELXB";

    //For Test Mode
    //$key = 'gtKFFx';
    //$salt = "eCwWELxi";

    $command1 = "validateCardNumber";   //validatecardnumber   luhn algo
    //$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
    $var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
    //$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
    //$var3 = "500";//  Amount to be used in case of refund

    $hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
    $response = curlCall($wsUrl,$qs,TRUE);  
    if($response == 'Invalid'){
        echo "1";
    }
    else if($response == 'valid'){
        echo "2";
    }
}

function curlCall($wsUrl, $qs, $true){
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
        if (curl_errno($c)) {
          $c_error = curl_error($c);
          if (empty($c_error)) {
              $c_error = 'Some server error';
            }
            return array('curl_status' => 'FAILURE', 'error' => $c_error);
        }
        $out = trim($o);
        $arr = json_decode($out,true);
        return $arr;
}

if ($_action == 'generatehash') {
    $posted = $_POST;

    if ($payment::PAY_MODE == 'Live') {
        switch ($posted['productinfo']) {
            case 'LearnerFeePayment':
            case 'Learner Fee Payment':
			case 'LearnerEnquiryPayment':
                $MERCHANT_KEY = "Es3Ozb";
                $SALT = "o9aZmTfx";
                break;
            case 'ReexamPayment':
            case 'Re Exam Event Name':
            case 'Correction Certificate':
            case 'Correction Fee Payment':
            case 'Duplicate Certificate':
            case 'NcrFeePayment':
            case 'Ncr Fee Payment':
            case 'Name Change Fee Payment':
            case 'Address Change Fee Payment':
            case 'EOI Fee Payment':
			case 'Ownership Fee Payment':
            case 'OwnershipFeePayment':  
			case 'PenaltyFeePayment':
            case 'AadharUpdFeePayment':
			case 'Duplicate Certificate with Correction':
			case 'BuyRedHatSubscription':
			case 'RscfaCertificatePayment':
            case 'RscfaReExamPayment':
			
                $MERCHANT_KEY = "X2ZPKM";
                $SALT = "8IaBELXB";
                break;
        }
        $PAYU_BASE_URL = "https://secure.payu.in";
        $surl = 'https://myrkcl.com/frmpaysuccess.php';
        $furl = 'https://myrkcl.com/frmpayfailure.php';
    } else {
        $PAYU_BASE_URL = "https://test.payu.in";
        $SALT = "eCwWELxi"; // For Test Mode
        $MERCHANT_KEY = 'gtKFFx'; // For Test Mode
        $surl = 'http://localhost/myrkcl/frmpaysuccess.php';
        $furl = 'http://localhost/myrkcl/frmpayfailure.php';
    }

    $action = $PAYU_BASE_URL . '/_payment';

    $hash = '';
    $formError = '';
    $response = $emp->GetDatabyTrnxid($posted['txnid']);
    if (empty($response) || !isset($response[2]) || empty($posted['transaction']) || empty($posted['txnid']) || $posted['txnid'] != $posted['transaction']) {
        $formError = 1;
    } elseif (empty($posted['transaction']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['phone']) || empty($posted['productinfo']) || empty($posted['pg'])) {
            $formError = 2;
    } elseif (($posted['pg']=='NB') && empty($posted['bankcode'])) {
         $formError = 3; 
    } elseif ((($posted['pg'] == 'CC') || ($posted['pg'] == 'DC')) && (empty($posted['bankcode']) || empty($posted['ccnum']) || empty($posted['ccvv']) || empty($posted['ccexpmon']) || empty($posted['ccname']) || empty($posted['ccexpyr']))) {
         $formError = 4;
    } else {
        $trnxData = mysqli_fetch_array($response[2]);
        //print_r($trnxData);
        //Sequence : "key|transaction|amount|productinfo|firstname|email|udf1|udf2|||||||||SALT";
        if ($trnxData['Pay_Tran_PG_Trnid'] == $posted['txnid'] && $trnxData['Pay_Tran_Amount'] == $posted['amount'] && $trnxData['Pay_Tran_ITGK'] == $posted['udf1']) {
            $_Response = $emp->updateTransactionStatus($posted['txnid']);
            if ($_Response[0] == Message::SuccessfullyUpdate) {
                $hash_string = $MERCHANT_KEY . '|' . $trnxData['Pay_Tran_PG_Trnid'] . '|' . $trnxData['Pay_Tran_Amount'] . '|' . $trnxData['Pay_Tran_ProdInfo'] . '|' . $trnxData['UserProfile_FirstName'] . '|' . $trnxData['UserProfile_Email'] . '|' . $trnxData['Pay_Tran_ITGK'] . '|' . $trnxData['Pay_Tran_RKCL_Trnid'] . '|' . $trnxData['Pay_Tran_Course'] . '|' . $trnxData['Pay_Tran_Batch'] . '|||||||' . $SALT;
                $hash = strtolower(hash('sha512', $hash_string));
                echo '<input type="hidden" name="key" value="' . $MERCHANT_KEY . '" />
                    <input type="hidden" name="hash" value="' . $hash . '"/>
                    <input type="hidden" name="surl" value="' . $surl . '"/>
                    <input type="hidden" name="furl" value="' . $furl . '"/>
                    <input type="hidden" name="action" id="action" value="' . $action . '"/>';
            }
        } else {
            $formError = 1;
        }
    }
    echo $formError;
}