<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clspolicy.php';

$response = array();
$emp = new clspolicy();

//Print_r($_POST);
if ($_action == "ADD") {
    if (!empty($_POST["txtmessage"])) {
       
		$_Msg=$_POST["txtmessage"];
		
       
        $response = $emp->Add($_Msg);
        echo $response[0];
    }
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}




if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    //echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Policy Content</th>";
   
	 echo "<th style='10%'>Timestamp</th>";
	  
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Message'] . "</td>";
      
		 echo "<td>" . $_Row['Timestamp'] . "</td>";
		 
         echo "<td>";
        if ($_Row['Status'] == 1) {
            echo "<a href='frmpolicy.php?code=" . $_Row['id'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a>";
        } else {
            echo "Inactive";
        }
        echo "</td></tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "GETPOLICY") {

    $userCode = (isset($_SESSION['LearnerLogin'])) ? $_SESSION['User_LearnerCode'] : (isset($_SESSION['User_LoginId']) ? $_SESSION['User_LoginId'] : '');
    $response = $emp->GetRecord($userCode);

    if (mysqli_num_rows($response[2])) {
        $row = mysqli_fetch_array($response[2]);
        $accepted = $emp->CheckIfAccepted($row['id'], $userCode);
        if (mysqli_num_rows($accepted[2])) {
            $rowAccepted = mysqli_fetch_array($accepted[2]);
            $row['acceptedId'] = $rowAccepted['id'];
        } else {
            $row['acceptedId'] = 0;
        }
        echo json_encode($row);
    } else {
        echo '0';
    }
}


if ($_action == "Accept") {

    $id = $_POST['policyid'];
    $userCode = (isset($_SESSION['LearnerLogin'])) ? $_SESSION['User_LearnerCode'] : (isset($_SESSION['User_LoginId']) ? $_SESSION['User_LoginId'] : '');
    $response = $emp->acceptPolicy($id, $userCode);
}
