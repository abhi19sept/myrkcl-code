<?php

/* 
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsSignUp.php';

$response = array();
$emp = new clsSignUp();


if ($_action == "ADD") {
    if (isset($_POST["emailid"])) {
        $_EmailId = $_POST["emailid"];
        $_Mobile=$_POST["mobile"];
        $response = $emp->Add($_EmailId,$_Mobile);
        echo $response[0];
    }
}

if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Parent</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Parent_Function_Code'] . ">" . $_Row['Parent_Function_Name'] . "</option>";
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["dlcname"])) {
        $_Code=$_POST["code"];
        $_DLCName = $_POST["dlcname"];
        $_PSAName=$_POST["psaname"];
      
        $response = $emp->Update($_Code,$_DLCName,$_PSAName);
        echo $response[0];
    }
}