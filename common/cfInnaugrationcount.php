<?php

/*
 * Created by Yogendra

 */

include './commonFunction.php';
require 'BAL/clsInnaugrationcount.php';

$response = array();
$emp = new clsInnaugrationcount();

if ($_action == "GETDETAILS") {
    //echo "Show";
		
		
		 
			  $response = $emp->GetAll($_POST['batch']);
			  
			   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
				if($_SESSION['User_UserRoll'] == '17'){
				echo "<th>Center Code</th>";
				echo "<th>Name of Public Representative</th>";
				echo "<th>Designation</th>";
				echo "<th>Area Details of Public Representative</th>";
				echo "<th>Inauguration / Closure Date</th>";
				echo "<th>Other Present Officers:</th>";				
				echo "<th>Inauguration  Photo1</th>";
				echo "<th>Inauguration  Photo2</th>";				
				}
				else{
					echo "<th>PO</th>";
					echo "<th>Count</th>";
				}
				
				
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;				
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr class='odd gradeX'>";
					echo "<td>" . $_Count . "</td>";
					if($_SESSION['User_UserRoll'] == '17'){
					echo "<td>" . $_Row['centercode'] . "</td>";	
					echo "<td>" . strtoupper( $_Row['Innaug_janpratinidhi_name']) . "</td>";	
					echo "<td>" . strtoupper( $_Row['Innaug_designation']). "</td>";	
					echo "<td>" . strtoupper( $_Row['Innaug_area_details']) . "</td>";	
					echo "<td>" . $_Row['Innaug_end_date'] . "</td>";	
					echo "<td>" . strtoupper( $_Row['Innaug_other_officer']) . "</td>";	
					
					echo "<td>";
					$center=$_Row['centercode'];
		 $l=1;
		 $l++;			
		 echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'.$center.$_Count.$l.'">Click Here</button>';
		 echo  "</td>";
		 
		 
		  
		 echo "<td>";
		  $j=2;
		  $j++;
		echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'.$center.$_Count.$j.'">Click Here</button>';
		 /* <img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/Inauguration/'.$_Row['Innaug_image2'].'"/> */
		 echo  "</td>";
		 
		 echo '<div id="'.$center.$_Count.$l.'" class="modal fade" tabindex="-1" role="dialog">';
					echo '<div class="modal-dialog">';
					echo '<div class="modal-content">';
					echo '<div class="modal-body">';
					
					echo '<img class="img-squre img-responsive"   src="upload/Inauguration/'.$_Row['Innaug_image1'].'" class="img-responsive"/>';
					
					
					echo '</div>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
					
					
					echo '<div id="'.$center.$_Count.$j.'" class="modal fade" tabindex="-1" role="dialog">';
					echo '<div class="modal-dialog">';
					echo '<div class="modal-content">';
					echo '<div class="modal-body">';
					
					echo '<img class="img-squre img-responsive"   src="upload/Inauguration/'.$_Row['Innaug_image2'].'" class="img-responsive"/>';
					
					
					echo '</div>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
		 
					}
					else{
						echo "<td>" . strtoupper( $_Row['DDLogin']) . "</td>";
						
						
						
						  echo "<td><a href='frmInnaugrationcountcenter.php?DDLogin=" . $_Row['DDLogin'] . "&batch=" . $_Row["Batchcode"] . "' target='_blank'>"
            . "" . $_Row['centercount'] . "</a></td>";
						
					}
					
				
						echo "</tr>";
					$_Count++;				
				}
				 echo "</tbody>";			
				 echo "</table>";
				 echo "</div>";
		 
		
   
}



if ($_action == "GETDETAILSPO") {
    //echo "Show";
		
			   $response = $emp->GetCenterList($_POST['Login'],$_POST['batch']);
			  
			  
			   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
				
				echo "<th>Center Code </th>";
				echo "<th>Name of Public Representative</th>";
				echo "<th>Designation</th>";
				echo "<th>Area Details of Public Representative</th>";
				echo "<th>Inauguration / Closure Date</th>";
				echo "<th>Other Present Officers:</th>";				
				echo "<th>Inauguration  Photo1</th>";
				echo "<th>Inauguration  Photo2</th>";		
				
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;				
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr class='odd gradeX'>";
					echo "<td>" . $_Count . "</td>";		
					echo "<td>" . $_Row['centercode'] . "</td>";	
					echo "<td>" .strtoupper($_Row['Innaug_janpratinidhi_name']) . "</td>";	
					echo "<td>" . strtoupper( $_Row['Innaug_designation']) . "</td>";	
					echo "<td>" . strtoupper( $_Row['Innaug_area_details']) . "</td>";	
					echo "<td>" . $_Row['Innaug_end_date'] . "</td>";	
					echo "<td>" . strtoupper( $_Row['Innaug_other_officer']) . "</td>";	
					echo "<td>";
					$center=$_Row['centercode'];
					$l=1;
					$l++;
		  echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'.$center.$_Count.$l.'">Click Here</button>';
		 /* <img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/Inauguration/'.$_Row['Innaug_image1'].'"/> */
		 echo  "</td>";
		 
		 
		  
		 echo "<td>";
		  $j=2;
		  $j++;
		 echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'.$center.$_Count.$j.'">Click Here</button>';
		 /* <img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/Inauguration/'.$_Row['Innaug_image2'].'"/> */
		 echo  "</td>";
					
				
						echo "</tr>";
						
						
						
						echo '<div id="'.$center.$_Count.$l.'" class="modal fade" tabindex="-1" role="dialog">';
					echo '<div class="modal-dialog">';
					echo '<div class="modal-content">';
					echo '<div class="modal-body">';
					
					echo '<img class="img-squre img-responsive"   src="upload/Inauguration/'.$_Row['Innaug_image1'].'" class="img-responsive"/>';
					
					
					echo '</div>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
					
					
					echo '<div id="'.$center.$_Count.$j.'" class="modal fade" tabindex="-1" role="dialog">';
					echo '<div class="modal-dialog">';
					echo '<div class="modal-content">';
					echo '<div class="modal-body">';
					
					echo '<img class="img-squre img-responsive"   src="upload/Inauguration/'.$_Row['Innaug_image2'].'" class="img-responsive"/>';
					
					
					echo '</div>';
					echo '</div>';
					echo '</div>';
					echo '</div>';
					$_Count++;				
				}
				 echo "</tbody>";			
				 echo "</table>";
				 echo "</div>";
		 
		
   
}






