<?php

/*
 * author Yogendra

 */
include './commonFunction.php';
require 'BAL/clsmodifybankaccountdetailsforaccount.php';

$response = array();
$emp = new clsmodifybankaccountdetailsforaccount();



if ($_action == "UPDATE") {
//    print_r($_POST);

    //$_code = $_POST["code"];

    $_AccountName = $_POST["txtaccountName"];
    $_AccountNumber = $_POST["txtaccountNumber"];


    $_ConfirmaccountNumber = $_POST["txtConfirmaccountNumber"];

    $_AccountType = $_POST["rbtaccountType_saving"];
    $_IfscCode = $_POST["txtIfscCode"];
    $_BankName = $_POST["ddlBankName"];
    $_BranchName = $_POST["ddlBranch"];
    $_MicrCode = $_POST["txtMicrcode"];
    $_PanNo = $_POST["txtpanno"];
    $_PanName = $_POST["txtpanname"];
    $_IdProof = $_POST["ddlidproof"];
    $_centercode = $_POST["txtcentercode"];




    $response = $emp->Update( $_AccountName, $_AccountNumber, $_AccountType, $_IfscCode, $_BankName, $_BranchName, $_MicrCode, $_PanNo, $_PanName, $_IdProof, $_centercode);
    echo $response[0];
}







if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='' >Select Banks</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['bankname'] . ">" . $_Row['bankname'] . "</option>";
    }
}

if ($_action == "FILLBRANCH") {
    $response = $emp->GetBranch();
    echo "<option value='' >Select Branch</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['branchname'] . ">" . $_Row['branchname'] . "</option>";
    }
}

if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_POST["values"]);

    $_Datatable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array(
            "Bank_Account_Name" => $_Row['Bank_Account_Name'],
            "Bank_Account_Number" => $_Row['Bank_Account_Number'],
            "Bank_Account_Type" => $_Row['Bank_Account_Type'],
            "Bank_Ifsc_code" => $_Row['Bank_Ifsc_code'],
            "Bank_Name" => $_Row['Bank_Name'],
            "Bank_Micr_Code" => $_Row['Bank_Micr_Code'],
            "Bank_Branch_Name" => $_Row['Bank_Branch_Name'],
            "Pan_No" => $_Row['Pan_No'],
            "Pan_Name" => $_Row['Pan_Name'],
            "Bank_Id_Proof" => $_Row['Bank_Id_Proof'],
            "Bank_Document" => $_Row['Bank_Document'],
            "Pan_Document" => $_Row['Pan_Document'],
            "Bank_User_Code" => $_Row['Bank_User_Code'],
            "Itgk_Name" => $_Row['ITGK_Name']
        );
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}



if ($_action == "SHOWGRID") {

    //echo "Show";
    $response = $emp->SHOWDATA();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Bank Account Name</th>";
    echo "<th style='40%'> 	Bank Account Number</th>";
    echo "<th style='10%'> 	Bank IFSC Code</th>";
    echo "<th style='10%'> 	Bank Name</th>";
    echo "<th style='10%'> 	Bank Micr Code</th>";
    echo "<th style='10%'>  Bank Branch Name</th>";
    echo "<th style='10%'>  Account Type</th>";
    echo "<th style='10%'>  Pan Card No</th>";
    echo "<th style='10%'>  Pan Card Holder Name</th>";
    echo "<th style='10%'>  Bank Document</th>";
    echo "<th style='10%'>  PAN  Document</th>";
    echo "<th style='10%'>  ITGK Code</th>";
    echo "<th style='10%'>  Action</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
        echo "<td>'" . strtoupper($_Row['Bank_Account_Number']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
        echo "<td>" . $_Row['Bank_Micr_Code'] . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Branch_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Type']) . "</td>";
        echo "<td>" . strtoupper($_Row['Pan_No']) . "</td>";
        echo "<td>" . strtoupper($_Row['Pan_Name']) . "</td>";
        $centercode = $_Row['Bank_User_Code'];

        echo "<td>";
        $l = 1;
        $l++;

        echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#' . $centercode . $_Count . $l . '">Click Here</button>';
        echo "</td>";
        echo "<td>";
        $j = 2;
        $j++;
        echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#' . $centercode . $_Count . $j . '">Click Here</button>';
        echo "</td>";


        echo "<td>" . $_Row['Bank_User_Code'] . "</td>";




        echo "<td><a href='frmfillbankaccountforaccount.php?code=" . $_Row['Bank_Account_Code'] . "&Mode=Edit'>"
        . "<img src='images/editicon.png' alt='Edit' width='30px' /></a> </td>";

        echo "</tr>";
        echo '<div id="' . $centercode . $_Count . $l . '" class="modal fade" tabindex="-1" role="dialog">';
        echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';
        echo '<div class="modal-body">';

        echo '<img class="img-squre img-responsive"   src="upload/Bankdocs/' . $_Row['Bank_Document'] . '" class="img-responsive"/>';


        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';


        echo '<div id="' . $centercode . $_Count . $j . '" class="modal fade" tabindex="-1" role="dialog">';
        echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';
        echo '<div class="modal-body">';

        echo '<img class="img-squre img-responsive"   src="upload/pancard/' . $_Row['Pan_Document'] . '" class="img-responsive"/>';


        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";




    echo "</div>";
}









if ($_action == "IFSCCHEK") {
    //print_r($_POST);
    if (isset($_POST["IFSC"])) {
        $_IFSC = $_POST["IFSC"];


        $response = $emp->checkifsccode($_IFSC);
       // print_r($response);
        if ($response[0] == "Success") {
            echo "1";
        } else {
            echo "0";
        }
    }
}




