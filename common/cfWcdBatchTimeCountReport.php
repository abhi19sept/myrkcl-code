<?php

/*
 * Created by Mayank

 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
include './commonFunction.php';
require 'BAL/clsWcdBatchTimeCountReport.php';

$response = array();
$emp = new clsWcdBatchTimeCountReport();

if ($_action == "GetBatchTimeCount") {
    //echo "Show";
		
		if($_POST['batch'] !='') {
		 
			  $response = $emp->GetAllCount($_POST['course'],$_POST['batch']);
			  
				if($_SESSION['User_UserRoll']=='14'){
					date_default_timezone_set("Asia/Kolkata");
					$datetime = date("Ymd_His");
					$filename = "WCDBatchTime_" . $datetime . ".csv";
					
					//$filename = "WCDBatchTime_" . $_POST['batch'] . " .csv";
					$fileNamePath = "../upload/wcdbatchtimeallocation/" . $filename;
		
					$myFile = fopen($fileNamePath, 'w');
					fputs($myFile, '"' . implode('","', array("ITGK Code", "ITGK Name", "ITGK Mobile", "District Name",
									"Tehsil Name", "Total Approved Count", "Total Reported Count", "08 AM", "09 AM",
									"10 AM","11 AM","12 PM","01 PM","02 PM","03 PM","04 PM","05 PM","06 PM" )) . '"' . "\n");
					while ($row1 = mysqli_fetch_row($response[2])) { 

					fputcsv($myFile, array($row1['0'], $row1['1'],$row1['2'], $row1['3'], $row1['4'], $row1['5'], $row1['6'],
							$row1['7'], $row1['8'], $row1['9'], $row1['10'], $row1['11'], $row1['12'], $row1['13'], $row1['14'],
							$row1['15'], $row1['16'], $row1['17']), ',', '"');
					}
							fclose($myFile);
							echo "upload/wcdbatchtimeallocation/" . $filename;
						}
				  else{
				  
			  
			   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
				echo "<th>ITGK Code</th>";
				echo "<th>ITGK Name</th>";
				echo "<th>ITGK Mobile</th>";
				echo "<th>District Name</th>";
				echo "<th>Tehsil Name</th>";
				echo "<th>Total Approved Count</th>";
				echo "<th>Total Reported Count</th>";
				echo "<th>08 AM</th>";
				echo "<th>09 AM</th>";
				echo "<th>10 AM</th>";
				echo "<th>11 AM</th>";
				echo "<th>12 PM</th>";
				echo "<th>01 PM</th>";
				echo "<th>02 PM</th>";
				echo "<th>03 PM</th>";
				echo "<th>04 PM</th>";
				echo "<th>05 PM</th>";
				echo "<th>06 PM</th>";
				
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;
					$_Total = 0;
					$_Report = 0;
					$_eight = 0;
					$_nine = 0;
					$_ten = 0;
					$_eleven = 0;
					$_twelve = 0;
					$_ones = 0;
					$_two = 0;
					$_three = 0;
					$_four = 0;
					$_five = 0;
					$_six = 0;
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr class='odd gradeX'>";
					echo "<td>" . $_Count . "</td>";		
					echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";
					echo "<td>" . $_Row['ITGK_Name'] . "</td>";
					echo "<td>" . $_Row['ITGKMOBILE'] . "</td>";
					echo "<td>" . $_Row['District_Name'] . "</td>";
					echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
					echo "<td>" . $_Row['Approved'] . "</td>";
					echo "<td>" . $_Row['reported'] . "</td>";
					echo "<td>" . $_Row['eight'] . "</td>";
					echo "<td>" . $_Row['nine'] . "</td>";
					echo "<td>" . $_Row['ten'] . "</td>";
					echo "<td>" . $_Row['eleven'] . "</td>";
					echo "<td>" . $_Row['twelve'] . "</td>";
					echo "<td>" . $_Row['ones'] . "</td>";
					echo "<td>" . $_Row['two'] . "</td>";
					echo "<td>" . $_Row['three'] . "</td>";
					echo "<td>" . $_Row['four'] . "</td>";
					echo "<td>" . $_Row['five'] . "</td>";
					echo "<td>" . $_Row['six'] . "</td>";
					  		
						echo "</tr>";
						$_Total = $_Total + $_Row['Approved'];
						$_Report = $_Report + $_Row['reported'];
						$_eight = $_eight + $_Row['eight'];
						$_nine = $_nine + $_Row['nine'];
						$_ten = $_ten + $_Row['ten'];
						$_eleven = $_eleven + $_Row['eleven'];
						$_twelve = $_twelve + $_Row['twelve'];
						$_ones = $_ones + $_Row['ones'];
						$_two = $_two + $_Row['two'];
						$_three = $_three + $_Row['three'];
						$_four = $_four + $_Row['four'];
						$_five = $_five + $_Row['five'];
						$_six = $_six + $_Row['six'];
					$_Count++;				
				}
				 echo "</tbody>";
					
						echo "<tfoot>";
						echo "<tr>";
						echo "<th >  </th>";
						echo "<th >  </th>";
						echo "<th >  </th>";
						echo "<th >  </th>";
						echo "<th >  </th>";
						echo "<th >Total Count </th>";
						echo "<th>";
						echo "$_Total";
						echo "</th>";
						echo "<th>";
						echo "$_Report";
						echo "</th>";
						echo "<th>";
						echo "$_eight";
						echo "</th>";
						echo "<th>";
						echo "$_nine";
						echo "</th>";
						echo "<th>";
						echo "$_ten";
						echo "</th>";
						echo "<th>";
						echo "$_eleven";
						echo "</th>";
						echo "<th>";
						echo "$_twelve";
						echo "</th>";
						echo "<th>";
						echo "$_ones";
						echo "</th>";
						echo "<th>";
						echo "$_two";
						echo "</th>";
						echo "<th>";
						echo "$_three";
						echo "</th>";
						echo "<th>";
						echo "$_four";
						echo "</th>";
						echo "<th>";
						echo "$_five";
						echo "</th>";
						echo "<th>";
						echo "$_six";
						echo "</th>";
						echo "</tr>";
						echo "</tfoot>";
		
				 echo "</table>";
				 echo "</div>";
			}
		}
		  else {
			  echo "1";
		  }
   
}



if ($_action == "GetBatchTimeCountForAdmin") {
    //echo "Show";
		
		if($_POST['batch'] !='') {
		 
			  $response = $emp->GetAllCountForAdmin($_POST['course'],$_POST['batch'],$_POST['district']);
					date_default_timezone_set("Asia/Kolkata");
					$datetime = date("Ymd_His");
					$filename = "WCDBatchTime_" . $datetime . ".csv";
					//$filename = "WCDBatchTime_" . $_POST['batch'] . " .csv";
					$fileNamePath = "../upload/wcdbatchtimeallocation/" . $filename;
		
					$myFile = fopen($fileNamePath, 'w');
					fputs($myFile, '"' . implode('","', array("ITGK Code", "ITGK Name", "ITGK Mobile", "District Name",
									"Tehsil Name", "Total Approved Count", "Total Reported Count", "08 AM", "09 AM",
									"10 AM","11 AM","12 PM","01 PM","02 PM","03 PM","04 PM","05 PM","06 PM" )) . '"' . "\n");
					while ($row1 = mysqli_fetch_row($response[2])) { 

					fputcsv($myFile, array($row1['0'], $row1['1'],$row1['2'], $row1['3'], $row1['4'], $row1['5'], $row1['6'],
							$row1['7'], $row1['8'], $row1['9'], $row1['10'], $row1['11'], $row1['12'], $row1['13'], $row1['14'],
							$row1['15'], $row1['16'], $row1['17']), ',', '"');
					}
							fclose($myFile);
							echo "upload/wcdbatchtimeallocation/" . $filename;
			   
		 
		}
		  else {
			  echo "1";
		  }
   
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FillCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FillDistrict") {
    $response = $emp->GetDistrict();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['District_Code'] . ">" . $_Row['District_Name'] . "</option>";
    }
}