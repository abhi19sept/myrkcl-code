<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsgetpassword.php';

$response = array();
$emp = new clsgetpassword();

if ($_action == "DETAILS") {
    $response = $emp->getpass($_POST['values']);
    $_DataTable = "";
    //$key = hex2bin('000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f');
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='30%'>Login ID</th>";
    echo "<th style='30%'>Login Password</th>";
    echo "<th style='20%'>Email ID</th>";
    echo "<th style='15%'>Mobile No.</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['User_LoginId'] . "</td>";
        echo "<td>" .  $_Row['User_Password'] . "</td>";
        echo "<td>" . $_Row['User_EmailId'] . "</td>";
        echo "<td>" . $_Row['User_MobileNo'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "DETAILSDEPASS") {
    $response = $emp->getdecryptpass();
    $_DataTable = "";
     echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='30%'>User Code</th>";
    echo "<th style='30%'>User LoginId </th>";
    echo "<th style='20%'>Decrypted Password </th>";
    echo "<th style='15%'>Encrypted Password</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_DataTable2 = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2], MYSQLI_ASSOC)) {
        $_Row['User_Password2']=$_Row['User_Password'];
        $_Row['User_Password']=htmlentities($_Row['User_Password']);

        $_Datatable2[$_i] = $_Row;
        $_i = $_i + 1;
    }
    
    $p=1;
    foreach ($_Datatable2 as $key => $value) {
        if($value['User_Password']==''){
//            echo $value['User_LoginId'];
//            echo "----";
//            echo $_Row['User_Password2'];
//            echo "<br>";

        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $value['User_Code'] . "</td>";        
        echo "<td>" . $value['User_LoginId'] . "</td>";
        echo "<td>" . $value['User_Password2'] . "</td>";
        echo "<td>" . $general->decrypt_with_key($value['User_Password2']) . "</td>";
        echo "</tr>";
        $_Count++;
        }
    }

    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "UPDATEDPASS") {
    
     $response = $emp->getdecryptpass();
     
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Row['User_Password2']=$_Row['User_Password'];
        $_Row['User_Password']=htmlentities($_Row['User_Password']);
        if($_Row['User_Password']=='')
            {       
            //echo $_Row['User_LoginId'];
            $originalpassword=$general->decrypt_with_key($_Row['User_Password2']);
            $response2 = $emp->decryptpassupdate($_Row['User_LoginId'],$originalpassword);
            
            }

        
    }
    
    echo $response2[0];

}
?>