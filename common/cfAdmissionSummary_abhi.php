<?php

/*
 * Created by Abhishek

 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
include './commonFunction.php';
require 'BAL/clsAdmissionSummary.php';

$response = array();
$emp = new clsAdmissionSummary();

if ($_action == "GETDATA") {

    $response = $emp->GetDataAll($_POST['role'], $_POST['course'], $_POST['batch']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    //echo "<th>CenterCode</th>";
    // echo "<th>Course</th>";
    // echo "<th>Batch</th>";
    echo "<th>Code</th>";
	echo "<th >Uploaded Admission Count</th>";
    echo "<th >Confirm Admission Count</th>";
    // echo "<th>Count</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;
	$_Totaleconfirm = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            // echo "<td>" . $_Row['Course'] . "</td>";
            //  echo "<td>" . $_Row['Batch'] . "</td>";
            echo "<td>" . $_Row['RoleName'] . "</td>";
			echo "<td><a href='frmAdmissionList.php?course=" . $_Row['Course'] . "&batch=" . $_Row['Batch'] . "&rolecode=" . $_Row['RoleCode'] . "&mode=ShowUpload' target='_blank'>"
            . "" . $_Row['uploadcount'] . "</a></td>";
            echo "<td><a href='frmAdmissionList.php?course=" . $_Row['Course'] . "&batch=" . $_Row['Batch'] . "&rolecode=" . $_Row['RoleCode'] . "&mode=ShowConfirm' target='_blank'>"
            . "" . $_Row['confirmcount'] . "</a></td>";
            echo "</tr>";
            $_Total = $_Total + $_Row['uploadcount'];
			$_Totaleconfirm = $_Totaleconfirm + $_Row['confirmcount'];
            $_Count++;
        }

        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
        echo "<th >TotalCount </th>";
        echo "<th>";
        echo "$_Total";
        echo "</th>";
		echo "<th>";
        echo "$_Totaleconfirm";
        echo "</th>";
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETLEARNERLIST") {
$ftp_conn = $_ObjConnection->ConnectFtp();

    $response = $emp->GetLearnerList($_POST['course'], $_POST['batch'], $_POST['rolecode'], $_POST['mode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
	echo "<th >Mobile NO</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
        	$image = $_Row['Admission_Photo'];
        	$server_file_photo="upload/admission_photo/" . $image;
			$local_file_photo ="abc.png";

            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
			echo "<td>" . $_Row['Admission_Mobile'] . "</td>";

            if ($_Row['Admission_Photo'] != "") {
               
                if (ftp_get($ftp_conn, $local_file_photo, $server_file_photo, FTP_BINARY))
					  {
					  	 echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="' . $local_file_photo . '"/>' . "</td>";
					  }
               
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETDATAITGK") {

	$response = $emp->GetLearnerListITGK($_POST['course'], $_POST['batch'], $_POST['rolecode']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
	echo "<th>Payment Status</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
        	$image = "remingtonlayout.jpg";
        	$server_file_photo="/upload/admission_photo/" . $image;
			$local_file_photo ="hfhyhhhabc.jpg";
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";

            //if ($_Row['Admission_Photo'] != "") {
            	echo "<td>". ImagegGet()."</td>";
                
            // } else {
            //     echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            // }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }
			 if ($_Row['Admission_Payment_Status'] == "1") {
               
                echo "<td> Learner Confirmed </td>";
            } elseif ($_Row['Admission_Payment_Status'] == "0")  {
                echo "<td> Learner Not Confirmed </td>";
            }
			elseif ($_Row['Admission_Payment_Status'] == "8")  {
                echo "<td> Confirmation Pending at RKCL </td>";
            }


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
function ImagegGet(){
     global $_ObjConnection;
    $ftp_conn = $_ObjConnection->ConnectFtp();
   $image = "remingtonlayout.jpg";
            $server_file_photo="/upload/admission_photo/" . $image;
            $local_file_photo ="abcgg.jpg";

    if (ftp_get($ftp_conn, $local_file_photo, $server_file_photo, FTP_BINARY))
  {


    $imags =  '<img src="'.$local_file_photo.'" alt="W3Schools.com" width="104" height="142">';
    return  $imags;
  }
}

if ($_action == "DETAILRPTITGK") {
    $response = $emp->DetailedListITGK($_POST['course'], $_POST['batch'], $_POST['rolecode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >DOB</th>";
    echo "<th>Gender</th>";
    echo "<th >Course</th>";
    echo "<th>Batch</th>";
    echo "<th >Medium</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "<th>Mother Tongue</th>";
    echo "<th>Marital Status</th>";
    echo "<th>UID</th>";
    echo "<th >PH Status</th>";
    echo "<th >District</th>";
    echo "<th>Tehsil</th>";
    echo "<th>Address</th>";
    echo "<th>PIN code</th>";
    echo "<th>Mobile</th>";
    echo "<th>Phone</th>";
    echo "<th>Email</th>";
    echo "<th >Qualification</th>";
    echo "<th >Learner Type</th>";
    echo "<th >GPFNO</th>";
    echo "<th>BioMetric Status</th>";
    echo "<th>Admission Date</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";
            echo "<td>" . $_Row['Admission_Gender'] . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Medium'] . "</td>";
            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }
            echo "<td>" . $_Row['Admission_MTongue'] . "</td>";
            echo "<td>" . $_Row['Admission_MaritalStatus'] . "</td>";
            echo "<td>" . $_Row['Admission_UID'] . "</td>";
            echo "<td>" . $_Row['Admission_PH'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Address'] . "</td>";
            echo "<td>" . $_Row['Admission_PIN'] . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            echo "<td>" . $_Row['Admission_Phone'] . "</td>";
            echo "<td>" . $_Row['Admission_Email'] . "</td>";
            echo "<td>" . $_Row['Qualification_Name'] . "</td>";
            echo "<td>" . $_Row['LearnerType_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_GPFNO'] . "</td>";
            if($_Row['BioMatric_Status']=='1'){
                 echo "<td>Learner Enrolled</td>";
            }
            else{
                 echo "<td>Learner Not Enrolled</td>";
            }
            //echo "<td>" . $_Row['BioMatric_Status'] . "</td>";
            echo "<td>" . date("d-m-Y", strtotime($_Row['Timestamp'])) . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_Name'] . "</td>";
//            echo "<td>" . $_Row['Admission_Fname'] . "</td>";


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
    if ($_action == "DETAILRPT") {
    $response = $emp->DetailedList($_POST['course'], $_POST['batch'], $_POST['role']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >DOB</th>";
    echo "<th>Gender</th>";
    echo "<th >Course</th>";
    echo "<th>Batch</th>";
    echo "<th >Medium</th>";
    echo "<th>Photo</th>";
    echo "<th>Sign</th>";
    echo "<th>Mother Tongue</th>";
    echo "<th>Marital Status</th>";
    echo "<th>UID</th>";
    echo "<th >PH Status</th>";
    echo "<th >District</th>";
    echo "<th>Tehsil</th>";
    echo "<th>Address</th>";
    echo "<th>PIN code</th>";
    echo "<th>Mobile</th>";
    echo "<th>Phone</th>";
    echo "<th>Email</th>";
    echo "<th >Qualification</th>";
    echo "<th >Learner Type</th>";
    echo "<th >GPFNO</th>";
    echo "<th>BioMetric Status</th>";
    echo "<th>Admission Date</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";
            echo "<td>" . $_Row['Admission_Gender'] . "</td>";
            echo "<td>" . $_Row['Course_Name'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Medium'] . "</td>";
            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }
            echo "<td>" . $_Row['Admission_MTongue'] . "</td>";
            echo "<td>" . $_Row['Admission_MaritalStatus'] . "</td>";
            echo "<td>" . $_Row['Admission_UID'] . "</td>";
            echo "<td>" . $_Row['Admission_PH'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Address'] . "</td>";
            echo "<td>" . $_Row['Admission_PIN'] . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            echo "<td>" . $_Row['Admission_Phone'] . "</td>";
            echo "<td>" . $_Row['Admission_Email'] . "</td>";
            echo "<td>" . $_Row['Qualification_Name'] . "</td>";
            echo "<td>" . $_Row['LearnerType_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_GPFNO'] . "</td>";
            if($_Row['BioMatric_Status']=='1'){
                 echo "<td>Learner Enrolled</td>";
            }
            else{
                 echo "<td>Learner Not Enrolled</td>";
            }
            //echo "<td>" . $_Row['BioMatric_Status'] . "</td>";
            echo "<td>" . date("d-m-Y", strtotime($_Row['Timestamp'])) . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_Name'] . "</td>";
//            echo "<td>" . $_Row['Admission_Fname'] . "</td>";


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}