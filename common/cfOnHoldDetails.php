<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsOnHoldDetails.php';

$response = array();
$emp = new clsOnHoldDetails();



if ($_action == "ShowDetails") {
    $response = $emp->GetAll();
    if($response[0] == 'Success'){
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>New IT-GK Name</th>";
    echo "<th>Ownership Change Ack No</th>";
    echo "<th>IT-GK Code</th>";

    echo "<th>IFSC Code</th>";
    echo "<th>Account Holder Name</th>";
    echo "<th>Account Number</th>";
    echo "<th>Bank Name</th>";
    echo "<th>Branch Name</th>";
    echo "<th>MICR Code</th>";
    echo "<th>Account Type</th>";
    echo "<th>Application Date</th>";
    echo "<th>Message</th>";
//    echo "<th>Approval Status</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Ack']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Itgk_Code']) . "</td>";

        echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Number']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Branch_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Micr_Code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Type']) . "</td>";
        echo "<td>" . date('d-m-Y', strtotime($_Row['Org_App_Date'])) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Message']) . "</td>";
//        if($_Row['Org_Message'] != ''){
//        echo "<td style='color:red'>On Hold</td>";    
//        } elseif($_Row['Org_Message'] == ''){
//        echo "<td>New</td>";   
//        } elseif($_Row['Org_Application_Approval_Status'] == 'Approved'){
//         echo "<td style='color:green'>Approved</td>";   
//        }

        if ($_Row['Org_Application_Approval_Status'] == 'OnHold') {
            echo "<td> <a href='frmprocessonhold.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit&cc=" . $_Row['Org_Itgk_Code'] . "&ack=" . $_Row['Org_Ack'] . "'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-danger' value='Edit Application'/></a>"
            . "</td>";
        } else if ($_Row['Org_Application_Approval_Status'] == 'Approved') {
            echo "<td style='color:green'>"
            . "Approved"
            . "</td>";
        } else if ($_Row['Org_Application_Approval_Status'] == 'Pending') {
            echo "<td style='color:red'>"
            . "OnHold"
            . "</td>";
        } else {
            echo "<td>"
            . "Rejected"
            . "</td>";
        }


        echo "</tr>";
        $_Count++;
    }
    
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
    } else{
        echo '0';
        return;
    }
}

if ($_action == "PROCESS") {
    $response = $emp->GetOrgDatabyCode();
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_role = 'Ownership Change';
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "orgcode" => $_Row['Organization_Code'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "orgpan" => $_Row['Org_PAN'],
            "orgaadhar" => $_Row['Org_AADHAR'],
            "role" => $_role,
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            "bankaccname" => strtoupper($_Row['Bank_Account_Name']),
            "bankaccno" => $_Row['Bank_Account_Number'],
            "bankacctype" => strtoupper($_Row['Bank_Account_Type']),
            "bankname" => strtoupper($_Row['Bank_Name']),
            "ifsccode" => strtoupper($_Row['Bank_Ifsc_code']),
            "micrcode" => $_Row['Bank_Micr_Code'],
            "branchname" => strtoupper($_Row['Bank_Branch_Name']),
            "panno" => strtoupper($_Row['Bank_Pan_No']),
            "panname" => strtoupper($_Row['Bank_Pan_Name']),
            "bankid" => strtoupper($_Row['Bank_Id_Proof']),
            "bankpandoc" => $_Row['Bank_Pan_Document'],
            "bankiddoc" => $_Row['Bank_Id_Doc'],
            "orgdoc" => $_Row['Organization_ScanDoc'],
            "orguid" => $_Row['Organization_UID'],
            "orgaddproof" => $_Row['Organization_AddProof'],
            "orgappform" => $_Row['Organization_AppForm'],
            "orgtypedoc1" => $_Row['Organization_TypeDocId'],);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "EDIT") {
    //print_r($_POST);
   // die;
    
            if (isset($_POST["txtRegno"]) && !empty($_POST["txtRegno"])) {
                if (isset($_POST["txtIfscCode"]) && !empty($_POST["txtIfscCode"])) {
                    if (isset($_POST["txtaccountName"]) && !empty($_POST["txtaccountName"])) {
                        if (isset($_POST["txtaccountNumber"]) && !empty($_POST["txtaccountNumber"])) {
                            if (isset($_POST["ddlBankName"]) && !empty($_POST["ddlBankName"])) {
                                if (isset($_POST["ddlBranch"]) && !empty($_POST["ddlBranch"])) {
                                    if (isset($_POST["txtMicrcode"]) && !empty($_POST["txtMicrcode"])) {
                                        if (isset($_POST["ddlidproof"]) && !empty($_POST["ddlidproof"])) {
                                            if (isset($_POST["txtAADHARNo"]) && !empty($_POST["txtAADHARNo"])) {
                                                    if (isset($_POST["txtPanNo"]) && !empty($_POST["txtPanNo"])) {
                                                        if (isset($_POST["txtpanname"]) && !empty($_POST["txtpanname"])) {


                                                            $_OrgEmail = $_POST["txtEmail"];
                                                            $_OrgMobile = $_POST["txtMobile"];

                                                            $_OrgName = trim($_POST["txtName1"]);
                                                            $_OrgCode = $_POST["txtOrgCode"];
                                                            $_OrgRegno = trim($_POST["txtRegno"]);
                                                            $_OrgEstDate = $_POST["txtEstdate"];


                                                            $_PanNo = $_POST["txtPanNo"];
                                                            $_AADHARNo = $_POST["txtAADHARNo"];

                                                            $_IFSC_Code = $_POST["txtIfscCode"];
                                                            $_AccountName = $_POST["txtaccountName"];
                                                            $_AccountNumber = $_POST["txtaccountNumber"];
                                                            $_BankName = $_POST["ddlBankName"];
                                                            $_BranchName = $_POST["ddlBranch"];
                                                            $_MicrCode = $_POST["txtMicrcode"];
                                                            $_AccountType = $_POST["rbtaccountType_saving"];
                                                            $_PanNo1 = $_POST["txtpanno1"];
                                                            $_PanName = $_POST["txtpanname"];
                                                            $_IdProof = $_POST["ddlidproof"];


                                                            $response = $emp->Add($_OrgCode,$_OrgEmail, $_OrgMobile, $_OrgName, $_OrgRegno, $_OrgEstDate, 
                                                                                  $_PanNo, $_AADHARNo, $_IFSC_Code, $_AccountName, $_AccountNumber, 
                                                                                  $_BankName,$_BranchName, $_MicrCode, $_AccountType, $_PanNo1, $_PanName, $_IdProof);
                                                            echo $response[0];
                                                        } else {
                                                            echo "Inavalid Entry1";
                                                        }
                                                    } else {
                                                        echo "Inavalid Entry2";
                                                    }
                                            } else {
                                                echo "Inavalid Entry4";
                                            }
                                        } else {
                                            echo "Inavalid Entry5";
                                        }
                                    } else {
                                        echo "Inavalid Entry6";
                                    }
                                } else {
                                    echo "Inavalid Entry7";
                                }
                            } else {
                                echo "Inavalid Entry8";
                            }
                        } else {
                            echo "Inavalid Entry9";
                        }
                    } else {
                        echo "Inavalid Entry11";
                    }
                } else {
                    echo "Inavalid Entry12";
                }
            } else {
                echo "Inavalid Entry13";
            }
    
}