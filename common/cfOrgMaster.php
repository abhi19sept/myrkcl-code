<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsOrgMaster.php';

$response = array();
$emp = new clsOrgMaster();


if ($_action == "ADD") {
    //print_r($_POST);
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        if (isset($_POST["type"]) && !empty($_POST["type"])) {
            if (isset($_POST["regno"]) && !empty($_POST["regno"])) {
                if (isset($_POST["state"]) && !empty($_POST["state"])) {
                    if (isset($_POST["region"]) && !empty($_POST["region"])) {
                        if (isset($_POST["district"]) && !empty($_POST["district"])) {
                            if (isset($_POST["tehsil"]) && !empty($_POST["tehsil"])) {
                                if (isset($_POST["landmark"]) && !empty($_POST["landmark"])) {
                                    if (isset($_POST["road"]) && !empty($_POST["road"])) {
                                        if (isset($_POST["street"]) && !empty($_POST["street"])) {
                                            if (isset($_POST["houseno"]) && !empty($_POST["houseno"])) {
                                                if (isset($_POST["docproof"]) && !empty($_POST["docproof"])) {
                                                    if (isset($_POST["country"]) && !empty($_POST["country"])) {
                                                        if (isset($_POST["doctype"]) && !empty($_POST["doctype"])) {


                                                            $_OrgAOType = $_POST["orgtype"];
                                                            $_OrgEmail = $_POST["email"];
                                                            $_OrgMobile = $_POST["mobile"];
                                                            $_OrgName = $_POST["name"];
                                                            $_OrgType = $_POST["type"];
                                                            $_OrgRegno = $_POST["regno"];
                                                            $_State = $_POST["state"];
                                                            $_Region = $_POST["region"];
                                                            $_District = $_POST["district"];
                                                            $_Tehsil = $_POST["tehsil"];
                                                            $_Landmark = $_POST["landmark"];
                                                            $_Road = $_POST["road"];
                                                            $_Street = $_POST["street"];
                                                            $_HouseNo = $_POST["houseno"];
                                                            $_OrgEstDate = $_POST["estdate"];
                                                            $_docproof = $_POST["docproof"];
                                                            $_doctype = $_POST["doctype"];
                                                            $_OrgCountry = $_POST["country"];
                                                            ///                                                  $_Genid = $_POST["txtGenerateId"];


                                                            $response = $emp->Add($_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_State, $_Region, $_District, $_Tehsil, $_Landmark, $_Road, $_Street, $_HouseNo, $_OrgEstDate, $_OrgCountry, $_docproof, $_doctype);
                                                            echo $response[0];
                                                        } else {
                                                            echo "Inavalid Entry";
                                                        }
                                                    } else {
                                                        echo "Inavalid Entry";
                                                    }
                                                } else {
                                                    echo "Inavalid Entry";
                                                }
                                            } else {
                                                echo "Inavalid Entry";
                                            }
                                        } else {
                                            echo "Inavalid Entry";
                                        }
                                    } else {
                                        echo "Inavalid Entry";
                                    }
                                } else {
                                    echo "Inavalid Entry";
                                }
                            } else {
                                echo "Inavalid Entry";
                            }
                        } else {
                            echo "Inavalid Entry";
                        }
                    } else {
                        echo "Inavalid Entry";
                    }
                } else {
                    echo "Inavalid Entry";
                }
            } else {
                echo "Inavalid Entry";
            }
        } else {
            echo "Inavalid Entry";
        }
    } else {
        echo "Inavalid Entry";
    }
}


if ($_action == "FILLStatus") {
    $response = $emp->FILLStatus();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Org_Status'] . ">" . $_Row['Org_Status'] . "</option>";
    }
}

if ($_action == "ShowDetails") {
    $response = $emp->GetAll($_POST['role'], $_POST['status']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Organization Name</th>";
    echo "<th>Organization Ack No</th>";
    echo "<th>Organization Address</th>";
    echo "<th>Organization District</th>";
    echo "<th>Organization Tehsil</th>";
    echo "<th>Organization Email</th>";
    echo "<th>Organization Mobile</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        if ($_Row['Org_Status'] == 'Approved') {
        echo "<td>" . $_Row['User_LoginId'] . "</td>";
        }
        else{
            echo "<td>". "Center Code NA". "</td>";
        }
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Ack']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Road']) . "</td>";
        echo "<td>" . $_Row['Organization_District'] . "</td>";
        echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";
        echo "<td>" . $_Row['Org_Email'] . "</td>";
        echo "<td>" . $_Row['Org_Mobile'] . "</td>";

        if ($_Row['Org_Status'] == 'Pending') {
            echo "<td> <a href='frmorgprocess.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='Approve/Reject'/></a>"
            . "</td>";
        } else if ($_Row['Org_Status'] == 'Approved') {
            echo "<td>"
            . "Approved"
            . "</td>";
        } else {
            echo "<td>"
            . "Rejected By RM"
            . "</td>";
        }

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        if($_Row['Org_Role']== '14')
        {
            $_role = 'RSP';
        }
        elseif($_Row['Org_Role']== '15')
        {
            $_role = 'IT-GK';
        }
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "doctype" => $_Row['Organization_DocType'],
            "role" => $_role,
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            "district" => $_Row['Organization_District_Name'],
            "districtcode" => $_Row['Organization_District'],
            "tehsil" => $_Row['Organization_Tehsil'],
            "street" => $_Row['Organization_Street'],
            "road" => $_Row['Organization_Road'],
            "orgdoc" => $_Row['Organization_ScanDoc']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "UPDATE") {
    $_OrgCode = $_POST["orgcode"];
    $_Status = $_POST["status"];
    $_Remark = $_POST["remark"];
    $_DistrictCode = $_POST["districtcode"];
    if ($_Status == "Reject") {
        $response = $emp->UpdateOrgMaster($_OrgCode, $_Status, $_Remark);
        echo $response[0];
    } elseif ($_Status == "Approve") {
        $response = $emp->UpdateToCreateLogin($_OrgCode, $_Status, $_DistrictCode);
        echo $response[0];
    }
}