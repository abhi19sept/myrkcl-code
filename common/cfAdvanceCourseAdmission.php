<?php

/*
 *  author Mayank
 */

require 'commonFunction.php';
require 'BAL/clsAdvanceCourseAdmission.php';

$response = array();
$emp = new clsAdvanceCourseAdmission();
//print_r($_POST);

if ($_action == "FILLAdvanceCourse") {
    $response = $emp->GetAdvanceCourse();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Courseitgk_Course'] . "</option>";
    }
}

if ($_action == "FILLAdvanceCourseName") {
    $response = $emp->GetAdvanceCourseName($_POST['values']);	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Ad_Course_Id'] . ">" . $_Row['Ad_Course_Name'] . "</option>";
    }
}

if ($_action == "FILLCourseCategory") {
    $response = $emp->GetAdvanceCourseCategory($_POST['values']);	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Ad_Category_Code'] . ">" . $_Row['Ad_Category_Name'] . "</option>";
    }
}

if ($_action == "FILLEventBatch") {
    $response = $emp->FILLEventBatch($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Batch'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "ADD") {
    //print_r($_POST); 
	 if (isset($_POST["learnercode"]) && !empty($_POST["learnercode"])) {
    if (isset($_POST["lname"]) && !empty($_POST["lname"])) {
        if (isset($_POST["parent"]) && !empty($_POST["parent"])) {
            if (isset($_POST["idproof"]) && !empty($_POST["idproof"])) {                
                    if (isset($_POST["dob"]) && !empty($_POST["dob"])) {
                        if (isset($_POST["mothertongue"]) && !empty($_POST["mothertongue"])) {
                            if (isset($_POST["medium"]) && !empty($_POST["medium"])) {
                                if (isset($_POST["gender"]) && !empty($_POST["gender"])) {
                                    if (isset($_POST["marital_type"]) && !empty($_POST["marital_type"])) {
                                        if (isset($_POST["district"]) && !empty($_POST["district"])) {
                                            if (isset($_POST["tehsil"]) && !empty($_POST["tehsil"])) {
                                                if (isset($_POST["address"]) && !empty($_POST["address"])) {
                                                    if (isset($_POST["pincode"]) && !empty($_POST["pincode"])) {                                                        
                                                            if (isset($_POST["mobile"]) && !empty($_POST["mobile"])) {
                                                                if (isset($_POST["qualification"]) && !empty($_POST["qualification"])) {
                                                                    if (isset($_POST["learnertype"]) && !empty($_POST["learnertype"])) {
                                                                        if (isset($_POST["physicallychallenged"]) && !empty($_POST["physicallychallenged"])) {		
                                                                                if (isset($_POST["coursecode"]) && !empty($_POST["coursecode"])) {
                                                                                    if (isset($_POST["batchcode"]) && !empty($_POST["batchcode"])) {
																						if (isset($_POST["packagecode"]) && !empty($_POST["packagecode"])) {
																							if (isset($_POST["categorycode"]) && !empty($_POST["categorycode"])) {
																			
																			$_LearnerName = trim($_POST["lname"]);
																			$_LearnerCode = trim($_POST["learnercode"]);
																			$_ParentName = trim($_POST["parent"]);
																			$_IdProof = trim($_POST["idproof"]);
																			$_IdNo = trim($_POST["idno"]);
																			$_DOB = trim($_POST["dob"]);
																			$_MotherTongue = trim($_POST["mothertongue"]);
																			$_Medium = trim($_POST["medium"]);
																			$_Gender = trim($_POST["gender"]);
																			$_MaritalStatus = trim($_POST["marital_type"]);
																			$_District = trim($_POST["district"]);
																			$_Tehsil = trim($_POST["tehsil"]);
																			$_Address = trim($_POST["address"]);
																			$_PIN = trim($_POST["pincode"]);
																			$_ResiPh = trim($_POST["resiph"]);
																			$_Mobile = trim($_POST["mobile"]);
																			$_Qualification = trim($_POST["qualification"]);
																			$_LearnerType = trim($_POST["learnertype"]);
																			$_PhysicallyChallenged = trim($_POST["physicallychallenged"]);
																			//$_GPFno = trim($_POST["GPFno"]);
																			$_Email = trim($_POST["email"]);
																			$_LearnerCourse = trim($_POST["coursecode"]);
																			$_LearnerBatch = trim($_POST["batchcode"]);
																			$_LearnerCoursePkg = trim($_POST["packagecode"]);
																			$_LearnerCategory = trim($_POST["categorycode"]);
																			$_LearnerFee = trim($_POST["fee"]);
																			$_LearnerInstall = trim($_POST["install"]);
																			$_Photo = $_POST["lphoto"];
																			$_Sign = $_POST["lsign"];
																			$_Scan = $_POST["lscan"];


       $response = $emp->Add($_LearnerName, $_LearnerCode, $_ParentName, $_IdProof, $_IdNo, $_DOB, $_MotherTongue, $_Medium,
		$_Gender, $_MaritalStatus, $_District, $_Tehsil, $_Address, $_ResiPh, $_Mobile, $_Qualification, $_LearnerType,
		$_PhysicallyChallenged, $_Email, $_PIN, $_LearnerCourse, $_LearnerBatch, $_LearnerCoursePkg, $_LearnerCategory,
		$_LearnerFee, $_LearnerInstall, $_Photo, $_Sign, $_Scan);
	echo $response[0];
																						} else {
                                                                                        echo "Inavalid Entry";
																						}
                                                                                     } else {
                                                                                        echo "Inavalid Entry";
																						}   
                                                                                    } else {
                                                                                        echo "Inavalid Entry";
                                                                                    }
                                                                                } else {
                                                                                    echo "Inavalid Entry";
                                                                                }
                                                                            } else {
                                                                            echo "Inavalid Entry";
                                                                        }
                                                                    } else {
                                                                        echo "Inavalid Entry";
                                                                    }
                                                                } else {
                                                                    echo "Inavalid Entry";
                                                                }
                                                            } else {
                                                                echo "Inavalid Entry";
                                                            }
                                                        } else {
                                                        echo "Inavalid Entry";
                                                    }
                                                } else {
                                                    echo "Inavalid Entry";
                                                }
                                            } else {
                                                echo "Inavalid Entry";
                                            }
                                        } else {
                                            echo "Inavalid Entry";
                                        }
                                    } else {
                                        echo "Inavalid Entry";
                                    }
                                } else {
                                    echo "Inavalid Entry";
                                }
                            } else {
                                echo "Inavalid Entry";
                            }
                        } else {
                            echo "Inavalid Entry";
                        }
                    } else {
                        echo "Inavalid Entry";
                    }                
            } else {
                echo "Inavalid Entry";
            }
        } else {
            echo "Inavalid Entry";
        }
    } else {
            echo "Inavalid Entry";
        }
	 }
}


if ($_action == "UPDATE") {
	//print_r($_POST);
	//print_r($_FILES);
	
	$_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_photo/';
	$_UploadDirectory2 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_sign/';
	
	$_LearnerName = $_POST["txtlname"];
	$_ParentName = $_POST["txtfname"];
	$_DOB = $_POST["dob"];                        
	$_Code = $_POST['txtAdmissionCode'];
	$_GeneratedId = $_POST['txtGenerateId'];
	
	if($_FILES["p1"]["name"]!='') {					     
				$imag =  $_FILES["p1"]["name"];
				$newpicture = $_GeneratedId .'_photo'.'.png';
				$_FILES["p1"]["name"] = $newpicture;
	  	}
	else  {					   
			$newpicture = $_POST['txtphoto'];
		}

	if($_FILES["p1"]["name"]!='') {
		$imageinfo = pathinfo($_FILES["p1"]["name"]);		
		if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg") {
			$error_msg = "Image must be in either PNG or JPG Format";			
		 }
		else {	
			if (file_exists("$_UploadDirectory1/" .$_GeneratedId .'_photo'.'.png'))			 
				{ 
					unlink("$_UploadDirectory1/".$newpicture); 
				}
			if (file_exists($_UploadDirectory1)) {
				if (is_writable($_UploadDirectory1)) {
				  move_uploaded_file($_FILES["p1"]["tmp_name"], "$_UploadDirectory1/".$newpicture);							 
			 }
		  }
		}
	 }	
	 
	 
	 if($_FILES["p2"]["name"]!='') {					     
				$imag =  $_FILES["p2"]["name"];
				$newsign = $_GeneratedId .'_sign'.'.png';
				$_FILES["p2"]["name"] = $newsign;
	  	}
	else  {					   
			$newsign = $_POST['txtsign'];
		}

	if($_FILES["p2"]["name"]!='') {
		$imageinfo = pathinfo($_FILES["p2"]["name"]);		
		if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg") {
			$error_msg = "Image must be in either PNG or JPG Format";			
		 }
		else {	
			if (file_exists("$_UploadDirectory2/" .$_GeneratedId .'_sign'.'.png'))			 
				{ 
					unlink("$_UploadDirectory2/".$newsign); 
				}
			if (file_exists($_UploadDirectory2)) {
				if (is_writable($_UploadDirectory2)) {
				  move_uploaded_file($_FILES["p2"]["tmp_name"], "$_UploadDirectory2/".$newsign);							 
			 }
		  }
		}
	 }	
					
	$response = $emp->Update($_LearnerName, $_ParentName, $_DOB, $newpicture, $newsign, $_Code);
	echo $response[0];
	
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCodeForUpdate($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    if (mysqli_num_rows($response[2])) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_DataTable[$_i] = array("AdmissionCode" => $_Row['Admission_Code'],
                "lcode" => $_Row['Admission_LearnerCode'],
                "lname" => $_Row['Admission_Name'],
                "fname" => $_Row['Admission_Fname'],
                "dob" => $_Row['Admission_DOB'],            
                "pid" => $_Row['Admission_PID'],
                
                "uid" => $_Row['Admission_UID'],
                "address" => $_Row['Admission_Address'],
                "pin" => $_Row['Admission_PIN'],
                "mobile" => $_Row['Admission_Mobile'],
                
                "phone" => $_Row['Admission_Phone'],
                "email" => $_Row['Admission_Email'],
                "photo" => $_Row['Admission_Photo'],
                "sign" => $_Row['Admission_Sign']);
                $_i = $_i + 1;
        }
        echo json_encode($_DataTable);
    }
}

if ($_action == "Fee") {
    // print_r($_POST);
    $response = $emp->GetAdmissionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Course_Fee'];
}

if ($_action == "GENERATELEARNERCODE") {
	   $random="";
		 $random.= (mt_rand(100, 999));
         $random.= date("y");
         $random.= date("m");
         $random.= date("d");
         $random.= date("h");
         $random.= date("i");
         $random.= date("s");
         $random.= '789';
		 
    echo $random;
}


if ($_action == "Install") {
    //echo "call";
    $response = $emp->GetAdmissionInstall($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Admission_Installation_Mode'];
}

if ($_action == "FILLIntake") {   
    $response = $emp->GetIntakeAvailable($_POST['values']);    
}