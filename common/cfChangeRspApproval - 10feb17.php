<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsChangeRspApproval.php';

$response = array();
$emp = new clsChangeRspApproval();

if ($_action == "APPROVE") {
    $response = $emp->GetDatabyCode($_POST['values']);
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if($co){
    while ($_Row = mysqli_fetch_array($response[2])) {
       
        $_DataTable[$_i] = array("orgname" => $_Row['Rspitgk_Rspname'],
            "rspcode" => $_Row['Rspitgk_Rspcode'],
            "regno" => $_Row['Rspitgk_Rspregno'],
            "fdate" => $_Row['Rspitgk_Rspestdate'],
            "orgtype" => $_Row['Rspitgk_Rsporgtype'],
            "district" => $_Row['Rspitgk_Rspdistrict'],
            "tehsil" => $_Row['Rspitgk_Rsptehsil'],
            "street" => $_Row['Rspitgk_Rspstreet'],
            "selectreason" => $_Row['Rspitgk_RspSelectReason'],
            "reason" => $_Row['Rspitgk_Reason'],
            "road" => $_Row['Rspitgk_Rsproad']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
} else {
        echo "";
    }
}

if ($_action == "CDetails") {
    $response = $emp->GetCenterDetails($_POST['values']);
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if($co){
    while ($_Row = mysqli_fetch_array($response[2])) {
       
        $_DataTable[$_i] = array("cname" => $_Row['Organization_Name'],
            "cmobile" => $_Row['User_MobileNo'],
            "cdistrict" => $_Row['District_Name'],
            "ctehsil" => $_Row['Tehsil_Name']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
} else {
        echo "";
    }
}

if ($_action == "RSPDetails") {
    $response = $emp->GetRSPDetails($_POST['values']);
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if($co){
    while ($_Row = mysqli_fetch_array($response[2])) {
       
        $_DataTable[$_i] = array("rname" => $_Row['Organization_Name'],
            "rmobile" => $_Row['Organization_FoundedDate'],
            "rdistrict" => $_Row['District_Name'],
            "rtehsil" => $_Row['Tehsil_Name']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
} else {
        echo "";
    }
}

if ($_action == "SHOWHISTORY") {

    //echo "Show";
    $response = $emp->GetAllHistory($_POST['values']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='60%'>RSP Name</th>";
    echo "<th style='10%'>RSP District</th>";
    echo "<th style='40%'>RSP Tehsil</th>";
    echo "<th style='40%'>RSP Change Request Date</th>";
    echo "<th style='10%'>RSP Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Rspitgk_ItgkCode'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspname'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspdistrict'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rsptehsil'] . "</td>";            
            echo "<td>" . $_Row['Rspitgk_Date'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Status'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='10%'>RSP Name</th>";
    echo "<th style='40%'>RSP District</th>";
    echo "<th style='10%'>RSP Tehsil</th>";
    echo "<th style='10%'>RSP Change Request Date</th>";
    echo "<th style='10%'>RSP Approval Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Rspitgk_ItgkCode'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspname'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspdistrict'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rsptehsil'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Date'] . "</td>";
            
            if ($_Row['Rspitgk_Status'] == 'Pending') {
                echo "<td> <a href='frmapproversp.php?Code=" . $_Row['Rspitgk_ItgkCode'] . "&Mode=Add'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='Approve'/></a>"
            . "</td>";
            }elseif ($_Row['Rspitgk_Status'] == 'Approved') {
                echo "<td>" . 'Approved' . "</td>";
            } else {
                echo "<td>" . 'Rejected' . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "UPDATE") {
    if (isset($_POST["centercode"])) {
        $_CenterCode = $_POST["centercode"];
        $_Status = $_POST["status"];
        $_Remark = $_POST["remark"];
        $_RspCode = $_POST["rspcode"];

        $response = $emp->Update($_CenterCode,$_Status,$_Remark,$_RspCode);
        echo $response[0];
    }
}