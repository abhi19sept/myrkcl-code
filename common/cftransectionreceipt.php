<?php

/*
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clstransectionreceipt.php';

$response = array();
$emp = new clstransectionauditreport();


if ($_action == "GETDATA") {

    //$output = "Show";
    $data = $emp->GetAll($_POST['transaction_id']);
    if (!empty($data['txnid'])) {
    require_once 'mpdf60/mpdf.php';
    $output = "<br>";
    $output .= "<br>";
    $output .= "<div class='row'>";
    $output .= "<div class='col-md-8 col-md-offset-2'>";
    $output .= "<div class='panel panel-success'>";
    $output .= " <div class='panel-heading'>";
    $output .= "    <h3 class='panel-title'>Payment Status</h3>";
    $output .= "  </div>";
    $output .= "  <div class='panel-body'>";
    $output .= " <table class='table table-hover table-bordered' border='1' width='100%' >";
    $output .= "  <tr class=''>";
    $output .= "    <td class='info' colspan='2' align='center'>";
    $output .= "Payment transaction is <b>" . ucwords($data['status']) . ".</b>";
    $output .= "</td>";
    $output .= "  </tr>";
    $output .= "  <tr class=''>";
    $output .= "    <td class=''>";
    $output .= "Transaction ID ";

    $output .= "</td>";
    $output .= "    <td class=''>";
    $output .= "" . $data['txnid'] . "";
    $output .= "</td>";
    $output .= "  </tr>";
    $output .= "  <tr class=''>";
    $output .= "    <td class='' nowrap>";

    $output .= "Payment transaction of Rs. ";
    $output .= "</td>";
    $output .= "    <td class=''>";
    $output .= "" . $data['amount'] . "";
    $output .= "</td>";
    $output .= "  </tr>";
    $output .= "  <tr class=''>";
    $output .= "    <td class=''>";
    $output .= "Payment For";
    $output .= "</td>";
    $output .= "    <td class=''>";
    $output .= "" . $data['productinfo'] . "";
    $output .= "</td>";
    $output .= "  </tr>";
    $output .= "  <tr class=''>";
    $output .= "    <td class=''>";
    $output .= "Center Code";
    $output .= "</td>";
    $output .= "    <td class=''>";
    $output .= "" . $data['udf1'] . "";
    $output .= "</td>";
    $output .= "  </tr>";
    $output .= "  <tr class=''>";
    $output .= "    <td class=''>";
    $output .= "User Name";
    $output .= "</td>";
    $output .= "    <td class=''>";
    $output .= "" . $data['firstname'] . "";
    $output .= "</td>";
    $output .= "  </tr>";
    $output .= "  <tr class=''>";
    $output .= "    <td class=''>";
    $output .= "Learner Count";
    $output .= "</td>";
    $output .= "    <td class=''>";
    $output .= "" . $data['lcount'] . "";
    $output .= "</td>";
    $output .= "  </tr>";
    $output .= "  <tr class=''>";
    $output .= "    <td class=''>";
    $output .= "Learner Codes";
    $output .= "</td>";
    $output .= "    <td class=''>";
    $output .= "" . $data['lcodes'] . "";
    $output .= "</td>";
    $output .= "  </tr>";
    $output .= "  <tr class=''>";
    $output .= "    <td class=''>";
    $output .= "Date";
    $output .= "</td>";
    $output .= "    <td class=''>";
    $date1 = $data['datetime'];
    $output .= "$date1";
    $output .= "</td>";
    $output .= "  </tr>";
    $output .= "</table>";
    $output .= "  </div>";
    $output .= "</div>";
    $output .= "</div>";
    $output .= "</div>";
    
	$filepath = 'upload/receipts/receipt_'. $data['txnid']. '.pdf';
	$path = $_SERVER['DOCUMENT_ROOT'] . '/' . $filepath ;

	$dirpath = $_SERVER['DOCUMENT_ROOT'] . '/upload/receipts';
	makeDir($dirpath);
    $logo = "<div><img src='" . $_SERVER['DOCUMENT_ROOT'] . "/images/logo1.jpg' /></div>";
	$mpdf = new mPDF('c','A4','','' ,12, 12, 12, 12, 12, 12);
	$mpdf->SetDisplayMode('fullpage');
	$mpdf->list_indent_first_level = 0;
	$mpdf->WriteHTML($stylesheet,1);
	$mpdf->WriteHTML($logo . $output, 2);
	$mpdf->Output($path, 'F');

	$output .= "<p><center><input class='hide-from-printer' type='button' value='Print' onclick='window.print()'> <a href='" . $filepath . "' target='_blank'><IMG src='images/pdf.png' height='25'></a></center></p>";
	
	} else {
		$output = "<div class='alert alert-danger' role='alert'><b>Invalid User Input / No Record Found !</b></div>";
	}
	
	echo $output;
}

function makeDir($path)
{
     return is_dir($path) || mkdir($path);
}

