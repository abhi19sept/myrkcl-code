<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
//require 'commonFunction.php';
set_include_path(dirname(__FILE__) . '/../');
$_action = (isset($_POST["action"]) ? $_POST["action"] : '');
$_actionvalue = (isset($_POST["values"]) ? $_POST["values"] : '');
if (!isset($_SESSION)) {
    session_start();
}
require 'BAL/clsGstInvoiceRpt.php';

$response = array();
$emp = new clsGstInvoiceRpt();

if (isset($_REQUEST['uid'])) {

    $bcodearray = $_REQUEST['uid'];
    $startdate = $_REQUEST['sdate'];
    $enddate = $_REQUEST['edate'];
    $batchname = $_REQUEST['batchname'];



    $filename = "GST_Invoice_LearnerFee_" . $batchname . "_" . $startdate . "to" . $enddate . ".csv";
    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));
    // $response = $emp->Learner_fee($startdate, $enddate, $bcodearray);
    $fp = fopen('php://output', 'w');
        if ($_SESSION['User_UserRoll'] == '1') {
            $response = $emp->Learner_fee_admin($startdate, $enddate, $bcodearray);

            $header = array("Invoice No.", "Amount", "Invoice Date", "Invoice Status", "Admission Code", "Transaction ID");

            header('Content-type: application/csv');
            header('Content-Disposition: attachment; filename=' . $filename);
            fputcsv($fp, $header);

            while ($row = mysqli_fetch_row($response[2])) {

                fputcsv($fp, $row);
            }

            exit;
        }
        else {
            $response = $emp->Learner_fee($startdate, $enddate, $bcodearray);
            $header = array("Invoice No.", "Amount", "Invoice Date", "Invoice Status");

            header('Content-type: application/csv');
            header('Content-Disposition: attachment; filename=' . $filename);
            fputcsv($fp, $header);

            while ($row = mysqli_fetch_row($response[2])) {

                fputcsv($fp, $row);
            }

            exit;
        }
}

if ($_action == "learner_fee") {

    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];
    $ccode = $_POST['ddlCourse'];


    $bcodearray = '';
    foreach ($_POST["ddlBatch"] as $row) {
        $bcodearray .= $row . ', ';
    }
    $bcodearray = substr($bcodearray, 0, -2);
    $batchnameresponse = $emp->GetBatchName($_POST["ddlBatch"][0]);
    $batchname = mysqli_fetch_array($batchnameresponse[2]);

    if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4') {

        echo "<a href='common/cfGstInvoiceRpt.php?action=abc&uid=" . $bcodearray . "&sdate=" . $startdate . "&edate=" . $enddate . "&batchname=" . $batchname[0] . "' style='display:none;'>"
        . "<input type='button' name='Approve' id='Approve' class='approvalLetter btn btn-primary' value='Download'/></a>";
    } else {


        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<th >S No.</th>";
        echo "<th >Invoice No.</th>";
//    echo "<th>Learner Code</th>";
//    echo "<th>ITGK Code</th>";
        echo "<th>Amount</th>";
        echo "<th>Invoice Date</th>";
        echo "<th >Invoice Status</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";


        $response = $emp->Learner_fee($startdate, $enddate, $bcodearray);
        $co = mysqli_num_rows($response[2]);
        if ($co) {
            $_Count = 1;
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $_Row['Invoice_No'] . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
                echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
                echo "<td>" . $_Row['Invoice_Date'] . "</td>";
                echo "<td>" . $_Row['Invoice_Status'] . "</td>";
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }
    }
    //  }
}
if ($_action == "reexam_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];
    // $ccode = $_POST['ddlCourse'];
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<th >S No.</th>";
    echo "<th >Invoice No.</th>";
//    echo "<th>Learner Code</th>";
//    echo "<th>ITGK Code</th>";
    echo "<th>Amount</th>";
    echo "<th>Invoice Date</th>";
    echo "<th >Invoice Status</th>";
    if($_SESSION['User_UserRoll'] == '1'){
    echo "<th >Transaction ID</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));

    $response = $emp->reexam_fee($startdate, $enddate);

    $co = mysqli_num_rows($response[2]);
    if ($co) {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Invoice_No'] . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
            echo "<td>" . $_Row['Invoice_Date'] . "</td>";
            echo "<td>" . $_Row['Invoice_Status'] . "</td>";
            echo "<td>" . $_Row['reexam_TranRefNo'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
    //  }
}
if ($_action == "NCR_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<th >S No.</th>";
    echo "<th >Invoice No.</th>";
//    echo "<th>Learner Code</th>";
//    echo "<th>ITGK Code</th>";
    echo "<th>Amount</th>";
    echo "<th>Invoice Date</th>";
    echo "<th >Invoice Status</th>";
    if($_SESSION['User_UserRoll'] == '1'){
    echo "<th >Transaction ID</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));
    $response = $emp->NCR_fee($startdate, $enddate);
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Invoice_No'] . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
            echo "<td>" . $_Row['Invoice_Date'] . "</td>";
            echo "<td>" . $_Row['Invoice_Status'] . "</td>";
            if($_SESSION['User_UserRoll'] == '1'){
            echo "<td>" . $_Row['Org_TranRefNo'] . "</td>";
        }
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
    //  }
}
if ($_action == "correction_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<th >S No.</th>";
    echo "<th >Invoice No.</th>";
//    echo "<th>Learner Code</th>";
//    echo "<th>ITGK Code</th>";
    echo "<th>Amount</th>";
    echo "<th>Invoice Date</th>";
    echo "<th >Invoice Status</th>";
    if($_SESSION['User_UserRoll'] == '1'){
    echo "<th >Transaction ID</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));
    $response = $emp->correction_fee($startdate, $enddate);
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Invoice_No'] . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
            echo "<td>" . $_Row['Invoice_Date'] . "</td>";
            echo "<td>" . $_Row['Invoice_Status'] . "</td>";
            if($_SESSION['User_UserRoll'] == '1'){
            echo "<td>" . $_Row['Correction_TranRefNo'] . "</td>";
        }
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
    //  }
}
if ($_action == "name_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<th >S No.</th>";
    echo "<th >Invoice No.</th>";
//    echo "<th>Learner Code</th>";
//    echo "<th>ITGK Code</th>";
    echo "<th>Amount</th>";
    echo "<th>Invoice Date</th>";
    echo "<th >Invoice Status</th>";
    if($_SESSION['User_UserRoll'] == '1'){
    echo "<th >Transaction ID</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));
    $response = $emp->name_fee($startdate, $enddate);
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Invoice_No'] . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
            echo "<td>" . $_Row['Invoice_Date'] . "</td>";
            echo "<td>" . $_Row['Invoice_Status'] . "</td>";
            if($_SESSION['User_UserRoll'] == '1'){
            echo "<td>" . $_Row['fld_transactionID'] . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
    //  }
}
if ($_action == "eoi_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<th >S No.</th>";
    echo "<th >Invoice No.</th>";
//    echo "<th>Learner Code</th>";
//    echo "<th>ITGK Code</th>";
    echo "<th>Amount</th>";
    echo "<th>Invoice Date</th>";
    echo "<th >Invoice Status</th>";
    if($_SESSION['User_UserRoll'] == '1'){
    echo "<th >Transaction ID</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));
    $response = $emp->eoi_fee($startdate, $enddate);
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Invoice_No'] . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
            echo "<td>" . $_Row['Invoice_Date'] . "</td>";
            echo "<td>" . $_Row['Invoice_Status'] . "</td>";
            if($_SESSION['User_UserRoll'] == '1'){
            echo "<td>" . $_Row['Courseitgk_TranRefNo'] . "</td>";
        }
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
    //  }
}

if ($_action == "RenewalPenalty_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<th >S No.</th>";
    echo "<th >Invoice No.</th>";
//    echo "<th>Learner Code</th>";
//    echo "<th>ITGK Code</th>";
    echo "<th>Amount</th>";
    echo "<th>Invoice Date</th>";
    echo "<th >Invoice Status</th>";
    if($_SESSION['User_UserRoll'] == '1'){
    echo "<th >Transaction ID</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));
    $response = $emp->RenewalPenalty_fee($startdate, $enddate);
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Invoice_No'] . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
            echo "<td>" . $_Row['Invoice_Date'] . "</td>";
            echo "<td>" . $_Row['Invoice_Status'] . "</td>";
            if($_SESSION['User_UserRoll'] == '1'){
            echo "<td>" . $_Row['RenewalPenalty_Trans_Id'] . "</td>";
        }
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
    //  }
}

if ($_action == "OwnerChange_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<th >S No.</th>";
    echo "<th >Invoice No.</th>";
//    echo "<th>Learner Code</th>";
//    echo "<th>ITGK Code</th>";
    echo "<th>Amount</th>";
    echo "<th>Invoice Date</th>";
    echo "<th >Invoice Status</th>";
    if($_SESSION['User_UserRoll'] == '1'){
    echo "<th >Transaction ID</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));
    $response = $emp->OwnerChange_fee($startdate, $enddate);
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Invoice_No'] . "</td>";
//            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
//            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
            echo "<td>" . $_Row['Invoice_Date'] . "</td>";
            echo "<td>" . $_Row['Invoice_Status'] . "</td>";
                if($_SESSION['User_UserRoll'] == '1'){
            echo "<td>" . $_Row['Org_TranRefNo'] . "</td>";
        }
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
    //  }
}

if ($_action == "CorrectionBfrExam_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<th >S No.</th>";
    echo "<th >Invoice No.</th>";
    echo "<th>Amount</th>";
    echo "<th>Invoice Date</th>";
    echo "<th >Invoice Status</th>";
    if($_SESSION['User_UserRoll'] == '1'){
    echo "<th >Transaction ID</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));
    $response = $emp->CorrectionBfrExam_fee($startdate, $enddate);
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Invoice_No'] . "</td>";
            echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
            echo "<td>" . $_Row['Invoice_Date'] . "</td>";
            echo "<td>" . $_Row['Invoice_Status'] . "</td>";
            if($_SESSION['User_UserRoll'] == '1'){
            echo "<td>" . $_Row['Adm_Log_TranRefNo'] . "</td>";
        }
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
    //  }
}
if ($_action == "RSCFACert_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<th >S No.</th>";
    echo "<th >Invoice No.</th>";
    echo "<th>Amount</th>";
    echo "<th>Invoice Date</th>";
    echo "<th >Invoice Status</th>";
    if($_SESSION['User_UserRoll'] == '1'){
    echo "<th >Transaction ID</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));
    $response = $emp->RSCFACert_fee($startdate, $enddate);
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Invoice_No'] . "</td>";
            echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
            echo "<td>" . $_Row['Invoice_Date'] . "</td>";
            echo "<td>" . $_Row['Invoice_Status'] . "</td>";
            if($_SESSION['User_UserRoll'] == '1'){

            echo "<td>" . $_Row['apply_rscfa_cert_txnid'] . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
    //  }
}
if ($_action == "RSCFAReexam_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<th >S No.</th>";
    echo "<th >Invoice No.</th>";
    echo "<th>Amount</th>";
    echo "<th>Invoice Date</th>";
    echo "<th >Invoice Status</th>";
    if($_SESSION['User_UserRoll'] == '1'){
    echo "<th >Transaction ID</th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));
    $response = $emp->RSCFAReexam_fee($startdate, $enddate);
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Invoice_No'] . "</td>";
            echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
            echo "<td>" . $_Row['Invoice_Date'] . "</td>";
            echo "<td>" . $_Row['Invoice_Status'] . "</td>";
        if($_SESSION['User_UserRoll'] == '1'){

             echo "<td>" . $_Row['rscfa_reexam_TranRefNo'] . "</td>";
         }
            // echo "<td>" . $_Row['Invoice_Status'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
    //  }
}