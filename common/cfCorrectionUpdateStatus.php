<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCorrectionUpdateStatus.php';

$response = array();
$emp = new clsCorrectionUpdateStatus();


if ($_action == "FILLOTFORUPDATESTATUS") {
    $response = $emp->FILLOTFORUPDATESTATUS();
	   print_r($response);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['lotid'] . ">" . $_Row['lotname'] . "</option>";
    }
}

if ($_action == "FILLSTATUSFORUPDATE") {
    $response = $emp->FILLSTATUSFORUPDATE();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}

if ($_action == "ShowDetailsToUpdate") {
    //echo "Show";
    $response = $emp->ShowDetailsToUpdate($_POST['status'], $_POST['lotid']);

    $_DataTable = "";
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>Center Code</th>";
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";
	echo "<th style='20%'>Father Name</th>";	
    echo "<th style='20%'>D.O.B</th>";
	echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	$_Total = 0; 
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Correction_ITGK_Code'] . "</td>";
        echo "<td>" . $_Row['lcode'] . "</td>";		 
			$fname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($_Row['cfname']))));
        echo "<td>" . $fname . "</td>";
			$faname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($_Row['cfaname']))));
		echo "<td>" . $faname . "</td>";
        echo "<td>" . $_Row['dob'] . "</td>";
        
		if($_Row['dispatchstatus'] == '3') {
					echo "<td>"
					. "<input type='button' name='Pending' id='Pending' class='btn btn-primary' value='Pending for Processing'/>"
					. "</td>";
				}
		else if($_Row['dispatchstatus'] == '0') {
			echo "<td><input type='checkbox' id=chk" . $_Row['cid'] .
            " name=chk" . $_Row['cid'] . "></input></td>";
		}
		
		else if($_Row['dispatchstatus'] == '4') {
					echo "<td>"
					. "<input type='button' name='Rejected' id='Rejected' class='btn btn-primary' value='Rejected by RKCL'/>"
					. "</td>";
				}
		else if($_Row['dispatchstatus'] == '1') {
				echo "<td><input type='checkbox' id=chk" . $_Row['cid'] .
				" name=chk" . $_Row['cid'] . "></input></td>";
			}		
		else {
				echo "<td>"
				. "<input type='button' name='Delivered' id='Delivered' class='btn btn-primary' value='Delivered to DLC'/>"
				. "</td>";
		}		
		
        echo "</tr>";
		 //$_Total = $_Total + $_Row['trnamount'];
         $_Count++;
       }
	
		echo "</tbody>";
		echo "</table>";
		echo "</div>";
}

if ($_action == "ADD")
	{    
       //print_r($_POST);
	   $_Status = $_POST["ddlstatus"];
	   $_Update_Status = "";
	      if($_Status == "0") {
			  $_Update_Status = '1';
		  }
		   else {
			   $_Update_Status = '2';
		   }
	$_UserPermissionArray=array();
	$_CorrectionId = array();
	$_i = 0;
        $_Count=0;
		$l="";
			foreach ($_POST as $key => $value)
			{
               if(substr($key, 0,3)=='chk')
               {
                   $_UserPermissionArray[$_Count]=array(
                       "Function" => substr($key, 3),
                       "Attribute"=>3,
                       "Status"=>1);
                   $_Count++;
					$l .= substr($key, 3) . ",";
				}
			   $_CorrectionId = rtrim($l, ",");						
			}
				$response=$emp->Update_LearnerStatus($_CorrectionId, $_Update_Status);		
				echo $response[0];   	   
	}
	
	
	if ($_action == "ShowDetailsToDownload") {
    //echo "Show";
    $response = $emp->ShowDetailsToDownload($_POST['status'], $_POST['lotid']);

    $_DataTable = "";
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>Center Code</th>";
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";
	echo "<th style='20%'>Father Name</th>";	
    echo "<th style='20%'>D.O.B</th>";	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	   $_Total = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Correction_ITGK_Code'] . "</td>";
        echo "<td>" . $_Row['lcode'] . "</td>";		 
			$fname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($_Row['cfname']))));
        echo "<td>" . $fname . "</td>";
			$faname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($_Row['cfaname']))));
		echo "<td>" . $faname . "</td>";
        echo "<td>" . $_Row['dob'] . "</td>";		
		
        echo "</tr>";
		 //$_Total = $_Total + $_Row['trnamount'];
         $_Count++;
    }
		echo "</tbody>";
		echo "</table>";
		echo "</div>";
}

	if ($_action == "Download")
	{
		$response = $emp->GetLearnerPhoto($_POST['ddlstatus'], $_POST['ddllot']);
		$_Count=0;
		$l="";
		$n="";
		while($_Row = mysqli_fetch_array($response[2])) {
			$lphotodoc = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_photo/'.$_Row['photo'];
				if (file_exists($lphotodoc)) {
					$l .= "http://myrkcl.com/upload/correction_photo/".$_Row['photo'] . ",";
				}
				else {
					$n .=$_Row['photo']. ",";
				}
			
		}
		   $_LearnerCode = rtrim($l, ",");
		   $_NotAvailable = rtrim($n, ",");
		   //$_LearnerCode = "http://localhost/myrkcladmin/upload/correction_photo/".$_Learner;
		   $_SESSION['LearnerPhoto'] = $_LearnerCode;
		   $_SESSION['NotAvailable'] = $_NotAvailable;
		   echo "yes";
	}
?>