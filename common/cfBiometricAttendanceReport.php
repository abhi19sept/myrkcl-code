<?php

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
//require 'commonFunction.php';
set_include_path(dirname(__FILE__) . '/../');
$_action = (isset($_POST["action"]) ? $_POST["action"] : '');
$_actionvalue = (isset($_POST["values"]) ? $_POST["values"] : '');
if (!isset($_SESSION)) {
    session_start();
}
require 'BAL/clsBiometricAttendanceReport.php';

$response = array();
$emp = new clsBiometricAttendanceReport();

if (isset($_REQUEST['uid'])) {

    $batchcode = $_REQUEST['uid'];
    $coursecode = $_REQUEST['coursecode'];
    $sdate = $_REQUEST['sdate'];
    $edate = $_REQUEST['edate'];
    $response = $emp->SHOWData($batchcode,$coursecode,$sdate,$edate);


    $filename = "Biometric_Women_" . $batchcode . ".csv";
    $fp = fopen('php://output', 'w');

    $header = array("ITGK", "LearnerCode", "LearnerName", "FatherHusbandName","Attendance_Count", "Attendance_Count_Online", "Attendance_Count_Offline");

    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename=' . $filename);
    fputcsv($fp, $header);

    while ($row = mysqli_fetch_row($response[2])) {

        fputcsv($fp, array($row['1'], $row['2'], $row['3'], $row['4'], $row['5'], $row['6'], $row['7']), ',', '"');
        // fputcsv($fp, $row);
    }

    exit;
}

// if (isset($_REQUEST['uid'])) {

//     $batchcode = $_REQUEST['uid'];
//     $coursecode = $_REQUEST['coursecode'];
//     $response = $emp->SHOWData($batchcode,$coursecode);


//     $filename = "Biometric_Women_" . $batchcode . ".csv";
//     $fp = fopen('php://output', 'w');

//     $header = array("ITGK", "LearnerCode", "LearnerName", "FatherHusbandName", "Attendance_Count");

//     header('Content-type: application/csv');
//     header('Content-Disposition: attachment; filename=' . $filename);
//     fputcsv($fp, $header);

//     while ($row = mysqli_fetch_row($response[2])) {

//         $responseL = $emp->GetAttendance($batchcode, $row[0]);
//         $rowL = mysqli_fetch_row($responseL[2]);
//         // $rownew = array_merge($row,$rowL);
//                fputcsv($fp, array($row['1'], $row['2'], $row['3'], $row['4'], $rowL['1']), ',', '"');
        
//         // fputcsv($fp, $rownew);
//     }

//     exit;
// }
if ($_action == "GETDETAILS") {

    if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '17' || $_SESSION['User_UserRoll'] == '8' || $_SESSION['User_UserRoll'] == '14') {

        $batchc = $_POST['batchcode'];
        $CourseCode = $_POST['coursecode'];
        $sdate = $_POST['sdate'];
        $edate = $_POST['edate'];
        echo "<a href='common/cfBiometricAttendanceReport.php?action=abc&uid=" . $batchc . "&coursecode=". $CourseCode . "&sdate=". $sdate . "&edate=". $edate . "' style='display:none;'>"
        . "<input type='button' name='Approve' id='Approve' class='approvalLetter btn btn-primary' value='Download'/></a>";
    } elseif ($_SESSION['User_UserRoll'] == '7') {
        $batchcode = $_POST['batchcode'];
        $CourseCode = $_POST['coursecode'];
                $sdate = $_POST['sdate'];
        $edate = $_POST['edate'];
        $response = $emp->SHOWDataITGK($batchcode, $CourseCode,$sdate,$edate);
        $_DataTable = "";

        echo "<div class='table-responsive' style='margin-top:10px'>";

        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='15%'>S No.</th>";
        echo "<th style='30%'>ITGK Code</th>";
        echo "<th style='25%'>Learner Code/Application No. </th>";
        echo "<th style='25%'>Learner Name </th>";
        echo "<th style='25%'>Father / Husband Name</th>";
        // echo "<th style='25%'>Enrollment Status </th>";
        echo "<th style='25%'>Total Attendance Count </th>";
        // echo "<th style='25%'>65% Attendance (43 Count ) Completed</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;

        if ($response[0] == 'Success') {
            while ($_Row = mysqli_fetch_array($response[2])) {

                if ($_Row['Admission_ITGK_Code'] != '') {
                    echo "<tr class='odd gradeX'>";
                    echo "<td class='count'>" . $_Count . "</td>";
                    echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
                    echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
                    echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
                    echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
                    // $status = '';
                    // if ($_Row['BioMatric_Status'] == '1') {
                    //     $status = 'Enrolled';
                    // } else if ($_Row['BioMatric_Status'] == '0') {
                    //     $status = 'Not Enrolled';
                    // }
                    // echo "<td>" . $status . "</td>";
                    echo "<td>" . $_Row['Attendance_Count'] . "</td>";
                    // $attendancecompleted = '';
                    // if ($_Row['Attendance_Count'] >= '43') {
                    //     $attendancecompleted = 'YES';
                    // } else if ($_Row['Attendance_Count'] < '43' && $_Row['Attendance_Count'] != '') {
                    //     $attendancecompleted = 'NO';
                    // }
                    // echo "<td>" . $attendancecompleted . "</td>";
                    echo "</tr>";


                    $_Count++;
                }
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        }
    }
}

if ($_action == "FILLBatch") {
    $CourseCode = $_POST['coursecode'];
    $response = $emp->GetBatch($CourseCode);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}