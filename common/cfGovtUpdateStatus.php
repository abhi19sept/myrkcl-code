<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsGovtUpdateStatus.php';

$response = array();
$emp = new clsGovtUpdateStatus();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll($_POST['status']);

    $_DataTable = "";
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>Center Code</th>";
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";
    echo "<th style='20%'>Father/Husband Name</th>";
    echo "<th style='20%'>Department</th>";
    echo "<th style='20%'>Designation</th>";
    echo "<th style='5%'>Office Address</th>";
	echo "<th style='5%'>Reimbursement Amount</th>";
	echo "<th style='5%'>Lot</th>";
    //echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	   $_Total = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Govemp_ITGK_Code'] . "</td>";
        echo "<td>" . $_Row['learnercode'] . "</td>";		 
			$fname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['fname']))));
        echo "<td>" . $fname . "</td>";
			$faname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['faname']))));
        echo "<td>" . $faname . "</td>";
        echo "<td>" . $_Row['gdname'] . "</td>";
        echo "<td>" . $_Row['designation'] . "</td>";
		echo "<td>" . $_Row['officeaddress'] . "</td>";
        echo "<td>" . $_Row['trnamount'] . "</td>";
       echo "<td>" . $_Row['lotname'] . "</td>";
        echo "</tr>";
		 $_Total = $_Total + $_Row['trnamount'];
        $_Count++;
    }
    echo "</tbody>";
	 echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
		 echo "<th >  </th>";
		  echo "<th >  </th>";
		   echo "<th >  </th>";
		    echo "<th >  </th>";
			echo "<th >  </th>";
		    echo "<th >  </th>";
        echo "<th >Total Amount </th>";
        echo "<th>";
        echo "$_Total";
        echo "</th>";
		 echo "<th >  </th>";
        echo "</tr>";
        echo "</tfoot>";
    echo "</table>";
	 echo "</div>";
}

if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Status</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Function_Code'] . ">" . $_Row['Function_Name'] . "</option>";
    }
}

if ($_action == "FILLOTFORUPDATESTATUS") {
    $response = $emp->FILLOTFORUPDATESTATUS();
	   print_r($response);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['lotid'] . ">" . $_Row['lotname'] . "</option>";
    }
}

if ($_action == "FILLSTATUSFORUPDATE") {
    $response = $emp->FILLSTATUSFORUPDATE();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}

if ($_action == "ShowDetailsToUpdate") {
    //echo "Show";
    $response = $emp->ShowDetailsToUpdate($_POST['status'], $_POST['lotid']);

    $_DataTable = "";
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>Center Code</th>";
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";
	echo "<th style='20%'>Father/Husband Name</th>";	
    echo "<th style='20%'>District</th>";
	echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	   $_Total = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Govemp_ITGK_Code'] . "</td>";
        echo "<td>" . $_Row['learnercode'] . "</td>";		 
			$fname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['fname']))));
        echo "<td>" . $fname . "</td>";
			$faname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['faname']))));
		echo "<td>" . $faname . "</td>";
        echo "<td>" . $_Row['ldistrictname'] . "</td>";
        
		if($_Row['trnpending'] == '3') {
					echo "<td>"
					. "<input type='button' name='Pending' id='Pending' class='btn btn-primary' value='Pending for Processing'/>"
					. "</td>";
				}
		else if($_Row['trnpending'] == '0') {
			echo "<td><input type='checkbox' id=chk" . $_Row['learnercode'] .
            " name=chk" . $_Row['learnercode'] . "></input></td>";
		}
		
		else if($_Row['trnpending'] == '4') {
					echo "<td>"
					. "<input type='button' name='Rejected' id='Rejected' class='btn btn-primary' value='Rejected by RKCL'/>"
					. "</td>";
				}
		else if($_Row['trnpending'] == '5') {
				echo "<td><input type='checkbox' id=chk" . $_Row['learnercode'] .
				" name=chk" . $_Row['learnercode'] . "></input></td>";
			}		
		else {
				echo "<td>"
				. "<input type='button' name='Payment Reimbursed' id='Payment Reimbursed' class='btn btn-primary' value='Payment Reimbursed'/>"
				. "</td>";
		}		
		
        echo "</tr>";
		 //$_Total = $_Total + $_Row['trnamount'];
         $_Count++;
    }
		echo "</tbody>";
		echo "</table>";
		echo "</div>";
}

if ($_action == "ADD")
	{    
       //print_r($_POST);
	   $_Status = $_POST["ddlstatus"];
	   $_Update_Status = "";
	      if($_Status == "0") {
			  $_Update_Status = '5';
		  }
		   else {
			   $_Update_Status = '6';
		   }
	$_UserPermissionArray=array();
	$_LearnerCode = array();
	$_i = 0;
        $_Count=0;
		$l="";
			foreach ($_POST as $key => $value)
			{
               if(substr($key, 0,3)=='chk')
               {
                   $_UserPermissionArray[$_Count]=array(
                       "Function" => substr($key, 3),
                       "Attribute"=>3,
                       "Status"=>1);
                   $_Count++;
					$l .= substr($key, 3) . ",";
				}
			   $_LearnerCode = rtrim($l, ",");						
			}
				$response=$emp->Update_LearnerStatus($_LearnerCode, $_Update_Status);		
				echo $response[0];   	   
	}
	
?>