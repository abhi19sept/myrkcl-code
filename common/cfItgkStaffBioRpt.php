<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsItgkStaffBioRpt.php';

$response = array();
$emp = new clsItgkStaffBioRpt();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
        
    echo "<th style='40%'>Staff Name</th>";
    echo "<th style='40%'>Staff Designation</th>";
    echo "<th style='40%'>Staff Gender</th>";
    
    echo "<th style='40%'>Staff Mobile</th>";    
    echo "<th style='40%'>Staff Email</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2],MYSQLI_ASSOC)) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['User_LoginId'] . "</td>";
             echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['fullname'] . "</td>";
            echo "<td>" . $_Row['employee_type'] . "</td>";
            echo "<td>" . $_Row['gender'] . "</td>";
            echo "<td>" . $_Row['mobile'] . "</td>";
            
            echo "<td>" . $_Row['email_id'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

