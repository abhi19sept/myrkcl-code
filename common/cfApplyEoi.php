<?php

/*
 * author Mayank

 */
include './commonFunction.php';
require 'BAL/clsApplyEoi.php';

$response = array();
$emp = new clsApplyEoi1();


if ($_action == "ADD") {
    if (isset($_POST["cname"])) {
        $_CourseName = $_POST["cname"];
        $_CourseCode = $_POST["EoiCCode"];
      //  $_ITGKCode = filter_var($_POST["itgkcode"], FILTER_SANITIZE_STRING);
        $_EOICode = filter_var($_POST["eoiname"], FILTER_SANITIZE_STRING);
       // $_EOIStartDate = filter_var($_POST["startdate"], FILTER_SANITIZE_STRING);
       // $_EOIEndDate = filter_var($_POST["enddate"], FILTER_SANITIZE_STRING);

//        $_Status=$_POST["status"];

        $response = $emp->Add($_CourseName, $_CourseCode, $_EOICode);
        echo $response[0];
    }
}

if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value=''>Select EOI</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['EOI_Code'] . ">" . $_Row['EOI_Name'] . "</option>";
    }
}

if ($_action == "FILLEOIPayment") {
    $response = $emp->GetEOI($_POST['eoicode']);
    echo "<option value=''>Select EOI</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['EOI_Code'] . ">" . $_Row['EOI_Name'] . "</option>";
    }
}

if ($_action == "FILLEOICourse") {
    $response = $emp->GetEOICourse();
    echo "<option value=''>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Courseitgk_EOI'] . ">" . $_Row['Courseitgk_Course'] . "</option>";
    }
}

if ($_action == "FILLEOICourseCode") {
    $response = $emp->GetEOICourseCode($_POST['code']);
    //echo "<option value=''>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_POST['values'], $_POST['paymode'], $_POST['eoicode']);
    //print_r($response);
    $_DataTable = array();
    $_i = 0;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("EOIName" => $_Row['EOI_Name'],
                "Ecode" => $_Row['EOI_Code'],
                "PFees" => $_Row['EOI_ProcessingFee'],
                "CFees" => $_Row['EOI_CourseFee']
            );
            $_i = $_i + 1;
        }
        if (!empty($_Datatable[0]['Ecode'])) {
            $_Datatable[0]['txnid'] = $emp->initiateEOIPayTran($_Datatable[0], $_POST);
        }
        echo json_encode($_Datatable);
    } else {
        echo "0";
    }
}

if ($_action == "GETEOIDetails") {
    if ($_POST['values'] == '0') {
        echo "0";
    } else {
        $response = $emp->GETEOIDetails($_POST['values']);
        //print_r($response);
        $_DataTable = array();
        $_i = 0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("EOIName" => $_Row['EOI_Name'],
                "Ecode" => $_Row['EOI_Code'],
                "PFees" => $_Row['EOI_ProcessingFee'],
                "CFees" => $_Row['EOI_CourseFee']);
            $_i = $_i + 1;
        }
        echo json_encode($_Datatable);
    }
}

if ($_action == "FILLEOIBYCOURSE") {
    $response = $emp->GetEOIbycourse($_POST['values']);
    echo "<option value='0'>Select EOI</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['EOI_Code'] . ">" . $_Row['EOI_Name'] . "</option>";
    }
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
    //print_r($response);
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
    //values
    $key = "X2ZPKM";
    $salt = "8IaBELXB";
    $command1 = "check_isDomestic";   //validatecardnumber   luhn algo	
    $var1 = $_POST['values']; // Payu ID (mihpayid) of transaction

    $hash_str = $key . '|' . $command1 . '|' . $var1 . '|' . $salt;
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key, 'hash' => $hash, 'command' => $command1, 'var1' => $var1);

    $qs = http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
    $response = curlCall($wsUrl, $qs, TRUE);
    //echo "<pre>"; print_r($response); echo "</pre>"; 
    $_DataTable = array();
    $_i = 0;
    $_DataTable[$_i] = array("cardType" => $response['cardType'],
        "cardCategory" => $response['cardCategory']);
    echo json_encode($_DataTable);
}

if ($_action == "FinalValidateCard") {
    //values
    $key = "X2ZPKM";
    $salt = "8IaBELXB";
    $command1 = "validateCardNumber";   //validatecardnumber   luhn algo
    $var1 = $_POST['values']; // Payu ID (mihpayid) of transaction

    $hash_str = $key . '|' . $command1 . '|' . $var1 . '|' . $salt;
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key, 'hash' => $hash, 'command' => $command1, 'var1' => $var1);

    $qs = http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
    $response = curlCall($wsUrl, $qs, TRUE);

    if ($response == 'Invalid') {
        echo "1";
    } else if ($response == 'valid') {
        echo "2";
    }
}

function curlCall($wsUrl, $qs, $true) {
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
    if (curl_errno($c)) {
        $c_error = curl_error($c);
        if (empty($c_error)) {
            $c_error = 'Some server error';
        }
        return array('curl_status' => 'FAILURE', 'error' => $c_error);
    }
    $out = trim($o);
    $arr = json_decode($out, true);
    return $arr;
}
