<?php
include './commonFunction.php';
require 'BAL/clsLearningsubscription.php';

$response = array();
$emp = new clsRHLSReport();

if ($_POST['action'] == "ViewRHLSReport") {
    $ddlstartdate = $_POST['ddlstartdate'];
    $ddlenddate = $_POST['ddlenddate'];
    $response = $emp->ShowRHLSReport($ddlstartdate,$ddlenddate);
    echo "<div class='table-responsive'>";
    echo  "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Company Name</th>";
    echo "<th>GISTN No.</th>";
    echo "<th>Candidate Name</th>";
    echo "<th>Ack. No.</th>";
    echo "<th>Email</th>";
    echo "<th>Mobile Number</th>";
    echo "<th>Address</th>";
    echo "<th>State</th>";             
    echo "<th>District</th>";             
    echo "<th>City</th>";             
    echo "<th>Postal Code</th>";             
    echo "<th>Amount</th>";             
    echo "<th>Payment Status</th>";             
    echo "<th>Delivery Status</th>";             
    echo "<th>Applied Time</th>";             
    echo "<th>Payment Time</th>";             
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>"; 
            echo "<td>" .strtoupper($row['Subs_User_Company_Name']). "</td>";
            echo "<td>" .$row['Subs_User_Gstin_no']. "</td>";
            echo "<td>" .strtoupper($row['Subs_User_First_Name']). "</td>";
            echo "<td>" .$row['Subs_User_Code']. "</td>";
            echo "<td>" .strtoupper($row['Subs_User_Email']). "</td>";
            echo "<td>" .$row['Subs_User_Phone']. "</td>";
            echo "<td>" .strtoupper($row['Subs_User_Address']). "</td>";
            echo "<td>" .strtoupper($row['State_Name']). "</td>";
            echo "<td>" .strtoupper($row['District_Name']). "</td>";
            echo "<td>" .strtoupper($row['Subs_User_City']). "</td>";
            echo "<td>" .$row['Subs_User_Postal_Code']. "</td>";
            echo "<td>" .$row['Subs_User_Amount']. "</td>";
            if($row['Subs_User_Pay_Status'] == 1){ $payment = "Payment Done"; }else{ $payment = "Payment Not Done"; }
            echo "<td>" .$payment. "</td>";
            if($row['Subs_User_Del_Status'] == 1){ $delev = "Delivery Done"; }else{ $delev = "Not Delivered"; }
            echo "<td>" .$delev. "</td>";
            echo "<td>" .$row['Subs_User_Applied_Time']. "</td>";
            echo "<td>" .$row['Subs_User_Payment_Time']. "</td>";
            echo "</tr>";
            $_Count++;
        }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";  
    } 
    
}

?>