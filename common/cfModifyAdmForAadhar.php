<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsModifyAdmForAadhar.php';
require 'DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();
$response = array();
$emp = new clsModifyAdmForAadhar();


if ($_action == "SHOWALL") {
    global  $_ObjFTPConnection;
    $response = $emp->GetAll($_POST['batch'], $_POST['course']);
    $_DataTable = "";
    if ($response[0] == 'Success') {
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th>Learner Code</th>";
        echo "<th>Learner Name</th>";
        echo "<th>Father/Husband Name</th>";
        echo "<th>D.O.B</th>";
        echo "<th>Mobile</th>";
        echo "<th>Photo</th>";
        echo "<th>Sign</th>";
        echo "<th>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        $arr = explode("|", $_SESSION['User_ParentId']);
        //print_r($arr);
        $PSA_CODE = $arr[0];
        $DLC_CODE = $arr[1];

        $responsegetBatchName = $emp->getBatchName($_POST['batch']);
        $_RowBatchName = mysqli_fetch_array($responsegetBatchName[2]);
        $_LearnerBatchNameFTP = $_ObjFTPConnection->LearnerBatchNameFTP($_POST['course'],$_RowBatchName['Batch_Name']);

        while ($_Row = mysqli_fetch_array($response[2])) {

            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            $image = $_Row['Admission_Photo'];

            if ($_Row['Admission_Photo'] != "") {
               
                echo "<td >"
                . "<input type='button' name='Edit' class='btn btn-primary view_photo' batchname='".$_LearnerBatchNameFTP."'
            image='".$image."' id='".$_Count.'_photo'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphoto_".$_Count."'> </div>"
            ."</td>";
            } else {
                echo "<td id='".$_Count.'_photo'."'>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }

            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>"
                . "<input type='button' name='Edit' class='btn btn-primary view_sign' batchname='".$_LearnerBatchNameFTP."'
            image='".$sign."' count='".$_Count."' id='".$_Count.'_sign'."' value='View Sign'/>". "<div style='display:none;' id='Viewsign_".$_Count."'> </div>".
            "</td>";
            } else {
                echo "<td id='".$_Count.'_sign'."'>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            }
            

            echo "<td> <a href='frmeditlearnerforaadhar.php?code=" . $_Row['Admission_Code'] . "&Mode=Edit'  target='_blank'>"
            . "<img src='images/editicon.png' alt='Edit' width='30px' height='25px'/></a>  "
            . "</td>";

            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "DELETE") {
    //print_r($_POST);
    $value = $_POST['values'];
    $batch = $_POST['batchcode'];

    $response = $emp->DeleteRecord($value, $batch);

    echo $response[0];
}

if ($_action == "GetLearner") {

    global $_ObjFTPConnection;

    $response = $emp->GetLearnerDetails($_POST['values']);

    $_DataTable = "";

    $_Row = mysqli_fetch_array($response[2]);
    $_LearnerBatchNameFTP = $_ObjFTPConnection->LearnerBatchNameFTP($_Row['Admission_Course'],$_Row['Batch_Name']);
    $ftpaddress = $_ObjFTPConnection->ftpPathIp();
    $AdmissionPhotoFile = $ftpaddress.'admission_photo/' . $_LearnerBatchNameFTP . '/' . $_Row['Admission_Photo'];
     
    $ch = curl_init($AdmissionPhotoFile);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch,CURLOPT_TIMEOUT,5000);
    curl_exec($ch);
    $responseCodePhoto = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    echo "<div align='center' class='col-md-2 col-lg-2'>";
    echo "<div style='float:left'>";
    if($responseCodePhoto == 200){

    $details= $_ObjFTPConnection->ftpdetails();

    $image =  file_get_contents($details.'admission_photo/'.$_LearnerBatchNameFTP.'/'.$_Row['Admission_Photo']);
    $imageData = base64_encode($image);

        echo "<img style='width:80px; height:107px;' class='img-circle img-responsive' src='data:image/png;base64, ".$imageData."' alt='Red dot' class='img-circle img-responsive'/><br>";
    }
    else{
        echo "<img style='width:80px; height:107px;' class='img-circle img-responsive' src='upload/admission_photo/" . $_Row['Admission_Photo'] . "'><br>";
    }
    echo "</div>";
    echo "<div style='float:left'>";
    echo "<input type='button' style='float:right; margin-left: 5px;' class='btn btn-warning btn-xs editbtnsreset' id='btnlphotoupdreset' name='btnlphotoupdreset' value='Reset'><input type='button' style='float:right' class='btn btn-danger btn-xs editbtns' id='btnlphotoupd' name='btnlphotoupd' value='Edit'>";
    echo "</div>";
    echo "</div>";

    echo "<div class='table-responsive col-md-8 col-lg-8'>";

    echo "<table class='table table-bordered table-user-information'>";
    echo "<tbody>";

    echo "<tr>";
    echo "<td style='5%'>Learner Code:</td>";
    echo "<td style='30%'>" . $_Row['Admission_LearnerCode'] . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td style='5%'>Learner Name:</td>";
    echo "<td  style='30%'>" . strtoupper($_Row['Admission_Name']) . "  <input type='button' style='float:right; margin-left: 5px;' class='btn btn-warning btn-xs editbtnsreset' id='btnlnameupdreset' name='btnlnameupdreset' value='Reset'><input type='button' style='float:right' class='btn btn-danger btn-xs editbtns' id='btnlnameupd' name='btnlnameupd' value='Edit'></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td style='5%'>Father Name</td>";
    echo "<td  style='10%'>" . strtoupper($_Row['Admission_Fname']) . " <input type='button' style='float:right; margin-left: 5px;' class='btn btn-warning btn-xs editbtnsreset' id='btnfnameupdreset' name='btnfnameupdreset' value='Reset'><input type='button' style='float:right' class='btn btn-danger btn-xs editbtns' id='btnfnameupd' name='btnfnameupd' value='Edit'></td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td style='5%'>Date of Birth:</td>";
    echo "<td style='5%'>" . date("F d, Y", strtotime($_Row['Admission_DOB'])) . " <input type='button' style='float:right; margin-left: 5px;' class='btn btn-warning btn-xs editbtnsreset' id='btndobupdreset' name='btndobupdreset' value='Reset'><input type='button' style='float:right' class='btn btn-danger btn-xs editbtns' id='btndobupd' name='btndobupd' value='Edit'></td>";
    echo "</tr>";


    echo "<tr>";
    echo "<td style='5%'>Course Name</td>";
    echo "<td style='10%'>" . $_Row['Course_Name'] . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td style='5%'>Batch Name</td>";
    echo "<td style='10%'>" . $_Row['Batch_Name'] . "</td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td style='5%'>ITGK Code</td>";
    echo "<td style='10%'>" . $_Row['Admission_ITGK_Code'] . "</td>";
    echo "</tr>";
    echo "<tr>";

    echo "<td style='5%'>Aadhar No</td>";
    echo "   <td>" . strtoupper(($_Row['Admission_UID'] == '') ? 'Not Available' : $_Row['Admission_UID']) . " <input type='button' style='float:right; margin-left: 5px;' class='btn btn-warning btn-xs editbtnsreset' id='btnaadharupdreset' name='btnaadharupdreset' value='Reset'><input type='button' style='float:right' class='btn btn-danger btn-xs editbtns' id='btnaadharupd' name='btnaadharupd' value='Edit'></td>";
    echo "</tr>";
    echo "</table>";
    echo " <input type='hidden' id='learnercode' name='learnercode' value='" . $_Row['Admission_LearnerCode'] . "'>";
    echo " <input type='hidden' id='lnameold' name='lnameold' value='" . $_Row['Admission_Name'] . "'>";
    echo " <input type='hidden' id='fnameold' name='fnameold' value='" . $_Row['Admission_Fname'] . "'>";
    echo " <input type='hidden' id='ldobold' name='ldobold' value='" . $_Row['Admission_DOB'] . "'>";
    echo " <input type='hidden' id='itgkcode' name='itgkcode' value='" . $_Row['Admission_ITGK_Code'] . "'>";
    echo " <input type='hidden' id='uidold' name='uidold' value='" . $_Row['Admission_UID'] . "'>";
    echo " <input type='hidden' id='course' name='course' value='" . $_Row['Admission_Course'] . "'>";
    echo " <input type='hidden' id='batch' name='batch' value='" . $_Row['Admission_Batch'] . "'>";
    echo "</div>";
}
if ($_action == "GetLearnerExsist") {
    global $_ObjFTPConnection;

    $response = $emp->GetLearnerDetailsExist($_POST['values']);

    $_DataTable = "";

    $_Row = mysqli_fetch_array($response[2]);

    echo "<div align='center' class='col-md-2 col-lg-2'>";
    echo "<div style='float:left'>";
    $details= $_ObjFTPConnection->ftpdetails();

    $image =  file_get_contents($details.'AdmissionCorrectionAfterPayment/'.$_Row['Adm_Log_Photo']);
    $imageData = base64_encode($image);

        

    if ($_Row['Adm_Log_IsUpd_Photo'] == '1') {
        echo "<img style='width:80px; height:107px;' class='img-circle img-responsive' src='data:image/png;base64, ".$imageData."' alt='Red dot' class='img-circle img-responsive'/><br>";
    }
    else{
        echo "<label>Photo Not Changed</label>";
    }


    echo "</div>";
    echo "</div>";

    echo "<div class='table-responsive col-md-10 col-lg-10'>";

    echo "<table class='table table-bordered table-user-information'>";
    echo "<tbody>";
    echo "<tr>";
    echo "<td style='5%'>Learner Code:</td>";
    echo "<td style='5%'>Learner Name:</td>";
    echo "<td style='5%'>Father Name</td>";
    echo "<td style='5%'>Date of Birth:</td>";
    echo "<td style='5%'>Aadhar No</td>";
    echo "<td style='5%'>Approval Status</td>";
    echo "<td style='5%'>OnHold Remarks</td>";
    echo "<td style='5%'>Application Resubmit Status</td>";
    echo "</tr>";
    echo "<tr>";

    echo "<td style='30%'>" . $_Row['Adm_Log_AdmissionLcode'] . "</td>";

    echo "<td  style='30%'>" . ($_Row['Adm_Log_IsUpd_Name'] == '1' ? strtoupper($_Row['Adm_Log_Lname_New']) : strtoupper($_Row['Adm_Log_Lname_Old'])) . "</td>";

    echo "<td  style='10%'>" . ($_Row['Adm_Log_IsUpd_Fname'] == '1' ? strtoupper($_Row['Adm_Log_Fname_New']) : strtoupper($_Row['Adm_Log_Fname_Old'])) . "</td>";

    echo "<td style='5%'>" . ($_Row['Adm_Log_IsUpd_Dob'] == '1' ? date('Y-m-d', strtotime($_Row['Adm_Log_Dob_New'])) : date('Y-m-d', strtotime($_Row['Adm_Log_Dob_Old']))) . "</td>";
    echo "   <td>" . ($_Row['Adm_Log_IsUpd_Uid'] == '1' ? $_Row['Adm_Log_Uid_New'] : $_Row['Adm_Log_Uid_Old']) . "</td>";

    if ($_Row['Adm_Log_ApproveStatus'] == '1') {

        echo "<td>Approved on " . date('F d, Y', strtotime($_Row['Adm_log_ApproveDate'])) . "</td>";
    } elseif ($_Row['Adm_Log_ApproveStatus'] == '0' && $_Row['Adm_log_ReApplyDate'] == '') {

        echo "<td>Pending on " . date("F d, Y", strtotime($_Row['Adm_Log_ApplyDate'])) . "</td>";
    } elseif ($_Row['Adm_Log_ApproveStatus'] == '0' && $_Row['Adm_log_ReApplyDate'] != '') {

        echo "<td>Pending on " . date("F d, Y", strtotime($_Row['Adm_log_ReApplyDate'])) . "</td>";
    } elseif ($_Row['Adm_Log_ApproveStatus'] == '2') {

        echo "<td>On Hold " . date("F d, Y", strtotime($_Row['Adm_log_ApproveDate'])) . "</td>";
    }
    echo "<td  style='10%'>" . ($_Row['Adm_Log_ApproveRemark'] != '' ? strtoupper($_Row['Adm_Log_ApproveRemark']) : '') . "</td>";
    echo "<td  style='10%'>" . ($_Row['Adm_log_ReApplyDate'] != '' ? "ReApply on " . date("F d, Y", strtotime($_Row['Adm_log_ReApplyDate'])) : '') . "</td>";

    echo "</tr>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "UpdLearner") {
    //print_r($_POST["txtlname");
// die;
    $_UploadDirectory1 = '/AdmissionCorrectionAfterPayment';
    if ($_POST["flaguidupd"] == 1) {
        if (strcasecmp(trim($_POST["txtlname"]), trim($_POST["uidname"])) == 0) {
            if (strcasecmp(trim(date('Y-m-d', strtotime($_POST["dob"]))), trim($_POST["uiddob"])) == 0) {
                if (isset($_POST["lcode"])) {
                    $_learnercode = $_POST["learnercode"];
                    if (trim($_POST["txtlname"]) == '') {
                        $_NewName = $_POST["lnameold"];
                        $n = '0';
                    } else {
                        $_NewName = $_POST["txtlname"];
                        $n = '1';
                    }
                    if (trim($_POST["txtfname"]) == '') {
                        $_NewFname = $_POST["fnameold"];
                        $fn = '0';
                    } else {
                        $_NewFname = $_POST["txtfname"];
                        $fn = '1';
                    }
                    if (trim($_POST["dob"]) == '') {
                        $_NewDob = $_POST["dob"];
                        $d = '0';
                    } else {
                        $_NewDob = date('Y-m-d', strtotime($_POST["dob"]));
                        $d = '1';
                    }
                    if (trim($_POST["txtACno"]) == '') {
                        $_newUid = $_POST["uidold"];
                        $u = '0';
                    } else {
                        $_newUid = $_POST["txtACno"];
                        $u = '1';
                    }
                    if (!empty($_FILES['uploadImage1']['tmp_name'])) {

                        $pic = '1';
                        $lphotoimg = $_FILES['uploadImage1']['name'];
                        $lphototmp = $_FILES['uploadImage1']['tmp_name'];
                        $lphototemp = explode(".", $lphotoimg);
                        $lphotofilename = $_POST["learnercode"] . '_photo.png';
                        $lphotofilestorepath = "../upload/AdmissionCorrectionAfterPayment/" . $lphotofilename;
                        $lphotoimageFileType = pathinfo($lphotofilestorepath, PATHINFO_EXTENSION);
                    } else {

                        $pic = '0';
                        $lphotofilename = '';
                    }
//                $_NewName = ($_POST["newname"] == '' ? $_POST["lnameold"] AND $n = '0' : $_POST["newname"]);
//                $_NewFname = ($_POST["newfname"] == '' ? $_POST["fnameold"] : $_POST["newfname"]);
                    $_oldName = $_POST["lnameold"];
                    $_oldFname = $_POST["fnameold"];
                    $_oldDob = $_POST["ldobold"];
                    $_Lcode = $_POST["lcode"];
                    $_itgkcode = $_POST["itgkcode"];
//                $_NewDob = ($_POST["newdob"] == '' ? $_POST["ldobold"] : date('Y-m-d', strtotime($_POST["newdob"])));
                    $_oldUid = $_POST["uidold"];
//                $_newUid = $_POST["uidnew"];
                    $course = $_POST["course"];
                    $batch = $_POST["batch"];

                    $docprooftype = $_POST["ddlidproof"];


                    $docproofimg = $_FILES['uploadImage7']['name'];
                    $docprooftmp = $_FILES['uploadImage7']['tmp_name'];
                    $docprooftemp = explode(".", $docproofimg);
                    $docprooffilename = $_POST["learnercode"] . '_DocProof.' . end($docprooftemp);
                    $docprooffilestorepath = "../upload/AdmissionCorrectionAfterPayment/" . $docprooffilename;
                    $docproofimageFileType = pathinfo($docprooffilestorepath, PATHINFO_EXTENSION);

                    if ($pic == '1') {
                        if (($_FILES["uploadImage1"]["size"] < 6000 && $_FILES["uploadImage1"]["size"] > 2000)) {

                            if ($lphotoimageFileType == "png" || $lphotoimageFileType == "jpg" || $lphotoimageFileType == "jpeg") {
                                    $response_ftp=ftpUploadFile($_UploadDirectory1,$lphotofilename,$_FILES["uploadImage1"]["tmp_name"]);

                                    if(trim($response_ftp) == "SuccessfullyUploaded"){
                                       $imageuploaded = '1';
                                    }

                                    else {
                                        $imageuploaded = '0';
                                        echo "Image not upload. Please try again.";
                                    }

                                // if (move_uploaded_file($lphototmp, $lphotofilestorepath)) {
                                //     $imageuploaded = '1';
                                // } else {
                                //     $imageuploaded = '0';
                                //     echo "Image not upload. Please try again.";
                                // }
                            } else {
                                echo "Sorry, File Not Valid";
                            }
                        } else {
                            echo "Photo File Size Incorrect.";
                        }
                    }
                    if (($_FILES["uploadImage7"]["size"] < 200000 && $_FILES["uploadImage7"]["size"] > 100000)) {

                        if ($docproofimageFileType == "pdf") {
                            $response_ftp2=ftpUploadFile($_UploadDirectory1,$docprooffilename,$_FILES["uploadImage7"]["tmp_name"]);
                            if(trim($response_ftp2) == "SuccessfullyUploaded"){
                            // if (move_uploaded_file($docprooftmp, $docprooffilestorepath)) {
                                if ($pic == '1') {
                                    if ($imageuploaded == '1') {
                                        $responses = $emp->Addtolog($_learnercode, $_NewName, $_NewFname, $_NewDob, $_Lcode, $_oldName, $_oldFname, $_oldDob, $_itgkcode, $_oldUid, $_newUid, $course, $batch, $n, $fn, $d, $u, $docprooftype, $docprooffilename, $lphotofilename, $pic);
                                        //print_r($responses); 
                                        echo $responses[0];
                                    } else {
                                        echo "Image not upload. Please try again.";
                                    }
                                } else {
                                    $responses = $emp->Addtolog($_learnercode, $_NewName, $_NewFname, $_NewDob, $_Lcode, $_oldName, $_oldFname, $_oldDob, $_itgkcode, $_oldUid, $_newUid, $course, $batch, $n, $fn, $d, $u, $docprooftype, $docprooffilename, $lphotofilename, $pic);
                                    echo $responses[0];
                                }
                            } else {
                                // echo $lphotofilestorepath;
                                // echo $docprooffilestorepath;
                                echo "Documents not upload. Please try again.";
                            }
                        } else {
                            echo "Sorry, File Not Valid";
                        }
                    } else {
                        echo "Doc File Size Incorrect.";
                    }
                    // $responses = $emp->Addtolog($_learnercode, $_NewName, $_NewFname, $_NewDob, $_Lcode, $_oldName, $_oldFname, $_oldDob, $_itgkcode, $_oldUid, $_newUid, $course, $batch, $n, $fn, $d, $u);
                    // //print_r($responses); 
                    // echo $responses[0];
                }
            } else {
                echo "DOB Not Matched as registered in Aadhar No.";
            }
        } else {
            echo "Learner Name Not Matched as registered in Aadhar No.";
        }
    } elseif ($_POST["flaguidupd"] == 0) {

        if (isset($_POST["lcode"])) {
            $_learnercode = $_POST["learnercode"];
            if (trim($_POST["txtlname"]) == '') {
                $_NewName = $_POST["lnameold"];
                $n = '0';
            } else {
                $_NewName = $_POST["txtlname"];
                $n = '1';
            }
            if (trim($_POST["txtfname"]) == '') {
                $_NewFname = $_POST["fnameold"];
                $fn = '0';
            } else {
                $_NewFname = $_POST["txtfname"];
                $fn = '1';
            }
            if (trim($_POST["dob"]) == '') {
                $_NewDob = $_POST["ldobold"];
                $d = '0';
            } else {
                $_NewDob = date('Y-m-d', strtotime($_POST["dob"]));
                $d = '1';
            }
            if (trim($_POST["txtACno"]) == '') {
                $_newUid = $_POST["uidold"];
                $u = '0';
            } else {
                $_newUid = $_POST["txtACno"];
                $u = '1';
            }
            if (!empty($_FILES['uploadImage1']['tmp_name'])) {

                $pic = '1';
                $lphotoimg = $_FILES['uploadImage1']['name'];
                $lphototmp = $_FILES['uploadImage1']['tmp_name'];
                $lphototemp = explode(".", $lphotoimg);
                $lphotofilename = $_POST["learnercode"] . '_photo.png';
                $lphotofilestorepath = "../upload/AdmissionCorrectionAfterPayment/" . $lphotofilename;
                $lphotoimageFileType = pathinfo($lphotofilestorepath, PATHINFO_EXTENSION);
            } else {

                $pic = '0';
                $lphotofilename = '';
            }
//                $_NewName = ($_POST["newname"] == '' ? $_POST["lnameold"] AND $n = '0' : $_POST["newname"]);
//                $_NewFname = ($_POST["newfname"] == '' ? $_POST["fnameold"] : $_POST["newfname"]);
            $_oldName = $_POST["lnameold"];
            $_oldFname = $_POST["fnameold"];
            $_oldDob = $_POST["ldobold"];
            $_Lcode = $_POST["lcode"];
            $_itgkcode = $_POST["itgkcode"];
//                $_NewDob = ($_POST["newdob"] == '' ? $_POST["ldobold"] : date('Y-m-d', strtotime($_POST["newdob"])));
            $_oldUid = $_POST["uidold"];
//                $_newUid = $_POST["uidnew"];
            $course = $_POST["course"];
            $batch = $_POST["batch"];
            $docprooftype = $_POST["ddlidproof"];


            $docproofimg = $_FILES['uploadImage7']['name'];
            $docprooftmp = $_FILES['uploadImage7']['tmp_name'];
            $docprooftemp = explode(".", $docproofimg);
            $docprooffilename = $_POST["learnercode"] . '_DocProof.' . end($docprooftemp);
            $docprooffilestorepath = "../upload/AdmissionCorrectionAfterPayment/" . $docprooffilename;
            $docproofimageFileType = pathinfo($docprooffilestorepath, PATHINFO_EXTENSION);

            if ($pic == '1') {
                if (($_FILES["uploadImage1"]["size"] < 6000 && $_FILES["uploadImage1"]["size"] > 2000)) {

                    if ($lphotoimageFileType == "png" || $lphotoimageFileType == "jpg" || $lphotoimageFileType == "jpeg") {
                        $response_ftp3=ftpUploadFile($_UploadDirectory1,$lphotofilename,$_FILES["uploadImage1"]["tmp_name"]);

                        if(trim($response_ftp3) == "SuccessfullyUploaded"){
                           $imageuploaded = '1';
                        }
                        // if (move_uploaded_file($lphototmp, $lphotofilestorepath)) {
                        //     $imageuploaded = '1';
                        // } else {
                        //     $imageuploaded = '0';
                        //     echo "Image not upload. Please try again.";
                        // }
                        else {
                            $imageuploaded = '0';
                            echo "Image not upload. Please try again.";
                        }
                    } else {
                        echo "Sorry, File Not Valid";
                    }
                } else {
                    echo "Photo File Size Incorrect.";
                }
            }
            if (($_FILES["uploadImage7"]["size"] < 200000 && $_FILES["uploadImage7"]["size"] > 100000)) {

                if ($docproofimageFileType == "pdf") {
                    $response_ftp4=ftpUploadFile($_UploadDirectory1,$docprooffilename,$_FILES["uploadImage7"]["tmp_name"]);
                    if(trim($response_ftp4) == "SuccessfullyUploaded"){
                    // if (move_uploaded_file($docprooftmp, $docprooffilestorepath)) {
                        if ($pic == '1') {
                            if ($imageuploaded == '1') {
                                $responses = $emp->Addtolog($_learnercode, $_NewName, $_NewFname, $_NewDob, $_Lcode, $_oldName, $_oldFname, $_oldDob, $_itgkcode, $_oldUid, $_newUid, $course, $batch, $n, $fn, $d, $u, $docprooftype, $docprooffilename, $lphotofilename, $pic);
                                //print_r($responses); 
                                echo $responses[0];
                            } else {
                                echo "Image not upload. Please try again.";
                            }
                        } else {
                            $responses = $emp->Addtolog($_learnercode, $_NewName, $_NewFname, $_NewDob, $_Lcode, $_oldName, $_oldFname, $_oldDob, $_itgkcode, $_oldUid, $_newUid, $course, $batch, $n, $fn, $d, $u, $docprooftype, $docprooffilename, $lphotofilename, $pic);
                            echo $responses[0];
                        }
                    } else {
                        // echo $lphotofilestorepath;
                        // echo $docprooffilestorepath;
                        echo "Documents not upload. Please try again.";
                    }
                } else {
                    echo "Sorry, File Not Valid";
                }
            } else {
                echo "Doc File Size Incorrect.";
            }
        }
    }
}


if ($_action == "GETDATAapprove") {
    //echo "Show";


    $response = $emp->GetAllPref($_POST['batchcode']);
    
    if ($response[0] == 'Success') {

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    $_LoginUserRole = $_SESSION['User_UserRoll'];

    echo "<th>ITGK Code</th>";

    echo "<th>Applicant Count</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    while ($_Row = mysqli_fetch_array($response[2])) {

        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Adm_Log_ITGK_Code'] . "</td>";
        
        echo "<td><a href='frmApproveEditAdmList.php?itgk=" . $_Row['Adm_Log_ITGK_Code'] . "&batch=" . $_POST['batchcode'] . "&course=" . $_POST['coursecode'] ."' target='_blank'>" . $_Row['cnt'] . "</a></td>";

        echo "</tr>";

        $_Count++;
    }
    echo "</tbody>";

    echo "</table>";
    echo "</div>";
    }
}



if ($_action == "GETLEARNERLIST") {

global $_ObjFTPConnection;
    $response = $emp->GetLearnerList($_POST['itgkcode'], $_POST['batchcode']);

    $_DataTable = "";
        $responsegetBatchName = $emp->getBatchName($_POST['batchcode']);
        $_RowBatchName = mysqli_fetch_array($responsegetBatchName[2]);
        $_LearnerBatchNameFTP = $_ObjFTPConnection->LearnerBatchNameFTP($_POST['course'],$_RowBatchName['Batch_Name']);
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th colspan='19'><div class='w3-row'>
            <div class='w3-tile w3-win-phone-red' >Color Code:</div>
            <div class='w3-tile w3-win-phone-red' style='background-color:#ff4d4d'>Full Change</div>
            <div class='w3-tile w3-win-phone-green' style='background-color:#ffbf80'>Major Change</div>
            <div class='w3-tile w3-win-phone-blue' style='background-color:#ffff4d'>Medium Change</div>
            <div class='w3-tile w3-win-phone-yellow'  style='background-color:#66ff66'>Minor Change</div>
            <div class='w3-tile w3-win-phone-yellow'  style='border: 1px solid;'>No Change</div>
            </div></th>";
    echo "</thead>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>Sr. No.</th>";
    echo "<th>IT-GK</th>";
    echo "<th>Learner Code</th>";
        echo "<th>Learner Admission Batch</th>";
    echo "<th>Existing Learner Photo</th>";
    echo "<th>Edited Learner Photo</th>";
    echo "<th>Existing Learner Name</th>";
    echo "<th>Edited Learner Name</th>";
    echo "<th>Existing Father Name</th>";
    echo "<th >Edited Father Name</th>";
    echo "<th >Existing DOB</th>";
    echo "<th >Edited DOB</th>";
    echo "<th >Existing Aadhar</th>";
    echo "<th >Edited Aadhar</th>";
    echo "<th >Correction History</th>";
    echo "<th > Application Date</th>";
    echo "<th > Application Status</th>";
    echo "<th > Doc Proof</th>";
    echo "<th style='width: 155px!important;'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $oldname = strtoupper($_Row['Adm_Log_Lname_Old']);
            $newname = strtoupper($_Row['Adm_Log_Lname_New']);
            $oldfname = strtoupper($_Row['Adm_Log_Fname_Old']);
            $newfname = strtoupper($_Row['Adm_Log_Fname_New']);
            $lcode = $_Row['Adm_Log_AdmissionLcode'];
            $itgkcode = $_Row['Adm_Log_ITGK_Code'];
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $itgkcode . "</td>";
            echo "<td>" . $lcode . "</td>";
                        echo "<td>" . $_Row['Batch_Name'] . "</td>";
            similar_text($oldname, $newname, $percent);
            //echo round($percent);
            // $percent = number_format((float) $percent, 2, '.', '');
            // echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='upload/admission_photo/" . $_Row['Adm_Log_AdmissionLcode'] . "_photo.png'></td>";
            // if ($_Row['Adm_Log_IsUpd_Photo'] == '1') {
            //     echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='/upload/AdmissionCorrectionAfterPayment/" . $_Row['Adm_Log_Photo'] . "'></td>";
            // }
            // else{
            //    echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='upload/admission_photo/" . $_Row['Adm_Log_AdmissionLcode'] . "_photo.png'></td>";
            // }


            echo "<td >"
                . "<input type='button' name='Edit' class='btn btn-primary view_photo' batchname='".$_LearnerBatchNameFTP."'
            image='".$_Row['Adm_Log_AdmissionLcode']."_photo.png' id='".$_Count.'_photo'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphoto_".$_Count."'> </div>"
            ."</td>";           

            if ($_Row['Adm_Log_IsUpd_Photo'] == '1') {
               $image = $_Row['Adm_Log_Photo'];
                echo "<td >"
                . "<input type='button' name='Edit' class='btn btn-primary view_photoafter' batchname='".$_LearnerBatchNameFTP."'
            image='".$image."' id='".$_Count.'_photoafter'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphotoafter_".$_Count."'> </div>"
            ."</td>";
            } else {
                $image = $_Row['Adm_Log_AdmissionLcode'];
            echo "<td >"
                . "<input type='button' name='Edit' class='btn btn-primary view_photonoedit' batchname='".$_LearnerBatchNameFTP."'
            image='".$_Row['Adm_Log_AdmissionLcode']."_photo.png' id='".$_Count.'_photonoedit'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphotonoedit_".$_Count."'> </div>"
            ."</td>"; 
            }


            echo "<td>" . strtoupper($_Row['Adm_Log_Lname_Old']) . "</td>";
            if ($_Row['Adm_Log_IsUpd_Name'] == '1') {
                if (round($percent) <= 25) {
                    echo "<td style='background-color:#ff4d4d'>" . strtoupper($_Row['Adm_Log_Lname_New']) . "</td>";
                } elseif (round($percent) > 25 && round($percent) <= 50) {
                    echo "<td style='background-color:#ffbf80'>" . strtoupper($_Row['Adm_Log_Lname_New']) . "</td>";
                } elseif (round($percent) > 50 && round($percent) <= 75) {
                    echo "<td style='background-color:#ffff4d'>" . strtoupper($_Row['Adm_Log_Lname_New']) . "</td>";
                } elseif (round($percent) > 75) {
                    echo "<td style='background-color:#66ff66'>" . strtoupper($_Row['Adm_Log_Lname_New']) . "</td>";
                }
            } else {
                echo "<td>" . strtoupper($_Row['Adm_Log_Lname_New']) . "</td>";
            }


            echo "<td>" . strtoupper($_Row['Adm_Log_Fname_Old']) . "</td>";
            similar_text($oldfname, $newfname, $percentfname);
            //echo $percentfname;
            // die;
            //  echo $percentfname;echo "<pre>";
            if ($_Row['Adm_Log_IsUpd_Fname'] == '1') {
                if (round($percentfname) <= 25) {
                    echo "<td style='background-color:#ff4d4d'>" . strtoupper($_Row['Adm_Log_Fname_New']) . "</td>";
                } elseif (round($percentfname) > 25 && round($percentfname) <= 50) {
                    echo "<td style='background-color:#ffbf80'>" . strtoupper($_Row['Adm_Log_Fname_New']) . "</td>";
                } elseif (round($percentfname) > 50 && round($percentfname) <= 75) {
                    echo "<td style='background-color:#ffff4d'>" . strtoupper($_Row['Adm_Log_Fname_New']) . "</td>";
                } elseif (round($percentfname) > 75) {
                    echo "<td style='background-color:#66ff66'>" . strtoupper($_Row['Adm_Log_Fname_New']) . "</td>";
                }
            } else {
                echo "<td>" . strtoupper($_Row['Adm_Log_Fname_New']) . "</td>";
            }

            echo "<td>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_Old'])) . "</td>";

            if ($_Row['Adm_Log_IsUpd_Dob'] == '1') {

                echo "<td style='background-color:#ff4d4d'>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_New'])) . "</td>";
            } else {
                echo "<td>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_New'])) . "</td>";
            }
            echo "<td>" . ($_Row['Adm_Log_Uid_Old'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_Old']) . "</td>";

            if ($_Row['Adm_Log_IsUpd_Uid'] == '1') {

                echo "<td style='background-color:#ff4d4d'>" . ($_Row['Adm_Log_Uid_New'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_New']) . "</td>";
            } else {
                echo "<td>" . ($_Row['Adm_Log_Uid_New'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_New']) . "</td>";
            }
            $response1 = $emp->GetCorrectionCount($_POST['itgkcode'], $lcode);
            $_Row1 = mysqli_fetch_array($response1[2]);
            $appcount = $_Row1['appcount'];
            //  echo "<td>" . ($appcount == '' ? 'Not Available' : $appcount) . "</td>";
            echo "<td><a href='frmAdmCorrectionHistory.php?lcode=" . $lcode . "&itgkcode=" . $itgkcode . "' target='_blank'>" . ($appcount == '' ? 'Not Available' : $appcount) . "</a></td>";
            echo "<td>" . date("d-m-Y", strtotime($_Row['Adm_Log_ApplyDate'])) . "</td>";
            echo "<td>" . ($_Row['Adm_Log_ApproveStatus'] == '2' ? 'OnHold Application' : 'New Application') . "</td>";
            // echo "<td> "
            //     . "<input type='button' name='" . $_Row['Adm_Log_ITGK_Code'] . "' id='" . $_Row['Adm_Log_AdmissionLcode'] . "' class='btn btn-xs btn-primary docadm' value='" . $_Row['Adm_Log_Doc_ProofType'] . "' />"
            //     . "<p id='" . $_Row['Adm_Log_AdmissionCode'] . "p' style='display:none'>Done</p>"
            //     . "</td>";
                    
    
            $docprf =  '/AdmissionCorrectionAfterPayment/'.$_Row['Adm_Log_AdmissionLcode'].'_DocProof.pdf';
            // $docprf = base64_encode($docprf);
            echo "<td> "

                . "<a data-fancybox data-type='iframe' data-src='common/showpdfftp.php?src=".$docprf."' href='javascript:;'>Click to View </a>"
                . "<p id='" . $_Row['Adm_Log_AdmissionCode'] . "p' style='display:none'>Done</p>"
                . "</td>";
            if ($_Row['Adm_Log_ApproveStatus'] == '0') {
                echo "<td> "
                . "<input type='button' name='" . $_Row['Adm_Log_ITGK_Code'] . "' id='app" . $_Row['Adm_Log_AdmissionCode'] . "' class='btn btn-xs btn-success  btn-block approveadm' value='Approve' /><br><br>"
                . "<input type='button' name='" . $_Row['Adm_Log_ITGK_Code'] . "' id='rej" . $_Row['Adm_Log_AdmissionCode'] . "' class='btn btn-xs  btn-warning  btn-block rejectadm' value='Set OnHold' /><br><br>"
                . "<input type='button' name='" . $_Row['Adm_Log_ITGK_Code'] . "' id='reject" . $_Row['Adm_Log_AdmissionCode'] . "' class='btn btn-xs  btn-danger  btn-block rejectapplication' value='Reject' /><p id='" . $_Row['Adm_Log_AdmissionCode'] . "p' style='display:none'>Done</p>"
                . "</td>";
            } elseif ($_Row['Adm_Log_ApproveStatus'] == '1') {
                echo "<td>Approved on " . date('d-m-Y', strtotime($_Row['Adm_log_ApproveDate'])) . "</td>";
            } elseif ($_Row['Adm_Log_ApproveStatus'] == '2') {
                echo "<td>Set OnHold on " . date('d-m-Y', strtotime($_Row['Adm_log_ApproveDate'])) . "</td>";
            }
            elseif ($_Row['Adm_Log_ApproveStatus'] == '3') {
                echo "<td>Rejected on " . date('d-m-Y', strtotime($_Row['Adm_log_ApproveDate'])) . "</td>";
            }


            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}


if ($_action == "AdmEditHistory") {

    $response = $emp->GetLearnerAdmEditHistory($_POST['lcode'], $_POST['itgkcode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";

    echo "<thead>";
    echo "<tr>";
    echo "<th>Sr. No.</th>";
    echo "<th>IT-GK</th>";
    echo "<th>Learner Code</th>";
        echo "<th>Learner Admission Batch</th>";
    echo "<th>Existing Learner Photo</th>";
    echo "<th>Edited Learner Photo</th>";
    echo "<th>Existing Learner Name</th>";
    echo "<th>Edited Learner Name</th>";
    echo "<th>Existing Father Name</th>";
    echo "<th >Edited Father Name</th>";
    echo "<th >Existing DOB</th>";
    echo "<th >Edited DOB</th>";
    echo "<th >Existing Aadhar</th>";
    echo "<th >Edited Aadhar</th>";
    echo "<th >Approve Status</th>";
    echo "<th >Approve Date</th>";
    echo "<th>Approve User</th>";
    echo "<th>Payment Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $oldname = strtoupper($_Row['Adm_Log_Lname_Old']);
            $newname = strtoupper($_Row['Adm_Log_Lname_New']);
            $oldfname = strtoupper($_Row['Adm_Log_Fname_Old']);
            $newfname = strtoupper($_Row['Adm_Log_Fname_New']);
            $lcode = $_Row['Adm_Log_AdmissionLcode'];
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Adm_Log_ITGK_Code'] . "</td>";
            echo "<td>" . $lcode . "</td>";
                        echo "<td>" . $_Row['Batch_Name'] . "</td>";
            // echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='upload/admission_photo/" . $_Row['Adm_Log_AdmissionLcode'] . "_photo.png'></td>";
            // if ($_Row['Adm_Log_IsUpd_Photo'] == '1') {
            //     echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='/upload/AdmissionCorrectionAfterPayment/" . $_Row['Adm_Log_Photo'] . "'></td>";
            // }
            // else{
            //    echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='upload/admission_photo/" . $_Row['Adm_Log_AdmissionLcode'] . "_photo.png'></td>";
            // }
            $_LearnerBatchNameFTP = $_ObjFTPConnection->LearnerBatchNameFTP($_Row['Adm_Log_Course'],$_Row['Batch_Name']);
            echo "<td >"
                . "<input type='button' name='Edit' class='btn btn-primary view_photo' batchname='".$_LearnerBatchNameFTP."'
            image='".$_Row['Adm_Log_AdmissionLcode']."_photo.png' id='".$_Count.'_photo'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphoto_".$_Count."'> </div>"
            ."</td>";           

            if ($_Row['Adm_Log_IsUpd_Photo'] == '1') {
               $image = $_Row['Adm_Log_Photo'];
            echo "<td >"
                . "<input type='button' name='Edit' class='btn btn-primary view_photoafter' batchname='".$_LearnerBatchNameFTP."'
            image='".$image."' id='".$_Count.'_photoafter'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphotoafter_".$_Count."'> </div>"
            ."</td>";
            } else {
                $image = $_Row['Adm_Log_AdmissionLcode'];
            echo "<td >"
                . "<input type='button' name='Edit' class='btn btn-primary view_photonoedit' batchname='".$_LearnerBatchNameFTP."'
            image='".$_Row['Adm_Log_AdmissionLcode']."_photo.png' id='".$_Count.'_photonoedit'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphotonoedit_".$_Count."'> </div>"
            ."</td>"; 
            }          
            echo "<td>" . strtoupper($_Row['Adm_Log_Lname_Old']) . "</td>";
            if ($_Row['Adm_Log_IsUpd_Name'] == '1') {

                echo "<td style='background-color:#ff4d4d'>" . strtoupper($_Row['Adm_Log_Lname_New']) . "</td>";
            } else {
                echo "<td>" . strtoupper($_Row['Adm_Log_Lname_New']) . "</td>";
            }

            echo "<td>" . strtoupper($_Row['Adm_Log_Fname_Old']) . "</td>";

            if ($_Row['Adm_Log_IsUpd_Fname'] == '1') {

                echo "<td style='background-color:#ff4d4d'>" . strtoupper($_Row['Adm_Log_Fname_New']) . "</td>";
            } else {
                echo "<td>" . strtoupper($_Row['Adm_Log_Fname_New']) . "</td>";
            }

            echo "<td>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_Old'])) . "</td>";

            if ($_Row['Adm_Log_IsUpd_Dob'] == '1') {

                echo "<td style='background-color:#ff4d4d'>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_New'])) . "</td>";
            } else {
                echo "<td>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_New'])) . "</td>";
            }
            echo "<td>" . ($_Row['Adm_Log_Uid_Old'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_Old']) . "</td>";

            if ($_Row['Adm_Log_IsUpd_Uid'] == '1') {

                echo "<td style='background-color:#ff4d4d'>" . ($_Row['Adm_Log_Uid_New'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_New']) . "</td>";
            } else {
                echo "<td>" . ($_Row['Adm_Log_Uid_New'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_New']) . "</td>";
            }
            if ($_Row['Adm_Log_ApproveStatus'] == 1) {
                echo "<td>Approved</td>";
            } elseif ($_Row['Adm_Log_ApproveStatus'] == 2) {
                echo "<td>Set OnHold</td>";
            }
            elseif ($_Row['Adm_Log_ApproveStatus'] == 0) {
                echo "<td>Action Pending</td>";
            }
            elseif ($_Row['Adm_Log_ApproveStatus'] == 3) {
                echo "<td>Rejected</td>";
            }
            if ($_Row['Adm_Log_ApproveStatus'] == 0) {
                echo "<td> Not Available</td>";
            }
            else{
                echo "<td> " .  date('d-m-Y', strtotime($_Row['Adm_log_ApproveDate'])) . "</td>";
                }
            
            echo "<td> " . $_Row['Adm_Log_ApproveUser'] . "</td>";
            echo "<td>" . ($_Row['Adm_Log_PayStatus'] == '1' ? 'Payment Confirmed' : 'Payment Not Confirmed') . "</td>";

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "UpdateApproveLearner") {
    $_lcode = $_POST["lcode"];
    $_ITGK = $_POST["itgkcode"];

    $response = $emp->UpdateApproveLearner($_lcode, $_ITGK);
    //print_r($response);
    if ($response[0] == 'Successfully Updated') {
        $response2 = $emp->UpdateApproveAdmission($_lcode, $_ITGK);
        echo $response2[0];
    }
    else{
        echo "Something Went Wrong";
    }
    
}
if ($_action == "UpdateRejectLearner") {
    $_lcode = $_POST["lcode"];
    $_ITGK = $_POST["itgkcode"];
    $_onholdrmk = strtoupper(trim($_POST["onholdrmk"]));

    $response = $emp->UpdateRejectLearner($_lcode, $_ITGK, $_onholdrmk);
    echo $response[0];
}
if ($_action == "FinalRejectLearner") {
    $_lcode = $_POST["lcode"];
    $_ITGK = $_POST["itgkcode"];
    $rejectrmk = strtoupper(trim($_POST["rejectrmk"]));

    $response = $emp->FinalRejectLearner($_lcode, $_ITGK, $rejectrmk);
    echo $response[0];
}
if ($_action == "AdmEditHistoryAll") {
    $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
    $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
    $response = $emp->GetLearnerAdmEditHistoryAll($sdate, $edate);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";

    echo "<thead>";
    echo "<tr>";
    echo "<th>Sr. No.</th>";
    echo "<th>IT-GK</th>";
    echo "<th>Learner Code</th>";
        echo "<th>Learner Admission Batch</th>";
    echo "<th>Existing Learner Photo</th>";
    echo "<th>Edited Learner Photo</th>";
    echo "<th>Existing Learner Name</th>";
    echo "<th>Edited Learner Name</th>";
    echo "<th>Existing Father Name</th>";
    echo "<th >Edited Father Name</th>";
    echo "<th >Existing DOB</th>";
    echo "<th >Edited DOB</th>";
    echo "<th >Existing Aadhar</th>";
    echo "<th >Edited Aadhar</th>";
    echo "<th >Approve Status</th>";
    echo "<th >Approve Remarks</th>";
    echo "<th >Approve Date</th>";
    echo "<th>Approve User</th>";
    echo "<th>Payment Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $oldname = strtoupper($_Row['Adm_Log_Lname_Old']);
            $newname = strtoupper($_Row['Adm_Log_Lname_New']);
            $oldfname = strtoupper($_Row['Adm_Log_Fname_Old']);
            $newfname = strtoupper($_Row['Adm_Log_Fname_New']);
            $lcode = $_Row['Adm_Log_AdmissionLcode'];
            $_LearnerBatchNameFTP = $_ObjFTPConnection->LearnerBatchNameFTP($_Row['Adm_Log_Course'],$_Row['Batch_Name']);
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Adm_Log_ITGK_Code'] . "</td>";
            echo "<td>" . $lcode . "</td>";
                        echo "<td>" . $_Row['Batch_Name'] . "</td>";
            // echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='upload/admission_photo/" . $_Row['Adm_Log_AdmissionLcode'] . "_photo.png'></td>";
            // if ($_Row['Adm_Log_IsUpd_Photo'] == '1') {
            //     echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='/myrkcllive6/upload/AdmissionCorrectionAfterPayment/" . $_Row['Adm_Log_Photo'] . "'></td>";
            // }
            // else{
            //    echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='upload/admission_photo/" . $_Row['Adm_Log_AdmissionLcode'] . "_photo.png'></td>";
            // }
            echo "<td >123"
                . "<input type='button' name='Edit' class='btn btn-primary view_photo' batchname='".$_LearnerBatchNameFTP."'
            image='".$_Row['Adm_Log_AdmissionLcode']."_photo.png' id='".$_Count.'_photo'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphoto_".$_Count."'> </div>"
            ."</td>";           

            if ($_Row['Adm_Log_IsUpd_Photo'] == '1') {
               $image = $_Row['Adm_Log_Photo'];
            echo "<td >456"
                . "<input type='button' name='Edit' class='btn btn-primary view_photoafter' batchname='".$_LearnerBatchNameFTP."'
            image='".$image."' id='".$_Count.'_photoafter'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphotoafter_".$_Count."'> </div>"
            ."</td>";
            } else {
                $image = $_Row['Adm_Log_AdmissionLcode'];
            echo "<td >789"
                . "<input type='button' name='Edit' class='btn btn-primary view_photonoedit' batchname='".$_LearnerBatchNameFTP."'
            image='".$_Row['Adm_Log_AdmissionLcode']."_photo.png' id='".$_Count.'_photonoedit'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphotonoedit_".$_Count."'> </div>"
            ."</td>"; 
            }
            echo "<td>" . strtoupper($_Row['Adm_Log_Lname_Old']) . "</td>";
            if ($_Row['Adm_Log_IsUpd_Name'] == '1') {

                echo "<td style='background-color:#ff4d4d'>" . strtoupper($_Row['Adm_Log_Lname_New']) . "</td>";
            } else {
                echo "<td>" . strtoupper($_Row['Adm_Log_Lname_New']) . "</td>";
            }

            echo "<td>" . strtoupper($_Row['Adm_Log_Fname_Old']) . "</td>";

            if ($_Row['Adm_Log_IsUpd_Fname'] == '1') {

                echo "<td style='background-color:#ff4d4d'>" . strtoupper($_Row['Adm_Log_Fname_New']) . "</td>";
            } else {
                echo "<td>" . strtoupper($_Row['Adm_Log_Fname_New']) . "</td>";
            }

            echo "<td>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_Old'])) . "</td>";

            if ($_Row['Adm_Log_IsUpd_Dob'] == '1') {

                echo "<td style='background-color:#ff4d4d'>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_New'])) . "</td>";
            } else {
                echo "<td>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_New'])) . "</td>";
            }
            echo "<td>" . ($_Row['Adm_Log_Uid_Old'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_Old']) . "</td>";

            if ($_Row['Adm_Log_IsUpd_Uid'] == '1') {

                echo "<td style='background-color:#ff4d4d'>" . ($_Row['Adm_Log_Uid_New'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_New']) . "</td>";
            } else {
                echo "<td>" . ($_Row['Adm_Log_Uid_New'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_New']) . "</td>";
            }
            if ($_Row['Adm_Log_ApproveStatus'] == 1) {
                echo "<td>Approved</td>";
            } elseif ($_Row['Adm_Log_ApproveStatus'] == 2) {
                echo "<td>Set OnHold</td>";
            }
            elseif ($_Row['Adm_Log_ApproveStatus'] == 0) {
                echo "<td>Action Pending</td>";
            }
            elseif ($_Row['Adm_Log_ApproveStatus'] == 3) {
                echo "<td>Rejected</td>";
            }
            if ($_Row['Adm_Log_ApproveStatus'] == 2) {
                echo "<td>".strtoupper($_Row['Adm_Log_ApproveRemark'])."</td>";
            } else{
                echo "<td>Not Available</td>";
            }
            if ($_Row['Adm_Log_ApproveStatus'] == 0) {
                echo "<td> Not Available</td>";
            }
            else{
                echo "<td> " .  date('d-m-Y', strtotime($_Row['Adm_log_ApproveDate'])) . "</td>";
                }
            
            echo "<td> " . $_Row['Adm_Log_ApproveUser'] . "</td>";
            echo "<td>" . ($_Row['Adm_Log_PayStatus'] == '1' ? 'Payment Confirmed' : 'Payment Not Confirmed') . "</td>";

            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
if ($_action == "ViewPhoto") {

    $batchname = $_POST['batchname'];
    $photopath = '/admission_photo/'.$batchname.'/';
    $image = $_POST['image'];
    
    global $_ObjFTPConnection;
    
    $details= $_ObjFTPConnection->ftpdetails();
  
    $photoimage =  file_get_contents($details.$photopath.$image);
    $imageData = base64_encode($photoimage);
    echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='80' height='110'/>";

    
}

if ($_action == "ViewSign") {
    //print_r($_POST); die;
    $batchname = $_POST['batchname'];
    $photopath = '/admission_sign/'.$batchname.'/';
    $image = $_POST['image'];
    
    global $_ObjFTPConnection;
    
    $details= $_ObjFTPConnection->ftpdetails();
    
    $photoimage =  file_get_contents($details.$photopath.$image);
$imageData = base64_encode($photoimage);
echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='50' height='80'/>";

    
}
if ($_action == "ViewPhotoAfter") {

    $batchname = $_POST['batchname'];
    $photopath = '/AdmissionCorrectionAfterPayment/';
    $image = $_POST['image'];
    
    global $_ObjFTPConnection;
    
    $details= $_ObjFTPConnection->ftpdetails();
  
    $photoimage =  file_get_contents($details.$photopath.$image);
    $imageData = base64_encode($photoimage);
    echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='80' height='110'/>";

    
}
if ($_action == "ViewPhotonoedit") {

    $batchname = $_POST['batchname'];
    $photopath = '/admission_photo/'.$batchname.'/';
    $image = $_POST['image'];
    
    global $_ObjFTPConnection;
    
    $details= $_ObjFTPConnection->ftpdetails();
  
    $photoimage =  file_get_contents($details.$photopath.$image);
    $imageData = base64_encode($photoimage);
    echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='80' height='110'/>";

    
}
?>
