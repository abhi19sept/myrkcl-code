<?php
/* 
 * Author Name:  Sunil Kumar Baindara
 */
include 'commonFunction.php';
require 'BAL/clsRkclApi.php';

$response = array();
$emp = new clsRkclApi();

if ($_action == "ADD") {
    if (isset($_POST["txtClientName"])) {
        //print_r($_POST);die;
        $_ClientName = $_POST["txtClientName"];
        $_Apikey=$_POST["txtApikey"];
        $_ClientIP=$_POST['txtClientIP'];
        $_Status=$_POST['txtStatus'];
//        $_Functionname=$_POST['txtFunctionName'];
//        $_Functionfileds=$_POST['txtddlFtable2'];
        
        $_Functionname=1;
        $_Functionfileds=2;
        $response = $emp->Add($_ClientName,$_Apikey,$_ClientIP,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["txtClientName"])) {
       // print_r($_POST);die;
        $_ClientName = $_POST["txtClientName"];
        $_Apikey=$_POST["txtApikey"];
        $_ClientIP=$_POST['txtClientIP'];
        $_Status=$_POST['txtStatus'];
        $_Code=$_POST['code'];
        //$_DisplayOrder=$_POST['display'];
        $response = $emp->Update($_Code,$_ClientName,$_Apikey,$_ClientIP,$_Status);
        echo $response[0];
    }
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("ClientName" => $_Row['ClientName'],
            "Apikey" => $_Row['Apikey'],
            "Status"=>$_Row['Status'],
            "ClientIP"=>$_Row['ClientIP']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}

if ($_action == "DELETE") {
    //print_r($_POST);die;
    $response = $emp->DeleteRecord($_actionvalue);
    echo $response[0];
}

if ($_action == "SHOW") {
    //echo "Show";
    $response = $emp->GetAll();
    $_DataTable = "";

    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Client Name</th>";
    echo "<th style='10%'>Client IP</th>";
    echo "<th style='30%'>Add Function</th>";
    echo "<th style='30%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        if($_Row['Status']==1){$Status='Active';}else{$Status='Deactive';}
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['ClientName'] . "</td>";
        echo "<td>" . $_Row['ClientIP'] . "</td>";
        echo "<td><a href='frmRkclApiFDetail.php?clientapiID=" . $_Row['rkclAPi_id'] . "'>"
                . "<img src='images/addicon.png' alt='Edit' width='30px' /></a> </td>";
        echo "<td>" . $Status . "</td>";
        echo "<td><a href='frmRkclApi.php?code=" . $_Row['rkclAPi_id'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmRkclApi.php?code=" . $_Row['rkclAPi_id'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}

if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Root</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Root_Menu_Code'] . ">" . $_Row['Root_Menu_Name'] . "</option>";
    }
}

//* This function is getting data form the function name of the apis*//
if ($_action == "FILLFNAME") {
    $response = $emp->GetAllFunctionName();
    echo "<option value='0' selected='selected'>Select Function</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['funtablename'] . ">" . $_Row['functionname'] . "</option>";
    }
}
//* This function is getting data form the function name of the apis*//
//
//* This function is getting table filed for the function apis*//
if ($_action == "FILLTBFIELDNAME") {
    //echo $_POST['ftablename'];die;
    $response = $emp->GetTableFiledByTabName($_POST['ftablename']);
    //echo "<option value='0' selected='selected'>Select Function</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['COLUMN_NAME'] . ">" . $_Row['COLUMN_NAME'] . "</option>";
    }
}
//* This function is getting table filed for the function apis*//