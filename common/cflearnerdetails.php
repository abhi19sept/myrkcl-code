<?php

    /**
     * Created by yogi
     * Update By Sunil Kuamr Baindara Dated: 9-2-2017
     */

    include './commonFunction.php';
    require 'BAL/clslearnerdetails.php';
require 'DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();

    $response = array();
    $response1 = array();
    $emp = new clslearnerdetails();

//print_r($_POST['ddlCenter']);

    if ($_action == "sendOTP") {

        $response = $emp->sendOTP($_POST['phoneNumber']);
        $_Row = mysqli_fetch_array($response[2]);

        if ($_Row['total_count'] == 0) {
            /*** GENERATE RANDOM OTP ***/
            $otp = mt_rand(100000, 999999);
            $response1 = $emp->insertOTP($otp, $_POST['phoneNumber']);
            if ($response1[0] == "Successfully Inserted") {
                echo "<script>$('#errorTextOTP').html('');
$('#showError').css('display','none');
$('#successText').html('OTP Sent to +91-" . $_POST['phoneNumber'] . "');
$('#showSuccess').css('display','block');$('#verifyOTP').css('display','block');$(\"#sendOTP\").text(\"Resend OTP\");$(\"#sendOTP\").removeClass(\"disabled\");</script>";
            }
        } else {
            echo "<script> $('#successText').html('');
                $('#showSuccess').css('display','none');
                $('#errorTextOTP').html('Mobile number already exist');
                $('#showError').css('display','block');$(\"#sendOTP\").text(\"Send OTP\");$(\"#sendOTP\").removeClass(\"disabled\");</script>";
        }
    }

    if ($_action == "verifyOTP") {

        $response = $emp->verifyOTP($_POST['otp']);
        $_Row = mysqli_fetch_array($response[2]);

        if ($_Row['total_count'] == 0) {
            echo "<script> $('#successText').html('');
                $('#showSuccess').css('display','none');
                $('#errorTextOTP').html('Invalid OTP');
                $('#showError').css('display','block');$(\"#sendOTP\").text(\"Send OTP\");$(\"#sendOTP\").removeClass(\"disabled\");</script>";
        } else {
		$responsegetdetail = $emp->GetAllLearner($_POST['learner']);
		$_Rowgetdetail = mysqli_fetch_array($responsegetdetail[2]);
		
            $response = $emp->updateLearnerMobile($_POST['mobile'], $_POST['learner'], $_POST['otp'],$_Rowgetdetail['Admission_Mobile'],$_Rowgetdetail["Admission_ITGK_Code"]);
            if ($response[0] == "Successfully Updated") {
                echo "<script>$('#errorTextOTP').html('');$('#Admission_Mobile_Text').html('".$_POST['mobile']." &nbsp;&nbsp;&nbsp;<button class=\"btn btn-danger btn-small\" type=\"button\" data-toggle=\"modal\" data-target=\"#otp_modal\">Update Number</button>');
$('#showError').css('display','none');
$('#successText').html('Mobile Number has been successfully updated');
$('#showSuccess').css('display','block');$('#verifyOTP').css('display','block');</script>";
            }
        }
    }

    if ($_action == "DETAILS") {

        //print_r($_POST);die;
        $response = $emp->GetAllLearner($_POST["LearnerCode"]);
		$responsecert = $emp->GetLearnerNameOnCert($_POST["LearnerCode"]);
$_RowCert = mysqli_fetch_array($responsecert[2]);

        $_DataTable = "";
        //$response1=$response[2];
        $_Row = mysqli_fetch_array($response[2]);
        //print_r($_SESSION);die;
        $num_rows = mysqli_num_rows($response[2]);
        $Tdate = date("d-m-Y", strtotime($_Row['exam_date']));
        $num_rows;
        $_LoginRole = $_SESSION['User_UserRoll'];
        if($num_rows==0)
        {
            if ($_LoginRole == '21' || $_LoginRole == '18' || $_LoginRole == '19' || $_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11' || $_LoginRole == '30' || $_LoginRole == '28')
            {

                ?><div class="error"><?php
                echo "No Detail Found.";
                ?></div>
                <?php
            }
            else if ($_LoginRole == '14')
            {

                ?><div class="error"><?php
                echo "This Learner Has Not Been Registered/Enrolled In Your SP.";
                ?></div>
                <?php
            }
            else
            {

                ?><div class="error"><?php
                echo "This Learner Has Not Been Registered/Enrolled In Your Center/ITGK/SP.";
                ?></div>
                <?php
            }
        }
        else
        {
            $responsesC = $emp->CenterDetails($_Row["Admission_ITGK_Code"]);
            $_RowC = mysqli_fetch_array($responsesC[2]);
            //print_r($_RowC);//die;

            $lphoto = ($_LoginRole == '1' || $_LoginRole == '8' || $_LoginRole == '30') ? 'lphoto' : 'lpic';
            $lsign = ($_LoginRole == '1' || $_LoginRole == '8' || $_LoginRole == '30') ? 'lsign' : 'lsin';

            echo "<div class='container marginT30'>
            <div class='row'>
                <div class='col-lg-3 col-md-3'>
                    <div class='profilebox'>
                        <h4 class='proheading'><b>Profile Picture</b></h4>
                        <div class='profilepic'>";
				$responsePic = $emp->GetCorrectionPhoto($_POST["LearnerCode"],$_Row["Admission_ITGK_Code"]);


                global $_ObjFTPConnection;

				if($responsePic[0]=='Success'){
					$_RowPic = mysqli_fetch_array($responsePic[2],true);					
					if($_RowPic['photo'] != "" ) {


                    $details= $_ObjFTPConnection->ftpdetails();
                    $photopath = '/correction_photo/';
                    $image =$_RowPic['photo'];
                    $photoimage =  file_get_contents($details.$photopath.$image);
                    $imageData = base64_encode($photoimage);
                    echo "<img class='img-squre img-responsive' width='200' id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' style='padding:2px;border:1px solid #428bca;height:180px;margin-top:10px;'/>";



					// echo '<img class="img-squre img-responsive" width="200"  style="padding:2px;border:1px solid #428bca;height:180px;margin-top:10px;" src="upload/correction_photo/' . $_RowPic['photo'].'"/>';
					}
					else
					{
						echo '<img class="img-squre img-responsive"  width="150" style="padding:2px;border:1px solid #428bca;height:130px;margin-top:10px; margin-left: auto; margin-right: auto;" src="images/user icon big.png"/>';
					}
				}
				else{


                    $_LearnerBatchNameFTP = $_ObjFTPConnection->LearnerBatchNameFTP($_Row['Admission_Course'],$_Row['Batch_Name']);
					if($_Row['Admission_Photo'] != "" ) {

                    $details= $_ObjFTPConnection->ftpdetails();
                    $photopath = '/admission_photo/'.$_LearnerBatchNameFTP.'/';
                    $image =$_Row['Admission_Photo'];
                    $photoimage =  file_get_contents($details.$photopath.$image);
                    $imageData = base64_encode($photoimage);
                    echo "<img class='img-squre img-responsive'". $lphoto ." width='200' id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' style='padding:2px;border:1px solid #428bca;height:180px;margin-top:10px;'/>";


						// echo '<img class="img-squre img-responsive ' . $lphoto . '" width="200"  style="padding:2px;border:1px solid #428bca;height:180px;margin-top:10px;" src="upload/admission_photo/' . $_Row['Admission_Photo'].'"/>';
					}
					else
					{
						echo '<img class="img-squre img-responsive ' . $lphoto . '"  width="150" style="padding:2px;border:1px solid #428bca;height:130px;margin-top:10px; margin-left: auto; margin-right: auto;" src="images/user icon big.png"/>';

					}
				}		
            //$src=$_SERVER['DOCUMENT_ROOT'].'/upload/admission_photo/' . $_Row['Admission_Photo'] ;
            $srcsign=$_SERVER['DOCUMENT_ROOT'].'/upload/admission_sign/' . $_Row['Admission_Sign'];
            
            
           // echo "<div style='display:none;'><input type='file' name='" . $lphoto . "' id='" . $lphoto . "' /></div></div>";
		   echo "</div>";
            echo '<p   style="margin-top:10px;">Sign </p ><p>';
			
            if($_Row['Admission_Sign'] !="" )

            {
                    $_LearnerBatchNameFTP = $_ObjFTPConnection->LearnerBatchNameFTP($_Row['Admission_Course'],$_Row['Batch_Name']);

                    $details= $_ObjFTPConnection->ftpdetails();
                    $photopath = '/admission_sign/'.$_LearnerBatchNameFTP.'/';
                    $image =$_Row['Admission_Sign'];
                    $photoimage =  file_get_contents($details.$photopath.$image);
                    $imageData = base64_encode($photoimage);
                echo "<img class='img-squre img-responsive'". $lsign ." width='80' id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' style='margin-top:10px;margin:0 auto;'/>";

                // echo '<img class="img-squre img-responsive ' . $lsign . '"  width="80"  style="margin-top:10px; margin:0 auto" src="upload/admission_sign/' . $_Row['Admission_Sign'].'"/>';
            }
            else
            {
                echo '<img class="img-squre img-responsive ' . $lsign . '"  width="80"  style="margin-top:10px; margin-left: auto; margin-right: auto;" src="images/no_image.png"/>';

            }
            // echo "  <div style='display:none;'><input type='file' name='" . $lsign . "' id='" . $lsign . "' /></div></p> </div>
                     
            //     </div><div class='personalblog'>";
            echo "  </p> </div>
                     
                </div><div class='personalblog'>";
                
                 echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Personal Details</span>
                    </div>";
                 
                 
                 if($_Row['Admission_DOB']=='' || $_Row['Admission_DOB']=='(NULL)'  || $_Row['Admission_DOB']=='0000-00-00 00:00:00')
                            { 
                                    $Admission_DOB="Not Available";
                            }
                     else
                           {
                                    $Admission_DOB = date("d M Y", strtotime($_Row['Admission_DOB']));
                           }
                 
                           
                 if($_Row['Admission_Batch']>='90')
                 {          
                           
                            if($_Row['Admission_Date_LastModified']=='' || $_Row['Admission_Date_LastModified']=='(NULL)' || $_Row['Admission_Date_LastModified']=='0000-00-00 00:00:00')
                                       { 
                                               $Admission_Date_LastModified="Not Available";
                                       }
                                else
                                      {
                                               $Admission_Date_LastModified = date("d M Y", strtotime($_Row['Admission_Date_LastModified']));
                                      }

                            if($_Row['Admission_Date_Payment']=='' || $_Row['Admission_Date_Payment']=='(NULL)'  || $_Row['Admission_Date_Payment']=='0000-00-00 00:00:00')
                                       { 
                                               $Admission_Date_Payment="Not Available";
                                       }
                                else
                                      {
                                                $Admission_Date_Payment = date("d M Y", strtotime($_Row['Admission_Date_Payment']));
                                      }

                            if($_Row['Admission_Date']=='' || $_Row['Admission_Date']=='(NULL)'  || $_Row['Admission_Date']=='0000-00-00 00:00:00')
                                       { 
                                               $Admission_Date="Not Available";
                                       }
                                else
                                   {
                                            $Admission_Date = date("d M Y", strtotime($_Row['Admission_Date']));
                                   }
                           
                 }
                 else
                 {
                    if($_Row['Admission_Batch']=='57' || $_Row['Admission_Batch']=='58')
                        { 
                                 if($_Row['Admission_Date_LastModified']=='' || $_Row['Admission_Date_LastModified']=='(NULL)' || $_Row['Admission_Date_LastModified']=='0000-00-00 00:00:00')
                                    { 
                                            $Admission_Date_LastModified="Not Available";
                                    }
                                 else
                                   {
                                            $Admission_Date_LastModified = date("d M Y", strtotime($_Row['Admission_Date_LastModified']));
                                   }
                                   
                                 if($_Row['Admission_Date_Payment']=='' || $_Row['Admission_Date_Payment']=='(NULL)'  || $_Row['Admission_Date_Payment']=='0000-00-00 00:00:00')
                                       { 
                                               $Admission_Date_Payment="Not Available";
                                       }
                                  else
                                      {
                                                $Admission_Date_Payment = date("d M Y", strtotime($_Row['Admission_Date_Payment']));
                                      }
                           
//                                  if($_Row['Admission_Date']=='' || $_Row['Admission_Date']=='(NULL)'  || $_Row['Admission_Date']=='0000-00-00 00:00:00')
//                                       { 
//                                               $Admission_Date="Not Available";
//                                       }
//                                  else
//                                      {
//                                               $Admission_Date = date("d M Y", strtotime($_Row['Admission_Date']));
//                                      }
                                      
                                       $Admission_Date="Not Available";
                        }
                    else
                        {
                            $Admission_Date_LastModified="Not Available";
                            $Admission_Date_Payment="Not Available";
                            $Admission_Date="Not Available";
                        }
                     
                 }        
                     
                    echo "<div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Name:</span>
                             <p>"  . strtoupper(($_Row['Admission_Name'] == '') ? 'Not Available' : $_Row['Admission_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Father/Husband Name :</span>
                             <p>" . strtoupper(($_Row['Admission_Fname'] == '') ? 'Not Available' : $_Row['Admission_Fname']) . "</p>
                        </div>
                    </div>
					
<div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Learner Name on Certificate:</span>
                             <p>"  . strtoupper(($_RowCert['cert_name'] == '') ? 'Not Available' : $_RowCert['cert_name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Father/Husband Name on Certificate:</span>
                             <p>" . strtoupper(($_RowCert['cert_fname'] == '') ? 'Not Available' : $_RowCert['cert_fname']) . "</p>
                        </div>
</div>

                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Date of Birth :</span>
                             <p>" . strtoupper($Admission_DOB) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Mobile :</span>
                             <p id='Admission_Mobile_Text'>" . strtoupper(($_Row['Admission_Mobile'] == '') ? 'Not Available' : $_Row['Admission_Mobile']) ;

            if ($_SESSION['User_UserRoll'] == "19") {
                echo "";
            } else {
                echo "&nbsp;&nbsp;<button class='btn btn-danger btn-small' type='button' data-toggle='modal' data-target='#otp_modal'>Update Number</button>";
            }

            echo "</p>
                             
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Address :</span>
                             <p >" . strtoupper(($_Row['Admission_Address'] == '') ? 'Not Available' : $_Row['Admission_Address']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Email :</span>
                             <p>" . strtoupper(($_Row['Admission_Email'] == '') ? 'Not Available' : $_Row['Admission_Email']);
            if ($_SESSION['User_UserRoll'] == "19") {
                echo "&nbsp;&nbsp;<button class='btn btn-primary btn-xs' type='button' id='updateEmailLearner'>Update Email</button>";
            }

            echo"</p>
                        </div>
                        
                    </div>
                    <div class='persdetablog'>
                         <div class='persdetacont'>
                             <span>Learner Code :</span>
                             <p>" . strtoupper($_Row['Admission_LearnerCode']) . "</p>
                        </div>
                       
                        <div class='persdetacont'>
                             <span>Medium :</span>
                             <p>" . strtoupper(($_Row['Admission_Medium'] == '') ? 'Not Available' : $_Row['Admission_Medium']) . "</p>
                        </div>
                    
                        
                    </div>
                    
                    <div class='persdetablog'>
                         <div class='persdetacont'>
                             <span>Admission Uploading Last Modified:</span>
                             <p>". strtoupper($Admission_Date_LastModified) ."</p>
                        </div>
                       
                        <div class='persdetacont'>
                             <span>Admission Payment confirmation:</span>
                             <p>". strtoupper($Admission_Date_Payment) ."</p>
                        </div>
                      
                    </div>
                    
<div class='persdetablog'>
                         <div class='persdetacont'>
                             <span>Admission uploading Date:</span>
                             <p>". strtoupper($Admission_Date) ."</p>
                        </div>
                       
                        <div class='persdetacont'>
                             <span>Admission Batch:</span>
                             <p>". strtoupper(($_Row['Batch_Name'] == '') ? 'Not Available' : $_Row['Batch_Name']) ."</p>
                        </div>
                      
                    </div>

               </div>";

            /* New Tab*/

            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>ITGK (Center) Details</span>
                    </div>";
            echo "<div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Center Name:</span>
                             <p>". strtoupper(($_RowC['Organization_Name'] == '') ? 'Not Available' : $_RowC['Organization_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Center Code :</span>
                             <p>". strtoupper(($_RowC['User_LoginId'] == '') ? 'Not Available' : $_RowC['User_LoginId']) . "</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Center Registration No :</span>
                             <p>". strtoupper(($_RowC['Organization_RegistrationNo'] == '') ? 'Not Available' : $_RowC['Organization_RegistrationNo']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Mobile :</span>
                             <p>". strtoupper(($_RowC['User_MobileNo'] == '') ? 'Not Available' : $_RowC['User_MobileNo']) ."</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Center Address :</span>
                             <p  >". strtoupper(($_RowC['Organization_Address'] == '') ? 'Not Available' : $_RowC['Organization_Address']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Email :</span>
                             <p>" . strtoupper(($_RowC['User_EmailId'] == '') ? 'Not Available' : $_RowC['User_EmailId']) . "</p>
                        </div>
                        
                    </div>
                   
               </div>";
                    
                $responsesSP = $emp->SPDetails($_RowC["User_LoginId"]);
                $_RowSP = mysqli_fetch_array($responsesSP[2]); 
                /*
                        <div class='persdetacont'>
                             <span>SP Registration No :</span>
                             <p>". strtoupper($_RowSP['Organization_RegistrationNo']) . "</p>
                        </div>
                     <div class='persdetacont'>
                             <span>SP Founded Date:</span>
                             <p>". strtoupper($Organization_FoundedDate) ."</p>
                        </div> */    
                  $Addresp=$_RowSP['Organization_HouseNo']." ".$_RowSP['Organization_Street']." ".$_RowSP['Organization_Road']." ".$_RowSP['Organization_Landmark']." ".$_RowSP['Org_formatted_address'] ; 
                    echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Service Provider Details</span>
                    </div>";
            $Organization_FoundedDate = date("d M Y", strtotime($_RowSP['Organization_FoundedDate']));
            echo "<div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>SP Name:</span>
                             <p>". strtoupper(($_RowSP['Organization_Name'] == '') ? 'Not Available' : $_RowSP['Organization_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>SP Mobile:</span>
                             <p>". strtoupper(($_RowSP['spmobile'] == '') ? 'Not Available' : $_RowSP['spmobile']) ."</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                       
                        <div class='persdetacont'>
                             <span>SP Email :</span>
                             <p  >".strtoupper(($_RowSP['spemail'] == '') ? 'Not Available' : $_RowSP['spemail']). "</p>
                        </div>
                         <div class='persdetacont'>
                             <span>SP Address:</span>
                             <p  >".strtoupper($Addresp). "</p>
                        </div>
                    </div>
                    
                    
                   
               </div>";

            /* New Tab*/

            if($_Row['Admission_Payment_Status']==1)
            {
                $temp='Confirm';
            }else{ $temp='Not Confirm';}

            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Payment Details </span>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Learner Course Fee Payment Status :</span>
                             <p>". strtoupper($temp) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Registered Course By Learner :</span>
                             <p>" . strtoupper(($_Row['Course_Name'] == '') ? 'Not Available' : $_Row['Course_Name']) . "</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Learner Data Approval Status :</span>
                             <p>" . strtoupper(($_Row['Admission_PhotoProcessing_Status'] == '') ? 'Not Available' : $_Row['Admission_PhotoProcessing_Status']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Learner Enrolled In Batch :</span>
                             <p>" . strtoupper(($_Row['Batch_Name'] == '') ? 'Not Available' : $_Row['Batch_Name']) . "</p>
                        </div>
                    </div>
                    
                </div>";





            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Internal Assessment Score Detail</span>
                    </div>";

                if($_Row["Admission_ITGK_Code"]>171)
                {

                ini_set("display_errors", "on");
                ini_set("error_reporting", E_ERROR);
                require_once("../nusoap/nuSOAP/lib/nusoap.php");
                define("SERVER_WSDL_API", "http://ilearn.myrkcl.com/nusoap/toc.php?wsdl");   
                $key = "RKCL_ILEARN";
                $server_ip = '49.50.79.170';
                $cname = 'RKCLILEARN';
                $client = new nusoap_client(SERVER_WSDL_API, true, false, false, false, false, 300, 300);
                $client->soap_defencoding = 'UTF-8';
                $client->decode_utf8 = false;
                ini_set('default_socket_timeout', 5000);
                $lcode=$_POST["LearnerCode"];
                $result = $client->call("getLearnerAssessementFinalResult",array("cname" => $cname, "hashkey" => $key, "clientip" => $server_ip, "lcode" => $lcode));
                $num_rowsSco=json_decode(base64_decode($result), true);
                //echo "<pre>";print_r($res);
                //echo $num_rowsSco;
                if($num_rowsSco==0)
                {?><div class="error"><?php
                    echo "<b>No Detail Found.</b>";
                    ?></div>
                    <?php
                }
                else
                {

                    echo "<div class='persdetablog'>
                            <div class='exampannelNew1'>
                                <div class='examname'>Learning Start Date</div>
                            </div>
                            <div class='exampannelNew1'>
                                <div class='examname'>Final Score</div>
                            </div>
                            <div class='exampannelNew1'>
                                <div class='examname'>Score %</div>
                            </div>
                         </div>";
                    $Batch_StartDate = date("d M Y", strtotime($_Row['Batch_StartDate']));
                    $Score_Per=($num_rowsSco/30*100);
                    echo " <div class=' persdetablog1'>

                            <div class='exampannelNew1'>

                                <div class='marks'>". strtoupper($Batch_StartDate) . " </div>
                            </div>
                            <div class='exampannelNew1'>

                                <div class='marks'>". $num_rowsSco . "</div>
                            </div>
                            <div class='exampannelNew1'>

                                <div class='marks'>". $Score_Per . "</div>
                            </div>

                        </div>";
                }





                }
                else   
                {

                $responseSco = $emp->GetAllLearnerScore($_POST["LearnerCode"], $_Row["Admission_ITGK_Code"]);

                $_DataTable = "";
                //$response1=$response[2];
                $_RowSco = mysqli_fetch_array($responseSco[2]);
                //print_r($_Row);//die;
                $num_rowsSco = mysqli_num_rows($responseSco[2]);
                if($num_rowsSco==0)
                {?><div class="error"><?php
                    echo "<b>No Detail Found.</b>";
                    ?></div>
                    <?php
                }
                else
                {

                    echo "<div class='persdetablog'>
                            <div class='exampannelNew1'>
                                <div class='examname'>Learning Start Date</div>
                            </div>
                            <div class='exampannelNew1'>
                                <div class='examname'>Final Score</div>
                            </div>
                            <div class='exampannelNew1'>
                                <div class='examname'>Score %</div>
                            </div>
                         </div>";
                    $Batch_StartDate = date("d M Y", strtotime($_Row['Batch_StartDate']));
                    echo " <div class=' persdetablog1'>

                            <div class='exampannelNew1'>

                                <div class='marks'>". strtoupper($Batch_StartDate) . " </div>
                            </div>
                            <div class='exampannelNew1'>

                                <div class='marks'>". strtoupper($_RowSco['Score']) . "</div>
                            </div>
                            <div class='exampannelNew1'>

                                <div class='marks'>". strtoupper($_RowSco['Score_Per']) . "</div>
                            </div>

                        </div>";
                }

            }

                echo"</div>";




            $responseLAM = $emp->GetLearnerAllMarks($_POST["LearnerCode"]);
            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Final Exam Marks Detail</span>
                    </div>";

            //$_Row1 = mysqli_fetch_array($responseLAM[2]);
            //print_r($_Row);//die;
            $num_rowsLAM = mysqli_num_rows($responseLAM[2]);
            if($num_rowsLAM==0)
            {?><div class="error">
                <?php echo "<b>No Detail Found.</b>";?>
                </div>
                <?php
            }
            else
            {





                echo "<div class='persdetablog'>
                        <div class='exampannelNew'>
                            <div class='examname'>Final Score</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Event Name</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Exam Date</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Result</div>
                        </div>
                    </div>";

                while($_Row1 = mysqli_fetch_array($responseLAM[2],true))
                {

                    echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew' style='height:62px;'>
                            
                            <div class='marks'>". strtoupper(($_Row1['tot_marks'] == '') ? 'Not Available' : $_Row1['tot_marks']) . " Marks</div>
                        </div>
                        <div class='exampannelNew' style='height:62px;'>
                            
                            <div class='marks'>". strtoupper(($_Row1['Event_Name'] == '') ? 'Not Available' : $_Row1['Event_Name']) . "</div>
                        </div>
                        <div class='exampannelNew' style='height:62px;'>
                            
                            <div class='marks'>". strtoupper(($_Row1['exam_date'] == '') ? 'Not Available' : date("d-m-Y", strtotime($_Row1['exam_date']))) . "</div>
                        </div>
                        <div class='exampannelNew' style='height:62px;'>
                            
                            <div class='marks'>". strtoupper(($_Row1['result'] == '') ? 'Not Available' : $_Row1['result']) . "</div>
                         </div>
                    </div>";

                }


            }

            echo "</div>";


            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>RS-CIT Fee Reimbursement Status </span>
                    </div>";
            $responseNew = $emp->checkGovtLearnerStatus($_POST["LearnerCode"],$_Row["Admission_ITGK_Code"]);
            if($responseNew[0]=='Success')
            {
                $responseDET = $emp->GetAllRDETAILS($_POST["LearnerCode"],$_Row["Admission_ITGK_Code"]);
                $_RowDET = mysqli_fetch_array($responseDET[2],true);
                //echo "<pre>"; print_r($_RowDET);
                echo"<div class='persdetablog'>
                            <div class='persdetacont'>
                             <span>Employee ID :</span>
                             <p>". strtoupper(($_RowDET['empid'] == '') ? 'Not Available' : $_RowDET['empid']) ."</p>
                             </div>
                            <div class='persdetacont'>
                             <span>Designation :</span>
                             <p>". strtoupper(($_RowDET['designation'] == '') ? 'Not Available' : $_RowDET['designation']) ."</p>
                             </div>
                         </div>";

                echo "<div class='persdetablog'>
                            <div class='persdetacont'>
                             <span>Office Address :</span>
                             <p>". strtoupper(($_RowDET['officeaddress'] == '') ? 'Not Available' : $_RowDET['officeaddress']) ."</p>
                            </div>
                            <div class='persdetacont'>
                             <span>GPF No.:</span>
                             <p>". strtoupper(($_RowDET['empgpfno'] == '') ? 'Not Available' : $_RowDET['empgpfno']) ."</p>
                            </div>
                        </div>";
                echo "<div class='persdetablog'>
                            <div class='persdetacont'>
                             <span>Account No. :</span>
                             <p>". strtoupper(($_RowDET['empaccountno'] == '') ? 'Not Available' : $_RowDET['empaccountno']) ."</p>
                            </div>
                            <div class='persdetacont'>
                             <span>IFSC No. :</span>
                             <p>". strtoupper(($_RowDET['gifsccode'] == '') ? 'Not Available' : $_RowDET['gifsccode']) ."</p>
                            </div>
                        </div>";

                if($_RowDET['aattempt']==1){$aattempt='First';}else{$aattempt='Not in 1st Attempt';}

                echo"<div class='persdetablog'>
                            <div class='persdetacont'>
                                 <span>Exam Marks :</span>
                                 <p>". strtoupper(($_RowDET['exammarks'] == '') ? 'Not Available' : $_RowDET['exammarks']) ."</p>
                                 </div>
                            <div class='persdetacont'>
                                 <span>Number Of Exam Attempt :</span>
                                 <p>". strtoupper($aattempt) ."</p>
                            </div>
                        </div>";

                $DET_datetime = date("d M Y", strtotime($_RowDET['datetime']));
                echo"<div class='persdetablog'>
                            <div class='persdetacont'>
                                 <span>Reimbursement Status :</span>
                                 <p>". strtoupper(($_RowDET['cstatus'] == '') ? 'Not Available' : $_RowDET['cstatus']) ."</p>
                            </div>
                            <div class='persdetacont'>
                             <span>Lot Name :</span>
                             <p>". strtoupper(($_RowDET['lotname'] == '') ? 'Not Available' : $_RowDET['lotname']) ."</p>
                            </div>
                           
                          </div>";
                echo"<div class='persdetablog'>
                            <div class='persdetacont'>
                             <span>Reimbursement Fee Amt. :</span>
                             <p>". strtoupper(($_RowDET['fee'] == '') ? 'Not Available' : $_RowDET['fee']) ."</p>
                             </div>
                            <div class='persdetacont'>
                             <span>Incentive :</span>
                             <p>". strtoupper(($_RowDET['incentive'] == '') ? 'Not Available' : $_RowDET['incentive']) ."</p>
                            </div>
                          </div>";
                $total = intval (trim($_RowDET['fee']))+ intval (trim($_RowDET['incentive']));
                echo"<div class='persdetablog'>
                            
                            <div class='persdetacont'>
                             <span>Total Fee Amt.  :</span>
                              <p>". strtoupper($total) ."</p>
                            </div>							
							<div class='persdetacont'>
                             <span>Remark  :</span>
                               <p>". strtoupper(($_RowDET['Remarks'] == '') ? 'Not Available' : $_RowDET['Remarks']) ."</p>
                            </div>
                          </div>";
                     
                    
                   
                        }
                  elseif($responseNew=='NRED')
                      {?><div class="error"><?php
                        echo "<b>You Have Not Appiled For Fee Reimbursement .</b>"; // no emplyee course found
                        ?></div>
                        <?php 
                      }
                   else
                      {?><div class="error"><?php
                        echo "<b>Not Available.</b>"; // no emplyee course found
                        ?></div>
                        <?php 
                      }
                         
                         
                         

            echo"</div>";




            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Correction / Duplicate Certificate </span>
                    </div>";

            $responseCS = $emp->checkcorrectionStatus($_POST["LearnerCode"],$_Row["Admission_ITGK_Code"]);
            if($responseCS[0]=='Success')
            {
                $responseDET = $emp->GetCorrectionDuplicateCertificate($_POST["LearnerCode"],$_Row["Admission_ITGK_Code"]);
                $_RowDET = mysqli_fetch_array($responseDET[2],true);
                //echo "<pre>"; print_r($_RowDET);
                if($_RowDET['Correction_Payment_Status']==1){$CPS="Payment DONE";}else{$CPS="Payment Not DONE";}
                echo"<div class='persdetablog'>
                            <div class='persdetacont'>
                             <span>Application Applied For :</span>
                             <p>". strtoupper(($_RowDET['applicationfor'] == '') ? 'Not Available' : $_RowDET['applicationfor']) ."</p>
                             </div>
                            <div class='persdetacont'>
                             <span>Payment Status For Correction Application :</span>
                             <p>". strtoupper($CPS) ."</p>
                             </div>
                        </div>";

                echo"<div class='persdetablog'>
                            
                            <div class='persdetacont'>
                                 <span>Correction Tran RefNo :</span>
                                 <p>". strtoupper(($_RowDET['Correction_TranRefNo'] == '') ? 'Not Available' : $_RowDET['Correction_TranRefNo']) ."</p>
                            </div>
                             <div class='persdetacont'>
                             <span>Status :</span>
                             <p>". strtoupper($_RowDET['cstatus']) ."</p>
                             </div>
                        </div>";
			if($_RowDET['dispatchstatus']!='3'){
				$responseLOT = $emp->GetCorrectionDuplicateCertificateLot($_POST["LearnerCode"],$_RowDET['lot']);
                $_RowLot = mysqli_fetch_array($responseLOT[2],true);
						echo" <div class='persdetablog'>
								 <div class='persdetacont'>
									  <span>Lot Name :</span>
									  <p>". strtoupper($_RowLot['lotname']) ."</p>
							</div>
									
								   
								  </div>";
                    
			}
                   
                        }
                    
                    else{?><div class="error"><?php
                        //echo "<b>You Have Not Applied Yet For Correction/Duplicate Certificate.</b>"; // no emplyee course found
                        echo "<b>Not Available.</b>"; // no emplyee course found
                        ?></div>
                        <?php 
                      }     
                     
                   echo" </div>";
                   
                    //correction bfr exam history start

    echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Learner Correction Before final Exam </span>
                    </div>";


            $responseCH = $emp->checkcorrectionhistory($_POST["LearnerCode"],$_Row["Admission_ITGK_Code"]);
            if($responseCH[0]=='Success')
            {   
                echo "<div class='table-responsive' style='width: inherit;'>";
                  echo "<br>";

                echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
                echo "<thead>";
                echo "<tr>";

                echo "<td>Existing Learner Photo</td>";
                echo "<td>Edited Learner Photo</td>";
                echo "<td>Existing Learner Name</td>";
                echo "<td>Edited Learner Name</td>";
                echo "<td>Existing Father Name</td>";
                echo "<td >Edited Father Name</td>";
                echo "<td >Existing DOB</td>";
                echo "<td >Edited DOB</td>";
                echo "<td >Existing Aadhar</td>";
                echo "<td >Edited Aadhar</td>";
                echo "<td > Application Status</td>";
                echo "<td > Application Processing Date</td>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
              
                while($_Row = mysqli_fetch_array($responseCH[2],true))
                {
                    echo "<tr>";

                    $_LearnerBatchNameFTP = $_ObjFTPConnection->LearnerBatchNameFTP($_Row['Adm_Log_Course'],$_Row['Batch_Name']);
                    $details= $_ObjFTPConnection->ftpdetails();
                    $photopath = '/admission_photo/'.$_LearnerBatchNameFTP.'/';
                    $image =$_Row['Adm_Log_AdmissionLcode'].'_photo.png';
                    $photoimage =  file_get_contents($details.$photopath.$image);
                    $imageData = base64_encode($photoimage);
                    echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='data:image/png;base64, ".$imageData."'></td>";
                    if ($_Row['Adm_Log_IsUpd_Photo'] == '1') {
                        $photopath = '/AdmissionCorrectionAfterPayment/';
                        $image =$_Row['Adm_Log_AdmissionLcode'].'_photo.png';
                        $photoimage =  file_get_contents($details.$photopath.$image);
                        $imageData = base64_encode($photoimage);
                        echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='data:image/png;base64, ".$imageData."'></td>";
                    }
                    else{
                       echo "<td><img style='width:40px; height:50px;' class='img-responsive' src='data:image/png;base64, ".$imageData."'></td>";
                    }


                    echo "<td>" . strtoupper($_Row['Adm_Log_Lname_Old']) . "</td>";
                    if ($_Row['Adm_Log_IsUpd_Name'] == '1') {
                        
                        echo "<td>" . strtoupper($_Row['Adm_Log_Lname_New']) . "</td>";
                    }
                    else{
                        echo "<td>No Change</td>";
                    }


                    echo "<td>" . strtoupper($_Row['Adm_Log_Fname_Old']) . "</td>";

                    if ($_Row['Adm_Log_IsUpd_Fname'] == '1') {

                        echo "<td>" . strtoupper($_Row['Adm_Log_Fname_New']) . "</td>";
                    }
                    else{
                        echo "<td>No Change</td>";
                    }

                    echo "<td>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_Old'])) . "</td>";

                    if ($_Row['Adm_Log_IsUpd_Dob'] == '1') {

                        echo "<td>" . date("d-m-Y", strtotime($_Row['Adm_Log_Dob_New'])) . "</td>";
                    }
                    else{
                        echo "<td>No Change</td>";
                    }
                    echo "<td>" . ($_Row['Adm_Log_Uid_Old'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_Old']) . "</td>";

                    if ($_Row['Adm_Log_IsUpd_Uid'] == '1') {

                        echo "<td>" . ($_Row['Adm_Log_Uid_New'] == '' ? 'Not Available' : $_Row['Adm_Log_Uid_New']) . "</td>";
                    }
                    else{
                        echo "<td>No Change</td>";
                    }
                    if ($_Row['Adm_Log_ApproveStatus'] == '0') {

                        echo "<td>Pending For Processing</td>";
                    }
                    elseif ($_Row['Adm_Log_ApproveStatus'] == '1') {

                        echo "<td>Application Approved</td>";
                    }
                    elseif ($_Row['Adm_Log_ApproveStatus'] == '2') {

                        echo "<td>Application On Hold</td>";
                    }
                    elseif ($_Row['Adm_Log_ApproveStatus'] == '3') {

                        echo "<td>Application Rejected</td>";
                    }
                    else{
                        echo "<td>NA</td>";
                    }
                    echo "<td>" . ($_Row['Adm_log_ApproveDate'] == '' ? 'Not Available' : date("d-m-Y", strtotime($_Row['Adm_log_ApproveDate']))) . "</td>";

                     echo "</tr>";
                }
       
                    echo "</tbody>";
                    echo "</table>";  
                    echo "</div>";  

        }
                    
            else{?><div class="error"><?php

                echo "<b>Not Available.</b>"; // no emplyee course found
                ?></div>
                <?php 
              }     
             
    echo" </div>";


    //correction bfr exam history end
                   
              //$tdate=date("d-M-Y");
              //$originalDate = "2010-03-21";
              /*$Batch_StartDate = date("d-m-Y", strtotime($_Row['Batch_StartDate']));
               echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Data Transfer To LMS</span>
                    </div>
                    
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Expected Learning Start Date :</span>
                             <p>". strtoupper($Batch_StartDate) ."</p>
                        </div>
                    </div>
                     
                </div>";*/
            
              /*echo "   <div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Other Details :</span>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Medium :</span>
                             <p>". strtoupper($_Row['Admission_Medium']) ."</p>
                        </div>
                    </div>
                     
                </div>";*/
        
            
               echo" </div>
                
                
            </div>
        </div>";



        }
        //echo $response[0];

    }


if ($_action == "DETAILSFOREMP"){
    //print_r($_POST);die;
    
     //$responseEMPAK = $emp->checkEMPACKDETAILS($_POST["User_LearnerCode"]);
           // if($responseEMPAK[0]=='Success')
               // {
                    //    echo "EMPAK"; // detail allready in the database
               // }
           // else
                //{
    
                        $response = $emp->GetAll($_POST["LearnerCode"]);
                        $_DataTable = "";
                        //$response1=$response[2];
                        $_Row = mysqli_fetch_array($response[2]);
                        //print_r($_Row);//die;
                        $num_rows = mysqli_num_rows($response[2]);
                        $Tdate = date("d-m-Y", strtotime($_Row['exam_date']));
                        $num_rows;
                        if($num_rows==0)
                        {?><div class="error"><?php
                                echo "Not Available";
                                ?></div>
                                <?php 
                        }
                        else
                        {
                $responsesC = $emp->CenterDetails($_Row["Admission_ITGK_Code"]);
                $_RowC = mysqli_fetch_array($responsesC[2]);
                //$Admission_DOB = date("d-M-Y", strtotime($_Row['Admission_DOB']));
                
                
                if($_Row['Admission_DOB']=='' || $_Row['Admission_DOB']=='(NULL)'  || $_Row['Admission_DOB']=='0000-00-00 00:00:00')
                            { 
                                    $Admission_DOB="Not Available";
                            }
                     else
                           {
                                    $Admission_DOB = date("d M Y", strtotime($_Row['Admission_DOB']));
                           }
                
                 echo "<div class='container'>
                    <div class='row'>
                        <div  style='float: left;
            width: 94%;'>";

            echo "<div class='col-lg-12 col-md-12 bdrblue'>";
            echo "<div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Learner Code:</span>
                                     <p>". strtoupper($_POST["LearnerCode"]) . "</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>ITGK(Center) Code :</span>
                                     <p>". strtoupper($_Row['Admission_ITGK_Code']) . "</p>
                                </div>
                                </div>";
            echo "<div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Registered Name:</span>
                                     <p>". strtoupper($_Row['Admission_Name']) . "</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Father/Husband Name :</span>
                                     <p>". strtoupper($_Row['Admission_Fname']) . "</p>
                                </div>
                            </div>
                            <div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Date of Birth :</span>
                                     <p>". strtoupper($Admission_DOB) ."</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Mobile No :</span>
                                     <p>". strtoupper($_Row['Admission_Mobile']) ."</p>
                                </div>
                            </div>
                            <div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Address :</span>
                                     <p >". strtoupper($_Row['Admission_Address']) ."</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Email :</span>
                                     <p>" . strtoupper($_Row['Admission_Email']) . "</p>
                                </div>

                            </div>
                            <div class='persdetablog'>
                                 <div class='persdetacont'>
                                     <span>Enrolled Batch :</span>
                                     <p>" . strtoupper($_Row['Batch_Name']) . "</p>
                                </div>
                                 <div class='persdetacont'>
                                     <span>Registered Course :</span>
                                     <p>" . strtoupper($_Row['Course_Name']) . "</p>
                                </div>

                            </div>
                       </div>";

            /* New Tab*/









            echo"</div>
                </div>";

        }

        // }
    }
    
  /// Added : 9-4-2017  
if ($_action == "DETAILSFOREMPOFRALL"){
    //print_r($_POST);die;
    
     $responseEMPAK = $emp->checkEMPACKDETAILS($_POST["lcode"]);
           // if($responseEMPAK[0]=='Success')
               // {
                    //    echo "EMPAK"; // detail allready in the database
               // }
           // else
                //{
    
                        $response = $emp->GetAll($_POST["lcode"]);
                        $_DataTable = "";
                        //$response1=$response[2];
                        $_Row = mysqli_fetch_array($response[2]);
                        //print_r($_Row);//die;
                        $num_rows = mysqli_num_rows($response[2]);
                        $Tdate = date("d-m-Y", strtotime($_Row['exam_date']));
                        $num_rows;
                        if($num_rows==0)
                        {?><div class="error"><?php
                                echo "Not Available";
                                ?></div>
                                <?php 
                        }
                        else
                        {
                $responsesC = $emp->CenterDetails($_Row["Admission_ITGK_Code"]);
                $_RowC = mysqli_fetch_array($responsesC[2]);
                
                //$Admission_DOB = date("d-M-Y", strtotime($_Row['Admission_DOB']));
                
                if($_Row['Admission_DOB']=='' || $_Row['Admission_DOB']=='(NULL)'  || $_Row['Admission_DOB']=='0000-00-00 00:00:00')
                            { 
                                    $Admission_DOB="Not Available";
                            }
                     else
                           {
                                    $Admission_DOB = date("d M Y", strtotime($_Row['Admission_DOB']));
                           }
                
                 echo "<div class='container'>
                    <div class='row'>
                        <div  style='float: left;
            width: 94%;'>";

            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                           ";
            echo "<div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Registred Name :</span>
                                     <p>". strtoupper($_Row['Admission_Name']) . "</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Father/Husband Name :</span>
                                     <p>". strtoupper($_Row['Admission_Fname']) . "</p>
                                </div>
                            </div>
                            <div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Date of Birth :</span>
                                     <p>". strtoupper($Admission_DOB) ."</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Mobile No :</span>
                                     <p>". strtoupper($_Row['Admission_Mobile']) ."</p>
                                </div>
                            </div>
                            <div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Address :</span>
                                     <p >". strtoupper($_Row['Admission_Address']) ."</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Email :</span>
                                     <p>" . strtoupper($_Row['Admission_Email']) . "</p>
                                </div>

                            </div>
                            <div class='persdetablog'>
                                 <div class='persdetacont'>
                                     <span>Enrolled Batch :</span>
                                     <p>" . strtoupper($_Row['Batch_Name']) . "</p>
                                </div>
                                 <div class='persdetacont'>
                                     <span>Registered Course :</span>
                                     <p>" . strtoupper($_Row['Course_Name']) . "</p>
                                </div>

                            </div>
                       </div>";

            /* New Tab*/









            echo"</div>
                </div>";

        }

        // }
    }
	
	//Bellow file required to perform photo sign functions for learners
    require 'functions/learner_photo_signs.php';

    if ($_action == "uploadphotosign") {
        $type = ($_POST['phototype'] == 'lphoto') ? 'photo' : 'sign';
        $_UploadDir = $general->getDocRootDir() . '/upload/admission' . $type . '/';
        $learnercode = $_POST['learnercode'];
        $msg = "Unable to upload.";
        if(isset($_FILES[$_POST['phototype']]) && $_FILES[$_POST['phototype']]["name"] != '') {
            $imag =  $_FILES[$_POST['phototype']]["name"];
            $imageinfo = pathinfo($imag);
            $ext = strtolower($imageinfo['extension']);
            if ($ext != "png" && $ext != "jpg") {
                $msg = "Image must be in either PNG or JPG Format";
            } else {
                move_uploaded_file($_FILES[$_POST['phototype']]["tmp_name"], $_UploadDir . $learnercode . '_' . $type . '.png');
                $msg = "Learner " . $type . " has been updated.";
            }
        }

        echo $msg;
    }
	
	if ($_action == 'FILLPUBLISHEVENT') {
        $response = $emp->finalExamMappingEvents();
        echo "<optgroup label='Select multiple'>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
        }
    }

    if ($_action == 'getJobs') {
        $response = $emp->finalExamMappingCount($_POST["eventid"]);
        $row = (isset($response[2])) ? mysqli_fetch_array($response[2]) : ['n'=>350000];
        $table = "<div class='table-responsive' style='margin-top:10px'>
            <table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'><thead><tr>
            <th style='10%' nowrap>S No.</th>
            <th style='20%' nowrap>Execute Job</th>
            <th style='70%'>Result</th></tr></thead><tbody>";
        $jobs = ceil($row['n'] / 10000);
        for ($i = 1; $i <= $jobs; $i++) {
            $table .= "<tr class='odd gradeX'>";
            $table .= "<td nowrap>Job-" . $i . "</td>";
            $table .= "<td><a href='#' class='exejob' id='".$i."'>Execute</a></td>";
            $table .= "<td><span id='job" . $i . "'></span></td>";
            $table .= "</tr>";
        }
        echo $table .= "</tbody></table></div>";
    }

    if ($_action == 'uploadPics') {
        $target_dir = $general->getDocRootDir() . '/upload/missingphotosigns/';
        dirMake($target_dir);
        $images_arr = array();
        foreach($_FILES['images']['name'] as $key => $val){
            //upload and stored images
            $target_file = $target_dir . $_FILES['images']['name'][$key];
            if(move_uploaded_file($_FILES['images']['tmp_name'][$key], $target_file)){
                $images_arr[] = $target_file;
            }
        }

        print count($images_arr) . ' images are uploaded successfully.';
    }

    if ($_action == 'getPics') {
        $path = '/upload/missingphotosigns/';
        $dir = $general->getDocRootDir() . $path;
        dirMake($dir);
        echo $table = generatePhotoSignTable();
    }
	
	if ($_action == 'updatePhotoSigns') {
        $pics = getPhotoSigns();
        $path = $pics[0];
        $names = $pics[1];
        $learners = $pics[2];
        $itgks = [];
        $learnerCodes = array_keys($learners);
        //$learnerCodes = ['575170906011401789', '822170915121658789', '349170915125306789'];
        if ($learnerCodes) {
            $codes = implode("','", $learnerCodes);
            //Accurate result will come with function GetAllLearner() but mostly this code is executs on staging server, where admission table not updated completely so to fetch learner details we are using eligible learners table as alternate way.

            //$response = $emp->GetAllLearner($codes);

            //By using getEligibleLearnerDetails() only those photo/signs will update who's learner code will found in eligible learners table once.
            $response = $emp->getEligibleLearnerDetails($codes);
            $itgks = processFetchedLearnerPics($itgks, $response, $path);
        }

        echo $table = generatePhotoSignTable();
    }

    if($_action == "getLearnerEmailMobile"){
        $arr = [];
        $response = $emp->getLearnerEmailMobile($_SESSION["User_LearnerCode"]);
        $row = mysqli_fetch_array($response[2]);
        $arr["Admission_Email"] = $row["Admission_Email"];
        echo json_encode($arr);
    }

    if($_action == "updateEmailLearner"){
        $code = time();
        /*** GENERATE RANDOM OTP ***/
        $response1 = $emp->insertCodeLearnerEmail($code, $_POST['Admission_Email']);
        echo $response1[0];
    }

    if ($_action == "verifyEmail") {
        $response = $emp->verifyEmail(base64_decode($_POST['Code']));
        $_Row = mysqli_fetch_array($response[2]);

        if ($_Row['total_count'] == 0) {
            echo "Error";
        } else {
            $response1 = $emp->updateLearnerEmail($_Row['AO_Email'], $_SESSION["User_LearnerCode"], $_POST['Code']);
            echo $response1[0];
        }
    }
	
	
	function generatePhotoSignTable() {
        $pics = getPhotoSigns();
        $path = $pics[0];
        if (isset($_SERVER['LOCAL_ADDR']) && $_SERVER['LOCAL_ADDR'] == '10.1.1.158') {
            $path = '/myrkcl' . $path;
        }
        $names = $pics[1];
        $learnerCodes = $pics[2];
        $sn = 1;
        $table = '';
        if ($learnerCodes) {
        $table = '<div class="text-right"><input type="button" name="btnUpdate" id="btnUpdate" class="btn btn-primary" value="Update All" /></div>';
        $table .= "<div class='table-responsive' style='margin-top:10px'>
            <table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'><thead><tr>
            <th style='10%' nowrap>S No.</th>
            <th style='30%' nowrap>Learner Code</th>
            <th style='30%'>Photo</th>
            <th style='30%'>Sign</th>
            </tr></thead><tbody>";
        foreach ($learnerCodes as $learnerCode => $fileInfo) {
            $photo = $sign = '';
            if (isset($learnerCodes[$learnerCode]['sign'])) {
                $file = $learnerCode . '_sign.' .  $learnerCodes[$learnerCode]['sign'];
                $sign = '<img src="' . $path . $file . '" height="50px" /><br />' . $file;
            }
            if (isset($learnerCodes[$learnerCode]['photo'])) {
                $file = $learnerCode . '_photo.' .  $learnerCodes[$learnerCode]['photo'];
                $photo = '<img src="' . $path . $file . '" height="50px" /><br />' . $file;
            }

            $table .= "<tr class='odd gradeX'>";
            $table .= "<td>" . $sn . "</td>";
            $table .= "<td>" . $learnerCode . "</td>";
            $table .= "<td align='center'>" . $photo . "</td>";
            $table .= "<td align='center'>" . $sign . "</td>";
            $table .= "</tr>";
            $sn++;
        }
            $table .= "</tbody></table></div>";
        }

        return $table;
    }

