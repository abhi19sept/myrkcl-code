<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsreexamdatastatus.php';

$response = array();
$emp = new clsreexamdatastatus();


if ($_action == "schedule") {
    $response = $emp->GetDataAll($_POST['examevent']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
	echo "<th>Learner Code</th>";
	echo "<th>Learner Name</th>";
    echo "<th>Father Name</th>";
	echo "<th>Learner Mobile</th>";
    echo "<th>Batch Name</th>";
	echo "<th>Payment Status</th>";
    
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['itgkcode'] . "</td>";
		 echo "<td>" . $_Row['learnercode'] . "</td>";
       
        echo "<td>" . $_Row['learnername'] . "</td>";
        echo "<td>" . $_Row['fathername'] . "</td>";
		echo "<td>" . $_Row['admission_learnermobile'] . "</td>";
        echo "<td>" . $_Row['Batch_Name'] . "</td>";
		if($_Row['paymentstatus']==1)
		{
			$_confirm='Payment Confirmed';
			echo "<td>" . $_confirm . "</td>";
		}
		else
		{
			$_confirm='Payment Not Confirmed';
			echo "<td>" . $_confirm . "</td>";
			
		}
		 
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
?>