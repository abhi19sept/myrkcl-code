<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsFacultyResultRPT.php';

$response = array();
$emp = new clsFacultyResultRPT();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='40%'>IT-GK District</th>"; 
    echo "<th style='40%'>IT-GK Tehsil</th>"; 
    echo "<th style='10%'>SP Name</th>";
    echo "<th style='10%'>Faculty Name</th>";
    echo "<th style='40%'>Exam Date</th>";
    echo "<th style='40%'>Attempt No</th>";    
    echo "<th style='40%'>Obtained Marks</th>";
    echo "<th style='10%'>Total Marks</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['ITGK_Name'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['RSP_Name'] . "</td>";
            echo "<td>" . $_Row['Staff_Name'] . "</td>";
            echo "<td>" . $_Row['ResExam_date']. "</td>";
            echo "<td>" . $_Row['ResExam_attempt'] . "</td>";
            echo "<td>" . $_Row['Obtained_Marks'] . "</td>";
            echo "<td>" . $_Row['ResExam_totalmarks'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

