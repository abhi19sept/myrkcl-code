<?php

/**
 * Created by Hariom Awasthi
 */

include './commonFunction.php';
require 'BAL/clsSupportStatusUpdate.php';

$response = array();
$response1 = array();
$emp = new clsSupportStatusUpdate();

if ($_action == "DETAILS") {

    //print_r($_POST);die;
    $itgkCode = $_POST["itgkCode"];
    $columnForUpdate = $_POST["columnForUpdate"];
    $response = $emp->updateITGKStausForSupport($itgkCode, $columnForUpdate);

    if ($response[0] == "Successfully Updated") {
        echo "<div class='success' style='color: green;'>";
        echo " Record Successfully Updated.";
        echo "</div>";
    } else {
        echo "<div class='error'>";
        echo "No Record Updated.";
        echo "</div>";
    }
    //echo $response[0];

}
