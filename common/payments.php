<?php
    
    @date_default_timezone_set("Asia/Kolkata");

	class paymentFunctions 
	{
        const PAY_MODE = 'Live'; //Test / Live
        const TEST_ITGK_CODE = '34290065'; //ncr: '11290436';

		private function dbConnection() {
			global $_ObjConnection;
        	$_ObjConnection->Connect();

        	return $_ObjConnection;
		}

        public function generateTransactionId() {
            $txtGeneratePayUId = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $txtGenerateId = (mt_rand(1000, 9999)) . date("ymdHis");
            $_RKCL_Id = $txtGenerateId . '_RKCLtranid';

            return [$txtGeneratePayUId, $_RKCL_Id];
        }
	 	/**
	     * Add Payment Transaction Record
	     */
	 	public function insertPaymentTransaction($productInfo, $_ITGK_Code, $unitAmount, $codes, $course, $event, $batch = 0, $pkg = 0) {
	 		$_ObjConnection = $this->dbConnection();

            if (self::PAY_MODE == 'Test' && $_ITGK_Code != self::TEST_ITGK_CODE) return '';

            if (! $unitAmount || $unitAmount < 0 || empty($productInfo) || empty($_ITGK_Code)) return '';

        //    $isValid = $this->validateTimeOfInitiatedTransaction($productInfo, $_ITGK_Code, $codes);
		if($productInfo == 'ExperienceCenterFeePayment' && $course=='2'){
					$codes[]=$codes[0];
					$isValid = $this->validateTimeOfInitiatedTransaction($productInfo, $_ITGK_Code, $codes);
					
				}
				else if($productInfo == 'BuyRedHatSubscription'){
					$codes[]=$codes[0];
					$isValid = $this->validateTimeOfInitiatedTransaction($productInfo, $_ITGK_Code, $codes);
				}

				
				else{
					$isValid = $this->validateTimeOfInitiatedTransaction($productInfo, $_ITGK_Code, $codes);
				}
		
            if (!$isValid) return 'TimeCapErr';

            $transactionIds = $this->generateTransactionId();
            $txtGeneratePayUId = $transactionIds[0];
            $_RKCL_Id = $transactionIds[1];

           if($productInfo == 'ExperienceCenterFeePayment'){
					if($course=='1'){
						$Lcount = count($codes);
						$amount = ($unitAmount * $Lcount);
						$return = '';
					}
					else{
						$Lcount = '1';
						$amount = $unitAmount;
						$return = '';
					}
				}
				
				else if($productInfo == 'BuyRedHatSubscription'){
						$Lcount = $course;
						$amount = $unitAmount;
						$return = '';
				}

				else {
					$Lcount = count($codes);
					$amount = ((int) trim($unitAmount) * (int) trim($Lcount));
					//$amount = ($unitAmount * $Lcount);
					$return = '';
					
				}
            

          //  $fieldname=($productInfo == 'EOI Fee Payment' || $productInfo == 'ExperienceCenterFeePayment') ? 'Pay_Tran_Eoi_Code' : 
			//			'Pay_Tran_ReexamEvent';
            $fieldname=($productInfo == 'EOI Fee Payment' || $productInfo == 'ExperienceCenterFeePayment' || 
						$productInfo == 'BuyRedHatSubscription') ? 'Pay_Tran_Eoi_Code' : 'Pay_Tran_ReexamEvent';



            $payStatus = 'PaymentInitiated';

        //    $_InsertQuery = "INSERT IGNORE INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_Fname, Pay_Tran_RKCL_Trnid, Pay_Tran_AdmissionArray, Pay_Tran_LCount, Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_Course, $fieldname, Pay_Tran_Batch, Pay_Tran_PG_Trnid, Pay_Tran_CoursePkg) VALUES ('" . $_ITGK_Code . "', (SELECT up.UserProfile_FirstName FROM tbl_user_master um INNER JOIN tbl_userprofile up ON up.UserProfile_User = um.User_Code WHERE um.User_LoginId = '" . $_ITGK_Code . "'),'" . $_RKCL_Id . "', '" . implode(',', $codes) . "', '" . $Lcount . "', '" . $amount . "', '" . $payStatus . "', '" . $productInfo . "', '" . $course . "', '" . $event . "', '" . $batch . "', '" . $txtGeneratePayUId . "', '" . $pkg . "')";
		
		if($productInfo == 'ExperienceCenterFeePayment' && $course=='2'){
					$_InsertQuery = "INSERT IGNORE INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_Fname, Pay_Tran_RKCL_Trnid, Pay_Tran_AdmissionArray, Pay_Tran_LCount, Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_Course, $fieldname, Pay_Tran_Batch, Pay_Tran_PG_Trnid, Pay_Tran_CoursePkg) VALUES ('" . $_ITGK_Code . "', (SELECT up.UserProfile_FirstName FROM tbl_user_master um INNER JOIN tbl_userprofile up ON up.UserProfile_User = um.User_Code WHERE um.User_LoginId = '" . $_ITGK_Code . "'),'" . $_RKCL_Id . "', '" . $codes[0] . "', '" . $Lcount . "', '" . $amount . "', '" . $payStatus . "', '" . $productInfo . "', '" . $course . "', '" . $event . "', '" . $batch . "', '" . $txtGeneratePayUId . "', '" . $pkg . "')";				
				}
				
else if($productInfo == 'BuyRedHatSubscription'){
					$_InsertQuery = "INSERT IGNORE INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_Fname, Pay_Tran_RKCL_Trnid, 
					Pay_Tran_AdmissionArray, Pay_Tran_LCount, Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_Course,
					$fieldname, Pay_Tran_Batch, Pay_Tran_PG_Trnid, Pay_Tran_CoursePkg) VALUES ('" . $_ITGK_Code . "', (SELECT
					up.UserProfile_FirstName FROM tbl_user_master um INNER JOIN tbl_userprofile up ON up.UserProfile_User = um.User_Code
					WHERE um.User_LoginId = '" . $_ITGK_Code . "'),'" . $_RKCL_Id . "', '" . $codes[0] . "', '" . $Lcount . "', '" . 
					$amount . "', '" . $payStatus . "', '" . $productInfo . "', '22', '" . $event . "', '" . $batch . "', 
					'" . $txtGeneratePayUId . "', '" . $pkg . "')";					
				}

				
				
				
				else{
					$_InsertQuery = "INSERT IGNORE INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_Fname, Pay_Tran_RKCL_Trnid, Pay_Tran_AdmissionArray, Pay_Tran_LCount, Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_Course, $fieldname, Pay_Tran_Batch, Pay_Tran_PG_Trnid, Pay_Tran_CoursePkg) VALUES ('" . $_ITGK_Code . "', (SELECT up.UserProfile_FirstName FROM tbl_user_master um INNER JOIN tbl_userprofile up ON up.UserProfile_User = um.User_Code WHERE um.User_LoginId = '" . $_ITGK_Code . "'),'" . $_RKCL_Id . "', '" . implode(',', $codes) . "', '" . $Lcount . "', '" . $amount . "', '" . $payStatus . "', '" . $productInfo . "', '" . $course . "', '" . $event . "', '" . $batch . "', '" . $txtGeneratePayUId . "', '" . $pkg . "')";
				}
		
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            if ($codes && $_Response[0] == Message::SuccessfullyInsert) {
                $_SelectQuery = "SELECT Pay_Tran_PG_Trnid FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $txtGeneratePayUId . "' AND Pay_Tran_ProdInfo = '" . $productInfo . "' AND Pay_Tran_Status = '" . $payStatus . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                if ($_Response1[0] != Message::NoRecordFound) {
                    $getStoredTrnxId = mysqli_fetch_array($_Response1[2]);
                    switch ($productInfo) {
                        case 'ReexamPayment':
                            $return = $this->updateExamData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes);
                            break;
                        case 'LearnerFeePayment':
                            $return = $this->updateAdmissionData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes, $course, $batch);
                            break;
                        case 'Correction Certificate':
                        case 'Duplicate Certificate':
						case 'Duplicate Certificate with Correction':

                            $return = $this->updateCorrectionData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes);
                            break;
                        case 'NcrFeePayment':
                            $return = $this->updateOrgMaster($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes[0]);
                            break;
                        case 'Name Change Fee Payment':
                        case 'Address Change Fee Payment':
                            $return = $this->updateAddressNameTrnx($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes[0], $productInfo);
                            break;
                        case 'EOI Fee Payment':
                            $return = $this->updateCourseitgkMapping($getStoredTrnxId['Pay_Tran_PG_Trnid'], $codes[0], $_ITGK_Code);
                            break;
						case 'RedHatFeePayment':
                            $return = $this->updateRedHatAdmissionData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes, $course, $batch);
                            break;
						case 'OwnershipFeePayment':
                            $return = $this->updateOwnershipChange($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes[0]);
                            break;
						case 'AadharUpdFeePayment':
                        $return = $this->updateAadharData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes, $course, $batch);
                        break;	
						case 'PenaltyFeePayment':
                            $return = $this->updateRenewalPenalty($getStoredTrnxId['Pay_Tran_PG_Trnid'], $codes[0], $_ITGK_Code);
                            break;
							
						case 'ExperienceCenterFeePayment':
                            $return = $this->updateExperienceCenter($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $course, $_ITGK_Code, $amount, $codes);
                            break;
                        
						case 'BuyRedHatSubscription':
                            $return = $this->updateRHLSDetails($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $course, 
							$_ITGK_Code, $amount, $codes);
                            break;
						case 'LearnerEnquiryPayment':
                            $return = $this->updateLearnerAdmissionData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes, $course, $batch);
                            break;
						case 'RscfaCertificatePayment':
                            $return = $this->updateRscfaCertificateData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes,
							$course, $batch);
                            break;
						case 'RscfaReExamPayment':
                            $return = $this->updateRscfaReExamData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes,
							$course, $batch);
                            break;
                    }
                }
            }

            return $return;
        }

        public function getTimeCap() {
            $timeCap = time() - (15*60);
            return date("Y-m-d H:i:s", $timeCap);
        }

        /**
         * Function to Validate Initiated Transaction with Time Limit
         */
        public function validateTimeOfInitiatedTransaction($productInfo, $_ITGK_Code, $codes) {
            $_ObjConnection = $this->dbConnection();
            $status = 1;
            $maxTimeCap = $this->getTimeCap();
            $filter = '';
            foreach ($codes as $code) {
                $conditions[] = "Pay_Tran_AdmissionArray LIKE ('%" . $code . "%')";
            }
            $filter = ' AND (' . implode(' OR ', $conditions) . ')';
            //Pay_Tran_AdmissionArray
            $query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_ITGK = '" . $_ITGK_Code . "' AND Pay_Tran_ProdInfo = '" . $productInfo . "' AND timestamp > '" . $maxTimeCap . "' AND Pay_Tran_Status IN ('PaymentInitiated','PaymentInProcess','PaymentReceive') $filter";

          //  if ($_ITGK_Code == '51290039') echo $query;

            $_Response1 = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            if ($_Response1[0] != Message::NoRecordFound) {
                $status = 0;
            }

            return $status;
        }

        private function updateAdmissionData($transactionId, $rkclId, $codes, $course, $batch) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_admission SET Admission_RKCL_Trnid = '" . $rkclId . "', Admission_TranRefNo = '" . $transactionId . "' WHERE Admission_Code IN (" . implode(',', $codes) . ") AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "' AND Admission_Payment_Status = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }
		
		private function updateRscfaCertificateData($transactionId, $rkclId, $codes, $course, $batch) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_apply_rscfa_certificate SET apply_rscfa_cert_rkcl_txnid = '" . $rkclId . "',
							apply_rscfa_cert_txnid = '" . $transactionId . "' WHERE
							apply_rscfa_cert_id IN (" . implode(',', $codes) . ") AND apply_rscfa_cert_course = '" . $course . "'
							AND apply_rscfa_cert_batch = '" . $batch . "' AND apply_rscfa_cert_payment_status = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        } 
		
		private function updateRscfaReExamData($transactionId, $rkclId, $codes, $course, $batch) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_rscfa_reexam_application SET rscfa_reexam_RKCL_Trnid = '" . $rkclId . "',
							rscfa_reexam_TranRefNo = '" . $transactionId . "' WHERE
							rscfa_reexam_code IN (" . implode(',', $codes) . ") AND coursename = '" . $course . "'
							AND batchname = '" . $batch . "' AND rscfa_reexam_paymentstatus = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        } 
		
		private function updateLearnerAdmissionData($transactionId, $rkclId, $codes, $course, $batch) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_admission SET Admission_RKCL_Trnid = '" . $rkclId . "', Admission_TranRefNo = '" . $transactionId . "' WHERE Admission_Code IN (" . implode(',', $codes) . ") AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "' AND Admission_Payment_Status = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }
		
		private function updateRedHatAdmissionData($transactionId, $rkclId, $codes, $course, $batch) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_admission SET Admission_RKCL_Trnid = '" . $rkclId . "', 
							Admission_TranRefNo = '" . $transactionId . "' WHERE Admission_Code IN (" . implode(',', $codes) . ")
							AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "' AND 
							Admission_Payment_Status = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }
		 private function updateOwnershipChange($transactionId, $rkclId, $ackCode) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_ownership_change SET Org_RKCL_Trnid = '" . $rkclId . "', Org_TranRefNo = '" . $transactionId . "' WHERE Org_Ack = '" . $ackCode . "' AND Org_PayStatus = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }
		
		  private function updateAadharData($transactionId, $rkclId, $codes, $course, $batch) {
        $_ObjConnection = $this->dbConnection();
        $return = '';
        $_UpdateQuery = "UPDATE tbl_adm_log_foraadhar SET Adm_Log_RKCL_Trnid = '" . $rkclId . "', Adm_Log_TranRefNo = '" . $transactionId . "' WHERE Adm_Log_AdmissionCode IN (" . implode(',', $codes) . ") AND Adm_Log_Course  = '" . $course . "' AND Adm_Log_Batch = '" . $batch . "' AND Adm_Log_PayStatus = '0'";
        $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
        if ($_Response2[0] == Message::SuccessfullyUpdate) {
            $return = $transactionId;
        }

        return $return;
    }
	
	private function updateRenewalPenalty($transactionId, $paneltyCode, $ITGK_Code) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_renewal_penalty SET RenewalPenalty_Trans_Id = '" . $transactionId . "' WHERE RenewalPenalty_ItgkCode = '" . $ITGK_Code . "' AND RenewalPenalty_Code = '" . $paneltyCode . "' AND RenewalPenalty_Payment_Status = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }

        private function updateExamData($transactionId, $rkclId, $codes) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE examdata SET reexam_TranRefNo = '" . $transactionId . "', reexam_RKCL_Trnid = '" . $rkclId . "' WHERE examdata_code IN (" . implode(',', $codes) . ") AND paymentstatus = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }

        private function updateCorrectionData($transactionId, $rkclId, $codes) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_correction_copy SET Correction_RKCL_Trnid = '" . $rkclId . "', Correction_TranRefNo = '" . $transactionId . "' WHERE cid IN (" . implode(',', $codes) . ") AND Correction_Payment_Status = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }

        private function updateOrgMaster($transactionId, $rkclId, $ackCode) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_org_master SET Org_RKCL_Trnid = '" . $rkclId . "', Org_TranRefNo = '" . $transactionId . "' WHERE Org_Ack = '" . $ackCode . "' AND Org_PayStatus = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }

   /**     private function updateAddressNameTrnx($transactionId, $rkclId, $refCode, $productInfo) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $payFor = ($productInfo == 'Name Change Fee Payment') ? 'name' : 'address';
            $_UpdateQuery = "UPDATE tbl_address_name_transaction SET fld_RKCL_Trnid = '" . $rkclId . "',
                fld_paymentTitle = '" . $payFor . "', 
                fld_transactionID = '" . $transactionId . "' 
                WHERE fld_ref_no = '" . $refCode . "' AND fld_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' AND fld_paymentStatus = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }
**/

private function updateAddressNameTrnx($transactionId, $rkclId, $refCode, $productInfo) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $payFor = ($productInfo == 'Name Change Fee Payment') ? 'name' : 'address';
            if($payFor == 'name'){
            $_UpdateQuery = "UPDATE tbl_address_name_transaction SET fld_RKCL_Trnid = '" . $rkclId . "',
                fld_paymentTitle = '" . $payFor . "', 
                fld_transactionID = '" . $transactionId . "' 
                WHERE fld_ref_no = '" . $refCode . "' AND fld_ITGK_Code = '" . $_SESSION['User_LoginId'] . "' AND fld_paymentStatus = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }
            } else {
                
            $fld_rsp = $_SESSION['User_Rsp'];
            $fld_ref_no = $refCode;
            $fld_ITGK_UserCode = $_SESSION['User_Code'];
            $fld_ITGK_Code = $_SESSION['User_LoginId'];

            $_InsertQuery = "INSERT INTO tbl_address_name_transaction (`fld_ref_no`,`fld_rsp`, `fld_ITGK_UserCode`,`fld_ITGK_Code`,`fld_paymentStatus`,`fld_RKCL_Trnid`,`fld_paymentTitle`,`fld_transactionID`) "
                    . "VALUES ('" . $fld_ref_no . "', '" . $fld_rsp . "', '" . $fld_ITGK_UserCode . "','" . $fld_ITGK_Code . "', '0','" . $rkclId . "','" . $payFor . "','" . $transactionId . "') ";

            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement); 
            
            if ($_Response[0] == Message::SuccessfullyInsert) {
                $return = $transactionId;
            }
            }
           
            
            
            

            return $return;
        }



        private function updateCourseitgkMapping($transactionId, $eoiCode, $ITGK_Code) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_courseitgk_mapping SET Courseitgk_TranRefNo = '" . $transactionId . "' WHERE Courseitgk_ITGK = '" . $ITGK_Code . "' AND Courseitgk_EOI = '" . $eoiCode . "' AND EOI_Fee_Confirm = '0'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }
		
		
		private function updateExperienceCenter($transactionId, $_RKCL_Id, $course, $_ITGK_Code, $amount, $codes) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
				$_SelectQuery="Select * FROM tbl_redhat_exp_center WHERE exp_center_code = '".$_ITGK_Code."' and exp_center_eoi='18'
								and exp_center_pay_status='0'";          
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					if($_Response[0]=='Success'){
						if($course=='1'){
							$_UpdateQuery = "UPDATE tbl_redhat_exp_center SET exp_center_txnid = '" . $transactionId . "',
										exp_center_rkcl_id='".$_RKCL_Id."' WHERE exp_center_code = '" . $_ITGK_Code . "' AND 
										exp_center_eoi = '18' AND exp_center_pay_status = '0'";
						}
						else{
							$_UpdateQuery = "UPDATE tbl_redhat_exp_center SET exp_center_txnid = '" . $transactionId . "',
										exp_center_rkcl_id='".$_RKCL_Id."',exp_center_lcode='".$codes[0]."',
										exp_center_lname='".$codes[1]."',exp_center_fname='".$codes[2]."',
										exp_center_dob='".$codes[3]."' WHERE exp_center_code = '" . $_ITGK_Code . "' AND 
										exp_center_eoi = '18' AND exp_center_pay_status = '0'";
						}
						
						$_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
						if ($_Response2[0] == Message::SuccessfullyUpdate) {
							$return = $transactionId;
						}
							return $return;
					}
					else{
						if($course=='1'){
							$_Insert = "Insert into tbl_redhat_exp_center (exp_center_code,exp_center_eoi,exp_center_course_code,
									exp_center_amount,exp_center_pay_status,exp_center_txnid,exp_center_rkcl_id,exp_center_sub_type) 
									values ('".$_ITGK_Code."','18','22','".$amount."','0','".$transactionId."','".$_RKCL_Id."',
											'".$course."')";
						}
						else{
							
							$_Insert = "Insert into tbl_redhat_exp_center (exp_center_code,exp_center_eoi,exp_center_course_code,
									exp_center_amount,exp_center_pay_status,exp_center_txnid,exp_center_rkcl_id,exp_center_sub_type,
									exp_center_lcode,exp_center_lname,exp_center_fname,exp_center_dob) 
									values ('".$_ITGK_Code."','18','22','".$amount."','0','".$transactionId."','".$_RKCL_Id."',
											'".$course."','".$codes[0]."','".$codes[1]."','".$codes[2]."','".$codes[3]."')";
						}
						$_chkDuplicate_learnerCode="Select * FROM tbl_redhat_exp_center WHERE 
															exp_center_lcode = '".$codes[0]."'";          
							$_Response2 = $_ObjConnection->ExecuteQuery($_chkDuplicate_learnerCode, Message::SelectStatement);
								if ($_Response2[0] == Message::NoRecordFound){
									$_Responses = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
								}
								else {
                                $return='duplicate';
                            }
						if ($_Responses[0] == Message::SuccessfullyInsert) {
							$return = $transactionId;
						}
							return $return;
					}           
        }
private function updateRHLSDetails($transactionId, $_RKCL_Id, $course, $_ITGK_Code, $amount, $codes) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
				$_SelectQuery="Select * FROM tbl_buy_RHLS WHERE rhls_code = '".$_ITGK_Code."' and rhls_eoi='18'
								and rhls_pay_status='0'";          
				$_Response=$_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
					if($_Response[0]=='Success'){						
							$_UpdateQuery = "UPDATE tbl_buy_RHLS SET rhls_txnid = '" . $transactionId . "', rhls_qty='".$course."',
										rhls_rkcl_id='".$_RKCL_Id."', rhls_amount='".$amount."' WHERE rhls_code = '" . $_ITGK_Code . "'
										AND rhls_eoi = '18' AND rhls_pay_status = '0'";
						
						$_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
						if ($_Response2[0] == Message::SuccessfullyUpdate) {
							$return = $transactionId;
						}
							return $return;
					}
					else{
							$_Insert = "Insert into tbl_buy_RHLS (rhls_code,rhls_eoi,rhls_course_code,
									rhls_amount,rhls_pay_status,rhls_txnid,rhls_rkcl_id,rhls_qty) 
									values ('".$_ITGK_Code."','18','22','".$amount."','0','".$transactionId."','".$_RKCL_Id."',
											'".$course."')";
						
							$_Responses = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
								
						if ($_Responses[0] == Message::SuccessfullyInsert) {
							$return = $transactionId;
						}
							return $return;
					}           
        }
        /**
         * Function to Validate Transaction Before Start Process with Time Limit
         */
        public function validateTransactionForTime($trnxId) {
            $_ObjConnection = $this->dbConnection();
            $status = 1;
            $maxTimeCap = $this->getTimeCap();
            $filter = '';
            $conditions = [];
            $query = "SELECT Pay_Tran_AdmissionArray FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $trnxId . "'";
            $_Response1 = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
            if ($_Response1[0] != Message::NoRecordFound) {
                $row = mysqli_fetch_array($_Response1[2]);
                $codes = explode(',', $row['Pay_Tran_AdmissionArray']);
                foreach ($codes as $code) {
                    $conditions[] = "pt2.Pay_Tran_AdmissionArray LIKE ('%" . $code . "%')";
                }

                $filter = ' AND (' . implode(' OR ', $conditions) . ')';

                //Pay_Tran_AdmissionArray
                $query = "SELECT pt2.* FROM tbl_payment_transaction pt1, tbl_payment_transaction pt2 WHERE pt1.Pay_Tran_PG_Trnid = '" . $trnxId . "' AND pt1.Pay_Tran_ITGK = pt2.Pay_Tran_ITGK AND pt1.Pay_Tran_ProdInfo = pt2.Pay_Tran_ProdInfo AND pt2.timestamp > '" . $maxTimeCap . "' AND pt2.Pay_Tran_Status IN ('PaymentInitiated','PaymentInProcess','PaymentReceive') AND pt2.Pay_Tran_PG_Trnid <> pt1.Pay_Tran_PG_Trnid $filter";

                $_Response2 = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
                if ($_Response2[0] != Message::NoRecordFound) {
                    $status = 0;
                }
            }

            return $status;
        }

        public function GetPaymentDataByTrnxid($trnxId, $status = 'PaymentInitiated') {
            $_ObjConnection = $this->dbConnection();

            $isValid = $this->validateTransactionForTime($trnxId);
            if (!$isValid) return '';

            try {
                if (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 19) {
                    $_SelectQuery = "SELECT pt.Pay_Tran_PG_Trnid, pt.Pay_Tran_ProdInfo, pt.Pay_Tran_Amount, pt.Pay_Tran_ITGK, pt.Pay_Tran_RKCL_Trnid, pt.Pay_Tran_Course, pt.Pay_Tran_Batch, pt.Pay_Tran_CoursePkg, pt.Pay_Tran_Status, pt.timestamp, ad.Admission_Name AS UserProfile_FirstName, ad.Admission_Mobile AS UserProfile_Mobile, IF(ad.Admission_Email <> '' AND ad.Admission_Email IS NOT NULL, ad.Admission_Email, um.User_EmailId) AS UserProfile_Email FROM tbl_payment_transaction pt 
                        LEFT OUTER JOIN examdata ed ON (pt.Pay_Tran_PG_Trnid = ed.reexam_TranRefNo AND pt.Pay_Tran_ProdInfo = 'ReexamPayment')
                        LEFT OUTER JOIN tbl_correction_copy tp ON (pt.Pay_Tran_PG_Trnid = tp.Correction_TranRefNo AND pt.Pay_Tran_ProdInfo LIKE '%Certificate%')
                        INNER JOIN tbl_admission ad ON (
                            ed.learnercode = ad.Admission_LearnerCode OR 
                            tp.lcode = ad.Admission_LearnerCode
                        ) 
                        INNER JOIN tbl_user_master um ON pt.Pay_Tran_ITGK = um.User_LoginId 
                        WHERE pt.Pay_Tran_PG_Trnid = '" . $trnxId . "' ORDER BY pt.timestamp LIMIT 1";
                } else {
                    $_SelectQuery = "SELECT pt.Pay_Tran_PG_Trnid, pt.Pay_Tran_ProdInfo, pt.Pay_Tran_Amount, pt.Pay_Tran_ITGK, pt.Pay_Tran_RKCL_Trnid, pt.Pay_Tran_Course, pt.timestamp, pt.Pay_Tran_Batch, pt.Pay_Tran_CoursePkg, um.User_EmailId AS UserProfile_Email, od.Organization_Name AS UserProfile_FirstName, um.User_MobileNo AS UserProfile_Mobile FROM tbl_payment_transaction pt INNER JOIN tbl_user_master um ON pt.Pay_Tran_ITGK = um.User_LoginId 
LEFT OUTER JOIN tbl_userprofile up ON (up.UserProfile_User = um.User_Code)
LEFT OUTER JOIN tbl_organization_detail od ON (od.Organization_User = um.User_Code)
WHERE pt.Pay_Tran_PG_Trnid = '" . trim($trnxId) . "'";
                }
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }

            return $_Response;
        }
	}

	$payment = new paymentFunctions();
	
?>