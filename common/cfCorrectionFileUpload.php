<?php

session_start();

//$_SESSION['ImageFile'] = "";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

$_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_photo/';
$_UploadDirectory2 = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_marksheet/';
$_UploadDirectory3 = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_certificate/';
$_UploadDirectory5 = $_SERVER['DOCUMENT_ROOT'] . '/upload/correction_provisional/';

$parentid = $_POST['UploadId'];

if ($_FILES["SelectedFile1"]["name"] != '') {
    $imag = $_FILES["SelectedFile1"]["name"];
    $imageinfo = pathinfo($_FILES["SelectedFile1"]["name"]);
    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
        $flag = 0;
    } else {
        if (file_exists("$_UploadDirectory1/" . $parentid . '_photo' . '.' . substr($imag, -3))) {
            $_FILES["SelectedFile1"]["name"] . "already exists";
        } else {
            if (file_exists($_UploadDirectory1)) {
                if (is_writable($_UploadDirectory1)) {
                    move_uploaded_file($_FILES["SelectedFile1"]["tmp_name"], "$_UploadDirectory1/" . $parentid . '_photo' . '.jpg');
                    //session_start();
                    $_SESSION['CorrectionPhoto'] = $parentid . '_photo' . '.jpg';
                } else {
                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
                }
            } else {
                outputJSON("<span class='error'>Upload Folder Not Available</span>");
            }
        }
    }
}


if ($_FILES["SelectedFile2"]["name"] != '') {
    $imag = $_FILES["SelectedFile2"]["name"];
    $imageinfo = pathinfo($_FILES["SelectedFile2"]["name"]);
    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
        $flag = 0;
    } else {
        if (file_exists("$_UploadDirectory2/" . $parentid . '_marksheet' . '.' . substr($imag, -3))) {
            $_FILES["SelectedFile2"]["name"] . "already exists";
        } else {
            if (file_exists($_UploadDirectory2)) {
                if (is_writable($_UploadDirectory2)) {
                    move_uploaded_file($_FILES["SelectedFile2"]["tmp_name"], "$_UploadDirectory2/" . $parentid . '_marksheet' . '.jpg');
                    //session_start();
                    $_SESSION['CorrectionMarksheet'] = $parentid . '_marksheet' . '.jpg';
                } else {
                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
                }
            } else {
                outputJSON("<span class='error'>Upload Folder Not Available</span>");
            }
        }
    }
}


if ($_FILES["SelectedFile3"]["name"] != '') {
    $imag = $_FILES["SelectedFile3"]["name"];
    $imageinfo = pathinfo($_FILES["SelectedFile3"]["name"]);
    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
        $flag = 0;
    } else {
        if (file_exists("$_UploadDirectory3/" . $parentid . '_certificate' . '.' . substr($imag, -3))) {
            $_FILES["SelectedFile3"]["name"] . "already exists";
        } else {
            if (file_exists($_UploadDirectory3)) {
                if (is_writable($_UploadDirectory3)) {
                    move_uploaded_file($_FILES["SelectedFile3"]["tmp_name"], "$_UploadDirectory3/" . $parentid . '_certificate' . '.jpg');
                    //session_start();
                    $_SESSION['CorrectionCertificate'] = $parentid . '_certificate' . '.jpg';
                } else {
                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
                }
            } else {
                outputJSON("<span class='error'>Upload Folder Not Available</span>");
            }
        }
    }
}

if ($_FILES["SelectedFile5"]["name"] != '') {
    $imag = $_FILES["SelectedFile5"]["name"];
    $imageinfo = pathinfo($_FILES["SelectedFile5"]["name"]);
    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
        $flag = 0;
    } else {
        if (file_exists("$_UploadDirectory5/" . $parentid . '_provisional' . '.' . substr($imag, -3))) {
            $_FILES["SelectedFile5"]["name"] . "already exists";
        } else {
            if (file_exists($_UploadDirectory5)) {
                if (is_writable($_UploadDirectory5)) {
                    move_uploaded_file($_FILES["SelectedFile5"]["tmp_name"], "$_UploadDirectory5/" . $parentid . '_provisional' . '.jpg');
                    //session_start();
                    $_SESSION['CorrectionProvisional'] = $parentid . '_provisional' . '.jpg';
                } else {
                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
                }
            } else {
                outputJSON("<span class='error'>Upload Folder Not Available</span>");
            }
        }
    }
}			