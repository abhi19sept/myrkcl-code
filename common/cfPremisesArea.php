<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsPremisesArea.php';

$response = array();
$emp = new clsPremisesArea();


if ($_action == "GETAREA") {
    $response = $emp->GetArea();
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if($co){
    while ($_Row = mysqli_fetch_array($response[2], MYSQLI_ASSOC)) {
       
        $_DataTable[$_i] = array("area" => $_Row['Premises_area']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
} else {
        echo "";
    }
}

if ($_action == "SubmitArea") {
//    print_r($_POST);
//    die;
    if (isset($_POST["TotalArea"]) || isset($_POST["TotalArea1"])) {
        if (isset($_POST["area"]) || isset($_POST["area"])) {
        //$_AreaType = $_POST["area"];
        if ($_POST["area"] == "Urban") {
            $_AreaType = 'Yes';
        if($_POST["TheoryRoomArea"] > '0' && $_POST["LabArea"] > '0' && $_POST["TotalArea"] > '0'){
         $_TheoryArea = $_POST["TheoryRoomArea"];
         $_LabArea = $_POST["LabArea"];
         $_TotalArea = $_POST["TotalArea"];
        } else {
            echo "Please Enter All Rooms Area in Sq.Ft.";
            return;
        }
        }
        if ($_POST["area"] == "Rural") {
            $_AreaType = 'No';
         $_TheoryArea = '0';
         $_LabArea = '0';
         if($_POST["TotalArea1"] > '0'){
         $_TotalArea = $_POST["TotalArea1"];
        } else {
            echo "Please Enter Total Premises Area in Sq.Ft.";
            return;
        }
        } 
        
        
        $response = $emp->UpgradePremisesArea($_AreaType, $_TheoryArea, $_LabArea, $_TotalArea);
        echo $response[0];
    } else {
            echo "Please Select whether Seperate Area or Not";
            return;
        }

    }
}