<?php
session_start();
//$_SESSION['ImageFile'] = "";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}


$_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_photo/';
$_UploadDirectory2 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_sign/';

$parentid = $_POST['UploadId'];

				if($_FILES["SelectedFile1"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile1"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile1"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
							if (file_exists($_UploadDirectory1)) {
								if (is_writable($_UploadDirectory1)) {
									$filepath = "$_UploadDirectory1/". $parentid . '_photo' . '.jpg';
									move_uploaded_file($_FILES["SelectedFile1"]["tmp_name"], $filepath);
									if (!file_exists($filepath)) {
										outputJSON("<span class='error'>Unable to upload photo, Please re-try!</span>");
									}
								} else {
									outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
									 }
							} else {
								outputJSON("<span class='error'>Upload Folder Not Available</span>");
							}
						}	
			     }
				 
				 
				 if($_FILES["SelectedFile2"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile2"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile2"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
							if (file_exists($_UploadDirectory2)) {
								if (is_writable($_UploadDirectory2)) {
									$filepath = "$_UploadDirectory2/".$parentid .'_sign'.'.jpg';
									move_uploaded_file($_FILES["SelectedFile2"]["tmp_name"], $filepath);
									if (!file_exists($filepath)) {
										outputJSON("<span class='error'>Unable to upload sign, Please re-try!</span>");
									} else {
                                        outputJSON("Upload Sucessfully.", 'success');
                                    }
								} 
								else {
										outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
									 }
							} else {
								outputJSON("<span class='error'>Upload Folder Not Available</span>");
							}
						}	
			     }