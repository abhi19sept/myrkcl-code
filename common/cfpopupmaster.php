<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clspopupmaster.php';

$response = array();
$emp = new clspopupmaster();

//Print_r($_POST);
if ($_action == "ADD") {
    if (!empty($_POST["txtmessage"])) {
        $_Link = $_POST["txtLink"];
        $_Centercode = $_POST["txtCentercode"];
		$_Msg=$_POST["txtmessage"];
		$_Sub=$_POST["txtSubject"];
		$_Role=$_POST["ddlRole"];
      
       
        $response = $emp->Add($_Msg, $_Centercode, $_Link,$_Sub,$_Role);
        echo $response[0];
    }
}





if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    //echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Message</th>";
    echo "<th style='20%'>Usercode</th>";
    echo "<th style='20%'>File Name</th>";
    echo "<th style='10%'>Title</th>";
	echo "<th style='10%'>Role</th>";
	 echo "<th style='10%'>Timestamp</th>";
	  
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['content'] . "</td>";
        echo "<td>" . $_Row['usercode'] . "</td>";
        echo "<td>" . $_Row['Link'] . "</td>";
        echo "<td>" . $_Row['subject'] . "</td>";
		echo "<td>" . $_Row['UserRoll_Name'] . "</td>";
		 echo "<td>" . $_Row['timestamp'] . "</td>";
		 
        echo "<td>";
        if ($_Row['status'] == 1) {
            echo "<a href='frmpopupmaster.php?code=" . $_Row['id'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a>";
        } else {
            echo "Inactive";
        }
        echo "</td></tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

