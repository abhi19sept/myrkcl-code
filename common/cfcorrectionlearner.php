<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clscorrectionlearner.php';
$_ObjFTPConnection = new ftpConnection();

    $response = array();
    $emp = new clscorrectionlearner();
    if ($_action == "ADD") {
        if (isset($_POST["txtLCode"]) && !empty($_POST["txtLCode"]) && !empty($_POST["txtLCorrectName"]) && 
			!empty($_POST["txtFCorrectName"]) && !empty($_POST["ddlApplication"])
			 && !empty($_POST["txtEventName"]) && !empty($_POST["txtMarks"]) && !empty($_POST["txtEmail"]) && 
			 !empty($_POST["txtMobile"]) && !empty($_POST["txtDob"]) && !empty($_POST["txtaddress"]) 
				&& !empty($_POST["ddlDistrictName"])){
				$_LearnerCode = $_POST["txtLCode"];
				$parentid = $_POST["txtLCode"];
				$_LearnerName = $_POST["txtLCorrectName"];				  
				$_FatherName = $_POST["txtFCorrectName"];				
				$_ExamName = $_POST["txtEventName"];
				$_TotalMarks = $_POST["txtMarks"];
				$_Email = $_POST["txtEmail"];
				$_MobileNo = $_POST["txtMobile"];
				$_DOB = $_POST["txtDob"];				
				$_GENERATEDID = $_POST["txtGenerateId"];
				$_District = $_POST["ddlDistrictName"];
				$_Address = $_POST["txtaddress"];
				$_LearnerDistrict = $_POST["ddllearnerdistrict"];
				$_LPincode = $_POST["txtlearnerpincode"];
				$_checkedtype = $_POST["checkedtype"];
				$_CenterCode = $_POST["txtCenterCode"];
				
				date_default_timezone_set("Asia/Kolkata");
				$dtstamp = date("Ymd_His");	
				
		if($_POST["ddlApplication"] == 8){
			$_LApplication = "Correction Certificate";							
				if (!empty($_FILES['filePhoto']['name']) && !empty($_FILES['fileMarkSheet']['name']) && 
						!empty($_FILES['fileCertficate']['name']) && !empty($_FILES['fileApplication']['name'])){
									
									$_UploadDirectory1 = '/correction_photo';
									$_UploadDirectory2 = '/correction_marksheet';
									$_UploadDirectory3 = '/correction_certificate';
									$_UploadDirectory6 = '/correction_application';
							
							if ($_FILES["filePhoto"]["name"] != '') {
									$imag = $_FILES["filePhoto"]["name"];
									$imageinfo = pathinfo($_FILES["filePhoto"]["name"]);
									if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
										$error_msg = "Image must be in either PNG or JPG or JPEG Format";
										$flag = 0;
									} else {
										if (file_exists("$_UploadDirectory1/" . $parentid . '_photo' . '_' . $dtstamp . '.' . substr($imag, -3))) {
											$_FILES["filePhoto"]["name"] . "already exists";
										} else {
										 $response=	ftpUploadFile($_UploadDirectory1,$parentid . '_photo' . '_' . $dtstamp . '.jpg',$_FILES["filePhoto"]["tmp_name"]);
											$_SESSION['CorrectionPhoto'] = $parentid . '_photo' . '_' . $dtstamp . '.jpg';
												
										}
									}
							}
								
							if ($_FILES["fileMarkSheet"]["name"] != '') {
								//echo "mayank";
								$imag = $_FILES["fileMarkSheet"]["name"];
								$imageinfo = pathinfo($_FILES["fileMarkSheet"]["name"]);
								if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
									$error_msg = "Image must be in either PNG or JPG or JPEG Format";
									$flag = 0;
								} else {
									if (file_exists("$_UploadDirectory2/" . $parentid . '_marksheet' . '_' . $dtstamp . '.' . substr($imag, -3))) {
										$_FILES["fileMarkSheet"]["name"] . "already exists";
										
									} else {
											ftpUploadFile($_UploadDirectory2,$parentid . '_marksheet' . '_' . $dtstamp . '.jpg',$_FILES["fileMarkSheet"]["tmp_name"]);
											$_SESSION['CorrectionMarksheet'] = $parentid . '_marksheet' . '_' . $dtstamp . '.jpg';
											
									}
								}
							}

							if ($_FILES["fileCertficate"]["name"] != '') {
								$imag = $_FILES["fileCertficate"]["name"];
								$imageinfo = pathinfo($_FILES["fileCertficate"]["name"]);
								if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
									$error_msg = "Image must be in either PNG or JPG or JPEG Format";
									$flag = 0;
								} else {
									if (file_exists("$_UploadDirectory3/" . $parentid . '_certificate' . '_' . $dtstamp . '.' . substr($imag, -3))) {
										$_FILES["fileCertficate"]["name"] . "already exists";
									} else {
										ftpUploadFile($_UploadDirectory3,$parentid . '_certificate' . '_' . $dtstamp . '.jpg',$_FILES["fileCertficate"]["tmp_name"]);
										$_SESSION['CorrectionCertificate'] = $parentid . '_certificate' . '_' . $dtstamp . '.jpg';
											
									}
								}
							}
							
							if ($_FILES["fileApplication"]["name"] != '') {
								$imag = $_FILES["fileApplication"]["name"];
								$imageinfo = pathinfo($_FILES["fileApplication"]["name"]);
								if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
									$error_msg = "Image must be in either PNG or JPG or JPEG Format";
									$flag = 0;
								} else {
									if (file_exists("$_UploadDirectory6/" . $parentid . '_application' . '_' . $dtstamp . '.' . substr($imag, -3))) {
										$_FILES["fileApplication"]["name"] . "already exists";
									} else {
										ftpUploadFile($_UploadDirectory6,$parentid . '_application' . '_' . $dtstamp . '.jpg',$_FILES["fileApplication"]["tmp_name"]);
										$_SESSION['CorrectionApplication'] = $parentid . '_application' . '_' . $dtstamp . '.jpg';
											
									}
								}
							}
							$response = $emp->Add($_LearnerCode, $_LearnerName, $_FatherName, $_LApplication, $_ExamName, $_TotalMarks, 
													$_Email, $_MobileNo, $_DOB, $_GENERATEDID, $_District, $_Address, $_checkedtype,
													$_LearnerDistrict, $_LPincode, $dtstamp, $_CenterCode);
							echo $response[0];
						}
						else{
							echo "Please upload all documents.";
						}
							
					}
					else{
							if($_POST["ddlApplication"] == 9){
								$_LApplication = "Duplicate Certificate";
							}else{
								$_LApplication = "Duplicate Certificate with Correction";
							}
						
							if (!empty($_FILES['filePhoto']['name']) && !empty($_FILES['fileMarkSheet']['name']) && 
								!empty($_FILES['fileCertficate']['name']) &&
									!empty($_FILES['fileProvisional']['name']) && !empty($_FILES['fileApplication']['name'])){
										$_UploadDirectory1 = '/correction_photo';
										$_UploadDirectory2 = '/correction_marksheet';
										$_UploadDirectory3 = '/correction_certificate';
										$_UploadDirectory5 = '/correction_provisional';
										$_UploadDirectory6 = '/correction_application';
										$_UploadDirectory7 = '/correction_affidavit';
								$parentid = $_POST['txtLCode'];
								
								if ($_FILES["filePhoto"]["name"] != '') {
									$imag = $_FILES["filePhoto"]["name"];
									$imageinfo = pathinfo($_FILES["filePhoto"]["name"]);
									if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
										$error_msg = "Image must be in either PNG or JPG or JPEG Format";
										$flag = 0;
									} else {
										if (file_exists("$_UploadDirectory1/" . $parentid . '_photo_' . $dtstamp . '.' . substr($imag, -3))) {
											$_FILES["filePhoto"]["name"] . "already exists";
										} else {
											ftpUploadFile($_UploadDirectory1,$parentid . '_photo' . '_' . $dtstamp . '.jpg',$_FILES["filePhoto"]["tmp_name"]);
											$_SESSION['CorrectionPhoto'] = $parentid . '_photo_'.$dtstamp . '.jpg';
												
										}
									}
								}
								
								if ($_FILES["fileMarkSheet"]["name"] != '') {
								$imag = $_FILES["fileMarkSheet"]["name"];
								$imageinfo = pathinfo($_FILES["fileMarkSheet"]["name"]);
								if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
									$error_msg = "Image must be in either PNG or JPG or JPEG Format";
									$flag = 0;
								} else {
									if (file_exists("$_UploadDirectory2/" . $parentid . '_marksheet_'.$dtstamp. '.' . substr($imag, -3))) {
										$_FILES["fileMarkSheet"]["name"] . "already exists";
									} else {
										ftpUploadFile($_UploadDirectory2,$parentid . '_marksheet' . '_' . $dtstamp . '.jpg',$_FILES["fileMarkSheet"]["tmp_name"]);
										$_SESSION['CorrectionMarksheet'] = $parentid . '_marksheet_'.$dtstamp . '.jpg';
											
									}
								}
							}


							if ($_FILES["fileCertficate"]["name"] != '') {
								$imag = $_FILES["fileCertficate"]["name"];
								$imageinfo = pathinfo($_FILES["fileCertficate"]["name"]);
								if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
									$error_msg = "Image must be in either PNG or JPG or JPEG Format";
									$flag = 0;
								} else {
									if (file_exists("$_UploadDirectory3/" . $parentid . '_certificate_'.$dtstamp . '.' . substr($imag, -3))) {
										$_FILES["fileCertficate"]["name"] . "already exists";
									} else {
										ftpUploadFile($_UploadDirectory3,$parentid . '_certificate' . '_' . $dtstamp . '.jpg',$_FILES["fileCertficate"]["tmp_name"]);
										$_SESSION['CorrectionCertificate'] = $parentid . '_certificate_'.$dtstamp . '.jpg';
											
									}
								}
							}

							if ($_FILES["fileProvisional"]["name"] != '') {
								$imag = $_FILES["fileProvisional"]["name"];
								$imageinfo = pathinfo($_FILES["fileProvisional"]["name"]);
								if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
									$error_msg = "Image must be in either PNG or JPG or JPEG Format";
									$flag = 0;
								} else {
									if (file_exists("$_UploadDirectory5/" . $parentid . '_provisional_'.$dtstamp . '.' . substr($imag, -3))) {
										$_FILES["fileProvisional"]["name"] . "already exists";
									} else {
										ftpUploadFile($_UploadDirectory5,$parentid . '_provisional' . '_' . $dtstamp . '.jpg',$_FILES["fileProvisional"]["tmp_name"]);
										$_SESSION['CorrectionProvisional'] = $parentid . '_provisional_'.$dtstamp . '.jpg';
											
									}
								}
							}
							
							if ($_FILES["fileApplication"]["name"] != '') {
								$imag = $_FILES["fileApplication"]["name"];
								$imageinfo = pathinfo($_FILES["fileApplication"]["name"]);
								if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
									$error_msg = "Image must be in either PNG or JPG or JPEG Format";
									$flag = 0;
								} else {
									if (file_exists("$_UploadDirectory6/" . $parentid . '_application_'.$dtstamp . '.' . substr($imag, -3))) {
										$_FILES["fileApplication"]["name"] . "already exists";
									} else {
										ftpUploadFile($_UploadDirectory6,$parentid . '_application' . '_' . $dtstamp . '.jpg',$_FILES["fileApplication"]["tmp_name"]);
										$_SESSION['CorrectionApplication'] = $parentid . '_application_'.$dtstamp . '.jpg';
											
									}
								}
							}							
							
							if ($_FILES["fileAffidavit"]["name"] != '') {
								$imag = $_FILES["fileAffidavit"]["name"];
								$imageinfo = pathinfo($_FILES["fileAffidavit"]["name"]);
								if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
									$error_msg = "Image must be in either PNG or JPG or JPEG Format";
									$flag = 0;
								} else {
									if (file_exists("$_UploadDirectory7/" . $parentid . '_affidavit_'.$dtstamp . '.' . substr($imag, -3))) {
										$_FILES["fileAffidavit"]["name"] . "already exists";
									} else {
										ftpUploadFile($_UploadDirectory7,$parentid . '_affidavit' . '_' . $dtstamp . '.jpg',$_FILES["fileAffidavit"]["tmp_name"]);
										$_SESSION['CorrectionAffidavit'] = $parentid . '_affidavit_'.$dtstamp . '.jpg';
											
									}
								}
							}
							
							if($_POST["checkedtype"]==''){
								$status='0';
							}
							else{
								$status=$_POST["checkedtype"];
							}
							$response = $emp->Add($_LearnerCode, $_LearnerName, $_FatherName, $_LApplication, $_ExamName, $_TotalMarks, $_Email, $_MobileNo, $_DOB, $_GENERATEDID, $_District, $_Address, $status, $_LearnerDistrict,
								$_LPincode, $dtstamp, $_CenterCode);
							echo $response[0];
						}
							else{
								echo "Please upload all documents.";
							}
					}
				
			 }
	}
    
    if ($_action == "UPDATE") {
        if (isset($_POST["name"])) {
            $_StatusName = $_POST["name"];
            $_StatusDescription = $_POST["description"];
            $_Status_Code=$_POST['code'];
            $response = $emp->Update($_Status_Code,$_StatusName, $_StatusDescription);
            echo $response[0];
        }
    }
    
    
    if ($_action == "EDIT") {


        $response = $emp->GetDatabyCode($_actionvalue);

        $_DataTable = array();
        $_i=0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("StatusCode" => $_Row['Status_Code'],
                "StatusName"=>$_Row['Status_Name'],
                "StatusDescription"=>$_Row['Status_Description']);
            $_i=$_i+1;
        }
        echo json_encode($_Datatable);
    }
    
    
    if ($_action == "DELETE") {


        $response = $emp->DeleteRecord($_actionvalue);

        echo $response[0];
    }


    if ($_action == "SHOW") {
        $response = $emp->GetAll();
        $_DataTable = "";

         echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='5%'>S No.</th>";
        echo "<th style='30%'>Name</th>";
        echo "<th style='55%'>Description</th>";
        echo "<th style='10%'>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Status_Name'] . "</td>";
            echo "<td>" . $_Row['Status_Description'] . "</td>";
            echo "<td><a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a>  <a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
		echo "</div>";
//echo $_DataTable;
    }
	
	
    if ($_action == "FILL") {
        $response = $emp->GetAll();
        echo "<option value='' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['District_Name'] . ">" . $_Row['District_Name'] . "</option>";    
        }
    }
	
	if ($_action == "FillITGKDistrict") {
        $response = $emp->GetItgkDistrict($_POST['itgkcode']);
			$_DataTable = array();
			$_i=0;	
			$co = mysqli_num_rows($response[2]);
			if ($co){
				while ($_Row = mysqli_fetch_array($response[2])) {
					$_Datatable[$_i] = array("District_Name" => $_Row['District_Name'],
						"District_Code_VMOU"=>$_Row['District_Code_VMOU']);
					$_i=$_i+1;
				}
					echo json_encode($_Datatable);
			}
        
    }
    
     if ($_action == "FILLEMPDEPARTMENT") {
        $response = $emp->GetEmpdepartment();
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['gdname'] . ">" . $_Row['gdname'] . "</option>";            
        }
    }
    
    
     if ($_action == "FILLEMPDESIGNATION") {
        $response = $emp->GetEmpdesignation();
        echo "<option value='' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['designationname'] . ">" . $_Row['designationname'] . "</option>";            
        }
    }
    
	if ($_action == "GetLearnerDistrict") {
        $response = $emp->GetLearnerDistrict();
        echo "<option value='' selected='selected'>Please Select</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['District_Code'] . ">" . $_Row['District_Name'] . "</option>";            
        }
    }
    
     if ($_action == "FILLBANKDISTRICT") {
        $response = $emp->GetBankdistrict();
        echo "<option value='' selected='selected'>Please Select</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['District_Name'] . ">" . $_Row['District_Name'] . "</option>";            
        }
    }
    
    
     if ($_action == "FILLBANKNAME") {
        $response = $emp->GetBankname();
        echo "<option value='' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['bankname'] . ">" . $_Row['bankname'] . "</option>";            
        }
    }
    
    
     if ($_action == "FillDobProof") {
        $response = $emp->GetDobProof();
        echo "<option value='' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['cdocname'] . ">" . $_Row['cdocname'] . "</option>";            
        }
    }
	
	
if ($_action == "FillApplicationFor") {
        $response = $emp->GetFillApplicationFor();
        echo "<option value='' selected='selected'>Please Select</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['docid'] . ">" . $_Row['applicationfor'] . "</option>";
           }
    }
	
	if ($_action == "FillExamEvent") {
        $response = $emp->GetExamEvent();
        echo "<option value='' selected='selected'>Please Select</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name_VMOU'] . "</option>";    
        }
    }
	
	if ($_action == "FillEventById") {
        $response = $emp->FillEventById($_POST['values']);        
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo $_Row['Event_Name_VMOU'] ;    
        }
    }
	
	if ($_action == "FillEventByLcode") {
        $response = $emp->FillEventByLcode($_POST['values']); 
			$_DataTable = array();
			$_i=0;	
			$co = mysqli_num_rows($response[2]);
			if ($co){
				while ($_Row = mysqli_fetch_array($response[2])) {
					$_Datatable[$_i] = array("Event_Name" => $_Row['Event_Name_VMOU'],
						"tot_marks"=>$_Row['tot_marks']);
					$_i=$_i+1;
				}
					echo json_encode($_Datatable);
			}
				else {
					echo "";
				}
    }
	
	
	if ($_action == "DETAILS") {
		global $_ObjFTPConnection;
        $response = $emp->GetAllDETAILS($_POST['values'],$_POST['centercode']);
		//print_r($response[2]);
        $_DataTable = array();
        $_i=0;
		$co = mysqli_num_rows($response[2]);
		if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
			$details= $_ObjFTPConnection->ftpdetails();
			$_BatchName =  $_Row['Batch_Name']; 
			$_Course = $_Row['Admission_Course'];
			
			if ($_Course == 5) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);                    

                } elseif ($_Course == 4) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

                }
                elseif ($_Course == 3) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }               
                
                elseif ($_Course == 23) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

                }   

                 elseif ($_Course == 25) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

                }
                elseif ($_Course == 28) {

                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

                }
                elseif ($_Course == 29) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

                }
                elseif ($_Course == 30) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

                }
                elseif ($_Course == 31) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

                }
                elseif ($_Course == 32) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

                }
				
					$ftpaddress = $_ObjFTPConnection->ftpPathIp();
					$AdmissionPhotoFile = $ftpaddress.'admission_photo/' . $_LearnerBatchNameFTP . '/' . $_Row['Admission_Photo'];
					 
					$ch = curl_init($AdmissionPhotoFile);
					curl_setopt($ch, CURLOPT_NOBODY, true);
					curl_setopt($ch,CURLOPT_TIMEOUT,5000);
					curl_exec($ch);
					   $responseCodePhoto = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					curl_close($ch);
	
				if($responseCodePhoto == 200){
					$photopath = '/admission_photo/'.$_LearnerBatchNameFTP.'/';
				$photoid = $_Row['Admission_Photo'];

				$photoimage =  file_get_contents($details.$photopath.$photoid);
				$imageData = base64_encode($photoimage);
				// <a data-fancybox='gallery' href='data:image/png;base64, ".$imageData."'><img src="small_1.jpg"></a>
				$showimage =  "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='80' height='110'/>";
				}	
				else{
				$showimage=	'NA';
				}
				
            $_Datatable[$_i] = array("Admission_Name" => $_Row['Admission_Name'],
                "Admission_Fname"=>$_Row['Admission_Fname'],
                "Admission_Mobile"=>$_Row['Admission_Mobile'],
				"Admission_Email"=>$_Row['Admission_Email'],
				"Admission_Address"=>$_Row['Admission_Address'],
				"Admission_Batch"=>$_Row['Admission_Batch'],
				"photo"=>$_Row['Admission_Photo'],
				"learner_photo" => $showimage,
				"Admission_ITGK_Code"=>$_Row['Admission_ITGK_Code'],
				"Admission_GPFNO"=>$_Row['Admission_GPFNO']);
            $_i=$_i+1;
        }
         echo json_encode($_Datatable);
		}
		else {
			echo "";
		}
    }
	
	if ($_action == "SendSMS") {
        if (isset($_POST["mobileno"]) && !empty($_POST["mobileno"])) {
		$mobileno = trim($_POST["mobileno"]);
		$response = $emp->SentSms($mobileno);
                //echo "<pre>"; print_r($response);die;
            if ($response[0] != Message::NoRecordFound) {
                $_row = mysqli_fetch_array($response[2]);
                $_Mobile =  "XXXXXX". substr($mobileno, 6);
                $_OTP = $_row["Correction_Otp_OTP"];
                $_OTP_StartOn = $_row["OTP_StartOn"];
                $_OTP_EndOn = $_row["OTP_EndOn"];
                //$_SMS = "Dear Learner, Your verification OTP is " . $_OTP ." for correction/duplicate certificate application.Valid till ".$_OTP_EndOn."."; 
				$_SMS = "Dear Learner, Your verification OTP is " . $_OTP ." for correction/duplicate certificate 
                                                            application. Valid till ".$_OTP_EndOn.". Rajasthan Knowledge Corporation Limited";
                SendSMS($mobileno, $_SMS);
                $result = [
                    //'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
                    'frm' => $emp->getVeriFyOTPForm($_row["Correction_Otp_Code"], $_Mobile),
                ];

                echo json_encode($result);
            } else {
                echo '0';
            }
        }
    }
	
	 if ($_action == "RESENDOTP") {
        if (isset($_POST["mobileno"]) && !empty($_POST["mobileno"])) {
		$mobileno = $_POST["mobileno"];
		$response = $emp->ResendOTP($mobileno);
                //echo "<pre>"; print_r($response);die;
            if ($response[0] != Message::NoRecordFound) {
                $_row = mysqli_fetch_array($response[2]);
                $_Mobile =  "XXXXXX". substr($mobileno, 6);
                $_OTP = $_row["Correction_Otp_OTP"];
                $_OTP_StartOn = $_row["OTP_StartOn"];
                $_OTP_EndOn = $_row["OTP_EndOn"];
                //$_SMS = "Dear Learner, Your verification OTP is " . $_OTP ." for correction/duplicate certificate application. Valid till ".$_OTP_EndOn.".";
				$_SMS = "Dear Learner, Your verification OTP is " . $_OTP ." for correction/duplicate certificate application.
                                                            Valid till ".$_OTP_EndOn.".Rajasthan Knowledge Corporation Limited";
                SendSMS($mobileno, $_SMS);
                $result = [
                    //'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
                    'frm' => $emp->getVeriFyOTPForm($_row["Correction_Otp_Code"], $_Mobile),
                ];

                echo json_encode($result);
            } else {
                echo '0';
            }
        }
    }
	
	if ($_action == "VerifyOTP" && isset($_POST["oid"]) && !empty($_POST["oid"])) {
        
        if($_POST["otp"]=='')
        {
          echo 'OTPBLANCK';  
        }
        else{
            $response = $emp->VerifyOTPNEW($_POST["oid"], $_POST["otp"]);			
            if($response[0]=='EXPIRE')
            {
               echo 'EXPIRE';

            }
            else if($response[0]=='NORECORD')
            {
               echo 'NORECORD';
            }
            else{
					if($response[0]=='Success'){
						if (mysqli_num_rows($response[2])) 
                        {                        
							 $result = [
								'msg' => 'OTP Successfully Verified',
								'oid' => $_POST["oid"],
							];
							echo json_encode($result);
                        } 
					}                
					else {
						echo '0';
					}
			}
        }
    }
?>
