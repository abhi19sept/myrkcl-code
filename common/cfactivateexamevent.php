<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsactivateexamevent.php';

$response = array();
$emp = new clsactivateexamevent();

if ($_action == "FILL") {
    $response = $emp->FillEvent();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "SHOW") {
    $response = $emp->getdata();
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Exam Event Name</th>";
  
	echo "<th>Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Event_Name'] . "</td>";
        echo "<td>" . $_Row['Status_Name'] . "</td>";
        
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}



if ($_action == "ADD") 
{
	$_Event = $_POST["ddlExamEvent"];
	$_Status = $_POST["ddlStatus"];
    $response = $emp->Add($_Event, $_Status);
     echo $response[0];   
}
?>