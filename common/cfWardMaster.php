<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsWardMaster.php';

$response = array();
$emp = new clsWardMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        $_WardName = $_POST["name"];
       // $_Parent=$_POST["parent"];
        $Ward_Municipality_Code=$_POST["Municipality_Code"];
        $_Status=$_POST["status"];

        $response = $emp->Add($_WardName,$Ward_Municipality_Code,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_WardName = $_POST["name"];
        //$_Parent=$_POST["parent"];
        $Ward_Municipality_Code=$_POST["Municipality_Code"];
        $_Status=$_POST["status"];
        $_Code=$_POST["code"];
        $response = $emp->Update($_Code,$_WardName,$Ward_Municipality_Code,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("WardCode" => $_Row['Ward_Code'],
            "WardName" => $_Row['Ward_Name'],
            "Ward_Municipality_Code" => $_Row['Ward_Municipality_Code'],
            "Status"=>$_Row['Ward_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll($_actionvalue);
	
	showWardList($response);

}
if ($_action == "FILL") {
    $response = $emp->GetAll($_actionvalue);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Ward_Code'] . ">" . $_Row['Ward_Name'] . "</option>";
    }
}

if ($_action == "Search") {
    $filters = [];
    if (!empty($_POST['name'])) {
        $filters['Ward_Name'] = " LIKE ('%" . $_POST['name'] . "%')";
    }
    if (!empty($_POST['districtId'])) {
        $filters['Municipality_District'] = ' = ' . $_POST['districtId'];
    }
    if (!empty($_POST['parent'])) {
        $filters['Ward_Municipality_Code'] = ' = ' . $_POST['parent'];
    }
    if (!empty($_POST['status'])) {
        $filters['Ward_Status'] = ' = ' . $_POST['status'];
    }
    if (!empty($filters)) {
        $response = $emp->GetAll($filters);
        showWardList($response);
    }
}

function showWardList($response) {
	$_DataTable = "";

    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>Name</th>";
    echo "<th style='15%'>State</th>";
    echo "<th style='15%'>District</th>";
    echo "<th style='15%'>Region</th>";
    echo "<th style='10%'>Municipality</th>";
    echo "<th style='10%'>Status</th>";
    echo "<th style='15%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Ward_Name'] . "</td>";
        echo "<td>" . $_Row['State_Name'] . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
        echo "<td>" . $_Row['Region_Name'] . "</td>";
        echo "<td>" . $_Row['Municipality_Name'] . "</td>";
        echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmWardMaster.php?code=" . $_Row['Ward_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmWardMaster.php?code=" . $_Row['Ward_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}