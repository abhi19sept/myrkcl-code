<?php

/*
 * author Yogendra

 */
include './commonFunction.php';
require 'BAL/clsquestionanswers.php';

$response = array();
$emp = new clsquestionanswers();

if ($_action == "INSERT") {
    //print_r($_POST);  


    if (!empty($_POST["ddlSurvey"])) {

        $_surveyid = $_POST["ddlSurvey"];
        $txtQuestion = $_POST["txtQuestion"];
        $_txtoption1 = $_POST["txtoption1"];
        $_txtoption2 = $_POST["txtoption2"];
        $_txtoption3 = $_POST["txtoption3"];

        //echo $_StatusName;
        $response = $emp->INSERT($_surveyid, $txtQuestion, $_txtoption1, $_txtoption2, $_txtoption3);
        echo $response[0];
    }
}







if ($_action == "SHOWSURVEY") {


    $response = $emp->Showsurvey();

    $_DataTable = "";
    echo "<div id='responsive'>";
    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%' style='margin-top:30px'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='15%'>Survey Name</th>";

    echo "<th style='30%'>Servey Question</th>";
    echo "<th style='30%'>Servey option1</th>";
    echo "<th style='30%'>Servey option2</th>";
    echo "<th style='30%'>Servey option3</th>";


    echo "<th style='20%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Survey_Name'] . "</td>";
        echo "<td>" . $_Row['Servey_Question'] . "</td>";
        echo "<td>" . $_Row['Servey_option1'] . "</td>";
        echo "<td>" . $_Row['Servey_option2'] . "</td>";
        echo "<td>" . $_Row['Servey_option3'] . "</td>";


        echo "<td><a href='frmquestionanswers.php?code=" . $_Row['survey_id'] . "&Mode=Edit'>"
        . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmquestionanswers.php?code=" . $_Row['survey_id'] . "&Mode=Delete'>"
        . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div >";
}








if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("Servey_Code" => $_Row['Servey_Code'],
            "Servey_Question" => $_Row['Servey_Question'],
            "Servey_option1" => $_Row['Servey_option1'],
            "Servey_option2" => $_Row['Servey_option2'],
            "Servey_option3" => $_Row['Servey_option3']
        );
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}



if ($_action == "UPDATE") {
    if (isset($_POST["ddlSurvey"])) {
        $_code = $_POST["code"];
        $_surveyid = $_POST["ddlSurvey"];
        $txtQuestion = $_POST["txtQuestion"];
        $_txtoption1 = $_POST["txtoption1"];
        $_txtoption2 = $_POST["txtoption2"];
        $_txtoption3 = $_POST["txtoption3"];
        $response = $emp->Update($_code, $_surveyid, $txtQuestion, $_txtoption1, $_txtoption2, $_txtoption3);
        echo $response[0];
    }
}



if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}

if ($_action == "FILLSurvey") {
    $response = $emp->GetSurvey($_actionvalue);
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Survey_id'] . ">" . $_Row['Survey_Name'] . "</option>";
    }
}
?>
