<?php

/* 
 * Created by yogi
 * Update By Sunil Kuamr Baindara Dated: 9-2-1017

 */

include './commonFunction.php';
require 'BAL/clslearnerdetails.php';

$response = array();
$response1 = array();
$emp = new clslearnerdetails();

//print_r($_POST['ddlCenter']);



if ($_action == "DETAILS") {

	//print_r($_POST);die;
        $response = $emp->GetAllLearner($_POST["LearnerCode"]);
        $_DataTable = "";
	//$response1=$response[2];
	$_Row = mysqli_fetch_array($response[2]);
        //print_r($_SESSION);die;
	$num_rows = mysqli_num_rows($response[2]);
	$Tdate = date("d-m-Y", strtotime($_Row['exam_date']));
        $num_rows;
	if($num_rows==0)
	{
           $_LoginRole = $_SESSION['User_UserRoll'];
           if ($_LoginRole == '21' || $_LoginRole == '18' || $_LoginRole == '19' || $_LoginRole == '1' || $_LoginRole == '2' || $_LoginRole == '3' || $_LoginRole == '4' || $_LoginRole == '8' || $_LoginRole == '9' || $_LoginRole == '10' || $_LoginRole == '11')
           {   
            
            ?><div class="error"><?php
		echo "No Detail Found.";
		?></div>
	    <?php
           }
           else if ($_LoginRole == '14')
           {   
            
            ?><div class="error"><?php
		echo "This Learner Has Not Been Registered/Enrolled In Your SP.";
		?></div>
	    <?php
           }
           else
          {
               
              ?><div class="error"><?php
		echo "This Learner Has Not Been Registered/Enrolled In Your Center/ITGK/SP.";
	     ?></div>
	    <?php 
           }
	}
	else
	{
	$responsesC = $emp->CenterDetails($_Row["Admission_ITGK_Code"]);
        $_RowC = mysqli_fetch_array($responsesC[2]);
        //print_r($_RowC);//die;
        
        
        
        echo "<div class='container marginT30'>
            <div class='row'>
                <div class='col-lg-3 col-md-3'>
                    <div class='profilebox'>
                        <h4 class='proheading'><b>Profile Picture</b></h4>
                        <div class='profilepic'>";
                       if($_Row['Admission_Photo'])
		{
		echo '<img class="img-squre img-responsive" width="200"  style="padding:2px;border:1px solid #428bca;height:180px;margin-top:10px;" src="upload/admission_photo/'.$_Row['Admission_Photo'].'"/>';
		}
		else
		{
			echo '<img class="img-circle img-responsive"  width="150" style="padding:2px;border:1px solid #428bca;height:130px[margin-top:10px;" src="upload/admission_photo/blank.png"/>';
			
		}
                        echo "</div>";
               echo '<p   style="margin-top:10px;">Sign </p ><p>';
		
               if($_Row['Admission_Sign']!='')
		{
		echo '<img class="img-squre img-responsive"  width="80"  style="margin-top:10px; margin:0 auto" src="upload/admission_sign/'.$_Row['Admission_Sign'].'"/>';
		}
		else
		{
			echo '<img class="img-squre img-responsive"  width="80"  style="margin-top:10px;" src="upload/admission_sign/blank.png"/>';
			
		}
                 echo "  </p> </div>
                     
                </div><div class='personalblog'>";
                
                 echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Personal Details</span>
                    </div>";
                 $Admission_DOB = date("d M Y", strtotime($_Row['Admission_DOB']));
                    echo "<div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Name:</span>
                             <p>". strtoupper($_Row['Admission_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Father/Husband Name :</span>
                             <p>". strtoupper($_Row['Admission_Fname']) . "</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Date of Birth :</span>
                             <p>". strtoupper($Admission_DOB) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Mobile :</span>
                             <p>". strtoupper($_Row['Admission_Mobile']) ."</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Address :</span>
                             <p >". strtoupper($_Row['Admission_Address']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Email :</span>
                             <p>" . strtoupper($_Row['Admission_Email']) . "</p>
                        </div>
                        
                    </div>
                    <div class='persdetablog'>
                         <div class='persdetacont'>
                             <span>Learner Code :</span>
                             <p>". strtoupper($_Row['Admission_LearnerCode']) ."</p>
                        </div>
                       
                        <div class='persdetacont'>
                             <span>Medium :</span>
                             <p>". strtoupper($_Row['Admission_Medium']) ."</p>
                        </div>
                    
                        
                    </div>
               </div>";
                    
                   /* New Tab*/ 
                    
                    echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>ITGK (Center) Details</span>
                    </div>";
                    echo "<div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Center Name:</span>
                             <p>". strtoupper($_RowC['Organization_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Center Code :</span>
                             <p>". strtoupper($_RowC['User_LoginId']) . "</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Center Registration No :</span>
                             <p>". strtoupper($_RowC['Organization_RegistrationNo']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Mobile :</span>
                             <p>". strtoupper($_RowC['User_MobileNo']) ."</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Center Address :</span>
                             <p  >". strtoupper($_RowC['Organization_Address']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Email :</span>
                             <p>" . strtoupper($_RowC['User_EmailId']) . "</p>
                        </div>
                        
                    </div>
                   
               </div>";
                    
                $responsesSP = $emp->SPDetails($_POST["LearnerCode"]);
                $_RowSP = mysqli_fetch_array($responsesSP[2]); 
                    
                  $Addresp=$_RowSP['Organization_HouseNo']." ".$_RowSP['Organization_Street']." ".$_RowSP['Organization_Road']." ".$_RowSP['Organization_Landmark']." ".$_RowSP['Org_formatted_address'] ; 
                    echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Service Provider Details</span>
                    </div>";
                    $Organization_FoundedDate = date("d M Y", strtotime($_RowSP['Organization_FoundedDate']));
                    echo "<div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>SP Name:</span>
                             <p>". strtoupper($_RowSP['Organization_Name']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>SP Registration No :</span>
                             <p>". strtoupper($_RowSP['Organization_RegistrationNo']) . "</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>SP Mobile:</span>
                             <p>". strtoupper($_RowSP['User_MobileNo']) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>SP Email :</span>
                             <p  >".strtoupper($_RowSP['User_EmailId']). "</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>SP Founded Date:</span>
                             <p>". strtoupper($Organization_FoundedDate) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>SP Address :</span>
                             <p  >".strtoupper($Addresp). "</p>
                        </div>
                    </div>
                    
                    
                   
               </div>";
                    
                  /* New Tab*/ 
                    
            if($_Row['Admission_Payment_Status']==1)
		{
			$temp='Confirm';
		}else{ $temp='Not Confirm';}

                echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Payment Details </span>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Learner Course Fee Payment Status :</span>
                             <p>". strtoupper($temp) ."</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Registered Course By Learner :</span>
                             <p>" . strtoupper($_Row['Course_Name']) . "</p>
                        </div>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Learner Data Approval Status :</span>
                             <p>" . strtoupper($_Row['Admission_PhotoProcessing_Status']) . "</p>
                        </div>
                        <div class='persdetacont'>
                             <span>Learner Enrolled In Batch :</span>
                             <p>" . strtoupper($_Row['Batch_Name']) . "</p>
                        </div>
                    </div>
                    
                </div>";
                

                
                          
                
        echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Internal Assessment Score Detail</span>
                    </div>";
                
                
                
                $responseSco = $emp->GetAllLearnerScore($_POST["LearnerCode"]);
       
                $_DataTable = "";
                //$response1=$response[2];
                $_RowSco = mysqli_fetch_array($responseSco[2]);
        //print_r($_Row);//die;
                $num_rowsSco = mysqli_num_rows($responseSco[2]);
                if($num_rowsSco==0)
                {?><div class="error"><?php
                        echo "<b>No Detail Found.</b>";
                        ?></div>
                        <?php 
                }
                else
                {
                
                echo "<div class='persdetablog'>
                        <div class='exampannelNew1'>
                            <div class='examname'>Learning Start Date</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Final Score</div>
                        </div>
                        <div class='exampannelNew1'>
                            <div class='examname'>Score %</div>
                        </div>
                     </div>";
               $Batch_StartDate = date("d M Y", strtotime($_Row['Batch_StartDate']));
                 echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew1'>
                            
                            <div class='marks'>". strtoupper($Batch_StartDate) . " </div>
                        </div>
                        <div class='exampannelNew1'>
                            
                            <div class='marks'>". strtoupper($_RowSco['Score']) . "</div>
                        </div>
                        <div class='exampannelNew1'>
                            
                            <div class='marks'>". strtoupper($_RowSco['Score_Per']) . "</div>
                        </div>

                    </div>";
                }
                
        echo"</div>";           
                
        
        
        
        $responseLAM = $emp->GetLearnerAllMarks($_POST["LearnerCode"]);
         echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Final Exam Marks Detail</span>
                    </div>";
                    
        //$_Row1 = mysqli_fetch_array($responseLAM[2]);
        //print_r($_Row);//die;
        $num_rowsLAM = mysqli_num_rows($responseLAM[2]);
                if($num_rowsLAM==0)
                {?><div class="error">
                    <?php echo "<b>No Detail Found.</b>";?>
                   </div>
                    <?php 
                }
                else
                {        
                
                
                
                
                   
                    echo "<div class='persdetablog'>
                        <div class='exampannelNew'>
                            <div class='examname'>Final Score</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Event Name</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Exam Date</div>
                        </div>
                        <div class='exampannelNew'>
                            <div class='examname'>Result</div>
                        </div>
                    </div>";

	while($_Row1 = mysqli_fetch_array($responseLAM[2],true))
		{

                   echo " <div class=' persdetablog1'>
                       
                        <div class='exampannelNew'>
                            
                            <div class='marks'>". strtoupper($_Row1['tot_marks']) . " Marks</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>". strtoupper($_Row1['Event_Name']) . "</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>". strtoupper($Tdate) . "</div>
                        </div>
                        <div class='exampannelNew'>
                            
                            <div class='marks'>". strtoupper($_Row1['result']) . "</div>
                         </div>
                    </div>";
                   
                }
         
          
                }
                
        echo "</div>";
        
        
            echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>RS-CIT Fee Reimbursement Status </span>
                    </div>";
                  $responseNew = $emp->checkGovtLearnerStatus($_POST["LearnerCode"],$_Row["Admission_ITGK_Code"]);
                  if($responseNew[0]=='Success')
                        {
                    $responseDET = $emp->GetAllRDETAILS($_POST["LearnerCode"],$_Row["Admission_ITGK_Code"]);
                    $_RowDET = mysqli_fetch_array($responseDET[2],true);
                    //echo "<pre>"; print_r($_RowDET);
                    echo"<div class='persdetablog'>
                            <div class='persdetacont'>
                             <span>Employee ID :</span>
                             <p>". strtoupper($_RowDET['empid']) ."</p>
                             </div>
                            <div class='persdetacont'>
                             <span>Designation :</span>
                             <p>". strtoupper($_RowDET['designation']) ."</p>
                             </div>
                         </div>";
                    
                   echo "<div class='persdetablog'>
                            <div class='persdetacont'>
                             <span>Office Address :</span>
                             <p>". strtoupper($_RowDET['officeaddress']) ."</p>
                            </div>
                            <div class='persdetacont'>
                             <span>GPF No.:</span>
                             <p>". strtoupper($_RowDET['empgpfno']) ."</p>
                            </div>
                        </div>";
                    echo "<div class='persdetablog'>
                            <div class='persdetacont'>
                             <span>Account No. :</span>
                             <p>". strtoupper($_RowDET['empaccountno']) ."</p>
                            </div>
                            <div class='persdetacont'>
                             <span>IFSC No. :</span>
                             <p>". strtoupper($_RowDET['gifsccode']) ."</p>
                            </div>
                        </div>";
                    
                   if($_RowDET['aattempt']==1){$aattempt='First';}else{$aattempt='Not in 1st Attempt';}
                   
                    echo"<div class='persdetablog'>
                            <div class='persdetacont'>
                                 <span>Exam Marks :</span>
                                 <p>". strtoupper($_RowDET['exammarks']) ."</p>
                                 </div>
                            <div class='persdetacont'>
                                 <span>Number Of Exam Attempt :</span>
                                 <p>". strtoupper($aattempt) ."</p>
                            </div>
                        </div>";
                    
                    $DET_datetime = date("d M Y", strtotime($_RowDET['datetime']));
                     echo"<div class='persdetablog'>
                            <div class='persdetacont'>
                                 <span>Reimbursement Status :</span>
                                 <p>". strtoupper($_RowDET['cstatus']) ."</p>
                            </div>
                             <div class='persdetacont'>
                               <span>Date-Time :</span>
                               <p>". strtoupper($DET_datetime) ."</p>
                            </div>
                           
                          </div>";
                      echo"<div class='persdetablog'>
                            <div class='persdetacont'>
                             <span>Reimbursement Fee Amt. :</span>
                             <p>". strtoupper($_RowDET['fee']) ."</p>
                             </div>
                            <div class='persdetacont'>
                             <span>Incentive :</span>
                             <p>". strtoupper($_RowDET['incentive']) ."</p>
                            </div>
                          </div>";
                       $total = intval (trim($_RowDET['fee']))+ intval (trim($_RowDET['incentive']));
                      echo"<div class='persdetablog'>
                            
                            <div class='persdetacont'>
                             <span>Total Fee Amt.  :</span>
                              <p>". strtoupper($total) ."</p>
                            </div>
                             <div class='persdetacont'>
                             <span>Lot Name :</span>
                             <p>". strtoupper($_RowDET['lotname']) ."</p>
                            </div>
                          </div>";
                     
                    
                   
                        }
                  elseif($responseNew=='NRED')
                      {?><div class="error"><?php
                        echo "<b>You Have Not Appiled For Fee Reimbursement .</b>"; // no emplyee course found
                        ?></div>
                        <?php 
                      }
                   else
                      {?><div class="error"><?php
                        echo "<b>Information Not Available.</b>"; // no emplyee course found
                        ?></div>
                        <?php 
                      }
                         
                         
                         

                   echo"</div>";
                   
                   
                   
        
              echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Correction / Duplicate Certificate </span>
                    </div>";
              
                  $responseCS = $emp->checkcorrectionStatus($_POST["LearnerCode"],$_Row["Admission_ITGK_Code"]);
                  if($responseCS[0]=='Success')
                        {
                    $responseDET = $emp->GetCorrectionDuplicateCertificate($_POST["LearnerCode"],$_Row["Admission_ITGK_Code"]);
                    $_RowDET = mysqli_fetch_array($responseDET[2],true);
                    //echo "<pre>"; print_r($_RowDET);
                    if($_RowDET['Correction_Payment_Status']==1){$CPS="Payment DONE";}else{$CPS="Payment Not DONE";}
                    echo"<div class='persdetablog'>
                            <div class='persdetacont'>
                             <span>Application Applied For :</span>
                             <p>". strtoupper($_RowDET['applicationfor']) ."</p>
                             </div>
                            <div class='persdetacont'>
                             <span>Payment Status For Correction Application :</span>
                             <p>". strtoupper($CPS) ."</p>
                             </div>
                        </div>";
             
                    echo"<div class='persdetablog'>
                            
                            <div class='persdetacont'>
                                 <span>Correction Tran RefNo :</span>
                                 <p>". strtoupper($_RowDET['Correction_TranRefNo']) ."</p>
                            </div>
                             <div class='persdetacont'>
                             <span>Status :</span>
                             <p>". strtoupper($_RowDET['cstatus']) ."</p>
                             </div>
                        </div>";
                        
                   /* echo" <div class='persdetablog'>
                            <div class='persdetacont'>
                                 <span>Exam Date :</span>
                                 <p>". strtoupper($_RowDET['exameventname']) ."</p>
                                </div>
<div class='persdetacont'>
                                <span>Total Marks:</span>
                                <p>". strtoupper($_RowDET['totalmarks']) ."</p>
                            </div> 
                           
                          </div>";*/
                    
                    
                   
                        }
                    
                    else{?><div class="error"><?php
                        echo "<b>You Have Not Applied Yet For Correction/Duplicate Certificate.</b>"; // no emplyee course found
                        ?></div>
                        <?php 
                      }     
                     
                   echo" </div>";
                   
                   
                   
              //$tdate=date("d-M-Y");
              //$originalDate = "2010-03-21";
              /*$Batch_StartDate = date("d-m-Y", strtotime($_Row['Batch_StartDate']));
               echo "<div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Data Transfer To LMS</span>
                    </div>
                    
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Expected Learning Start Date :</span>
                             <p>". strtoupper($Batch_StartDate) ."</p>
                        </div>
                    </div>
                     
                </div>";*/
            
              /*echo "   <div class='col-lg-12 col-md-12 bdrblue'>
                    <div class='persdeta'>
                        <span>Other Details :</span>
                    </div>
                    <div class='persdetablog'>
                        <div class='persdetacont'>
                             <span>Medium :</span>
                             <p>". strtoupper($_Row['Admission_Medium']) ."</p>
                        </div>
                    </div>
                     
                </div>";*/
        
            
               echo" </div>
                
                
            </div>
        </div>";
        
        
		 
	}	 
	//echo $response[0];
	
}


if ($_action == "DETAILSFOREMP"){
    //print_r($_POST);die;
    
     //$responseEMPAK = $emp->checkEMPACKDETAILS($_POST["User_LearnerCode"]);
           // if($responseEMPAK[0]=='Success')
               // {
                    //    echo "EMPAK"; // detail allready in the database
               // }
           // else
                //{
    
                        $response = $emp->GetAll($_POST["LearnerCode"]);
                        $_DataTable = "";
                        //$response1=$response[2];
                        $_Row = mysqli_fetch_array($response[2]);
                        //print_r($_Row);//die;
                        $num_rows = mysqli_num_rows($response[2]);
                        $Tdate = date("d-m-Y", strtotime($_Row['exam_date']));
                        $num_rows;
                        if($num_rows==0)
                        {?><div class="error"><?php
                                echo "No record Found";
                                ?></div>
                                <?php 
                        }
                        else
                        {
                $responsesC = $emp->CenterDetails($_Row["Admission_ITGK_Code"]);
                $_RowC = mysqli_fetch_array($responsesC[2]);
                $Admission_DOB = date("d-M-Y", strtotime($_Row['Admission_DOB']));
                 echo "<div class='container'>
                    <div class='row'>
                        <div  style='float: left;
            width: 94%;'>";

                         echo "<div class='col-lg-12 col-md-12 bdrblue'>";
                         echo "<div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Learner Code:</span>
                                     <p>". strtoupper($_POST["LearnerCode"]) . "</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>ITGK(Center) Code :</span>
                                     <p>". strtoupper($_Row['Admission_ITGK_Code']) . "</p>
                                </div>
                                </div>";
                            echo "<div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Registered Name:</span>
                                     <p>". strtoupper($_Row['Admission_Name']) . "</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Father/Husband Name :</span>
                                     <p>". strtoupper($_Row['Admission_Fname']) . "</p>
                                </div>
                            </div>
                            <div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Date of Birth :</span>
                                     <p>". strtoupper($Admission_DOB) ."</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Mobile No :</span>
                                     <p>". strtoupper($_Row['Admission_Mobile']) ."</p>
                                </div>
                            </div>
                            <div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Address :</span>
                                     <p >". strtoupper($_Row['Admission_Address']) ."</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Email :</span>
                                     <p>" . strtoupper($_Row['Admission_Email']) . "</p>
                                </div>

                            </div>
                            <div class='persdetablog'>
                                 <div class='persdetacont'>
                                     <span>Enrolled Batch :</span>
                                     <p>" . strtoupper($_Row['Batch_Name']) . "</p>
                                </div>
                                 <div class='persdetacont'>
                                     <span>Registered Course :</span>
                                     <p>" . strtoupper($_Row['Course_Name']) . "</p>
                                </div>

                            </div>
                       </div>";

                           /* New Tab*/ 









                    echo"</div>
                </div>";

                }
                
               // }
    }
    
  /// Added : 9-4-2017  
if ($_action == "DETAILSFOREMPOFRALL"){
    //print_r($_POST);die;
    
     $responseEMPAK = $emp->checkEMPACKDETAILS($_POST["lcode"]);
           // if($responseEMPAK[0]=='Success')
               // {
                    //    echo "EMPAK"; // detail allready in the database
               // }
           // else
                //{
    
                        $response = $emp->GetAll($_POST["lcode"]);
                        $_DataTable = "";
                        //$response1=$response[2];
                        $_Row = mysqli_fetch_array($response[2]);
                        //print_r($_Row);//die;
                        $num_rows = mysqli_num_rows($response[2]);
                        $Tdate = date("d-m-Y", strtotime($_Row['exam_date']));
                        $num_rows;
                        if($num_rows==0)
                        {?><div class="error"><?php
                                echo "No record Found";
                                ?></div>
                                <?php 
                        }
                        else
                        {
                $responsesC = $emp->CenterDetails($_Row["Admission_ITGK_Code"]);
                $_RowC = mysqli_fetch_array($responsesC[2]);
                $Admission_DOB = date("d-M-Y", strtotime($_Row['Admission_DOB']));
                 echo "<div class='container'>
                    <div class='row'>
                        <div  style='float: left;
            width: 94%;'>";

                         echo "<div class='col-lg-12 col-md-12 bdrblue'>
                           ";
                            echo "<div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Registred Name :</span>
                                     <p>". strtoupper($_Row['Admission_Name']) . "</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Father/Husband Name :</span>
                                     <p>". strtoupper($_Row['Admission_Fname']) . "</p>
                                </div>
                            </div>
                            <div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Date of Birth :</span>
                                     <p>". strtoupper($Admission_DOB) ."</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Mobile No :</span>
                                     <p>". strtoupper($_Row['Admission_Mobile']) ."</p>
                                </div>
                            </div>
                            <div class='persdetablog'>
                                <div class='persdetacont'>
                                     <span>Address :</span>
                                     <p >". strtoupper($_Row['Admission_Address']) ."</p>
                                </div>
                                <div class='persdetacont'>
                                     <span>Email :</span>
                                     <p>" . strtoupper($_Row['Admission_Email']) . "</p>
                                </div>

                            </div>
                            <div class='persdetablog'>
                                 <div class='persdetacont'>
                                     <span>Enrolled Batch :</span>
                                     <p>" . strtoupper($_Row['Batch_Name']) . "</p>
                                </div>
                                 <div class='persdetacont'>
                                     <span>Registered Course :</span>
                                     <p>" . strtoupper($_Row['Course_Name']) . "</p>
                                </div>

                            </div>
                       </div>";

                           /* New Tab*/ 









                    echo"</div>
                </div>";

                }
                
               // }
    }

