<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCorrectionReport.php';

$response = array();
$emp = new clsCorrectionReport();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll($_POST['status']);

    $_DataTable = "";
	 echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='20%'>Center Code</th>";
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";  
    echo "<th style='20%'>Father/Husband Name</th>"; 
	if($_POST['status']=='4') {
		   echo "<th style='20%'>Reason</th>"; 
	   }
    echo "<th style='20%'>Correction Id</th>";	
    echo "<th style='20%'>Marks</th>";
    echo "<th style='5%'>Lot</th>";
    echo "<th style='5%'>Mobile</th>";
    //echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Correction_ITGK_Code'] . "</td>";
        echo "<td>" . $_Row['lcode'] . "</td>";
			$cfname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['cfname']))));
        echo "<td>" . $cfname . "</td>";  
			$cfaname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['cfaname']))));
		echo "<td>" . $cfaname . "</td>";
		if($_POST['status']=='4') {
				echo "<td>" . $_Row['remarks'] . "</td>"; 
			}
		echo "<td>" . $_Row['cid'] . "</td>";
        echo "<td>" . $_Row['totalmarks'] . "</td>";
        echo "<td>" . $_Row['lotname'] . "</td>";
        echo "<td>" . $_Row['mobile'] . "</td>";
        		
		echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	 echo "</div>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Status</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Function_Code'] . ">" . $_Row['Function_Name'] . "</option>";
    }
}

if ($_action == "FILLCorrectionLot") {
    $response = $emp->FILLCorrectionLot();
	   print_r($response);
    echo "<option value='' selected='selected'>Select Lot</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['lotid'] . ">" . $_Row['lotname'] . "</option>";
    }
}

if ($_action == "GetCorrectionApprovalStatus") {
    $response = $emp->GetCorrectionApprovalStatus();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}
?>
