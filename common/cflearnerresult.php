<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clslearnerresult.php';
require 'DAL/upload_ftp_doc.php';
$response = array();
$emp = new clslearnerresult();


if ($_action == "schedule") {
    $response = $emp->getschedule($_POST['examevent']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Learner Code</th>";
	echo "<th>Learner Mobile</th>";
	
    echo "<th >Learner Name</th>";
   echo "<th>Father Name</th>";
    
   
    
    echo "<th>Result</th>";
    echo "<th>Percentage</th>";
    echo "<th>Action </th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['scol_no'] . "</td>";
		 echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
       
        echo "<td>" . $_Row['name'] . "</td>";
        echo "<td>" . $_Row['fname'] . "</td>";
       
      
        echo "<td>" . $_Row['result'] . "</td>";
        echo "<td>" . $_Row['percentage'] . "% </td>";
        if (strtoupper($_Row['result']) == 'PASS' ) {
        // if (strtoupper($_Row['result']) == 'PASS' && $_POST['examevent'] >= '1227') {
            echo "<td class='org'><button type='button' data-toggle='modal' data-target='#approvalLetter' class='btn btn-primary approvalLetter' lid='" . $_Row['scol_no'] . "'eid='" . $_POST['examevent'] . "'>View Letter </button></td>";
        } else {
            echo "<td>Not Available</td>";
        }
        
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "DwnldPrintRecp") {
    date_default_timezone_set("Asia/Kolkata");


    // $path = $_SERVER['DOCUMENT_ROOT'].'/upload/examletterhead/';
    $path ='../upload/examletterhead/';
    //$path =  "http://49.50.64.199/upload/examletterhead/";
    $learnerCode = $_POST['lid'];
    $file = $_POST['lid'] . '.fdf';
    $filePath = $path . $file;
    $allreadyfoundAkcRecipt = $path . $learnerCode . '_ExamResultLetter.pdf';

    $response = $emp->GetAllPrintRecpDwnld($_POST['lid'],$_POST['eid']);

    $co = mysqli_num_rows($response[2]);
    $res = '';
    if ($co) {
      
        $_Row1 = mysqli_fetch_array($response[2], true);
        $name=strtoupper($_Row1['name']);
        $fname=strtoupper($_Row1['fname']);


        $_Row1["examdate"] = date("d-M-Y", strtotime($_Row1['exam_held_date']));
        $_Row1["resultdate"] = date("d-M-Y", strtotime($_Row1['result_date']));

        $_Row1["lname"] = $name." S/D/H/W/o ".$fname;
        //$_Row1['fname'] = " S/D/H/W/o ".strtoupper($_Row1['fname']);
        $_Row1["lcode"] = $_Row1['scol_no'];
        $_Row1["obtmarks"] = $_Row1['tot_marks'];
        $_Row1["lbatch"] = $_Row1['Batch_Name'];

        $_Row1["issuedate"] = date("d-M-Y");
        $fdfContent = '%FDF-1.2
            %Ã¢Ã£Ã?Ã“
            1 0 obj 
            <<
            /FDF 
            <<
            /Fields [';
        foreach ($_Row1 as $key => $val) {
            $fdfContent .= '
                    <<
                    /V (' . $val . ')
                    /T (' . $key . ')
                    >> 
                    ';
        }
        $fdfContent .= ']
            >>
            >>
            endobj 
            trailer
            <<
            /Root 1 0 R
            >>
            %%EOF';
        file_put_contents($filePath, $fdfContent);

        $fdfFilePath = $filePath;
        $fdfPath = str_replace('/', '//', $fdfFilePath);
        $resultFile = '';
       //  $defaulPermissionLetterFilePath = $_SERVER['DOCUMENT_ROOT'].'/myrkclphp7/upload/examletterhead/_RS-CIT-Letter.pdf';
         $defaulPermissionLetterFilePath = $path.'/_RS-CIT-Letter.pdf';
        if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
            $defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);
            $resultFile = $learnerCode . '_RS-CIT-Letter.pdf';

            $newURL = $path . $resultFile;

            $resultPath = str_replace('/', '//', $newURL);
            $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';


            $command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
            
            exec($command);

            // $filepath5 = 'upload/examletterhead/' . $resultFile;
            // //$filepath5 = $path.$resultFile;
            // echo $filepath5;

            $filepath5 = 'common/showpdfftp.php?src=examletterhead/' . $resultFile;
            unlink($fdfFilePath);
            $directoy = '/examletterhead/';
            $file = $resultFile;
            $response_ftp = ftpUploadFile($directoy, $file, $newURL);
            if(trim($response_ftp) == "SuccessfullyUploaded"){

                $response1 = $emp->SetRecord($_Row1['scol_no'],$_Row1['exam_event_name'],$_Row1['Batch_Code']);

                echo $filepath5;
            }
            $res = "DONE";
        
          } 
        }
    }
    if ($_action == "delgeninvoice") {
    $filename = explode("/", $_POST['lid']);
   echo $path = $_SERVER['DOCUMENT_ROOT'].'/upload/examletterhead/' . $filename[2];
    unlink($path);
}
?>