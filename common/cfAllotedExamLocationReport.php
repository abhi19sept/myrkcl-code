<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);

include './commonFunction.php';
require 'BAL/clsAllotedExamLocationReport.php';

$response = array();
$emp = new clsAllotedExamLocationReport();

if ($_action == "SHOW") 
{
	if($_POST['Event']==''){
			echo "blank";
	}
	else{		
		$eventid = $_POST['Event'];
		$filename = "AllotedExamLocation_" . $eventid . ".csv";
		$fileNamePath = "../upload/eventresultdata/" . $filename;
		$response = $emp->GetAll($_POST['Event']);

		$myFile = fopen($fileNamePath, 'w');

    fputs($myFile, '"' . implode('","', array("IT-GK Code", "Exam District", "Exam Tehsil", "Mapped Choice", "Count")) . '"' . "\n");

        while ($row1 = mysqli_fetch_row($response[2])) { 

            fputcsv($myFile, array($row1['0'], $row1['1'],$row1['2'], $row1['3'], $row1['4']), ',', '"');
        }
        fclose($myFile);
        echo "upload/eventresultdata/" . $filename;	
	}	
}
?>