<?php
include './commonFunction.php';
require 'BAL/clsEnquiryReport.php';

$response = array();
$emp = new clsEnquiryReport();

if ($_POST['action'] == "ViewEnquiryReport") {
    
    $ddlEnquirytype = $_POST['ddlEnquirytype'];
    $ddlstartdate = $_POST['ddlstartdate'];
    $ddlenddate = $_POST['ddlenddate'];
    if($ddlEnquirytype == "learner"){
        $response = $emp->ShowEnquiryReport($ddlstartdate,$ddlenddate);
        echo "<div class='table-responsive'>";
        echo  "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th>Name</th>";
        echo "<th>Qualification</th>";
        echo "<th>District</th>";
        echo "<th>Tehsil</th>";
        echo "<th>Mobile</th>";
        echo "<th>Email</th>";             
        echo "<th>Enquiry Date</th>";             
        echo "<th>Message</th>";             
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        if ($response[0] == 'Success') {
            while ($row = mysqli_fetch_array($response[2])) {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" .strtoupper($row['Enquiry_learnerName']). "</td>";
                echo "<td>'" .strtoupper($row['Enquiry_learnerQualification']). "</td>";
                echo "<td>" .strtoupper($row['District_Name'])."</td>";
                echo "<td>" . strtoupper($row['Tehsil_Name']). "</td>";
                echo "<td>" .$row['Enquiry_learnerMobile']. "</td>";
                echo "<td>" .$row['Enquiry_learnerEmail']. "</td>";
                echo "<td>" .$row['Enquiry_timestamp']. "</td>";
                echo "<td>" .$row['Enquiry_message']. "</td>";
                echo "</tr>";
                $_Count++;
            }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";  
        }
    }
    if($ddlEnquirytype == "center"){
        $response = $emp->ShowCenterReport($ddlstartdate,$ddlenddate);
        echo "<div class='table-responsive'>";
        echo  "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th>Name</th>";
        echo "<th>District</th>";
        echo "<th>Mobile</th>";
        echo "<th>Email</th>";             
        echo "<th>Center Code</th>";             
        echo "<th>Enquiry Date</th>";             
        echo "<th>Message</th>";             
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        if ($response[0] == 'Success') {
            while ($row = mysqli_fetch_array($response[2])) {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" .strtoupper($row['Enquiry_centerName']). "</td>";
                echo "<td>" .strtoupper($row['District_Name'])."</td>";
                echo "<td>" .$row['Enquiry_centerMobile']. "</td>";
                echo "<td>" .$row['Enquiry_centerEmail']. "</td>";
                echo "<td>" .$row['Enquiry_centercode']. "</td>";
                echo "<td>" .$row['Enquiry_timestamp']. "</td>";
                echo "<td>" .$row['Enquiry_centermessage']. "</td>";
                echo "</tr>";
                $_Count++;
            }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";  
        }
    }
    
}

?>