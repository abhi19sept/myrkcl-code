<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsVisitFeedbackRPT.php';

$response = array();
$emp = new clsVisitFeedbackRPT();


if ($_action == "DETAILS") {
    //print_r($_POST);die;
    //echo "Show";
    $response = $emp->GetAll($_POST['values']);
    if ($count = mysqli_num_rows($response[2])) {
        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='5%'>S No.</th>";
        echo "<th style='10%'>Visit ID</th>";
        echo "<th style='10%'>IT-GK Code</th>";
        echo "<th style='40%'>Visit Status</th>";
        echo "<th style='10%'>Visit Date</th>";

        echo "<th style='40%'>Visit Confirm Date</th>";
    echo "<th style='10%'>Counsellor Name</th>";
    echo "<th style='10%'>Counsellor Mobile No</th>";
     echo "<th style='10%'>Faculty Name</th>";
     
     echo "<th style='40%'>Faculty Mob No</th>";
    echo "<th style='10%'>Reception Area</th>";
    echo "<th style='10%'>Waiting Areo</th>";
     echo "<th style='10%'>Theory Aree</th>";
     echo "<th style='40%'>Lab Are</th>";
    echo "<th style='10%'>Infra Quality</th>";
    echo "<th style='10%'>Internet</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        if ($response[0] == 'Success') {

            while ($_Row = mysqli_fetch_array($response[2])) {
                $result  = getat($_Row);
              

                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $_Row['visitid'] . "</td>";
                echo "<td>" . $_Row['itgk_code'] . "</td>";
                if($_Row['status'] == '0'){
                    echo "<td>Pending</td>";
                }elseif ($_Row['status'] == '1') {
                    echo "<td>Confirm</td>";
                }else {
                    echo "<td>NA</td>";
                }
                
                echo "<td>" . $_Row['visit_date'] . "</td>";

                echo "<td>" . date("Y-M-d",$_Row['confirm_date']) . "</td>";
            echo "<td>" . $result['counsellor_name'] . "</td>";
            echo "<td>" . $result['counsellorMobileNumber'] . "</td>";
            echo "<td>" . $result['faculty_name'] . "</td>";
            
            echo "<td>" . $result['facultyMobileNumber'] . "</td>";
            echo "<td>" . $result['receptionArea'] . "</td>";
            echo "<td>" . $result['waitingArea'] . "</td>";
            echo "<td>" . $result['theoryArea'] . "</td>";
            echo "<td>" . $result['labArea'] . "</td>";
            echo "<td>" . $result['InfraQuality'] . "</td>";
            echo "<td>" . $result['internet'] . "</td>";
                echo "</tr>";
                $_Count++;
            }
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo '0';
    }
}

function getat($row) {
$result = [];
    if (!empty($row['feedbackdata'])) {
       
        $fdata = unserialize($row['feedbackdata']);
        //echo "<pre>";print_r($fdata['labArea']); 
        $exefull = 1;
        if ($exefull) {
            $result['counsellor_name'] = (!empty($fdata['counsellor_name'])) ? $fdata['counsellor_name'] : 'NA';
            $result['counsellorMobileNumber'] = (!empty($fdata['counsellorMobileNumber'])) ? $fdata['counsellorMobileNumber'] : 'NA';
            $result['faculty_name'] = (!empty($fdata['faculty_name'])) ? $fdata['faculty_name'] : 'NA';
            
            $result['facultyMobileNumber'] = (!empty($fdata['facultyMobileNumber'])) ? $fdata['facultyMobileNumber'] : 'NA';
            $result['receptionArea'] = (!empty($fdata['receptionArea'])) ? $fdata['receptionArea'] : 'NA';
            $result['waitingArea'] = (!empty($fdata['waitingArea'])) ? $fdata['waitingArea'] : 'NA';
            $result['theoryArea'] = (!empty($fdata['theoryArea'])) ? $fdata['theoryArea'] : 'NA';
            $result['labArea'] = (!empty($fdata['labArea'])) ? $fdata['labArea'] : 'NA';
            $result['InfraQuality'] = (!empty($fdata['InfraQuality'])) ? $fdata['InfraQuality'] : 'NA';
            $result['internet'] = (!empty($fdata['internet'])) ? $fdata['internet'] : 'NA';
        }

    }
    return  $result;
}
