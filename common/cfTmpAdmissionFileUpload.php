<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

include './commonFunction.php';
include 'BAL/clsTmpAdmissionFileUpload.php';

$response = array();
$emp = new clsTmpAdmissionFileUpload();

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

  //print_r($_FILES);

$_UploadDirectory = $_SERVER['DOCUMENT_ROOT'] . '/upload/temp_admission_data/';

if ($_FILES['SelectedFile']['error'] > 0) {
    outputJSON("<span class='error'>An error ocurred when uploading.</span>");
}

// Check filesize
if ($_FILES['SelectedFile']['size'] > 2000000000000) {
    outputJSON("<span class='error'>" . $_FILES['SelectedFile']['size'] . "File uploaded exceeds maximum upload size.</span>");
}

$ext = pathinfo($_FILES['SelectedFile']['name'], PATHINFO_EXTENSION);
//$ext="xls";
// Check if the file exists
if ($ext == "csv") {
   /* if (file_exists($_UploadDirectory . $_POST['UploadId'] . "." . $ext)) {
        //outputJSON("<span class='error'>File with that name already exists.</span>");
        unlink($_UploadDirectory . $_POST['UploadId'] . "." . $ext);
    }*/

// Upload file

    if (file_exists($_UploadDirectory)) {
        if (is_writable($_UploadDirectory)) {
			date_default_timezone_set('Asia/Calcutta');
			 //$login = date("Y-m-d_H-i-s");
            if (!copy($_FILES['SelectedFile']['tmp_name'], $_UploadDirectory . $_POST['UploadId'] . "." . $ext)) {
                outputJSON("<span class='error'>Error uploading file - check destination is writeable.</span>");
            }
        } else {
            outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
			}
		} else {
        outputJSON("<span class='error'>Upload Folder Not Available</span>");
    }
		
		
} 

else {
    outputJSON("<span class='error'>Invalid File Formet. Please Upload .csv Files</span>");
}