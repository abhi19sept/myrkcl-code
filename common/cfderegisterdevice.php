<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsderegisterdevice.php';

$response = array();
$emp = new clsderegisterdevice();


if ($_action == "ADD") {
    if ($_POST["reason"] != '') {
        $_CenterCode = $_POST["Ccode"];
        $_Reason = $_POST["reason"];
        $_serialno = $_POST["serialno"];
        $response = $emp->Update($_CenterCode, $_Reason, $_serialno);
        echo $response[0];
    } else {
        echo "1";
    }
}




if ($_action == "DETAILS") {
    if ($_POST['values'] != '') {
        $response = $emp->GetDeviceDetails($_POST['values']);
        $_DataTable = "";

        if ($response[0] == 'Success') {
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th style='5%'>S No.</th>";
            echo "<th style='30%'>BioMatric Serial No.</th>";
            echo "<th style='55%'>BioMatric Model</th>";
            echo "<th style='10%'>BioMatric Make</th>";
            echo "<th>Action</th>";

            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $_Row['BioMatric_SerailNo'] . "</td>";
                echo "<td>" . $_Row['BioMatric_Model'] . "</td>";
                echo "<td>" . $_Row['BioMatric_Make'] . "</td>";
                //echo "<td><input type='button' class='viewdata' id=" . $_Row['BioMatric_SerailNo'] .
                //" name=" . $_Row['BioMatric_SerailNo'] . "></input></td>";
                echo "<td><input type='button' style='margin-left:25px; border-color: red;' name='punchin' id='" . $_Row['BioMatric_SerailNo'] . "' class='punchin btn btn-danger' value='Deregister' /> </td> ";
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "2";
        }
    } else {
        echo "1";
    }
}
?>