<?php

include './commonFunction.php';
require 'BAL/clsmodifyITGK.php';
require '../DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();

$response = array();
$emp = new clsmodifyITGK();

if ($_action == "EDIT_NEW") {
    $response = $emp->GetDatabyCode_NEW();
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("Organization_Code" => $_Row['Organization_Code'],
            "Organization_Name" => $_Row['Organization_Name'],
            "Organization_Type" => $_Row['Organization_Type'],
            "Organization_Country" => $_Row["TCC"],
            "Organization_State" => $_Row["TSC"],
            "Organization_Region" => $_Row["TRC"],
            "Organization_District" => $_Row["TDC"],
            "Organization_Tehsil" => $_Row["TTC"],
            "Organization_Municipality_Type" => $_Row["TMTC"],
            "Organization_Panchayat" => $_Row["Organization_Panchayat"],
            "Organization_Gram" => $_Row["Organization_Gram"],
            "Organization_Village" => $_Row["Organization_Village"],
            "Organization_Municipality_Raj_Code" => $_Row["TMRC"],
            "Organization_Ward_Raj_Code" => $_Row["TWRC"],
            "Organization_Block_District" => $_Row["TBDC"],
            "Organization_GP_Block" => $_Row["TGB"],
            "Organization_Village_Code" => $_Row["TVC"],
            "Organization_AreaType" => $_Row["Organization_AreaType"],
            "Organization_OwnershipType" => $_Row["Organization_OwnershipType"],
            "Organization_DocType" => $_Row['Organization_DocType'],
            "Organization_Address" => $_Row['Organization_Address'],
            "Organization_Country_Name" => $_Row["Organization_Country"],
            "Organization_State_Name" => $_Row["Organization_State"],
            "Organization_Region_Name" => $_Row["Organization_Region"],
            "Organization_District_Name" => $_Row["Organization_District"],
            "Organization_Tehsil_Name" => $_Row["Organization_Tehsil"]);

        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}


if ($_action == "firstCheck") {
    $arr = [];
    $response = $emp->getAddressUpdateStatus($_SESSION["User_Code"], $_POST['fld_requestChangeType']);
    $row = mysqli_fetch_array($response[2]);
    $arr["total_count"] = mysqli_num_rows($response[2]);
    if ($arr["total_count"] > 0) {
        $arr["ref_no"] = $row["fld_ref_no"];
        $arr["fld_status"] = $row["fld_status"];
    } else {
        $arr["total_count"] = "-1";
        $arr["ref_no"] = "null";
        $arr["fld_status"] = "null";
    }
    echo json_encode($arr);
}

if ($_action == "firstCheck_edit") {

    $arr = [];

    $response = $emp->pullUpdateRequest($_POST["fld_requestChangeType"], $_POST['code']);

    $row = mysqli_fetch_array($response[2]);

    if (mysqli_num_rows($response[2]) > 0) {
        $arr["row_count"] = mysqli_num_rows($response[2]);
        $arr["fld_status"] = $row["fld_status"];

        if ($_POST["fld_requestChangeType"] == "address") {
            $response2 = $emp->GetDatabyCode_NEW();
            $_Row = mysqli_fetch_array($response2[2]);
            $arr = [
                "Organization_Code" => $_Row['Organization_Code'],
                "Organization_Name" => $_Row['Organization_Name'],
                "Organization_Type" => $_Row['Organization_Type'],
                "Organization_Country" => $_Row["TCC"],
                "Organization_State" => $_Row["TSC"],
                "Organization_Region" => $_Row["TRC"],
                "Organization_District" => $_Row["TDC"],
                "Organization_Tehsil" => $_Row["TTC"],
                "Organization_Municipality_Type" => $_Row["TMTC"],
                "Organization_Panchayat" => $_Row["Organization_Panchayat"],
                "Organization_Gram" => $_Row["Organization_Gram"],
                "Organization_Village" => $_Row["Organization_Village"],
                "Organization_Municipality_Raj_Code" => $_Row["TMRC"],
                "Organization_Ward_Raj_Code" => $_Row["TWRC"],
                "Organization_Block_District" => $_Row["TBDC"],
                "Organization_GP_Block" => $_Row["TGB"],
                "Organization_Village_Code" => $_Row["TVC"],
                "Organization_AreaType" => $_Row["Organization_AreaType"],
                "Organization_OwnershipType" => $_Row["Organization_OwnershipType"],
                "Organization_DocType" => $_Row['Organization_DocType'],
                "Organization_Address" => $_Row['Organization_Address'],
                "Organization_Country_Name" => $_Row["Organization_Country"],
                "Organization_State_Name" => $_Row["Organization_State"],
                "Organization_Region_Name" => $_Row["Organization_Region"],
                "Organization_District_Name" => $_Row["Organization_District"],
                "Organization_Tehsil_Name" => $_Row["Organization_Tehsil"],
                "row_count" => mysqli_num_rows($response[2]),
                "fld_status" => $row["fld_status"]
            ];
        }

        if ($_POST["fld_requestChangeType"] == "name") {
            $response2 = $emp->GetDatabyCode_NEW();
            $_Row = mysqli_fetch_array($response2[2]);
            $arr = [
                "Organization_Name_old" => $_Row['Organization_Name'],
                "Organization_Name" => $_Row['Organization_Name'],
                "row_count" => mysqli_num_rows($response[2]),
                "fld_status" => $row["fld_status"]
            ];
        }
    } else {
        $arr["row_count"] = 0;
        $arr["fld_status"] = $row["fld_status"];
    }
    echo json_encode($arr);
}

if ($_action == "checkMobile") {
    $arr = [];
    $response = $emp->checkMobile($_POST['phoneNumber'], $_SESSION["User_Code"]);
    $row = mysqli_fetch_array($response[2]);
    $arr["total_count"] = $row["total_count"];
    echo json_encode($arr);
}

if ($_action == "sendOTP") {
    /*     * * GENERATE RANDOM OTP ** */
    $otp = time();
    $response1 = $emp->insertOTP($otp, $_POST['phoneNumber'], $_POST['fld_requestChangeType']);
    if ($response1[0] == "Successfully Inserted") {
        echo "<script>$('#errorTextOTP_updateAddressITGK').html('');$('#showError_updateAddressITGK').css('display','none');$('#successText_updateAddressITGK').html('OTP Sent to +91-" . $_POST['phoneNumber'] . "');$('#showSuccess_updateAddressITGK').css('display','block');$('#verifyOTP_updateAddressITGK').css('display','block');$(\"#sendOTP_updateAddressITGK\").text(\"Resend OTP\").removeClass(\"disabled\");</script>";
    } else {
        echo "<script> $('#successText_updateAddressITGK').html('');$('#showSuccess_updateAddressITGK').css('display','none'); $('#errorTextOTP_updateAddressITGK').html('Mobile number already exist');$('#showError_updateAddressITGK').css('display','block');$(\"#sendOTP_updateAddressITGK\").text(\"Send OTP\").removeClass(\"disabled\");</script>";
    }
}

if ($_action == "verifyOTP") {
    $response = $emp->verifyOTP($_POST['otp']);
    $_Row = mysqli_fetch_array($response[2]);
    echo json_encode($_Row);
}

if ($_action == "revokeReference") {
    $response = $emp->revokeReference($_POST['otp']);
    echo $response["0"];
}

if ($_action == "updateOrgDetails") {
//    print_r($_POST["ddlReason"]);
//    print_r($_POST["Organization_OwnershipType"]);
// die;
    if ($_POST["Organization_OwnershipType"] == "Rent" || $_POST["Organization_OwnershipType"] == "Relative" || $_POST["Organization_OwnershipType"] == "Special") {
        $fileArray = ["fld_rentAgreement", "fld_utilityBill", "fld_document"];
        for ($i = 0; $i < count($fileArray); $i++) {
            $arr = $_FILES[$fileArray[$i]];
            $status[$fileArray[$i]] = uploadFiles($arr);
        }
    } else {
        $fileArray = ["fld_ownershipDocument", "fld_document"];
        for ($i = 0; $i < count($fileArray); $i++) {
            $arr = $_FILES[$fileArray[$i]];
            $status[$fileArray[$i]] = uploadFiles($arr);
        }
    }

    if (strpos(json_encode($status), "Failed")) {
        echo "Error in uploading images, please try again";
    } elseif($_POST['ddlTehsilNew'] == '' || empty($_POST["ddlTehsilNew"])){
        echo "Please Select Tehsil";
    } 
    else {
        $fileArray = $status;
        $response = $emp->updateOrgAddressRef($_POST, $fileArray);
        echo $response[0];
    }
}

function uploadFiles($temp) {
    global $_ObjFTPConnection;
$ftpaddress = $_ObjFTPConnection->ftpPathIp();
            $ftpconn = $_ObjFTPConnection->ftpConnect();
            $ftpdetail = $_ObjFTPConnection->ftpdetails();

    if ($_SERVER['HTTP_HOST'] == "localhost") {
        //$imageFolder = $_SERVER['DOCUMENT_ROOT'] . "/myrkcl/upload/address_proof/";
        $imageFolder = "/address_proof/";
    } else {
        //$imageFolder = $_SERVER['DOCUMENT_ROOT'] . "/upload/address_proof/";
        $imageFolder = "/address_proof/";
    }

    if (is_uploaded_file($temp['tmp_name'])) {

        if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
            return "Image upload Failed ! Invalid file name";
        }

        if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("jpg", "jpeg", "pdf"))) {
            return "Failed ! Invalid extension";
        }

        $string = "IMG_" . $_SESSION['User_Code'] . "_" . date("YmdHis");
        $fileName = $string . "_" . $temp['name'];
        $filetowrite = $imageFolder . $_SESSION['User_Code'] . '/' . $fileName;
        
        
        $ftpdirexists = $ftpdetail.$imageFolder.$_SESSION['User_Code'];
        
$folder_exists = is_dir($ftpdirexists);
if($folder_exists){
    $path= $imageFolder.$_SESSION['User_Code'];
ftpUploadFile($path,$fileName,$temp['tmp_name']);
return $fileName;
} else{
    if (!(ftp_mkdir($ftpconn, $imageFolder . $_SESSION['User_Code'])))
              {
              echo "Error while creating directory";
              }
              $path= $imageFolder.$_SESSION['User_Code'];
ftpUploadFile($path,$fileName,$temp['tmp_name']);
return $fileName;
}
    
    
//        if (!file_exists($imageFolder . $_SESSION['User_Code'])) {
//            mkdir($imageFolder . $_SESSION['User_Code'], 0777, true);
//        }
//        $path= $imageFolder.$_SESSION['User_Code'];
//        echo "<br/>";
//        echo $filetowrite;
//        echo  $temp['tmp_name'];
//        die;
//        
//ftpUploadFile($path,$fileName,$temp['tmp_name']);
//return $fileName;
//        if (move_uploaded_file($temp['tmp_name'], $filetowrite)) {
//            return $fileName;
//        } else {
//            return "Image upload Failed ! File upload error, please try again";
//        }
    } else {
        return "Image upload Failed ! File not found";
    }
}

if ($_action == "updateOrgDetails_name") {
    global $_ObjFTPConnection;
$ftpaddress = $_ObjFTPConnection->ftpPathIp();
            $ftpconn = $_ObjFTPConnection->ftpConnect();
            $ftpdetail = $_ObjFTPConnection->ftpdetails();

    if ($_SERVER['HTTP_HOST'] == "localhost") {
        //$imageFolder = $_SERVER['DOCUMENT_ROOT'] . "/myrkcl/upload/identity_proof/";
        $imageFolderIdentity = "/identity_proof/";
    } else {
//        $imageFolder = $_SERVER['DOCUMENT_ROOT'] . "/upload/identity_proof/";
        $imageFolderIdentity = "/identity_proof/";
    }

    $arr = $_FILES;
    reset($_FILES);
    $temp = current($_FILES);

    if (is_uploaded_file($temp['tmp_name'])) {

        if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
            echo "Invalid file name";
        }

        if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("jpg", "jpeg", "pdf"))) {
            echo "Invalid extension";
        }

        $string = "IMG_" . $_SESSION['User_Code'] . "_" . date("YmdHis");
        $fileName = $string . "_" . $temp['name'];
        $filetowrite = $imageFolderIdentity . $_SESSION['User_Code'] . '/' . $fileName;

        $ftpidentitydirexists = $ftpdetail.$imageFolderIdentity.$_SESSION['User_Code'];
        
$folder_identity_exists = is_dir($ftpidentitydirexists);
if($folder_identity_exists){
    $path1= $imageFolderIdentity.$_SESSION['User_Code'];
ftpUploadFile($path1,$fileName,$temp['tmp_name']);
//return $fileName;
            $responseArray = [];
            $response = $emp->updateOrgNameRef($_POST, $fileName);
            echo $response[0];
} else{
    if (!(ftp_mkdir($ftpconn, $imageFolderIdentity . $_SESSION['User_Code'])))
              {
              echo "Error while creating directory";
              }
              $path1= $imageFolderIdentity.$_SESSION['User_Code'];
ftpUploadFile($path1,$fileName,$temp['tmp_name']);
//return $fileName;
            $responseArray = [];
            $response = $emp->updateOrgNameRef($_POST, $fileName);
            echo $response[0];
}


//        if (!file_exists($imageFolder . $_SESSION['User_Code'])) {
//            mkdir($imageFolder . $_SESSION['User_Code'], 0777, true);
//        }
//
//        if (move_uploaded_file($temp['tmp_name'], $filetowrite)) {
//            $responseArray = [];
//            $response = $emp->updateOrgNameRef($_POST, $fileName);
//            echo $response[0];
//        } else {
//            echo "File upload error, please try again";
//        }
    } else {
        echo "File not found";
        return;
    }
}

if ($_action == "sendEmailToSP") {
    $responseArray = [];
    $response = $emp->getSPDetails($_SESSION['User_Rsp']);
    $row = mysqli_fetch_array($response[2]);
    $response2 = $emp->sendEmailtoSP($row['email'], $row['mobile'], $row['username'], $_POST['reference_number'], $_REQUEST["requestType"]);
    echo "1";
}

if ($_action == "sendEmailToITGK") {
    $responseArray = [];
    $response4 = $emp->getCenterDetails($_POST['reference_number']);
    $row = mysqli_fetch_assoc($response4[2]);
    $response2 = $emp->sendEmailtoITGK($row['User_EmailId'], $row['User_MobileNo'], $row['ITGK_Name'], $_POST['reference_number'], $row['requestType']);
    echo "1";
}

if ($_action == "showAddressUpdateRequestTable") {


    echo "<div class='table-responsive'>";
    echo "<table id='example1' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<tbody>";
    echo "<tr>";
    echo "<td style='font-size:12px !important;'>" . "<b>Click on ITGK Code to view Update Requests made by various ITGK's</b></td>";
    echo "</tr>";
    echo "</tbody>";
    echo "</table>";
    echo "</div>";


    echo "<div class='table-responsive'>";
    echo "<table id='example' class='table table-striped table-bordered'>";
    echo "<thead>
            <tr>
                <th style='width: 1%'>S.No</th>
                <th style='width: 83%;'>ITGK Code</th>
                <th style='width: 16%;' >Modified By</th>                
            </tr>
        </thead>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "showAddressUpdateRequest") {
    //print_r($_POST);
    $response = $emp->showUpdateRequest('address');
    echo json_encode($response);
}

if ($_action == "showNameUpdateRequest") {
    $response = $emp->showUpdateRequest('name');
    echo json_encode($response);
}

if ($_action == "getPaymentDetails") {
    $response = $emp->getPaymentDetails($_POST['Code']);
    $_i = 0;
    $_DataTable = [];

    while ($row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array(
            "fld_ref_no" => $row['fld_ref_no'],
            "fld_paymentTitle" => $row['fld_paymentTitle'],
            "fld_rsp" => $row['fld_rsp'],
            "fld_ITGK_UserCode" => $row['fld_ITGK_UserCode'],
            "fld_ITGK_Code" => $row['fld_ITGK_Code'],
            "fld_transactionID" => $row['fld_transactionID'],
            "fld_bankTransactionID" => $row['fld_bankTransactionID'],
            "fld_amount" => $row['fld_amount'],
            "fld_updatedOn" => date("F d, Y h:i a", strtotime($row['fld_updatedOn'])),
            "fld_paymentStatus" => ($row["fld_paymentStatus"] == '1') ? "Paid" : "Payment Pending"
        );
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "showNameUpdateRequestTableITGK") {

    echo "<div class='table-responsive'>";
    echo "<table id='example1' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<tbody>";
    echo "<tr>";
    echo "<td style='font-size:12px !important;'>" . "<b>Click on Ref # for details</b></td>";
    echo "</tr>";
    echo "</tbody>";
    echo "</table>";
    echo "</div>";


    echo "<div class='table-responsive'>";
    echo "<table id='example' class='table table-striped table-bordered'>";
    echo "<thead>
            <tr>
                <th style='width: 5%'>S.No</th>
                <th style='width: 10%;'>Ref #</th>
                <th style='width: 24%;'>ITGK Code</th> 
                <th style='width: 24%;'>ITGK Name</th> 
                <th style='width: 24%;'>ITGK District</th>
                <th style='width: 15%;'>Generated At</th>
                <th style='width: 15%;'>Last Modified On</th>
                <th style='width: 11%;'>Modified By</th>    
                <th style='width: 14%;'>Process Type</th>
                <th style='width: 14%;'>Status</th>
            </tr>
        </thead>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "showAddressUpdateRequestITGK") {
    $flag = 'name';
    $response = $emp->showNameUpdateRequestITGK($_POST['Code'], $_POST['flag']);
    echo json_encode($response);
}

if ($_action == "showAddressUpdateRequestTableITGK") {
    $flag = 'address';
    $response = $emp->showUpdateRequestITGK($_POST['Code'], $flag);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='width: 5%'>S.No</th>";
    echo "<th style='width: 10%;'>Ref #</th>";
    echo "<th style='width: 4%;'>ITGK Code</th>";
    echo "<th style='width: 24%;'>ITGK Name</th>";
    echo "<th style='width: 4%;'>ITGK District</th>";
    echo "<th style='width: 15%;'>Generated At</th>";
    echo "<th style='width: 15%;'>Last Modified On</th>";
    echo "<th style='width: 11%;'>Modified By</th>";
    echo "<th style='width: 14%;'>Process Type</th>";
    echo "<th style='width: 14%;'>Status</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    if ($response[0] == 'Success') {
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            //print_r($_Row['fld_ref_no']);
            $fld_ref_no = base64_encode($_Row['fld_ref_no']);
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            if ($_SESSION["User_UserRoll"] == "14") {
                echo "<td> <a href='viewDetail.php?Code=$fld_ref_no'>" . $_Row['fld_ref_no'] . "</a></td>";
            } elseif ($_SESSION["User_UserRoll"] == "1" || $_SESSION["User_UserRoll"] == "4" || $_SESSION["User_UserRoll"] == "11") {
                echo "<td> <a href='viewDetail_rkcl.php?Code=$fld_ref_no'>" . $_Row['fld_ref_no'] . "</a></td>";
            }elseif ($_SESSION["User_UserRoll"] == "7") {
                echo "<td> <a href='viewDetails_ITGK.php?Code=$fld_ref_no'>" . $_Row['fld_ref_no'] . "</a></td>";
            }
            echo "<td>" . ($_Row['fld_ITGK_Code']) . "</td>";
            echo "<td>" . ($_Row['Organization_Name']) . "</td>";
            echo "<td>" . ($_Row['District_Name']) . "</td>";
            echo "<td>" . ($_Row['fld_addedOn']) . "</td>";
            echo "<td>" . ($_Row['fld_updatedOn']) . "</td>";
            echo "<td>" . ($_Row['fld_updatedBy']) . "</td>";
            echo "<td>" . ($_Row['Process_Type']) . "</td>";
            echo "<td>" . ($_Row['fld_status']) . "</td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
}

if ($_action == "SendSMS") {
//    print_r($_POST);
//    die;
    $_SMS = $_POST["sms"];
    $_CenterCode = $_POST["centercode"];
    $RefId = $_POST["fld_ref_id"];
    $response = $emp->SendSMS($_SMS, $_CenterCode, $RefId);
    echo $response[0];
    
}

if ($_action == "DOWNLOAD") {
    
    $flag='address';
    $filename = "Address Update Request Center Detail.csv";

    $fileNamePath = "../upload/" . $filename;

    $myFile = fopen($fileNamePath, 'w');

    fputs($myFile, '"' . implode('","', array('S.No','Ref #','ITGK Code','ITGK Name','ITGK District','Generated At','Last Modified On','Modified By','Process Type','Status')) . '"' . "\n");

    $response = $emp->Download_Excel($_POST['usercode'], $flag);


	$csv_row ='';
	$count=1;
    while ($row = mysqli_fetch_array($response[2])) {
        if($row['fld_addedOn'] < '2019-09-19 00:00:00'){
            $ProcessType = "Old Process";
        } else {
            $ProcessType = "New Process";
        }
        $csv_row .= '"' . $count . '",';
		$csv_row .= '"' . $row['fld_ref_no'] . '",';
		$csv_row .= '"' . $row['fld_ITGK_Code'] . '",';
		$csv_row .= '"' . $row['Organization_Name'] . '",';
		$csv_row .= '"' . $row['District_Name'] . '",';
		$csv_row .= '"' . $row['fld_addedOn'] . '",';
		$csv_row .= '"' . $row['fld_updatedOn'] . '",';
        $csv_row .= '"' . $row['fld_updatedBy'] . '",';
        $csv_row .= '"' . $ProcessType . '",';
        $csv_row .= '"' . $row['fld_status'] . '",';
	
		$count++;
		$csv_row .= "\n";
    }
   // fputcsv($fp, $csv_row);
    fwrite($myFile, $csv_row);
    fclose($myFile);
    echo '/'.$fileNamePath;
        
}

if ($_action == "showNameUpdateRequestITGK") {
    $response = $emp->showUpdateRequestITGK($_POST['Code'], $_POST['flag']);
    echo json_encode($response);
}

if ($_action == "FillUpdateAddressDetails") {
    global  $_ObjFTPConnection;
    $_DataTable = array();
    $_i = 0;
    $response = $emp->FillUpdateAddressDetails($_POST['Code']);
     $details= $_ObjFTPConnection->ftpdetails();
    while ($_Row = mysqli_fetch_array($response[2])) {
        if($_Row["fld_requestChangeType"] == 'address'){
          $photopath = '/address_proof/'.$_Row["Organization_User_Code"].'/'; 
          
          if($_Row["fld_ownershipDocument"] !=''){
              $ext = pathinfo($_Row['fld_ownershipDocument'], PATHINFO_EXTENSION);
                if($ext == 'pdf'){
                    $OwnershipPath =  '/address_proof/'.$_Row["Organization_User_Code"].'/'.$_Row['fld_ownershipDocument'];

                    $showOwnership = "<a data-fancybox data-type='iframe' data-src='common/showpdfftp.php?src=".$OwnershipPath."' href='javascript:;'>Click to View </a>";
                }
              else{
                    $OwnershipDoc = $_Row["fld_ownershipDocument"];
                    $OwnershipImg =  file_get_contents($details.$photopath.$OwnershipDoc);
                    $OwnershipData = base64_encode($OwnershipImg);
                    $showOwnership =  "<iframe id='ftpimg' src='data:image/png;base64, ".$OwnershipData."' alt='Red dot' width='100%' height='500px'></iframe>";
                }
            
          } else {
              $showOwnership = $_Row["fld_ownershipDocument"];
          }
          
          if($_Row["fld_rentAgreement"] !=''){
              $ext = pathinfo($_Row['fld_rentAgreement'], PATHINFO_EXTENSION);
                    if($ext == 'pdf'){
			$RentPath =  '/address_proof/'.$_Row["Organization_User_Code"].'/'.$_Row['fld_rentAgreement'];

                        $showRent = "<a data-fancybox data-type='iframe' data-src='common/showpdfftp.php?src=".$RentPath."' href='javascript:;'>Click to View </a>";
		    }
		  else{
                        $RentAgreement = $_Row["fld_rentAgreement"];
			$RentImg =  file_get_contents($details.$photopath.$RentAgreement);
                        $RentData = base64_encode($RentImg);
                        $showRent =  "<iframe id='ftpimg' src='data:image/png;base64, ".$RentData."' alt='Red dot' width='100%' height='500px'></iframe>";
                    }
          } else {
              $showRent = $_Row["fld_rentAgreement"];
          }
          
          if($_Row["fld_utilityBill"] !=''){
            $ext = pathinfo($_Row['fld_utilityBill'], PATHINFO_EXTENSION);
                if($ext == 'pdf'){
                    $UtilityPath =  '/address_proof/'.$_Row["Organization_User_Code"].'/'.$_Row['fld_utilityBill'];

                    $showUtilityBill = "<a data-fancybox data-type='iframe' data-src='common/showpdfftp.php?src=".$UtilityPath."' href='javascript:;'>Click to View </a>";
                }
              else{
                    $UtilityBill = $_Row["fld_utilityBill"];
                    $UtilityBillImg =  file_get_contents($details.$photopath.$UtilityBill);
                    $UtilityBillData = base64_encode($UtilityBillImg);
                    $showUtilityBill =  "<iframe id='ftpimg' src='data:image/png;base64, ".$UtilityBillData."' alt='Red dot' width='100%' height='500px'></iframe>";
                }
            
          } else {
              $showUtilityBill = $_Row["fld_utilityBill"];
          }
          
          if($_Row["fld_status"] =='2'){
              
            $visitphotopath = "/AddressApprovalPhoto/";
            $FrontPhoto = $_Row["fld_FrontPhoto"];
            $FrontPhotoImg =  file_get_contents($details.$visitphotopath.$FrontPhoto);
            $FrontPhotoData = base64_encode($FrontPhotoImg);
            $showFrontPhoto =  "<img id='ftpimg' src='data:image/png;base64, ".$FrontPhotoData."' alt='Red dot' width='100%' height='500px'/>";
            
            $LeftPhoto = $_Row["fld_LeftPhoto"];
            $LeftPhotoImg =  file_get_contents($details.$visitphotopath.$LeftPhoto);
            $LeftPhotoData = base64_encode($LeftPhotoImg);
            $showLeftPhoto =  "<iframe id='ftpimg' src='data:image/png;base64, ".$LeftPhotoData."' alt='Red dot' width='100%' height='500px'></iframe>";
            
            $RightPhoto = $_Row["fld_RightPhoto"];
            $RightPhotoImg =  file_get_contents($details.$visitphotopath.$RightPhoto);
            $RightPhotoData = base64_encode($RightPhotoImg);
            $showRightPhoto =  "<iframe id='ftpimg' src='data:image/png;base64, ".$RightPhotoData."' alt='Red dot' width='100%' height='500px'></iframe>";
            
            $LabPhoto = $_Row["fld_LabSpace"];
            $LabSpaceImg =  file_get_contents($details.$visitphotopath.$LabPhoto);
            $LabSpaceData = base64_encode($LabSpaceImg);
            $showLabSpace =  "<iframe id='ftpimg' src='data:image/png;base64, ".$LabSpaceData."' alt='Red dot' width='100%' height='500px'></iframe>";
          } else {
            $showFrontPhoto = $_Row["fld_FrontPhoto"];
            $showLeftPhoto = $_Row["fld_LeftPhoto"];
            $showRightPhoto = $_Row["fld_RightPhoto"];
            $showLabSpace = $_Row["fld_LabSpace"];
              
          }

        } else {
            $photopath = '/identity_proof/'.$_Row["Organization_User_Code"].'/';
            $showOwnership = $_Row["fld_ownershipDocument"];
            $showRent = $_Row["fld_rentAgreement"];
            $showUtilityBill = $_Row["fld_utilityBill"];
            $showFrontPhoto = $_Row["fld_FrontPhoto"];
            $showLeftPhoto = $_Row["fld_LeftPhoto"];
            $showRightPhoto = $_Row["fld_RightPhoto"];
            $showLabSpace = $_Row["fld_LabSpace"];
        }
        
         $extdoc = pathinfo($_Row['fld_document'], PATHINFO_EXTENSION);
                if($extdoc == 'pdf'){
                    $DocsPath =  '/address_proof/'.$_Row["Organization_User_Code"].'/'.$_Row['fld_document'];

                    $showimage = "<a data-fancybox data-type='iframe' data-src='common/showpdfftp.php?src=".$DocsPath."' href='javascript:;'>Click to View </a>";
                }
              else{
                    $photoid = $_Row['fld_document'];
                    $photoimage =  file_get_contents($details.$photopath.$photoid);
                    $imageData = base64_encode($photoimage);
                    $showimage =  "<iframe id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='100%' height='500px'></iframe>";
                }
        

        $_DataTable[$_i] = array(
            "fld_remarks" => $_Row["fld_remarks"],
            "fld_remarks_rkcl" => $_Row["fld_remarks_rkcl"],

           // "fld_document" => $_Row['fld_document'],
            "fld_document" => $showimage,
            
            "fld_VisitDate" => $_Row['fld_VisitDate'],
            //"fld_FrontPhoto" => $_Row['fld_FrontPhoto'],
            "fld_FrontPhoto" => $showFrontPhoto,
            //"fld_LeftPhoto" => $_Row['fld_LeftPhoto'],
            "fld_LeftPhoto" => $showLeftPhoto,
            //"fld_RightPhoto" => $_Row['fld_RightPhoto'],
            "fld_RightPhoto" => $showRightPhoto,
            //"fld_LabSpace" => $_Row['fld_LabSpace'],
            "fld_LabSpace" => $showLabSpace,
            "fld_MarkByRKCL" => $_Row['fld_MarkByRKCL'],
            
            "fld_document_encoded" => str_rot13($_Row['fld_document']),
            "Organization_User_Code" => $_Row["Organization_User_Code"],
            "Organization_User_Code_encoded" => str_rot13($_Row["Organization_User_Code"]),
            "Organization_ITGK_Code" => $_Row['fld_ITGK_Code'],
            "Organization_Name" => $_Row['Organization_Name_NEW'],
            "Organization_Name_old" => $_Row['Organization_Name_old'],
            "Organization_Type" => $_Row['Organization_Type'],
            "Organization_AreaType" => $_Row["Organization_AreaType"],
            "Organization_OwnershipType" => $_Row["Organization_OwnershipType"],
            "Organization_OwnershipType_old" => $_Row["Organization_OwnershipType_old"],
                "Owner_Name" => $_Row["Organization_Owner_Name"],
            //"fld_rentAgreement" => $_Row["fld_rentAgreement"],
            "fld_rentAgreement" => $showRent,
            //"fld_utilityBill" => $_Row["fld_utilityBill"],
            "fld_utilityBill" => $showUtilityBill,
            "fld_ownershipDocument" => $showOwnership,
            "fld_rentAgreement_encoded" => str_rot13($_Row["fld_rentAgreement"]),
            "fld_utilityBill_encoded" => str_rot13($_Row["fld_utilityBill"]),
            "fld_ownershipDocument_encoded" => str_rot13($_Row["fld_ownershipDocument"]),
            "fld_ownershipDocument_encoded" => $showOwnership,
            "Organization_Municipal_Type" => $_Row["Organization_Municipal_Type"],
            "Organization_Municipal" => $_Row["Organization_Municipal"],
            "Organization_WardNo" => $_Row["Organization_WardNo"],
            "Organization_Panchayat" => $_Row["Organization_Panchayat"],
            "Organization_Gram" => $_Row["Organization_Gram"],
            "Organization_Village" => $_Row["Organization_Village"],
            "Organization_Address" => $_Row['Organization_Address'],
            "Organization_Type_old" => $_Row['Organization_Type_old'],
            "Organization_AreaType_old" => $_Row["Organization_AreaType_old"],
            "Organization_Municipal_Type_old" => $_Row["Organization_Municipal_Type_old"],
            "Organization_Municipal_old" => $_Row["Organization_Municipal_old"],
            "Organization_WardNo_old" => $_Row["Organization_WardNo_old"],
            "Organization_Panchayat_old" => $_Row["Organization_Panchayat_old"],
            "Organization_Gram_old" => $_Row["Organization_Gram_old"],
            "Organization_Village_old" => $_Row["Organization_Village_old"],
            "Organization_Address_old" => $_Row['Organization_Address_old'],
            "submission_date" => date('F d, Y \a\t h:i a', strtotime($_Row['submission_date'])),
            "submission_date_sp" => date('F d, Y \a\t h:i a', strtotime($_Row['submission_date_sp'])),
            "submission_date_rkcl" => date('F d, Y \a\t h:i a', strtotime($_Row['submission_date_rkcl'])),
            "fld_updatedOn" => date('F d, Y \a\t h:i a', strtotime($_Row['fld_updatedOn'])),
            "fld_status" => $_Row["fld_status"]
        );
        $_i = $_i + 1;
    }

    $_DataTable[0]["key"] = ($_SERVER["HTTP_HOST"] == "localhost") ? "gtKFFx" : "X2ZPKM";

    if (isset($_POST["Command"]) && $_POST["Command"] <> "") {
        if (base64_decode($_POST['Command']) == "Location Change") {
            $response_url = ($_SERVER["HTTP_HOST"] == "localhost") ? "http://localhost/myrkcl/response_address.php" : "https://www.myrkcl.com/response_address.php";
            $_DataTable[0]["surl"] = $_DataTable[0]["furl"] = $_DataTable[0]["curl"] = $response_url;
        } else {
            $response_url = ($_SERVER["HTTP_HOST"] == "localhost") ? "http://localhost/myrkcl/response_name.php" : "https://www.myrkcl.com/response_name.php";
            $_DataTable[0]["surl"] = $_DataTable[0]["furl"] = $_DataTable[0]["curl"] = $response_url;
        }
        $response2 = $emp->getInvoiceMasterDetails(base64_decode($_POST['Command']));
        $row = mysqli_fetch_assoc($response2[2]);
        $_DataTable[0]['fld_amount'] = $row['Gst_Invoce_TotalFee'];
    }

    echo json_encode($_DataTable);
}

if ($_action == "FillLocationDetails") {
    $_DataTable = array();
    $_i = 0;
    $response = $emp->FillLocationDetails($_POST['Code'], $_POST['fld_ref_id']);
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array(
            "Organization_Country" => $_Row["Organization_Country"],
            "Organization_State" => $_Row['Organization_State'],
            "Organization_Region" => $_Row['Organization_Region'],
            "Organization_District" => $_Row['Organization_District'],
            "Organization_Tehsil_Old" => $_Row['Organization_Tehsil_Old'],
             "Organization_Tehsil" => $_Row['Organization_Tehsil'],
        );
        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}

/* * * CHANGES ACCEPTED BY SERVICE PROVIDER ** */
if ($_action == "commit") {
  //  echo "heelo";
    
//    print_r($_POST);
//print_r($_FILES);
//die;
    
     global $_ObjFTPConnection;
            $ftpaddress = $_ObjFTPConnection->ftpPathIp();
            $ftpconn = $_ObjFTPConnection->ftpConnect();
            $ftpdetail = $_ObjFTPConnection->ftpdetails();
        $RefNo = $_POST['Code'];

        $_POST['requestType'] = 'address';
        
        $VisitDate = date('Y-m-d', strtotime($_POST['txtEstdate']));
        
        $_UploadDirectory = "/AddressApprovalPhoto";
        
        $owntypeimg = $_FILES['uploadImage7']['name'];
        $owntypetmp = $_FILES['uploadImage7']['tmp_name'];
        $owntypetemp = explode(".", $owntypeimg);
        //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
        $owntypefilename = $RefNo . '_FrontView.' . end($owntypetemp);
        $owntypefilestorepath = "../upload/AddressApprovalPhoto/" . $owntypefilename;
        $owntypeimageFileType = pathinfo($owntypefilestorepath, PATHINFO_EXTENSION);
        $FrontView_response_ftp=ftpUploadFile($_UploadDirectory,$owntypefilename,$owntypetmp);

        $salpiimg = $_FILES['uploadImage8']['name'];
        $salpitmp = $_FILES['uploadImage8']['tmp_name'];
        $salpitemp = explode(".", $salpiimg);
        $salpifilename = $RefNo . '_LeftView.' . end($salpitemp);
        $salpifilestorepath = "../upload/AddressApprovalPhoto/" . $salpifilename;
        $salpiimageFileType = pathinfo($salpifilestorepath, PATHINFO_EXTENSION);
        $LeftView_response_ftp=ftpUploadFile($_UploadDirectory,$salpifilename,$salpitmp);

        $panpiimg = $_FILES['uploadImage9']['name'];
        $panpitmp = $_FILES['uploadImage9']['tmp_name'];
        $panpitemp = explode(".", $panpiimg);
        $panpifilename = $RefNo . '_RightView.' . end($panpitemp);
        $panpifilestorepath = "../upload/AddressApprovalPhoto/" . $panpifilename;
        $panpiimageFileType = pathinfo($panpifilestorepath, PATHINFO_EXTENSION);
        $RightView_response_ftp=ftpUploadFile($_UploadDirectory,$panpifilename,$panpitmp);

        $ccpiimg = $_FILES['uploadImage6']['name'];
        $ccpitmp = $_FILES['uploadImage6']['tmp_name'];
        $ccpitemp = explode(".", $ccpiimg);
        $ccpifilename = $RefNo . '_LabSpace.' . end($ccpitemp);
        $ccpifilestorepath = "../upload/AddressApprovalPhoto/" . $ccpifilename;
        $ccpiimageFileType = pathinfo($ccpifilestorepath, PATHINFO_EXTENSION);
        $LabSpace_response_ftp=ftpUploadFile($_UploadDirectory,$ccpifilename,$ccpitmp);

        if (($_FILES["uploadImage7"]["size"] < 200000 && $_FILES["uploadImage7"]["size"] > 100000) || ($_FILES["uploadImage8"]["size"] < 200000 && $_FILES["uploadImage8"]["size"] > 100000) || ($_FILES["uploadImage9"]["size"] < 200000 && $_FILES["uploadImage9"]["size"] > 100000) || ($_FILES["uploadImage6"]["size"] < 200000 && $_FILES["uploadImage6"]["size"] > 100000)) {

            if ($owntypeimageFileType == "jpg" && $salpiimageFileType == "jpg" && $panpiimageFileType == "jpg" && $ccpiimageFileType == "jpg") {
            if($VisitDate !='' && trim($FrontView_response_ftp) == "SuccessfullyUploaded" && trim($LeftView_response_ftp) == "SuccessfullyUploaded" && trim($RightView_response_ftp) == "SuccessfullyUploaded" && trim($LabSpace_response_ftp) == "SuccessfullyUploaded"){
                //if ($VisitDate !='' && move_uploaded_file($owntypetmp, $owntypefilestorepath) && move_uploaded_file($salpitmp, $salpifilestorepath) && move_uploaded_file($panpitmp, $panpifilestorepath) && move_uploaded_file($ccpitmp, $ccpifilestorepath)) {

                    $_DataTable = [];
    $response2 = $response3 = $response4 = [];
    $_i = 0;
    $response = $emp->commitChangesAccept($_POST['Code'], $_POST['fld_remarks'], $VisitDate, $owntypefilename, $salpifilename, $panpifilename, $ccpifilename);
//echo"Upload Successful";
    echo $response["0"];
                } else {
                    echo "Visit Photo not upload. Please try again.";
                }
            } else {
                echo "Sorry, File Not Valid";
            }
        } else {
            echo "File Size should be between 100KB to 200KB.";
        }
        
    


    /*     * * SEND EMAIL/SMS TO ITGK ** */
    $response4 = $emp->getCenterDetails($_POST['Code']);
    $row = mysqli_fetch_assoc($response4[2]);
    $row["type"] = "accepted";
    $emp->sendSMSEmail($row);

    /*     * * SEND EMAIL/SMS TO SERVICE PROVIDER ** */
    $response5 = $emp->getSPDetails($_SESSION['User_Code']);
    $row5 = mysqli_fetch_assoc($response5[2]);
    $response6 = $emp->sendEmailtoSPAccept($row5['username'], $row5['email'], $row5['mobile'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType']);

    /*     * * SEND EMAIL/SMS TO RKCL ** */
    $rkcl_UserCode = ($_SERVER['HTTP_HOST'] = "localhost") ? '7504' : '4257';
    $response6 = $emp->getRKCLDetails($rkcl_UserCode);
    $row6 = mysqli_fetch_assoc($response6[2]);
    $response7 = $emp->sendEmailtoRKCLAccept($row5['username'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType'], $row6);
}

/* * * NAME CHANGES ACCEPTED BY SERVICE PROVIDER ** */
if ($_action == "commitname") {
    $_DataTable = [];
    $response2 = $response3 = $response4 = [];
    $_i = 0;
    $response = $emp->commitChangesAcceptName($_POST['Code'], $_POST['fld_remarks']);

    echo $response["0"];


    /*     * * SEND EMAIL/SMS TO ITGK ** */
    $response4 = $emp->getCenterDetails($_POST['Code']);
    $row = mysqli_fetch_assoc($response4[2]);
    $row["type"] = "accepted";
    $emp->sendSMSEmail($row);

    /*     * * SEND EMAIL/SMS TO SERVICE PROVIDER ** */
    $response5 = $emp->getSPDetails($_SESSION['User_Code']);
    $row5 = mysqli_fetch_assoc($response5[2]);
    $response6 = $emp->sendEmailtoSPAccept($row5['username'], $row5['email'], $row5['mobile'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType']);

    /*     * * SEND EMAIL/SMS TO RKCL ** */
    $rkcl_UserCode = ($_SERVER['HTTP_HOST'] = "localhost") ? '7504' : '4257';
    $response6 = $emp->getRKCLDetails($rkcl_UserCode);
    $row6 = mysqli_fetch_assoc($response6[2]);
    $response7 = $emp->sendEmailtoRKCLAccept($row5['username'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType'], $row6);
}

/* * * CHANGES ACCEPTED BY RKCL ** */

if ($_action == "mark") {
    $_DataTable = [];
    $response2 = $response3 = $response4 = [];
    $_i = 0;
    $response = $emp->MarkIncorrectApplication($_POST['Code']);
    echo $response["0"];
        }

if ($_action == "commit_rkcl") {
    $_DataTable = [];
    $response2 = $response3 = $response4 = [];
    $_i = 0;
    $response = $emp->commitChangesAccept_rkcl($_POST['Code'], $_POST['fld_remarks_rkcl']);

    if ($response[0] == "Successfully Updated") {
        $response1 = $emp->GetAddressData($_POST['Code']);
        if ($response1[1] == 1) {
            $response2 = mysqli_fetch_assoc($response1[2]);

            /*             * * ADD ENTRY IN PAYMENT TABLE HERE ** */
            $response3 = $emp->commitChangeAddress($response2);
        }
    }

    echo $response3["0"];

    /*     * * SEND EMAIL TO ITGK ** */
    $response4 = $emp->getCenterDetails($_POST['Code']);
    $row = mysqli_fetch_assoc($response4[2]);
    $row["type"] = "accepted2";
    $emp->sendSMSEmail($row);

    /*     * * SEND EMAIL/SMS TO SERVICE PROVIDER ** */
    $response5 = $emp->getSPDetails($row['fld_rspCode']);
    $row5 = mysqli_fetch_assoc($response5[2]);
    $response6 = $emp->sendEmailtoSPAccept2($row5['username'], $row5['email'], $row5['mobile'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType']);

    /*     * * SEND EMAIL/SMS TO RKCL ** */
    $rkcl_UserCode = ($_SERVER['HTTP_HOST'] = "localhost") ? '7504' : '4257';
    $response6 = $emp->getRKCLDetails($rkcl_UserCode);
    $row6 = mysqli_fetch_assoc($response6[2]);
    $response7 = $emp->sendEmailtoRKCLAccept2($row5['username'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType'], $row6);
}

if ($_action == "commit_rkcl_address") {
    $_DataTable = [];
    $response2 = $response3 = $response4 = [];
    $_i = 0;
    $response = $emp->commitChangesAccept_rkcl_address($_POST['Code'], $_POST['fld_remarks_rkcl']);
//
//    if ($response[0] == "Successfully Updated") {
//        $response1 = $emp->GetAddressData($_POST['Code']);
//        if ($response1[1] == 1) {
//            $response2 = mysqli_fetch_assoc($response1[2]);
//
//            /*             * * ADD ENTRY IN PAYMENT TABLE HERE ** */
//            $response3 = $emp->commitChangeAddress($response2);
//        }
//    }

//    echo $response3["0"];
    echo $response["0"];

    /*     * * SEND EMAIL TO ITGK ** */
    $response4 = $emp->getCenterDetails($_POST['Code']);
    $row = mysqli_fetch_assoc($response4[2]);
    $row["type"] = "accepted2";
    $emp->sendSMSEmail($row);

    /*     * * SEND EMAIL/SMS TO SERVICE PROVIDER ** */
    $response5 = $emp->getSPDetails($row['fld_rspCode']);
    $row5 = mysqli_fetch_assoc($response5[2]);
    $response6 = $emp->sendEmailtoSPAccept2($row5['username'], $row5['email'], $row5['mobile'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType']);

    /*     * * SEND EMAIL/SMS TO RKCL ** */
    $rkcl_UserCode = ($_SERVER['HTTP_HOST'] = "localhost") ? '7504' : '4257';
    $response6 = $emp->getRKCLDetails($rkcl_UserCode);
    $row6 = mysqli_fetch_assoc($response6[2]);
    $response7 = $emp->sendEmailtoRKCLAccept2($row5['username'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType'], $row6);
}


if ($_action == "reject") {
    $_DataTable = [];
    $response2 = [];
    $_i = 0;
    $response = $emp->commitChangesReject($_POST['Code'], $_POST['fld_remarks_rkcl'], $_POST['Reason']);
    echo $response["0"];

    /*     * * SEND EMAIL TO ITGK ** */
    $response4 = $emp->getCenterDetails($_POST['Code']);
    $row = mysqli_fetch_assoc($response4[2]);
    $row["type"] = "rejected";
    $emp->sendSMSEmail($row);

    /*     * * SEND EMAIL/SMS TO SERVICE PROVIDER ** */
    $response5 = $emp->getSPDetails($row['fld_rspCode']);
    $row5 = mysqli_fetch_assoc($response5[2]);
    $response6 = $emp->sendEmailtoSPReject($row5['username'], $row5['email'], $row5['mobile'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType']);

    /*     * * SEND EMAIL/SMS TO RKCL ** */
    $rkcl_UserCode = ($_SERVER['HTTP_HOST'] = "localhost") ? '7504' : '4257';
    $response6 = $emp->getRKCLDetails($rkcl_UserCode);
    $row6 = mysqli_fetch_assoc($response6[2]);
    $response7 = $emp->sendEmailtoRKCLReject($row5['username'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType'], $row6);
}

if ($_action == "rejectbysp") {
    $_DataTable = [];
    $response2 = [];
    $_i = 0;
    $response = $emp->commitChangesRejectBySP($_POST['Code'], $_POST['Reason']);
    echo $response["0"];

    /*     * * SEND EMAIL TO ITGK ** */
    $response4 = $emp->getCenterDetails($_POST['Code']);
    $row = mysqli_fetch_assoc($response4[2]);
    $row["type"] = "rejected";
    $emp->sendSMSEmail($row);

    /*     * * SEND EMAIL/SMS TO SERVICE PROVIDER ** */
    $response5 = $emp->getSPDetails($row['fld_rspCode']);
    $row5 = mysqli_fetch_assoc($response5[2]);
    $response6 = $emp->sendEmailtoSPReject($row5['username'], $row5['email'], $row5['mobile'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType']);

    /*     * * SEND EMAIL/SMS TO RKCL ** */
    $rkcl_UserCode = ($_SERVER['HTTP_HOST'] = "localhost") ? '7504' : '4257';
    $response6 = $emp->getRKCLDetails($rkcl_UserCode);
    $row6 = mysqli_fetch_assoc($response6[2]);
    $response7 = $emp->sendEmailtoRKCLReject($row5['username'], $row['ITGK_Name'], $_POST['Code'], $_POST['requestType'], $row6);
}

if ($_action == "revoke") {
    $response4 = $emp->getCenterDetails($_POST['Code']);
    if ($response4[0] == "Success") {
        $row = mysqli_fetch_assoc($response4[2]);
        $row["type"] = "revoked";
        $emp->sendSMSEmail($row);
        $response = $emp->commitChangesRevoke($_POST['Code']);

        //echo "<pre>", print_r($response);

        echo $response["0"];
    }
}

if ($_action == "getCenterDetails") {
    $response4 = $emp->getCenterDetails($_POST['Code']);
    $row = mysqli_fetch_assoc($response4[2]);
    echo $row['User_MobileNo'];
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
    $key = ($_SERVER["HTTP_HOST"] == "localhost") ? "gtKFFx" : "X2ZPKM";
    $salt = ($_SERVER["HTTP_HOST"] == "localhost") ? "eCwWELxi" : "8IaBELXB";
    $wsUrl = ($_SERVER["HTTP_HOST"] == "localhost") ? "https://test.payu.in/merchant/postservice.php?form=2" : "https://info.payu.in/merchant/postservice?form=2";
    $command1 = "check_isDomestic";
    $var1 = $_POST['values'];

    $hash_str = $key . '|' . $command1 . '|' . $var1 . '|' . $salt;
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key, 'hash' => $hash, 'command' => $command1, 'var1' => $var1);

    $qs = http_build_query($r);


    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
    if (curl_errno($c)) {
        $c_error = curl_error($c);
        if (empty($c_error)) {
            $c_error = 'Some server error';
        }
        return array('curl_status' => 'FAILURE', 'error' => $c_error);
    }
    $out = trim($o);
    $arr = json_decode($out, true);


    $response = $arr;
    //echo "<pre>"; print_r($response); echo "</pre>";
    $_DataTable = array();
    $_i = 0;
    $_DataTable[$_i] = array("cardType" => $response['cardType'],
        "cardCategory" => $response['cardCategory']);
    echo json_encode($_DataTable);
}

if ($_action == "FinalValidateCard") {
    $key = ($_SERVER["HTTP_HOST"] == "localhost") ? "gtKFFx" : "X2ZPKM";
    $salt = ($_SERVER["HTTP_HOST"] == "localhost") ? "eCwWELxi" : "8IaBELXB";
    $wsUrl = ($_SERVER["HTTP_HOST"] == "localhost") ? "https://test.payu.in/merchant/postservice.php?form=2" : "https://info.payu.in/merchant/postservice?form=2";
    $command1 = "validateCardNumber";
    $var1 = $_POST['values'];

    $hash_str = $key . '|' . $command1 . '|' . $var1 . '|' . $salt;
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key, 'hash' => $hash, 'command' => $command1, 'var1' => $var1);

    $qs = http_build_query($r);


    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
    if (curl_errno($c)) {
        $c_error = curl_error($c);
        if (empty($c_error)) {
            $c_error = 'Some server error';
        }
        return array('curl_status' => 'FAILURE', 'error' => $c_error);
    }
    $out = trim($o);
    $arr = json_decode($out, true);


    $response = $arr;

    if ($response == 'Invalid') {
        echo "1";
    } else if ($response == 'valid') {
        echo "2";
    }
}

if ($_action == "checkTransactionStatus") {
    $arr = [];
    $arr["txnid"] = $_POST["txnid"];
    $response = $emp->getTransactionStatusByPayU($arr);
    echo $response["status"];
}

if ($_action == "generateInvoice") {
    $response = $emp->getInvoiceMasterDetails(base64_decode($_POST['Command']));
    $row = mysqli_fetch_assoc($response[2]);
    $response1 = $emp->generateInvoice($_POST, $row);
    echo $response1["0"];
}

if ($_action == "downloadGSTInvoice") {

    $path = $_SERVER['DOCUMENT_ROOT'] .'/upload/itgk_gst_invoice/';
//print_r($_POST);
    $learnerCode = base64_decode($_POST['fld_ref_no']);
    $file = $_POST['fld_ref_no']. '.fdf';
    $filePath = $path . $file;
    $allreadyfoundAkcRecipt = $path . $learnerCode . '_GST_Invoice.pdf';

    $response = $emp->getITGKInvoiceDetails(base64_decode($_POST['fld_ref_no']));
    // print_r($response);
    $co = mysqli_num_rows($response[2]);
    $res = '';
    if ($co) {
        // print_r($co);
        $_Row = array();
        $_Row1 = array();
        $_Row = mysqli_fetch_array($response[2], true);
        foreach ($_Row as $key => $value) {
            $_Row1[$key] = $value;
        }

//            $_Row1['rkcl_gstin'] = '08AAECR1253K1ZB';
//            $_Row1['rkcl_pan'] = 'AAECR1253K';
//            $_Row1['cgst'] = ceil(($_Row1['cg'] / $_Row1['service_charge']) * 100) . "%";
//            $_Row1['sgst'] = ceil(($_Row1['sg'] / $_Row1['service_charge']) * 100) . "%";
//            $_Row1['amount_txt'] = 'Rs. ' . ucwords($emp->getIndianCurrency($_Row1['amount_figures'])) . ' Only';
         $_Row1['rkcl_gstin'] = '08AAECR1253K1ZB';
            $_Row1['rkcl_pan'] = 'AAECR1253K';
            $basic = ($_Row1['amount_figures'])*100/118 ;
            $_Row1['service_charge'] =  number_format((float)$basic, 2, '.', '');
            $foo = ($basic)*9/100;
            $_Row1['cg'] = number_format((float)$foo, 2, '.', '');
            $_Row1['sg'] = number_format((float)$foo, 2, '.', '');
            //$_Row1['cgst'] = ceil(($_Row1['cg'] / $_Row1['service_charge']) * 100) . "%";
           // $_Row1['sgst'] = ceil(($_Row1['sg'] / $_Row1['service_charge']) * 100) . "%";
             $_Row1['cgst'] = "9%";
            $_Row1['sgst'] = "9%";
            $_Row1['amount_txt'] = 'Rs. ' . ucwords($emp->getIndianCurrency($_Row1['amount_figures'])) . ' Only';

            $fdfContent = '%FDF-1.2
			%âã�?Ó
			1 0 obj 
			<<
			/FDF 
			<<
			/Fields [';
			foreach ($_Row1 as $key => $val) {
				$fdfContent .= '
					<<
					/V (' . $val . ')
					/T (' . $key . ')
					>> 
					';
			}
			$fdfContent .= ']
			>>
			>>
			endobj 
			trailer
			<<
			/Root 1 0 R
			>>
			%%EOF';
            
        file_put_contents($filePath, $fdfContent);

        $fdfFilePath = $filePath;
        $fdfPath = str_replace('/', '//', $fdfFilePath);
        $resultFile = '';
     
        $defaulPermissionLetterFilePath = $_SERVER['DOCUMENT_ROOT'].'/upload/itgk_gst_invoice/inputForm.pdf'; //die;

        if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
            $defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);
            $resultFile = $learnerCode . '_GST_Invoice.pdf';

            $newURL = $path . $resultFile;

            //$resultPath = str_replace('/', '//', getPermitionLetterFilePath($learnerCode));
            $resultPath = str_replace('/', '//', $newURL);
            $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            // $pdftkPath = ($_SERVER['HTTP_HOST'] == "click.rkcl.in") ? '"C://inetpub//vhosts//rkcl.in//click.rkcl.in//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            //$pdftkPath = ($_SERVER['HTTP_HOST']=="10.1.1.20") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            $command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  '. $resultPath . ' flatten';
            //echo $command;
            
            exec($command);
            //$command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten 2>&1';
            //exec($command,$output);
              //echo "<pre>";
            //print_r($output);
           
            //unlink($fdfFilePath);
            
            // $filepath5 = 'upload/itgk_gst_invoice/' . $resultFile;
            // echo $filepath5;
            $filepath5 = 'common/showpdfftp.php?src=itgk_gst_invoice/' . $resultFile;
            // unlink($fdfFilePath);
            $directoy = '/itgk_gst_invoice/';
            $file = $resultFile;
            $response_ftp = ftpUploadFile($directoy, $file, $newURL);
            if(trim($response_ftp) == "SuccessfullyUploaded"){
                
                echo $filepath5;
            } 

            $res = "DONE";
        }
    } else {
        echo "Not record found";
    }
}

if ($_action == "uploadGeoTaggedImage") {

    $responseArray = [];

    if ($_SERVER['HTTP_HOST'] == "localhost") {
        $imageFolder = $_SERVER['DOCUMENT_ROOT'] . "/myrkcl/upload/LatLong/";
    } else {
        $imageFolder = $_SERVER['DOCUMENT_ROOT'] . "/upload/LatLong/";
    }

    $arr = $_FILES;
    reset($_FILES);
    $temp = current($_FILES);

    if (is_uploaded_file($temp['tmp_name'])) {

        if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
            echo "Invalid file name";
        }

        if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("jpg", "jpeg"))) {
            echo "Invalid extension";
        }

        $string = "IMG_" . $_SESSION['User_Code'] . "_" . date("YmdHis");
        $fileName = $string . "_" . $temp['name'];
        $filetowrite = $imageFolder . $_SESSION['User_Code'] . '/' . $fileName;


        if (!file_exists($imageFolder . $_SESSION['User_Code'])) {
            mkdir($imageFolder . $_SESSION['User_Code'], 0777, true);
        }

        if (move_uploaded_file($temp['tmp_name'], $filetowrite)) {

            $imgLocation = [];

            $imgLocation = $emp->get_image_location($filetowrite);

            if (!empty($imgLocation)) {

                $getLatLongRS = $emp->getLatitudeLongitude($_SESSION["User_Code"]);
                $_Row = mysqli_fetch_assoc($getLatLongRS[2]);

                $response2 = $emp->fetchAddress($imgLocation);

                $imgLat = $imgLocation['latitude'];
                $imgLng = $imgLocation['longitude'];

                $imgLocation["fld_UserCode"] = $_SESSION["User_Code"];

                $imgLocation["fld_oldCordinates"] = $_Row["Organization_lat"] . ":" . $_Row["Organization_long"];
                $imgLocation["fld_newCordinates"] = $imgLat . ":" . $imgLng;
                $imgLocation["fld_formattedAddress"] = $response2;
                $imgLocation["fld_UserRoll"] = $_SESSION["User_UserRoll"];

                $response1 = $emp->updateLatitudeLongitude($imgLocation);

                if ($response1["0"] == "Successfully Updated") {
                    $responseArray["location"] = $response2;
                    $responseArray["status"] = "Successfully Updated";
                }
            } else {
                $responseArray["status"] = "Invalid Image, could not find geo-tags";
            }
        } else {
            $responseArray["status"] = "File upload error, please try again";
        }
    } else {
        $responseArray["status"] = "File not found";
    }
    echo json_encode($responseArray);
}

if ($_action == "addTransaction") {
    $response = $emp->generateID();
    $postData['Pay_Tran_ITGK'] = $_SESSION['User_LoginId'];
    $postData['Pay_Tran_Fname'] = $_SESSION['Organization_Name'];
    $postData['Pay_Tran_RKCL_Trnid'] = $response . '_RKCLtranid';
    $postData['Pay_Tran_AdmissionArray'] = $_SESSION['User_LoginId'];
    $postData['Pay_Tran_LCount'] = '1';
    $postData['Pay_Tran_Amount'] = $_POST["fld_amount"];
    $postData['Pay_Tran_Status'] = 'PaymentInProcess';
    $postData['Pay_Tran_ProdInfo'] = $_POST["fld_productInfo"];
    $postData['Pay_Tran_PG_Trnid'] = $_POST["txnid"];
    $postData['paymentgateway'] = 'Payu';
    $response1 = $emp->addTransaction($postData);
    echo $response1["0"];
}

if ($_action == "updateTransaction") {
    $postData['Pay_Tran_PG_Trnid'] = $_POST['txnid'];
    $postData['Pay_Tran_Status'] = 'PaymentDone';
    $response1 = $emp->updateTransaction($postData);
    echo $response1["0"];
}

if ($_action == "delgeninvoice") {
    $name = trim($_POST['values']);
    $image_url = $_SERVER['DOCUMENT_ROOT'] . '/' . $name;
    //print_r($image_url);
    if (file_exists($image_url)) {
        unlink($image_url);
    } else {
        die('file does not exist');
    }
}