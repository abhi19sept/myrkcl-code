<?php

/*
 * @author Abhi

 */
include 'commonFunction.php';
require 'BAL/clsLIVRrpt.php';
require 'DAL/upload_ftp_doc.php';
$response = array();
$emp = new clsLIVRrpt();

if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_Datatable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("LearnerCode" => $_Row['Admission_LearnerCode'],
            "LearnerName" => $_Row['Admission_Name'],
            "FatherName" => $_Row['Admission_Fname'],
            "LearnerPhoto" => $_Row['Admission_Photo'],
            "LearnerSign" => $_Row['Admission_Sign'],
            //        "Mobile" => $_Row['UserProfile_Mobile'],
            "CenterCode" => $_Row['Admission_ITGK_Code'],
            "AdmissionDate" => $_Row['Admission_Date'],
            "CourseName" => $_Row['Course_Name'],
            "CenterName" => $_Row['Organization_Name'],
            "Batch" => $_Row['Batch_Name'],
            "BatchStartDate" => $_Row['Batch_StartDate'],
            "Medium" => $_Row['Admission_Medium'],
            "District" => $_Row['District_Name'],
            "CenterCoordinator" => $_Row['Staff_Name'],
            "CourseFee" => $_Row['Admission_Fee'],
            "DOB" => $_Row['Admission_DOB']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}

if ($_action == "PRINT") {


    $response = $emp->GetDatabyCode($_actionvalue);
    $_row = mysqli_fetch_array($response[2]);

    $html1 = '<center><legend>Learner Information Verification Report (ITGK Copy)</center></legend>

  	<p>Date & Time: 25/03/2016                                                                                                                                   </p>			  
         <div class="col-md-9 col-lg-9" style="border:1px solid #428bca;">
								  	<fieldset style="width: 100%">
								
         <table class="table table-user-information">
         <tbody>
		
         <tr>
        <td class="col-md-2 col-lg-2">ITGK Code :</td>
        
		
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_ITGK_Code'] . '</td>
		
		
		 </tr>
		
		
		  <tr>
        
		 </tr>
		 <tr>
		 <td class="col-md-2 col-lg-2">ITGK Name:</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Organization_Name'] . '</td>
		
	
         </tr>
		
		 <tr>
	 <td class="col-md-2 col-lg-2">Learner Id :</td>
         <td class="col-md-3 col-lg-3">' . $_row['Admission_LearnerCode'] . '</td>
		
		 </tr>
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Form No. :</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Code'] . '</td>
		
	
		 </tr>
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Learner Name :</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Name'] . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Father/Husband Name:</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Fname'] . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Date of Birth:</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_DOB'] . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Mobile No:</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Mobile'] . '</td>
		
	
		 </tr>
		
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Email: </td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Email'] . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Course Name: </td>
		 <td class="col-md-3 col-lg-3">' . $_row['Course_Name'] . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Batch : </td>
		 <td class="col-md-3 col-lg-3">' . $_row['Batch_Name'] . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Medium : </td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Medium'] . '</td>
		
	
		 </tr>
		
		
			 <tr>
		 <td class="col-md-2 col-lg-2">Course Fee (Rs.) : </td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Fee'] . '/-</td>
		
	
		 </tr>
		
		
		
         </tbody>
       
       
         </table>
		 	</fieldset >
            
		
		  </div>
		 
		 
		 
		 
		  <div align="center" class="col-md-3 col-lg-3" style="height:525px;border:1px solid #428bca;" >
		
		 <p>Profile Picture </p >
		
		 <img class="img-squre img-responsive" width="80"  style="padding:2px;height:107px;border:1px solid #428bca;margin-top:10px;" src="upload/admission_photo/' . $_row['Admission_Photo'] . '"/>
		
	
		 <p   style="margin-top:10px;">Sign </p>
		
		 <img class="img-squre img-responsive"  width="80"  style="margin-top:10px;height:35px;" src="upload/admission_sign/' . $_row['Admission_Sign'] . '"/>
		
		 </div>
		 
		 
		   <div align="center" class="col-md-12 col-lg-12"  >
		  	 <p align="left">I hereby declare that I have accepted filled up application form from the learner. I have accepted all necessary documents (ID proof, DOB Proof) from the learner and verified the same before entering into the system. </p >
		   </div>
                   	  	 <div class="col-md-12 col-lg-12"><p align="right">IT-GK Seal & Signature </p></div >
	<br>
        <br>
        <hr>';
    $html1 .= '<center><legend>Learner Information Verification Report (Learner Copy)</center></legend>

  	<p>Date & Time: 25/03/2016                                                                                                                                   </p>			  
         <div class="col-md-9 col-lg-9" style="border:1px solid #428bca;">
								  	<fieldset style="width: 100%">
								
         <table class="table table-user-information">
         <tbody>
		
         <tr>
        <td class="col-md-2 col-lg-2">ITGK Code :</td>
        
		
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_ITGK_Code'] . '</td>
		
		
		 </tr>
		
		
		  <tr>
        
		 </tr>
		 <tr>
		 <td class="col-md-2 col-lg-2">ITGK Name:</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Organization_Name'] . '</td>
		
	
         </tr>
		
		 <tr>
	 <td class="col-md-2 col-lg-2">Learner Id :</td>
         <td class="col-md-3 col-lg-3">' . $_row['Admission_LearnerCode'] . '</td>
		
		 </tr>
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Form No. :</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Code'] . '</td>
		
	
		 </tr>
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Learner Name :</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Name'] . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Father/Husband Name:</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Fname'] . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Date of Birth:</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_DOB'] . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Mobile No:</td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Mobile'] . '</td>
		
	
		 </tr>
		
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Email: </td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Email'] . '</td>
		
	
		 </tr>
		
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Course Name: </td>
		 <td class="col-md-3 col-lg-3">' . $_row['Course_Name'] . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Batch : </td>
		 <td class="col-md-3 col-lg-3">' . $_row['Batch_Name'] . '</td>
		
	
		 </tr>
		
		
		
		 <tr>
		 <td class="col-md-2 col-lg-2">Medium : </td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Medium'] . '</td>
		
	
		 </tr>
		
		
			 <tr>
		 <td class="col-md-2 col-lg-2">Course Fee (Rs.) : </td>
		 <td class="col-md-3 col-lg-3">' . $_row['Admission_Fee'] . '/-</td>
		
	
		 </tr>
		
		
		
         </tbody>
       
       
         </table>
		 	</fieldset >
            
		
		  </div>
		 
		 
		 
		 
		  <div align="center" class="col-md-3 col-lg-3" style="height:525px;border:1px solid #428bca;" >
		
		 <p>Profile Picture </p >
		
		 <img class="img-squre img-responsive" width="80"  style="padding:2px;height:107px;border:1px solid #428bca;margin-top:10px;" src="upload/admission_photo/' . $_row['Admission_Photo'] . '"/>
		
	
		 <p   style="margin-top:10px;">Sign </p>
		
		 <img class="img-squre img-responsive"  width="80"  style="margin-top:10px;height:35px;" src="upload/admission_sign/' . $_row['Admission_Sign'] . '"/>
		
		 </div>
		 
		 
		   <div align="center" class="col-md-12 col-lg-12"  >
		  	 <p>I hereby declare that I have provided filled up application form and all necessary documents to ITGK and information given above are accurate and correct.  </p >
		   </div>
                   <div class="col-md-12 col-lg-12" ><p align="right">Learner Signature </p></div >';
    echo $html1;
}

function convert_number_to_words($number) {

    $hyphen = ' ';
    $conjunction = ' and ';
    $separator = ', ';
    $negative = 'negative ';
    $decimal = ' point ';
    $dictionary = array(
        0 => 'Zero',
        1 => 'One',
        2 => 'Two',
        3 => 'Three',
        4 => 'Four',
        5 => 'Five',
        6 => 'Six',
        7 => 'Seven',
        8 => 'Eight',
        9 => 'Nine',
        10 => 'Ten',
        11 => 'Eleven',
        12 => 'Twelve',
        13 => 'Thirteen',
        14 => 'Fourteen',
        15 => 'Fifteen',
        16 => 'Sixteen',
        17 => 'Seventeen',
        18 => 'Eighteen',
        19 => 'Nineteen',
        20 => 'Twenty',
        30 => 'Thirty',
        40 => 'Fourty',
        50 => 'Fifty',
        60 => 'Sixty',
        70 => 'Seventy',
        80 => 'Eighty',
        90 => 'Ninety',
        100 => 'Hundred',
        1000 => 'Thousand',
        1000000 => 'Million',
        1000000000 => 'Billion',
        1000000000000 => 'Trillion',
        1000000000000000 => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens = ((int) ($number / 10)) * 10;
            $units = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}

function getdata($code) {
    $response = $emp->GetDatabyCode($_actionvalue);

    $_Datatable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("LearnerCode" => $_Row['Admission_LearnerCode'],
            "LearnerName" => $_Row['Admission_Name'],
            "FatherName" => $_Row['Admission_Fname'],
            "LearnerPhoto" => $_Row['Admission_Photo'],
            "LearnerSign" => $_Row['Admission_Sign'],
            //        "Mobile" => $_Row['UserProfile_Mobile'],
            "CenterCode" => $_Row['Admission_ITGK_Code'],
            "AdmissionDate" => $_Row['Admission_Date'],
            "CourseName" => $_Row['Course_Name'],
            "CenterName" => $_Row['Organization_Name'],
            "Batch" => $_Row['Batch_Name'],
            "BatchStartDate" => $_Row['Batch_StartDate'],
            "Medium" => $_Row['Admission_Medium'],
            "District" => $_Row['District_Name'],
            "CenterCoordinator" => $_Row['Staff_Name'],
            "CourseFee" => $_Row['Admission_Fee'],
            "DOB" => $_Row['Admission_DOB']);
        $_i = $_i + 1;
    }
    return $_Datatable;
}

if ($_action == "SHOWALL") {

    $response = $emp->GetAll($_POST['batch'], $_POST['course']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th>Father/Husband Name</th>";
    echo "<th>D.O.B</th>";
    // echo "<th>Photo</th>";
    // echo "<th>Sign</th>";
    echo "<th>Download LIVR Report</th>";
    //echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	$_LearnerBatchNameFTP="";
	$_Course = $_POST['course'];
                if ($_Course == 5) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP;                    

                } elseif ($_Course == 4) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

                }
                elseif ($_Course == 3) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }               
                
                elseif ($_Course == 23) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

                }   

                 elseif ($_Course == 25) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

                }
                elseif ($_Course == 28) {


                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

                }
                elseif ($_Course == 29) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

                }
                elseif ($_Course == 30) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

                }
                elseif ($_Course == 31) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

                }
                elseif ($_Course == 32) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

                }

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        echo "<td>" . $_Row['Admission_DOB'] . "</td>";
$_LearnerBatchNameFTP2 = str_replace(' ', '_', $_Row['Batch_Name'].$_LearnerBatchNameFTP);

        // if ($_Row['Admission_Photo'] != "") {
        //     $image = $_Row['Admission_Photo'];
        //     echo "<td align='center'>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
        // } else {
        //     echo "<td align='center'>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
        // }
        // if ($_Row['Admission_Sign'] != "") {
        //     $sign = $_Row['Admission_Sign'];
        //     echo "<td align='center'>" . '<img alt="No Image Found" width="80" height="35" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
        // } else {
        //     echo "<td align='center'>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
        // }

        echo "<td align='center'><a href='printLIVRpdf.php?code=" . $_Row['Admission_Code'] . "&action=pdfprint&ftpbatch=" . $_LearnerBatchNameFTP2 . "' target='_blank'>"
        . "<img src='images/dwnpdf.png' alt='Edit' width='35px' height='35px'  align='center'/></a> </td> ";



        // if ($_Row['Admission_Payment_Status'] == '0') {
        // //    echo "<td><a href='frmLIVRrpt.php?code=" . $_Row['Admission_Code'] . "&Mode=Delete&batchcode=" . $_Row['Admission_Batch'] . "'>"
        // //    . "<img src='images/deleteicon.png' alt='Delete' width='25px' height='25px'/></a> "
        // //     . "</td>";
        //  } else {
        //     echo "<td>"
        //     . "<img src='images/editicon.png' alt='Edit' width='30px' height='25px' />"
        //      . "</td>";
        //  }
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "DELETE") {
    //print_r($_POST);
    $value = $_POST['values'];
    $batch = $_POST['batchcode'];

    $response = $emp->DeleteRecord($value, $batch);

    echo $response[0];
}

if ($_action == "SHOWALLPrintRecp") {
    $response = $emp->GetAllPrintRecp($_POST['batch'], $_POST['course']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th>Father/Husband Name</th>";
    echo "<th>D.O.B</th>";

    echo "<th>Download Print Receipt</th>";
    //echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        echo "<td>" . $_Row['Admission_DOB'] . "</td>";

        // if ($_Row['Admission_Photo'] != "") {
        //     $image = $_Row['Admission_Photo'];
        //     echo "<td align='center'>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
        // } else {
        //     echo "<td align='center'>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
        // }
        // if ($_Row['Admission_Sign'] != "") {
        //     $sign = $_Row['Admission_Sign'];
        //     echo "<td align='center'>" . '<img alt="No Image Found" width="80" height="35" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
        // } else {
        //     echo "<td align='center'>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
        // }
        if ($_POST['batch'] >= 136) {
            if ($_POST['course'] == 3 || $_POST['course'] == 20) {
                echo "<td>Invoice Not Found</td>";
            } else {
                echo "<td class='org'><button type='button' data-toggle='modal' data-target='#approvalLetter' class='btn btn-primary approvalLetter' id='" . $_Row['Admission_Code'] . "' >Download Invoice</button></td>";
            }
        } elseif ($_POST['batch'] <= 135 && $_POST['batch'] >= 90) {
            echo "<td><a href=javascript:window.open('PrintReceiptpdf.php?code=" . $_Row['Admission_Code'] . "&Mode=printreceipt');>"
            . "<img src='images/print_printer.png' alt='Edit' width='25px' height='25px'  align='center'/></a> </td> ";
        } else {
            echo "<td>Invoice Not Found</td>";
        }


        //  echo "<td class='org'><button type='button' data-toggle='modal' data-target='#ITGKDetailBox' class='ITGKDetailBox' id='".$_Row['Admission_Code']."' >Print</button></td>";

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "DwnldPrintRecp") {
    $path = '../upload/DownloadPrintRecpt/';
    $learnerCode = $_POST['lid'];
    $file = $_POST['lid'] . '.fdf';
    $filePath = $path . $file;



    $response = $emp->GetAllPrintRecpDwnld($_POST['lid']);
    $co = mysqli_num_rows($response[2]);
    $res = '';
    if ($co) {
        $_Row1 = mysqli_fetch_array($response[2], true);
        if ($_Row1['Admission_Course'] == '21') {
            $allreadyfoundAkcRecipt = $path . $learnerCode . '_RedHatInvoice.pdf';
        }elseif ($_Row1['Admission_Course'] == '25') {
        	$allreadyfoundAkcRecipt = $path . $learnerCode . '_Admission_Invoice_RSCEE.pdf';
        } else {
            $allreadyfoundAkcRecipt = $path . $learnerCode . '_Admission_Invoice.pdf';
        }
        //echo "<pre>";print_r($_Row1);die;
        //$_DataTable = array();
        //$_Row1["todate"] = date("d-m-Y");
        $_Row1["invoice_no"] = "ADM/" . $_Row1['invoice_no'];
        $_Row1["invoice_date"] = date("d-m-Y", strtotime($_Row1['addtime']));
        $_Row1["Lname"] = strtoupper($_Row1['Admission_Name']);
        $_Row1['Fname'] = strtoupper($_Row1['Admission_Fname']);
        $_Row1["Lcode"] = $_Row1['Admission_LearnerCode'];
        $_Row1["Laddress"] = strtoupper($_Row1['District_Name']);
        $_Row1["Ldob"] = date("d-m-Y", strtotime($_Row1['Admission_DOB']));
        $_Row1["Lmobile"] = $_Row1['Admission_Mobile'];
        $_Row1["Lcourse"] = $_Row1['Course_Name'] . " Course";

        if ($_Row1['Admission_Course'] == '21') {
            $allreadyfoundAkcRecipt = $path . $learnerCode . '_RedHatInvoice.pdf';
            $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'];
            $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
            $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
            $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
            $_Row1["totalrkcl"] = $_Row1["Gst_Invoce_TutionFee"] . ".00";
            $_Row1["amounttxt"] = $emp->convert_number_to_words($_Row1['totalrkcl']) . " Only";

            $_Row1["Lbatch"] = $_Row1['Batch_Name'];
            $_Row1["Litgk"] = $_Row1['Admission_ITGK_Code'];
            $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
            $_Row1["cgstvalue"] = "9";
            $_Row1["sgstvalue"] = "9";
        }

               if ($_Row1['Admission_Course'] == '5') {

            $_Row1["customcourse"] = "RS-CFA";
            $_Row1["coursedec"] = $_Row1['Course_Name'] . " (Financial Accounting)";
            $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'];
            $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
            $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
            $_Row1["totalrkcl"] = ($_Row1["Gst_Invoce_BaseFee"] + $_Row1["cgst"] + $_Row1["cgst"]);
            //$_Row1["TuitionFee"] = '2200.00';
            $_Row1["TuitionFee"] = $_Row1['Gst_Invoce_TutionFee'] . ".00";
            $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
            $_Row1["Rkclshare2"] = '1000.00';
            $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'] . ".00";
            $_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
            $_Row1["customtext4"] = "and RKCL ** to confirm the admission.";
            $_Row1["customtext6"] = "/ 2 months";
            $_Row1["customtext11"] = "* Taxes on Tuition Fee, if applicable, shall be borne by ITGK. ";
            $_Row1["customtext10"] = "Learning, facilitation and Course Material **";
            $_Row1["amounttxt"] = $emp->convert_number_to_words(1000) . " Only";
            $_Row1["CourseDuration"] = $_Row1['Course_Duration'] . $_Row1['Course_DurationType'];
            $_Row1["Lbatch"] = $_Row1['Batch_Name'];
            $_Row1["Litgk"] = $_Row1['Admission_ITGK_Code'];
            $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
            $_Row1["cgstvalue"] = "9";
            $_Row1["sgstvalue"] = "9";
        }
        if ($_Row1['Admission_Course'] == '1' || $_Row1['Admission_Course'] == '4') {
            $_Row1["customtext1"] = "Total";
            $_Row1["customtext2"] = "Less - Tax Component borne by RKCL";
            $_Row1["customtext3"] = "Enrollment, Examination and Certification fee ***";
            $_Row1["customtext4"] = "RKCL ** and VMOU*** to confirm the admission.";
//            $_Row1["customtext5"] = "certificate, you should visit https://myrkcl.com and file your claim online for reimbursement by yourself or through your ITGK. No physical documents will be entertained by RKCL in this regard.  ";
            $_Row1["customtext6"] = "/ 3 months";
//            $_Row1["customtext7"] = "If you are a ";
//            $_Row1["customtext8"] = "Govt. Employee and eligible for fee reimbursement ";
//            $_Row1["customtext9"] = "then after getting your VMOU ";
            $_Row1["customtext5"] = "सरकारी कर्मचारियों हेतु शुल्क पुनर्भरण  मय प्रोत्साहन राशी (वर्तमान एवं लंबित) का भुगतान नियमानुसार राज्य कर्मी के पैतृक विभाग (मुख्यालय संबंधित विभागाध्यक्ष) के स्तर से किया जायेगा | अतः सरकारी कर्मचारी आवेदन करने एवं शुल्क पुनर्भरण हेतु अपने से सम्बंधित विभाग में ही संपर्क करें | ";
            $_Row1["customtext10"] = "Internal assessment, learning, facilitation and Course Material **";
            $_Row1["customcourse"] = "RS-CIT";
            $_Row1["coursedec"] = $_Row1['Course_Name'] . " (Digital Literacy Course)";
            $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
            $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
            $_Row1["TuitionFee"] = $_Row1['Gst_Invoce_TutionFee'] . ".00";
            $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
            $_Row1["Enrollmentfee"] = $_Row1['Gst_Invoce_VMOU'] . '.00';
            $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'] . '.00';
            $_Row1["Rkclshare2"] = $_Row1['Gst_Invoce_BaseFee'] . '.00';
            $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'] . '.00';
            $_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
            //$_Row1["totalrkclplus"] = ($_Row1["Gst_Invoce_BaseFee"] + $_Row1["cgst"] + $_Row1["cgst"]) . "0";
            $_Row1["totalrkclplus"] = ($_Row1["Gst_Invoce_BaseFee"] + $_Row1["cgst"] + $_Row1["cgst"]) . "0";
            //$_Row1["totaltaxless"] = ($_Row1["cgst"] + $_Row1["cgst"]) . "0";
            $_Row1["totaltaxless"] = ($_Row1["cgst"] + $_Row1["cgst"]) . "0";
            $_Row1["totalrkcl"] = ($_Row1["totalrkclplus"] - $_Row1["totaltaxless"]) . ".00";
            $_Row1["amounttxt"] = $emp->convert_number_to_words($_Row1['Gst_Invoce_BaseFee']) . " Only";
            $_Row1["CourseDuration"] = $_Row1['Course_Duration'] . $_Row1['Course_DurationType'];
            $_Row1["Lbatch"] = $_Row1['Batch_Name'];
            $_Row1["Litgk"] = $_Row1['Admission_ITGK_Code'];
            $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
            $_Row1["cgstvalue"] = "9";
            $_Row1["sgstvalue"] = "9";
        }
        if ($_Row1['Admission_Course'] == '23') {


            $_Row1["coursedec"] = $_Row1['Course_Name'] . " (Digital Literacy Course)";
            $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
            $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
            $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
            $_Row1["Enrollmentfee"] = $_Row1['Gst_Invoce_VMOU'] . '.00';
            $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'];
            $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'] . '.00';
           	$_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
            $_Row1["totalrkclplus"] = ($_Row1["Gst_Invoce_BaseFee"] + $_Row1["cgst"] + $_Row1["cgst"]). '0';

            $_Row1["totaltaxless"] = ($_Row1["cgst"] + $_Row1["cgst"]). '0';

            $_Row1["amounttxt"] = $emp->convert_number_to_words($_Row1['Gst_Invoce_BaseFee']) . " Only";
            $_Row1["CourseDuration"] = $_Row1['Course_Duration'] . $_Row1['Course_DurationType'];
            $_Row1["Lbatch"] = $_Row1['Batch_Name'];
            $_Row1["Litgk"] = $_Row1['Admission_ITGK_Code'];
            $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
            $_Row1["cgstvalue"] = "9";
            $_Row1["sgstvalue"] = "9";
        }
      	if ($_Row1['Admission_Course'] == '25') {

        	$allreadyfoundAkcRecipt = $path . $learnerCode . '_Admission_Invoice_RSCEE.pdf';
            $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
            $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
            $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
            $_Row1["Enrollmentfee"] = '1400.00';
            $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'];
            $_Row1["Tutionfee"] = $_Row1['Gst_Invoce_TutionFee']. '.00';
            $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'] . '.00';
           	$_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
            $_Row1["totalrkclplus"] = ($_Row1["Gst_Invoce_BaseFee"] + $_Row1["cgst"] + $_Row1["cgst"]);

            $_Row1["totaltaxless"] = ($_Row1["cgst"] + $_Row1["cgst"]). '0';

            $_Row1["amounttxt"] = $emp->convert_number_to_words($_Row1['Gst_Invoce_TutionFee']) . " Only";
            $_Row1["CourseDuration"] = $_Row1['Course_Duration'] . $_Row1['Course_DurationType'];
            $_Row1["Lbatch"] = $_Row1['Batch_Name'];
            $_Row1["Litgk"] = $_Row1['Admission_ITGK_Code'];
            $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
            $_Row1["cgstvalue"] = "9";
            $_Row1["sgstvalue"] = "9";
        }




        $fdfContent = '%FDF-1.2
			%Ã¢Ã£Ã?Ã“
			1 0 obj 
			<<
			/FDF 
			<<
			/Fields [';
        foreach ($_Row1 as $key => $val) {
            $fdfContent .= '
					<<
					/V (' . $val . ')
					/T (' . $key . ')
					>> 
					';
        }
        $fdfContent .= ']
			>>
			>>
			endobj 
			trailer
			<<
			/Root 1 0 R
			>>
			%%EOF';
        file_put_contents($filePath, $fdfContent);

        $fdfFilePath = $filePath;
        $fdfPath = str_replace('/', '//', $fdfFilePath);
        $resultFile = '';
        //$defaulPermissionLetterFilePath = getPermissionLetterPath() . '_ITGK_Approval_Letter.pdf';
        // echo "sunil";

        if ($_Row1['Admission_Course'] == '21') {
            $defaulPermissionLetterFilePath = '../upload/DownloadPrintRecpt/_RedHatInvoice.pdf'; //die;
        } elseif($_Row1['Admission_Course'] == '23')  {
            $defaulPermissionLetterFilePath = '../upload/DownloadPrintRecpt/_Admission_Invoice_CLICK.pdf'; //die;
        
        } elseif($_Row1['Admission_Course'] == '25')  {
            $defaulPermissionLetterFilePath = '../upload/DownloadPrintRecpt/_Admission_Invoice_RSCEE.pdf'; //die;
        } else {
            $defaulPermissionLetterFilePath = '../upload/DownloadPrintRecpt/_Admission_Invoice.pdf'; //die;
        }


        if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
            $defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);
            if ($_Row1['Admission_Course'] == '21') {

                $resultFile = $learnerCode . '_RedHatInvoice.pdf';

            }elseif ($_Row1['Admission_Course'] == '23') {
                $resultFile = $learnerCode . '_Admission_Invoice_CLICK.pdf';
            }elseif ($_Row1['Admission_Course'] == '25') {
                $resultFile = $learnerCode . '_Admission_Invoice_RSCEE.pdf';
            } 
            else {
                $resultFile = $learnerCode . '_Admission_Invoice.pdf';
            }



            $newURL = $path . $resultFile;

            //$resultPath = str_replace('/', '//', getPermitionLetterFilePath($learnerCode));
            $resultPath = str_replace('/', '//', $newURL);
            $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            // $pdftkPath = ($_SERVER['HTTP_HOST'] == "click.rkcl.in") ? '"C://inetpub//vhosts//rkcl.in//click.rkcl.in//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            //$pdftkPath = ($_SERVER['HTTP_HOST']=="10.1.1.20") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';

            $command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
            exec($command);
      //       if ($_Row1['Admission_Course'] != '21') {
      //           $emp->addPhotoInPDF($newURL, $_Row1['Admission_Course']);
				  // if ($_Row1['Admission_Course'] != '5') {
      //                             $emp->addNotePhotoInPDF($newURL, $_Row1['Admission_Course']);
      //                           }
      //       }
            if ($_Row1['Admission_Course'] == '1' || $_Row1['Admission_Course'] == '4' || $_Row1['Admission_Course'] == '5') {
                $emp->addPhotoInPDF($newURL, $_Row1['Admission_Course']);
            }
			
			  if ($_Row1['Admission_Course'] == '1' || $_Row1['Admission_Course'] == '4') {
                $emp->addNotePhotoInPDF($newURL, $_Row1['Admission_Course']);
            }

			
            unlink($fdfFilePath);
            // $filepath5 = 'upload/DownloadPrintRecpt/' . $resultFile;
            // echo $filepath5;

			$filepath5 = 'common/showpdfftp.php?src=DownloadPrintRecpt/' . $resultFile;

			$directoy = '/DownloadPrintRecpt/';
			$file = $resultFile;
			$response_ftp = ftpUploadFile($directoy, $file, $newURL);
            if(trim($response_ftp) == "SuccessfullyUploaded"){
            	
            	echo $filepath5;
            }
            else{
            	echo "error";
            }

            $res = "DONE";
        }
    } else {
        echo "";
    }

    function getPermissionLetterPath() {
        $path = '../upload/DownloadPrintRecpt/';
        makeDir($path);

        return $path;
    }

    function getPermitionLetterFilePath($code) {
        $resultFile = $code . '_Admission_Invoice.pdf';
        // $resultPath = getPermissionLetterPath() . $resultFile;
        $resultPath = '../upload/DownloadPrintRecpt/' . $resultFile;

        return $resultPath;
    }

    function makeDir($path) {
        return is_dir($path) || mkdir($path);
    }

}

if ($_action == "delgeninvoice") {

    $path = $_SERVER['DOCUMENT_ROOT'] . '/' . $_POST['values'];
    unlink($path);
}