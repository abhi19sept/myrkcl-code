<?php

/*
 * Created by Mayank
cfWcdItgkApprovedCount
 */

include './commonFunction.php';
require 'BAL/clsWcdItgkApprovedCount.php';

$response = array();
$emp = new clsWcdItgkApprovedCount();

if ($_action == "GETDATA") {    
	if($_POST['course'] !=''){
		if($_POST['batch'] !='') {		  
		   $response = $emp->GetAllCount($_POST['course'],$_POST['batch']);
			  
			   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
				echo "<th>District Name</th>";
				echo "<th>IT-GK Code</th>";
				echo "<th>Total Applicant</th>";
				echo "<th>Approved Applicant</th>";
				echo "<th>Rejected Applicant</th>";				
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;
				$_Total = 0;				
				$_Approve = 0;				
				$_Reject = 0;				
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr class='odd gradeX'>";
					echo "<td>" . $_Count . "</td>";						
					echo "<td>" . $_Row['District_Name'] . "</td>";
					echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";
					echo "<td>" . $_Row['total'] . "</td>";
					echo "<td>" . $_Row['approved'] . "</td>";
					echo "<td>" . $_Row['reject'] . "</td>";
					
					echo "</tr>";
						$_Total = $_Total + $_Row['total'];
						$_Approve = $_Approve + $_Row['approved'];
						$_Reject = $_Reject + $_Row['reject'];
					$_Count++;				
				}
				echo "</tbody>";
				echo "<tfoot>";
						echo "<tr>";
						echo "<th >  </th>";
						echo "<th >  </th>";
						echo "<th >Total Count </th>";
						echo "<th>";
						echo "$_Total";
						echo "</th>";
						echo "<th>";
						echo "$_Approve";
						echo "</th>";
						echo "<th>";
						echo "$_Reject";
						echo "</th>";
						echo "</tr>";
						echo "</tfoot>";
				 echo "</table>";
				 echo "</div>";			
		}
			else {
			  echo "1";
			}
	}
	  else {
		  echo "2";
	  }   
}


if ($_action == "FillAdmissionCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}