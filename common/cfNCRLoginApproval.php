<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsNCRLoginApproval.php';

$response = array();
$emp = new clsNCRLoginApproval();


if ($_action == "FILLStatus") {
    $response = $emp->FILLStatus();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Org_Status'] . ">" . $_Row['Org_Status'] . "</option>";
    }
}

if ($_action == "ShowDetails") {
    $response = $emp->GetAll();
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Organization Name</th>";
    echo "<th>Organization Ack No</th>";
    echo "<th>Organization Address</th>";
    echo "<th>Organization District</th>";
    echo "<th>Organization Tehsil</th>";
    echo "<th>Organization Email</th>";
    echo "<th>Organization Mobile</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Ack']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Road']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_District']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Tehsil']) . "</td>";
        echo "<td>" . $_Row['Org_Email'] . "</td>";
        echo "<td>" . $_Row['Org_Mobile'] . "</td>";

        if ($_Row['Org_Login_Approval_Status'] == 'Pending') {
            echo "<td> <a href='frmncrloginprocess.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='Approve Login'/></a>"
            . "</td>";
        } else if ($_Row['Org_Login_Approval_Status'] == 'Approved') {
            echo "<td>"
            . "Approved"
            . "</td>";
        } else {
            echo "<td>"
            . "Rejected"
            . "</td>";
        }

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "PROCESS") {
    $response = $emp->GetOrgDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
            $_role = 'IT-GK';
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "doctype" => $_Row['Organization_DocType'],
            "role" => $_role,
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            "district" => $_Row['Organization_District_Name'],
            "districtcode" => $_Row['Organization_District'],
            "tehsil" => $_Row['Organization_Tehsil'],
            "street" => $_Row['Organization_Street'],
            "road" => $_Row['Organization_Road'],
            "areatype" => $_Row['Org_AreaType'],
            "orgdoc" => $_Row['Organization_ScanDoc'],
            "orguid" => $_Row['Organization_UID'],
            "orgaddproof" => $_Row['Organization_AddProof'],
            "orgappform" => $_Row['Organization_AppForm'],
            "orgtypedoc1" => $_Row['Organization_TypeDocId'],);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "LoginApprove") {
    
    $_OrgCode = $_POST["orgcode"];
    $_DistrictCode = $_POST["districtcode"];
    $response = $emp->SPCreateCenter($_OrgCode, $_DistrictCode);
    echo $response[0];
    
}