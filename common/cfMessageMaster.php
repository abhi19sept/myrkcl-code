<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require '../BAL/clsMessageMaster.php';

$response = array();
$emp = new clsMessageMaster();


if ($_REQUEST['action'] == "ADD") {
    if (!empty($_POST["txt"])) {
       $_Name = $_POST["txt"];
       $_Title = $_POST["title"];
        $_ITGK_Code =   $_SESSION['User_LoginId'];
        $_Cat = $_POST["cat"];
        $_Type = $_POST["type"];
        $_Mfor = $_POST["Mfor"];

        $response = $emp->Add($_ITGK_Code,$_Title,$_Name, $_Cat, $_Type,$_Mfor);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["txt"])) {
        $_Name = $_POST["txt"];
        $_Title = $_POST["title"];
        $_ITGK_Code =   $_SESSION['User_LoginId'];
        $_Cat = $_POST["cat"];
        $_Type = $_POST["type"];
        $_Mfor = $_POST["Mfor"];

        $_Code = $_POST['id'];
        $response = $emp->Update($_Code, $_ITGK_Code,$_Title,$_Name, $_Cat, $_Type,$_Mfor);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {

        $_Datatable[] = $_Row;
    }
    echo json_encode($_Datatable);
}


if ($_action == "COUNT") {


    $response = $emp->GetCount();
    $_row = mysqli_fetch_array($response[2]);
    echo $_row['count'];
}

if ($_action == "DELETE") {
    $_ITGK_Code =   $_SESSION['User_LoginId'];

    $response = $emp->DeleteRecord($_ITGK_Code,$_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    //echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th>Title</th>";
    echo "<th>Message</th>";
    echo "<th>Category</th>";
    echo "<th>Type</th>";
    echo "<th>Message For</th>";

    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td></td>";
        echo "<td>" . $_Row['Message_Title'] . "</td>";
        echo "<td>" . $_Row['Message_Text'] . "</td>";
        echo "<td>" . $_Row['Message_Cat_Name'] . "</td>";
        echo "<td>" . $_Row['Message_Type_Name'] . "</td>";
        echo "<td>" . $_Row['Message_For'] . "</td>";
        echo "<td><a href='frmmessagemaster.php?code=" . $_Row['Message_Temp_ID'] . "&Mode=Edit'><i class='fa fa-edit' title='Edit' ></i>' </a> | "
        . "<a href='javascript:void(0)' class='deleteRecord error' data-id='" . $_Row['Message_Temp_ID'] . "' title='Delete'>"
        . "<i class='fa fa-trash-o'></i></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "SHOWMSG") {

    //echo "Show";
    $response = $emp->GetAllMSG();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    //echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='75%'>Message</th>";
    echo "<th style='10%'>Sender</th>";
    echo "<th style='10%'>Start Date</th>";
    echo "<th style='10%'>Expire Date</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $date = date("Y");
    $date.= '-';
    $date.= date("m");
    $date.= '-';
    $date.= date("d");
    

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        if ($date <= $_Row['Message_EndDate']) {
            echo "<th>" . $_Row['Message_Name'] . "</th>";
        } else {
            echo "<td>" . $_Row['Message_Name'] . "</td>";
        }
        echo "<td>" . $_Row['Organization_Name'] . "</td>";
        echo "<td>" . $_Row['Message_StartDate'] . "</td>";
        echo "<td>" . $_Row['Message_EndDate'] . "</td>";


        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

//$_POST["action"]
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Message Name</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Message_Code'] . ">" . $_Row['Message_Name'] . "</option>";
    }
}


if ($_action == "FILLCAT") {
    $_ITGK_Code =   $_SESSION['User_LoginId'];

    $response = $emp->GetAllCat($_ITGK_Code);
    $entities = [];
    //echo '<pre>';print_r($response);exit;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $entity['Message_Cat_ID'] = $_Row['Message_Cat_ID'];
        $entity['Message_Cat_Name'] = $_Row['Message_Cat_Name'];
        $entities[] = $entity;

    }
    echo json_encode($entities);
}

if ($_action == "FILLTYPE") {
    $_ITGK_Code =   $_SESSION['User_LoginId'];

    $response = $emp->GetAllType($_ITGK_Code);

    $entities = [];
    while ($_Row = mysqli_fetch_array($response[2])) {
        $entity['Message_Type_ID'] = $_Row['Message_Type_ID'];
        $entity['Message_Type_Name'] = $_Row['Message_Type_Name'];
        $entities[] = $entity;

    }
    echo json_encode($entities);
}

if ($_action == "ADDCAT") {
    $_ITGK_Code =   $_SESSION['User_LoginId'];

    $response = $emp->AddCat($_ITGK_Code,$_POST['name']);

    echo $response[0];
}

if ($_action == "ADDTYPE") {
    $_ITGK_Code =   $_SESSION['User_LoginId'];

    $response = $emp->AddType($_ITGK_Code,$_POST['name']);
    echo $response[0];
}