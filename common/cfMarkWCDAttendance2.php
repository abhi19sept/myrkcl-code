<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

/*
 * author Abhishek

 */
include './commonFunction.php';
require 'BAL/clsMarkWCDAttendance.php';
require 'DAL/upload_ftp_doc.php';
$response = array();
$emp = new clsMarkWCDAttendance();
$category = array();

if ($_action == "SHOWALL") {
    $response = $emp->GetAllLearner($_POST['batch'], $_POST['course']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";

    echo "<th style='10%'>Learner Code</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    echo "<th style='8%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_CountGrid = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        echo "<td><input type='button' data-toggle='modal' data-target='#Update' id='" . $_Row['Admission_Code'] . "' class='btn btn-primary Present' value='Mark Present'/>&nbsp<input type='button' data-toggle='modal' data-target='#Update' id='" . $_Row['Admission_Code'] . "' class='btn btn-danger Absent' value='Mark Absent'/></td>";
      


        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
if ($_action == "MarkWCDAttendance") {
    if ((isset($_POST["lcode"]) && !empty($_POST["lcode"])) || (isset($_POST["bcode"]) && !empty($_POST["bcode"])) || (isset($_POST["ccode"]) && !empty($_POST["ccode"])) || (isset($_POST["dob"]) && !empty($_POST["dob"])) || (isset($_POST["atype"]) && !empty($_POST["atype"]))) {
        $lcode  = $_POST['lcode'];
        $batch = $_POST['bcode'];
        $course = $_POST['ccode'];
        $date = $_POST['dob'];
        $atype =$_POST['atype'];
        date_default_timezone_set('Asia/Kolkata');
        $_Date = date("Y-m-d");
        $date1=date_create($date);
        $date2=date_create($_Date);
        $diff=date_diff($date2,$date1);
        $interval=$diff->format('%R%a');

        $arr = array(

            [
                [2021, 03, 25],
                [2021, 03, 26]
            ],
            [
                [2021, 03, 27],
                [2021, 03, 28]
            ],
            [
                [2021, 03, 29],
                [2021, 03, 30]
            ],
            [
                [2021, 03, 31],
                [2021, 04, 1]
            ],
            [
                [2021, 04, 2],
                [2021, 04, 3]
            ],
            [
                [2021, 04, 4],
                [2021, 04, 5],

            ],
            [
                [2021, 04, 6],
                [2021, 04, 6]
            ]

        );

        function is_in_array($array, $date) {
            $timestamp = strtotime($date);
            $date = date('d',$timestamp);
            $month = date('m',$timestamp);
            $year = date('Y',$timestamp);
            foreach ($array as $key => $value) {
                foreach($value as $value2) {
                    if($value2[0]==$year && $value2[1] == $month && $date == $value2[2])
                        return true;
                }
            }
            return false;
        }
        $isolddate = is_in_array($arr, $_POST['dob']);
        if($isolddate==1){
            $response = $emp->markWCDAttendance($lcode, $batch, $course, $date, $atype);

            echo $response[0];
        }
        else{
            if($interval == '-3' || $interval == '-2' || $interval == '-1')
            {
                $response = $emp->markWCDAttendance($lcode, $batch, $course, $date, $atype);

                echo $response[0];
                
            }
            else{
                echo "Invalid Date Entry";
            }
        }
    }
    else{
        echo "Invalid Entry";
    }
}
if ($_action == "CheckAttendance") {
    $lcode  = $_POST['lcode'];
    $batch =$_POST['batch'];
    $attendate =$_POST['attendate'];
    $response = $emp->CheckAttendance($lcode, $batch, $attendate);
    $response2 = $emp->CheckOnlineClass($batch, $attendate);
        $_Datatable = array();
    if($response[0]=="Success"){
        $_Row = mysqli_fetch_array($response[2]);
        $_Datatable[] = array("lcode" => $_Row['Attendance_Admission_Code'],
                "atype" => $_Row['Attendance_Type'],
                "atime" => $_Row['Attendance_In_Time']);
    }
    elseif($response[0]=="No Record Found"){
        $_Datatable[] = array("lcode" => '1',
                "atype" => '1',
                "atime" => '1');
    }
    else{
        $_Datatable[] = array("lcode" => '0',
                "atype" => '0',
                "atime" => '0');
    }
   
    echo json_encode($_Datatable);
}
if ($_action == "CheckCaptureEvent") {
    $coursecode = $_POST['course'];
    $batchcode = $_POST['batch'];
    $response2 = $emp->CheckCaptureEvent($coursecode, $batchcode);
    if ($response2[0] == 'Success') {
        echo 1;
    } else {
        echo 0;
    }
}
if ($_action == "FILLEventBatch") {
    $response = $emp->FILLEventBatch($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Batch'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}
if ($_action == "ADD") {
    print_r($_POST);
	echo "<br/>";
	print_r($_FILES);
    $filePhotoupd =0;
    if (isset($_POST["classdur"]) && !empty($_POST["classdur"]) && isset($_POST["ddlBatch"]) && !empty($_POST["ddlBatch"]) && isset($_POST["dob"]) && !empty($_POST["dob"]) && isset($_POST["txtclassurl"]) && !empty($_POST["txtclassurl"])) {
        $_classdur = $_POST["classdur"];
        $_ddlBatch = $_POST["ddlBatch"];
        $_ddlCourse = $_POST["ddlCourse"];
        $_classDate = $_POST["dob"];
        $_txtclassurl = $_POST["txtclassurl"];
        // $_UploadDirectory1 = '../upload/markwcdattendance';
        $_UploadDirectory1 = '/markwcdattendance';
        $_filename = $_SESSION['User_LoginId'] . '_' . $_classDate . '_wcdonlineclass.jpg';
        // if (file_exists($_UploadDirectory1)) {
            if (!empty($_FILES['filePhoto']['name'])) {


                if ($_FILES["filePhoto"]["name"] != '') {
                    $imag = $_FILES["filePhoto"]["name"];
                    $imageinfo = pathinfo($_FILES["filePhoto"]["name"]);
                    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
                        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
                        $flag = 0;
                    } else {
                        $response_ftp=ftpUploadFile($_UploadDirectory1,$_filename,$_FILES["filePhoto"]["tmp_name"]);
							print_r($response_ftp);
                        if(trim($response_ftp) == "SuccessfullyUploaded"){
                            $filePhotoupd = '1';
                            $response = $emp->ADD($_classdur, $_ddlBatch, $_ddlCourse, $_classDate, $_txtclassurl, $_filename);
                            echo $response[0];
                        }
                        // if (move_uploaded_file($_FILES["filePhoto"]["tmp_name"], "$_UploadDirectory1/" . $_filename)) {
                        //     $filePhotoupd = '1';
                        //     $response = $emp->ADD($_classdur, $_ddlBatch, $_ddlCourse, $_classDate, $_txtclassurl, $_filename);
                        //     echo $response[0];
                        // }
                         else {
                            $filePhotoupd = '0';
                            echo "Image not upload. Please try again.";
                        }
                    }
                }
            } else {
                echo "Please upload Highest Qualification Document";
            }
        //     } else {
        //     echo "Upload Folder Not Available";
        // }
    }
}
if ($_POST['action'] == "CHKclass") {

    if (isset($_POST["course"]) && !empty($_POST["course"]) && isset($_POST["batch"]) && !empty($_POST["batch"]) && isset($_POST["classdate"]) && !empty($_POST["classdate"]))
     {
        $course = $_POST['course'];
        $batch = $_POST['batch'];

        $classdate = $_POST['classdate'];
        
        $response = $emp->Chkslottime($course,$batch,$classdate);
        $co = mysqli_num_rows($response[2]);
        $_Datatable = array();
        if ($co) {
            $_Row = mysqli_fetch_array($response[2]);


$_ObjConnection = new ftpConnection();
    $details= $_ObjConnection->ftpdetails();



$id = $_Row['OnlineClass_Photo'];
// $id = '361210510064554789_photo.png';
$path = '/markwcdattendance/';

$image =  file_get_contents($details.$path.$id);
$imageData = base64_encode($image);
// <a data-fancybox='gallery' href='data:image/png;base64, ".$imageData."'><img src="small_1.jpg"></a>
$showimage =  "<a data-fancybox='gallery' href='data:image/png;base64, ".$imageData."'><img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='150'/></a>";
// $ext = pathinfo($_Row['Bank_Document'], PATHINFO_EXTENSION);

            $_Datatable[] = array("classphoto" => $showimage,
                "classurl" => $_Row['OnlineClass_URL'],
                "classduration" => $_Row['OnlineClass_Duration']);
        }
        else{
            $_Datatable[] = array("classphoto" => '0',
                "classurl" => '0',
                "classduration" => '0');
        } 
       echo json_encode($_Datatable);
    }
    else {
        echo "Invalid Details";
    }
}
