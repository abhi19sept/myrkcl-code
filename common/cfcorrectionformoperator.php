<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clscorrectionformoperator.php';

    $response = array();
    $emp = new clscorrectionformoperator();
    if ($_action == "ADD") {
        if (isset($_POST["lcode"]) && !empty($_POST["lcode"]) && !empty($_POST["lcorrectname"]) && !empty($_POST["faname"])
    		&& !empty($_POST["application_for"])
			 && !empty($_POST["examname"]) && !empty($_POST["totalmarks"]) && !empty($_POST["email"]) && !empty($_POST["mobileno"]) && !empty($_POST["dob"]))
			 {
				$_LearnerCode = $_POST["lcode"];
				$_CenterCode = $_POST["centercode"];
				$_LearnerName = $_POST["lcorrectname"];				  
				$_FatherName = $_POST["faname"];				
				$_ExamName = $_POST["examname"];
				$_TotalMarks = $_POST["totalmarks"];
				$_Email = $_POST["email"];
				$_MobileNo = $_POST["mobileno"];
				$_DOB = $_POST["dob"];				
				$_GENERATEDID = $_POST["genratedid"];	
				
				if($_POST["application_for"] == 1){
					$_LApplication = "Correction Certificate";
				}
				else{
					$_LApplication = "Duplicate Certificate";
				}
				
            $response = $emp->Add($_LearnerCode, $_CenterCode, $_LearnerName, $_FatherName, $_LApplication, $_ExamName, $_TotalMarks, $_Email, $_MobileNo, $_DOB, $_GENERATEDID);
            echo $response[0];
        }
    }
    
    if ($_action == "UPDATE") {
        if (isset($_POST["name"])) {
            $_StatusName = $_POST["name"];
            $_StatusDescription = $_POST["description"];
            $_Status_Code=$_POST['code'];
            $response = $emp->Update($_Status_Code,$_StatusName, $_StatusDescription);
            echo $response[0];
        }
    }
    
    
    if ($_action == "EDIT") {


        $response = $emp->GetDatabyCode($_actionvalue);

        $_DataTable = array();
        $_i=0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("StatusCode" => $_Row['Status_Code'],
                "StatusName"=>$_Row['Status_Name'],
                "StatusDescription"=>$_Row['Status_Description']);
            $_i=$_i+1;
        }
        echo json_encode($_Datatable);
    }
    
    
    if ($_action == "DELETE") {


        $response = $emp->DeleteRecord($_actionvalue);

        echo $response[0];
    }


    if ($_action == "SHOW") {
        $response = $emp->GetAll();
        $_DataTable = "";

         echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='5%'>S No.</th>";
        echo "<th style='30%'>Name</th>";
        echo "<th style='55%'>Description</th>";
        echo "<th style='10%'>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Status_Name'] . "</td>";
            echo "<td>" . $_Row['Status_Description'] . "</td>";
            echo "<td><a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a>  <a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
		echo "</div>";
//echo $_DataTable;
    }
	
	
    if ($_action == "FILL") {
        $response = $emp->GetAll();
        echo "<option value='' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['District_Name'] . ">" . $_Row['District_Name'] . "</option>";    
        }
    }
    
     if ($_action == "FILLEMPDEPARTMENT") {
        $response = $emp->GetEmpdepartment();
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['gdname'] . ">" . $_Row['gdname'] . "</option>";            
        }
    }
    
    
     if ($_action == "FILLEMPDESIGNATION") {
        $response = $emp->GetEmpdesignation();
        echo "<option value='' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['designationname'] . ">" . $_Row['designationname'] . "</option>";            
        }
    }
    
    
     if ($_action == "FILLBANKDISTRICT") {
        $response = $emp->GetBankdistrict();
        echo "<option value='' selected='selected'>Please Select</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['District_Name'] . ">" . $_Row['District_Name'] . "</option>";            
        }
    }
    
    
     if ($_action == "FILLBANKNAME") {
        $response = $emp->GetBankname();
        echo "<option value='' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['bankname'] . ">" . $_Row['bankname'] . "</option>";            
        }
    }
    
    
     if ($_action == "FillDobProof") {
        $response = $emp->GetDobProof();
        echo "<option value='' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['cdocname'] . ">" . $_Row['cdocname'] . "</option>";            
        }
    }
	
	
	if ($_action == "FillApplicationFor") {
        $response = $emp->GetFillApplicationFor();
        echo "<option value='' selected='selected'>Please Select</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['docid'] . ">" . $_Row['applicationfor'] . "</option>";
           }
    }
	
	if ($_action == "FillExamEvent") {
        $response = $emp->GetExamEvent();
        echo "<option value='' selected='selected'>Please Select</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";    
        }
    }
	
	if ($_action == "FillEventById") {
        $response = $emp->FillEventById($_POST['values']);        
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo $_Row['Event_Name'] ;    
        }
    }
	
	if ($_action == "DETAILS") {
        $response = $emp->GetAllDETAILS($_POST['values'],$_POST['centercode']);
		//print_r($response[2]);
        $_DataTable = array();
        $_i=0;
		$co = mysqli_num_rows($response[2]);
		if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("Admission_Name" => $_Row['Admission_Name'],
                "Admission_Fname"=>$_Row['Admission_Fname'],
                "Admission_Mobile"=>$_Row['Admission_Mobile'],
				"Admission_Email"=>$_Row['Admission_Email'],
				"Admission_Address"=>$_Row['Admission_Address'],
				"Admission_GPFNO"=>$_Row['Admission_GPFNO']);
            $_i=$_i+1;
        }
         echo json_encode($_Datatable);
		}
		else {
			echo "";
		}
    }
?>
