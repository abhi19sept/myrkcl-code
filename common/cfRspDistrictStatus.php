<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsRspDistrictStatus.php';

$response = array();
$emp = new clsRspDistrictStatus();


if ($_action == "FILLStatus") {
    $response = $emp->FILLStatus();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Rsptarget_Status'] . ">" . $_Row['Rsptarget_Status'] . "</option>";
    }
}

if ($_action == "ShowDetails") {

    //print_r($_POST['status']);
    $response = $emp->GetAll($_POST['status']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>District Name</th>";
    echo "<th style='40%'>2 Month Rural</th>";
    echo "<th style='10%'>2 Month Urban</th>";
    echo "<th style='40%'>4 Month Rural</th>";
    echo "<th style='10%'>4 Month Urban</th>";
    echo "<th style='40%'>6 Month Rural</th>";
    echo "<th style='10%'>6 Month Urban</th>";
    echo "<th style='40%'>Total ITGKs in 12 months</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_2MonthRural'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_2MonthUrban'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_4MonthRural'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_4MonthUrban'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_6MonthRural'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_6MonthUrban'] . "</td>";
         echo "<td>" . $_Row['Rsptarget_Total'] . "</td>";
//        echo "<td><a href='frmBankMaster.php?code=" . $_Row['Bank_Code'] . "&Mode=Edit'>"
//                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
//        . "<a href='frmBankMaster.php?code=" . $_Row['Bank_Code'] . "&Mode=Delete'>"
//                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
     echo "</div>";
}