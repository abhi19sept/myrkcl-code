<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsAadharLinkCountReport.php';

$response = array();
$emp = new clsAadharLinkCountReport();

if ($_action == "SHOW") {
    if (isset($_POST["startdate"]) && !empty($_POST["startdate"])) {
        if (isset($_POST["enddate"]) && !empty($_POST["enddate"])) {				
			
            $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
            
            $response = $emp->Show($sdate, $edate);
           
			echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th> S No.</th>";
            echo "<th> Learner Code </th>";           
            echo "<th> ITGK Code </th>";
            echo "<th> Learner Name</th>";
            echo "<th> Father/Husband Name </th>";
            echo "<th> Mobile No. </th>";           
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;
            while ($row = mysqli_fetch_array($response[2])) {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $row['Admission_LearnerCode'] . "</td>";
                echo "<td>" . $row['Admission_ITGK_Code'] . "</td>";
                echo "<td>" . $row['name'] . "</td>";
                echo "<td>" . $row['father'] . "</td>";
                echo "<td>" . $row['Admission_Mobile'] . "</td>";
				echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";					
        }else{
			echo "e";
		}
    }else {
		echo "s";
	}
}

if ($_action == "FillEvent") {
    $response = $emp->FillEvent();
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "GetCertificateCount"){
	if (isset($_POST["eventid"]) && !empty($_POST["eventid"])){
		if (isset($_POST["batchtype"]) && !empty($_POST["batchtype"])){
			if($_POST["batchtype"]=='1' || $_POST["batchtype"]=='2'){
				$response = $emp->GetEventBatch($_POST["eventid"],$_POST["batchtype"]);
					if($response[0]=='Success'){
						$row=mysqli_fetch_array($response[2]);
						$batch=$row['batchcode'];
						$fresh = $emp->GeneratedCertificateCount($batch);
						
						$_DataTable = "";

						echo "<div class='table-responsive'>";
						echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
						echo "<thead>";
						echo "<tr>";
						echo "<th>S No.</th>";
						echo "<th style='text-align:center'>Course Name</th>";	
						echo "<th style='text-align:center'>Batch Name</th>";	
						echo "<th style='text-align:center'>Generate Certificate Count</th>";
						echo "</tr>";						
						echo "</thead>";
						echo "<tbody>";
						$_Count = 1;
							while ($_Row = mysqli_fetch_array($fresh[2])) {
								echo "<tr>";
								echo "<td>" . $_Count . "</td>";
								echo "<td>" . $_Row['Course_Name'] . "</td>";
								echo "<td>" . $_Row['Batch_Name'] . "</td>";
								//echo "<td>" .$_Row['codes'] . "</td>";
								echo "<td><button type='button' data-toggle='modal' data-target='#GetLearner' class='GetLearner' id='".$_Row['Admission_Batch']."' >" . $_Row['codes'] . "</button></td>";
			
								echo "</tr>";
								$_Count++;
							}
						echo "</tbody>";
						echo "</table>";
						echo "</div>";
			}
			}else {
				echo "0";
			}
		}else{
			echo "t";
		}
	}
	else {
		echo "e";
	}
}

if ($_action == "GetLearnerDetails") {
	$batchcode = $_REQUEST['batchcode'];
	$response = $emp->GetLearnerDetails($batchcode);
		 $_DataTable = "";

		echo "<div class='table-responsive' style='margin-top:10px'>";
   
		echo "<table id='examples' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th style='15%'>S No.</th>";
		echo "<th style='15%'>ITGK-Code</th>";
		echo "<th style='15%'>LearnerCode</th>";
		echo "<th style='15%'>Learner Name</th>";
		echo "<th style='15%'>Father/Husband Name</th>";
		echo "<th style='15%'>Mobile No.</th>";
		echo "<th style='15%'>Aadhar No.</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$_Count = 1;
		$co = mysqli_num_rows($response[2]);
		if ($co) {
			while ($_Row = mysqli_fetch_array($response[2])){
				echo "<tr class='odd gradeX'>";
				echo "<td>" . $_Count . "</td>";
				echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
				echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
				echo "<td>" . $_Row['name'] . "</td>";
				echo "<td>" . $_Row['fname'] . "</td>";
				echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
				echo "<td>" . $_Row['Admission_UID'] . "</td>";
				echo "</tr>";
				$_Count++;
			}
			echo "</tbody>";
			echo "</table>";
			echo "</div>";
		}
		
}