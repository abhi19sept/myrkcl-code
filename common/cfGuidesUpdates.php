<?php

require 'commonFunction.php';
require 'BAL/clsGuidesUpdates.php';

$emp = new clsGuidesUpdates();

if ($_POST['action'] == "Addslider") {
    if (isset($_POST["imageTittle"]) && !empty($_POST["imageTittle"]) && isset($_POST["sliderStatus"]) && !empty($_POST["sliderStatus"]) && isset($_FILES["silederImage"]) && !empty($_FILES["silederImage"])
    ) {
        $imageTittle = $_POST['imageTittle'];
        $sliderStatus = $_POST['sliderStatus'];
        if ($_POST['ddlheadname']) {
            $headingname = $_POST['ddlheadname'];
        } else {
            $headingname = $_POST['txtheadname'];
        }
        $img = $_FILES['silederImage']['name'];
        $tmp = $_FILES['silederImage']['tmp_name'];
        $temp = explode(".", $img);
        $newfilename = round(microtime(true)) . '.' . end($temp);
        $filestorepath = "../download/" . $newfilename;
        $imageFileType = pathinfo($filestorepath, PATHINFO_EXTENSION);
//        if ($_FILES["silederImage"]["size"] > 500000000000) {
//            echo "Sorry, your file is too large.";
//        }
        if ($imageFileType != "zip" && $imageFileType != "pdf" && $imageFileType != "rar") {
            echo "Sorry, File Not Valid";
        } else {
            if (move_uploaded_file($tmp, $filestorepath)) {
                $response = $emp->ImageSliderNew($imageTittle, $headingname, $sliderStatus, $newfilename);
                echo $response[0];
            } else {
                echo "Invalid Details";
            }
        }
    }
}

//Show Data Or View Details...
if ($_action == "SHOW") {
    if ($_POST['status'] == '1') {
        $response = $emp->ShowSlideImages($_POST['status']);
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>Process Guide Heading</th>";
        echo "<th>Process Guide Tittle</th>";
        echo "<th>Status</th>";
        echo "<th>Date</th>";
        echo "<th>Download</th>";
        echo "<th>Edit</th>";
        echo "<th>Delete</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        if ($response[0] == 'Success') {
            while ($row = mysqli_fetch_array($response[2])) {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $row['headingname'] . "</td>";
                echo "<td>" . $row['imagetittle'] . "</td>";
                echo "<td>" . $row['status'] . "</td>";
                echo "<td>" . date("F d, Y H:i:s", strtotime($row['time'])) . "</td>";
                echo "<td><a target='_blanck' href='download/" . $row['photo'] . "'><img class='thumbnail img-responsive' src='images/dwnpdf.png'  width='30'></a></td>";
                echo "<td><button id='" . $row['id'] . "' class='fun_update_slider'>Update</button></td>";
                echo "<td><input type='hidden' id='file_id_" . $row['id'] . "' value='" . $row['photo'] . "'><button id='" . $row['id'] . "' class='fun_delete_slider'>Delete</button></td>";
                echo "</tr>";
                $_Count++;
            }
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
}

//Delete Slider Images....
if ($_action == "DELETE") {
    if (isset($_POST["deleteid"])) {
        $deleteid = $_POST["deleteid"];
        $response = $emp->DeleteSliderImage($deleteid);
        $delfie = $_POST["fileid"];
        unlink("../uploads/slider/" . $delfie);
    }
}

//Edit Or Fill Data for Slider Image
if ($_action == "EDIT") {
    $editid = $_POST['editid'];
    $_POST['fileid'];
    $response = $emp->ShowSlideImagesForEdit($editid);
    $_DataTable = array();
    $_i = 0;
    if ($response[0] == 'Success') {
        while ($row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("sliderid" => $row['id'],
                "imagetittle" => $row['imagetittle'],
                "status" => $row['status'],
                "photo" => $row['photo'],);
            $_i = $_i + 1;
        }
    }
    echo json_encode($_Datatable);
}

//Update Slider Image And Data....
if ($_POST['action'] == "Updateslider") {
    if (isset($_POST["imageTittleupdate"]) && !empty($_POST["imageTittleupdate"]) && isset($_POST["sliderStatusupdate"]) && !empty($_POST["sliderStatusupdate"]) && isset($_FILES["silederImageupdate"]) && !empty($_FILES["silederImageupdate"]) && isset($_POST["sliderid"]) && !empty($_POST["sliderid"]) && isset($_POST["oldimagefile"]) && !empty($_POST["oldimagefile"])
    ) {
        $imageTittleupdate = $_POST['imageTittleupdate'];
        $sliderStatusupdate = $_POST['sliderStatusupdate'];
        $silederImageupdate = $_FILES['silederImageupdate'];
        $sliderid = $_POST['sliderid'];
        $oldimagefile = $_POST['oldimagefile'];
        if (empty($_FILES['silederImageupdate']['name'])) {
            $newfilename = $_POST["oldimagefile"];
            $response = $emp->SliderImageUpdate($sliderid, $imageTittleupdate, $sliderStatusupdate, $newfilename);
        } else {
            $img = $_FILES['silederImageupdate']['name'];
            $tmp = $_FILES['silederImageupdate']['tmp_name'];
            $temp = explode(".", $img);
            $newfilename = round(microtime(true)) . '.' . end($temp);
            $filestorepath = "../download/" . $newfilename;
            $imageFileType = pathinfo($filestorepath, PATHINFO_EXTENSION);
            if ($_FILES["silederImageupdate"]["size"] > 500000) {
                echo "Sorry, your file is too large.";
            }
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf" && $imageFileType != "gif") {
                echo "Sorry, File Not Valid";
            } else {
                if (move_uploaded_file($tmp, $filestorepath)) {
                    $delfie = $_POST["oldimagefile"];
                    unlink("../uploads/slider/" . $delfie);
                    $response = $emp->SliderImageUpdate($sliderid, $imageTittleupdate, $sliderStatusupdate, $newfilename);
                } else {
                    echo "Invalid details";
                }
            }
        }
    }
}
if ($_action == "SHOWPDF") {
    if ($_POST['status'] == '1') {
        $response = $emp->GetHeading($_POST['status']);
//            $row = mysqli_fetch_array($response[2]);
//            echo "<div class='panel panel-info'>";
//            echo "<div class='panel-heading'>" .$row['headingname']. "</div>";
//            echo"<div class='panel-body' id='tabledatares'>";
//            echo  "<table class='table table-user-information table table-hover'>";
//            echo "<tbody>";
//            echo "<div class='panel panel-info'>";
//            echo "<div class='panel-heading'>Guides</div>";
//            echo"<div class='panel-body'>";
        $_Count = 1;
        $_Count1 = 1;
        if ($response[0] == 'Success') {
            while ($row = mysqli_fetch_array($response[2])) {
                //$row = mysqli_fetch_array($response[2]);
                echo "<div class='panel panel-info'>";
                echo "<div class='panel-heading'>" . $row['headingname'] . "</div>";
                echo"<div class='panel-body'>";
//                echo "<table class='table table-user-information table table-hover'>";
//                echo "<tbody>";
//                echo "<tr>";
//                echo "<th class='col-md-8 col-lg-8'>Presentation Name:</th>";
//                echo "<th class='col-md-2 col-lg-2'>Download</th>";
//                echo "</tr>";
//                echo "</tbody>";
//                echo "</table>";
                $response1 = $emp->GetRows($row['headingname']);
                while ($row1 = mysqli_fetch_array($response1[2])) {
                    echo "<table class='table table-user-information table table-hover'>";
                    echo "<tbody>";
                    echo "<tr class='odd gradeX'>";
                    echo "<td class='col-md-10 col-lg-10'>" . $row1['imagetittle'] . "</td>";
                    echo "<td><a target='_blanck' href='download/" . $row1['photo'] . "'><img class='thumbnail img-responsive' src='images/dwnpdf.png'  width='30'></a></td>";
                    echo "</tr>";

                    $_Count++;
                    echo "</tbody>";
                    echo "</table>";
                }
                $_Count1++;
                echo "</div>";
                echo "</div>";
            }
        }
    }
}

if ($_action == "FILL") {
    $response = $emp->GetHeading();
    echo "<option value='0' selected='selected'>Select Heading</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['headingname'] . "'>" . $_Row['headingname'] . "</option>";
    }
}
?>