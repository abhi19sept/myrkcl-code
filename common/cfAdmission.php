<?php

/*
 *  author Viveks
 */

require 'commonFunction.php';
require 'BAL/clsAdmission.php';

$response = array();
$emp = new clsAdmission();
//print_r($_POST);

if ($_action == "ADD") {
    if ($_POST["isaadhar"] == "n") {
        $_POST["aadharname"] = $_POST["lname"];
        $_POST["aadhardob"] = $_POST["dob"];
    }
    if (strcasecmp(trim($_POST["lname"]), trim($_POST["aadharname"])) == 0) {
        if (strcasecmp(trim($_POST["dob"]), trim($_POST["aadhardob"])) == 0) {
            if (isset($_POST["learnercode"]) && !empty($_POST["learnercode"])) {
                if (isset($_POST["lname"]) && !empty($_POST["lname"])) {
                    if (isset($_POST["parent"]) && !empty($_POST["parent"])) {
                        if (isset($_POST["idproof"]) && !empty($_POST["idproof"])) {
                            if (isset($_POST["dob"]) && !empty($_POST["dob"])) {
                                if (isset($_POST["mothertongue"]) && !empty($_POST["mothertongue"])) {
                                    if (isset($_POST["medium"]) && !empty($_POST["medium"])) {
                                        if (isset($_POST["gender"]) && !empty($_POST["gender"])) {
                                            if (isset($_POST["marital_type"]) && !empty($_POST["marital_type"])) {
                                                if (isset($_POST["district"]) && !empty($_POST["district"])) {
                                                    if (isset($_POST["tehsil"]) && !empty($_POST["tehsil"])) {
                                                        if (isset($_POST["address"]) && !empty($_POST["address"])) {
                                                            if (isset($_POST["pincode"]) && !empty($_POST["pincode"])) {
                                                                if (isset($_POST["mobile"]) && !empty($_POST["mobile"])) {
                                                                    if (isset($_POST["qualification"]) && !empty($_POST["qualification"])) {
                                                                        if (isset($_POST["learnertype"]) && !empty($_POST["learnertype"])) {
                                                                            if (isset($_POST["physicallychallenged"]) && !empty($_POST["physicallychallenged"])) {
                                                                                if (isset($_POST["coursecode"]) && !empty($_POST["coursecode"])) {
                                                                                    if (isset($_POST["batchcode"]) && !empty($_POST["batchcode"])) {

                                                                                        $_LearnerName = trim($_POST["lname"]);
                                                                                        $_LearnerCode = trim($_POST["learnercode"]);
                                                                                        $_ParentName = trim($_POST["parent"]);
                                                                                        $_IdProof = trim($_POST["idproof"]);
                                                                                        $_IdNo = trim($_POST["idno"]);
                                                                                        $_DOB = trim($_POST["dob"]);
                                                                                        $_MotherTongue = trim($_POST["mothertongue"]);
                                                                                        $_Medium = trim($_POST["medium"]);
                                                                                        $_Gender = trim($_POST["gender"]);
                                                                                        $_MaritalStatus = trim($_POST["marital_type"]);
                                                                                        $_District = trim($_POST["district"]);
                                                                                        $_Tehsil = trim($_POST["tehsil"]);
                                                                                        $_Address = trim($_POST["address"]);
                                                                                        $_PIN = trim($_POST["pincode"]);
                                                                                        $_ResiPh = trim($_POST["resiph"]);
                                                                                        $_Mobile = trim($_POST["mobile"]);
                                                                                        $_Qualification = trim($_POST["qualification"]);
                                                                                        $_LearnerType = trim($_POST["learnertype"]);
                                                                                        $_PhysicallyChallenged = trim($_POST["physicallychallenged"]);
                                                                                        $_GPFno = trim($_POST["GPFno"]);
                                                                                        $_Email = trim($_POST["email"]);
                                                                                        $_LearnerCourse = trim($_POST["coursecode"]);
                                                                                        $_LearnerBatch = trim($_POST["batchcode"]);
                                                                                        $_LearnerFee = trim($_POST["fee"]);
                                                                                        $_LearnerInstall = trim($_POST["install"]);
                                                                                        $_Photo = $_POST["lphoto"];
                                                                                        $_Sign = $_POST["lsign"];
                                                                                        $_Scan = $_POST["lscan"];

                                                                                        // echo $_LearnerName.$_ParentName.$_IdProof.$_IdNo.$_DOB.$_MotherTongue.$_Medium.$_Gender.$_MaritalStatus . $_District.$_Tehsil.$_Address.$_ResiPh.$_Mobile.$_Qualification.$_LearnerType.$_GPFno.$_PhysicallyChallenged.$_Email.$_LearnerPhoto.$_LearnerSign.$_PIN;

                                                                                      $response = $emp->Add($_LearnerName, $_LearnerCode, $_ParentName, $_IdProof, $_IdNo, $_DOB, $_MotherTongue, $_Medium, $_Gender, $_MaritalStatus, $_District, $_Tehsil, $_Address, $_ResiPh, $_Mobile, $_Qualification, $_LearnerType, $_GPFno, $_PhysicallyChallenged, $_Email, $_PIN, $_LearnerCourse, $_LearnerBatch, $_LearnerFee, $_LearnerInstall, $_Photo, $_Sign, $_POST["isaadhar"]);
                                                                                        echo $response[0];
                                                                                    } else {
                                                                                        echo "Inavalid Entry";
                                                                                    }
                                                                                } else {
                                                                                    echo "Inavalid Entry";
                                                                                }
                                                                            } else {
                                                                                echo "Inavalid Entry";
                                                                            }
                                                                        } else {
                                                                            echo "Inavalid Entry";
                                                                        }
                                                                    } else {
                                                                        echo "Inavalid Entry";
                                                                    }
                                                                } else {
                                                                    echo "Inavalid Entry";
                                                                }
                                                            } else {
                                                                echo "Inavalid Entry";
                                                            }
                                                        } else {
                                                            echo "Inavalid Entry";
                                                        }
                                                    } else {
                                                        echo "Inavalid Entry";
                                                    }
                                                } else {
                                                    echo "Inavalid Entry";
                                                }
                                            } else {
                                                echo "Inavalid Entry";
                                            }
                                        } else {
                                            echo "Inavalid Entry";
                                        }
                                    } else {
                                        echo "Inavalid Entry";
                                    }
                                } else {
                                    echo "Inavalid Entry";
                                }
                            } else {
                                echo "Inavalid Entry";
                            }
                        } else {
                            echo "Inavalid Entry";
                        }
                    } else {
                        echo "Inavalid Entry";
                    }
                } else {
                    echo "Inavalid Entry";
                }
            }
        } else {
            echo "DOB Not Matched as registered in Aadhar No.";
        }
    } else {
        echo "Learner Name Not Matched as registered in Aadhar No.";
    }
}


if ($_action == "UPDATE") {
    //print_r($_POST);
    //print_r($_FILES);

    $_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_photo/';
    $_UploadDirectory2 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_sign/';

    $_LearnerName = $_POST["txtlname"];
    $_ParentName = $_POST["txtfname"];
    $_DOB = $_POST["dob"];
    $_Code = $_POST['txtAdmissionCode'];
    $_GeneratedId = $_POST['txtGenerateId'];

    if ($_FILES["p1"]["name"] != '') {
        $imag = $_FILES["p1"]["name"];
        $newpicture = $_GeneratedId . '_photo' . '.png';
        $_FILES["p1"]["name"] = $newpicture;
    } else {
        $newpicture = $_POST['txtphoto'];
    }

    if ($_FILES["p1"]["name"] != '') {
        $imageinfo = pathinfo($_FILES["p1"]["name"]);
        if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg") {
            $error_msg = "Image must be in either PNG or JPG Format";
        } else {
            if (file_exists("$_UploadDirectory1/" . $_GeneratedId . '_photo' . '.png')) {
                unlink("$_UploadDirectory1/" . $newpicture);
            }
            if (file_exists($_UploadDirectory1)) {
                if (is_writable($_UploadDirectory1)) {
                    move_uploaded_file($_FILES["p1"]["tmp_name"], "$_UploadDirectory1/" . $newpicture);
                }
            }
        }
    }


    if ($_FILES["p2"]["name"] != '') {
        $imag = $_FILES["p2"]["name"];
        $newsign = $_GeneratedId . '_sign' . '.png';
        $_FILES["p2"]["name"] = $newsign;
    } else {
        $newsign = $_POST['txtsign'];
    }

    if ($_FILES["p2"]["name"] != '') {
        $imageinfo = pathinfo($_FILES["p2"]["name"]);
        if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg") {
            $error_msg = "Image must be in either PNG or JPG Format";
        } else {
            if (file_exists("$_UploadDirectory2/" . $_GeneratedId . '_sign' . '.png')) {
                unlink("$_UploadDirectory2/" . $newsign);
            }
            if (file_exists($_UploadDirectory2)) {
                if (is_writable($_UploadDirectory2)) {
                    move_uploaded_file($_FILES["p2"]["tmp_name"], "$_UploadDirectory2/" . $newsign);
                }
            }
        }
    }

    $response = $emp->Update($_LearnerName, $_ParentName, $_DOB, $newpicture, $newsign, $_Code);
    echo $response[0];
}

if ($_action == "EDIT") {

global $_ObjFTPConnection;

    $response = $emp->GetDatabyCodeForUpdate($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    if (mysqli_num_rows($response[2])) {
        while ($_Row = mysqli_fetch_array($response[2])) {
    $details= $_ObjFTPConnection->ftpdetails();


//$batch=$_Row['Batch_Name'];
$_BatchName =  $_Row['Batch_Name']; 
$_Course = $_Row['Admission_Course'];


                if ($_Course == 5) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);                    

                } elseif ($_Course == 4) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

                }
                elseif ($_Course == 3) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }               
                
                elseif ($_Course == 23) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

                }   

                 elseif ($_Course == 25) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

                }
                elseif ($_Course == 28) {

                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

                }
                elseif ($_Course == 29) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

                }
                elseif ($_Course == 30) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

                }
                elseif ($_Course == 31) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

                }
                elseif ($_Course == 32) {
                    $_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

                }
$photopath = '/admission_photo/'.$_LearnerBatchNameFTP.'/';
$photoid = $_Row['Admission_Photo'];
$signid = $_Row['Admission_Sign'];
$signpath = '/admission_sign/'.$_LearnerBatchNameFTP.'/';

$photoimage =  file_get_contents($details.$photopath.$photoid);
$imageData = base64_encode($photoimage);
// <a data-fancybox='gallery' href='data:image/png;base64, ".$imageData."'><img src="small_1.jpg"></a>
$showimage =  "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='80' height='110'/>";

$signimage =  file_get_contents($details.$signpath.$signid);
$imageDatasign = base64_encode($signimage);
// <a data-fancybox='gallery' href='data:image/png;base64, ".$imageData."'><img src="small_1.jpg"></a>
$showimagesign =  "<img id='ftpimg' src='data:image/png;base64, ".$imageDatasign."' alt='Red dot' width='80' height='30'/>";

            $_DataTable[$_i] = array("AdmissionCode" => $_Row['Admission_Code'],
                "lcode" => $_Row['Admission_LearnerCode'],
                "lname" => $_Row['Admission_Name'],
                "fname" => $_Row['Admission_Fname'],
                "dob" => $_Row['Admission_DOB'],
                "pid" => $_Row['Admission_PID'],
                "uid" => $_Row['Admission_UID'],
                "address" => $_Row['Admission_Address'],
                "pin" => $_Row['Admission_PIN'],
                "mobile" => $_Row['Admission_Mobile'],
                "phone" => $_Row['Admission_Phone'],
                "email" => $_Row['Admission_Email'],
                "photoad" => $_Row['Admission_Photo'],
                "signad" => $_Row['Admission_Sign'],
                "coursecode" => $_Row['Admission_Course'],
                "batchname" => $_LearnerBatchNameFTP,
                "photo" => $showimage,
                "sign" => $showimagesign);
            $_i = $_i + 1;
        }
        echo json_encode($_DataTable);
    }
}

if ($_action == "Fee") {
    // print_r($_POST);
    $response = $emp->GetAdmissionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Course_Fee'];
}

if ($_action == "GENERATELEARNERCODE") {
    $random = "";
    $random .= (mt_rand(100, 999));
    $random .= date("y");
    $random .= date("m");
    $random .= date("d");
    $random .= date("h");
    $random .= date("i");
    $random .= date("s");
    $random .= '789';

    echo $random;
}


if ($_action == "Install") {
    //echo "call";
    $response = $emp->GetAdmissionInstall($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Admission_Installation_Mode'];
}

if ($_action == "FILLIntake") {
    //echo "call";
    $response = $emp->GetIntakeAvailable($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Intake_Available'];
}



if ($_action == "generateOTP") {
    $aadhaar_no = $_POST['txtACno'];
    $generateOTP = generateOTP($aadhaar_no);
    echo json_encode($generateOTP);
}

/* * *
 * @function generateOTP : function to generate Adhaar OTP
 * @param int $aadhaar_no : The adhaar number of User
 * @param int $pinCode : Area pin code
 * @return array : Returns an array containing the status and transaction id
 * * */

function generateOTP($aadhaar_no) {
    /*     * * DO NOT MODIFY THE VALUES BELOW ** */
    //$licence_key = "MEmVkcpNLahCE-9skCRMK36S_ufQGPaCiNFAZ33o_ICd01JIE6IBLpU";
    //$licence_key = "MPjsZ77ep-Mu0gTYs5_1mOfkfozxJ-3Q2AGHyhhjkTOeg7JSFAolRqI";
   // $licence_key = "MJqg1YjQL8GkseVac8FM2g1BUzLmiKN_I6gjETFFD27i6RGUYysHeKo";
//$licence_key ="MAlCZADE4gHodMG6QJ5GRY7-NlR-7A18QUrh2zo-WspGIibbrm0YgOc"; //updaated on 27 August 2019//
// $licence_key ="MBw0j-om9sFUsiMuspIGUJawr6gpdboMmdHohGCLa7L7nygm8hi0ib8"; //updaated on 25 April 2020//
$licence_key ="MFsosPRwFp2OnNFg7XP_uvZu3QUKwaEJ0Twb3EpyPCsItGtNNN3xZnY"; //updaated on 12 April 2021//

    $tid = "public";
    //$subaua = "STGDOIT006";
   // $subaua = "STGDOIT238";
   $subaua = "PRKCL22857"; // 27Aug2019 2.5
    $udc = "RKCL123";
    $ip = "127.0.0.1";
    $fdc = "NA";
    $idc = "NA";
    $macadd = "";
    $lot = "P";
    $rc = "Y";
    $pinCode = 'NA';

    /*     * * GENERATE AUTH XML BLOCK HERE (DO NOT MODIFY) ** */
    $auth_xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<authrequest uid="' . $aadhaar_no . '" subaua="' . $subaua . '" ip="' . $ip . '" fdc="' . $fdc . '" idc="' . $idc . '" macadd="' . $macadd . '" lot="' . $lot . '" lov="' . $pinCode . '" lk="' . $licence_key . '" rc="' . $rc . '"  ver="2.5">
    <otp />
</authrequest>';

    /*     * * INITIATE CURL REQUEST (DO NOT MODIFY) ** */
    //$ch = curl_init("http://103.203.138.120/api/aua/otp/request/encr");
 //   $ch = curl_init("https://api.sewadwaar.rajasthan.gov.in/app/live/api/aua/otp/request?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69");
$ch = curl_init("https://api.sewadwaar.rajasthan.gov.in/app/live/was/otp/request/prod?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69");   
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSLVERSION, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $auth_xml);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        "Accept: application/xml",
        "Content-Type: application/xml",
        "appname: MYRKCL"
    ]);

    /*     * * EXECUTE CURL HERE ** */

    $result = curl_exec($ch);
    if (!$result) {
        print curl_errno($ch) . ': ' . curl_error($ch);
    }
    /*     * * CLOSING CURL HERE ** */

    curl_close($ch);

    /*     * * STORE CURL RESPONSE INTO XML ** */

    $xml = @simplexml_load_string($result);

    /*     * * CONVERT XML INTO JSON FORMAT ** */

    $json = json_encode($xml);

    /*     * * CONVERT JSON INTO ARRAY FORMAT ** */

    $array = json_decode($json, true);

    /*     * * RETURNING RESPONSE AND TRANSACTION ** */

    $responseArray = [
        "txn" => $array["auth"]["@attributes"]["txn"],
        "status" => $array["auth"]["@attributes"]["status"]
    ];
    return $responseArray;
}

if ($_action == "authenticateOTP") {
    $aadhaar_no = $_POST['txtACno'];
    $txnID = $_POST['txtTxn'];
    $otp = $_POST['txtotpTxt'];
//    $txtCourse = $_POST['txtCourse'];
//    $txtBatch = $_POST['txtBatch'];
//    $txtLcode = $_POST['txtLcode'];
    $authenticateOTP = authenticateOTPbyEXC($aadhaar_no, $txnID, $otp);

    if ($authenticateOTP['status'] == 'Y') {

        $namew = trim($authenticateOTP['LName']);

        $dobw = trim($authenticateOTP['LDOB']);
        $newDOB = date("Y-m-d", strtotime($dobw));
    //    $Aadharno = $authenticateOTP['Aadharno'];
$Aadharno = $aadhaar_no;
        $_DataTable = array();

        $_Datatable = array("learnername" => $namew,
            "learnerdob" => $newDOB,
            "learneraadhar" => $Aadharno,
            "status" => 'y');

        echo json_encode($_Datatable);
    }else {
		$message=$authenticateOTP['message'];
		$_DataTable = array();
		$_Datatable = array("message" => $message,
            "status" => 'n');
       echo json_encode($_Datatable);
    }

}

function authenticateOTPbyEXC($aadhaar_no, $txnID, $otp) {

   // $URL = "http://lms.rkcl.co.in/UIDAIAPI/uidai/otp/auth/" . $aadhaar_no . "/" . $otp . "/" . $txnID;
	 //$URL="http://192.168.49.45/UIDAIAPI/uidai/otp/auth/".$aadhaar_no."/".$otp."/".$txnID;
	 $URL="http://192.168.164.202/UIDAIAPI/uidai/otp/auth/".$aadhaar_no."/".$otp."/".$txnID;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    $array = json_decode($output, true);

    if ($array["authresponse"]["auth"]["@status"] == 'Y') {
        $responseArray = [
            "status" => $array["authresponse"]["auth"]["@status"],
            "Aadharno" => $array["authresponse"]["UidData"]["@UUID"],
            "LName" => $array["authresponse"]["UidData"]["pi"]["@name"],
            "Fname" => $array["authresponse"]["UidData"]["pa"]["@co"],
            "Gender" => $array["authresponse"]["UidData"]["pi"]["@gender"],
            "LDOB" => $array["authresponse"]["UidData"]["pi"]["@dob"],
            "District" => $array["authresponse"]["UidData"]["pa"]["@dist"],
            "Tehsil" => $array["authresponse"]["UidData"]["pa"]["@loc"],
            "Pincode" => $array["authresponse"]["UidData"]["pa"]["@pc"]
        ];
    } else {
        $responseArray = [
            "status" => $array["authresponse"]["auth"]["@status"],
            "message" => $array["authresponse"]["message"]
        ];
    }


    return $responseArray;
}
