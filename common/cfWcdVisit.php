<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsWcdVisit.php';

$response = array();
$emp = new clsWcdVisit();


if ($_action == 'FILLAO') {
    $response = $emp->GetAOList();
    echo "<option value='' >Select ITGK Code</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Wcd_Intake_Center'] . ">" . $_Row['Wcd_Intake_Center'] . "</option>";
    }
}

if ($_action == "GETAODETAILS") {

    $response = $emp->GetDatabyCode($_POST["itgk"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example1' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>ITGK Code</th>";
    echo "<th style='20%'>ITGK Name</th>";
    echo "<th style='10%'>ITGK Email</th>";
    echo "<th style='10%'>ITGK Mobile</th>";
    echo "<th style='10%'>ITGK Address</th>";
    echo "<th style='10%'>ITGK District</th>";
    echo "<th style='10%'>ITGK Tehsil.</th>";
   
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	 echo "</div>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['User_LoginId'] . "</td>";
         echo "<td>" . $_Row['Organization_Name'] . "</td>";
         echo "<td>" . $_Row['User_EmailId'] . "</td>";
         echo "<td>" . $_Row['User_MobileNo'] . "</td>";
         echo "<td>" . $_Row['Organization_Address'] . "</td>";
         echo "<td>" . $_Row['Organization_District_Name'] . "</td>";
         echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";
        
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}

if ($_action == "ADDVISIT") {
    if (isset($_POST["AOCode"]) && !empty($_POST["Date"])) {
        $_AOCode = $_POST["AOCode"];
        $_Date=$_POST["Date"];

        $response = $emp->AddVisit($_AOCode,$_Date);
        echo $response[0];
    }
}

