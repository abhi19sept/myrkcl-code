<?php
	class generalFunctions 
	{
		/**
	     * Returns an DOCUMENT_ROOT Path
	     */
		public function getDocRootDir() {
			//$rootdir = 'C:\Inetpub\vhosts\staging.myrkcl.com\httpdocs/myrkcl/'; 
			$rootdir = $_SERVER['DOCUMENT_ROOT'];
			if (isset($_SERVER['LOCAL_ADDR']) && $_SERVER['LOCAL_ADDR'] == '10.1.1.158') {
				$rootdir .= '/myrkcl';
			}
			
			return $rootdir;
		}

	 	/**
	     * Returns an encrypted & utf8-encoded
	     */
	    public function encrypt_with_key($pure_string, $encryption_key = "rkcluser") {
	        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	        $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);

	        return $encrypted_string;
	    }

	    /**
	     * Returns decrypted original string
	     */
	    public function decrypt_with_key($encrypted_string, $encryption_key = "rkcluser") {
	        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	        $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);

	        return rtrim($decrypted_string, "\0");
	    }

	    /**
	     * Returns decrypted original string
	     */
	    public function getTransactionItgkDetails($transactionId) {
	    	global $_ObjConnection;
	    //    $query = "SELECT pt.Pay_Tran_Status, pt.Pay_Tran_Code, pt.Pay_Tran_Amount, pt.Pay_Tran_ITGK, pt.Pay_Tran_Fname, pt.Pay_Tran_PG_Trnid, pt.Pay_Tran_Batch, pt.Pay_Tran_Course, pt.Pay_Tran_ProdInfo, IF(pt.Pay_Tran_ProdInfo LIKE('%correct%'), 'Correction Fee Payment', IF(pt.Pay_Tran_ProdInfo LIKE('%duplic%'), 'Duplicate Correction Fee', IF(pt.Pay_Tran_ProdInfo LIKE('%exam%'), 'Re-Exam Fee Payment', IF(pt.Pay_Tran_ProdInfo LIKE('%learner%'), 'Learner Fee Payment', IF(pt.Pay_Tran_ProdInfo LIKE('%Change%'), pt.Pay_Tran_ProdInfo, IF(pt.Pay_Tran_ProdInfo LIKE('%eoi%'), pt.Pay_Tran_ProdInfo, IF(pt.Pay_Tran_ProdInfo LIKE('%ncr%'), 'NCR Fee Payment', 'Process to Payment'))))))) AS payfor, pt.razorpay_order_id, um.User_EmailId, um.User_MobileNo, od.Organization_Name, od.Organization_Address FROM tbl_payment_transaction pt INNER JOIN tbl_user_master um ON um.User_LoginId = pt.Pay_Tran_ITGK INNER JOIN tbl_organization_detail od ON od.Organization_User = um.User_Code WHERE pt.Pay_Tran_PG_Trnid = '" . $transactionId . "'";
		
		$query = "SELECT pt.Pay_Tran_Status, pt.Pay_Tran_Code, pt.Pay_Tran_Amount, pt.Pay_Tran_ITGK, pt.Pay_Tran_Fname, pt.Pay_Tran_PG_Trnid, 
pt.Pay_Tran_Batch, pt.Pay_Tran_Course, pt.Pay_Tran_ProdInfo, IF(pt.Pay_Tran_ProdInfo LIKE('%correct%'),
 'Correction Fee Payment', IF(pt.Pay_Tran_ProdInfo LIKE('%duplic%'), 'Duplicate Correction Fee', 
IF(pt.Pay_Tran_ProdInfo LIKE('%exam%'), 'Re-Exam Fee Payment',IF(pt.Pay_Tran_ProdInfo LIKE('%AadharUpdFeePayment%'), 'Correction Before Exam Fee Payment',
IF(pt.Pay_Tran_ProdInfo LIKE('%learner%'), 
'Learner Fee Payment', IF(pt.Pay_Tran_ProdInfo LIKE('%Change%'), pt.Pay_Tran_ProdInfo, IF(pt.Pay_Tran_ProdInfo LIKE('%eoi%'), 
pt.Pay_Tran_ProdInfo, IF(pt.Pay_Tran_ProdInfo LIKE('%ncr%'), 'NCR Fee Payment',
IF(pt.Pay_Tran_ProdInfo LIKE('%owner%'), 'Ownership Fee Payment', IF(pt.Pay_Tran_ProdInfo LIKE('%Penalty%'), 
'Penalty Fee Payment', 'Process to Payment')))))))))) AS payfor, 
pt.razorpay_order_id, um.User_EmailId, um.User_MobileNo, od.Organization_Name, od.Organization_Address 
FROM tbl_payment_transaction pt INNER JOIN tbl_user_master um ON um.User_LoginId = pt.Pay_Tran_ITGK INNER JOIN
 tbl_organization_detail od ON od.Organization_User = um.User_Code WHERE pt.Pay_Tran_PG_Trnid = '" . $transactionId . "'";
		
	        $_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
	        $result = [];
	        if (mysqli_num_rows($_Response[2])) {
	        	$result = mysqli_fetch_array($_Response[2]);
	        }

	        return $result;
	    }

	    public function saveRazorPayOrderId($orderId, $razorpayOrderId, $razorpayPaymentId = '', $razorpaySignature = '') {
	    	global $_ObjConnection;
	    	$updateQuery = "UPDATE tbl_payment_transaction SET razorpay_order_id = '" . $razorpayOrderId . "', paymentgateway = 'Razorpay', razorpay_payment_id = '" . $razorpayPaymentId . "', razorpay_payment_signature = '" . $razorpaySignature . "' , Pay_Tran_Status = 'PaymentInProcess' WHERE Pay_Tran_Code = '" . $orderId . "'";
	    	$_Response = $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	    }

	    public function verifyRozorpayTransactionResponse($orderId, $amount) {
	    	global $_ObjConnection;
	    	$id = 0;
	    	$query = "SELECT Pay_Tran_PG_Trnid FROM tbl_payment_transaction WHERE Pay_Tran_Code = '" . $orderId . "' AND Pay_Tran_Amount = '" . $amount . "'";
	    	$_Response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($_Response[2])) {
	        	$row = mysqli_fetch_array($_Response[2]);
	        	$id = $row['Pay_Tran_PG_Trnid'];
	        }

	        return $id;
	    }
	}

	$general = new generalFunctions();
	
?>