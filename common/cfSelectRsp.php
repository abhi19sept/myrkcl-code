<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsSelectRsp.php';

$response = array();
$emp = new clsSelectRsp();

if ($_action == "ADD") {
    //print_r($_POST);
    if (isset($_POST["ddlRsp"])) {
        $_Rspcode = $_POST["ddlRsp"];
        $_Rspname = $_POST["txtName1"];
        $_Rspregno = $_POST["txtRegno"];
        $_Rspestdate = $_POST["txtEstdate"];
        $_Rsptype = $_POST["txtType"];
        $_Rspdistrict = $_POST["txtDistrict"];
        $_Rsptehsil = $_POST["txtTehsil"];
        $_Rspstreet = $_POST["txtStreet"];
        $_Rsproad = $_POST["txtRoad"];


        $response = $emp->Add($_Rspcode, $_Rspname, $_Rspregno, $_Rspestdate, $_Rsptype, $_Rspdistrict, $_Rsptehsil, $_Rspstreet, $_Rsproad);
        echo $response[0];
    }
}

if ($_action == "DETAILS") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {

        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['User_MobileNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "district" => $_Row['Organization_District'],
            "tehsil" => $_Row['Organization_Tehsil'],
            "street" => $_Row['Organization_Street'],
            "road" => $_Row['Organization_Road']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "SENDOTP") {
    $response = $emp->SendOTP($_POST['values']);
    echo $response[0];
}


//$_POST["action"]
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select RSP Name</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Organization_User'] . ">" . strtoupper($_Row['Organization_Name']) . "</option>";
    }
}


if ($_action == "VERIFY") {
    //print_r($_POST);
    if (isset($_POST["email"])) {
        $_Email = $_POST["email"];
        $_Mobile = $_POST["mobile"];
        $_Otp = $_POST["opt"];

        $response = $emp->Verify($_Email, $_Mobile, $_Otp);
        echo $response[0];
    }
}