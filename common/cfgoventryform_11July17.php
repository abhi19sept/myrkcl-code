<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsgoventryform.php';
####################
ini_set("display_errors", "on");
ini_set("error_reporting", E_ERROR);
require_once("../nusoap/nuSOAP/lib/nusoap.php");
define("SERVER_WSDL_API", "http://103.203.137.17:8081/DepartmentWebService.asmx?wsdl");
####################
    $response = array();
    $emp = new clsgoventryform();
    if ($_action == "ADD") {
		
        if (isset($_POST["lcode"]) && !empty($_POST["lcode"]) && !empty($_POST["empname"]) && !empty($_POST["faname"]) && !empty($_POST["empdistrict"])
			 && !empty($_POST["empmobile"]) && !empty($_POST["empemail"]) && !empty($_POST["empaddress"]) && !empty($_POST["empdeptname"])
			 && !empty($_POST["empid"]) && !empty($_POST["empgpfno"]) && !empty($_POST["empdesignation"]) && !empty($_POST["empmarks"])
			 && !empty($_POST["empexamattempt"]) && !empty($_POST["emppan"]) && !empty($_POST["empbankaccount"]) && !empty($_POST["empbankdistrict"])
			 && !empty($_POST["empbankname"]) && !empty($_POST["empbranchname"]) && !empty($_POST["empifscno"]) && !empty($_POST["empmicrno"]))
			 {
				$_LearnerCode = $_POST["lcode"];
				$_EmpName = $_POST["empname"];
				$_FatherName = $_POST["faname"];
				$_EmpDistrict = $_POST["empdistrict"];
				$_EmpMobile = $_POST["empmobile"];
				$_EmpEmail = $_POST["empemail"];
				$_EmpAddress = $_POST["empaddress"];
				$_EmpDeptName = $_POST["empdeptname"];
				$_EmpId = $_POST["empid"];
				$_EmpGPF = $_POST["empgpfno"];
				$_EmpDesignation = $_POST["empdesignation"];
				$_EmpMarks = $_POST["empmarks"];
				$_EmpExamAttempt = $_POST["empexamattempt"];
				$_EmpPan = $_POST["emppan"];
				$_EmpBankAccount = $_POST["empbankaccount"];
				$_EmpBankDistrict = $_POST["empbankdistrict"];
				$_EmpBankName = $_POST["empbankname"];
				$_EmpBranchName = $_POST["empbranchname"];
				$_EmpIFSC = $_POST["empifscno"];
				$_EmpMICR = $_POST["empmicrno"];
				$_fileReceipt = $_POST["fileReceipt"];
				$_fileBirth = $_POST["fileBirth"];
				$_EmpDobProof = $_POST["empdobproof"];
			
            $response = $emp->Add($_LearnerCode, $_EmpName, $_FatherName, $_EmpDistrict, $_EmpMobile, $_EmpEmail, $_EmpAddress, $_EmpDeptName, 
								  $_EmpId , $_EmpGPF, $_EmpDesignation, $_EmpMarks, $_EmpExamAttempt, $_EmpPan, $_EmpBankAccount, $_EmpBankDistrict,
								  $_EmpBankName, $_EmpBranchName, $_EmpIFSC, $_EmpMICR, $_fileReceipt, $_fileBirth, $_EmpDobProof );
            echo $response[0];
        }
    }
    
    if ($_action == "UPDATE") {
        if (isset($_POST["name"])) {
            $_StatusName = $_POST["name"];
            $_StatusDescription = $_POST["description"];
            $_Status_Code=$_POST['code'];
            $response = $emp->Update($_Status_Code,$_StatusName, $_StatusDescription);
            echo $response[0];
        }
    }
    
    
    if ($_action == "EDIT") {


        $response = $emp->GetDatabyCode($_actionvalue);

        $_DataTable = array();
        $_i=0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("StatusCode" => $_Row['Status_Code'],
                "StatusName"=>$_Row['Status_Name'],
                "StatusDescription"=>$_Row['Status_Description']);
            $_i=$_i+1;
        }
        echo json_encode($_Datatable);
    }
    
    
    if ($_action == "DELETE") {


        $response = $emp->DeleteRecord($_actionvalue);

        echo $response[0];
    }


    if ($_action == "SHOW") {


        $response = $emp->GetAll();

        $_DataTable = "";

         echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='5%'>S No.</th>";
        echo "<th style='30%'>Name</th>";
        echo "<th style='55%'>Description</th>";
        echo "<th style='10%'>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Status_Name'] . "</td>";
            echo "<td>" . $_Row['Status_Description'] . "</td>";
            echo "<td><a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a>  <a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
		echo "</div>";
//echo $_DataTable;
    }
    
    /* Added the edit format in this function*/
    
    if ($_action == "FILL") {
        //print_r($_POST);die;
        if(isset($_POST['lcodevalues'])){
            $responseNew = $emp->CheckLearnerCodeStatus($_POST['lcodevalues']); ///for all   
        }else{
            $responseNew = $emp->CheckLearnerCodeStatus($_SESSION['User_LearnerCode']);// only for learner 
        }
        //echo "<pre>";print_r($responseNew);die;
        if($responseNew[0]=='Success')
        {
            $_Row1 = mysqli_fetch_array($responseNew[2],true);
            //echo "<pre>";print_r($_Row1);die;
            $ldistrict=$_Row1['ldistrictname'];
            $response = $emp->GetAll();
            echo "<option value=''>Select District</option>";
            while ($_Row = mysqli_fetch_array($response[2])) {
                if($ldistrict==$_Row['District_Name']){
                    $sel='selected="selected"';
                }else{
                    $sel=''; 
                }
            echo "<option value=" . $_Row['District_Name'] . " ".$sel." >" . $_Row['District_Name'] . "</option>";
           }
        }else{
            $response = $emp->GetAll();
            echo "<option value=''>Select District</option>";
            while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['District_Name'] . ">" . $_Row['District_Name'] . "</option>";
           }
        }
        
    }
    
    /* Added the edit format in this function*/
    
     if ($_action == "FILLEMPDEPARTMENT") {
        $response = $emp->GetEmpdepartment();
        echo "<option value=''>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['gdname'] . ">" . $_Row['gdname'] . "</option>";
            
        }
    }
    
    
     if ($_action == "FILLEMPDESIGNATION") {
        $response = $emp->GetEmpdesignation();
        echo "<option value=''>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['designationname'] . ">" . $_Row['designationname'] . "</option>";
            
        }
    }
    
    
     if ($_action == "FILLBANKDISTRICT") {
         
        if(isset($_POST['lcodevalues'])){
            $responseNew = $emp->CheckLearnerCodeStatus($_POST['lcodevalues']); ///for all   
        }else{
            $responseNew = $emp->CheckLearnerCodeStatus($_SESSION['User_LearnerCode']);// only for learner 
        }
        if($responseNew[0]=='Success')
        {
            $_Row1 = mysqli_fetch_array($responseNew[2],true);
            //echo "<pre>";print_r($_Row1);die;
            $ldistrict=$_Row1['bankdistrict'];
            $response = $emp->GetBankdistrict();
            echo "<option value=''>Select Bank District</option>";
            while ($_Row = mysqli_fetch_array($response[2])) {
                if($ldistrict==$_Row['District_Name']){
                    $sel='selected="selected"';
                }else{
                    $sel=''; 
                }
            echo "<option value=" . $_Row['District_Name'] . " ".$sel." >" . $_Row['District_Name'] . "</option>";
           }
        }
        else
        {
            $response = $emp->GetBankdistrict();
            echo "<option value=''>Select Bank District</option>";
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<option value='" . $_Row['District_Name'] . "'>" . $_Row['District_Name'] . "</option>";

            }
        }
    }
    
    
     if ($_action == "FILLBANKNAME") {
         ///echo "<pre>";print_r($_POST);die;
        if(isset($_POST['districtid']))
            {
                $response = $emp->GetBankname($_POST['districtid']);
                echo "<option value=''>Select Bank Name</option>";
                while ($_Row = mysqli_fetch_array($response[2])) {
                    echo "<option value='" . $_Row['bankname'] . "'>" . $_Row['bankname'] . "</option>";

                }
            }
        else
            {
            
                if(isset($_POST['lcodevalues'])){
                        $responseNew = $emp->CheckLearnerCodeStatus($_POST['lcodevalues']); ///for all   
                    }
                else{
                        $responseNew = $emp->CheckLearnerCodeStatus($_SESSION['User_LearnerCode']);// only for learner 
                    }
                //echo "<pre>";print_r($responseNew);die;
                if($responseNew[0]=='Success')
                    {
                        $_Row1 = mysqli_fetch_array($responseNew[2],true);
                        //echo "<pre>";print_r($_Row1);die;
                        //$ldistrict=$_Row1['gbankname'];
                        //$response = $emp->GetBankdistrict();
                        //echo "<option value=''>Select Bank Name2</option>";
                        $sel='selected="selected"';
                        echo "<option value=" . $_Row1['gbankname'] . " ".$sel." >" . $_Row1['gbankname'] . "</option>";
                       
                    }
            
            
            }
            
        }
    
   
     if ($_action == "FillDobProof") {
        $response = $emp->GetDobProof();
        echo "<option value=''>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['cdocname'] . ">" . $_Row['cdocname'] . "</option>";            
        }
    }
	
	if ($_action == "DETAILS") {
        $response = $emp->GetAllDETAILS($_POST['values']);
		//print_r($response[2]);
        $_DataTable = array();
        $_i=0;
		$co = mysqli_num_rows($response[2]);
		if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("Admission_Name" => $_Row['Admission_Name'],
                "Admission_Fname"=>$_Row['Admission_Fname'],
                "Admission_Mobile"=>$_Row['Admission_Mobile'],
				"Admission_Email"=>$_Row['Admission_Email'],
				"Admission_Address"=>$_Row['Admission_Address'],
				"Admission_GPFNO"=>$_Row['Admission_GPFNO']);
            $_i=$_i+1;
        }
         echo json_encode($_Datatable);
		}
		else {
        echo "";
    }
    }
    
    /*Added new function for Exam attempt*/
    
     if ($_action == "FILLEXAMATTEMPT") {
        
        
        if(isset($_POST['lcodevalues'])){
            $responseNew = $emp->CheckLearnerCodeStatus($_POST['lcodevalues']); ///for all   
        }else{
            $responseNew = $emp->CheckLearnerCodeStatus($_SESSION['User_LearnerCode']);// only for learner 
        }
        //echo "<pre>";print_r($responseNew);die;
        if($responseNew[0]=='Success')
        {
            $_Row1 = mysqli_fetch_array($responseNew[2],true);
            $aattempt=$_Row1['aattempt'];
           
                if($aattempt==1){
                    $sel='selected="selected"';
                }else{
                    $sel=''; 
                }
                if($aattempt==2){
                    $sel2='selected="selected"';
                }else{
                    $sel2=''; 
                }
                echo "<option value=''>Select Attempt</option>";
                echo "<option value='1' ".$sel.">1</option>";
                echo "<option value='2' ".$sel2.">2</option>";
        
        }
        else
        {
                echo "<option value=''>Select Attempt</option>";
                echo "<option value='1'>1</option>";
                echo "<option value='2'>2</option>";
        }
    }
    
	if ($_action == "FILLREFEE"){
        
                    $responseLearnerBatch = $emp->GetLearnerBatch($_POST['txtLCode']);
                    $_RowLearnerBatch = mysqli_fetch_array($responseLearnerBatch[2],true);
                    //echo "<pre>";print_r($_RowLearnerBatch);die;
                             
                                $_DataTable = array();
				
				$incentive="";
				
				$_attempt = $_POST['examattepmt'];
				if($_attempt=='1')
                                    {
					$incentive = $_RowLearnerBatch['Govt_Incentive'];
                                    }
				else{
					$incentive=0;
                                    }				  
				  
				$_DataTable[0] = array("Fee" => $_RowLearnerBatch['Govt_Fee'],
                                                         "Incentive" => $incentive);        
				
				
				echo json_encode($_DataTable);  
        
    }
	
	
	
	
    /* For fee Reimbursement amount*/
    if ($_action == "FILLREFEE2"){
        //echo "<pre>";print_r($_POST);
        $response = $emp->FILLGovFee($_POST['txtLCode']);
        //echo "<pre>";print_r($_POST);
                                $_DataTable = array();
				$_i = 0;
				$_Row = mysqli_fetch_array($response[2]);
				$incentive="";
				
				$_attempt = $_POST['examattepmt'];
				if($_attempt=='1'){
							$incentives = ($_Row['Admission_Fee']*25/100);
							$incentive = round($incentives);
						 }
				else{
							$incentive=0;
						 }				  
				  
				$_DataTable[$_i] = array("Fee" => $_Row['Admission_Fee'],
                                                         "Incentive" => $incentive);        
				$_i = $_i + 1;
				
				echo json_encode($_DataTable);  
        
    }
    
    
   /*Added new function for Exam attempt*/
    
    
    /*Added new function for */
    
      /* Added By SUNIL: 2-2-2017 for checking the course of learner */
    
    if ($_action == "DETAILSNEW") {
        $responseNew = $emp->checkLearnerCourse($_POST['values']);
        //print_r($responseNew);//die;
        if($responseNew[0]=='Success')
            {
                //
                $response = $emp->GetAllDETAILS($_POST['values']);
                        //print_r($response[2]);
                $_DataTable = array();
                $_i=0;
                $co = mysqli_num_rows($response[2]);
                if ($co) {
                        while ($_Row = mysqli_fetch_array($response[2])) {
                            $_Datatable[$_i] = array("Admission_Name" => $_Row['Admission_Name'],
                                "Admission_Fname"=>$_Row['Admission_Fname'],
                                "Admission_Mobile"=>$_Row['Admission_Mobile'],
                                                "Admission_Email"=>$_Row['Admission_Email'],
                                                "Admission_Address"=>$_Row['Admission_Address'],
                                                "Admission_GPFNO"=>$_Row['Admission_GPFNO']);
                            $_i=$_i+1;
                        }
                        echo json_encode($_Datatable);
                        }
                else    {
                echo "";
                }
            }
        else{
            echo "NOE"; // no emplyee course found
        }
                
    }
    
        /* Added By SUNIL: 2-2-2017 for checking the course of learner */
    
    
    
    
    
        /* Added By SUNIL: 7-7-2017 for checking the course of learner */
    
    if ($_action == "DETAILSEMP") {
        $responseNew = $emp->checkLearnerCourse($_POST['values']);
        //print_r($_POST);die;
        if($responseNew[0]=='Success')
            {
            //// included file on the upper side of this file///
                $client = new nusoap_client(SERVER_WSDL_API, 'WSDL');
                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }
               // $EmployeeID = "RJJP198919006250";
                $EmployeeID = $_POST['EmpId'];

                $result = $client->call('Authenticate', array('EmployeeID' => $EmployeeID), '', '', false, true);
                //echo '<h2>Result-EMP</h2><pre>';
                $error = $client->getError();
                if ($error) {
                    //print_r($client->response);
                    //print_r($client->getDebug());
                    //die();
                     echo "APIERROR";
                 }
                elseif($result['AuthenticateResult']['diffgram']=='')
                {
                    
                    echo "WONGEMPID"; 
                }
                else
                 {
                    
                    $responseLearnerBatch = $emp->GetLearnerBatch($_POST['values']);
                    $_RowLearnerBatch = mysqli_fetch_array($responseLearnerBatch[2],true);
                    $_RowLearnerBatch['Batch_Name'];
                    $Batch_StartDate=$_RowLearnerBatch['Batch_StartDate'];
                    $batchYear = explode("-", $Batch_StartDate);
                    //echo $batchYear[0]; // learner batch year
                    
                    //echo $pieces[2]; // piece2
                    //echo "<pre>";print_r($_RowLearnerBatch);die;
                    $empdata=$result['AuthenticateResult']['diffgram']['NewDataSet']['Table'];
                    ///echo "<pre>";print_r($result);  // final result is coming in this part.
                 
                    //echo $empdata['DATE_OF_BIRTH'];
                    $date=substr($empdata['DATE_OF_BIRTH'],-2);
                    $month=substr($empdata['DATE_OF_BIRTH'],4,-2);
                    $year=substr($empdata['DATE_OF_BIRTH'],0,4);
                    $employeDOB=$date."-".$month."-".$year;
                    $end['DOB']=$employeDOB;
                    //$dob='1981-10-07';
                    // age compiare here 55. age should not be greater then 55.//  
                    
                    $batchCalculateYear='01-01-'.$batchYear[0];///echo "<br>";
                    $cage=calculateAge($employeDOB, $batchCalculateYear);///echo "<br>";
                    $pieces = explode("-", $cage);
                    //echo $pieces[0]; // day
                    //echo $pieces[1]; // month
                    //echo $pieces[2]; // year
                    
                    //die;
                   // $diff = (date('Y') - date('Y',strtotime($dob)));
                    
                    if($pieces[2] < 55)
                    {
                        $result = array_merge((array)$empdata, (array)$end);
                        if($result[SCH_TYPE]=='GPF')
                        {
                            $result[GPF_NO]=$result[GPF_NO];
                        }else{
                            $result[GPF_NO]=$result[PRAN];                           
                        }
                        echo json_encode($result);// final 
                    }
                    else
                    {
                       echo "AGE";
                    }
                 }
                 
            }
        elseif($responseNew[0]=='Already Exists')
            {
            $responseEMPid = $emp->checkLearnerEMPid($_POST['values'],$_POST['EmpId']);
            //print_r($responseEMPid);die;
            if($responseEMPid[0]=='Success')
                {
                    $responseStatus = $emp->checkLearnerStatus($_POST['values']);
                    //print_r($responseStatus);die;
                    if($responseStatus[0]=='Success')
                    {
                        echo "AMNTREM"; // Amount Reimbursed to Beneficiary's Account
                    }
                    else
                    {
                        //echo "ALE"; // emplyee for pending process
                        $responseStatus = $emp->checkLearnerStatusZero($_POST['values']);
                        //print_r($responseStatus);die;
                        if($responseStatus[0]=='Success')
                        {
                            echo "ABYRKCL"; // Approved by RKCL
                        }
                        else
                        {
                            echo "ALE"; // emplyee for pending process
                        }
                        
                       
                    }
                }
                else
                {
                    echo "WONGEMPIDDB"; // worng emplyee ID
                }
            }
        else{
            echo "NOE"; // no emplyee course found
        }
                
    }
    
        /* Added By SUNIL: 7-3-2017 for checking the course of learner */
     if ($_action == "SENDLCODETONEXTPAGE2") {
        $responseStatus = $emp->checkLearnerStatus($_POST['values']);
                //print_r($responseStatus);die;
        if($responseStatus[0]=='Success')
        {
                    echo "AMNTREM"; // Amount Reimbursed to Beneficiary's Account
        }
        else
        { 
         $responseNew = $emp->checkLearnerCode($_POST['values']);
        //print_r($responseNew);die;
            echo $responseNew[0];
        }
    }
    
    if ($_action == "SENDLCODETONEXTPAGE") {
        $responseStatusL = $emp->checkLearnerCode($_POST['values']);
        if($responseStatusL[0]=='Success')
        {
            $responseStatus = $emp->checkLearnerStatus($_POST['values']);
                    //print_r($responseStatus);die;
            if($responseStatus[0]=='Success')
            {
                        echo "AMNTREM"; // Amount Reimbursed to Beneficiary's Account
            }
            else
            {
                $responseNew12 = $emp->GetCheckEditEMPStatus12($_POST['values']);
                if($responseNew12[0]=='Success')
                    {
                      echo "NOE12";  
                    }
                else
                    {
                     $responseNew = $emp->GetCheckEditEMPStatus($_POST['values']);
                    //print_r($responseNew);die;

                        if($responseNew[0]=='Success')
                        { 
                            echo $responseNew[0];

                        }
                        else
                        {
                            echo "INVALID";
                        }
                    }
            }
        }
        else
        {
            echo "ERROR";
        }
    }
    
        /* Added By SUNIL: 14-3-2017 for checking the course of learner */
    
    
        /* Added By SUNIL: 29-5-2017 for checking the course of learner */
    if ($_action == "REJECTIONPAGE") {
        $responseStatusL = $emp->checkLearnerCode($_POST['values']);
        if($responseStatusL[0]=='Success')
        {
            $responseStatus = $emp->checkLearnerStatus($_POST['values']);
                    //print_r($responseStatus);die;
            if($responseStatus[0]=='Success')
            {
                        echo "AMNTREM"; // Amount Reimbursed to Beneficiary's Account
            }
            else
            {
                $responseNew12 = $emp->GetCheckEditEMPStatus12($_POST['values']);
                if($responseNew12[0]=='Success')
                    {
                      echo "NOE12";  
                    }
                else
                    {
                     $responseNew = $emp->GetCheckRejEMPStatus($_POST['values']);
                    //print_r($responseNew);die;

                        if($responseNew[0]=='Success')
                        { 
                            echo $responseNew[0];

                        }
                        else
                        {
                            echo "INVALID";
                        }
                    }
            }
        }
        else
        {
            echo "ERROR";
        }
    }
        /* Added By SUNIL: 29-5-2017 for checking the course of learner */
    
    
    if ($_action == "DETAILSEDITEMP") {
        $responseNew = $emp->GetCheckEditEMPStatus($_POST['values']);
        //print_r($_POST);die;
        if($responseNew[0]=='Success')
            {
            
                $client = new nusoap_client(SERVER_WSDL_API, 'WSDL');
                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }
                $EmployeeID = $_POST['EmpId'];

                $result = $client->call('Authenticate', array('EmployeeID' => $EmployeeID), '', '', false, true);
                //echo '<h2>Result-EMP</h2><pre>';
                $error = $client->getError();
                if ($error) 
                 {
                     echo "APIERROR";
                 }
                elseif($result['AuthenticateResult']['diffgram']=='')
                {
                    echo "WONGEMPID"; 
                }
                else
                 {
                    $responseLearnerBatch = $emp->GetLearnerBatch($_POST['values']);
                    $_RowLearnerBatch = mysqli_fetch_array($responseLearnerBatch[2],true);
                    $_RowLearnerBatch['Batch_Name'];
                    $Batch_StartDate=$_RowLearnerBatch['Batch_StartDate'];
                    $batchYear = explode("-", $Batch_StartDate);
                    //echo $batchYear[0]; // learner batch year
                    
                    //echo $pieces[2]; // piece2
                    //echo "<pre>";print_r($_RowLearnerBatch);die;
                    $empdata=$result['AuthenticateResult']['diffgram']['NewDataSet']['Table'];
                    ///echo "<pre>";print_r($result);  // final result is coming in this part.
                 
                    //echo $empdata['DATE_OF_BIRTH'];
                    $date=substr($empdata['DATE_OF_BIRTH'],-2);
                    $month=substr($empdata['DATE_OF_BIRTH'],4,-2);
                    $year=substr($empdata['DATE_OF_BIRTH'],0,4);
                    $employeDOB=$date."-".$month."-".$year;
                    $end['DOB']=$employeDOB;
                    //$dob='1981-10-07';
                    // age compiare here 55. age should not be greater then 55.//  
                    
                    $batchCalculateYear='01-01-'.$batchYear[0];///echo "<br>";
                    $cage=calculateAge($employeDOB, $batchCalculateYear);///echo "<br>";
                    $pieces = explode("-", $cage);
                    if($pieces[2] < 55)
                    {
                        $result = array_merge((array)$empdata, (array)$end);
                        //print_r($result);die;
                        $_Row1 = mysqli_fetch_array($responseNew[2],true);
                        //print_r($_Row1);die;

                        $resultFinal = array_merge((array)$result, (array)$_Row1);
                        //print_r($resultFinal);die;
                        if($resultFinal[SCH_TYPE]=='GPF')
                        {
                            $resultFinal[GPF_NO]=$resultFinal[GPF_NO];
                        }else{
                            $resultFinal[GPF_NO]=$resultFinal[PRAN];                           
                        }
                        echo json_encode($resultFinal);// final
                    }
                    else
                    {
                       echo "AGE";
                    }
                 }
            //die;
            
                    
              
            }
        else{
            $responseNew12 = $emp->GetCheckEditEMPStatus12($_POST['values']);
            if($responseNew12[0]=='Success')
            {
              echo "NOE12";  
            }
            else
            {
            echo "NOE"; // no emplyee course found
            }
        }
                
    }
    
    if ($_action == "DETAILSREJEMP") {
        $responseNew = $emp->GetCheckRejEMPStatus($_POST['values']);
        //print_r($_POST);die;
        if($responseNew[0]=='Success')
            {
            $client = new nusoap_client(SERVER_WSDL_API, 'WSDL');
                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }
                $EmployeeID = $_POST['EmpId'];

                $result = $client->call('Authenticate', array('EmployeeID' => $EmployeeID), '', '', false, true);
                //echo '<h2>Result-EMP</h2><pre>';
                $error = $client->getError();
                if ($error) 
                 {
                     echo "APIERROR";
                 }
                elseif($result['AuthenticateResult']['diffgram']=='')
                {
                    echo "WONGEMPID"; 
                }
                else
                 {
                    $responseLearnerBatch = $emp->GetLearnerBatch($_POST['values']);
                    $_RowLearnerBatch = mysqli_fetch_array($responseLearnerBatch[2],true);
                    $_RowLearnerBatch['Batch_Name'];
                    $Batch_StartDate=$_RowLearnerBatch['Batch_StartDate'];
                    $batchYear = explode("-", $Batch_StartDate);
                    //echo $batchYear[0]; // learner batch year
                    
                    //echo $pieces[2]; // piece2
                    //echo "<pre>";print_r($_RowLearnerBatch);die;
                    $empdata=$result['AuthenticateResult']['diffgram']['NewDataSet']['Table'];
                    ///echo "<pre>";print_r($result);  // final result is coming in this part.
                 
                    //echo $empdata['DATE_OF_BIRTH'];
                    $date=substr($empdata['DATE_OF_BIRTH'],-2);
                    $month=substr($empdata['DATE_OF_BIRTH'],4,-2);
                    $year=substr($empdata['DATE_OF_BIRTH'],0,4);
                    $employeDOB=$date."-".$month."-".$year;
                    $end['DOB']=$employeDOB;
                    //$dob='1981-10-07';
                    // age compiare here 55. age should not be greater then 55.//  
                    
                    $batchCalculateYear='01-01-'.$batchYear[0];///echo "<br>";
                    $cage=calculateAge($employeDOB, $batchCalculateYear);///echo "<br>";
                    $pieces = explode("-", $cage);
                    if($pieces[2] < 55)
                    {
                        $result = array_merge((array)$empdata, (array)$end);
                        //print_r($result);die;
                        $_Row1 = mysqli_fetch_array($responseNew[2],true);
                        //print_r($_Row1);die;

                        $resultFinal = array_merge((array)$result, (array)$_Row1);
                        //print_r($resultFinal);die;
                        if($resultFinal[SCH_TYPE]=='GPF')
                        {
                            $resultFinal[GPF_NO]=$resultFinal[GPF_NO];
                        }else{
                            $resultFinal[GPF_NO]=$resultFinal[PRAN];                           
                        }
                        echo json_encode($resultFinal);// final
                    }
                    else
                    {
                       echo "AGE";
                    }
                 }
            //die;  
            }
        else{
            $responseNew12 = $emp->GetCheckEditEMPStatus12($_POST['values']);
            if($responseNew12[0]=='Success')
            {
              echo "NOE12";  
            }
            else
            {
            echo "NOE"; // no emplyee course found
            }
        }
                
    }
    
        /* Added By SUNIL: 3-4-2017 for checking the course of learner */
    
    
    if ($_action == "EDITEMPRESUB") {
	//echo "<pre>";print_r($_POST);//die;	
        
        $response = $emp->ActMobValidate($_POST['lcode'],$_POST['empmobile'],$_POST['empbankaccount']);
                         //echo "<pre>";print_r($response);die;
                        if ($response[0]=='Success') 
                            {
                            $_Row1 = mysqli_fetch_array($response[2],true);
                             //echo "<pre>";print_r($_Row1);die;                                           
                                    if($_Row1['empaccountno']==$_POST['empbankaccount'] && $_Row1['lmobileno']==$_POST['empmobile'])
                                        {
                                            echo "MOBACNT";
                                        }
                                    elseif($_Row1['lmobileno']==$_POST['empmobile'])
                                        {
                                            echo "MOB";
                                        }
                                      else
                                        {
                                            echo "ACNT";
                                        }
                               
                            
                            }
                        else{
        if (isset($_POST["lcode"]) && !empty($_POST["lcode"]) && !empty($_POST["empname"]) && !empty($_POST["faname"]) && !empty($_POST["empdistrict"])
			 && !empty($_POST["empmobile"]) && !empty($_POST["empemail"]) && !empty($_POST["empaddress"]) && !empty($_POST["empdeptname"])
			 && !empty($_POST["empid"]) && !empty($_POST["empgpfno"]) && !empty($_POST["empdesignation"]) && !empty($_POST["empmarks"])
			 && !empty($_POST["empexamattempt"]) && !empty($_POST["empfee"])  && !empty($_POST["emptotalamt"])
                         && !empty($_POST["empbankaccount"]) && !empty($_POST["empbankdistrict"])
			 && !empty($_POST["empbankname"]) && !empty($_POST["empbranchname"]) && !empty($_POST["empifscno"]) && !empty($_POST["empmicrno"]) && !empty($_POST["empdob"]))
			 {
				$_LearnerCode = $_POST["lcode"];
				$_EmpName = $_POST["empname"];
				$_FatherName = $_POST["faname"];
				$_EmpDistrict = $_POST["empdistrict"];
				$_EmpMobile = $_POST["empmobile"];
				$_EmpEmail = $_POST["empemail"];
				$_EmpAddress = $_POST["empaddress"];
				$_EmpDeptName = $_POST["empdeptname"];
				$_EmpId = $_POST["empid"];
				$_EmpGPF = $_POST["empgpfno"];
				$_EmpDesignation = $_POST["empdesignation"];
				$_EmpMarks = $_POST["empmarks"];
				$_EmpExamAttempt = $_POST["empexamattempt"];
				$_EmpPan = $_POST["emppan"];
				$_EmpBankAccount = $_POST["empbankaccount"];
				$_EmpBankDistrict = $_POST["empbankdistrict"];
				$_EmpBankName = $_POST["empbankname"];
				$_EmpBranchName = $_POST["empbranchname"];
				$_EmpIFSC = $_POST["empifscno"];
				$_EmpMICR = $_POST["empmicrno"];
//				$_fileReceipt = $_POST["fileReceipt"];
//				$_fileBirth = $_POST["fileBirth"];
//				$_EmpDobProof = $_POST["empdobproof"];
                                $_EmpFee = $_POST["empfee"];
				$_EmpIncentive = $_POST["empincentive"];
				$_EmpTotalAmt = $_POST["emptotalamt"];
                                $_EmpDob = $_POST["empdob"];
                                $_fileReceipt = $_POST["fileReceipt"];
				$_fileDPLetter = $_POST["empdpletter"];
				$_fileRSCITCer = $_POST["emprscitcer"];
			
            $response = $emp->EditEmpReSub($_LearnerCode, $_EmpName, $_FatherName, $_EmpDistrict, $_EmpMobile, $_EmpEmail, $_EmpAddress, $_EmpDeptName, 
								  $_EmpId , $_EmpGPF, $_EmpDesignation, $_EmpMarks, $_EmpExamAttempt, $_EmpFee, $_EmpIncentive, $_EmpTotalAmt, $_EmpPan, $_EmpBankAccount, $_EmpBankDistrict,
								  $_EmpBankName, $_EmpBranchName, $_EmpIFSC, $_EmpMICR, $_EmpDob, $_fileReceipt, $_fileDPLetter, $_fileRSCITCer );
            echo $response[0];
        }
                        }
    }
    
    
    
    if ($_action == "EDITEMP") {
	//echo "<pre>";print_r($_POST);//die;	
        
        $response = $emp->ActMobValidate($_POST['lcode'],$_POST['empmobile'],$_POST['empbankaccount']);
                         //echo "<pre>";print_r($response);die;
                        if ($response[0]=='Success') 
                            {
                            $_Row1 = mysqli_fetch_array($response[2],true);
                             //echo "<pre>";print_r($_Row1);die;                                           
                                    if($_Row1['empaccountno']==$_POST['empbankaccount'] && $_Row1['lmobileno']==$_POST['empmobile'])
                                        {
                                            echo "MOBACNT";
                                        }
                                    elseif($_Row1['lmobileno']==$_POST['empmobile'])
                                        {
                                            echo "MOB";
                                        }
                                      else
                                        {
                                            echo "ACNT";
                                        }
                            
                            
                            }
                        else{
        
        if (isset($_POST["lcode"]) && !empty($_POST["lcode"]) && !empty($_POST["empname"]) && !empty($_POST["faname"]) && !empty($_POST["empdistrict"])
			 && !empty($_POST["empmobile"]) && !empty($_POST["empemail"]) && !empty($_POST["empaddress"]) && !empty($_POST["empdeptname"])
			 && !empty($_POST["empid"]) && !empty($_POST["empgpfno"]) && !empty($_POST["empdesignation"]) && !empty($_POST["empmarks"])
			 && !empty($_POST["empexamattempt"]) && !empty($_POST["empfee"])  && !empty($_POST["emptotalamt"])
                         && !empty($_POST["empbankaccount"]) && !empty($_POST["empbankdistrict"])&& !empty($_POST["empbankname"]) 
			 && !empty($_POST["empbranchname"]) && !empty($_POST["empifscno"]) && !empty($_POST["empmicrno"]) && !empty($_POST["empdob"]))
			 {
				$_LearnerCode = $_POST["lcode"];
				$_EmpName = $_POST["empname"];
				$_FatherName = $_POST["faname"];
				$_EmpDistrict = $_POST["empdistrict"];
				$_EmpMobile = $_POST["empmobile"];
				$_EmpEmail = $_POST["empemail"];
				$_EmpAddress = $_POST["empaddress"];
				$_EmpDeptName = $_POST["empdeptname"];
				$_EmpId = $_POST["empid"];
				$_EmpGPF = $_POST["empgpfno"];
				$_EmpDesignation = $_POST["empdesignation"];
				$_EmpMarks = $_POST["empmarks"];
				$_EmpExamAttempt = $_POST["empexamattempt"];
				$_EmpPan = $_POST["emppan"];
				$_EmpBankAccount = $_POST["empbankaccount"];
				$_EmpBankDistrict = $_POST["empbankdistrict"];
				$_EmpBankName = $_POST["empbankname"];
				$_EmpBranchName = $_POST["empbranchname"];
				$_EmpIFSC = $_POST["empifscno"];
				$_EmpMICR = $_POST["empmicrno"];
//				$_fileReceipt = $_POST["fileReceipt"];
//				$_fileBirth = $_POST["fileBirth"];
//				$_EmpDobProof = $_POST["empdobproof"];
                                $_EmpFee = $_POST["empfee"];
				$_EmpIncentive = $_POST["empincentive"];
				$_EmpTotalAmt = $_POST["emptotalamt"];
                                $_EmpDob = $_POST["empdob"];
                                $_fileReceipt = $_POST["fileReceipt"];
				$_fileDPLetter = $_POST["empdpletter"];
				$_fileRSCITCer = $_POST["emprscitcer"];
			
            $response = $emp->EditEmp($_LearnerCode, $_EmpName, $_FatherName, $_EmpDistrict, $_EmpMobile, $_EmpEmail, $_EmpAddress, 
                                     $_EmpDeptName, $_EmpId , $_EmpGPF, $_EmpDesignation, $_EmpMarks, $_EmpExamAttempt, $_EmpFee, 
                                     $_EmpIncentive, $_EmpTotalAmt, $_EmpPan, $_EmpBankAccount, $_EmpBankDistrict, $_EmpBankName, 
                                     $_EmpBranchName, $_EmpIFSC, $_EmpMICR, $_EmpDob, $_fileReceipt, $_fileDPLetter, $_fileRSCITCer );
            
            echo $response[0];
        }
                        }
    }
    
    
    if ($_action == "ADDEMP") {
	//echo "<pre>";print_r($_POST);//die;/*&& !empty($_POST["emppan"]) */
        
        $response = $emp->ActMobValidate($_POST['lcode'],$_POST['empmobile'],$_POST['empbankaccount']);
                         //echo "<pre>";print_r($response);die;
                        if ($response[0]=='Success') 
                            {
                            $_Row1 = mysqli_fetch_array($response[2],true);
                             //echo "<pre>";print_r($_Row1);die;                                           
                                    if($_Row1['empaccountno']==$_POST['empbankaccount'] && $_Row1['lmobileno']==$_POST['empmobile'])
                                        {
                                            echo "MOBACNT";
                                        }
                                    elseif($_Row1['lmobileno']==$_POST['empmobile'])
                                        {
                                            echo "MOB";
                                        }
                                      else
                                        {
                                            echo "ACNT";
                                        }
                              
                            
                            }
                        else{
        
        
        if (isset($_POST["lcode"]) && !empty($_POST["lcode"]) && !empty($_POST["empname"]) && !empty($_POST["faname"]) && !empty($_POST["empdistrict"])
			 && !empty($_POST["empmobile"]) && !empty($_POST["empemail"]) && !empty($_POST["empaddress"]) && !empty($_POST["empdeptname"])
			 && !empty($_POST["empid"]) && !empty($_POST["empgpfno"]) && !empty($_POST["empdesignation"]) && !empty($_POST["empmarks"])
			 && !empty($_POST["empexamattempt"]) && !empty($_POST["empfee"]) && !empty($_POST["emptotalamt"])
                         && !empty($_POST["empbankaccount"]) && !empty($_POST["empbankdistrict"])&& !empty($_POST["empbankname"]) 
			 && !empty($_POST["empbranchname"]) && !empty($_POST["empifscno"]) && !empty($_POST["empmicrno"]) && !empty($_POST["empdob"]))
			 {
				$_LearnerCode = $_POST["lcode"];
				$_EmpName = $_POST["empname"];
				$_FatherName = $_POST["faname"];
				$_EmpDistrict = $_POST["empdistrict"];
				$_EmpMobile = $_POST["empmobile"];
				$_EmpEmail = $_POST["empemail"];
				$_EmpAddress = $_POST["empaddress"];
				$_EmpDeptName = $_POST["empdeptname"];
				$_EmpId = $_POST["empid"];
				$_EmpGPF = $_POST["empgpfno"];
				$_EmpDesignation = $_POST["empdesignation"];
				$_EmpMarks = $_POST["empmarks"];
				$_EmpExamAttempt = $_POST["empexamattempt"];
				$_EmpFee = $_POST["empfee"];
				$_EmpIncentive = $_POST["empincentive"];
				$_EmpTotalAmt = $_POST["emptotalamt"];
				$_EmpPan = $_POST["emppan"];
				$_EmpBankAccount = $_POST["empbankaccount"];
				$_EmpBankDistrict = $_POST["empbankdistrict"];
				$_EmpBankName = $_POST["empbankname"];
				$_EmpBranchName = $_POST["empbranchname"];
				$_EmpIFSC = $_POST["empifscno"];
				$_EmpMICR = $_POST["empmicrno"];
//				$_fileReceipt = $_POST["fileReceipt"];
//				$_fileBirth = $_POST["fileBirth"];
//				$_EmpDobProof = $_POST["empdobproof"];
                                $_EmpDob = $_POST["empdob"];
                                $_fileReceipt = $_POST["fileReceipt"];
				$_fileDPLetter = $_POST["empdpletter"];
				$_fileRSCITCer = $_POST["emprscitcer"];
                                /* Who has applied for Reimbursement */
                                //$response2 = $emp->GetUserRollName($_SESSION['User_UserRoll']);
                                //$_Row2 = mysqli_fetch_array($response2[2],true);
                                //$_appliedby=$_Row2['UserRoll_Name'];
                                /* Who has applied for Reimbursement */
                                
            $response = $emp->AddEmp($_LearnerCode, $_EmpName, $_FatherName, $_EmpDistrict, $_EmpMobile, $_EmpEmail, 
                                     $_EmpAddress, $_EmpDeptName, $_EmpId , $_EmpGPF, $_EmpDesignation, $_EmpMarks, 
                                     $_EmpExamAttempt, $_EmpFee, $_EmpIncentive, $_EmpTotalAmt, $_EmpPan, $_EmpBankAccount, 
                                     $_EmpBankDistrict, $_EmpBankName, $_EmpBranchName, $_EmpIFSC, $_EmpMICR, $_EmpDob, 
                                     $_fileReceipt, $_fileDPLetter, $_fileRSCITCer );
            echo $response[0];
        }
        
                        }
    }
    
    
    
    
    if ($_action == "EMPACKDETAILS") {
       //echo "<pre>";print_r($_POST);die;
        
       $responseStatus = $emp->checkLearnerStatus($_POST['values']);
                //print_r($responseStatus);die;
        if($responseStatus[0]=='Success')
        {
                    echo "AMNTREM"; // Amount Reimbursed to Beneficiary's Account
        }
        else
        { 
            $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_reimbursement/';
            //$file = $_SESSION['User_LearnerCode'] . '.fdf';

            if($_SESSION['User_UserRoll']==19){
                $learnerCode=$_SESSION['User_LearnerCode'];
                $file = $_SESSION['User_LearnerCode'] . '.fdf';
            }
            else{
                $learnerCode=$_POST['values'];
                $file = $_POST['values'] . '.fdf';
            }

            $filePath = $path . $file;
            $allreadyfoundAkcRecipt = $path . $learnerCode.'_PRE_AKN_PAYMENT_RECEIPT.pdf';
            //if (file_exists($allreadyfoundAkcRecipt))  
               // {
                    //echo "DONE" ; 
                //}
            //else{
            // if file is not there so come form here//
            
            $response = $emp->GetEMPACKDETAILS($_POST['values']);
	//echo "<pre>";print_r($_SESSION);
            $co = mysqli_num_rows($response[2]);
            if ($co) 
            {
               $_Row1 = mysqli_fetch_array($response[2],true);
               //echo "<pre>";print_r($_Row1);die;
               $_DataTable = array();
               
               $_Datatable["AdmissionName"] =  strtoupper($_Row1['Admission_Name']);
               $_Datatable["AdmissionFname"] =  strtoupper($_Row1['Admission_Fname']);
               $_Row1['Admission_Name']=strtoupper($_Row1['Admission_Name']);
               $_Row1['Admission_Fname']=strtoupper($_Row1['Admission_Fname']);
               
               $Admission_DOB = date("d-m-Y", strtotime($_Row1['Admission_DOB']));
               $_Datatable["Admission_DOB"] =  $Admission_DOB;
               $_Datatable["AdmissionDOB"] =  $Admission_DOB;
               
               $empdob = date("d-m-Y", strtotime($_Row1['empdob']));
               $_Datatable["empdob"] =  $empdob;
               
              
               
                
               if($_Row1['trnamount']!=''){
                    //$total = intval (trim($_Row1['trnamount']))+ intval (trim($_Row1['incentive']));
                    $_Datatable["inrs"]=$_Row1['trnamount'];
                    $inwords=convert_number_to_words($_Row1['trnamount']);
                    $_Datatable["inwords"]=$inwords;
               }
               $matched=0;
               
               $new_admissiname=trim(str_replace(' ','',$_Row1['Admission_Name']));
               $new_fname=trim(str_replace(' ','',$_Row1['fname']));//die;
               if($new_admissiname==$new_fname){
                  $_Datatable["remark1"]='Matched';
                  $mvalue1='';
               }else{
                  $_Datatable["remark1"]='Not Matched';
                  $matched=1;
                  $mvalue1='Name';
               }
               
               
               $new_admissiFname=trim(str_replace(' ','',$_Row1['Admission_Fname']));
               $new_faname=trim(str_replace(' ','',$_Row1['faname']));//die;
               if($new_admissiFname==$new_faname){
                   $_Datatable["remark2"]='Matched';
                   $mvalue2=''; 
               }else{
                    $_Datatable["remark2"]='Not Matched';
                    if($matched==1)
                        {
                            $mvalue2=' And Father / Husband Name ';
                            $matched=1;
                        }
                    else{
                            $mvalue2=' Father / Husband Name ';
                            $matched=1;
                        }
               
                    }
               
                    
               $NewAdmission_DOB=strtotime($_Row1['Admission_DOB']);
               $Newempdob=strtotime($_Row1['empdob']);
               if($NewAdmission_DOB==$Newempdob){
                  $mvalue3='';
                  $_Datatable["remark3"]='Matched';
               }else{
                  $_Datatable["remark3"]='Not Matched';
                  
                  if($matched==1){
                      $mvalue3=' And Date of Birth ';
                  }else{
                      $mvalue3=' Date of Birth ';
                      $matched=1;
                  }
               }
               
               if($matched==1)
                        {
                            if($mvalue1!='' && $mvalue2!='' && $mvalue3!='')
                                 {
                                      $_Datatable["matched"]=' All ';   
                                 }
                            else
                                 {
                                      $_Datatable["matched"]=$mvalue1.$mvalue2.$mvalue3;
                                 }

                            //$_Datatable["matched"]=' All ';
                        }
               
               
               $_Row1['officeaddress']=strtoupper($_Row1['officeaddress']);
               $_Row1['designation']=strtoupper($_Row1['designation']);
               $_Row1['gifsccode']=strtoupper($_Row1['gifsccode']);
               $_Row1['empgpfno']=strtoupper($_Row1['empgpfno']);
               //$_Row1['gbranchname']=strtoupper($_Row1['gbranchname']);
               
               $_Row = array_merge($_Row1, $_Datatable);
              //echo "<pre>";print_r($_Row);die;
               $fdfContent = '%FDF-1.2
			%âã�?Ó
			1 0 obj 
			<<
			/FDF 
			<<
			/Fields [';
			foreach ($_Row as $key => $val) {
				$fdfContent .= '
					<<
					/V (' . $val . ')
					/T (' . $key . ')
					>> 
					';
			}
			$fdfContent .= ']
			>>
			>>
			endobj 
			trailer
			<<
			/Root 1 0 R
			>>
			%%EOF';
                file_put_contents($filePath, $fdfContent);
               
                $fdfFilePath = $filePath;
		$fdfPath = str_replace('/', '//', $fdfFilePath);
		$resultFile = '';
		$defaulPermissionLetterFilePath = getPermissionLetterPath() . 'PRE_AKN_PAYMENT_RECEIPT.pdf';
		if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
			$defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);
			$resultFile = $learnerCode . '_PRE_AKN_PAYMENT_RECEIPT.pdf';
			$resultPath = str_replace('/', '//', getPermitionLetterFilePath($learnerCode));
                        $pdftkPath = ($_SERVER['HTTP_HOST']=="myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';

			$command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  '. $resultPath . ' flatten';
			exec($command);
			unlink($fdfFilePath);
                        echo "DONE" ; 
		}
                   
                //$permissionLetterFile = $resultFile;
		//$filePath = getPermissionLetterPath() . $permissionLetterFile;
                //downloadFile($resultFile);
        
        //die;
           }
            else 
             {
                echo "";
             }
            
       // }
        
        } 
	
    }
    
    /* Sunil CHnaged this  */
    if ($_action == "CHECKEMPRECIPTSHOW") {
        //echo "<pre>";print_r($_POST);die;
            $response = $emp->CheckLearnerCodeStatus($_POST['values']);
            /*/ in this function we are checking the learener has applied for reimbursment or not/*/
            $co = mysqli_num_rows($response[2]);
            if ($co) 
            {
        
                    $response = $emp->CHECKEMPRECIPT($_POST['values']);
                    //print_r($response);die;
                    //$co = mysqli_num_rows($response[2]);
                    if ($response[0]=='Success') 
                    {
                        echo "DONE" ;

                    }
                    else{

                        echo "NOEMPID";
                    }
            }
            else{
                echo "NORECD";
            }
    }
     /* Sunil Chnaged this and checking here that the image of recipt is blank or not */
    if ($_action == "CHECKEMPRECIPT") {
        $responseStatus = $emp->checkLearnerStatus($_POST['values']);
                //print_r($responseStatus);die;
        if($responseStatus[0]=='Success')
        {
                    echo "AMNTREM"; // Amount Reimbursed to Beneficiary's Account
        }
        else
        {
            $response = $emp->CheckLearnerCodeStatus($_POST['values']);
            /*/ in this function we are checking the learener has applied for reimbursment or not/*/
            $co = mysqli_num_rows($response[2]);
            if ($co) 
            {

                         $response = $emp->GetImageAckReceipt($_POST['values']);
                        // echo "<pre>";print_r($response);
                         if ($response[0]=='Success') 
                            {
                                    $_Row1 = mysqli_fetch_array($response[2],true);
                                                                        
                                    if($_Row1['attach1']!='')
                                        {
                                            echo $_Row1['attach1'];
                                        }
                                      else
                                        {
                                            echo "NOIMG";
                                        }
                                    

                            }
                       
              }
            else{
                echo "NORECD";
            }    
        }    
    }
    
    /* Sunil Chnaged this and checking here that the image of recipt is blank or not */  
    
    
    if ($_action == "UPDATEEMPRECIPT") {
        //echo "hwe";die;
        if (isset($_POST["EmpId"]) && $_POST["EmpId"]!='') 
            {
              //echo "hwe2";print_r($_POST);die;
            
                $client = new nusoap_client(SERVER_WSDL_API, 'WSDL');
                $error = $client->getError();
                if ($error) {
                    die("client construction error: {$error}\n");
                }
               // $EmployeeID = "RJJP198919006250";
                $EmployeeID = $_POST['EmpId'];

                $result = $client->call('Authenticate', array('EmployeeID' => $EmployeeID), '', '', false, true);
                //echo '<h2>Result-EMP</h2><pre>';
                $error = $client->getError();
                if ($error) {
                    //print_r($client->response);
                    //print_r($client->getDebug());
                    //die();
                     echo "APIERROR";
                 }
                elseif($result['AuthenticateResult']['diffgram']==''){
                    
                    echo "WONGEMPID"; 
                }
                else
                 { //echo "sunil"; die;
                    $_EmpId = $_POST["EmpId"];
                    $_learnercode = $_POST["values"];
                    $response = $emp->UpdateEmpRecipt($_EmpId,$_learnercode);
                    echo $response[0];
            
                 }
            
            
            
        }
        else{
            echo "NOEmpId";//die;
        }
    }
    
    if ($_action == "UPDATEAKCRECIPT") {
        //echo "<pre>"; print_r($_POST);die;
        if (isset($_POST["values"]) && !empty($_POST["values"])) {
            $_fileReceipt = $_POST["values"];
            $_learnercode = $_POST["values"];
            $response = $emp->UpdateEmpAkcRecipt($_fileReceipt,$_learnercode);
            echo $response[0];
        }
    }
    
    
    if ($_action == "CVALIDATE"){
        //echo "<pre>";print_r($_POST);//die;
        $response = $emp->ActMobValidate($_POST['lcode'],$_POST['empmobile'],$_POST['empbankaccount']);
                         //echo "<pre>";print_r($response);die;
                        if ($response[0]=='Success') 
                            {
                            $_Row1 = mysqli_fetch_array($response[2],true);
                             //echo "<pre>";print_r($_Row1);die;                                           
                                    if($_Row1['empaccountno']==$_POST['empbankaccount'] && $_Row1['lmobileno']==$_POST['empmobile'])
                                        {
                                            echo "MOBACNT";
                                        }
                                    elseif($_Row1['lmobileno']==$_POST['empmobile'])
                                        {
                                            echo "MOB";
                                        }
                                      else
                                        {
                                            echo "ACNT";
                                        }
                                //echo "Mobile Number OR Bank Account Number Allready Exits In Our Record. Please Try Again With Different Mobile Number OR Bank Account Number";
                                //echo "Mobile Number OR Bank Account Number Has Already Registered With Us, Please Try Again With Different Mobile Number OR Bank Account Number.";
                                //echo "RCDfOUND";
                            
                            }
                        else{
                                //echo $_POST['empmobile'];die;
                                $response = $emp->AddLernerMob($_POST['empmobile']);
                                $last4=substr($_POST['empmobile'],6);
                                echo $result = "OTP sent to your registered mobile number XXXXXX".$last4;
                                
                            }
                       
    }
    
    if ($_action == "OTPVALIDATE"){
        //echo "<pre>";print_r($_POST);die;
       if(isset($_POST['txtOTP']) && $_POST['txtOTP']!='')
           {
                 $_otp = $_POST["txtOTP"];
                 $response = $emp->verifyLotp($_POST['empmobile'],$_otp);

                      if($response=='InvalidOTP')
                        {

                         echo $response;

                        }
                     else
                        {
                         
                         echo "OTPMATCHED";
                         
                        }
           }
       else
       {
          echo "NOTP"; 
       }
                       
    }
    
    ################################33
    
    
        
     function calculateAge($date1,$date2)
        {
            // date2 should be greater then to date1
            $date1 = date_create($date1); //date of birth
            //creating a date object
            $date2 = date_create($date2); //current date

            $diff12 = date_diff($date2, $date1); 
                //accesing days
            $days = $diff12->d;
             //accesing months
            $months = $diff12->m;
             //accesing years
            $years = $diff12->y;

            return   $days . '-' . $months . '-' . $years;
        }   
           
    
    
    
    
    function convert_number_to_words($number) {

    $hyphen = '-';
    $conjunction = ' and ';
    $separator = ', ';
    $negative = 'negative ';
    $decimal = ' point ';
    $dictionary = array(
        0 => 'Zero',
        1 => 'One',
        2 => 'Two',
        3 => 'Three',
        4 => 'Four',
        5 => 'Five',
        6 => 'Six',
        7 => 'Seven',
        8 => 'Eight',
        9 => 'Nine',
        10 => 'Ten',
        11 => 'Eleven',
        12 => 'Twelve',
        13 => 'Thirteen',
        14 => 'Fourteen',
        15 => 'Fifteen',
        16 => 'Sixteen',
        17 => 'Seventeen',
        18 => 'Eighteen',
        19 => 'Nineteen',
        20 => 'Twenty',
        30 => 'Thirty',
        40 => 'Fourty',
        50 => 'Fifty',
        60 => 'Sixty',
        70 => 'Seventy',
        80 => 'Eighty',
        90 => 'Ninety',
        100 => 'Hundred',
        1000 => 'Thousand',
        1000000 => 'Million',
        1000000000 => 'Billion',
        1000000000000 => 'Trillion',
        1000000000000000 => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens = ((int) ($number / 10)) * 10;
            $units = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}
    
    
        function getPermitionLetterFilePath($code, $type) {
		$resultFile = ($type == 'center') ? $code . '_PRE_AKN_PAYMENT_RECEIPT.pdf' : $code . '_PRE_AKN_PAYMENT_RECEIPT.pdf';
		$resultPath = getPermissionLetterPath() . $resultFile;

		return $resultPath;
	}
        function getPermissionLetterPath() {
		$path = $_SERVER['DOCUMENT_ROOT'] . '/upload/government_reimbursement/';
		makeDir($path);

		return $path;
	}
        function makeDir($path)	{
	     return is_dir($path) || mkdir($path);
	}
        function buildDownloadFile($file) {
        if ($file) {
            $filename = basename($file);
            $file_extension = strtolower(substr(strrchr($filename, "."), 1));
            //This will set the Content-Type to the appropriate setting for the file
            switch ($file_extension) {
                case "pdf":
                    $ctype = "application/pdf";
                    break;
                case "exe":
                    $ctype = "application/octet-stream";
                    break;
                case "zip":
                    $ctype = "application/zip";
                    break;
                case "doc":
                    $ctype = "application/msword";
                    break;
                case "xls":
                    $ctype = "application/vnd.ms-excel";
                    break;
                case "csv":
                    $ctype = "text/csv";
                    break;
                case "ppt":
                    $ctype = "application/vnd.ms-powerpoint";
                    break;
                case "gif":
                    $ctype = "image/gif";
                    break;
                case "png":
                    $ctype = "image/png";
                    break;
                case "jpeg":
                case "jpg":
                    $ctype = "image/jpg";
                    break;
                case "mp3":
                    $ctype = "audio/mpeg";
                    break;
                case "wav":
                    $ctype = "audio/x-wav";
                    break;
                case "mpeg":
                case "mpg":
                case "mpe":
                    $ctype = "video/mpeg";
                    break;
                case "mov":
                    $ctype = "video/quicktime";
                    break;
                case "avi":
                    $ctype = "video/x-msvideo";
                    break;
                //The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
                case "php":
                case "htm":
                case "html":
                case "txt":
                    die("<b>Cannot be used for " . $file_extension . " files!</b>");
                    break;
                default:
                    $ctype = "application/force-download";
            }

            //Begin writing headers
            $header[] = "Pragma: public";
            $header[] = "Expires: 0";
            $header[] = "Cache-Control: must-revalidate, post-check=0, pre-check=0";
            $header[] = "Cache-Control: public";
            $header[] = "Content-Description: File Transfer";

            //Use the switch-generated Content-Type
            $header[] = "Content-Type: $ctype";

            //Force the download
            $header[] = "Content-Disposition: attachment; filename=\"" . $filename . "\";";

            return $header;
        }
    }
        function transferFile($source, $headers) {
        ob_end_clean();
        foreach ($headers as $header) {
            $header = preg_replace('/\r?\n(?!\t| )/', '', $header);
            header($header);
        }

        // Transfer file in 1024 byte chunks to save memory usage.
        $fd = fopen($source, 'rb');
        if (is_resource($fd)) {
            while (!feof($fd)) {
                print fread($fd, 1024);
            }
        }
        fclose($fd);
        exit();
    }
        function downloadFile($filePath) {
    	$headers = buildDownloadFile($filePath);
		if (is_array($headers)) {
            transferFile($filePath, $headers);
        }
    }

        
        
        /* Added By SUNIL: 14-3-2017 for checking the course of learner */
    
    
    
    
?>
