<?php

/*  clsNameAddressFeePayment.php
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsNameAddressFeePayment.php';


$response = array();
$emp = new clsNameAddressFeePayment();

if ($_action == "CheckCenter") {
    $response = $emp->Check_Center();
    if($response[0] == 'Success'){
        echo "2";
    } else {
        echo "1";
    } 
}

if ($_action == "GetPayEligibility") {
    // print_r($_POST);
    if ($_POST['payfor'] == '' || empty($_POST['payfor'])) {
        echo "1";
    } else {
        if ($_POST['payfor'] == "1") {
            $payfor = "name";
            $response = $emp->GetPayEligibility($_SESSION['User_LoginId'], $payfor);
            if ($response[0] == 'Success') {
                $_DataTable = array();
                $_i = 0;
                $_Row = mysqli_fetch_array($response[2]);

                $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                    "orgnameold" => $_Row['Organization_Name_old'],
                    "orgtype" => $_Row['Organization_Type'],
                    "orgtypeold" => $_Row['Organization_Type_old'],
                    "areatype" => $_Row['Organization_AreaType'],
                    "areatypeold" => $_Row['Organization_AreaType_old'],
                    "mtype" => $_Row['Organization_Municipal_Type'],
                    "mtypeold" => $_Row['Organization_Municipal_Type_old'],
                    "municipal" => $_Row['Organization_Municipal'],
                    "municipalold" => $_Row['Organization_Municipal_old'],
                    "wardnum" => $_Row['Organization_WardNo'],
                    "wardnumold" => $_Row['Organization_WardNo_old'],
                    "panchayat" => $_Row['Organization_Panchayat'],
                    "panchayatold" => $_Row['Organization_Panchayat_old'],
                    "gram" => $_Row['Organization_Gram'],
                    "gramold" => $_Row['Organization_Gram_old'],
                    "village" => $_Row['Organization_Village'],
                    "villageold" => $_Row['Organization_Village_old'],
                    "address" => $_Row['Organization_Address'],
                    "addressold" => $_Row['Organization_Address_old'],
                    "refid" => $_Row['fld_ref_no'],
                    "payfor" => $payfor);
                $_i = $_i + 1;

                echo json_encode($_DataTable);
            } else {
                echo "3";
            }
        } else if ($_POST['payfor'] == "2") {
            $payfor = "address";
            $response = $emp->GetPayEligibility($_SESSION['User_LoginId'], $payfor);
            if ($response[0] == 'Success') {
                $_DataTable = array();
                $_i = 0;
                $_Row = mysqli_fetch_array($response[2]);
                //if ($_Row['Organization_AreaType'] == 'Rural') {
                    $responseRural = $emp->GetOrgRuralDetails($payfor);
                    $_Row1 = mysqli_fetch_array($responseRural[2]);
                    $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                        "orgnameold" => $_Row['Organization_Name_old'],
                        "orgtype" => $_Row['Organization_Type'],
                        "orgtypeold" => $_Row['Organization_Type_old'],
                        "areatype" => $_Row['Organization_AreaType'],
                        "areatypeold" => $_Row['Organization_AreaType_old'],
                        "mtype" => $_Row1['new_mtype'],
                        "mtypeold" => $_Row1['old_mtype'],
                        "municipal" => $_Row1['new_mname'],
                        "municipalold" => $_Row1['old_mname'],
                        "wardnum" => $_Row1['new_ward'],
                        "wardnumold" => $_Row1['old_ward'],
                        "panchayat" => $_Row1['new_block'],
                        "panchayatold" => $_Row1['old_block'],
                        "gram" => $_Row1['new_gram'],
                        "gramold" => $_Row1['old_gram'],
                        "village" => $_Row1['new_village'],
                        "villageold" => $_Row1['old_village'],
                        "address" => $_Row['Organization_Address'],
                        "addressold" => $_Row['Organization_Address_old'],
                        "refid" => $_Row['fld_ref_no'],
                        "payfor" => $payfor);

//                    $_i = $_i + 1;
//                    echo json_encode($_DataTable);
//                } else if ($_Row['Organization_AreaType'] == 'Urban') {
//                    $responseRural = $emp->GetOrgUrbanDetails($payfor);
//                    $_Row2 = mysqli_fetch_array($responseRural[2]);
//                    $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
//                        "orgnameold" => $_Row['Organization_Name_old'],
//                        "orgtype" => $_Row['Organization_Type'],
//                        "orgtypeold" => $_Row['Organization_Type_old'],
//                        "areatype" => $_Row['Organization_AreaType'],
//                        "areatypeold" => $_Row['Organization_Type_old'],
//                        "mtype" => $_Row2['new_mtype'],
//                        "mtypeold" => $_Row2['old_mtype'],
//                        "municipal" => $_Row2['new_mname'],
//                        "municipalold" => $_Row2['old_mname'],
//                        "wardnum" => $_Row2['new_ward'],
//                        "wardnumold" => $_Row2['old_ward'],
//                        "panchayat" => $_Row['Organization_Panchayat'],
//                        "panchayatold" => $_Row['Organization_Panchayat_old'],
//                        "gram" => $_Row['Organization_Gram'],
//                        "gramold" => $_Row['Organization_Gram_old'],
//                        "village" => $_Row['Organization_Village'],
//                        "villageold" => $_Row['Organization_Village_old'],
//                        "address" => $_Row['Organization_Address'],
//                        "addressold" => $_Row['Organization_Address_old'],
//                        "refid" => $_Row['fld_ref_no'],
//                        "payfor" => $payfor);

                    $_i = $_i + 1;
                    echo json_encode($_DataTable);
                }
//            } else {
//                echo "3";
//            }
        } else {
            echo "4";
        }
    }
}


/** if ($_action == "ADD") {
//    print_r($_POST);
    //die;
    $amount = $_POST["amounts"];
    $amt2 ='0';
    if ($_POST['ddlnameaddress'] == "1") {
        $payfor = "name";
    } else if ($_POST['ddlnameaddress'] == "2") {
        $payfor = "address";
        
    $response1 = $emp->Getamtncr();
    if($response1[0] == 'Success'){
    $_rows1 = mysqli_fetch_array($response1[2]);
     $amttemp = $_rows1['NCRamount_Amount'];
    
    $response2 = $emp->Getamturban($_POST['OrgAddAreaNew']);
    $_rows2 = mysqli_fetch_array($response2[2]);
     $amturban = $_rows2['Ncr_Fee_Amount'];
    
    $amt3 = ($amturban - $amttemp);
    $amt2 = ($amt2 + $amt3);
    //echo $amt2;
    }
    }
    
    //die;
    $response = $emp->GetPayForAmt($payfor);
    $_rows = mysqli_fetch_array($response[2]);
    $amt1 = $_rows['NameAddress_Fee_Amount'];
    
    if($amt2 > '0'){
        $amt = $amt1 + $amt2;
    } else {
        $amt = $amt1;
    }

//    if (! $amt) {
//        echo "0";
//    } else {
        echo $response = $emp->AddNameAddressPayTran($amt, $_POST);
//    }
}
**/

/**
if ($_action == "ADD") {
//    print_r($_POST);
//    die;
    $amount = $_POST["amounts"];
    $amt2 ='0';
    if ($_POST['ddlnameaddress'] == "1") {
        $payfor = "name";
    } else if ($_POST['ddlnameaddress'] == "2") {
        $payfor = "address";
    
    $response5 = $emp->GetBothAreaType();
    $_rows3 = mysqli_fetch_array($response5[2]);
    $Areatype = $_rows3['Organization_AreaType'];
    $AreatypeOld = $_rows3['Organization_AreaType_old'];
    
    if($Areatype != $AreatypeOld) {
        
    $response1 = $emp->Getamtncr();
    if($response1[0] == 'Success'){
    $_rows1 = mysqli_fetch_array($response1[2]);
     $amttemp = $_rows1['NCRamount_Amount'];
    
    $response2 = $emp->Getamturban($Areatype);
    $_rows2 = mysqli_fetch_array($response2[2]);
     $amturban = $_rows2['Ncr_Fee_Amount'];
    
    $amt3 = ($amturban - $amttemp);
    $amt2 = ($amt2 + $amt3);
    //echo $amt2;
    }
    }
    }
    
    //die;
    $response = $emp->GetPayForAmt($payfor);
    $_rows = mysqli_fetch_array($response[2]);
    $amt1 = $_rows['NameAddress_Fee_Amount'];
    
    if($amt2 > '0'){
    //$response5 = $emp->UpdateAndLog($amt2);   
        $amt = $amt1 + $amt2;
    } else {
        $amt = $amt1;
    }

//    if (! $amt) {
//        echo "0";
//    } else {
        echo $response = $emp->AddNameAddressPayTran($amt, $_POST);
//    }
}
**/

if ($_action == "ADD") {
//    print_r($_POST);
//    die;
    $amount = $_POST["amounts"];
    $amt2 ='0';
    if ($_POST['ddlnameaddress'] == "1") {
        $payfor = "name";
    } else if ($_POST['ddlnameaddress'] == "2") {
        $payfor = "address";
    
    $response5 = $emp->GetBothAreaType();
    $_rows3 = mysqli_fetch_array($response5[2]);
    $Areatype = $_rows3['Organization_AreaType'];
    $AreatypeOld = $_rows3['Organization_AreaType_old'];
    
    if($Areatype != $AreatypeOld) {
        
    $response1 = $emp->Getamtncr();
    if($response1[0] == 'Success'){
    $_rows1 = mysqli_fetch_array($response1[2]);
     $amttemp = $_rows1['NCRamount_Amount'];
    
    $response2 = $emp->Getamturban($Areatype);
    $_rows2 = mysqli_fetch_array($response2[2]);
     $amturban = $_rows2['Ncr_Fee_Amount'];
    
    $amt3 = ($amturban - $amttemp);
    $amt2 = ($amt2 + $amt3);
    //echo $amt2;
    }
    } 
    }
    
    //die;
    $response = $emp->GetPayForAmt($payfor);
    $_rows = mysqli_fetch_array($response[2]);
    $amt1 = $_rows['NameAddress_Fee_Amount'];
    
    if($amt2 > '0'){  
        $amt = $amt1 + $amt2;
    } else {
        $amt = $amt1;
    }

    if (! $amt) {
        echo "0";
    } else {
        echo $response = $emp->AddNameAddressPayTran($amt, $_POST);
    }
}

if ($_action == "SHOW") {
    $response = $emp->GetDetails($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("OwnerName" => $_Row['UserProfile_FirstName'],
            "OwnerMobile" => $_Row['UserProfile_Mobile']);

        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}


if ($_action == "GetPayForAmt") {
    // print_r($_POST);
    $response = $emp->GetPayForAmt($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['NameAddress_Fee_Amount'];
}

if ($_action == "ChkPayEvent") {

    $response = $emp->ChkPayEvent();

    $num_rows = mysqli_num_rows($response[2]);
//    print_r($response);
//    echo "<pre>";
//    print_r($num_rows);
//    die;
    if ($response[0] == "Success") {
        echo "1";
    } else {
        echo "0";
    }
}

if ($_action == "ChkPaymentStatus") {

    $response = $emp->ChkPaymentStatus($_POST['refid']);

    $num_rows = mysqli_num_rows($response[2]);
//    print_r($response);
//    echo "<pre>";
//    print_r($num_rows);
//    die;
    if ($response[0] == "Success") {
        echo "1";
    } else {
        echo "0";
    }
}
if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
    //print_r($response);
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
    //values
    $key = "X2ZPKM";
    $salt = "8IaBELXB";
    $command1 = "check_isDomestic";   //validatecardnumber   luhn algo
    //$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
    $var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
    //$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
    //$var3 = "500";//  Amount to be used in case of refund

    $hash_str = $key . '|' . $command1 . '|' . $var1 . '|' . $salt;
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key, 'hash' => $hash, 'command' => $command1, 'var1' => $var1);

    $qs = http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
    $response = curlCall($wsUrl, $qs, TRUE);
    //echo "<pre>"; print_r($response); echo "</pre>"; 
    $_DataTable = array();
    $_i = 0;
    $_DataTable[$_i] = array("cardType" => $response['cardType'],
        "cardCategory" => $response['cardCategory']);
    echo json_encode($_DataTable);
    //echo $cardType = $response['cardType'];
    //echo $cardCategory = $response['cardCategory'];
}

if ($_action == "FinalValidateCard") {
    //values
    $key = "X2ZPKM";
    $salt = "8IaBELXB";
    $command1 = "validateCardNumber";   //validatecardnumber   luhn algo
    //$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
    $var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
    //$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
    //$var3 = "500";//  Amount to be used in case of refund

    $hash_str = $key . '|' . $command1 . '|' . $var1 . '|' . $salt;
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key, 'hash' => $hash, 'command' => $command1, 'var1' => $var1);

    $qs = http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
    $response = curlCall($wsUrl, $qs, TRUE);
    //echo "<pre>"; print_r($response); echo "</pre>"; 
    if ($response == 'Invalid') {
        echo "1";
    } else if ($response == 'valid') {
        echo "2";
    }
}

function curlCall($wsUrl, $qs, $true) {
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
    if (curl_errno($c)) {
        $c_error = curl_error($c);
        if (empty($c_error)) {
            $c_error = 'Some server error';
        }
        return array('curl_status' => 'FAILURE', 'error' => $c_error);
    }
    $out = trim($o);
    $arr = json_decode($out, true);
    return $arr;
}

?>
