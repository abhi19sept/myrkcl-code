<?php

/* 
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsOrgTypeMaster.php';

$response = array();
$emp = new clsOrgTypeMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"])) {
        $_OrgTypeName = $_POST["name"];
        $_Status=$_POST["status"];

        $response = $emp->Add($_OrgTypeName,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_OrgTypeName = $_POST["name"];
        $_Status=$_POST["status"];
        $_Code=$_POST["code"];
        $response = $emp->Update($_Code,$_OrgTypeName,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);
    //echo $_actionvalue;
    //print_r($response);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("OrgTypeCode" => $_Row['Org_Type_Code'],
            "OrgTypeName" => $_Row['Org_Type_Name'],
            "Status"=>$_Row['Org_Type_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<table border='0' cellpedding='0' cellspacing='0' width='100%' class='gridview'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'>OrgType</th>";
    echo "<th style='30%'>Status</th>";
    echo "<th style='20%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Org_Type_Name'] . "</td>";
         echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmOrgTypeMaster.php?code=" . $_Row['Org_Type_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmOrgTypeMaster.php?code=" . $_Row['Org_Type_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='' selected='selected'>Select Organization Type</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Org_Type_Code'] . ">" . $_Row['Org_Type_Name'] . "</option>";
    }
}