<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsSpChangeRpt.php';

$response = array();
$emp = new clsSpChangeRpt();



if ($_action == "GETDATA") {

    $response = $emp->GetDatafirst();

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th >IT-GK Name</th>";
     echo "<th >IT-GK Mobile</th>";
      echo "<th >IT-GK Email</th>";
    echo "<th >IT-GK District</th>";
    echo "<th >SP Name</th>";
    echo "<th>SP-ITGK Application Date</th>";
    echo "<th>SP-ITGK Approval Date</th>";
    echo "<th>SP-ITGK End Date</th>";
    echo "<th >SP Status</th>";
    echo "<th>SP-ITGK RM Remark</th>";
    echo "<th>SP-ITGK Selected Reason</th>";
    echo "<th>SP-ITGK Entered Reason</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Rspitgk_ItgkCode'] . "</td>";
            echo "<td>" . $_Row['ITGK_Name'] . "</td>";
            echo "<td>" . $_Row['ITGKMOBILE'] . "</td>";
            echo "<td>" . $_Row['ITGKEMAIL'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspname'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Date'] . "</td>";
            echo "<td>" . ($_Row['Rspitgk_RspApproval_Date'] ? $_Row['Rspitgk_RspApproval_Date'] : 'NA') . "</td>";

            if ($_Row['Rspitgk_Status'] == "Rejected") {
                echo "<td>NA</td>";
            } else {
                echo "<td>" . $_Row['Rspitgk_End_Date'] . "</td>";
            }
//            if ($_Row['Rspitgk_End_Date'] == "9999-12-31 11:59:59" && $_Row['Rspitgk_Status'] =="Approved") {
//                echo "<td> Current RSP </td>";
//            }
//            if($_Row['Rspitgk_Status'] =="Rejected" && $_Row['Rspitgk_End_Date'] == "9999-12-31 11:59:59") {
//                echo "<td> Request Rejected </td>";
//            }
//            elseif($_Row['Rspitgk_UserType'] =="Old"){
//                echo "<td>Previous RSP</td>";
//            }
            echo "<td>" . $_Row['Rspitgk_Status'] . "</td>";
            echo "<td>" . ($_Row['Rspitgk_Remark'] ? $_Row['Rspitgk_Remark'] : 'NA') . "</td>";
            echo "<td>" . ($_Row['Rspitgk_RspSelectReason'] ? $_Row['Rspitgk_RspSelectReason'] : 'NA') . "</td>";
            echo "<td>" . ($_Row['Rspitgk_Reason'] ? $_Row['Rspitgk_Reason'] : 'NA') . "</td>";
            echo "</tr>";

            $_Count++;
        }

        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
if ($_action == "GETDATASP") {

    $response = $emp->GetDataSP();

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th >IT-GK Name</th>";
    echo "<th >IT-GK District</th>";
    echo "<th >SP Name</th>";
    echo "<th>SP-ITGK Start Date</th>";
    echo "<th>SP-ITGK End Date</th>";
    echo "<th >SP Status</th>";


    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Rspitgk_ItgkCode'] . "</td>";
            echo "<td>" . $_Row['ITGK_Name'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspname'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_RspApproval_Date'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_End_Date'] . "</td>";
            if ($_Row['Rspitgk_End_Date'] == "9999-12-31 11:59:59") {
                echo "<td> Current RSP </td>";
            } else {
                echo "<td> Previous RSP </td>";
            }

            echo "</tr>";

            $_Count++;
        }

        echo "</tbody>";

        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
