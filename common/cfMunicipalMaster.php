<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsMunicipalMaster.php';

$response = array();
$emp = new clsMunicipalMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        $_MunicipalName = $_POST["name"];
       // $_Parent=$_POST["parent"];
        $_District=$_POST["district"];
        $_Status=$_POST["status"];

        $response = $emp->Add($_MunicipalName,$_District,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_MunicipalName = $_POST["name"];
        //$_Parent=$_POST["parent"];
        $_District=$_POST["district"];
        $_Status=$_POST["status"];
        $_Code=$_POST["code"];
        $response = $emp->Update($_Code,$_MunicipalName,$_District,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("MunicipalCode" => $_Row['Municipality_Code'],
            "MunicipalName" => $_Row['Municipality_Name'],
            "District" => $_Row['Municipality_District'],
            "Status"=>$_Row['Municipality_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll($_actionvalue);

    $_DataTable = "";

    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>Name</th>";
    echo "<th style='15%'>Country</th>";
    echo "<th style='15%'>State</th>";
    echo "<th style='15%'>Region</th>";
    echo "<th style='10%'>District</th>";
    echo "<th style='10%'>Status</th>";
    echo "<th style='15%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Municipality_Name'] . "</td>";
        echo "<td>" . $_Row['Country_Name'] . "</td>";
        echo "<td>" . $_Row['State_Name'] . "</td>";
        echo "<td>" . $_Row['Region_Name'] . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
        echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmMunicipalMaster.php?code=" . $_Row['Municipality_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmMunicipalMaster.php?code=" . $_Row['Municipality_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll($_actionvalue);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Municipality_Code'] . ">" . $_Row['Municipality_Name'] . "</option>";
    }
}