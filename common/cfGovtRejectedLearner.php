<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsGovtRejectedLearner.php';

$response = array();
$emp = new clsGovtRejectedLearner();

	if ($_action == "ShowRejectedLearnerList")
	{
        $response = $emp->GetAllRejectedList();
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>S No.</th>";
		echo "<th>Learner Code</th>";
		echo "<th>Learner Name</th>";
		echo "<th>Father/Husband Name</th>";
		echo "<th>District</th>";
		echo "<th>Marks</th>";				
		echo "<th>Action</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		
			$_Count = 1;	
			while ($_Row = mysqli_fetch_array($response[2])) {
			echo "<tr>";
			echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['learnercode'] . "</td>";
			echo "<td>" . strtoupper($_Row['fname']) . "</td>";
			echo "<td>" . strtoupper($_Row['faname']) . "</td>";
			echo "<td>" . $_Row['ldistrictname'] . "</td>"; 
			echo "<td>" . $_Row['exammarks'] . "</td>";			
			echo "<td> <a href='frmgoveditrejectedlearner.php?code=" . $_Row['learnercode'] . "&Mode=Edit'>"
						. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='Edit'/></a>"						
						. "</td>";				
			echo "</tr>";
			$_Count++;
		}
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyLearnerCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("LearnerCode" => $_Row['learnercode'],
            "lname" => $_Row['fname'],
            "fname" => $_Row['faname'],
            "email" => $_Row['lemailid'],
            "mobile" => $_Row['lmobileno'],
			"address" => $_Row['officeaddress'],
			"gpf" => $_Row['empgpfno'],
            "district" => $_Row['ldistrictname'],
			"department" => $_Row['gdname'],
			"empid" => $_Row['empid'],
			"designation" => $_Row['designation'],
			"marks" => $_Row['exammarks'],
			"attempt" => $_Row['aattempt'],
			"panno" => $_Row['panno'],
            "accountno" => $_Row['empaccountno'],
			"bdistrict" => $_Row['bankdistrict'],
			"bankname" => $_Row['gbankname'],
			"branchname" => $_Row['gbranchname'],
            "ifsc" => $_Row['gifsccode'],
			"micr" => $_Row['gmicrcode'],
			"birth" => $_Row['attach2'],
			"receipt" => $_Row['attach1'],
			"dobproof" => $_Row['dobtype']);
        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}
