<?php
ini_set("memory_limit", "-1");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
ini_set("upload_max_filesize", "855M");
ini_set("post_max_size", "1000M");
ini_set("max_execution_time", "30000000000");
ini_set("max_input_time", "30000000000");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsUpdExamResult.php';

$response = array();
$emp = new clsUpdExamResult();


if ($_action == "updfile") {
//    print_r($_POST);
//   print_r($_FILES);
//
//    die;

    $filename = $_POST['txtfilename'];
    $eventname = $_POST['ddlExamEvent'];
    $roimg = $_FILES['resultfile']['name'];
    $rotmp = $_FILES['resultfile']['tmp_name'];
    $rotemp = explode(".", $roimg);
    //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
    $rofilename = $filename . '_resultfile.' . end($rotemp);
    $rofilestorepath = "../upload/finalexamresult/" . $rofilename;
    //echo $rofilestorepath;
    //die;
    $roimageFileType = pathinfo($rofilestorepath, PATHINFO_EXTENSION);
    if (($_FILES["resultfile"]["size"] < 2000000000000 && $_FILES["resultfile"]["size"] > 100)) {
        if ($roimageFileType == "csv") {
            if (move_uploaded_file($rotmp, $rofilestorepath)) {
                //echo "1";
                $response_return["resultfile"] = array("status" => "1", "filename" => $rofilestorepath, "eventname" => $eventname);
                echo json_encode($response_return);
            } else {
                echo "File not uploaded. Please try again.";
                return FALSE;
            }
        } else {
            echo "Sorry, File Not Valid";
            return FALSE;
        }
    } else {
        echo "File Size should be between 100KB to 2MB.";
        return FALSE;
    }
}

if ($_action == "uploadexamresult") {

    if (isset($_POST["filepath"])) {
        $_filepath = $_POST["filepath"];
        $csvData = file_get_contents($_filepath);
        $lines1 = array_filter(explode(PHP_EOL, $csvData));
       
        $lines = array_slice($lines1, 1);
     //    print_r($lines);
        $valuestring = "";
        $j = 0;
        foreach ($lines as $key => $value) {
            # code...
 //$valuestring1 = "'" . implode("', '", $value)  . "'";
                     
            $temp1 = explode(",", $value);
            //print_r($temp1);
            $result = "'" . implode ( "', '", $temp1) . "'";
           //  print_r($temp1);
            $valuestring1 = "" . ($result) . "";
            $valuestring .= trim("(" . $valuestring1 . ")");
            $j++;
            $valuestring .= (count($lines) != $j) ? "," : "";
            
        }

        $response = $emp->Addfinalexamresult($valuestring);


        if($response['0']== "Successfully Done")
        {
            echo json_encode(['status' => 'success', 'rows' => $response['2']]);
        }
         else{
            echo json_encode(['status' => 'fail', 'rows' => '0']);
               // return $response;
         }
    }
}
if ($_action == "transferexamresult"){
  if (isset($_POST["eventname"])) {
     $response = $emp->transferexamresult($_POST["eventname"]);
// print_r($response);
// die;
        if($response['0']== "Successfully Done")
        {
            echo json_encode(['status' => 'success', 'rows' => $response['2']]);
        }
         else{
            echo json_encode(['status' => 'fail', 'rows' => '0']);
               // return $response;
         }
}
}

if ($_action == "transferexamresultfinal"){
  if (isset($_POST["eventname"])) {
     $response = $emp->transferexamresultfinal($_POST["eventname"],$_POST["result_date"]);
// print_r($response);
// die;
        if($response['0']== "Successfully Done")
        {
            echo json_encode(['status' => 'success', 'rows' => $response['2']]);
        }
         else{
            echo json_encode(['status' => 'fail', 'rows' => '0']);
               // return $response;
         }
}
}
function data_insertion($file_name) {
    Global $JasonFilePath;
    /* json file read and decode */

    $str1 = file_get_contents($JasonFilePath . $file_name);
    $str = encrypt_decrypt('decrypt', $str1);
    $json = json_decode($str, true);
    $r = "";
    $co = count($json);
    $i = 1;
    if (count($json) > 0) {
        foreach ($json as $value) {
            // FOR MULTIPLE INSERTION MAKE STRING
            $r .= '("' . $value['Fresult_learner'] . '","' . $value['Fresult_itgk'] . '","' . $value['Fresult_rsp'] . '","' . $value['Fresult_district'] . '","' . $value['Fresult_batch'] . '","' . $value['Fresult_course'] . '","' . $value['Fresult_noofassessment'] . '","' . $value['Fresult_obtainmarks'] . '","' . $value['Fresult_timestamp'] . '","' . $value['IsNewMRecord'] . '","' . $value['Re_exam_status'] . '")';
            if ($co != $i) {
                $r .= ',';
            }
            $i++;
        }
        // DATA INSERTION
        $emp = new clsSentSMS();
        $emp->Add_final_result($r);
    }
}
