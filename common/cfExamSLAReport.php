<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsExamSLAReport.php';
$response = array();
$emp = new clsExamSLAReport();

$_action = $_REQUEST['action'];

if ($_action == "fillReportForTypes") {
    echo "<option value='itgk'>ITGK</option>";
    if ($_SESSION['User_UserRoll'] != '7') {
        echo "<option value='sp'>SP</option>";
    }
}

if ($_action == "FILLEXAMEVENT") {
    $response = $emp->GetExamEvent();
    echo "<option value=''>Select Exam Event</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Affilate_Event'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "itgkexamslareport" && isset($_SESSION['User_UserRoll'])) {
    $response = $emp->getExamEventSummary($_POST['examid']);
    generateExamSummaryGrid($response);
    if ($_POST['reportFor'] == 'itgk') {
        $response = $emp->getItgkExamSlaReport($_POST['examid']);
        generateGrid($response);
    } elseif ($_POST['reportFor'] == 'sp') {
        $response = $emp->getSPExamSlaReport($_POST['examid']);
        generateSPGrid($response);
    }
    echo "<a href='common/cfExamSLAReport.php?action=downloadReport&reportFor=" . $_POST['reportFor'] . "&examid=" . $_POST['examid'] . "' style='display:none;'>"
        . "<input type='button' name='Approve' id='Approve' class='approvalLetter btn btn-primary' value='Download'/></a>";
}

function generateExamSummaryGrid($response) {
    echo "<div class='table-responsive'><b>Exam Event Summary:</b>";
    echo "<table id='example1' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered small'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>ID</th>";
    echo "<th style='10%'>Event Name</th>";
    echo "<th style='10%'>Exam Date</th>";
    echo "<th style='10%'>Total ITGK</th>";
    echo "<th style='10%'>Permission Letters</th>";
    echo "<th style='10%'>Total Absent</th>";
    echo "<th style='10%'>Total Appear</th>";
    echo "<th style='10%'>Total Passed</th>";
    echo "<th style='10%'>Total Failed</th>";
    echo "<th style='10%'>VMOU Result</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Row['examid'] . "</td>";
        echo "<td>" . $_Row['Event_Name'] . "</td>";
        echo "<td>" . $_Row['Exam_Date'] . "</td>";
        echo "<td>" . $_Row['Total_ITGK'] . "</td>";
        echo "<td>" . $_Row['permissionLetters'] . "</td>";
        echo "<td>" . $_Row['absentCount'] . "</td>";
        echo "<td>" . $_Row['TotalAppear'] . "</td>";
        echo "<td>" . $_Row['passCount'] . "</td>";
        echo "<td>" . $_Row['failCount'] . "</td>";
        echo "<td>" . sprintf("%.2f", $_Row['vmou_result']) . "%</td>";
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

function generateGrid($response) {
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>ITGK Code</th>";
    echo "<th style='10%'>Permission Letters</th>";
    echo "<th style='10%'>Total Absent</th>";
    echo "<th style='10%'>Total Appear</th>";
    echo "<th style='10%'>Pass Count</th>";
    echo "<th style='10%'>Fail Count</th>";
    echo "<th style='10%'>Result Percent</th>";
    echo "<th style='10%'>VMOU Result</th>";
    echo "<th style='10%'>Status</th>";
    echo "<th style='10%'>Panelty Applicable</th>";
    echo "<th style='10%'>Panelty Amount</th>";
    if ($_SESSION['User_UserRoll'] != 7 && $_SESSION['User_UserRoll'] != 14) {
        echo "<th style='10%'></th>";
    }
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['itgkcode'] . "</td>";
        echo "<td>" . $_Row['totalAppear'] . "</td>";
        echo "<td>" . $_Row['absentCount'] . "</td>";
        echo "<td>" . $_Row['eventitgklearners'] . "</td>";
        echo "<td>" . $_Row['passCount'] . "</td>";
        echo "<td>" . $_Row['failCount'] . "</td>";
        echo "<td>" . sprintf("%.2f", $_Row['itgk_result']) . "%</td>";
        echo "<td>" . sprintf("%.2f", $_Row['vmou_result']) . "%</td>";
        echo "<td nowrap>" . $_Row['rstatus'] . "</td>";
        echo "<td>" . $_Row['panelty'] . "</td>";
        echo "<td>" . ceil(sprintf("%.2f", $_Row['panelty_applied_amount'])) . "</td>";
        //echo "<td>" . sprintf("%.2f", $_Row['itgk_result_diff']) . "%</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

function generateSPGrid($response) {
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>SP Name</th>";
    echo "<th style='10%'>VMOU Result</th>";
    echo "<th style='10%'>Permission Letters</th>";
    echo "<th style='10%'>Total Absent</th>";
    echo "<th style='10%'>Total Appear</th>";
    echo "<th style='10%'>Pass Count</th>";
    echo "<th style='10%'>Fail Count</th>";
    echo "<th style='10%'>Result Percent</th>";
    echo "<th style='10%'>SLA Failed</th>";
    echo "<th style='10%'>Panelty Amount</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $panelty = $_Row['faileditgks'] * 100;
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Organization_Name'] . "</td>";
        echo "<td>" . sprintf("%.2f", $_Row['vmou_result']) . "%</td>";
        echo "<td>" . $_Row['totalAppear'] . "</td>";
        echo "<td>" . $_Row['absentCount'] . "</td>";
        echo "<td>" . $_Row['eventitgklearners'] . "</td>";
        echo "<td>" . $_Row['passCount'] . "</td>";
        echo "<td>" . $_Row['failCount'] . "</td>";
        echo "<td>" . sprintf("%.2f", ($_Row['passCount'] / $_Row['eventitgklearners']) * 100) . "%</td>";
        echo "<td>" . $_Row['faileditgks'] . "</td>";
        echo "<td>" . (($panelty > 5000) ? 5000 : $panelty) . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "downloadReport" && $_REQUEST['reportFor'] == 'itgk') {
    $response = $emp->getItgkExamSlaReport($_REQUEST['examid']);

    $filename = "ITGK_Exam_SLA_Report.csv";
    $fp = fopen('php://output', 'w');

    $header = array("ITGK Code", "Permission Letters", "Total Absent", "Total Appered", "Pass Count", "Fail Count", "Result Percent", "VMOU Result", "Status", "Panelty Applicable", "Panelty Amount");

    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename=' . $filename);
    fputcsv($fp, $header);
    if ($response) {
        $result = [];
        while ($row = mysqli_fetch_array($response[2])) {
            $result = [
                'itgkcode' => $row['itgkcode'],
                'Permission_Letters' => $row['totalAppear'],
                'Total_Absent' => $row['absentCount'],
                'Total_Appered' => $row['eventitgklearners'],
                'Pass_Count' => $row['passCount'],
                'failCount' => $row['failCount'],
                'Result_Percent' => sprintf("%.2f", $row['itgk_result']),
                'vmou_result' => $row['vmou_result'],
                'Status' => $row['rstatus'],
                'Panelty_Applicable' => $row['panelty'],
                'Panelty_Amount' => ceil(sprintf("%.2f", $row['panelty_applied_amount'])),
            ];
            fputcsv($fp, $result);
        }

    }

    exit;
}

if ($_action == "downloadReport" && $_REQUEST['reportFor'] == 'sp') {
    $response = $emp->getSPExamSlaReport($_REQUEST['examid']);

    $filename = "SP_Exam_SLA_Report.csv";
    $fp = fopen('php://output', 'w');

    $header = array("SP Name", "VMOU Result", "Permission Letters", "Total Absent", "Total Appered", "Pass Count", "Fail Count", "Result Percent", "SLA Failed", "Panelty Amount");

    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename=' . $filename);
    fputcsv($fp, $header);
    if ($response) {
        $result = [];
        while ($row = mysqli_fetch_array($response[2])) {
            $panelty = $row['faileditgks'] * 100;
            $result = [
                'itgkcode' => $row['Organization_Name'],
                'vmou_result' => $row['vmou_result'],
                'Permission_Letters' => $row['totalAppear'],
                'Total_Absent' => $row['absentCount'],
                'Total_Appered' => $row['eventitgklearners'],
                'Pass_Count' => $row['passCount'],
                'failCount' => $row['failCount'],
                'Result_Percent' => sprintf("%.2f", ($row['passCount'] / $row['eventitgklearners']) * 100),
                'ITGK_Failed' => $row['faileditgks'],
                'Panelty_Amount' => (($panelty > 5000) ? 5000 : $panelty),
            ];
            fputcsv($fp, $result);
        }

    }

    exit;
}

if ($_action == "getlearnerresults") {
    $response = $emp->getlearnerresults($_POST['examid']);
    generateSPResultGrid($response);
    echo "<a href='common/cfExamSLAReport.php?action=downloadReport&reportFor=lresult&examid=" . $_POST['examid'] . "' style='display:none;'>"
        . "<input type='button' name='Approve' id='Approve' class='approvalLetter btn btn-primary' value='Download'/></a>";
}

function generateSPResultGrid($response) {
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>ITGK Code</th>";
    echo "<th style='10%'>Learner Code</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='10%'>Total Marks</th>";
    echo "<th style='10%'>Marks Obtained</th>";
    echo "<th style='10%'>Percentage</th>";
    echo "<th style='10%'>Result</th>";
    //echo "<th style='10%'></th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['study_cen'] . "</td>";
        echo "<td>" . $_Row['scol_no'] . "</td>";
        echo "<td>" . $_Row['name'] . "</td>";
        echo "<td>" . $_Row['tot_marks'] . "</td>";
        echo "<td>" . $_Row['net_marks'] . "</td>";
        echo "<td>" . $_Row['percentage'] . "</td>";
        echo "<td>" . $_Row['result'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "downloadReport" && $_REQUEST['reportFor'] == 'lresult') {
    $response = $emp->getlearnerresults($_REQUEST['examid']);

    $filename = "SP_Learner_Result.csv";
    $fp = fopen('php://output', 'w');

    $header = array("ITGK Code", "Learner Code", "Learner Name", "Total Marks", "Marks Obtained", "Percentage", "Result");

    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename=' . $filename);
    fputcsv($fp, $header);
    if ($response) {
        $result = [];
        while ($row = mysqli_fetch_array($response[2])) {
            $result = [
                'itgkcode' => $row['study_cen'],
                'Learner_Code' => $row['scol_no'] . '`',
                'Learner_Name' => $row['name'],
                'Total_Marks' => $row['tot_marks'],
                'Marks_Obtained' => $row['net_marks'],
                'Percentage' => $row['percentage'],
                'Result' => $row['result'],
            ];
            fputcsv($fp, $result);
        }
    }

    exit;
}


?>
