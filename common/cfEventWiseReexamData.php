<?php

/*cfEventWiseReexamData
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
include './commonFunction.php';
require 'BAL/clsEventWiseReexamData.php';
require '../DAL/upload_ftp_doc.php';

$response = array();
$emp = new clsEventWiseReexamData();

if ($_action == "SHOW") 
{
	$_ObjFTPConnection = new ftpConnection();
   $abc = $_ObjFTPConnection->ftpdetails();
   
	if($_POST['Event']==''){
			echo "blank";
	}
	else
	{
		$eventid = $_POST['Event'];
		$response = $emp->GetAll($eventid);
		
		$filename = "ExamData_" . $_POST['Event'] . ".csv";
	    $fileNamePath = $abc."/eventresultdata/" . $filename;
		
		$context = stream_context_create(['ftp' => ['overwrite' => true]]);
		$myFile = fopen($fileNamePath, 'w', false, $context);

		//$myFile = fopen($fileNamePath, 'w');

		fputs($myFile, '"' . implode('","', array("Exam Data Code","Exam ID", "LearnerCode", "Learner Name", "Father Name", "dob", 
			"Learner Mobile", "Course Name", "Batch Name", "IT-GK Code", "IT-GK Name", "IT-GK District", "IT-GK Tehsil",
			"Remark", "Exam Date", "Learner Type"	)) . '"' . "\n");

        while ($row1 = mysqli_fetch_row($response[2])) {
            fputcsv($myFile, array($row1['0'], $row1['1'],"'".$row1['2'], $row1['3'], $row1['4'], $row1['5'], $row1['6'], $row1['7'],
			$row1['8'], $row1['9'], $row1['10'], $row1['11'], $row1['12'], $row1['13'], $row1['14'], $row1['15']), ',', '"');
        }
        fclose($myFile);
		$filepath5 = 'common/showpdfftp.php?src=eventresultdata/' . $filename;
		echo $filepath5;
        //echo "upload/eventresultdata/" . $filename;	
	}
}
?>