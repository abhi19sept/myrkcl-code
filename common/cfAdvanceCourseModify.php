<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsAdvanceCourseModify.php';
$response = array();
$emp = new clsAdvanceCourseModify();


if ($_action == "SHOWALL")
	{
        $response = $emp->GetAllForModify($_POST['batch'], $_POST['course'], $_POST['category'], $_POST['pkg']);
		$_DataTable = "";

     echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
	echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th>Father/Husband Name</th>";
    echo "<th>D.O.B</th>";
	echo "<th>Mobile</th>";
	echo "<th>Photo</th>";
	echo "<th>Sign</th>";
	//echo "<th>Print Receipt</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" .strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        echo "<td>" . $_Row['Admission_DOB'] . "</td>"; 	
		echo "<td>" . $_Row['Admission_Mobile'] . "</td>";		
		
		if($_Row['Admission_Photo']!="")
				{
					$image = $_Row['Admission_Photo'];
					echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/'.$image.'"/>' . "</td>";
				}
			else
				{
					echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
				}
		if($_Row['Admission_Sign']!="")
			{
				$sign = $_Row['Admission_Sign'];
				echo "<td>" . '<img alt="No Image Found" width="80" height="35" src="upload/admission_sign/'.$sign.'"/>' . "</td>";
			}
		else
				{
					echo "<td>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
				}

       // echo "<td><a href=javascript:window.open('PrintReceiptpdf.php?code=" . $_Row['Admission_Code'] . "&Mode=printreceipt');>"
              // . "<img src='images/print_printer.png' alt='Edit' width='25px' height='25px'  align='center'/></a> </td> ";
				
		
		
		if($_Row['Admission_Payment_Status'] == '0')
		{
			echo "<td> <a href='frmeditadvancecourselearner.php?code=" . $_Row['Admission_Code'] . "&Mode=Edit'>"
					. "<img src='images/editicon.png' alt='Edit' width='30px' height='25px'/></a>  "
					 . "<a href='frmadvancecoursemodify.php?code=" . $_Row['Admission_Code'] . "&Mode=Delete&batchcode=" . $_Row['Admission_Batch'] . "'>"
                . "<img src='images/deleteicon.png' alt='Delete' width='30px' height='25px'/></a> "
			. "</td>";
		}
		else{
			echo "<td>"
					. "<img src='images/editicon.png' alt='Edit' width='30px' height='25px' />"
			. "</td>";
		}
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	 echo "</div>";
	}
	
	
	if ($_action == "DELETE") {
		$value = $_POST['values'];
		$batch = $_POST['batchcode'];
    
		$response = $emp->DeleteRecord($value,$batch);
		echo $response[0];
	}
?>
	