<?php
    /*
     * Created by Mayank
     */

    include './commonFunction.php';
    require 'BAL/clsLearnerAttendance.php';

    $response = array();
    $emp = new clsLearnerAttendance();

    if ($_action == "GETDATA") {
        /*$response = $emp->GetDataAll($_POST['course'], $_POST['batch']);
        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th>Code</th>";
        echo "<th >AdmissionCount</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        $_Total = 0;

        if ($response) {
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $_Row['RoleName'] . "</td>";
                echo "<td><a href='frmAttendanceList.php?course=" . $_Row['Course'] . "&batch=" . $_Row['Batch'] . "&rolecode=" . $_Row['RoleCode'] . "&mode=Show'  target='_blank'>"
                    . "" . $_Row['admissioncount'] . "</a></td>";
                echo "</tr>";
                $_Total = $_Total + $_Row['admissioncount'];
                $_Count++;
            }

            echo "</tbody>";
            echo "<tfoot>";
            echo "<tr>";
            echo "<th > Sum </th>";
            echo "<th > </th>";
            echo "<th>";
            echo "$_Total";
            echo "</th>";
            echo "</tr>";
            echo "</tfoot>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }*/


        echo "<table id='example' class='table table-striped table-bordered'>";
        echo "<thead>
            <tr>
                <th style='width: 5%'>S.No</th>
                <th>Code</th>
                <th style='width: 10%;'>AdmissionCount</th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
               <th></th>
                <th>Total Admission Count</th>
                <th><div id='sum'></div></th>
            </tr>
        </tfoot>";
        echo "</table>";
    }


    if ($_action == "GETDATA1") {
        $response = $emp->GetDataAll($_POST['course'], $_POST['batch']);
        echo json_encode($response);
    }

    if ($_action == "GETDATA2") {
        $response = $emp->getSumAllData($_POST['course'], $_POST['batch']);

        if (count($response[2]) > 0) {
            $_Total = 0;
            while ($_Row = mysqli_fetch_array($response[2])) {
                $_Total = $_Total + $_Row['admissioncount'];
            }
        } else {
            $_Total = 0;
        }
        echo($_Total);
    }


    if ($_action == "GETLEARNERLIST") {
        $response = $emp->GetLearnerList($_POST['course'], $_POST['batch'], $_POST['rolecode']);
        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example1' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<tbody>";
        echo "<tr>";
        echo "<td style='font-size:12px !important;'>" . "<b>Center Code: </b>" . $_POST['rolecode'] . "</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td style='font-size:12px !important;'>" . "<b>Note:- To calculate daily attendance, Learner should Punch IN and Punch OUT both on Same Day.</b></td>";
        echo "</tr>";
        echo "</tbody>";
        echo "</table>";
        echo "</div>";


        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        // echo "<th>Center Code</th>";
        echo "<th>Learner Code</th>";
        echo "<th>Learner Name</th>";
        echo "<th >Father/Husband Name</th>";
        echo "<th>Enrollment Status</th>";
        echo "<th>Days Attended</th>";
        //echo "<th>Time Duration</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;

        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            // echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";

            if ($_Row['BioMatric_Status'] == '0') {
                echo "<td> Learner Not Enrolled </td>";
            } else {
                echo "<td> Learner Enrolled </td>";
            }

            //$quotient = (int)($_Row['Duration'] / 60);
            //$remainder = $_Row['Duration'] % 60;

            if ($_Row['Days'] == '') {
                echo "<td> No Attendance Recorded </td>";
                //echo "<td> No Attendance Recorded </td>";
            } else {
                echo "<td>" . $_Row['Days'] . "</td>";
                //echo "<td>" . $quotient . " hr :".$remainder." min</td>";
            }


            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }

    if ($_action == "GETDATAITGK") {
        $response = $emp->GetLearnerList($_POST['course'], $_POST['batch'], $_POST['role']);
        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example1' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<tbody>";
        echo "<tr>";
        echo "<td style='font-size:12px !important;'>" . "<b>Center Code: </b>" . $_POST['role'] . "</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td style='font-size:12px !important;'>" . "<b>Note:- To calculate daily attendance, Learner should Punch IN and Punch OUT both on Same Day.</b></td>";
        echo "</tr>";
        echo "</tbody>";
        echo "</table>";
        echo "</div>";


        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        // echo "<th>Center Code</th>";
        echo "<th>Learner Code</th>";
        echo "<th>Learner Name</th>";
        echo "<th >Father/Husband Name</th>";
        echo "<th>Enrollment Status</th>";
        echo "<th>Days Attended</th>";
        //echo "<th>Time Duration</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;

        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            // echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
            if ($_Row['BioMatric_Status'] == '0') {
                echo "<td> Learner Not Enrolled </td>";
            } else {
                echo "<td> Learner Enrolled </td>";
            }

            //$quotient = (int)($_Row['Duration'] / 60);
            //$remainder = $_Row['Duration'] % 60;

            if ($_Row['Days'] == '') {
                echo "<td> No Attendance Recorded </td>";
                //echo "<td> No Attendance Recorded </td>";
            } else {
                echo "<td>" . $_Row['Days'] . "</td>";
                //echo "<td>" . $quotient . " hr :".$remainder." min</td>";
            }


            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";

    }

    if ($_action == "getAttendanceGraph") {
        $attendanceDetail = [];
        $responseData = $emp->getUserAttendanceData($_POST['course'], $_POST['batch']);

        for ($i = 1; $i <= date('d'); $i++) {
            // add the date to the dates array
            $attendanceDetail[date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT)] = 0;
        }

        while ($_Row1 = mysqli_fetch_array($responseData[2])) {
            $attendanceDetail[$_Row1['attendance_date']] = $_Row1['attendance_count'];
        }
        echo json_encode($attendanceDetail);
    }
	

if ($_action == "GETDATAAll") {
    $response = $emp->GetAllLearnerAttendance($_POST['course'], $_POST['batch']);
    $_DataTable = "";
    if($_SESSION['User_UserRoll'] == 14){
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>S No.</th>";
            echo "<th>Center Code</th>";
            echo "<th>Learner Code</th>";
            echo "<th>Learner Name</th>";
            echo "<th >Father/Husband Name</th>";

            echo "<th>Enrollment Status</th>";
            echo "<th>Working Days</th>";
            echo "<th>Days Remaining to complete 22 attendance</th>";
            echo "<th>Eligible For Final Exam (22 attendance)</th>";

            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;

            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                    echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
                echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
                echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
                echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";

                if ($_Row['BioMatric_Status'] == '0') {
                    echo "<td> Learner Not Enrolled </td>";
                } else {
                    echo "<td> Learner Enrolled </td>";
                }

                echo "<td>" . $_Row['attcount'] . "</td>";
                $rem = (22-$_Row['attcount']);
                if($rem <= 0){
                    echo "<td>0</td>";
                    echo "<td>YES</td>";
                }
                else{
                    echo "<td>" . $rem . "</td>";
                    echo "<td>NO</td>";
                }
               
                

                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
    }
    else{
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>S No.</th>";
            echo "<th>Center Code</th>";
            echo "<th>Learner Code</th>";
            echo "<th>Learner Name</th>";
            echo "<th >Father/Husband Name</th>";
            if ($_SESSION['User_UserRoll'] != 17) {
                echo "<th>Enrollment Status</th>";
            }
            echo "<th>Working Days</th>";
        //    echo "<th>Time Duration</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;

            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                    echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
                echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
                echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
                echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
                if ($_SESSION['User_UserRoll'] != 17) {
                    if ($_Row['BioMatric_Status'] == '0') {
                        echo "<td> Learner Not Enrolled </td>";
                    } else {
                        echo "<td> Learner Enrolled </td>";
                    }
                }
                echo "<td>" . $_Row['attcount'] . "</td>";


                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        }
}
	
	
	
	
	