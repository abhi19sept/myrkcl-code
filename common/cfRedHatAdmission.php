<?php

/*
 *  author Mayank
 */

require 'commonFunction.php';
require 'BAL/clsRedHatAdmission.php';

$response = array();
$emp = new clsRedHatAdmission();
//print_r($_POST);


if ($_action == "FillAdmissionCount") {   
    $response = $emp->GetAdmissionCount();
	echo $response[0];
}


if ($_action == "ADD") {
    //print_r($_POST); 
	if (isset($_POST["learnercode"]) && !empty($_POST["learnercode"])) {
		if (isset($_POST["lname"]) && !empty($_POST["lname"])) {
			if (isset($_POST["parent"]) && !empty($_POST["parent"])) {
				if (isset($_POST["dob"]) && !empty($_POST["dob"])) {
					if (isset($_POST["mobile"]) && !empty($_POST["mobile"])) {							
							$_LearnerName = trim($_POST["lname"]);
							$_LearnerCode = trim($_POST["learnercode"]);
							$_ParentName = trim($_POST["parent"]);
							$_DOB = trim($_POST["dob"]);							
							$DOB = date("d-m-Y", strtotime($_DOB));							
							$_Mobile = trim($_POST["mobile"]);						

						$response = $emp->Add($_LearnerName, $_LearnerCode, $_ParentName, $DOB, $_Mobile);
						echo $response[0];
					} else {
							echo "something";
						}
				} 	else {
						echo "something";
					}   
			} else {
					echo "something";
				}
		} else {
			echo "something";
		}
	} 	else {
			echo "something";
		}

}

if ($_action == "GENERATELEARNERCODE") {
	   $random="";
		 $random.= (mt_rand(100, 999));
         $random.= date("h");
         $random.= date("i");
         $random.= date("s");
         $random.= '8';
		 
    echo $random;
}

if ($_action == "SendSMS") {
        if (isset($_POST["mobileno"]) && !empty($_POST["mobileno"])) {
		$mobileno = trim($_POST["mobileno"]);
		$response = $emp->SentSms($mobileno);
                //echo "<pre>"; print_r($response);die;
            if ($response[0] != Message::NoRecordFound) {
                $_row = mysqli_fetch_array($response[2]);
				$_Mobile =  "XXXXXX". substr($mobileno, 6);
                $_OTP = $_row["RHLS_Otp_OTP"];
                $_OTP_StartOn = $_row["OTP_StartOn"];
                $_OTP_EndOn = $_row["OTP_EndOn"];
                $_SMS = "Dear Learner, Your verification OTP is " . $_OTP ." for Red Hat Learning Subscription(RHLS) admission.Valid till ".$_OTP_EndOn."."; 
                SendSMS($mobileno, $_SMS);
                $result = [
                    //'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
                    'frm' => $emp->getVeriFyOTPForm($_row["RHLS_Otp_Code"], $_Mobile),
                ];

                echo json_encode($result);
            } else {
                echo '0';
            }
        }
		else{
			echo "empty";
		}
    }
	
	 if ($_action == "RESENDOTP") {
        if (isset($_POST["mobileno"]) && !empty($_POST["mobileno"])) {
		$mobileno = $_POST["mobileno"];
		$response = $emp->ResendOTP($mobileno);
                //echo "<pre>"; print_r($response);die;
            if ($response[0] != Message::NoRecordFound) {
                $_row = mysqli_fetch_array($response[2]);
                $_Mobile =  "XXXXXX". substr($mobileno, 6);
                $_OTP = $_row["RHLS_Otp_OTP"];
                $_OTP_StartOn = $_row["OTP_StartOn"];
                $_OTP_EndOn = $_row["OTP_EndOn"];
                $_SMS = "Dear Learner, Your verification OTP is " . $_OTP ." for Red Hat Learning Subscription(RHLS) admission. Valid till ".$_OTP_EndOn."."; 
                SendSMS($mobileno, $_SMS);
                $result = [
                    //'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
                    'frm' => $emp->getVeriFyOTPForm($_row["RHLS_Otp_Code"], $_Mobile),
                ];

                echo json_encode($result);
            } else {
                echo '0';
            }
        }
    }
	
	if ($_action == "VerifyOTP" && isset($_POST["oid"]) && !empty($_POST["oid"])) {
        
        if($_POST["otp"]=='')
        {
          echo 'OTPBLANCK';  
        }
        else{
            $response = $emp->VerifyOTPNEW($_POST["oid"], $_POST["otp"]);			
            if($response[0]=='EXPIRE')
            {
               echo 'EXPIRE';

            }
            else if($response[0]=='NORECORD')
            {
               echo 'NORECORD';
            }
            else{
					if($response[0]=='Success'){
						if (mysqli_num_rows($response[2])) 
                        {                        
							 $result = [
								'msg' => 'OTP Successfully Verified',
								'oid' => $_POST["oid"],
							];
							echo json_encode($result);
                        } 
					}                
					else {
						echo '0';
					}
			}
        }
    }

if ($_action == "TRACKSTATUS") {
    $txtacknownumber = $_POST["txtacknownumber"];
    $response = $emp->TrackStatus($txtacknownumber);
    if($response[0] == 'Success'){
       $_Row = mysqli_fetch_array($response[2]);
    echo"<ul class='progress-indicator'>";
        echo"<li class='completed'>
            <span class='bubble'></span><span class='fa fa-user messagecls' aria-hidden='true' style='margin-left: 15px;'></span>
            <div class='messagecls'>Candidate Registration Completed</div>
            <div style='color: #0a242b;font-weight: bold;'><span class='fa fa-clock-o' style='color: #0a242b;margin: 0 0 0 0;'></span>  ".$_Row['datetime']."</div>
        </li>";
        
        if($_Row['rhls_pay_status'] == '1'){
            $paystatus = "Payment Done Successfully";
            $payclass = "completed";
            $mesclass = "messagecls";
        }else{
            $paystatus = "Payment Not Done";
            $payclass = "danger";
            $mesclass = "messageclsalert";
        } 
        echo"<li class='".$payclass."'>
            <span class='bubble'></span><span class='fa fa-credit-card ".$mesclass."' aria-hidden='true' style='margin-left: 15px;'></span>
            <div class='".$mesclass."'>".$paystatus."</div>
            <div style='color: #0a242b;font-weight: bold;'><span class='fa fa-clock-o' style='color: #0a242b;margin: 0 0 0 0;'></span>  ".$_Row['rhls_payment_date']."</div>
            </li>";
        if($_Row['rhls_Deliver_status'] == '1'){
            $delstatus = "Subscription Delivered";
            $delclass = "completed";
            $delmesclass = "messagecls";
        }else{
            $delstatus = "Subscription Not Delivered";
            $delclass = "danger";
            $delmesclass = "messageclsalert";
            $messgnot = "Generally RED HAT Learning Subscription Details Delivery Take 7 Working Days.";
        } 
        
        echo"<li class='".$delclass."'>
            <span class='bubble'></span><span class='fa fa-list-alt ".$delmesclass."' style='margin-left: 15px;'></span>
            <div class='".$delmesclass."'>".$delstatus."</div>
            <div style='color: #0a242b;font-weight: bold;'><span class='fa fa-clock-o' style='color: #0a242b;margin: 0 0 0 0;'></span>   ".$messgnot.$_Row['rhcls_Deliver_datetime']."</div>
        </li>";
    echo"</ul>"; 
    }else{
        echo $response[0];
    }   
    
}