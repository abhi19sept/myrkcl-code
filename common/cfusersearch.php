<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsUserSearch.php';

    $response = array();
    $emp = new clsUserSearch();
    if ($_action == "ADD") {
         //print_r($_POST);
				$_ddlempId = $_POST["ddlempId"];
				$_ddlempdistrict = $_POST["ddlempdistrict"];
				$_ddlempTehsil = $_POST["ddlempTehsil"];
				$_ddlempLocation = $_POST["ddlempLocation"];
				
			
            $response = $emp->Add($_ddlempId,$_ddlempdistrict,$_ddlempTehsil,$_ddlempLocation );
            echo $response[0];
        }
    
    if ($_action == "UPDATE") {
        if (isset($_POST["name"])) {
            $_StatusName = $_POST["name"];
            $_StatusDescription = $_POST["description"];
            $_Status_Code=$_POST['code'];
            $response = $emp->Update($_Status_Code,$_StatusName, $_StatusDescription);
            echo $response[0];
        }
    }
    
    
    if ($_action == "EDIT") {


        $response = $emp->GetDatabyCode($_actionvalue);

        $_DataTable = array();
        $_i=0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("StatusCode" => $_Row['Status_Code'],
                "StatusName"=>$_Row['Status_Name'],
                "StatusDescription"=>$_Row['Status_Description']);
            $_i=$_i+1;
        }
        echo json_encode($_Datatable);
    }
    
    
    if ($_action == "DELETE") {


        $response = $emp->DeleteRecord($_actionvalue);

        echo $response[0];
    }


    if ($_action == "SHOW") {


        $response = $emp->GetAll();

        $_DataTable = "";

        echo "<table border='0' cellpedding='0' cellspacing='0' width='100%' class='gridview'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='5%'>S No.</th>";
        echo "<th style='30%'>Name</th>";
        echo "<th style='55%'>Description</th>";
        echo "<th style='10%'>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Status_Name'] . "</td>";
            echo "<td>" . $_Row['Status_Description'] . "</td>";
            echo "<td><a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a>  <a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
//echo $_DataTable;
    }
    if ($_action == "FILL") {
        $response = $emp->GetAll();
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['District_Code'] . ">" . $_Row['District_Name'] . "</option>";
             
     
        }
    }
   
   if ($_action == "FILLID") {
        $response = $emp->GetEmpId();
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['	id'] . ">" . $_Row['id'] . "</option>";
             
     
        }
    }
   
    
    if ($_action == "FillTehsilName") {
        $response = $emp->GettehsilName($_POST['districtid']);
        echo "<option value='0' selected='selected'>Select Tehsil</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['Tehsil_Name'] . ">" . $_Row['Tehsil_Name'] . "</option>";
            
        }
    }
  
?>

