<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsOwnershipChangeReport.php';

$response = array();
$emp = new clsOwnershipChangeReport();


if ($_action == "ShowDetails") {
    $response = $emp->GetAll();
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>New IT-GK Name</th>";
    echo "<th>Ownership Change Ack No</th>";
    echo "<th>IT-GK Code</th>";
    
    echo "<th>IFSC Code</th>";
    echo "<th>Account Holder Name</th>";
    echo "<th>Account Number</th>";
    echo "<th>Bank Name</th>";
    echo "<th>Branch Name</th>";
    echo "<th>MICR Code</th>";
    echo "<th>Account Type</th>";
    echo "<th>Application Date</th>";
    echo "<th>Payment Status</th>";
    echo "<th>Primary Approval Status</th>";
    echo "<th>Final Approval Status</th>";
     echo "<th>View Details</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Ack']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Itgk_Code']) . "</td>";
        
        echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Number']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Branch_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Micr_Code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Type']) . "</td>";
        
        echo "<td>" . date('d-m-Y', strtotime($_Row['Org_App_Date'])) . "</td>";
        if($_Row['Org_PayStatus'] == '0'){
        echo "<td>" . "Payment Not Done" . "</td>";    
        }else if ($_Row['Org_PayStatus'] == '1'){
        echo "<td>" . "Payment Confirmed" . "</td>";     
        }
        echo "<td>" . strtoupper($_Row['Org_Application_Approval_Status']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Final_Approval_Status']) . "</td>";
        echo "<td> <a href='frmviewownershipchangedetails.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit&cc=" . $_Row['Org_Itgk_Code'] ."&ack=" . $_Row['Org_Ack'] ."'>"
            . "<input type='button' name='view_details' id='view_details' class='btn btn-danger' value='View Details'/></a>"
            . "</td>";
        

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}



