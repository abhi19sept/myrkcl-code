<?php

/*
 *  author Mayank
 */

require 'commonFunction.php';
require 'BAL/clsClickAdmission.php';

$response = array();
$emp = new clsClickAdmission();

if ($_action == "FillAdmissionCourse") {
    $response = $emp->GetAdmissionCourseName();	    
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo $_Row['Course_Name'];
    }
}

if ($_action == "FillAdmissionBatch") {
    $response = $emp->GetAdmissionBatchName();
	$_DataTable = array();
    $_i = 0;
		if($response[0]=='Success'){
			while ($_Row = mysqli_fetch_array($response[2])) {
				$_DataTable[$_i] = array("Event_Batch" => $_Row['Event_Batch'],
                "Batch_Name" => $_Row['Batch_Name']);
				$_i = $_i + 1;
			}
			echo json_encode($_DataTable);
		}
		else {
			echo "0";
		}    
}

if ($_action == "getlearnerdetails") {
	global $_ObjFTPConnection;
		if (isset($_POST["lcode"]) && !empty($_POST["lcode"])){
			 $response = $emp->Getclickadmissiondetails($_POST["lcode"]);
				if($response[0]=='Success'){
					$_DataTable = array();
					$_i = 0;
					$_Row = mysqli_fetch_array($response[2]); 
					if($_Row['Myrkcl_Transfer_Status']=='1'){
						echo "1";
					}
					else {
						$details= $_ObjFTPConnection->ftpdetails();
						$ftpaddress = $_ObjFTPConnection->ftpPathIp();
						  $AdmissionPhotoFile = $ftpaddress.'click_admission_photo/' . $_Row['CLICK_Admission_Photo'];
						 
                                
								 $ch = curl_init($AdmissionPhotoFile);
                                curl_setopt($ch, CURLOPT_NOBODY, true);
                                curl_setopt($ch,CURLOPT_TIMEOUT,5000);
                                curl_exec($ch);
                                  $responseCodePhoto = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                                curl_close($ch);
								
								

							if($responseCodePhoto == 200){
								$photopath = '/click_admission_photo/';
								$photoid = $_Row['CLICK_Admission_Photo'];
								
								$photoimage =  file_get_contents($details.$photopath.$photoid);
								$imageData = base64_encode($photoimage);
			$showimage =  "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='80' height='110'/>";			
							}
							else{
								$showimage= 'NA';
							}
						 $AdmissionSignFile = $ftpaddress.'click_admission_sign/' . $_Row['CLICK_Admission_Sign'];
						//die; 
						$ch = curl_init($AdmissionSignFile);
                                curl_setopt($ch, CURLOPT_NOBODY, true);
                                curl_setopt($ch,CURLOPT_TIMEOUT,5000);
                                curl_exec($ch);
                                  $responseCodeSign = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                                curl_close($ch);
						
						if($responseCodeSign == 200){
							$signid = $_Row['CLICK_Admission_Sign'];
						$signpath = '/click_admission_sign/';

						

						$signimage =  file_get_contents($details.$signpath.$signid);
						$imageDatasign = base64_encode($signimage);
			$showimagesign =  "<img id='ftpimg' src='data:image/png;base64, ".$imageDatasign."' alt='Red dot' width='80' height='30'/>";
						}
						else{
								$showimagesign= 'NA';
							}
						

						$_DataTable[$_i] = array("CLICK_Admission_Name" => $_Row['CLICK_Admission_Name'],
												 "CLICK_Admission_Fname" => $_Row['CLICK_Admission_Fname'],
												 "CLICK_Admission_DOB" => $_Row['CLICK_Admission_DOB'],
												 "photo" => $showimage,
												 "sign" => $showimagesign,
												 "CLICK_Admission_Photo" => $_Row['CLICK_Admission_Photo'],
												 "CLICK_Admission_Sign" => $_Row['CLICK_Admission_Sign']);					
					echo json_encode($_DataTable);
					}						
				}
				else {
					echo "no";
				}
		}
		else {
			echo "0";
		}   
}

if ($_action == "Fee") {   
    $response = $emp->GetAdmissionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);   
    echo $_row['Course_Fee'];
}

if ($_action == "GENERATELEARNERCODE") {
    $random = "";
    $random .= (mt_rand(100, 999));
    $random .= date("y");
    $random .= date("m");
    $random .= date("d");
    $random .= date("h");
    $random .= date("i");
    $random .= date("s");
    $random .= '789';

    echo $random;
}


if ($_action == "Install") {    
    $response = $emp->GetAdmissionInstall($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);   
    echo $_row['Admission_Installation_Mode'];
}

if ($_action == "FILLIntake") {    
    $response = $emp->GetIntakeAvailable($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);    
    echo $_row['Intake_Available'];
}

if ($_action == "generateOTP") {
    $aadhaar_no = $_POST['txtACno'];
    $generateOTP = generateOTP($aadhaar_no);
    echo json_encode($generateOTP);
}

/* * *
 * @function generateOTP : function to generate Adhaar OTP
 * @param int $aadhaar_no : The adhaar number of User
 * @param int $pinCode : Area pin code
 * @return array : Returns an array containing the status and transaction id
 * * */

function generateOTP($aadhaar_no) {
    /*     * * DO NOT MODIFY THE VALUES BELOW ** */
     //$licence_key = "MEmVkcpNLahCE-9skCRMK36S_ufQGPaCiNFAZ33o_ICd01JIE6IBLpU";
        //$licence_key = "MPjsZ77ep-Mu0gTYs5_1mOfkfozxJ-3Q2AGHyhhjkTOeg7JSFAolRqI";
		// $licence_key = "MMa5z-ryHS_tvB5t9Hmxyjbte7icRdddVAbDRL_WN1DXyUBLz8bB2-M";
		 //$licence_key ="MJqg1YjQL8GkseVac8FM2g1BUzLmiKN_I6gjETFFD27i6RGUYysHeKo"; //updaated on 30 August 2018//
		 $licence_key ="MAlCZADE4gHodMG6QJ5GRY7-NlR-7A18QUrh2zo-WspGIibbrm0YgOc"; //updaated on 27 August 2019//
		
        $tid = "public";
        //$subaua = "STGDOIT006";
        $subaua = "STGDOIT238";
       // $subaua = "PRKCL22857"; // 27Aug2019 2.5
        $udc = "RKCL123";
        $ip = "127.0.0.1";
        $fdc = "NA";
        $idc = "NA";
        $macadd = "";
        $lot = "P";
        $rc = "Y";
        $pinCode='NA';

        /*** GENERATE AUTH XML BLOCK HERE (DO NOT MODIFY) ***/
        $auth_xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<authrequest uid="' . $aadhaar_no . '" tid="' . $tid . '" subaua="' . $subaua . '" ip="' . $ip . '" fdc="' . $fdc . '" idc="' . $idc . '" udc="' . $udc . '" macadd="' . $macadd . '" lot="' . $lot . '" lov="'.$pinCode.'" lk="' . $licence_key . '" rc="' . $rc . '">
    <otp channel="00"/>
</authrequest>';

        /*** INITIATE CURL REQUEST (DO NOT MODIFY) ***/
        //$ch = curl_init("http://103.203.138.120/api/aua/otp/request/encr");
        $ch = curl_init("https://api.sewadwaar.rajasthan.gov.in/app/live/api/aua/otp/request?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $auth_xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Accept: application/xml",
            "Content-Type: application/xml",
            "appname: MYRKCL"
        ]);


    /*     * * EXECUTE CURL HERE ** */

    $result = curl_exec($ch);
    if (!$result) {
        print curl_errno($ch) . ': ' . curl_error($ch);
    }
    /*     * * CLOSING CURL HERE ** */

    curl_close($ch);

    /*     * * STORE CURL RESPONSE INTO XML ** */

    $xml = @simplexml_load_string($result);

    /*     * * CONVERT XML INTO JSON FORMAT ** */

    $json = json_encode($xml);

    /*     * * CONVERT JSON INTO ARRAY FORMAT ** */

    $array = json_decode($json, true);

    /*     * * RETURNING RESPONSE AND TRANSACTION ** */

    $responseArray = [
        "txn" => $array["auth"]["@attributes"]["txn"],
        "status" => $array["auth"]["@attributes"]["status"]
    ];
    return $responseArray;
}

if ($_action == "authenticateOTP") {
    $aadhaar_no = $_POST['txtACno'];
    $txnID = $_POST['txtTxn'];
    $otp = $_POST['txtotpTxt'];
//    $txtCourse = $_POST['txtCourse'];
//    $txtBatch = $_POST['txtBatch'];
//    $txtLcode = $_POST['txtLcode'];
    $authenticateOTP = authenticateOTPbyEXC($aadhaar_no, $txnID, $otp);

    if ($authenticateOTP['status'] == 'Y') {

        $namew = trim($authenticateOTP['LName']);

        $dobw = trim($authenticateOTP['LDOB']);
        $newDOB = date("Y-m-d", strtotime($dobw));
        $Aadharno = $authenticateOTP['Aadharno'];

        $_DataTable = array();

        $_Datatable = array("learnername" => $namew,
            "learnerdob" => $newDOB,
            "learneraadhar" => $Aadharno,
            "status" => 'y');

        echo json_encode($_Datatable);
    }else {
		$message=$authenticateOTP['message'];
		$_DataTable = array();
		$_Datatable = array("message" => $message,
            "status" => 'n');
       echo json_encode($_Datatable);
    }

}

function authenticateOTPbyEXC($aadhaar_no, $txnID, $otp) {

   //$URL = "http://lms.rkcl.co.in/UIDAIAPI/uidai/otp/auth/" . $aadhaar_no . "/" . $otp . "/" . $txnID;
	 $URL="http://192.168.49.45/UIDAIAPI/uidai/otp/auth/".$aadhaar_no."/".$otp."/".$txnID;
	 //$URL = "http://lms.rkcl.co.in/UIDAIAPI/uidai/otp/auth/" . $aadhaar_no . "/" . $otp . "/" . $txnID;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    $array = json_decode($output, true);

    if ($array["authresponse"]["auth"]["@status"] == 'Y') {
        $responseArray = [
            "status" => $array["authresponse"]["auth"]["@status"],
           "Aadharno" => $array["authresponse"]["UidData"]["@UUID"],
            "LName" => $array["authresponse"]["UidData"]["pi"]["@name"],
            "Fname" => $array["authresponse"]["UidData"]["pa"]["@co"],
            "Gender" => $array["authresponse"]["UidData"]["pi"]["@gender"],
            "LDOB" => $array["authresponse"]["UidData"]["pi"]["@dob"],
            "District" => $array["authresponse"]["UidData"]["pa"]["@dist"],
            "Tehsil" => $array["authresponse"]["UidData"]["pa"]["@loc"],
            "Pincode" => $array["authresponse"]["UidData"]["pa"]["@pc"]
        ];
    } else {
        $responseArray = [
            "status" => $array["authresponse"]["auth"]["@status"],
            "message" => $array["authresponse"]["message"]
        ];
    }
    return $responseArray;
}

if ($_action == "Add"){
	
	global $_ObjFTPConnection;
	
	if ($_POST["ddlAadhar"] == "n") {
        $_POST["paadharname"] = $_POST["txtlname"];
        $_POST["paadhardob"] = trim(date("Y-m-d", strtotime($_POST["dob"])));
    }
	if (strcasecmp(trim($_POST["txtlname"]), trim($_POST["paadharname"])) == 0) {
        if (strcasecmp(trim(date("Y-m-d", strtotime($_POST["dob"]))), trim($_POST["paadhardob"])) == 0) {
		if (isset($_POST["txtLearnercode"]) && !empty($_POST["txtLearnercode"])) {
		if (isset($_POST["txtlname"]) && !empty($_POST["txtlname"])) {
		if (isset($_POST["txtfname"]) && !empty($_POST["txtfname"])) {
		if (isset($_POST["ddlidproof"]) && !empty($_POST["ddlidproof"])) {
		if (isset($_POST["dob"]) && !empty($_POST["dob"])) {
		if (isset($_POST["ddlmotherTongue"]) && !empty($_POST["ddlmotherTongue"])) {
   //   if (isset($_POST["Medium"]) && !empty($_POST["Medium"])) {
		if (isset($_POST["gender"]) && !empty($_POST["gender"])) {
		if (isset($_POST["mstatus"]) && !empty($_POST["mstatus"])) {
		if (isset($_POST["ddlDistrict"]) && !empty($_POST["ddlDistrict"])) {
		if (isset($_POST["ddlTehsil"]) && !empty($_POST["ddlTehsil"])) {
		if (isset($_POST["txtAddress"]) && !empty($_POST["txtAddress"])) {
		if (isset($_POST["txtPIN"]) && !empty($_POST["txtPIN"])) {
		if (isset($_POST["txtmobile"]) && !empty($_POST["txtmobile"])) {
		if (isset($_POST["ddlQualification"]) && !empty($_POST["ddlQualification"])) {
		if (isset($_POST["ddlLearnerType"]) && !empty($_POST["ddlLearnerType"])) {
		if (isset($_POST["PH"]) && !empty($_POST["PH"])) {
		if (isset($_POST["coursecode"]) && !empty($_POST["coursecode"])) {
		if (isset($_POST["batchcode"]) && !empty($_POST["batchcode"])) {
			$_LearnerName = trim($_POST["txtlname"]);
				$_LearnerCode = trim($_POST["txtLearnercode"]);
				$_ParentName = trim($_POST["txtfname"]);
				$_IdProof = trim($_POST["ddlidproof"]);
				$_IdNo = trim($_POST["paadharno"]);
				$_BirthDate = trim($_POST["dob"]);				
				$_DOB = date("Y-m-d", strtotime($_BirthDate));
				$_MotherTongue = trim($_POST["ddlmotherTongue"]);
				$_Medium = 'HindiPlusEnglish';
				$_Gender = trim($_POST["gender"]);
				$_MaritalStatus = trim($_POST["mstatus"]);
				$_District = trim($_POST["ddlDistrict"]);
				$_Tehsil = trim($_POST["ddlTehsil"]);
				$_Address = trim($_POST["txtAddress"]);
				$_PIN = trim($_POST["txtPIN"]);
				$_ResiPh = trim($_POST["txtResiPh"]);
				$_Mobile = trim($_POST["txtmobile"]);
				$_Qualification = trim($_POST["ddlQualification"]);
				$_LearnerType = trim($_POST["ddlLearnerType"]);
				$_PhysicallyChallenged = trim($_POST["PH"]);
				$_GPFno = 'NA';
				$_Email = trim($_POST["txtemail"]);
				$_LearnerCourse = trim($_POST["coursecode"]);
				$_LearnerBatch = trim($_POST["batchcode"]);
				$_LearnerFee = trim($_POST["txtCourseFee"]);
				$_LearnerInstall = trim($_POST["txtInstallMode"]);				
				
				$responsebatchname = $emp->GetBatchName($_LearnerCourse,$_LearnerBatch);
				$_row = mysqli_fetch_array($responsebatchname[2]);   
				$_BatchName= $_row['Batch_Name'];
				
				$_LearnerBatchNameFTP = str_replace(' ', '_', $_BatchName);
                $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';
				
				 $_UploadDirectory1 = "/admission_photo/".$_LearnerBatchNameFTP."";
				$_UploadDirectory2 = "/admission_sign/".$_LearnerBatchNameFTP."";
					
				$_OldPic = '/click_admission_photo/';
				$_OldSign =  '/click_admission_sign/';
				
						$newpicture = $_LearnerCode . '_photo' . '.png';
						$oldpic=$_OldPic . $_POST["txtphoto"];
						$newpic=$_UploadDirectory1.'/' . $newpicture;
						
					//ftp_copy($conn_distant , $pathftp , $pathftpimg ,$img)	
				 ftp_copy($_UploadDirectory1,$newpic,$oldpic,$newpicture);
	
					
					$newsign = $_LearnerCode . '_sign' . '.png';
					$oldsign=$_OldSign . $_POST["txtsign"];
					$newsignature=$_UploadDirectory2.'/' . $newsign;
						
				ftp_copy($_UploadDirectory2,$newsignature,$oldsign,$newsign);
				
				$response = $emp->Add($_LearnerName, $_LearnerCode, $_ParentName, $_IdProof, $_IdNo, $_DOB, $_MotherTongue,
									$_Medium, $_Gender, $_MaritalStatus, $_District, $_Tehsil, $_Address, $_ResiPh, $_Mobile,
									$_Qualification, $_LearnerType, $_GPFno, $_PhysicallyChallenged, $_Email, $_PIN,
									$_LearnerCourse, $_LearnerBatch, $_LearnerFee, $_LearnerInstall, $_POST["ddlAadhar"],$_BatchName);	
				echo $response[0];
				
			} else {
					echo "Inavalid1 Entry";
				}
			} else {
					echo "Inavalid2 Entry";
				}
			} else {
					echo "Inavalid3 Entry";
				}
			} else {
				echo "Inavalid4 Entry";
				}
			} else {
				echo "Inavalid5 Entry";
				}
			} else {
				echo "Inavalid6 Entry";
				}
			} else {
				echo "Inavalid7 Entry";
				}
			} else {
				echo "Inavalid8 Entry";
				}
			} else {
				echo "Inavalid9 Entry";
				}
			} else {
				echo "Inavalid0 Entry";
				}
			} else {
				echo "Inavalid11 Entry";
				}
			} else {
				echo "Inavalid12 Entry";
				}
			} else {
				echo "Inavalid14 Entry";
				}
			} else {
				echo "Inavalid15 Entry";
				}
			} else {
				echo "Inavalid16 Entry";
				}
			} else {
				echo "Inavalid17 Entry";
				}
			} else {
				echo "Inavalid18 Entry";
			}
		}
	} else {
            echo "DOB Not Matched as registered in Aadhar No.";
        }
    } else {
			echo "Learner Name Not Matched as registered in Aadhar No.";
		}
}
