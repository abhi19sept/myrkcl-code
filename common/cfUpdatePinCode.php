<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsUpdatePinCode.php';

$response = array();
$emp = new clsUpdatePinCode();



if ($_action == "GetDetails") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                "usercode" => $_Row['Organization_User'],
                "email" => $_Row['User_EmailId'],
                "mobile" => $_Row['User_MobileNo'],
                "pin" => $_Row['Organization_PinCode'],
                "districtname" => $_Row['District_Name'],
                "tehsilname" => $_Row['Tehsil_Name'],
                "address" => $_Row['Organization_Address']);
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "You are not Auhorized to use this functionality on MYRKCL.";
    }
}

if ($_action == "Update") {
    //print_r($_POST);
    if (isset($_POST["pincode"]) && !empty($_POST["pincode"])) {        
        if (isset($_POST["usercode"]) && !empty($_POST["usercode"])) {
            if($_POST["pincode"]!='000000'){                                              
            $_PinCode = $_POST["pincode"];
            $_OrgCode = $_POST["usercode"];
            
            $response = $emp->Update($_PinCode, $_OrgCode);
            echo $response[0];
			} else {
                echo "Please enter valid Pin Code.";
                   }
            } else {
                echo "Sommething Went Wrong. Reload the page and try again.";
                   }
        
    } else {
        echo "Please enter pin code";
    }
}
