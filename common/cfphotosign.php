<?php
/* 
 * author Mayank
 */
include './commonFunction.php';
require 'BAL/clsPhotoSignProcess.php';

$response = array();
$emp = new clsPhotoSignProcess();

if ($_action == "SHOWALL") {
		$_batch = $_POST['batch'];
		$_course = $_POST['course'];

        $response = $emp->GetAll($_batch,$_course);
        $_DataTable = "";

        echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table-striped table-bordered'>";
			echo "<thead>";		
				echo "<tr>";		
					echo "<th style='5%'>S No.</th>";
					echo "<th style='30%'>CenterCode</th>";       
					echo "<th style='10%'>Total Learner</th>";
					echo "<th style='10%'>Approved Learner</th>";
					echo "<th style='10%'>Pending Leaner</th>";
					echo "<th style='10%'>Rejected Learner</th>";
					echo "<th style='10%'>Reprocess Learner</th>";
				echo "</tr>";
			echo "</thead>";
        echo "<tbody>";
			$_Count = 1;
			if($response[0]=='Success') {
			while ($_Row = mysqli_fetch_array($response[2])) {
				echo "<tr class='odd gradeX'>";
				echo "<td>" . $_Count . "</td>";
				echo "<td>" . $_Row['Admission_ITGK_Code'] . " </td> ";
				echo "<td>" . $_Row['TotalStudent'] . "</td>";
				
			    if($_Row['Approved'] > 0 ) { 
					echo "<td> <a href='frmupdatephotosignstatus.php?code=" . $_Row['Admission_ITGK_Code'] . "&CourseCode=" . $_Row['Admission_Course'] . "&BatchCode=" . $_Row['Admission_Batch'] . "&Mode=Approved'>" . $_Row['Approved'] . "  </a> </td>";
				}
				 else{
					 echo "<td>" . $_Row['Approved'] . "</td>";
				 }
			  
				if($_Row['Pending'] > 0 )
				{
					echo "<td> <a href='frmupdatephotosignstatus.php?code=" . $_Row['Admission_ITGK_Code'] . "&CourseCode=" . $_Row['Admission_Course'] . "&BatchCode=" . $_Row['Admission_Batch'] . "&Mode=Pending'>" . $_Row['Pending'] . "  </a> </td>";
				}
			     else{
					  echo "<td>" . $_Row['Pending'] . "</td>";
				 }
				 
				 if($_Row['Rejected'] > 0 )
				{
					echo "<td> <a href='frmupdatephotosignstatus.php?code=" . $_Row['Admission_ITGK_Code'] . "&CourseCode=" . $_Row['Admission_Course'] . "&BatchCode=" . $_Row['Admission_Batch'] . "&Mode=Rejected'>" . $_Row['Rejected'] . "  </a> </td>";
				}
					else{
						echo "<td>" . $_Row['Rejected'] . "</td>";
					}
					
				if($_Row['Reprocess'] > 0 )
				{
					echo "<td> <a href='frmupdatephotosignstatus.php?code=" . $_Row['Admission_ITGK_Code'] . "&CourseCode=" . $_Row['Admission_Course'] . "&BatchCode=" . $_Row['Admission_Batch'] . "&Mode=Reprocess'>" . $_Row['Reprocess'] . "  </a> </td>";
				}
					else{
						echo "<td>" . $_Row['Reprocess'] . "</td>";
					}
            echo "</tr>";
            $_Count++;
        }
			}
        echo "</tbody>";
        echo "</table>";
		echo "</div>";
//echo $_DataTable;
    }


if ($_action == "FILLCourseName") {
    $response = $emp->GetAllCourse();	
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}


if ($_action == "FILLBatchName") {
    $response = $emp->GetBatchName($_POST['values']);	
    echo "<option value=''>Select Batch</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}



if ($_action == "FillLearnerDetail") {
	$code = $_POST['values'];
	$course = $_POST['course'];
	$batch = $_POST['batch'];
	$mode = $_POST['mode'];
	
    $response = $emp->GetAllLearner($code,$course,$batch,$mode);	
    $_DataTable = "";
		
		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='5%'>S No.</th>";
        echo "<th style='8%'>CenterCode</th>";	
		echo "<th style='8%'>Name</th>";
		echo "<th style='8%'>Gender</th>";
        echo "<th style='15%'>Photo</th>";
		echo "<th style='15%'>Sign</th>";
		
		if($mode == 'Pending' || $mode == 'Reprocess') {
			echo "<th style='15%'>Action</th>";
			echo "<th style='10%'>Rejected Reason</th>";
			echo "<th style='10%'>Rejected Type</th>";
		}
		  
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
			
			echo "<td>" . $_Row['Admission_ITGK_Code'].  "</td> ";
			
			echo "<td>" . $_Row['Admission_Name'].  "</td> ";
			
			echo "<td>" . $_Row['Admission_Gender'].  "</td> ";
			
			if($_Row['Admission_Photo']!="")
				{
					$image = $_Row['Admission_Photo'];
					echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/'.$image.'"/>' . "</td>";
				}
			else
				{
					echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
				}
		if($_Row['Admission_Sign']!="")
			{
				$sign = $_Row['Admission_Sign'];
				echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/'.$sign.'"/>' . "</td>";
			}
		else
				{
					echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
				}
		if($mode == 'Pending' || $mode == 'Reprocess')
		{				
			echo "<td> <input type='radio' name= chk".$_Row['Admission_Code']."   id=chk".$_Row['Admission_Code'].".Rejected value='Rejected'> Rejected "	
					. "<input type='radio' name= chk".$_Row['Admission_Code']."   id=chk".$_Row['Admission_Code'].".Approved value='Approved'  checked> Approved"
				. "</td>";
				
			echo "<td> <select name='reason" . $_Row['Admission_Code'].  "' id='reason" . $_Row['Admission_Code'].  "'>"	
					. "<option value='Bad Photo Quality'> Bad Photo Quality </option>"
					. "<option value='Invalid Gender'> Invalid Gender </option>"
					. "<option value='Duplicate Image'> Duplicate Image </option>"
					. "<option value='Incorrect Size of Image'> Incorrect Size of Image </option>"
					. "<option value='Incorrect/Bad Quality Sign'> Incorrect/Bad Quality Sign </option>"
					. "<option value='Photo/Sign not Uploaded'> Photo/Sign not Uploaded </option>"
					. "<option value='Face/Ears not clearly visible'> Face/Ears not clearly visible </option>"
					. "<option value='Image is not bright'> Image is not bright </option>"
					. "<option value='Photo from social function/occ'> Photo from social function/occ </option>"
					. "<option value='Overlay of color in photo'> Overlay of color in photo </option>"
				. "</td>";
				
			echo "<td> <select name='type" . $_Row['Admission_Code'].  "' id='type" . $_Row['Admission_Code'].  "'>"	
					. "<option value='Both' Selected='selected'> Both </option>"
					. "<option value='Photo'> Photo </option>"
					. "<option value='Sign'> Sign </option>"
				. "</td>";
		}
			
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
		echo "</div>";
}


if ($_action == "UPDATE") {
    //print_r($_POST);
	$_UserPermissionArray=array();	
        $_Count=0;		
        $_Counts=0;
            foreach ($_POST as $key => $value)
            {
               if(substr($key, 0,3)=='chk')
               {                  
                  $_UserPermissionArray[$_Count]=array(
                       "Function" => substr($key, 3),
					   "Values" => $value,
					   "Reason" => $_POST['reason'.substr($key, 3)],
					   "Type" => $_POST['type'.substr($key, 3)]);
                   $_Count++;			  
               }			     
			}            
                $response1=$emp->Update_System($_UserPermissionArray);				
		echo $response1[0];  	   
}





if ($_action == "SHOWREJECTED") {
		$_batch = $_POST['batch'];
		$_course = $_POST['course'];

        $response = $emp->GetAllRejected($_batch,$_course);

        $_DataTable = "";

        echo "<div class='table-responsive'>";
		echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
        echo "<thead>";
        echo "<tr>";
		
        echo "<th style='5%'>S No.</th>";
        echo "<th style='8%'>CenterCode</th>";	
		echo "<th style='8%'>Name</th>";
		echo "<th style='8%'>Gender</th>";
        echo "<th style='15%'>Photo</th>";
		echo "<th style='15%'>Sign</th>";
		echo "<th style='15%'>Reason</th>";
		echo "<th>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
           echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
			
			echo "<td>" . $_Row['Admission_ITGK_Code'].  "</td> ";
			
			echo "<td>" . $_Row['Admission_Name'].  "</td> ";
			
			echo "<td>" . $_Row['Admission_Gender'].  "</td> ";	

			if($_Row['Admission_Photo']!="")
				{
					$image = $_Row['Admission_Photo'];
					echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/'.$image.'"/>' . "</td>";
				}
			else
				{
					echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
				}
		if($_Row['Admission_Sign']!="")
			{
				$sign = $_Row['Admission_Sign'];
				echo "<td>" . '<img alt="No Image Found" width="80" height="35" src="upload/admission_sign/'.$sign.'"/>' . "</td>";
			}
		else
				{
					echo "<td>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
				}
		echo "<td>" . $_Row['rejection_reason'].  "</td> ";
		
		echo "<td> <a href='frmeditrejectedlearner.php?code=" . $_Row['Admission_Code'] . "&Mode=Edit'>"
					. "<input type='button' name='Rejected' id='Rejected' class='btn btn-primary' value='Edit Learner'/></a>  "					 
			. "</td>";
			
		echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
		echo "</div>";
//echo $_DataTable;
    }
	
	