<?php

/**
 * Description of cfPrintRecpReexam
 *
 * @author Abhishek
 */
include 'commonFunction.php';
require 'BAL/clsPrintRecpReexam.php';
require 'DAL/upload_ftp_doc.php';
$response = array();
$emp = new clsPrintRecpReexam();


if ($_action == "schedule") {
    $response = $emp->GetDataAll($_POST['examevent']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th>Father Name</th>";
    echo "<th>Print Invoice</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['itgkcode'] . "</td>";
        echo "<td>" . $_Row['learnercode'] . "</td>";

        echo "<td>" . $_Row['learnername'] . "</td>";
        echo "<td>" . $_Row['fathername'] . "</td>";
        if ($_Row['Reexam_Transaction_timestamp'] >= '2017-07-01 00:00:00') {
            echo "<td class='org'><button type='button' data-toggle='modal' data-target='#approvalLetter' class='btn btn-primary approvalLetter' eid='" . $_Row['examdata_code'] . "'>Download Invoice</button></td>";
        } else {
            echo "<td>Invoice Not Available</td>";
        }

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

/**if ($_action == "DwnldPrintRecp") {
    $path = $_SERVER['DOCUMENT_ROOT'].'/upload/ReexamPrintRecpt/';

    $learnerCode = $_POST['eid'];
    $file = $_POST['eid'] . '.fdf';
    $filePath = $path . $file;
    $allreadyfoundAkcRecipt = $path . $learnerCode . '_ReexamInvoice.pdf';

    $response = $emp->GetAllPrintRecpDwnld($_POST['eid']);

    $co = mysqli_num_rows($response[2]);
    $res = '';
    if ($co) {
      
        $_Row1 = mysqli_fetch_array($response[2], true);


        $_Row1["invoice_no"] = "REX/" . $_Row1['invoice_no'];
        $_Row1["invoice_date"] = date("d-m-Y", strtotime($_Row1['addtime']));
        $_Row1["Lname"] = strtoupper($_Row1['learnername']);
        $_Row1['Fname'] = strtoupper($_Row1['fathername']);
        $_Row1["Lcode"] = $_Row1['learnercode'];
        $_Row1["Laddress"] = strtoupper($_Row1['District_Name']);
        $_Row1["Ldob"] = date("d-m-Y", strtotime($_Row1['dob']));
        $_Row1["Lmobile"] = $_Row1['Admission_Mobile'];
        $_Row1["Lbatch"] = $_Row1['Batch_Name'];

        $_Row1["Litgk"] = $_Row1['itgkcode'];
        $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
        $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
        $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'].'.00';
        $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
        $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
        $_Row1["cgstvalue"] = "9";
        $_Row1["sgstvalue"] = "9";
        $_Row1["totalrkclplus"] = ($_Row1["Gst_Invoce_BaseFee"] + $_Row1["cgst"] + $_Row1["cgst"]) . "0";
        $_Row1["totaltaxless"] = ($_Row1["cgst"] + $_Row1["cgst"]) . "0";

        $_Row1["amounttxt"] = $emp->convert_number_to_words($_Row1['Gst_Invoce_TutionFee']) . " Only";
        $_Row1["totalrkcl"] = ($_Row1['Gst_Invoce_TutionFee']) . ".00";
        $_Row1["Enrollmentfee"] = $_Row1['Gst_Invoce_VMOU'] . '.00';

        $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'] . '.00';
        $_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
        $_Row1["coursedec"] = "RS-CIT (Digital Literacy Course)";
        $_Row1["itgkshare"] = '50.00';
        $fdfContent = '%FDF-1.2
			%Ã¢Ã£Ã?Ã“
			1 0 obj 
			<<
			/FDF 
			<<
			/Fields [';
        foreach ($_Row1 as $key => $val) {
            $fdfContent .= '
					<<
					/V (' . $val . ')
					/T (' . $key . ')
					>> 
					';
        }
        $fdfContent .= ']
			>>
			>>
			endobj 
			trailer
			<<
			/Root 1 0 R
			>>
			%%EOF';
        file_put_contents($filePath, $fdfContent);

        $fdfFilePath = $filePath;
        $fdfPath = str_replace('/', '//', $fdfFilePath);
        $resultFile = '';
        //$defaulPermissionLetterFilePath = getPermissionLetterPath() . '_ITGK_Approval_Letter.pdf';
        // echo "sunil";
    //    $defaulPermissionLetterFilePath = $_SERVER['DOCUMENT_ROOT'].'/upload/ReexamPrintRecpt/_ReexamInvoice.pdf'; //die;
	
	if($_Row1['Gst_Invoce_TotalFee'] == '350'){
        $defaulPermissionLetterFilePath = $_SERVER['DOCUMENT_ROOT'].'/upload/ReexamPrintRecpt/_ReexamInvoice_01_08_19.pdf'; //die;
}
else{
        $defaulPermissionLetterFilePath = $_SERVER['DOCUMENT_ROOT'].'/upload/ReexamPrintRecpt/_ReexamInvoice.pdf'; //die;
}

        if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
            $defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);
            $resultFile = $learnerCode . '_ReexamInvoice.pdf';

            $newURL = $path . $resultFile;

            //$resultPath = str_replace('/', '//', getPermitionLetterFilePath($learnerCode));
            $resultPath = str_replace('/', '//', $newURL);
            $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            // $pdftkPath = ($_SERVER['HTTP_HOST'] == "click.rkcl.in") ? '"C://inetpub//vhosts//rkcl.in//click.rkcl.in//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            //$pdftkPath = ($_SERVER['HTTP_HOST']=="10.1.1.20") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';

            $command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
            
			exec($command);
			unlink($fdfFilePath);
            $emp->addPhotoInPDF($newURL, 1);
            
            $filepath5 = 'upload/ReexamPrintRecpt/' . $resultFile;
            echo $filepath5;


            $res = "DONE";
        }
    } else {
        echo "";
    }
**/
if ($_action == "DwnldPrintRecp") {
    $path = $_SERVER['DOCUMENT_ROOT'].'/upload/ReexamPrintRecpt/';

    $learnerCode = $_POST['eid'];
    $file = $_POST['eid'] . '.fdf';
    $filePath = $path . $file;
    $allreadyfoundAkcRecipt = $path . $learnerCode . '_ReexamInvoice.pdf';

    $response = $emp->GetAllPrintRecpDwnld($_POST['eid']);

    $co = mysqli_num_rows($response[2]);
    $res = '';
    if ($co) {
      
        $_Row1 = mysqli_fetch_array($response[2], true);


        $_Row1["invoice_no"] = "REX/" . $_Row1['invoice_no'];
        $_Row1["invoice_date"] = date("d-m-Y", strtotime($_Row1['addtime']));
        $_Row1["Lname"] = strtoupper($_Row1['learnername']);
        $_Row1['Fname'] = strtoupper($_Row1['fathername']);
        $_Row1["Lcode"] = $_Row1['learnercode'];
        $_Row1["Laddress"] = strtoupper($_Row1['District_Name']);
        $_Row1["Ldob"] = date("d-m-Y", strtotime($_Row1['dob']));
        $_Row1["Lmobile"] = $_Row1['Admission_Mobile'];
        $_Row1["Lbatch"] = $_Row1['Batch_Name'];

        $_Row1["Litgk"] = $_Row1['itgkcode'];
        $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
        $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
        $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'];
        $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
        $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
        $_Row1["cgstvalue"] = "9";
        $_Row1["sgstvalue"] = "9";
        $_Row1["totalrkclplus"] = ($_Row1["Gst_Invoce_BaseFee"] + $_Row1["cgst"] + $_Row1["cgst"]) . "0";
        $_Row1["totaltaxless"] = ($_Row1["cgst"] + $_Row1["cgst"]) . "0";

        $_Row1["amounttxt"] = $emp->convert_number_to_words($_Row1['Gst_Invoce_TutionFee']) . " Only";
        $_Row1["totalrkcl"] = ($_Row1['Gst_Invoce_TutionFee']) . ".00";
        $_Row1["Enrollmentfee"] = $_Row1['Gst_Invoce_VMOU'] . '.00';
        if($_SESSION['User_UserRoll'] == '19') {

        $_Row1["Totalfee"] = ($_Row1['Gst_Invoce_TotalFee']-50) . '.00';
        $_Row1["Totalfee1"] = ($_Row1['Gst_Invoce_TotalFee']-50);
        }
        else{

        $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'] . '.00';
        $_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
        }                                                                                            
        $_Row1["coursedec"] = "RS-CIT (Digital Literacy Course)";
        $_Row1["itgkshare"] = '50.00';
        $fdfContent = '%FDF-1.2
			%Ã¢Ã£Ã?Ã“
			1 0 obj 
			<<
			/FDF 
			<<
			/Fields [';
        foreach ($_Row1 as $key => $val) {
            $fdfContent .= '
					<<
					/V (' . $val . ')
					/T (' . $key . ')
					>> 
					';
        }
        $fdfContent .= ']
			>>
			>>
			endobj 
			trailer
			<<
			/Root 1 0 R
			>>
			%%EOF';
        file_put_contents($filePath, $fdfContent);

        $fdfFilePath = $filePath;
        $fdfPath = str_replace('/', '//', $fdfFilePath);
        $resultFile = '';
        //$defaulPermissionLetterFilePath = getPermissionLetterPath() . '_ITGK_Approval_Letter.pdf';
        // echo "sunil";
        // date_default_timezone_set("Asia/Kolkata");
        // $datetoday = date("Y-m-d");
      

if($_Row1['Gst_Invoce_TotalFee'] == '350' && $_SESSION['User_UserRoll'] == '7'){
        $defaulPermissionLetterFilePath = $_SERVER['DOCUMENT_ROOT'].'/upload/ReexamPrintRecpt/_ReexamInvoice_01_08_19.pdf'; //die;
}
else{
        $defaulPermissionLetterFilePath = $_SERVER['DOCUMENT_ROOT'].'/upload/ReexamPrintRecpt/_ReexamInvoice.pdf'; //die;
}
        if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
            $defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);
            $resultFile = $learnerCode . '_ReexamInvoice.pdf';

            $newURL = $path . $resultFile;

            //$resultPath = str_replace('/', '//', getPermitionLetterFilePath($learnerCode));
            $resultPath = str_replace('/', '//', $newURL);
            $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            // $pdftkPath = ($_SERVER['HTTP_HOST'] == "click.rkcl.in") ? '"C://inetpub//vhosts//rkcl.in//click.rkcl.in//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            //$pdftkPath = ($_SERVER['HTTP_HOST']=="10.1.1.20") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';

            $command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
            
			exec($command);
			// unlink($fdfFilePath);
            $emp->addPhotoInPDF($newURL, 1);
            
            // $filepath5 = 'upload/ReexamPrintRecpt/' . $resultFile;
            // echo $filepath5;
            $filepath5 = 'common/showpdfftp.php?src=ReexamPrintRecpt/' . $resultFile;
            unlink($fdfFilePath);
            $directoy = '/ReexamPrintRecpt/';
            $file = $resultFile;
            $response_ftp = ftpUploadFile($directoy, $file, $newURL);
            if(trim($response_ftp) == "SuccessfullyUploaded"){
                
                echo $filepath5;
            }

            $res = "DONE";
        }
    } else {
        echo "";
    }
    function getPermissionLetterPath() {
        $path = '../upload/ReexamPrintRecpt/';
        makeDir($path);

        return $path;
    }

    function getPermitionLetterFilePath($code) {
        $resultFile = $code . '_ReexamInvoice.pdf';
        // $resultPath = getPermissionLetterPath() . $resultFile;
        $resultPath = '../upload/ReexamPrintRecpt/' . $resultFile;

        return $resultPath;
    }

    function makeDir($path) {
        return is_dir($path) || mkdir($path);
    }

}

if ($_action == "delgeninvoice") {
    $name = trim($_POST['values']);
    $image_url = $_SERVER['DOCUMENT_ROOT'] . '/' . $name;
    //print_r($image_url);
    if (file_exists($image_url)) {
        unlink($image_url);
    } else {
        die('file does not exist');
    }
}

function convert_number_to_words($number) {

    $hyphen = ' ';
    $conjunction = ' and ';
    $separator = ', ';
    $negative = 'negative ';
    $decimal = ' point ';
    $dictionary = array(
        0 => 'Zero',
        1 => 'One',
        2 => 'Two',
        3 => 'Three',
        4 => 'Four',
        5 => 'Five',
        6 => 'Six',
        7 => 'Seven',
        8 => 'Eight',
        9 => 'Nine',
        10 => 'Ten',
        11 => 'Eleven',
        12 => 'Twelve',
        13 => 'Thirteen',
        14 => 'Fourteen',
        15 => 'Fifteen',
        16 => 'Sixteen',
        17 => 'Seventeen',
        18 => 'Eighteen',
        19 => 'Nineteen',
        20 => 'Twenty',
        30 => 'Thirty',
        40 => 'Fourty',
        50 => 'Fifty',
        60 => 'Sixty',
        70 => 'Seventy',
        80 => 'Eighty',
        90 => 'Ninety',
        100 => 'Hundred',
        1000 => 'Thousand',
        1000000 => 'Million',
        1000000000 => 'Billion',
        1000000000000 => 'Trillion',
        1000000000000000 => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens = ((int) ($number / 10)) * 10;
            $units = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}

?>