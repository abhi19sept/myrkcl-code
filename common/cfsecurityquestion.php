<?php

/* 
 *  author SUNIL KUAMR BAINDARASSSSS

 */

include './commonFunction.php';
require 'BAL/clssecurityquestion.php';

$response = array();
$emp = new clssecurityquestion();


if ($_action == "ADD") {
    if (isset($_POST["Security_Question"]) && !empty($_POST["Security_Question"])) {
        $Security_Question = addslashes($_POST["Security_Question"]);
        $Security_Status = $_POST["status"];
        $response = $emp->Add($Security_Question,$Security_Status);
        echo $response[0];
    }else{
        echo "Please Enter Security Question";
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["Security_Question"])) {
        $Security_Question = $_POST["Security_Question"];
        $Security_Status = $_POST["status"];
        $SQ_ID = $_POST['code'];
        $response = $emp->Update($SQ_ID, $Security_Question, $Security_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("Security_Question" => $_Row['Security_Question'],
		"Security_Status" => $_Row['Security_Status'] );
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();   

    echo "<table  id='example' class='table table-striped table-bordered' border='0' cellpedding='0' cellspacing='0' width='100%' class='gridview'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='30%'>Name</th>";
    echo "<th style='55%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
            if( $_Row['Security_Status']==1 ) {
                $x = 'Active';
            } else {
                $x = 'Not Active';
            }
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Security_Question'] . "</td>";
        echo "<td>" . $x . "</td>";
        echo "<td><a href='frmsecurityquestion.php?code=" . $_Row['SQ_ID'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmsecurityquestion.php?code=" . $_Row['SQ_ID'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Status</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['CourseType_Code'] . ">" . $_Row['CourseType_Name'] . "</option>";
    }
}