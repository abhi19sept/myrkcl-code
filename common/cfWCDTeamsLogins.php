<?php

/*
 * author Abhishek

 */
include './commonFunction.php';
require 'BAL/clsWCDTeamsLogins.php';

$response = array();
$emp = new clsWCDTeamsLogins();
$category = array();

if ($_action == "SHOWALL") {
    $response = $emp->GetAllLearner($_POST['batch'], $_POST['course']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";

    echo "<th style='10%'>Learner Code</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    echo "<th style='8%'>Learner Teams User Name</th>";
    echo "<th style='8%'>Learner Teams Initial Password</th>";
    echo "<th style='8%'>Learner Mobile</th>";
    echo "<th style='8%'>ITGK Code</th>";
    echo "<th style='8%'>ITGK Name</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Learner_Code'] . "</td>";
        echo "<td>" . strtoupper($_Row['Learner_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Father_Name']) . "</td>";
        echo "<td>" . $_Row['Teams_ID'] . "</td>";
        echo "<td>" . $_Row['Teams_Password'] . "</td>";
        echo "<td>" . $_Row['Mobile'] . "</td>";
        echo "<td>" . $_Row['ITGK_Code'] . "</td>";
        echo "<td>" . $_Row['ITGK_Name'] . "</td>";


        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
if ($_action == "SHOWALLITGK") {
    $response = $emp->GetAllITGK($_POST['batch'], $_POST['course']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";

    echo "<th style='10%'>ITGK Code</th>";
    echo "<th style='10%'>ITGK Name</th>";
    echo "<th style='8%'>ITGK Mobile</th>";
    echo "<th style='8%'>ITGK Teams User Name</th>";
    echo "<th style='8%'>ITGK Teams Initial Password</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['ITGK_Code'] . "</td>";
        echo "<td>" . $_Row['ITGK_Name'] . "</td>";
        echo "<td>" . $_Row['ITGK_Mobile'] . "</td>";
        echo "<td>" . $_Row['Teams_User_Name'] . "</td>";
        echo "<td>" . $_Row['Teams_Initial_Password'] . "</td>";


        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
if ($_action == "FILLCourse") {
    $response = $emp->GetAdmissionCourse(); 
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}   
if ($_action == "FILLEventBatch") {
    $response = $emp->FILLEventBatch($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Batch'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

