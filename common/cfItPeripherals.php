<?php

/* 
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsItPeripherals.php';

$response = array();
$emp = new clsItPeripherals();


if ($_action == "ADD") {
    if (isset($_POST["name"])) {
        $_DeviceName = $_POST["name"];
    	$_Available = $_POST["available"];
    	$_Make = $_POST["make"];
    	$_Model = $_POST["model"];
    	$_Quantity = $_POST["quantity"];
    	$_Detail = $_POST["detail"];
	//echo $_DeviceName;
        $response = $emp->Add($_DeviceName,$_Available,$_Make,$_Model,$_Quantity,$_Detail);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        
        $_DeviceName = $_POST["name"];
        //echo $_DeviceName;
    	$_Available = $_POST["available"];
    	$_Make = $_POST["make"];
    	$_Model = $_POST["model"];
    	$_Quantity = $_POST["quantity"];
    	$_Detail = $_POST["detail"];
        $_Code=$_POST["code"];
        $response = $emp->Update($_Code,$_DeviceName,$_Available,$_Make,$_Model,$_Quantity,$_Detail);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("ItPeripheralsCode" => $_Row['IT_Peripherals_Code'],
            "Name" => $_Row['IT_Peripherals_Device_Type'],
            "Available" => $_Row['IT_Peripherals_Availablity'],
            "Make" => $_Row['IT_Peripherals_Make'],
            "Model" => $_Row['IT_Peripherals_Model'],
            "Quantity" => $_Row['IT_Peripherals_Quantity'],
            "Detail" => $_Row['IT_Peripherals_Detail'],);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>Device Name</th>";
    echo "<th style='10%'>Availablity</th>";
    echo "<th style='20%'>Make</th>";
    echo "<th style='20%'>Model</th>";
    echo "<th style='10%'>Quantity</th>";
    echo "<th style='20%'>Details</th>";
	echo "<th style='20%'>User code</th>";
    echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Device_Name'] . "</td>";
         echo "<td>" . $_Row['IT_Peripherals_Availablity'] . "</td>";
         echo "<td>" . $_Row['IT_Peripherals_Make'] . "</td>";
         echo "<td>" . $_Row['IT_Peripherals_Model'] . "</td>";
         echo "<td>" . $_Row['IT_Peripherals_Quantity'] . "</td>";
         echo "<td>" . $_Row['IT_Peripherals_Detail'] . "</td>";
		  echo "<td>" . $_Row['IT_UserCode'] . "</td>";
        echo "<td><a href='frmItPeripherals.php?code=" . $_Row['IT_Peripherals_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a></td>  ";
       
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value=''>Select Device</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Device_Code'] . ">" . $_Row['Device_Name'] . "</option>";
    }
}