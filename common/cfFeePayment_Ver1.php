<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsFeePayment_Ver1.php';
$response = array();
$emp = new clsFeePayment();

if ($_action == "ADD") {    
    $amount = $_POST["amounts"];    
    $_UserPermissionArray = array();
    $_AdmissionCode = array();
    $_i = 0;
    $_Count = 0;
    $l = "";
    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 3) == 'chk') {
            $_UserPermissionArray[$_Count] = array(
                "Function" => substr($key, 3),
                "Attribute" => 3,
                "Status" => 1);

            $_Count++;
			$l .= substr($key, 3) . ",";           
        }
        $_AdmissionCode = rtrim($l, ",");        
        $count = count($_UserPermissionArray);
        $amount1 = $count * $amount;
    }
    if ($amount1 == '0') {
        echo "0";
    } else {				
        $response = $emp->AddPayTran($_POST['ddlBatch'], $_POST['ddlCourse'], $amount1, $_AdmissionCode, $_POST['txtGenerateId'], $count, $_POST['txtGeneratePayUId']);
        if($_POST['gateway'] == 'razorpay') {
            //Pay By Razorpay Start
            $itgk = $general->getTransactionItgkDetails($_POST['txtGeneratePayUId']);
            $details = [
                "pay_for" => $itgk['payfor'],
                "name"    => "ITGK(" . $itgk['Pay_Tran_ITGK'] . ")",
                "email"   => $itgk['User_EmailId'],
                "phone"   => $itgk['User_MobileNo'],
                "address" => $itgk['Organization_Address'],
                "orderId" => $itgk['Pay_Tran_Code'],
                "firstname"=> $itgk['Pay_Tran_Fname'],
                "itgkcode"=> $itgk['Pay_Tran_ITGK'],
                "txnid"   => $itgk['Pay_Tran_PG_Trnid'],
                "batch"   => $itgk['Pay_Tran_Batch'],
                "course"  => $itgk['Pay_Tran_Course'],
                "amount"  => $itgk['Pay_Tran_Amount'],
            ];
            require("razorpay.php");
        } else {
            $result = [];
            //$_SESSION['AdmissionArray'] = $_AdmissionCode;
            //$cid = $emp->
            $result['payby'] = "payu";
            $result['amount'] = $amount1;
            echo json_encode($result);
        }
    }
}

if ($_action == "SHOWALL") {
    $response = $emp->GetAll($_POST['batch'], $_POST['course'], $_POST['paymode']);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    echo "<th style='12%'>D.O.B</th>";
    echo "<th style='10%'>Photo</th>";
    echo "<th style='10%'>Sign</th>";

    echo "<th style='10%'>Amount</th>";   
    echo "<th style='8%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $response1 = $emp->GetAdmissionFee($_POST['batch']);
    $_row1 = mysqli_fetch_array($response1[2]);
    $fee = ($_row1['RKCL_Share'] + $_row1['VMOU_Share']);
   
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {

            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";

            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="80" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="80" src="images/user icon big.png"/>' . "</td>";
            }
            if ($_Row['Admission_Sign'] != "") {
                $sign = $_Row['Admission_Sign'];
                echo "<td>" . '<img alt="No Image Found" width="80" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="80" src="images/no_image.png"/>' . "</td>";
            }

            echo "<td>" . $fee . "</td>";

            if ($_Row['Admission_Payment_Status'] == '0') {
                echo "<td><input type='checkbox' id=chk" . $_Row['Admission_Code'] .
                " name=chk" . $_Row['Admission_Code'] . "></input></td>";
            } elseif ($_Row['Admission_Payment_Status'] == '8') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='DD in process'>"
                . "</td>";
            } elseif ($_Row['Admission_Payment_Status'] == '1') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='Payment Confirmed'>"
                . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    } else {        
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "Fee") {    
    $response = $emp->GetAdmissionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);
    $RKCLshare = $_row['RKCL_Share'];
    $VMOUShare = $_row['VMOU_Share'];
    $totalshare = $RKCLshare + $VMOUShare;
    echo $totalshare;
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();	
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
	
	$key = "X2ZPKM";
	$salt = "8IaBELXB";
	$command1 = "check_isDomestic";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	
	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);		
	$_DataTable = array();
    $_i = 0;
		$_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
				echo json_encode($_DataTable);		
 }

 if ($_action == "FinalValidateCard") {	
	$key = "X2ZPKM";
	$salt = "8IaBELXB";
	$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	
	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
   // $wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);	 
	if($response == 'Invalid'){
		echo "1";
	}
	else if($response == 'valid'){
		echo "2";
	}
 }
 
function curlCall($wsUrl, $qs, $true){
   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
		if (curl_errno($c)) {
		  $c_error = curl_error($c);
		  if (empty($c_error)) {
			  $c_error = 'Some server error';
			}
			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);
		return $arr;
}
?>	