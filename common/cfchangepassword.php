<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsforgotpassword.php';
require 'BAL/clschangepassword.php';
require 'DAL/sendsms.php';
include 'mail_sent.php';

    $response = array();
    $emp = new clschangepassword();
    $forgotPass = new clsforgotpassword();

    if ($_action == "CHANGE") {
		//print_r($_POST);
        if (isset($_POST["old_password"]) && !empty($_POST["old_password"]) && !empty($_POST["new_password"]) && !empty($_POST["confirm_password"]) && $_POST["new_password"] === $_POST["confirm_password"]) {
			$_old = $_POST["old_password"];
			$_new = trim($_POST["new_password"]);
			$_confirm = trim($_POST["confirm_password"]);
			$response = $emp->VerifyToChangePassword($_old, $_new, $_confirm);
			if ($response[0] == 1) {
				$_row = $response[2];
				$_Response = $forgotPass->SendOTP($_row['User_LoginId']);
				if ($_Response[0] != Message::NoRecordFound) {
	                $otpRow = mysqli_fetch_array($_Response[2]);
	                $_Mobile =  "XXXXXX". substr($otpRow['AO_Mobile'], 6);
	                $_OTP = $otpRow["AO_OTP"];
	                $_SMS = "Dear MYRKCL User ". $_row['User_LoginId'] .", Your verification OTP is " . $_OTP; 
	                SendSMS($otpRow['AO_Mobile'], $_SMS);
	                $result = [
	                    'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
	                    'frm' => $forgotPass->getVeriFyOTPForm($otpRow["AO_Code"], $_row['User_LoginId']) . '<input type="hidden" value="' . $_new . '" name="keyid" id="keyid">',
	                ];

	                echo json_encode($result);
	            } else {
	                echo '0';
	            }
			} else {
				echo $response[0];
			}
		} else {
			echo '0';
		}
    } 
    elseif ($_action == "VerifyOTP" && isset($_POST["oid"]) && !empty($_POST["oid"])) {
        $response = $forgotPass->VerifyOTP($_POST["oid"], $_POST["otp"]);
        if (mysqli_num_rows($response[2])) {
        	$emp->updatePassword($_POST["keyid"]);
             $result = [
                'msg' => 'Your login details has been updated successfully.',
            ];

            echo json_encode($result);
        } else {
            echo '0';
        }
    }
    
    
    
    
     if ($_action == "CHANGEPASSWORD") {
		//print_r($_POST);
        if (isset($_POST["old_password"]) && !empty($_POST["old_password"]) && !empty($_POST["psw"]) && !empty($_POST["repsw"]) && $_POST["psw"] === $_POST["repsw"]) {
			$_old = $_POST["old_password"];
			$_new = $_POST["psw"];
			$_confirm = $_POST["repsw"];
			$response = $emp->VerifyToChangePasswordNEW($_old, $_new, $_confirm);
                        //echo "<pre>"; print_r($response);die;
			if ($response[0] == 'Success') {
				$_row = $response[2];
				$_Response = $forgotPass->SendOTP($_row['User_LoginId']);
				if ($_Response[0] != Message::NoRecordFound) {
	                $otpRow = mysqli_fetch_array($_Response[2]);
	                $_Mobile =  "XXXXXX". substr($otpRow['AO_Mobile'], 6);
	                $_OTP = $otpRow["AO_OTP"];
	                $_SMS = "Dear MYRKCL User ". $_row['User_LoginId'] .", Your verification OTP is " . $_OTP; 
	                SendSMS($otpRow['AO_Mobile'], $_SMS);
	                $result = [
	                    'msg' => 'Dear MYRKCL User please verify the OTP that you will receive in your registered Mobile No. ' . $_Mobile,
	                    'frm' => $forgotPass->getVeriFyOTPForm($otpRow["AO_Code"], $_row['User_LoginId']) . '<input type="hidden" value="' . $_new . '" name="keyid" id="keyid">',
	                ];

	                echo json_encode($result);
	            } else {
	                echo '0';
	            }
			} else {
				echo $response[0];
			}
		} else {
			echo '0';
		}
    } 
     elseif ($_action == "VerifyOTPNEW" && isset($_POST["oid"]) && !empty($_POST["oid"])) {
        $response = $forgotPass->VerifyOTP($_POST["oid"], $_POST["otp"]);
        //echo "<pre>"; print_r($response);die;
        if ($response[0] == 'Success') 
        {
        	$responceNew = $emp->updateUserPasswordNew($_POST["keyid"]);
                //echo "<pre>"; print_r($responceNew);die;
                if ($responceNew[0]=='Successfully Inserted') 
                {
                    echo '1';
                } 
                else if ($responceNew[0]=='MORE3') 
                {
                    echo 'MORE3';
                } 
                else {
                    echo '0';
                }
                
                //$result = ['msg' => 'Your login details has been updated successfully.',];

           // echo json_encode($result);
        } else {
            echo '0';
        }
    }
?>
