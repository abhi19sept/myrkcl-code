<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsPaymentReconcile.php';

$response = array();
$emp = new clsPaymentReconcile();

if ($_action == "FILL") {
    $response = $emp->GetAllProdInfo();
    echo "<option value=''>Select Product Info</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Pay_Tran_ProdInfo'] . ">" . $_Row['Pay_Tran_ProdInfo'] . "</option>";
    }
}

if ($_action == "SHOWDATA") {
    
		   $response = $emp->ShowDetails($_POST['status']);
	   
            echo "<div class='table-responsive'>";
            echo  "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>S No.</th>";
            echo "<th> ITGK Code </th>";
			echo "<th> RKCL_Transaction Id </th>";
			echo "<th> Admission Code</th>";
            echo "<th> Amount</th>";
            echo "<th> Transaction Status </th>";
            echo "<th> PayUMoney Transaction Id </th>";
			 echo "<th>Action</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
             $_Count = 1;
			 if($response[0]=='Success'){
            while ($row = mysqli_fetch_array($response[2])) {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
									
				echo "<td>" .$row['Pay_Tran_ITGK']. "</td>";			
				echo "<td>" .$row['Pay_Tran_RKCL_Trnid']. "</td>";
				echo "<td>" .$row['Pay_Tran_AdmissionArray']. "</td>";
				echo "<td>" .$row['Pay_Tran_Amount'] . "</td>"; 
				echo "<td>" .$row['Pay_Tran_Status'] . "</td>"; 
				echo "<td>" .$row['Pay_Tran_PG_Trnid'] . "</td>";
				echo "<td><input type='checkbox' id=chk" .  $row['Pay_Tran_PG_Trnid']. " name=chk" . $row['Pay_Tran_PG_Trnid']."></input></td>";
			
                echo "</tr>";
				
                $_Count++;
            }
		}
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
            //echo $html;       
} 

if ($_action == "Update") {
	//print_r($_POST);
        if (isset($_POST["ddlProdInfo"])) {
            $_ProductInfo = $_POST["ddlProdInfo"]; 
				
	$_UserPermissionArray=array();
	$_TxnId = array();
	$_i = 0;
        $_Count=0;
		$l="";
			foreach ($_POST as $key => $value)
			{
               if(substr($key, 0,3)=='chk')
               {
                   $_UserPermissionArray[$_Count]=array(
                       "Function" => substr($key, 3),
                       "Attribute"=>3,
                       "Status"=>1);
                   $_Count++;
				   $l .= "\"" . substr($key, 3) . "\"".",";
				   //$l .= substr($key, 3) . ",";
				}
			   $_TxnId = rtrim($l, ",");
			} 
				//print_r($_TxnId);
				$count = count($_UserPermissionArray);
			$response = $emp->Update($_ProductInfo, $count, $_TxnId);
            echo $response[0];
        }
    }

