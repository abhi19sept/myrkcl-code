<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsReExamDataAdmin.php';

$response = array();
$emp = new clsReExamDataAdmin();


if ($_action == "examdata") {


    $response = $emp->GetDataAll($_POST['examevent']);

    $date = new DateTime();

    $fileName = "ReExamData_" . $_POST['examevent'] . "_" . $date->getTimestamp() . ".csv";

    $fileNamePath = "../upload/reexamdata/" . $fileName;


    $myFile = fopen($fileNamePath, 'w');

    fputs($myFile, '"' . implode('","', array('LCODE', 'LNAME', 'FNAME', 'LDOB', 'LMOBILE', 'COURSE', 'BATCH', 'ITGKCODE', 'ITGKNAME', 'ITGKDISTRICT', 'ITGKTEHSIL')) . '"' . "\n");

        while ($row1 = mysqli_fetch_row($response[2])) { 

            fputcsv($myFile, array("'".$row1['0'], strtoupper($row1['1']),strtoupper($row1['2']), $row1['3'], $row1['4'], $row1['5'], $row1['6'], $row1['7'], $row1['8'], $row1['9'], $row1['10']), ',', '"');
        }
        fclose($myFile);
       // return ob_get_clean();
        echo $fileNamePath;
    
}


if ($_action == "FILL") {
    $response = $emp->GetEvent();
    echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Event_Id'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}
?>