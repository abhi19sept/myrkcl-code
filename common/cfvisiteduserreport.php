<?php

/* 
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsvisiteduserreport.php';

$response = array();
$emp = new clsvisiteduserreport();


if ($_action == "SHOW") {

    //echo "Show";
	//print_r($_POST);
    $response = $emp->GetAll($_POST["txtstart"],$_POST["txtend"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
   
    echo "<th style='10%'>Center Code</th>";
    echo "<th style='20%'>Location</th>";
    echo "<th style='20%'>IP Address</th>";
    echo "<th style='10%'>Date </th>";
	echo "<th style='10%'>No Of Times Login </th>";
    
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
       
        // echo "<td>" . $_Row['Staff_User'] . "</td>";
         echo "<td>" . $_Row['usercode'] . "</td>";
         echo "<td>" . $_Row['usercity'] . "</td>";
         echo "<td>" . $_Row['ip'] . "</td>";
         echo "<td>" . $_Row['date'] . "</td>";
		 echo "<td><a href='frmshowvisiteduserreport.php?code=".$_Row['usercode']. "&Date1=" . $_POST["txtstart"] . "&Date2=" . $_POST["txtend"] . "&mode=Show' target='_blank'>" . $_Row['usercount'] . "</a></td>";
       
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}





if ($_action == "SHOWHISTORY") {

    //echo "Show";
	//print_r($_POST);
    $response = $emp->SHOWHISTORY($_POST["values"],$_POST["Date1"],$_POST["Date2"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
   
    echo "<th style='10%'>Location</th>";
    echo "<th style='20%'>Date</th>";
   
    
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
       
       
         
         echo "<td>" . $_Row['usercity'] . "</td>";
         echo "<td>" . $_Row['date'] . "</td>";
        
       
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}
