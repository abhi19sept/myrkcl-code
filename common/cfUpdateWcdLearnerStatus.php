<?php

/* 
 * author Mayank

 */
include './commonFunction.php';
require 'BAL/clsUpdateWcdLearnerStatus.php';

$response = array();
$emp = new clsUpdateWcdLearnerStatus();

if ($_action == "GETLEARNERDATA") {
	
		if($_POST['itgk'] != "" && $_POST['applicantid'] == "") {
			$response = $emp->GETITGKLEARNERDATA($_POST['itgk']);
			
			if ($response[0]=='Success') {
						$_DataTable = "";

					echo "<div class='table-responsive'>";
					echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
					echo "<thead>";
					echo "<tr>";
					echo "<th>S No.</th>";    
					echo "<th >Learner Name</th>";
					echo "<th >Father Name</th>";	
					echo "<th >Marital Status</th>";	
					echo "<th >Sub Category</th>";	
					echo "<th >Caste Category</th>";
					echo "<th >Qualification</th>";
					echo "<th >Mobile No.</th>";
					echo "<th >Eligibility Status</th>";
					echo "<th >Status</th>";
					echo "<th >Remark</th>";
					
					echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
					$_Count = 1;
						while ($_Row = mysqli_fetch_array($response[2])) {
							echo "<tr>";
							echo "<td>" . $_Count . "</td>";           
							echo "<td>" . $_Row['Oasis_Admission_Name'] . "</td>";
							echo "<td>" . $_Row['Oasis_Admission_Fname'] . "</td>";
							echo "<td>" . $_Row['Oasis_Admission_MaritalStatus'] . "</td>";
							echo "<td>" . $_Row['Oasis_Admission_Subcategory'] . "</td>";			
							echo "<td>" . $_Row['Category_Name'] . "</td>";
							echo "<td>" . $_Row['Qualification_Name'] . "</td>";			
							echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
							 if($_Row['Oasis_Admission_Eligibility'] == 'eligible') {
								 echo "<td><b>" . 'Eligible' .  "</b></td>";
							 }
							 else {
								 echo "<td><b>" . 'Not Eligible' .  "</b></td>";
							 }
							
							 if($_Row['Oasis_Admission_Eligibility'] == 'eligible') {
									 echo "<td>" . $_Row['Oasis_Admission_LearnerStatus'] . "</td>";
								
								   if($_Row['Oasis_Admission_LearnerStatus'] == 'Rejected'){
									   echo "<td>" . $_Row['Oasis_Admission_Reason'] . "</td>";
								   }
								   else {
									   $status = 'NA';
									   echo "<td>" . $status .  "</td>";
								   }
							 }
							else {
								echo "<td>" . 'NA' .  "</td>";
								
								echo "<td>" . 'NA' .  "</td>";
							}
							
							
							echo "</tr>";
							$_Count++;
						}

						echo "</tbody>";
						echo "</table>";
						echo "</div>";
					} 
					else {
							echo "2";
						}
				}
		
		else if($_POST['applicantid'] != "" && $_POST['itgk'] == "") {
			$response = $emp->GETLEARNERDATA($_POST['applicantid']);
					
					if ($response[0]=='Success') {
							$_DataTable = "";

						echo "<div class='table-responsive'>";
						echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
						echo "<thead>";
						echo "<tr>";
						echo "<th>S No.</th>";    
						echo "<th >Learner Name</th>";
						echo "<th >Father Name</th>";	
						echo "<th >Marital Status</th>";	
						echo "<th >Sub Category</th>";	
						echo "<th >Caste Category</th>";
						echo "<th >Qualification</th>";
						echo "<th >Mobile No.</th>";
						echo "<th >Eligibility Status</th>";
						echo "<th >Status</th>";
						echo "<th >Remark</th>";
						
						echo "</tr>";
						echo "</thead>";
						echo "<tbody>";
						$_Count = 1;
							while ($_Row = mysqli_fetch_array($response[2])) {
								echo "<tr>";
								echo "<td>" . $_Count . "</td>";           
								echo "<td>" . $_Row['Oasis_Admission_Name'] . "</td>";
								echo "<td>" . $_Row['Oasis_Admission_Fname'] . "</td>";
								echo "<td>" . $_Row['Oasis_Admission_MaritalStatus'] . "</td>";
								echo "<td>" . $_Row['Oasis_Admission_Subcategory'] . "</td>";			
								echo "<td>" . $_Row['Category_Name'] . "</td>";
								echo "<td>" . $_Row['Qualification_Name'] . "</td>";			
								echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
								 if($_Row['Oasis_Admission_Eligibility'] == 'eligible') {
									 echo "<td><b>" . 'Eligible' .  "</b></td>";
								 }
								 else {
									 echo "<td><b>" . 'Not Eligible' .  "</b></td>";
								 }
								
								 if($_Row['Oasis_Admission_Eligibility'] == 'eligible') {
										 echo "<td>" . $_Row['Oasis_Admission_LearnerStatus'] . "</td>";
									
									   if($_Row['Oasis_Admission_LearnerStatus'] == 'Rejected'){
										   echo "<td>" . $_Row['Oasis_Admission_Reason'] . "</td>";
									   }
									   else {
										   $status = 'NA';
										   echo "<td>" . $status .  "</td>";
									   }
								 }
								else {
									echo "<td>" . 'NA' .  "</td>";
									
									echo "<td>" . 'NA' .  "</td>";
								}
								
								
								echo "</tr>";
								$_Count++;
							}

							echo "</tbody>";
							echo "</table>";
							echo "</div>";
						} 
						else {
							echo "2";
						}
					}
					else if($_POST['applicantid'] != "" && $_POST['itgk'] != "") {
						echo "3";
					}
					else {
						echo "1";
					}
		}
		
if ($_action == "UpdateWcdLearner") {
   if ($_POST["status"] == "")	{
	   echo "1";
   }
   
   else if($_POST["lcode"] != ""){
	    $_LCode = $_POST["lcode"];
		$_ProcessStatus = $_POST["status"];	                       
		
		$response = $emp->UpdateWcdLearnerCode($_LCode, $_ProcessStatus);
		echo $response[0];
   }
   
   else if($_POST["itgk"] != ""){
	    $_ITGK = $_POST["itgk"];
		$_ProcessStatus = $_POST["status"];	                       
		
		$response = $emp->UpdateWcdLearnerITGK($_ITGK, $_ProcessStatus);
		echo $response[0];
   }
}
