<?php

include './commonFunction.php';
require 'BAL/clsDigitalCertificate.php';

$response = array();
$emp = new clsDigitalCertificate();


function changeDateFormatStandered($date) {
    $timestamp = strtotime($date);
    return $new_date_format = date('d-M-Y', $timestamp);
}

if ($_action == "FILLLinkAadharCourse") {
    $response = $emp->GetCourseCertificate();
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLAdmissionBatch") {
    $response = $emp->GetAllBatchCertificate($_POST['values']);
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "GETDATAITGKWISE") {
    $response = $emp->GetLearnerListITGK($_POST['course'], $_POST['batch']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th>D.O.B</th>";
    echo "<th>Mobile</th>";
    //echo "<th>Aadhar No.</th>";
    echo "<th>Action</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
            echo "<td>" . changeDateFormatStandered($_Row['Admission_DOB']) . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            //echo "<td>" . $_Row['Admission_UID'] . "</td>";
//            if($_Row['Admission_Aadhar_Status']=='0'){
//                 echo "<td><input type='button' name='editaadhar' id='" . $_Row['Admission_LearnerCode'] . "'
//						value='Update Aadhar No.' class='btn btn-primary link_aadhar'></td>";              
//            }
            if ($_Row['Admission_Aadhar_Status'] == '1' || $_Row['Admission_Aadhar_Status'] == '0') {
                echo "<td><input type='button' name='generatecertificate' id='" . $_Row['Admission_LearnerCode'] . "'
						value='Generate RS-CIT E-Certificate' class='btn btn-primary generate_certificate'></td>";
            } else if ($_Row['Admission_Aadhar_Status'] == '2') {
                //echo "<td><input type='button' name='downloadcertificate' id='downloadcertificate' value='RS-CIT E-Certificate Generated' class='btn btn-primary'></td>";
                //echo "<td><a href='http://localhost/myrkcl_git/upload/singedDecertificate/".$_Row['Admission_LearnerCode']."_RSCIT.pdf' target='_blank'><button class='btn btn-primary downloadEcertificate' type='button'>View or Download</button></a></td>";
                echo "<td><a href='https://myrkcl.com/upload/singedDecertificate/".$_Row['Admission_LearnerCode']."_RSCIT.pdf' target='_blank'><button class='btn btn-primary downloadEcertificate' type='button'>View or Download</button></a></td>";
            } else if ($_Row['Admission_Aadhar_Status'] == '3') {
                echo "<td><input type='button' name='updatecertificate' id='" . $_Row['Admission_LearnerCode'] . "'
						value='Update RS-CIT E-Certificate' class='btn btn-primary update_certificate'></td>";
            } else if ($_Row['Admission_Aadhar_Status'] == '4') {
                //echo "<td><input type='button' name='downloadcertificate' id='downloadcertificate' value='RS-CIT E-Certificate Generated' class='btn btn-primary'></td>";
                //echo "<td><a href='http://localhost/myrkcl_git/upload/singedDecertificate/".$_Row['Admission_LearnerCode']."_RSCIT.pdf' target='_blank'><button class='btn btn-primary downloadEcertificate' type='button'>View or Download</button></a></td>";
                echo "<td><a href='https://myrkcl.com/upload/singedDecertificate/".$_Row['Admission_LearnerCode']."_RSCIT.pdf' target='_blank'><button class='btn btn-primary downloadEcertificate' type='button'>View or Download</button></a></td>";
            }
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "generateandaddcertificate") {
    $lcode = $_POST['lcode'];
    $path = '../upload/digitalEcertificate';
    makeDir($path);

    $qrpath = $path . '/qrcodes/';
    makeDir($qrpath);

    $cropperpath = $path . '/cropper';
    makeDir($cropperpath);

    $masterPdf = $path . '/certificate_verified.pdf';

    if (!file_exists($masterPdf)) {
        die('Main pdf not found.');
    }
    
    $designation = "Registrar";
    $department = "Examination";
    
    $vmouregistrar = $emp->getSignAuthoriser($designation, $department);
    //print_r($vmouregistrar);
    if($vmouregistrar[0] == 'Success'){
        $regstrar = mysqli_fetch_array($vmouregistrar[2]);
        $signedNameReg = $regstrar['Sign_Name'];
        $signeId = $regstrar['Sign_Sno'];
        $results = $emp->getLearnerResults($lcode, $signedNameReg);
        if (!mysqli_num_rows($results[2])) {
            die('No Record Found!!');
        }

        $pathBkp = '../upload/certificates_' . date("d-m-Y_h_i_s_a") . '_bkp';

        $centerRow = mysqli_fetch_assoc($results[2]);
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $centerRow['result_date'])) {

            $centerRow['result_date'] = date("d-m-Y", strtotime($centerRow['result_date']));
        }
        $filepath = '../upload/digitalEcertificate/' . $centerRow['learnercode'] . '_certificate.pdf';
        if (file_exists($filepath)) {
            unlink($filepath);
        }

        $learnerBatch = $emp->getData($centerRow['learnercode']);
        if ($learnerBatch) {
            $centerRow = array_merge($centerRow, $learnerBatch);
            $exam_date = explode('-', $centerRow['exam_date']);
            $centerRow['exam_year'] = $exam_date[2];
        }


        $photo = getPath('photo', $centerRow);
        $fdfPath = generateFDF($centerRow, $path);
        if ($fdfPath) {
            generateCertificate($centerRow, $path, $fdfPath, $masterPdf, $photo);
            if (file_exists($filepath)) {
                /*   Add Certificate to Rajevault  */
                $response = $emp->ShowLearnerEcertificate($lcode);
                $successCount = "0";
                $FailCount = "0";
                $Count = "0";
                $statusmessage = "no record found in result";
                if ($response) {
                    $_Row = mysqli_fetch_array($response[2]);
                    $Count = $Count + 1;
                    $filepath = "../upload/digitalEcertificate/" . $_Row['scol_no'] . "_certificate.pdf";
                    $date = date_create($_Row['result_date']);
                    $result_date = date_format($date, "d/m/Y");
                    $datedob = date_create($_Row['dob']);
                    $dob = date_format($datedob, "d/m/Y");
                    $documentname = "" . $_Row['scol_no'] . "_RSCIT";
                    $b64Doc = base64_encode(file_get_contents($filepath));
                    $request = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
                        <Body>
                            <signPdf xmlns="http://ds.ws.emas/">
                                <arg0 xmlns="">' . $documentname . '</arg0>
                                <arg1 xmlns="">' . $signeId . '</arg1>
                                <arg2 xmlns="">' . $b64Doc . '</arg2>
                                <arg3 xmlns=""></arg3>
                                <arg4 xmlns="">218,100,355,145</arg4>
                                <arg5 xmlns="">first</arg5>
                                <arg6 xmlns="">Document Approve</arg6>
                                <arg7 xmlns="">Kota (Rajasthan)</arg7>
                            </signPdf>
                        </Body>
                    </Envelope>';

                    $header = array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "Accept: text/xml",
                        "Cache-Control: no-cache",
                        "Pragma: no-cache",
                        "SOAPAction: \"run\"",
                        "Content-length: " . strlen($request),
                    );

                    $soap_do = curl_init();
                    curl_setopt($soap_do, CURLOPT_URL, "http://secmsgdemo.emudhra.com/emsigner/services/dsverifyWS?wsdl");
                    curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
                    curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
                    curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($soap_do, CURLOPT_POST, true);
                    curl_setopt($soap_do, CURLOPT_POSTFIELDS, $request);
                    curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);

                    $result = curl_exec($soap_do);

                    $doc = new DOMDocument('1.0', 'utf-8');
                    $doc->loadXML($result);
                    $XMLresults = $doc->getElementsByTagName("return");
                    $output = $XMLresults->item(0)->nodeValue;
                    $Res = explode("~", $output);
                    if (isset($Res[0])) {
                        $message = $Res[1];
                        if (trim($Res[1]) == 'success') {
                            $filestorepath = "../upload/singedDecertificate/";
                            //$filedownloadlinkpath = "http://localhost/myrkcl_git/upload/singedDecertificate/";
                            $filedownloadlinkpath = "https://myrkcl.com/upload/singedDecertificate/";
                            $DocumentTitle = $documentname;
                            $file = $Res[2];
                            $pdf_decoded = base64_decode($file);
                            $genfile = $filestorepath . $DocumentTitle . '.pdf';
                            $pdf = fopen($genfile, 'w');
                            fwrite($pdf, $pdf_decoded);
                            fclose($pdf);
                            if (file_exists($genfile)) {
                                $response1 = $emp->UpdateEcertificateStatus($_Row['scol_no'], $_Row['exameventnameID'], $_Row['study_cen']);
                                if($response1[0] == "Successfully Updated"){
                                    $link = "<td><a href='$filedownloadlinkpath$DocumentTitle.pdf' target='_blank'><button class='btn btn-primary downloadEcertificate' type='button'>View or Download</button></a></td>";
                                }
                            }
                            $statusmessage = "<img src='images/righttick.gif' style='height: 115px;' /><div style='color: green;margin: 15px 0 15px 0;'>RS-CIT eCertificate has been uploaded to MYRKCL website. RS-CIT e certificate can be downloaded from here " . $link . "</div>";
                        } else {
                            $statusmessage = "<img src='images/wrongtick.gif' style='height: 115px;' /><div style='color: red;margin: 15px 0 15px 0;'>Failed to upload RS-CIT eCertificate to MYRKCL website." . $message . "</div>";
                        }
                        curl_close($soap_do);
                    } else {
                        $statusmessage = "<img src='images/wrongtick.gif' style='height: 115px;' /><div style='color: red;margin: 15px 0 15px 0;'>Failed to upload RS-CIT eCertificate to MYRKCL website.</div>";
                    }
                    echo "<div style='font-size: 20px;margin-top: 45px;'>" . $statusmessage . "</div>";
                } else {
                    echo "No Record Found";
                }
            }
        }
    }
    
    
}

if ($_action == "generateandupdatecertificate") {
    $lcode = $_POST['lcode'];
    $path = '../upload/digitalEcertificate';
    makeDir($path);

    $qrpath = $path . '/qrcodes/';
    makeDir($qrpath);

    $cropperpath = $path . '/cropper';
    makeDir($cropperpath);

    $masterPdf = $path . '/certificate_verified.pdf';

    if (!file_exists($masterPdf)) {
        die('Main pdf not found.');
    }
    
    $designation = "Registrar";
    $department = "Examination";
    
    $vmouregistrar = $emp->getSignAuthoriser($designation, $department);
    if($vmouregistrar[0] == 'Success'){
        $regstrar = mysqli_fetch_array($vmouregistrar[2]);
        $signedNameReg = $regstrar['Sign_Name'];
        $signeId = $regstrar['Sign_Sno'];
        $results = $emp->getLearnerResults($lcode, $signedNameReg);
        if (!mysqli_num_rows($results[2])) {
            die('No Record Found!!');
        }

        $pathBkp = '../upload/certificates_' . date("d-m-Y_h_i_s_a") . '_bkp';

        $centerRow = mysqli_fetch_assoc($results[2]);
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $centerRow['result_date'])) {

            $centerRow['result_date'] = date("d-m-Y", strtotime($centerRow['result_date']));
        }
        $filepath = '../upload/digitalEcertificate/' . $centerRow['learnercode'] . '_certificate.pdf';
        if (file_exists($filepath)) {
            unlink($filepath);
        }

        $learnerBatch = $emp->getData($centerRow['learnercode']);
        if ($learnerBatch) {
            $centerRow = array_merge($centerRow, $learnerBatch);
            $exam_date = explode('-', $centerRow['exam_date']);
            $centerRow['exam_year'] = $exam_date[2];
        }


        $photo = getPath('photo', $centerRow);
        $fdfPath = generateFDF($centerRow, $path);
        if ($fdfPath) {
            generateCertificate($centerRow, $path, $fdfPath, $masterPdf, $photo);
            if (file_exists($filepath)) {
                /*   Add Certificate to Rajevault  */
                $response = $emp->ShowLearnerEcertificateForUpdate($lcode);
                $successCount = "0";
                $FailCount = "0";
                $Count = "0";
                $statusmessage = "no record found in result";
                if ($response) {
                    $_Row = mysqli_fetch_array($response[2]);
                    $Count = $Count + 1;
                    $filepath = "../upload/digitalEcertificate/" . $_Row['scol_no'] . "_certificate.pdf";
                    $date = date_create($_Row['result_date']);
                    $result_date = date_format($date, "d/m/Y");
                    $datedob = date_create($_Row['dob']);
                    $dob = date_format($datedob, "d/m/Y");
                    $documentname = "" . $_Row['scol_no'] . "_RSCIT";
                    $b64Doc = base64_encode(file_get_contents($filepath));
                    $request = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
                        <Body>
                            <signPdf xmlns="http://ds.ws.emas/">
                                <arg0 xmlns="">' . $documentname . '</arg0>
                                <arg1 xmlns="">' . $signeId . '</arg1>
                                <arg2 xmlns="">' . $b64Doc . '</arg2>
                                <arg3 xmlns=""></arg3>
                                <arg4 xmlns="">218,100,355,145</arg4>
                                <arg5 xmlns="">first</arg5>
                                <arg6 xmlns="">Document Approve</arg6>
                                <arg7 xmlns="">Kota (Rajasthan)</arg7>
                            </signPdf>
                        </Body>
                    </Envelope>';

                    $header = array(
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "Accept: text/xml",
                        "Cache-Control: no-cache",
                        "Pragma: no-cache",
                        "SOAPAction: \"run\"",
                        "Content-length: " . strlen($request),
                    );

                    $soap_do = curl_init();
                    curl_setopt($soap_do, CURLOPT_URL, "http://secmsgdemo.emudhra.com/emsigner/services/dsverifyWS?wsdl");
                    curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
                    curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
                    curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($soap_do, CURLOPT_POST, true);
                    curl_setopt($soap_do, CURLOPT_POSTFIELDS, $request);
                    curl_setopt($soap_do, CURLOPT_HTTPHEADER, $header);

                    $result = curl_exec($soap_do);

                    $doc = new DOMDocument('1.0', 'utf-8');
                    $doc->loadXML($result);
                    $XMLresults = $doc->getElementsByTagName("return");
                    $output = $XMLresults->item(0)->nodeValue;
                    $Res = explode("~", $output);
                    if (isset($Res[0])) {
                        $message = $Res[1];
                        if (trim($Res[1]) == 'success') {
                            $filestorepath = "../upload/singedDecertificate/";
                            //$filedownloadlinkpath = "http://localhost/myrkcl_git/upload/singedDecertificate/";
                            $filedownloadlinkpath = "https://myrkcl.com/upload/singedDecertificate/";
                            $DocumentTitle = $documentname;
                            $file = $Res[2];
                            $pdf_decoded = base64_decode($file);
                            $genfile = $filestorepath . $DocumentTitle . '.pdf';
                            $pdf = fopen($genfile, 'w');
                            fwrite($pdf, $pdf_decoded);
                            fclose($pdf);
                            if (file_exists($genfile)) {
                                $response1 = $emp->UpdateEcertificateStatusForUpdateCertificate($_Row['scol_no'], $_Row['exameventnameID'],$_Row['study_cen']);
                                if($response1[0] == "Successfully Updated"){
                                    $link = "<td><a href='$filedownloadlinkpath$DocumentTitle.pdf' target='_blank'><button class='btn btn-primary downloadEcertificate' type='button'>View or Download Updated Certificate</button></a></td>";
                                }
                            }
                            $statusmessage = "<img src='images/righttick.gif' style='height: 115px;' /><div style='color: green;margin: 15px 0 15px 0;'>RS-CIT eCertificate has been uploaded to MYRKCL website. RS-CIT e certificate can be downloaded from here " . $link . "</div>";
                        } else {
                            $statusmessage = "<img src='images/wrongtick.gif' style='height: 115px;' /><div style='color: red;margin: 15px 0 15px 0;'>Failed to upload RS-CIT eCertificate to MYRKCL website." . $message . "</div>";
                        }
                        curl_close($soap_do);
                    } else {
                        $statusmessage = "<img src='images/wrongtick.gif' style='height: 115px;' /><div style='color: red;margin: 15px 0 15px 0;'>Failed to upload RS-CIT eCertificate to MYRKCL website.</div>";
                    }
                    echo "<div style='font-size: 20px;margin-top: 45px;'>" . $statusmessage . "</div>";
                } else {
                    echo "No Record Found";
                }
            }
        }
    }
    
}


/* Generate PDF Functions */

function getPath($type, $row) {
    global $general;
    $batchname = trim(ucwords($row['batchname']));
    $coursename = $row['coursename'];
    $isGovtEmp = stripos($coursename, 'gov');
    if ($isGovtEmp) {
        $batchname = $batchname . '_Gov';
    } else {
        $isWoman = stripos($coursename, 'women');
        if ($isWoman) {
            $batchname = $batchname . '_Women';
        } else {
            $isMadarsa = stripos($coursename, 'madar');
            if ($isMadarsa) {
                $batchname = $batchname . '_Madarsa';
            }
        }
    }

    $batchname = str_replace(' ', '_', $batchname);
    $dirPath = '../upload/admission' . $type . '/' . $row['learnercode'] . '_' . $type . '.png';

    $imgPath = $dirPath;
    $imgPath = str_replace('//', '/', $imgPath);

    $dirPathJpg = str_replace('.png', '.jpg', $dirPath);
    $imgPathJpg = str_replace('.png', '.jpg', $imgPath);

    $imgBatchPath = '../upload/admission' . $type . '/' . $batchname . '/' . $row['learnercode'] . '_' . $type . '.png';
    $imgBatchPath = str_replace('//', '/', $imgBatchPath);
    $imgBatchPathJpg = str_replace('.png', '.jpg', $imgBatchPath);

    $path = '';
    if (file_exists($imgBatchPath)) {
        $path = $imgBatchPath;
    } elseif (file_exists($imgBatchPathJpg)) {
        $path = $imgBatchPathJpg;
    } elseif (file_exists($imgPath)) {
        $path = $imgPath;
    } elseif (file_exists($imgPathJpg)) {
        $path = $imgPathJpg;
    } else {
        
    }

    return $path;
}

function makeDir($path) {
    return is_dir($path) || mkdir($path);
}

function generateFDF($data, $path) {
    $fdfContent = '%FDF-1.2
		%ÃƒÂ¢ÃƒÂ£Ãƒ?Ãƒâ€œ
		1 0 obj 
		<<
		/FDF 
		<<
		/Fields [';
    foreach ($data as $key => $val) {
        $fdfContent .= '
				<<
				/V (' . $val . ')
				/T (' . $key . ')
				>> 
				';
    }
    $fdfContent .= ']
		>>
		>>
		endobj 
		trailer
		<<
		/Root 1 0 R
		>>
		%%EOF';

    $filePath = $path . '/' . $data['learnercode'] . '.fdf';
    file_put_contents($filePath, $fdfContent);
    $return = '';
    if (file_exists($filePath)) {
        $return = $filePath;
    }

    return $return;
}

function generateCertificate($centerRow, $path, $fdfPath, $masterPdf, $photo) {
    $resultPath = $path . '/' . $centerRow['learnercode'] . '_certificate.pdf';

    $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : (($_SERVER['HTTP_HOST'] == "staging.rkcl.co.in") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"');

    $command = $pdftkPath . '  ' . $masterPdf . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
    exec($command);
    unlink($fdfPath);
    addPhotoInPDF($centerRow, $photo, $path, $resultPath);
}

function addPhotoInPDF($learnercode, $Imagepath, $path, $pdfPath) {

    require_once('fpdi/fpdf/fpdf.php');
    require_once('fpdi/fpdi.php');
    require_once('fpdi/fpdf_tpl.php');
    require_once("cropper.php");

    global $general;
    global $cropperpath;
    $qrResource = $picResource = 0;
    $qrpath = '';
    $qrpath = generateQRCode($learnercode, $path);
    if (!empty($qrpath) && file_exists($qrpath)) {
        $qrResource = imagecreatefromstring(file_get_contents($qrpath));
    }
    if (!empty($Imagepath) && file_exists($Imagepath)) {
        $picResource = imagecreatefromstring(file_get_contents($Imagepath));
    }

    $pdf = new FPDI();
    $pdf->AddPage();
    $pdf->setSourceFile($pdfPath);
    $template = $pdf->importPage(1);
    $pdf->useTemplate($template);
    try {
        if (!empty($qrpath) && $qrResource) {
            $pdf->Image($qrpath, 25, 238, 25, 25);
        }
    } catch (Exception $_ex) {
        
    }

    try {
        if (!empty($Imagepath) && $picResource)
            $pdf->Image($Imagepath, 92.8, 79.5, 29, 32.8);
    } catch (Exception $_ex) {
        $pdf->Output($pdfPath, "F");

        if (!empty($Imagepath) && file_exists($Imagepath)) {
            $cropper = new Cropper($cropperpath);
            $args = array("size" => array(30, 30), "image" => $Imagepath, "placeholder" => 2);
            $newImgPath = $cropper->crop($args);

            if (file_exists($newImgPath)) {
                addPhotoInPDF($learnercode['learnercode'], $newImgPath, $path, $pdfPath);
            }
        }
        return;
    }

    $pdf->Output($pdfPath, "F");
}

function generateQRCode($learnercode, $path) {
    if (is_array($learnercode) || is_object($learnercode)) {
        $json_string = encrypt_decrypt('encrypt', json_encode($learnercode));
        echo $secure = base64_encode($json_string);
        //$contents = file_get_contents('http://localhost/myrkcl_git/QRCodeGenerater.php?data=' . $secure . '');
       echo "<br>". $contents = file_get_contents('https://myrkcl.com/QRCodeGenerater.php?data=' . $secure . '');
       echo "<br>". $qrpath = $path.'/qrcodes/' . $learnercode['learnercode'] . '.png';
       echo "<br>" .$file = file_put_contents($qrpath, $contents);
        if (file_exists($qrpath)) {
            echo "<br>". $qrpath;
            return $qrpath;
        }
        else {
            echo 'An error occurred.';
        }
        return $qrpath;
    }
}

if ($_action == "ReadQrCode") {
    echo $data = $_POST['data'] . "<br>";
    echo $str = encrypt_decrypt('decrypt', $data);
}

function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-128-CBC";
    $secret_key = '1212121211121212';
    $secret_iv = 'axwepghrtycgarvt'; 
    $key = substr(hash('sha256', $secret_key), 0, 16);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, OPENSSL_RAW_DATA, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, OPENSSL_RAW_DATA, $iv);
    }
    return $output;
}

?>