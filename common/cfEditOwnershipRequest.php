<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsEditOwnershipRequest.php';
require '../DAL/upload_ftp_doc.php';

$response = array();
$emp = new clsEditOwnershipRequest();



if ($_action == "PROCESS") {
    $response = $emp->GetOrgDatabyCode();
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_role = 'IT-GK';
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "doctype" => $_Row['Organization_DocType'],
            "role" => $_role,
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            "orgdoc" => $_Row['Organization_ScanDoc'],
            "orguid" => $_Row['Organization_UID'],
            "orgaddproof" => $_Row['Organization_AddProof'],
            "orgappform" => $_Row['Organization_AppForm'],
            "orgtypedoc1" => $_Row['Organization_TypeDocId'],);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}


if ($_action == "ADD") {
//    print_r(($_POST));
//    die;
    if (isset($_POST["txtfilename"]) && !empty($_POST["txtfilename"])) {
        $_FilePath = "../" . $_POST["txtfilename"];
        $FileName = explode('/', $_POST["txtfilename"]);
        
        $panimg = $_FILES['uploadImage7']['name'];
        $pantmp = $_FILES['uploadImage7']['tmp_name'];
        //$pantemp = explode(".", $panimg);
        //$PANfilename = round(microtime(true)) . '_' . $i . '.' . end($pantemp);
        //$PANfilename = $_GeneratedId . '_agreement.' . end($pantemp);
        //$panfilestorepath = "../upload/SPCENTERAGREEMENT/" . $PANfilename;
        $panimageFileType = pathinfo($_FilePath, PATHINFO_EXTENSION);

        
        
        if (($_FILES["uploadImage7"]["size"] < 2000000 && $_FILES["uploadImage7"]["size"] > 100000)) {

            if ($panimageFileType == "pdf") {
//                if (file_exists($_FilePath)) {
//                    unlink($_FilePath);
//                } 
                //if (move_uploaded_file($pantmp, $_FilePath)) {
                $appform_response_ftp=ftpUploadFile('/'.$FileName[1],$FileName[2],$pantmp);
                if (trim($appform_response_ftp) == "SuccessfullyUploaded") {

                    echo "Files Updated Successfully.";
                } else {
                    echo "Files Not Updated. Please try again.";
                }
            } else {
                echo "Sorry, File Not Valid";
            }
        } else {
            echo "File Size should be between 100KB to 2MB.";
        }
    } else {
        echo "Inavalid Entry15";
    }
}