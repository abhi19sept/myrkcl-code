<?php
/*
 * Created by Mayank and Anoop

 */
include './commonFunction.php';
require 'BAL/clsEcertificate.php';

$response = array();
$emp = new clsEcertificate();

// Staging Testing URL For Ecertificate

//$addServiceURL = "https://apitest.sewadwaar.rajasthan.gov.in/app/live/RajeVault/UAT/RBSE/addDocument?client_id=9cb343e2-791b-4eb6-8a36-f4a146e28db4";
//$updateServiceURL = "https://apitest.sewadwaar.rajasthan.gov.in/app/live/RajeVault/uat/RBSE/UpdateDocument?client_id=9cb343e2-791b-4eb6-8a36-f4a146e28db4";
//$getServiceURL = "https://apitest.sewadwaar.rajasthan.gov.in/app/live/RajeVault/Uat/RBSE/GetDocument?client_id=9cb343e2-791b-4eb6-8a36-f4a146e28db4";

// Production URL For Ecertificate

$addServiceURL = "https://api.sewadwaar.rajasthan.gov.in/app/live/RajeVault/Prod/RSCIT/Add/document?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69";
$updateServiceURL = "https://api.sewadwaar.rajasthan.gov.in/app/live/RajeVault/Prod/RSCIT/docupdate?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69";
$getServiceURL = "https://api.sewadwaar.rajasthan.gov.in/app/live/RajeVault/Prod/RSCIT/Get/document?client_id=8f5cd943-2b64-4711-8cfd-433757dc9f69";

// Signature Authority Aadhar Number
//$signedAdhaar = "434098406796"; //Test
$signedAdhaar = "290942569229"; //Mr. Murlidhar Pratihar(Live)

// File Store path for get service in MYRKCL

$filestorepath = "../upload/singedEcertificate/";
$filedownloadlinkpath = "http://localhost/myrkcl_git/upload/singedEcertificate/";


if ($_action == "FILLEventName") 
{
    $response = $emp->FillEventName();
    echo "<option value='0' selected='selected'>--Select Event--</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['exameventnameID'] . ">" . $_Row['exam_event_name'] . "</option>";
    }

}

if ($_action == "GETDATAEcertificate") {
    
    $response = $emp->ShowLearnerEcertificate($_POST['event'], $_POST['lot']);
    $successCount = "0";
    $FailCount = "0";
    $Count = "0";
    $statusmessage = "no record found in result";
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $Count = $Count+1;
            $filepath = "../upload/".$_Row['scol_no']."_certificate.pdf";
            $date= date_create($_Row['result_date']);
	    $result_date = date_format($date,"d/m/Y"); 
            $datedob= date_create($_Row['dob']);
	    $dob = date_format($datedob,"d/m/Y"); 
            $documentname = "".$_Row['scol_no']."_RSCIT";
            $b64Doc = base64_encode(file_get_contents($filepath));
            $data = [
                    'metaData' => [
                            'FileExtension' => 'pdf',
                            'Attributes' => [
                                    [
                                            'Value' => $_Row['name'],
                                            'SymbolicName' => 'LearnersName',
                                    ],
                                    [
                                            'Value' => $_Row['fname'],
                                            'SymbolicName' => 'LearnersFathersHusbandsName',
                                    ],
                                    [
                                            'Value' => $_Row['scol_no'],
                                            'SymbolicName' => 'LearnerCode',
                                    ],
                                    [
                                            'Value' => $_Row['study_cen'],
                                            'SymbolicName' => 'CenterCode',
                                    ],
                                    [
                                            'Value' => $_Row['exam_event_name'],
                                            'SymbolicName' => 'AdmissionBatch',
                                    ],
                                    [
                                            'Value' => $_Row['tot_marks'],
                                            'SymbolicName' => 'Marks',
                                    ],
                                    [
                                            'Value' => $result_date,
                                            'SymbolicName' => 'ResultDate',
                                    ],
                                    [
                                            'Value' => $_Row['exam_event_name'],
                                            'SymbolicName' => 'ExamEventName',
                                    ],
                                    [
                                            'Value' => $dob,
                                            'SymbolicName' => 'DOB',
                                    ],
                                    [
                                            'Value' => $documentname,
                                            'SymbolicName' => 'NameOfDocument',
                                    ],
                                    [
                                            'Value' => $documentname,
                                            'SymbolicName' => 'DocumentTitle',
                                    ],
                            ],
                            'ClassName' => 'RSCITeCertificates',
                            'MimeType' => 'application/pdf',
                            'RepoDocID' => $_Row['scol_no'],
                            'File' => $b64Doc,
                    ],
                    'llx' =>  '218',
                    'lly' =>  '85',
                    'positionX' =>  '61.5',
                    'positionY' =>  '80',
                    'aadharId' => $_Row['learner_aadhar'],
                    'fullname' => $_Row['name'],
                    'SigningAuthorityAadhaar' => $signedAdhaar,
                ];

             $json = json_encode($data);
            

            /*** INITIATE CURL REQUEST (DO NOT MODIFY) ***/
            $ch = curl_init($addServiceURL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);		
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                "Accept: application/json",
                "Content-Type: application/json"
            ]);

                    /*** EXECUTE CURL HERE ***/

            $result = curl_exec($ch);
            if($errno = curl_errno($ch)) {
                $error_message = curl_strerror($errno);
                echo "cURL error ({$errno}):\n {$error_message}";
            }
            
            $jsonresultArray = json_decode($result, true);
            $status = $jsonresultArray["status"];
            $message = $jsonresultArray["Message"];
            if($jsonresultArray['status']=='1'){
                $response1 = $emp->UpdateEcertificateStatus($_Row['scol_no'],$_Row['exameventnameID'],$_Row['study_cen']);
                $successCount = $successCount+1; 
                $statusmessage = "document send to DOIT successfully = <span style='font-weight: bold;'>".$successCount."</span> and failed <span style='font-weight: bold;'>".$FailCount."</span> <br>" . $message;
                 
            }else{
                 $FailCount = $FailCount+1;
                 $statusmessage = "error to send the document to DOIT failed = <span style='font-weight: bold;'>".$FailCount."</span> and success = <span style='font-weight: bold;'>".$successCount."</span> <br>" . $message;
                 
            }
             
            curl_close($ch);
            
        }
        echo "<div style='background-color: darkgray;text-align: center;padding: 10px;border-radius: 10px;font-size: 18px;'>Total no. of E-certificate = <span style='font-weight: bold;'>".$Count."</span> and ".$statusmessage ."</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "UpdateEcertificate") {
    
    $response = $emp->ShowLearnerEcertificateUpdate($_POST['event'], $_POST['lot']);
    $successCount = "0";
    $FailCount = "0";
    $Count = "0";
    $statusmessage = "no record found in result";
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            $Count = $Count+1;
            $filepath = "../upload/".$_Row['scol_no']."_certificate.pdf";
            $date= date_create($_Row['result_date']);
	    $result_date = date_format($date,"d/m/Y"); 
            $datedob= date_create($_Row['dob']);
	    $dob = date_format($datedob,"d/m/Y"); 
            $documentname = "".$_Row['scol_no']."_RSCIT";
            $b64Doc = base64_encode(file_get_contents($filepath));
            $data = [
                'metaData' => 
                    [
                           'FileExtension' =>  'pdf',
                           'Attributes' =>  [[
                                            'Value' =>  $_Row['name'],
                                            'SymbolicName' =>  'LearnersName'
                                    ], [
                                            'Value' =>  $_Row['fname'],
                                            'SymbolicName' =>  'LearnersFathersHusbandsName'
                                    ],[
                                            'Value' =>  $_Row['scol_no'],
                                            'SymbolicName' =>  'LearnerCode'
                                    ],[
                                            'Value' =>  $_Row['study_cen'],
                                            'SymbolicName' =>  'CenterCode'
                                    ],[
                                            'Value' =>  $_Row['exam_event_name'],
                                            'SymbolicName' =>  'AdmissionBatch'
                                    ],[
                                            'Value' =>  $_Row['tot_marks'],
                                            'SymbolicName' =>  'Marks'
                                    ],[
                                            'Value' =>  $result_date,
                                            'SymbolicName' =>  'ResultDate'
                                    ],[
                                            'Value' =>  $_Row['exam_event_name'],
                                            'SymbolicName' =>  'ExamEventName'
                                    ],[
                                            'Value' =>  $dob,
                                            'SymbolicName' =>  'DOB '
                                    ],[
                                            'Value' =>  $documentname,
                                            'SymbolicName' =>  'DocumentTitle'
                            ]]
                    ],
                    'aadharId' =>  $_Row['learner_aadhar'],
                    'docId' =>  $_Row['scol_no'],
                    'classname' => 'RSCITeCertificates',
                    'fileContent' =>  $b64Doc,
                    'llx' =>  '218',
                    'lly' =>  '85',
                    'positionX' =>  '61.5',
                    'positionY' =>  '80',
                    'SigningAuthorityAadhaar'  =>  $signedAdhaar
          ];

            $json = json_encode($data);
            //die;

            /*** INITIATE CURL REQUEST (DO NOT MODIFY) ***/
            $ch = curl_init($updateServiceURL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);		
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                "Accept: application/json",
                "Content-Type: application/json"
            ]);

                    /*** EXECUTE CURL HERE ***/

            $result = curl_exec($ch);
            if($errno = curl_errno($ch)) {
                $error_message = curl_strerror($errno);
                echo "cURL error ({$errno}):\n {$error_message}";
            }
            
            $jsonresultArray = json_decode($result, true);
            $status = $jsonresultArray["status"];
            $message = $jsonresultArray["Message"];
            if($jsonresultArray['status']=='1'){
                $response1 = $emp->UpdateEcertificateStatusForUpdateCertificate($_Row['scol_no'],$_Row['exameventnameID'],$_Row['study_cen']);
                 $successCount = $successCount+1; 
                 $statusmessage = "document send to DOIT successfully = <span style='font-weight: bold;'>".$successCount."</span> and failed <span style='font-weight: bold;'>".$FailCount."</span> <br>" . $message;
            }else{
                $FailCount = $FailCount+1;
                $statusmessage = "error to send the document to DOIT failed = <span style='font-weight: bold;'>".$FailCount."</span> and success = <span style='font-weight: bold;'>".$successCount."</span> <br>" . $message;
            }
             
             
            curl_close($ch);
            
        }
        echo "<div  style='background-color: darkgray;text-align: center;padding: 10px;border-radius: 10px;font-size: 18px;'>Total no. of E-certificate = <span style='font-weight: bold;'>".$Count."</span> and ".$statusmessage ."</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "GETDATAEcertificateRajevolt") {
    
    $response = $emp->ShowLearnerEcertificateGet($_POST['event'], $_POST['lot']);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Event Name</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Roll No.</th>";
    echo "<th>Learner Name</th>";
    echo "<th>Father Name</th>";
    echo "<th>Total Marks</th>";
    echo "<th>Percentage</th>";
    echo "<th>Result</th>";
    echo "<th>Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['exam_event_name'] . "</td>";
            echo "<td>" . $_Row['scol_no'] . "</td>";
            echo "<td>" . $_Row['rollno'] . "</td>";
            echo "<td>" . $_Row['name'] . "</td>";
            echo "<td>" . $_Row['fname'] . "</td>";
            echo "<td>" . $_Row['tot_marks'] . "</td>";
            echo "<td>" . $_Row['percentage'] . "</td>";
            echo "<td>" . $_Row['result'] . "</td>";
            $status =' ';
            if($_Row['status']=='1'){
                $status = "Get Certificate";
            } 
            else if($_Row['status']=='2'){
                $status = "Get Updated Certificate";
            }
            else{
                $status = "Pending";
            }
            echo "<td><button class='btn btn-primary downloadEcertificate' type='button' id='".$_Row['scol_no']."' name='".$_Row['learner_aadhar']."'>" . $status . "</button></td>";
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "DownloadEcertificate") {
    //print_r($_POST); die;
    $data = [
            "aadharId" => $_POST['aadhar'],
            "docId" => $_POST['docId'],
            "classname" => "RSCITeCertificates"	
	 	
	];

    $json = json_encode($data);
  
    $ch = curl_init($getServiceURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);		
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Accept: application/json",
            "Content-Type: application/json"
    ]);
    
    $result = curl_exec($ch);
    if($errno = curl_errno($ch)) {
        $error_message = curl_strerror($errno);
        echo "cURL error ({$errno}):\n {$error_message}";
    }
    //print_r($result);
    $jsonresultArray = json_decode($result, true);
    $status = $jsonresultArray["status"];
    if($jsonresultArray['status']=='1'){
        $file = $jsonresultArray["file"];
        $DocumentTitle = $jsonresultArray["DocumentTitle"];
        $pdf_decoded = base64_decode($file);
        $genfile = $filestorepath.$DocumentTitle.'.pdf';
        $pdf = fopen ($genfile,'w');
        fwrite ($pdf,$pdf_decoded);
        fclose ($pdf);
        if(file_exists($genfile)) {
               echo "<td><a href='$filedownloadlinkpath$DocumentTitle.pdf' target='_blank'><button class='btn btn-primary downloadEcertificate' type='button'>View or Download</button></a></td>";
        }
    }else{
        $message = $jsonresultArray["Message"];
        echo "<div  style='background-color: darkgray;text-align: center;padding: 10px;border-radius: 10px;font-size: 18px;'><span style='font-weight: bold;'>".$message."</span></div>";
    }
    curl_close($ch);
}