<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsWcdVisitConfirm.php';

$response = array();
$emp = new clsWcdVisitConfirm();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>Visit Date</th>";
    echo "<th style='20%'>AO Code</th>";
    echo "<th style='10%'>Scheduled By</th>";
        
    echo "<th style='40%'>Email</th>";
    echo "<th style='40%'>Mobile</th>";
    echo "<th style='40%'>Visit Status</th>";
    
    echo "<th style='40%'>Feedback</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['WCDVisit_Selected_Date'] . "</td>";
            echo "<td>" . $_Row['WCDVisit_Center_Code'] . "</td>";
            echo "<td>" . $_Row['WCDVisit_LoginId'] . "</td>";
            echo "<td>" . $_Row['User_EmailId'] . "</td>";
            echo "<td>" . $_Row['User_MobileNo'] . "</td>";
            if($_Row['WCDVisit_Status'] == '0'){
                if($_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '15'){
                    echo "<td> <input type='button' name='Approve' id=" . $_Row['WCDVisit_Code'] . " class='aslink' value='Confirm'/></a>"
            . "</td>";
                } else{
            echo "<td>Pending</td>";
                }
            } elseif($_Row['WCDVisit_Status'] == '1') {
            echo "<td>Confirmed</td>";
            } else {
            echo "<td>NA</td>";    
            }
            
        $response1 = $emp->GetFeedback($_Row['WCDVisit_Center_Code'], $_Row['WCDVisit_LoginId']);
        $co = mysqli_num_rows($response1[2]);
        if($co > 0){
           echo "<td>Feedback Completed</td>"; 
        } else if( $co == '0') {
            if($_Row['WCDVisit_Status'] == '0'){
              echo "<td>Visit not Confirmed</td>";  
            } else if($_Row['WCDVisit_Status'] == '1'){
                if($_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '23'){
                 echo "<td> <a href='frmwcdfeedback.php?code=" . $_Row['WCDVisit_Code'] . "&Mode=Edit&cc=" . $_Row['WCDVisit_Center_Code'] ."'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='Submit Feedback'/></a>"
            . "</td>";   
                } else {
            echo "<td>Feedback Pending</td>";
                }
            }
        }
        
//            if($_Row['NCRVisit_Status'] == '0' && ($_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '15')){
//            echo "<td> <input type='button' name='Approve' id=" . $_Row['NCRVisit_Code'] . " class='aslink' value='Confirm'/></a>"
//            . "</td>";
//            } elseif($_Row['NCRVisit_Status'] == '1') {
//                if($_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '23'){
//                 echo "<td> <a href='frmncrfeedback.php?code=" . $_Row['NCRVisit_Code'] . "&Mode=Edit&cc=" . $_Row['NCRVisit_Center_Code'] ."'>"
//            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='Submit Feedback'/></a>"
//            . "</td>";   
//                } else {
//            echo "<td>Confirmed</td>";
//                }
//            } else {
//            echo "<td>NA</td>";    
//            }
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "GetLearnerByPIN") {

    //print_r($_POST);
    if($_POST['visitorname'] =='0'){
        echo "0";
        return;
    }
    else{
    $response = $emp->GetLearnerByPIN($_POST['biopin'], $_POST['params'], $_POST['visitorname']);

    //print_r($response);

    if ($response[0] == "Success") {
        $_Row = mysqli_fetch_array($response[2]);
        $_DataTable = array();
        $_i = 0;

        $attendanceFlag = 0;

        $_DataTable[$_i] = array("AdmissionCode" => $_Row['userId'],
            "quality" => $_Row['BioMatric_Quality'],
            "nfiq" => $_Row['BioMatric_NFIQ'],
            "template" => $_Row['BioMatric_ISO_Template'],
            "image" => $_Row['BioMatric_ISO_Image'],
            "raw" => $_Row['BioMatric_Raw_Data'],
            "attendance_flag" => $attendanceFlag);

        echo json_encode($_DataTable);
    } else {
        echo "invalidpin";
    }
    }
}

if ($_action == "ConfirmVisit") {    
        //print_r($_POST);
         if (isset($_POST["visitid"]))
		{
			$_VisitId = $_POST["visitid"];
                        $_VisitorId = $_POST["visitorid"];
			$response = $emp->ConfirmVisit($_VisitId, $_VisitorId);
            echo $response[0];
    }
}

	 
if ($_action == "LoadVisitor") {
    if(isset($_POST['visitid'])){
    $response = $emp->GetVisitor($_POST['visitid']);	
    echo "<option value='0'>Select Visitor</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['id'] . "'>" . $_Row['fullname'] . "</option>";
    }
    }else {
        echo "0";
    }
}


if ($_action == "LoadITGK") {
    if(isset($_POST['visitid'])){
    $response = $emp->GetITGK($_POST['visitid']);	
    echo "<option value='0'>Select ITGK</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['id'] . "'>" . $_Row['fullname'] . "</option>";
    }
    }else {
        echo "0";
    }
}

