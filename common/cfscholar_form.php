<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsscholarform.php';

    $response = array();
    $emp = new clsscholarform();
    if ($_action == "ADD") {
        if (isset($_POST["lcode"]) && !empty($_POST["lcode"]) && !empty($_POST["lname"]) && !empty($_POST["faname"]) && !empty($_POST["mothername"])
			 && !empty($_POST["address"]) && !empty($_POST["mobileno"]) && !empty($_POST["email"]) && !empty($_POST["batch"])
			 && !empty($_POST["district"]) && !empty($_POST["tehsil"]) && !empty($_POST["pinno"]) && !empty($_POST["qualification"])
			 && !empty($_POST["rollno"]))
			 {
				$_LearnerCode = $_POST["lcode"];
				$_LearnerName = $_POST["lname"];
				$_FatherName = $_POST["faname"];
				$_MotherName = $_POST["mothername"];
				$_Address = $_POST["address"];
				$_Mobile = $_POST["mobileno"];
				$_Email = $_POST["email"];
				$_Batch = $_POST["batch"];
				$_District = $_POST["district"];
				$_Tehsil = $_POST["tehsil"];
				$_PinNo = $_POST["pinno"];
				$_Qualification = $_POST["qualification"];
				$_RollNo = $_POST["rollno"];
				$_PermissionLetter = $_POST["permission_letter"];
				$_ScholarForm = $_POST["scholar_form"];
				
			
            $response = $emp->Add($_LearnerCode, $_LearnerName, $_FatherName, $_MotherName, $_Address, $_Mobile, $_Email, $_Batch, 
								  $_District , $_Tehsil, $_PinNo, $_Qualification, $_RollNo, $_PermissionLetter, $_ScholarForm );
            echo $response[0];
        }
    }
    
    if ($_action == "UPDATE") {
        if (isset($_POST["name"])) {
            $_StatusName = $_POST["name"];
            $_StatusDescription = $_POST["description"];
            $_Status_Code=$_POST['code'];
            $response = $emp->Update($_Status_Code,$_StatusName, $_StatusDescription);
            echo $response[0];
        }
    }
    
    
    if ($_action == "EDIT") {


        $response = $emp->GetDatabyCode($_actionvalue);

        $_DataTable = array();
        $_i=0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("StatusCode" => $_Row['Status_Code'],
                "StatusName"=>$_Row['Status_Name'],
                "StatusDescription"=>$_Row['Status_Description']);
            $_i=$_i+1;
        }
        echo json_encode($_Datatable);
    }
    
    
    if ($_action == "DELETE") {


        $response = $emp->DeleteRecord($_actionvalue);

        echo $response[0];
    }


    if ($_action == "SHOW") {


        $response = $emp->GetAll();

        $_DataTable = "";

        echo "<table border='0' cellpedding='0' cellspacing='0' width='100%' class='gridview'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='5%'>S No.</th>";
        echo "<th style='30%'>Name</th>";
        echo "<th style='55%'>Description</th>";
        echo "<th style='10%'>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Status_Name'] . "</td>";
            echo "<td>" . $_Row['Status_Description'] . "</td>";
            echo "<td><a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a>  <a href='frmstatusmaster.php?code=" . $_Row['Status_Code'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
//echo $_DataTable;
    }
    if ($_action == "FILL") {
        $response = $emp->GetAll();
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['District_Name'] . ">" . $_Row['District_Name'] . "</option>";
             
     
        }
    }
    
     if ($_action == "FILLEMPDEPARTMENT") {
        $response = $emp->GetEmpdepartment();
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['gdname'] . ">" . $_Row['gdname'] . "</option>";
            
        }
    }
    
    
     if ($_action == "FILLEMPDESIGNATION") {
        $response = $emp->GetEmpdesignation();
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['designationname'] . ">" . $_Row['designationname'] . "</option>";
            
        }
    }
    
    
     if ($_action == "FILLBANKDISTRICT") {
        $response = $emp->GetBankdistrict();
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['District_Name'] . ">" . $_Row['District_Name'] . "</option>";
            
        }
    }
    
    
     if ($_action == "FILLBANKNAME") {
        $response = $emp->GetBankname($_POST['districtid']);
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['bankname'] . ">" . $_Row['bankname'] . "</option>";
            
        }
    }
    
    
     if ($_action == "FillDobProof") {
        $response = $emp->GetDobProof();
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['cdocname'] . ">" . $_Row['cdocname'] . "</option>";
            
        }
    }
?>
