<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsPaymentRefund.php';

$response = array();
$emp = new clsPaymentRefund();

if ($_action == "SHOW") {
    if (isset($_POST["startdate"])) {
        if (isset($_POST["enddate"])) {
            $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
            $response = $emp->Show($sdate, $edate);
            // print_r($response);
            //$html = "";
            //$_centerdetail=  mysqli_fetch_array($response[2]);
            //$response = $emp->GetCenterWiseReport($_POST['CenterCode']);
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th> S No.</th>";
            echo "<th> ITGK Code </th>";
            echo "<th> Transaction Number </th>";
            echo "<th> Payment Amount</th>";
            //echo "<th> Amount </th>";
            echo "<th> Payment Status </th>";
            echo "<th> Payment Type </th>";
            //echo "<th> Payment Account Email </th>";
            echo "<th> Payment Date</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;
            $_Total = 0;
            $co = mysqli_num_rows($response[2]);
            if ($co) {
            while ($row = mysqli_fetch_array($response[2])) {

                $Tdate = date("d-m-Y", strtotime($row['Payment_Refund_Timestamp']));
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";

                echo "<td>" . $row['Payment_Refund_ITGK'] . "</td>";

                echo "<td>" . $row['Payment_Refund_Txnid'] . "</td>";

                echo "<td>" . $row['Payment_Refund_Amount'] . "</td>";
               // echo "<td>" . $row['Total_Refund_Amount'] . "</td>";

                //echo "<td>" . $row['Admission_Transaction_Amount'] . "</td>";

                echo "<td>" . $row['Payment_Refund_Status'] . "</td>";

                echo "<td>" . $row['Payment_Refund_ProdInfo'] . "</td>";


                echo "<td>" . $Tdate . "</td>";

                echo "</tr>";
                $_Total = $_Total + $row['Payment_Refund_Amount'];
                $_Count++;
            }
            echo "</tbody>";
            echo "<tfoot>";
            echo "<tr>";
            echo "<th >  </th>";
            echo "<th >  </th>";
            echo "<th >Total Count </th>";
            echo "<th>";
            echo "$_Total";
            echo "</th>";
            echo "<th >  </th>";
            echo "<th >  </th>";
            echo "<th >  </th>";            
            echo "</tr>";
            echo "</tfoot>";
            echo "</table>";
            echo "</div>";
            } else {
        echo "No Record Found";
    }
            //echo $html;
        }
    }
}

