<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCorrectionTransRpt.php';

$response = array();
$emp = new clsCorrectionTransRpt();

if ($_action == "SHOW") {
    if (isset($_POST["startdate"])) {
        if (isset($_POST["enddate"])) {
            $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
            $response = $emp->Show($sdate, $edate);
            // print_r($response);
            //$html = "";
            //$_centerdetail=  mysqli_fetch_array($response[2]);
            //$response = $emp->GetCenterWiseReport($_POST['CenterCode']);
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>S No.</th>";
            echo "<th> Admission Code </th>";
            echo "<th> Admission Name </th>";
            echo "<th> Payment Account Name</th>";
            //echo "<th> Amount </th>";
            echo "<th> TransactionID </th>";
            echo "<th> Payment Type </th>";
            //  echo "<th> Payment Account Email </th>";
            echo "<th> Center Code</th>";
            echo "<th> Payment Status</th>";
            echo "<th> Payment Date</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;
            $_TotalAmount = 0;

            while ($row = mysqli_fetch_array($response[2])) {
                $Tdate = date("d-m-Y", strtotime($row['Correction_Transaction_timestamp']));

                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $row['lcode'] . "</td>";
                echo "<td>" . strtoupper($row['cfname']) . "</td>";

                echo "<td>" . strtoupper($row['Correction_Transaction_Fname']) . "</td>";

                //echo "<td>" . $row['Correction_Transaction_Amount'] . "</td>";

                echo "<td>" . $row['Correction_Transaction_Txtid'] . "</td>";

                echo "<td>" . $row['Correction_Transaction_ProdInfo'] . "</td>";

                //  echo "<td>" . $row['Correction_Transaction_Email'] . "</td>";

                echo "<td>" . $row['Correction_Transaction_CenterCode'] . "</td>";
				
				if($row['Correction_Payment_Status']=='1'){
					echo "<td>Payment Confirmed</td>";
				}
				else {
					echo "<td>Payment Not Confirmed</td>";
				}
                

                echo "<td>" . $Tdate . "</td>";

                echo "</tr>";
                $_TotalAmount = $_TotalAmount + $row['Correction_Transaction_Amount'];
                $_Count++;
            }

            echo "</tbody>";
            echo "<tfoot>";

            echo "</tfoot>";
            echo "</table>";
            echo "</div>";
            //echo $html;
        }
    }
}

if ($_action == "SHOWsummary") {
    if (isset($_POST["startdate"])) {
        if (isset($_POST["enddate"])) {
            $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
            $response = $emp->ShowSummary($sdate, $edate);
            // print_r($response);
            //$html = "";
            //$_centerdetail=  mysqli_fetch_array($response[2]);
            //$response = $emp->GetCenterWiseReport($_POST['CenterCode']);
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>S No.</th>";
            echo "<th> Center Code</th>";
            echo "<th> Learner Count</th>";
            echo "<th> Amount </th>";
            echo "<th> TrnasactionID </th>";
            echo "<th> Payment Account Name</th>";
            echo "<th> Payment Type </th>";
            echo "<th> Payment Status</th>";
            echo "<th> Payment Date</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;
            $_TotalAmount = 0;
            $_TotalLearner = 0;
            while ($row = mysqli_fetch_array($response[2])) {
                $Tdate = date("d-m-Y", strtotime($row['Correction_Transaction_timestamp']));
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $row['Correction_Transaction_CenterCode'] . "</td>";
                echo "<td>" . $row['lcount'] . "</td>";
                echo "<td>" . $row['totalamount'] . "</td>";
                echo "<td>" . $row['Correction_Transaction_Txtid'] . "</td>";
                echo "<td>" . strtoupper($row['Correction_Transaction_Fname']) . "</td>";
                echo "<td>" . $row['Correction_Transaction_ProdInfo'] . "</td>";
                echo "<td>" . $row['Correction_Transaction_Status'] . "</td>";
                echo "<td>" . $Tdate . "</td>";
                echo "</tr>";
                $_Count++;
                $_TotalAmount = $_TotalAmount + $row['totalamount'];
                $_TotalLearner = $_TotalLearner + $row['lcount'];
            }
            echo "<tr style='tr:last-child;'>";

            echo "<th >Total </th>";
            echo "<th >  </th>";
            echo "<th >";
            echo "$_TotalLearner";
            echo "</th>";
            echo "<th>";
            echo "$_TotalAmount";
            echo "</th>";
            echo "<th >  </th>";
            echo "<th >  </th>";
            echo "<th >  </th>";
            echo "<th >  </th>";
            echo "<th >  </th>";
            echo "</tr>";
            echo "</tbody>";
            echo "<tfoot>";

            echo "</tfoot>";
            echo "</table>";
            echo "</div>";
            //echo $html;
        }
    }
}
?>