<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsBioMetricEnrollmentRpt.php';

$response = array();
$emp = new clsBioMetricEnrollmentRpt();

if ($_action == "SHOW") {

    if ($_POST['status'] == '1') {
        $response = $emp->ShowEnrolled($_POST['status'], $_POST['course'], $_POST['batch']);
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th> BioMetric PIN </th>";
        echo "<th> Learner Code </th>";
        echo "<th> ITGK Code </th>";
        echo "<th> Learner Name</th>";
        echo "<th> Father Name</th>";
        //echo "<th>Learner Mobile</th>";
        echo "<th> BioMetric Status </th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;

        if ($response[0] == 'Success') {
            while ($row = mysqli_fetch_array($response[2])) {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";

                echo "<td>" . $row['BioMatric_Admission_BioPIN'] . "</td>";
                echo "<td>'" . $row['BioMatric_Admission_LCode'] . "</td>";
                echo "<td>" . $row['BioMatric_ITGK_Code'] . "</td>";
                echo "<td>" . strtoupper($row['BioMatric_Admission_Name']) . "</td>";
                echo "<td>" . strtoupper($row['BioMatric_Admission_Fname']) . "</td>";
                //   echo "<td>" . strtoupper($row['Admission_Mobile']) . "</td>";
//                if ($row['BioMatric_Status'] == "1") {
                $_status = "Learner Enrolled";
                echo "<td>" . $_status . "</td>";
//                } else {
//                    $_status = "Learner Not Enrolled";
//                    echo "<td>" . $_status . "</td>";
//                }
                echo "</tr>";

                $_Count++;
            }
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        $response = $emp->ShowNotEnrolled($_POST['status'], $_POST['course'], $_POST['batch']);
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th> Learner Code </th>";
        echo "<th> ITGK Code </th>";
        echo "<th> Learner Name</th>";
        echo "<th> Father Name</th>";
        echo "<th>Learner Mobile</th>";
        echo "<th> BioMatric Status </th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;

        if ($response[0] == 'Success') {
            while ($row = mysqli_fetch_array($response[2])) {
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";

                echo "<td>'" . $row['Admission_LearnerCode'] . "</td>";
                echo "<td>" . $row['Admission_ITGK_Code'] . "</td>";
                echo "<td>" . strtoupper($row['Admission_Name']) . "</td>";
                echo "<td>" . strtoupper($row['Admission_Fname']) . "</td>";
                echo "<td>" . $row['Admission_Mobile'] . "</td>";

//                if ($row['BioMatric_Status'] == "1") {
//                    $_status = "Learner Enrolled";
//                    echo "<td>" . $_status . "</td>";
//                } else {
                $_status = "Learner Not Enrolled";
                echo "<td>" . $_status . "</td>";
//                }
                echo "</tr>";

                $_Count++;
            }
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }



    //echo $html;
} 

