<?php

/* 
 * Created by yogi

 */

include './commonFunction.php';
require 'BAL/clsexamcenterdetails.php';

$response = array();
$emp = new clsexamcenterdetails();

//print_r($_POST);
	
if ($_action == "ADD") {
	//print_r($_POST);
    if (isset($_POST["txtName"]) && !empty($_POST["txtName"]) && !empty($_POST["txtexamid"])) {
		$_District = $_POST["ddlDistrict"];
        $_Center = $_POST["ddlCenter"];
		$_Tehsil = $_POST["ddlTehsil"];
		$_Name = $_POST["txtName"];
        $_Address = $_POST["txtAddress"];
        $_Mobile = $_POST["txtMobile"];
        $_Email = $_POST["txtEmail"];
        $_Capacity = $_POST["txtCapacity"];
        $_Rooms = $_POST["txtRooms"];
        $_Coordinator = $_POST["txtCoordinator"];
        $_Event= $_POST["ddlEvent"];
		$_txtdate= $_POST["txtdate"];
        $_examid = $_POST["txtexamid"];
        $response = $emp->Add($_District,$_Center,$_Tehsil,$_Name,$_Address,$_Mobile,$_Email,$_Capacity,$_Rooms,$_Coordinator,$_Event,$_txtdate,$_examid);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
//echo  $_ddlCenter = $_POST["ddlCenter"];
    if (isset($_POST["txtName"]) && !empty($_POST["txtName"])) {
		$_Code = $_POST["code"];
       $_District = $_POST["ddlDistrict"];
        $_Center = $_POST["ddlCenter"];
		$_Tehsil = $_POST["ddlTehsil"];
		$_Name = $_POST["txtName"];
        $_Address = $_POST["txtAddress"];
        $_Mobile = $_POST["txtMobile"];
        $_Email = $_POST["txtEmail"];
        $_Capacity = $_POST["txtCapacity"];
        $_Rooms = $_POST["txtRooms"];
        $_Coordinator = $_POST["txtCoordinator"];
        $_Event= $_POST["ddlEvent"];
		$_txtdate= $_POST["txtdate"];
        $_examid = $_POST["txtexamid"];
        $response = $emp->Update($_Code,$_District,$_Center,$_Tehsil,$_Name,$_Address,$_Mobile,$_Email,$_Capacity,$_Rooms,$_Coordinator,$_Event,$_txtdate,$_examid);
        echo $response[0];
    }
}


if ($_action == "EDIT") {
$response = $emp->GetDatabyCode($_actionvalue,$_POST["examid"]);
    //echo $_actionvalue;
   //print_r($response);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("district" => $_Row['district'],
            "examcentercode" => $_Row['examcentercode'],
			"tehsil" => $_Row['examtehsil'],
			"examcentername" => $_Row['examcentername'],
			"examcenteradd" => $_Row['examcenteradd'],
			"examcentermobile" => $_Row['examcentermobile'],
			"examcenteremail" => $_Row['examcenteremail'],
			"examcentercapacity" => $_Row['examcentercapacity'],
			"noofrooms" => $_Row['noofrooms'],
			"examcentercoordinator" => $_Row['examcentercoordinator'],
			"examtehsil" => $_Row['examtehsil'],
			"date" => $_Row['date'],
			"examid" => $_Row['examid']);
		 $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue,$_POST["examid"]);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:20px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th >S No.</th>";
    echo "<th >Exam Center Name</th>";
	echo "<th >Exam Center Code</th>";
	echo "<th >Exam District </th>";
	echo "<th >Exam Tehsil </th>";
	echo "<th >Exam Center Address </th>";
	echo "<th >Event Name </th>";
	//echo "<th style='35%'>Exam Id </th>";
	 echo "<th >Mobile </th>";
	// echo "<th >Email </th>"; 
    echo "<th >Capacity </th>"; 
	 echo "<th >No of Rooms </th>"; 
	
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['examcentername'] . "</td>";
		 echo "<td>" . $_Row['examcentercode'] . "</td>";
		  echo "<td>" . $_Row['District_Name'] . "</td>";
		   echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
		   echo "<td>" . $_Row['examcenteradd'] . "</td>";
		    echo "<td>" . $_Row['Event_Name'] . "</td>";
			// echo "<td>" . $_Row['examid'] . "</td>";
			 
		echo "<td>" . $_Row['examcentermobile'] . "</td>";
	//	echo "<td>" . $_Row['examcenteremail'] . "</td>";
		echo "<td>" . $_Row['examcentercapacity'] . "</td>";
		echo "<td>" . $_Row['noofrooms'] . "</td>";
	
        echo "<td><a href='frmexamcenterdetails.php?code=" . $_Row['examcentercode'] . "&examid=" . $_Row["examid"] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmexamcenterdetails.php?code=" . $_Row['examcentercode'] . "&examid=" . $_Row["examid"] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}



if ($_action == "FILLCENTER") {   // FIll Center
    $eventId = (isset($_POST['eventId']) && !empty($_POST['eventId']) && is_numeric($_POST['eventId'])) ? $_POST['eventId'] : 0;
    $response = $emp->Getcenter($_actionvalue, $eventId);
    echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['examcentercode'] . ">" . $_Row['examcentercode'] . "</option>";
    }
}

