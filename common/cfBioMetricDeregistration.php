<?php
/*
 * Created by : SUNIL KUMAR BAINDARA
 * DATED: 26-09-2018
  * Updated by : Abhishek
 * DATED: 15-05-2019

 */
include './commonFunction.php';
require 'BAL/clsBioMetricDeregistration.php';
$response = array();
$emp = new clsBioMetricDeregistration();
if ($_action == "FILLCurrentOpenBatch") {
    $response = $emp->FILLCurrentOpenBatch($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}
if ($_action == "GETCURRENTBATCHLEARNER") {
    $response = $emp->GetCurrentBatchLearnerList($_POST['course'], $_POST['batch']);
    //echo "<pre>"; print_r($response);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Center Code</th>";
    echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th>Photo</th>";
    echo "<th>Payment Status</th>";
    echo "<th><input type='checkbox' name='select_all' value='1' id='example-select-all'></th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
            echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
            }
            //            if ($_Row['Admission_Sign'] != "") {
            //                $sign = $_Row['Admission_Sign'];
            //                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
            //            } else {
            //                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
            //            }
            if ($_Row['Admission_Payment_Status'] == "1") {
                echo "<td> Learner Confirmed </td>";
            } elseif ($_Row['Admission_Payment_Status'] == "0") {
                echo "<td> Learner Not Confirmed </td>";
            } elseif ($_Row['Admission_Payment_Status'] == "8") {
                echo "<td> Confirmation Pending at RKCL </td>";
            }
            $response_t = $emp->checkLearnerCode($_Row['Admission_LearnerCode']);
            //echo "<pre>"; print_r($response_t);die;
            if ($response_t[0] = 'Success') {
                echo "<td><b>Pending with RKCL</b></td>";
            } else {
                echo "<td><input type='checkbox' name='id[]' value='" . $_Row['Admission_LearnerCode'] . "'/></td>";
            }
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}
// if ($_action == "AddDeregisterLearnerPreview") {
//    //echo "<pre>"; print_r($_POST);//die;
//     if (isset($_POST["id"])) {
//         $LD_Course = $_POST["ddlCourse"];
//         $LD_Batch = $_POST["ddlBatch"];
//         //$LD_Batch = '123';
//         // this condition is checking that the user is not modifing the data by inspect...
//         $response = $emp->FILLCurrentOpenBatch($LD_Course);
//         $yes=0;
//          while ($_Row = mysqli_fetch_array($response[2],true)) {
//                 if($_Row['Batch_Code']==$LD_Batch){
//                     $yes=1;
//                 }
//         }
//         // this condition is checking that the user is not modifing the data by inspect...
//         if($yes==1){
//                 $LD_LearnerCode=implode(", ",$_POST["id"]);
//                 $response = $emp->GetCurrentBatchLearnerListPreview($LD_Course, $LD_Batch, $LD_LearnerCode);
//                 //echo "<pre>"; print_r($response);
//                 echo "<div class='table-responsive'>";
//                 echo "<table class='table table-striped table-bordered'>";
//                 echo "<thead>";
//                 echo "<tr>";
//                 echo "<th>S No.</th>";
//                 echo "<th>Center Code</th>";
//                 echo "<th>Learner Code</th>";
//                 echo "<th>Learner Name</th>";
//                 echo "<th >Father Name</th>";
//                 echo "<th>Photo</th>";
//                 //echo "<th>Payment Status</th>";
//                 //echo "<th><input type='checkbox' name='select_all' value='1' id='example-select-all'></th>";
//                 echo "</tr>";
//                 echo "</thead>";
//                 echo "<tbody>";
//                 $_Count = 1;
//                 if ($response) {
//                     while ($_Row = mysqli_fetch_array($response[2])) {
//                         echo "<tr>";
//                         echo "<td>" . $_Count . "</td>";
//                         echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
//                         echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
//                         echo "<td>" . $_Row['Admission_Name'] . "</td>";
//                         echo "<td>" . $_Row['Admission_Fname'] . "</td>";
//                         if ($_Row['Admission_Photo'] != "") {
//                             $image = $_Row['Admission_Photo'];
//                             echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
//                         } else {
//                             echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
//                         }
//                         echo "</tr>";
//                         $_Count++;
//                     }
//                     echo "</tbody>";
//                     echo "</table>";
//                     echo "</div>";
//                 }
//                 else {
//                     echo "No Record Found";
//                 }
//                 die;
//         }else{
//            echo 'DONOTDOTHIS';
//         }
//     }
//     else{
//         echo 'IDEMPTY';
//     }
// }
// if ($_action == "AddDeregisterLearner") {
//    //echo "<pre>"; print_r($_POST);die;
//     if (isset($_POST["id"])) {
//         $LD_Course = $_POST["ddlCourse"];
//         $LD_Batch = $_POST["ddlBatch"];
//         //$LD_Batch = '123';
//         // this condition is checking that the user is not modifing the data by inspect...
//         $response = $emp->FILLCurrentOpenBatch($LD_Course);
//         $yes=0;
//          while ($_Row = mysqli_fetch_array($response[2],true)) {
//                 if($_Row['Batch_Code']==$LD_Batch){
//                     $yes=1;
//                 }
//         }
//         // this condition is checking that the user is not modifing the data by inspect...
//         if($yes==1){
//             for($i=0; $i<count($_POST["id"]); $i++){
//                 $LD_LearnerCode=$_POST["id"][$i];
//                 $LD_ITGK_Code=$_SESSION['User_LoginId'];
//                 $responseName = $emp->getLearnerNameFname($LD_LearnerCode);
//                 //echo "<pre>"; print_r($responseName);die;
//                 $_RowName = mysqli_fetch_array($responseName[2]);
//                 $response = $emp->AddDeregisterLearner($LD_LearnerCode, $LD_ITGK_Code, $LD_Batch, $LD_Course, $_RowName);
//             }
//             if($response[0]=='Successfully Inserted'){
//                     $response_SMS = $emp->SendSMSTOITGK($_SESSION['User_LoginId'],count($_POST["id"]));
//                }
//             echo $response[0];
//         }else{
//            echo 'DONOTDOTHIS';
//         }
//     }
//     else{
//         echo 'IDEMPTY';
//     }
// }
if ($_action == "APPLYDREG") {
    $chkbatch = $emp->FILLCurrentOpenBatch($_POST['course']);
    while ($_Row1 = mysqli_fetch_array($chkbatch[2])) {
        $_Row_chkbatch[] = $_Row1['Batch_Code'];
    }
    // $_Row_chkbatch = mysqli_fetch_array($chkbatch[2]);
    if (in_array($_POST['batch'], $_Row_chkbatch)) {
        $response = $emp->GetCurrentBatchLearnerList($_POST['course'], $_POST['batch']);
        //echo "<pre>"; print_r($response);
        $_DataTable = "";
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th>Center Code</th>";
        echo "<th>Learner Code</th>";
        echo "<th>Learner Name</th>";
        echo "<th >Father Name</th>";
        echo "<th>Photo</th>";
        echo "<th>Payment Status</th>";
        echo "<th>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        if ($response) {
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
                echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
                echo "<td>" . $_Row['Admission_Name'] . "</td>";
                echo "<td>" . $_Row['Admission_Fname'] . "</td>";
                if ($_Row['Admission_Photo'] != "") {
                    $image = $_Row['Admission_Photo'];
                    echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
                } else {
                    echo "<td>" . '<img alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' . "</td>";
                }
                //            if ($_Row['Admission_Sign'] != "") {
                //                $sign = $_Row['Admission_Sign'];
                //                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
                //            } else {
                //                echo "<td>" . '<img alt="No Image Found" width="40" height="15" src="images/no_image.png"/>' . "</td>";
                //            }
                if ($_Row['Admission_Payment_Status'] == "1") {
                    echo "<td> Learner Confirmed </td>";
                } elseif ($_Row['Admission_Payment_Status'] == "0") {
                    echo "<td> Learner Not Confirmed </td>";
                } elseif ($_Row['Admission_Payment_Status'] == "8") {
                    echo "<td> Confirmation Pending at RKCL </td>";
                }
                ?>
                <td> <a href="frmderegapplication.php?code=<?php echo $_Row['Admission_Code'] ?>" target="_blank">
                        <input type="button" name="dereg" id="dereg" class="btn btn-danger" value="Apply">
                    </a>
                </td>
                <?php
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }
    } else {
        session_destroy();
        ?>
        <script> window.location.href = "index.php";</script>
        <?php
    }
}
if ($_action == "LearnerDeregister") {
    if ($_POST['deregreason'] == '') {
        echo "0";
    } else {
        //print_r($_POST);
        //for log creation
        $responseGetLearner = $emp->GetLearnerByLID($_POST['lcode']);
        //print_r($responseGetLearner);
        if ($responseGetLearner[0] == "Success") {
            $_Row = mysqli_fetch_array($responseGetLearner[2]);
            $OldIso_template = $_Row['BioMatric_ISO_Template'];
            $itgkcode = $_Row['BioMatric_ITGK_Code'];
            $Course = $_Row['Course_Code'];
            $Batch = $_Row['Batch_Code'];
            $AdmLCode = $_Row['BioMatric_Admission_LCode'];
            //Code to check attendance event is open for this learner batch
            $chkbatch = $emp->FILLCurrentOpenBatch($Course);
            while ($_Row1 = mysqli_fetch_array($chkbatch[2])) {
                $_Row_chkbatch[] = $_Row1['Batch_Code'];
            }
            // $_Row_chkbatch = mysqli_fetch_array($chkbatch[2]);
            if (in_array($Batch, $_Row_chkbatch)) {
                //code to get Lcore at time of deregister
                ini_set("display_errors", "on");
                ini_set("error_reporting", E_ERROR);
                require_once ("../nusoap/nuSOAP/lib/nusoap.php");
                define("SERVER_WSDL_API", "http://ilearn.myrkcl.com/nusoap/toc.php?wsdl");
                //define("SERVER_WSDL_API", "http://localhost/toc/nusoap/toc.php?wsdl");
                $key = "RKCL_ILEARN";
                $server_ip = '49.50.79.170';
                $cname = 'RKCLILEARN';
                $client = new nusoap_client(SERVER_WSDL_API, true, false, false, false, false, 300, 300);
                $client->soap_defencoding = 'UTF-8';
                $client->decode_utf8 = false;
                ini_set('default_socket_timeout', 5000);
                $lcode = $_POST["lcode"];
                $result = $client->call("getLearnerAssessementFinalResult", array("cname" => $cname, "hashkey" => $key, "clientip" => $server_ip, "lcode" => $AdmLCode));
                $num_rowsSco = json_decode(base64_decode($result), true);
                //code to get Lcore at time of deregister
                // code for get learner attandace at the time of derigester
                $response2 = $emp->GetAttendanceCode($AdmLCode , $itgkcode);
                // print_r($response2);
                if ($response2[0] == "Success") {
                    $_Row1 = mysqli_fetch_array($response2[2]);
                    $attnd = $_Row1['Days'];
                } else {
                    $attnd = "0";
                }
                $deregreason = trim($_POST['deregreason']);
                $responseUpdAdmLog = $emp->UpdDeregApplication($_POST['lcode'], $AdmLCode, $OldIso_template, $num_rowsSco, $deregreason, $attnd, $Course, $Batch);
                echo $responseUpdAdmLog[0];
            } else {
                session_destroy();
                ?>
                <script> window.location.href = "index.php";</script>
                <?php
            }
        }
        //for log creation
        // $response = $emp->LearnerDeregister($_POST['lcode']);
    }
}
