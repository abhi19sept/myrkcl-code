<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsnews.php';

    $response = array();
    $emp = new clsnews();
    if ($_action == "ADD") {
        if (!empty($_POST["News"]))
			 {
				$_icons = $_POST["icons"];
				$_News = $_POST["News"];
				$_Link= $_POST["Link"];
				
			
            $response = $emp->Add($_icons,$_News,$_Link);
            echo $response[0];
        }
    }
    
    if ($_action == "UPDATE") {
        if (isset($_POST["$_News"])) {
			$_code = $_POST["code"];
            $_icons = $_POST["icons"];
            $_News = $_POST["News"];
            $_Link=$_POST['Link'];
            $response = $emp->Update($_code,$_icons,$_News, $_Link);
            echo $response[0];
        }
    }
    
    
    if ($_action == "EDIT") {


        $response = $emp->GetDatabyCode($_actionvalue);

        $_DataTable = array();
        $_i=0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("id" => $_Row['id'],
                "value"=>$_Row['value'],
				"news"=>$_Row['news'],
                "link"=>$_Row['link']);
            $_i=$_i+1;
        }
        echo json_encode($_Datatable);
    }
    
    
    if ($_action == "DELETE") {


	
        $response = $emp->DeleteRecord($_POST['values']);

        echo $response[0];
    }


    if ($_action == "SHOW") {


        $response = $emp->Get();

        $_DataTable = "";

        echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%' style='margin-top:20px ;'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='5%'>S No.</th>";
        echo "<th style='30%'>icon</th>";
        echo "<th style='55%'>News</th>";
        echo "<th style='10%'>Action</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['value'] . "</td>";
            echo "<td>" . $_Row['news'] . "</td>";
			echo "<td>" . $_Row['link'] . "</td>";
            echo "<td><a href='frmnews.php?code=" . $_Row['id'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a> 
			<a href='frmnews.php?code=" . $_Row['id'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
//echo $_DataTable;
    }
    if ($_action == "FILL") {
        $response = $emp->GetAll();
        echo "<option value='0' selected='selected'>Select Status</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['value'] . " class='" .$_Row['value'] . "' style='font-family:FontAwesome !important;'>" . $_Row['inclusions'] . "</option>";
             
     
        }
    }
    
     
    
    
    
?>
