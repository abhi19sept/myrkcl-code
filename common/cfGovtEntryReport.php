<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsGovtEntryReport.php';

$response = array();
$emp = new clsGovtEntryReport();

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll($_POST['status']);

    $_DataTable = "";
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>Center Code</th>";
	echo "<th style='5%'>Center Name</th>";
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";
    echo "<th style='20%'>Father/Husband Name</th>";
    echo "<th style='20%'>Department</th>";
    echo "<th style='20%'>Designation</th>";
    echo "<th style='5%'>Office Address</th>";
	echo "<th style='5%'>Reimbursement Amount</th>";
	echo "<th style='5%'>Lot</th>";
    //echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	   $_Total = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Govemp_ITGK_Code']."</td>";
		echo "<td>" .strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . $_Row['learnercode'] . "</td>";		 
			$fname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['fname']))));
        echo "<td>" . $fname . "</td>";
			$faname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['faname']))));
        echo "<td>" . $faname . "</td>";
        echo "<td>" . $_Row['gdname'] . "</td>";
        echo "<td>" . $_Row['designation'] . "</td>";
		echo "<td>" . $_Row['officeaddress'] . "</td>";
        echo "<td>" . $_Row['trnamount'] . "</td>";
       echo "<td>" . $_Row['lotname'] . "</td>";
        echo "</tr>";
		 $_Total = $_Total + $_Row['trnamount'];
        $_Count++;
    }
    echo "</tbody>";
	 echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
		 echo "<th >  </th>";
		  echo "<th >  </th>";
		   echo "<th >  </th>";
		    echo "<th >  </th>";
			echo "<th >  </th>";
		    echo "<th >  </th>";
        echo "<th >Total Amount </th>";
        echo "<th>";
        echo "$_Total";
        echo "</th>";
		 echo "<th >  </th>";
        echo "</tr>";
        echo "</tfoot>";
    echo "</table>";
	 echo "</div>";
}

if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Status</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Function_Code'] . ">" . $_Row['Function_Name'] . "</option>";
    }
}

if ($_action == "FILLGovLot") {
    $response = $emp->FILLGovLot();
	   print_r($response);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['lotid'] . ">" . $_Row['lotname'] . "</option>";
    }
}

if ($_action == "GetGovtApprovalStatus") {
    $response = $emp->GetGovtApprovalStatus();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}

if ($_action == "ShowReimbursement") {
    //echo "Show";
    $response = $emp->ShowReimbursement($_POST['lotid']);

    $_DataTable = "";
	echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='5%'>Center Code</th>";
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";
	echo "<th style='20%'>Father/Husband Name</th>";	
    echo "<th style='20%'>District</th>";
	echo "<th style='20%'>Department</th>";
    echo "<th style='20%'>Mobile</th>";
	echo "<th style='20%'>Designation</th>";
	echo "<th style='20%'>Office Address</th>";
	echo "<th style='20%'>Account No.</th>";
	echo "<th style='20%'>Bank Name</th>";
	echo "<th style='20%'>IFSC Code</th>";
	echo "<th style='20%'>Fee</th>";
	echo "<th style='20%'>Incentive</th>";
	echo "<th style='20%'>Amount</th>";	
    echo "<th style='20%'>Batch Id</th>";    
    //echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	   $_Total = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Govemp_ITGK_Code'] . "</td>";
        echo "<td>" . $_Row['learnercode'] . "</td>";		 
			$fname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['fname']))));
        echo "<td>" . $fname . "</td>";
			$faname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['faname']))));
		echo "<td>" . $faname . "</td>";
        echo "<td>" . $_Row['ldistrictname'] . "</td>";
        echo "<td>" . $_Row['gdname'] . "</td>";
        echo "<td>" . $_Row['lmobileno'] . "</td>";
        echo "<td>" . $_Row['designation'] . "</td>";
		echo "<td>" . $_Row['officeaddress'] . "</td>";
        echo "<td>" . $_Row['empaccountno'] . "</td>";
        echo "<td>" . $_Row['gbankname'] . "</td>";
        echo "<td>" . strtoupper($_Row['gifsccode']) . "</td>";
		echo "<td>" . $_Row['fee'] . "</td>";
        echo "<td>" . $_Row['incentive'] . "</td>";
        echo "<td>" . $_Row['trnamount'] . "</td>";
        echo "<td>" . $_Row['batchid'] . "</td>";		
        echo "</tr>";
		 //$_Total = $_Total + $_Row['trnamount'];
         $_Count++;
    }
		echo "</tbody>";
		echo "</table>";
		echo "</div>";
}
?>