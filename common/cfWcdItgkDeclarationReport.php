<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsWcdItgkDeclarationReport.php';

$response = array();
$emp = new clsWcdItgkDeclarationReport();

if ($_action == "FillCourse") {
    $response = $emp->GetAdmissionCourse();	
    echo "<option value=''>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FillBatch") {
    $response = $emp->GetAdmissionBatchcode($_POST['values']);
    echo "<option value='' selected='selected'>Select Batch</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "GETDATA") {
    $response = $emp->GetDataAll($_POST['course'], $_POST['batch']);
    $_DataTable = "";

		echo "<div class='table-responsive' style='margin-top:10px'>";
   
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0'
			class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th style='15%'>S No.</th>";
		echo "<th style='15%'>ITGK-Code</th>";
		echo "<th style='15%'>IT-GK Name</th>";
		echo "<th style='15%'>IT-GK Mobile</th>";
		echo "<th style='15%'>IT-GK District</th>";
		echo "<th style='15%'>Course Name</th>";		
		echo "<th style='15%'>SP Name</th>";
		echo "<th style='15%'>SP Code</th>";
		echo "<th style='15%'>Status</th>";
        echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$_Count = 1;
		$co = mysqli_num_rows($response[2]);
		if ($co) {
			while ($_Row = mysqli_fetch_array($response[2])){
				echo "<tr class='odd gradeX'>";
				echo "<td>" . $_Count . "</td>";
				echo "<td>" . $_Row['Courseitgk_ITGK'] . "</td>";
				echo "<td>" . $_Row['ITGK_Name'] . "</td>";
				echo "<td>" . $_Row['ITGKMOBILE'] . "</td>";
				echo "<td>" . $_Row['District_Name'] . "</td>";				
				echo "<td>" . $_Row['Courseitgk_Course'] . "</td>";							
				echo "<td>" . $_Row['RSP_Name'] . "</td>";				
				echo "<td>" . $_Row['RSP_Code'] . "</td>";
				echo "<td>" . $_Row['status'] . "</td>";
					
				echo "</tr>";
				$_Count++;
			}
			echo "</tbody>";
			echo "</table>";
			echo "</div>";
		}
}