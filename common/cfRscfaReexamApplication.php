<?php

/*
 * author Mayank
clsRscfaReexamApplication.php
 */
include './commonFunction.php';
require 'BAL/clsRscfaReexamApplication.php';

$response = array();
$emp = new clsRscfaReexamApplication();

if ($_action == "FillCourse") {
    $response = $emp->GetCourse();
		if($response[0]=="Success"){
			echo "<option value='' selected='selected'>Select Course</option>";
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
				}
		}
		else{
			echo "1";
		}   
}

if ($_action == "FillBatch") {
    $response = $emp->FILLBatchName($_POST['values']);
		if($_POST['values']=='24'){
			echo "<option value='' selected='selected'>Select Batch </option>";
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
			}
		}    
}

if ($_action == "FILLEXAMEVENTMANAGE") {
    $response = $emp->GetExamEventManage($_POST['values']);
    echo "<option value=''>Select Exam Event</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Affilate_Event'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}


function generateLearnerGrid($response) {
    global $emp;

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
		echo "<th style='15%'>ITGK-Code</th>";
		echo "<th style='15%'>Learner Code</th>";		
		echo "<th style='15%'>Learner Name</th>";
		echo "<th style='15%'>Father Name</th>";
		echo "<th style='15%'>D.O.B</th>";
		echo "<th style='15%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['study_cen'] . "</td>";
        echo "<td>'" . $_Row['lcode'] . "</td>";
        echo "<td>" . strtoupper($_Row['name']) . "</td>";
        echo "<td>" . strtoupper($_Row['fname']) . "</td>";
        echo "<td>" . $_Row['dob'] . "</td>";

      if($_Row['rscfa_reexam_lcode'] == ''){
					echo "<td><input type='button' class='updcount btn btn-primary btn-sm' id='".$_Row['lcode']."' AckCode='".$_Row['lcode']."' CourseCode='" . $_Row['course'] . "' BatchCode='" . $_Row['batch'] . "' mode='ShowUpload' value='Apply for Re-Exam'/></td>";
				}
				else{
					echo "<td>Already Applied</td>";
				} 
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "SHOWALL") {
    if (!empty($_POST['batch'])) {
        $response = $emp->GetAllRscfaLearner($_POST['batch'], $_POST['course']);
        generateLearnerGrid($response);
    }
}

if ($_action == "ApplyForReexamApplication") {
        $lcode  = $_POST['lcode'];
        $batch = $_POST['bcode'];
        $course = $_POST['ccode'];
        $result = $emp->ApplyForReexamApplication($lcode, $batch, $course);
        echo $result[0];
    } 
	