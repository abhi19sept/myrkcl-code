<?php

/*
 * Created by Mayank
clsICTReport
 */

include './commonFunction.php';
require 'BAL/clsICTReport.php';

$response = array();
$emp = new clsICTReport();

if ($_action == "GETDATA") {
    //echo "Show";
		
		if($_POST['batch'] !='') {
		 
			  $response = $emp->GetAllDetails($_POST['course'],$_POST['batch']);
			  
			   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' 
							class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";				
				echo "<th>Govt. Employee Category </th>";
				echo "<th>Learner Code</th>";
				echo "<th>Learner Name</th>";
				echo "<th>ITGK Code</th>";
				echo "<th>ITGK Name</th>";
				echo "<th>District</th>";
				echo "<th>Result</th>";
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;
					while ($_Row = mysqli_fetch_array($response[2])) {
						echo "<tr class='odd gradeX'>";
						echo "<td>" . $_Count . "</td>";
									
						echo "<td>" . strtoupper($_Row['LearnerType_Name']) . "</td>";
						echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
						echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
						echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
						echo "<td>" . strtoupper($_Row['ITGK_Name']) . "</td>";
						echo "<td>" . strtoupper($_Row['District_Name']) . "</td>";
						echo "<td>" . strtoupper($_Row['result']) . "</td>";
						echo "</tr>";
								
						$_Count++;	
					}
				 echo "</tbody>";					
				 echo "</table>";
				 echo "</div>";
		 
		}
		  else {
			  echo "1";
		  }
   
}

if ($_action == "FillCourse") {
    $response = $emp->FillCourse($_POST['values']);    
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo $_Row['Course_Name'];
    }
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}
