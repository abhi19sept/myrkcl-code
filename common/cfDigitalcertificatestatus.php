<?php

include './commonFunction.php';
require 'BAL/clsDigitalcertificatestatus.php';

$response = array();
$emp = new clsDigitalCertificateStatus();


if ($_action == "FILLLinkAadharCourse") {
    $response = $emp->GetCourseCertificate();
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLAdmissionBatch") {
    $response = $emp->GetAllBatchCertificate($_POST['values']);
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "UPDATESTATUS") {
    $responseres = "";
    $course = $_POST["course"];
    $batch = $_POST["batch"];
    $learnercode = $_POST["lcode"];
    $status = $_POST["status"];
    if($status == "0"){
        $statusAddmission = "0";
        $statusResult = "0";
    } else if($status == "1"){
        $statusAddmission = "1";
        $statusResult = "0";
    } else if($status == "2"){
        $statusAddmission = "2";
        $statusResult = "1";
    } else if($status == "3"){
        $statusAddmission = "3";
        $statusResult = "1";
    } else if($status == "4"){
        $statusAddmission = "4";
        $statusResult = "2";
    }else{
        $responseres = "error";
    }
    
    if($responseres == "error"){
        echo "error";
    }else{
        $response = $emp->UpdateCertificateStatus($course, $batch, $learnercode, $statusAddmission, $statusResult);
        if ($response[2] != "0") {
            echo "success";
        }
    }
    
    
}