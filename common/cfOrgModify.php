<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsOrgModify.php';

$response = array();
$emp = new clsOrgModify();



if ($_action == "DETAILS") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                "regno" => $_Row['Organization_RegistrationNo'],
                "rsploginid" => $_Row['Org_RspLoginId'],
                "fdate" => $_Row['Organization_FoundedDate'],
                "panno" => $_Row['Org_PAN'],
                "pincode" => $_Row['Org_PinCode'],
                "landmark" => $_Row['Organization_Landmark'],
                "policestation" => $_Row['Org_Police'],
                "village" => $_Row['Org_Village'],
                "mohalla" => $_Row['Org_Mohalla'],
                "wardno" => $_Row['Org_WardNo'],
                "municipal" => $_Row['Org_Municipal'],
                "email" => $_Row['Org_Email'],
                "mobile" => $_Row['Org_Mobile'],
                "road" => $_Row['Organization_Road']);
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "";
    }
}

if ($_action == "UPDATE") {
    //print_r($_POST);
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        if (isset($_POST["type"]) && !empty($_POST["type"])) {
            if (isset($_POST["regno"]) && !empty($_POST["regno"])) {
                if (isset($_POST["state"]) && !empty($_POST["state"])) {
                    if (isset($_POST["region"]) && !empty($_POST["region"])) {
                        if (isset($_POST["district"]) && !empty($_POST["district"])) {
                            if (isset($_POST["tehsil"]) && !empty($_POST["tehsil"])) {
                                if (isset($_POST["landmark"]) && !empty($_POST["landmark"])) {
                                    if (isset($_POST["road"]) && !empty($_POST["road"])) {
                                        if (isset($_POST["panno"]) && !empty($_POST["panno"])) {
                                            if (isset($_POST["areatype"]) && !empty($_POST["areatype"])) {
                                                if (isset($_POST["docproof"]) && !empty($_POST["docproof"])) {
                                                    if (isset($_POST["country"]) && !empty($_POST["country"])) {
                                                        if (isset($_POST["doctype"]) && !empty($_POST["doctype"])) {

                                                            $_Ackno = $_POST["code"];
                                                            $_RspLoginId = $_POST["rsploginid"];
                                                            $_OrgAOType = $_POST["orgtype"];
                                                            $_OrgEmail = $_POST["email"];
                                                            $_OrgMobile = $_POST["mobile"];
                                                            $_OrgName = $_POST["name"];
                                                            $_OrgType = $_POST["type"];
                                                            $_OrgRegno = $_POST["regno"];
                                                            $_State = $_POST["state"];
                                                            $_Region = $_POST["region"];
                                                            $_District = $_POST["district"];
                                                            $_Tehsil = $_POST["tehsil"];
                                                            $_Landmark = $_POST["landmark"];
                                                            $_Road = $_POST["road"];
                                                            $_PinCode = $_POST["pincode"];
                                                            $_OrgEstDate = $_POST["estdate"];
                                                            $_docproof = $_POST["docproof"];
                                                            $_doctype = $_POST["doctype"];
                                                            $_OrgCountry = $_POST["country"];
                                                            $_PanNo = $_POST["panno"];

                                                            $_AreaType = $_POST["areatype"];
                                                            $_Mohalla = $_POST["mohalla"];
                                                            $_WardNo = $_POST["wardno"];
                                                            $_Police = $_POST["police"];
                                                            $_Municipal = $_POST["municipal"];
                                                            $_Village = $_POST["village"];
                                                            $_Gram = $_POST["gram"];
                                                            $_Panchayat = $_POST["panchayat"];
                                                            ///                                                  $_Genid = $_POST["txtGenerateId"];


                                                            $response = $emp->Update($_Ackno, $_RspLoginId, $_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, $_State, $_Region, $_District, $_Tehsil, $_Landmark, $_Road, $_PinCode, $_OrgEstDate, $_OrgCountry, $_docproof, $_doctype, $_PanNo, $_AreaType, $_Mohalla, $_WardNo, $_Police, $_Municipal, $_Village, $_Gram, $_Panchayat);
                                                            echo $response[0];
                                                        } else {
                                                            echo "Inavalid Entry1";
                                                        }
                                                    } else {
                                                        echo "Inavalid Entry2";
                                                    }
                                                } else {
                                                    echo "Inavalid Entry3";
                                                }
                                            } else {
                                                echo "Inavalid Entry4";
                                            }
                                        } else {
                                            echo "Inavalid Entry5";
                                        }
                                    } else {
                                        echo "Inavalid Entry6";
                                    }
                                } else {
                                    echo "Inavalid Entry7";
                                }
                            } else {
                                echo "Inavalid Entry8";
                            }
                        } else {
                            echo "Inavalid Entry9";
                        }
                    } else {
                        echo "Inavalid Entry11";
                    }
                } else {
                    echo "Inavalid Entry12";
                }
            } else {
                echo "Inavalid Entry13";
            }
        } else {
            echo "Inavalid Entry14";
        }
    } else {
        echo "Inavalid Entry15";
    }
}
