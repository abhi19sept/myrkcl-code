<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsOrgStatusReport.php';

$response = array();
$emp = new clsOrgStatusReport();


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='60%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='60%'>RSP Name</th>";
    echo "<th style='10%'>IT-GK Area Type</th>";
    
    echo "<th style='10%'>Mohalla (Urban)/Panchayat Samiti (Rural)</th>";
    echo "<th style='10%'>Ward No (Urban)/Gram Panchayat (Rural)</th>";
    echo "<th style='10%'>Municipal Town (Urban)/Village (Rural)</th>";
    
    echo "<th style='40%'>IT-GK District</th>";
    echo "<th style='10%'>IT-GK Tehsil</th>";
    echo "<th style='10%'>IT-GK Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['User_LoginId'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspname'] . "</td>";
            if ($_Row['Organization_AreaType'] == '') {
                echo "<td>" . "NA" . "</td>";
            } else {
                echo "<td>" . $_Row['Organization_AreaType'] . "</td>";
            }
            if ($_Row['Organization_AreaType'] == 'Urban') {
                echo "<td>" . $_Row['Organization_Mohalla'] . "</td>";
                echo "<td>" . $_Row['Organization_WardNo'] . "</td>";
                echo "<td>" . $_Row['Organization_Municipal'] . "</td>";
            } elseif ($_Row['Organization_Status'] == '0') {
                echo "<td>" . "NA". "</td>";
                echo "<td>" . "NA" . "</td>";
                echo "<td>" . "NA" . "</td>";
            } else {
                echo "<td>" . $_Row['Block_Name'] . "</td>";
                echo "<td>" . $_Row['GP_Name'] . "</td>";
                echo "<td>" . $_Row['Village_Name'] . "</td>";
            }
            
            
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            if ($_Row['Organization_Status'] == '0') {
                echo "<td>" . "Details Not Filled by IT-GK" . "</td>";
            } else {
                echo "<td>" . "Complete" . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


