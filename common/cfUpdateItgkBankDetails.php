<?php

/* 
 * author Mayank
 */
include './commonFunction.php';
require 'BAL/clsUpdateItgkBankDetails.php';

$response = array();
$emp = new clsUpdateItgkBankDetails();
$_ObjFTPConnection = new ftpConnection();


if ($_action == "AuthenticateItgk") {
    $response = $emp->AuthenticateItgk();
        if($response[0] == 'Success') 
		{
			$row=mysqli_fetch_array($response[2]);
			$updstatus=$row['BA_Status'];
				if($updstatus=='1'){
					 echo "2";
				}
				else{
					$response = $emp->SHOWDATA();

    $_DataTable = "";
global  $_ObjFTPConnection;
   echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";	
	echo "<th style='10%'>  ITGK Code</th>";
    echo "<th style='45%'>	Bank Account Name</th>";
    echo "<th style='40%'> 	Bank Account Number</th>";
    echo "<th style='10%'> 	Bank IFSC Code</th>";
	echo "<th style='10%'> 	Bank Name</th>";
	echo "<th style='10%'>  Bank Branch Name</th>";
	echo "<th style='10%'>  Pan Card No</th>";
	echo "<th style='10%'>  Bank Document</th>";
	echo "<th style='10%'>  Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
		$ext = pathinfo($_Row['Bank_Document'], PATHINFO_EXTENSION);
		
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Bank_User_Code'] . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
        echo "<td>'" . $_Row['Bank_Account_Number'] . "</td>";
		echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
		echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
		echo "<td>" . strtoupper($_Row['Bank_Branch_Name']) . "</td>";
		echo "<td>" . strtoupper($_Row['Pan_No']) . "</td>";
		
		$image = $_Row['Bank_Document'];
			echo "<td >"
				. "<input type='button' name='Edit' class='btn btn-primary view_photo'
			image='".$image."' id='".$_Count.'_photo'."' count='".$_Count."' value='View Document'/>". "<div style='display:none;' id='Viewphoto_".$_Count."'> </div>"
			."</td>";
			
		echo "<td>"
				. "<input type='button' name='Edit' class='btn btn-primary upd_details' itgk='".$_Row['Bank_User_Code']."'
			id='".$_Row['Bank_Account_Code']."' accno='".$_Row['Bank_Account_Number']."' value='Update Details'/>". "</td>";
        echo "</tr>";
        $_Count++;
    }
   echo "</tbody>";
    echo "</table>";
	
	
	
	
	echo "</div>";
				}			
		}		
		else
		{
			echo "1";
		}
    
}

if ($_action == "ViewPhoto") {
    $photopath = '/Bankdocs/';
	$ext = pathinfo($_POST['image'], PATHINFO_EXTENSION);
		
    $image = $_POST['image'];
	
    global $_ObjFTPConnection;
	
	$details= $_ObjFTPConnection->ftpdetails();
	
    $photoimage =  file_get_contents($details.$photopath.$image);
	$imageData = base64_encode($photoimage);
		
		if($ext == 'pdf'){
			echo "<iframe id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='100' height='130'></iframe>";
		}else{
			echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='100' height='130'/>";
		}
}

if ($_action == "UpdateBankDetails") {
	if (isset($_POST["txtIfscCode"]) && !empty($_POST["txtIfscCode"])){
			if (isset($_POST["ddlBankName"]) && !empty($_POST["ddlBankName"])){
				if (isset($_POST["autoid"]) && !empty($_POST["autoid"])){
					$_IfscCode = $_POST["txtIfscCode"];
					$_BankName = $_POST["ddlBankName"];
					$_AccountNumber = $_POST["accno"];
					$_ITGKCode = $_SESSION['User_LoginId'];
					$autoid = $_POST["autoid"];
					$docname="";	
						if (!empty($_FILES['chequeImage']['name'])){
							$_UploadDirectory1 = '/Bankdocs';
							
							if ($_FILES["chequeImage"]["name"] != '') {
									$imag = $_FILES["chequeImage"]["name"];
									$imageinfo = pathinfo($_FILES["chequeImage"]["name"]);
									if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
										$error_msg = "Image must be in either PNG or JPG or JPEG or PDF Format";
										$flag = 0;
									} else {
										if (file_exists("$_UploadDirectory1/" . $_SESSION['User_LoginId'].'_'.$_AccountNumber.'_'.'document'. '.' . substr($imag, -3))) {
											$_FILES["chequeImage"]["name"] . "already exists";
										}
										else {
											ftpUploadFile($_UploadDirectory1,$_SESSION['User_LoginId'].'_'.$_AccountNumber.'_'.'document'.'.jpg',$_FILES["chequeImage"]["tmp_name"]);
										}
											
									}
									
									$docname= $_SESSION['User_LoginId'].'_'.$_AccountNumber.'_'.'document'.'.jpg';
								}
						}
					if($docname==''){
						$response = $emp->Updatewithoutdoc($autoid,$_IfscCode,$_BankName);
						echo $response[0];
					}
					else{
						$response = $emp->Updatewithdoc($autoid,$_IfscCode,$_BankName,$docname);
						echo $response[0];
					}
						
				}
				else{
					echo "some";
				}
			}
			else{
				echo "bank";
			}
		}
		else{
				echo "ifsc";
			}
     }


if ($_action == "FILLBanks") {
    $response = $emp->GetAllBanks();
    echo "<option value='' >Select Banks</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['bankname'] . "'>" . $_Row['bankname'] . "</option>";
    }
}