<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsUserRoleMaster.php';

$response = array();
$emp = new clsUserRoleMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"])) {
        $_RoleName = $_POST["name"];
        $_RoleStatus = $_POST["status"];
        $_RoleEntity=$_POST["entity"];

        $response = $emp->Add($_RoleName, $_RoleStatus,$_RoleEntity);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_RoleName = $_POST["name"];
        $_RoleStatus = $_POST["status"];
        $_Role_Code = $_POST['code'];
        $_RoleEntity=$_POST["entity"];
        $response = $emp->Update($_Role_Code, $_RoleName, $_RoleStatus,$_RoleEntity);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("RoleCode" => $_Row['UserRoll_Code'],
            "RoleName" => $_Row['UserRoll_Name'],
            "Status" => $_Row['UserRoll_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
     echo "<th style='30%'>Entity Name</th>";
    echo "<th style='30%'>Name</th>";
    echo "<th style='25%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
         echo "<td>" . $_Row['Entity_Name'] . "</td>";
        echo "<td>" . $_Row['UserRoll_Name'] . "</td>";
        echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmuserrolemaster.php?code=" . $_Row['UserRoll_Code'] . "&Mode=Edit'><img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmuserrolemaster.php?code=" . $_Row['UserRoll_Code'] . "&Mode=Delete'><img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Role</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['UserRoll_Code'] . ">" . $_Row['UserRoll_Name'] . "</option>";
    }
}

if ($_action == "FILLBYENTITY") {
    
    $response = $emp->GetByEntity($_POST['entity']);
    //print_r($response);
    echo "<option value='0' selected='selected'>Select Role</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['UserRoll_Code'] . ">" . $_Row['UserRoll_Name'] . "</option>";
    }
}

if ($_action == "FILLENTITY") {
    $response = $emp->GetEntity();
    echo "<option value='0' selected='selected'>Select Entity</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Entity_Code'] . ">" . $_Row['Entity_Name'] . "</option>";
    }
}
?>
<script>
		$(document).ready(function() {
    $('#example').DataTable();
} );
	</script>