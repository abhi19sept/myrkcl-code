<?php
include './commonFunction.php';
require 'BAL/clsusertrackreport.php';

$response = array();
$emp = new clsUserReport();

if ($_POST['action'] == "FillUserRoll") {
    $response = $emp->GetUserRoll();
    echo "<option value=''>Are you looking for ?</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['UserRoll_Code'] . ">" . $_Row['UserRoll_Name'] . "</option>";
    }
}
if ($_POST['action'] == "FillUsers") {
    $response = $emp->GetAllUser($_POST['Userroll']);
    echo "<option value=''>Select User</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_LoginId'] . ">" . $_Row['User_LoginId'] . "</option>";
    }
}
if ($_POST['action'] == "ViewTrackReport") {
    $ddlUserroll = $_POST['ddlUserroll'];
    $ddlusersloginid = $_POST['ddlusers'];
    $ddlstartdate = $_POST['ddlstartdate'];
    $ddlenddate = $_POST['ddlenddate']." 23:59:59";
    $response = $emp->ShowUserLoginReport($ddlUserroll,$ddlusersloginid,$ddlstartdate,$ddlenddate);
    echo "<div class='table-responsive'>";
    echo  "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>User Login Id</th>";
    echo "<th>IP Address</th>";
    echo "<th>Lat</th>";
    echo "<th>Long</th>";
    echo "<th>Location</th>";
    echo "<th>Time</th>";                    
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" .$row['Track_Userloginid']. "</td>";
            echo "<td>" .$row['Track_REMOTE_ADDR']. "</td>";
            echo "<td>" .$row['Track_Lat']. "</td>";
            echo "<td>" .$row['Track_Long']. "</td>";
            echo "<td>" .$row['Track_Location']. "</td>";
            echo "<td>" .$row['Track_Datetime']. "</td>";
            echo "</tr>";
            $_Count++;
        }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";  
    }
}
if ($_POST['action'] == "ViewTrackReportMapOld") {
    $ddlUserroll = $_POST['ddlUserroll'];
    $ddlusersloginid = $_POST['ddlusers'];
    $ddlstartdate = $_POST['ddlstartdate'];
    $ddlenddate = $_POST['ddlenddate']." 23:59:59";
    $response = $emp->ShowUserLoginReport($ddlUserroll,$ddlusersloginid,$ddlstartdate,$ddlenddate);
    $_i = 0;
    if ($response[0] == 'Success') {
        echo '<script>var markers = [';
        while ($row = mysqli_fetch_array($response[2])) {
           echo '{'; 
           echo '"title":'.'"'.$row['Track_Userloginid'].'",';
           echo '"lat":'.'"'.$row['Track_Lat'].'",';
           echo '"lng":'.'"'.$row['Track_Long'].'",';
           echo '"description":'.'"'.$row['Track_Datetime'].$row['Track_Location'].'"';
           echo '},';
            $_i = $_i + 1;
        }
        echo '];</script>';
    }else{
       echo '<script>var markers = [';
           echo '{'; 
           echo '"title":'.'"null",';
           echo '"lat":'.'"null",';
           echo '"lng":'.'"null",';
           echo '"description":'.'"null"';
           echo '},';
        echo '];</script>';
    }
}
if ($_POST['action'] == "ViewTrackReportMap") {
    $ddlUserroll = $_POST['ddlUserroll'];
    $ddlusersloginid = $_POST['ddlusers'];
    $ddlstartdate = $_POST['ddlstartdate'];
    $ddlenddate = $_POST['ddlenddate']." 23:59:59";
    $response = $emp->ShowUserLoginReport($ddlUserroll,$ddlusersloginid,$ddlstartdate,$ddlenddate);
    $_i = 0;
    if ($response[0] == 'Success') {
        echo '<script>var locations = [';
        while ($row = mysqli_fetch_array($response[2])) {
           echo '{'; 
           echo 'lat:'.$row['Track_Lat'].',';
           echo 'lng:'.$row['Track_Long'];
           echo '},';
            $_i = $_i + 1;
        }
        echo '];</script>';
    }else{
       echo '<script>var locations = [';
           echo '{'; 
           echo 'lat:null,';
           echo 'lng:null';
           echo '},';
        echo '];</script>';
    }
}

?>