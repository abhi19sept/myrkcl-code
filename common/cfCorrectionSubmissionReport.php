<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCorrectionSubmissionReport.php';

$response = array();
$emp = new clsCorrectionSubmissionReport();

if ($_action == "SHOWDETAILS") {

    //echo "Show";
    $response = $emp->GetAllDetails($_POST['lot']);

    $_DataTable = "";
	 echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
	echo "<th style='20%'>Study Center</th>";	
    echo "<th style='20%'>Learner Code</th>";
    echo "<th style='20%'>Name</th>";  
    echo "<th style='20%'>Father/Husband Name</th>";
	echo "<th style='20%'>Applied For</th>";
	echo "<th style='20%'>Exam Event Name</th>";
    echo "<th style='20%'>Marks</th>";
    echo "<th style='5%'>Result</th>";
    echo "<th style='5%'>Email</th>";
    echo "<th style='5%'>Mobile</th>";
    echo "<th style='5%'>Application Date</th>";
    echo "<th style='5%'>Result Date</th>";
    echo "<th style='5%'>Correction Id</th>";
    echo "<th style='5%'>DOB</th>";
    echo "<th style='5%'>District</th>";	
	echo "<th style='20%'>District Code</th>";
	echo "<th style='20%'>Photo</th>";
	echo "<th style='20%'>Correction Certificate</th>";
	echo "<th style='20%'>Marksheet</th>";
	echo "<th style='20%'>Provisional Certificate</th>";
	echo "<th style='20%'>Application Form</th>";
	echo "<th style='20%'>Affidavit</th>";
	
	echo "<th style='display:none;'>Photo</th>";
	echo "<th style='display:none;'>Correction Certificate</th>";
	echo "<th style='display:none;'>Marksheet</th>";
	echo "<th style='display:none;'>Provisional Certificate</th>";
	echo "<th style='display:none;'>Application Form</th>";
	echo "<th style='display:none;'>Affidavit</th>";
	
    
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
		echo "<td>" . $_Row['Correction_ITGK_Code'] . "</td>";
		echo "<td>'" . $_Row['lcode'] . "</td>";
			$cfname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['cfname']))));
        echo "<td>" . $cfname . "</td>";  
			$cfaname = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtoupper($_Row['cfaname']))));
		echo "<td>" . $cfaname . "</td>";
		echo "<td>" . $_Row['applicationfor'] . "</td>";
		echo "<td>" . $_Row['exameventname'] . "</td>";
        echo "<td>" . $_Row['totalmarks'] . "</td>";
		  if($_Row['totalmarks'] >=40) {
			  $_Result='Pass';
			  echo "<td>" . $_Result . "</td>";
		  }
		  else {
			  $_Result='-';
			  echo "<td>" . $_Result . "</td>";
		  }			
        echo "<td>" . $_Row['emailid'] . "</td>";
        echo "<td>'" . $_Row['mobile'] . "</td>";
        echo "<td>" . $_Row['applicationdate'] . "</td>";
        echo "<td>" . $_Row['result_date'] . "</td>";
        echo "<td>" . $_Row['cid'] . "</td>";
        echo "<td>" . $_Row['dob'] . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";	
		echo "<td>" . $_Row['district_code'] . "</td>";
			if($_Row['applicationfor']=='Correction Certificate'){
				$type='correction';
			}
			else if($_Row['applicationfor']=='Duplicate Certificate'){
				$type='duplicate';
			}
			else {
				$type='correctionwithduplicate';
			}
			
		echo "<td><button type='button' data-toggle='modal' data-target='#GetPhoto' class='GetPhoto'
						id='".$_Row['photo']."' >View Photo</button></td>";
						
		echo "<td><button type='button' data-toggle='modal' data-target='#Getcertificate' class='Getcertificate'
						id='".$_Row['attach2']."' >View Correction Certificate</button></td>";
						
		echo "<td><button type='button' data-toggle='modal' data-target='#GetMarksheet' class='GetMarksheet'
						id='".$_Row['attach1']."' >View Marksheet</button></td>";
		
			if($_Row['attach4']!=''){
				echo "<td><button type='button' data-toggle='modal' data-target='#GetProvisional' class='GetProvisional'
						id='".$_Row['attach4']."' >View Provisional Certificate</button></td>";
			}
			else{
				echo "<td> NA </td>";
			}
			
			if($_Row['attach5']!=''){
				echo "<td><button type='button' data-toggle='modal' data-target='#GetApplication' class='GetApplication'
						id='".$_Row['attach5']."' >View Application Form</button></td>";
			}
			else{
				echo "<td> NA </td>";
			}
			
			if($_Row['attach6']!=''){
				echo "<td><button type='button' data-toggle='modal' data-target='#GetAffidavit' class='GetAffidavit'
						id='".$_Row['attach6']."' >View Affidavit</button></td>";
			}
			else{
				echo "<td> NA </td>";
			}			
		echo "<td style='display:none'> <span>".$_Row['photo']."</span> </td>";
		echo "<td style='display:none'> <span>".$_Row['attach1']."</span> </td>";
		echo "<td style='display:none'> <span>".$_Row['attach2']."</span> </td>";
			if($_Row['attach4']!=''){
				echo "<td style='display:none'> <span>".$_Row['attach4']."</span> </td>";
			}
			else{
				echo "<td style='display:none'> NA </td>";
			}
		
			if($_Row['attach5']!=''){
				echo "<td style='display:none'> <span>".$_Row['attach5']."</span> </td>";
			}
			else{
				echo "<td style='display:none'> NA </td>";
			}
			
			if($_Row['attach6']!=''){
				echo "<td style='display:none'> <span>".$_Row['attach6']."</span> </td>";
			}
			else{
				echo "<td style='display:none'> NA </td>";
			}
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	 echo "</div>";
}


if ($_action == "FILLCorrectionLot") {
    $response = $emp->FILLCorrectionLot();
	   print_r($response);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['lotid'] . ">" . $_Row['lotname'] . "</option>";
    }
}

?>
