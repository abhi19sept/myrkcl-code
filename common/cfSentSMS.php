<?php

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
//require 'commonFunction.php';
set_include_path(dirname(__FILE__) . '/../');
$_action = (isset($_POST["action"]) ? $_POST["action"] : '');
$_actionvalue = (isset($_POST["values"]) ? $_POST["values"] : '');
if (!isset($_SESSION)) {
    session_start();
}
require 'BAL/clsSentSMS.php';
include 'DAL/sendsms.php';

$response = array();

$emp = new clsSentSMS();

if ($_action == "FILLCourse") {
    $response = $emp->GetAllCourse();
    echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLBatch") {
    $response = $emp->GetBatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_POST['action'] == "VIEWREPORT") {
    $coursecode = $_POST['ddlCourse'];
    $batchcode = $_POST['ddlBatch'];
    $psms = $_POST['smstextmessage'];
    if ($_POST['ddlCategory'] == '2') {
        $response = $emp->SendMsgLearner($coursecode, $batchcode);
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th>IT-GK Code</th>";
        echo "<th>Learner Code</th>";
        echo "<th>Learner Name</th>";
        echo "<th>Learner Mobile</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;
        $sendmsgreport = array();
        if ($response[0] == 'Success') {
            $resultinternal = 0;
            while ($row = mysqli_fetch_array($response[2])) {
                $mobilelen = strlen($row['Admission_Mobile']);
                if (strlen($row['Admission_Mobile']) == 10 && substr($row['Admission_Mobile'], 0, 1) != 0) {

                    $sendmsgreport[] = $row['Admission_Mobile'];
                }
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $row['Admission_ITGK_Code'] . "</td>";
                echo "<td>" . $row['Admission_LearnerCode'] . "</td>";
                echo "<td>" . $row['Admission_Name'] . "</td>";
                echo "<td>" . $row['Admission_Mobile'] . "</td>";
                echo "</tr>";
                $_Count++;
            }
        }


        echo "</tbody>";
        echo "</table>";
        echo "</div>";
        $commaList = implode(', ', $sendmsgreport);

        echo "<button style='margin-bottom: 15px;' type='button' name='sendsmsbtnpop'  id='sendsmsbtnpop' class='btnyogi btn btn-success btn-sm'>Send Message To All</button>";
        echo "<div id='myModaldesc' class='modal'>
            <div class='modal-content' style='width: 80%; margin: 100px 0 0 130px;'>
              <div class='modal-header'>
                <span class='close'>&times;</span>
                <h6>Send Message</h6>
              </div>
              <div class='modal-body'>
                  <form style='margin-top: 35px;' class='form-horizontal' method='POST' id='sendsmsForm' name='sendsmsForm'>
                        <input type='hidden' id='action' name='action' value='SendSmsAc'>
                        <input type='hidden' id='mobilenumbers' name='mobilenumbers' value='" . str_replace(' ', '', $commaList) . "'>
                        <div class='form-group'>
                            <label for='inputEmail3' class='col-sm-4 control-label examlebel'>Type Mesage Here</label>
                            <div class='col-sm-6'>
                                <textarea class='form-control' id='smstextmessage' name='smstextmessage' placeholder='Enter Message' type='text' required/></textarea>
                            </div>
                        </div>
                        <div class='form-group'>
                            <div class='col-sm-offset-3 col-sm-6' id='responsesms'></div>
                        </div>
                        <div class='form-group last'>
                            <div class='col-sm-offset-3 col-sm-6'>
                                <button type='button' name='sendsmsbtn'  id='sendsmsbtn' class='form-control btn btn-success btn-sm'>Sent Message</button>		
                            </div>
                        </div>
                  </form>
              </div>
            </div>
          </div>";
    }
    if ($_POST['ddlCategory'] == '1') {

        //  echo "<a href='common/cfSentSMS.php?ccode=" . $coursecode . "&bcode=" . $batchcode . "&psms=" . $psms . "' style='display:none;'>"
        //   . "<input type='button' name='Approve' id='Approve' class='approvalLetter btn btn-primary' value='Download'/></a>";


        $response = $emp->SendMsgItgk($coursecode, $batchcode);

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th>IT-GK Code</th>";
//        echo "<th>SP Name</th>";
        echo "<th>ITGK Mobile</th>";
        echo "<th>Not Enroll Count</th>";
        echo "<th>Total Admission</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
//
        $_Count = 1;
        $sendmsgreport = array();
        if ($response[0] == 'Success') {
            $resultinternal = 0;

            while ($row = mysqli_fetch_array($response[2])) {

                if (strlen($row['User_MobileNo']) == 10 && substr($row['User_MobileNo'], 0, 1) != 0) {
                    //echo strlen($row['User_MobileNo']);
                    // echo substr($row['User_MobileNo'], 0, 1);

                    $sendmsgreport[] = $row['User_MobileNo'];
                }

                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $row['User_LoginId'] . "</td>";
                echo "<td>" . $row['User_MobileNo'] . "</td>";
                echo "<td>" . $row['notenrollcount'] . "</td>";
                echo "<td>" . $row['totalcount'] . "</td>";

                echo "</tr>";
                $_Count++;
            }
            $response1 = $emp->SendMsgItgk($coursecode, $batchcode);

            $fp = fopen('php://output', 'w');
            $date = new DateTime();

            $fileName = "../upload/gupshupmsg/GupshupSmsFileITGK" . $date->getTimestamp() . ".csv";
            echo "<div class='container'>";
            echo "<input type='button' name='sendpsmsbtn' id='sendpsmsbtn' class='btn btn-primary' value='Send Message'/>";
            echo "<input type='text' id='gupshupfilename' name='gupshupfilename' value='" . $fileName . "'>";
            echo "</div>";

            $myFile = fopen($fileName, 'w');

            fputs($myFile, '"' . implode('","', array('PHONE', 'MESSAGE', 'MSGID')) . '"' . "\n");
//            fputs($myFile, '"' . implode('","', array('9649900905', $psms)) . '"' . "\r\n");
//            fputs($myFile, '"' . implode('","', array('9649900809', $psms)) . '"' . "\r\n");
            while ($row1 = mysqli_fetch_row($response1[2])) {
                if (strpos($psms, '$')) {
                    $psms1 = explode("$", $psms);
                    $row1['2'] = $psms1[0] . " " . $row1['2'] . " " . $psms1[1];

                    fputcsv($myFile, array($row1['1'], $row1['2']), ',', '"');
                } else {
                    fputcsv($myFile, array($row1['1'], $psms), ',', '"');
                }
            }
            fputcsv($myFile, array('9649900809', $psms), ',', '"');
            fputcsv($myFile, array('9649900905', $psms), ',', '"');
            fputcsv($myFile, array('9649900903', $psms), ',', '"');

            exit;
        }

        echo "</tbody>";
        echo "</table>";

        echo "</div>";
    }
    if ($_POST['ddlCategory'] == '3') {

        //  echo "<a href='common/cfSentSMS.php?ccode=" . $coursecode . "&bcode=" . $batchcode . "&psms=" . $psms . "' style='display:none;'>"
        //   . "<input type='button' name='Approve' id='Approve' class='approvalLetter btn btn-primary' value='Download'/></a>";


        $response = $emp->SendMsgSP($coursecode, $batchcode);

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>S No.</th>";
        echo "<th>SP Name</th>";
//        echo "<th>SP Name</th>";
        echo "<th>SP Mobile</th>";
        echo "<th>Not Enroll Count</th>";
        echo "<th>Total Admission</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
//
        $_Count = 1;
        $sendmsgreport = array();
        if ($response[0] == 'Success') {
            $resultinternal = 0;

            while ($row = mysqli_fetch_array($response[2])) {

                if (strlen($row['User_MobileNo']) == 10 && substr($row['User_MobileNo'], 0, 1) != 0) {
                    //echo strlen($row['User_MobileNo']);
                    // echo substr($row['User_MobileNo'], 0, 1);

                    $sendmsgreport[] = $row['User_MobileNo'];
                }

                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $row['User_LoginId'] . "</td>";
                echo "<td>" . $row['User_MobileNo'] . "</td>";
                echo "<td>" . $row['notenrollcount'] . "</td>";
                echo "<td>" . $row['totalcount'] . "</td>";

                echo "</tr>";
                $_Count++;
            }
            $response1 = $emp->SendMsgSP($coursecode, $batchcode);

            $fp = fopen('php://output', 'w');
            $date = new DateTime();

            $fileName = "../upload/gupshupmsg/GupshupSmsFileSP" . $date->getTimestamp() . ".csv";
            echo "<div class='container'>";
            echo "<input type='button' name='sendpsmsbtn' id='sendpsmsbtn' class='btn btn-primary' value='Send Message'/>";
            echo "<input type='text' id='gupshupfilename' name='gupshupfilename' value='" . $fileName . "'>";
            echo "</div>";
            ;

            $myFile = fopen($fileName, 'w');

            fputs($myFile, '"' . implode('","', array('PHONE', 'MESSAGE', 'MSGID')) . '"' . "\n");
            //  fputs($myFile, '"' . implode('","', array('9649900903', '9649900901')) . '"' . "\r\n");
            while ($row1 = mysqli_fetch_row($response1[2])) {
                if (strpos($psms, '$')) {
                    $psms1 = explode("$", $psms);
                    $row1['2'] = $psms1[0] . " " . $row1['2'] . " " . $psms1[1];

                    fputcsv($myFile, array($row1['1'], $row1['2']), ',', '"');
                } else {
                    fputcsv($myFile, array($row1['1'], $psms), ',', '"');
                }
            }
            fputcsv($myFile, array('9649900809', $psms), ',', '"');
            fputcsv($myFile, array('9649900905', $psms), ',', '"');
            fputcsv($myFile, array('9649900903', $psms), ',', '"');
            exit;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
}

if ($_POST['action'] == "SendSmsAc") {

    $mobilenos = $_POST['mobilenumbers'] . ',9649900809,9649900905,9649900940,9649900717,9649900913,9649905446,9001008979,9649901188';
    $Message = $_POST['smstextmessage'];
    $mnumber = explode(",", $mobilenos);
    $arraychunk = array_chunk($mnumber, 900);
    $count = count($arraychunk);
    for ($i = 0; $i < $count; $i++) {
        $commaList = implode(',', $arraychunk[$i]);
        SendSMS($commaList, $Message);
    }
    echo "Success";
}

if ($_POST['action'] == "sendpmsgitgk") {

    function getCurlValue($filename, $contentType, $postname) {

        if (function_exists('curl_file_create')) {
            return curl_file_create($filename, $contentType, $postname);
        }

        $value = "@{$filename};filename=" . $postname;
        if ($contentType) {
            $value .= ';type=' . $contentType;
        }

        return $value;
    }

    $filename = realpath($_POST['gupshupfilepath']);
    $cfile = getCurlValue($filename, 'Excel/csv', 'EnterpriseUpload.csv');

    $data = array('file' => $cfile);
    $target_url = 'http://enterprise.smsgupshup.com/GatewayAPI/rest';
    $post = array('method' => 'xlsUpload',
        'userid' => '2000134086',
        'password' => 'Rkcl@123!',
        'msg_type' => 'UNICODE_TEXT',
        'xlsFile' => $cfile, #This will be the path where csv file located.
        'auth_scheme' => 'PLAIN',
        'v' => '1.1',
        'filetype' => 'csv');
    //print_r($post);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $target_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_VERBOSE, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    $result = curl_exec($ch);
    if ($result === false) {
        echo 'errorinmsg';
    } else {
        echo 'send';
    }
//        if ($result === false) {
//        echo 'Curl error:' . curl_error($ch);
//    } else {
//        echo "Operation complete without any error \n :" . $result;
//    }
    curl_close($ch);
    echo $result;
}