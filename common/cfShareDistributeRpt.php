<?php

/*
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsShareDistributeRpt.php';

$response = array();
$emp = new clsShareDistributeRpt();

if ($_action == "GETDATA") {

    $response = $emp->GetDataAll($_POST['course'], $_POST['batch']);
    $_Row = mysqli_fetch_assoc($response[2]);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
   // echo "<div class='panel panel-default'>";
   // echo "<div class='panel-heading'>Panel heading</div>";

    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th colspan=3>Course Fee Per Learner : </th>";
    echo "<th>" . $_Row['Course_Fee'] . "</th>";
    //echo "<th colspan=3>RKCL Share Per Learner : </th>";
    //echo "<th>" . $_Row1['RKCL_Share'] . "</th>";
    echo "<th colspan=3>IT-GK Share Per Learner : </th>";
    //$ITGKSHARE = $_Row1['Course_Fee'] - ($_Row1['RKCL_Share'] + $_Row1['VMOU_Share']);
    echo "<th>" . $_Row['ITGK_Share'] . "</th>";
    echo "<th></th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>IT-GK Code</th>";
    echo "<th>Confirmed Learner Count</th>";
    echo "<th>Course</th>";
    echo "<th>Batch</th>";
    echo "<th>Financial Year</th>";
    echo "<th>Total Course Fee</th>";
    //echo "<th >RKCL Share</th>";
    echo "<th >Total IT-GK Share</th>";
    echo "<th >IT-GK Account No.</th>";
    echo "<th >Account Holder Name</th>";
    echo "<th >IT-GK Account IFSC</th>";
    echo "<th>RSP Name</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Totalitgkshare = 0;
    $_Totalrkclshare = 0;
    $_Totalcoursefee = 0;
    $_Totaleconfirm = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
	do
		{
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";

            echo "<td>" . $_Row['confirmcount'] . "</a></td>";
            echo "<td>" . $_Row['course_Name'] . "</td>";
            echo "<td>" . $_Row['Batch_Name'] . "</td>";
            echo "<td>" . $_Row['Financial_Year'] . "</td>";
            $coursefeeitgk = $_Row['Course_Fee'] * $_Row['confirmcount'];
            echo "<td>" . $coursefeeitgk . "</td>";
            //$rkclshareitgk = $_Row1['RKCL_Share'] * $_Row['confirmcount'];
            //echo "<td>" . $rkclshareitgk . "</td>";
            $itgkshareitgk = $_Row['ITGK_Share'] * $_Row['confirmcount'];
            echo "<td>" . $itgkshareitgk . "</td>";
            if ($_Row['Bank_Account_Number'] == '') {
                echo "<td><b>Bank Account Info Not Found</b></td>";
                echo "<td><b>Bank Account Info Not Found</b></td>";
                echo "<td><b>Bank Account Info Not Found</b></td>";
            } else {
                echo "<td>'" . $_Row['Bank_Account_Number'] . "</td>";
                echo "<td nowrap>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
                echo "<td>" . $_Row['Bank_Ifsc_code'] . "</td>";
            }

            echo "<td nowrap>" . $_Row['RSP_Name'] . "</td>";
            echo "</tr>";
            $_Totalitgkshare = $_Totalitgkshare + $itgkshareitgk;
            $_Totalcoursefee = $_Totalcoursefee + $coursefeeitgk;
            //$_Totalrkclshare = $_Totalrkclshare + $rkclshareitgk;
            $_Totaleconfirm = $_Totaleconfirm + $_Row['confirmcount'];

            $_Count++;
        } while ($_Row = mysqli_fetch_assoc($response[2])) ;

        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
        echo "<th >TotalCount </th>";
        echo "<th>";
        echo "$_Totaleconfirm";
        echo "</th>";
        echo "<th >  </th>";
        echo "<th >  </th>";
        echo "<th >  </th>";
        echo "<th > ";
        echo "$_Totalcoursefee";
        echo "</th>";
        //echo "<th > ";
       // echo "$_Totalrkclshare";
       // echo "</th>";
        echo "<th > ";
        echo "$_Totalitgkshare";
        echo "</th>";
        echo "<th >  </th>";
        echo "<th >  </th>";
        echo "<th >  </th>";
        echo "<th >  </th>";
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
        echo "</div>";
       // echo "</div>";
    } else {
        echo "No Record Found";
    }
}
