<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsNcrFeePayment.php';

$response = array();
$emp = new clsNcrFeePayment();


if ($_action == "ADD") {
    $responses = $emp->CheckPayStatus($_POST['ack']);
    $_row = mysqli_fetch_array($responses[2]);
    $paystatus = $_row['Org_PayStatus'];

    $atype = $_row['Org_AreaType'];

    $response = $emp->GetNcrAmt($atype);
    $_rows = mysqli_fetch_array($response[2]);
    $amt = $_rows['Ncr_Fee_Amount'];

    if (! $amt || $paystatus == '1') {
        echo "0";
    } else {
       $productinfo = 'NcrFeePayment';
        $trnxId = $emp->AddNcrPayTran($amt, $productinfo, $_POST);
        if ($_POST['gateway'] == 'razorpay' && !empty($trnxId) && $trnxId != 'TimeCapErr') {
            require("razorpay.php");
        } else {
            echo $trnxId;
        }
    }
}

if ($_action == "SHOW") {
    $response = $emp->GetDetails($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("OwnerName" => $_Row['Organization_Name'],
            "OwnerMobile" => $_Row['User_MobileNo']);

        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}

if ($_action == "APPROVE") {
    $response = $emp->GetDatabyCode($_SESSION['User_LoginId']);
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                "regno" => $_Row['Organization_RegistrationNo'],
                "fdate" => $_Row['Organization_FoundedDate'],
                "orgtype" => $_Row['Org_Type_Name'],
                "doctype" => $_Row['Organization_DocType'],
                "district" => $_Row['District_Name'],
                "tehsil" => $_Row['Tehsil_Name'],
                "road" => $_Row['Organization_Road'],
                "type" => $_Row['Org_AreaType'],
                "ack" => $_Row['Org_Ack']);
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "";
    }
}


if ($_action == "GetNcrAmt") {
    // print_r($_POST);
    $response = $emp->GetNcrAmt($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Ncr_Fee_Amount'];
}


if ($_action == "CheckPayStatus") {
    // print_r($_POST);
    $response = $emp->CheckPayStatus($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Org_PayStatus'];
}

if ($_action == "ChkPayEvent") {

    $response = $emp->ChkPayEvent();

    $num_rows = mysqli_num_rows($response[2]);

    if ($response[0] == "Success") {
        echo "1";
    } else {
        echo "0";
    }
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();
    //print_r($response);
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
    //values
    $key = "X2ZPKM";
    $salt = "8IaBELXB";
    $command1 = "check_isDomestic";   //validatecardnumber   luhn algo
    //$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
    $var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
    //$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
    //$var3 = "500";//  Amount to be used in case of refund

    $hash_str = $key . '|' . $command1 . '|' . $var1 . '|' . $salt;
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key, 'hash' => $hash, 'command' => $command1, 'var1' => $var1);

    $qs = http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
    $response = curlCall($wsUrl, $qs, TRUE);
    //echo "<pre>"; print_r($response); echo "</pre>"; 
    $_DataTable = array();
    $_i = 0;
    $_DataTable[$_i] = array("cardType" => $response['cardType'],
        "cardCategory" => $response['cardCategory']);
    echo json_encode($_DataTable);
    //echo $cardType = $response['cardType'];
    //echo $cardCategory = $response['cardCategory'];
}

if ($_action == "FinalValidateCard") {
    //values
    $key = "X2ZPKM";
    $salt = "8IaBELXB";
    $command1 = "validateCardNumber";   //validatecardnumber   luhn algo
    //$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
    $var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
    //$var2 = "4675764";//"7893238";// Token ID to be used in case of refund
    //$var3 = "500";//  Amount to be used in case of refund

    $hash_str = $key . '|' . $command1 . '|' . $var1 . '|' . $salt;
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key, 'hash' => $hash, 'command' => $command1, 'var1' => $var1);

    $qs = http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
    $response = curlCall($wsUrl, $qs, TRUE);
    //echo "<pre>"; print_r($response); echo "</pre>"; 
    if ($response == 'Invalid') {
        echo "1";
    } else if ($response == 'valid') {
        echo "2";
    }
}

function curlCall($wsUrl, $qs, $true) {
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
    if (curl_errno($c)) {
        $c_error = curl_error($c);
        if (empty($c_error)) {
            $c_error = 'Some server error';
        }
        return array('curl_status' => 'FAILURE', 'error' => $c_error);
    }
    $out = trim($o);
    $arr = json_decode($out, true);
    return $arr;
}

?>
