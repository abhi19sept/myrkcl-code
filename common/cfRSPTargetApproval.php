<?php

/*
 * author Mayank

 */
include './commonFunction.php';
require 'BAL/clsRSPTargetApproval.php';

$response = array();
$emp = new clsRSPTargetApproval();


if ($_action == "ADD") {
    // print_r($_POST);	
    $_UserPermissionArray = array();
    $_RSPCode = array();
    $_i = 0;
    $_Count = 0;
    $l = "";
    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 3) == 'chk') {
            $_UserPermissionArray[$_Count] = array(
                "Function" => substr($key, 3),
                "Attribute" => 3,
                "Status" => 1);
            $_Count++;
            $l .= substr($key, 3) . ",";
        }
        $_RSPCode = rtrim($l, ",");
        //print_r($_RSPCode);
        $count = count($_UserPermissionArray);
    }
    if ($count == '0') {
        echo "0";
    } else {
        $response = $emp->UpdateRSPStatus($_RSPCode);
        echo $response[0];
    }
}

if ($_action == "ShowDetails") {

    //echo "Show";
    $response = $emp->GetAll($_POST['user']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>District Name</th>";
    echo "<th style='40%'>2 Month Rural</th>";
    echo "<th style='10%'>2 Month Urban</th>";
    echo "<th style='40%'>4 Month Rural</th>";
    echo "<th style='10%'>4 Month Urban</th>";
    echo "<th style='40%'>6 Month Rural</th>";
    echo "<th style='10%'>6 Month Urban</th>";
    echo "<th style='40%'>Total ITGKs in 12 months</th>";
    echo "<th style='40%'>Select to Approve</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_TotalTwoR = 0;
    $_TotalTwoU = 0;
    $_TotalFourR = 0;
    $_TotalFourU = 0;
    $_TotalSixR = 0;
    $_TotalSixU = 0;
    $_TotalCount = 0;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
        echo "<td>" . $_Row['Rsptarget_2MonthRural'] . "</td>";
        echo "<td>" . $_Row['Rsptarget_2MonthUrban'] . "</td>";
        echo "<td>" . $_Row['Rsptarget_4MonthRural'] . "</td>";
        echo "<td>" . $_Row['Rsptarget_4MonthUrban'] . "</td>";
        echo "<td>" . $_Row['Rsptarget_6MonthRural'] . "</td>";
        echo "<td>" . $_Row['Rsptarget_6MonthUrban'] . "</td>";
        echo "<td>" . $_Row['Rsptarget_Total'] . "</td>";
        echo "<td><input type='checkbox' id=chk" . $_Row['Rsptarget_Code'] .
        " name=chk" . $_Row['Rsptarget_Code'] . "></input></td>";
        echo "</tr>";
        $_Count++;
        $_TotalTwoR = $_TotalTwoR + $_Row['Rsptarget_2MonthRural'];
        $_TotalTwoU = $_TotalTwoU + $_Row['Rsptarget_2MonthUrban'];
        $_TotalFourR = $_TotalFourR + $_Row['Rsptarget_4MonthRural'];
        $_TotalFourU = $_TotalFourU + $_Row['Rsptarget_4MonthUrban'];
        $_TotalSixR = $_TotalSixR + $_Row['Rsptarget_6MonthRural'];
        $_TotalSixU = $_TotalSixU + $_Row['Rsptarget_6MonthUrban'];
        $_TotalCount = $_TotalCount + $_Row['Rsptarget_Total'];
    }
    echo "</tbody>";
    echo "<tfoot>";
    echo "<tr>";
    echo "<th >  </th>";
    echo "<th >Total</th>";
    echo "<th>";
    echo "$_TotalTwoR";
    echo "</th>";
    echo "<th>";
    echo "$_TotalTwoU";
    echo "</th>";
    echo "<th>";
    echo "$_TotalFourR";
    echo "</th>";
    echo "<th>";
    echo "$_TotalFourU";
    echo "</th>";
    echo "<th>";
    echo "$_TotalSixR";
    echo "</th>";
    echo "<th>";
    echo "$_TotalSixU";
    echo "</th>";
    echo "<th>";
    echo "$_TotalCount";
    echo "</th>";
    echo "<th >  </th>";
    echo "</tr>";
    echo "</tfoot>";
    echo "</table>";
    echo "</div>";
}

//$_POST["action"]
if ($_action == "FILLRSP") {
    $response = $emp->GetAllRSP();
    echo "<option value='0' selected='selected'>Select RSP</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Rsptarget_User'] . ">" . $_Row['Rsptarget_User'] . "</option>";
    }
}

if ($_action == "GETUSER") {
    $response = $emp->GetRSPUser($_POST['values']);
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo $_Row['User_Code'];
    }
}