<?php

/* 
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsParentFunctionMaster.php';

$response = array();
$emp = new clsParentFunctionMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"])) {
        $_ParentFunctionName = $_POST["name"];
        $_Status=$_POST["status"];
        $_DisplayOrder=$_POST['display'];
        $_RootMenu=$_POST['root'];
        $response = $emp->Add($_ParentFunctionName,$_Status,$_DisplayOrder,$_RootMenu);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_ParentFunctionName = $_POST["name"];
        $_Status=$_POST["status"];
        $_Code=$_POST['code'];
        $_DisplayOrder=$_POST['display'];
         $_RootMenu=$_POST['root'];
        $response = $emp->Update($_Code,$_ParentFunctionName,$_Status,$_DisplayOrder,$_RootMenu);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("ParentFunctionCode" => $_Row['Parent_Function_Code'],
            "ParentFunctionName" => $_Row['Parent_Function_Name'],
            "Status"=>$_Row['Parent_Function_Status'],
            "Display"=>$_Row['Parent_Function_Display'],
            "Root"=>$_Row['Parent_Function_Root']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
     echo "<th style='30%'>Root Name</th>";
    echo "<th style='30%'>Parent Function Name</th>";
    echo "<th style='10%'>Display Order</th>";
    echo "<th style='10%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Root_Menu_Name'] . "</td>";
        echo "<td>" . $_Row['Parent_Function_Name'] . "</td>";
         echo "<td>" . $_Row['Parent_Function_Display'] . "</td>";
         echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmparentfunctionmaster.php?code=" . $_Row['Parent_Function_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmparentfunctionmaster.php?code=" . $_Row['Parent_Function_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetDatabyRoot($_actionvalue);
    echo "<option value='0' selected='selected'>Select Parent</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Parent_Function_Code'] . ">" . $_Row['Parent_Function_Name'] . "</option>";
    }
}
?>
<script>
		$(document).ready(function() {
    $('#example').DataTable();
} );
	</script>