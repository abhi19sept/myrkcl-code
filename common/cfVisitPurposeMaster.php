<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsVisitPurposeMaster.php';

$response = array();
$emp = new clsVisitPurposeMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        $_FunctionName = $_POST["name"];
        $_Status=$_POST["status"];

        $response = $emp->Add($_FunctionName,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_FunctionName = $_POST["name"];
        $_Status=$_POST["status"];
        $_Code=$_POST["code"];
        $response = $emp->Update($_Code,$_FunctionName,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);
    //echo $_actionvalue;
    //print_r($response);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("VisitTypeCode" => $_Row['VisitType_Code'],
            "VisitTypeName" => $_Row['VisitType_Name'],
            "Status"=>$_Row['VisitType_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'>Name</th>";
    echo "<th style='30%'>Status</th>";
    echo "<th style='20%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['VisitType_Name'] . "</td>";
         echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmvisitpurposemaster.php?code=" . $_Row['VisitType_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmvisitpurposemaster.php?code=" . $_Row['VisitType_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAllActive();
    echo "<option value='' >Select Visit Purpose</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['VisitType_Code'] . ">" . $_Row['VisitType_Name'] . "</option>";
    }
}