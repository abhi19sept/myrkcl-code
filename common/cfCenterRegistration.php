<?php

/*
 * Created by VIVEK

 */


include './commonFunction.php';
require 'BAL/clsCenterRegistration.php';

$response = array();
$emp = new clsCenterRegistration();


if ($_action == "ADD") {
    //print_r($_POST);
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        if (isset($_POST["type"]) && !empty($_POST["type"])) {
            if (isset($_POST["regno"]) && !empty($_POST["regno"])) {
                if (isset($_POST["state"]) && !empty($_POST["state"])) {
                    if (isset($_POST["region"]) && !empty($_POST["region"])) {
                        if (isset($_POST["district"]) && !empty($_POST["district"])) {
                            if (isset($_POST["tehsil"]) && !empty($_POST["tehsil"])) {
                                if (isset($_POST["landmark"]) && !empty($_POST["landmark"])) {
                                    if (isset($_POST["road"]) && !empty($_POST["road"])) {
                                        if (isset($_POST["panno"]) && !empty($_POST["panno"])) {
                                            if (isset($_POST["areatype"]) && !empty($_POST["areatype"])) {
                                                if (isset($_POST["docproof"]) && !empty($_POST["docproof"])) {
                                                    if (isset($_POST["country"]) && !empty($_POST["country"])) {
                                                        if (isset($_POST["doctype"]) && !empty($_POST["doctype"])) {


                                                            $_OrgAOType = $_POST["orgtype"];
                                                            $_OrgEmail = $_POST["email"];
                                                            $_OrgMobile = $_POST["mobile"];
                                                            $_OrgName = $_POST["name"];
                                                            $_OrgType = $_POST["type"];
                                                            $_OrgRegno = $_POST["regno"];
                                                            $_State = $_POST["state"];
                                                            $_Region = $_POST["region"];
                                                            $_District = $_POST["district"];
                                                            $_Tehsil = $_POST["tehsil"];
                                                            $_Landmark = $_POST["landmark"];
                                                            $_Road = $_POST["road"];
                                                            $_PinCode = $_POST["pincode"];                                                           
                                                            $_OrgEstDate = $_POST["estdate"];
                                                            $_docproof = $_POST["docproof"];
                                                            $_doctype = $_POST["doctype"];
                                                            $_OrgCountry = $_POST["country"];
                                                            $_PanNo = $_POST["panno"];
                                                            
                                                            $_AreaType = $_POST["areatype"];
                                                            $_MunicipalType = $_POST["municipaltype"];
                                                            $_WardNo = $_POST["wardno"];                                                           
                                                            $_Police = $_POST["police"];
                                                            $_MunicipalName = $_POST["municipalname"];
                                                            $_Village = $_POST["village"];
                                                            $_Gram = $_POST["gram"];
                                                            $_Panchayat = $_POST["panchayat"];
                                                            ///                                                  $_Genid = $_POST["txtGenerateId"];


                                                            $response = $emp->Add($_OrgAOType, $_OrgEmail, $_OrgMobile, $_OrgName, $_OrgType, $_OrgRegno, 
                                                                    $_State, $_Region, $_District, $_Tehsil, $_Landmark, $_Road, $_PinCode, $_OrgEstDate, 
                                                                    $_OrgCountry, $_docproof, $_doctype, $_PanNo, $_AreaType, $_MunicipalType, $_WardNo, $_Police,
                                                                    $_MunicipalName, $_Village, $_Gram, $_Panchayat);
                                                            echo $response[0];
                                                        } else {
                                                            echo "Inavalid Entry1";
                                                        }
                                                    } else {
                                                        echo "Inavalid Entry2";
                                                    }
                                                } else {
                                                    echo "Inavalid Entry3";
                                                }
                                            } else {
                                                echo "Inavalid Entry4";
                                            }
                                        } else {
                                            echo "Inavalid Entry5";
                                        }
                                    } else {
                                        echo "Inavalid Entry6";
                                    }
                                } else {
                                    echo "Inavalid Entry7";
                                }
                            } else {
                                echo "Inavalid Entry8";
                            }
                        } else {
                            echo "Inavalid Entry9";
                        }
                    } else {
                        echo "Inavalid Entry11";
                    }
                } else {
                    echo "Inavalid Entry12";
                }
            } else {
                echo "Inavalid Entry13";
            }
        } else {
            echo "Inavalid Entry14";
        }
    } else {
        echo "Inavalid Entry15";
    }
}


if ($_action == "FILLStatus") {
    $response = $emp->FILLStatus();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Org_Status'] . ">" . $_Row['Org_Status'] . "</option>";
    }
}

if ($_action == "FILLDistrict") {
    $response = $emp->FILLDistrict($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['District_Code'] . ">" . $_Row['District_Name'] . "</option>";
    }
}

if ($_action == "ShowDetails") {
    $response = $emp->GetAll($_POST['role'], $_POST['status']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>Organization Name</th>";
    echo "<th>Organization Registration No</th>";
    echo "<th>Organization Type</th>";
    echo "<th>Organization District</th>";
    echo "<th>Organization Tehsil</th>";
    echo "<th>Organization Email</th>";
    echo "<th>Organization Mobile</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_RegistrationNo']) . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Type']) . "</td>";
        echo "<td>" . $_Row['Organization_District'] . "</td>";
        echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";
        echo "<td>" . $_Row['Org_Email'] . "</td>";
        echo "<td>" . $_Row['Org_Mobile'] . "</td>";

        if ($_Row['Org_Status'] == 'Pending') {
            echo "<td> <a href='frmorgprocess.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='Approve/Reject'/></a>"
            . "</td>";
        } else if ($_Row['Org_Status'] == 'Approved') {
            echo "<td>"
            . "<input type='button' name='Approved' id='Approved' class='btn btn-primary' value='Approved'/>"
            . "</td>";
        } else {
            echo "<td>"
            . "<input type='button' name='Rejected' id='Rejected' class='btn btn-primary' value='Rejected By RM'/>"
            . "</td>";
        }

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        if($_Row['Org_Role']== '14')
        {
            $_role = 'RSP';
        }
        elseif($_Row['Org_Role']== '15')
        {
            $_role = 'IT-GK';
        }
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "doctype" => $_Row['Organization_DocType'],
            "role" => $_role,
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            "district" => $_Row['Organization_District'],
            "tehsil" => $_Row['Organization_Tehsil'],
            "street" => $_Row['Organization_Street'],
            "road" => $_Row['Organization_Road'],
            "orgdoc" => $_Row['Organization_ScanDoc']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "UPDATE") {
    $_OrgCode = $_POST["orgcode"];
    $_Status = $_POST["status"];
    $_Remark = $_POST["remark"];
    if ($_Status == "Reject") {
        $response = $emp->UpdateOrgMaster($_OrgCode, $_Status, $_Remark);
        echo $response[0];
    } elseif ($_Status == "Approve") {
        $response = $emp->UpdateToCreateLogin($_OrgCode, $_Status);
        echo $response[0];
    }
}

if ($_action == "FILLPanchayat") {
    $response = $emp->GetAllPanchayat($_actionvalue);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Block_Code'] . ">" . $_Row['Block_Name'] . "</option>";
    }
}

if ($_action == "FILLGramPanchayat") {
    $response = $emp->GetAllGramPanchayat($_actionvalue);
    echo "<option value='00' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['GP_Code'] . ">" . $_Row['GP_Name'] . "</option>";
    }
	echo "<option value='0'>Others</option>";
}



    if ($_action == "FILLPanchayat_NEW") {
        $response = $emp->GetAllPanchayat($_actionvalue);
        echo "<option value='' selected='selected'>Select </option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['Block_Code'] . ">" . $_Row['Block_Name'] . "</option>";
        }
    }

    if ($_action == "FILLGramPanchayat_NEW") {
        $response = $emp->GetAllGramPanchayat($_actionvalue);
        echo "<option value='' selected='selected'>Select </option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['GP_Code'] . ">" . $_Row['GP_Name'] . "</option>";
        }
        echo "<option value='0'>Others</option>";
    }