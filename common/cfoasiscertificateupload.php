<?php
session_start();
//$_SESSION['ImageFile'] = "";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

$_UploadDirectory3 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_minimum_certificate/';
$_UploadDirectory5 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_highest_certificate/';
$_UploadDirectory20 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_HSC_certificate/';
$st=0;
$parentid = $_POST['UploadId'];
                                if(isset($_FILES["SelectedFile3"])){
				if($_FILES["SelectedFile3"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile3"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile3"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
						  
								if (file_exists($_UploadDirectory3)) {
									if (is_writable($_UploadDirectory3)) {
										$filepath = "$_UploadDirectory3/". $parentid . '_mincertificate' . '.jpg';
										move_uploaded_file($_FILES["SelectedFile3"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
									outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Upload Folder Not Available</span>");
										}
						    				
						}	
			     }
                                }
								
								 if(isset($_FILES["SelectedFile20"])){
				if($_FILES["SelectedFile20"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile20"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile20"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
						  
								if (file_exists($_UploadDirectory20)) {
									if (is_writable($_UploadDirectory20)) {
										$filepath = "$_UploadDirectory20/". $parentid . '_HSCcertificate' . '.jpg';
										move_uploaded_file($_FILES["SelectedFile20"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
									outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Upload Folder Not Available</span>");
										}
						    				
						}	
			     }
                                }
								
				 if(isset($_FILES["SelectedFile5"])){
				 if($_FILES["SelectedFile5"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile5"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile5"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
						  
								if (file_exists($_UploadDirectory5)) {
									if (is_writable($_UploadDirectory5)) {
										$filepath = "$_UploadDirectory5/" . $parentid . '_highcertificate' . '.jpg';
										move_uploaded_file($_FILES["SelectedFile5"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
											outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Upload Folder Not Available</span>");
										}
						    				
						}	
			     }		

                                 }     
                             if($st==1){
                                  outputJSON("Upload Sucessfully.", 'success');
                             }