<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
include 'BAL/clsBlockUnblock.php';

$response = array();
$emp = new clsBlockUnblock();


if ($_action == "BLOCK") {
	//print_r($_POST);
    //$_UploadId = '0';
    $_BlockCategory = $_POST['ddlBlockCategory'];
    $_BlockDate=$_POST['txtBlockDate'];
    $_Duration = $_POST['txtBlockDuration'];
    $_DurationType = $_POST['ddlDurationType'];
    $_CenterCode = $_POST['txtCenterCode'];
    $_CenterName = $_POST['txtCenterName'];
    $_Remark = $_POST['txtRemark'];
	
	$_rval = $_POST['radiovaue'];	
	$ddlCourse = $_POST['ddlCourse'];	
	
	if($_rval == "no")
	{
    $response = $emp->Add('Block', '1', $_CenterCode, $_BlockDate, $_BlockCategory, $_Remark, $_Duration, $_DurationType);
	}
	else{
		$_CourseCode = implode(",", $ddlCourse);
		$response = $emp->AddCourseBlock('Block', '1', $_CenterCode, $_BlockDate, $_BlockCategory, $_Remark, $_Duration, $_DurationType, $_CourseCode);
	}
    echo $response[0];
}


if ($_action == "UNBLOCK") {
    $_UploadId = $_SESSION['User_LoginId'];
    $_BlockCategory = $_POST['ddlBlockCategory'];
    $_BlockDate=$_POST['txtBlockDate'];
    $_Duration = 0;
    $_DurationType = 'Days';
    $_CenterCode = $_POST['txtCenterCode'];
    $_CenterName = $_POST['txtCenterName'];
    $_Remark = $_POST['txtRemark'];
	
	$_rval = $_POST['radiovaue'];	
	$ddlCourse = $_POST['ddlCourse'];	
	
	if($_rval == "no")
	{
		$response = $emp->UnblockCenter('unblock', '2', $_CenterCode, $_BlockDate, $_BlockCategory, $_Remark, $_UploadId, 
										$_Duration, $_DurationType);
	}
	else{
		$_CourseCode = implode(",", $ddlCourse);
		$response = $emp->AddCourseUnBlock('unblock', '2', $_CenterCode, $_BlockDate, $_BlockCategory, $_Remark, $_UploadId, 
											$_Duration, $_DurationType, $_CourseCode);
	}
    echo $response[0];
}


if ($_action == "BULKBLOCK") {
    $_UploadId = $_POST['UploadId'];
    $_BlockCategory = $_POST['BlockCategory'];
    $_BlockDate=$_POST['BlockDate'];
    $_Duration = $_POST['Duration'];
    $_DurationType = $_POST['DurationType'];
    $_Query = "";	
	$_rval = $_POST['radiovaue'];	
	$ddlCourse = $_POST['coursename'];	
	
	if($_rval == "no")
	{
		if (isset($_SESSION['DataArray'])) {
			for ($i = 1; $i <= count($_SESSION['DataArray']); $i++) {
				//echo $_SESSION['DataArray'][$i][0];
				$response = $emp->Add('Block', '1', $_SESSION['DataArray'][$i][0],$_BlockDate, $_BlockCategory, 
										$_SESSION['DataArray'][$i][2], $_UploadId, $_Duration, $_DurationType);
			}
			echo $response[0];
		}
	}
	else{
		if (isset($_SESSION['DataArray'])) {
			$response1 = $emp->GetCourseCenter($ddlCourse);
			while($_Row = mysqli_fetch_array($response1[2]))
			{
				for ($i = 1; $i <= count($_SESSION['DataArray']); $i++) {
				   if($_Row['Courseitgk_ITGK'] == $_SESSION['DataArray'][$i][0])
				   {
					$_ITGKCenter = $_Row['Courseitgk_ITGK'];
					$response = $emp->BulkCourseBlock('Block', '1', $_SESSION['DataArray'][$i][0],$_BlockDate, 
													$_BlockCategory, $_SESSION['DataArray'][$i][2], $_UploadId, 
													$_Duration, $_DurationType, $_ITGKCenter, $ddlCourse);
				   }
				}
			}
			echo $response[0];
		}
	}
}

if ($_action == "BULKUNBLOCK") {
    $_UploadId = $_POST['UploadId'];
    $_BlockCategory = $_POST['BlockCategory'];
    $_BlockDate=$_POST['BlockDate'];
    $_Duration = 0;
    $_DurationType = 'Days';
    $_Query = "";
	
	$_rval = $_POST['radiovaue'];	
	$ddlCourse = $_POST['coursename'];
	//print_r($_POST);
    if($_rval == "no")
	{
		if (isset($_SESSION['DataArray'])) {
        for ($i = 1; $i <= count($_SESSION['DataArray']); $i++) {
            //echo $_SESSION['DataArray'][$i][0];
            $response = $emp->BULKUNBLOCK('unblock', '2', $_SESSION['DataArray'][$i][0], $_BlockDate, $_BlockCategory, $_SESSION['DataArray'][$i][2], $_UploadId, $_Duration, $_DurationType);
			}
				echo $response[0];
		}
	}
	else{
		if (isset($_SESSION['DataArray'])) {
			$response2 = $emp->GetCourseCenter($ddlCourse);
			while($_Row = mysqli_fetch_array($response2[2]))
			{
				for ($i = 1; $i <= count($_SESSION['DataArray']); $i++) {
				   if($_Row['Courseitgk_ITGK'] == $_SESSION['DataArray'][$i][0])
				   {
					$_ITGKCenter = $_Row['Courseitgk_ITGK'];
					$response = $emp->BulkCourseUnBlock('unblock', '2', $_SESSION['DataArray'][$i][0],$_BlockDate,
					$_BlockCategory, $_SESSION['DataArray'][$i][2], $_UploadId, $_Duration, $_DurationType, $_ITGKCenter, 
					$ddlCourse);
				   }
				}
			}
				echo $response[0];
		}
	}	
}

if ($_action == "GENERATEID") {
    //echo "call";
    $response = $emp->GenerateUploadID();
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['UploadId'];
}

if ($_action == "GETCourse") {
    //echo "call";
    $response = $emp->GETCourse($_POST['CenterCode']);
	echo "<option value='' selected='selected'>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Courseitgk_Code'] . "'>" . $_Row['Courseitgk_Course'] . "</option>";
    }    
}

if ($_action == "GETUnBlockCourse") {
    //echo "call";
    $response = $emp->GETBlockCourse($_POST['CenterCode']);
	echo "<option value='' selected='selected'>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Courseitgk_Code'] . "'>" . $_Row['Courseitgk_Course'] . "</option>";
    }    
}

if ($_action == "GETBulkUnBlockCourse") {
    //echo "call";
    $response = $emp->GETUnBlockCourse();
	echo "<option value='' selected='selected'>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Courseitgk_Course'] . "'>" . $_Row['Courseitgk_Course'] . "</option>";
    }    
}


if ($_action == "GETBulkBlockCourse") {
    //echo "call";
    $response = $emp->GETBulkBlockCourse();
	echo "<option value='' selected='selected'>Select Course</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Courseitgk_Course'] . "'>" . $_Row['Courseitgk_Course'] . "</option>";
    }    
}


if ($_action == "GETCENTERDETAIL") {    
    $response = $emp->GetCenterDetail($_POST['CenterCode']);
	$CenterDetail = array();
    //$_i = 0;   
       $_row = mysqli_fetch_array($response[2]);
	 
            $CenterDetail = array("CenterName" => $_row['CenterName'],
                "Tehsil" => $_row['Tehsil_Name'],
                "District"=> $_row['District_Name'],
                "Mobile" => $_row['User_MobileNo'],
                
                "SP"=>$_row['SPName']);
            //$_i = $_i + 1;
   
    echo json_encode($CenterDetail);
  
}


if ($_action == "GETCENTERDETAIL1") {   
    $response = $emp->GetCenterDetail($_POST['CenterCode']);
	  $CenterDetail = array();
        $_row = mysqli_fetch_array($response[2]);
       
            $CenterDetail=array("CenterName" => $_row['CenterName'],
                "Tehsil" => $_row['Tehsil_Name'],
                "District"=> $_row['District_Name'],
                "Mobile" => $_row['User_MobileNo'],
                 "SP"=>$_row['SPName']);
            echo json_encode($CenterDetail);       
    
}


if ($_action == "FILLUNBLOCKCENTER") {
    $response = $emp->GetUnBlockCenter();
    echo "<option value='' selected='selected'>Select Center</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['centercode'] . "'>" . $_Row['centercode'] . "</option>";
    }
}
if ($_action == "FILLBLOCKCATEGORY") {
    $response = $emp->GetBlockCategory();
    echo "<option value='' selected='selected'>Select Category</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['blockcategories'] . "'>" . $_Row['blockcategories'] . "</option>";
    }
}


if ($_action == "FILLUNBLOCKCATEGORY") {
    $response = $emp->GetUnBlockCategory();
    echo "<option value=''>Select Category</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['categories'] . "'>" . $_Row['categories'] . "</option>";
    }
}

if ($_action == "CENTERWISE") {
    
    $response=$emp->GetCenterDetailReport($_POST['CenterCode']);
	$html="";
    $_centerdetail=  mysqli_fetch_array($response[2]);
    
    $html.="<table border='0' cellpedding='0' cellspacing='0' width='100%' style='margin-top:20px; font-size:13px !important;' class='table table-striped table-bordered'>";
    $html.="<tr>";
    $html.="<td>";
    $html.="Center Code : " . $_centerdetail['CenterCode'];
    $html.="</td>";
    $html.="<td>";
    $html.="Center Name : " . $_centerdetail['CenterName'];
    $html.="</td>";
    $html.="</tr>";
    $html.="<tr>";
    $html.="<td>";
    $html.="Tehsil : " . $_centerdetail['Tehsil_Name'];
    $html.="</td>";
    $html.="<td>";
    $html.="District : " . $_centerdetail['District_Name'];
    $html.="</td>";
    $html.="</tr>";
    $html.="<tr>";
    $html.="<td>";
    $html.="Mobile : " . $_centerdetail['User_MobileNo'];
    $html.="</td>";
    $html.="<td>";
    $html.="Email : " . $_centerdetail['User_EmailId'];
    $html.="</td>";
    $html.="</tr>";
    $html.="<tr>";
    $html.="<td>";
    $html.="Service Provider Name : " . $_centerdetail['RSPName'];
    $html.="</td>";
    $html.="</tr>";
     $html.="</table>";
    
    $response = $emp->GetCenterWiseReport($_POST['CenterCode']);
    $html.="<tr>";
    $html.="<td colspan='2'>";
	$html.= "<div class='table-responsive'>";
    $html.= "<table id='example' border='0' cellpedding='0' cellspacing='0' style='margin-top:20px;' class='table table-striped table-bordered'>";
    $html.="<thead>";
    $html.="<tr>";
	
    $html.="<th>";
    $html.='Event Date';
    $html.="</th>";
	
    $html.="<th>";
    $html.='Activity Date';
    $html.="</th>";
	
    $html.="<th>";	
    $html.='Block Type';
    $html.="</th>";
	
	$html.="<th>";
    $html.='Activity';
    $html.="</th>";
	
    $html.="<th>";
    $html.='Reason';
    $html.="</th>";
    $html.="<th>";
    $html.='Remark';
    $html.="</th>";
    $html.="<th>";
    $html.='Duration';
    $html.="</th>";
    $html.="<th>";
    $html.='Upload ID';
    $html.="</th>";
    $html.="</tr>";
    $html.="</thead>";
    $html.="<tbody>";
    while($row=  mysqli_fetch_array($response[2]))
    {
        $html.="<tr class='odd gradeX'>";
        $html.="<td>";
        $html.=$row['eventdate'];
        $html.="</td>";
        $html.="<td>";
        $html.=$row['activitydate'];
        $html.="</td>";
		
		$html.="<td>";
        $html.=$row['blocktype'];
        $html.="</td>";
		
        $html.="<td>";
        $html.=$row['activity'];
        $html.="</td>";
		
        $html.="<td>";
        $html.=$row['activityreason'];
        $html.="</td>";
        $html.="<td>";
        $html.=$row['remark'];
        $html.="</td>";
        $html.="<td>";
        $html.=$row['duration'] . " " . $row['durationtype'];
        $html.="</td>";
        $html.="<td>";
        $html.=($row['UploadID']!='0' ? "<a href=upload/BulkBlockCenter/" . $row['UploadID'] . ".xls target=_blank>" . $row['UploadID'] . "</a>" : "" );
        $html.="</td>";
        $html.="</tr>";
    }
    $html.="</tbody>";
    $html.="</table>";
	$html.= "</div>";
    $html.="</td>";
   
    echo $html;
}

if ($_action == "STATUSWISE") {
	//print_r($_POST)
	$html="";
    $response = $emp->GetStatusWise($_POST['Status']);
    //$html.="<tr>";
   // $html.="<td colspan='2'>";
    $html.= "<div class='table-responsive'>";
    $html.= "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    $html.="<thead>";
    $html.="<tr>";
    $html.="<th>";
    $html.='Center Code';
    $html.="</th>";
    $html.="<th>";
    $html.='Center Name';
    $html.="</th>";
	$html.="<th>";
    $html.='Event Date';
    $html.="</th>";
    $html.="<th>";
    $html.='Email Id';
    $html.="</th>";
    $html.="<th>";
    $html.='Mobile No';
    $html.="</th>";
    $html.="<th>";
    $html.='Tehsil';
    $html.="</th>";
    $html.="<th>";
    $html.='District';
    $html.="</th>";
    $html.="<th>";
    $html.='Service Provider';
    $html.="</th>";
    $html.="<th>";
    $html.='Remark';
    $html.="</th>";
    $html.="</tr>";
    $html.="</thead>";
    $html.="<tbody>";
    while($row=  mysqli_fetch_array($response[2]))
    {
        $html.="<tr class='odd gradeX'>";
        $html.="<td>";
        $html.=$row['CenterCode'];
        $html.="</td>";
        $html.="<td>";
        $html.=$row['CenterName'];
        $html.="</td>";
        $html.="<td>";
        $html.=date("d M Y", strtotime($row['activitydate']));
        $html.="</td>";
		$html.="<td>";
        $html.=$row['User_EmailId'];
        $html.="</td>";
         $html.="<td>";
        $html.=$row['User_MobileNo'];
        $html.="</td>";
        $html.="<td>";
        $html.=$row['Tehsil_Name'];
        $html.="</td>";
        $html.="<td>";
        $html.=$row['District_Name'];
        $html.="</td>";
        $html.="<td>";
        $html.=$row['RSPName'];
        $html.="</td>";
        if($row['User_Status'] == '1'){
        $html.="<td>";
        $html.="Not Blocked";
        $html.="</td>";
        }else{
        $html.="<td>";
        $html.=$row['remark'];
        $html.="</td>"; 
        }
        $html.="</tr>";
    }
    $html.="</tbody>";
    $html.="</table>";
    
	$html.= "</div>";
    echo $html;
}

?>
