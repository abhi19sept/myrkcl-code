<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsEoiPaymentRpt.php';

$response = array();
$emp = new clsEoiPaymentRpt();

if ($_action == "SHOW") {
    if (isset($_POST["startdate"])) {
        if (isset($_POST["enddate"])) {
			$sdate = date_format(date_create($_POST["startdate"]),"Y-m-d").' 00:00:00';
			$edate = date_format(date_create($_POST["enddate"]),"Y-m-d").' 23:59:59';

            $response = $emp->Show($sdate, $edate);
            //$html = "";
            //$_centerdetail=  mysqli_fetch_array($response[2]);
            //$response = $emp->GetCenterWiseReport($_POST['CenterCode']);
            echo "<div class='table-responsive'>";
            echo  "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th> S No.</th>";
            echo "<th> Center Code</th>";
            echo "<th> Course Name </th>";
            echo "<th> EOI Name </th>";
            echo "<th> Payment Account Name</th>";
            echo "<th> Amount </th>";
            echo "<th> TrnasactionID </th>";
           // echo "<th> Payment Type </th>";
            echo "<th> Payment Account Email </th>";
            echo "<th> Payment Status</th>";
			echo "<th> Date</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
             $_Count = 1;
            while ($row = mysqli_fetch_array($response[2])) {
				 $Edate = date("d-m-Y", strtotime($row['EOI_Transaction_timestamp']));
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                               
                echo "<td>" . $row['EOI_Transaction_CenterCode'] . "</td>";
                
                echo "<td>" . $row['Courseitgk_Course'] . "</td>";
                
                echo "<td>" . $row['EOI_Name'] . "</td>";
               
                echo "<td>" . strtoupper($row['EOI_Transaction_Fname']) . "</td>";
              
                echo "<td>" . $row['EOI_Transaction_Amount'] . "</td>";
              
                echo "<td>" . $row['EOI_Transaction_Txtid'] . "</td>";
               
                //echo "<td>" . $row['EOI_Transaction_ProdInfo'] . "</td>";
              
                echo "<td>" . $row['EOI_Transaction_Email'] . "</td>";
                             
                echo "<td>" .$row['EOI_Transaction_Status']. "</td>";
				
				 echo "<td>" .$Edate. "</td>";
                echo "</tr>";
                 $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
            //echo $html;
        }
    }
} 

if ($_action == "SHOWEOIAPPLY") {
    if (isset($_POST["startdate"])) {
        if (isset($_POST["enddate"])) {
			$sdate = date_format(date_create($_POST["startdate"]),"Y-m-d").' 00:00:00';
			$edate = date_format(date_create($_POST["enddate"]),"Y-m-d").' 23:59:59';
			
            $response = $emp->Showeoiapply($sdate, $edate, $_POST["course"], $_POST["courseeoi"]);
            //$html = "";
            //$_centerdetail=  mysqli_fetch_array($response[2]);
            //$response = $emp->GetCenterWiseReport($_POST['CenterCode']);
            echo "<div class='table-responsive'>";
            echo  "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th> S No.</th>";
            echo "<th> Center Code</th>";
            echo "<th> Course Name </th>";
            echo "<th> EOI Name</th>";
            echo "<th> Status</th>";
            echo "<th> Date</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
             $_Count = 1;
            while ($row = mysqli_fetch_array($response[2])) {
                $Tdate = date("d-m-Y", strtotime($row['Courseitgk_timestamp']));
                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                               
                echo "<td>" . $row['Courseitgk_ITGK'] . "</td>";
                
                echo "<td>" . $row['Courseitgk_Course'] . "</td>";
               
                //echo "<td>" . $row['EOI_Transaction_Fname'] . "</td>";
              
                //echo "<td>" . $row['EOI_Transaction_Amount'] . "</td>";
              
                //echo "<td>" . $row['EOI_Transaction_Txtid'] . "</td>";
               
                echo "<td>" . $row['EOI_Name'] . "</td>";
              
                //echo "<td>" . $row['EOI_Transaction_Email'] . "</td>";
                if($row['EOI_Fee_Confirm'] == '1')
                {
					if($row['Courseitgk_TranRefNo'] == 'PayByWithoutFee')
					{
						 echo "<td> Payment Without Fee </td>";
					}
					else{
						 echo "<td> Payment Successful </td>";
					}
               
                }
                else
                {
                     echo "<td> Applied Successful</td>";
                }
                echo "<td>" .$Tdate. "</td>";
                echo "</tr>";
                 $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
            //echo $html;
        }
    }
} 
?>

