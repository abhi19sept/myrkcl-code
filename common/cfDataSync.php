<?php

require 'commonFunction.php';
require 'BAL/clsDataSync.php';
require 'cflog.php';
$common = new DataSync();

$common->$_action();

class DataSync {

    public function __construct() {
        ini_set("memory_limit", "-1");
        ini_set("max_execution_time", 0);
        ini_set("max_input_vars", 10000);
        set_time_limit(0);
        //error_reporting(-1);
        //$this->BASE_ILEARN = "http://localhost/toc/";
        $this->BASE_ILEARN = "http://49.50.65.36/";
        $this->ILEEARN_FILE_PATH = "uploads/jsonfile/"; // path for ilearn server
        $this->BASE_SERVER_FILE = "../upload/jsonfile/"; // path for same/myrkcl server
        $this->BAL = new clsDataSync();

    }

    /**
     * Total STUDENT COUNT AS PER EXAM AND BETCH BY I LEARN DATABASE USEING CURL REQUESRT
     */
    public function ExamBetchCountIlearn() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post = filter_input_array(INPUT_POST);
            $batches = implode($post['ddlBatch'], ',');
            $data_json = json_encode(["FunctionName" => "GetLearnerCountSynch", "Batches" => $batches]);
            //$curl_request = curlPost("http://49.50.65.36/webservice/allFunctions.php", $data_json);
            $curl_request = $this->curlPost($this->BASE_ILEARN . "webservice/allFunctions.php", $data_json);
            if ($curl_request['status'] == '200') {
                echo $curl_request['success'];
            } else {
                echo "0";
            }
        }
    }

    /**
     * ON PAGE LOAD SHOW AL EXAM EVENT
     */
    public function FILLEXAMEVENT() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $response = $this->BAL->GetExamEvent();
            echo "<option value=''>Select Exam Event</option>";
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<option value=" . $_Row['Affilate_Event'] . ">" . $_Row['Event_Name'] . "</option>";
            }
        }
    }

    /**
     * GETALL BETCH EXAM 
     */
    public function FILLREEXAMBATCH() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post = filter_input_array(INPUT_POST);
            $response = $this->BAL->GetReexamBatch($post['examevent']);
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<option value=" . $_Row['Batch_Code'] . ">(" . $_Row['Course_Name'] . ")" . $_Row['Batch_Name'] . "</option>";
            }
        }
    }
    /*
     * ALL BETCH MAKE STATUS Y
     */

    public function updstatus() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post = filter_input_array(INPUT_POST);
            $batches = implode($post['ddlBatch'], ',');
            $data_json = json_encode(["FunctionName" => "UpdateStatusLearnerSynch", "Batches" => $batches]);
            //  $curl_request = curlPost("http://49.50.65.36/webservice/allFunctions.php", $data_json);
            $curl_request = $this->curlPost($this->BASE_ILEARN . "webservice/allFunctions.php", $data_json);
            if ($curl_request['status'] == '200') {
                echo $curl_request['success'];
            } else {
                echo "0";
            }
        }
    }

        public function IlearnDataSync() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
             $ccode = (isset($_POST['ddlCourse'])) ? $_POST['ddlCourse'] : Null;
    $batches = (isset($_POST['ddlBatch'])) ? implode(",", $_POST["ddlBatch"]) : Null;
  m_log("Batch Code Passed : ". $batches." ".date('Y-m-d h:i:s'));
            // CURL REQUEST TO MAKE JSON DATA
            $result = $this->IlearnRequestData($ccode,$batches);

            if ($result['status'] == '200') {
                // insert
                $insert = $this->DataInsertFinalAssment($result['success']);
                if ($insert['status'] == '200') {
                     m_log("Ileaen_StatusUpdate Call : ". $result['success']." ".date('Y-m-d h:i:s'));
                  //  $this->Ileaen_StatusUpdate($result['success']);
					 $this->Ileaen_StatusUpdate($result['success'],$batches);
                    echo json_encode($insert);
                } else {
                    echo json_encode($insert);
                }
            } else {
                echo json_encode($result);
            }
        }
    }

    private function IlearnRequestData($ccode,$batches) {
        $data_json = json_encode(["action" => "ileandatasycn","ccode" => $ccode,"batches" => trim($batches)]);
        m_log("CURL request Post : ". $data_json." ".date('Y-m-d h:i:s'));
        $curlresp = $this->curlPost($this->BASE_ILEARN . "common/cfiLearnDatasync.php?action=ileandatasycn", $data_json);
        m_log("CURL response : ". json_encode($curlresp)." ".date('Y-m-d h:i:s'));

        return $curlresp;
    }


    private function DataInsertFinalAssment($fileName) {
        if ($str1 = file_get_contents($this->BASE_ILEARN . $this->ILEEARN_FILE_PATH . $fileName)) {
            $str = $this->encrypt_decrypt('decrypt', $str1);
            $json = json_decode($str, true);
            $r = "";
            $co = count($json);
            $i = 1;
            if (count($json) > 0) {
                foreach ($json as $value) {
                    // FOR MULTIPLE INSERTION MAKE STRING

                    $r .= '("' . $value['Fresult_learner'] . '","' . $value['Fresult_itgk'] . '","' . $value['Fresult_rsp'] . '","' . $value['Fresult_district'] . '","' . $value['Fresult_batch'] . '","' . $value['Fresult_course'] . '","' . $value['Fresult_noofassessment'] . '","' . $value['Fresult_obtainmarks'] . '","' . $value['Fresult_timestamp'] . '","' . $value['IsNewMRecord'] . '","' . $value['Re_exam_status'] . '")';
                    if ($co != $i) {
                        $r .= ',';
                    }
                    $i++;
                }
                // DATA INSERTION
                $result = $this->BAL->Add_final_result2($r);
                return ['status' => '200', 'success' => $result['2']];
            } else {
                 return ['status' => '404', 'error' => 'Data not Exist'];
            }
        } else {
            return ['status' => '404', 'error' => 'file not read ' . $fileName];
        }
    }
/**
    private function Ileaen_StatusUpdate($file_name) {
        $payload = json_encode(['filename' => $file_name]);
        return $this->curlPost($this->BASE_ILEARN . "common/cfiLearnDatasync.php?action=ileanstatusupdate", $payload);
    }
**/

    private function Ileaen_StatusUpdate($file_name,$batches) {
        $payload = json_encode(['filename' => $file_name, 'batch' => $batches]);
        return $this->curlPost($this->BASE_ILEARN . "common/cfiLearnDatasync.php?action=ileanstatusupdate", $payload);
    }
    public function chkScorecount() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            //error_reporting(0);
            $batcharray = $_POST['ddlLearnScoreBatch'];
            $batches = implode($batcharray, ',');
            $response = $this->BAL->GetLearnerCountSynch($batches);

            $total_N = "0";
            $response_N = $this->BAL->GetLearnerCountSynch_N_Number($batches);
            if ($response_N[0] == "Success") {
                while ($Result_N = mysqli_fetch_array($response_N[2])) {
                    $total_N = $Result_N["LearnerCount"];
                }
            }
            $response_P = $this->BAL->GetLearnerCountSynch_P_Number($batches);
            if ($response_P[0] == "Success") {
                while ($Result_P = mysqli_fetch_array($response_P[2])) {
                    $total_P = $Result_P["LearnerCountP"];
                }
            }

            $response_return = [];
            if ($response[0] == "Success") {
                while ($Result = mysqli_fetch_array($response[2])) {
                    $response_return["error"] = FALSE;
                    $nstatuscount = $Result["LearnerCount"];
                    $response_return["LearnerCount"] = array("NsatusCount" => $total_N, "PsatusCount" => $total_P, "TotalCount" => $nstatuscount);
                }
            } else {
                $response_return["error"] = TRUE;
                $response_return["error_msg"] = "No Count Availabe";
            }
            echo json_encode($response_return);
        }
    }

    private function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = '1212121211';
        $secret_iv = 'axwepghrtycgarvt'; // ONLY 16 BYTES
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    public function updstatusScore() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post = filter_input_array(INPUT_POST);
            $batches = implode($post['ddlLearnScoreBatch'], ',');
            $response = $this->BAL->UpdateStatusLearnerSynch($batches);
            //print_r($response); die;
            if ($response[0] == "Successfully Done") {
                $response1["error"] = FALSE;
                $response1["LearnerUpdated"] = $response['2'];
                echo json_encode($response1);
            } else {
                $response1["error"] = TRUE;
                $response1["error_msg"] = "No Status Update";
                echo json_encode($response1);
            }
        }
    }
    public function updstatusTbl() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post = filter_input_array(INPUT_POST);

            $batches = $post['batches'];
            $tblname = $post['tblname'];
            $response = $this->BAL->UpdateStatusTable($tblname, $batches);
            //print_r($response); die;
            if ($response[0] == "Successfully Done") {
                $response1["error"] = FALSE;
                $response1["LearnerUpdated"] = $response['2'];
                echo json_encode($response1);
            } else {
                $response1["error"] = TRUE;
                $response1["error_msg"] = "No Status Update";
                echo json_encode($response1);
            }
        }
    }

     public function Rkcl_AssihnDataSync() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post = filter_input_array(INPUT_POST);
            $batches = implode($post['ddlLearnScoreBatch'], ',');
            $examid = $post['ddlLearnScoreEvent'];
            $response = $this->BAL->AssmenttoScore($batches,$examid);
            //print_r( $response);
            if($response[0] == "Successfully Done"){
                 echo json_encode(['status' => '200', 'success' => $response['2']]);
            }
            else{
                 echo json_encode(['status' => '404', 'error' => $response['0']]);
            }
           
        }
    }
    public function Rkcl_AssihnDataSyncFinal() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        
            $response = $this->BAL->AssmenttoScoreFinal();
            //print_r( $response);
            if($response[0] == "Successfully Done"){
                 echo json_encode(['status' => '200', 'success' => $response['2']]);
            }
            else{
                 echo json_encode(['status' => '404', 'error' => $response['0']]);
            }
           
        }
    }

    private function LearnScoreFileCreate() {
        $response = $this->BAL->GetAll();
        $resultset = [];
        if (!empty($response)) {
            while ($_Row = mysqli_fetch_array($response[2], MYSQLI_ASSOC)) {
                $resultset[] = $_Row;
            }
            $json_string = $this->encrypt_decrypt('encrypt', json_encode($resultset));
            $file = 'tbl_learner_score.txt';
            $file_path = $this->BASE_SERVER_FILE . $file;
            // CREATE JSON FILE
            if (!(file_put_contents($file_path, $json_string))) {
                echo 'file cant created';
            }
            $result = [
                'status' => '200',
                'result' => $file
            ];
        } else {
            $result = ['status' => '404', 'error' => 'file not found'];
        }
        return $result;
    }

    private function InsertLearnerScore($file_name) {
        if ($str1 = file_get_contents($this->BASE_SERVER_FILE . $file_name)) {
            $str = $this->encrypt_decrypt('decrypt', $str1);
            $json = json_decode($str, true);
            $r = "";
            $co = count($json);
            $i = 1;
            if (count($json) > 0) {
                foreach ($json as $value) {
                    // FOR MULTIPLE INSERTION MAKE STRING
                    $scper = ($value['Fresult_obtainmarks']) * (100 / 30);
                    $scper = number_format((float) $scper, 2, '.', '');
                    $r .= '("' . $value['Admission_Code'] . '","' . $value['Fresult_learner'] . '","' . $value['Fresult_obtainmarks'] . '","' . $scper . '","' . $value['Fresult_itgk'] . '","' . $value['Fresult_timestamp'] . '")';
                    if ($co != $i) {
                        $r .= ',';
                    }
                    $i++;
                }
                // DATA INSERTION

                $result = $this->BAL->Add_final_result($r);
                return ['status' => '200', 'success' => $result['2']];
            } else {
                return ['status' => '404', 'error' => 'Data not Exist'];
            }
        } else {
            return ['status' => '404', 'error' => 'file not read ' . $file_name];
        }
    }

    private function assessementfinalresultChangeStatus($file_name) {
        $str1 = file_get_contents($this->BASE_SERVER_FILE . $file_name);

        $str = $this->encrypt_decrypt('decrypt', $str1);
        $json = json_decode($str, true);

        $result_id = [];
        if (count($json) > 0) {
            foreach ($json as $value) {
                $result_id[] = $value['Fresult_id'];
            }
            $ids = join("', '", $result_id);

            if ($ids) {
                $this->BAL->update_result($ids);
            }
        }
    }
    
    private function curlPost($url, $data_json = NULL) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 300,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data_json,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: 5c22b1ed-b568-1c45-3d0d-ebff096e4cda"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);

    if ($err) {
        $data = ['status' => '404', 'error' => $err];
    } else {
        $data = ['status' => '200', 'success' => $response];
    }
    return $data;
}

  // public function __destruct() {
  //       $filename = $base;
  //       unlink($filename);
  //   }


    /**
     * Total table list I LEARN DATABASE USEING CURL REQUESRT
     */
    public function loadilearndbtbl() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                     
            $data_json = json_encode(["FunctionName" => "Getilearndbtbl"]);
            //$curl_request = curlPost("http://49.50.65.36/webservice/allFunctions.php", $data_json);
            $curl_request = $this->curlPost($this->BASE_ILEARN . "webservice/allFunctions.php", $data_json);
            if ($curl_request['status'] == '200') {
                echo $curl_request['success'];
            } else {
                echo "0";
            }
        }
    }


        /**
     * Total Record COUNT in selected tbl in MYRKCL DATABASE 
     */
    public function ChkCountIlearn() {
        // if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        //     $post = filter_input_array(INPUT_POST);
        //     $batches = $post['tblname'];
        //     $data_json = json_encode(["FunctionName" => "ChkCountIlearn", "tblname" => $batches]);

        //     $curl_request = $this->curlPost($this->BASE_ILEARN . "webservice/allFunctions.php", $data_json);
        //     if ($curl_request['status'] == '200') {
        //         echo $curl_request['success'];
        //     } else {
        //         echo "0";
        //     }
        // }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $post = filter_input_array(INPUT_POST);
            $Tblname  = $post['tblname'];
            $response = $this->BAL->GettblCountSynch($Tblname);

            $total_N = "0";
            $response_N = $this->BAL->GettblCountSynchNstatus($Tblname);
            if ($response_N[0] == "Success") {
                while ($Result_N = mysqli_fetch_array($response_N[2])) {
                    $total_N = $Result_N["RecordCountN"];
                }
            }
            $response_P = $this->BAL->GettblCountSynchPstatus($Tblname);
            if ($response_P[0] == "Success") {
                while ($Result_P = mysqli_fetch_array($response_P[2])) {
                    $total_P = $Result_P["RecordCountP"];
                }
            }

            $response_return = [];
            if ($response[0] == "Success") {
                while ($Result = mysqli_fetch_array($response[2])) {
                    $response_return["error"] = FALSE;
                    $nstatuscount = $Result["RecordCount"];
                    $response_return["RecordCount"] = array("NsatusCount" => $total_N, "PsatusCount" => $total_P, "TotalCount" => $nstatuscount);
                }
            } else {
                $response_return["error"] = TRUE;
                $response_return["error_msg"] = "No Count Availabe";
            }
            echo json_encode($response_return);

        }
    }

    public function myrkcltoilearnDataSync() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            m_log("ajax request post accpect".date('Y-m-d h:i:s'));
            $post = filter_input_array(INPUT_POST);
            $Tblname  = $post['tblname'];
            $batches = $post['batches'];
            $result = $this->myrkcltoilearnFileCreate($Tblname, $batches);
            if ($result['status'] == '200') {
            m_log("sen request to i learn".date('Y-m-d h:i:s'));
                $insert = $this->InsertiLearnTbl($result['result']);
              // print_r($insert); die;
                if ($insert['status'] == '200') {
                    if($insert['success'] == "success"){
                       
                        $this->BAL->TblStatusUpdtoN($post['tblname'], $batches);
                         m_log("change status to N".date('Y-m-d h:i:s'));
                     echo json_encode($insert);    
                    } else {
                        m_log("curl request success not data not insert".date('Y-m-d h:i:s'));
                       // echo $insert['error'] ;
                        echo json_encode($insert); 
                    }
                    
                } else {

                    echo json_encode($insert);
                }
            } else {
                echo json_encode($result);
            }
        }
    }

    private function myrkcltoilearnFileCreate($Tblname, $batches) {
        $response = $this->BAL->GetAllMyrkclTbl($Tblname, $batches);
 
        $resultset = [];
        if (!empty($response)) {
            while ($_Row = mysqli_fetch_array($response[2], MYSQLI_ASSOC)) {
              //  $resultset[] = $_Row;
			   $resultset[] = array_map("utf8_encode", $_Row);
			  
            }
            $json_string = $this->encrypt_decrypt('encrypt', json_encode($resultset));
            $file = $Tblname.'.txt';
            $file_path = $this->BASE_SERVER_FILE . $file;
            // CREATE JSON FILE
            if (!(file_put_contents($file_path, $json_string))) {
                echo 'file cant created';
                 m_log("file cnot created".date('Y-m-d h:i:s'));
            }
              m_log("encrypt file create".date('Y-m-d h:i:s'));
            $result = [
                'status' => '200',
                'result' => $file
            ];
        } else {
            $result = ['status' => '404', 'error' => 'file not found'];
            m_log("file not found".date('Y-m-d h:i:s'));
        }
        return $result;
    }

    private function InsertiLearnTbl($file_name) {

        $data_json = json_encode(["filen" => $file_name]);
        $abc= $this->curlPost($this->BASE_ILEARN . "common/cfiLearnDatasync.php?action=myrkcldatatoilearnsync", $data_json);
        m_log("curl response".date('Y-m-d h:i:s').json_encode($abc));
       return $abc;


    }
    public function FILLADMBATCH() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post = filter_input_array(INPUT_POST);
            $response = $this->BAL->GetAdmBatch($post['tblname']);
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<option value=" . $_Row['Batch_Code'] . ">(" . $_Row['Course_Name'] . ")" . $_Row['Batch_Name'] . "</option>";
            }
        }
    }

    public function ChkCountIlearnTblAdm() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post = filter_input_array(INPUT_POST);
           // print_r($post);
           // die;
            $batches = $post['batches'];
             $Tblname  = $post['tblname'];
            $response = $this->BAL->GettblCountSynch($Tblname, $batches);
            
            $total_N = "0";
            $response_N = $this->BAL->GettblCountSynchNstatus($Tblname, $batches);
            if ($response_N[0] == "Success") {
                while ($Result_N = mysqli_fetch_array($response_N[2])) {
                    $total_N = $Result_N["RecordCountN"];
                }
            }
            $response_P = $this->BAL->GettblCountSynchPstatus($Tblname, $batches);
            if ($response_P[0] == "Success") {
                while ($Result_P = mysqli_fetch_array($response_P[2])) {
                    $total_P = $Result_P["RecordCountP"];
                }
            }

            $response_return = [];
            if ($response[0] == "Success") {
                while ($Result = mysqli_fetch_array($response[2])) {
                    $response_return["error"] = FALSE;
                    $nstatuscount = $Result["RecordCount"];
                    $response_return["RecordCount"] = array("NsatusCount" => $total_N, "PsatusCount" => $total_P, "TotalCount" => $nstatuscount);
                }
            } else {
                $response_return["error"] = TRUE;
                $response_return["error_msg"] = "No Count Availabe";
            }
            echo json_encode($response_return);
        }
    }
	 public function FILLCOURSE() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post = filter_input_array(INPUT_POST);
            $response = $this->BAL->FILLCourse();
            echo "<option value='0'>Select Course</option>";
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
            }
        }
    }
        /**
     * GETALL BETCH EXAM 
     */
    public function FILLCOURSEBATCH() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post = filter_input_array(INPUT_POST);
            $response = $this->BAL->GetBatch($post['ccode']);
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
            }
        }
    }
}
