<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsGovApproved.php';

$response = array();
$emp = new clsGovApproved();

	if ($_action == "ShowDetails")		
	{
		//print_r($_POST);			 

		$response = $emp->GetAll(trim($_POST['code']),trim($_POST['lcode']),trim($_POST['eid']));
		
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>S No.</th>";
		echo "<th>Learner Code</th>";
		echo "<th>Learner Name</th>";
		echo "<th>Father/Husband Name</th>";
		echo "<th>District</th>";
		echo "<th>Marks</th>";
		echo "<th>Application Date</th>";
		echo "<th>Application Type</th>";
		echo "<th>Reimbursement Amount</th>";		
		echo "<th>Action</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		if($response[0]=='Success'){
			$_Count = 1;	
			while ($_Row = mysqli_fetch_array($response[2])) {
			echo "<tr>";
			echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['learnercode'] . "</td>";
			echo "<td>" . strtoupper($_Row['fname']) . "</td>";
			echo "<td>" . strtoupper($_Row['faname']) . "</td>";
			echo "<td>" . strtoupper($_Row['ldistrictname']) . "</td>"; 
			echo "<td>" . $_Row['exammarks'] . "</td>";
			echo "<td>" . $_Row['applicationdate'] . "</td>";
			echo "<td>" . strtoupper($_Row['applicationtype']) . "</td>";
			if($_Row['trnpending'] == '3'){
					echo "<td>" . 0 . "</td>";
				}
			else{
				echo "<td>" . $_Row['trnamount'] . "</td>"; 
			}
			
			if($_Row['trnpending'] == '3')
				{
					echo "<td> <a href='frmgovformprocess.php?code=" . $_Row['learnercode'] . "&Mode=Edit'>"
						. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='Edit'/></a>"						
						. "</td>";
				}
			else if($_Row['trnpending'] == '0') 
				{
					echo "<td> <a href='frmgovformprocess.php?code=" . $_Row['learnercode'] . "&Mode=Edited'>"
					. "<input type='button' name='Processed' id='Processed' class='btn btn-primary' value='Processed by RKCL'/></a>"
					. "</td>";
				}
			else if($_Row['trnpending'] == '4') 
				{
					echo "<td>"
					. "<input type='button' name='Rejected' id='Rejected' class='btn btn-primary' value='Rejected by RKCL'/>"
					. "</td>";
				}
			else if($_Row['trnpending'] == '5')
				{
					echo "<td>"
					. "<input type='button' name='Sent to DOIT' id='Sent to DOIT' class='btn btn-primary' value='Sent to DOIT'/>"
					. "</td>";
				}		
			else {
					echo "<td>"
					. "<input type='button' name='Payment Reimbursed' id='Payment Reimbursed' class='btn btn-primary' value='Payment Reimbursed'/>"
					. "</td>";
			}
		
			echo "</tr>";
			$_Count++;
		}
		}
	else{
		echo "";
	}	
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
		
		
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        
        if($_Row['empdobapp']!=''){
        $dobapp=date_format(date_create($_Row['empdobapp']), "d-m-Y");
        }else{
        $dobapp='';    
        }
        
        
        $_DataTable[$_i] = array("LearnerCode" => $_Row['learnercode'],
            "lname" => strtoupper($_Row['fname']),
            "fname" => strtoupper($_Row['faname']),
            "email" => $_Row['lemailid'],
            "itgk" => $_Row['Govemp_ITGK_Code'],			
            "mobile" => $_Row['lmobileno'],
            "district" => strtoupper($_Row['ldistrictname']),
			"marks" => $_Row['exammarks'],
            "accountno" => $_Row['empaccountno'],
            "ifsc" => strtoupper($_Row['gifsccode']),
			"gpf" => $_Row['empgpfno'],
			"dname" => strtoupper($_Row['gdname']),
			"designation" => strtoupper($_Row['designation']),
			"birth" => $_Row['attach2'],
			"receipt" => $_Row['attach1'],
			"offadd" => strtoupper($_Row['officeaddress']),
			"eid" => $_Row['empid'],
			"Fee" => $_Row['fee'],
			"BatchId" => $_Row['batchid'],
			"BatchName" => $_Row['Batch_Name'],
			"Incentive" => $_Row['incentive'],
			"TotalAmt" => $_Row['trnamount'],
			"dob" => date_format(date_create($_Row['empdob']), "d-m-Y"),
                        "dobapp" => $dobapp,
			"cheque" => $_Row['attach4'],
			"dpletter" => $_Row['attach5'],
			"certificate" => $_Row['attach3']);        
        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}

if ($_action == "GetRegisteredData") {
    $response = $emp->GetRegisteredDatabyCode($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("itgk" => $_Row['Admission_ITGK_Code'],
            "lname" => strtoupper($_Row['Admission_Name']),
            "fname" => strtoupper($_Row['Admission_Fname']),
            "dob" => date_format(date_create($_Row['Admission_DOB']), "d-m-Y"),
            "mobile" => $_Row['Admission_Mobile'],			
            "course" => $_Row['Course_Name'],
            "batch" => strtoupper($_Row['Batch_Name']));        
        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}


if ($_action == "FILLAPPROVEDSTATUS") {
    $response = $emp->FILLAPPROVEDSTATUS();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}

if ($_action == "GETALLLOT") {
    $response = $emp->GETALLLOT();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['lotid'] . ">" . $_Row['lotname'] . "</option>";
    }
}

if ($_action == "FILLStatus") {
    $response = $emp->FILLStatus();
    //echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
		echo $_Row['cstatus'];
        //echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}

if ($_action == "UPDATE") {	
	$_LearnerCode = $_POST["learnercode"];
	$_ProcessStatus = $_POST["status"];
	$_IFSCNO = $_POST["ifsc"];
	$_AccountNo = $_POST["accountno"];
	$_GPFNo = $_POST["gpf"];
	$_LOT = $_POST["lot"]; 
	$_BatchId = $_POST["batchid"];
	$_Fee = $_POST["fee"];
	$_Incentive = $_POST["incentive"];
	$_TotalAmt = $_POST["totalamt"];
	$_Remark = $_POST["remark"];
	$_Address = $_POST["address"];
	$_Mobile = $_POST["mobile"];
	$_ITGK = $_POST["itgk"];
	$_DOB = date_format(date_create($_POST["dob"]), "Y-m-d");
	
	$response = $emp->Update($_LearnerCode, $_ProcessStatus, $_IFSCNO, $_AccountNo, $_GPFNo, $_LOT, $_BatchId, $_Fee, $_Incentive, $_TotalAmt, $_Remark, $_Address, $_Mobile, $_DOB, $_ITGK);
	if($response[0]=='Successfully Updated') {
		echo $response[0];
	}
			
}

if ($_action == "FILLGovFee") {
		if($_POST['mode'] == 'Edited') {
			 $response = $emp->FILLGovFee($_POST['code']);
			 $_DataTable = array();
				$_i = 0;
				$_Row = mysqli_fetch_array($response[2]);
				
				$attempt = $emp->FILLGovFeeAttempted($_POST['code']);
				$_Row1 = mysqli_fetch_array($attempt[2]);
				$incentive = $_Row1['incentive'];
				$fee = $_Row1['fee'];
				//$_attempt = $_Row1['trnamount'];
				
				$_DataTable[$_i] = array("Fee" => $fee,
						"BatchId" => $_Row['Admission_Batch'],
						"BatchName" => strtoupper($_Row['Batch_Name']),
						"Incentive" => $incentive);        
					$_i = $_i + 1;
				
				echo json_encode($_DataTable);  
		}
		
		
		else {
			 $response = $emp->FILLGovFee($_POST['code']);			 
			 $_DataTable = array();
				$_i = 0;
				$_Row = mysqli_fetch_array($response[2]);
				
				$attempt = $emp->FILLGovAttempt($_POST['code']);
				$incentive="";
				  $_Row1 = mysqli_fetch_array($attempt[2]);
						$_attempt = $_Row1['aattempt'];
						 if($_attempt=='1'){
							$incentives = ($_Row['Admission_Fee']*25/100);
							$incentive = round($incentives);
						 }
						 else{
							 $incentive=0;
						 }				  
				  
					$_DataTable[$_i] = array("Fee" => $_Row['Admission_Fee'],
						"BatchId" => $_Row['Admission_Batch'],
						"BatchName" => strtoupper($_Row['Batch_Name']),
						"Incentive" => $incentive);        
					$_i = $_i + 1;
				
					echo json_encode($_DataTable);   
			}     
}



if ($_action == "GetPendingApplicationData")		
	{
		//print_r($_POST);			 

		$response = $emp->GetPendingApplicationData();
		
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='examples' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>S No.</th>";
		echo "<th>Learner Code</th>";
		echo "<th>Learner Name</th>";
		echo "<th>Father/Husband Name</th>";
		echo "<th>District</th>";
		echo "<th>Marks</th>";
		echo "<th>Application Date</th>";
		echo "<th>Application Type</th>";
		echo "<th>Reimbursement Amount</th>";		
		echo "<th>Action</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		if($response[0]=='Success'){
			$_Count = 1;	
			while ($_Row = mysqli_fetch_array($response[2])) {
			echo "<tr>";
			echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['learnercode'] . "</td>";
			echo "<td>" . strtoupper($_Row['fname']) . "</td>";
			echo "<td>" . strtoupper($_Row['faname']) . "</td>";
			echo "<td>" . strtoupper($_Row['ldistrictname']) . "</td>"; 
			echo "<td>" . $_Row['exammarks'] . "</td>";
			echo "<td>" . $_Row['applicationdate'] . "</td>";
			echo "<td>" . strtoupper($_Row['applicationtype']) . "</td>";
			if($_Row['trnpending'] == '3'){
					echo "<td>" . 0 . "</td>";
				}
			else{
				echo "<td>" . $_Row['trnamount'] . "</td>"; 
			}
			
			if($_Row['trnpending'] == '3')
				{
					echo "<td> <a href='frmgovformprocess.php?code=" . $_Row['learnercode'] . "&Mode=Edit'>"
						. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='Edit'/></a>"						
						. "</td>";
				}
			else if($_Row['trnpending'] == '0') 
				{
					echo "<td> <a href='frmgovformprocess.php?code=" . $_Row['learnercode'] . "&Mode=Edited'>"
					. "<input type='button' name='Processed' id='Processed' class='btn btn-primary' value='Processed by RKCL'/></a>"
					. "</td>";
				}
			else if($_Row['trnpending'] == '4') 
				{
					echo "<td>"
					. "<input type='button' name='Rejected' id='Rejected' class='btn btn-primary' value='Rejected by RKCL'/>"
					. "</td>";
				}
			else if($_Row['trnpending'] == '5')
				{
					echo "<td>"
					. "<input type='button' name='Sent to DOIT' id='Sent to DOIT' class='btn btn-primary' value='Sent to DOIT'/>"
					. "</td>";
				}		
			else {
					echo "<td>"
					. "<input type='button' name='Payment Reimbursed' id='Payment Reimbursed' class='btn btn-primary' value='Payment Reimbursed'/>"
					. "</td>";
			}
		
			echo "</tr>";
			$_Count++;
		}
		}
	else{
		echo "";
	}	
    echo "</tbody>";
    echo "</table>";
	echo "</div>";		
}


if ($_action == "ValidateData") {	
	$_LearnerCode = $_POST["learnercode"];	
	$_AccountNo = $_POST["accountno"];	
	$_Mobile = $_POST["mobile"];
	
	 $_DOB = date_format(date_create($_POST["dob"]), "d-m-Y");	
	$responseLearnerBatch = $emp->GetLearnerBatch($_POST['learnercode']);
                    $_RowLearnerBatch = mysqli_fetch_array($responseLearnerBatch[2],true);
                    $_RowLearnerBatch['Batch_Name'];
                    $Batch_StartDate=$_RowLearnerBatch['Batch_StartDate'];
                    $batchYear = explode("-", $Batch_StartDate);
                    
                    
                     $batchCalculateYear='01-01-'.$batchYear[0];///echo "<br>";
                    $cage=calculateAge($_DOB, $batchCalculateYear);///echo "<br>";
                    $pieces = explode("-", $cage);
                    if($pieces[2] < 55)
                    {
                        $response = $emp->ValidateLearnerDetails($_LearnerCode, $_AccountNo, $_Mobile, $_DOB);
									if($response[0]=='No Record Found') {
										echo "1";
									}
					}
                    else
                    {
                       echo "2";
                    }
	
}
function calculateAge($date1,$date2)
        {
            // date2 should be greater then to date1
            $date1 = date_create($date1); //date of birth
            //creating a date object
            $date2 = date_create($date2); //current date

            $diff12 = date_diff($date2, $date1); 
                //accesing days
            $days = $diff12->d;
             //accesing months
            $months = $diff12->m;
             //accesing years
            $years = $diff12->y;

            return   $days . '-' . $months . '-' . $years;
        }   
