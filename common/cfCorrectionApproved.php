<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCorrectionApproved.php';
require 'DAL/sendsms.php';

require 'DAL/upload_ftp_doc.php';
$_ObjFTPConnection = new ftpConnection();

$response = array();
$emp = new clsCorrectionApproved();

	if ($_action == "ShowDetails")
	{
        $response = $emp->GetAll($_POST['code'],$_POST['status']);
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>S No.</th>";
		echo "<th>Learner Code</th>";
		echo "<th>Learner Name</th>";
		echo "<th>Father Name</th>";
		echo "<th>D.O.B</th>";
		//echo "<th>Application For</th>";
		echo "<th>Exam Event</th>";
		echo "<th>Payment Status</th>";
		echo "<th>Marks</th>";				
		echo "<th>History</th>";				
		//echo "<th>Action</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		
			$_Count = 1;	
			while ($_Row = mysqli_fetch_array($response[2])) {
			echo "<tr>";
			echo "<td>" . $_Count . "</td>";
			//echo "<td>" . $_Row['lcode'] . "</td>";
			echo "<td> ". "<input type='button' id='".$_Row['lcode']."' style='text-decoration:none;outline: none !important;' 
							name='learnerdash' id='learnerdash' class='learnerdashboard btn btn-link btn-cons' value='" . $_Row['lcode'] . "'/>"
				. "</td>";
			echo "<td>" . strtoupper($_Row['cfname']) . "</td>";
			echo "<td>" . strtoupper($_Row['cfaname']) . "</td>";
			echo "<td>" . $_Row['dob'] . "</td>"; 
			//echo "<td>" . strtoupper($_Row['applicationfor']) . "</td>"; 
			echo "<td>" . strtoupper($_Row['exameventname']) . "</td>"; 
			  if($_Row['Correction_Payment_Status'] == '0'){
				  $status = 'Pending';
				  echo "<td>" . $status . "</td>";
			  }
			  else {
				  $status = 'Confirmed';
				  echo "<td>" . $status . "</td>";
			  }
			 
			echo "<td>" . $_Row['totalmarks'] . "</td>";
			
			echo "<td> ". "<input type='button' data-toggle='modal' data-target='#GetHistory' id='".$_Row['lcode']."' 
							name='Edit' id='Edit' class='btn btn-primary GetHistory' value='View History'/>"
				. "</td>";					
			
		
		
			echo "</tr>";
			$_Count++;
		}
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "EDIT") {
	global  $_ObjFTPConnection;
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
	$details= $_ObjFTPConnection->ftpdetails();
    while ($_Row = mysqli_fetch_array($response[2])) {
		
			$CorrectionPhoto = $_Row['photo'];
			$photopath='correction_photo/';
            $pic =  file_get_contents($details.$photopath.$CorrectionPhoto);
            $correctionimg = base64_encode($pic);
            $correctionphoto =  "<img id='ftpimg' src='data:image/png;base64, ".$correctionimg."' alt='Red dot' width='90px' height='113px'/>";

		if($_Row['attach1']==""){
			$marksheet="<img id='ftpimg' src='images/not-found.png' alt='Red dot' width='100%' height='500px'/>";
		}
		else{
			$CorrectionMarksheet = $_Row['attach1'];
			$photopath='correction_marksheet/';
            $pic =  file_get_contents($details.$photopath.$CorrectionMarksheet);
            $correctionimg = base64_encode($pic);
            $marksheet =  "<img id='ftpimg' src='data:image/png;base64, ".$correctionimg."' alt='Red dot' width='100%' height='500px'/>";
		}
		
		if($_Row['attach2']==""){
			$certificate="<img id='ftpimg' src='images/not-found.png' alt='Red dot' width='100%' height='500px'/>";
		}
		else{
			$Correctioncertificate = $_Row['attach2'];
			$photopath='correction_certificate/';
            $pic =  file_get_contents($details.$photopath.$Correctioncertificate);
            $correctionimg = base64_encode($pic);
            $certificate =  "<img id='ftpimg' src='data:image/png;base64, ".$correctionimg."' alt='Red dot' width='100%' height='500px'/>";
		}
		
		if($_Row['attach4']==""){
			$provisional="<img id='ftpimg' src='images/not-found.png' alt='Red dot' width='100%' height='500px'/>";
		}
		else{
			$Correctionprovisional = $_Row['attach4'];
			$photopath='correction_provisional/';
            $pic4 =  file_get_contents($details.$photopath.$Correctionprovisional);
            $correctionimg4 = base64_encode($pic4);
            $provisional =  "<img id='ftpimg' src='data:image/png;base64, ".$correctionimg4."' alt='Red dot' width='100%' height='500px'/>";
		}
		
		if($_Row['attach5']==""){
			 $application =  "<img id='ftpimg' src='images/not-found.png' alt='Red dot' width='100%' height='500px'/>";
		}
		else{
			$Correctionapplication = $_Row['attach5'];
			$photopath='correction_application/';
            $pic5 =  file_get_contents($details.$photopath.$Correctionapplication);
            $correctionimg5 = base64_encode($pic5);
            $application =  "<img id='ftpimg' src='data:image/png;base64, ".$correctionimg5."' alt='Red dot' width='100%' height='500px'/>";
		}
		
		if($_Row['attach6']==""){
			$affidavit="<img id='ftpimg' src='images/not-found.png' alt='Red dot' width='100%' height='500px'/>";
		}
		else{
			$Correctionaffidavit = $_Row['attach6'];
			$photopath='correction_affidavit/';
            $pic6 =  file_get_contents($details.$photopath.$Correctionaffidavit);
            $correctionimg6 = base64_encode($pic6);
            $affidavit =  "<img id='ftpimg' src='data:image/png;base64, ".$correctionimg6."' alt='Red dot' width='100%' height='500px'/>";
		}

		$cf="";
			if($_Row['application_type']=='0'){
				$cf='NA';
			}
			else{
				$arr=explode(",",$_Row['application_type']);
				$countarr=count($arr);
					for($i=0;$i<$countarr;$i++){
						if($arr[$i]=='1'){
							$cf .="Name Correction, ";
						}
						else if($arr[$i]=='2'){
							$cf.="Father/Husband Name Correction, ";
						}
						else if($arr[$i]=='3'){
							$cf.="Photo Correction, ";
						}
						else{
							$cf.="Not Available, ";
						}
					}				
				$cf=rtrim($cf,", ");	
			}
        $_DataTable[$_i] = array("LearnerCode" => $_Row['lcode'],
            "lname" => strtoupper($_Row['cfname']),
            "fname" => strtoupper($_Row['cfaname']),
            "applicationfor" => strtoupper($_Row['applicationfor']),
            "correctionfor" => $cf,
            "exameventname" => strtoupper($_Row['exameventname']),            
            "txnid" => $_Row['Correction_TranRefNo'],            
			"marks" => $_Row['totalmarks'],
			"photo" => $correctionphoto,
			"certificate" => $certificate,
			"marksheet" => $marksheet,
			"provisional" => $provisional,
			"application" => $application,
			"affidavit" => $affidavit);        
        $_i = $_i + 1;
		
    }
	
    echo json_encode($_DataTable);
}

if ($_action == "GetOldValues") {
    $response = $emp->GetOldValues($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("AdmissionCode" => $_Row['Admission_Code'],
            "lname" => $_Row['Admission_Name'],
            "fname" => $_Row['Admission_Fname'],
			"lmobile" => $_Row['mobile']);        
        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}


if ($_action == "FILLAPPROVEDSTATUS") {
    $response = $emp->FILLAPPROVEDSTATUS();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}

if ($_action == "GETALLLOT") {
    $response = $emp->GETALLLOT($_POST['status']);
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['lotid'] . ">" . $_Row['lotname'] . "</option>";
    }
}

if ($_action == "FILLStatus") {
    $response = $emp->FILLStatus();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['capprovalid'] . ">" . $_Row['cstatus'] . "</option>";
    }
}

if ($_action == "UPDATE") {	
	$_Cid = $_POST["cid"];
	$_ProcessStatus = $_POST["status"];
	$_LOT = $_POST["lot"];                        
	$_Remark = $_POST["remark"];
	$_EventName = $_POST["eventname"];
	$_Marks = $_POST['marks'];
	$_Txnid = $_POST['txnid'];
	$_Lcode = $_POST['lcode'];
	$_Mobile = $_POST['mobile'];
	if($_EventName == "") {
		$response = $emp->UpdateToProcess($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_Marks, $_Txnid, $_Lcode, $_Mobile);
		echo $response[0];	
	}
	else {	
		$response = $emp->UpdateToProcessWithEvent($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_EventName, $_Marks, $_Txnid, $_Lcode, 
		$_Mobile);
		echo $response[0];
	}
}


if ($_action == "UpdateRejectedLearner") {
	$_Cid = $_POST["cid"];
	$_ProcessStatus = $_POST["status"];
	$_LOT = $_POST["lot"];                        
	$_Remark = $_POST["remark"];
	$_EventName = $_POST["eventname"];
	$_Marks = $_POST['marks'];	
	$_LName = $_POST["lname"];
	$_Fname = $_POST['fname'];
	$_Mobile = $_POST['mobile'];
	
	if($_EventName == "") {
		$response = $emp->UpdateToProcessRejected($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_Marks, $_LName, $_Fname, $_Mobile);
		echo $response[0];	
	}
	else {	
		$response = $emp->UpdateToProcessRejectedWithEvent($_Cid, $_ProcessStatus, $_LOT, $_Remark, $_EventName, $_Marks, $_LName, $_Fname, $_Mobile);
		echo $response[0];
	}
}

if ($_action == "GetLearnerHistory") {
	$learnercode = $_REQUEST['learnercode'];
	$response = $emp->GetLearnerHistory($learnercode);
		 $_DataTable = "";

		echo "<div class='table-responsive' style='margin-top:10px'>";
   
		echo "<table id='examples' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th style='15%'>S No.</th>";
		echo "<th style='15%'>ITGK-Code</th>";
		echo "<th style='15%'>LearnerCode</th>";
		echo "<th style='15%'>Learner Name</th>";
		echo "<th style='15%'>Father/Husband Name</th>";
		echo "<th style='15%'>Mobile No.</th>";
		echo "<th style='15%'>Application For</th>";
		echo "<th style='15%'>Application Date</th>";
		echo "<th style='15%'>Application Status</th>";
		echo "<th style='15%'>Lot</th>";
		echo "<th style='15%'>Action</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$_Count = 1;
		$co = mysqli_num_rows($response[2]);
		if ($co) {
			while ($_Row = mysqli_fetch_array($response[2])){
				echo "<tr class='odd gradeX'>";
				echo "<td>" . $_Count . "</td>";
				echo "<td>" . $_Row['Correction_ITGK_Code'] . "</td>";
				echo "<td>" . $_Row['lcode'] . "</td>";
				echo "<td>" . strtoupper($_Row['name']) . "</td>";
				echo "<td>" . strtoupper($_Row['fname']) . "</td>";
				echo "<td>" . $_Row['mobile'] . "</td>";
				echo "<td>" . strtoupper($_Row['applicationfor']) . "</td>";
				echo "<td>" . date("d-m-Y",strtotime($_Row['applicationdate'])) . "</td>";
				echo "<td>" . strtoupper($_Row['cstatus']) . "</td>";
					if($_Row['dispatchstatus'] !=3){
						$responses = $emp->GetLotName($_Row['cid']);
							$_Rows = mysqli_fetch_array($responses[2]);
							$lot=strtoupper($_Rows['lotname']);
					}
					else{
						$lot='NA';
					}
				echo "<td>" . $lot . "</td>";
				
					if($_Row['dispatchstatus'] == '3')
				{
					echo "<td> <a href='frmcorrectionprocess.php?code=" . $_Row['lcode'] . "&cid=" . $_Row['cid'] . "&Mode=Edit'>"
						. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='Edit'/></a>"						
						. "</td>";
				}
			else if($_Row['dispatchstatus'] == '4')
				{
					echo "<td> <a href='frmcorrectionprocess.php?code=" . $_Row['lcode'] . "&cid=" . $_Row['cid'] . "&Mode=Reject'>"
						. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='Edit'/></a>"						
						. "</td>";
				}
			else if($_Row['dispatchstatus'] == '2')
				{
					echo "<td>"
					. "<input type='button' name='Delivered' id='Delivered' class='btn btn-primary' value='Delivered to DLC'/>"
					. "</td>";
				}
		    else if($_Row['dispatchstatus'] == '0') 
				{
					echo "<td>"
					. "<input type='button' name='Processed' id='Processed' class='btn btn-primary' value='Processed by RKCL'/>"
					. "</td>";
				}
			else
				{
					echo "<td>"
					. "<input type='button' name='Sent' id='Sent' class='btn btn-primary' value='Sent to VMOU for printing'/>"
					. "</td>";
				}
				
				echo "</tr>";
				$_Count++;
			}
			echo "</tbody>";
			echo "</table>";
			echo "</div>";
		}
		
}