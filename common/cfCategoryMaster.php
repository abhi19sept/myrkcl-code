<?php

/* 
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsCategoryMaster.php';

$response = array();
$emp = new clsCategoryMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        $_FunctionName = $_POST["name"];
        $_Status=$_POST["status"];

        $response = $emp->Add($_FunctionName,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_FunctionName = $_POST["name"];
        $_Status=$_POST["status"];
        $_Code=$_POST["code"];
        $response = $emp->Update($_Code,$_FunctionName,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);
    //echo $_actionvalue;
    //print_r($response);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("CategoryCode" => $_Row['Category_Code'],
            "CategoryName" => $_Row['Category_Name'],
            "Status"=>$_Row['Category_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'>Name</th>";
    echo "<th style='30%'>Status</th>";
    echo "<th style='20%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Category_Name'] . "</td>";
         echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmcategorymaster.php?code=" . $_Row['Category_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmcategorymaster.php?code=" . $_Row['Category_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='' >Select Category</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Category_Code'] . ">" . $_Row['Category_Name'] . "</option>";
    }
}