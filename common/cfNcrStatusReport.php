<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsNcrStatusReport.php';

    $response = array();
    $emp = new clsNcrStatusReport();

    if ($_action == "DETAILS") {
		
	$response = $emp->GetITGK_NCR_Details();
       
	$_DataTable = "";
		
	echo "<div class='table-responsive'>";
	echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
	echo "<thead>";
	echo "<tr>";
	echo "<th style='5%'>S No.</th>";
        echo "<th style='5%'>AO CODE</th>";
	echo "<th style='5%'>ITGK-CODE</th>";
        echo "<th style='25%'>ITGK Name</th>";
         echo "<th style='25%'>ITGK Email</th>";
          echo "<th style='25%'>ITGK Mobile</th>";
           echo "<th style='25%'>ITGK Address</th>";
            echo "<th style='25%'>ITGK District</th>";
             echo "<th style='25%'>ITGK Tehsil</th>";
        echo "<th style='15%'>SP Code</th>";
        echo "<th style='15%'>SP Name</th>";
        echo "<th style='15%'>Application Date</th>";
        echo "<th style='15%'>Owner Name</th>";
        echo "<th style='15%'>Faculty Name</th>";
        echo "<th style='15%'>Payment Status</th>";
        echo "<th style='5%'>Login Approval Date</th>";
        echo "<th style='5%'>Final Verification Date</th>";
        echo "<th style='5%'>Course Authorization Date</th>";
//	echo "<th style='5%'>Admission Count</th>";
//        echo "<th style='5%'>Shortfall admission count as per area condition</th>";
//        echo "<th style='5%'>Penalty  amount as per area condition</th>";

	echo "</tr>";
	echo "</thead>";
	echo "<tbody>";
	if($response[0] == 'Success') {
	$_Count = 1;
	while ($_Row = mysqli_fetch_array($response[2])) {

	echo "<tr class='odd gradeX'>";
	echo "<td>" . $_Count . "</td>";
	echo "<td>" . $_Row['Org_Ack'] . "</td>";
        echo "<td>" . ($_Row['ITGKCODE'] ? $_Row['ITGKCODE'] : 'Login Approval Pending') . "</td>";
	echo "<td>" . ($_Row['Organization_Name'] ? strtoupper($_Row['Organization_Name']) : 'NA') . "</td>";
        echo "<td>" . ($_Row['Org_Email'] ? strtoupper($_Row['Org_Email']) : 'NA') . "</td>";
        echo "<td>" . ($_Row['Org_Mobile'] ? $_Row['Org_Mobile'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['Organization_Road'] ? strtoupper($_Row['Organization_Road']) : 'NA') . "</td>";
        echo "<td>" . ($_Row['Organization_District_Name'] ? strtoupper($_Row['Organization_District_Name']) : 'NA') . "</td>";
        echo "<td>" . ($_Row['Organization_Tehsil'] ? strtoupper($_Row['Organization_Tehsil']) : 'NA') . "</td>";
        echo "<td>" . strtoupper($_Row['Org_RspLoginId']) . "</td>";
        echo "<td>" . strtoupper($_Row['RSP_Name']) . "</td>";
        echo "<td>" . ($_Row['Org_NCR_App_Date'] ? $_Row['Org_NCR_App_Date'] : 'NA') . "</td>";
        echo "<td>" . ($_Row['UserProfile_FirstName'] ? strtoupper($_Row['UserProfile_FirstName']) : 'Owner Details Pending') . "</td>";
        if($_Row['ITGKCODE']){
        $response1 = $emp->Get_Faculty_Details($_Row['ITGKCODE']);
        $_Row1 = mysqli_fetch_array($response1[2]);
        echo "<td>" . ($_Row1['Staff_Name'] ? $_Row1['Staff_Name'] : 'Faculty Details Not Available') . "</td>";
        } else {
        echo "<td>Faculty Details Not Available</td>";    
        }
        echo "<td>" . ($_Row['Org_PayStatus'] ? 'Payment Confirmed' : 'Payment Pending') . "</td>";
        
        echo "<td>" . ($_Row['Org_NCR_Login_Approval_Date'] ? $_Row['Org_NCR_Login_Approval_Date'] : 'Login Approval Pending') . "</td>";
        echo "<td>" . ($_Row['Org_NCR_Final_Approval_Date'] ? $_Row['Org_NCR_Final_Approval_Date'] : 'SP Verification Pending') . "</td>";
        echo "<td>" . ($_Row['Org_NCR_Course_Allocation_Date'] ? $_Row['Org_NCR_Course_Allocation_Date'] : 'Course Authorization Pending') . "</td>";
	echo "</tr>";
	$_Count++;
	}
	echo "</tbody>";
	echo "</table>";
	echo "</div>";
	}
    }
	 
//if ($_action == "FillCourse") {
//    $response = $emp->GetAllCourse();	
//    echo "<option value=''>Select Courses</option>";
//    while ($_Row = mysqli_fetch_array($response[2])) {
//        echo "<option value='" . $_Row['Courseitgk_Course'] . "'>" . $_Row['Courseitgk_Course'] . "</option>";
//    }
//}
?>