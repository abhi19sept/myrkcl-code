<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include './commonFunction.php';
require 'BAL/clsNcrFinalApproval.php';

$response = array();
$emp = new clsNcrFinalApproval();


if ($_action == "APPROVE") {
    $response = $emp->GetDatabyCode($_POST['values']);
    //echo $response;
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if($co){
    while ($_Row = mysqli_fetch_array($response[2])) {
       
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "doctype" => $_Row['Organization_DocType'],
           
            "district" => $_Row['Organization_District_Name'],
            "districtcode" => $_Row['Organization_District'],
            "tehsil" => $_Row['Organization_Tehsil'],
            
            "lat" => $_Row['Organization_lat'],
            "long" => $_Row['Organization_long'],
           
            "road" => $_Row['Organization_Address']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
} else {
        echo "";
    }
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>AO Code</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='10%'>IT-GK Name</th>";
    echo "<th style='10%'>IT-GK Type</th>";
    echo "<th style='40%'>IT-GK Mob No</th>"; 
    echo "<th style='40%'>IT-GK Email Id</th>";
    echo "<th style='40%'>IT-GK Address</th>";
   
    echo "<th style='40%'>IT-GK District</th>";
    echo "<th style='10%'>IT-GK Tehsil</th>";
    echo "<th style='10%'>IT-GK Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['User_Ack'] . "</td>";
            echo "<td>" . $_Row['User_LoginId'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Type'] . "</td>";
            echo "<td>" . $_Row['User_MobileNo'] . "</td>";
            echo "<td>" . $_Row['User_EmailId'] . "</td>";
            echo "<td>" . $_Row['Organization_Road'] . "</td>";
            echo "<td>" . $_Row['Organization_District_Name'] . "</td>";
            echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";
            
            if ($_SESSION['User_UserRoll'] == '14'  && $_Row['User_UserRoll'] == '15' ) {
                echo "<td> <a href='frmncrfinalprocess.php?code=" . $_Row['User_LoginId'] . "&Mode=Add&ack=" . $_Row['User_Ack'] ."'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-primary' value='Verify'/></a>"
            . "</td>";
            } 
            elseif ($_SESSION['User_UserRoll'] == '14' && $_Row['User_UserRoll'] == '7' ) {
                echo "<td>" . 'Verified' . "</td>";
            } else {
                echo "<td>" . 'NA' . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}





if ($_action == "ADD") {
    if (isset($_POST["centercode"])) {
        $_CenterCode = $_POST["centercode"];
        $_Ack = $_POST["ack"];

        $response = $emp->Upgrade($_CenterCode, $_Ack);
        echo $response[0];
    }
}

if ($_action == "BANKACCOUNT") {

    //echo "Show";
    $response = $emp->SHOWBANKACCOUNTDATA($_POST["centercode"]);

    $_DataTable = "";

   echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Bank Account Name</th>";
    echo "<th style='40%'> 	Bank Account Number</th>";
    echo "<th style='10%'> 	Bank IFSC Code</th>";
	echo "<th style='10%'> 	Bank Name</th>";
	echo "<th style='10%'> 	Bank Micr Code</th>";
	echo "<th style='10%'>  Bank Branch Name</th>";
	echo "<th style='10%'>  Account Type</th>";
	echo "<th style='10%'>  Pan Card No</th>";
	echo "<th style='10%'>  Pan Card Holder Name</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Bank_Account_Name'] . "</td>";
         echo "<td>" . $_Row['Bank_Account_Number'] . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
		 echo "<td>" . $_Row['Bank_Micr_Code'] . "</td>";
		 echo "<td>" . $_Row['Bank_Branch_Name'] . "</td>";
		 echo "<td>" . $_Row['Bank_Account_Type'] . "</td>";
		 echo "<td>" . strtoupper($_Row['Pan_No']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Pan_Name']) . "</td>";
		 
        echo "</tr>";
        $_Count++;
    }
   echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "OWNERDATA") {

    //echo "Show";
    $response = $emp->SHOWOWNERDATA($_POST["centercode"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example1' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Owner Name</th>";
    echo "<th style='20%'>Owner DOB</th>";
    echo "<th style='20%'>Owner Address</th>";
    echo "<th style='10%'>Owner Qualification</th>";
    echo "<th style='10%'>Owner Mobile No.</th>";
    echo "<th style='10%'>Owner Email ID.</th>";
   
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	 echo "</div>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['UserProfile_FirstName'] . "</td>";
         echo "<td>" . $_Row['UserProfile_DOB'] . "</td>";
         echo "<td>" . $_Row['UserProfile_Address'] . "</td>";
         echo "<td>" . $_Row['UserProfile_Qualification'] . "</td>";
         echo "<td>" . $_Row['UserProfile_Mobile'] . "</td>";
         echo "<td>" . $_Row['UserProfile_Email'] . "</td>";
        
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}

if ($_action == "HRDATA") {

    //echo "Show";
    $response = $emp->SHOWHRDETAIL($_POST["centercode"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    //echo "<th style='20%'>Staff User</th>";
    echo "<th style='10%'>Staff Name</th>";
    echo "<th style='20%'>Staff Mobile</th>";
    echo "<th style='20%'>Staff Experience</th>";
    echo "<th style='10%'>Staff Email Id</th>";
    echo "<th style='10%'>Staff User code</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
       
        // echo "<td>" . $_Row['Staff_User'] . "</td>";
         echo "<td>" . $_Row['Staff_Name'] . "</td>";
         echo "<td>" . $_Row['Staff_Mobile'] . "</td>";
         echo "<td>" . $_Row['Staff_Experience'] . "</td>";
         echo "<td>" . $_Row['Staff_Email_Id'] . "</td>";
	 echo "<td>" . $_Row['Staff_User'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "SIDATA") {

    //echo "Show";
    $response = $emp->SHOWSIDATA($_POST["centercode"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example3' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='10%'>System Type</th>";
    echo "<th style='20%'>Processor</th>";
    echo "<th style='20%'>RAM</th>";
    echo "<th style='10%'>HDD</th>";
    echo "<th style='10%'>Syatem Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	 echo "</div>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['System_Type'] . "</td>";
         echo "<td>" . $_Row['Processor_Name'] . "</td>";
         echo "<td>" . $_Row['System_RAM'] . "</td>";
         echo "<td>" . $_Row['System_HDD'] . "</td>";
		 echo "<td>" . $_Row['System_Status'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}

if ($_action == "PREMISESDATA") {

	
    $response = $emp->SHOWPREMISESDATA($_POST["centercode"]);

    $_DataTable = "";
    echo "<div class='table-responsive' style='margin-top:10px'>";
   
    echo "<table id='example4' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'> 	Premises User</th>";
    echo "<th style='30%'> Reception Type</th>";
	echo "<th style='30%'> Seating Capacity</th>";
	 echo "<th style='40%'>Premises Seperate</th>";    
    echo "<th style='40%'>Theory Area in Sq.Ft.</th>";
    echo "<th style='10%'>Lab Area in Sq.Ft.</th>";
    echo "<th style='10%'>Total Area in Sq.Ft.</th>";
	echo "<th style='30%'> Parking Facility</th>";
	echo "<th style='30%'> Ownership</th>";
	echo "<th style='30%'> Parking For</th>";
	echo "<th style='30%'> Toilet Available</th>";
	echo "<th style='30%'> Toilet Ownership </th>";
	echo "<th style='30%'>Toilet Type</th>";
	echo "<th style='30%'>Panatry</th>";
	echo "<th style='30%'>Panatry Capacity</th>";
	echo "<th style='30%'>Library Available</th>";
	echo "<th style='30%'>Library capacity</th>";
	echo "<th style='30%'>Staff Available</th>";
	echo "<th style='30%'>Staff Capacity</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
       echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Premises_User'] . "</td>";
		 echo "<td>" . $_Row['Premises_Receptiontype'] . "</td>";
         echo "<td>" . $_Row['Premises_seatingcapacity'] . "</td>";
		 echo "<td>" . ($_Row['Permises_Separate'] ? $_Row['Permises_Separate'] : 'NA' ) . "</td>";
            echo "<td>" . ($_Row['Premises_Theory_area'] ? $_Row['Premises_Theory_area'] : '0' ) . "</td>";
            echo "<td>" . ($_Row['Premises_Lab_area'] ? $_Row['Premises_Lab_area'] : '0' ) . "</td>";
            
            echo "<td>" . ($_Row['Premises_Total_area'] ? $_Row['Premises_Total_area'] : '0' ) . "</td>";
		  echo "<td>" . $_Row['Premises_parkingfacility'] . "</td>";
		   echo "<td>" . $_Row['Premises_ownership'] . "</td>";
		   echo "<td>" . $_Row['Premises_parkingfor'] . "</td>";
		   echo "<td>" . $_Row['Premises_toiletavailable'] . "</td>";
		    echo "<td>" . $_Row['Premises_ownershiptoilet'] . "</td>";
			echo "<td>" . $_Row['Premises_toilet_type'] . "</td>";
			
			echo "<td>" . $_Row['Premises_panatry'] . "</td>";
			echo "<td>" . $_Row['Premises_panatry_capacity'] . "</td>";
			echo "<td>" . $_Row['Premises_library_available'] . "</td>";
			echo "<td>" . $_Row['Premises_library_capacity'] . "</td>";
			echo "<td>" . $_Row['Premises_Staff_available'] . "</td>";
			echo "<td>" . $_Row['Premises_Staff_capacity'] . "</td>";
		
       
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "ITDATA") {

    //echo "Show";
    $response = $emp->SHOWITDATA($_POST["centercode"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example5' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>Device Name</th>";
    echo "<th style='10%'>Availablity</th>";
    echo "<th style='20%'>Make</th>";
    echo "<th style='20%'>Model</th>";
    echo "<th style='10%'>Quantity</th>";
    echo "<th style='20%'>Details</th>";
    echo "<th style='20%'>User code</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Device_Name'] . "</td>";
         echo "<td>" . $_Row['IT_Peripherals_Availablity'] . "</td>";
         echo "<td>" . $_Row['IT_Peripherals_Make'] . "</td>";
         echo "<td>" . $_Row['IT_Peripherals_Model'] . "</td>";
         echo "<td>" . $_Row['IT_Peripherals_Quantity'] . "</td>";
         echo "<td>" . $_Row['IT_Peripherals_Detail'] . "</td>";
		  echo "<td>" . $_Row['IT_UserCode'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "SPVISIT") {

    //echo "Show";
    $response = $emp->SHOWSPVISITDATA($_POST["centercode"],$_POST["visitorcode"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example6' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    //echo "<th style='20%'>SP Code</th>";
    echo "<th style='10%'>SP Name</th>";
    echo "<th style='20%'>AO Code</th>";
    echo "<th style='20%'>AO Name</th>";
    echo "<th style='10%'>Whther at Address</th>";
   // echo "<th style='20%'>AO Address</th>";
    echo "<th style='20%'>Owner Name</th>";
     echo "<th style='20%'>Owner Mob No</th>";
      echo "<th style='20%'>Faculty NAme Name</th>";
       echo "<th style='20%'>Faculty Qualification</th>";
        echo "<th style='20%'>Furniture(Yes/No)</th>";
        echo "<th style='20%'>Electrict(Yes/No)</th>";
        echo "<th style='20%'>Water(Yes/No)</th>";
        echo "<th style='20%'>Toilet(Yes/No)</th>";
        echo "<th style='20%'>Area Sepeate(Yes/No)</th>";
        echo "<th style='20%'>Theory Area</th>";
        echo "<th style='20%'>LAB Area</th>";
        echo "<th style='20%'>Total Area</th>";
        echo "<th style='20%'>No of Desktop</th>";
        echo "<th style='20%'>No of Laptop</th>";
        echo "<th style='20%'>No of N-Computing</th>";
        echo "<th style='20%'>Printer(Yes/No)</th>";
        echo "<th style='20%'>WebCAM(Yes/No)</th>";
        echo "<th style='20%'>BioMetric(Yes/No)</th>";
        echo "<th style='20%'>Internet(Yes/No)</th>";
        echo "<th style='20%'>Power Backup(Yes/No)</th>";
        echo "<th style='20%'>Infurastructure Quality</th>";
        echo "<th style='20%'>Faculty Quality</th>";
        echo "<th style='20%'>Overall Remarks</th>";
        echo "<th style='20%'>SP Official Name</th>";
        echo "<th style='20%'>SP Official Mob No</th>";
        echo "<th style='20%'>Feedback Date</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        //echo "<td>" . $_Row['NCRFeedback_User'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_SPName'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_AOCode'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_AOName'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Location'] . "</td>";
        //echo "<td>" . $_Row['NCRFeedback_Address'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_OwnerName'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_OwnerMob'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_FacultyName'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Qualification'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Furniture'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Electricity'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Water'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Toilet'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Separate'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_TheoryArea'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_LABArea'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_TotalArea'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Desktop'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Laptop'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_NComputing'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Printer'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Webcam'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Biometric'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Internet'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Backup'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_AO_Infrastructure'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_AO_Faculty_Quality'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Overall_Remark'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_SPOfficialName'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_SPOfficialMob'] . "</td>";
        echo "<td>" . $_Row['NCRFeedback_Date'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "FACULTYRESULT") {

    //echo "Show";
    $response = $emp->SHOWFACULTYRESULT($_POST["centercode"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example7' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='30%'>Faculty Name</th>";
    echo "<th style='10%'>Attempt No</th>";
    echo "<th style='20%'>Obtained Marks</th>";
    echo "<th style='20%'>Total Marks</th>";
    echo "<th style='10%'>Exam Date</th>";
    echo "<th style='20%'>DPO</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Staff_Name'] . "</td>";
         echo "<td>" . $_Row['ResExam_attempt'] . "</td>";
         echo "<td>" . $_Row['ResExam_obtainmarks'] . "</td>";
         echo "<td>" . $_Row['ResExam_totalmarks'] . "</td>";
         echo "<td>" . $_Row['ResExam_date'] . "</td>";
         echo "<td>" . $_Row['DPO'] . "</td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}