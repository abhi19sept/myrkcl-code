<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsSwitchReport.php';

$response = array();
$emp = new clsSwitchReport();


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='60%'>SP Name</th>";
    echo "<th style='60%'>IT-GK Code</th>";
    echo "<th style='60%'>IT-GK Name</th>";
    echo "<th style='40%'>IT-GK District</th>";
    echo "<th style='10%'>IT-GK Tehsil</th>";
    echo "<th style='10%'>IT-GK Mob No</th>";
    echo "<th style='10%'>IT-GK Email ID</th>";
	echo "<th style='10%'>Application Status</th>";
    
	echo "<th style='10%'>Application Date</th>";
    echo "<th style='10%'>IT-GK Remark</th>";
    echo "<th style='10%'>IT-GK Selected Reason</th>";



    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspname'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_ItgkCode'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Tehsil_Name'] . "</td>";
            echo "<td>" . $_Row['User_MobileNo'] . "</td>";
            echo "<td>" . $_Row['User_EmailId'] . "</td>";
			echo "<td>" . $_Row['Rspitgk_Status'] . "</td>";
			
			echo "<td>" . $_Row['Rspitgk_Date'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Remark'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_RspSelectReason'] . "</td>";
			
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


