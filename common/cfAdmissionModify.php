<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsAdmissionModify.php';
require '../DAL/upload_ftp_doc.php';
$response = array();
$emp = new clsAdmissionModify();
$_ObjFTPConnection = new ftpConnection();

if ($_action == "SHOWALL")
	{
        $response = $emp->GetAllForModify($_POST['batch'], $_POST['course']);
		$_DataTable = "";
global  $_ObjFTPConnection;
     echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
	echo "<th>Learner Code</th>";
    echo "<th>Learner Name</th>";
    echo "<th>Father/Husband Name</th>";
    echo "<th>D.O.B</th>";
	echo "<th>Mobile</th>";
	echo "<th>Photo</th>";
	echo "<th>Sign</th>";
	//echo "<th style='10%'>PSA Name</th>";
	//echo "<th style='10%'>DLC Name</th>";
//	echo "<th>Print Receipt</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
	$arr = explode("|", $_SESSION['User_ParentId']);
	//print_r($arr);
	$PSA_CODE = $arr[0];
	$DLC_CODE = $arr[1];
	$ftpaddress = $_ObjFTPConnection->ftpPathIp();
	$_Course = $_POST['course'];
	$_LearnerBatchNameFTP="";

                if ($_Course == 5) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP;                    

                } elseif ($_Course == 4) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

                }
                elseif ($_Course == 3) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }               
                
                elseif ($_Course == 23) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

                }   

                 elseif ($_Course == 25) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

                }
                elseif ($_Course == 28) {


                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

                }
                elseif ($_Course == 29) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

                }
                elseif ($_Course == 30) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

                }
                elseif ($_Course == 31) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

                }
                elseif ($_Course == 32) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

                }
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
		   echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" .strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        echo "<td>" . $_Row['Admission_DOB'] . "</td>"; 	
		echo "<td>" . $_Row['Admission_Mobile'] . "</td>";	

		$_LearnerBatchNameFTP2 = str_replace(' ', '_', $_Row['Batch_Name'].$_LearnerBatchNameFTP);	
		
		if($_Row['Admission_Photo']!="")
				{
					$image = $_Row['Admission_Photo'];
					echo "<td id='".$_Count.'_photo'."'>"
				. "<input type='button' name='Edit' class='btn btn-primary view_photo' batchname='".$_LearnerBatchNameFTP2."'
			id='".$image."' count='".$_Count."' value='View Photo'/>". "  
				
			</td>";
					}
			else
				{
					
					echo "<td id='".$_Count.'_photo'."'>" . '<img  alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' 
							.   "</td>";
				}
			echo "<td id='".$_Count.'_pic'."' style='display:none;'>" . "<div id='Viewphoto_".$_Count."'> </div>" . "</td>";	
			
			
			
		if($_Row['Admission_Sign']!="")
			{
				$sign = $_Row['Admission_Sign'];
				echo "<td id='".$_Count.'_sign'."'>"
				. "<input type='button' name='Edit' class='btn btn-primary view_sign' batchname='".$_LearnerBatchNameFTP2."'
			id='".$sign."' count='".$_Count."' value='View Sign'/>". "</td>";
			}
		else
				{
					echo "<td id='".$_Count.'_sign'."'>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
				}
		echo "<td id='".$_Count.'_signature'."' style='display:none;'>" . "<div id='Viewsign_".$_Count."'> </div>" . "</td>";	
		
		
		
        //echo "<td>" . $PSA_CODE . "</td>";
		//echo "<td>" . $DLC_CODE . "</td>";
    //    echo "<td><a href=javascript:window.open('PrintReceiptpdf.php?code=" . $_Row['Admission_Code'] . "&Mode=printreceipt');>"
        //       . "<img src='images/print_printer.png' alt='Edit' width='25px' height='25px'  align='center'/></a> </td> ";
				
		if($_Row['Admission_Payment_Status'] == '0')
		{
			if($_Row['IsNewRecord'] == 'Y'){
				echo "<td> <a href='frmeditlearneradmission.php?code=" . $_Row['Admission_Code'] . "&Mode=Edit'>"
					. "<img src='images/editicon.png' alt='Edit' width='30px' height='25px'/></a>  "
					 . "<a href='frmModifyAdmission.php?code=" . $_Row['Admission_Code'] . "&Mode=Delete&batchcode=" . $_Row['Admission_Batch'] . "&coursecode=".$_Row['Admission_Course']."'>"
                . "<img src='images/deleteicon.png' alt='Delete' width='30px' height='25px'/></a> "
			. "</td>";			
			}
			else{
				$status='Learning Already Activated';
				echo "<td> $status </td>";
			}			
		}
		else{
			$status='Payment Confirm';
			echo "<td> $status </td>";
		}
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	 echo "</div>";
	}
	
	
	if ($_action == "DELETE") {
    //print_r($_POST);
    $value = $_POST['values'];
    $batch = $_POST['batchcode'];
    
    $response = $emp->DeleteRecord($value,$batch);

    echo $response[0];
}

if ($_action == "ViewPhoto") {
    //print_r($_POST); die;
    $batchname = $_POST['batchname'];
	$photopath = '/admission_photo/'.$batchname.'/';
    $image = $_POST['image'];
	
    global $_ObjFTPConnection;
	
	$details= $_ObjFTPConnection->ftpdetails();
	
    $photoimage =  file_get_contents($details.$photopath.$image);
$imageData = base64_encode($photoimage);
echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='80' height='110'/>";

    
}

if ($_action == "ViewSign") {
    //print_r($_POST); die;
    $batchname = $_POST['batchname'];
	$photopath = '/admission_sign/'.$batchname.'/';
    $image = $_POST['image'];
	
    global $_ObjFTPConnection;
	
	$details= $_ObjFTPConnection->ftpdetails();
	
    $photoimage =  file_get_contents($details.$photopath.$image);
$imageData = base64_encode($photoimage);
echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='50' height='80'/>";

    
}
	
	
		
		?>
	