<?php

/*
 * Created by Mayank

 */

include './commonFunction.php';
require 'BAL/clsWcdrejectedlearnercount.php';

$response = array();
$emp = new clsWcdrejectedlearnercount();

if ($_action == "GETREJECTEDDATA") {
    //echo "Show";
		
		if($_POST['batch'] !='') {
		 
			  $response = $emp->GetAllRejected($_POST['course'],$_POST['batch']);
			  
			   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
				echo "<th>ITGK Code</th>";
				echo "<th>Eligible Applicant Count</th>";
				
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;
					$_Total = 0;
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr class='odd gradeX'>";
					echo "<td>" . $_Count . "</td>";		
					echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";
					  if($_Row['pref'] == 0) {
						echo "<td>" . $_Row['pref'] . "</td>";  
					  }
					  else {
						echo "<td><a href='frmWcdRejectedLearnerCountList.php?batch=" . $_POST['batch'] . "&itgk=" . $_Row['Oasis_Admission_Final_Preference'] . "&districtcode=" . $_Row['District_Code'] . "&mode=one' target='_blank'>"
							. "" . $_Row['pref'] . "</a></td>";  
					  }				
						echo "</tr>";
						$_Total = $_Total + $_Row['pref'];
					$_Count++;				
				}
				 echo "</tbody>";
					
						echo "<tfoot>";
						echo "<tr>";
						echo "<th >  </th>";
						echo "<th >Total Count </th>";
						echo "<th>";
						echo "$_Total";
						echo "</th>";
						echo "</tr>";
						echo "</tfoot>";
		
				 echo "</table>";
				 echo "</div>";
		 
		}
		  else {
			  echo "1";
		  }
   
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}


if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetLearnerList($_POST['mode'], $_POST['batch'], $_POST['districtcode'], $_POST['itgkcode']);

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";   
    echo "<th>Applicant Id</th>";
    echo "<th>Learner Name</th>";
    echo "<th >Father Name</th>";	
    echo "<th >Marital Status</th>";	
    echo "<th >Sub Category</th>";	
	echo "<th >Caste Category</th>";
	echo "<th >Qualification</th>";
	echo "<th >Mobile No.</th>";
	echo "<th >Status</th>";
    echo "<th >Reason</th>";
	echo "<th >Details</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response) {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";			
            echo "<td>" . $_Row['Oasis_Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_MaritalStatus'] . "</td>";
            echo "<td>" . $_Row['Oasis_Admission_Subcategory'] . "</td>";
			
			echo "<td>" . $_Row['Category_Name'] . "</td>";
			echo "<td>" . $_Row['Qualification_Name'] . "</td>";
			
			echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
			echo "<td>" . $_Row['Oasis_Admission_LearnerStatus'] . "</td>";
			echo "<td>" . $_Row['Oasis_Admission_Reason'] . "</td>";
			echo "<td> <a href='frmwcdlearnerdetails.php?code=" . $_Row['Oasis_Admission_LearnerCode'] . "&month=" . $_Row['Oasis_Admission_Batch'] . "&Mode=Reject' target='_Blank'>"
						. "<input type='button' name='Edit' id='Edit' class='btn btn-primary' value='View More Details' /></a>"						
						. "</td>";
			
           
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        echo "No Record Found";
    }
}

if ($_action == "FillCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}