<?php
/* 
 * Author Name:  Sunil Kumar Baindara
 */
include 'commonFunction.php';
require 'BAL/clsRkclApiFDetail.php';

$response = array();
$emp = new clsRkclApiFDetail();

if ($_action == "ADD") {
    if (isset($_POST["ClientApiId"])) {
        //print_r($_POST);die;
        $_ClientApiId = $_POST["ClientApiId"];
        $_Functionname=$_POST['txtFunctionName'];
        $_Functionfileds=$_POST['txtddlFtable2'];
        $response = $emp->Add($_ClientApiId,$_Functionname,$_Functionfileds);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_RootName = $_POST["name"];
        $_Status=$_POST["status"];
        $_Code=$_POST['code'];
        $_DisplayOrder=$_POST['display'];
        $response = $emp->Update($_Code,$_RootName,$_Status,$_DisplayOrder);
        echo $response[0];
    }
}

if ($_action == "EDIT") {
    //print_r($_POST);die;
    $_Code=$_POST['code'];
    $response = $emp->GetDatabyCode($_Code);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("Functionname" => $_Row['Functionname'],
            "Functionfileds" => $_Row['Functionfileds']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}

if ($_action == "DELETE") {
   // print_r($_POST);die;
    $response = $emp->DeleteRecord($_actionvalue);
    echo $response[0];
}

if ($_action == "SHOW") {
    //echo "Show";
    //print_r($_POST);die;
    $response = $emp->GetAll($_POST['ClientApiId']);
    //print_r($response);die;
    
    $_DataTable = "";

    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Table Name</th>";
    echo "<th style='10%'>Funciton Fields</th>";
    
    echo "<th style='30%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if($response[0]=='Success')
        {
            while ($_Row = mysqli_fetch_array($response[2])) {
            if($_Row['Status']==1){$Status='Active';}else{$Status='Deactive';}
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Functionname'] . "</td>";
            echo "<td>" . $_Row['Functionfileds'] . "</td>";

            echo "<td>" . $Status . "</td>";
//            echo "<td><a href='frmRkclApiFDetail.php?clientapiID=".$_POST['ClientApiId']."&code=" . $_Row['client_fun_id'] . "&Mode=Edit'>"
//                    . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
            echo "<td><a href='frmRkclApiFDetail.php?clientapiID=".$_POST['ClientApiId']."&code=".$_Row['client_fun_id']."&Mode=Delete'>"
                    . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
            echo "</tr>";
            $_Count++;
        }
        }
    else
        {
         echo "<tr>";
            echo "<td colspan='5'>" . $response[0] . "</td>";
         echo "</tr>";
        }
    echo "</tbody>";
    echo "</table>";
}

if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='0' selected='selected'>Select Root</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Root_Menu_Code'] . ">" . $_Row['Root_Menu_Name'] . "</option>";
    }
}

//* This function is getting data form the function name of the apis*//
if ($_action == "FILLFNAME") {
    $response = $emp->GetAllFunctionName();
    echo "<option value='0' selected='selected'>Select Table Name</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['funtablename'] . ">" . $_Row['functionname'] . "</option>";
    }
}
//* This function is getting data form the function name of the apis*//
//
//* This function is getting table filed for the function apis*//
if ($_action == "FILLTBFIELDNAME") {
    //echo $_POST['ftablename'];die;
    $response = $emp->GetTableFiledByTabName($_POST['ftablename']);
    //echo "<option value='0' selected='selected'>Select Function</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['COLUMN_NAME'] . ">" . $_Row['COLUMN_NAME'] . "</option>";
    }
}
//* This function is getting table filed for the function apis*//