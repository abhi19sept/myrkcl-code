<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsBankAccount.php';

$response = array();
$emp = new clsBankAccount();

//print_r($_POST["rbtpaymentType"]);
if ($_action == "ADD") {
    if (isset($_POST["txtaccountName"]) && !empty($_POST["txtaccountName"])) {
        if (isset($_POST["txtaccountNumber"]) && !empty($_POST["txtaccountNumber"])) {
            if (isset($_POST["rbtaccountType_saving"]) && !empty($_POST["rbtaccountType_saving"])) {
                if (isset($_POST["txtIfscCode"]) && !empty($_POST["txtIfscCode"])) {
                    if (isset($_POST["ddlBankName"]) && !empty($_POST["ddlBankName"])) {
                        if (isset($_POST["ddlBranch"]) && !empty($_POST["ddlBranch"])) {
                            if (isset($_POST["txtMicrcode"]) && !empty($_POST["txtMicrcode"])) {
                                if (isset($_POST["txtpanno"]) && !empty($_POST["txtpanno"])) {
                                    if (isset($_POST["txtpanname"]) && !empty($_POST["txtpanname"])) {
                                        if (isset($_POST["ddlidproof"]) && !empty($_POST["ddlidproof"])) {



                                            $_AccountName = $_POST["txtaccountName"];
                                            $_AccountNumber = $_POST["txtaccountNumber"];


                                            $_ConfirmaccountNumber = $_POST["txtConfirmaccountNumber"];

                                            $_AccountType = $_POST["rbtaccountType_saving"];
                                            $_IfscCode = $_POST["txtIfscCode"];
                                            $_BankName = $_POST["ddlBankName"];
                                            $_BranchName = $_POST["ddlBranch"];
                                            $_MicrCode = $_POST["txtMicrcode"];
                                            $_PanNo = $_POST["txtpanno"];
                                            $_PanName = $_POST["txtpanname"];
                                            $_IdProof = $_POST["ddlidproof"];
                                            $_Genid = $_POST["txtGenerateId"];


                                            $response = $emp->Add($_AccountName, $_AccountNumber, $_AccountType, $_IfscCode, $_BankName, $_BranchName, $_MicrCode, $_PanNo, $_PanName, $_IdProof, $_Genid);
                                            echo $response[0];
                                        } else {
                                            echo "Inavalid Entry";
                                        }
                                    } else {
                                        echo "Inavalid Entry";
                                    }
                                } else {
                                    echo "Inavalid Entry";
                                }
                            } else {
                                echo "Inavalid Entry";
                            }
                        } else {
                            echo "Inavalid Entry";
                        }
                    } else {
                        echo "Inavalid Entry";
                    }
                } else {
                    echo "Inavalid Entry";
                }
            } else {
                echo "Inavalid Entry";
            }
        } else {
            echo "Inavalid Entry";
        }
    } else {
        echo "Inavalid Entry";
    }
}


if ($_action == "UPDATE") {
    //print_r($_POST);
    if (isset($_POST["code"])) {
        $_code = $_POST["code"];

        $_AccountName = $_POST["txtaccountName"];
        $_AccountNumber = $_POST["txtaccountNumber"];


        $_ConfirmaccountNumber = $_POST["txtConfirmaccountNumber"];

        $_AccountType = $_POST["rbtaccountType_saving"];
        $_IfscCode = $_POST["txtIfscCode"];
        $_BankName = $_POST["ddlBankName"];
        $_BranchName = $_POST["ddlBranch"];
        $_MicrCode = $_POST["txtMicrcode"];
        $_PanNo = $_POST["txtpanno"];
        $_PanName = $_POST["txtpanname"];
        $_IdProof = $_POST["ddlidproof"];
        $_Genid = $_POST["txtGenerateId"];




        $response = $emp->Update($_code, $_AccountName, $_AccountNumber, $_AccountType, $_IfscCode, $_BankName, $_BranchName, $_MicrCode, $_PanNo, $_PanName, $_IdProof, $_Genid);
        echo $response[0];
    }
}





if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->SHOWDATA();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Bank Account Name</th>";
    echo "<th style='40%'> 	Bank Account Number</th>";
    echo "<th style='10%'> 	Bank IFSC Code</th>";
    echo "<th style='10%'> 	Bank Name</th>";
    echo "<th style='10%'> 	Bank Micr Code</th>";
    echo "<th style='10%'>  Bank Branch Name</th>";
    echo "<th style='10%'>  Account Type</th>";
    echo "<th style='10%'>  Pan Card No</th>";
    echo "<th style='10%'>  Pan Card Holder Name</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Bank_Account_Name'] . "</td>";
        echo "<td>'" . $_Row['Bank_Account_Number'] . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
        echo "<td>" . $_Row['Bank_Micr_Code'] . "</td>";
        echo "<td>" . $_Row['Bank_Branch_Name'] . "</td>";
        echo "<td>" . $_Row['Bank_Account_Type'] . "</td>";
        echo "<td>" . strtoupper($_Row['Pan_No']) . "</td>";
        echo "<td>" . strtoupper($_Row['Pan_Name']) . "</td>";

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "SHOWEDIT") {

    //echo "Show";
    $response = $emp->SHOWDATA();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Bank Account Name</th>";
    echo "<th style='40%'> 	Bank Account Number</th>";
    echo "<th style='10%'> 	Bank IFSC Code</th>";
    echo "<th style='10%'> 	Bank Name</th>";
    echo "<th style='10%'> 	Bank Micr Code</th>";
    echo "<th style='10%'>  Bank Branch Name</th>";
    echo "<th style='10%'>  Account Type</th>";
    echo "<th style='10%'>  Pan Card No</th>";
    echo "<th style='10%'>  Pan Card Holder Name</th>";
    echo "<th style='10%'>  Bank Document</th>";
    echo "<th style='10%'>  Pan Document</th>";
    echo "<th style='10%'>  ITGK Code</th>";

    echo "<th style='10%'>  Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
        echo "<td>'" . $_Row['Bank_Account_Number'] . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
        echo "<td>" . $_Row['Bank_Micr_Code'] . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Branch_Name']) . "</td>";
        echo "<td>" . $_Row['Bank_Account_Type'] . "</td>";
        echo "<td>" . strtoupper($_Row['Pan_No']) . "</td>";
        echo "<td>" . strtoupper($_Row['Pan_Name']) . "</td>";


        echo "<td>";
        echo '<a href="upload/Bankdocs/' . $_Row['Bank_Document'] . '" target="_blank"><img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/Bankdocs/' . $_Row['Bank_Document'] . '"/></a>';
        echo "</td>";
        echo "<td>";
        echo '<a href="upload/pancard/' . $_Row['Pan_Document'] . '" target="_blank"><img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="upload/pancard/' . $_Row['Pan_Document'] . '"/></a>';
        echo "</td>";


        echo "<td>" . $_Row['Bank_User_Code'] . "</td>";


        echo "<td><a href='frmbankaccount.php?code=" . $_Row['Bank_Account_Code'] . "&Mode=Edit'>"
        . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmbankaccount.php?code=" . $_Row['Bank_Account_Code'] . "&Mode=Delete'>"
        . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";




    echo "</div>";
}












if ($_action == "FILL") {
    $response = $emp->GetAll();
    echo "<option value='' >Select Banks</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['bankname'] . ">" . $_Row['bankname'] . "</option>";
    }
}

if ($_action == "FILLBRANCH") {
    $response = $emp->GetBranch();
    echo "<option value='' >Select Branch</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['branchname'] . ">" . $_Row['branchname'] . "</option>";
    }
}

if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_POST["values"]);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array(
            "Bank_Account_Name" => $_Row['Bank_Account_Name'],
            "Bank_Account_Number" => $_Row['Bank_Account_Number'],
            "Bank_Account_Type" => $_Row['Bank_Account_Type'],
            "Bank_Ifsc_code" => $_Row['Bank_Ifsc_code'],
            "Bank_Name" => $_Row['Bank_Name'],
            "Bank_Micr_Code" => $_Row['Bank_Micr_Code'],
            "Bank_Branch_Name" => $_Row['Bank_Branch_Name'],
            "Pan_No" => $_Row['Pan_No'],
            "Pan_Name" => $_Row['Pan_Name'],
            "Bank_Id_Proof" => $_Row['Bank_Id_Proof'],
        );
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "FILLDETAILS") {


    $response = $emp->GetDetails($_POST["txtIfscCode"]);

    if ($response[0] == 'Success') {
        $_Row = mysqli_fetch_array($response[2]);
        $_DataTable = array();
        $_i = 0;
        $_Datatable[$_i] = array("bankname" => $_Row['bankname'],
            "micrcode" => $_Row['micrcode'],
            "branchname" => $_Row['branchname']);
        echo json_encode($_Datatable);
    } elseif ($response[0] == 'No Record Found') {
        echo "0";
    }
}



if ($_action == "SHOWGRID") {

    //echo "Show";
    $response = $emp->SHOWDATA();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
     echo "<th style='10%'> ITGK Code</th>";
      echo "<th style='10%'> ITGK Name</th>";
    echo "<th style='45%'>Bank Account Name</th>";
    echo "<th style='40%'> Bank Account Number</th>";
    echo "<th style='10%'> Bank IFSC Code</th>";
    echo "<th style='10%'> Bank Name</th>";
    echo "<th style='10%'> Bank Micr Code</th>";
    echo "<th style='10%'>  Bank Branch Name</th>";
    echo "<th style='10%'>  Account Type</th>";
    echo "<th style='10%'>  Pan Card No</th>";
    echo "<th style='10%'>  Pan Card Holder Name</th>";
    echo "<th style='10%'>  Bank Document</th>";
    echo "<th style='10%'>  PAN  Document</th>";
   


    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
          echo "<td>" . $_Row['Bank_User_Code'] . "</td>";
           echo "<td>" . $_Row['ITGK_Name'] . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
        echo "<td>'" . strtoupper($_Row['Bank_Account_Number']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
        echo "<td>" . $_Row['Bank_Micr_Code'] . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Branch_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Type']) . "</td>";
        echo "<td>" . strtoupper($_Row['Pan_No']) . "</td>";
        echo "<td>" . strtoupper($_Row['Pan_Name']) . "</td>";


        echo "<td>";
        echo '<a href="upload/Bankdocs/' . $_Row['Bank_Document'] . '" target="_blank"><img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="images/view.png"/></a>';
        echo "</td>";
        echo "<td>";
        echo '<a href="upload/pancard/' . $_Row['Pan_Document'] . '" target="_blank"><img class="img-squre img-responsive" width="50"  style="padding:2px;border:1px solid #428bca;margin-top:10px;" src="images/view.png"/></a>';
        echo "</td>";


      



        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";




    echo "</div>";
}





if ($_action == "SENDOTP") {
    $response = $emp->SendOTP();
    echo $response[0];
}


if ($_action == "VERIFY") {
    //print_r($_POST);
    if (isset($_POST["otp"])) {

        $_Otp = $_POST["otp"];

        $response = $emp->Verify($_Otp);
        echo $response[0];
    }
}

if ($_action == "FILLMOBILE") {
    $response = $emp->Getmobile();
    $_Row = mysqli_fetch_array($response[2]);
    $_Mobile = $_Row['User_MobileNo'];
    $_Mobile1 = substr($_Mobile, 6);
    $_Mobile2 = "XXXXXX" . $_Mobile1;
    echo $_Mobile2;
}


