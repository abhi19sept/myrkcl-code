<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsCenterCountGPwise.php';

$response = array();
$emp = new clsCenterCountGPwise();

if ($_action == "GETDATA") {

    $response = $emp->GetDataAll($_POST['district']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>District Name</th>";
    echo "<th>Panchayat Samiti Name</th>";
    echo "<th>Gram Panchayat Name</th>";
    echo "<th>IT-GK Count</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_Total = 0;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['District_Name'] . "</td>";
        echo "<td>" . $_Row['Block_Name'] . "</td>";
        echo "<td>" . $_Row['GP_Name'] . "</td>";
        if ($_Row['cnt'] == '0') {
            echo "<td class='odd gradeX warning'>" . $_Row['cnt'] . "</td>";
        } else {
            echo "<td class='odd gradeX'>" . $_Row['cnt'] . "</td>";
        }



        $_Count++;
    }

    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}