<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsReleaseIntake.php';

$response = array();
$emp = new clsReleaseIntake();


if ($_action == "FILLAllocate") {
    $response = $emp->Allocate($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FILLRelease") {
    $response = $emp->Release($_POST['values'],$_POST['course']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "ADD") {
    if (isset($_POST["allocate"])) {
        $_Allocate = $_POST["allocate"];
        $_Release=$_POST["release"];
        $_Course=$_POST["course"];
		
        $response = $emp->Add($_Allocate,$_Release,$_Course);
        echo $response[0];
    }
}


