<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
set_time_limit(0);
require 'commonFunction.php';
require 'BAL/clsGstInvoiceSummary.php';

$response = array();
$emp = new clsGstInvoiceSummary();


if ($_action == "learner_fee") {

    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));

        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<th >S No.</th>";
        echo "<th >Invoice Number</th>";
        echo "<th>Invoice Date</th>";
        echo "<th>GSTIN of Recipient</th>";
        echo "<th>Name of the recipient</th>";
        echo "<th>Address of recipient</th>";
        echo "<th>Place of Supply</th>";
        echo "<th>HSN</th>";
        echo "<th>Description</th>";
        echo "<th>Quantity</th>";
        echo "<th>Taxable value</th>";
        echo "<th>IGST Rate</th>";
        echo "<th>IGST Value</th>";
        echo "<th>CGST Rate</th>";
        echo "<th>CGST Value</th>";
        echo "<th >SGST Rate</th>";
        echo "<th >SGST Value</th>";
        // echo "<th >Whether RCM Applicable</th>";
        // echo "<th >Supply Type</th>";
        // echo "<th >Considered in Financials</th>";
        // echo "<th >Considered in GST</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";


        $response = $emp->Learner_fee($startdate, $enddate);
        $co = mysqli_num_rows($response[2]);
        if ($co) {
            $_Count = 1;
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>ADM/" . $_Row['invoice_no'] . "</td>";
                 echo "<td>" . $_Row['Invoice_Date'] . "</td>";
                 echo "<td></td>";
               echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
               echo "<td>" . $_Row['District_Name'] . "</td>";
                echo "<td>Rajasthan</td>";
                 echo "<td></td>";
                  echo "<td>RS-CIT Course Fee Share</td>";
                   echo "<td>1</td>";
                echo "<td>" . $_Row['Gst_Invoce_BaseFee'] . "</td>";
                echo "<td></td>";
                 echo "<td></td>";
                  echo "<td>9</td>";
                echo "<td>" . $_Row['Gst_Invoce_CGST'] . "</td>";
                 echo "<td>9</td>";
                echo "<td>" . $_Row['Gst_Invoce_SGST'] . "</td>";
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }
    
    //  }
}
if ($_action == "reexam_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));

        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<th >S No.</th>";
        echo "<th >Invoice Number</th>";
        echo "<th>Invoice Date</th>";
        echo "<th>GSTIN of Recipient</th>";
        echo "<th>Name of the recipient</th>";
        echo "<th>Address of recipient</th>";
        echo "<th>Place of Supply</th>";
        echo "<th>HSN</th>";
        echo "<th>Description</th>";
        echo "<th>Quantity</th>";
        echo "<th>Taxable value</th>";
        echo "<th>IGST Rate</th>";
        echo "<th>IGST Value</th>";
        echo "<th>CGST Rate</th>";
        echo "<th>CGST Value</th>";
        echo "<th >SGST Rate</th>";
        echo "<th >SGST Value</th>";
        // echo "<th >Whether RCM Applicable</th>";
        // echo "<th >Supply Type</th>";
        // echo "<th >Considered in Financials</th>";
        // echo "<th >Considered in GST</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";


        $response = $emp->reexam_fee($startdate, $enddate);
        $co = mysqli_num_rows($response[2]);
        if ($co) {
            $_Count = 1;
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>REX/" . $_Row['invoice_no'] . "</td>";
                 echo "<td>" . $_Row['Invoice_Date'] . "</td>";
                 echo "<td></td>";
               echo "<td>" . strtoupper($_Row['learnername']) . "</td>";
               echo "<td>" . $_Row['District_Name'] . "</td>";
                echo "<td>Rajasthan</td>";
                 echo "<td></td>";
                  echo "<td>RS-CIT Re-exam Fee Share</td>";
                   echo "<td>1</td>";
                echo "<td>" . $_Row['Gst_Invoce_BaseFee'] . "</td>";
                echo "<td></td>";
                 echo "<td></td>";
                  echo "<td>9</td>";
                echo "<td>" . $_Row['Gst_Invoce_CGST'] . "</td>";
                 echo "<td>9</td>";
                echo "<td>" . $_Row['Gst_Invoce_SGST'] . "</td>";
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }
    
    //  }
}
if ($_action == "NCR_fee") {
     $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));

        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<th >S No.</th>";
        echo "<th >Invoice Number</th>";
        echo "<th>Invoice Date</th>";
        echo "<th>GSTIN of Recipient</th>";
        echo "<th>Name of the recipient</th>";
        echo "<th>Address of recipient</th>";
        echo "<th>Place of Supply</th>";
        echo "<th>HSN</th>";
        echo "<th>Description</th>";
        echo "<th>Quantity</th>";
        echo "<th>Taxable value</th>";
        echo "<th>IGST Rate</th>";
        echo "<th>IGST Value</th>";
        echo "<th>CGST Rate</th>";
        echo "<th>CGST Value</th>";
        echo "<th >SGST Rate</th>";
        echo "<th >SGST Value</th>";
        // echo "<th >Whether RCM Applicable</th>";
        // echo "<th >Supply Type</th>";
        // echo "<th >Considered in Financials</th>";
        // echo "<th >Considered in GST</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";


        $response = $emp->NCR_fee($startdate, $enddate);
        $co = mysqli_num_rows($response[2]);
        if ($co) {
            $_Count = 1;
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>NCR/Online/" . $_Row['invoice_no'] . "</td>";
                 echo "<td>" . $_Row['Invoice_Date'] . "</td>";
                 echo "<td></td>";
               echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
               echo "<td>" . $_Row['District_Name'] . "</td>";
                echo "<td>Rajasthan</td>";
                 echo "<td></td>";
                  echo "<td>RS-CIT New Center Registration</td>";
                   echo "<td>1</td>";
                echo "<td>" . $_Row['Gst_Invoce_BaseFee'] . "</td>";
                echo "<td></td>";
                 echo "<td></td>";
                  echo "<td>9</td>";
                echo "<td>" . $_Row['Gst_Invoce_CGST'] . "</td>";
                 echo "<td>9</td>";
                echo "<td>" . $_Row['Gst_Invoce_SGST'] . "</td>";
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }
    
}
if ($_action == "correction_fee") {
      $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));

        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<th >S No.</th>";
        echo "<th >Invoice Number</th>";
        echo "<th>Invoice Date</th>";
        echo "<th>GSTIN of Recipient</th>";
        echo "<th>Name of the recipient</th>";
        echo "<th>Address of recipient</th>";
        echo "<th>Place of Supply</th>";
        echo "<th>HSN</th>";
        echo "<th>Description</th>";
        echo "<th>Quantity</th>";
        echo "<th>Taxable value</th>";
        echo "<th>IGST Rate</th>";
        echo "<th>IGST Value</th>";
        echo "<th>CGST Rate</th>";
        echo "<th>CGST Value</th>";
        echo "<th >SGST Rate</th>";
        echo "<th >SGST Value</th>";
        // echo "<th >Whether RCM Applicable</th>";
        // echo "<th >Supply Type</th>";
        // echo "<th >Considered in Financials</th>";
        // echo "<th >Considered in GST</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";


        $response = $emp->correction_fee($startdate, $enddate);
        $co = mysqli_num_rows($response[2]);
        if ($co) {
            $_Count = 1;
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>CCD/" . $_Row['invoice_no'] . "</td>";
                 echo "<td>" . $_Row['Invoice_Date'] . "</td>";
                 echo "<td></td>";
               echo "<td>" . strtoupper($_Row['loglearnername']) . "</td>";
               echo "<td>" . $_Row['District_Name'] . "</td>";
                echo "<td>Rajasthan</td>";
                 echo "<td></td>";
                  echo "<td>Certificate Correction/ Duplicate Certificate charges </td>";
                   echo "<td>1</td>";
                echo "<td>" . $_Row['Gst_Invoce_BaseFee'] . "</td>";
                echo "<td></td>";
                 echo "<td></td>";
                  echo "<td>9</td>";
                echo "<td>" . $_Row['Gst_Invoce_CGST'] . "</td>";
                 echo "<td>9</td>";
                echo "<td>" . $_Row['Gst_Invoce_SGST'] . "</td>";
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }
}
if ($_action == "name_fee") {
   $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));

        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<th >S No.</th>";
        echo "<th >Invoice Number</th>";
        echo "<th>Invoice Date</th>";
        echo "<th>GSTIN of Recipient</th>";
        echo "<th>Name of the recipient</th>";
        echo "<th>Address of recipient</th>";
        echo "<th>Place of Supply</th>";
        echo "<th>HSN</th>";
        echo "<th>Description</th>";
        echo "<th>Quantity</th>";
        echo "<th>Taxable value</th>";
        echo "<th>IGST Rate</th>";
        echo "<th>IGST Value</th>";
        echo "<th>CGST Rate</th>";
        echo "<th>CGST Value</th>";
        echo "<th >SGST Rate</th>";
        echo "<th >SGST Value</th>";
        // echo "<th >Whether RCM Applicable</th>";
        // echo "<th >Supply Type</th>";
        // echo "<th >Considered in Financials</th>";
        // echo "<th >Considered in GST</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";


        $response = $emp->name_fee($startdate, $enddate);
        $co = mysqli_num_rows($response[2]);
        if ($co) {
            $_Count = 1;
            while ($_Row = mysqli_fetch_array($response[2])) {

        $baseamount = ($_Row['amount']) * 100 / 118;
        $baseamountnew = number_format((float)$baseamount, 2, '.', '');        
        $cgst1 = ($baseamountnew) * 9 / 100;
        $cgst = number_format((float)$cgst1, 2, '.', '');



                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>NLC/Online/" . $_Row['invoice_no'] . "</td>";
                 echo "<td>" . $_Row['Invoice_Date'] . "</td>";
                 echo "<td></td>";
               echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
               echo "<td>" . $_Row['District_Name'] . "</td>";
                echo "<td>Rajasthan</td>";
                 echo "<td></td>";
                  echo "<td>Charges Against Location Change</td>";
                   echo "<td>1</td>";
                echo "<td>" .  $baseamountnew . "</td>";
                echo "<td></td>";
                 echo "<td></td>";
                  echo "<td>9</td>";
                echo "<td>" .  $cgst . "</td>";
                 echo "<td>9</td>";
                echo "<td>" .  $cgst . "</td>";
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }
}
if ($_action == "eoi_fee") {
     $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));

        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<th >S No.</th>";
        echo "<th >Invoice Number</th>";
        echo "<th>Invoice Date</th>";
        echo "<th>GSTIN of Recipient</th>";
        echo "<th>Name of the recipient</th>";
        echo "<th>Address of recipient</th>";
        echo "<th>Place of Supply</th>";
        echo "<th>HSN</th>";
        echo "<th>Description</th>";
        echo "<th>Quantity</th>";
        echo "<th>Taxable value</th>";
        echo "<th>IGST Rate</th>";
        echo "<th>IGST Value</th>";
        echo "<th>CGST Rate</th>";
        echo "<th>CGST Value</th>";
        echo "<th >SGST Rate</th>";
        echo "<th >SGST Value</th>";
        // echo "<th >Whether RCM Applicable</th>";
        // echo "<th >Supply Type</th>";
        // echo "<th >Considered in Financials</th>";
        // echo "<th >Considered in GST</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";


        $response = $emp->eoi_fee($startdate, $enddate);
        $co = mysqli_num_rows($response[2]);
        if ($co) {
            $_Count = 1;
            while ($_Row = mysqli_fetch_array($response[2])) {
                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>CCD/" . $_Row['invoice_no'] . "</td>";
                 echo "<td>" . $_Row['Invoice_Date'] . "</td>";
                 echo "<td></td>";
               echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
               echo "<td>" . $_Row['District_Name'] . "</td>";
                echo "<td>Rajasthan</td>";
                 echo "<td></td>";
                  echo "<td>Certificate Correction/ Duplicate Certificate charges </td>";
                   echo "<td>1</td>";
                echo "<td>" . $_Row['Gst_Invoce_BaseFee'] . "</td>";
                echo "<td></td>";
                 echo "<td></td>";
                  echo "<td>9</td>";
                echo "<td>" . $_Row['Gst_Invoce_CGST'] . "</td>";
                 echo "<td>9</td>";
                echo "<td>" . $_Row['Gst_Invoce_SGST'] . "</td>";
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }
}

if ($_action == "RenewalPenalty_fee") {
     $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));

        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<th >S No.</th>";
        echo "<th >Invoice Number</th>";
        echo "<th>Invoice Date</th>";
        echo "<th>GSTIN of Recipient</th>";
        echo "<th>Name of the recipient</th>";
        echo "<th>Address of recipient</th>";
        echo "<th>Place of Supply</th>";
        echo "<th>HSN</th>";
        echo "<th>Description</th>";
        echo "<th>Quantity</th>";
        echo "<th>Taxable value</th>";
        echo "<th>IGST Rate</th>";
        echo "<th>IGST Value</th>";
        echo "<th>CGST Rate</th>";
        echo "<th>CGST Value</th>";
        echo "<th >SGST Rate</th>";
        echo "<th >SGST Value</th>";
        // echo "<th >Whether RCM Applicable</th>";
        // echo "<th >Supply Type</th>";
        // echo "<th >Considered in Financials</th>";
        // echo "<th >Considered in GST</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";


        $response = $emp->RenewalPenalty_fee($startdate, $enddate);
        $co = mysqli_num_rows($response[2]);
        if ($co) {

            $_Count = 1;
            while ($_Row = mysqli_fetch_array($response[2])) {

        $baseamount = ($_Row['amount']) * 100 / 118;
        $baseamountnew = number_format((float)$baseamount, 2, '.', '');        
        $cgst1 = ($baseamountnew) * 9 / 100;
        $cgst = number_format((float)$cgst1, 2, '.', '');



                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>FIN/Online/" . $_Row['invoice_no'] . "</td>";
                 echo "<td>" . $_Row['Invoice_Date'] . "</td>";
                 echo "<td></td>";
               echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
               echo "<td>" . $_Row['District_Name'] . "</td>";
                echo "<td>Rajasthan</td>";
                 echo "<td></td>";
                  echo "<td>Charges Against RS-CIT (Renewal) </td>";
                   echo "<td>1</td>";
                echo "<td>" .  $baseamountnew . "</td>";
                echo "<td></td>";
                 echo "<td></td>";
                  echo "<td>9</td>";
                echo "<td>" .  $cgst . "</td>";
                 echo "<td>9</td>";
                echo "<td>" .  $cgst . "</td>";
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }
}

if ($_action == "OwnerChange_fee") {
    $startdate = $_POST['txtstartdate'];
    $enddate = $_POST['txtenddate'];

    $startdate = date("Y-m-d", strtotime($startdate));
    $enddate = date("Y-m-d", strtotime($enddate));

        $_DataTable = "";

        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<th >S No.</th>";
        echo "<th >Invoice Number</th>";
        echo "<th>Invoice Date</th>";
        echo "<th>GSTIN of Recipient</th>";
        echo "<th>Name of the recipient</th>";
        echo "<th>Address of recipient</th>";
        echo "<th>Place of Supply</th>";
        echo "<th>HSN</th>";
        echo "<th>Description</th>";
        echo "<th>Quantity</th>";
        echo "<th>Taxable value</th>";
        echo "<th>IGST Rate</th>";
        echo "<th>IGST Value</th>";
        echo "<th>CGST Rate</th>";
        echo "<th>CGST Value</th>";
        echo "<th >SGST Rate</th>";
        echo "<th >SGST Value</th>";
        // echo "<th >Whether RCM Applicable</th>";
        // echo "<th >Supply Type</th>";
        // echo "<th >Considered in Financials</th>";
        // echo "<th >Considered in GST</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";


        $response = $emp->OwnerChange_fee($startdate, $enddate);
        $co = mysqli_num_rows($response[2]);
        if ($co) {

            $_Count = 1;
            while ($_Row = mysqli_fetch_array($response[2])) {

        $baseamount = ($_Row['amount']) * 100 / 118;
        $baseamountnew = number_format((float)$baseamount, 2, '.', '');        
        $cgst1 = ($baseamountnew) * 9 / 100;
        $cgst = number_format((float)$cgst1, 2, '.', '');



                echo "<tr>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>FEE/OC/" . $_Row['invoice_no'] . "</td>";
                 echo "<td>" . $_Row['Invoice_Date'] . "</td>";
                 echo "<td></td>";
               echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
               echo "<td>" . $_Row['District_Name'] . "</td>";
                echo "<td>Rajasthan</td>";
                 echo "<td></td>";
                  echo "<td>Charges Against Ownership Change </td>";
                   echo "<td>1</td>";
                echo "<td>" .  $baseamountnew . "</td>";
                echo "<td></td>";
                 echo "<td></td>";
                  echo "<td>9</td>";
                echo "<td>" .  $cgst . "</td>";
                 echo "<td>9</td>";
                echo "<td>" .  $cgst . "</td>";
                echo "</tr>";
                $_Count++;
            }
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
        } else {
            echo "No Record Found";
        }
}

