<?php

/* 
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsVillageMaster.php';

$response = array();
$emp = new clsVillageMaster();


if ($_action == "ADD") {
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        $_VillageName = $_POST["name"];
        $_Parent=$_POST["parent"];
        $_Status=$_POST["status"];

        $response = $emp->Add($_VillageName,$_Parent,$_Status);
        echo $response[0];
    }
}

if ($_action == "UPDATE") {
    if (isset($_POST["name"])) {
        $_VillageName = $_POST["name"];
        $_Parent=$_POST["parent"];
        $_Status=$_POST["status"];
        $_Code=$_POST['code'];
        $response = $emp->Update($_Code,$_VillageName,$_Parent,$_Status);
        echo $response[0];
    }
}


if ($_action == "EDIT") {


    $response = $emp->GetDatabyCode($_actionvalue);

    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_Datatable[$_i] = array("VillageCode" => $_Row['Village_Code'],
            "VillageName" => $_Row['Village_Name'],
            "District" => $_Row['Village_GP'],
            "Status"=>$_Row['Village_Status']);
        $_i = $_i + 1;
    }
    echo json_encode($_Datatable);
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll($_actionvalue);
    showGramPanchayatList($response);
    
}
if ($_action == "FILL") {
    $response = $emp->GetAll($_actionvalue);
    echo "<option value='0'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Village_Code'] . ">" . $_Row['Village_Name'] . "</option>";
    }
}

if ($_action == "Search") {
    $filters = [];
    if (!empty($_POST['name'])) {
        $filters['Village_Name'] = " LIKE ('%" . $_POST['name'] . "%')";
    }
    if (!empty($_POST['parent']) && !is_null($_POST['parent']) && $_POST['parent'] != 'null') {
        $filters['Village_GP'] = ' = ' . $_POST['parent'];
    }
    if (!empty($_POST['status'])) {
        $filters['Village_Status'] = ' = ' . $_POST['status'];
    }
    if (!empty($_POST['panchayatid']) && !is_null($_POST['panchayatid']) && $_POST['panchayatid'] != 'null') {
        $filters['GP_Block'] = ' = ' . $_POST['panchayatid'];
    }
    if (!empty($filters)) {
        $response = $emp->GetAll($filters);
        showGramPanchayatList($response);
    }
}


function showGramPanchayatList($response) {
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='25%'>Name</th>";
    echo "<th style='20%'>Parent</th>";
    echo "<th style='20%'>Status</th>";
    echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Village_Name'] . "</td>";
        echo "<td>" . $_Row['GP_Name'] . "</td>";
         echo "<td>" . $_Row['Status_Name'] . "</td>";
        echo "<td><a href='frmVillageMaster.php?code=" . $_Row['Village_Code'] . "&Mode=Edit'>"
                . "<img src='images/editicon.png' alt='Edit' width='30px' /></a>  "
        . "<a href='frmVillageMaster.php?code=" . $_Row['Village_Code'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}