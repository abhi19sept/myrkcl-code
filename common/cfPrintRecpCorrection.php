<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsPrintRecpCorrection.php';
require 'DAL/upload_ftp_doc.php';
$response = array();
$emp = new clsPrintRecpCorrection();

if ($_action == "SHOW") {
    if (isset($_POST["startdate"])) {
        if (isset($_POST["enddate"])) {
            $sdate = date_format(date_create($_POST["startdate"]), "Y-m-d") . ' 00:00:00';
            $edate = date_format(date_create($_POST["enddate"]), "Y-m-d") . ' 23:59:59';
            $response = $emp->Show($sdate, $edate);
            echo "<div class='table-responsive'>";
            echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th>S No.</th>";
            echo "<th> Admission Code </th>";
            echo "<th> Admission Name </th>";
            echo "<th> Payment Account Name</th>";

            echo "<th> TransactionID </th>";
            echo "<th> Payment Type </th>";

            echo "<th> Payment Date</th>";
            echo "<th>Invoice Detail</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            $_Count = 1;
            $_TotalAmount = 0;

            while ($row = mysqli_fetch_array($response[2])) {
                $Tdate = date("d-m-Y", strtotime($row['Correction_Transaction_timestamp']));

                echo "<tr class='odd gradeX'>";
                echo "<td>" . $_Count . "</td>";
                echo "<td>" . $row['lcode'] . "</td>";
                echo "<td>" . strtoupper($row['cfname']) . "</td>";

                echo "<td>" . strtoupper($row['Correction_Transaction_Fname']) . "</td>";

                echo "<td>" . $row['Correction_Transaction_Txtid'] . "</td>";

                echo "<td>" . $row['Correction_Transaction_ProdInfo'] . "</td>";
                echo "<td>" . $Tdate . "</td>";

                if ($row['Correction_Transaction_timestamp'] >= '2017-07-01 00:00:00') {
                    echo "<td class='org'><button type='button' data-toggle='modal' data-target='#approvalLetter' class='btn btn-primary approvalLetter' id='" . $row['cid'] . "' >Download Invoice</button></td>";
                } else {
                    echo "<td>Invoice Not Available</td>";
                }
                echo "</tr>";
                $_TotalAmount = $_TotalAmount + $row['Correction_Transaction_Amount'];
                $_Count++;
            }

            echo "</tbody>";
            echo "<tfoot>";

            echo "</tfoot>";
            echo "</table>";
            echo "</div>";
            //echo $html;
        }
    }
}

/** if ($_action == "DwnldPrintRecp") {
    $path = '../upload/CorrectionDuplicatePrintRecpt/';
    $learnerCode = $_POST['cid'];
    $file = $_POST['cid'] . '.fdf';
    $filePath = $path . $file;
    $allreadyfoundAkcRecipt = $path . $learnerCode . '_CorrectionDuplicateInvoice.pdf';

    $response = $emp->GetAllPrintRecpDwnld($_POST['cid']);
    
    $co = mysqli_num_rows($response[2]);
    $res = '';
    if ($co) {
        $_Row1 = mysqli_fetch_array($response[2], true);

        $_Row1["invoice_no"] = "CCD/" . $_Row1['invoice_no'];
        $_Row1["invoice_date"] = date("d-m-Y", strtotime($_Row1['addtime']));
        $_Row1["Lname"] = strtoupper($_Row1['cfname']);
        $_Row1['Fname'] = strtoupper($_Row1['cfaname']);
        $_Row1["Lcode"] = $_Row1['lcode'];
        $_Row1["Laddress"] = strtoupper($_Row1['District_Name']);
        $_Row1["Ldob"] = date("d-m-Y", strtotime($_Row1['Admission_DOB']));
        $_Row1["Lmobile"] = $_Row1['Admission_Mobile'];

        $_Row1["Litgk"] = $_Row1['Admission_ITGK_Code'];
        $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
        $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
        $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'];
        $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
        $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
        $_Row1["cgstvalue"] = "9";
        $_Row1["sgstvalue"] = "9";
        $_Row1["amounttxt"] = $emp->convert_number_to_words($_Row1['Gst_Invoce_TutionFee']) . " Only";
//        $_Row1["amounttxt"]=$_Row1["amounttxt"]."*";
        $_Row1["totalrkcl"] = ($_Row1['Gst_Invoce_TutionFee']) . ".00";
        $_Row1["TuitionFee"] = $_Row1['Gst_Invoce_TutionFee'] . ".00";
        $_Row1["Rkclshare2"] = $_Row1['Gst_Invoce_TutionFee'] . ".00";
        $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'] . '.00';
        $_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
        $_Row1["coursedec"] = $_Row1['Course_Name'] . " (Digital Literacy Course)";
        $_Row1["itgkshare"] = '50.00';




        $fdfContent = '%FDF-1.2
			%Ã¢Ã£Ã?Ã“
			1 0 obj 
			<<
			/FDF 
			<<
			/Fields [';
        foreach ($_Row1 as $key => $val) {
            $fdfContent .= '
					<<
					/V (' . $val . ')
					/T (' . $key . ')
					>> 
					';
        }
        $fdfContent .= ']
			>>
			>>
			endobj 
			trailer
			<<
			/Root 1 0 R
			>>
			%%EOF';
        file_put_contents($filePath, $fdfContent);

        $fdfFilePath = $filePath;
        $fdfPath = str_replace('/', '//', $fdfFilePath);
        $resultFile = '';
        //$defaulPermissionLetterFilePath = getPermissionLetterPath() . '_ITGK_Approval_Letter.pdf';
        // echo "sunil";
      //  $defaulPermissionLetterFilePath = '../upload/CorrectionDuplicatePrintRecpt/_CorrectionDuplicateInvoice.pdf'; //die;
if($_Row1['Gst_Invoce_TotalFee'] == '250'){
        $defaulPermissionLetterFilePath = '../upload/CorrectionDuplicatePrintRecpt/_CorrectionDuplicateInvoice_01_08_19.pdf'; 
}
else{
        $defaulPermissionLetterFilePath = '../upload/CorrectionDuplicatePrintRecpt/_CorrectionDuplicateInvoice.pdf'; //die;
}
        if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
            $defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);
            $resultFile = $learnerCode . '_CorrectionDuplicateInvoice.pdf';

            $newURL = $path . $resultFile;

            //$resultPath = str_replace('/', '//', getPermitionLetterFilePath($learnerCode));
            $resultPath = str_replace('/', '//', $newURL);
            $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            // $pdftkPath = ($_SERVER['HTTP_HOST'] == "click.rkcl.in") ? '"C://inetpub//vhosts//rkcl.in//click.rkcl.in//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            //$pdftkPath = ($_SERVER['HTTP_HOST']=="10.1.1.20") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';

            $command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
            exec($command);

            $emp->addPhotoInPDF($newURL, $_Row1['Admission_Course']);
            unlink($fdfFilePath);
            $filepath5 = 'upload/CorrectionDuplicatePrintRecpt/' . $resultFile;
            echo $filepath5;


            $res = "DONE";
        }
    } else {
        echo "";
    }
**/

if ($_action == "DwnldPrintRecp") {
    $path = '../upload/CorrectionDuplicatePrintRecpt/';
    $learnerCode = $_POST['cid'];
    $file = $_POST['cid'] . '.fdf';
    $filePath = $path . $file;
    $allreadyfoundAkcRecipt = $path . $learnerCode . '_CorrectionDuplicateInvoice.pdf';

    $response = $emp->GetAllPrintRecpDwnld($_POST['cid']);
    
    $co = mysqli_num_rows($response[2]);
    $res = '';
    if ($co) {
        $_Row1 = mysqli_fetch_array($response[2], true);

        $_Row1["invoice_no"] = "CCD/" . $_Row1['invoice_no'];
        $_Row1["invoice_date"] = date("d-m-Y", strtotime($_Row1['addtime']));
        $_Row1["Lname"] = strtoupper($_Row1['cfname']);
        $_Row1['Fname'] = strtoupper($_Row1['cfaname']);
        $_Row1["Lcode"] = $_Row1['lcode'];
        $_Row1["Laddress"] = strtoupper($_Row1['District_Name']);
        $_Row1["Ldob"] = date("d-m-Y", strtotime($_Row1['Admission_DOB']));
        $_Row1["Lmobile"] = $_Row1['Admission_Mobile'];

        $_Row1["Litgk"] = $_Row1['Admission_ITGK_Code'];
        $_Row1["Litgkname"] = strtoupper($_Row1['Organization_Name']);
        $_Row1["SAC"] = $_Row1['Gst_Invoce_SAC'];
        $_Row1["Rkclshare"] = $_Row1['Gst_Invoce_BaseFee'];
        $_Row1["cgst"] = $_Row1['Gst_Invoce_CGST'];
        $_Row1["sgst"] = $_Row1['Gst_Invoce_SGST'];
        $_Row1["cgstvalue"] = "9";
        $_Row1["sgstvalue"] = "9";
        $_Row1["amounttxt"] = $emp->convert_number_to_words($_Row1['Gst_Invoce_TutionFee']) . " Only";
//        $_Row1["amounttxt"]=$_Row1["amounttxt"]."*";
        $_Row1["totalrkcl"] = ($_Row1['Gst_Invoce_TutionFee']) . ".00";
        $_Row1["TuitionFee"] = $_Row1['Gst_Invoce_TutionFee'] . ".00";
        $_Row1["Rkclshare2"] = $_Row1['Gst_Invoce_TutionFee'] . ".00";
        $_Row1["Totalfee"] = $_Row1['Gst_Invoce_TotalFee'] . '.00';
        $_Row1["Totalfee1"] = $_Row1['Gst_Invoce_TotalFee'];
        $_Row1["coursedec"] = $_Row1['Course_Name'] . " (Digital Literacy Course)";
        $_Row1["itgkshare"] = '50.00';




        $fdfContent = '%FDF-1.2
			%Ã¢Ã£Ã?Ã“
			1 0 obj 
			<<
			/FDF 
			<<
			/Fields [';
        foreach ($_Row1 as $key => $val) {
            $fdfContent .= '
					<<
					/V (' . $val . ')
					/T (' . $key . ')
					>> 
					';
        }
        $fdfContent .= ']
			>>
			>>
			endobj 
			trailer
			<<
			/Root 1 0 R
			>>
			%%EOF';
        file_put_contents($filePath, $fdfContent);

        $fdfFilePath = $filePath;
        $fdfPath = str_replace('/', '//', $fdfFilePath);
        $resultFile = '';
        //$defaulPermissionLetterFilePath = getPermissionLetterPath() . '_ITGK_Approval_Letter.pdf';
        // echo "sunil";

if (($_Row1['Gst_Invoce_TotalFee'] == '250'  || $_Row1['Gst_Invoce_TotalFee'] == '450') && $_SESSION['User_UserRoll'] == '7'){
        $defaulPermissionLetterFilePath = '../upload/CorrectionDuplicatePrintRecpt/_CorrectionDuplicateInvoice_01_08_19.pdf'; 
}
else{
        $defaulPermissionLetterFilePath = '../upload/CorrectionDuplicatePrintRecpt/_CorrectionDuplicateInvoice.pdf'; //die;
}

        if (file_exists($fdfFilePath) && file_exists($defaulPermissionLetterFilePath)) {
            $defaulPermissionLetterFilePath = str_replace('/', '//', $defaulPermissionLetterFilePath);
            $resultFile = $learnerCode . '_CorrectionDuplicateInvoice.pdf';

            $newURL = $path . $resultFile;

            //$resultPath = str_replace('/', '//', getPermitionLetterFilePath($learnerCode));
            $resultPath = str_replace('/', '//', $newURL);
            $pdftkPath = ($_SERVER['HTTP_HOST'] == "myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            // $pdftkPath = ($_SERVER['HTTP_HOST'] == "click.rkcl.in") ? '"C://inetpub//vhosts//rkcl.in//click.rkcl.in//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';
            //$pdftkPath = ($_SERVER['HTTP_HOST']=="10.1.1.20") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"';

            $command = $pdftkPath . '  ' . $defaulPermissionLetterFilePath . ' fill_form   ' . $fdfPath . ' output  ' . $resultPath . ' flatten';
            exec($command);

            $emp->addPhotoInPDF($newURL, $_Row1['Admission_Course']);
            // unlink($fdfFilePath);
            // $filepath5 = 'upload/CorrectionDuplicatePrintRecpt/' . $resultFile;
            // echo $filepath5;

            $filepath5 = 'common/showpdfftp.php?src=CorrectionDuplicatePrintRecpt/' . $resultFile;
            unlink($fdfFilePath);
            $directoy = '/CorrectionDuplicatePrintRecpt/';
            $file = $resultFile;
            $response_ftp = ftpUploadFile($directoy, $file, $newURL);
            if(trim($response_ftp) == "SuccessfullyUploaded"){
                
                echo $filepath5;
            }
            $res = "DONE";
        }
    } else {
        echo "";
    }

    function getPermissionLetterPath() {
        $path = '../upload/CorrectionDuplicatePrintRecpt/';
        makeDir($path);

        return $path;
    }

    function getPermitionLetterFilePath($code) {
        $resultFile = $code . '_CorrectionDuplicateInvoice.pdf';
        // $resultPath = getPermissionLetterPath() . $resultFile;
        $resultPath = '../upload/CorrectionDuplicatePrintRecpt/' . $resultFile;

        return $resultPath;
    }

    function makeDir($path) {
        return is_dir($path) || mkdir($path);
    }

}

if ($_action == "delgeninvoice") {

    $path = $_SERVER['DOCUMENT_ROOT'] . '/' . $_POST['values'];
    unlink($path);
}
