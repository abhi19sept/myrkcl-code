<?php

/*
 * author Abhishek

 */
include './commonFunction.php';
require 'BAL/clsReexamApplication.php';

$response = array();
$emp = new clsReexamApplication();

if ($_action == "FILLEXAMEVENT") {
    $response = $emp->GetExamEvent($_POST['values']);
    echo "<option value=''>Select Exam Event</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Affilate_Event'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "FILLEXAMEVENTMANAGE") {
    $response = $emp->GetExamEventManage($_POST['values']);
    echo "<option value=''>Select Exam Event</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Affilate_Event'] . ">" . $_Row['Event_Name'] . "</option>";
    }
}

if ($_action == "FILLREEXAMBATCH") {
    $response = $emp->GetReexamBatch($_POST['examevent'],$_POST['coursename']);
    echo "<option value=''>Select Batch</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FillReExamEvent") {
    $learnerCourseBatch = $emp->GetLearnerCourseBatch($_SESSION['User_LearnerCode']);
    if ($learnerCourseBatch) {
        $response = $emp->GetExamEvent($learnerCourseBatch['Admission_Course']);
        echo "<option value=''>Select Exam Event</option>";
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<option value=" . $_Row['Affilate_Event'] . ">" . $_Row['Event_Name'] . "</option>";
        }
    }
}

function generateLearnerGrid($response, $alreadyApplied = 0) {
    global $emp;

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Course</th>";
    echo "<th style='10%'>Learner Code</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    echo "<th style='12%'>D.O.B</th>";
    echo "<th style='8%'><input type='checkbox' id='checkuncheckall' name='checkuncheckall' value=''> Select All</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['coursename'] . "</td>";
        echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
        echo "<td>" . $_Row['Admission_DOB'] . "</td>";

        if ($_Row['Admission_Payment_Status'] == '1') {
            echo "<td>";
            if (! $alreadyApplied) {
                $isBatchEnableForEvent = 1;
                if (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 19) {
                	$batchResponse = $emp->GetReexamBatch($_Row['exameventid'], $_Row['Admission_Course'], $_Row['Admission_Batch']);
                	$isBatchEnableForEvent = ($batchResponse[0] != Message::NoRecordFound) ? 1 : 0;
                }
                if ($isBatchEnableForEvent) {
                    echo "<input type='checkbox' id=chk" . $_Row['Admission_Code'] . " name='Admission_Codes[]' class='admcodes' value='" . $_Row['Admission_Code'] . "'></input>";
                }
            } else {
                $paidResponse = $emp->CheckIfAlreadyAppliedForReExam($_Row['Admission_LearnerCode'], $_Row['exameventid']);
                if ($paidResponse[0] != Message::NoRecordFound) {
                    $examdata = mysqli_fetch_array($paidResponse[2]);
                    if ($examdata['paymentstatus'] == 0) {
                        echo "Fee Unpaid";
                    } else {
                        echo "Applied Successfully";
                    }
                }
            }
            echo "</td>";
        } 
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "SHOWALL") {
    if (!empty($_POST['ExamEvent'])) {
        $response = $emp->GetAllReexam($_POST['batch'], $_POST['course'], $_POST['ExamEvent']);
        generateLearnerGrid($response);
    }
}

 /* Added By: Sunil for single learner*/

if ($_action == "SHOWApplyingLEARNER") {
    $learnerCourseBatch = $emp->GetLearnerCourseBatch($_SESSION['User_LearnerCode']);
    if ($learnerCourseBatch && !empty($_POST['ExamEvent'])) {
        $response = $emp->GetAllReexam($learnerCourseBatch['Admission_Batch'], $learnerCourseBatch['Admission_Course'], $_POST['ExamEvent'], $_SESSION['User_LearnerCode']);
        $_Response = $emp->CheckIfAlreadyAppliedForReExam($_SESSION['User_LearnerCode'], $_POST['ExamEvent']);
        $alreadyApplied = ($_Response[0] != Message::NoRecordFound) ? 1 : 0;
        generateLearnerGrid($response, $alreadyApplied);
    }
}

if ($_action == "GetLearnerCourseBatch") {
    $learnerCourseBatch = $emp->GetLearnerCourseBatch($_SESSION['User_LearnerCode']);
    echo json_encode($learnerCourseBatch);
}


if ($_action == "ADD") {
    if (isset($_POST['Admission_Codes']) && !empty($_POST['Admission_Codes'])) {
        $_SESSION['AdmissionArray'] = $_POST['Admission_Codes'];
        $response = $emp->addexamdata($_POST['Admission_Codes'], $_POST['ddlExamEvent']);
    } else {
        $response[0] = 'Please select any learner.';
    }

    echo $response[0];
}
	