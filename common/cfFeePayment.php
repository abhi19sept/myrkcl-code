<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsFeePayment.php';
require '../DAL/upload_ftp_doc.php';
$response = array();
$emp = new clsFeePayment();
$_ObjFTPConnection = new ftpConnection();
if ($_action == "ADD") {    
    if (empty($_POST["amounts"]) || !isset($_POST["AdmissionCodes"])) {
        echo "0";
    } else {
        $productinfo = 'LearnerFeePayment';
        $trnxId = $emp->AddPayTran($_POST, $productinfo);
        if ($_POST['gateway'] == 'razorpay' && !empty($trnxId) && $trnxId != 'TimeCapErr') {
            require("razorpay.php");
        } else {
            echo $trnxId;
        }
    }
}

if ($_action == "SHOWALL") {
	global  $_ObjFTPConnection;
    $response = $emp->GetAll($_POST['batch'], $_POST['course'], $_POST['paymode']);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Learner Code</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    echo "<th style='12%'>D.O.B</th>";
    echo "<th style='10%'>Photo</th>";
    echo "<th style='10%'>Sign</th>";

    echo "<th style='10%'>Amount</th>";   
    echo "<th style='8%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $response1 = $emp->GetAdmissionFee($_POST['batch']);
    $_row1 = mysqli_fetch_array($response1[2]);
    $fee = ($_row1['RKCL_Share'] + $_row1['VMOU_Share']);
   
	$_Course = $_POST['course'];
	$_LearnerBatchNameFTP="";

                if ($_Course == 5) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFA';

                } elseif ($_Course == 1) {
                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP;                    

                } elseif ($_Course == 4) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Gov';                    

                }
                elseif ($_Course == 3) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Women';

                }
                elseif ($_Course == 22) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_Jda';

                }
                elseif ($_Course == 26) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SPMRM';

                }
                elseif ($_Course == 27) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_SoftTAD';

                }
                elseif ($_Course == 24) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CFAWomen';

                }               
                
                elseif ($_Course == 23) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CLICK';

                }   

                 elseif ($_Course == 25) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CEE';

                }
                elseif ($_Course == 28) {


                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CAE';

                }
                elseif ($_Course == 29) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CBC';

                }
                elseif ($_Course == 30) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CCS';

                }
                elseif ($_Course == 31) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDM';

                }
                elseif ($_Course == 32) {

                    $_LearnerBatchNameFTP = $_LearnerBatchNameFTP.'_CDP';

                }
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
			
			
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Admission_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_Fname'] . "</td>";
            echo "<td>" . $_Row['Admission_DOB'] . "</td>";
			
			$_LearnerBatchNameFTP2 = str_replace(' ', '_', $_Row['Batch_Name'].$_LearnerBatchNameFTP);
			
            if($_Row['Admission_Photo']!="")
				{
					$image = $_Row['Admission_Photo'];
					echo "<td >"
				. "<input type='button' name='Edit' class='btn btn-primary view_photo' batchname='".$_LearnerBatchNameFTP2."'
			image='".$image."' id='".$_Count.'_photo'."' count='".$_Count."' value='View Photo'/>". "<div style='display:none;' id='Viewphoto_".$_Count."'> </div>"
			."</td>";
					}
			else
				{
					
					echo "<td id='".$_Count.'_photo'."'>" . '<img  alt="No Image Found" width="40" height="55" src="images/user icon big.png"/>' 
							.   "</td>";
				}
			//echo "<div id='".$_Count.'_pic'."' style='display:none;'>" . "<div id='Viewphoto_".$_Count."'> </div>" . "</div>";	
			
			
			
		if($_Row['Admission_Sign']!="")
			{
				$sign = $_Row['Admission_Sign'];
				echo "<td>"
				. "<input type='button' name='Edit' class='btn btn-primary view_sign' batchname='".$_LearnerBatchNameFTP2."'
			image='".$sign."' count='".$_Count."' id='".$_Count.'_sign'."' value='View Sign'/>". "<div style='display:none;' id='Viewsign_".$_Count."'> </div>".
			"</td>";
			}
		else
				{
					echo "<td id='".$_Count.'_sign'."'>" . '<img alt="No Image Found" width="80" height="35" src="images/no_image.png"/>' . "</td>";
				}
		//echo "<div id='".$_Count.'_signature'."' style='display:none;'>" . "<div id='Viewsign_".$_Count."'> </div>" . "</div>";	
		
        

            echo "<td>" . $fee . "</td>";

            if ($_Row['Admission_Payment_Status'] == '0') {
                echo "<td><input type='checkbox' id='chk" . $_Row['Admission_Code'] .
                "' name='AdmissionCodes[]' value='" . $_Row['Admission_Code'] . "'></input></td>";
            } elseif ($_Row['Admission_Payment_Status'] == '8') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='DD in process'>"
                . "</td>";
            } elseif ($_Row['Admission_Payment_Status'] == '1') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='Payment Confirmed'>"
                . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    } else {        
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "Fee") {    
    $response = $emp->GetAdmissionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);
    $RKCLshare = $_row['RKCL_Share'];
    $VMOUShare = $_row['VMOU_Share'];
    $totalshare = $RKCLshare + $VMOUShare;
    echo $totalshare;
}

if ($_action == "FillNetBankingName") {
    $response = $emp->FillNetBankingName();	
    echo "<option value=''>Select Bank</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['BankCode'] . "'>" . $_Row['BankName'] . "</option>";
    }
}

if ($_action == "ValidateCard") {
	
	$key = "Es3Ozb";
	$salt = "o9aZmTfx";
	$command1 = "check_isDomestic";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	
	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
    //$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);		
	$_DataTable = array();
    $_i = 0;
		$_DataTable[$_i] = array("cardType" => $response['cardType'],
                "cardCategory" => $response['cardCategory']);
				echo json_encode($_DataTable);		
 }

 if ($_action == "FinalValidateCard") {	
	$key = "Es3Ozb";
	$salt = "o9aZmTfx";
	$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	//$command1 = "validateCardNumber";   //validatecardnumber   luhn algo
	$var1 = $_POST['values']; // Payu ID (mihpayid) of transaction
	
	$hash_str = $key  . '|' . $command1 . '|' . $var1 . '|' . $salt ;
	$hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key , 'hash' =>$hash , 'command' => $command1, 'var1' => $var1);
    
    $qs= http_build_query($r);
   // $wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsUrl = "https://info.payu.in/merchant/postservice?form=2";
	$response = curlCall($wsUrl,$qs,TRUE);	 
	if($response == 'Invalid'){
		echo "1";
	}
	else if($response == 'valid'){
		echo "2";
	}
 }
 
function curlCall($wsUrl, $qs, $true){
   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
		if (curl_errno($c)) {
		  $c_error = curl_error($c);
		  if (empty($c_error)) {
			  $c_error = 'Some server error';
			}
			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);
		return $arr;
}

if ($_action == "ViewPhoto") {
    //print_r($_POST); die;
    $batchname = $_POST['batchname'];
	$photopath = '/admission_photo/'.$batchname.'/';
    $image = $_POST['image'];
	
    global $_ObjFTPConnection;
	
	$details= $_ObjFTPConnection->ftpdetails();
	
    $photoimage =  file_get_contents($details.$photopath.$image);
$imageData = base64_encode($photoimage);
echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='80' height='110'/>";

    
}

if ($_action == "ViewSign") {
    //print_r($_POST); die;
    $batchname = $_POST['batchname'];
	$photopath = '/admission_sign/'.$batchname.'/';
    $image = $_POST['image'];
	
    global $_ObjFTPConnection;
	
	$details= $_ObjFTPConnection->ftpdetails();
	
    $photoimage =  file_get_contents($details.$photopath.$image);
$imageData = base64_encode($photoimage);
echo "<img id='ftpimg' src='data:image/png;base64, ".$imageData."' alt='Red dot' width='50' height='80'/>";

    
}
?>	