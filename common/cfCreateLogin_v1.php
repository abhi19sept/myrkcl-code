<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCreateLogin.php';

$response = array();
$emp = new clsCreateLogin();


if ($_action == "ADD") {
    //echo "<pre>"; print_r($_POST);die;
    //if (isset($_POST["txtype"]) && !empty($_POST["txtype"])) {
        //$_Type = $_POST["txtype"];
        $_Type = 'DPO';
        
        $response = $emp->GetAllDistrict_Name();
    
        while ($_Row = mysqli_fetch_array($response[2],true)) {
            //print_r($_Row);
            //echo $_Row['District_Code'];
            //echo $_Row['District_Name'];
            $emp->Add($_Type,$_Row['District_Code'],$_Row['District_Name']);
            //echo $response222[0];//die;
        }    
        
    //}
}

if ($_action == "ADDUSER") {
    //echo "<pre>"; print_r($_POST);die;
    //$emp->AddUser($_POST);
    
    if ( isset($_POST["selUserRoll"]) && !empty($_POST["selUserRoll"]) 
            && isset($_POST["txtUsername"]) && !empty($_POST["txtUsername"])  
            && isset($_POST["txtPassword"]) && !empty($_POST["txtPassword"]) 
            && isset($_POST["txtEmail"]) && !empty($_POST["txtEmail"])  
            && isset($_POST["txtMobile"]) && !empty($_POST["txtMobile"]) )
            {
                $User_EmailId = $_POST["txtEmail"];
		$User_MobileNo = $_POST["txtMobile"];
		$User_LoginId = $_POST["txtUsername"];
		$User_Password = $_POST["txtPassword"];
		$User_UserRoll = $_POST["selUserRoll"];
		$User_ParentId = 1;
		$User_Rsp = $_POST["txtRSP"];
		$User_Status=1;
                $User_Type='New';
                $User_Ack='NA';
                $User_CreatedDate=date("Y-m-d h:i:s");
                //$User_Timestamp=$value['User_Timestamp'];
                
                $response = $emp->AddUSERMASTER($User_EmailId, $User_MobileNo, $User_LoginId, $User_Password, $User_UserRoll, $User_ParentId, $User_Rsp, $User_Status, $User_Type, $User_Ack, $User_CreatedDate);
                $_Row2 = mysqli_fetch_array($response[2],true);
                $_usercode = $_Row2['User_Code'];//die;
                
                if ( isset($_POST["Organization_Name"]) && !empty($_POST["Organization_Name"]) 
                    && isset($_POST["selTypeOrg"]) && !empty($_POST["selTypeOrg"])  
                    && isset($_POST["Organization_RegistrationNo"]) && !empty($_POST["Organization_RegistrationNo"]) 
                    && isset($_POST["Organization_Landmark"]) && !empty($_POST["Organization_Landmark"])  
                    && isset($_POST["Organization_PinCode"]) && !empty($_POST["Organization_PinCode"])
                    && isset($_POST["selState"]) && !empty($_POST["selState"])  
                    && isset($_POST["selRegion"]) && !empty($_POST["selRegion"]) 
                    && isset($_POST["selDistrict"]) && !empty($_POST["selDistrict"])  
                    && isset($_POST["selDoctype"]) && !empty($_POST["selDoctype"])
                    )
                    {
                    
                        if(isset($_POST["Organization_lat"]) && $_POST["Organization_lat"]!=''){
                                $Organization_lat = $_POST["Organization_lat"];
                        }else{
                                $Organization_lat = 'NA'; 
                        }

                        if(isset($_POST["Organization_long"]) && $_POST["Organization_long"]!=''){
                                $Organization_long = $_POST["Organization_long"];
                        }else{
                                $Organization_long = 'NA'; 
                        }
                        
                        if(isset($_POST["selGramPanchayat"]) && $_POST["selGramPanchayat"]!=''){
                                $Organization_Gram = $_POST["selGramPanchayat"];
                        }else{
                                $Organization_Gram = 'NA'; 
                        }
                        
                        if(isset($_POST["selVillage"]) && $_POST["selVillage"]!=''){
                                $Organization_Village = $_POST["selVillage"];
                        }else{
                                $Organization_Village = 'NA'; 
                        }
                              
                        
                        
                            $Organization_User=$_usercode;
                            $Organization_Name=$_POST['Organization_Name'];
                            $Organization_RegistrationNo=$_POST['Organization_RegistrationNo'];
                            
                            $Organization_FoundedDate=$_POST['txtEstdate'];
                            $Organization_Course=$_POST['Organization_Course'];
                            $Organization_Type=$_POST['selTypeOrg'];
                            $Organization_DocType=$_POST['selDoctype'];
                            $ScanDoc= $_SESSION['User_LoginId'] . '_' . $_POST['txtGenerateId']. '_' . $Organization_DocType. '.jpg';
            
                            $Organization_ScanDoc=$ScanDoc;
                            $Organization_State=$_POST['selState'];
                            $Organization_Region=$_POST['selRegion'];
                            $Organization_District=$_POST['selDistrict'];
                            
                            $Organization_Tehsil=$_POST['selTehsil'];
                            $Organization_Area=$_POST['Organization_Area'];
                            $Organization_Landmark=$_POST['Organization_Landmark'];
                            $Organization_Road=$_POST['Organization_Road'];
                            
                            $Organization_Street=$_POST['Organization_Street'];
                            $Organization_HouseNo=$_POST['Organization_HouseNo'];
                            $Organization_Country=$_POST['selCountry'];
                            $Organization_Address=$_POST['Organization_Address'];
                            
                            $Organization_PAN=$_POST['Organization_PAN'];
                            $Organization_AreaType=$_POST['arearadio'];
                            $Organization_Mohalla=$_POST['txtMohalla'];
                            $Organization_WardNo=$_POST['txtWard'];
                            
                            $Organization_Police=$_POST['Organization_Police'];
                            $Organization_Municipal=$_POST['Organization_Municipal'];
                            $Organization_Municipal_Type=$_POST['selMuncitype2'];
                            $Organization_Status=1;
                            
                            //$Organization_Village=$value['Organization_Village'];
                            $Organization_Panchayat=$_POST['selPanchayat'];
                            //$Organization_Gram=$_POST['Organization_Gram'];
                            $Organization_PinCode=$_POST['Organization_PinCode'];
                            
                            //$Organization_lat=$value['Organization_lat'];
                            //$Organization_long=$value['Organization_long'];
                            $Org_formatted_address=$_POST['txtEmpAddress'];
                            
                            $response = $emp->AddOrganization($Organization_User, $Organization_Name, $Organization_RegistrationNo, $Organization_FoundedDate, $Organization_Course, $Organization_Type, $Organization_DocType, $Organization_ScanDoc, $Organization_State, $Organization_Region, $Organization_District, $Organization_Tehsil, $Organization_Area, $Organization_Landmark, $Organization_Road, $Organization_Street, $Organization_HouseNo, $Organization_Country, $Organization_Address, $Organization_PAN, $Organization_AreaType, $Organization_Mohalla, $Organization_WardNo, $Organization_Police, $Organization_Municipal, $Organization_Municipal_Type, $Organization_Status, $Organization_Village, $Organization_Panchayat, $Organization_Gram, $Organization_PinCode, $Organization_lat, $Organization_long, $Org_formatted_address);
                            echo $response[0];
                        
                        

                    }
               
            }
    
    
     
}

if ($_action == "ADDUSERold") {
    //echo "<pre>"; print_r($_POST);die;
    //$emp->AddUser($_POST);
    
    if (isset($_POST["selUserRoll"]) && !empty($_POST["selUserRoll"])){
        if (isset($_POST["txtUsername"]) && !empty($_POST["txtUsername"])){
            if (isset($_POST["txtPassword"]) && !empty($_POST["txtPassword"])){
              if (isset($_POST["txtEmail"]) && !empty($_POST["txtEmail"])){
                if (isset($_POST["txtMobile"]) && !empty($_POST["txtMobile"])){
                    
                   if (isset($_POST["Organization_Name"]) && !empty($_POST["Organization_Name"])){
                      if (isset($_POST["selTypeOrg"]) && !empty($_POST["selTypeOrg"])) {
                         if (isset($_POST["Organization_RegistrationNo"]) && !empty($_POST["Organization_RegistrationNo"])) {
                            if (isset($_POST["Organization_Landmark"]) && !empty($_POST["Organization_Landmark"])) {
                               if (isset($_POST["Organization_PinCode"]) && !empty($_POST["Organization_PinCode"])) {
                                   if (isset($_POST["selState"]) && !empty($_POST["selState"])) {
                                       if (isset($_POST["selRegion"]) && !empty($_POST["selRegion"])) {
                                           if (isset($_POST["selDistrict"]) && !empty($_POST["selDistrict"])) {
                                                if (isset($_POST["selDoctype"]) && !empty($_POST["selDoctype"])) {
                                                    
                                                                    $_roll = $_POST["selUserRoll"];
                                                                    $username = strtoupper($_POST["txtUsername"]);
                                                                    $password = $_POST["txtPassword"];
                                                                    $_Email = $_POST["txtEmail"];
                                                                    $_mobile = $_POST["txtMobile"];
                                                                    $_txtRSP = $_POST["txtRSP"];
                                                                    //$_User_Rsp = $_POST["txtRSP"];
                                                                    
                                                                    $_OrgName = $_POST["Organization_Name"];
                                                                    $_OrgType = $_POST["selTypeOrg"];
                                                                    $_OrgRegno = $_POST["Organization_RegistrationNo"];
                                                                    $_Landmark = $_POST["Organization_Landmark"];
                                                                    $_Organization_PinCode = $_POST["Organization_PinCode"];
                                                                    $_selState = $_POST["selState"];
                                                                    $_selRegion = $_POST["selRegion"];
                                                                    $_OrgEstDate = $_POST["txtEstdate"];
                                                                    $_selDoctype= $_POST["selDoctype"];//$_Road,$_Street,$_HouseNo,$_OrgEstDate,$_address,$_docproof
                                                                    
                                                                    $_txtGenerateId = $_POST["txtGenerateId"];
                                                                    $_Organization_Course = $_POST["Organization_Course"];
                                                                    $_HouseNo = $_POST["Organization_HouseNo"];
                                                                    $_Street = $_POST["Organization_Street"];
                                                                    $_Road = $_POST["Organization_Road"];
                                                                    $_Organization_Area = $_POST["Organization_Area"];
                                                                    $_Organization_Address = $_POST["Organization_Address"];
                                                                    $_selCountry = $_POST["selCountry"];
                                                                    
                                                                    $_selDistrict = $_POST["selDistrict"];
                                                                    $_selTehsil = $_POST["selTehsil"];
                                                                    $_Organization_Police = $_POST["Organization_Police"];
                                                                    
                                                                    
                                                                    
                                                                    if(isset($_POST["Organization_lat"]) && $_POST["Organization_lat"]!=''){
                                                                        $_Organization_lat = $_POST["Organization_lat"];
                                                                    }else{
                                                                       $_Organization_lat = 'NA'; 
                                                                    }
                                                                    if(isset($_POST["Organization_long"]) && $_POST["Organization_long"]!=''){
                                                                        $_Organization_long = $_POST["Organization_long"];
                                                                    }else{
                                                                       $_Organization_long = 'NA'; 
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    $_Organization_PAN = $_POST["Organization_PAN"];
                                                                    $_txtEmpAddress = $_POST["txtEmpAddress"];
                                                                    
                                                                    $_arearadio = $_POST["arearadio"];
                                                                    $_selMuncitype = $_POST["selMuncitype"];
                                                                    $_Organization_Municipal = $_POST["Organization_Municipal"];
                                                                    $_txtWard = $_POST["txtWard"];
                                                                    $_txtMohalla = $_POST["txtMohalla"];
                                                                    $_selPanchayat = $_POST["selPanchayat"];
                                                                    
                                                                    if(isset($_POST["selGramPanchayat"]) && $_POST["selGramPanchayat"]!=''){
                                                                        $_selGramPanchayat = $_POST["selGramPanchayat"];
                                                                    }else{
                                                                       $_selGramPanchayat = 'NA'; 
                                                                    }
                                                                    if(isset($_POST["selVillage"]) && $_POST["selVillage"]!=''){
                                                                        $_selVillage = $_POST["selVillage"];
                                                                    }else{
                                                                       $_selVillage = 'NA'; 
                                                                    }
                                                                    //$_selVillage = $_POST["selVillage"];
                                                                    
                                                                    

                                                                    
               
                                                                    
  $response = $emp->AddUser($_roll,$username,$password,$_Email,$_mobile,$_txtRSP, $_OrgName,$_OrgType,$_OrgRegno,$_Landmark,$_Organization_PinCode,$_selState,$_selRegion,$_OrgEstDate,$_selDoctype,$_txtGenerateId,$_Organization_Course,$_HouseNo, $_Street,$_Road,$_OrgRegno,$_Organization_Area,$_Organization_Address,$_selCountry,$_selDistrict,$_selTehsil,$_Organization_Police, $_Organization_lat,$_Organization_long,$_Organization_PAN,$_txtEmpAddress,$_arearadio,$_selMuncitype,$_Organization_Municipal,$_txtWard,$_txtMohalla, $_selPanchayat,$_selGramPanchayat,$_selVillage);
    echo $response[0];
                                                }else{
                                                        echo "Select Document Type";
                                                    }
                                            } 
                                             else {
                                                    echo "Select District";
                                                   }
                                        } 
                                        else {
                                                echo "Select Region";
                                             }
                                    }
                                    else {
                                                        echo "Select State";
                                         }
                                }
                                else {
                                                    echo "Enter Pincode";
                                     }
                            } 
                            else {
                                                echo "Enter Landmark";
                                 }
                        }
                         else {
                                                echo "Enter RegistrationNo";
                              }
                      }
                      else 
                        {
                                                echo "Enter organization Type";
                        }
                    }
                   else 
                    {
                                        echo "Please Enter organization name.";
                    }
                }
                else 
                     {
                                echo "Please Enter Mobile Number.";
                } 
                }
                else 
                    {
                                        echo "Please Enter Email.";
                    } 
                }
            else 
                {
                        echo "Please Enter Password.";
                }
            }
        else 
        {
            echo "Please Enter User Name.";
        }  
    }
    else 
    {
	echo "Please Select User Roll.";
    }
    
    
     
}

 if ($_action == "CVALIDATE"){
        //echo "<pre>";print_r($_POST);die;
        $response = $emp->AddLernerMob($_POST['orgmobile']);
        $last4=substr($_POST['orgmobile'],6);
        echo $result = "OTP sent to your registered mobile number XXXXXX".$last4;
                                
                          
                       
    }
    
    if ($_action == "OTPVALIDATE"){
        //echo "<pre>";print_r($_POST);die;
       if(isset($_POST['txtOTP']) && $_POST['txtOTP']!='')
           {
                 $_otp = $_POST["txtOTP"];
                 $response = $emp->verifyLotp($_POST['orgmobile'],$_otp);

                      if($response=='InvalidOTP')
                        {

                         echo $response;

                        }
                     else
                        {
                         
                         echo "OTPMATCHED";
                         
                        }
           }
       else
       {
          echo "NOTP"; 
       }
                       
    }








/** Get all district */
if ($_action == "FILL") {
    $response = $emp->GetAll($_actionvalue);
    echo "<option value='' >Select District </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['District_Code'] . ">" . $_Row['District_Name'] . "</option>";
    }
}

/** Get all RSP */
if ($_action == "FillRSP") {
    $response = $emp->GetAllRSP($_POST['districtid']);
    echo "<option value='' >Select RSP </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Rsptarget_User'] . ">" . $_Row['Rsptarget_User'] . "</option>";
    }
}

/** Get all USERRSP */
if ($_action == "FillUserRSP") {
    $response = $emp->GetAllUserRSP($_POST['rspid']);
    $_Row = mysqli_fetch_array($response[2]);
    echo  $_Row['User_Code'];
}


/** Get all entity */

if ($_action == "FILLEntity") {
    $response = $emp->GetAllEntity();
    echo "<option value='' >Select Entity</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['entity_code'] . ">" . $_Row['entity_name'] . "</option>";
    }
}

/** Get all UserRoll */
if ($_action == "FillUserRoll") {
    //echo "<pre>"; print_r($_POST);
    $response = $emp->GetAllUserRoll($_POST['entityid']);
    echo "<option value='' >Select User ROll</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['UserRoll_Code'] . ">" . $_Row['UserRoll_Name'] . "</option>";
    }
}


/** Get all country */
if ($_action == "FILLCountry") {
    $response = $emp->GetAllCountry($_actionvalue);
    echo "<option value='' >Select Country</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Country_Code'] . ">" . $_Row['Country_Name'] . "</option>";
    }
}

/** Get all state */
if ($_action == "FillState") {
    //echo "<pre>"; print_r($_POST);
    $response = $emp->GetAllState($_POST['countryid']);
    echo "<option value='' >Select State</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['State_Code'] . ">" . $_Row['State_Name'] . "</option>";
    }
}

/** Get all region */
if ($_action == "FillRegion") {
    //echo "<pre>"; print_r($_POST);
    $response = $emp->GetAllRegion($_POST['stateid']);
    echo "<option value='' >Select Division</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Region_Code'] . ">" . $_Row['Region_Name'] . "</option>";
    }
}


/** Get all district */
if ($_action == "FillDistrict") {
    //echo "<pre>"; print_r($_POST);
    $response = $emp->GetAllDistrict($_POST['regionid']);
    echo "<option value='' >Select District</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['District_Code'] . ">" . $_Row['District_Name'] . "</option>";
    }
}

/** Get all tehsil */
if ($_action == "FillTehsil") {
    //echo "<pre>"; print_r($_POST);
    $response = $emp->GetAllTehsil($_POST['disticrid']);
    echo "<option value='' >Select Tehsil</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Tehsil_Code'] . ">" . $_Row['Tehsil_Name'] . "</option>";
    }
}


/** Get all Area */
if ($_action == "FillArea") {
    //echo "<pre>"; print_r($_POST);
    $response = $emp->GetAllArea($_POST['tehsilid']);
    echo "<option value='' >Select Area Type</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Area_Code'] . ">" . $_Row['Area_Name'] . "</option>";
    }
}


/** Get all tehsil */
if ($_action == "FillMuncitype2") {
    //echo "<pre>"; print_r($_POST);
    $response = $emp->GetAllMuncitype2();
    echo "<option value='' >Select Muncipal Type</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Municipality_Type_Code'] . ">" . $_Row['Municipality_Type_Name'] . "</option>";
    }
}
if ($_action == "FillMuncitype") {
    //echo "<pre>"; print_r($_POST);
    $response = $emp->GetAllMuncitype($_POST['disticrid']);
    echo "<option value='' >Select Muncipal Type</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Municipality_Code'] . ">" . $_Row['Municipality_Name'] . "</option>";
    }
}

if ($_action == "FillMunicipalName") {
    //echo "<pre>"; print_r($_POST);
    $response = $emp->GetMunicipalName($_POST['Municipalid']);
    $_Row = mysqli_fetch_array($response[2]);
    echo  $_Row['Municipality_Raj_Code'];
    
}
if ($_action == "FillWardName") {
    //echo "<pre>"; print_r($_POST);
    $response = $emp->GetWardName($_POST['Municipalid']);
    echo "<option value='' >Select Ward Name</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Ward_Code'] . ">" . $_Row['Ward_Name'] . "</option>";
    }
//    $response = $emp->GetWardName($_POST['Municipalid']);
//    $_Row = mysqli_fetch_array($response[2]);
//    echo  $_Row['Ward_Name'];
    
}

if ($_action == "FILLPanchayat") {
    $response = $emp->GetAllPanchayat($_POST['disticrid']);
    echo "<option value='0' selected='selected'>Select Panchayat Samiti/Block</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Block_Code'] . ">" . $_Row['Block_Name'] . "</option>";
    }
}

if ($_action == "FILLGramPanchayat") {
    $response = $emp->GetAllGramPanchayat($_POST['panchayatid']);
    echo "<option value='00' selected='selected'>Select Gram Panchayat</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['GP_Code'] . ">" . $_Row['GP_Name'] . "</option>";
    }
	echo "<option value='0'>Others</option>";
}

if ($_action == "FILLVillage") {
    $response = $emp->GetAllVillage($_POST['panchayatid']);
    echo "<option value='' >Select Village</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Village_Code'] . ">" . $_Row['Village_Name'] . "</option>";
    }
}

if ($_action == "FILLORGType") {
    $response = $emp->GetAllORGType();
    echo "<option value='' selected='selected'>Select Organization Type</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Org_Type_Code'] . ">" . $_Row['Org_Type_Name'] . "</option>";
    }
}
