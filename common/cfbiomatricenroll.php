<?php
/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsBioMatricEnroll.php';

$response = array();
$emp = new clsBioMatricEnroll();
$category = array();

if ($_action == "FILLCourse") {
    $response = $emp->GetAllCourse();
    echo "<option value='0'>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        if ($_SESSION["batch_value"] == $_Row["Course_Code"]) {
            $sel = "selected";
        } else {
            $sel = '';
        }
        echo "<option value=" . $_Row['Course_Code'] . " $sel >" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FillMachines") {
    $response = $emp->GetAllMachines();
    echo "<option value='n'>- - - Please Select - - -</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        $sel = ($_SESSION["machineID"] == strtolower(substr($_Row['BioMatric_Make'], 0, 1))) ? "selected" : "";
        echo "<option value=" . strtolower(substr($_Row['BioMatric_Make'], 0, 1)) . " $sel >" . $_Row['BioMatric_Make'] . " (Model No. " . $_Row['BioMatric_Model'] . " )" . "</option>";
    }
}

if ($_action == "FillSelectedCourse") {
    $response = $emp->SelectedCourse($_POST['values']);
    //echo "<option value=''>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo $_Row['Course_Name'];
    }
}

if ($_action == "FILLBatch") {
    $response = $emp->Batch($_POST['values']);

    $_SESSION["batch_value"] = $_POST['values'];

    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        $sel = ($_SESSION["batch_value"] == $_Row["Batch_Code"]) ? "selected" : "";
        echo "<option value=" . $_Row['Batch_Code'] . " $sel >" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FillSelectedBatch") {
    $response = $emp->SelectedBatch($_POST['values']);
    //echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo $_Row['Batch_Name'];
    }
}

if ($_action == "SHOWALL") {

    $_SESSION["batch_value"] = $_POST['batch'];
    $_SESSION["course_value"] = $_POST['course'];

    $response = $emp->GetAllLearner($_SESSION["batch_value"], $_SESSION["course_value"]);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Learner Code</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    echo "<th style='12%'>D.O.B</th>";
    //echo "<th style='10%'>Photo</th>";
    //echo "<th style='10%'>Sign</th>";
    echo "<th style='8%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . $_Row['Admission_Name'] . "</td>";
        echo "<td>" . $_Row['Admission_Fname'] . "</td>";
        echo "<td>" . date("F d, Y", strtotime($_Row['Admission_DOB'])) . "</td>";

        if ($_Row['Admission_Photo'] != "") {
            //$image = $_Row['Admission_Photo'];
            //echo "<td>" . '<img alt="No Image Found" style="margin-left:25px;" width="75" height="55" src="upload/admission_photo/'.$image.'"/>' . "</td>";
        } else {
            //echo "<td>" . '<img alt="No Image Found" style="margin-left:25px;" width="75" height="55" src="images/user icon big.png"/>' . "</td>";
        }
        if ($_Row['Admission_Sign'] != "") {
            //$sign = $_Row['Admission_Sign'];
            //echo "<td>" . '<img alt="No Image Found" style="margin-left:25px;"  width="75" height="25" src="upload/admission_sign/'.$sign.'"/>' . "</td>";
        } else {
            //echo "<td>" . '<img alt="No Image Found" style="margin-left:25px;" width="75" height="25" src="images/no_image.png"/>' . "</td>";
        }

        /* echo "<td><a href=javascript:window.open('frmlearnerenroll.php?code=" . $_Row['Admission_Code'] . "&Mode=capture');>"
          . "<input type='button' style='margin-left:25px;' name='capture' id='capture' class='btn btn-primary btn-xs' value='Capture'/></a> </td> "; */
        ?>

        <td>
            <a href="javascript:void(0);"
               onclick="PopupCenter('frmlearnerenroll.php?code=<?php echo $_Row['Admission_Code'] ?>&Mode=capture', 'mywindow', '1300', '500');">
                <input type="button" name="capture" id="capture" class="btn btn-primary btn-xs" value="Capture">
            </a>
        </td>
        <?php
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "GetLearner") {


    $response = $emp->GetLearnerDetails($_POST['values']);

    $_DataTable = "";

    $_Row = mysqli_fetch_array($response[2]);
    echo "<div align='center' class='col-md-2 col-lg-2'>";

    echo "<img style='width:80px; height:107px;' class='img-circle img-responsive' src='upload/admission_photo/" . $_Row['Admission_Photo'] . "'>";
    echo "</div>";


    echo "<div class='table-responsive col-md-8 col-lg-8'>";

    echo "<table class='table table-bordered table-user-information'>";
    echo "<tbody>";

    echo "<tr>";
    echo "<td style='5%'>Learner Code:</td>";
    echo "<td style='30%'>" . $_Row['Admission_LearnerCode'] . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td style='5%'>Learner Name:</td>";
    echo "<td style='30%'>" . $_Row['Admission_Name'] . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td style='5%'>Father Name</td>";
    echo "<td style='10%'>" . $_Row['Admission_Fname'] . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td style='5%'>Date of Birth:</td>";
    echo "<td style='5%'>" . date("F d, Y", strtotime($_Row['Admission_DOB'])) . "</td>";
    echo "</tr>";


    echo "<tr>";
    echo "<td style='5%'>Course Name</td>";
    echo "<td style='10%'>" . $_Row['Course_Name'] . "</td>";
    echo "</tr>";

    echo "<tr>";
    echo "<td style='5%'>Batch Name</td>";
    echo "<td style='10%'>" . $_Row['Batch_Name'] . "</td>";
    echo "</tr>";

    echo "</table>";
    echo "</div>";
}


if ($_action == "Add") {
//         print_r($_POST);
//         die;
    $txtLCourse = $_POST["txtLCourse"];
    $txtLBatch = $_POST["txtLBatch"];
    $txtQuality = $_POST["txtQuality"];
    $txtNFIQ = $_POST["txtNFIQ"];
    $txtIsoTemplate = $_POST["txtIsoTemplate"];

    /*     * * $txtIsoImage = $_POST["txtIsoImage"];
     * $txtRawData = $_POST["txtRawData"];
     * $txtWsqData = $_POST["txtWsqData"]; ** */

    $txtIsoImage = '';
    $txtRawData = '';
    $txtWsqData = '';

    $admission_code = $_POST["admission_code"];


    $response1 = $emp->GetLearnerDetails($_POST['admission_code']);
    $_Row1 = mysqli_fetch_array($response1[2]);
    $txtLCode = $_Row1['Admission_LearnerCode'];
    $txtLName = $_Row1['Admission_Name'];
    $txtLFname = $_Row1['Admission_Fname'];
    $txtLDOB = $_Row1['Admission_DOB'];
//            $_Row['Course_Name']
//            $_Row['Batch_Name']
    $txtLPhoto = $_Row1['Admission_Photo'];


    $response = $emp->AddLearnerDetails($txtLCode, $txtLName, $txtLFname, $txtLDOB, $txtLPhoto, $txtLCourse, $txtLBatch, $txtQuality, $txtNFIQ, $txtIsoTemplate, $txtIsoImage, $txtRawData, $txtWsqData, $admission_code);
    echo $response[0];
}


if ($_action == "SHOWREGISTERLEARNER") {
    $response = $emp->GetAllRegisterLearner($_POST['batch'], $_POST['course']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Learner Code</th>";
    echo "<th style='10%'>Learner Name</th>";
    echo "<th style='8%'>Father Name</th>";
    echo "<th style='12%'>D.O.B</th>";
    echo "<th style='10%'>Photo</th>";
    echo "<th style='10%'>Sign</th>";
    echo "<th style='8%'>Learner Attendance</th>";
    //echo "<th style='8%'>Punch-Out</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;

    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";

        echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
        echo "<td>" . $_Row['Admission_Name'] . "</td>";
        echo "<td>" . $_Row['Admission_Fname'] . "</td>";
        echo "<td>" . $_Row['Admission_DOB'] . "</td>";

        if ($_Row['Admission_Photo'] != "") {
            $image = $_Row['Admission_Photo'];
            echo "<td>" . '<img alt="No Image Found" style="margin-left:25px;" width="75" height="55" src="upload/admission_photo/' . $image . '"/>' . "</td>";
        } else {
            echo "<td>" . '<img alt="No Image Found" style="margin-left:25px;" width="75" height="55" src="images/user icon big.png"/>' . "</td>";
        }
        if ($_Row['Admission_Sign'] != "") {
            $sign = $_Row['Admission_Sign'];
            echo "<td>" . '<img alt="No Image Found" style="margin-left:25px;"  width="75" height="25" src="upload/admission_sign/' . $sign . '"/>' . "</td>";
        } else {
            echo "<td>" . '<img alt="No Image Found" style="margin-left:25px;" width="75" height="25" src="images/no_image.png"/>' . "</td>";
        }

        echo "<td>"
        . "<input type='button' style='margin-left:25px; border-color: green;' name='punchin' id='" . $_Row['Admission_Code'] . "' class='punchin btn btn-success' value='Capture Attendance' /> </td> ";

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "GetVerifyLearnerData") {


    $response = $emp->GetVerifyLearnerDetails($_POST['values']);

    $_Row = mysqli_fetch_array($response[2]);


    $_DataTable = array();
    $_i = 0;
    $_DataTable[$_i] = array("AdmissionCode" => $_Row['Admission_Code'],
        "lname" => $_Row['Admission_Name'],
        "quality" => $_Row['BioMatric_Quality'],
        "nfiq" => $_Row['BioMatric_NFIQ'],
        "template" => $_Row['BioMatric_ISO_Template'],
        "image" => $_Row['BioMatric_ISO_Image'],
        "raw" => $_Row['BioMatric_Raw_Data'],
        "wsq" => $_Row['BioMatric_WSQ_Image']);

    echo json_encode($_DataTable);
}


if ($_action == "In") {
    ///print_r($_POST);

    $txtQuality = $_POST["txtQuality"];
    $txtNFIQ = $_POST["txtNFIQ"];
    //$txtIsoTemplate = $_POST["txtIsoTemplate"];
    // $txtIsoImage = $_POST["txtIsoImage"];
    //$txtRawData = $_POST["txtRawData"];
    //$txtWsqData = $_POST["txtWsqData"];
    $admission_code = $_POST["learnercode"];

    $response = $emp->LearnerIn($txtQuality, $txtNFIQ, $admission_code);
    echo $response[0];
}


if ($_action == "Out") {
    $txtQuality = $_POST["txtQuality"];
    $txtNFIQ = $_POST["txtNFIQ"];
    $txtIsoTemplate = $_POST["txtIsoTemplate"];
    $txtIsoImage = $_POST["txtIsoImage"];
    $txtRawData = $_POST["txtRawData"];
    $txtWsqData = $_POST["txtWsqData"];
    $admission_code = $_POST["admission_code"];

    $responses = $emp->GetMaxAttendanceCode($admission_code);
    $_AttendanceCode = mysqli_fetch_array($responses[2]);

    $response = $emp->LearnerOut($txtQuality, $txtNFIQ, $admission_code, $_AttendanceCode['Code']);
    echo $response[0];
}

if ($_action == "GetExistingLearnerData") {
    $response = $emp->GetExistingLearnerData($_POST['course'], $_POST['batch']);
    $result = mysqli_num_rows($response[2]);
//        print_r($result);
//        die;
    if ($result > 0) {
        $_DataTable = array();
        $_i = 0;
        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Datatable[$_i] = array("BioMatric_ISO_Template" => $_Row['BioMatric_ISO_Template']);
            $_i = $_i + 1;
        }
        //print_r($_DataTable);
        echo json_encode($_Datatable);
    } else {
        echo "";
    }
}

if ($_action == "GetLearnerCourse") {
    $response = $emp->GetLearnerCourse($_POST['values']);
    $_Row = mysqli_fetch_array($response[2]);
    echo $_Row['Admission_Course'];
}

if ($_action == "GetLearnerBatch") {
    $response = $emp->GetLearnerBatch($_POST['values']);
    $_Row = mysqli_fetch_array($response[2]);
    echo $_Row['Admission_Batch'];
}

if ($_action == "GetRegisterDeviceInfo") {
    $response = $emp->GetRegisterDeviceInfo();
    $result = mysqli_num_rows($response[2]);
    if ($result > 0) {
        echo "1";
    } else {
        echo "";
    }
}


if ($_action == "GetLearnerByPIN") {

    //print_r($_POST);
    $response = $emp->GetLearnerByPIN($_POST['biopin']);

    //print_r($response);

    if ($response[0] == "Success") {
        $_Row = mysqli_fetch_array($response[2]);
        $_DataTable = array();
        $_i = 0;

        $getLearnerAttendanceForToday = $emp->GetMaxAttendanceCode($_Row['BioMatric_Admission_LCode']);
        $_Row1 = mysqli_fetch_array($getLearnerAttendanceForToday[2]);
        $dbDate = explode(" ", $_Row1["Attendance_In_Time"]);

        /* if ($dbDate[0] == date("Y-m-d")) {
          $attendanceFlag = "Attendance taken on ". date("F d, Y", strtotime($_Row1["Attendance_In_Time"]))." at ".date("h:i A", strtotime($_Row1["Attendance_In_Time"]));
          } else { */
        $attendanceFlag = 0;
        //}

        $_DataTable[$_i] = array("AdmissionCode" => $_Row['BioMatric_Admission_Code'],
            "quality" => $_Row['BioMatric_Quality'],
            "nfiq" => $_Row['BioMatric_NFIQ'],
            "template" => $_Row['BioMatric_ISO_Template'],
            "image" => $_Row['BioMatric_ISO_Image'],
            "lcode" => $_Row['BioMatric_Admission_LCode'],
            "lname" => $_Row['BioMatric_Admission_Name'],
            "lfname" => $_Row['BioMatric_Admission_Fname'],
            "lphoto" => $_Row['BioMatric_Admission_Photo'],
            "ldob" => $_Row['BioMatric_Admission_DOB'],
            "lcourse" => $_Row['Course_Name'],
            "lbatch" => $_Row['Batch_Name'],
            "attendance_flag" => $attendanceFlag);

        echo json_encode($_DataTable);
    } else {
        echo "invalidpin";
    }
}
if ($_action == "LearnerDeregister") {
    if ($_POST['deregreason'] == '') {
        echo "0";
    } else {
        //print_r($_POST);
//for log creation 
        $responseGetLearner = $emp->GetLearnerByLID($_POST['lcode']);
        if ($responseGetLearner[0] == "Success") {
            $_Row = mysqli_fetch_array($responseGetLearner[2]);
            $OldIso_template = $_Row['BioMatric_ISO_Template'];
            $itgkcode = $_Row['BioMatric_ITGK_Code'];

            //code to get Lcore at time of deregister
            ini_set("display_errors", "on");
            ini_set("error_reporting", E_ERROR);
            require_once("../nusoap/nuSOAP/lib/nusoap.php");
            define("SERVER_WSDL_API", "http://ilearn.myrkcl.com/nusoap/toc.php?wsdl");
            //define("SERVER_WSDL_API", "http://localhost/toc/nusoap/toc.php?wsdl");   
            $key = "RKCL_ILEARN";
            $server_ip = '49.50.79.170';
            $cname = 'RKCLILEARN';
            $client = new nusoap_client(SERVER_WSDL_API, true, false, false, false, false, 300, 300);
            $client->soap_defencoding = 'UTF-8';
            $client->decode_utf8 = false;
            ini_set('default_socket_timeout', 5000);
            $lcode = $_POST["lcode"];
            $result = $client->call("getLearnerAssessementFinalResult", array("cname" => $cname, "hashkey" => $key, "clientip" => $server_ip, "lcode" => $lcode));
            $num_rowsSco = json_decode(base64_decode($result), true);

//code to get Lcore at time of deregister
// code for get learner attandace at the time of derigester
            $response2 = $emp->GetAttendanceCode($_POST["lcode"], $itgkcode);
// print_r($response2);
            if ($response2[0] == "Success") {
                $_Row1 = mysqli_fetch_array($response2[2]);
                $attnd = $_Row1['Days'];
            } else {
                $attnd = "0";
            }
            $deregreason = trim($_POST['deregreason']);

            $responseUpdAdmLog = $emp->UpdAdmLogLearnerDeregister($_POST['lcode'], $OldIso_template, $itgkcode, $num_rowsSco, $deregreason, $attnd);
        }
        //for log creation 
        $response = $emp->LearnerDeregister($_POST['lcode']);
        echo $response[0];
    }
}
if ($_action == "GetLearnerByLID") {

    //print_r($_POST);
    $response = $emp->GetLearnerByLID($_POST['lcode']);
    //print_r($response);

    if ($response[0] == "Success") {
        $_Row = mysqli_fetch_array($response[2]);
        $_DataTable = array();
        $_i = 0;
        $_DataTable[$_i] = array("AdmissionCode" => $_Row['BioMatric_Admission_Code'],
            "quality" => $_Row['BioMatric_Quality'],
            "nfiq" => $_Row['BioMatric_NFIQ'],
            "template" => $_Row['BioMatric_ISO_Template'],
            "image" => $_Row['BioMatric_ISO_Image'],
            "itgkcode" => $_Row['BioMatric_ITGK_Code'],
            "lcode" => $_Row['BioMatric_Admission_LCode'],
            "lname" => $_Row['BioMatric_Admission_Name'],
            "lfname" => $_Row['BioMatric_Admission_Fname'],
            "lphoto" => $_Row['BioMatric_Admission_Photo'],
            "ldob" => $_Row['BioMatric_Admission_DOB'],
            "lcourse" => $_Row['Course_Name'],
            "lbatch" => $_Row['Batch_Name']);

        echo json_encode($_DataTable);
    } else {
        echo "1";
    }
}

if ($_action == "setMachine") {
    unset($_SESSION["machineID"]);
    $_SESSION["machineID"] = $_POST["value"];
    echo "machine id has been set";
}

if ($_action == "CheckCaptureEvent") {
    $response = $emp->GetLearnerByPIN($_POST['biopin']);
    $_Row = mysqli_fetch_array($response[2]);
    $coursecode = $_Row['BioMatric_Admission_Course'];
    $batchcode = $_Row['BioMatric_Admission_Batch'];
    $response2 = $emp->CheckCaptureEvent($coursecode, $batchcode);
    if ($response2[0] == 'Success') {
        echo "1";
    } else {
        echo "0";
    }
}

if ($_action == "SHOWRPT") {
    if ($_POST['cat'] == '2') {

        $response = $emp->GetRptByLearner($_POST['batch'], $_POST['course']);


        $_DataTable = "";
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='10%'>S No.</th>";
        echo "<th style='10%'>Learner Code</th>";
        echo "<th style='10%'>Learner Name</th>";
        echo "<th style='8%'>Father Name</th>";
        echo "<th style='8%'>Learner Mobile</th>";
        echo "<th style='12%'>IT-GK</th>";
        echo "<th style='10%'>Deregister Count</th>";
        echo "<th style='8%'>Learner Attendance(in Days)</th>";
        echo "<th style='8%'>Learner LMS Score</th>";
        echo "<th style='10%'>Reason</th>";
        //echo "<th style='8%'>Punch-Out</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;

        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";

            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
            echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
            echo "<td>" . $_Row['Adm_Data_Log_ITGK'] . "</td>";
            echo "<td>" . $_Row['ct'] . "</td>";
            echo "<td>" . $_Row['Adm_Data_Log_Attendance'] . "</td>";
            echo "<td>" . $_Row['Adm_Data_Log_LMSscore'] . "</td>";
            echo "<td>" . $_Row['Adm_Data_Log_Reason'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    } else {
        $response = $emp->GetRptByITGK($_POST['batch'], $_POST['course']);


        $_DataTable = "";
        echo "<div class='table-responsive'>";
        echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th style='10%'>S No.</th>";

        echo "<th style='12%'>IT-GK Code</th>";
        echo "<th style='12%'>IT-GK Name</th>";
        echo "<th style='12%'>IT-GK Mobile</th>";
        echo "<th style='12%'>SP Name</th>";
        echo "<th style='10%'>Total Deregister Count</th>";
        echo "<th style='10%'>Total Admission Count</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $_Count = 1;

        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr>";
            echo "<td>" . $_Count . "</td>";

            echo "<td>" . $_Row['Adm_Data_Log_ITGK'] . "</td>";
            echo "<td>" . ($_Row['ITGK_Name']) . "</td>";
            echo "<td>" . ($_Row['ITGKMOBILE']) . "</td>";
            echo "<td>" . $_Row['RSP_Name'] . "</td>";
            echo "<td>" . $_Row['ct'] . "</td>";
            echo "<td>" . $_Row['tct'] . "</td>";
            echo "</tr>";
            $_Count++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
}

if ($_action == "GetAllCourseRpt") {
    $response = $emp->GetAllCourseRpt();
    echo "<option value='0'>Select Courses</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {

        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Courseitgk_Course'] . "</option>";
    }
}

if ($_action == "FILLBatchRpt") {
    $response = $emp->BatchRpt($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}
    
    
if ($_action == "GetRegMachineSrNo") {
    $response = $emp->GetAllMachines();

            $response_return = [];
            if ($response[0] == "Success") {
                while ($Result = mysqli_fetch_array($response[2])) {
                  //  $response_return["error"] = FALSE;
                   
                    $response_return[] = array("make" => $Result['BioMatric_Make'], "srno" => $Result['BioMatric_SerailNo']);
                }
            } else {
                $response_return["error"] = TRUE;
                $response_return["error_msg"] = "No Macine Available";
            }
            //print_r($response_return);
            //die;
            echo json_encode($response_return);
}
    