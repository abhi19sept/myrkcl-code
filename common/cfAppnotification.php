<?php

/*
 * author HariOm

 */
include "./appcommonFunction.php";
include './commonFunction.php';
require '../BAL/clsAppnotification.php';
require "../common/cfJWTHelper.php";
require "../common/validation.class.php";
require '../include/constants.php';

$response = array();
$cfRM = new clsAppNotification();
$validator = new VALIDATION();
$valid_for = 3600;
if ($_action === "createToken") {
    $rules = [];
    $filters = [];
    if (isset($_POST["token"]) && trim($_POST['token'] == "")) {
        $_POST = $validator->sanitize($_POST);

        /*         * * SERVER SIDE VALIDATION STARTS HERE ** */
        if (in_array($_POST["mode"], ["UPDATE", "ADD"])) {

            $rules = [
                "notificationTittle" => "required",
                "notificationType" => "required",
                "notificationUsers" => "required",
                "notificationMessage" => "required"
            ];

            $filters = [
                'notificationTittle' => 'trim|sanitize_string',
                'notificationType' => 'trim|sanitize_string',
                'notificationUsers' => 'trim|sanitize_string',
                'notificationMessage' => 'trim|sanitize_string'
            ];
        }



        if (in_array($_POST["mode"], ["UPDATE", "DELETE"])) {

            $rules = array_merge($rules, array(
                'Code' => 'required'
            ));

            $filters = array_merge($filters, array(
                'Code' => 'trim'
            ));
        }
        /*         * * SERVER SIDE VALIDATION ENDS HERE ** */


        
        
        
        /*** BEGIN FORM VALIDATION HERE ***/
        
        $validated_data = $validator->validate($_POST, $rules);


        if ($validated_data === TRUE) {
            $_Response = [];

            $token = [];

            $token['exp'] = time() + $valid_for;

            if (in_array($_POST["mode"], ["UPDATE", "ADD", "DELETE"])) {
                /*                 * * GENERATING TOKEN HERE ** */
                foreach ($_POST as $key => $value) {
                    $token[$key] = $value;
                }
            }

            if (in_array($_POST["mode"], ["UPDATE", "DELETE"])) {// using for auto incremented ID.
                $token["Code"] = $_POST['Code'];
            }

            $jwt = JWT::encode($token, SECRET_KEY);

            $encodedJWT = $jwt;
            $_Response["generatedToken"] = $encodedJWT;
            $_Response["tokenGenerationStatus"] = "Success";

            echo json_encode($_Response);
        } else {
            $message = [];
            foreach ($validated_data as $v) {
                $field = $v['field'];
                $param = $v['param'];
                $rule = $v['rule'];


                switch ($v['field']) {
                    case 'notificationTittle':
                        $field = "Title";
                        break;
                    case 'notificationType':
                        $field = "Type";
                        break;
                    case 'notificationUsers':
                        $field = "Users";
                        break;
                }

                if (array_key_exists($rule, $fields)) {
                    $message[] = str_replace('{param}', $param, str_replace('{field}', $field, $fields[$v['rule']]));
                }
            }

            $_Response["generatedToken"] = "";
            $_Response["tokenGenerationStatus"] = $message;
            echo json_encode($_Response);
        }
    } else {

        /*         * * IF TOKEN HAS BEEN CREATED PROCEED HERE ** */

        $postedArray = [];

        /*         * * IF ADD ** */
        if (strtoupper($_POST["mode"]) === "ADD") {


            $_Response = [];
            $postedArray = [];
            $receivedJWT = trim($_POST['token']);
            $signatureMatch = json_decode(JWT::decode($receivedJWT, SECRET_KEY));

          
         
            
            if ($signatureMatch->message == "Success") {

                foreach ($signatureMatch->payload as $key => $value) {
                    $postedArray[$key] = $value;
                }
                $_Response = $cfRM->Add($postedArray);
                $_Response["status"] = ($_Response[0] == Message::SuccessfullyUpdate) ? "Success" : "";
                //echo_pre($_Response,true);
                
            } else {
                $_Response["status"] = "Token Mismatch Error";
            }
            echo json_encode($_Response);
        }
    }
}

if ($_action == "FILLNOTIFICATION") {
    
    $response = $cfRM->GetAllNotificationType();
    echo "<option value=''>--Select Type--</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Ntype_id'] . ">" . $_Row['Ntype'] . "</option>";
    }
    
}

if ($_action == "FILLDISTRICT") {
    $response = $cfRM->GetDistrict();
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['District_Code'] . ">" . $_Row['District_Name'] . "</option>";
    }
}

if ($_action == "FILLBYENTITY") {
    $response = $cfRM->GetByEntity();
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['UserRoll_Code'] . ">" . $_Row['UserRoll_Name'] . "</option>";
    }
}

if($_action == "FillUserNotification") 
{  
    $UserRoll = $_POST['UserRoll'];
    $response = $cfRM->FillUserNotification($UserRoll);
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_LoginId'] . ">" . $_Row['User_LoginId'] . "</option>";
    }
   
}

if($_action == "FillUserNotificationForSP") 
{  
    $UserSPD = $_POST['UserSPD'];
    $response = $cfRM->FillUserNotificationForSP($UserSPD);
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_LoginId'] . ">" . $_Row['User_LoginId'] . " (". $_Row['Organization_Name'] .")</option>";
    }
   
}

if($_action == "FillUserNotificationForITGK") 
{  
    $UserTIGK = $_POST['UserTIGK'];
    $course = $_POST['course'];
    $response = $cfRM->FillUserNotificationForITGK($UserTIGK,$course);
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_LoginId'] . ">" . $_Row['User_LoginId'] . " (". $_Row['Organization_Name'] .")</option>";
    }
   
}
if($_action == "FillUserNotificationForITGKALL") 
{  
    $course = $_POST['course'];
    $response = $cfRM->FillUserNotificationForITGKALL($course);
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_LoginId'] . ">" . $_Row['User_LoginId'] . " (". $_Row['Organization_Name'] .")</option>";
    }
   
}

if ($_action == "FILLALLUSERNOTIFICATION") {
    
    $response = $cfRM->GetAllUSER();
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['User_LoginId'] . ">" . $_Row['User_LoginId'] . "</option>";
    }
    
}

if ($_action == "FILLCourseName") {
    $response = $cfRM->GetAllCourse();
    echo "<option value=''>--Select Course--</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . " name=" . $_Row['Course_Name'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLCourseNameforitgk") {
    $response = $cfRM->GetAllCourse();
    echo "<option value=''>--Select Course--</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Name'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLALLBatch") {
    
    $response = $cfRM->ALLBatch($_POST['course']);
    echo "<option value='' selected='selected'>--Select Batch--</option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}

if ($_action == "FillSELETEDLEARNER") {
    
   $UserTIGK =  $_POST['UserTIGK'];
   $course =  $_POST['course'];
   $batch =  $_POST['batch'];
    $response = $cfRM->ALLSelectedLearner($UserTIGK,$course,$batch);
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Admission_LearnerCode'] . ">" . $_Row['Admission_LearnerCode'] . " (" . $_Row['Admission_Name'] . ")</option>";
    }
}
