<?php

session_start();

//$_SESSION['ImageFile'] = "";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

//print_r($_POST);
//die();
$_UploadDirectory1 = '../upload/Bankdocs/';
$_UploadDirectory2 = '../upload/pancard/';

//$name = explode(",", $_POST['UploadId']);
//$_time = $name[0];
//$_Proof = $name[1];
//echo $_POST['Centercode'];
//echo $_POST['UploadId'];
//die();
$parentid = $_POST['Centercode'] . '_' . $_POST['UploadId'];
$docname = $_POST['Centercode'] . '_' . $_POST['UploadId'] . '_' . $_POST['UploadDocName'];



if (isset($_FILES["SelectedFile2"])) {
    if ($_FILES["SelectedFile2"]["name"] != '') {
        $imag = $_FILES["SelectedFile2"]["name"];
        $imageinfo = pathinfo($_FILES["SelectedFile2"]["name"]);
        if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
            $error_msg = "Image must be in either PNG or JPG or JPEG Format";
            $flag = 0;
        } else {
            if (file_exists("$_UploadDirectory2/" . $parentid . '_pancard' . '.' . substr($imag, -3))) {
                $_FILES["SelectedFile2"]["name"] . "already exists";
            } else {
                if (file_exists($_UploadDirectory2)) {
                    if (is_writable($_UploadDirectory2)) {
                        move_uploaded_file($_FILES["SelectedFile2"]["tmp_name"], "$_UploadDirectory2/" . $parentid . '_pancard' . '.jpg');
                        //session_start();
                        //$_SESSION['SignFile'] =  $parentid .'_sign'.'.png';
                    } else {
                        outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
                    }
                } else {
                    outputJSON("<span class='error'>Upload Folder Not Available</span>");
                }
            }
        }
    }
}

if ($_FILES["SelectedFile1"]["name"] != '') {
    $imag = $_FILES["SelectedFile1"]["name"];
    $imageinfo = pathinfo($_FILES["SelectedFile1"]["name"]);
    if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
        $error_msg = "Image must be in either PNG or JPG or JPEG Format";
        $flag = 0;
    } else {
        if (file_exists("$_UploadDirectory1/" . $docname . '_bankdocs' . '.' . substr($imag, -3))) {
            $_FILES["SelectedFile1"]["name"] . "already exists";
        } else {
            if (file_exists($_UploadDirectory1)) {
                if (is_writable($_UploadDirectory1)) {
                    move_uploaded_file($_FILES["SelectedFile1"]["tmp_name"], "$_UploadDirectory1/" . $docname . '.jpg');
                    //session_start();
                    //$_SESSION['PaymentReceiptFile'] =  $docname .'_bankdocs'.'.jpg';
                } else {
                    outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
                }
            } else {
                outputJSON("<span class='error'>Upload Folder Not Available</span>");
            }
        }
    }
}
				 
				 
				