<?php

/* 
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsUserProfile.php';

$response = array();
$emp = new clsUserProfile();


if ($_action == "ADD") {
    if (isset($_POST["userfirstname"])) {

        $_UserInitial = $_POST["userinitial"];
        $_UserFname = $_POST["userfirstname"];
        $_UserMname = $_POST["usermidname"];
        $_UserLname = $_POST["userlastname"];
        $_UserGender = $_POST["usergender"];
        $_UserTagName = $_POST["usertagname"];
        $_UserParentName = $_POST["userparentname"];
        $_UserOccupation = $_POST["useroccupation"];
        $_UserMobile = $_POST["usermobile"];
        $_UserSTDcode = $_POST["userstdcode"];
        $_UserPhone = $_POST["userphone"];
        $_UserWebsite = $_POST["userwebsite"];
        $_UserFBID = $_POST["userfacebookid"];
        $_UserTWID = $_POST["usertwitterid"];
        $_UserHouseno = $_POST["userhouseno"];
        $_UserStreet = $_POST["userstreet"];
        $_UserRoad = $_POST["userroad"];
        $_UserLandmark = $_POST["userlandmark"];
        $_UserArea = $_POST["selArea"];
        $_UserPhoto = $_POST["userphoto"];
        $_UsePANno = $_POST["userpanno"];
        $_UserAadharNo = $_POST["useraadharno"];
        $_UserKYC = $_POST["userkyc"];

        $response = $emp->Add($_UserInitial,$_UserFname,$_UserMname,$_UserLname,$_UserGender,$_UserTagName,$_UserParentName,$_UserOccupation,$_UserMobile,$_UserSTDcode,$_UserPhone,$_UserWebsite,$_UserFBID,$_UserTWID,$_UserHouseno,$_UserStreet,$_UserRoad,$_UserLandmark,$_UserArea,$_UserPhoto,$_UsePANno,$_UserAadharNo,$_UserKYC);
        echo $response[0];
    }
}

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Owner Name</th>";
    echo "<th style='20%'>Owner DOB</th>";
    echo "<th style='20%'>Owner Address</th>";
    echo "<th style='10%'>Owner Qualification</th>";
    echo "<th style='10%'>Owner Mobile No.</th>";
    echo "<th style='10%'>Owner Email ID.</th>";
   
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	 echo "</div>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['UserProfile_FirstName'] . "</td>";
         echo "<td>" . $_Row['UserProfile_DOB'] . "</td>";
         echo "<td>" . $_Row['UserProfile_Address'] . "</td>";
         echo "<td>" . $_Row['UserProfile_Qualification'] . "</td>";
         echo "<td>" . $_Row['UserProfile_Mobile'] . "</td>";
         echo "<td>" . $_Row['UserProfile_Email'] . "</td>";
        
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}