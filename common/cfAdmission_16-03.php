<?php

//session_start
/*
 *  author Viveks
 */

include 'commonFunction.php';
require 'BAL/clsAdmission.php';

$response = array();
$emp = new clsAdmission();
//print_r($_POST);

if ($_action == "ADD") {
      //print_r($_POST);
    if (isset($_POST["lname"])) {
        if (isset($_POST["parent"])) {
			
			$_LearnerName = $_POST["lname"];
			$_ParentName = $_POST["parent"];
			$_IdProof = $_POST["idproof"];
			$_IdNo = $_POST["idno"];
			$_DOB = $_POST["dob"];
			$_MotherTongue = $_POST["mothertongue"];
			$_Medium = $_POST["medium"];
			$_Gender = $_POST["gender"];
			$_MaritalStatus = $_POST["marital_type"];
			$_District = $_POST["district"];
			$_Tehsil = $_POST["tehsil"];
			$_Address = $_POST["address"];
			$_PIN = $_POST["pincode"];
			$_ResiPh = $_POST["resiph"];
			$_Mobile = $_POST["mobile"];
			$_Qualification = $_POST["qualification"];
			$_LearnerType = $_POST["learnertype"];
			$_PhysicallyChallenged = $_POST["physicallychallenged"];
			$_GPFno = $_POST["GPFno"];
			$_Email = $_POST["email"];
			$_LearnerCourse = $_POST["coursecode"];
			$_LearnerBatch = $_POST["batchcode"];
			$_LearnerFee = $_POST["fee"];
			$_LearnerInstall = $_POST["install"];
			$_Photo = $_POST["lphoto"];
			$_Sign = $_POST["lsign"];
			$_Scan = $_POST["lscan"];
			// echo $_LearnerName.$_ParentName.$_IdProof.$_IdNo.$_DOB.$_MotherTongue.$_Medium.$_Gender.$_MaritalStatus . $_District.$_Tehsil.$_Address.$_ResiPh.$_Mobile.$_Qualification.$_LearnerType.$_GPFno.$_PhysicallyChallenged.$_Email.$_LearnerPhoto.$_LearnerSign.$_PIN;

			$response = $emp->Add($_LearnerName, $_ParentName, $_IdProof, $_IdNo, $_DOB, $_MotherTongue, $_Medium, $_Gender, $_MaritalStatus, $_District, $_Tehsil, $_Address, $_ResiPh, $_Mobile, $_Qualification, $_LearnerType, $_GPFno, $_PhysicallyChallenged, $_Email, $_PIN, $_LearnerCourse, $_LearnerBatch, $_LearnerFee, $_LearnerInstall, $_Photo, $_Sign, $_Scan);
			echo $response[0];
           
        } else {
            echo Message::RLearnerFName;
        }
    } else {
        echo Message::RLearnerName;
    }
}

if ($_action == "UPDATE") {
	//print_r($_POST);
	//print_r($_FILES);
	
	$_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_photo/';
	$_UploadDirectory2 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_sign/';
	
	$_LearnerName = $_POST["txtlname"];
	$_ParentName = $_POST["txtfname"];
	$_DOB = $_POST["dob"];                        
	$_Code = $_POST['txtAdmissionCode'];
	$_GeneratedId = $_POST['txtGenerateId'];
	
	if($_FILES["p1"]["name"]!='') {					     
				$imag =  $_FILES["p1"]["name"];
				$newpicture = $_GeneratedId .'_photo'.'.png';
				$_FILES["p1"]["name"] = $newpicture;
	  	}
	else  {					   
			$newpicture = $_POST['txtphoto'];
		}

	if($_FILES["p1"]["name"]!='') {
		$imageinfo = pathinfo($_FILES["p1"]["name"]);		
		if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg") {
			$error_msg = "Image must be in either PNG or JPG Format";			
		 }
		else {	
			if (file_exists("$_UploadDirectory1/" .$_GeneratedId .'_photo'.'.png'))			 
				{ 
					unlink("$_UploadDirectory1/".$newpicture); 
				}
			if (file_exists($_UploadDirectory1)) {
				if (is_writable($_UploadDirectory1)) {
				  move_uploaded_file($_FILES["p1"]["tmp_name"], "$_UploadDirectory1/".$newpicture);							 
			 }
		  }
		}
	 }	
	 
	 
	 if($_FILES["p2"]["name"]!='') {					     
				$imag =  $_FILES["p2"]["name"];
				$newsign = $_GeneratedId .'_sign'.'.png';
				$_FILES["p2"]["name"] = $newsign;
	  	}
	else  {					   
			$newsign = $_POST['txtsign'];
		}

	if($_FILES["p2"]["name"]!='') {
		$imageinfo = pathinfo($_FILES["p2"]["name"]);		
		if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg") {
			$error_msg = "Image must be in either PNG or JPG Format";			
		 }
		else {	
			if (file_exists("$_UploadDirectory2/" .$_GeneratedId .'_sign'.'.png'))			 
				{ 
					unlink("$_UploadDirectory2/".$newsign); 
				}
			if (file_exists($_UploadDirectory2)) {
				if (is_writable($_UploadDirectory2)) {
				  move_uploaded_file($_FILES["p2"]["tmp_name"], "$_UploadDirectory2/".$newsign);							 
			 }
		  }
		}
	 }	
					
	$response = $emp->Update($_LearnerName, $_ParentName, $_DOB, $newpicture, $newsign, $_Code);
	echo $response[0];
	
}

if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_actionvalue);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("AdmissionCode" => $_Row['Admission_Code'],
            "lname" => $_Row['Admission_Name'],
            "fname" => $_Row['Admission_Fname'],
            "dob" => $_Row['Admission_DOB'],
            "photo" => $_Row['Admission_Photo'],
            "sign" => $_Row['Admission_Sign']);
        //$_SESSION['ImageFileEdit'] = $_Row['Admission_Photo'];
        //$_SESSION['SignFileEdit'] = $_Row['Admission_Sign'];

        $_i = $_i + 1;
    }
    echo json_encode($_DataTable);
}

if ($_action == "Fee") {
    // print_r($_POST);
    $response = $emp->GetAdmissionFee($_POST['codes']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Course_Fee'];
}


if ($_action == "Install") {
    //echo "call";
    $response = $emp->GetAdmissionInstall($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Admission_Installation_Mode'];
}

if ($_action == "FILLIntake") {
    //echo "call";
    $response = $emp->GetIntakeAvailable($_POST['values']);
    $_row = mysqli_fetch_array($response[2]);
    //print_r($response[2]);
    echo $_row['Intake_Available'];
}



