<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsOwnershipChangeApproval.php';

$response = array();
$emp = new clsOwnershipChangeApproval();



if ($_action == "ShowDetails") {
    $response = $emp->GetAll();
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>New IT-GK Name</th>";
    echo "<th>Ownership Change Ack No</th>";
    echo "<th>IT-GK Code</th>";
    
    echo "<th>IFSC Code</th>";
    echo "<th>Account Holder Name</th>";
    echo "<th>Account Number</th>";
    echo "<th>Bank Name</th>";
    echo "<th>Branch Name</th>";
    echo "<th>MICR Code</th>";
    echo "<th>Account Type</th>";
    echo "<th>Application Date</th>";
    echo "<th>Message</th>";
    echo "<th>Application Status</th>";
     echo "<th>Re-Submitted Date</th>";
    echo "<th>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";

    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . strtoupper($_Row['Organization_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Ack']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Itgk_Code']) . "</td>";
        
        echo "<td>" . strtoupper($_Row['Bank_Ifsc_code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Number']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Branch_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Micr_Code']) . "</td>";
        echo "<td>" . strtoupper($_Row['Bank_Account_Type']) . "</td>";
        echo "<td>" . date('d-m-Y', strtotime($_Row['Org_App_Date'])) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Message']) . "</td>";
        echo "<td>" . strtoupper($_Row['Org_Edited_Status']) . "</td>";
        echo "<td>" . ($_Row['Org_Last_Updated_Date']?date('d-m-Y', strtotime($_Row['Org_Last_Updated_Date'])):'NA') . "</td>";
//        if($_Row['Org_Message'] != ''){
//        echo "<td style='color:red'>On Hold</td>";    
//        } elseif($_Row['Org_Message'] == ''){
//        echo "<td>New</td>";   
//        } elseif($_Row['Org_Application_Approval_Status'] == 'Approved'){
//         echo "<td style='color:green'>Approved</td>";   
//        }
        
        if ($_Row['Org_Application_Approval_Status'] == 'Pending') {
        if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 9) {
            echo "<td> <a href='frmprocessownershipchangerequest.php?code=" . $_Row['Organization_Code'] . "&Mode=Edit&cc=" . $_Row['Org_Itgk_Code'] ."&ack=" . $_Row['Org_Ack'] ."'>"
            . "<input type='button' name='Approve' id='Approve' class='btn btn-danger' value='Approve'/></a>"
            . "</td>";
        } else if ($_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8') {
            echo "<td>"
            . "Pending for Ownership Change Approval"
            . "</td>";
        } else {
            echo "<td>"
            . "Rejected"
            . "</td>";
        }
        } else if ($_Row['Org_Application_Approval_Status'] == 'Approved') {
            echo "<td style='color:green'>"
            . "Approved"
            . "</td>";
        } else if ($_Row['Org_Application_Approval_Status'] == 'OnHold') {
            echo "<td style='color:red'>"
            . "OnHold"
            . "</td>";
        } else {
            echo "<td>"
            . "Rejected"
            . "</td>";
        }
        

        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
//
//if ($_action == "GETDETAILS") {
//    print_r($_POST['CenterCode']);
//    $response = $emp->GetITGK_Details($_POST['CenterCode']);
//    
//    $_DataTable = array();
//    $_i = 0;
//    while ($_Row = mysqli_fetch_array($response[2])) {
//        
//        
//    if ($_Row['Organization_AreaType'] == 'Urban') {
//        $_AT1 = ($_Row['Municipality_Type_Name'] ? $_Row['Municipality_Type_Name'] : 'NA');
//        $_AT2 = ($_Row['Municipality_Name'] ? $_Row['Municipality_Name'] : 'NA');
//        $_AT3 = ($_Row['Ward_Name'] ? $_Row['Ward_Name'] : 'NA');
//    } elseif ($_Row['Organization_AreaType'] == 'Rural') {
//        $_AT1 = ($_Row['Block_Name'] ? $_Row['Block_Name'] : 'NA');
//        $_AT2 = ($_Row['GP_Name'] ? $_Row['GP_Name'] : 'NA');
//        $_AT3 = ($_Row['Village_Name'] ? $_Row['Village_Name'] : 'NA');
//    } else {
//        $_AT1 = 'NA';
//        $_AT2 = 'NA';
//        $_AT3 = 'NA';
//    }
//           
//
//        $_DataTable[$_i] = array( "itgkname" => $_Row['ITGK_Name'],
//            "itgktype" => $_Row['Org_Type_Name'],
//            "email" => $_Row['User_EmailId'],
//            "mobile" => $_Row['User_MobileNo'],
//            "address" => $_Row['Organization_Address'],
//            "spname" => $_Row['RSP_Name'],
//            "district" => $_Row['District_Name'],
//            "tehsil" => $_Row['Tehsil_Name'],
//            "areatype" => $_Row['Organization_AreaType'],
//            "ownertype" => $_Row['Organization_OwnershipType'],
//            "at1" => $_AT1,
//            "at2" => $_AT2,
//            "at3" => $_AT3);
//        $_i = $_i + 1;
//    }
//
//    echo json_encode($_DataTable);
//}


if ($_action == "PROCESS") {
    header('Content-Type', 'pdf/*');
//    print($_POST['values']);
//    die;
    global $_ObjFTPConnection;
    $response = $emp->GetOrgDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $details= $_ObjFTPConnection->ftpdetails();
            $_role = 'IT-GK';
        $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
            "regno" => $_Row['Organization_RegistrationNo'],
            "fdate" => $_Row['Organization_FoundedDate'],
            "orgtype" => $_Row['Organization_Type'],
            "orgcourse" => $_Row['Org_Application_Type'],
            "doctype" => $_Row['Organization_DocType'],
            "email" => $_Row['Org_Email'],
            "mobile" => $_Row['Org_Mobile'],
            
            "bankaccname" => strtoupper($_Row['Bank_Account_Name']),
            "bankaccno" => $_Row['Bank_Account_Number'],
            "bankacctype" => strtoupper($_Row['Bank_Account_Type']),
            "bankname" => strtoupper($_Row['Bank_Name']),
            "ifsccode" => strtoupper($_Row['Bank_Ifsc_code']),
            
            "micrcode" => $_Row['Bank_Micr_Code'],
            "branchname" => strtoupper($_Row['Bank_Branch_Name']),
            "panno" => strtoupper($_Row['Bank_Pan_No']),
            "panname" => strtoupper($_Row['Bank_Pan_Name']),
            "bankid" => strtoupper($_Row['Bank_Id_Proof']),
            
            "bankpandoc" => $_Row['Bank_Pan_Document'],
            "bankiddoc" => $_Row['Bank_Id_Doc'],
            "orgdoc" => $_Row['Organization_ScanDoc'],
            "orguid" => $_Row['Organization_UID'],
            "orgaddproof" => $_Row['Organization_AddProof'],
            "orgappform" => $_Row['Organization_AppForm'],
            "orgtypedoc1" => $_Row['Organization_TypeDocId'],);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}

if ($_action == "ChangeOwner") {
//    print_r($_POST);
//    die;
    $_Code = $_POST["Code"];
    $_Mobile = $_POST["mobile"];
    $_CenterCode = $_POST["centercode"];
    $response = $emp->ApproveOwnershipChange($_Code,$_Mobile,$_CenterCode);
    echo $response[0];
    
}

if ($_action == "Reject") {
//    print_r($_POST);
//    die;
    $_Code = $_POST["Code"];
    $_Mobile = $_POST["mobile"];
    $_CenterCode = $_POST["centercode"];
    $response = $emp->RejectOwnershipChange($_Code,$_Mobile,$_CenterCode);
    echo $response[0];
    
}

if ($_action == "SendSMS") {
    
    $_SMS = $_POST["sms"];
    $_CenterCode = $_POST["centercode"];
    $_MobileNew = $_POST["mobile"];
    $response = $emp->SendSMS($_SMS, $_CenterCode, $_MobileNew);
    echo $response[0];
    
}

if ($_action == "PAYMENTDETAIL") {

    //echo "Show";
    $response = $emp->SHOWPAYENTDETAILS($_POST["centercode"]);

    $_DataTable = "";

   echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='examplepay' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>IT-GK Code</th>";
    echo "<th style='40%'>IT-GK Name</th>";
    echo "<th style='10%'>Ownership Change Fee Amount</th>";
	echo "<th style='10%'>Payment Status</th>";
	echo "<th style='10%'>Product Info</th>";
	echo "<th style='10%'>Date and Time</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Pay_Tran_ITGK'] . "</td>";
         echo "<td>" . strtoupper($_Row['Pay_Tran_Fname']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Pay_Tran_Amount']) . "</td>";
		 echo "<td>" . strtoupper($_Row['Pay_Tran_Status']) . "</td>";
		 echo "<td>" . $_Row['Pay_Tran_ProdInfo'] . "</td>";
		 echo "<td>" . $_Row['timestamp'] . "</td>";
		 
        echo "</tr>";
        $_Count++;
    }
   echo "</tbody>";
    echo "</table>";
	echo "</div>";
}



if ($_action == "GETREGRSP") {
    $response = $emp->GetRspDetails($_POST["centercode"]);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {

        $_DataTable[$_i] = array("rspname" => $_Row['Rspitgk_Rspname'],
            "mobno" => $_Row['Rspitgk_Rsproad'],
            "date" => $_Row['Rspitgk_Rspestdate'],
            "rsptype" => $_Row['Rspitgk_Rsporgtype']);
        $_i = $_i + 1;
    }

    echo json_encode($_DataTable);
}