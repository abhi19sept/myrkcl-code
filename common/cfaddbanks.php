<?php

/* 
 * Created by Abhishek

 */

include './commonFunction.php';
require 'BAL/clsaddbanks.php';

$response = array();
$emp = new clsaddbanks();

//print_r($_POST);
if ($_action == "ADD") {
	
    if (isset($_POST["txtifsc"]) && !empty($_POST["txtifsc"])
		&& isset($_POST["txtMicr"]) && !empty($_POST["txtMicr"])
		&& isset($_POST["txtBranch"]) && !empty($_POST["txtBranch"])
		&& isset($_POST["txtAddress"]) && !empty($_POST["txtAddress"])
		&& isset($_POST["txtCity"]) && !empty($_POST["txtCity"])
		&& isset($_POST["txtDistrict"]) && !empty($_POST["txtDistrict"])
		&& isset($_POST["txtState"]) && !empty($_POST["txtState"])
		&& isset($_POST["ddlStatus"]) && !empty($_POST["ddlStatus"])
		) {

		if(isset($_POST["ddlBankname"]) && !empty($_POST["ddlBankname"]))
		{
			$_Bankname = $_POST['ddlBankname'];
		
		}
		else
		{
			$_Bankname = trim($_POST["txtbank"]);
		}
        
		
        $_txtifsc = trim($_POST["txtifsc"]);
        $_txtMicr = trim($_POST["txtMicr"]);
        $_txtBranch = trim($_POST["txtBranch"]);
        $_txtAddress = trim($_POST["txtAddress"]);
        $_txtContact = trim($_POST["txtContact"]);
        $_txtCity = trim($_POST["txtCity"]);
        $_txtDistrict = trim($_POST["txtDistrict"]);
        $_txtState = trim($_POST["txtState"]);
        $_ddlStatus = trim($_POST["ddlStatus"]);
       
        //echo $_StatusName;


        $response = $emp->Add($_Bankname,$_txtifsc,$_txtMicr,$_txtBranch,$_txtAddress,$_txtContact,$_txtCity,$_txtDistrict,$_txtState,$_ddlStatus);
        echo $response[0];
    }
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_POST['values']);

    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    //echo "<th style='20%'>Staff User</th>";
    echo "<th style='10%'>Bank Name</th>";
    echo "<th style='20%'>IFSC Code</th>";
    echo "<th style='20%'>Micrcode</th>";
	
    echo "<th style='10%'>Branch Name</th>";
	 echo "<th style='10%'>Bank Address</th>";
	  
	   echo "<th style='10%'>Bank Contact</th>";
	   echo "<th style='10%'>Bank City</th>";
	   echo "<th style='10%'>Bank District</th>";
	   echo "<th style='10%'>Bank State</th>";
    
    echo "<th style='5%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
         echo "<td>" . $_Row['bankname'] . "</td>";
         echo "<td>" . $_Row['ifsccode'] . "</td>";
         echo "<td>" . $_Row['micrcode'] . "</td>";
         echo "<td>" . $_Row['branchname'] . "</td>";
		 echo "<td>" . $_Row['bankaddress'] . "</td>";
		 echo "<td>" . $_Row['bankcontact'] . "</td>";
		  echo "<td>" . $_Row['bankcity'] . "</td>";
		  echo "<td>" . $_Row['bankdistrict'] . "</td>";
		   echo "<td>" . $_Row['bankstate'] . "</td>";
        echo "<td><a href='frmaddbanks.php?code=" . $_Row['bankid'] . "&Mode=Delete'>"
                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}









if ($_action == "FILLDISTRICT") {
    $response = $emp->GetDistrict();
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['District_Name'] . ">" . $_Row['District_Name'] . "</option>";
    }
}

if ($_action == "FILLBANKS") {
    $response = $emp->GetBanks();
    echo "<option value='' >Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['bankname'] . "'>" . $_Row['bankname'] . "</option>";
    }
}


