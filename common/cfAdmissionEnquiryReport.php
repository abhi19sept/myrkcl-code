<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsAdmissionEnquiryReport.php';

    $response = array();
    $emp = new clsAdmissionEnquiryReport();

    if ($_action == "DETAILS") {
		
	$response = $emp->GetITGK_NCR_Details($_POST['values'],$_POST['batch']);
       
	$_DataTable = "";
		
	echo "<div class='table-responsive'>";
	echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered table-condensed'>";
	echo "<thead>";
	echo "<tr>";
	echo "<th style='5%'>S No.</th>";
        echo "<th style='5%'>IT-GK Code</th>";
        echo "<th style='5%'>Ack Code</th>";
        echo "<th style='5%'>Learner Name</th>";
        echo "<th style='5%'>Father Name</th>";
          echo "<th style='5%'>Learner Mobile</th>";
          echo "<th style='5%'>Enquiry Date</th>";
         echo "<th style='5%'>Action</th>";

	echo "</tr>";
	echo "</thead>";
	echo "<tbody>";
	if($response[0] == 'Success') {
	$_Count = 1;
	while ($_Row = mysqli_fetch_array($response[2])) {

	echo "<tr class='odd gradeX'>";
	echo "<td>" . $_Count . "</td>";
    echo "<td>" . $_Row['AdmissionEnquiry_ITGK'] . "</td>";
        echo "<td>" . ($_Row['AdmissionEnquiry_Code']) . "</td>";
        echo "<td>" . strtoupper($_Row['AdmissionEnquiry_Name']) . "</td>";
        echo "<td>" . strtoupper($_Row['AdmissionEnquiry_Fname']) . "</td>";
	echo "<td>" . ($_Row['AdmissionEnquiry_Mobile']) . "</td>";
        $date=date_create($_Row['AdmissionEnquiry_Timestamp']);
        echo "<td>" . date_format($date,"d-M-Y") . "</td>";
        if($_Row['AdmissionEnquiry_Status'] == 'active'){
         echo "<td><input type='button' class='updcount btn btn-primary btn-sm' AckCode='".$_Row['AdmissionEnquiry_Code']."' CourseCode='" . $_Row['AdmissionEnquiry_Course'] . "' mode='ShowUpload' value='Convert Enquiry to Admission'/></td>";
        }
        else{
            echo "<td>Added to Admission</td>";
        }
		
        
//        if($_Row['ITGKCODE']){
//        $response1 = $emp->Get_Faculty_Details($_Row['ITGKCODE']);
//        $_Row1 = mysql_fetch_array($response1[2]);
//        echo "<td>" . ($_Row1['Staff_Name'] ? $_Row1['Staff_Name'] : 'Faculty Details Not Available') . "</td>";
//        } else {
//        echo "<td>Faculty Details Not Available</td>";    
//        }
        echo "</tr>";
	$_Count++;
	}
	echo "</tbody>";
	echo "</table>";
	echo "</div>";
	}
    }
	 
if ($_action == "GETLEARNERLIST") {

    $response = $emp->GetDatabyCode($_POST["itgkcode"], $_POST["learnercode"]);

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example2' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>Learner Code</th>";
    echo "<th style='20%'>Learner Name</th>";
    echo "<th style='10%'>Father Name</th>";
    echo "<th style='10%'>DOB</th>";
   
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
	 echo "</div>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
         echo "<td>" . $_Row['Admission_Name'] . "</td>";
         echo "<td>" . $_Row['Admission_Fname'] . "</td>";
         echo "<td>" . $_Row['Admission_DOB'] . "</td>";
        
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILLAdmissionBatchcode") {
    $response = $emp->FILLAdmissionBatch($_POST['values']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}
?>