<?php

/*
 *  author Viveks
 */

require 'commonFunction.php';
require 'BAL/clsReconcilationReport.php';

$response = array();
$emp = new clsReconcilationReport();
//print_r($_POST);


if ($_action == "reconcilation") {
	
	$startdate = $_POST["txtstartdate"]." 00:00:00";
	$enddate = $_POST["txtenddate"]." 23:59:59";
	$reporttype = $_POST["reportType"];                        
	
	
    //$_DataTable = "";
    $_DataTable = array();
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";

    
    $_Count = 1;
/* Type 1 */
	if($reporttype == "learner_fee"){
		$report = $_POST['report1'];
		if($report == "report_1"){
			echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Admission_Code</th>";
    echo "<th style='8%'>Admission_LearnerCode</th>";
    echo "<th style='8%'>Admission_ITGK_Code</th>";
    echo "<th style='8%'>BioMatric_Status</th>";
    echo "<th style='8%'>Admission_Date</th>";
    echo "<th style='8%'>Admission_Date_LastModified</th>";
    echo "<th style='8%'>Admission_Date_Payment</th>";
    echo "<th style='12%'>Admission_Course</th>";
    echo "<th style='10%'>Admission_Batch</th>";
    echo "<th style='10%'>Admission_Fee</th>";
    echo "<th style='10%'>Admission_Installation_Mode</th>";
    echo "<th style='10%'>Admission_PhotoUpload_Status</th>";
    echo "<th style='10%'>Admission_SignUpload_Status</th>";
    echo "<th style='10%'>Admission_PhotoProcessing_Status</th>";
    echo "<th style='10%'>rejection_reason</th>";
    echo "<th style='10%'>rejection_type</th>";
    echo "<th style='10%'>Admission_Payment_Status</th>";
    echo "<th style='10%'>Admission_ReceiptPrint_Status</th>";
    echo "<th style='10%'>Admission_TranRefNo</th>";
    echo "<th style='10%'>Admission_RKCL_Trnid</th>";
    echo "<th style='10%'>Admission_Name</th>";
    echo "<th style='10%'>Admission_Fname</th>";
    echo "<th style='10%'>Admission_DOB</th>";
    echo "<th style='10%'>Admission_MTongue</th>";
    echo "<th style='10%'>Admission_Scan</th>";
    echo "<th style='10%'>Admission_Photo</th>";
    echo "<th style='10%'>Admission_Sign</th>";
    echo "<th style='10%'>Admission_Gender</th>";
    echo "<th style='10%'>Admission_MaritalStatus</th>";
    echo "<th style='10%'>Admission_Medium</th>";
    echo "<th style='10%'>Admission_PH</th>";
    echo "<th style='10%'>Admission_PID</th>";
    echo "<th style='10%'>Admission_UID</th>";
    echo "<th style='10%'>Admission_District</th>";
    echo "<th style='10%'>Admission_Tehsil</th>";
    echo "<th style='10%'>Admission_Address</th>";
    echo "<th style='10%'>Admission_PIN</th>";
    echo "<th style='10%'>Admission_Mobile</th>";
    echo "<th style='10%'>Admission_Phone</th>";
    echo "<th style='10%'>Admission_Email</th>";
    echo "<th style='10%'>Admission_Qualification</th>";
    echo "<th style='10%'>Admission_Ltype</th>";
    echo "<th style='10%'>Admission_GPFNO</th>";
    echo "<th style='10%'>User_Code</th>";
    echo "<th style='10%'>Admission_RspName</th>";
    echo "<th style='10%'>Timestamp</th>";
    echo "<th style='10%'>Admission_yearid</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
			$response = $emp->reconcilation_type1_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";	
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Admission_Code'] . "</td>";
					echo "<td>" . $_Row['Admission_LearnerCode'] . "</td>";
					echo "<td>" . $_Row['Admission_ITGK_Code'] . "</td>";
					echo "<td>" . $_Row['BioMatric_Status'] . "</td>";
					echo "<td>" . $_Row['Admission_Date'] . "</td>";
					echo "<td>" . $_Row['Admission_Date_LastModified'] . "</td>";
					echo "<td>" . $_Row['Admission_Date_Payment'] . "</td>";
					echo "<td>" . $_Row['Admission_Course'] . "</td>";
					echo "<td>" . $_Row['Admission_Batch'] . "</td>";
					echo "<td>" . $_Row['Admission_Fee'] . "</td>";
					echo "<td>" . $_Row['Admission_Installation_Mode'] . "</td>";
					echo "<td>" . $_Row['Admission_PhotoUpload_Status'] . "</td>";
					echo "<td>" . $_Row['Admission_SignUpload_Status'] . "</td>";
					echo "<td>" . $_Row['Admission_PhotoProcessing_Status'] . "</td>";
					echo "<td>" . $_Row['rejection_reason'] . "</td>";
					echo "<td>" . $_Row['rejection_type'] . "</td>";
					echo "<td>" . $_Row['Admission_Payment_Status'] . "</td>";
					echo "<td>" . $_Row['Admission_ReceiptPrint_Status'] . "</td>";
					echo "<td>" . $_Row['Admission_TranRefNo'] . "</td>";
					echo "<td>" . $_Row['Admission_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['Admission_Name'] . "</td>";
					echo "<td>" . $_Row['Admission_Fname'] . "</td>";
					echo "<td>" . $_Row['Admission_DOB'] . "</td>";
					echo "<td>" . $_Row['Admission_MTongue'] . "</td>";
					echo "<td>" . $_Row['Admission_Scan'] . "</td>";
					echo "<td>" . $_Row['Admission_Photo'] . "</td>";
					echo "<td>" . $_Row['Admission_Sign'] . "</td>";
					echo "<td>" . $_Row['Admission_Gender'] . "</td>";
					echo "<td>" . $_Row['Admission_MaritalStatus'] . "</td>";
					echo "<td>" . $_Row['Admission_Medium'] . "</td>";
					echo "<td>" . $_Row['Admission_PH'] . "</td>";
					echo "<td>" . $_Row['Admission_PID'] . "</td>";
					echo "<td>" . $_Row['Admission_UID'] . "</td>";
					echo "<td>" . $_Row['Admission_District'] . "</td>";
					echo "<td>" . $_Row['Admission_Tehsil'] . "</td>";
					echo "<td>" . $_Row['Admission_Address'] . "</td>";
					echo "<td>" . $_Row['Admission_PIN'] . "</td>";
					echo "<td>" . $_Row['Admission_Mobile'] . "</td>";
					echo "<td>" . $_Row['Admission_Phone'] . "</td>";
					echo "<td>" . $_Row['Admission_Email'] . "</td>";
					echo "<td>" . $_Row['Admission_Qualification'] . "</td>";
					echo "<td>" . $_Row['Admission_Ltype'] . "</td>";
					echo "<td>" . $_Row['Admission_GPFNO'] . "</td>";
					echo "<td>" . $_Row['User_Code'] . "</td>";
					echo "<td>" . $_Row['Admission_RspName'] . "</td>";
					echo "<td>" . $_Row['Timestamp'] . "</td>";
					echo "<td>" . $_Row['Admission_yearid'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		
		if($report == "report_2"){
			echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Admission_Transaction_Code</th>";
    echo "<th style='8%'>Admission_Transaction_Status</th>";
    echo "<th style='8%'>Admission_Transaction_Fname</th>";
    echo "<th style='8%'>Admission_Transaction_Amount</th>";
    echo "<th style='8%'>Admission_Transaction_Txtid</th>";
    echo "<th style='8%'>Admission_Transaction_RKCL_Txid</th>";
    echo "<th style='8%'>Admission_Transaction_Hash</th>";
    echo "<th style='12%'>Admission_Transaction_Key</th>";
    echo "<th style='10%'>Admission_Transaction_ProdInfo</th>";
    echo "<th style='10%'>Admission_Transaction_Email</th>";
    echo "<th style='10%'>Admission_Transaction_CenterCode</th>";
    echo "<th style='10%'>Admission_Payment_Mode</th>";
    echo "<th style='10%'>Admission_Transaction_Batch</th>";
    echo "<th style='10%'>Admission_Transaction_Course</th>";
    echo "<th style='10%'>Admission_Transaction_DateTime</th>";
    echo "<th style='10%'>Admission_Transaction_Reconcile_Status</th>";
    echo "<th style='10%'>Admission_Transaction_timestamp</th>";
	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
			$response = $emp->reconcilation_type1_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Code'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Status'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Fname'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Amount'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Txtid'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_RKCL_Txid'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Hash'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Key'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Email'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_CenterCode'] . "</td>";
					echo "<td>" . $_Row['Admission_Payment_Mode'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Batch'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Course'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_DateTime'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_Reconcile_Status'] . "</td>";
					echo "<td>" . $_Row['Admission_Transaction_timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
																				
		if($report == "report_3"){
			echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Pay_Tran_Code</th>";
    echo "<th style='8%'>Pay_Tran_ITGK</th>";
    echo "<th style='8%'>Pay_Tran_Fname</th>";
    echo "<th style='8%'>Pay_Tran_RKCL_Trnid</th>";
    echo "<th style='8%'>Pay_Tran_AdmissionArray</th>";
    echo "<th style='8%'>Pay_Tran_LCount</th>";
    echo "<th style='8%'>Pay_Tran_Amount</th>";
    echo "<th style='10%'>Pay_Tran_Status</th>";
    echo "<th style='8%'>Pay_Tran_ProdInfo</th>";
    echo "<th style='8%'>Pay_Tran_Course</th>";
    echo "<th style='8%'>Pay_Tran_Batch</th>";
    echo "<th style='8%'>Pay_Tran_ReexamEvent</th>";
    echo "<th style='8%'>Pay_Tran_Eoi_Code</th>";
    echo "<th style='8%'>Pay_Tran_PG_ID</th>";
    echo "<th style='10%'>Pay_Tran_PG_Trnid</th>";
    echo "<th style='8%'>timestamp</th>";
    echo "<th style='8%'>Payumoney_Transaction</th>";
    echo "<th style='8%'>pay_verifyapi_status</th>";
    echo "<th style='8%'>Pay_Tran_Reconcile_status</th>";
    echo "<th style='8%'>razorpay_order_id</th>";
    echo "<th style='8%'>razorpay_payment_id</th>";
    echo "<th style='10%'>razorpay_payment_signature</th>";
    echo "<th style='8%'>paymentgateway</th>";
																							

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
			$response = $emp->reconcilation_type1_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ITGK'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Fname'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_AdmissionArray'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_LCount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Amount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Course'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Batch'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ReexamEvent'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Eoi_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_ID'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_Trnid'] . "</td>";
					echo "<td>" . $_Row['timestamp'] . "</td>";
					echo "<td>" . $_Row['Payumoney_Transaction'] . "</td>";
					echo "<td>" . $_Row['pay_verifyapi_status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Reconcile_status'] . "</td>";
					echo "<td>" . $_Row['razorpay_order_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_signature'] . "</td>";
					echo "<td>" . $_Row['paymentgateway'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
																	
		if($report == "report_4"){
			echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Invoice_No</th>";
    echo "<th style='8%'>Invoice_Amount</th>";
    echo "<th style='8%'>Invoice_Date</th>";
    echo "<th style='8%'>Invoice_Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
			$response = $emp->reconcilation_type1_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Invoice_No'] . "</td>";
					echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
					echo "<td>" . $_Row['Invoice_Date'] . "</td>";
					echo "<td>" . $_Row['Invoice_Status'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
																				
		if($report == "report_5"){
			echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>ITGK Code</th>";
    echo "<th style='8%'>Transaction Number</th>";
    echo "<th style='8%'>Payment Amount</th>";
    echo "<th style='8%'>Payment Status</th>";
    echo "<th style='10%'>Payment Type</th>";
    echo "<th style='8%'>Payment Date</th>";

    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
			$response = $emp->reconcilation_type1_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ITGK'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Txnid'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Amount'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Status'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
																				
	}
/* Type 2 */	
	if($reporttype == "reexam_fee"){
		$report = $_POST['report2'];
		if($report == "report_1"){

			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>examdata_code</th>";
			echo "<th style='8%'>examid</th>";
			echo "<th style='8%'>learnercode</th>";
			echo "<th style='8%'>learnername</th>";
			echo "<th style='10%'>fathername</th>";
			echo "<th style='8%'>dob</th>";
			echo "<th style='10%'>coursename</th>";
			echo "<th style='8%'>batchname</th>";
			echo "<th style='10%'>itgkcode</th>";
			echo "<th style='8%'>itgkname</th>";
			echo "<th style='8%'>itgkdistrict</th>";
			echo "<th style='8%'>itgktehsil</th>";
			echo "<th style='8%'>remark</th>";
			echo "<th style='8%'>status</th>";
			echo "<th style='8%'>examdate</th>";
			echo "<th style='8%'>reexam_TranRefNo</th>";
			echo "<th style='8%'>reexam_RKCL_Trnid</th>";
			echo "<th style='8%'>paymentstatus</th>";
			echo "<th style='8%'>learnertype</th>";
			echo "<th style='8%'>reexamapplyby</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
																									

			$response = $emp->reconcilation_type2_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['examdata_code'] . "</td>";
					echo "<td>" . $_Row['examid'] . "</td>";
					echo "<td>" . $_Row['learnercode'] . "</td>";
					echo "<td>" . $_Row['learnername'] . "</td>";
					echo "<td>" . $_Row['fathername'] . "</td>";
					echo "<td>" . $_Row['dob'] . "</td>";
					echo "<td>" . $_Row['coursename'] . "</td>";
					echo "<td>" . $_Row['batchname'] . "</td>";
					echo "<td>" . $_Row['itgkcode'] . "</td>";
					echo "<td>" . $_Row['itgkname'] . "</td>";
					echo "<td>" . $_Row['itgkdistrict'] . "</td>";
					echo "<td>" . $_Row['itgktehsil'] . "</td>";
					echo "<td>" . $_Row['remark'] . "</td>";
					echo "<td>" . $_Row['status'] . "</td>";
					echo "<td>" . $_Row['examdate'] . "</td>";
					echo "<td>" . $_Row['reexam_TranRefNo'] . "</td>";
					echo "<td>" . $_Row['reexam_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['paymentstatus'] . "</td>";
					echo "<td>" . $_Row['learnertype'] . "</td>";
					echo "<td>" . $_Row['reexamapplyby'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_2"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Reexam_Transaction_Code</th>";
			echo "<th style='8%'>Reexam_Transaction_Status</th>";
			echo "<th style='8%'>Reexam_Transaction_Fname</th>";
			echo "<th style='8%'>Reexam_Transaction_Amount</th>";
			echo "<th style='10%'>Reexam_Transaction_Txtid</th>";
			echo "<th style='8%'>Reexam_Transaction_RKCL_Txid</th>";
			echo "<th style='10%'>Reexam_Transaction_Hash</th>";
			echo "<th style='8%'>Reexam_Transaction_Key</th>";
			echo "<th style='8%'>Reexam_Transaction_ProdInfo</th>";
			echo "<th style='8%'>Reexam_Transaction_Email</th>";
			echo "<th style='10%'>Reexam_Transaction_CenterCode</th>";
			echo "<th style='10%'>Reexam_Payment_Mode</th>";
			echo "<th style='10%'>Reexam_Reconcile_Status</th>";
			echo "<th style='8%'>Reexam_DateTime</th>";
			echo "<th style='8%'>Reexam_Transaction_timestamp</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
																	

			$response = $emp->reconcilation_type2_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_Code'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_Status'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_Fname'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_Amount'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_Txtid'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_RKCL_Txid'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_Hash'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_Key'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_Email'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_CenterCode'] . "</td>";
					echo "<td>" . $_Row['Reexam_Payment_Mode'] . "</td>";
					echo "<td>" . $_Row['Reexam_Reconcile_Status'] . "</td>";
					echo "<td>" . $_Row['Reexam_DateTime'] . "</td>";
					echo "<td>" . $_Row['Reexam_Transaction_timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_3"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Pay_Tran_Code</th>";
			echo "<th style='8%'>Pay_Tran_ITGK</th>";
			echo "<th style='8%'>Pay_Tran_Fname</th>";
			echo "<th style='8%'>Pay_Tran_RKCL_Trnid</th>";
			echo "<th style='8%'>Pay_Tran_AdmissionArray</th>";
			echo "<th style='8%'>Pay_Tran_LCount</th>";
			echo "<th style='8%'>Pay_Tran_Amount</th>";
			echo "<th style='10%'>Pay_Tran_Status</th>";
			echo "<th style='8%'>Pay_Tran_ProdInfo</th>";
			echo "<th style='8%'>Pay_Tran_Course</th>";
			echo "<th style='8%'>Pay_Tran_Batch</th>";
			echo "<th style='8%'>Pay_Tran_ReexamEvent</th>";
			echo "<th style='8%'>Pay_Tran_Eoi_Code</th>";
			echo "<th style='8%'>Pay_Tran_PG_ID</th>";
			echo "<th style='10%'>Pay_Tran_PG_Trnid</th>";
			echo "<th style='8%'>timestamp</th>";
			echo "<th style='8%'>Payumoney_Transaction</th>";
			echo "<th style='8%'>pay_verifyapi_status</th>";
			echo "<th style='8%'>Pay_Tran_Reconcile_status</th>";
			echo "<th style='8%'>razorpay_order_id</th>";
			echo "<th style='8%'>razorpay_payment_id</th>";
			echo "<th style='10%'>razorpay_payment_signature</th>";
			echo "<th style='8%'>paymentgateway</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$response = $emp->reconcilation_type2_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ITGK'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Fname'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_AdmissionArray'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_LCount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Amount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Course'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Batch'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ReexamEvent'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Eoi_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_ID'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_Trnid'] . "</td>";
					echo "<td>" . $_Row['timestamp'] . "</td>";
					echo "<td>" . $_Row['Payumoney_Transaction'] . "</td>";
					echo "<td>" . $_Row['pay_verifyapi_status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Reconcile_status'] . "</td>";
					echo "<td>" . $_Row['razorpay_order_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_signature'] . "</td>";
					echo "<td>" . $_Row['paymentgateway'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_4"){
			echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Invoice_No</th>";
    echo "<th style='8%'>Invoice_Amount</th>";
    echo "<th style='8%'>Invoice_Date</th>";
    echo "<th style='8%'>Invoice_Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
			$response = $emp->reconcilation_type2_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Invoice_No'] . "</td>";
					echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
					echo "<td>" . $_Row['Invoice_Date'] . "</td>";
					echo "<td>" . $_Row['Invoice_Status'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_5"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>ITGK Code</th>";
			echo "<th style='8%'>Transaction Number</th>";
			echo "<th style='8%'>Payment Amount</th>";
			echo "<th style='8%'>Payment Status</th>";
			echo "<th style='10%'>Payment Type</th>";
			echo "<th style='8%'>Payment Date</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$response = $emp->reconcilation_type2_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ITGK'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Txnid'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Amount'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Status'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
	}
/* Type 3 */	
	if($reporttype == "NCR_fee"){
		$report = $_POST['report3'];
		if($report == "report_1"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Organization_Code</th>";
			echo "<th style='8%'>Organization_Name</th>";
			echo "<th style='8%'>Organization_RegistrationNo</th>";
			echo "<th style='8%'>Organization_FoundedDate</th>";
			echo "<th style='10%'>Organization_Type</th>";
			echo "<th style='8%'>Organization_DocType</th>";
			echo "<th style='8%'>Organization_ScanDoc</th>";
			echo "<th style='10%'>Organization_UID</th>";
			echo "<th style='8%'>Organization_AddProof</th>";
			echo "<th style='8%'>Organization_AppForm</th>";
			echo "<th style='10%'>Organization_TypeDocId</th>";
			echo "<th style='8%'>Organization_State</th>";
			echo "<th style='10%'>Organization_Region</th>";
			echo "<th style='8%'>Organization_District</th>";
			echo "<th style='10%'>Organization_Tehsil</th>";
			echo "<th style='8%'>Organization_Landmark</th>";
			echo "<th style='8%'>Organization_Road</th>";
			echo "<th style='8%'>Organization_Street</th>";
			echo "<th style='8%'>Organization_HouseNo</th>";
			echo "<th style='8%'>Organization_Country</th>";
			echo "<th style='8%'>Org_Ack</th>";
			echo "<th style='8%'>Org_RKCL_Trnid</th>";
			echo "<th style='8%'>Org_PayStatus</th>";
			echo "<th style='8%'>Org_PAN</th>";
			echo "<th style='8%'>Org_AADHAR</th>";
			echo "<th style='8%'>Org_AreaType</th>";
			echo "<th style='8%'>Org_Timestamp</th>";
			echo "<th style='8%'>Org_NCR_App_Date</th>";
			echo "<th style='8%'>Org_NCR_Login_Approval_Date</th>";
			echo "<th style='8%'>Org_NCR_Final_Approval_Date</th>";
			echo "<th style='8%'>Org_NCR_Course_Allocation_Date</th>";
			echo "<th style='8%'>Org_Role</th>";
			echo "<th style='8%'>Org_Email</th>";
			echo "<th style='8%'>Org_Mobile</th>";
			echo "<th style='8%'>Org_Status</th>";
			echo "<th style='8%'>Org_Login_Approval_Status</th>";
			echo "<th style='8%'>Org_Final_Approval_Status</th>";
			echo "<th style='8%'>Org_Course_Allocation_Status</th>";
			echo "<th style='8%'>Org_Remark</th>";
			echo "<th style='8%'>Org_RspCode</th>";
			echo "<th style='8%'>Org_RspLoginId</th>";
			echo "<th style='8%'>Org_TranRefNo</th>";
			echo "<th style='8%'>Org_Mohalla</th>";
			echo "<th style='8%'>Org_Municipal_Type</th>";
			echo "<th style='8%'>Org_WardNo</th>";
			echo "<th style='8%'>Org_Police</th>";
			echo "<th style='8%'>Org_Municipal</th>";
			echo "<th style='8%'>Org_Village</th>";
			echo "<th style='8%'>Org_Gram</th>";
			echo "<th style='8%'>Org_Panchayat</th>";
			echo "<th style='8%'>Org_PinCode</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
																		

			$response = $emp->reconcilation_type3_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Organization_Code'] . "</td>";
					echo "<td>" . $_Row['Organization_Name'] . "</td>";
					echo "<td>" . $_Row['Organization_RegistrationNo'] . "</td>";
					echo "<td>" . $_Row['Organization_FoundedDate'] . "</td>";
					echo "<td>" . $_Row['Organization_Type'] . "</td>";
					echo "<td>" . $_Row['Organization_DocType'] . "</td>";
					echo "<td>" . $_Row['Organization_ScanDoc'] . "</td>";
					echo "<td>" . $_Row['Organization_UID'] . "</td>";
					echo "<td>" . $_Row['Organization_AddProof'] . "</td>";
					echo "<td>" . $_Row['Organization_AppForm'] . "</td>";
					echo "<td>" . $_Row['Organization_TypeDocId'] . "</td>";
					echo "<td>" . $_Row['Organization_State'] . "</td>";
					echo "<td>" . $_Row['Organization_Region'] . "</td>";
					echo "<td>" . $_Row['Organization_District'] . "</td>";
					echo "<td>" . $_Row['Organization_Tehsil'] . "</td>";
					echo "<td>" . $_Row['Organization_Landmark'] . "</td>";
					echo "<td>" . $_Row['Organization_Road'] . "</td>";
					echo "<td>" . $_Row['Organization_Street'] . "</td>";
					echo "<td>" . $_Row['Organization_HouseNo'] . "</td>";
					echo "<td>" . $_Row['Organization_Country'] . "</td>";
					echo "<td>" . $_Row['Org_Ack'] . "</td>";
					echo "<td>" . $_Row['Org_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['Org_PayStatus'] . "</td>";
					echo "<td>" . $_Row['Org_PAN'] . "</td>";
					echo "<td>" . $_Row['Org_AADHAR'] . "</td>";
					echo "<td>" . $_Row['Org_AreaType'] . "</td>";
					echo "<td>" . $_Row['Org_Timestamp'] . "</td>";
					echo "<td>" . $_Row['Org_NCR_App_Date'] . "</td>";
					echo "<td>" . $_Row['Org_NCR_Login_Approval_Date'] . "</td>";
					echo "<td>" . $_Row['Org_NCR_Final_Approval_Date'] . "</td>";
					echo "<td>" . $_Row['Org_NCR_Course_Allocation_Date'] . "</td>";
					echo "<td>" . $_Row['Org_Role'] . "</td>";
					echo "<td>" . $_Row['Org_Email'] . "</td>";
					echo "<td>" . $_Row['Org_Mobile'] . "</td>";
					echo "<td>" . $_Row['Org_Status'] . "</td>";
					echo "<td>" . $_Row['Org_Login_Approval_Status'] . "</td>";
					echo "<td>" . $_Row['Org_Final_Approval_Status'] . "</td>";
					echo "<td>" . $_Row['Org_Course_Allocation_Status'] . "</td>";
					echo "<td>" . $_Row['Org_Remark'] . "</td>";
					echo "<td>" . $_Row['Org_RspCode'] . "</td>";
					echo "<td>" . $_Row['Org_RspLoginId'] . "</td>";
					echo "<td>" . $_Row['Org_TranRefNo'] . "</td>";
					echo "<td>" . $_Row['Org_Mohalla'] . "</td>";
					echo "<td>" . $_Row['Org_Municipal_Type'] . "</td>";
					echo "<td>" . $_Row['Org_WardNo'] . "</td>";
					echo "<td>" . $_Row['Org_Police'] . "</td>";
					echo "<td>" . $_Row['Org_Municipal'] . "</td>";
					echo "<td>" . $_Row['Org_Village'] . "</td>";
					echo "<td>" . $_Row['Org_Gram'] . "</td>";
					echo "<td>" . $_Row['Org_Panchayat'] . "</td>";
					echo "<td>" . $_Row['Org_PinCode'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_2"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Ncr_Transaction_Code</th>";
			echo "<th style='8%'>Ncr_Transaction_Status</th>";
			echo "<th style='8%'>Ncr_Transaction_Fname</th>";
			echo "<th style='8%'>Ncr_Transaction_Amount</th>";
			echo "<th style='10%'>Ncr_Transaction_RKCL_Txid</th>";
			echo "<th style='8%'>Ncr_Transaction_Txtid</th>";
			echo "<th style='8%'>Ncr_Transaction_Hash</th>";
			echo "<th style='8%'>Ncr_Transaction_Key</th>";
			echo "<th style='10%'>Ncr_Payment_Mode</th>";
			echo "<th style='8%'>Ncr_Transaction_ProdInfo</th>";
			echo "<th style='8%'>Ncr_Transaction_Email</th>";
			echo "<th style='8%'>Ncr_Transaction_CenterCode</th>";
			echo "<th style='10%'>Ncr_Transaction_DateTime</th>";
			echo "<th style='8%'>Ncr_Transaction_timestamp</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
																

			$response = $emp->reconcilation_type3_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_Code'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_Status'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_Fname'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_Amount'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_RKCL_Txid'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_Txtid'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_Hash'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_Key'] . "</td>";
					echo "<td>" . $_Row['Ncr_Payment_Mode'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_Email'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_CenterCode'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_DateTime'] . "</td>";
					echo "<td>" . $_Row['Ncr_Transaction_timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_3"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Pay_Tran_Code</th>";
			echo "<th style='8%'>Pay_Tran_ITGK</th>";
			echo "<th style='8%'>Pay_Tran_Fname</th>";
			echo "<th style='8%'>Pay_Tran_RKCL_Trnid</th>";
			echo "<th style='8%'>Pay_Tran_AdmissionArray</th>";
			echo "<th style='8%'>Pay_Tran_LCount</th>";
			echo "<th style='8%'>Pay_Tran_Amount</th>";
			echo "<th style='10%'>Pay_Tran_Status</th>";
			echo "<th style='8%'>Pay_Tran_ProdInfo</th>";
			echo "<th style='8%'>Pay_Tran_Course</th>";
			echo "<th style='8%'>Pay_Tran_Batch</th>";
			echo "<th style='8%'>Pay_Tran_ReexamEvent</th>";
			echo "<th style='8%'>Pay_Tran_Eoi_Code</th>";
			echo "<th style='8%'>Pay_Tran_PG_ID</th>";
			echo "<th style='10%'>Pay_Tran_PG_Trnid</th>";
			echo "<th style='8%'>timestamp</th>";
			echo "<th style='8%'>Payumoney_Transaction</th>";
			echo "<th style='8%'>pay_verifyapi_status</th>";
			echo "<th style='8%'>Pay_Tran_Reconcile_status</th>";
			echo "<th style='8%'>razorpay_order_id</th>";
			echo "<th style='8%'>razorpay_payment_id</th>";
			echo "<th style='10%'>razorpay_payment_signature</th>";
			echo "<th style='8%'>paymentgateway</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$response = $emp->reconcilation_type3_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ITGK'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Fname'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_AdmissionArray'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_LCount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Amount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Course'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Batch'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ReexamEvent'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Eoi_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_ID'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_Trnid'] . "</td>";
					echo "<td>" . $_Row['timestamp'] . "</td>";
					echo "<td>" . $_Row['Payumoney_Transaction'] . "</td>";
					echo "<td>" . $_Row['pay_verifyapi_status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Reconcile_status'] . "</td>";
					echo "<td>" . $_Row['razorpay_order_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_signature'] . "</td>";
					echo "<td>" . $_Row['paymentgateway'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_4"){
			echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Invoice_No</th>";
    echo "<th style='8%'>Invoice_Amount</th>";
    echo "<th style='8%'>Invoice_Date</th>";
    echo "<th style='8%'>Invoice_Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
			$response = $emp->reconcilation_type3_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Invoice_No'] . "</td>";
					echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
					echo "<td>" . $_Row['Invoice_Date'] . "</td>";
					echo "<td>" . $_Row['Invoice_Status'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_5"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>ITGK Code</th>";
			echo "<th style='8%'>Transaction Number</th>";
			echo "<th style='8%'>Payment Amount</th>";
			echo "<th style='8%'>Payment Status</th>";
			echo "<th style='10%'>Payment Type</th>";
			echo "<th style='8%'>Payment Date</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$response = $emp->reconcilation_type3_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ITGK'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Txnid'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Amount'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Status'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
	}
/* Type 4 */
	if($reporttype == "eoi_fee"){
		$report = $_POST['report4'];
		if($report == "report_1"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Courseitgk_Code</th>";
			echo "<th style='8%'>Courseitgk_Course</th>";
			echo "<th style='8%'>Courseitgk_ITGK</th>";
			echo "<th style='8%'>Courseitgk_EOI</th>";
			echo "<th style='10%'>CourseITGK_BlockStatus</th>";
			echo "<th style='8%'>CourseITGK_UserCreatedDate</th>";
			echo "<th style='10%'>CourseITGK_UserFinalApprovalDate</th>";
			echo "<th style='8%'>CourseITGK_StartDate</th>";
			echo "<th style='8%'>CourseITGK_ExpireDate</th>";
			echo "<th style='8%'>EOI_Fee_Confirm</th>";
			echo "<th style='10%'>CourseITGK_Renewal_Status</th>";
			echo "<th style='8%'>Courseitgk_TranRefNo</th>";
			echo "<th style='10%'>Courseitgk_timestamp</th>";
			echo "<th style='8%'>IsNewRecord</th>";
			echo "<th style='8%'>IsNewCRecord</th>";
			echo "<th style='8%'>IsNewSRecord</th>";
			echo "<th style='10%'>IsNewOnlineLMSRecord</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
																			

			$response = $emp->reconcilation_type4_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Courseitgk_Code'] . "</td>";
					echo "<td>" . $_Row['Courseitgk_Course'] . "</td>";
					echo "<td>" . $_Row['Courseitgk_ITGK'] . "</td>";
					echo "<td>" . $_Row['Courseitgk_EOI'] . "</td>";
					echo "<td>" . $_Row['CourseITGK_BlockStatus'] . "</td>";
					echo "<td>" . $_Row['CourseITGK_UserCreatedDate'] . "</td>";
					echo "<td>" . $_Row['CourseITGK_UserFinalApprovalDate'] . "</td>";
					echo "<td>" . $_Row['CourseITGK_StartDate'] . "</td>";
					echo "<td>" . $_Row['CourseITGK_ExpireDate'] . "</td>";
					echo "<td>" . $_Row['EOI_Fee_Confirm'] . "</td>";
					echo "<td>" . $_Row['CourseITGK_Renewal_Status'] . "</td>";
					echo "<td>" . $_Row['Courseitgk_TranRefNo'] . "</td>";
					echo "<td>" . $_Row['Courseitgk_timestamp'] . "</td>";
					echo "<td>" . $_Row['IsNewRecord'] . "</td>";
					echo "<td>" . $_Row['IsNewCRecord'] . "</td>";
					echo "<td>" . $_Row['IsNewSRecord'] . "</td>";
					echo "<td>" . $_Row['IsNewOnlineLMSRecord'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_2"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>EOI_Transaction_Code</th>";
			echo "<th style='8%'>EOI_Transaction_Status</th>";
			echo "<th style='8%'>EOI_Transaction_Fname</th>";
			echo "<th style='8%'>EOI_Transaction_Amount</th>";
			echo "<th style='10%'>EOI_Transaction_Txtid</th>";
			echo "<th style='8%'>EOI_Transaction_RKCL_Txid</th>";
			echo "<th style='8%'>EOI_Payment_Mode</th>";
			echo "<th style='8%'>EOI_Transaction_Hash</th>";
			echo "<th style='10%'>EOI_Transaction_Key</th>";
			echo "<th style='8%'>EOI_Transaction_ProdInfo</th>";
			echo "<th style='8%'>EOI_Transaction_Email</th>";
			echo "<th style='8%'>EOI_Transaction_CenterCode</th>";
			echo "<th style='10%'>EOI_Transaction_timestamp</th>";
			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
															

			$response = $emp->reconcilation_type4_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_Code'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_Status'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_Fname'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_Amount'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_Txtid'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_RKCL_Txid'] . "</td>";
					echo "<td>" . $_Row['EOI_Payment_Mode'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_Hash'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_Key'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_Email'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_CenterCode'] . "</td>";
					echo "<td>" . $_Row['EOI_Transaction_timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_3"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Pay_Tran_Code</th>";
			echo "<th style='8%'>Pay_Tran_ITGK</th>";
			echo "<th style='8%'>Pay_Tran_Fname</th>";
			echo "<th style='8%'>Pay_Tran_RKCL_Trnid</th>";
			echo "<th style='8%'>Pay_Tran_AdmissionArray</th>";
			echo "<th style='8%'>Pay_Tran_LCount</th>";
			echo "<th style='8%'>Pay_Tran_Amount</th>";
			echo "<th style='10%'>Pay_Tran_Status</th>";
			echo "<th style='8%'>Pay_Tran_ProdInfo</th>";
			echo "<th style='8%'>Pay_Tran_Course</th>";
			echo "<th style='8%'>Pay_Tran_Batch</th>";
			echo "<th style='8%'>Pay_Tran_ReexamEvent</th>";
			echo "<th style='8%'>Pay_Tran_Eoi_Code</th>";
			echo "<th style='8%'>Pay_Tran_PG_ID</th>";
			echo "<th style='10%'>Pay_Tran_PG_Trnid</th>";
			echo "<th style='8%'>timestamp</th>";
			echo "<th style='8%'>Payumoney_Transaction</th>";
			echo "<th style='8%'>pay_verifyapi_status</th>";
			echo "<th style='8%'>Pay_Tran_Reconcile_status</th>";
			echo "<th style='8%'>razorpay_order_id</th>";
			echo "<th style='8%'>razorpay_payment_id</th>";
			echo "<th style='10%'>razorpay_payment_signature</th>";
			echo "<th style='8%'>paymentgateway</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$response = $emp->reconcilation_type4_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ITGK'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Fname'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_AdmissionArray'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_LCount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Amount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Course'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Batch'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ReexamEvent'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Eoi_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_ID'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_Trnid'] . "</td>";
					echo "<td>" . $_Row['timestamp'] . "</td>";
					echo "<td>" . $_Row['Payumoney_Transaction'] . "</td>";
					echo "<td>" . $_Row['pay_verifyapi_status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Reconcile_status'] . "</td>";
					echo "<td>" . $_Row['razorpay_order_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_signature'] . "</td>";
					echo "<td>" . $_Row['paymentgateway'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_4"){
			echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Invoice_No</th>";
    echo "<th style='8%'>Invoice_Amount</th>";
    echo "<th style='8%'>Invoice_Date</th>";
    echo "<th style='8%'>Invoice_Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
			$response = $emp->reconcilation_type4_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Invoice_No'] . "</td>";
					echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
					echo "<td>" . $_Row['Invoice_Date'] . "</td>";
					echo "<td>" . $_Row['Invoice_Status'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_5"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>ITGK Code</th>";
			echo "<th style='8%'>Transaction Number</th>";
			echo "<th style='8%'>Payment Amount</th>";
			echo "<th style='8%'>Payment Status</th>";
			echo "<th style='10%'>Payment Type</th>";
			echo "<th style='8%'>Payment Date</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$response = $emp->reconcilation_type4_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ITGK'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Txnid'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Amount'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Status'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
	}
/* Type 5 */
	if($reporttype == "correction_fee"){
		$report = $_POST['report5'];
		if($report == "report_1"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>userid</th>";
			echo "<th style='8%'>Correction_ITGK_Code</th>";
			echo "<th style='8%'>lcode</th>";
			echo "<th style='8%'>cfname</th>";
			echo "<th style='10%'>cfaname</th>";
			echo "<th style='8%'>applicationfor</th>";
			echo "<th style='8%'>Correction_Payment_Status</th>";
			echo "<th style='8%'>Correction_TranRefNo</th>";
			echo "<th style='10%'>Correction_RKCL_Trnid</th>";
			echo "<th style='8%'>exameventname</th>";
			echo "<th style='8%'>totalmarks</th>";
			echo "<th style='8%'>emailid</th>";
			echo "<th style='10%'>mobile</th>";
			echo "<th style='8%'>photo</th>";
			echo "<th style='8%'>applicationdate</th>";
			echo "<th style='8%'>cid</th>";
			echo "<th style='10%'>dob</th>";
			echo "<th style='8%'>attach1</th>";
			echo "<th style='8%'>attach2</th>";
			echo "<th style='10%'>attach4</th>";
			echo "<th style='8%'>remarks</th>";
			echo "<th style='8%'>dispatchstatus</th>";
			echo "<th style='10%'>discrepency</th>";
			echo "<th style='8%'>lot</th>";
			echo "<th style='10%'>district_code</th>";
			echo "<th style='8%'>Reconcile_Status</th>";
			echo "<th style='8%'>SentByVMOU</th>";
			echo "<th style='8%'>appliedby</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
														

			$response = $emp->reconcilation_type5_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['userid'] . "</td>";
					echo "<td>" . $_Row['Correction_ITGK_Code'] . "</td>";
					echo "<td>" . $_Row['lcode'] . "</td>";
					echo "<td>" . $_Row['cfname'] . "</td>";
					echo "<td>" . $_Row['cfaname'] . "</td>";
					echo "<td>" . $_Row['applicationfor'] . "</td>";
					echo "<td>" . $_Row['Correction_Payment_Status'] . "</td>";
					echo "<td>" . $_Row['Correction_TranRefNo'] . "</td>";
					echo "<td>" . $_Row['Correction_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['exameventname'] . "</td>";
					echo "<td>" . $_Row['totalmarks'] . "</td>";
					echo "<td>" . $_Row['emailid'] . "</td>";
					echo "<td>" . $_Row['mobile'] . "</td>";
					echo "<td>" . $_Row['photo'] . "</td>";
					echo "<td>" . $_Row['applicationdate'] . "</td>";
					echo "<td>" . $_Row['cid'] . "</td>";
					echo "<td>" . $_Row['dob'] . "</td>";
					echo "<td>" . $_Row['attach1'] . "</td>";
					echo "<td>" . $_Row['attach2'] . "</td>";
					echo "<td>" . $_Row['attach4'] . "</td>";
					echo "<td>" . $_Row['remarks'] . "</td>";
					echo "<td>" . $_Row['dispatchstatus'] . "</td>";
					echo "<td>" . $_Row['discrepency'] . "</td>";
					echo "<td>" . $_Row['lot'] . "</td>";
					echo "<td>" . $_Row['district_code'] . "</td>";
					echo "<td>" . $_Row['Reconcile_Status'] . "</td>";
					echo "<td>" . $_Row['SentByVMOU'] . "</td>";
					echo "<td>" . $_Row['appliedby'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_2"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Correction_Transaction_Code</th>";
			echo "<th style='8%'>Correction_Transaction_Status</th>";
			echo "<th style='8%'>Correction_Transaction_Fname</th>";
			echo "<th style='8%'>Correction_Transaction_Amount</th>";
			echo "<th style='10%'>Correction_Transaction_Txtid</th>";
			echo "<th style='8%'>Correction_Transaction_RKCL_Txid</th>";
			echo "<th style='8%'>Correction_Transaction_Hash</th>";
			echo "<th style='8%'>Correction_Transaction_Key</th>";
			echo "<th style='10%'>Correction_Payment_Mode</th>";
			echo "<th style='8%'>Correction_Transaction_ProdInfo</th>";
			echo "<th style='8%'>Correction_Transaction_Email</th>";
			echo "<th style='8%'>Correction_Transaction_CenterCode</th>";
			echo "<th style='10%'>Correction_Transaction_DateTime</th>";
			echo "<th style='8%'>Correction_Transaction_Month</th>";
			echo "<th style='10%'>Correction_Transaction_Year</th>";
			echo "<th style='8%'>Correction_Transaction_Reconcile_Status</th>";
			echo "<th style='8%'>Correction_Transaction_timestamp</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
																			

			$response = $emp->reconcilation_type5_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Code'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Status'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Fname'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Amount'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Txtid'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_RKCL_Txid'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Hash'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Key'] . "</td>";
					echo "<td>" . $_Row['Correction_Payment_Mode'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Email'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_CenterCode'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_DateTime'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Month'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Year'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_Reconcile_Status'] . "</td>";
					echo "<td>" . $_Row['Correction_Transaction_timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_3"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Pay_Tran_Code</th>";
			echo "<th style='8%'>Pay_Tran_ITGK</th>";
			echo "<th style='8%'>Pay_Tran_Fname</th>";
			echo "<th style='8%'>Pay_Tran_RKCL_Trnid</th>";
			echo "<th style='8%'>Pay_Tran_AdmissionArray</th>";
			echo "<th style='8%'>Pay_Tran_LCount</th>";
			echo "<th style='8%'>Pay_Tran_Amount</th>";
			echo "<th style='10%'>Pay_Tran_Status</th>";
			echo "<th style='8%'>Pay_Tran_ProdInfo</th>";
			echo "<th style='8%'>Pay_Tran_Course</th>";
			echo "<th style='8%'>Pay_Tran_Batch</th>";
			echo "<th style='8%'>Pay_Tran_ReexamEvent</th>";
			echo "<th style='8%'>Pay_Tran_Eoi_Code</th>";
			echo "<th style='8%'>Pay_Tran_PG_ID</th>";
			echo "<th style='10%'>Pay_Tran_PG_Trnid</th>";
			echo "<th style='8%'>timestamp</th>";
			echo "<th style='8%'>Payumoney_Transaction</th>";
			echo "<th style='8%'>pay_verifyapi_status</th>";
			echo "<th style='8%'>Pay_Tran_Reconcile_status</th>";
			echo "<th style='8%'>razorpay_order_id</th>";
			echo "<th style='8%'>razorpay_payment_id</th>";
			echo "<th style='10%'>razorpay_payment_signature</th>";
			echo "<th style='8%'>paymentgateway</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$response = $emp->reconcilation_type5_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ITGK'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Fname'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_AdmissionArray'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_LCount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Amount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Course'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Batch'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ReexamEvent'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Eoi_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_ID'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_Trnid'] . "</td>";
					echo "<td>" . $_Row['timestamp'] . "</td>";
					echo "<td>" . $_Row['Payumoney_Transaction'] . "</td>";
					echo "<td>" . $_Row['pay_verifyapi_status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Reconcile_status'] . "</td>";
					echo "<td>" . $_Row['razorpay_order_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_signature'] . "</td>";
					echo "<td>" . $_Row['paymentgateway'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_4"){
			echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Invoice_No</th>";
    echo "<th style='8%'>Invoice_Amount</th>";
    echo "<th style='8%'>Invoice_Date</th>";
    echo "<th style='8%'>Invoice_Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
			$response = $emp->reconcilation_type5_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Invoice_No'] . "</td>";
					echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
					echo "<td>" . $_Row['Invoice_Date'] . "</td>";
					echo "<td>" . $_Row['Invoice_Status'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_5"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>ITGK Code</th>";
			echo "<th style='8%'>Transaction Number</th>";
			echo "<th style='8%'>Payment Amount</th>";
			echo "<th style='8%'>Payment Status</th>";
			echo "<th style='10%'>Payment Type</th>";
			echo "<th style='8%'>Payment Date</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$response = $emp->reconcilation_type5_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ITGK'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Txnid'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Amount'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Status'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
	}
/* Type 6 */
    if($reporttype == "name_fee"){
		$report = $_POST['report6'];
		if($report == "report_1"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>fld_ID</th>";
			echo "<th style='8%'>fld_requestChangeType</th>";
			echo "<th style='8%'>fld_ITGK_Code</th>";
			echo "<th style='8%'>fld_ITGK_UserCode</th>";
			echo "<th style='10%'>fld_RSP</th>";
			echo "<th style='8%'>fld_ref_no</th>";
			echo "<th style='10%'>fld_status</th>";
			echo "<th style='8%'>fld_updatedBy</th>";
			echo "<th style='8%'>Organization_Name</th>";
			echo "<th style='8%'>Organization_Name_old</th>";
			echo "<th style='10%'>Organization_Type</th>";
			echo "<th style='8%'>Organization_Type_old</th>";
			echo "<th style='10%'>Organization_OwnershipType</th>";
			echo "<th style='8%'>Organization_OwnershipType_old</th>";
			echo "<th style='8%'>Organization_AreaType</th>";
			echo "<th style='8%'>Organization_AreaType_old</th>";
			echo "<th style='10%'>Organization_Municipal_Type</th>";
			echo "<th style='8%'>Organization_Municipal_Type_old</th>";
			echo "<th style='8%'>Organization_Municipal</th>";
			echo "<th style='10%'>Organization_Municipal_old</th>";
			echo "<th style='8%'>Organization_WardNo</th>";
			echo "<th style='8%'>Organization_WardNo_old</th>";
			echo "<th style='10%'>Organization_Panchayat</th>";
			echo "<th style='8%'>Organization_Panchayat_old</th>";
			echo "<th style='10%'>Organization_Gram</th>";
			echo "<th style='8%'>Organization_Gram_old</th>";
			echo "<th style='10%'>Organization_Village</th>";
			echo "<th style='8%'>Organization_Village_old</th>";
			echo "<th style='10%'>Organization_Address</th>";
			echo "<th style='8%'>Organization_Address_old</th>";
			echo "<th style='10%'>fld_remarks</th>";
			echo "<th style='8%'>fld_remarks_rkcl</th>";
			echo "<th style='10%'>fld_document</th>";
			echo "<th style='8%'>fld_rentAgreement</th>";
			echo "<th style='10%'>fld_utilityBill</th>";
			echo "<th style='8%'>fld_ownershipDocument</th>";
			echo "<th style='8%'>fld_addedOn</th>";
			echo "<th style='8%'>fld_spActionDate</th>";
			echo "<th style='8%'>fld_rkclActionDate</th>";
			echo "<th style='8%'>fld_paymentDate</th>";
			echo "<th style='8%'>fld_updatedOn</th>";
			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
													

			$response = $emp->reconcilation_type6_1($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['fld_ID'] . "</td>";
					echo "<td>" . $_Row['fld_requestChangeType'] . "</td>";
					echo "<td>" . $_Row['fld_ITGK_Code'] . "</td>";
					echo "<td>" . $_Row['fld_ITGK_UserCode'] . "</td>";
					echo "<td>" . $_Row['fld_RSP'] . "</td>";
					echo "<td>" . $_Row['fld_ref_no'] . "</td>";
					echo "<td>" . $_Row['fld_status'] . "</td>";
					echo "<td>" . $_Row['fld_updatedBy'] . "</td>";
					echo "<td>" . $_Row['Organization_Name'] . "</td>";
					echo "<td>" . $_Row['Organization_Name_old'] . "</td>";
					echo "<td>" . $_Row['Organization_Type'] . "</td>";
					echo "<td>" . $_Row['Organization_Type_old'] . "</td>";
					echo "<td>" . $_Row['Organization_OwnershipType'] . "</td>";
					echo "<td>" . $_Row['Organization_OwnershipType_old'] . "</td>";
					echo "<td>" . $_Row['Organization_AreaType'] . "</td>";
					echo "<td>" . $_Row['Organization_AreaType_old'] . "</td>";
					echo "<td>" . $_Row['Organization_Municipal_Type'] . "</td>";
					echo "<td>" . $_Row['Organization_Municipal_Type_old'] . "</td>";
					echo "<td>" . $_Row['Organization_Municipal'] . "</td>";
					echo "<td>" . $_Row['Organization_Municipal_old'] . "</td>";
					echo "<td>" . $_Row['Organization_WardNo'] . "</td>";
					echo "<td>" . $_Row['Organization_WardNo_old'] . "</td>";
					echo "<td>" . $_Row['Organization_Panchayat'] . "</td>";
					echo "<td>" . $_Row['Organization_Panchayat_old'] . "</td>";
					echo "<td>" . $_Row['Organization_Gram'] . "</td>";
					echo "<td>" . $_Row['Organization_Gram_old'] . "</td>";
					echo "<td>" . $_Row['Organization_Village'] . "</td>";
					echo "<td>" . $_Row['Organization_Village_old'] . "</td>";
					echo "<td>" . $_Row['Organization_Address'] . "</td>";
					echo "<td>" . $_Row['Organization_Address_old'] . "</td>";
					echo "<td>" . $_Row['fld_remarks'] . "</td>";
					echo "<td>" . $_Row['fld_remarks_rkcl'] . "</td>";
					echo "<td>" . $_Row['fld_document'] . "</td>";
					echo "<td>" . $_Row['fld_rentAgreement'] . "</td>";
					echo "<td>" . $_Row['fld_utilityBill'] . "</td>";
					echo "<td>" . $_Row['fld_ownershipDocument'] . "</td>";
					echo "<td>" . $_Row['fld_addedOn'] . "</td>";
					echo "<td>" . $_Row['fld_spActionDate'] . "</td>";
					echo "<td>" . $_Row['fld_rkclActionDate'] . "</td>";
					echo "<td>" . $_Row['fld_paymentDate'] . "</td>";
					echo "<td>" . $_Row['fld_updatedOn'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_2"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>fld_ID</th>";
			echo "<th style='8%'>fld_ref_no</th>";
			echo "<th style='8%'>fld_Transaction_Status</th>";
			echo "<th style='8%'>fld_paymentTitle</th>";
			echo "<th style='10%'>fld_rsp</th>";
			echo "<th style='8%'>fld_ITGK_UserCode</th>";
			echo "<th style='10%'>fld_ITGK_Code</th>";
			echo "<th style='8%'>fld_RKCL_Trnid</th>";
			echo "<th style='8%'>fld_transactionID</th>";
			echo "<th style='8%'>fld_bankTransactionID</th>";
			echo "<th style='10%'>fld_amount</th>";
			echo "<th style='8%'>fld_paymentStatus</th>";
			echo "<th style='10%'>fld_addedOn</th>";
			echo "<th style='8%'>fld_updatedOn</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
																

			$response = $emp->reconcilation_type6_2($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['fld_ID'] . "</td>";
					echo "<td>" . $_Row['fld_ref_no'] . "</td>";
					echo "<td>" . $_Row['fld_Transaction_Status'] . "</td>";
					echo "<td>" . $_Row['fld_paymentTitle'] . "</td>";
					echo "<td>" . $_Row['fld_rsp'] . "</td>";
					echo "<td>" . $_Row['fld_ITGK_UserCode'] . "</td>";
					echo "<td>" . $_Row['fld_ITGK_Code'] . "</td>";
					echo "<td>" . $_Row['fld_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['fld_transactionID'] . "</td>";
					echo "<td>" . $_Row['fld_bankTransactionID'] . "</td>";
					echo "<td>" . $_Row['fld_amount'] . "</td>";
					echo "<td>" . $_Row['fld_paymentStatus'] . "</td>";
					echo "<td>" . $_Row['fld_addedOn'] . "</td>";
					echo "<td>" . $_Row['fld_updatedOn'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_3"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>Pay_Tran_Code</th>";
			echo "<th style='8%'>Pay_Tran_ITGK</th>";
			echo "<th style='8%'>Pay_Tran_Fname</th>";
			echo "<th style='8%'>Pay_Tran_RKCL_Trnid</th>";
			echo "<th style='8%'>Pay_Tran_AdmissionArray</th>";
			echo "<th style='8%'>Pay_Tran_LCount</th>";
			echo "<th style='8%'>Pay_Tran_Amount</th>";
			echo "<th style='10%'>Pay_Tran_Status</th>";
			echo "<th style='8%'>Pay_Tran_ProdInfo</th>";
			echo "<th style='8%'>Pay_Tran_Course</th>";
			echo "<th style='8%'>Pay_Tran_Batch</th>";
			echo "<th style='8%'>Pay_Tran_ReexamEvent</th>";
			echo "<th style='8%'>Pay_Tran_Eoi_Code</th>";
			echo "<th style='8%'>Pay_Tran_PG_ID</th>";
			echo "<th style='10%'>Pay_Tran_PG_Trnid</th>";
			echo "<th style='8%'>timestamp</th>";
			echo "<th style='8%'>Payumoney_Transaction</th>";
			echo "<th style='8%'>pay_verifyapi_status</th>";
			echo "<th style='8%'>Pay_Tran_Reconcile_status</th>";
			echo "<th style='8%'>razorpay_order_id</th>";
			echo "<th style='8%'>razorpay_payment_id</th>";
			echo "<th style='10%'>razorpay_payment_signature</th>";
			echo "<th style='8%'>paymentgateway</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$response = $emp->reconcilation_type6_3($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ITGK'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Fname'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_RKCL_Trnid'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_AdmissionArray'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_LCount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Amount'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Course'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Batch'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_ReexamEvent'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Eoi_Code'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_ID'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_PG_Trnid'] . "</td>";
					echo "<td>" . $_Row['timestamp'] . "</td>";
					echo "<td>" . $_Row['Payumoney_Transaction'] . "</td>";
					echo "<td>" . $_Row['pay_verifyapi_status'] . "</td>";
					echo "<td>" . $_Row['Pay_Tran_Reconcile_status'] . "</td>";
					echo "<td>" . $_Row['razorpay_order_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_id'] . "</td>";
					echo "<td>" . $_Row['razorpay_payment_signature'] . "</td>";
					echo "<td>" . $_Row['paymentgateway'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_4"){
			echo "<th style='10%'>S No.</th>";
    echo "<th style='10%'>Invoice_No</th>";
    echo "<th style='8%'>Invoice_Amount</th>";
    echo "<th style='8%'>Invoice_Date</th>";
    echo "<th style='8%'>Invoice_Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
			$response = $emp->reconcilation_type6_4($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Invoice_No'] . "</td>";
					echo "<td>" . $_Row['Invoice_Amount'] . "</td>";
					echo "<td>" . $_Row['Invoice_Date'] . "</td>";
					echo "<td>" . $_Row['Invoice_Status'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
		if($report == "report_5"){
			echo "<th style='10%'>S No.</th>";
			echo "<th style='10%'>ITGK Code</th>";
			echo "<th style='8%'>Transaction Number</th>";
			echo "<th style='8%'>Payment Amount</th>";
			echo "<th style='8%'>Payment Status</th>";
			echo "<th style='10%'>Payment Type</th>";
			echo "<th style='8%'>Payment Date</th>";

			echo "</tr>";
			echo "</thead>";
			echo "<tbody>";
			$response = $emp->reconcilation_type6_5($startdate, $enddate);
			$co = mysqli_num_rows($response[2]);	
			if ($co) {
				while ($_Row = mysqli_fetch_array($response[2])) {
					echo "<tr>";
					echo "<td>" . $_Count . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ITGK'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Txnid'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Amount'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Status'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_ProdInfo'] . "</td>";
					echo "<td>" . $_Row['Payment_Refund_Timestamp'] . "</td>";
					echo "</tr>";
					$_Count++;
				}
			} 
			else {
				echo "No Record Found";
			}
		}
	}
    
	echo "</tbody>";
	echo "</div>";
    
	
}



