<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsCorrectionStatusUpdate.php';
require 'DAL/sendsms.php';

$response = array();
$emp = new clsCorrectionStatusUpdate();

	if ($_action == "ShowDetails")
	{
        $response = $emp->GetAll($_POST['lcode']);
		$_DataTable = "";

		echo "<div class='table-responsive'>";
		echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>S No.</th>";
		echo "<th>Learner Code</th>";
		echo "<th>Learner Name</th>";
		echo "<th>Father Name</th>";
		echo "<th>D.O.B</th>";
		echo "<th>Correction Id</th>";
		echo "<th>Application For</th>";
		echo "<th>Exam Event</th>";
		echo "<th>Payment Status</th>";
		echo "<th>Marks</th>";						
		echo "<th>Action</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		
			$_Count = 1;	
			while ($_Row = mysqli_fetch_array($response[2])) {
			echo "<tr>";
			echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['lcode'] . "</td>";
			echo "<td>" . strtoupper($_Row['cfname']) . "</td>";
			echo "<td>" . strtoupper($_Row['cfaname']) . "</td>";
			echo "<td>" . $_Row['dob'] . "</td>"; 
			echo "<td>" . $_Row['cid'] . "</td>"; 
			echo "<td>" . strtoupper($_Row['applicationfor']) . "</td>"; 
			echo "<td>" . strtoupper($_Row['exameventname']) . "</td>"; 
			  if($_Row['Correction_Payment_Status'] == '0'){
				  $status = 'Pending';
				  echo "<td>" . $status . "</td>";
			  }
			  else {
				  $status = 'Confirmed';
				  echo "<td>" . $status . "</td>";
			  }
			 
			echo "<td>" . $_Row['totalmarks'] . "</td>";
			
			echo "<td> ". "<input type='button' data-toggle='modal' data-target='#UpdateStatus' id='".$_Row['cid']."' 
						name='Edit' id='Edit' class='btn btn-primary UpdateStatus' value='Update Status'/>"
				. "</td>";					
			
		
		
			echo "</tr>";
			$_Count++;
		}
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
}

if ($_action == "UpdateLearnerStatus") {
		$_Cid = $_POST["cid"];		
		$_Reason = $_POST["reason"];		
		$_Delv_Date = $_POST["delvdate"];		
		$response = $emp->UpdateLearnerProcessStatus($_Cid, $_Reason, $_Delv_Date);
		echo $response[0];
	
}

