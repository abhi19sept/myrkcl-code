<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsNcrDDPayment.php';

$response = array();
$emp = new clsNcrDDPayment();

if ($_action == "Update") {
    //print_r($_POST);
    if (isset($_POST["txtCenterCode"])) {
        $_AckCode = filter_var($_POST["txtAckcode"], FILTER_SANITIZE_STRING);
        $_Code = filter_var($_POST["txtCenterCode"], FILTER_SANITIZE_STRING);
        $_PayTypeCode = filter_var($_POST["productinfo"], FILTER_SANITIZE_STRING);
        //$_PayType = filter_var($_POST["productinfocode"], FILTER_SANITIZE_STRING);
        $_TranRefNo = filter_var($_POST["txtGenerateId"], FILTER_SANITIZE_STRING);

        $_firstname = filter_var($_POST["firstname"], FILTER_SANITIZE_STRING);
        $_phone = filter_var($_POST["phone"], FILTER_SANITIZE_STRING);
        $_email = filter_var($_POST["email"], FILTER_SANITIZE_STRING);

        $_amount = filter_var($_POST["amount"], FILTER_SANITIZE_STRING);
        $_ddno = filter_var($_POST["ddno"], FILTER_SANITIZE_STRING);
        $_dddate = filter_var($_POST["dddate"], FILTER_SANITIZE_STRING);

        $_txtMicrNo = filter_var($_POST["txtMicrNo"], FILTER_SANITIZE_STRING);
        $_ddlBankDistrict = filter_var($_POST["ddlBankDistrict"], FILTER_SANITIZE_STRING);
        $_ddlBankName = filter_var($_POST["ddlBankName"], FILTER_SANITIZE_STRING);
        $_txtBranchName = filter_var($_POST["txtBranchName"], FILTER_SANITIZE_STRING);

        $response = $emp->Update($_AckCode, $_Code, $_PayTypeCode, $_TranRefNo, $_firstname, $_phone, $_email, $_amount, $_ddno, $_dddate, $_txtMicrNo, $_ddlBankDistrict, $_ddlBankName, $_txtBranchName);
        echo $response[0];
    }
}

if ($_action == "GETCENTERCODE") {  
    $response = $emp->GetCenterCode($_POST['code']);
    echo "<option value='0' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Correction_TranRefNo'] . ">" . $_Row['Correction_ITGK_Code'] . "</option>";
    }
}


if ($_action == "GETDD") {
    $response = $emp->GETDD($_POST['ackno']);
    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";  
    echo "<th>RSP Code.</th>";
    echo "<th>Center Name</th>";  
    echo "<th>DD No.</th>";
    echo "<th >DD Date</th>";  
    echo "<th >DD Amount</th>"; 
    echo "<th >DD Image</th>"; 
    echo "<th >Action</th>"; 	
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
             echo "<td>" . $_Row['Org_RspLoginId'] . "</td>";          
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>" . $_Row['dd_no'] . "</td>";          
            echo "<td>" . $_Row['dd_date'] . "</td>";
			echo "<td>" . $_Row['dd_amount'] . "</td>";
			echo "<td>".'<a href="upload/Ncr_dd_payment/'.$_Row['dd_Transaction_Txtid'].'_ddpayment.png'.'" target="_blank">' . '<img alt="No Image Found" width="50" height="35" src="upload/Ncr_dd_payment/'.$_Row['dd_Transaction_Txtid'].'_ddpayment.png'.'"/>' . "</a></td>";
			//echo "<td><a href='upload/admission_dd_payment/".$_Row['dd_Transaction_Txtid']."_ddpayment.png'>". "</a></td>";
            echo "<td><input type='checkbox' id=chk" . $_Row['dd_Transaction_Txtid'].
			" name=chk" . $_Row['dd_Transaction_Txtid']."> </input>"                
                . "</td>";
            echo "</tr>";
            $_Count++;
        }

        echo "</tbody>";
        echo "</table>";
		echo "</div>";
   }
//     else {
//        echo "No";
//    }
}


if ($_action == "UPDATEDDSTATUS") {           
	$_UserPermissionArray=array();
        $_AdmissionCode = array();
	$_i = 0;
        $_Count=0;
        $l="";
        foreach ($_POST as $key => $value)
            {
               if(substr($key, 0,3)=='chk')
               {                				  
                $l .= substr($key, 3) . ",";				 
               }			   
                $_AdmissionCode = rtrim($l, ",");											
            }
            if($_AdmissionCode==""){
                echo "0";
            }
            else {
//                $_SESSION['LearnerCorrectionCodes'] = $_AdmissionCode;                 				
            $response=$emp->UpdateDDPaymentStatus($_AdmissionCode);               		
            echo $response[0]; 
            }            
	}


?>
