<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsAdmissionRspWiseCount.php';

$response = array();
$emp = new clsAdmissionRspWiseCount();

if ($_action == "GETDATA") {

    $response = $emp->GetDataAll($_POST['course'], $_POST['batch']);

    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' width='100%' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th>S No.</th>";
    echo "<th>RSP Code</th>";   
    echo "<th>RSP Name</th>";
    echo "<th>Total Uploaded Learner Count</th>";
    echo "<th>Total Confirmed Learner Count</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    $_TotalUploaded = 0;
    $_TotalConfirmed = 0;
   
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";       
            echo "<td>" . $_Row['User_LoginID'] . "</td>";
            echo "<td>" . $_Row['Organization_Name'] . "</td>";
            echo "<td>'" . $_Row['Admission_LearnerCode'] . "</td>";
            echo "<td>" . $_Row['Confirmed_Learner'] . "</td>";
            $_Count++;
            $_TotalUploaded = $_TotalUploaded + $_Row['Admission_LearnerCode'];
            $_TotalConfirmed = $_TotalConfirmed + $_Row['Confirmed_Learner'];
        }

        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        echo "<th >  </th>";
        echo "<th >  </th>";
        echo "<th >Total</th>";
        echo "<th>";
        echo "$_TotalUploaded";
        echo "</th>";
        echo "<th>";
        echo "$_TotalConfirmed";
        echo "</th>";
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
        echo "</div>";
   
}

