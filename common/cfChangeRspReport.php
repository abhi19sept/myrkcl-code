<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsChangeRspReport.php';

$response = array();
$emp = new clsChangeRspReport();


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='20%'>IT-GK Code</th>";
    echo "<th style='60%'>RSP Name</th>";
    echo "<th style='10%'>Aplication Date</th>";
    echo "<th style='40%'>Application Status</th>";
    echo "<th style='40%'>Reson of change</th>";
    echo "<th style='10%'>Remarks</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {
            echo "<tr class='odd gradeX'>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . $_Row['Rspitgk_ItgkCode'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Rspname'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Date'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Status'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_RspSelectReason'] . "</td>";
            echo "<td>" . $_Row['Rspitgk_Reason'] . "</td>";            
            echo "</tr>";
            $_Count++;
        }
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


