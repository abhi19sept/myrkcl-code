<?php

/*
 * author Viveks

 */
include './commonFunction.php';
require 'BAL/clsexamlocations.php';

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$response = array();
$emp = new clsexamchoicestatus();

if ($_action == "SwitchChoices") {
	if (! $_SESSION['final_count']) {
		if (isset($_POST['eventid']) && !empty($_POST['eventid'])) {
			$eventId = $_POST['eventid'];
		} else {
			$events = $emp->getExamEvents();
        	if ($events) {
	            foreach ($events as $examId => $eventName) {
					$eventId = $examId;
				}
			}
		}
		
		for ($i = 2; $i < 6; $i++) {
			$emp->switchLessCountLocations($i, $_POST['eventid']);
		}

		$emp->setInitialAllotments($_POST['eventid']);

		echo "<b>Auto switch done.</b>";
	} else {
		echo "<b>Unable to switch after Final Mapping Done.</b>";
	}
}

if ($_action == 'setasfinalized') {
	$emp->setAllotmentsAsFinalized($_POST['eventid']);

	echo "<b>Allotments set as finalyzed.</b>";
}

if ($_action == 'resetonfinalized') {
	$emp->resetonFinalizedAllotments($_POST['eventid']);

	echo "<b>Reset on finalyzed allotments.</b>";	
}

if ($_action == "hardreset") {
	$emp->resetAsHard();
	echo "<b>Reset done, Now ready to assign locations.</b>";
}

if ($_action == "assignchoices") {
	if (! $_SESSION['final_count']) {
		$emp->assignChoices();
		echo "<b>Locations assigned.</b>";
	} else {
		echo "<b>Unable to re-assign choices, after Final Mapping Done.</b>";
	}
}

if ($_action == "resetChoices") {
	if (! $_SESSION['final_count']) {
		$emp->resetChoices($_POST['eventid']);
		echo "<b>Set as default.</b>";
	} else {
		echo "<b>Unable to reset choices, after Final Mapping Done.</b>";
	}
}

if ($_action == "generateFinalMapping") {
	if (! $_SESSION['final_count']) {
		if (isset($_SESSION['Exam_Districts'])) {
			unset($_SESSION['Exam_Districts']);
		}
		$emp->generateFinalMapping($_POST['eventid']);
		$_SESSION['final_count'] = $emp->getFinalMappingCount($_POST['eventid']);
		echo "<b>Final mapping generated.</b>";
	} else {
		echo "<b>Final mapping already generated, to regenerate please remove previously gererated mapping first.</b>";
	}
}

if ($_action == "ShowDistricts") {
	if (! $_SESSION['final_count']) {
		$_SESSION['final_count'] = $emp->getFinalMappingCount($_POST['eventid']);
	}
	fetchExamDistricts($_POST['eventid'], $_POST['itgk']);
	if (isset($_SESSION['Exam_Districts']) && ! empty($_SESSION['Exam_Districts'])) {
		$thesilsWithLessCounts = $emp->getThesilsWithLessCounts(150, $_POST['eventid']);
	}
	echo getExamDistrictsDropDown('', $thesilsWithLessCounts, 'all');
}

function fetchExamDistricts($eventid, $itgk) {
	global $emp;
	$response = $emp->GetAllLocationDistricts($eventid, [], $itgk);
	$_SESSION['Exam_Districts'] = [];
	if ($response) {
		$_SESSION['Exam_Districts'] = $response;
	}
}

function getExamDistrictsDropDown($selectId = '', $thesilsWithLessCounts = [], $all = '') {
	$content = '';
	if (isset($_SESSION['Exam_Districts'])) {
		$content = "<option value=''>Select District</option>";
		if ($all == 'all') $content .= "<option value='all'>All Locations</option>";
		foreach ($_SESSION['Exam_Districts'] as $districtId => $district) {
			$selected = ($selectId == $districtId) ? 'selected' : '';
			$style = ((isset($thesilsWithLessCounts['less_count']) || isset($thesilsWithLessCounts['vmou_count_missmatch'])) && (in_array($districtId, $thesilsWithLessCounts['less_count']) || in_array($districtId, $thesilsWithLessCounts['vmou_count_missmatch']))) ? 'style="color:red;" title="Invalid Thesil(s) counts."' : '';
			$content .= '<option value="' . $districtId . '" ' . $selected . $style . ' >' . $district['name'] . '</option>';
		}
	}

	return $content;
}

function getExamDistrictWiseThesilDropDown($eventid, $district, $itgkthesil, $examthesil = '', $preid = '', $splClass = 'setExamThesil') {
	$content = '';
	if (isset($_SESSION['Exam_Districts'][$district]['thesils'])) {
		$content = "<select id='" . $preid . $eventid . "_" . $itgkthesil . "_" . $examthesil . "' name='ddlThesil' class='form-control " . $splClass . "'><option value=''>Select Thesil</option>";

		foreach ($_SESSION['Exam_Districts'][$district]['thesils'] as $tehsilCode => $tehsilName) {
			$selected = ($examthesil == $tehsilCode) ? 'selected' : '';
			$content .= '<option value="' . $tehsilCode . '" ' . $selected . '>' . $tehsilName . '</option>';
		}
		$content .= "</select>";
	}

	return $content;
}

function getAllExamLocationWiseCounts ($eventid) {
	//$_SESSION['Exam_Districts'][$_POST['districtId']]['location_details']
	$response = $_SESSION['Exam_Districts'];
	$content = "<div class='table-responsive' style='margin-top:10px'>";
    $content .= "<b>Final Exam Location Counts:</b><table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    $content .= "<thead>";
    $content .= "<tr>";
	$content .= "<th style='22%'>Exam District</th>";
	$content .= "<th style='22%'>Exam Location</th>";
	$content .= "<th style='22%'>Location Count</th>";
	$content .= "<th style='22%'>VMOU Count</th>";
    $content .= "</tr>";
    $content .= "</thead>";
    $content .= "<tbody>";
	foreach ($response as $districtId => $location_details) {
		$content .= getExamLocationCounts($eventid, $districtId);
	}

	$content .= "</tbody>";
    $content .= "</table>";
	$content .= "</div>";

	return $content;
}

function getExamLocationCounts($eventid, $districtId) {
	global $emp;
	$response = $emp->showExamLocationCounts($eventid, $districtId);
    $content = '';
	foreach ($response as $row) {
		$count = $row['n'];
		$vmouCount = (!empty($row['vmou_count'])) ? $row['vmou_count'] : 0;
		$style = ($count < 150 || (!empty($vmouCount) && $vmouCount != $count)) ? 'style="color:red;" title="Either location count < 150 OR VMOU count not matched."' : '';
		$diff = (!empty($vmouCount) && $vmouCount != $count) ? ($vmouCount - $count) : '';
		$diff = ($diff < 0) ? ' (' . $diff . ')' : ( $diff > 0 ? ' (+' . $diff . ')' : '' );
		$content .= "<tr $style>";
		$content .= '<td>' . $row['District_Name'] . '</td>';
		$content .= '<td>' . $row['exam_thesil'] . '</td>';
		$content .= '<td>' . $count . '</td>';
		$content .= '<td>' . $vmouCount . $diff . '</td>';
		$content .= "</tr>";
	}

	return $content;
}

function getExamLocationWiseCounts($eventid, $districtId) {
	global $emp;
	$response = $emp->showExamLocationCounts($eventid, $districtId);
	$content = "<div class='table-responsive' style='margin-top:10px'>";
    $content .= "<b>Final Exam Location Counts:</b><table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    $content .= "<thead>";
    $content .= "<tr>";
    $content .= "<th style='12%'>S No.</th>";
	$content .= "<th style='22%'>Exam District</th>";
	$content .= "<th style='22%'>Exam Location</th>";
	$content .= "<th style='22%'>Location Count</th>";
	$content .= "<th style='22%'>VMOU Count</th>";
    $content .= "</tr>";
    $content .= "</thead>";
    $content .= "<tbody>";
    $sn = 1;
	foreach ($response as $row) {
		$count = $row['n'];
		$vmouCount = (!empty($row['vmou_count'])) ? $row['vmou_count'] : 0;
		$style = ($count < 150 || (!empty($vmouCount) && $vmouCount != $count)) ? 'style="color:red;" title="Either location count < 150 OR VMOU count not matched."' : '';
		$diff = (!empty($vmouCount) && $vmouCount != $count) ? ($vmouCount - $count) : '';
		$diff = ($diff < 0) ? ' (' . $diff . ')' : ( $diff > 0 ? ' (+' . $diff . ')' : '' );
		$content .= "<tr $style>";
		$content .= '<td>' . $sn . '</td>';
		$content .= '<td>' . $row['District_Name'] . '</td>';
		$content .= '<td>' . $row['exam_thesil'] . '</td>';
		$content .= '<td>' . $count . '</td>';
		$content .= '<td>' . $vmouCount . $diff . '</td>';
		$content .= "</tr>";
		$sn++;
	}
	$content .= "</tbody>";
    $content .= "</table>";
	$content .= "</div>";

	return $content;
}

function resetSession($eventid, $newThesil,$itgkThesil,$examThesil, $itgk = '') {
	global $emp;
	$getDistricts = $emp->getThesilDistrict([$newThesil,$itgkThesil,$examThesil]);
	if ($getDistricts) {
		$districts = array_unique($getDistricts);
		$examDistricts = $emp->GetAllLocationDistricts($eventid, $districts, $itgk);
		foreach ($examDistricts as $districtId => $locationDetails) {
			$_SESSION['Exam_Districts'][$districtId] = $locationDetails;
		}
	}
}

if ($_action == "FillExamEvents") {
	$response = $emp->getExamEvents();
	if ($response) {
		$content = "<option value=''>Select Exam Event</option>";
		foreach ($response as $eventId => $eventName) {
			$content .= '<option value="' . $eventId . '">' . $eventName . '</option>';
		}
		echo $content;
	}
}

if ($_action == "getDistrictExamThesils") {
	echo getExamDistrictWiseThesilDropDown($_POST['eventId'], $_POST['districtId'], $_POST['tehsilId']);
}

if ($_action == "getDistrictThesilsDD") {
	$elem = explode('_', $_POST['elem']);
	echo getExamDistrictWiseThesilDropDown($_POST['eventId'], $_POST['districtId'], $elem[1], $elem[2], 'location_', '');
}

if ($_action == "setExamThesil") {
	$elementInfo = explode('_', $_POST['elementId']);
    $eventid = $elementInfo[0];
    $newThesil = $_POST['tehsilId'];
    $itgkThesil = $elementInfo[1];
    $examThesil = $elementInfo[2];
    $follow = $_POST['follow'];
	$emp->setExamThesil($newThesil, $eventid, $itgkThesil, $examThesil, $_POST['itgk'], '', $follow);
	resetSession($eventid, $newThesil, $itgkThesil, $examThesil);
}

if ($_action == "showDistrictLocationCounts" || $_action == "showEditableLocationCounts") {
	$eventid = $_POST['eventid'];
	if ($_action == "showEditableLocationCounts" && $_POST['districtId'] == 'all') die;
	if (!empty($_POST['districtId']) && $_POST['districtId'] != 'all') {
		$response = (isset($_SESSION['Exam_Districts'])) ? $_SESSION['Exam_Districts'][$_POST['districtId']]['location_details'] : '';
	}
	$content = "";
	if ($response || empty($_POST['districtId'])) {
		if ($_action == "showDistrictLocationCounts") {
			echo $content .= getExamLocationWiseCounts($eventid, $_POST['districtId']);
		} elseif ($_action == "showEditableLocationCounts" && !empty($_POST['districtId'])) {
		$content = "<div class='table-responsive' style='margin-top:10px'>";
	    $content .= "<b>Update Exam Locations:</b>";
	    $content .= '<label class="radio-inline small"><input type="checkbox" id="follow_choice" checked class="follow_choice" value="1" name="follow_choice" /> <b>Follow ITGK Choices Strictly.</b></label>';
	    $content .= "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
	    $content .= "<thead>";
	    $content .= "<tr>";
	    $content .= "<th style='12%'>S No.</th>";
	    $content .= "<th style='22%'>ITGK Thesil</th>";
		$content .= "<th style='22%'>Exam District</th>";
		$content .= "<th style='22%'>Exam Location</th>";
		$content .= "<th style='22%'>Location Count</th>";
	    $content .= "</tr>";
	    $content .= "</thead>";
	    $content .= "<tbody>";
	    $sn = 1;
	     $chooseThesilClass = $countClass = $setThesilClass = '';
	    if (! $_SESSION['final_count']) {
	    	$countClass = 'ThesilCounts';
	    	$chooseThesilClass = 'chooseThesil';
	    	$setThesilClass = 'setExamThesil';
	    }
		foreach ($response as $row) {
			$thesil = $row['thesil'];
			if ($row['districtId'] <> $row['Tehsil_District']) {
				$actualDistrict = $emp->getDistrict($row['districtId']);
				if ($actualDistrict) {
					$thesil .= ' (' . $actualDistrict[$row['districtId']] . ')';
				}
			}
			$content .= "<tr>";
			$content .= '<td>' . $sn . '</td>';
			$content .= '<td>' . $thesil . '</td>';
			$content .= '<td><select id="' . $row['location_rkcl'] . '" name="ddlThesilDistrict" class="form-control ' . $chooseThesilClass . '">' . getExamDistrictsDropDown($row['Tehsil_District']) . '</select></td>';
			$content .= '<td><div class="ddlThesil_' . $row['location_rkcl'] . '">' . getExamDistrictWiseThesilDropDown($eventid, $row['Tehsil_District'], $row['thesilId'], $row['location_rkcl'], '', $setThesilClass) . '</div></td>';
			$content .= '<td><div class="' . $countClass . '" id="' . $row['thesilId'] . '_' . $row['location_rkcl'] . '" align="center"><span class="aslink">' . $row['n'] . '</span></div></td>';
			$content .= "</tr>";
			$sn++;
		}
		$content .= "</tbody>";
	    $content .= "</table>";
		echo $content .= "</div>";
		}
	} elseif ($_POST['districtId'] == 'all' && isset($_SESSION['Exam_Districts'])) {
		echo $content = getAllExamLocationWiseCounts($eventid);
	}
}

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

   echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='15%'>S No.</th>";
    echo "<th style='35%'>CenterCode</th>";
	 
	   echo "<th style='35%'>District</th>";
	    
		 echo "<th style='35%'>1st Preference</th>";
		  echo "<th style='35%'>2nd  Preference</th>";
		   echo "<th style='35%'>3rd Preference</th>";
		   echo "<th style='35%'>4th Preference</th>";
		   echo "<th style='35%'>5th Preference</th>";
		   
    
    echo "<th style='40%'>Action</th>";
	 echo "<th style='20%'>Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
         echo "<td>" . $_Row['centercode'] . "</td>";
		 
		   echo "<td>" . $_Row['District_Name'] . "</td>";
		   
		    echo "<td>" . $_Row['choice1'] . "</td>";
			 echo "<td>" . $_Row['choice2'] . "</td>";
			  echo "<td>" . $_Row['choice3'] . "</td>";
			  echo "<td>" . $_Row['choice4'] . "</td>";
			  echo "<td>" . $_Row['choice5'] . "</td>";
			 echo "<td nowrap>";
			 if($_Row['status']=='Approve')
			  {
				  echo "<a href='frmexamchoicefinalapproval.php?code=" . $_Row['id'] . "&Mode=Disapprove' style='font-size:15px;'>"
				. "<span >Deny</span></a>";
				
			  }
			  else  if($_Row['status']=='Disapprove')
			  {
					echo "<a href='frmexamchoicefinalapproval.php?code=" . $_Row['id'] . "&Mode=Approve' style='font-size:15px;'>"
                . "<span  >Approve  </span></a>";
		      }
			  else  
			  {
				  
				  echo "<a href='frmexamchoicefinalapproval.php?code=" . $_Row['id'] . "&Mode=Approve' style='font-size:15px;'>"
                . "<span  >Approve / </span></a>
				 <a href='frmexamchoicefinalapproval.php?code=" . $_Row['id'] . "&Mode=Disapprove' style='font-size:15px;'>"
				. "<span >Deny</span></a>";
					
		      }
				
			echo "</td>";
				
			  if($_Row['status']=='Approve')
			  {
			 
				echo "<td><span   >Approved</span></td>";
			  }
			  else if($_Row['status']=='Disapprove')
			  {
				echo "<td><span  >Denied</span> <br>(" . $_Row['Reasion'] . ")</td>";
			  }
			  else 
			  {
				echo "<td><span>Pending</span></td>";
			  }
				
		
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
	echo "</div>";
	
}

if ($_action == "getLocationName") {
	$thesils = explode('_', $_POST['elem']);
	$districts = $emp->getThesilDistrict([$thesils[0]]);
	foreach ($districts as $thesilCode => $thesilDistrictId) {
		$thesil = $emp->getDistrictThesils($thesilDistrictId, $thesilCode);
	}
	print $thesil[$thesilCode];
}

function getLocationWiseITGK($eventid, $elem) {
	global $emp;

	$thesils = explode('_', $elem);
	$districts = $emp->getThesilDistrict([$thesils[0]]);
	foreach ($districts as $thesilCode => $thesilDistrictId) {
		$districtDD = getExamDistrictsDropDown($thesilDistrictId);
		$locationDD = getExamDistrictWiseThesilDropDown($eventid, $thesilDistrictId, $thesilCode, $thesils[1], 'location_', 'switch_thesil');
		$itgkLocationCounts = $emp->getLocationWiseItgkCount($eventid, $thesils[0], $thesils[1]);
		$itgkDD = '';
		if ($itgkLocationCounts) {
			$totalCount = array_sum($itgkLocationCounts);
			$itgkDD = '<select id="itgk_' . $elem . '" name="ddlThesilItgk" class="form-control chooseItgk">';
			$itgkDD .= "<option value=''>Select</option>";
			$itgkDD .= "<option value='all'>All ITGK (" . $totalCount . ")</option>";
			foreach ($itgkLocationCounts as $itgkCode => $count) {
				$itgkDD .= "<option value='" . $itgkCode . "'>" . $itgkCode . " (" . $count . ")</option>";
			}
			$itgkDD .= '</select>';
		}
	}

	return [$itgkDD, $districtDD, $locationDD];
}

if ($_action == "getLocationList") {
	$DD = getLocationWiseITGK($_POST['eventid'], $_POST['elem']);
	$itgkDD = $DD[0];
	$districtDD = $DD[1];
	$locationDD = $DD[2];
	$content = "<div class='table-responsive' style='margin-top:10px'>";
    $content .= "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
    $content .= "<thead>";
    $content .= "<tr>";
    $content .= "<th style='22%'>ITGK</th>";
	$content .= "<th style='22%'>District</th>";
	$content .= "<th nowrap style='22%'>Switch to Location</th>";
	$content .= "<th style='22%'></th>";
    $content .= "</tr>";
    $content .= "</thead>";
    $content .= "<tbody>";
    $content .= "<tr>";
    $content .= '<td style="22%"><span id="itgk_choice">' . $itgkDD . '</span></td>';
	$content .= '<td style="22%"><select id="district_' . $_POST['elem'] . '" name="ddlThesilDistrict" class="form-control chooseDistrict">' . $districtDD . '</select></td>';
	$content .= "<td style='22%'><div id='district_thesils'>" . $locationDD . "</div></td>";
	$content .= '<td style="22%"><input type="button" id="' . $_POST['elem'] . '" class="switch_location" value="Switch Location"></td>';
    $content .= "</tr>";
	$content .= "</tbody>";
    $content .= "</table>";
	echo $content .= "</div><div id='itgk_list'></div>";
}

if ($_action == "getLocationItgkIds") {
	global $emp;
	$thesils = array_reverse(explode('_', $_POST['elem']));
	echo $emp->getLocationITGKIds($_POST['eventid'],$thesils[1], $thesils[0], $_POST['newthesil']);
}

if ($_action == "getItgkList") {
	$newthesil = ($_POST['follow']) ? $_POST['newthesil'] : '';
	$thesils = array_reverse(explode('_', $_POST['elem']));
	$itgkList = $emp->getItgkWiseLearnerListOfaLocation($_POST['eventid'], $_POST['itgk'], $thesils[1], $thesils[0], $newthesil);
	if ($itgkList) {
		$content = "<div class='table-responsive' style='margin-top:10px'>";
	    $content .= "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered small'>";
	    $content .= "<thead>";
	    $content .= "<tr>";
	    $content .= "<th style='22%'>Sno.</th>";
	    $content .= "<th style='22%'>ITGK</th>";
		$content .= "<th style='22%'>Learner Code</th>";
		$content .= "<th style='22%'>Name</th>";
		$content .= "<th style='22%' nowrap align='right'><input type='text' maxlength='6' size='3' id='checkbycount' name='checkbycount' /><label class='radio-inline'><b> / All</b> <input type='checkbox' id='checkall' class='checkall' value='1' name='checkall' /> </label></th>";
	    $content .= "</tr>";
	    $content .= "</thead>";
	    $content .= "<tbody>";
	    $sn = 1;
	    foreach ($itgkList as $id => $row) {
		    $content .= "<tr>";
		    $content .= '<td style="10%">' . $sn . '</td>';
		    $content .= '<td style="20%">' . $row['itgkcode'] . '</td>';
			$content .= '<td style="20%">' . $row['learnercode'] . '</td>';
			$content .= "<td style='20%'>" . ucwords($row['learnername']) . "</td>";
			$content .= "<td style='20%' align='center'><input type='checkbox' id='check_" . $id . "' class='checklearner' value='" . $id . "' name='chkids[]" . $id . "' /></td>";
		    $content .= "</tr>";
		    $sn++;
		}
		$content .= "</tbody>";
	    $content .= "</table>";
		echo $content .= "</div>";
	} else {
		echo "No record found.";
	}
}

if ($_action == "switchSelectedLearnerLocation") {
	$newTehsilId = $_POST['location'];
	$eventId = $_POST['eventid'];
	$elem = explode('_', $_POST['elem']);
	$itgkThesil = $elem[0];
	$examThesil = $elem[1];
	$itgkcode = ($_POST['itgk'] != 'all') ? $_POST['itgk'] : '';
	$ids = ($_POST['ids'] != 'all') ? $_POST['ids'] : '';
	$follow = $_POST['follow'];
	//print_r($elem);
	//echo "$newTehsilId, $eventId, $itgkThesil, $examThesil, $itgkcode, $ids";
	$emp->setExamThesil($newTehsilId, $eventId, $itgkThesil, $examThesil, $itgkcode, $ids, $follow);
	resetSession($eventId, $newTehsilId, $itgkThesil, $examThesil);
}

