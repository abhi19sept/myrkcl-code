<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'commonFunction.php';
require 'BAL/clsWcdApplicantReporting.php';

$response = array();
$emp = new clsWcdApplicantReporting();

if ($_action == "FillAdmissionCourse") {
    $response = $emp->GetCourse();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Course_Code'] . ">" . $_Row['Course_Name'] . "</option>";
    }
}

if ($_action == "FILLBatchName") {
    $response = $emp->FILLBatchName($_POST['values']);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Batch_Code'] . ">" . $_Row['Batch_Name'] . "</option>";
    }
}


if ($_action == "showdata") {
	if($_POST['batch'] !='') {
    $response = $emp->getwcdlearnerdetails($_POST['course'],$_POST['batch']);

   $_DataTable = "";
				echo "<div class='table-responsive'>";
				echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
				echo "<thead>";
				echo "<tr>";
				echo "<th>S No.</th>";
					$_LoginUserRole = $_SESSION['User_UserRoll'];
					if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4') {
						echo "<th>ITGK Code</th>";
						echo "<th>Approved Applicant Count</th>";
						echo "<th>Reported Applicant Count</th>";
						echo "<th>Not Reported Applicant Count</th>";
					}
					else if($_LoginUserRole == '14') {
						echo "<th>ITGK Code</th>";
						echo "<th>Approved Applicant Count</th>";
						echo "<th>Reported Applicant Count</th>";
						echo "<th>Not Reported Applicant Count</th>";
					}else if($_LoginUserRole == '17') {
						echo "<th>ITGK Code</th>";
						echo "<th>Approved Applicant Count</th>";
						echo "<th>Reported Applicant Count</th>";
						echo "<th>Not Reported Applicant Count</th>";
					}
					else{
						echo "<th>Applicant Id</th>";
						echo "<th>Learner Name</th>";
						echo "<th >Father Name</th>";	
						echo "<th >Marital Status</th>";	
						echo "<th >Sub Category</th>";	
						echo "<th >Caste Category</th>";
						echo "<th >Mobile No.</th>";	
						echo "<th >Action</th>";
					}
			
				echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				$_Count = 1;
					$_Total = 0;
					$_ATotal = 0;
					$_NTotal = 0;
				while ($_Row = mysqli_fetch_array($response[2])) {
				//	if($_Row['pref'] > 9) {
							echo "<tr class='odd gradeX'>";
							echo "<td>" . $_Count . "</td>";
									$_LoginUserRole = $_SESSION['User_UserRoll'];
								if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '4') {
										//echo "<td>" . $_Row['District_Name'] . "</td>";		
										echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";		
										echo "<td>" . $_Row['pref'] . "</td>";		
										echo "<td>" . $_Row['Reported_Learner'] . "</td>";											
										echo "<td>" . $_Row['NotReported_Learner'] . "</td>";
								}
								else if($_LoginUserRole == '14') {
										echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";	
										echo "<td>" . "" . $_Row['pref'] . "</td>";
										echo "<td>" . $_Row['Reported_Learner'] . "</td>";
										echo "<td>" . $_Row['NotReported_Learner'] . "</td>";
								}else if($_LoginUserRole == '17') {
										echo "<td>" . $_Row['Oasis_Admission_Final_Preference'] . "</td>";	
										echo "<td>" . "" . $_Row['pref'] . "</td>";
										echo "<td>" . $_Row['Reported_Learner'] . "</td>";
										echo "<td>" . $_Row['NotReported_Learner'] . "</td>";
								}
								else{
									echo "<td>" . $_Row['Oasis_Admission_LearnerCode'] . "</td>";
									echo "<td>" . $_Row['Oasis_Admission_Name'] . "</td>";
									echo "<td>" . $_Row['Oasis_Admission_Fname'] . "</td>";
									echo "<td>" . $_Row['Oasis_Admission_MaritalStatus'] . "</td>";
									echo "<td>" . $_Row['Oasis_Admission_Subcategory'] . "</td>";
									
									echo "<td>" . $_Row['Category_Name'] . "</td>";
			
									echo "<td>" . $_Row['Oasis_Admission_Mobile'] . "</td>";
										if($_Row['Oasis_Admission_ReportedStatus'] == 'Reported') {
											echo "<td>" . 'Reported' . "</td>";
										}										
										else {
											echo "<td>"
											. "<input type='button' name='Edit' class='btn btn-primary upd_details'
											batch='".$_POST['batch']."' itgk='".$_Row['Oasis_Admission_Final_Preference']."'
											course='".$_POST['course']."' id='".$_Row['Oasis_Admission_LearnerCode']."'
											value='MARK REPORTED'/>"						
											. "</td>";
											 
										}
								}
							
								echo "</tr>";
			if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '14' || $_LoginUserRole == '16' || $_LoginUserRole == '4'
				|| $_LoginUserRole == '17'){				
								$_Total = $_Total + $_Row['pref'];
								$_ATotal = $_ATotal + $_Row['Reported_Learner'];
								$_NTotal = $_NTotal + $_Row['NotReported_Learner'];
							
					}
						$_Count++;			
				}
				 echo "</tbody>";
		if($_LoginUserRole == '1' || $_LoginUserRole == '11' || $_LoginUserRole == '16' || $_LoginUserRole == '14' || $_LoginUserRole == '4'
				|| $_LoginUserRole == '17'){
						echo "<tfoot>";
						echo "<tr>";
						echo "<th >  </th>";
						echo "<th >Total Count </th>";
						echo "<th>";
						echo "$_Total";
						echo "</th>";
						echo "<th >";
						echo "$_ATotal";
						echo "</th>";
						echo "<th >";
						echo "$_NTotal";
						echo "</th>";
						echo "</tr>";
						echo "</tfoot>";
	}
				 echo "</table>";
				 echo "</div>";
		 
		}
		  else {
			  echo "1";
		  }
}


if ($_action == "EDIT") {
    $response = $emp->GetDatabyCode($_POST['batch'], $_POST['lcode']);
    $_DataTable = array();
    $_i = 0;
    while ($_Row = mysqli_fetch_array($response[2])) {
        $_DataTable[$_i] = array("LearnerCode" => $_Row['Oasis_Admission_LearnerCode'],
            "lname" => $_Row['Oasis_Admission_Name'],
            "fname" => $_Row['Oasis_Admission_Fname'],
            "dob" => $_Row['Oasis_Admission_DOB'],
			"itgk" => $_Row['Oasis_Admission_Final_Preference'],
			"district" => $_Row['Oasis_Admission_District'],			
			"minper" => $_Row['Oasis_Admission_MINPercentage'],			
			"highper" => $_Row['Oasis_Admission_Percentage'],			
            "mstatus" => $_Row['Oasis_Admission_MaritalStatus'],            
			"categoryname" => $_Row['Category_Name'],
			"category" => $_Row['Oasis_Admission_Category'],
			"mobile" => $_Row['Oasis_Admission_Mobile'],
			"qualification" => $_Row['Qualification_Name'],
			"caste" => $_Row['Oasis_Admission_Caste'],
			"address" => $_Row['Oasis_Admission_Address'],
			"photo" => $_Row['Oasis_Admission_Photo'],
			"sign" => $_Row['Oasis_Admission_Sign'],
			"mincert" => $_Row['Oasis_Admission_MinCertificate'],
			"highcert" => $_Row['Oasis_Admission_HighCertificate'],
			"ageproof" => $_Row['Oasis_Admission_AgeProof'],
			"casteproof" => $_Row['Oasis_Admission_CasteProof']);        
        $_i = $_i + 1;
		
    }
	
    echo json_encode($_DataTable);
}

if ($_action == "UpdateNotReportingLearner") {	
	$_LCode = $_POST["lcode"];
	$_ProcessStatus = $_POST["status"];	                       
	
	$response = $emp->UpdateWcdNotReported($_LCode, $_ProcessStatus);
	echo $response[0];	
	
}

if ($_action == "UpdateApproveReportedLearner") {

date_default_timezone_set("Asia/Kolkata");
				$dtstamp = date("Ymd_His");
if (isset($_POST["course"]) && !empty($_POST["course"])){
	if($_POST["course"]=='24'){
		if($_POST["txtemail"]==""){
			echo "e";
		}
		else{
				if (isset($_POST["lcode"]) && !empty($_POST["lcode"])){
				if (isset($_POST["Status"]) && $_POST["Status"] == "on"){
					if (isset($_POST["ddltime"]) && !empty($_POST["ddltime"])){
						if (!empty($_FILES['uploadImage6']['name'])){
							$_UploadDirectory1 = '../upload/wcd_reported_documents';
							
							if ($_FILES["uploadImage6"]["name"] != '') {
									$imag = $_FILES["uploadImage6"]["name"];
									$imageinfo = pathinfo($_FILES["uploadImage6"]["name"]);
									if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
										$error_msg = "Image must be in either PNG or JPG or JPEG Format";
										$flag = 0;
									} else {
										if (file_exists("$_UploadDirectory1/" . $_POST["lcode"] . '_reportdoc' . '_' . $dtstamp . '.' . substr($imag, -3))) {
											$_FILES["uploadImage6"]["name"] . "already exists";
										} else {
											if (file_exists($_UploadDirectory1)) {
												if (is_writable($_UploadDirectory1)) {
													move_uploaded_file($_FILES["uploadImage6"]["tmp_name"], "$_UploadDirectory1/" . $_POST["lcode"] . '_reportdoc' . '_' . $dtstamp . '.jpg');
													//session_start();
													//$_SESSION['ReportedDoc'] = $_POST["lcode"] . '_reportdoc' . '_' . $dtstamp . '.jpg';
												} else {
													outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
												}
											} else {
												outputJSON("<span class='error'>Upload Folder Not Available</span>");
											}
										}
									}
									
									$docname= $_POST["lcode"] . '_reportdoc' . '_' . $dtstamp . '.jpg';
									if($_POST["course"]=='24'){
										$email=$_POST["txtemail"];
										$response = $emp->UpdateWcdRSCFAReported($_POST["lcode"],$docname,$_POST["Status"], 
												$dtstamp,$_POST["batch"],$_POST["itgk"], $_POST["ddltime"],$email);
										echo $response[0];
									}
									else{
										$response = $emp->UpdateWcdRSCITReported($_POST["lcode"],$docname,$_POST["Status"], 
												$dtstamp,$_POST["batch"],$_POST["itgk"], $_POST["ddltime"]);
										echo $response[0];
									}
									
							}
						}
						else{
							echo "d";
						}
					}
					else{
						echo "t";
					}
				}
				else{
					echo "s";
				}
				
			}
			else{
				echo "w";
			}
		}
	}
		else{
				if (isset($_POST["lcode"]) && !empty($_POST["lcode"])){
				if (isset($_POST["Status"]) && $_POST["Status"] == "on"){
					if (isset($_POST["ddltime"]) && !empty($_POST["ddltime"])){
						if (!empty($_FILES['uploadImage6']['name'])){
							$_UploadDirectory1 = '../upload/wcd_reported_documents';
							
							if ($_FILES["uploadImage6"]["name"] != '') {
									$imag = $_FILES["uploadImage6"]["name"];
									$imageinfo = pathinfo($_FILES["uploadImage6"]["name"]);
									if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg" && strtolower($imageinfo['extension']) != "jpeg") {
										$error_msg = "Image must be in either PNG or JPG or JPEG Format";
										$flag = 0;
									} else {
										if (file_exists("$_UploadDirectory1/" . $_POST["lcode"] . '_reportdoc' . '_' . $dtstamp . '.' . substr($imag, -3))) {
											$_FILES["uploadImage6"]["name"] . "already exists";
										} else {
											if (file_exists($_UploadDirectory1)) {
												if (is_writable($_UploadDirectory1)) {
													move_uploaded_file($_FILES["uploadImage6"]["tmp_name"], "$_UploadDirectory1/" . $_POST["lcode"] . '_reportdoc' . '_' . $dtstamp . '.jpg');
													//session_start();
													//$_SESSION['ReportedDoc'] = $_POST["lcode"] . '_reportdoc' . '_' . $dtstamp . '.jpg';
												} else {
													outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
												}
											} else {
												outputJSON("<span class='error'>Upload Folder Not Available</span>");
											}
										}
									}
									
									$docname= $_POST["lcode"] . '_reportdoc' . '_' . $dtstamp . '.jpg';
									if($_POST["course"]=='24'){
										$email=$_POST["txtemail"];
										$response = $emp->UpdateWcdRSCFAReported($_POST["lcode"],$docname,$_POST["Status"], 
												$dtstamp,$_POST["batch"],$_POST["itgk"], $_POST["ddltime"],$email);
										echo $response[0];
									}
									else{
										$response = $emp->UpdateWcdRSCITReported($_POST["lcode"],$docname,$_POST["Status"], 
												$dtstamp,$_POST["batch"],$_POST["itgk"], $_POST["ddltime"]);
										echo $response[0];
									}
									
							}
						}
						else{
							echo "d";
						}
					}
					else{
						echo "t";
					}
				}
				else{
					echo "s";
				}
				
			}
			else{
				echo "w";
			}
		}
		
}


}


?>