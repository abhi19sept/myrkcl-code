<?php
session_start();
//$_SESSION['ImageFile'] = "";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Output JSON

function outputJSON($msg, $status = 'error') {
    header('Content-Type: application/json');
    die(json_encode(array(
        'data' => $msg,
        'status' => $status
    )));
}

$_UploadDirectory5 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_age_proof/';
$_UploadDirectory6 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_identity_proof/';
$_UploadDirectory7 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_sub_category_proof/';
$_UploadDirectory8 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_caste_proof/';
$_UploadDirectory9 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_physical_disability_proof/';
$_UploadDirectory10 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_marital_status_proof/';
$_UploadDirectory11 = $_SERVER['DOCUMENT_ROOT'] . '/upload/oasis_victim_of_violence_proof/';

//print_r($_FILES);
$st=0;
$parentid = $_POST['UploadId'];
                                //oasis_age_proof
                            if(isset($_FILES["SelectedFile5"])){
				if($_FILES["SelectedFile5"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile5"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile5"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg" )
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
//						  if (file_exists("$_UploadDirectory5/" . $parentid . '_ageproof' . '.' .substr($imag,-3)))
//						   {
//							 $_FILES["SelectedFile5"]["name"]. "already exists";
//						   }
//						  else
//						   {
								if (file_exists($_UploadDirectory5)) {
									if (is_writable($_UploadDirectory5)) {
										$filepath = "$_UploadDirectory5/". $parentid . '_ageproof' . '.jpg';
										move_uploaded_file($_FILES["SelectedFile5"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
									outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Ageproof Upload Folder Not Available.</span>");
										}
						   //}  				
						}	
			     }
                                
                            }
                            
                            //oasis_identity_proof
                            if(isset($_FILES["SelectedFile6"])){
				if($_FILES["SelectedFile6"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile6"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile6"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
//						  if (file_exists("$_UploadDirectory6/" . $parentid . '_identityproof' . '.' .substr($imag,-3)))
//						   {
//							 $_FILES["SelectedFile6"]["name"]. "already exists";
//						   }
//						  else
//						   {
								if (file_exists($_UploadDirectory6)) {
									if (is_writable($_UploadDirectory6)) {
										$filepath = "$_UploadDirectory6/". $parentid . '_identityproof' . '.jpg';
										move_uploaded_file($_FILES["SelectedFile6"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
									outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Identityproof Upload Folder Not Available.</span>");
										}
						   //}  				
						}	
			     }
                            }

                //oasis_adhaar card proof
                            if(isset($_FILES["SelectedFile12"])){
				if($_FILES["SelectedFile12"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile12"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile12"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
//						  if (file_exists("$_UploadDirectory6/" . $parentid . '_identityproof' . '.' .substr($imag,-3)))
//						   {
//							 $_FILES["SelectedFile12"]["name"]. "already exists";
//						   }
//						  else
//						   {
								if (file_exists($_UploadDirectory6)) {
									if (is_writable($_UploadDirectory6)) {
										$filepath = "$_UploadDirectory6/". $parentid . '_aadhaar_card_proof' . '.jpg';
										move_uploaded_file($_FILES["SelectedFile12"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
									outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Identityproof Upload Folder Not Available.</span>");
										}
						   //}  				
						}	
			     }
                            } 
                            
                            if(isset($_FILES["SelectedFile7"])){
                                //oasis_sub_category_proof
				if($_FILES["SelectedFile7"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile7"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile7"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
//						  if (file_exists("$_UploadDirectory7/" . $parentid . '_subcategoryproof' . '.' .substr($imag,-3)))
//						   {
//							 $_FILES["SelectedFile7"]["name"]. "already exists";
//						   }
//						  else
//						   {
								if (file_exists($_UploadDirectory7)) {
									if (is_writable($_UploadDirectory7)) {
										$filepath = "$_UploadDirectory7/". $parentid . '_subcategoryproof' . '.jpg';
										move_uploaded_file($_FILES["SelectedFile7"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
									outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Subcategoryproof Upload Folder Not Available.</span>");
										}
						   //}  				
						}	
			     }
                            } 
				 //oasis_caste_proof
                             if(isset($_FILES["SelectedFile8"])){
				 if($_FILES["SelectedFile8"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile8"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile8"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
//						  if (file_exists("$_UploadDirectory8/" .$parentid .'_casteproof'.'.'.substr($imag,-3)))
//						   {
//							 $_FILES["SelectedFile8"]["name"]. "already exists";
//						   }
//						  else
//						   {
								if (file_exists($_UploadDirectory8)) {
									if (is_writable($_UploadDirectory8)) {
										$filepath = "$_UploadDirectory8/".$parentid .'_casteproof'.'.jpg';
										move_uploaded_file($_FILES["SelectedFile8"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
											outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Casteproof Upload Folder Not Available.</span>");
										}
						  // }  				
						}	
			     }	
                             }
				 //oasis_physical_disability_proof
                             if(isset($_FILES["SelectedFile9"])){
				 if($_FILES["SelectedFile9"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile9"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile9"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
//						  if (file_exists("$_UploadDirectory9/" .$parentid .'_physicaldisabilityproof'.'.'.substr($imag,-3)))
//						   {
//							 $_FILES["SelectedFile9"]["name"]. "already exists";
//						   }
//						  else
//						   {
								if (file_exists($_UploadDirectory9)) {
									if (is_writable($_UploadDirectory9)) {
										$filepath = "$_UploadDirectory9/".$parentid .'_physicaldisabilityproof'.'.jpg';
										move_uploaded_file($_FILES["SelectedFile9"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
											outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Physicaldisabilityproof Upload Folder Not Available.</span>");
										}
						  // }  				
						}	
			     }	
                             }
				 //oasis_marital_status_proof
                             if(isset($_FILES["SelectedFile10"])){
				 if($_FILES["SelectedFile10"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile10"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile10"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
//						  if (file_exists("$_UploadDirectory10/" .$parentid .'_maritalstatusproof'.'.'.substr($imag,-3)))
//						   {
//							 $_FILES["SelectedFile10"]["name"]. "already exists";
//						   }
//						  else
//						   {
								if (file_exists($_UploadDirectory10)) {
									if (is_writable($_UploadDirectory10)) {
										$filepath = "$_UploadDirectory10/".$parentid .'_maritalstatusproof'.'.jpg';
										move_uploaded_file($_FILES["SelectedFile10"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
											outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Maritalstatusproof Upload Folder Not Available.</span>");
										}
						   //}  				
						}	
			     }	
                             }
				 //oasis_victim_of_violence_proof
                             if(isset($_FILES["SelectedFile11"])){
				 if($_FILES["SelectedFile11"]["name"]!='')
				 {
					 $imag =  $_FILES["SelectedFile11"]["name"];
					 $imageinfo = pathinfo($_FILES["SelectedFile11"]["name"]);					 
				     if(strtolower($imageinfo['extension'])!= "png" && strtolower($imageinfo['extension'])!="jpg" && strtolower($imageinfo['extension'])!="jpeg")
		             	{
						  $error_msg = "Image must be in either PNG or JPG Format";
						  $flag=0;
						 }			
			         else
			         	{
//						  if (file_exists("$_UploadDirectory11/" .$parentid .'_victimofviolenceproof'.'.'.substr($imag,-3)))
//						   {
//							 $_FILES["SelectedFile11"]["name"]. "already exists";
//						   }
//						  else
//						   {
								if (file_exists($_UploadDirectory11)) {
									if (is_writable($_UploadDirectory11)) {
										$filepath = "$_UploadDirectory11/".$parentid .'_victimofviolenceproof'.'.jpg';
										move_uploaded_file($_FILES["SelectedFile11"]["tmp_name"], $filepath);
										if (!file_exists($filepath)) {
											outputJSON("<span class='error'>Unable to upload, Please re-try!</span>");
										} else {
                                            $st = 1;
                                        }
									} 
									else {
											outputJSON("<span class='error'>Please Check permission on upload folder. Upload Folder not Writable</span>");
										 }
								} else {
								outputJSON("<span class='error'>Victimofviolenceproof Upload Folder Not Available.</span>");
										}
						  // }  				
						}	
			     }		
                             }
                             
                             
                             if($st==1){
                                  outputJSON("Upload Sucessfully.", 'success');
                             }