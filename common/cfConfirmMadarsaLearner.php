<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include './commonFunction.php';
require 'BAL/clsConfirmMadarsaLearner.php';
$response = array();
$emp = new clsConfirmMadarsaLearner();

if ($_action == "SHOWALL") {
    $response = $emp->GetAll($_POST['batch'], $_POST['course'], $_POST['district']);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th >S No.</th>";
    echo "<th >Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >Gender</th>";
    echo "<th >Photo</th>";
    echo "<th >Madarsa Details</th>";
    echo "<th >IT-GK Details</th>";
    echo "<th >Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;


    //echo $rowcount=mysqli_num_rows($response[2]);
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {

            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
            echo "<td>" . $_Row['Admission_Gender'] . "</td>";

            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" src="images/user icon big.png"/>' . "</td>";
            }
            //echo "<td>" . $_Row['Madarsa_Regd_code'] . " - " . $_Row['Madarsa_Name'] . " - " . $_Row['Madarsa_Tehsil'] . " - " . $_Row['Madarsa_District'] . "</td>";
            echo "<td>" . $_Row['Madarsa_Regd_code'] . " - " . $_Row['Madarsa_Name'] . " - " . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . " - " . $_Row['Organization_Name'] . "</td>";




            if ($_Row['Admission_Payment_Status'] == '0') {
                echo "<td><input type='checkbox' id=chk" . $_Row['Admission_Code'] .
                " name=chk" . $_Row['Admission_Code'] . "></input></td>";
            } elseif ($_Row['Admission_Payment_Status'] == '8') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='DD in process'>"
                . "</td>";
            } elseif ($_Row['Admission_Payment_Status'] == '1') {

                echo "<td>"
                . "<input type='button' name='conf' id='conf' class='btn btn-primary' value='Payment Confirmed'>"
                . "</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    } else {
        //echo "invaliddata";
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}

if ($_action == "ADD") {

    $_UserPermissionArray = array();
    $_AdmissionCode = array();
    $_i = 0;
    $_Count = 0;
    $l = "";
    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 3) == 'chk') {
            $_UserPermissionArray[$_Count] = array(
                "Function" => substr($key, 3),
                "Attribute" => 3,
                "Status" => 1);

            $_Count++;

            $l .= substr($key, 3) . ",";
        }
        $_AdmissionCode = rtrim($l, ",");

        $count = count($_UserPermissionArray);
    }
    if ($count == '0') {
        echo "0";
    } else {

        $response = $emp->ConfirmMadAdmission($_POST['ddlBatch'], $_POST['ddlCourse'], $_AdmissionCode);
        echo $response[0];
    }
}

if ($_action == "SHOWALLRPT") {

    $response = $emp->GetAllRpt($_POST['batch'], $_POST['course'], $_POST['district']);
    $_DataTable = "";
    echo "<div class='table-responsive'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th >S No.</th>";
    echo "<th >Learner Name</th>";
    echo "<th >Father Name</th>";
    echo "<th >Gender</th>";
    echo "<th >Photo</th>";
    echo "<th >Madarsa Details</th>";
    echo "<th >IT-GK Details</th>";
    echo "<th >Confirm Status</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;


    //echo $rowcount=mysqli_num_rows($response[2]);
    if ($response[0] == 'Success') {
        while ($_Row = mysqli_fetch_array($response[2])) {

            echo "<tr>";
            echo "<td>" . $_Count . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Name']) . "</td>";
            echo "<td>" . strtoupper($_Row['Admission_Fname']) . "</td>";
            echo "<td>" . $_Row['Admission_Gender'] . "</td>";

            if ($_Row['Admission_Photo'] != "") {
                $image = $_Row['Admission_Photo'];
                echo "<td>" . '<img alt="No Image Found" width="40" src="upload/admission_photo/' . $image . '"/>' . "</td>";
            } else {
                echo "<td>" . '<img alt="No Image Found" width="40" src="images/user icon big.png"/>' . "</td>";
            }
            //echo "<td>" . $_Row['Madarsa_Regd_code'] . " - " . $_Row['Madarsa_Name'] . " - " . $_Row['Madarsa_Tehsil'] . " - " . $_Row['Madarsa_District'] . "</td>";
            echo "<td>" . $_Row['Madarsa_Regd_code'] . " - " . $_Row['Madarsa_Name'] . " - " . $_Row['District_Name'] . "</td>";
            echo "<td>" . $_Row['Admission_ITGK_Code'] . " - " . $_Row['Organization_Name'] . "</td>";




            if ($_Row['Admission_Payment_Status'] == '0') {
                echo "<td><b>Confirmation Pending</b></td>";
            } elseif ($_Row['Admission_Payment_Status'] == '1') {

                echo "<td>Admission Confirmed</td>";
            }
            echo "</tr>";
            $_Count++;
        }
    } else {
        //echo "invaliddata";
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}
