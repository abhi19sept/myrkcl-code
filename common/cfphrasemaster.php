<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include './commonFunction.php';
require 'BAL/clsphrasemaster.php';

$response = array();
$emp = new clsphrasemaster();


if ($_action == "ADD") {
    if (isset($_POST["name"]) && !empty($_POST["name"])) {
        $_StateName = $_POST["name"];
        $response = $emp->AddPhrase_Ke($_StateName);
        echo $response[0];
    }
}


if ($_action == "DELETE") {


    $response = $emp->DeleteRecord($_actionvalue);
    //echo "<pre>"; print_r($response);die;
    echo $response[0];
}


if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll($_actionvalue);
//echo "<pre>"; print_r($response[2]);
    $_DataTable = "";

    echo "<table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='25%'>Phrase Name</th>";
    echo "<th style='20%'>Status</th>";
    //echo "<th style='10%'>Action</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['Phrase_Ke'] . "</td>";
        echo "<td>";
        echo $_Row['Phrase_Status']==1 ? 'Active' : 'InActive'; 
        echo "</td>";
//        echo "<td><a href='frmphrasemaster.php?code=" . $_Row['Phrase_ID'] . "&Mode=Delete'>"
//                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a></td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
}
if ($_action == "FILL") {
    $response = $emp->GetAll($_actionvalue);
    echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['State_Code'] . ">" . $_Row['State_Name'] . "</option>";
    }
}