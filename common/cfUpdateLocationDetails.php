<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsUpdateLocationDetails.php';

$response = array();
$emp = new clsUpdateLocationDetails();



if ($_action == "DETAILS") {
    $response = $emp->GetDatabyCode($_POST['values']);
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            $_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                "orgcode" => $_Row['Organization_Code'],
                "districtcode" => $_Row['Organization_District'],
                "tehsilcode" => $_Row['Organization_Tehsil'],
                "districtname" => $_Row['District_Name'],
                "tehsilname" => $_Row['Tehsil_Name'],
                "areatype" => $_Row['Organization_AreaType']);
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "You are not Auhorized to use this functionality on MYRKCL.";
    }
}

if ($_action == "UPDATE") {
    //print_r($_POST);
    if (isset($_POST["tehsil"]) && !empty($_POST["tehsil"])) {
        if (isset($_POST["areatype"]) && !empty($_POST["areatype"])) {
            if (isset($_POST["orgcode"]) && !empty($_POST["orgcode"])) {
                                                          
            $_Tehsil = $_POST["tehsil"];
            $_AreaType = $_POST["areatype"];
            $_OrgCode = $_POST["orgcode"];
            
            $response = $emp->Update($_Tehsil, $_AreaType, $_OrgCode);
            echo $response[0];
            } else {
                echo "Sommething Went Wrong. Reload the page  and try again.";
                   }
        } else {
            echo "Please select Area Type";
               }
    } else {
        echo "Please select Tehsil";
    }
}
