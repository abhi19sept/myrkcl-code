<?php

/*
 * @author yogi

 */
//include './commonFunction.php';
session_start();

require 'BAL/clsexaminationeventmaster.php';

$response = array();
$emp1 = new clsexaminationeventmaster();

 $code = $_REQUEST['code'];
 $centercode = $_REQUEST['centercode'];
    function getdata($code,$centercode, $learnerCode = '') {
        global $emp1;
        //global $code;
		//print_r($centercode);
		$response = $emp1->GetAll($code,$centercode, $learnerCode);
		return $response[2];
		
    }

    function getCenters($args) {
        global $emp1;
        //global $code;
		//print_r($centercode);
		$response = $emp1->GetCenters($args);
		return $response[2];
		
    }
	
	function makeDir($path) {
        return is_dir($path) || mkdir($path);
    }

    function uploadPhotoSigns($examId) {
    	global $emp1;
		$pics = getPhotoSigns();
	    $path = $pics[0];
	    $names = $pics[1];
	    $learners = $pics[2];
	    $itgks = [];
		$return = '';

	    $learnerCodes = array_keys($learners);
	    //$learnerCodes = ['575170906011401789', '822170915121658789', '349170915125306789'];
	    if ($learnerCodes) {
	        $response = $emp1->GetAllMappedLearner($learnerCodes, $examId);
	        $itgks = processFetchedLearnerPics($itgks, $response, $path);
	    }

	    if ($itgks) {
	    	$requestArgs = [
	    		'code' => $examId,
	    		'centercode' => implode(',', $itgks),
	    	];
	    	$itgks = $emp1->GetCenters($requestArgs);
			$return = $itgks[2];
	    }

	    return $return;
	}