<?php

/*
 *  author Viveks

 */
include 'commonFunction.php';
require 'BAL/clsUpdateIntake.php';

$response = array();
$emp = new clsUpdateIntake();

if ($_action == "DELETE") {
    //print_r($_POST);
    $value = $_POST['values'];
    $type = $_POST['type'];
    $status = $_POST['status'];
    $response = $emp->DeleteRecord($value, $type, $status);

    echo $response[0];
}

if ($_action == "SHOW") {

    //echo "Show";
    $response = $emp->GetAll();

    $_DataTable = "";

    echo "<div class='table-responsive' style='margin-top:10px'>";
    echo "<table id='example' border='0' cellpedding='0' cellspacing='0' class='table table-striped table-bordered'>";
    echo "<thead>";
    echo "<tr>";
    echo "<th style='5%'>S No.</th>";
    echo "<th style='45%'>System Type</th>";
    echo "<th style='20%'>Processor</th>";
    echo "<th style='20%'>RAM</th>";
    echo "<th style='10%'>HDD</th>";
    echo "<th style='10%'>Status</th>";
    echo "<th style='10%'>Approve System</th>";
//    echo "<th style='10%'>Delete System</th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    $_Count = 1;
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<tr class='odd gradeX'>";
        echo "<td>" . $_Count . "</td>";
        echo "<td>" . $_Row['System_Type'] . "</td>";
        echo "<td>" . $_Row['Processor_Name'] . "</td>";
        echo "<td>" . $_Row['System_RAM'] . "</td>";
        echo "<td>" . $_Row['System_HDD'] . "</td>";
        echo "<td>" . $_Row['System_Status'] . "</td>";
        if ($_Row['System_Status'] == 'Confirmed By Center') {
            echo "<td>System Confirmed</td>";
        } else {
            echo "<td><input type='checkbox' id=chk" . $_Row['System_Code'] . $_Row['System_Type'] .
            " name=chk" . $_Row['System_Code'] . $_Row['System_Type'] . "></input>"
//                 . "<a href='frmupdateintake.php?code=" . $_Row['System_Code'] . "&Mode=Delete&type=" . $_Row['System_Type'] . "&status=".$_Row['System_Status']."'>"
//                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a>"
            . "</td>";
        }
//        echo "<td><a href='frmupdateintake.php?code=" . $_Row['System_Code'] . "&Mode=Delete&type=" . $_Row['System_Type'] . "&status=".$_Row['System_Status']."'>"
//                . "<img src='images/deleteicon.png' alt='Edit' width='30px' /></a>
//                </td>";
        echo "</tr>";
        $_Count++;
    }
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
}


if ($_action == "ADD") {
    //print_r($_POST);
    $Course = $_POST['ddlCourse'];
    $Batch = $_POST['ddlBatch'];
    $_UserPermissionArray = array();
    $_Count = 0;
    $_Counts = 0;
    foreach ($_POST as $key => $value) {
        if (substr($key, 0, 3) == 'chk') {

            if (substr($key, -6) == 'Client') {
                $_Count++;
            }
        }
        $_UserPermissionArray[$_Counts] = array(
            "Function" => (substr($key, 3, -6)));
        $_Counts++;
    }

    $response = $emp->Update($_Count, $Course, $Batch);
    $response1 = $emp->Update_System($_UserPermissionArray);

    echo $response[0];
}

        