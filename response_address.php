<?php
    $title = "Payment Result";
    include('header.php');
    include('root_menu.php');

    ini_set("memory_limit", "5000M");
    ini_set("max_execution_time", 0);
    set_time_limit(0);

    $status = $_POST["status"];
    $firstname = $_POST["firstname"];
    $amount = $_POST["amount"]; //Please use the amount value from database
    $txnid = $_POST["txnid"];
    $posted_hash = $_POST["hash"];
    $key = $_POST["key"];
    $productinfo = $_POST["productinfo"];
    $email = $_POST["email"];
    $salt = "eCwWELxi"; //Please change the value with the live salt for production environment
    $udf1 = $_POST["udf1"];


    //Validating the reverse hash
    If (isset($_POST["additionalCharges"])) {
        $additionalCharges = $_POST["additionalCharges"];
        $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '||||||||||' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

    } else {

        $retHashSeq = $salt . '|' . $status . '||||||||||' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;

    }
    $hash = hash("sha512", $retHashSeq);

?>

<style type="text/css">

    .asterisk {
        color: red;
        font-weight: bolder;
        font-size: 18px;
        vertical-align: middle;
    }

    .division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 20px;
        color: #000;
        margin-bottom: 20px;

    }

    .extra-footer-class {
        margin-top: 0;
        margin-bottom: -10px;
        padding: 16px;
        background-color: #fafafa;
        border-top: 1px solid #e5e5e5;
    }

    .ref_id {
        text-align: center;
        font-size: 20px;
        color: #000;
        margin: 0 0 10px 0;
    }

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eeeeee;
        box-shadow: inset 0 0 5px 1px #d5d5d5;
    }

    .form-control {
        border-radius: 2px;
    }

    input[type=text]:hover, textarea:hover {
        box-shadow: 0 1px 3px #aaa;
        -webkit-box-shadow: 0 1px 3px #aaa;
        -moz-box-shadow: 0 1px 3px #aaa;
    }

    .col-sm-3:hover {
        background: none !important;
    }

    .modal-open .container-fluid, .modal-open .container {
        -webkit-filter: blur(5px) grayscale(50%);
        filter: blur(5px) grayscale(50%);
    }

    .btn-success {
        background-color: #00A65A !important;
    }

    .btn-success:hover {
        color: #fff !important;
        background-color: #04884D !important;
        border-color: #398439 !important;
    }

    .addBottom {
        margin-bottom: 12px;
    }

</style>

<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>

<div class="container" id="showdata">
    <div class="panel panel-primary" style="margin-top:46px !important;">
        <div class="panel-heading">Transaction Details</span></div>


        <?php

            include 'frmPayuTransactionResponse.php';
            $response = $general->getTransactionItgkDetails($txnid);

            if (($hash == $posted_hash) && ($status == "success") && ($response['Pay_Tran_Status'] == "PaymentReceive")) {
                ?>
                <div class="panel-body">
                    <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateOrgDetails" name="updateOrgDetails">
                        <div class="ref_id">
                            Reference ID: <span id="ref_id"><?php echo $_POST['udf1']; ?></span>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>&nbsp;</p>
                                <div class="division_heading">
                                    Thank You ! Your Transaction was Successful
                                </div>
                                <div class="box-body" style="margin: 0 100px;">
                                    <div class="form-group">
                                        <div class="col-sm-12 addBottom">
                                            Payment Title : <span><?php echo $_POST['productinfo']; ?></span>
                                        </div>

                                        <div class="col-sm-12 addBottom">
                                            Payment Status : <span class="text-success">Success</span>
                                        </div>

                                        <div class="col-sm-12 addBottom">
                                            Transaction Reference Number : <span><?php echo $_POST['txnid']; ?></span>
                                        </div>

                                        <div class="col-sm-12 addBottom">
                                            Bank Reference Number: <span><?php echo $_POST['bank_ref_num']; ?></span>
                                        </div>

                                        <div class="col-sm-12 addBottom">
                                            Transaction Date and Time: <span><?php echo date("F d, Y h:i a", strtotime($_POST['addedon'])); ?></span>
                                        </div>

                                        <div class="col-sm-12 addBottom">
                                            Transaction Amount: <span><?php echo $_POST['amount'] ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="division_heading"></div>
                                <div class="box-body" style="margin: 0 100px;">
                                    <div class="form-group">
                                        <div class="col-sm-12 addBottom">
                                            <button type="button" class="btn btn-danger" onclick='downloadGSTInvoice()'><i
                                                        class="fa fa-download"
                                                        aria-hidden="true"></i>&nbsp;&nbsp;
                                                Download GST Invoice
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
            } else {
                ?>
                <div class="panel-body">
                    <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateOrgDetails1" name="updateOrgDetails1">
                        <div class="ref_id">
                            Reference ID: <span id="ref_id"><?php echo $_POST['udf1']; ?></span>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>&nbsp;</p>
                                <div class="division_heading">
                                    Unfortunately, the transaction could not be completed
                                </div>
                                <div class="box-body" style="margin: 0 100px;">
                                    <div class="form-group">
                                        <div class="col-sm-12 addBottom">
                                            Payment for : <span><?php echo $_POST['productinfo']; ?></span>
                                        </div>
                                        <div class="col-sm-12 addBottom">
                                            Payment Status : <span class="text-danger"><?php echo $_POST['status']; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="division_heading"></div>
                                <div class="box-body" style="margin: 0 100px;">
                                    <div class="form-group">
                                        <div class="col-sm-12 addBottom">
                                            <button type="button" class="btn btn-danger" onclick="window.location.href='frmprofileDetails.php'"><i class="fa fa-retweet"
                                                                                                                                                   aria-hidden="true"></i>&nbsp;
                                                Retry
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <?php
            }
        ?>
    </div>
</div>

</body>
<?php
    include 'footer.php';
?>
<script type="text/javascript" src="bootcss/js/secure.js"></script>
<script type="text/javascript">

    function downloadGSTInvoice() {
        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=downloadGSTInvoice" + '&fld_ref_no=' + '<?php echo base64_encode($_REQUEST['udf1']);?>',
            success: function (data) {
                location.href = 'downloadFile.php?id=' + data;
            }
        });
    }

    $(document).ready(function () {
        function disableBack() {
            window.history.forward()
        }

        window.onload = disableBack();
        window.onpageshow = function (evt) {
            if (evt.persisted) disableBack()
        };

        function printTransactionDetails(code) {
            window.open('printTransactionDetails.php?Code=' + code, '_blank');
        }
    });
</script>
