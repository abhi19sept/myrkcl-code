<?php
$title = "Lottery Result Report";
include ('header.php');
include ('root_menu.php');

if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '8' ||
$_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '9' ||
$_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258' || $_SESSION['User_Code'] == '9063') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}

echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Lottery Result Report</div>
            <div class="panel-body">

                <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">

                                </select>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                    <!--                                <option value="0">All Batch</option>-->
                                </select>
                            </div> 

                        </div>

                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<div id="myModalviewdocument" class="modal">
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h6 id="modelheading">Document Details</h6>
    </div>
    <div class="modal-body" style="min-height: 350px;">
        <button type="button" id="btnremarkbox" name="" class="btnyogi btn btn-success btn-sm" style="margin-bottom: 10px;">Add Remark</button>
          <div id="respre"></div>
          <form class="form-horizontal"  name="frmAddRemark" id="frmAddRemark" enctype="multipart/form-data" style="display: none;">
                <div class="form-group">
                    <input type='hidden' name="action" id="action" value="ADDREMARK">
                    <input type='hidden' name="lcode" id="lcode" value="">
                    <label for="inputEmail3" class="col-sm-3 control-label">Add Remark</label>
                    <div class="col-sm-6">
                        <textarea id="remarktext" name="remarktext" class="form-control" required="required" >

                        </textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6" id="responseremark"></div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="button" name="btn-remark-submit"  id="btn-remark-submit" class="btnyogi btn btn-success btn-sm">Submit</button>
                        <button type="reset" id="reset" class="btn btn-default btn-sm">Reset</button>			
                    </div>
                </div>
          </form>
          <iframe id="docpath" name="docpath" src="" style="width: 100%;height: 500px;border: none;"></iframe>
    </div>
  </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillCourse() {           
            $.ajax({
                type: "post",
                url: "common/cfLotteryResultReport.php",
                data: "action=FillAdmissionCourse",
                success: function (data) {                   
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfLotteryResultReport.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });


        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfLotteryResultReport.php"; // the script where you handle the form input.
            var role_type = '';
            var data;            
            var batchvalue = $("#ddlBatch").val();

            data = "action=GETDATA&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    $('#response').empty();
                    $("#grid").html(data);
                   $('#example').DataTable({
										dom: 'Bfrtip',
										buttons: [
											'copy', 'csv', 'excel', 'pdf', 'print'
										]
									});


                }
            });
        }

        $("#btnSubmit").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {
                showData();
			}
            return false; // avoid to execute the actual submit of the form.
        });
	
  
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        
        // Start-> Added By Anoop
        
        $("#grid").on("click",".Fun_ViewDocument",function(){
            var lcode = $(this).attr("id");
            var url = "upload/admission_fee_waiver_scheme/"+lcode+"_admitcard.pdf?refresh";
             $("#lcode").val(lcode);
             $("#docpath").attr("src",url);
             $("#btnremarkbox").attr("name",lcode);
            var modal = document.getElementById('myModalviewdocument');
            var span = document.getElementsByClassName("close")[0];
            modal.style.display = "block";
            span.onclick = function() { 
                modal.style.display = "none";
                $("#docpath").empty();
                $('#respre').empty();
            }
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                    $("#docpath").empty();
                    $('#respre').empty();
                }
            }
         });
         $("#btnremarkbox").click(function () {
             var lcode = $(this).attr("name");
             $.ajax({ 
                  url: "common/cfLotteryResultReport.php",
                  type: "POST",
                  data: "action=CheckRemark&lcode="+lcode,
                  success: function(data)
                    {  
                        if(data == "1"){
                            $('#respre').empty();
                            $("#btnremarkbox").css("display", "none");
                            $("#frmAddRemark").css("display", "block");
                        }else{
                            $('#respre').append("<div class='alert-success'><span><img src=images/warning.jpg width=10px /></span><span>Already added remark.</span></div>");  
                        }
                        
                    } 	        
              });
             
         });
         $("#reset").click(function () {
             $("#btnremarkbox").css("display", "block");
             $("#frmAddRemark").css("display", "none");
         });
        $("#btn-remark-submit").click(function () {
            
            if ($("#frmAddRemark").valid())
              {
              $('#responseremark').empty();
              $('#responseremark').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
              $.ajax({ 
                  url: "common/cfLotteryResultReport.php",
                  type: "POST",
                  data: $("#frmAddRemark").serialize(),
                  success: function(data)
                    { 
                        $('#responseremark').empty();
                        if (data == SuccessfullyUpdate){
                          $('#responseremark').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>Remark Added Successfully..</span></div>");
						  /*window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmLotteryResultReport.php';
                            }, 5000);*/
                        }else{
                          $('#responseremark').append("<div class='alert-success'><span><img src=images/warning.jpg width=10px /></span><span>Error To add Remark Please Try Again</span></div>");  
                        }
                    } 	        
              });
              }
              return false;
        }); 
         
         // End-> End By Anoop
         
    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
</html>
