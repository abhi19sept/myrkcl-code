<?php
$title = "WCD Learner Details List";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Batch='" . $_REQUEST['month'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
      echo "<script>var spcategory='" . $_REQUEST['category'] . "'</script>";
   
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Batch=0</script>";
    echo "<script>var Mode='Add'</script>";
    echo "<script>var spcategory='0'</script>";

}
if (isset($_REQUEST['Redir'])) {
     echo "<script>var Redir='" . $_REQUEST['Redir'] . "'</script>";
}
else{   
    echo "<script>var Redir='0'</script>";}

?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">			 
        <div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">WCD Learner Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data"> 
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Application Id:</label>
                            <input type="text" readonly="true" class="form-control" maxlength="50" name="txtLCode" id="txtLCode" placeholder="Application Id">
                            <input type="hidden" name="txtcategorycode" id="txtcategorycode"	class="form-control">
                            <input type="hidden" name="txtaadharcard" id="txtaadharcard"	class="form-control">
                            <input type="hidden" name="txtapplicantid" id="txtapplicantid"	class="form-control" value="<?php echo $_REQUEST['code']; ?>">
                            <input type="hidden" name="txtitgk" id="txtitgk" class="form-control">
                            <input type="hidden" name="txtdistrict" id="txtdistrict" class="form-control">

                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="learnercode">Learner Name:</label>
                            <input type="text" readonly="true" class="form-control text-uppercase" name="txtlname" id="txtlname" placeholder="Learner Name">
                        </div>

                        <div class="col-sm-4 form-group">     
                            <label for="fname">Father's Name:</label>
                            <input type="text" readonly="true" class="form-control text-uppercase" name="txtfname" id="txtfname" placeholder="Father's Name"> 
                        </div>

                        <div class="col-sm-4 form-group">     
                            <label for="dob">Date of Birth:</label>								
                            <input type="text" class="form-control" readonly="true" name="dob" id="dob"  placeholder="YYYY-MM-DD">
                        </div>
                    </div> 

                    <div class="container">
                        <div class="col-sm-4 form-group"> 
                            <label for="email">Marital Status:</label>
                            <input type="text" class="form-control" readonly="true" name="mstatus" id="mstatus"  placeholder="Marital Status">
                        </div> 

                        <div class="col-sm-4 form-group"> 
                            <label for="t_marks">Mobile No.:</label>
                            <input type="text" class="form-control" readonly="true" name="txtMobile" id="txtMobile" placeholder="Mobile No.">    
                        </div> 

                        <div class="col-sm-4 form-group"> 
                            <label for="category">Category:</label>
                            <input type="text" class="form-control" readonly="true" name="txtCategory" id="txtCategory" placeholder="Category">
                        </div> 

                        <div class="col-sm-4 form-group"> 
                            <label for="caste">Caste:</label>
                            <input type="text" placeholder="Caste" readonly="true" class="form-control" id="txtcaste" name="txtcaste"/>
                        </div> 

                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group"> 
                            <label for="attempt">Address:</label>  
                            <textarea class="form-control" rows="3" readonly="true" id="txtAddress" name="txtAddress" placeholder="Address"></textarea>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="branchname">Qualification:</label>
                            <input type="text" placeholder="Qualification" readonly="true" class="form-control" id="txtQualification" name="txtQualification"/>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="branchname">Minimum Qualification Percentage:</label>
                            <input type="text" placeholder="Minimum Qualification Percentage" readonly="true" class="form-control" id="txtminpercent" name="txtminpercent"/>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="branchname">Highest Qualification Percentage:</label>
                            <input type="text" placeholder="Highest Qualification Percentage" readonly="true" class="form-control" id="txthighpercent" name="txthighpercent"/>
                        </div>
                    </div>

                    <div class="container">
                        <?php if ($_REQUEST['Mode'] == 'Rejected') { ?>	
                            <div class="col-sm-4 form-group"> 
                                <label for="status">Status:<span class="star">*</span></label>
                                <select id="ddlstatus" name="ddlstatus" class="form-control" onchange="toggle_visibility1('remark')">
                                    <option value=""> Select Status </option>
                                    <option value="Rejected"> Reject </option>
                                </select>    
                            </div>	

                            <div class="col-sm-4 form-group" id="remark" style="display:none;"> 
                                <label for="pan">Remark:<span class="star">*</span></label>
                                <input type="text" class="form-control"  name="txtRemark" id="txtRemark"  placeholder="Remark">
                            </div>
                        <?php } ?>  					
                    </div>

                    <br>
                    <div class="panel panel-info">
                        <div class="panel-heading">Photo Sign</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-2" > 
                                    <label for="photo">Photo:</label> </br> </br> 
                                    <img id="uploadPreview1" src="images/not-found.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px">
                                </div>
                                <div class="col-sm-1"> 

                                </div>
                                <div class="col-sm-2"> 
                                    <label for="photo">Signature:</label> </br> </br>
                                    <img id="uploadPreview2" src="images/not-found.png" id="uploadPreview2" name="filePhoto" width="80px" height="107px">
                                </div>  
                                <div class="col-sm-1"> 

                                </div>

                            </div>

                        </div>
                    </div>

                    <br>

                    <div class="panel panel-info">
                        <div class="panel-heading">Qualification Documents</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-2" > 
                                    <label for="photo">Minimum Education Certificate:</label> </br>

                                    <img id="uploadPreview5" class="thumbnail img-responsive fix-image-sizes" src="images/not-found.png" id="uploadPreview5"
                                         name="fileMinQualifyCertificate" width="150px" height="150px">
                                </div>
                                <div class="col-sm-1"> 

                                </div>
                                <div class="col-sm-2" id="highestcertificate"> 
                                    <label for="photo">Highest Education Certificate:</label> </br>

                                    <img id="uploadPreview3" class="thumbnail img-responsive fix-image-sizes" src="images/not-found.png" id="uploadPreview3"
                                         name="fileCertificate" width="150px" height="150px">
                                </div>
                                <div class="col-sm-1"> 

                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="panel panel-info">
                        <div class="panel-heading">Proof Documents</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-2"> 
                                    <label for="photo">Age Proof Document:</label> </br>
                                    <img id="uploadPreview4" class="thumbnail img-responsive fix-image-sizes" src="images/not-found.png" id="uploadPreview4" name="fileProof" width="150px" height="150px">
                                </div>									
                                <div class="col-sm-1"> 

                                </div>

                                <div class="col-sm-2" id="caste"> 
                                    <label for="photo">Caste Proof Certificate:</label> </br>
                                    <img id="uploadPreview6" class="thumbnail img-responsive fix-image-sizes" src="images/not-found.png" id="uploadPreview6" name="fileCasteCertificate" width="150px" height="150px">
                                </div>

                                <div class="col-sm-2" id="aadhar"> 
                                    <label for="photo">Aadhar Card:</label> </br>
                                    <img id="uploadPreview7" class="thumbnail img-responsive fix-image-sizes" src="images/not-found.png" id="uploadPreview7" name="fileAadharCertificate" width="150px"
                                         height="150px">
                                </div>

                                <div class="col-sm-1"> 

                                </div>

                            </div>
                        </div>
                    </div>
                    <?php if ($_REQUEST['Mode'] == 'Rejected') { ?>					
                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div> 
                    <?php } ?>  
                    <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button class="close close2" type="button" data-dismiss="modal">×</button>
                                    <h3 class="modal-title">View Document</h3>
                                </div>
                                <div class="modal-body">

                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-default close2" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnailBig {margin-bottom:6px; width:800px;}
    .thumbnail {margin-bottom:6px;}
</style>

<script type="text/javascript">

    $(document).ready(function () {
        jQuery(".thumbnail").click(function () {
            $('.modal-body').empty();
            var title = $(this).parent('div').attr("title");
            $(this).addClass('thumbnailBig');
            $('.modal-title').html(title);
            $($(this).parents('div').html()).appendTo('.modal-body');
            $('#myModal').modal({show: true});
        });

        jQuery(".close2").click(function () {
            jQuery(".thumbnail").removeClass('thumbnailBig');
        });

    });
</script>

<script type="text/javascript">
    function toggle_visibility1(id) {
        var e = document.getElementById(id);

        //alert(e);
        var f = document.getElementById('ddlstatus').value;
        //alert(f);
        if (f == "Rejected")
        {
            e.style.display = 'block';
        } else {
            e.style.display = 'none';
        }

    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        } else if (Mode == 'Rejected')
        {
            fillForm();
        } else
            (Mode == 'Reject')
        {
            fillForm();
            //fillOldLearnerDetails();
        }

        function fillForm()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfWcdlearnercount.php",
                data: "action=EDIT&batch=" + Batch + "&lcode=" + txtapplicantid.value + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    txtLCode.value = data[0].LearnerCode;
                    txtlname.value = data[0].lname;
                    txtfname.value = data[0].fname;
                    dob.value = data[0].dob;
                    mstatus.value = data[0].mstatus;
                    txtdistrict.value = data[0].district;
                    txthighpercent.value = data[0].highper;

                    if (data[0].course == '24') {
                        txtminpercent.value = data[0].hscper;
                    } else {
                        txtminpercent.value = data[0].minper;
                    }

                    txtitgk.value = data[0].itgk;
                    txtMobile.value = data[0].mobile;
                    txtCategory.value = data[0].categoryname;
                    txtcategorycode.value = data[0].category;
                    txtQualification.value = data[0].qualification;
                    txtcaste.value = data[0].caste;
                    txtAddress.value = data[0].address;
                    txtaadharcard.value = data[0].pid;
                    $("#uploadPreview1").attr('src', "upload/oasis_photo/" + data[0].photo);
                    $("#uploadPreview2").attr('src', "upload/oasis_sign/" + data[0].sign);

                    if (data[0].highcert == "") {
                        $("#highestcertificate").hide();
                    } else {
                        $("#uploadPreview3").attr('src', "upload/oasis_highest_certificate/" + data[0].highcert);
                    }

                    $("#uploadPreview4").attr('src', "upload/oasis_age_proof/" + data[0].ageproof);

                    if (data[0].course == '24') {
                        $("#uploadPreview5").attr('src', "upload/oasis_HSC_certificate/" + data[0].hsccert);
                    } else {
                        $("#uploadPreview5").attr('src', "upload/oasis_minimum_certificate/" + data[0].mincert);
                    }

                    var txtcat = document.getElementById("txtcategorycode").value;
                    if (txtcat == "2" || txtcat == "3") {
                        $("#uploadPreview6").attr('src', "upload/oasis_caste_proof/" + data[0].casteproof);
                    } else {
                        $("#caste").hide();
                    }
                    var txtadhar = document.getElementById("txtaadharcard").value;
                    var txtlcode = document.getElementById("txtLCode").value;
                    if (txtadhar == "") {
                        //$("#aadhar").hide();
                        $("#uploadPreview7").attr('src', "upload/oasis_identity_proof/" + txtlcode + '_identityproof.jpg');

                        document.getElementById("uploadPreview7").onerror = function () {
                            $("#uploadPreview7").attr('src', "upload/oasis_identity_proof/" + txtlcode + '_aadhaar_card_proof.jpg');
                        }
                    } else {
                        $("#uploadPreview7").attr('src', "upload/oasis_identity_proof/" + txtlcode + '_aadhaar_card_proof.jpg');
                        document.getElementById("uploadPreview7").onerror = function () {
                            $("#uploadPreview7").attr('src', "upload/oasis_identity_proof/" + txtlcode + '_identityproof.jpg');
                        }
                    }

                }
            });
        }


        $("#btnSubmit").click(function () {
            if ($("#form").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfWcdlearnercount.php"; // the script where you handle the form input.            
                var data;
                if (Mode == 'Add')
                {

                } else {
                    data = "action=UpdateRejectingLearner&lcode=" + txtapplicantid.value + "&status=" + ddlstatus.value + "&remark=" + txtRemark.value + ""; // serializes the form's 
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            if(Redir=='WL')
                            {
                            window.setTimeout(function () {
                                                        window.location.href = "frmWcdWLLearnerCountList.php?batch=" + Batch + "&itgk=" + txtitgk.value + "&districtcode=" + txtdistrict.value + "&category=" + spcategory + "&mode=three";
                                                    }, 1000);
                            }
                            else{
                            window.setTimeout(function () {
                                                        window.location.href = "frmWcdLearnerCountList.php?batch=" + Batch + "&itgk=" + txtitgk.value + "&districtcode=" + txtdistrict.value + "&category=" + spcategory + "&mode=one";
                                                    }, 1000);
                            }
                            Mode = "Add";
                            resetForm("form");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                    }
                });
            }
            return false;
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmwcdrejectedlearner_validation.js"></script>
</html>