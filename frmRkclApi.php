<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of frm Rkcl Apis
 *
 *  Author Name:  Sunil Kumar Baindara
 */
$title="RKCl Apis Clients";
include ('header.php'); 
include ('root_menu.php'); 
//include './rkcltheme/include/menu.php';
		if (isset($_REQUEST['code'])) {
			echo "<script>var RootMenuCode=" . $_REQUEST['code'] . "</script>";
			echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
		} else {
			echo "<script>var RootMenuCode=0</script>";
			echo "<script>var Mode='Add'</script>";
		}
if ($_SESSION['User_Code'] == '1') {		
		?>		
<style type='text/css'>
.selectmulti {
    width:200px;
    height:50px;
    overflow:auto
}
</style>			
        <div class="container"> 			  

            <div class="panel panel-primary" style="margin-top:36px;">

                <div class="panel-heading">RKCl Apis Master</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmRootMenu" id="frmRootMenu" class="form-inline" role="form" action="">     
                        
                        <div id="addFrm" style="display: none;">
                             <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-md-6 form-group">     
                                <label for="learnercode"> Client Name:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtClientName" id="txtClientName" placeholder="Client Name">
                            </div>                            

                        </div>  

                        <div class="container">

                            <div class="col-sm-8 form-group">     
                                <label for="order">Api Password:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtApikey" id="txtApikey" placeholder="Api Password" >
                            </div>                            

                        </div>
                            <div class="container">

                            <div class="col-sm-8 form-group">     
                                <label for="order">Client IP:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtClientIP" id="txtClientIP" placeholder="Client IP" >
                            </div>                            

                        </div> 

                        <div class="container">

                            <div class="col-sm-10 form-group"> 
                                <label for="status">Status:</label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control">
                                  <option value="">Please Select</option>          
                                </select>
                            </div>                            
                        </div>


                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/> 
                            
                            <input type="button" name="btnCancle" id="btnCancle" class="btn btn-primary" onclick="cancleadd();" value="Cancel" /> 
                        </div>
                            
                        </div>
                        
                        
                        <div class="container"  id="btnShow">
                            <input type="button" name="btnShowFrm" id="btnShowFrm" class="btn btn-primary" value="Add Api Clients"/>    
                        </div>
						
			<div id="gird" style="margin-top:35px;"> </div>		
						
                </div>
            </div>   
        </div>
    </form>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
</body>
                <script type="text/javascript">
                    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                    $(document).ready(function () {

                        if (Mode == 'Delete')
                        {
                            if (confirm("Do You Want To Delete This Item ?"))
                            {
                                deleteRecord();
                            }
                        }
                        else if (Mode == 'Edit')
                        {
                            fillForm();
                            $("#gird").hide();
                            $("#btnShow").hide();
                            $("#addFrm").show();
                        }

                        function FillStatus() {
                            $.ajax({
                                type: "post",
                                url: "common/cfStatusMaster.php",
                                data: "action=FILL",
                                success: function (data) {
                                    $("#ddlStatus").html(data);
                                }
                            });
                        }

                        FillStatus();
                        /* get all function name in dropdown boxes*/
                        
                        function FillFname() {
                            $.ajax({
                                type: "post",
                                url: "common/cfRkclApi.php",
                                data: "action=FILLFNAME",
                                success: function (data) {
                                    $("#ddlFunctionName").html(data);
                                }
                            });
                        }

                        FillFname();
                        
                       
                        /* get all function name in dropdown boxes*/



                        function deleteRecord()
                        {//alert(RootMenuCode);
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                            $.ajax({
                                type: "post",
                                url: "common/cfRkclAPi.php",
                                data: "action=DELETE&values=" + RootMenuCode + "",
                                success: function (data) {
                                    //alert(data);
                                    if (data == SuccessfullyDelete)
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                        window.setTimeout(function () {
                                            window.location.href = "frmRkclApi.php";
                                        }, 1000);
                                        Mode = "Add";
                                        //resetForm("frmRootMenu");
                                    }
                                    else
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                    }
                                    showData();
                                }
                            });
                        }


                        function fillForm()
                        {
							//alert("HII");
                            $.ajax({
                                type: "post",
                                url: "common/cfRkclApi.php",
                                data: "action=EDIT&values=" + RootMenuCode + "",
                                success: function (data) {
									//alert(data);
                                    //alert($.parseJSON(data)[0]['Status']);
                                    data = $.parseJSON(data);
                                    txtClientName.value = data[0].ClientName;
                                    txtApikey.value = data[0].Apikey;
                                    txtClientIP.value = data[0].ClientIP;
                                    ddlStatus.value = data[0].Status;
                                }
                            });
                        }

                        function showData() {

                            $.ajax({
                                type: "post",
                                url: "common/cfRkclAPi.php",
                                data: "action=SHOW",
                                success: function (data) {
                                    //alert(data);
                                    $("#gird").html(data);

                                }
                            });
                        }
							
                        showData();
						
                        $("#btnShowFrm").click(function () {

                            $("#gird").hide();
                            $("#btnShow").hide();
                            $("#addFrm").show();
                        });
                        
                        $("#btnSubmit").click(function () {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                            var url = "common/cfRkclApi.php"; // the script where you handle the form input.
                            var data;
                            var strtxtFtable = "";
//                            $( "#ddlFtable2 option:selected" ).each(function() {
//                              strtxtFtable += $( this ).text() + ", ";
//                            });
//                            alert("You have selected the country - " + strtxtFtable);
                            
                            if (Mode == 'Add')
                            {
                                data = "action=ADD&txtClientName=" + txtClientName.value + "&txtStatus=" + ddlStatus.value + "&txtApikey=" + txtApikey.value + "&txtClientIP=" + txtClientIP.value + "";
//                                data = "action=ADD&txtClientName=" + txtClientName.value + "&txtStatus=" + ddlStatus.value + "&txtApikey=" + txtApikey.value + "&txtClientIP=" + txtClientIP.value + "&txtFunctionName=" + ddlFunctionName.value + "&txtddlFtable2=" + strtxtFtable +"";
                                // serializes the form's elements.UPDATE
                            }
                            else
                            {
                                data = "action=UPDATE&code=" + RootMenuCode + "&txtClientName=" + txtClientName.value + "&txtStatus=" + ddlStatus.value + "&txtApikey=" + txtApikey.value + "&txtClientIP=" + txtClientIP.value + "";
                                //data = "action=UPDATE&code=" + RootMenuCode + "&name=" + txtRootMenu.value + "&status=" + ddlStatus.value + "&display=" + txtDisplayOrder.value + ""; // serializes the form's elements.
                            }
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: data,
                                success: function (data)
                                {//alert(data);
                                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                        window.setTimeout(function () {
                                            window.location.href = "frmRkclApi.php";
                                        }, 1000);

                                        Mode = "Add";
                                        resetForm("frmRootMenu");
                                    }
                                    else
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                    }
                                    showData();


                                }
                            });

                            return false; // avoid to execute the actual submit of the form.
                        });
                        
                        function resetForm(formid) {
                            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                        }

                    });
                    
                    
                    function cancleadd(clientapiID)
                    {
                        window.location.href = "frmRkclApi.php";
                    }
                    
                    $("#ddlFunctionName").change(function() {
                        //get the selected value
                        var selectedValue = this.value;
                        //alert(selectedValue);
                        //make the ajax call
                        $.ajax({
                            url: "common/cfRkclApi.php",
                            type: 'POST',
                            data: "action=FILLTBFIELDNAME&ftablename="+selectedValue,
                            success: function(data) {
                                //console.log("Data sent!");
                                //alert(data);
                                $("#ddlFtable").html(data);
                            }
                        });
                    });
                    /* for add to left select box values*/
                    $(window).load(function(){
        
                    $('#ddlFtable').click(function(){
                        $('#ddlFtable option:selected').appendTo('#ddlFtable2');
                    });

                    $('#ddlFtable2').click(function(){
                        $('#ddlFtable2 option:selected').appendTo('#ddlFtable');
                    });

                    

                });
                    /* for add to left select box values*/

                </script>
                </html>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>