<?php
$title = "Course Allocation Process";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var CenterCode='" . $_REQUEST['cc'] . "'</script>";
    echo "<script>var Ack='" . $_REQUEST['ack'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">			 
        <div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">Approve Course Allocation</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" action="" class="form-inline">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <br>
                    <div class="container">

                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Name of Organization/Center:</label>
                            <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="ename">Registration No:</label>
                            <input type="text" class="form-control" readonly="true" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="faname">Date of Establishment:</label>
                            <input type="text" class="form-control" readonly="true" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Type of Organization:</label>
                            <input type="text" class="form-control" readonly="true" name="txtType" id="txtType" placeholder="Type Of Organization">  
                        </div>
                    </div>




                    <div class="container">




                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Document Type:</label>
                            <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                        </div>

                        <div class="col-sm-4 form-group">     
                            <label for="SelectType">Select Type of Application:</label>
                            <input type="text" class="form-control" readonly="true" name="txtRole" id="txtRole" placeholder="Role"/>     

                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="email">Enter Email:</label>
                            <input type="text" class="form-control" readonly="true" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="Mobile">Enter Mobile Number:</label>
                            <input type="text" class="form-control" readonly="true" name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                        </div>


                    </div>	


                    <div class="container">




                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">District:</label>
                            <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                            <input type="hidden" class="form-control"  name="txtDistrictCode" id="txtDistrictCode">
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Tehsil:</label>
                            <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="address">Area Type:</label>
                            <input type="text" class="form-control" readonly="true" name="txtAreaType" id="txtAreaType" placeholder="Area Type">    
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="address">Address:</label>
                            <textarea class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road"></textarea>

                        </div>
                    </div>
                    <div class="panel panel-success">
                        <div class="panel-heading">NCR Payment Details</div>
                        <div class="panel-body">
                            <div id="ncrpayment"></div>

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">Uploaded Signed SP-Center Agreement</div>
                        <div class="panel-body">	

                            <div class="col-sm-12" > 
                                <!--<label for="photo">SP-Center Agreement:<span class="star">*</span></label> </br>-->
                                <button type="button" id="uploadPreviewagreement" style="width:100%" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-success">
                        <div class="panel-heading">Uploaded NCR Center Visit Photos</div>
                        <div class="panel-body">	

                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Theory Room Photo:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreviewphoto1" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Reception Photo:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreviewphoto2" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Exterior Photo:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreviewphoto3" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Other Photo:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreviewphoto4" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">Uploaded Owner Documents</div>
                        <div class="panel-body">	

                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Pan Card:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview1" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">AADHAR Card:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview2" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Address Proof:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview3" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">NCR Applcation Form:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview4" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-success">
                        <div class="panel-heading">Uploaded Organization Type Documents</div>
                        <div class="panel-body">
                            <button type="button" id="showmodal" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc1"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview10" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc2"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview11" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc3"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview12" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc4"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview13" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc5"> 
                                <label for="orgtypedoc5"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview14" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc6"> 
                                <label for="orgtypedoc6"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview15" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc7"> 
                                <label for="orgtypedoc7"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview16" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">Organization Details</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">Bank Account Details</label> </br>
                                    <button type="button" id="bankdetail" class="btn btn-primary" name="bankdetail" width="150px" height="150px">
                                        View Details</button>	
                                </div>
                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">Owner Details</label> </br>
                                    <button type="button" id="ownerdetail" class="btn btn-primary" name="ownerdetail" width="150px" height="150px">
                                        View Details</button>	
                                </div>
                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">HR Details</label> </br>
                                    <button type="button" id="hrdetail" class="btn btn-primary" name="hrdetail" width="150px" height="150px">
                                        View Details</button>	
                                </div>
                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">System And Intake Details</label> </br>
                                    <button type="button" id="sidetail" class="btn btn-primary" name="sidetail" width="150px" height="150px">
                                        View Details</button>	
                                </div>
                            </div>
                            <div class="container">
                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">Premises Details</label> </br>
                                    <button type="button" id="premisesdetail" class="btn btn-primary" name="premisesdetail" width="150px" height="150px">
                                        View Details</button>	
                                </div>
                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">IT Peripherals Details</label> </br>
                                    <button type="button" id="itdetail" class="btn btn-primary" name="itdetail" width="150px" height="150px">
                                        View Details</button>	
                                </div>
                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">SP Details</label> </br>
                                    <button type="button" id="showspdetail" class="btn btn-primary" name="showspdetail" width="150px" height="150px">
                                        View Details</button>	
                                </div>

                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">Hide All Details</label> </br>
                                    <button type="button" id="hideall" class="btn btn-primary" name="hideall" width="150px" height="150px">
                                        Hide Details</button>	
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info" id="bankaccount" style="display:none">
                        <div class="panel-heading">Bank Account Details</div>
                        <div class="panel-body">
                            <div id="bankdatagird"></div>

                        </div>
                    </div>
                    <div class="panel panel-info" id="ownergrid" style="display:none">
                        <div class="panel-heading">Owner Details</div>
                        <div class="panel-body">
                            <div id="ownerdatagrid"></div>

                        </div>
                    </div>
                    <div class="panel panel-info" id="hrgrid" style="display:none">
                        <div class="panel-heading">HR Details</div>
                        <div class="panel-body">
                            <div id="hrdatagrid"></div>
                        </div>
                    </div>
                    <div class="panel panel-info" id="sigrid" style="display:none">
                        <div class="panel-heading">System and Intake Details</div>
                        <div class="panel-body">
                            <div id="sidatagrid"></div>
                        </div>
                    </div>
                    <div class="panel panel-info" id="premisesgrid" style="display:none">
                        <div class="panel-heading">Premises Details</div>
                        <div class="panel-body">
                            <div id="premisesdatagrid"></div>
                        </div>
                    </div>
                    <div class="panel panel-info" id="itgrid" style="display:none">
                        <div class="panel-heading">IT Peripherals Details</div>
                        <div class="panel-body">
                            <div id="itdatagrid"></div>
                        </div>
                    </div>
                    <div class="panel panel-info" id="locationgrid" style="display:none">
                        <div class="panel-heading">Location Details</div>
                        <div class="panel-body">
                            <!--                                <div id="googleMap"  class="mapclass"></div>-->
                        </div>
                    </div>
                    <div class="panel panel-info" id="spdetail" style="display:none">
                        <div class="panel-heading">Current Service Provider's Registered - Details</div>
                        <div class="panel-body">

                            <div class="container">
                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Name of Organization/Center:</label>
                                    <textarea class="form-control" readonly="true" name="txtrspname" id="txtrspname" placeholder="Name of the Organization/Center"></textarea>
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">Address:</label>
                                    <input type="text" class="form-control" readonly="true" name="Address" id="Address" placeholder="Registration No">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="faname">Date of Establishment:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtdate" id="txtdate"  placeholder="YYYY-MM-DD">
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Type of Organization:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtRspType" id="txtRspType" placeholder="Type Of Organization">  
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">NCR Visit Details</div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">SP Visit Feedback</label> </br>
                                    <button type="button" id="spvisit" class="btn btn-primary" name="spvisit" width="150px" height="150px">
                                        View Details</button>	
                                </div>
                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">DPO Visit Feedback</label> </br>
                                    <button type="button" id="dpovisit" class="btn btn-primary" name="dpovisit" width="150px" height="150px">
                                        View Details</button>	
                                </div>
                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">Faculty Result</label> </br>
                                    <button type="button" id="facultyresult" class="btn btn-primary" name="facultyresult" width="150px" height="150px">
                                        View Details</button>	
                                </div>

                                <div class="col-sm-4 form-group" > 
                                    <label for="orgdetail1">Hide All Details</label> </br>
                                    <button type="button" id="hideallvisit" class="btn btn-primary" name="hideallvisit" width="150px" height="150px">
                                        Hide Details</button>	
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info" id="spvisitdiv" style="display:none">
                        <div class="panel-heading">SP Visit Feedbcak Details</div>
                        <div class="panel-body">
                            <div id="spvisitgird"></div>

                        </div>
                    </div>
                    <div class="panel panel-info" id="dpovisitdiv" style="display:none">
                        <div class="panel-heading">DPO Visit Feedbcak Details</div>
                        <div class="panel-body">
                            <div id="dpovisitgrid"></div>

                        </div>
                    </div>
                    <div class="panel panel-info" id="facultyresultdiv" style="display:none">
                        <div class="panel-heading">Faculty Result Details</div>
                        <div class="panel-body">
                            <div id="facultyresultgrid"></div>
                        </div>
                    </div>
                    <!--                    <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="status">Action:<span class="star">*</span></label>
                                                <select id="ddlstatus" name="ddlstatus" class="form-control" onchange="toggle_visibility1('remark')">
                                                    <option selected="true" value="">Select</option>
                                                    <option value="Approve" >Approve</option>
                                                    <option value="Reject" >Reject</option>
                                                </select>    
                                            </div>
                                            <div class="col-sm-4 form-group" id="remark" style="display:none;"> 
                                                <label for="pan">Remark:<span class="star">*</span></label>
                                                <input type="text" class="form-control"  name="txtRemark" id="txtRemark"  placeholder="Remark">
                                            </div>
                    
                    
                                        </div>		-->

                    <!--                    <div class="container">
                    
                                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-danger" value="Approve"/> &nbsp;&nbsp;   
                                            <input type="button" name="btnshowsms" id="btnshowsms" class="btn btn-default" value="Send SMS to Service Provider"/>    
                                        </div>-->
                    <div class="panel panel-success" id="submitdiv">
                        <div class="panel-heading">Approve/Reject NCR Application</div>
                        <div class="panel-body">
                            <!--<div class="container" id="submitdiv">-->

                            <button type="button" name="btnSubmit" id="btnSubmit" class="btn btn-lg btn-success"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Click here to Approve</button>
                            <span id="spanid" style="font-size:15px; color: red;">Note : Action can not be revert.</span>

                            <button type="button" name="btnReject" id="btnReject" class="btn btn-lg btn-danger pull-right"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;Reject</button>

                            <!--</div>-->
                        </div>
                    </div>
                    <!--<br>-->
                    <!--<input type="button" name="btnshowsms" id="btnshowsms" class="btn btn-default" value="Send SMS to Service Provider"/>-->  

                    <!--                    <div class="panel panel-info" id="smspanel" style="display:none">
                                            <div class="panel-heading">Send SMS to Service Provider</div>
                                            <div class="panel-body">	
                                                <div id="SMSresponse"></div>
                                            <div class="container">
                                                <div class="col-sm-8">     
                                                    <label for="learnercode">Enter SMS text:</label>
                                                    <textarea class="form-control" width="700px" height="103px"   name="txtsms" id="txtsms" value=""></textarea>
                                                </div>
                                                <br><br>
                                                <div class="col-sm-4"> 
                                                    <input type="button" name="btnsendsms" id="btnsendsms" class="btn btn-primary" value="Send SMS to Service Provider"/>    
                                                </div>
                                            </div>
                                        </div>
                                </div>-->
            </div>
            </form>
        </div>  

    </div>

    <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" style="width: 100%;height: 500px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h3 id="heading-tittle" class="modal-title">Heading</h3>
                </div>
                <iframe id="viewimagesrc" src="uploads/news/1487051012.pdf" style="width: 100%;height: 500px;border: none;"></iframe>
                <div id="viewimagesrc1"></div>
                <!--<img id="viewimagesrc" class="thumbnail img-responsive" src="images/not-found.png" name="filePhoto3" width="800px" height="880px">-->
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>
<!--<script type="text/javascript">
    function toggle_visibility1(id) {
        var e = document.getElementById(id);

        //alert(e);
        var f = document.getElementById('ddlstatus').value;
        //alert(f);
        if (f == "Reject")
        {
            e.style.display = 'block';
        } else {
            e.style.display = 'none';
        }

    }
</script>-->

<script type="text/javascript">

    $(document).ready(function () {
        jQuery(".thumbnailmodal").click(function () {
            $('.modal-body').empty();
            var title = $(this).parent('a').attr("title");
            $('.modal-title').html(title);
            $($(this).parents('div').html()).appendTo('.modal-body');
            $('#showmodal').click();
        });
    });
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        } else if (Mode == 'Edit')
        {
            //alert(1);
            fillForm();
            showPaymentDetails();
            fillCenterVisitPhoto();
            fillSPCenterAgreement();

        }
        
         $(".close").on("click",function(){
        $("#viewimagesrc").attr("src"," "); 
        $("#viewimagesrc").css("display","block");
    });

        function showPaymentDetails() {

            $.ajax({
                type: "post",
                url: "common/cfCourseAuthorization.php",
                data: "action=PAYMENTDETAIL&centercode=" + CenterCode + "",
                success: function (data) {

                    $("#ncrpayment").html(data);
                    $('#examplepay').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }

        function fillSPCenterAgreement()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfCourseAuthorization.php",
                data: "action=SPCenterAgreement&values=" + CenterCode + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);

                    $("#uploadPreviewagreement").click(function () {
                        $("#viewimagesrc").css("display","block");
                        $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "SPCENTERAGREEMENT/" + data[0].SPCEN + "?nocache="+Math.random());
                    });
                }
            });
        }

        function fillCenterVisitPhoto()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfCourseAuthorization.php",
                data: "action=NCRVISIT&values=" + CenterCode + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    $("#uploadPreviewphoto1").click(function () {
                        //$("#viewimagesrc").attr('src', "upload/NCRVISIT/" + data[0].NCRTP);
                         $("#viewimagesrc").css("display","none");
                        $("#viewimagesrc1").html(data[0].NCRTP);
                    });
                    $("#uploadPreviewphoto2").click(function () {
                        //$("#viewimagesrc1").attr('src', "upload/NCRVISIT/" + data[0].NCRRP);
                        $("#viewimagesrc").css("display","none");
                        $("#viewimagesrc1").html(data[0].NCRRP);
                    });
                    $("#uploadPreviewphoto3").click(function () {
                        //$("#viewimagesrc1").attr('src', "upload/NCRVISIT/" + data[0].NCREP);
                        $("#viewimagesrc").css("display","none");
                        $("#viewimagesrc1").html(data[0].NCREP);
                    });
                    $("#uploadPreviewphoto4").click(function () {
                        //$("#viewimagesrc1").attr('src', "upload/NCRVISIT/" + data[0].NCROP);
                        $("#viewimagesrc").css("display","none");
                        $("#viewimagesrc1").html(data[0].NCROP);
                    });
                }
            });
        }

        function fillForm()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfNCRLoginApproval.php",
                data: "action=PROCESS&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    txtName1.value = data[0].orgname;
                    txtRegno.value = data[0].regno;
                    txtEstdate.value = data[0].fdate;
                    txtType.value = data[0].orgtype;
                    txtDocType.value = data[0].doctype;
                    txtRole.value = data[0].role;

                    txtEmail.value = data[0].email;
                    txtMobile.value = data[0].mobile;
                    txtDistrict.value = data[0].district;
                    txtDistrictCode.value = data[0].districtcode;
                    txtTehsil.value = data[0].tehsil;
                    //txtStreet.value = data[0].street;
                    txtRoad.value = data[0].road;
                    txtAreaType.value = data[0].areatype;
                    $("#uploadPreview1").click(function () {
                        $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRPAN/" + data[0].orgdoc + "?nocache="+Math.random());
                    });
                    $("#uploadPreview2").click(function () {
                        $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRUID/" + data[0].orguid + "?nocache="+Math.random());
                    });
                    $("#uploadPreview3").click(function () {
                        $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRAddProof/" + data[0].orgaddproof + "?nocache="+Math.random());
                    });
                    $("#uploadPreview4").click(function () {
                        $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRAppForm/" + data[0].orgappform + "?nocache="+Math.random());
                    });
//                    $("#uploadPreview1").attr('src', "upload/NCRPAN/" + data[0].orgdoc);
//                    $("#uploadPreview2").attr('src', "upload/NCRUID/" + data[0].orguid);
//                    $("#uploadPreview3").attr('src', "upload/NCRAddProof/" + data[0].orgaddproof);
//                    $("#uploadPreview4").attr('src', "upload/NCRAppForm/" + data[0].orgappform);

                    if (txtType.value == "Proprietorship/Individual")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Ownership Type Document");
                        jQuery("label[for='orgtypedoc2']").html("Shop Act License");
                        jQuery("label[for='orgtypedoc3']").html("PAN card copy of Proprietor");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled Cheque");

                        $("#doc5").hide();
                        $("#doc6").hide();
                        $("#doc7").hide();


                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_owntype.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_salpi.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_panpi.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_ccpi.pdf");
                        });

                    }
                    if (txtType.value == "Partnership")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Copy of Partnership Deed");
                        jQuery("label[for='orgtypedoc2']").html("Authorization letter");
                        jQuery("label[for='orgtypedoc3']").html("PAN card of Partnership firm");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled cheque of firm");
                        jQuery("label[for='orgtypedoc5']").html("Address Proof documents");
                        jQuery("label[for='orgtypedoc6']").html("Registration certificate");
                        jQuery("label[for='orgtypedoc7']").html("Shop Act Registration Copy");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_copd.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_al.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ppf.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ccpf.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_apdpf.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_rcrf.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_sarpf.pdf");
                        });

                    }
                    if (txtType.value == "Private Ltd." || txtType.value == "Public Ltd.")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Incorporation");
                        jQuery("label[for='orgtypedoc2']").html("List Of Directors");
                        jQuery("label[for='orgtypedoc3']").html("Pan Card Of Company");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled Cheque");
                        jQuery("label[for='orgtypedoc5']").html("Board Resolution Copy");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof Document");
                        jQuery("label[for='orgtypedoc7']").html("Shop Act Registration Copy");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_coi.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_lod.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_copan.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_ccc.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_cobr.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_apd.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_csar.pdf");
                        });

                    }
                    if (txtType.value == "Limited Liability Partnership (LLP)")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Incorporation");
                        jQuery("label[for='orgtypedoc2']").html("List of Partners");
                        jQuery("label[for='orgtypedoc3']").html("Copy of PAN card of LLP");
                        jQuery("label[for='orgtypedoc4']").html("Canceled cheque of LLP");
                        jQuery("label[for='orgtypedoc5']").html("Copy of Board Resolutions");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof Document");
                        jQuery("label[for='orgtypedoc7']").html("Copy of Shop Act Registration");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_coillp.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_lop.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_panllp.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_ccllp.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_cobrllp.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_apdllp.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_csarllp.pdf");
                        });

                    }
                    if (txtType.value == "Trust" || txtType.value == "Society" || txtType.value == "Coorperative Society" || txtType.value == "Others")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Registration");
                        jQuery("label[for='orgtypedoc2']").html("PAN Card of Organization");
                        jQuery("label[for='orgtypedoc3']").html("Cancelled cheque");
                        jQuery("label[for='orgtypedoc4']").html("List of Executive Body");
                        jQuery("label[for='orgtypedoc5']").html("Copy of Board Resolution");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof documents");

                        $("#doc7").hide();

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cor.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_panoth.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_ccoth.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_leb.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cbr.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_adpoth.pdf");
                        });

                    }
                }
            });
        }

        function showBankAccountData() {

            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=BANKACCOUNT&centercode=" + CenterCode + "",
                success: function (data) {data = data.trim();

                    $("#bankdatagird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }
        $("#bankdetail").click(function () {
            showBankAccountData();
            $("#bankaccount").show(3000);
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#spdetail").hide();
        });

        function showOwnerData() {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=OWNERDATA&centercode=" + CenterCode + "",
                success: function (data) {data = data.trim();
//alert(data);
                    $("#ownerdatagrid").html(data);
                    $('#example1').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
//alert(data);
                }
            });
        }

        $("#ownerdetail").click(function () {
            showOwnerData();
            $("#bankaccount").hide();
            $("#ownergrid").show(3000);
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#spdetail").hide();
        });

        function showHRData() {

            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=HRDATA&centercode=" + CenterCode + "",
                success: function (data) {data = data.trim();

                    $("#hrdatagrid").html(data);
                    $('#example2').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }

        $("#hrdetail").click(function () {
            showHRData();
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").show(3000);
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#spdetail").hide();
        });

        function showSIData() {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=SIDATA&centercode=" + CenterCode + "",
                success: function (data) {data = data.trim();
//alert(data);
                    $("#sidatagrid").html(data);
                    $('#example3').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });
        }


        $("#sidetail").click(function () {
            showSIData();
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").show(3000);
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#spdetail").hide();
        });

        function showPremisesData() {

            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=PREMISESDATA&centercode=" + CenterCode + "",
                success: function (data) {data = data.trim();

                    $("#premisesdatagrid").html(data);
                    $('#example4').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });
        }

        $("#premisesdetail").click(function () {
            showPremisesData();
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").show(3000);
            $("#itgrid").hide();
            $("#spdetail").hide();
        });

        function showITData() {

            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=ITDATA&centercode=" + CenterCode + "",
                success: function (data) {data = data.trim();

                    $("#itdatagrid").html(data);
                    $('#example5').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }


        $("#itdetail").click(function () {
            showITData();
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").show(3000);
            $("#spdetail").hide();
        });

        function FillCurrentRsp() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfCourseAuthorization.php",
                data: "action=GETREGRSP&centercode=" + CenterCode + "",
                success: function (data) {data = data.trim();
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Serice Provider Not Found " + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        txtrspname.value = data[0].rspname;
                        Address.value = data[0].mobno;
                        txtdate.value = data[0].date;
                        txtRspType.value = data[0].rsptype;
                    }
                }
            });
        }


        $("#showspdetail").click(function () {
            FillCurrentRsp();
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#spdetail").show(3000);
        });

        $("#hideall").click(function () {
            $("#bankaccount").hide();
            $("#ownergrid").hide();
            $("#hrgrid").hide();
            $("#sigrid").hide();
            $("#premisesgrid").hide();
            $("#itgrid").hide();
            $("#spdetail").hide();
        });

//        $("#btnshowsms").click(function () {
//
//            $("#submitdiv").hide();
//            $("#btnshowsms").hide();
//            $("#smspanel").show(3000);
//            document.getElementById("txtsms").defaultValue = "Example: Dear Service Provider, The NCR Application of Center Code " + CenterCode + " has been put on hold due to incomplete/incorrect documents uploaded. Kindly update document name for approval from RKCL.";
//
//        });


        function showSPVisitDetails(UserCode) {

            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=SPVISIT&centercode=" + CenterCode + "&visitorcode=" + UserCode + "",
                success: function (data) {data = data.trim();
                    $("#spvisitgird").html(data);
                    $('#example6').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }

        $("#spvisit").click(function () {
            showSPVisitDetails('14');
            $("#spvisitdiv").show(3000);
            $("#dpovisitdiv").hide();
            $("#facultyresultdiv").hide();
        });

        function DPOVisitDetails(UserCode) {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=SPVISIT&centercode=" + CenterCode + "&visitorcode=" + UserCode + "",
                success: function (data) {data = data.trim();
//alert(data);
                    $("#dpovisitgrid").html(data);
                    $('#example6').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
//alert(data);
                }
            });
        }

        $("#dpovisit").click(function () {
            $("#spvisitgird").html('');
            DPOVisitDetails('23');
            $("#spvisitdiv").hide();
            $("#dpovisitdiv").show(3000);
            $("#facultyresultdiv").hide();
        });

        function FacultyResultDetails() {

            $.ajax({
                type: "post",
                url: "common/cfNcrFinalApproval.php",
                data: "action=FACULTYRESULT&centercode=" + CenterCode + "",
                success: function (data) {data = data.trim();

                    $("#facultyresultgrid").html(data);
                    $('#example7').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }

        $("#facultyresult").click(function () {
            FacultyResultDetails();
            $("#spvisitdiv").hide();
            $("#dpovisitdiv").hide();
            $("#facultyresultdiv").show(3000);
        });

        $("#hideallvisit").click(function () {
            $("#spvisitdiv").hide();
            $("#dpovisitdiv").hide();
            $("#facultyresultdiv").hide();
        });

        $("#btnSubmit").click(function () {
            var result = confirm('Are You Sure to APPROVE NCR Appplication');
            if (result == true) {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfCourseAuthorization.php"; // the script where you handle the form input.            
                var data;
                if (Mode == 'Add')
                {

                } else
                {
                    //data = "action=LoginApprove&orgcode=" + Code + "&status=" + ddlstatus.value + "&remark=" + txtRemark.value + "&districtcode=" + txtDistrictCode.value + ""; // serializes the form's elements.
                    data = "action=CourseAllocation&ack=" + Ack + "&centercode=" + CenterCode + "&mobile=" + txtMobile.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {data = data.trim();
                        //alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Course Allocation Approved Successfully</span></p>");
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Course Allocation Approved Successfully.</span>");
                            window.setTimeout(function () {
                                window.location.href = "frmcourseauthorization.php";
                            }, 3000);

                            Mode = "Add";
                            resetForm("form");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();


                    }
                });

            } else {
                return false;
            }
        });

//          $("#btnsendsms").click(function () {
//            $('#SMSresponse').empty();
//            $('#SMSresponse').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
//            var url = "common/cfCourseAuthorization.php"; // the script where you handle the form input.            
//            var data;
//            if (Mode == 'Add')
//            {
//
//            } else
//            {
//                //data = "action=LoginApprove&orgcode=" + Code + "&status=" + ddlstatus.value + "&remark=" + txtRemark.value + "&districtcode=" + txtDistrictCode.value + ""; // serializes the form's elements.
//                data = "action=SendSMS&sms=" + txtsms.value + "&centercode=" + CenterCode + "&mobile=" + txtMobile.value + ""; // serializes the form's elements.
//            }
//            $.ajax({
//                type: "POST",
//                url: url,
//                data: data,
//                success: function (data)
//                {
//                    //alert(data);
//                    if (data == SuccessfullyInsert || data == 'Success')
//                    {
//                        $('#response').empty();
//                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>SMS has been sent to Service Provider Successfully</span></p>");
//                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; SMS has been sent to Service Provider Successfully.</span>");
//                        window.setTimeout(function () {
//                            window.location.href = "frmcourseauthorization.php";
//                        }, 3000);
//
//                        Mode = "Add";
//                        resetForm("form");
//                    } else
//                    {
//                        $('#response').empty();
//                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
//                    }
//                    //showData();
//
//
//                }
//            });
//
//            return false;
//        });

        $("#btnReject").click(function () {
            var result = confirm('Are You Sure to REJECT NCR Appplication');
            if (result == true) {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfCourseAuthorization.php"; // the script where you handle the form input.            
                var data;
                if (Mode == 'Add')
                {

                } else
                {
                    //data = "action=LoginApprove&orgcode=" + Code + "&status=" + ddlstatus.value + "&remark=" + txtRemark.value + "&districtcode=" + txtDistrictCode.value + ""; // serializes the form's elements.
                    data = "action=REJECT&ack=" + Ack + "&centercode=" + CenterCode + "&mobile=" + txtMobile.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {data = data.trim();
                        //alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Course Allocation Rejected Successfully</span></p>");
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Course Allocation Rejected Successfully.</span>");
                            window.setTimeout(function () {
                                window.location.href = "frmcourseauthorization.php";
                            }, 3000);

                            Mode = "Add";
                            resetForm("form");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();


                    }
                });

            } else {
                return false;
            }
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmcorrectionprocess_validation.js"></script>
</html>