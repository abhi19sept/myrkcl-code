<?php
$title = "WCD Scheme Declaration Report";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '4'
|| $_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '28' || $_SESSION['User_UserRoll'] == '8'){
	
}
else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">WCD Scheme Declaration Report</div>
            <div class="panel-body">

                <form name="frmWcdSchemedeclareRpt" id="frmWcdSchemedeclareRpt" class="form-inline" role="form"
					enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                     
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdSchemeDeclarationRpt.php"; // the script where you handle the form input.
            var data;
            data = "action=GETDATA"; //
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}
            });
        }        
	
	showData();

        
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>

<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
