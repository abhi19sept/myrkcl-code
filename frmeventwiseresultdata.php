<?php
$title = "Exam Result";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_Code'] == '8550') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Exam Result</div>
            <div class="panel-body">
               <form name="form" id="frmlearnerresult" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                    <div class="container">
                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control" required="true">

                            </select>
                        </div> 
                    </div>

                    <div class="container" style="margin-top:15px;margin-left:12px;">
						<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" 
							style="display:none;"/>
					</div> 
					
					<div id="grid" style="margin-top:5px;"> </div> 
						    <div class="container">
								<iframe id="testdoc" src="" style="width: 100%;height: 500px;border: none; display: none;"></iframe>
							</div> 

            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillEvent() {
            $.ajax({
                type: "post",
                url: "common/cfEventMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlExamEvent").html(data);
                }
            });
        }
        FillEvent();
    });
	
	$("#btnSubmit").click(function () {
               // e.preventDefault();
			$('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

        $.ajax({
            type: "post",
            url: "common/cfEventWiseResultData.php",
            data: "action=GetData&examevent=" + ddlExamEvent.value + "",
            success: function (data)
            {
				var data = data.trim();
                $('#response').empty();
				if(data=='blank'){
					$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Select Event.</span></div>");
				}else{
					//window.open(data, '_blank');
					$("#testdoc").attr('src', data);
				}
                		
            }
        });
	});
	
    $("#ddlExamEvent").change(function () {
		var eventid = $(this).val();
		if(eventid==''){
				$('#btnSubmit').hide();
			}else{
				$('#btnSubmit').show(1000);
			}	
    });

</script>
</html>