<?php
$title = "Final Result Manage";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1') {
    //Login here with ITGK or Superadmin
} else {
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>
<style>
    .modal {z-index: 1050!important;} 
    #turmcandition{
        background-color: whitesmoke;
        text-align: justify;
        padding: 10px;
    }
    .noturm{
        margin-left: 10px !important;
    }
    .yesturm{
        margin-right: 15px;
    }
</style>
<div style="min-height:430px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Manage Final Result</div>
            <div class="panel-body">
                <form name="frmmanagefinalresult" id="frmmanagefinalresult" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">

                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-3 form-group"> 
                                <label for="course">Select Event Name:<span class="star">*</span></label>
                                <select id="ddlevent" name="ddlevent" class="form-control" required="required">

                                </select>
                            </div> 
                        </div>
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="button" name="btnSubmitlist" id="btnSubmitlist" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        <div class="container">
                            <div id="response" style="width: 90%;"></div>
                        </div>
                    </div>   
                </form>
                <div id="grid" style="margin-top:5px; width:94%;"> </div>
                <form name="frmupdatefinalresult" id="frmupdatefinalresult" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div id="updatedatafiels"></div>
                        <div class="container">
                            <div id="responseupdate" style="width: 90%;"></div>
                        </div>
                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">

    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")

        if (key == 8 || key == 0) {
            keychar = 8;
        }

        return reg.test(keychar);
    }

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillEvent() {
            $.ajax({
                type: "post",
                url: "common/cffinalresultmanage.php",
                data: "action=FILLEvent",
                success: function (data) {
                    $("#ddlevent").html(data);
                }
            });
        }
        FillEvent();
        $("#grid").on("click",".btnupdatefinaldata",function(){
            var event_id = $(this).attr("id");
                $.ajax({
                    type: "post",
                    url: "common/cffinalresultmanage.php",
                    data: "action=GETDATAEDIT&event_id=" + event_id + "",
                    success: function (data) {
                        $("#hidearea").css('display','none');
                        $('#updatedatafiels').html(data);
                    }
                });
           
        });
        $("#btnSubmitlist").click(function () {
            if ($("#frmmanagefinalresult").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=20px style='margin: 5px;' /></span><span style='font-weight: bold;'> Processing please wait..... </span></p>");
                var url = "common/cffinalresultmanage.php"; // the script where you handle the form input.     
                var data;
                var event = $('#ddlevent').val();
                data = "action=ViewEventData&event=" + event + ""; //            
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) { 
                         $('#response').empty();
                         $('#grid').html(data);
                    }
                });

            }
            return false;
        });
        $("#updatedatafiels").on("click","#btnSubmitUpdate",function(){
            if ($("#frmupdatefinalresult").valid())
            {
                var result = confirm('Are You Sure Want To Update This Exam Event Data');
                if (result == true) {
                   var event = $('#ddlevent').val();
                   var txtexam_date = $('#txtexam_date').val();
                   var txtexam_held_date = $('#txtexam_held_date').val();
                   var txtresult_date = $('#txtresult_date').val();
                   if(isExamDate(txtexam_date)==true){
                      $("#errorexamdate").html("");
                      if(isValidDate(txtexam_held_date)==true){
                        $("#errorhelddate").html("");
                            if(isValidDate(txtresult_date)==true){
                                $("#errorresultdate").html(""); 
                                $('#responseupdate').empty();
                                $('#responseupdate').append("<p class='error'><span><img src=images/ajax-loader.gif width=20px style='margin: 5px;' /></span><span style='font-weight: bold;'> Processing please wait..... </span></p>");
                                var url = "common/cffinalresultmanage.php"; // the script where you handle the form input.     
                                var data;

                                data = "action=UPDATEDATES&event=" + event + "&txtexam_date=" + txtexam_date + "&txtexam_held_date=" + txtexam_held_date + "&txtresult_date=" + txtresult_date + ""; //            
                                $.ajax({
                                    type: "post",
                                    url: url,
                                    data: data,
                                    success: function (data) { 
                                        if (data === "success")
                                        {
                                            $('#responseupdate').empty();
                                            $('#responseupdate').append("<p class='alert-success'><span><img src=images/correct.gif width=10px style='width: 25px;margin: 0 10px 4px 0;' /></span><span style='color: green;'>Status Update Successfully.</span></p>");
                                            window.setTimeout(function () {
                                                window.location.href = "frmfinalresultmanage.php";
                                            }, 2000);

                                        }else{
                                            $('#responseupdate').empty();
                                            $('#responseupdate').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span style='color: red;'><span>Error To Update Status.</span></p>");
                                        }
                                    }
                                });
                            }else{
                               $("#errorresultdate").html("Date Format Should <br> YYYY-MM-DD");
                               return false; 
                            }
                        }else{
                            $("#errorhelddate").html("Date Format Should <br> YYYY-MM-DD");
                            return false;
                        }
                   }else{
                      $("#errorexamdate").html("Date Format Should <br> DD-MM-YYYY");
                       return false; 
                   }
                   
            }else {
                return false;
            }
            }
            return false;
        });

    });
function isValidDate(dateString) {
  var regEx = /^\d{4}-\d{2}-\d{2}$/;
  return dateString.match(regEx) != null;
}
function isExamDate(dateString) {
  var dtValue = dateString;
  var dtRegex = new RegExp("^([0]?[1-9]|[1-2]\\d|3[0-1])-(01|02|03|04|05|06|07|08|09|10|11|12)-[1-2]\\d{3}$", 'i');
  return dtRegex.test(dtValue);
}
</script>
<script>
    $("#frmmanagefinalresult").validate({
        rules: {
            ddlevent: "required"

        },
        messages: {
            ddlevent: "Please Select Event"
        },
    });
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
        background-color: #e2d9d9;
        width: 91%;
    }
</style>
</html>
?>