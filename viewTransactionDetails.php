<?php
        $title = "Transaction Details";
        include('header.php');
        include('root_menu.php');
        include 'common/modals.php';
        if ($_SESSION["User_UserRoll"] == 14 || $_SESSION["User_UserRoll"] == 7) {
            echo "<script>var OrgCode = '" . $_SESSION['User_Code'] . "'; </script>";
            ?>
            <style type="text/css">

                .division_heading {
                    border-bottom: 1px solid #e5e5e5;
                    padding-bottom: 10px;
                    font-size: 20px;
                    color: #000;
                    margin-bottom: 20px;

                }

                .extra-footer-class {
                    margin-top: 0;
                    margin-bottom: -10px;
                    padding: 16px;
                    background-color: #fafafa;
                    border-top: 1px solid #e5e5e5;
                }

                .ref_id {
                    text-align: center;
                    font-size: 20px;
                    color: #000;
                    margin: 0 0 10px 0;
                }

                .form-control {
                    border-radius: 2px;
                }

                input[type=text]:hover, textarea:hover {
                    box-shadow: 0 1px 3px #aaa;
                    -webkit-box-shadow: 0 1px 3px #aaa;
                    -moz-box-shadow: 0 1px 3px #aaa;
                }

                .col-sm-3:hover {
                    background: none !important;
                }

                .modal-open .container-fluid, .modal-open .container {
                    -webkit-filter: blur(5px) grayscale(50%);
                    filter: blur(5px) grayscale(50%);
                }

                .btn-success {
                    background-color: #00A65A !important;
                }

                .btn-success:hover {
                    color: #fff !important;
                    background-color: #04884D !important;
                    border-color: #398439 !important;
                }

                .addBottom {
                    margin-bottom: 12px;
                }

            </style>

            <div class="container" id="showdata">


                <div class="panel panel-primary" style="margin-top:46px !important;">

                    <div class="panel-heading">Transaction Details</span></div>
                    <div class="panel-body">

                        <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateOrgDetails" name="updateOrgDetails">


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="division_heading">
                                        Transaction Details for Reference ID: <span id="fld_ref_no"></span>
                                    </div>


                                    <div class="box-body" style="margin: 0 100px;">

                                        <div class="form-group">

                                            <div class="col-sm-12 addBottom">
                                                Payment Status : <span class="text-success">Success</span>
                                            </div>

                                            <div class="col-sm-12 addBottom">
                                                ITGK Code : <span id="fld_ITGK_Code"></span>
                                            </div>

                                            <div class="col-sm-12 addBottom">
                                                Transaction Reference Number : <span id="fld_transactionID"></span>
                                            </div>

                                           <!-- <div class="col-sm-12 addBottom">
                                                Bank Reference Number: <span id="fld_bankTransactionID"></span>
                                            </div>-->

                                            <div class="col-sm-12 addBottom">
                                                Transaction Date and Time: <span id="fld_updatedOn"></span>
                                            </div>

                                            <div class="col-sm-12 addBottom">
                                                Transaction Amount: <span id="fld_amount"></span>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="division_heading">

                                    </div>


                                    <div class="box-body" style="margin: 0 100px;">

                                        <div class="form-group">

                                            <div class="col-sm-12 addBottom">
                                               <!-- <button type="button" class="btn btn-danger" id="print"><i class="fa fa-print" aria-hidden="true"></i>
                                                    &nbsp;Printable Version</button>

                                                &nbsp;&nbsp;-->

                                                <button type="button" class="btn btn-primary" onclick='downloadGSTInvoice()'><i
                                                            class="fa fa-download"
                                                            aria-hidden="true"></i>&nbsp;&nbsp;
                                                    Download GST Invoice
                                                </button>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="container">
                            <iframe id="testdoc" src="" style="width: 100%;height: 500px;border: none; display: none;"></iframe>
                            </div>  
                        </form>
                    </div>

                </div>
            </div>
            </div>


            </body>
            <?php
            include 'common/message.php';
            include 'footer.php';

            ?>

            <?php
        }


        ?>
        </html>
        <?php

?>
<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>
<script>
    $.ajax({
        type: "post",
        url: "common/cfmodifyITGK.php",
        data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code']?>'),
        success: function (data2) {
            data2 = $.parseJSON(data2);
            $("#fld_ref_no").html(data2[0].fld_ref_no);
            $("#fld_ITGK_Code").html(data2[0].fld_ITGK_Code);
            $("#fld_transactionID").html(data2[0].fld_transactionID);
            /*$("#fld_bankTransactionID").html(data2[0].fld_bankTransactionID);*/
            $("#fld_updatedOn").html(data2[0].fld_updatedOn);
            $("#fld_amount").html(data2[0].fld_amount);
        }
    });

    $("#print").click(function(){
        window.open("printTransactionDetails.php?Code="+'<?php echo $_REQUEST['Code']?>','_blank');
    });

    function downloadGSTInvoice() {
        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=downloadGSTInvoice"+'&fld_ref_no=' + '<?php echo $_REQUEST['Code']?>',
            success: function (data) {
			//alert(data);
                //location.href='downloadFile.php?id='+data;
                window.open(data,"_blank");
                if(data == 'error'){
                $('#response').empty();
                $('#response').append("<p class='error'>Receipt Not Available Right Now.</span></p>");
                }
                else{
                    $("#testdoc").attr('src', data);
                }
                /* setTimeout(function () {
                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=delgeninvoice&values=" + data,
                        success: function (data) {

                        }
                    });
                }, 10000) */
            }
        });
    }
</script>
