<?php
$title = "Search Center Location";
include ('outer_header1.php');
include ('DAL/sendsms.php');
//include ('root_menu.php');


if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Update'</script>";
}
?>

	<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
		 aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
							aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel2">Enter Mobile No.</h4>
				</div>
				<div class="modal-body">
					<div id="success_msg" style="color:green;"></div>
					<div class="row">
						<div class="col-md-12 form-group"> 
							<label for="ddlContact">Mobile No:<span class="star">*</span></label>
							<!--<input id="ddlContact" name="ddlContact" class="form-control" class="bfh-phone" required="required" />-->
							<input class="form-control boxfontsize" id="ddlContact" maxlength="10" name="ddlContact" placeholder="Mobile Number" onkeypress="javascript:return allownumbers(event);"  type="text">
						</div>
						<div id="OTP_divblock" class="col-md-12 form-group" style="display:none;"> 
							<label for="ddlContactOTP">OTP <span style="font-size:12px;">(One time password)</span> :<span class="star">*</span></label>
							<p style="font-size:12px">Please enter OTP sent to your mobile no.</p>
							<input id="ddlContactOTP" name="ddlContactOTP" class="form-control" class="bfh-phoneotp" required="required" />
							<div class="error" id="ddlContactOTP_error"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="modal_dismiss" type="button" class="btn btn-default dismiss_locmodal"
							data-dismiss="modal">Close
					</button>
					<button type="button" class="btn btn-default sendotp_btn">
						Send OTP
					</button>
					<button type="button" class="btn btn-default sendsms" style="display:none;">
						Send
					</button>
				</div>
			</div>
		</div>
	</div>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container learner_nearcenter"> 
        <div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">Search Center Location</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
					<form name="frmcenter_learner" id="frmcenter_learner" class="form-inline" role="form" enctype="multipart/form-data">     

						<div class="container">
							<div class="container">
								<div id="response"></div>

							</div>        
							<div id="errorBox"></div>
						</div>


						<div id="main-content">
							<div class="container">

								<div id="errorBox"></div>
								<!--<div class="col-sm-4 form-group"> 
									<label for="ddlContact">Contact No:<span class="star">*</span></label>
									<input id="ddlContact" name="ddlContact" class="form-control" class="bfh-phone" required="required" />
								</div> -->
								
								<div class="col-sm-4 form-group"> 
									<label for="ddlState">State:<span class="star">*</span></label>
									<select id="ddlState" name="ddlState" class="form-control" >

									</select>
								</div>
								<div class="col-sm-4 form-group" id="ddlDistrict_box"> 
									<label for="ddlDistrict">District:<span class="star">*</span></label>
									<select id="ddlDistrict" name="ddlDistrict" class="form-control" >

									</select>
								</div>

								<div class="col-sm-4 form-group" id="ddlTehsil_box" style="display2:none;"> 
									<label for="ddlTehsil">Tehsil:</label>
									<select id="ddlTehsil" name="ddlTehsil" class="form-control" >

									</select>
								</div>
							</div>    

							<div class="container">
								<div class="col-sm-4 form-group">     
									<label for="area">Area Type:<span class="star">*</span></label> <br/>                               
									<label class="radio-inline"> <input type="radio" id="areaUrban" name="area" value="Urban" /> Urban </label>
									<label class="radio-inline"> <input type="radio" id="areaRural" name="area" value="Rural" /> Rural </label>
									<div id="areaErrorMessageBox"></div>
								</div>

								<div class="container" id="Urban" style="display:none">
									<!--<div class="col-sm-4 form-group"> 
										<label for="edistrict">Municipality Type:</label>
										<select id="ddlMunicipalType" name="ddlMunicipalType" class="form-control" >

										</select>                                         
									</div>-->
									
									 <div class="col-sm-4 form-group"> 
										<label for="edistrict">Municipality Name:</label>
										<select id="ddlMunicipalName" name="ddlMunicipalName" class="form-control" >

										</select>                                            
									</div>

									<div class="col-sm-4 form-group"> 
										<label for="edistrict">Ward No.:</label>
										<select id="ddlWardno" name="ddlWardno" class="form-control" >

										</select>                                       
									</div>
								</div>
								
								<div class="container" id="Rural" style="display:none">
									<div class="col-sm-4 form-group"> 
										<label for="edistrict">Panchayat Samiti/Block:</label>
										<select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >

										</select>
									</div>

									<div class="col-sm-4 form-group"> 
										<label for="edistrict">Gram Panchayat:</label>
										<select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control" >

										</select>
									</div>

									<div class="col-sm-4 form-group"> 
										<label for="edistrict">Village:</label>
										<select id="ddlVillage" name="ddlVillage" class="form-control" >

										</select>
									</div>
								</div>
							</div> 
							<div class="container">
								<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
							</div>
						</div>
					</form>
					<div id="marker_count" style="text-align: right;" ></div>
					<div id="google_map" style="width: 100%;margin-top: 20px;"></div>
					<div class="container map-direction">
						<div class="col-sm-4" id="directions_1">
							<h2></h2>
							<div id="right-panel_1" class="right-panel"></div>
						</div>
						<div class="col-sm-4" id="directions_2">
							<h2></h2>
							<div id="right-panel_2" class="right-panel"></div>
						</div>
						<div class="col-sm-4" id="directions_3">
							<h2></h2>
							<div id="right-panel_3" class="right-panel"></div>
						</div>
					</div>
					
					
				</div>
			
			</div>   
		</div>
	</div>
</div>
<input type="hidden" value="" id="otpval_locmodal">
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>



<!--<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3LDfJa6bQsDLslWSt7MdkTD45zSyw4Ac&libraries=places"></script>
<script type="text/javascript" src="bootcss/js/learner_google_places.js"></script>


<script src="rkcltheme/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="bootcss/css/learner-location.css">


</html>