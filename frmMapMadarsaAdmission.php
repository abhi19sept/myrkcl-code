<?php
$title = "Map Madarsa Learner";
include ('header.php');
include ('root_menu.php');
if (isset($_SESSION)) {
    //echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var Role='" . $_SESSION['User_UserRoll'] . "'</script>";
} else {

    echo "<script>var Mode='Add'</script>";
    echo "<script>var Role='0'</script>";
}
?>		

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Map Madarsa Learner</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmMapMadarsaAdmission" id="frmMapMadarsaAdmission" class="form-inline" role="form" method="post">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                    </div>


                    <div id="detailsdiv">				


                        <div class="container"> 
                            <div class="col-md-6 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">
                                    <option value="0" selected>Select Course</option>
                                    <option value="20">RS-CIT Madarsa</option>
                                </select>
                            </div> 

                            <div class="col-md-6 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">

                                </select>
                            </div> 

                        </div>					
                        <div id="grid" name="grid" style="margin-top:35px;"> </div> 
                    </div>
                    <div class="container" style="margin-top:20px; margin-left:15px;" >						
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none"/> 					 
                    </div>	
            </div>
        </div>   
    </div>
</form>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            if (Role == '7')
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLEventBatch&values=" + selCourse + "",
                    success: function (data) {

                        $("#ddlBatch").html(data);
                    }
                });
            } else
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILL&values=" + selCourse + "",
                    success: function (data) {

                        $("#ddlBatch").html(data);
                    }
                });
            }

        });

        function showAllData(val, val1) {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfMapMadarsaAdmission.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "",
                success: function (data)
                {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $('#btnSubmit').show();

                    //showData();
                }
            });
        }


        $("#ddlBatch").change(function () {
            showAllData(this.value, ddlCourse.value);
        });
        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfMapMadarsaAdmission.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmMapMadarsaAdmission").serialize();

            data = "action=submitclick&" + forminput;


            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    //alert(data);
                    if (data == 0) {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Select Atleast one choice.</span></div>");

                    } else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                        window.setTimeout(function () {
                            Mode = "Add";
                            window.location.href = 'frmMapMadarsaAdmission.php';
                        }, 1000);
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                    }
                }
            });
            return false; // avoid to execute the actual submit of the form.
        });
    });
</script>
</html>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>