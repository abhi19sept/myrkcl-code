<?php
$title="Root Menu Add";
include ('header.php'); 
include ('root_menu.php'); 
//include './rkcltheme/include/menu.php';
		if (isset($_REQUEST['code'])) {
			echo "<script>var RootMenuCode=" . $_REQUEST['code'] . "</script>";
			echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
		} else {
			echo "<script>var RootMenuCode=0</script>";
			echo "<script>var Mode='Add'</script>";
		}
if ($_SESSION['User_Code'] == '1') {		
		?>		
			
        <div class="container"> 			  

            <div class="panel panel-primary" style="margin-top:36px;">

                <div class="panel-heading">Root Menu Master</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmRootMenu" id="frmRootMenu" class="form-inline" role="form" action="">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-md-6 form-group">     
                                <label for="learnercode"> Root Menu:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtRootMenu" id="txtRootMenu" placeholder="Root Menu">
                            </div>                            

                        </div>  

                        <div class="container">

                            <div class="col-sm-8 form-group">     
                                <label for="order">Display Order:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtDisplayOrder" id="txtDisplayOrder" placeholder="Display Order" >
                            </div>                            

                        </div>    

                        <div class="container">

                            <div class="col-sm-10 form-group"> 
                                <label for="status">Status:</label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control">
                                  <option value="">Please Select</option>          
                                </select>
                            </div>                            
                        </div>

                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
							<div id="gird" style="margin-top:35px;"> </div>		
						
                </div>
            </div>   
        </div>
    </form>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
</body>
                <script type="text/javascript">
                    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                    $(document).ready(function () {

                        if (Mode == 'Delete')
                        {
                            if (confirm("Do You Want To Delete This Item ?"))
                            {
                                deleteRecord();
                            }
                        }
                        else if (Mode == 'Edit')
                        {
                            fillForm();
                        }

                        function FillStatus() {
                            $.ajax({
                                type: "post",
                                url: "common/cfStatusMaster.php",
                                data: "action=FILL",
                                success: function (data) {
                                    $("#ddlStatus").html(data);
                                }
                            });
                        }

                        FillStatus();




                        function deleteRecord()
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                            $.ajax({
                                type: "post",
                                url: "common/cfRootMenu.php",
                                data: "action=DELETE&values=" + RootMenuCode + "",
                                success: function (data) {
                                    //alert(data);
                                    if (data == SuccessfullyDelete)
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                        window.setTimeout(function () {
                                            window.location.href = "frmrootmenu.php";
                                        }, 1000);
                                        Mode = "Add";
                                        resetForm("frmRootMenu");
                                    }
                                    else
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                    }
                                    showData();
                                }
                            });
                        }


                        function fillForm()
                        {
							//alert("HII");
                            $.ajax({
                                type: "post",
                                url: "common/cfRootMenu.php",
                                data: "action=EDIT&values=" + RootMenuCode + "",
                                success: function (data) {
									//alert(data);
                                    //alert($.parseJSON(data)[0]['Status']);
                                    data = $.parseJSON(data);
                                    txtRootMenu.value = data[0].RootName;
                                    txtDisplayOrder.value = data[0].Display;
                                    ddlStatus.value = data[0].Status;
                                }
                            });
                        }

                        function showData() {

                            $.ajax({
                                type: "post",
                                url: "common/cfRootMenu.php",
                                data: "action=SHOW",
                                success: function (data) {
                                    //alert(data);
                                    $("#gird").html(data);

                                }
                            });
                        }
							
                        showData();
						

                        $("#btnSubmit").click(function () {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                            var url = "common/cfRootMenu.php"; // the script where you handle the form input.
                            var data;
                            if (Mode == 'Add')
                            {
                                data = "action=ADD&name=" + txtRootMenu.value + "&status=" + ddlStatus.value + "&display=" + txtDisplayOrder.value + ""; // serializes the form's elements.
                            }
                            else
                            {
                                data = "action=UPDATE&code=" + RootMenuCode + "&name=" + txtRootMenu.value + "&status=" + ddlStatus.value + "&display=" + txtDisplayOrder.value + ""; // serializes the form's elements.
                            }
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: data,
                                success: function (data)
                                {
                                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                        window.setTimeout(function () {
                                            window.location.href = "frmrootmenu.php";
                                        }, 1000);

                                        Mode = "Add";
                                        resetForm("frmRootMenu");
                                    }
                                    else
                                    {
                                        $('#response').empty();
                                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                    }
                                    showData();


                                }
                            });

                            return false; // avoid to execute the actual submit of the form.
                        });
                        function resetForm(formid) {
                            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                        }

                    });

                </script>
                </html>
				    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>