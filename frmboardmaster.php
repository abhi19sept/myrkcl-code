<?php
$title="Board Master";
include ('header.php'); 
include ('root_menu.php'); 

   
            if (isset($_REQUEST['code'])) {
                echo "<script>var BoardCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var BoardCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
<div style="min-height:430px !important;max-height:1500px !important;">
        <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Board Master</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmboardmaster" id="frmboardmaster" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="relegionname">Board Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtBoardName" id="txtBoardName" placeholder="Board Name">
                            </div>


                            




                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Board Status:</label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control" >
								  
                                </select>    
                            </div>
							
							
							
							
							
							
						

                        </div>  

                       



                       

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
						
						
                </div>
				<div id="gird"></div>
            </div>   
        </div>

</div>
    </form>




</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
 .star{
	color:red;
}
</style>

<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
            
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfBoardMaster.php",
                    data: "action=DELETE&values=" + BoardCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                            }, 3000);
                            Mode="Add";
                            resetForm("frmboardmaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBoardMaster.php",
                    data: "action=EDIT&values=" + BoardCode + "",
                    success: function (data) {
                       alert(data);
                        //alert($.parseJSON(data)[0]['CountryName']);
                        data = $.parseJSON(data);
                        txtBoardName.value = data[0].BoardName;
                        ddlStatus.value = data[0].Status;
                        
                    }
                });
            }

            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfBoardMaster.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);

                    }
                });
            }

            showData();


            $("#btnSubmit").click(function () {
			if ($("#frmboardmaster").valid())
			{
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfBoardMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtBoardName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + BoardCode + "&name=" + txtBoardyName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmboardmaster.php";
                           }, 1000);

                            Mode="Add";
                            resetForm("frmboardmaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			}
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmboardmastervalidation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>