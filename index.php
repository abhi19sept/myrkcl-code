<?php
include ('outer_header.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if(isset($_COOKIE["useridCook"])){
	unset($_COOKIE);
	}
?>
<!--
<div id="indexpopupimageopen" class="modal">
  <div class="modal-content" style="text-align: center;width: 60%;margin: 15px 0 0 245px;">
    <div class="modal-header">
        <span class="close closeh" style="margin: -10px 5px 0 0;">&times;</span>
    </div>
    <div class="modal-body">
        <img src="upload/posteraward.jpg" /> 
    </div>
  </div>
</div>
-->
<script>
    var modal = document.getElementById('indexpopupimageopen');
    var span = document.getElementsByClassName("closeh")[0];
    modal.style.display = "block";
    span.onclick = function() { 
        modal.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }  
</script>

<style>
body {font-family: "Lato", sans-serif;}

ul.tab {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #5698fa; border-bottom: none;
}

/* Float the list items side by side */
ul.tab li {float: left;}

/* Style the links inside the list items */
ul.tab li a {
    display: inline-block;
    color: #333;
    text-align: center;
    padding: 10px 15px;
    text-decoration: none;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of links on hover */
ul.tab li a:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
ul.tab li a:focus, .active {
    background-color: #f1f1f1; border: 1px solid #333; border-bottom: none;
}

/* Style the tab content */
.tabcontent {
    display: none;
    /*padding: 6px 12px 25px 12px;*/
    border: 1px solid #ccc;
    border-top: none;
}

.boxfontsize{
    font-size: 12px;
}
#responselernaerotp p span{
     font-size: 10px;
}
.marginbuttom{
    margin-bottom: 3px;
}
</style>
<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

<div class="container" style=" margin-top: 27px; padding-bottom: 0px; width:96%;">
    <div class="row" >

        <div class="col-md-4 col-md-4" >
            
        </div>
        <div id="channelpartnerpop" class="col-md-4 col-md-4">
            <div class="panel panel-default learnerlogin">
                        <div class="panel-heading"  style="background:#47505a none repeat scroll 0 0 !important;color:#fff;" > <img src="images/Channel_Partner_login_icon.png"/> 
                            <b>Channel Partner Login</b>

                        </div>

        <div class="tabcontent" style="display: block; font-size:13px;">
           <div class="panel-body" style="min-height:279px;">
                            <form class="form-horizontal" role="form" name="frmlogin" id="frmlogin">
                                <div id="response"> <!--<p class='error'><span></span><span><font color=red> नोट : Login करते समय Location Access को Allow करें |</font></span></p> --> </div>
                                <div class="form-group marginbuttom">
                                    <label for="inputEmail3" class="col-sm-3 control-label boxfontsize">User Name</label>
                                    <div class="col-sm-9">
                                        <input class="form-control boxfontsize" id="txtUserName" maxlength="25" name="txtUserName"  onkeypress="javascript:return validUserName(event);"  placeholder="Username" type="text">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group marginbuttom">
                                    <label for="inputPassword3" class="col-sm-3 control-label boxfontsize">Password</label>
                                    <div class="col-sm-9">
                                        <input class="form-control boxfontsize" id="txtPassword" name="txtPassword" placeholder="Password" type="password">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group marginbuttom">
                                    <label for="inputPassword3" class="col-sm-3 control-label boxfontsize">Enter the code</label>
                                    <div class="col-sm-9">
                                       <img src="captcha.php?rand=<?php echo rand(); ?>" id='captchaimg'>
                                            <input class="input-sm boxfontsize"   id="6_letters_code" name="6_letters_code" type="text"  maxlength="6"   onkeypress="javascript:return validCaptcha(event);"   placeholder="Enter Captcha">
                                            <br>
                                            <small >Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
                                     </div>
                                </div>

                                <div class="form-group marginbuttom">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <label class="option block boxfontsize">
                                            <a href="forgot_password.php">Forgot Password ?</a>            
                                        </label>                                       
                                    </div>
                                </div>
                                <div class="form-group marginbuttom last">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="button" name="btnSubmit"  id="btnSubmit" class="btn btn-success btn-sm">Sign in</button>
                                        <button type="reset" class="btn btn-default btn-sm">Reset</button>

                                    </div>
                                </div>
                                
                                <div class="modal fade" id="previewModal" role="dialog">
                                    <div>
                                        <div class="modal-content" style="margin: 150px auto; width: 433px;" >
                                            <div class="modal-header" style="background-color: #f97f06;">
                                                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button>-->
                                                <h3 class="modal-title text-center">MYRKCL</h3>
                                            </div>
                                            <div class="modal-body" id="rec_type">
                                                <div class="reload"><img src="images/reload_icon.png"></div>
                                                <div class="clicktext" id="userfields">Your MYRKCL password has been expired, Please <a href='forgot_password.php'>Click Here</a> to change your password.</div>
                                            </div>
                                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                                            <div class="closetext" data-dismiss="modal">
                                                CLOSE
                                                <!-- <button class="btn btn-default" data-dismiss="modal">Close</button>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                            </form>
           </div>
        </div>

                    </div>
        </div>
        
        <div id="learnerloginpop" class="col-md-4 col-md-4" style="display:none">
            <div class="panel panel-default learnerlogin">
                <div class="panel-heading"  style="
                     background:#47505a none repeat scroll 0 0 !important;color:#fff;" > <img src="images/Learner_login.png"/> 
                    <b>Learner Login</b>

                </div>
  
                <div class="panel-body" style=" font-size:13px;min-height:280px;">
                    <form class="form-horizontal" role="form" name="frmlearnerlogin" id="frmlearnerlogin">
                        <div id="responselernaer">
                            <p style="padding:0 0 20px 0px"></p>
                        </div>
                        <div id="responselernaerotp"></div>
                        
                        <div class="form-group marginbuttom">
                            <label for="inputPassword3" class="col-sm-3 control-label boxfontsize">Learner Code</label>
                            <div class="col-sm-9">
                                <input class="form-control boxfontsize" style="width: 205px;float: left;margin-right: 10px;" id="txtLearnerCode" name="txtLearnerCode" placeholder="Learner Code" onkeypress="javascript:return allownumbers(event);" type="text">
                                <button type="button" name="btnVerify" onclick="checklearner();"  id="btnVerify" style="padding:6px 10px 7px 10px" class="btn btn-success btn-sm">Verify</button>
                           
                            </div>
                            
                        </div>
                        <div class="form-group marginbuttom">
                             <span id="otpmsg"></span>
                        </div>
                        
                     
                        <div class="form-group marginbuttom">
                            <label for="inputPassword3" class="col-sm-3 control-label boxfontsize">OTP Code</label>
                            <div class="col-sm-9">
                                <input class="form-control boxfontsize" id="txtOTPCode" name="txtOTPCode" placeholder="OTP Code"  onkeypress="javascript:return allownumbers(event);" type="password">
                            </div>
                        </div>
                        
                        
                       			
                       
                        <div class="form-group last marginbuttom" style="padding-top: 7px">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" name="btnLenSubmit"  id="btnLenSubmit" class="btn btn-success btn-sm">Sign in</button>
                                <button type="reset" class="btn btn-default btn-sm">Reset</button>
                              
                            </div>
                        </div>
                    </form>
   </div>
            </div>
        </div>


    </div>
    
    <div class="newsblog">
        <div  class="newsblog_limiter">
            <div style="width: auto; display:inline-block; padding: 5px;">महत्वपूर्ण ख़बरे :</div>
            <div class="newscont">  
                <marquee id='scroll_news' onMouseOver="document.getElementById('scroll_news').stop();" onMouseOut="document.getElementById('scroll_news').start();" style="font-family:Book Antiqua; color: #FFFFFF"> <div id="news"></div>   
                </marquee>

            </div>
        </div>      
    </div>


</div>

<?php include ('./common/message.php');
include ('footer.php');
?>
</div>  
</div>



<script language='JavaScript' type='text/javascript'>
            function refreshCaptcha()
            {
                    var img = document.images['captchaimg'];
                    img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
            }
            function refreshLCaptcha()
            {
                    var img = document.images['captchaLimg'];
                    img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
            }
       
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
            
        function validUserName(e){
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("^[ 0-9A-Za-z_@.-]*$");

            if (key === 8 || key === 0 || key === 32) {
                keychar = "a";
            }
            return reg.test(keychar);
        }
        
        function validCaptcha(e){
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("^[ 0-9a-z]*$");

            if (key === 8 || key === 0 || key === 32) {
                keychar = "a";
            }
            return reg.test(keychar);
        }


            function checklearner(){
     
                if($("#txtLearnerCode").val() == "")
                     {
                    $('#responselernaer').empty();
                    $('#responselernaer').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + "   Please Enter Learner Code." + "</font></span></p>");
                    }
                else{
       data = "action=CheckLCode&txtLearnerCode=" + $("#txtLearnerCode").val(); // serializes the form's elements.
               $.ajax({
                type: "post",
                url: "common/cfLogin.php",
                data: data,
                success: function (data) {
                //alert(data);
                if(data == 'NO')
                        {
                           $('#responselernaer').empty();
                           $('#responselernaer').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + "   Invalid Learner Code." + "</font></span></p>");
                           // resetForm("frmlogin");

			}
                else if(data == 'NOMD')
                        {
                           $('#responselernaer').empty();
                           $('#responselernaer').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + "   No Mobile Number Found In Our Record." + "</font></span></p>");
                           // resetForm("frmlogin");

			}
                else
                {
                    //alert(data);
                    $('#responselernaer').empty();
                    $('#responselernaer').append("<p class='state-success'><span><img src=images/correct.gif width=20px /></span><span><span>OTP Sent To Your Register Mobile Number " + data + ".</span></p>");
                    //$('#responselernaer').hide();
                    //$('#otpmsg').fadeOut(4000);
                    //data = $.parseJSON(data);
                    // console.log(data);
                    //alert(data.EMPLOYEE_NAME);
                    //txtMobileNo.value = data.EMPLOYEE_NAME;
                    //txtMobileNo.value = '989898989898';
                    //txtMobileNo.value = data;
                }
                
                
                
                
                
                }
             });
         }
         
         
            }   
 
 
            function mycreditvalidate() {
                if($("#txtMobileNo").val().length == 10){
               //alert("hello you have enter 10 digit ");
               $('#responselernaer').empty();
               $('#otpmsg').empty();
               $('#responselernaer').append("<p class='state-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Authenticating.....Please wait for a while..</span></p>");
               data = "action=CheckLMob&txtMobileNo=" + $("#txtMobileNo").val(); // serializes the form's elements.
               $.ajax({
                type: "post",
                url: "common/cfLogin.php",
                data: data,
                success: function (data) {
                //alert(data);
                if (data === Success)
                {
                    //alert(data);<span id="errmsg" style=" display: none; ">OPT Sent</span>
                    $('#otpmsg').append("<p class='state-success'><span>OPT Sent To Your Mobile Number.</span></p>");
                    $('#responselernaer').hide();
                    $('#otpmsg').fadeOut(4000);
                    

                }
                else if(data == 'Successfully Inserted')
                        {
                           $('#responselernaer').empty();
                           $('#otpmsg').append("<p class='state-success'><span>OPT Sent Successfully To Your Mobile Number.</span></p>");
                           $('#responselernaer').hide();
                           $('#otpmsg').fadeOut(4000);
                           //$('#otpmsg').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span><font color=red>" + "   OTP Sent Successfully." + "</font></span></p>");
                            //resetForm("frmlogin");
                        }
		else if(data == 'NO')
                        {
                           $('#responselernaer').empty();
                           $('#responselernaer').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + "   Invalid Mobile Number." + "</font></span></p>");
                           // resetForm("frmlogin");

			}
                else
                {
                    $('#responselernaer').empty();
                    $('#otpmsg').append("<p class='error'><span><img src=images/warning.png width=10px height=20px /></span><span><font color=red>" + "   Invalid Mobile Number. Please try again." + "</font></span></p>");
                    //resetForm("frmlogin");
                }

                }
            });
           }
            }
    
</script>

<script>
    $(document).ready(function () {
        
       ////function for the numeric value only//////
        
      $("#loginas").change(function() {
           var selectedValue = this.value;
           if(selectedValue=='CP'){
               $('#channelpartnerpop').show();
               $('#learnerloginpop').hide();
           }else{
               $('#learnerloginpop').show();
               $('#channelpartnerpop').hide();
           }
          });
        
        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfLogin.php",
                data: "action=FILL1",
                success: function (data) {
//alert(data);
                    $("#news").html(data);
                    //populate(data);

                }
            });
        }
        showData();


    });
</script>


<script type="text/javascript">
    var Success = "<?php echo Message::SuccessfullyFetch; ?>";
    
    $("#btnLenSubmit").click(function () {
        //alert('Button Click');
        //alert(txtUserName.value);
        //alert(txtPassword.value);
        $('#responselernaer').empty();

        $('#responselernaer').append("<p class='state-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Authenticating.....</span></p>");
        var url = "./common/cfLogin.php"; // the script where you handle the form input.
        var data;
        var forminput = $("#frmlearnerlogin").serialize();
        if (Mode == 'Add')
        {
            data = "action=CheckLernerLoginFinal&" + forminput; // serializes the form's elements.
        }
        //alert(data)
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
               //alert(data);
                // show response from the php script.
                if (data === Success)
                {
                    //alert(data);
                    //$('#responselernaer').empty();
                    $('#responselernaer').html("<p class='state-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                    window.setTimeout(function ()
                    {
                        SuccessfullyLogin();
                    }, 1000);

                }
                else if(data == 'EMPTYLCODE')
                        {
                           $('#responselernaer').empty();
                           $('#responselernaer').html("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   Please Verify Learner Code First." + "</font></span></p>");
                            //resetForm("frmlearnerlogin");
                        }
                else if(data == 'EMPTYOTP')
                        {
                           $('#responselernaer').empty();
                           $('#responselernaer').html("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   Please Enter OTP." + "</font></span></p>");
                            //resetForm("frmlearnerlogin");
                        }
		else if(data == 'NOMD')
                        { //alert('yes');
                           $('#responselernaer').empty();
                           $('#responselernaer').html("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   No Mobile Number Found With This Learner. Please contact to your SP." + "</font></span></p>");
                            //resetForm("frmlearnerlogin");

			}
                else if(data == 'InvalidOTP')
                        { //alert('yes');
                           $('#responselernaer').empty();
                           $('#responselernaer').html("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   Invalid OTP. Please Try Again." + "</font></span></p>");
                            //resetForm("frmlearnerlogin");

			}
                else if(data == 'NORCD')
                        { //alert('yes');
                           $('#responselernaer').empty();
                           $('#responselernaer').html("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   No Record Found." + "</font></span></p>");
                            //resetForm("frmlearnerlogin");

			}
                else
                {
                    $('#responselernaer').empty();
                    $('#responselernaer').html("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   Invalid Learner Code OR OTP. Please try again." + "</font></span></p>");
                    //resetForm("frmlearnerlogin");
                }

            }

        });

        return false; // avoid to execute the actual submit of the form.
    });
    
    $("#btnSubmitOLD").click(function () {
        //alert('Button Click');
        //alert(txtUserName.value);
        //alert(txtPassword.value);
        $('#response').empty();

        $('#response').append("<p class='state-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Authenticating.....</span></p>");
        var url = "./common/cfLogin.php"; // the script where you handle the form input.
        var data;
        var forminput = $("#frmlogin").serialize();
        if (Mode == 'Add')
        {
            data = "action=CheckLoginNew&" + forminput; // serializes the form's elements.
        }
        //alert(data)
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
               //alert(data);
                // show response from the php script.
                if (data === Success)
                {
                    //alert(data);
                    $('#response').empty();
                    $('#response').append("<p class='state-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                    window.setTimeout(function ()
                    {
                        SuccessfullyLogin();
                    }, 1000);

                }
                else if(data == 'PROBLEM')
                        {
                           $('#response').empty();
                           $('#myModal').modal('toggle');
                           $('#previewModal').modal({show: true});                           
                           $('#userfields').html("Your MYRKCL password has been expired, Please <a href='forgot_password.php?itgk=" +txtUserName.value+"'>Click Here</a> to change your password.");
                           $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "Your password has expired. Please Reset it <a href='forgot_password.php'>here</a>." + "</font></span></p>");
//                             setTimeout(function () {
//                                    window.location.href = 'frmresetinto.php';
//                                }, 3000);
//                          
                        }
                else if(data == 'WRONGDATA')
                        {
                           $('#response').empty();
                           $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "Your have entered wrong password." + "</font></span></p>");
                        }
                else if(data == 'EMPTY')
                        {
                            $('#response').empty();
                           $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   Please Enter User Name and Password." + "</font></span></p>");
                            resetForm("frmlogin");
                        }
		else if(data == 'No')
                        {
                            $('#response').empty();
                           $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   Invalid Captcha. Please try again." + "</font></span></p>");
                            resetForm("frmlogin");

			}
                else
                {
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   Invalid Username or Password. Please try again." + "</font></span></p>");
                    resetForm("frmlogin");
                }

            }

        });

        return false; // avoid to execute the actual submit of the form.
    });
    
    $("#btnSubmit").click(function () {
        //alert('Button Click');
        //alert(txtUserName.value);
        //alert(txtPassword.value);
        $('#response').empty();
        
    if ($("#frmlogin").valid()) {
                        
            $('#response').append("<p class='state-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Authenticating.....</span></p>");
            var url = "./common/cfLogin.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmlogin").serialize();
            if (Mode == 'Add')
            {
                data = "action=CheckLoginNew&" + forminput; // serializes the form's elements.
            }
            //alert(data)
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   //alert(data);
                    // show response from the php script.


                    if (data === Success)
                    {
                         getLocation();
                        //alert(data);
//                        $('#response').empty();
//                        $('#response').append("<p class='state-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
//                        window.setTimeout(function ()
//                        {
//                            SuccessfullyLogin();
//                        }, 1000);

                    }
                    else if(data == "NoRecordFound")
                            {
                               $('#response').empty();
                               $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "Your have entered Wrong/ Invalid  Username or Your Center may be inactive/block." + "</font></span></p>");
                            }
    //                else if(data == "AccountisInactive")
    //                        {
    //                            $('#response').empty();
    //                            $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span><font color=red>" + "   Your Account is Inactive. Please contact your Service Provider for Reactivation." + "</font></span></p>");
    //                            resetForm("frmlogin");
    //                        } 
                    else if(data == 'NORECORDNEWTABLE')
                            {
                               $('#response').empty();
                               $('#myModal').modal('toggle');
                               $('#previewModal').modal({show: true});                           
                               $('#userfields').html("Your MYRKCL password has been expired, Please <a href='forgot_password.php?itgk=" +txtUserName.value+"'>Click Here</a> to change your password.");
                               $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "Your password has expired. Please Reset it <a href='forgot_password.php'>here</a>." + "</font></span></p>");
    //                             setTimeout(function () {
    //                                    window.location.href = 'frmresetinto.php';
    //                                }, 3000);
    //                          
                            }
                    else if(data == 'WRONGPASSWORD2')
                            {
                               $('#response').empty();
                               $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "Your have entered wrong password. Now you have only 2 attempts left." + "</font></span></p>");
                            }
                    else if(data == 'WRONGPASSWORD1')
                            {
                               $('#response').empty();
                               $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "Your have entered wrong password. Now you have only 1 attempt left." + "</font></span></p>");
                            }
                    else if(data == 'WRONGPASSWORD0')
                            {
                               $('#response').empty();
                               $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "Your have entered wrong password. Now 0 attempt left. If you enter worng password again then it will be possible that your center may be blocked." + "</font></span></p>");
                            }
                    else if(data == 'WRONGPASSWORD4')
                            {
                               $('#response').empty();
                               $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "Your have entered wrong password. Your center can be blocked. Please contact in RKCL." + "</font></span></p>");
                               setTimeout(function () {
                                    window.location.href = 'securityvalidate.php?itgk='+txtUserName.value;
                               }, 1000);

                            }
                    else if(data == 'EMPTY')
                            {
                                $('#response').empty();
                               $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   Please Enter User Name and Password." + "</font></span></p>");
                                resetForm("frmlogin");
                            }
                    else if(data == 'No')
                            {
                                $('#response').empty();
                               $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   Invalid Captcha. Please try again." + "</font></span></p>");
                                resetForm("frmlogin");

                            }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red>" + "   Invalid Username or Password. Please try again." + "</font></span></p>");
                        resetForm("frmlogin");
                    }

                }

            });

            return false; // avoid to execute the actual submit of the form.
        }
    });
    
    
    function SuccessfullyLogin()
    {
        window.location.href = 'dashboard.php';
    }
    function resetForm(formid) {
        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
    }

  /**  $(function () {
        $(".demo1").bootstrapNews({
            newsPerPage: 3,
            autoplay: true,
            pauseOnHover: true,
            direction: 'up',
            newsTickerInterval: 4000,
            onToDo: function () {
                //console.log(this);
            }
        });

        $(".demo2").bootstrapNews({
            newsPerPage: 4,
            autoplay: true,
            pauseOnHover: true,
            navigation: false,
            direction: 'down',
            newsTickerInterval: 2500,
            onToDo: function () {
                //console.log(this);
            }
        });

        $("#demo3").bootstrapNews({
            newsPerPage: 3,
            autoplay: false,
            onToDo: function () {
                //console.log(this);
            }
        });
    }); **/



</script>




<!-- Location capture script -->
<script>
function getLocation() {
    var run = 0;
    navigator.geolocation.watchPosition(function(position) {
        var lat = position.coords.latitude; 
        var long = position.coords.longitude;
        run++;
        if(run == 1){
            FillTrackLocation(lat,long); 
        }else{
           
        } 
    },
    function(error) {
        if (error.code == error.PERMISSION_DENIED)
		
  $.ajax({
            type: "post",
            url: "common/cfLogin.php",
            data: "action=DestroySession",
            success: function (data) {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red> Location access allow नहीं करने पर Login नहीं हो पायेगा |</font></span></p>");
            }
        }); 
    });
}
function FillTrackLocation(lat,long) {
    $.ajax({
        type: "post",
        url: "common/cfLogin.php",
        data: "action=TrackLoaction&lat="+lat+"&long="+long+"",
        success: function (data) {
          if(data === "Success"){
                $('#response').empty();
                $('#response').append("<p class='state-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                window.setTimeout(function ()
                {
                    SuccessfullyLogin();
                }, 1000);
          }else{
                $.ajax({
                    type: "post",
                    url: "common/cfLogin.php",
                    data: "action=DestroySession",
                    success: function (data) {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/warning.png width=20px height=20px /></span><span><font color=red> Location access allow नहीं करने पर Login नहीं हो पायेगा |</font></span></p>");
                    }
                });
          }
            
        }
    });
}
</script>
 <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
 <script src="bootcss/js/login_validation.js" type="text/javascript"></script>