<?php
$title = "Admission DD Payment Confirm";
include ('header.php');
include ('root_menu.php');
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
?>
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">DD Payment Confirm</div>
        <div class="panel-body">
            <form name="frmddpaymentconfirm" id="frmddpaymentconfirm" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>


                    <div class="col-sm-4 form-group">     
                        <label for="course">Select Course:</label>
                        <select id="ddlCourse" name="ddlCourse" class="form-control">

                        </select>
                        <input type="hidden" name="coursename" id="coursename"/>
                    </div> 
                </div>

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Batch:</label>
                        <select id="ddlBatch" name="ddlBatch" class="form-control">

                        </select>
                        <input type="hidden" name="batchname" id="batchname"/>


                    </div> 
                </div>

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Center Code:</label>
                        <select id="ddlcentercode" name="ddlcentercode" class="form-control">

                        </select>											
                    </div> 
                </div>
                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select DD No:</label>
                        <select id="ddlddno" name="ddlddno" class="form-control">

                        </select>											
                    </div> 
                </div>

                <div id="menuList" name="menuList" style="margin-top:35px;"> </div> 

                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-warning" value="Confirm DD"/>    
                </div>
        </div>
    </div>   
</div>
</form>


</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLAdmissionSummaryCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FillAdmissionCourse&values=" + ddlCourse.value + "",
                success: function (data) {
                    coursename.value = data;
                    //document.getElementById('coursename').innerHTML = data;

                }
            });

        });

//   OLD     function showCenterCode(val, val1) {
//            $.ajax({
//                type: "post",
//                url: "common/cfddAdmissionPayment.php",
//                data: "action=GETCENTERCODE&batch=" + val + "&course=" + val1 + "",
//                success: function (data) {
//                    //alert(data);
//                    $("#ddlcentercode").html(data);
//                }
//            });
//        }
        function showCenterCode(val, val1) {
            $.ajax({
                type: "post",
                url: "common/cfddAdmissionPayment.php",
                data: "action=GETCENTERCODE&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlcentercode").html(data);
                }
            });
        }

        function showDdModeData() {
            $.ajax({
                type: "post",
                url: "common/cfddAdmissionPayment.php",
                data: "action=GETDDMODEDATA&ddno=" + ddlddno.value + "",
                success: function (data) {
                    //alert(data);
                    $("#menuList").html(data);
                }
            });
        }

        function showDdNo() {
            $.ajax({
                type: "post",
                url: "common/cfddAdmissionPayment.php",
                data: "action=GETDDNO&centercode=" + ddlcentercode.value + "",
                success: function (data) {
                    //alert(data);ddlddno
                    $("#ddlddno").html(data);
                }
            });
        }
        $("#ddlBatch").change(function () {
            showCenterCode(ddlBatch.value, ddlCourse.value);
        });

        $("#ddlBatch").change(function () {
            //var selBatch = $("#ddlBatch").val();
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FillAdmissionBatch&values=" + ddlBatch.value + "",
                success: function (data) {
                    batchname.value = data;
                    // document.getElementById('batchname').innerHTML = data;
                }
            });

        });

        $("#ddlcentercode").change(function () {
            showDdNo(ddlcentercode.value);
            //showDdModeData(ddlcentercode.value);
        });

        $("#ddlddno").change(function () {
            //showCenterCode(ddlBatch.value, ddlCourse.value);
            showDdModeData();
             
        });

        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfddAdmissionPayment.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmddpaymentconfirm").serialize();

            data = "action=ADD&" + forminput; // serializes the form's elements.

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmadmissionddconfirm.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmddpaymentconfirm");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>