<?php
$title="Correction Submission Report To VMOU";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var FunctionCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
?>
<div style="min-height:430px !important;max-height:auto !important;">
	 <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Correction Submission Report To VMOU</div>
                <div class="panel-body">
					<form name="frmcorrectionsubmission" id="frmcorrectionsubmission" class="form-inline" role="form" enctype="multipart/form-data">
						<div class="container">
							<div class="container">						
								<div id="response"></div>
							</div>
							<div id="errorBox"></div>
								<div class="col-sm-4 form-group">     
									<label for="batch"> Select Lot:</label>
									<select id="ddllot" name="ddllot" class="form-control">

									</select>									
								</div>
								
								<div class="col-sm-4 form-group">     
									<label for="batch"> Status:<span class="star">*</span></label>
									 <input type="text" class="form-control" name="txtstatus" id="txtstatus" readonly="true" Value="Process by RKCL"/>				
								</div> 		

								<div class="col-sm-4 form-group">                                  
									<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
								</div>							
						</div>							
                         <div id="gird" style="margin-top:5px;"> </div>                   
                  </div>
            </div>   
        </div>
	</form>
  </div>
  
<div class="modal" id="myModal">
    <div class="modal-content">
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal">×</button>
			<h3 id="heading-tittle" class="modal-title">Heading</h3>
		</div>
		<div class="modal-body">
			<img id="viewimagesrc" class="thumbnail img-responsive" src="images/not-found.png" name="filePhoto3" width="800px" height="880px">
		</div>
		</div>
</div>

 </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>                
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {			
			function FillCorrectionLot() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionSubmissionReport.php",
                    data: "action=FILLCorrectionLot",
                    success: function (data) {
						//alert(data);
                        $("#ddllot").html(data);						 
                    }
                });
            }
            FillCorrectionLot();			
			
			$("#btnSubmit").click(function () {
				showData(ddllot.value);			   
        });

            function showData(val1) {
			  if ($("#frmcorrectionsubmission").valid())
				{
					$('#response').empty();
					$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");              
					$.ajax({
						type: "post",
						url: "common/cfCorrectionSubmissionReport.php",
						data: "action=SHOWDETAILS&lot=" + val1 + "",
						success: function (data) {
							$('#response').empty();
								//alert(data);
							$("#gird").html(data);
							$('#example').DataTable({
							dom: 'Bfrtip',
							buttons: [
								{
           extend: 'pdf',
           footer: true,
           exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,23,24,25,26,27,28]
            }
       },
       {
           extend: 'csv',
           exportOptions: {
               columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,23,24,25,26,27,28]
            }
          
       },
       {
           extend: 'excel',
          exportOptions: {
                columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,23,24,25,26,27,28]
            }
       }  
							]
						});
						}
					});
				}
            return false;
            }
			
				$("#gird").on('click', '.GetPhoto',function(){
					var learnercode = $(this).attr('id');
					var modal = document.getElementById('myModal');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#viewimagesrc").attr('src','');
					}									
					$("#viewimagesrc").attr('src',"upload/correction_photo/" + learnercode);
					$("#heading-tittle").html('Learner Photo');
				});
				
				$("#gird").on('click', '.Getcertificate',function(){
					var learnercode = $(this).attr('id');
					var modal = document.getElementById('myModal');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#viewimagesrc").attr('src','');
					}									
					$("#viewimagesrc").attr('src',"upload/correction_certificate/" + learnercode);
					$("#heading-tittle").html('Correction Certificate');
				});
				
				$("#gird").on('click', '.GetMarksheet',function(){
					var learnercode = $(this).attr('id');
					var modal = document.getElementById('myModal');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#viewimagesrc").attr('src','');
					}									
					$("#viewimagesrc").attr('src',"upload/correction_marksheet/" + learnercode);
					$("#heading-tittle").html('Learner Marksheet');
				});
				
				$("#gird").on('click', '.GetProvisional',function(){
					var learnercode = $(this).attr('id');
					var modal = document.getElementById('myModal');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#viewimagesrc").attr('src','');
					}									
					$("#viewimagesrc").attr('src',"upload/correction_provisional/" + learnercode);
					$("#heading-tittle").html('Correction Provisional');
				});
				
				$("#gird").on('click', '.GetApplication',function(){
					var learnercode = $(this).attr('id');
					var modal = document.getElementById('myModal');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#viewimagesrc").attr('src','');
					}									
					$("#viewimagesrc").attr('src',"upload/correction_application/" + learnercode);
					$("#heading-tittle").html('Correction Application');
				});
				
				$("#gird").on('click', '.GetAffidavit',function(){
					var learnercode = $(this).attr('id');
					var modal = document.getElementById('myModal');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#viewimagesrc").attr('src','');
					}									
					$("#viewimagesrc").attr('src',"upload/correction_affidavit/" + learnercode);
					$("#heading-tittle").html('Correction Affidavit');
				});
	});
</script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcorrectionsubmissionreport_validation.js"></script>
</html>