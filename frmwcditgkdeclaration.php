<?php
$title = "WCD Scheme ITGK Declaration";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '7' || $_SESSION['User_UserRoll'] == '4') {	

?>

<style>
@font-face {
font-family: 'Devlys';
src: url('fonts/Devlys.ttf') format('truetype')}
.KrutiDev_hindi_text {font-family: Devlys !important;font-size: 18px;}
.feedbackquiz{width:30%; float:right;}
.feedbackquiz p{font-family: Devlys !important; font-size: 13px; font-weight: bold; margin: 0px; padding: 0px; width: 35%;float: left;}
.answers li { list-style: upper-alpha; } 
.feedbackquiz label { margin-left: 0.2em; cursor: pointer; font-size: 20px; font-family: Devlys !important;font-size:} 


#categorylist { margin-top: 6px; display: none; } 
table {border: 1px solid #ccc;border-collapse: collapse; margin: 0; padding: 0; width: 100%;  font-family: Myriad Pro SemiExtended;}
.table-striped tbody>tr:nth-child(odd)>td, .table-striped tbody>tr:nth-child(odd)>th {background-color: #fff;}
table caption {font-size: 1.5em; margin: .5em 0 .75em;}
table tr { background: #f8f8f8; border: 1px solid #ddd; padding: .35em;}
table th,
table td { padding: .625em;text-align: left; font-size:  14px; border: 1px solid #ddd;}
table th {font-size: 17px; font-family: Devlys !important;}
@media screen and (max-width: 600px) {
table { border: 0;}
table caption { font-size: 1.3em;}
table thead { border: none; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}
table tr {border-bottom: 3px solid #ddd; display: block; margin-bottom: .625em;}
table td { border-bottom: 1px solid #ddd;display: block; font-size: .8em; text-align: right;}
table td:before { content: attr(data-label); float: left; font-weight: bold; text-transform: uppercase;}
table td:last-child { border-bottom: 0;}
.quizblog{ width: 70%; float: left; border: 1px solid #000;}
.batch{float: left; width: 20%;}
</style> 

<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			  
            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading" style="height:40px">
					<div  class="col-sm-5"> WCD Scheme Declaration Form </div>
					
				</div>
				
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
				
				
					<form name="frmfeedback" id="frmfeedback" class="form-inline" role="form" enctype="multipart/form-data">     
					<div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }">  </select>
                        </div>
						 <div class="col-md-6 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"> </select>
                        </div>
                    </div>    
                    <div class="container" id="view">
						<div class="col-md-4 form-group">     
							<input type="button" name="btnView" id="btnView" class="btn btn-primary" value="View"/>    
						</div>
					</div>   
				<div id="show" style="display:none;">		
		<fieldset style="border: 1px groove #ddd !important;padding: 0 25px 25px 25px;" class="">
			
			<h4 style="text-align:center;">घोषणा पत्र ( DECLARATION )</h4>
			<p style="padding-left:5px; text-align:center;">(ITGK द्वारा महिला बैच जनवरी 2021 के भुगतान प्राप्त करने हेतु आवश्यक घोषणा)</br>
			</p>
		
		<p style="margin-left:15;margin-top: 25px;">	मैं , ज्ञानकेंद्र कोड संख्या  <span id="itgkcode"></span> ज्ञानकेंद्र का नाम <span id="itgkname"></span> का मालिक / अधिकृत संचालक / अधिकृत कोऑर्डिनेटर हूँ | हमारे ITGK को <span id="fcourse"></span> Women Jan 2021 के 
			अंतर्गत कुल <span id="totalseat"></span> <span id="scourse"></span> की सीटें आवंटित की गयी है | मैं यह स्वीकार करता/करती हूँ की – </p>
		<span >	
		<ol>
			<li>	चयनित सभी प्रशिक्षुओं का प्रशिक्षण महिला विभाग की शर्तो के अनुरूप पूर्ण करना मेरा उत्तरदायित्व एवं प्रतिबद्धता है |</li>
			
			<li>	चयनित सभी प्रशिक्षुओं को योजनान्तर्गत व RKCL प्रदत्त सभी सेवाओं को प्रदान करना मेरा उत्तरदायित्व एवं प्रतिबद्धता है |  </li>
			<li>योजनान्तर्गत महिला बैच जनवरी 2021 में चयनित प्रशिक्षुओं के प्रशिक्षण से सम्बंधित प्रथम किश्त (1st Installment) का भुगतान RKCL द्वारा मुझे विभाग से 
											प्राप्त होने पर किया जायेगा परन्तु भविष्य में  शर्तों की अनुपालना में त्रुटी पाए जाने पर यदि महिला विभाग द्वारा प्रदत 
											प्रथम किश्त (1st Installment)
											पर कटौती की जाती है तो उसके वहन की जिम्मेवारी संस्था / मालिक / अधिकृत संचालक / अधिकृत 
											कोऑर्डिनेटर की  अथवा उसके उत्तराधिकारी की होगी |</li>
			 	<li>RKCL/महिला विभाग, राजस्थान द्वारा समय समय पर जारी सभी निर्देशों की पालना करूँगा/करुँगी |</li>
				<li>योजना के सफल संचालन हेतु RKCL तथा महिला विभाग, राजस्थान को पूर्ण सहयोग करूँगा/करुँगी |</li> </span>
			</ol>
			
		<input type="checkbox" id="yes" name="fooby[1][]" style="margin-left:25;" /><span>    उपरोक्त सभी कथन में/हम स्वीकार करते है </span>

		</fieldset>
			
			<br>
			
		
		
		  <div class="container">
		  
                    <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"
						style="float: left;width: 6%;"/>    
                
				</div>
					</form>
				</div>
				
			</div></div>
		</div>
</div>

<?php include ('footer.php'); ?>
<?php include 'common/message.php' ; ?>

<script type="text/javascript">
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
	$(document).ready(function () {
		function FillCourse() {
			$('#response').empty();
            $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfWcdItgkDeclaration.php",
                data: "action=FillCourse",
                success: function (data) {
					if(data=='1'){
						$('#response').empty();
                        $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + "   You have not alloted WCD Scheme Course." + "</span></p>");
					}else{
						$('#response').empty();
						$("#ddlCourse").html(data);
					}
                    
                }
            });
        }
        FillCourse();
				
		$("#btnSubmit").click(function () {
			if ($("#yes").is(":checked")){
				var returnVal = confirm("Are you sure?");
					if(returnVal){
						$('#response').empty();
						$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						                             $(this).prop("disabled", true);
						$.ajax({
							type: "post",
							url: "common/cfWcdItgkDeclaration.php",
							data: "action=AddDetails&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "",
							success: function (data)
							{
								$('#response').empty();
									if(data=="course"){
									$('#response').empty();
									$('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select Course." + "</span></p>");
									}
									else if(data=="batch"){
									$('#response').empty();
									$('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select Batch." + "</span></p>");
									}
								   else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
									{
										$('#response').empty();
										$('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
										window.setTimeout(function () {
											Mode = "Add";
											window.location.href = 'frmwcditgkdeclaration.php';
										}, 3000);
									} else
									{
										$('#response').empty();
										$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
									}
							}
						});
					}
					else{
						
					}
			}else{
				alert("Please read and accepted all the above declaration to proceed.");
			}
		});

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();          
                $.ajax({
                    type: "post",
                    url: "common/cfWcdItgkDeclaration.php",
                    data: "action=FillBatch&values=" + selCourse + "",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });
            
        });
		 
			$("#btnView").click(function () {
				$('#response').empty();
				$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
                    type: "post",
                    url: "common/cfWcdItgkDeclaration.php",
                    data: "action=verifyDetails&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "",
                    success: function (data)
                    {
                        $('#response').empty();
                        if (data == "y")
                        {
							 //$("#show").show();     
							$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   WCD Scheme Declaration Already Submitted." + "</span></p>");
							 $("#show").hide();
                        } else
                        {  
							data = $.parseJSON(data);
							$('#itgkcode').html("<b>"+data[0].ItgkCode+"</b>");
							$('#totalseat').html("<b>"+data[0].seat+"</b>");
							$('#itgkname').html("<b>"+data[0].itgkname+"</b>");
							$("#show").show();
							$("#view").hide();
							if(ddlCourse.value=='3'){
									$('#fcourse').html("RS-CIT");
									$('#scourse').html("RS-CIT");
								}
								else{
									$('#fcourse').html("RS-CFA");
									$('#scourse').html("RS-CFA");
								}
                        }
                    }
                });
			});		
});
</script>
    <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
  
</html>
    
<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>




