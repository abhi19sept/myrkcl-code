<?php
$title = "List Of Centers For Renewal Analysis";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var UserParentID=" . $_SESSION['User_ParentId'] . "</script>";
    echo "<script>var UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var UserParentID=0</script>";
    echo "<script>var UserRole=0</script>";
}
echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";

  //echo strtotime('12-06-2017');
  
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">  	 <div class="panel-heading">
				List Of Centers For Renewal Analysis 
			</div>
            <div class="panel-body">
			
				<div class="panel with-nav-tabs panel-info">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1info" data-toggle="tab"><i class="fa fa-info-circle" aria-hidden="true"></i> UI View</a></li>
                            <li><a href="#tab2info" data-toggle="tab"><i class="fa fa-university" aria-hidden="true"></i> Admin View</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1info">
							<div id="grid" style="margin-top:5px; width:100%;">
								<div id="image" style="display: block;text-align: center;margin: 30px 0;"><img src="images/icons/loading43.gif" /></div>
							</div>
						</div>
                        <div class="tab-pane fade" id="tab2info">
							<div id="grid_admin" style="margin-top:5px; width:100%;"> 
								<div id="image" style="display: block;text-align: center;margin: 30px 0;"><img src="images/icons/loading43.gif" /></div>
							</div>
						</div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function showData() {
            var url = "common/cfCentersForRenewal.php"; // the script where 
           // data = "action=GETDATA&date=" + txtstartdate.value + ""; //
            data = "action=GETDATA"; //
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					if (data == 1){
						$('#grid').empty();
						$('#grid').html('<div id="image" style="display: block;text-align: center;margin: 30px 0;"><img src="images/icons/loading43.gif" /></div>');
					}
					else {
						$("#grid").html(data);
						$('#example').DataTable({
							dom: 'Bfrtip',
							buttons: [
								'copy', 'csv', 'excel', 'pdf', 'print'
							]
						});
					}
                }
            });
        }
		showData();
		
		function showDataAdmin() {
            var url = "common/cfCentersForRenewal.php"; // the script where 
            data = "action=GETADMINDATA"; //
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					if (data == 1){
						$('#response').empty();
						$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Batch." + "</span></div>");
					}
					else {
						$("#grid_admin").html(data);
						$('#example_admin').DataTable({
							dom: 'Bfrtip',
							buttons: [
								'copy', 'csv', 'excel', 'pdf', 'print'
							]
						});
					}
                }
            });
        }
		showDataAdmin();
		
    });

</script>

<style>
    .error {
        color: #D95C5C!important;
    }
	table.dataTable table {
		font-size: 12px;
		width: 100%;
	}
	table
	{
		box-sizing: border-box!important;
	}
	table.dataTable table tbody tr {
		background: transparent;
	}
	.panel-info > .panel-heading{
		padding: 5px 5px 0 5px;
	}
</style>
</html>
