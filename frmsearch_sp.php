<?php
$title = "Search Service Providers";
include ('outer_header1.php');
//include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}

?>
	<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
		 aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
							aria-label="Close"><span aria-hidden="true">×</span>
					</button>
					
					<h4 class="modal-title" id="myModalLabel2">Update Service Provider</h4>
				</div>
				<div class="modal-body">
					<div id="success_msg" style="color:green;margin-bottom: 10px;"></div>
					<div class="row">
					<form id="SP_detail_update" name="SP_detail_update" method="post" class="form" role="form">
						<div class="col-md-12 form-group"> 
							<label for="ddlContactPerson">Contact Person:<span class="star">*</span></label>
							<input type="text" id="ddlContactPerson" name="ddlContactPerson" class="form-control" onkeypress="javascript:return allowChars(event);" required="required" />
						</div>
						<div class="col-md-12 form-group"> 
							<label for="ddlContactP_mobile">Mobile No:<span class="star">*</span></label>
							<input class="form-control boxfontsize" id="ddlContactP_mobile" maxlength="10" name="ddlContactP_mobile" placeholder="Mobile Number" onkeypress="javascript:return allownumbers(event);"  type="text">
						</div>
						<div class="col-md-12 form-group"> 
							<label for="ddlContactP_email">Email ID:<span class="star">*</span></label>
							<input type="email" id="ddlContactP_email" name="ddlContactP_email" class="form-control" required="required" />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button id="modal_dismiss" type="button" class="btn btn-default dismiss_spmodal"
							data-dismiss="modal">Close
					</button>
					<button type="button" class="btn btn-default updatesp_btn">
						Update
					</button>
					</form>
				</div>
			</div>
		</div>
	</div>
<div style="min-height:450px !important;max-height:1500px !important">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading"> Search Service Providers</div>
            <div class="panel-body">
				<form name="frmsearch_sp" id="frmsearch_sp" class="form-inline" role="form" enctype="multipart/form-data">     
					<div class="container">
						<div class="container">
							<div id="response"></div>
						</div>        
						<div id="errorBox"></div>
					</div>
					<div id="main-content">
						<div class="container">

							<div id="errorBox"></div>
							
							
							<div class="col-sm-4 form-group"> 
								<label for="ddlState">State:<span class="star">*</span></label>
								<select id="ddlState" name="ddlState" class="form-control" >

								</select>
							</div>
							<div class="col-sm-4 form-group" id="ddlDistrict_box"> 
								<label for="ddlDistrict">District:<span class="star">*</span></label>
								<select id="ddlDistrict" name="ddlDistrict" class="form-control" >

								</select>
							</div>

							
						</div>    
						<div class="container">
							<input style="display:none;" type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
						</div>
					</div>
				</form>
				<div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                   

                    <div id="grid" name="grid" style="margin-top:35px;"> </div>
				
            </div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
		$("#frmsearch_sp").validate({
			errorPlacement: function(error, element) {
				if (element.attr("name") == "area") {
					error.insertAfter("#areaErrorMessageBox");
				   // error.html($("#areaErrorMessageBox"));
				} else {
					error.insertAfter(element);
				}
			},
			rules: {
				ddlDistrict: { required: true },
			},			
			messages: {	
				ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Select your District</span>' }
			},
		});
		
		$("#btnSubmit").click(function(e){
			if($("#frmsearch_sp").valid()){
				e.preventDefault();
				var selDistrict = $("#ddlDistrict option:selected").val();
				var input_data = "action=SPlist&district="+selDistrict;
					
				$.ajax({
					url: 'common/cfsearch_sp.php',
					type: "post",
					data: input_data,
					success: function (data) {
						$('#response').empty();
						$("#grid").html(data);
						$('#example').DataTable({
							dom: 'Bfrtip',
							buttons: [
								
							],
							  
								
								paging: false

						});
					}
				});
				
			}
		});
    });
	function allowChars(e) {
		var key = window.event ? e.keyCode : e.which;
		var keychar = String.fromCharCode(key);
		var reg = new RegExp('[a-zA-Z ]+$');

		if (key == 8 || key == 0) {
			keychar = 8;
		}

		return reg.test(keychar);
	}
	function allownumbers(e) {
		var key = window.event ? e.keyCode : e.which;
		var keychar = String.fromCharCode(key);
		var reg = new RegExp("[0-9.,]")

		if (key == 8 || key == 0) {
			keychar = 8;
		}

		return reg.test(keychar);
	}

    $(document).ready(function () { 
        FillStates();
		function FillStates() {
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillStates",
                success: function (data) {
                    //alert(data);
                    $('#ddlState').html(data);
					FillDistricts();
                }
            });
        }
		

		function FillDistricts() {
			var state = $('#ddlState').val();
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillDistricts&values="+state,
                success: function (data) {
					//alert(data);
                    $('#ddlDistrict').html(data);
					$("input:radio").attr("checked", false);
                }
            });
        }
		$('#ddlDistrict').change(function(){
			$("#btnSubmit").click();
		});
		$("#ddlState").change(function(){
			$("input:radio").attr("checked", false);
			var state = $(this).val();
			$('#ddlDistrict_box').css('display','block');
			$.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillDistricts&values="+state,
                success: function (data) {
                    $('#ddlDistrict').html(data);
                }
            });
		}); 
		$("#grid").on("click",".update_spmodal",function(){
			$(".update_spmodal").removeClass("active_update_spmodal");
			$(this).addClass("active_update_spmodal");
			var contactp = $(this).attr('contactp');
			$("#ddlContactPerson").val(contactp);
			var contactpmobile = $(this).attr('contactpmobile');
			$("#ddlContactP_mobile").val(contactpmobile);
			var contactpemail = $(this).attr('contactpemail');
			$("#ddlContactP_email").val(contactpemail);
		});
		$(".modal-footer").on('click','#modal_dismiss',function(){
			$("#ddlContactPerson").val("");
			$("#ddlContactP_mobile").val("");
			$("#ddlContactP_email").val("");
			$("#ddlContactOTP_error").html("");
			$("#success_msg").html("");
			$("#ddlContactPerson_error").css('display','none');
		});
		$("#SP_detail_update").validate({
			rules: {
				ddlContactPerson: { required: true },
				ddlContactP_email: {
				  required: true,
				  email: true
				},
				ddlContactP_mobile: {
				  required: true,
				  minlength: 10
    			}
			},			
			messages: {	
				ddlContactPerson: { required: '<span style="color:red; font-size: 12px;">Please Fill Contact Person Name</span>' },
				ddlContactP_email: {
				  required: '<span style="color:red; font-size: 12px;">Select your District</span>',
				  email: '<span style="color:red; font-size: 12px;">Invalid Email ID.</span>'
				},
				ddlContactP_mobile: {
				  required: '<span style="color:red; font-size: 12px;">Please Fill Mobile No.</span>',
				  minlength: '<span style="color:red; font-size: 12px;">Minimum 10 Numbers Required.</span>'
    			}
			},
		});
		$(".modal-footer").on('click','.updatesp_btn',function(){
			if($("#SP_detail_update").valid()){
				var Rsptarget_Code = $(".active_update_spmodal").attr("id");
				Rsptarget_Code = Rsptarget_Code.replace('spupdate_', '');
				var inputdata = $("#SP_detail_update").serialize();
				
				var ddlContactPerson = $("#ddlContactPerson").val();
				if(ddlContactPerson == ''){
					$("#ddlContactPerson_error").css('display','block');
				}
				else{
					$.ajax({
						url: 'common/cfsearch_sp.php',
						type: "post",
						data: "action=update_spcontact&value="+Rsptarget_Code+"&"+inputdata,
						success: function (data) {
							$("#success_msg").html(data);
							window.setTimeout(function () {
								Mode = "Add";
								$("#modal_dismiss").click();
								$("#btnSubmit").click();
								//window.location.href = 'frmsearch_sp.php';
							}, 2000);
						}
					});
				}
			}
		});
		
    });
   

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<style>
.sp_editbtn{
	background: #c20000;
	padding: 3px 7px 2px;
	color: #fff;
	border-radius: 3px;
	text-transform: capitalize;
	font-weight: bold;
	border:none;
}
</style>
</html>