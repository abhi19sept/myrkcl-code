<?php
$title = "Update Center Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Update'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Update Center Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmOrgMaster" id="frmOrgMaster" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>


                    <div id="main-content">
                        <div class="container">

                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Name of Organization/Center:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtName" id="txtName" placeholder="Name of the IT-GK" readonly="">
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">District:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtDistrict" id="txtDistrict" placeholder="District" readonly="">
                                <input type="hidden" class="form-control" name="txtDistrictCode" id="txtDistrictCode" placeholder="District" readonly="">
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="edistrict">Tehsil:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtTehsil" id="txtTehsil" placeholder="Tehsil" readonly="">
                                <input type="hidden" class="form-control" name="txtTehsilCode" id="txtTehsilCode" placeholder="Tehsil" readonly="">
                            </div>
                        </div>    

                        <div class="container">
                            <div class="col-sm-4 form-group">     
                                <label for="area">Area Type:<span class="star">*</span></label> <br/>                               
                                <label class="radio-inline"> <input type="radio" id="areaUrban" name="area" onChange="findselected()"/> Urban </label>
                                <label class="radio-inline"> <input type="radio" id="areaRural" name="area" onChange="findselected1()"/> Rural </label>
                            </div>

                            <div class="container" id="Urban" style="display:none">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Municipality Type:</label>
									<select id="ddlMunicipalType" name="ddlMunicipalType" class="form-control" >

                                    </select>                                         
                                </div>
								
								 <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Municipality Name:</label>
									<select id="ddlMunicipalName" name="ddlMunicipalName" class="form-control" >

                                    </select>                                            
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Ward No.:</label>
									<select id="ddlWardno" name="ddlWardno" class="form-control" >

                                    </select>                                       
                                </div>
							</div>
							
                            <div class="container" id="Rural" style="display:none">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Panchayat Samiti/Block:</label>
                                    <select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >

                                    </select>
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Gram Panchayat:</label>
                                    <select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control" >

                                    </select>
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Village:</label>
                                    <select id="ddlVillage" name="ddlVillage" class="form-control" >

                                    </select>
                                </div>


                            </div>
                        </div> 
                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
                    </div>
                </form>
            </div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        onload();

        function FillPanchayat() {
            var selDistrict = txtDistrictCode.value;
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLPanchayat&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlPanchayat').html(data);
                }
            });
        }

		function FillMunicipalName() {
            var selDistrict = txtDistrictCode.value;
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalName&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlMunicipalName').html(data);
                }
            });
        }
		
		function FillMunicipalType() {
            var selDistrict = txtDistrictCode.value;
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalType",
                success: function (data) {
                    //alert(data);
                    $('#ddlMunicipalType').html(data);
                }
            });
        }
		
		 $("#ddlMunicipalName").change(function () {
            var selMunicipalName = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FILLWardno&values=" + selMunicipalName + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlWardno').html(data);
                }
            });
        });
		
		
        $("#ddlPanchayat").change(function () {
            var selPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlGramPanchayat').html(data);
                }
            });
        });
        
        $("#ddlGramPanchayat").change(function () {
            var selGramPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfVillageMaster.php',
                type: "post",
                data: "action=FILL&values=" + selGramPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlVillage').html(data);
                }
            });
        });

        function findselected() {

            var UrbanDiv = document.getElementById("areaUrban");
            var category = document.getElementById("Urban");

            if (UrbanDiv) {
                Urban.style.display = areaUrban.checked ? "block" : "none";
                Rural.style.display = "none";
            }
            else
            {
                Urban.style.display = "none";
            }
        }
        function findselected1() {


            var RuralDiv = document.getElementById("areaRural");
            var category1 = document.getElementById("Rural");

            if (RuralDiv) {
                Rural.style.display = areaRural.checked ? "block" : "none";
                Urban.style.display = "none";
            }
            else
            {
                Rural.style.display = "none";
            }
        }
        $("#areaUrban").change(function () {
            findselected();
			FillMunicipalName();
			FillMunicipalType();
        });
        $("#areaRural").change(function () {
            findselected1();
            FillPanchayat();
        });

        function onload() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfOrgUpdate.php",
                data: "action=DETAILS",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Acknowledgement Number" + "</span></p>");
                    }
                    else {
                        data = $.parseJSON(data);
                        txtName.value = data[0].orgname;
                        txtDistrict.value = data[0].district;
                        txtTehsil.value = data[0].tehsil;
                        txtDistrictCode.value = data[0].districtcode;
                        txtTehsilCode.value = data[0].tehsilcode;
                    }

                }
            });
        }

        $("#btnSubmit").click(function () {
			if ($("#frmOrgMaster").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfOrgUpdate.php"; // the script where you handle the form input.
                if (document.getElementById('areaUrban').checked) //for radio button
                {
                    var area_type = 'Urban';
                }
                else {
                    area_type = 'Rural';
                }         

                var data;          
                
                    data = "action=UPDATE&areatype=" + area_type + "&wardno=" + ddlWardno.value + "&village=" + ddlVillage.value +
										"&gram=" + ddlGramPanchayat.value + "&panchayat=" + ddlPanchayat.value +
										"&municipalname=" + ddlMunicipalName.value + "&municipaltype=" + ddlMunicipalType.value + ""; // serializes the form's elements.              
				
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmorgupdate.php";
                            }, 2000);

                            Mode = "Add";
                            resetForm("frmorgmaster");
                        }
						
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }

                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmorgregistrationvalidation.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>