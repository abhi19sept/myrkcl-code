<?php
$title="Admission Form";
include ('header.php'); 
include ('root_menu.php'); 
if (isset($_REQUEST['code'])) {
echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
$Mode=$_REQUEST['Mode'];
} else {
echo "<script>var AdmissionCode=0</script>";
echo "<script>var Mode='Add'</script>";
$Mode="Add";
}
if (isset($_REQUEST['ddlCourse'])) {
echo "<script>var CourseCode='" . $_REQUEST['ddlCourse'] . "'</script>";
echo "<script>var BatchCode='" . $_REQUEST['ddlBatch'] . "'</script>";
}
else {
echo "<script>var CourseCode=0</script>";
echo "<script>var BatchCode=0</script>";
}
 ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
        <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">Admission Form </div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmadmission" id="frmadmission" class="form-inline" role="form" enctype="multipart/form-data"> 
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Learner Name:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtlname" id="txtlname" onkeypress="javascript:return allowchar(event);" placeholder="Learner Name">
								<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
								<input type="hidden" class="form-control" maxlength="50" name="txtAdmissionCode" id="txtAdmissionCode"/>
								<select id="ddlBatch" style="display:none;" name="ddlBatch" class="form-control">  </select>
								<input type="hidden" class="form-control" maxlength="50" name="txtCourseFee" id="txtCourseFee"/>
								<input type="hidden" class="form-control" maxlength="50" name="txtInstallMode" id="txtInstallMode"/>
								<input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
								<input type="hidden" class="form-control"  name="txtsign" id="txtsign"/>
								<input type="hidden" class="form-control" maxlength="50" name="txtmode" id="txtmode" value="<?php echo $Mode; ?>" />
                            </div>                            
                        </div>   
                        
                        <div class="container"> 
								
								<div class="col-sm-4 form-group" > 
								  <label for="photo">Attach Photo:</label> </br>
								  <img id="uploadPreview1" src="images/user icon big.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">								  
									<input id="uploadImage1" type="file" name="p1" onchange="PreviewImage(1);"/>									  
								</div>							
						</div>

						<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
                </div>
            </div>   
        </div>
    </form>

</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox
{	color:#F00;	 } 
</style>

<script type="text/javascript"> 
 $('#dob').datepicker({                   
		format: "yyyy-mm-dd"
	});  
	</script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>

<script type="text/javascript">
	function PreviewImage(no) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
		oFReader.onload = function (oFREvent) {
			document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
		};
	};
</script>	
	
	
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
		
        $(document).ready(function () {
			//alert(BatchCode);
			function FillCourseCode() {				
                 $.ajax({
                    type: "post",
                    url: "common/cfCourseMaster.php",
                    data: "action=FILLCourseCode&values=" + CourseName + "",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });
            }
            //FillCourseCode();
			
			function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();
			
			function AdmissionFee()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfAdmission.php",
                    data: "action=Fee&codes=" + BatchCode + "",
                    success: function (data) {                      
                    txtCourseFee.value = data;				
                    }
                });
            }		
            AdmissionFee();			
			
			function InstallMode()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfAdmission.php",
                    data: "action=Install&values=" + BatchCode + "",
                    success: function (data) {
                        txtInstallMode.value = data;					
                    }
                });
            }
            InstallMode();
			
			$( "#txtGPFno" ).prop( "disabled", true );
         
            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
				//alert(1);
                fillForm();
            }

            $("input[type='image']").click(function () {
                $("input[id='my_file']").click();
            });

            function FillDistrict() {
                //alert();
                $.ajax({
                    type: "post",
                    url: "common/cfDistrictMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlDistrict").html(data);
                    }
                });
            }
            FillDistrict();

            function FillQualification() {
                $.ajax({
                    type: "post",
                    url: "common/cfQualificationMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlQualification").html(data);
                    }
                });
            }
            FillQualification();

            function FillLearnerType() {
                $.ajax({
                    type: "post",
                    url: "common/cfLearnerTypeMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlLearnerType").html(data);
                    }
                });
            }
            FillLearnerType();

            function findselected() {
                var selMenu = document.getElementById('ddlLearnerType');
                var txtField = document.getElementById('txtGPFno');
                if (selMenu.value == '3')
                    txtField.disabled = false
                else
                    txtField.disabled = true
            }

            $("#ddlDistrict").change(function () {
                var selDistrict = $(this).val();
                //alert(selregion);
                $.ajax({
                    url: 'common/cfTehsilMaster.php',
                    type: "post",
                    data: "action=FILL&values=" + selDistrict + "",
                    success: function (data) {
                        //alert(data);
                        $('#ddlTehsil').html(data);
                    }
                });
            });

            $("#ddlLearnerType").change(function () {
                findselected();
            });
       
            // statement block start for delete record function
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfAdmission.php",
                    data: "action=DELETE&values=" + AdmissionCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmLearnerProfile.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmLearnerProfile");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }
            // statement block end for delete function statment
          
            function fillForm()
            {
               
				$.ajax({
                    type: "post",
                    url: "common/cfAdmission.php",
                    data: "action=EDIT&values=" + AdmissionCode + "",
                    success: function (data) {
						
                        data = $.parseJSON(data);						
                        txtlname.value = data[0].lname;
                        txtfname.value = data[0].fname;
                        dob.value = data[0].dob;
						txtAdmissionCode.value = data[0].AdmissionCode;
						txtphoto.value= data[0].photo;
						txtsign.value=data[0].sign;
						$("#uploadPreview1").attr('src',"upload/admission_photo/" + data[0].photo);
						$("#uploadPreview2").attr('src',"upload/admission_sign/" + data[0].sign);
                        //p1.value = data[0].photo;
                        //p2.value = data[0].sign; 
					}
                });
            }
           
            $("#btnSubmit").click(function () {				
                if ($("#frmadmission").valid())
                {
					$('#response').empty();
					$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
					var url = "common/cfaddimageupload.php"; // the script where you handle the form input.
								
                var lphoto = $('#txtGenerateId').val();
                var data = new FormData($('#frmadmission')[0]);
                //var data;
                        if (Mode == 'Add')
                {
				
                data = "action=ADD&lname=" + txtlname.value + "&lphoto=" + lphoto + "&lsign=" + lsign + ""; // serializes the form's elements.
                        //alert (data);
				
                }
                else
                {
                data = "action=UPDATE&lname=" + txtlname.value +	"&parent=" + txtfname.value + "&dob=" + dob +	"&lphoto=" + lphoto +	"&lsign=" + lsign + "&Code= " + txtAdmissionCode.value + ""; // serializes the form's elements.
                }
                //alert(data);
                $.ajax({
                type: "POST",
                        url: url,
						data:data,
						mimeType: "multipart/form-data",
						contentType: false,
						cache: false,
						processData: false,
                        success: function (data)
                        {
							//alert(data);
                            if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                            {
                                $('#response').empty();
                                $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                                window.setTimeout(function () {
                                    Mode = "Add";
                                    window.location.href = 'frmselbatch.php';
                                }, 3000);                        
                            }
                            else
                            {
                                $('#response').empty();
                                $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                            }
                        }
					});
                }
                        return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }
            // statement block end for submit click
        });

    </script>
	<script src="scripts/imageupload.js"></script>
	<!--<script src="scripts/signupload.js"></script>-->
    <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmadmission_validation.js" type="text/javascript"></script>	
</html>