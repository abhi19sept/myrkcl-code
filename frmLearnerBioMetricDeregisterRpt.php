<?php
$title = "Learner Deregister Report";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:300px !important;min-height:500px !important">

    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Learner Deregister Report</div>
            <div class="panel-body">

                <form name="frmdisplaysms" id="frmdisplaysms" class="form-inline" role="form" enctype="multipart/form-data" action="">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-3" >
                            <label for="course">Select Report Type:<span class="star">*</span></label>
                            <select id="ddlRpttype" name="ddlRpttype" class="form-control" required="required" oninvalid="setCustomValidity('Please Select')"
                                    onchange="try {
                                        setCustomValidity('')
                                    } catch (e) {
                                    }"> 
                                    <option value="1">IT-GK Wise</option>
                                    <option value="2">Learner Wise</option>

                                     </select>
                        </div>
                        <div class="col-sm-3" >
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select')"
                                    onchange="try {
                                        setCustomValidity('')
                                    } catch (e) {
                                    }"> 


                                     </select>
                        </div>
                        <div class="col-sm-3" >
                            <label for="course">Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" oninvalid="setCustomValidity('Please Select')"
                                    onchange="try {
                                        setCustomValidity('')
                                    } catch (e) {
                                    }"> 


                                     </select>
                        </div>
                         <div class="col-sm-4" >
                        <button type="button" class="btn btn-primary" name="btnDataSync" id="btnDataSync" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing...">Show Data</button>
                         </div>

                        

                    </div>   
                    <div class="container">
                            <div id="grid" style="margin-top:25px; width:94%;"> </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>



<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function showCourse() {
          
            
            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=GetAllCourseRpt",
                success: function (data) {
                    
                    $("#ddlCourse").html(data);

                }
            });
        }

        function showBatch() {
          
            
            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=FILLBatchRpt&values=" + ddlCourse.value + "",
                success: function (data) {
                    
                    $("#ddlBatch").html(data);

                }
            });
        }

        showCourse();

         $("#ddlCourse").change(function () {

             showBatch();
          });

        function showData() {
           $('#response').empty();
            $("#btnDataSync").button('loading');
            $.ajax({
                type: "post",
                url: "common/cfbiomatricenroll.php",
                data: "action=SHOWRPT&cat=" + ddlRpttype.value + "&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "",
                success: function (data) {
                    $('#response').empty();
                    
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    $("#btnDataSync").button('reset');
                }
            });
        }

       
         $("#btnDataSync").click(function () {

             showData();
          });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });
</script>


<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
