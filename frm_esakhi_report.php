<?php
$title = "User Details";
include ('header.php');
include ('root_menu.php');
?>
<style type="text/css">

form > div.container {
   filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
}

    .modal-open .container-fluid, .modal-open .container{
        filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
    }
    #errorBox{
        color:#F00;
    }
    .aslink {
        cursor: pointer;
    }
    .viewbox-container {
        z-index: 1200 !important;
    }
</style>
<link rel="stylesheet" href="css/viewbox.css">
<script src="js/jquery.viewbox.min.js"></script>
<div style="min-height:450px !important;max-height:1500px !important">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">eSakhi Registration Report</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="frmesakhienrollments" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div> 
                        <div id="errorBox"></div>
                    </div>
                    <div class="container">
                        <div class="col-sm-4 form-group">
                            <label for="designation">Select Report Type:<span class="star">*</span></label>
                            <select id="ddleSakhiReport" name="ddleSakhiReport" class="form-control">
                                <option value="eSakhi_Reg">eSakhi Registration Report</option>
                                <option value="eSakhi_Trainee_Reg">eSakhi Trainee Registration Report</option>
                                <option value="ITGK_Trainer_Reg">ITGK Trainer Registration Report</option>
                            </select>
                        </div>
                    </div>
                    <div id="grid" name="grid" style="margin-top:35px;"> </div>
                    </form>
            </div>
        </div>   
    </div>

<div id="eSakhi_Registration_model" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 1300px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">eSakhi Registration List</h4>
            </div>
            <div class="modal-body">
                <div id="grid_esakhi" name="grid_esakhi" style="margin-top:35px;">&nbsp;</div>
            </div>
        </div>
    </div>
</div>

<div id="eSakhi_Picture_model" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 1040;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">eSakhi Training Photos</h4>
            </div>
            <div class="modal-body">
                 <form name="frmpictures" id="frmpictures" action="common/cfIdentityInfo.php" method="post" class="form-inline" enctype="multipart/form-data">
                    <div class="container">
                        <div id="photo_grid" name="photo_grid" style="margin-top:35px;border:1px solid #999; width: 83%; padding: 10px;"><center class="small">No photo found.</center></div>
                    </div>
            </form>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); 
    include 'common/modals.php';
    $mac = explode(" ", exec('getmac'));
?>
<script type="text/javascript">
    $(document).ready(function () {

        $("#grid").on('click', '.aslink', function () {
            var act = this.id.split('-');
            if (act[0] == 'showlist') {
                showeSakhiList(this);
            } else if (act[0] == 'showfiles') {
                showeSakhiPics(this);
            }
        });

        $("#photo_grid").on('click', '#links', function () {
            var vb = $('.image-link').viewbox();
            vb.trigger('viewbox.next');
            vb.trigger('viewbox.prev');
        });

        $("#ddleSakhiReport").change(function () {
            var report_type = this.value;
            if (report_type == 'ITGK_Trainer_Reg') {
                $('#footer').hide();
            } else {
                $('#footer').show();
            }
            getData(report_type);
        });

        getData('eSakhi_Reg');

    });

    function showeSakhiPics(eObj) {
        var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
        var data = "action=eSakhiPhotos&elemId=" + eObj.id;
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
                $('#photo_response').empty();
                $("#photo_grid").html(data);
                $("#eSakhi_Picture_model").modal("show");
            }
        });
    }

    function showeSakhiList(eObj) {
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfIdentityInfo.php",
            data: "action=showlist&elemId=" + eObj.id,
            success: function (data) {
            	$('#response').empty();
                $("#grid_esakhi").html(data);
                var table = $('#example1').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ],
                    scrollY: 900,
                    scrollCollapse: true,
                    paging: true
                });
                table.columns.adjust().draw();
                $('#eSakhi_Registration_model').on('shown.bs.modal', function (e) {
                    $('.list_itc').trigger("click");
                });
                $("#eSakhi_Registration_model").modal("show");
            }
        });
    }

    function getData(report_type) {
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $("#grid").html('');
        var url = "common/cfIdentityInfo.php"; // the script where you handle the form input.
        var data = "action=itgkreport&report_type=" + report_type;
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
                $('#response').empty();
                $("#grid").html(data);
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ],
                    scrollY: 900,
                    scrollCollapse: true,
                    paging: true
                });
            }
        });
    }

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmidentity_validation.js" type="text/javascript"></script>
</html>