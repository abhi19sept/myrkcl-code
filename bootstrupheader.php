<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Blueprint: Horizontal Drop-Down Menu</title>
		<meta name="description" content="Blueprint: Horizontal Drop-Down Menu" />
		<meta name="keywords" content="horizontal menu, microsoft menu, drop-down menu, mega menu, javascript, jquery, simple menu" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link href="ddmenu/ddmenu.css" rel="stylesheet" type="text/css" />
		<script src="ddmenu/ddmenu.js" type="text/javascript"></script>
		<link rel="stylesheet" href="assets/demo.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="assets/header-search.css">
		<link rel="stylesheet" href="assets/footer-distributed-with-address-and-phones.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
	</head>
	<body>
	
	<header >
		 <div class="container">

  <div class="row">
    <div class="col-sm-4">
     <h1><a href="#"><img src="images/logo.png"></a></h1>	
    </div>
    <div class="col-sm-4 vcenter">
     <form class="navbar-form" role="search">
    <div class="input-group add-on">
      <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
      <div class="input-group-btn">
        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
      </div>
    </div>
  </form>
    </div>
    <div class="col-sm-4 vcenter">
	
	<div class="header-user-menu">
			<img src="assets/2.jpg" alt="User Image"/>

			<ul>
				<li><a href="#">Settings</a></li>
				<li><a href="#">Payments</a></li>
				<li><a href="#" class="highlight">Logout</a></li>
			</ul>
		</div>
         
   </div>
      
    </div>
  </div>
</div>
		
	</header>

			<nav id="ddmenu">
    <div class="menu-icon"></div>
    <ul>
	 <li class="no-sub"><a href="#"> <span class="glyphicon glyphicon-home top-heading"></span> </a> </li>
	 
	 <li class="no-sub">
            <a class="top-heading" href="#">Dashboard</a>
        </li>
		
        <li class="full-width">
            <span class="top-heading">Channel Partner</span>
			<i class="caret"></i>           
            <div class="dropdown">
                <div class="dd-inner">
                    <div class="column">
                        <h3>Lorem Ipsum</h3>
                        <a href="#">Dolor sit amet</a>
                        <a href="#">Consectetur elit</a>
                        <a href="#">Etiam massa</a>
                        <a href="#">Suscipit sapien</a>
                        <a href="#">Quis turpis</a>
                        <a href="#">Web Menu Builder</a>
                        <a href="#">Quos torpusior</a>
                        <a href="#">Velit a dapibus</a>
                    </div>
                    <div class="column">
                        <h3>Etiam Massa</h3>
                        <a href="#">Sed interdum</a>
                        <a href="#">Fringilla congue</a>
                        <a href="#">Dolor nisl auctor</a>
                        <a href="#">Quisque dictum</a>
                        <a href="#">Porttitor</a>
                        <a href="#">Tellus ullamcorper</a>
                        <a href="#">Orci quis</a>
                    </div>
                    <div class="column column260 mayHide">
                        <br /><img src="ddmenu/img1.jpg" />
                    </div>
                </div>
            </div>
        </li>
        <li class="no-sub"><a class="top-heading" href="#">Learner</a></li>
        <li class="full-width">
			<span class="top-heading">Examination</span>
			<i class="caret"></i>  
            <div class="dropdown">
                <div class="dd-inner">
                    <div class="column">
                        <h3>Vestibulum Ut</h3>
                        <a href="#">Nunc pharetra</a>
                        <a href="#">Vestibulum ante</a>
                        <a href="#">Nulla id laoreet</a>
                        <a href="#">Elementum blandit</a>
                        <a href="#">Quisque dictum</a>
                        <a href="#">Ullamcorper</a>
                        <a href="#">Duis ut mauris</a>
                        <a href="#">Vel faucibus mollis</a>
                        <a href="#">Quisque tempor</a>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <span class="top-heading">Finance</span>
			<i class="caret"></i>           
            <div class="dropdown offset300">
                <div class="dd-inner">
                    <div class="column">
                        <h3>Pellentesq</h3>
                        <div>
                            <a href="#">Fermentum ut nulla</a>
                            <a href="#">Duis ut mauris</a>
                            <a href="#">Quisque tempor</a>
                        </div>
                        <h3>Volutpat</h3>
                        <div>
                            <a href="#">Quisque dictum</a>
                            <a href="#">Nulla scelerisque</a>
                            <a href="#">hendrerit tincidunt</a>
                        </div>
                    </div>
                    <div class="column">
                        <h3>Suspendiss</h3>
                        <div>
                            <a href="#">Suspendisse potenti</a>
                            <a href="#">Curabitur in mauris</a>
                            <a href="#">Phasellus ultrices</a>
                            <a href="#">Quisque ornare</a>
                            <a href="#">Vestibulum</a>
                            <a href="#">Vitae tempus risus</a>
                            <a href="#">Proin sed magna</a>
                            <a href="#">Etiam aliquet</a>
                        </div>
                    </div>
                    <div class="column column340 mayHide">
                        <br /><img src="ddmenu/img2.jpg" />
                    </div>
                </div>
            </div>
        </li>
        <li class="no-sub">
            <a class="top-heading" href="#">Reports</a>
        </li>
        <li>
            <span class="top-heading">Administration</span>
			<i class="caret"></i>           
            <div class="dropdown right-aligned">
                <div class="dd-inner">
                    <div class="column">
                        <h3>Nam a leo</h3>
                        <a href="#">Vel faucibus leo</a>
                        <a href="#">Duis ut mauris</a>
                        <a href="#">In tempus semper</a>
                        <a href="#">laoreet erat</a>
                        <a href="#">Hendrerit tincidunt</a>
                        <a href="#">Nulla scelerisque</a>
                    </div>
                    <div class="column">
                        <h3>Proin iaculis</h3>
                        <a href="#">In tempus semper</a>
                        <a href="#">Hendrerit tincidunt</a>
                        <a href="#">Duis ut mauris</a>
                        <a href="#">pretium amet</a>
                        <a href="#">Nulla scelerisque</a>
                        <a href="#">Vel faucibus leo</a>
                    </div>
                </div>
            </div>
        </li>
		<li class="no-sub">
            <a class="top-heading" href="#">MyLMS</a>
        </li>
		<li class="no-sub">
            <a class="top-heading" href="#">RKCL News Bulletin</a>
        </li>
    </ul>
</nav>
	</body>
	<script>

	$(document).ready(function() {

		$('.header-search form').on('click', function(e) {

			// If the form (which is turned into the search button) was
			// clicked directly, toggle the visibility of the search box.

			if(e.target == this) {
				$(this).find('input').toggle();
			}

		});
	});

</script>
</html>
