<?php
$title="SMS Module";
include ('header.php'); 
include ('root_menu.php'); 

if (isset($_REQUEST['code'])) {
	echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
	echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
	echo "<script>var Code=0</script>";
	echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_UserRoll'] == '4') {	
            ?>
<div style="min-height:500px !important;max-height:2500px !important">
        <div class="container"> 
			<div class="panel panel-primary" style="margin-top:46px !important;">
				<div class="panel-heading btn-info ">RKCL Messaging System</div>
					<div class="panel-body">
						<form name="frmrmsmsmodule" id="frmrmsmsmodule" class="form-inline"> 
						<input type="hidden" id="txttehsilall"  class="form-control" value="" name="txttehsilall">
						<input type="hidden" id="txttehsil"  class="form-control" value="" name="txttehsil">
						<!--<p style="display:none;" id="txtcountadmin"  class="form-control" />-->
						<p style="display:none;" id="txttotrspadmin"  class="form-control" />
						
						<div class="row">
						
						<a href="#"><div class="boxone col-md-3 col-xs-12 col-sm-3 " style="background-color:#0072BB"><span><img src="images/message.png" /></span><p>No Of Messages </p><p id="tech"></p></div></a>
						<a href="#"><div class="boxtwo col-md-3 col-xs-12 col-sm-3 " style="background-color:#FF4C3B"><span><img src="images/payment-status.png" /></span><p>Payment Status </p><p id="active"></p></div></a>
						<a href="frmlogmessage.php"> <div class="boxthree col-md-3 col-xs-12 col-sm-3 " style="background-color:#FFD034"><span><img src="images/message-remain.png" /></span><p>Messages Sent</p><p id="yogendra"></p></div></a>
						<a href="#"><div class="boxfour col-md-3 col-xs-12 col-sm-3 " style="background-color:#C6C8CA"><span><img src="images/message-sent.png" /></span><p>Messages Remaining</p><p id="sunil"></p></div></a>
						<a href="frmreqsms.php"> <div class="boxthree col-md-3 col-xs-12 col-sm-3 " style="background-color:#FFD034"><span><img src="images/message-remain.png" /></span><p>Request SMS (Thanks)</p><p id="yogendra"></p></div></a>
						</div>
                   
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
							
							<div class="container">
                            <div class="col-sm-4 form-group"> 
                               
                            </div>
							</div>
							
							<div class="container">
                            <div class="col-sm-4 form-group"  id="non-printable"> 
                                <label for="edistrict">Entity Name<font color="red">*</font> 
								:</label>
                               <input type="text" id="txtentity"  class="form-control" value="" name="txtentity">
                            </div>
							
							<div class="col-sm-4 form-group"  > 
                                <label for="edistrict">District Name<font color="red">*</font>
								:</label>
                                <select id="ddlDistrict" name="ddlDistrict" class="form-control" >
								 
                                </select>    
								</div>
								
								
								<div class="col-sm-4" > 
                                <label for="edistrict">Tehsil Name<font color="red">*</font>
								:</label>
								<button type='button' id='multiselecttehsil' > Select all </button>
								<button type='button' id='multiDeselecttehsil' > Deselect all </button>
                                <select id="ddlTehsil" name="ddlTehsil[]" class="form-control" multiple="multiple"  >
								 
                                </select>    
								</div>
							</div>	
							
							
							<div class="container">
							 
							
							<div class="col-sm-6 "  id="non-printable"> 
                                <label for="ddlCenter">Center Code<font color="red">*</font>
								:</label>
								<button type='button' id='multiselectall' > Select all</button>
								<button type='button' id='multiDeselectcenter' > Deselect all </button>
                                <select id="ddlCenter" name="ddlCenter[]" class="form-control" rows="100" cols="100"   multiple="multiple" onchange="updateTextarea()" >
								 
                                </select>
							
								
							</div>	
								
								
                            </div>
							
							
							
							<div class="container">
							<div class="col-sm-4 "  id="learner" style="display:none;"> 
                                <label for="ddlCenter">Learner Code<font color="red">*</font>
								:</label>
								<button type='button' id='MultiselectLearner' > Select all / Unselect All</button>
                                <select id="ddllearner" name="ddllearner[]" class="form-control" rows="100" cols="100"   multiple="multiple" onchange="updateTextarea()" >
								 
                                </select>    
								
								</ul>
                            </div>
							
							</div>
							
							
							<div class="container">
							
                            <div class="col-sm-10 "> 
                                <label for="ename">Mobile NO:<font color="red">*</font></label>
                               <textarea class="form-control" readonly="true" maxlength="5000" rows="4" cols="100" id="txtmobile" name="txtmobile" placeholder="Mobile" ></textarea>
                            </div>
							
							
							<div class="col-sm-1" style='Margin-top:20px;'> 
							 <label for="ename" style='color:red;'> Count:</label>
                                <p style="display:block;color:red; "id="txtcountadmin" />
                            </div>
							</div>	

                            
							<div class="container">
							<div class="col-sm-10">     
                                <label for="address">Message:<font color="red">*</font></label>
                                <textarea  class="form-control" maxlength="160" rows="4" cols="100" id="txtMessage" name="txtMessage" placeholder="Message" ></textarea>

                            </div>
							
							</div>
							
							
							
							<div class="container">
							<div id='CharCountLabel1'  class="col-sm-10" style="color:red;"></div>
							
							</div>


									
					
                        <div class="container">

                            <input type="submit"  name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Send" style='margin-left:10px;margin-top:10px'/>    
                        </div>
						</div>
						</div>
						
						
						
                 </div>
            </div>   
        </div>


    </form>

</div>



</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script language="javascript">
	$$('.multiselectall').each(function(el){
		new MultipleSelect(el);
	});
</script>
 <script type='text/javascript'>

CharacterCount = function(TextArea,FieldToCount){

	var myField = document.getElementById(TextArea);

	var myLabel = document.getElementById(FieldToCount); 

	if(!myField || !myLabel){return false}; // catches errors

	var MaxChars =  myField.maxLengh;

	if(!MaxChars){MaxChars =  myField.getAttribute('maxlength') ; }; 	if(!MaxChars){return false};

	var remainingChars =   MaxChars - myField.value.length

	myLabel.innerHTML = remainingChars+" Characters Remaining of Maximum "+MaxChars

}

 

setInterval(function(){CharacterCount('txtMessage','CharCountLabel1')},55);

</script>
 <script type="text/javascript">
						var SuccessfullySend = "<?php echo Message::SuccessfullySend ?>";
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
						 
						
						function FillMessage() 
						{
							$.ajax({
								type: "post",
								url: "common/cfrmsmsmodule.php",
								data: "action=FILLMSG",
								success: function (data) {
									$("#tech").html(data);
								}
							});
						}
						 FillMessage();
						 
						 
						 
						 
						 function Fillstatus() 
							{
								//alert("hello");
								$.ajax({
									type: "post",
									url: "common/cfrmsmsmodule.php",
									data: "action=FILLSTATUS",
									success: function (data) {
										//alert(data);
										$("#active").html(data);
										
									}
								});
							}
							Fillstatus();

						

						function FillCounting()
						{
								$.ajax(
								{
								type: "post",
								url: "common/cfrmsmsmodule.php",
								data: "action=FILL",
								success: function (data) 
								{
								 //alert(data);
								 data = $.parseJSON(data);
								 //console.log(data);
								 //alert(data.response);
								 document.getElementById("sunil").innerHTML=data.Ramain;
								 document.getElementById("yogendra").innerHTML=data.sent;
								 document.getElementById("tech").innerHTML=data.tot;

								}
								});
						}
						FillCounting();
						
						
						
						function FillEntityName()
						{
							$.ajax(
							{
							type: "post",
							url: "common/cfrmsmsmodule.php",
							data: "action=FILLENTITYNAME",
							success: function (data) 
							{
							 //alert(data);
							 //alert(data);
							 //data = $.parseJSON();
							 //console.log(data);
							 //alert(data.response);
							txtentity.value = data;
							document.getElementById("txtentity").readOnly = true;
							 

							}
							});
						}
						FillEntityName();
						
						
						function FillDistrict() {
						//alert();
						$.ajax({
							type: "post",
							url: "common/cfDistrictMaster.php",
							data: "action=FILL",
							success: function (data) {
								$("#ddlDistrict").html(data);
							}
						});
						}
						FillDistrict();
						
						
						
						$("#ddlDistrict").change(function () {
						$('#response').empty();
					    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						var selDistrict = $(this).val();
						GettehsilforRM(selDistrict);
						document.getElementById("response").innerHTML='';
						});
						
						
						
						
						
						function GettehsilforRM(selDistrict)
						{
							
						//alert(selregion);
						$.ajax({
							url: 'common/cfTehsilMaster.php',
							type: "post",
							data: "action=FILL&values=" + selDistrict + "",
							success: function (data) {
								//alert(data);
								$('#ddlTehsil').html(data);
								$('#ddlCenter').val("");
								document.getElementById("txtmobile").innerHTML='';
								document.getElementById("ddlCenter").innerHTML='';
								
							}
						});
						}
						
						$("#ddlTehsil").click(function () 
						{
						
						var ddlTehsil = $(this).val();
						//alert(ddlTehsil);
						
						$("#txttehsil").val(ddlTehsil);
						//alert(selregion);
						$.ajax({
							url: 'common/cfrmsmsmodule.php',
							type: "post",
							data: "action=FILLCENTER&district=" + ddlDistrict.value + "&tehsil=" + ddlTehsil +  "",
							success: function (data) {
								//alert(data);
								$('#ddlCenter').html(data);
								document.getElementById("txtmobile").innerHTML='';
							}
						});
						});
						
						
						
						$("#ddlTehsil").blur(function () {
						var ddlTehsil = $(this).val();
						$("#txttehsil").val(ddlTehsil);
						//alert(selregion);
						$.ajax({
							url: 'common/cfrmsmsmodule.php',
							type: "post",
							data: "action=FILLCENTER&district=" + ddlDistrict.value + "&tehsil=" + ddlTehsil +  "",
							success: function (data) {
								//alert(data);
								$('#ddlCenter').html(data);
								document.getElementById("txtmobile").innerHTML='';
							}
						});
						});
						
						
						
						
						
						
						
						
						$("#multiselecttehsil").click(function()
						{	
							
						$('#response').empty();
					    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");		
					    $('select#ddlTehsil option').attr("selected","selected");
						
						
						var str_val = "";
						
						$('#ddlTehsil :selected').each(function(i, sel){ 
						
						if($(sel).val() !=0){
							str_val += $(sel).val()+",";
						}
							
								
							
						});
						
						str_val = str_val.substring(0, str_val.length - 1);
						//document.getElementById("txttehsilall").value=str_val;
						//alert(str_val);
						
						$("#txttehsilall").val(str_val);
						
						$.ajax({
							url: 'common/cfrmsmsmodule.php',
							type: "post",
							data: "action=FILLALLTEHSILCENTERS&district=" + ddlDistrict.value +    "&tehsil=" + txttehsilall.value +   "",
							success: function (data) {
								//alert(data);
								$('#ddlCenter').html(data);
								document.getElementById("txtmobile").innerHTML='';
								document.getElementById("response").innerHTML='';
							}
						});
						
						
						});
						
						
						$('#multiDeselecttehsil').click(function() {
						 GettehsilforRM($("#ddlDistrict").val());
						 
						document.getElementById("txtmobile").innerHTML='';
						
						});
						
						
						
						$("#ddlCenter").click(function()
						{
						var ddlCenter1 = $(this).val(); 
						//alert(ddlCenterrsp);
						$.ajax({
							  url: 'common/cfrmsmsmodule.php',
							  type: "post",
							  data: "action=FILLMOBILE&center=" + ddlCenter1 +  "",
							  success: function(data)
							  {
								//alert(data);
									 data = $.parseJSON(data);
									 //alert(data.countmob);
									 document.getElementById("txtcountadmin").innerHTML=data.countall;
									 document.getElementById("txtmobile").innerHTML=data.moball;
								     document.getElementById("txtmobile").readOnly = true;
							  }
							  });
						 });
						 
						 
						 $("#ddlCenter").blur(function()
						{
						var ddlCenter1 = $(this).val(); 
						//alert(ddlCenterrsp);
						$.ajax({
							  url: 'common/cfrmsmsmodule.php',
							  type: "post",
							  data: "action=FILLMOBILE&center=" + ddlCenter1 +  "",
							  success: function(data)
							  {
								//alert(data);
									 data = $.parseJSON(data);
									 //alert(data.countmob);
									 document.getElementById("txtcountadmin").innerHTML=data.countall;
									 document.getElementById("txtmobile").innerHTML=data.moball;
								     document.getElementById("txtmobile").readOnly = true;
							  }
							  });
						 });
							 
							
						$("#multiselectall").click(function()
						{	
							 
							 //alert(txttehsilall.value);
							 
							 var a = $('#txttehsilall').val();
							 var b = $('#txttehsil').val();
							 //alert(a);
							 //alert(b);
							 if(b==0)
							 {
								a=a; 
							 }
							 else
							 {
								a=b; 
							 }
							
							//var ddllearner = $(this).val(); 
							
							$('select#ddlCenter option').attr("selected","selected");
							$.ajax({
								  url: 'common/cfrmsmsmodule.php',
								  type: "post",
								  data: "action=FILLCENTERMOBILE&district=" + ddlDistrict.value + "&tehsil=" + a +   "",
								  success: function(data)
								  {
									//alert(data);
									//alert(data);
									 data = $.parseJSON(data);
									 //alert(data.countmob);
									 document.getElementById("txtcountadmin").innerHTML=data.countmob;
									 document.getElementById("txtmobile").innerHTML=data.mobile;
								     document.getElementById("txtmobile").readOnly = true;
									 document.getElementById("response").innerHTML='';
								  }
								   });
						});
						
						
						
						
						
						$('#multiDeselectcenter').click(function() {
						$('#response').empty();
					    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						$('select#ddlCenter option').removeAttr("selected");
						
						document.getElementById("txtmobile").innerHTML='';
						document.getElementById("response").innerHTML='';
						
						});
						
						
						
						
						$("#btnSubmit").click(function () {
							
							if ($("#frmrmsmsmodule").valid())
							{	
								
								var Remain = document.getElementById('sunil').innerHTML;
								var sent = document.getElementById('yogendra').innerHTML;
								var tot = document.getElementById('txttotrspadmin').innerHTML;
								var tempsel = document.getElementById('txtcountadmin').innerHTML;

								var totcount=Remain+tempsel;
								
								var totalsend = parseInt(tempsel);
								var totalremain = parseInt(Remain);
							    //alert(totalremain);
								//alert(totalsend);
							if(totalsend<=totalremain) 
							{	
							    
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");	
								var url = "common/cfrmsmsmodule.php"; // the script where you handle the form input.
							
								var data;
								//var forminput=$(frmsmsmodule).serialize();
								if (Mode == 'Add')
								{
									data = "action=ADD&txtmobile="+txtmobile.value+"&txtMessage="+txtMessage.value+"";
								}
								else
								{
									
									data = "action=UPDATE&code=" + OrganizationCode +"&" + forminput;
									//data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
								}
								$.ajax({
									type: "POST",
									url: url,
									data: data,
									success: function (data)
									{
										if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
										{
											$('#response').empty();
											$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
											 window.setTimeout(function () {
												window.location.href = "frmrmsmsmodule.php";
											}, 1000);
											Mode = "Add";
											resetForm("frmrmsmsmodule");
										}
										else
										{
											$('#response').empty();
											$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
										}
										showData();


									}
								});
									
									
								}
								else 
								{
									
									$('#response').empty();
								    $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Sorry, You are Not having Sufficient Message Package</span></p>");
								
								}
							}
							return false; // avoid to execute the actual submit of the form.
							});

											
						function resetForm(formid) 
						{
							$(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
						}

					});

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmrmsmsmodule_validation.js"></script>
		
		
		   <script type="text/javascript">
      
    </script>
<style>
.error {
	color: #D95C5C!important;
}
.panel-primary > .panel-heading {
    background-color: #5bc0de !important;
}
.col-md-3 {
    width: 20%;
}
</style>

</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>