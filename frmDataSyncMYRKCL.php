<?php
/*
 * Created by Hitesh Gupta

 */
$title = "Data Transfer";
include ('header.php');
include ('root_menu.php');
ini_set("memory_limit", "-1");
ini_set('max_execution_time', 0);
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '14') { 
?>
<style>
    .usercontrol {
        display: block;

        float: left;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.428571429;
        color: #555555;
        vertical-align: middle;
        background-color: #ffffff;
        background-image: none;
        border: 1px solid #cccccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
        transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
    }
    .btn-outline-secondary {
        color: #6c757d;
        background-color: transparent;
        background-image: none;
        border-color: #6c757d;border-top-left-radius: 0;
        border-bottom-left-radius: 0;border-left: none;
    }
    .btn-outline-secondary:hover {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .W25 {
        width: 20%;
        -webkit-transition: width 25%; /* Safari */
        transition: width 25%;
    }
    .form-inline .has-feedback .form-control-feedback {
        top: -24px;
        left: 220px;
    }
</style>

<div class="container"> 
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">MYRKCL Data Transfer to iLearn</b></div>
        <div class="panel-body">
            <form name="frmsyncScoredatamyrkcl" id="frmsyncScoredatamyrkcl" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div id="responsewebScore"></div>

                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">     
                            <label for="batch"> Select Table Name<span class="star">*</span></label>
                            <select id="ddlIlearntbl" name="ddlIlearntbl" class="form-control"></select>
                        </div> 
                        <div class="col-md-6" id='divbatchallsync' style="display: none;"> 
                            <div style="width: 80%; float: left;">
                                 <label for="sdate">Select Batches:<span class="star">*</span></label>


                            <select id="ddlBatch1" name="ddlBatch1[]" multiple class="form-control" required="required">
                               </select>  
                            </div>
                           

                           
                              <div style=" width: 18%;float: right;margin-top: 25px";>
                                  <button type="button" class="btn btn-primary" id="btnchkadmbatchcnt">Check Count</button>
                              </div>
                            
                        </div>

                    </div>


                    <div class="row">
                        <div id="divsynccountresult" style="margin-top: 20px;"> 
                            <div class="col-md-2 W25"> 
                                <label for="sdate">Total Records:</label>
                                <div class="input-group mb-3">
                                    <input type="text" id="synccountTotRecord" class="usercontrol" readonly="readonly" placeholder="Totat Records" aria-label="Totat Records" aria-describedby="basic-addon2">
                                </div>
                            </div>

                            <div class="col-md-2 W25"> 
                                <label for="sdate">Total P Records:</label>
                                <div class="input-group mb-3">
                                    <input type="text" id="synccountPRecord" class="usercontrol" readonly="readonly" placeholder="Totat P Records" aria-label="Totat P Records" aria-describedby="basic-addon2">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="sdate">Total N Records:</label>

                                <div class="input-group mb-3">
                                    <input type="text"  id="synccountNRecord" class="usercontrol" readonly="readonly" placeholder="Total Status 'N' Records" aria-label="Total Status 'N' Records" aria-describedby="basic-addon2">
                                    <button class="btn btn-outline-secondary" type="button" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Update Status.." id="updstatusTbl">Change Status</button>

                                </div>
                            </div>
                        </div>



                        <div class="form-group has-success has-feedback hidden" id="divsyncupdTbl">
                            <label for="inputSuccess2">Status Change</label>
                            <input type="text" id="syncupdTbl"  class="form-control" readonly="readonly">

                            <span class="glyphicon glyphicon-ok form-control-feedback"></span>
                        </div>



                    </div>


                </div>  
            </form>

            <div id="gird">


                <table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>
                    <thead>
                        <tr>
                            <th width="10%">S No.</th>
                            <th width="40%">From: DB || Table</th>
                            <th width="40%">To: DB || Table</th>
                            <th width="10%">Action</th>
                           <!--  <th width="10%">Rows</th> -->
                        </tr>
                    </thead>
                    <tbody>


                        <tr class='odd gradeX'>
                            <td>1</td>
                            <td>database_rkclcoin_1 || <input readonly="readonly" class="synctbl"></td>
                            <td>rkcl_toc || <input readonly="readonly" class="synctbl"></td>
                            <td><button type="button" class="btn btn-primary btndatasync" id="myrkcltoilearn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> sync...">Sync</button>
                            </td>
                            <!-- <td id="ChangeScoreResuttRow"></td> -->
                        </tr>

                    </tbody>
                </table>

            </div>
        </div> 
    </div> 


    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">MYRKCL Data Transfer <b>tbl_assessementfinalresult</b> from iLearn <b>tbl_assessementfinalresult</b></div>
        <div class="panel-body">
            <form name="frmsyncdatamyrkcl" id="frmsyncdatamyrkcl" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div id="responseweb"></div>

                </div>
                <div class="container">
					<div >
                        <div class="col-md-3"> 
                        <label for="bymonth" class="radio-inline"> 
                        <input type="radio" name="radiobuttonPeriod" value="radio1" title="bymonth" id="bymonth" >By Exam Event
                    </label>

                    <label for="last6month" class="radio-inline">
                        
                            <input type="radio" name="radiobuttonPeriod" value="radio2" title="last6month" id="last6month" >
                             By Course
                    </label>
                            </div>
                        <div class="col-md-3" id="divExamEvent" style="display: none;">     
                            <label for="batch"> Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div>
                        <div class="col-md-3" id="divCourse" style="display: none;">     
                            <label for="batch"> Select Course<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">

                            </select>
                        </div> 
                        <div class="col-md-3"> 
                            <label for="sdate">Select Batches:<span class="star">*</span></label>


                            <select id="ddlBatch" name="ddlBatch[]" multiple class="form-control" required="required">

                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="sdate"></label><br><br>
                            <button type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Counting.." id="chkcount">Check Count</button>
                        </div>

                    </div>

                    <div class="row">
                        <div id="divsynccountresult" style="margin-top: 20px;"> 
                            <div class="col-md-2 W25"> 
                                <label for="sdate">Total Leaner:</label>
                                <div class="input-group mb-3">
                                    <input type="text" id="synccountresult" class="usercontrol" readonly="readonly" placeholder="Totat Learner" aria-label="Totat Learner" aria-describedby="basic-addon2">
                                </div>
                            </div>

                            <div class="col-md-2 W25"> 
                                <label for="sdate">Total P Leaner:</label>
                                <div class="input-group mb-3">
                                    <input type="text" id="synccountpresult" class="usercontrol" readonly="readonly" placeholder="Totat P Learner" aria-label="Totat P Learner" aria-describedby="basic-addon2">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="sdate">Total N Leaner:</label>

                                <div class="input-group mb-3">
                                    <input type="text"  id="synccountnststus" class="usercontrol" readonly="readonly" placeholder="Total Status 'N' Learner" aria-label="Total Status 'N' Learner" aria-describedby="basic-addon2">
                                    <button class="btn btn-outline-secondary" type="button" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Update Status.." id="updstatus">Change Status</button>

                                </div>
                            </div>
                        </div>



                        <div class="form-group has-success has-feedback hidden" id="divsyncupdresult">
                            <label for="inputSuccess2">Status Change</label>
                            <input type="text" id="syncupdresult"  class="form-control" readonly="readonly">

                            <span class="glyphicon glyphicon-ok form-control-feedback"></span>
                        </div>



                    </div>







                </div>  
            </form>
        </div> 





        <div id="gird">
            <table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>
                <thead>
                    <tr>
                        <th width="10%">S No.</th>
                        <th width="40%">From: DB || Table</th>
                        <th width="40%">To: DB || Table</th>
                        <th width="10%">Action</th>
                        <th width="10%">Rows</th>
                    </tr>
                </thead>
                <tbody>

                    <tr class='odd gradeX'>
                        <td>1</td>
                        <td>rkcl_toc || tbl_assessementfinalresult</td>
                        <td>database_rkclcoin_1 || tbl_assessementfinalresult</td>
                        <td ><button class="btn btn-primary btndatasync" id="FinalResult" value="Sync" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> sync..."/>Sync</button>
                        </td>
                        <td id="ChangeResuttRow"></td>
                    </tr>


                </tbody>
            </table>

        </div>
    </div> 





    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">MYRKCL Data Transfer <b>tbl_assessementfinalresult</b> to <b>tbl_learner_score</b></div>
        <div class="panel-body">
            <form name="frmsyncScoredatamyrkcl2" id="frmsyncScoredatamyrkcl2" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div id="responsewebScore"></div>

                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">     
                            <label for="batch"> Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlLearnScoreEvent" name="ddlLearnScoreEvent" class="form-control"></select>
                        </div> 
                        <div class="col-md-4"> 
                            <label for="sdate">Select Batches:<span class="star">*</span></label>
                            <select id="ddlLearnScoreBatch" name="ddlLearnScoreBatch[]" multiple class="form-control" required="required">
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="sdate"></label><br><br>
                            <button type="button" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Counting.." id="chkScorecount">Check Count</button>
                        </div>

                    </div>


                    <div class="row">
                        <div id="divsynccountresult" style="margin-top: 20px;"> 
                            <div class="col-md-2 W25"> 
                                <label for="sdate">Total Leaner:</label>
                                <div class="input-group mb-3">
                                    <input type="text" id="synccountresultScore" class="usercontrol" readonly="readonly" placeholder="Totat Learner" aria-label="Totat Learner" aria-describedby="basic-addon2">
                                </div>
                            </div>

                            <div class="col-md-2 W25"> 
                                <label for="sdate">Total P Leaner:</label>
                                <div class="input-group mb-3">
                                    <input type="text" id="synccountpststusScore" class="usercontrol" readonly="readonly" placeholder="Totat P Learner" aria-label="Totat P Learner" aria-describedby="basic-addon2">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="sdate">Total N Leaner:</label>

                                <div class="input-group mb-3">
                                    <input type="text"  id="synccountnststusScore" class="usercontrol" readonly="readonly" placeholder="Total Status 'N' Learner" aria-label="Total Status 'N' Learner" aria-describedby="basic-addon2">
                                    <button class="btn btn-outline-secondary" type="button" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Update Status.." id="updstatusScore">Change Status</button>

                                </div>
                            </div>
                        </div>



                        <div class="form-group has-success has-feedback hidden" id="divsyncupdresultScore">
                            <label for="inputSuccess2">Status Change</label>
                            <input type="text" id="syncupdresultScore"  class="form-control" readonly="readonly">

                            <span class="glyphicon glyphicon-ok form-control-feedback"></span>
                        </div>



                    </div>


                </div>  
            </form>

            <div id="gird">


                <table id='example' class='table table-striped table-bordered' cellspacing='0' width='100%'>
                    <thead>
                        <tr>
                            <th width="10%">S No.</th>
                            <th width="40%">From: DB || Table</th>
                            <th width="40%">To: DB || Table</th>
                            <th width="10%">Action</th>
                            <th width="10%">Rows</th>
                        </tr>
                    </thead>
                    <tbody>


                        <tr class='odd gradeX'>
                            <td>1</td>
                            <td>database_rkclcoin_1 || tbl_assessementfinalresult</td>
                            <td>database_rkclcoin_1 || tbl_learner_score</td>
                            <td><button type="button" class="btn btn-primary btndatasync" id="assmenttoscore" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> sync...">Sync Temp</button>
                                <label id="ChangeScoreResuttRow1"></label>
                            </td>
                            <td><button type="button" class="btn btn-primary btndatasync2" id="assmenttoscore2" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> sync...">Sync Final</button> <label id="ChangeScoreResuttRow2"></label></td>
                        </tr>

                    </tbody>
                </table>

            </div>
        </div> 
    </div> 




</div>


</form>

<?php include'common/message.php'; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>      

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
<style>


    .selectBox {
        position: relative;
    }

    .selectBox select {
        width: 100%;

    }

    .overSelect {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
    }

    #checkboxes {
        display: none;
        border: 1px #dadada solid;
    }

    #checkboxes label {
        display: block;
    }

    #checkboxes label:hover {
        background-color: #1e90ff;
    }
</style>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
    .star{
        color:red;
    }
    .error {
        color: #D95C5C!important;
    }
</style>

<script type="text/javascript">
// first Step
    $(document).ready(function () {
// page load all iLearn DB tables load 

function load_ilearn_db_tbl(){
        $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=loadilearndbtbl",
                success: function (data) {

                    //console.log(data);
                    if (data === '0') {
                        $("#chkcount").button('reset');
                        $('#responseweb').empty();

                        $('#responseweb').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something Went Wrong</span></p>").show('slow');


                    } else {
                        $('#responseweb').empty();
                        $("#chkcount").button('reset');
                        var obj = JSON.parse(data);
                        $("#ddlIlearntbl").append($('<option>').text("- - - Please Select - - -").attr('value', ''));
                          $.each(obj,function(index,value){

                          
                           $("#ddlIlearntbl").append($('<option>').text(value.tbl_Name).attr('value', value.tbl_Name));
                        
                          });
                // $('#ddlIlearntbl').selectpicker('refresh');
                    }
                }
            });
}
load_ilearn_db_tbl();
// End page load all iLearn DB tables load 
//check record count in selected ilearn table
        $("#ddlIlearntbl").change(function () {

            var selIlearntbl = $(this).val();
            $('.synctbl').val(selIlearntbl);

            if(selIlearntbl == 'tbl_admission'){
                $('#divbatchallsync').show();
                //$('#btnchkadmbatchcnt').show();
                $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=FILLADMBATCH&tblname=" + selIlearntbl + "",
                success: function (data) {
                    $("#ddlBatch1").append(data);
                    sortbatch();
                    $("#ddlBatch1 option[value='0']").remove();
                    multisel();

                    $('#ddlBatch1').multiselect('rebuild');

                    $("#ddlBatch1 option[value='0']").remove();
                }
            });
//for count in admission tble batch wise
            $("#btnchkadmbatchcnt").click(function () {
             var selectednumbers = [];
            $('#ddlBatch1 :selected').each(function(i, selected) {
                        selectednumbers[i] = $(selected).val();
                    });
                        $.ajax({
                            type: "post",
                            url: "common/cfDataSync.php",
                            data: "action=ChkCountIlearnTblAdm&tblname=" + selIlearntbl + "&batches=" + selectednumbers + "",
                            success: function (data) {

                                console.log(data);
                                if (data === '0') {
                                    $("#chkcount").button('reset');
                                    $('#responseweb').empty();

                                    $('#responseweb').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something Went Wrong</span></p>").show('slow');


                                } else {
                                    $('#responseweb').empty();
                                    $("#chkcount").button('reset');
                                    var obj = JSON.parse(data);
                                    //  alert(obj['LearnerCount']);
                                    $("#synccountNRecord").val("Total-N-Count: " + obj['RecordCount']['NsatusCount']);
                                    $("#synccountTotRecord").val("TotalLearner: " + obj['RecordCount']['TotalCount']);
                                    $("#synccountPRecord").val("TotalLearner: " + obj['RecordCount']['PsatusCount']);
                                }
                            }
                        });
            });

            }
            else{
            $('#divbatchallsync').hide();
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=ChkCountIlearn&tblname=" + selIlearntbl + "",
                success: function (data) {

                    console.log(data);
                    if (data === '0') {
                        $("#chkcount").button('reset');
                        $('#responseweb').empty();

                        $('#responseweb').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something Went Wrong</span></p>").show('slow');


                    } else {
                        $('#responseweb').empty();
                        $("#chkcount").button('reset');
                        var obj = JSON.parse(data);
                        //  alert(obj['LearnerCount']);
                        $("#synccountNRecord").val("Total-N-Count: " + obj['RecordCount']['NsatusCount']);
                        $("#synccountTotRecord").val("TotalLearner: " + obj['RecordCount']['TotalCount']);
                        $("#synccountPRecord").val("TotalLearner: " + obj['RecordCount']['PsatusCount']);
                    }
                }
            });
            }
        });

       function multisel1() {
            $('#ddlBatch1').multiselect({
			    ncludeSelectAllOption: true, // add select all option as usual
                nonSelectedText: 'Select Batch',
                includeResetOption: true,
                resetText: "Reset all",
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,

                minHeight: 200,
                maxHeight: '200',
                width: '33%',
                buttonWidth: '100%',
                dropRight: true

            });
        }
multisel1();
//End check record count in selected ilearn table

        // DATA SYNC MYRKCL TO ilearn selected tbl
        $("#myrkcltoilearn").click(function () {
            $('#response').empty();
           $(this).button('loading');

            var selIlearntbl = $(ddlIlearntbl).val();
           
            var selectednumbers2 = [];
            if(selIlearntbl == 'tbl_admission'){

            $('#ddlBatch1 :selected').each(function(i, selected) {
                        selectednumbers2[i] = $(selected).val();
                    });
           
            }


            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=myrkcltoilearnDataSync&tblname=" + ddlIlearntbl.value + "&batches=" + selectednumbers2 + "",
                success: function (data) {
                    var obj = JSON.parse(data);
                    if (obj.status === '200') {
                        $('#myrkcltoilearn').button('reset');
                        $('#ChangeResuttRow').html(obj.success);

                    } else {
                        $('#myrkcltoilearn').button('reset');
                        $('#ChangeResuttRow').html(obj.error);

                    }
                }
            });
        });

  // END DATA SYNC MYRKCL TO ilearn selected tbl

        multisel();
    // PAGE ON LOAD ALL EXAM LOAD BY AJAX
        function FillExamEvent() {
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=FILLEXAMEVENT",
                success: function (data) {
                    //SHOW EXAM EVENT FOR ILEARN TABLE
                    $("#ddlExamEvent").html(data);
                    //SHOW EXAM EVENT FOR RKCL TABLE
                    //Hitesh start
                    $('#ddlLearnScoreEvent').html(data);
                    // Hitesh End
                }
            });
        }
        // CALL PAGE ON LOAD
        FillExamEvent();
		function FillCourses() {
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=FILLCOURSE",
                success: function (data) {
                    //SHOW EXAM EVENT FOR ILEARN TABLE
                    $("#ddlCourse").html(data);
                }
            });
        }
        // CALL PAGE ON LOAD
        FillCourses();
      
        function sortbatch() {

            var x = document.getElementById("ddlBatch");
            var txt = "";
            var i;
            var tmpAry = new Array();
            for (i = 0; i < x.length; i++) {
                tmpAry[i] = new Array();
                tmpAry[i][0] = x.options[i].text;
                tmpAry[i][1] = x.options[i].value;
            }
            tmpAry.sort(function (a, b) {
                return a[1] - b[1];
            });

            tmpAry.reverse();
            while (x.options.length > 0) {
                x.options[0] = null;
            }
            for (var i = 0; i < tmpAry.length; i++) {
                var op = new Option(tmpAry[i][0], tmpAry[i][1]);
                x.options[i] = op;
            }
            //alert(tmpAry);
            return;


        }

        function multisel() {
            $('#ddlBatch').multiselect({
			ncludeSelectAllOption: true, // add select all option as usual
                nonSelectedText: 'Select Batch',
                includeResetOption: true,
                resetText: "Reset all",
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,

                minHeight: 200,
                maxHeight: '200',
                width: '33%',
                buttonWidth: '100%',
                dropRight: true

            });
        }
	 $(document).on('change', '[name="radiobuttonPeriod"]' , function(){
             rpttype = $('[name="radiobuttonPeriod"]:checked').val();
           // alert(val);
                if(rpttype === "radio1"){
                    $("#divExamEvent").show();
                    $("#divCourse").hide();
                }
                else if(rpttype === "radio2"){
                    $("#divExamEvent").hide();
                    $("#divCourse").show();
                }
              }); 
        // ON EXAM SELECT EVENT CALL AND GET ALL BETCH
        $("#ddlExamEvent").change(function () {
            $("#ddlBatch").html('');

            $("#ddlCourse option[value='0']").attr("selected","selected") ;
            var selExamEvent = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=FILLREEXAMBATCH&examevent=" + selExamEvent + "",
                success: function (data) {
                    $("#ddlBatch").append(data);
                    sortbatch();
                    $("#ddlBatch option[value='0']").remove();
                    multisel();

                    $('#ddlBatch').multiselect('rebuild');

                    $("#ddlBatch option[value='0']").remove();
                }
            });

        });
		// ON Course SELECT GET ALL BETCH
        $("#ddlCourse").change(function () {
            $("#ddlBatch").html('');

            $("#ddlExamEvent option[value='0']").attr("selected","selected") ;

            var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=FILLCOURSEBATCH&ccode=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").append(data);
                    sortbatch();
                    $("#ddlBatch option[value='0']").remove();
                    multisel();

                    $('#ddlBatch').multiselect('rebuild');

                    $("#ddlBatch option[value='0']").remove();
                }
            });

        });

        // CHECK ALL DATA AS PER BETCH AND EXAM
        $("#chkcount").click(function () {
            var data = $('#frmsyncdatamyrkcl').serialize();
            $(this).button('loading');
            $('#responseweb').empty();
            // $('#responseweb').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>").show('slow');
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=ExamBetchCountIlearn&batches=" + data + "",
                success: function (data) {

                    console.log(data);
                    if (data === '0') {
                        $("#chkcount").button('reset');
                        $('#responseweb').empty();

                        $('#responseweb').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something Went Wrong</span></p>").show('slow');


                    } else {
                        $('#responseweb').empty();
                        $("#chkcount").button('reset');
                        var obj = JSON.parse(data);
                        //  alert(obj['LearnerCount']);
                        $("#synccountnststus").val("Total-N-Count: " + obj['LearnerCount']['NsatusCount']);
                        $("#synccountresult").val("TotalLearner: " + obj['LearnerCount']['TotalCount']);
                        $("#synccountpresult").val("TotalLearner: " + obj['LearnerCount']['PsatusCount']);
                    }
                }
            });
        });
        // UPDATE STATUS N-Y AND P-Y
        $("#updstatus").click(function () {
            var data = $('#frmsyncdatamyrkcl').serialize();
            $('#responseweb').empty();
            $(this).button('loading');
            //$('#responseweb').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>").show('slow');
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=updstatus&batches=" + data + "",
                success: function (data) {
                    $("#updstatus").button('reset');
                    if (data === '0') {
                        $('#responseweb').empty();

                        $('#responseweb').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something Went Wrong</span></p>").show('slow');


                    } else {
                        $('#responseweb').empty();
                        var obj = JSON.parse(data);
                        $("#divsyncupdresult").removeClass("hidden");
                        if (obj['error']) {
                            $("#syncupdresult").val("RecordUpdate: " + obj['error_msg']);
                        } else {
                            $("#divsyncupdresult").removeClass("hidden");
                            $("#syncupdresult").val("RecordUpdate: " + obj['LearnerUpdated']);
                        }
                    }
                }
            });
        });


        // DATA SYNC ILEAN TO ASSIGNMENT
        $("#FinalResult").click(function () {
            $('#response').empty();
            $(this).button('loading');
			 var data1 = $('#frmsyncdatamyrkcl').serialize();
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                 data: "action=IlearnDataSync&batchcode=" + data1 + "",
                success: function (data) {
                    var obj = JSON.parse(data);
                    if (obj.status === '200') {
                        $('#FinalResult').button('reset');
                        $('#ChangeResuttRow').html(obj.success);

                    } else {
                        $('#FinalResult').button('reset');
                        $('#ChangeResuttRow').html(obj.error);

                    }
                }
            });
        });

    });

    $(document).ready(function () {
        multiselScore();


        // Hitesh Get Betshc by Exam ID
        $("#ddlLearnScoreEvent").change(function () {

            var selExamEvent = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=FILLREEXAMBATCH&examevent=" + selExamEvent + "",
                success: function (data) {
                    $("#ddlLearnScoreBatch").append(data);
                    sortbatchScore();
                    $("#ddlLearnScoreBatch option[value='0']").remove();
                    multiselScore();

                    $('#ddlLearnScoreBatch').multiselect('rebuild');

                    $("#ddlLearnScoreBatch option[value='0']").remove();
                }
            });
        });
        //Start
        function multiselScore() {
            $('#ddlLearnScoreBatch').multiselect({
			includeSelectAllOption: true, // add select all option as usual
                nonSelectedText: 'Select Batch',
                includeResetOption: true,
                resetText: "Reset all",
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,

                minHeight: 200,
                maxHeight: '200',
                width: '33%',
                buttonWidth: '100%',
                dropRight: true

            });
        } // end





        function sortbatchScore() {

            var x = document.getElementById("ddlLearnScoreBatch");
            var txt = "";
            var i;
            var tmpAry = new Array();
            for (i = 0; i < x.length; i++) {
                tmpAry[i] = new Array();
                tmpAry[i][0] = x.options[i].text;
                tmpAry[i][1] = x.options[i].value;
            }
            tmpAry.sort(function (a, b) {
                return a[1] - b[1];
            });

            tmpAry.reverse();
            while (x.options.length > 0) {
                x.options[0] = null;
            }
            for (var i = 0; i < tmpAry.length; i++) {
                var op = new Option(tmpAry[i][0], tmpAry[i][1]);
                x.options[i] = op;
            }
            //alert(tmpAry);
            return;
        }


        $("#chkScorecount").click(function () {
             var forminput = $('#frmsyncScoredatamyrkcl2').serialize();
            $('#responsewebScore').empty();
            $("#chkScorecount").button('loading');
            // $('#responsewebScore').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>").show('slow');
 var data = "action=chkScorecount&" + forminput; // serializes the form's elements.
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: data,
                success: function (data) {
                    $("#chkScorecount").button('reset');
                    if (data === '0') {
                        $('#responsewebScore').empty();
                        $('#responsewebScore').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something Went Wrong</span></p>").show('slow');
                    } else {
                        $('#responsewebScore').empty();
                        var obj = JSON.parse(data);
                        $("#synccountnststusScore").val("Total-N-Count: " + obj['LearnerCount']['NsatusCount']);
                        $("#synccountpststusScore").val("Total-P-Count: " + obj['LearnerCount']['PsatusCount']);
                        $("#synccountresultScore").val("TotalLearner: " + obj['LearnerCount']['TotalCount']);
                        $("#divsyncupdresultScore").show();
                    }
                }
            });
        });

        $("#updstatusScore").click(function () {
            var data = $('#frmsyncScoredatamyrkcl2').serialize();
            $('#responsewebScore').empty();
            $(this).button('loading');

            //$('#responsewebScore').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>").show('slow');
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                //data: "action=updstatusScore&batches=" + data + "",
				data: "action=updstatusScore&"+data,
                success: function (data) {
                    $("#updstatusScore").button('reset');
                    if (data === '0') {
                        $('#responsewebScore').empty();

                        $('#responsewebScore').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something Went Wrong</span></p>").show('slow');
                    } else {
                        $('#responsewebScore').empty();
                        $("#divsyncupdresultScore").removeClass("hidden");
                        var obj = JSON.parse(data);
                        if (obj['error']) {
                            $("#syncupdresultScore").val("RecordUpdate: " + obj['error_msg']);
                        } else {
                            $("#syncupdresultScore").val("RecordUpdate: " + obj['LearnerUpdated']);
                        }
                    }
                }
            });
        });
        $("#updstatusTbl").click(function () {
            //var data = $('#frmsyncScoredatamyrkcl').serialize();
            $('#responsewebScore').empty();
            $(this).button('loading');

            //$('#responsewebScore').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>").show('slow');


            var selIlearntbl = $(ddlIlearntbl).val();
           
            var selectednumbers1 = [];
            if(selIlearntbl == 'tbl_admission'){

            $('#ddlBatch1 :selected').each(function(i, selected) {
                        selectednumbers1[i] = $(selected).val();
                    });
           
            }

            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=updstatusTbl&tblname=" + ddlIlearntbl.value + "&batches=" + selectednumbers1 + "",
                success: function (data) {
                    $("#updstatusTbl").button('reset');
                    if (data === '0') {
                        $('#responsewebScore').empty();

                        $('#responsewebScore').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something Went Wrong</span></p>").show('slow');
                    } else {
                        $('#responsewebScore').empty();
                        $("#divsyncupdTbl").removeClass("hidden");
                        var obj = JSON.parse(data);
                        if (obj['error']) {
                            $("#syncupdTbl").val("RecordUpdate: " + obj['error_msg']);
                        } else {
                            $("#syncupdTbl").val("RecordUpdate: " + obj['LearnerUpdated']);
                        }
                    }
                }
            });
        });


        $("#assmenttoscore").click(function () {
            var data = $('#frmsyncScoredatamyrkcl2').serialize();
            $('#response').empty();
            $(this).button('loading');
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=Rkcl_AssihnDataSync&"+data,
                success: function (data) {
                    // console.log(data);
                   // return false;
                    $('#assmenttoscore').button('reset');
                    var obj = JSON.parse(data);
                    //alert(obj.status);
                    if (obj.status === '200') {
                        
                        $('#ChangeScoreResuttRow1').html(obj.success+' Records Inserted');

                    } else {
                        
                        $('#ChangeScoreResuttRow1').html(obj.error);

                    }
                }
            });
        });
        $("#assmenttoscore2").click(function () {
            //var data = $('#frmsyncScoredatamyrkcl2').serialize();
            $('#response').empty();
            $(this).button('loading');
            $.ajax({
                type: "post",
                url: "common/cfDataSync.php",
                data: "action=Rkcl_AssihnDataSyncFinal",
                success: function (data) {
                    // console.log(data);
                   // return false;
                    $('#assmenttoscore2').button('reset');
                    var obj = JSON.parse(data);
                    alert(obj.status);
                    if (obj.status === '200') {
                        
                        $('#ChangeScoreResuttRow2').html(obj.success+' Records Inserted');

                    } else {
                        
                        $('#ChangeScoreResuttRow2').html(obj.error);

                    }
                }
            });
        });

    });







</script>


</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>