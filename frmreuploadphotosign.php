<?php
$title="Re-Upload Photo Sign";
include ('header.php'); 
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
echo "<script>var Admission_Name=0</script>";
echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
?>

<div style="min-height:430px !important;max-height:auto !important;">
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Photo Sign Re-Process</div>
                <div class="panel-body">
                    <form name="frmprocess" id="frmprocess" class="form-inline" role="form" enctype="multipart/form-data">
						<div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
								
								
									<div class="col-sm-4 form-group">     
										<label for="course">Select Course:<span class="star">*</span></label>
										<select id="ddlCourse" name="ddlCourse" class="form-control">
										
										</select>
										
									</div> 
								</div>
								
								<div class="container">
									<div class="col-sm-4 form-group">     
										<label for="batch"> Select Batch:<span class="star">*</span></label>
										<select id="ddlBatch" name="ddlBatch" class="form-control">
												   
										</select>									
									</div> 
								</div>
							
								<div id="menuList" name="menuList"> </div> 
								
								<div class="container">
									<!--	<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    -->
								</div>
                    </div>
            </div>   
        </div>
    </form>
  </body>  
  </div>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>

<script language="javascript" type="text/javascript">
    function checkPhoto(target) {
                var ext = $('#photo').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("photo").value = '';
            return false;
        }

        if (target.files[0].size > 5000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 5 KB");
            document.getElementById("photo").value = '';
            return false;
        }
        else if (target.files[0].size < 3000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("photo").value = '';
            return false;
        }
        document.getElementById("photo").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkSign(target) {
        var ext = $('#sign').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("sign").value = '';
            return false;
        }

        if (target.files[0].size > 3000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 3 KB");
            document.getElementById("sign").value = '';
            return false;
        }
        else if (target.files[0].size < 1000)
        {
            alert("Image size should be greater than 1 KB");
            document.getElementById("sign").value = '';
            return false;
        }
        document.getElementById("sign").innerHTML = "";
        return true;
    }
</script>
		
        <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {			
			
			
			function FillCourse() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfphotosign.php",
                    data: "action=FILLCourseName",
                    success: function (data) {
						//alert(data);
                        $("#ddlCourse").html(data);
                    }
                });
            }
            FillCourse();
			
			
			$("#ddlCourse").change(function(){
				var selCourse = $(this).val(); 
				//alert(selCourse);
				 $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLPhotoSignReuploadEventBatch&values=" + selCourse + "",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });				
           });		   
		    
            function showAllData(val,val1) {
              //alert(val);
			  //alert(val1);
                $.ajax({
                    type: "post",
                    url: "common/cfphotosign.php",
                    data: "action=SHOWREJECTED&batch="+ val +"&course="+ val1 +"",
                    success: function (data) {
						//alert(data);
                        $("#menuList").html(data);
						$('#example').DataTable();						
                    }
                });
            }
		
			 $("#ddlBatch").change(function(){
                showAllData(this.value,ddlCourse.value);
            });            
        });
    </script>
    </body>    
</html>