<?php
$title = "Update Intake";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var SystemCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var Type='" . $_REQUEST['type'] . "'</script>";
    echo "<script>var Status='" . $_REQUEST['status'] . "'</script>";
} else {
    echo "<script>var SystemCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Update Intake</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmupdateintake" id="frmupdateintake" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">  
                                <option value="">Select Course:</option>
                                <option value="1">RS-CIT</option>
                            </select>

                        </div> 
                        <div class="col-md-6 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control"> </select>
                        </div>

                    </div>  
                    <div class="container">

                    </div>

                    <div id="grid" style="margin-top: 35px;">
                    </div>
                    <div class="container" style="margin-top: 35px;">
                        <div class="col-md-12"> 							
                            <label for="tnc">Intake Generation Rule :</label></br>
                            <label for="tnc">1 Server System + 1 Client System = 24 Intake Capacity.</label></br> 
                            <label for="tnc">1 Extra Client System = 24 Intake Capacity.</label></br>                         
                        </div>
                        <br>
                        <div class="container" id="updateintake" style="display:none">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Update Intake"/>    
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include 'common/message.php'; ?>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode === 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        } else if (Mode === 'Edit')
        {
            fillForm();
        }

        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        // FillCourse();

        $("#ddlCourse").change(function () {
            $("#grid").hide();
            var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLEventBatch&values=" + selCourse + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlBatch").html(data);

                }
            });
        });

        $("#ddlBatch").change(function () {
            showData();
            updateintake.style.display = "block";
        });
        function showData() {
            //alert("HII");
            $.ajax({
                type: "post",
                url: "common/cfUpdateIntake.php",
                data: "action=SHOW",
                success: function (data) {
                    $("#grid").html(data);
                    $("#grid").show();
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                    //alert(data);
                }
            });
        }




        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfUpdateIntake.php",
                data: "action=DELETE&values=" + SystemCode + "&type=" + Type + "&status=" + Status + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmupdateintake.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmupdateintake");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }

        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfUpdateIntake.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmupdateintake").serialize();
            //alert(forminput);			

            if (Mode == 'Add')
            {
                data = "action=ADD&" + forminput; // serializes the form's elements.
            } else
            {
                //data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmupdateintake.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmupdateintake");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });



        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>