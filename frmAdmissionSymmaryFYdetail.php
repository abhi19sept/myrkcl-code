<?php
$title = "Admission Summary";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['course'])) {
    $fyy = '"'. $_REQUEST["fy"] . '"';
    echo "<script>var coursecode=" . $_REQUEST['course'] . "</script>";
    echo "<script>var fyear=" . $fyy . "</script>";
   
} else {
    echo "<script>var coursecode=0</script>";
    echo "<script>var fyear=0</script>";

}
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Admission Count Financial Year Wise</div>
            <div class="panel-body">

                <form name="frmAdmissionSymmaryFYdetail" id="frmAdmissionSymmaryFYdetail" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
//alert("hello");

        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span>Report Loading in.....</span><span><img src=images/sec-loader.gif width=30px /></span></p>");
            var url = "common/cfAdmissionSummaryFY.php"; // the script where you handle the form input.

            var data;
           //alert ("data");
            data = "action=GETAdmissionSymmaryFYdetail&fy=" + fyear + "&cc=" + coursecode + ""; //
//alert (data);
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                   
                    $('#response').empty();

                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }
showData() ;

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>

<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
