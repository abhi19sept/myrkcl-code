<?php
$title = "RS-CIT Women Feedback Form";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '19') {	

?>

<style>
@font-face {
font-family: 'Devlys';
src: url('fonts/Devlys.ttf') format('truetype')}
.KrutiDev_hindi_text {font-family: Devlys !important;font-size: 18px;}
.feedbackquiz{width:30%; float:right;}
.feedbackquiz p{font-family: Devlys !important; font-size: 13px; font-weight: bold; margin: 0px; padding: 0px; width: 35%;float: left;}
.answers li { list-style: upper-alpha; } 
.feedbackquiz label { margin-left: 0.2em; cursor: pointer; font-size: 20px; font-family: Devlys !important;font-size:} 


#categorylist { margin-top: 6px; display: none; } 
table {border: 1px solid #ccc;border-collapse: collapse; margin: 0; padding: 0; width: 100%;  font-family: Myriad Pro SemiExtended;}
.table-striped tbody>tr:nth-child(odd)>td, .table-striped tbody>tr:nth-child(odd)>th {background-color: #fff;}
table caption {font-size: 1.5em; margin: .5em 0 .75em;}
table tr { background: #f8f8f8; border: 1px solid #ddd; padding: .35em;}
table th,
table td { padding: .625em;text-align: left; font-size:  14px; border: 1px solid #ddd;}
table th {font-size: 17px; font-family: Devlys !important;}
@media screen and (max-width: 600px) {
table { border: 0;}
table caption { font-size: 1.3em;}
table thead { border: none; clip: rect(0 0 0 0); height: 1px; margin: -1px; overflow: hidden; padding: 0; position: absolute; width: 1px;}
table tr {border-bottom: 3px solid #ddd; display: block; margin-bottom: .625em;}
table td { border-bottom: 1px solid #ddd;display: block; font-size: .8em; text-align: right;}
table td:before { content: attr(data-label); float: left; font-weight: bold; text-transform: uppercase;}
table td:last-child { border-bottom: 0;}
.quizblog{ width: 70%; float: left; border: 1px solid #000;}
.batch{float: left; width: 20%;}
</style> 

<div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
			  
            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading" style="height:40px">
					<div  class="col-sm-5"> RS-CIT Women Feedback Form </div>
					<div class="col-sm-7 feedbackquiz"><p>प्र्रशिक्षण बैच</p>
					<div class="batch" id="txtbatchname"> </div>
					</div>
				</div>
				
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
					<form name="frmfeedback1" id="frmfeedback1" class="form-inline" role="form" enctype="multipart/form-data" style="display:none;">     

                        <div class="container">
                            <div class="container">
                                

                            </div>        
							<div id="errorBox">
								</div>
							
								<div class="col-sm-4 form-group"> 
									<label for="ename">Learner Name:</label>
										<input type="text" readonly="true" class="form-control text-uppercase" value="" maxlength="50" 
											name="firstname" id="firstname" placeholder="Learner Name">    
								</div>
								
								<div class="col-sm-4 form-group"> 
									<label for="ename">Learner Code:</label>
										<input type="text" readonly="true" class="form-control text-uppercase" value="" maxlength="50" 
											name="lcode" id="lcode" placeholder="Learner Code">
										<input type="hidden" name="bcode" id="bcode">
								</div>
								
								<div class="col-sm-4 form-group"> 
									<label for="ename">IT-GK Name:</label>
										<input type="text" readonly="true" class="form-control text-uppercase" value="" maxlength="50" 
											name="itgkname" id="itgkname" placeholder="IT-GK Name">    
								</div>
								
								<div class="col-sm-4 form-group"> 
									<label for="ename">IT-GK Code:</label>
										<input type="text" readonly="true" class="form-control text-uppercase" value="" maxlength="50" 
											name="itgkcode" id="itgkcode" placeholder="IT-GK Code">    
								</div>
						</div>
					
		<fieldset style="border: 1px groove #ddd !important;" class="">
			<table class="table-striped">
                 <tbody>
                    <tr>
                     <th scope="col">Ø-la- </th>
                    <td scope="col" colspan="5" style="text-align: center;"><b>Theoretical</b> </td>
                  </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">1 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">प्रशिक्षण पाठ्यक्रमानुसार पूर्ण किया गया  </p></td>
                     <td scope="col"><input type="radio" name="qt1" value="पूरा" id="qt1a" style='margin-right:10px;'><label for="q1a"> पूरा </label></td>
                     <td scope="col"><input type="radio" name="qt1" value="लगभग पूरा" id="qt1b" style='margin-right:10px;'><label for="q1b"> लगभग पूरा </label></td>
                     <td scope="col"><input type="radio" name="qt1" value="कम" id="qt1c" style='margin-right:10px;'><label for="q1c"> कम </label></td>
                     <td scope="col"><input type="radio" name="qt1" value="बहुत कम" id="qt1d" style='margin-right:10px;'><label for="q1d"> बहुत कम </label></td>
                    </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">2 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">अतिरिक्त संबंधित जानकारी प्रदान की गई    </p></td>
                     <td scope="col"><input type="radio" name="qt2" value="अत्यधिक" id="qt2a" style='margin-right:10px;'><label for="q1a">अत्यधिक</label></td>
                     <td scope="col"><input type="radio" name="qt2" value="उचित" id="qt2b" style='margin-right:10px;'><label for="q1b">उचित</label></td>
                     <td scope="col"><input type="radio" name="qt2" value="कम" id="qt2c" style='margin-right:10px;'><label for="q1c">कम</label></td>
                     <td scope="col"><input type="radio" name="qt2" value="बहुत कम" id="qt2d" style='margin-right:10px;'><label for="q1d">बहुत कम</label></td>
                    </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">3 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">मुख्य विषयों को दिया गया समय</p></td>
                     <td scope="col"><input type="radio" name="qt3" value="अत्यधिक" id="qt3a" style='margin-right:10px;'><label for="q1a">अत्यधिक</label></td>
                     <td scope="col"><input type="radio" name="qt3" value="उचित" id="qt3b" style='margin-right:10px;'><label for="q1b">उचित</label></td>
                     <td scope="col"><input type="radio" name="qt3" value="कम" id="qt3c" style='margin-right:10px;'><label for="q1c">कम</label></td>
                     <td scope="col"><input type="radio" name="qt3" value="बहुत कम" id="qt3d" style='margin-right:10px;'><label for="q1d">बहुत कम</label></td>
                    </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">4 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">समयबद्धता व नियमित्ता</p></td>
                     <td scope="col"><input type="radio" name="qt4" value="सदैव" id="qt4a" style='margin-right:10px;'><label for="q1a">सदैव</label></td>
                     <td scope="col"><input type="radio" name="qt4" value="अधिकतर" id="qt4b" style='margin-right:10px;'><label for="q1b">अधिकतर</label></td>
                     <td scope="col"><input type="radio" name="qt4" value="कभी-कभी" id="qt4c" style='margin-right:10px;'><label for="q1c">कभी-कभी</label></td>
                     <td scope="col"><input type="radio" name="qt4" value="कभी नहीं" id="qt4d" style='margin-right:10px;'><label for="q1d">कभी नहीं</label></td>
                    </tr>
                 </tbody>
			</table>
		</fieldset>
						  
			<br>
			
		<fieldset style="border: 1px groove #ddd !important;" class="">
			<table class="table-striped">
                 <tbody>
                    <tr>
                     <th scope="col">Ø-la- </th>
                    <td scope="col" colspan="5" style="text-align: center;"><b>Practical session</b> </td>
                  </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">1 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">समस्त आवश्यक विषयों पर प्रायोगिक सत्र  </p></td>
                     <td scope="col"><input type="radio" name="qps1" value="समस्त विषयों पर" id="qps1a" style='margin-right:10px;'><label for="q1a">समस्त विषयों पर</label></td>
                     <td scope="col"><input type="radio" name="qps1" value="मुख्य विषयों पर" id="qps1b" style='margin-right:10px;'><label for="q1b">मुख्य विषयों पर</label></td>
                     <td scope="col"><input type="radio" name="qps1" value="कुछ विषयों पर" id="qps1c" style='margin-right:10px;'><label for="q1c">कुछ विषयों पर</label></td>
                     <td scope="col"><input type="radio" name="qps1" value="कभी नहीं" id="qps1d" style='margin-right:10px;'><label for="q1d">कभी नहीं</label></td>
                    </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">2 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">प्रायोगिक सत्र के दौरान कम्प्यूटर/संबंधित संसाधन सही कार्य कर रहे थे</p></td>
                     <td scope="col"><input type="radio" name="qps2" value="सदैव" id="qps2a" style='margin-right:10px;'><label for="q1a">सदैव</label></td>
                     <td scope="col"><input type="radio" name="qps2" value="अधिकतर" id="qps2b" style='margin-right:10px;'><label for="q1b">अधिकतर</label></td>
                     <td scope="col"><input type="radio" name="qps2" value="कभी-कभी" id="qps2c" style='margin-right:10px;'><label for="q1c">कभी-कभी</label></td>
                     <td scope="col"><input type="radio" name="qps2" value="कभी नहीं" id="qps2d" style='margin-right:10px;'><label for="q1d">कभी नहीं</label></td>
                    </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">3 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">कठनाईया दूर की गई  </p></td>
                     <td scope="col"><input type="radio" name="qps3" value="सदैव" id="qps3a" style='margin-right:10px;'><label for="q1a">सदैव</label></td>
                     <td scope="col"><input type="radio" name="qps3" value="अधिकतर" id="qps3b" style='margin-right:10px;'><label for="q1b">अधिकतर</label></td>
                     <td scope="col"><input type="radio" name="qps3" value="कभी-कभी" id="qps3c" style='margin-right:10px;'><label for="q1c">कभी-कभी</label></td>
                     <td scope="col"><input type="radio" name="qps3" value="नहीं" id="qps3d" style='margin-right:10px;'><label for="q1d">नहीं</label></td>
                    </tr>
                 </tbody>
                
			</table>
		</fieldset>
			
			<br>
			
		<fieldset style="border: 1px groove #ddd !important;" class="">
				<table class="table-striped">
                 <tbody>
                    <tr>
                     <th scope="col">Ø-la- </th>
                    <td scope="col" colspan="5" style="text-align: center;"><b>Faculty Feedback</b> </td>
                  </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">1 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">पढ़ाने/समझाने की क्षमता</p></td>
                     <td scope="col"><input type="radio" name="qff1" value="बहुत अच्छा" id="qff1a" style='margin-right:10px;'><label for="q1a">बहुत अच्छा</label></td>
                     <td scope="col"><input type="radio" name="qff1" value="अच्छा" id="qff1b" style='margin-right:10px;'><label for="q1b">अच्छा</label></td>
                     <td scope="col"><input type="radio" name="qff1" value="संतोषप्रद" id="qff1c" style='margin-right:10px;'><label for="q1c">संतोषप्रद</label></td>
                     <td scope="col"><input type="radio" name="qff1" value="सामान्य" id="qff1d" style='margin-right:10px;'><label for="q1d">सामान्य</label></td>
                    </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">2 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">विषय का ज्ञान</p></td>
                     <td scope="col"><input type="radio" name="qff2" value="बहुत अच्छा" id="qff2a" style='margin-right:10px;'><label for="q1a">बहुत अच्छा</label></td>
                     <td scope="col"><input type="radio" name="qff2" value="अच्छा" id="qff2b" style='margin-right:10px;'><label for="q1b">अच्छा</label></td>
                     <td scope="col"><input type="radio" name="qff2" value="संतोषप्रद" id="qff2c" style='margin-right:10px;'><label for="q1c">संतोषप्रद</label></td>
                     <td scope="col"><input type="radio" name="qff2" value="सामान्य" id="qff2d" style='margin-right:10px;'><label for="q1d">सामान्य</label></td>
                    </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">3 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">कठनाईया दूर की गई </p></td>
                     <td scope="col"><input type="radio" name="qff3" value="सदैव" id="qff3a" style='margin-right:10px;'><label for="q1a">सदैव</label></td>
                     <td scope="col"><input type="radio" name="qff3" value="अधिकतर" id="qff3b" style='margin-right:10px;'><label for="q1b">अधिकतर</label></td>
                     <td scope="col"><input type="radio" name="qff3" value="कभी-कभी" id="qff3c" style='margin-right:10px;'><label for="q1c">कभी-कभी</label></td>
                     <td scope="col"><input type="radio" name="qff3" value="कभी नहीं" id="qff3d" style='margin-right:10px;'><label for="q1d">कभी नहीं</label></td>
                    </tr>
                 </tbody>
                
       </table>
		</fieldset>
			
			<br>
			
		<fieldset style="border: 1px groove #ddd !important;" class="">
				<table class="table-striped">
                 <tbody>
                    <tr>
                     <th scope="col">Ø-la- </th>
                    <td scope="col" colspan="5" style="text-align: center;"><b>Others</b> </td>
                  </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">1 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">आवश्यकतानुसार प्रोजेक्टर व अन्य उपकरणों का उपयोग</p></td>
                     <td scope="col"><input type="radio" name="qo1" value="सदैव" id="qo1a" style='margin-right:10px;'><label for="q1a">सदैव</label></td>
                     <td scope="col"><input type="radio" name="qo1" value="अधिकतर" id="qo1b" style='margin-right:10px;'><label for="q1b">अधिकतर</label></td>
                     <td scope="col"><input type="radio" name="qo1" value="कभी-कभी" id="qo1c" style='margin-right:10px;'><label for="q1c">कभी-कभी</label></td>
                     <td scope="col"><input type="radio" name="qo1" value="कभी नहीं" id="qo1d" style='margin-right:10px;'><label for="q1d">कभी नहीं</label></td>
                    </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">2 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">चाही गई भाषा में पाठ्य सामग्री उपलब्ध कराई गई</p></td>
                     <td scope="col" colspan="2"><input type="radio" name="qo2" value="हा" id="qo2a" style='margin-right:10px;'><label for="q1a">हा</label></td>
                     <td scope="col" colspan="2"><input type="radio" name="qo2" value="नहीं" id="qo2b" style='margin-right:10px;'><label for="q1b">नहीं</label></td>
                    </tr>
                 </tbody>
            
                
       </table>
		</fieldset>
			
			<br>
			
		<fieldset style="border: 1px groove #ddd !important;" class="">
				<table class="table-striped">
                 <tbody>
                    <tr>
                     <th scope="col"></th>
                    <td scope="col" style="text-align: center;"><b></b> </td>
                  </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">1 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">स्वयं के विकास में आपको यह पाठ्यक्रम कितनी सहायता प्रदान कर सकता है </p></td>
                     <td scope="col"><input type="radio" name="qe1" value="अत्यधिक" id="qe1a" style='margin-right:10px;'><label for="q1a">अत्यधिक</label></td>
                     <td scope="col"><input type="radio" name="qe1" value="अधिक" id="qe1b" style='margin-right:10px;'><label for="q1b">अधिक</label></td>
                     <td scope="col"><input type="radio" name="qe1" value="सामान्य" id="qe1c" style='margin-right:10px;'><label for="q1c">सामान्य</label></td>
                     <td scope="col"><input type="radio" name="qe1" value="कम" id="qe1d" style='margin-right:10px;'><label for="q1d">कम</label></td>
                    </tr>
                 </tbody>
        </table>
		</fieldset>
		<br><br>
		
		
		  <div class="container">
		  
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="float: left;width: 6%;"/>    
                <div id="response" style="float: left; margin-left: 20px;"></div>
				</div>
					</form>
					
					
					
					
					
					
					
					
					
					
					
					
					<form name="frmfeedback2" id="frmfeedback2" class="form-inline" role="form" enctype="multipart/form-data" style="display:none;">     

                        <div class="container">
                            <div class="container">
                                

                            </div>        
							<div id="errorBox">
								</div>
							
								<div class="col-sm-4 form-group"> 
									<label for="ename">Learner Name:</label>
										<input type="text" readonly="true" class="form-control text-uppercase" value="" maxlength="50" 
											name="firstname1" id="firstname1" placeholder="Learner Name">    
								</div>
								
								<div class="col-sm-4 form-group"> 
									<label for="ename">Learner Code:</label>
										<input type="text" readonly="true" class="form-control text-uppercase" value="" maxlength="50" 
											name="lcode1" id="lcode1" placeholder="Learner Code">
										<input type="hidden" name="bcode1" id="bcode1">
								</div>
								
								<div class="col-sm-4 form-group"> 
									<label for="ename">IT-GK Name:</label>
										<input type="text" readonly="true" class="form-control text-uppercase" value="" maxlength="50" 
											name="itgkname1" id="itgkname1" placeholder="IT-GK Name">    
								</div>
								
								<div class="col-sm-4 form-group"> 
									<label for="ename">IT-GK Code:</label>
										<input type="text" readonly="true" class="form-control text-uppercase" value="" maxlength="50" 
											name="itgkcode1" id="itgkcode1" placeholder="IT-GK Code">    
								</div>
						</div>
						
			<fieldset style="border: 1px groove #ddd !important;" class="">
			<table class="table-striped">
                 
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">1 </p></td>
                     <td  scope="col"><p class="question" style="font-size:13px;">आपके द्वारा ऑनलाइन प्रशिक्षण/क्लास हेतु किस DEVICE का प्रयोग किया गया </p></td>
                     <td scope="col" ><input type="radio" name="qt11" value="Desktop" id="qt11a" style='margin-right:10px;'><label for="qt11a"> Desktop </label></td>
                     <td  scope="col" ><input type="radio" name="qt11" value="Laptop" id="qt11b" style='margin-right:10px;'><label for="qt11b">Laptop </label></td>
                     <td scope="col" ><input type="radio" name="qt11" value="Smartphone" id="qt11c" style='margin-right:10px;'><label for="qt11c"> Smartphone </label></td>
                     <td scope="col"  ><input type="radio" name="qt11" value="संशाधन उपलब्ध नहीं" id="qt11d" style='margin-right:10px;'><label for="qt11d"> संशाधन उपलब्ध नहीं </label></td>
                    </tr>
                 </tbody>
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">2 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">ऑनलाइन क्लास हेतु आपके द्वारा कौन सा TOOL का उपयोग किया गया  </p></td>
                     <td scope="col" ><input type="radio" name="qt12" value="Microsoft Team" id="qt12a" style='margin-right:10px;font-size:12px;'><label for="qt12a" >Microsoft Team</label></td>
                     <td scope="col" ><input  type="radio" name="qt12" value="Zoom App" id="qt12b" style='margin-right:10px;'><label for="qt12b" >Zoom App </label></td>
                     <td scope="col" ><input type="radio" name="qt12" value="Google" id="qt12c" style='margin-right:10px;'><label for="qt12c" >Google </label></td>
                     <td scope="col"><input type="radio" name="qt12" value="Webex" id="qt12d" style='margin-right:10px;'><label for="qt12d"  >Webex</label></td>
					 
					
                    </tr>
					
					
					<tr>
                    <td scope="col"><p class="question"> </p></td>
					 <td scope="col"><p class="question"> </p></td>
                      <td scope="col" ><input type="radio" name="qt12" value="WhatsApp" id="qt12e" style='margin-right:10px;'><label for="qt12e" >WhatsApp </label></td>
                     <td scope="col"><input type="radio" name="qt12" value="Telegram" id="qt12f" style='margin-right:10px;'><label for="qt12f" >Telegram </label></td>
                     <td scope="col"><input type="radio"  name="qt12" value="कोई अन्य टूल" id="qt12g" style='margin-right:10px;'><label for="qt12g" >कोई अन्य टूल </label></td>
                    </tr>
                 </tbody>
                
                <tbody>
                    <tr>
                    <td scope="col"><p class="question">3 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">प्रशिक्षण पूर्ण करने हेतु ली गया ऑनलाइन क्लास/मार्गदर्शन की संख्या </p></td>
                     <td scope="col"><input type="radio" name="qt13" value="अत्यधिक" id="qt13a" style='margin-right:10px;'><label for="qt13a">अत्यधिक</label></td>
                     <td scope="col"><input type="radio" name="qt13" value="उचित" id="qt13b" style='margin-right:10px;'><label for="qt13b">उचित</label></td>
                     <td scope="col"><input type="radio" name="qt13" value="कम" id="qt13c" style='margin-right:10px;'><label for="qt13c">कम</label></td>
                     <td scope="col" ><input type="radio" name="qt13" value="बहुत कम" id="qt13d" style='margin-right:10px;'><label for="qt13d"> बहुत कम </label></td>
                    </tr>
					
					<tr>
                    <td scope="col"><p class="question">4 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">Faculty द्वारा पढ़ाने की क्षमता, ज्ञान व प्रतिबद्धता  </p></td>
                     <td scope="col"><input  type="radio" name="qt14" value="बहुत अच्छा" id="qt14a" style='margin-right:10px;'><label for="qt14a">बहुत अच्छा</label></td>
                     <td scope="col"><input type="radio" name="qt14" value="सामान्य" id="qt14b" style='margin-right:10px;'><label for="qt14b">सामान्य</label></td>
                     <td scope="col"><input type="radio" name="qt14" value="संतोषप्रद" id="qt14c" style='margin-right:10px;'><label for="qt14c">संतोषप्रद</label></td>
                     <td  scope="col"><input type="radio" name="qt14" value="असंतुष्ट" id="qt14d" style='margin-right:10px;'><label for="qt14d"> असंतुष्ट </label></td>
                    </tr>
					
					<tr>
                    <td  scope="col"><p class="question">5 </p></td>
                     <td scope="col"><p class="question" style="font-size:13px;">ऑनलाइन प्रशिक्षण पूर्ण किया गया</p></td>
                     <td scope="col"><input type="radio" name="qt15" value="हाँ" id="qt15a" style='margin-right:10px;'><label for="qt15a">हाँ</label></td>
                     <td scope="col"><input type="radio" name="qt15" value="नहीं" id="qt15b" style='margin-right:10px;'><label for="qt15b">नहीं</label></td>
                     
                    </tr>
					
					<tr>
                    <td ><p class="question">6 </p></td>
                     <td ><p class="question" style="font-size:13px;">ऑनलाइन प्रशिक्षण से आप कितने संतुष्ट है  </p></td>
                     <td ><input type="radio" name="qt16" value="अत्यधिक" id="qt16a" style='margin-right:10px;'><label for="qt16a">अत्यधिक</label></td>
                     <td ><input type="radio" name="qt16" value="सामान्य" id="qt16b" style='margin-right:10px;'><label for="qt16b">सामान्य</label></td>
                     <td ><input type="radio" name="qt16" value="कम" id="qt16c" style='margin-right:10px;'><label for="qt16c">कम</label></td>
                     <td ><input type="radio" name="qt16" value="बिल्कुल नहीं" id="qt16d" style='margin-right:10px;'><label for="qt16d"> बिल्कुल नहीं </label></td>
                    </tr>
                 </tbody>
			</table>
		</fieldset>
					
		
		  <div class="container">
		  
                    <input style='margin-top:10px;' type="submit" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Submit" style="float: left;width: 6%;"/>    
					
		  </div>
		  <div id="response1" style="float: left; margin-left: 20px;"></div>
		</form>
					
					
					
		
				</div>
			</div>
		</div>
</div>

<?php include 'common/message.php' ; ?>

<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
			$(document).ready(function () {
				
				function FillBatchName() {
					$.ajax({
						type: "post",
						url: "common/cfWcdFeedbackForm.php",
						data: "action=FillBatch",
						success: function (data) {	
							//alert(data);	
							data = $.parseJSON(data);
							document.getElementById('txtbatchname').innerHTML = data[0].BatchName;
							bcode.value = data[0].BatchId;
							bcode1.value = data[0].BatchId; 
							//bcode.value = 200;
							//alert(bcode.value);
						
					
								$("#frmfeedback2").show();
						
														
						}
					});
				}
				FillBatchName();
		
				function FillLearnerDetails() {
					$.ajax({
						type: "post",
						url: "common/cfWcdFeedbackForm.php",
						data: "action=FillDetails",
						success: function (data) {
							//alert(data);
							data = $.parseJSON(data);
							firstname.value = data[0].lname;
							lcode.value = data[0].lcode;
							itgkname.value = data[0].itgkname;
							itgkcode.value = data[0].itgkcode;
							
							firstname1.value = data[0].lname;
							lcode1.value = data[0].lcode;
							itgkname1.value = data[0].itgkname;
							itgkcode1.value = data[0].itgkcode;
						}
					});
				}
				FillLearnerDetails();
				
				$("#btnSubmit").click(function () {
				 
					$('#response').empty();
					$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
					var url = "common/cfWcdFeedbackForm.php"; // the script where you handle the form input.
					var data;
					var forminput = $("#frmfeedback1").serialize();
					data = "action=ADD&" + forminput;
						$.ajax({
							type: "POST",
							url: url,
							data: data,
							success: function (data)
							{
								
							$('#response').empty();
								if (data == 1) {
									$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Something has gone wrong. Please try again." + "</span></p>");
								}
								else if (data == 2) {
									$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please complete all questions." + "</span></p>");
								}
								else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
										window.setTimeout(function () {
											window.location.href = "frmwcdfeedbackform.php";
										}, 3000);
									}
								else
									{
										$('#response').empty();
										$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
									}
							}
						});
					 // avoid to execute the actual submit of the form.
				return false;
				});
				
				
				
				
				$("#btnSubmit1").click(function () {
				 
					$('#response1').empty();
					$('#response1').html("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
					var url = "common/cfWcdFeedbackForm.php"; // the script where you handle the form input.
					var data;
					var forminput = $("#frmfeedback2").serialize();
					data = "action=ADD1&" + forminput;
						$.ajax({
							type: "POST",
							url: url,
							data: data,
							success: function (data)
							{
								
							$('#response1').empty();
								if (data == 1) {
									$('#response1').html("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Something has gone wrong. Please try again." + "</span></p>");
								}
								else if (data == 2) {
									$('#response1').html("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please complete all questions." + "</span></p>");
								}
								else if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
									{
										$('#response1').empty();
										$('#response1').html("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
										window.setTimeout(function () {
											window.location.href = "frmwcdfeedbackform.php";
										}, 3000);
									}
								else
									{
										$('#response1').empty();
										$('#response1').html("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
									}
							}
						});
					 // avoid to execute the actual submit of the form.
				return false;
				});
				
				
				
				
		
});
</script>
    <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
  
</html>
    
<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>




