<?php
$title = "Admission Form";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    $Mode = $_REQUEST['Mode'];
} else {
    echo "<script>var AdmissionCode=0</script>";
    echo "<script>var Mode='Add'</script>";
    $Mode = "Add";
}
if (isset($_REQUEST['ddlCourse'])) {
    $_SESSION['CourseCode'] = $_REQUEST['ddlCourse'];
    $_SESSION['PackageCode'] = $_REQUEST['ddlPkg'];
    $_SESSION['CourseCategory'] = $_REQUEST['ddlcategory'];
    $_SESSION['BatchCode'] = $_REQUEST['ddlBatch'];
} else {
    echo "<script>var CourseCode=0</script>";
    echo "<script>var PackageCode=0</script>";
    echo "<script>var CourseCategory=0</script>";
    echo "<script>var BatchCode=0</script>";
}

echo "<script>var CourseCode = '" . $_SESSION['CourseCode'] . "'</script>";
echo "<script>var PackageCode  = '" . $_SESSION['PackageCode'] . "'</script>";
echo "<script>var CourseCategory  = '" . $_SESSION['CourseCategory'] . "'</script>";
echo "<script>var BatchCode  = '" . $_SESSION['BatchCode'] . "'</script>";

//print_r($_SESSION['CourseCode']);
//print_r($_SESSION['BatchCode']);
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div class="container"> 

    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading" style="height:55px">
            <div class="col-sm-3" style="margin-top:10px;"> Admission Form </div>

            <div class="col-sm-3" style="margin-left:-60px;">
                Course: <div style="display: inline" id="txtcoursename"> </div>
            </div>

            <div class="col-sm-3" style="margin-top:10px; margin-left:100px;">
                Batch: <div style="display: inline" id="txtbatchname"> </div>
            </div>

            <div class="col-sm-3" style="margin-top:10px; margin-left:-50px;">
                Total Admission Count: <div style="display: inline" id="txtintakeavailable"> </div>
            </div>


        </div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            <form name="frmadmission" id="frmadmission" class="form-inline" role="form" enctype="multipart/form-data"> 
                <div class="container">
                    <div class="container">
                        <div class="col-sm-4 form-group" id="response"></div>
                    </div>        
                    <div id="errorBox"></div>
                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Learner Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" name="txtlname" id="txtlname" style="text-transform:uppercase" onkeypress="javascript:return allowchar(event);" placeholder="Learner Name">
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtAdmissionCode" id="txtAdmissionCode"/>
                        <select id="ddlBatch" style="display:none;" name="ddlBatch" class="form-control">  </select>
                        <input type="hidden" class="form-control" maxlength="50" name="txtCourseFee" id="txtCourseFee"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtInstallMode" id="txtInstallMode"/>
                        <input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
                        <input type="hidden" class="form-control"  name="txtsign" id="txtsign"/>
						<input type="hidden" class="form-control"  name="txtLearnercode" id="txtLearnercode"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtmode" id="txtmode" 
								value="<?php echo $Mode; ?>" />
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="fname">Father/Husband Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" style="text-transform:uppercase" maxlength="50" name="txtfname" id="txtfname" onkeypress="javascript:return allowchar(event);" placeholder="Father Name">     
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="dob">Date of Birth:<span class="star">*</span></label>								
                        <input type="text" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="YYYY-MM-DD">
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="mtongue">Mother Tongue:</label>
                        <select id="ddlmotherTongue" name="ddlmotherTongue" class="form-control" >								  
                            <option value="Hindi" selected>Hindi</option>
                            <option value="English">English</option>
                        </select>    
                    </div>
                </div>  

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="gender">Gender:</label> <br/>                               
                        <label class="radio-inline"> <input type="radio" id="genderMale" checked="checked" name="gender" /> Male </label>
                        <label class="radio-inline"> <input type="radio" id="genderFemale" name="gender" /> Female </label>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="email">Marital Status:</label> <br/>  
                        <label class="radio-inline"><input type="radio" id="mstatusSingle" checked="checked" name="mstatus" /> Single </label>
                        <label class="radio-inline"><input type="radio" id="mstatusMarried" name="mstatus" /> Married	</label>							
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="address">Medium Of Study:</label> <br/> 
                        <label class="radio-inline"> <input type="radio" id="mediumEnglish" name="Medium" /> English </label>
                        <label class="radio-inline"> <input type="radio" id="mediumHindi" name="Medium" checked="checked" /> Hindi </label>
                    </div>

                    <div class="col-sm-6 form-group"> 
                        <label for="deptname">Physically Challenged:</label> <br/> 
                        <label class="radio-inline"> <input type="radio" id="physicallyChStYes" name="PH" /> Yes </label>
                        <label class="radio-inline"> <input type="radio" id="physicallyChStNo" name="PH" checked="checked"/> No </label>
                    </div>
                </div>    

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="empid">Proof Of Identity:<span class="star">*</span></label>
                        <select id="ddlidproof" name="ddlidproof" class="form-control">
                            <option value="">Select</option>
                            <option value="PAN Card">PAN Card</option>
                            <option value="Voter ID Card">Voter ID Card</option>
                            <option value="Driving License">Driving License</option>
                            <option value="Passport">Passport</option>
                            <option value="Employer ID card">Employer ID card</option>
                            <option value="Government ID Card">Governments ID Card</option>
                            <option value="College ID Card">College ID Card</option>
                            <option value="School ID Card">School ID Card</option>
                        </select>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="gpfno">Aadhar No:</label>
                        <input type="text" class="form-control" maxlength="12"  name="txtidno" id="txtidno" onkeypress="javascript:return allownumbers(event);" placeholder="Aadhar No">
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="designation">District:<span class="star">*</span></label>
                        <select id="ddlDistrict" name="ddlDistrict" class="form-control">                                  
                        </select>
                    </div>     

                    <div class="col-sm-4 form-group"> 
                        <label for="marks">Tehsil:<span class="star">*</span></label>
                        <select id="ddlTehsil" name="ddlTehsil" class="form-control">                                 
                        </select>
                    </div>
                </div>

                <div class="container">                          
                    <div class="col-sm-4 form-group"> 
                        <label for="attempt">Address:<span class="star">*</span></label>  
                        <textarea class="form-control" rows="3" style="text-transform:uppercase" id="txtAddress" name="txtAddress" placeholder="Address" onkeypress="javascript:return validAddress(event);"></textarea>

                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="pan">PINCODE:<span class="star">*</span></label>
                        <input type="text" class="form-control" name="txtPIN" id="txtPIN"  placeholder="PINCODE" onkeypress="javascript:return allownumbers(event);" maxlength="6"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankaccount">Mobile No:<span class="star">*</span></label>
                        <input type="text" class="form-control"  name="txtmobile"  id="txtmobile" placeholder="Mobile No" onkeypress="javascript:return allownumbers(event);" maxlength="10"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankdistrict">Phone No:</label>
                        <input type="text" placeholder="Phone No" class="form-control" id="txtResiPh" name="txtResiPh" onkeypress="javascript:return allownumbers(event);" maxlength="11"/>
                    </div>   
                </div>

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="bankname">Email Id:</label>
                        <input type="text" placeholder="Email Id" class="form-control" id="txtemail" name="txtemail" maxlength="100" />
                    </div>   

                    <div class="col-sm-4 form-group"> 
                        <label for="branchname">Qualification:<span class="star">*</span></label>
                        <select id="ddlQualification" name="ddlQualification" class="form-control">                                  
                        </select>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="ifsc">Type of Learner:<span class="star">*</span></label>
                        <select id="ddlLearnerType" name="ddlLearnerType" class="form-control">                                 
                        </select>
                    </div> 
			
                </div>

                <fieldset style="border: 1px groove #ddd !important;" class="">
                    <br>
                    <div class="container">
                         <div class="col-sm-1"> 
                            
                        </div>
                        <div class="col-sm-3" style="display: none"> 
                            <label for="payreceipt">Upload Scan Application Form:<span class="star">*</span></label>
                            <input type="file" class="form-control"  name="txtUploadScanDoc" id="txtUploadScanDoc" onchange="checkScanForm(this)"/>
                        </div>											

                        <div class="col-sm-3" > 
                            <label for="photo">Attach Photo:<span class="star">*</span></label> </br>
                            <img id="uploadPreview1" src="images/samplephoto.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">								  
                            
                            <input style="margin-top:5px !important;" id="uploadImage1" type="file" name="p1" onchange="checkPhoto(this);
                                    PreviewImage(1)"/>									  
                        </div>

                        <div class="col-sm-3"> 
                            <label for="photo">Attach Signature:<span class="star">*</span></label> </br>
                            <img id="uploadPreview2" src="images/samplesign.png" id="uploadPreview2" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage2').click();">							  
                            <input id="uploadImage2" type="file" name="p2" onchange="checkSign(this);
                                    PreviewImage(2)"/>
                        </div>  
                        <div class="col-sm-3"> 
                            <input style="margin-top:50px !important;" type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload Photo Sign"/>   
                        </div>
                        <div class="col-sm-1"> 
                            
                        </div>
                        
                    </div>
                    <br>
                </fieldset>
                <br><br>
                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                </div>
        </div>
    </div>   
</div>
</form>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script src="scripts/imageupload.js"></script>

<style>
    #errorBox
    {	color:#F00;	 } 
</style>

<script type="text/javascript">
                                $('#dob').datepicker({
                                    format: "yyyy-mm-dd",
                                    orientation: "bottom auto",
                                    todayHighlight: true
                                });
</script>


<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>

<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
    ;
</script>	

<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#txtUploadScanDoc').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }
        else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }
        document.getElementById("txtUploadScanDoc").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkPhoto(target) {
        var ext = $('#uploadImage1').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage1").value = '';
            return false;
        }

        if (target.files[0].size > 5000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 5 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        else if (target.files[0].size < 3000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        document.getElementById("uploadImage1").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkSign(target) {
        var ext = $('#uploadImage2').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage2").value = '';
            return false;
        }

        if (target.files[0].size > 3000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 3 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        else if (target.files[0].size < 1000)
        {
            alert("Image size should be greater than 1 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        document.getElementById("uploadImage2").innerHTML = "";
        return true;
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {
        //alert(BatchCode);

        function FillIntake() {
            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=FILLIntake&values=" + CourseCode + "",
                success: function (data) {
					if(data == '10000'){
						alert("Subscription Not Available");
                        window.location.href = "frmadvancecoursemessage.php";
					}
                    else {
						document.getElementById('txtintakeavailable').innerHTML = data;
					}                    
                }
            });
        }
        FillIntake();

        function FillCourseName() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FillAdmissionCourse&values=" + CourseCode + "",
                success: function (data) {
                    document.getElementById('txtcoursename').innerHTML = data;
                }
            });
        }
        FillCourseName();

        function FillBatchName() {
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FillAdmissionBatch&values=" + BatchCode + "",
                success: function (data) {
                    document.getElementById('txtbatchname').innerHTML = data;
                }
            });
        }
        FillBatchName();

        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                }
            });
        }
        GenerateUploadId();
		
		
		function GenerateLearnercode()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=GENERATELEARNERCODE",
                success: function (data) {
                    txtLearnercode.value = data;
                }
            });
        }
        GenerateLearnercode();

        function AdmissionFee()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=Fee&codes=" + BatchCode + "",
                success: function (data) {
                    txtCourseFee.value = data;
                }
            });
        }
        AdmissionFee();

        function InstallMode()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=Install&values=" + BatchCode + "",
                success: function (data) {
                    txtInstallMode.value = data;
                }
            });
        }
        InstallMode();

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            //alert(1);
            fillForm();
        }

        $("input[type='image']").click(function () {
            $("input[id='my_file']").click();
        });

        function FillDistrict() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        }
        FillDistrict();

        function FillQualification() {
            $.ajax({
                type: "post",
                url: "common/cfQualificationMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlQualification").html(data);
                }
            });
        }
        FillQualification();

        function FillLearnerType() {
            $.ajax({
                type: "post",
                url: "common/cfLearnerTypeMaster.php",
                data: "action=FILL&coursecode=" + CourseCode + "",
                success: function (data) {
                    $("#ddlLearnerType").html(data);
                }
            });
        }
        FillLearnerType();

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });     

        // statement block start for delete record function
        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=DELETE&values=" + AdmissionCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmLearnerProfile.php";
                        }, 1000);
                        Mode = "Add";
                        resetForm("frmLearnerProfile");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }
        // statement block end for delete function statment

        function fillForm()
        {

            $.ajax({
                type: "post",
                url: "common/cfAdvanceCourseAdmission.php",
                data: "action=EDIT&values=" + AdmissionCode + "",
                success: function (data) {

                    data = $.parseJSON(data);
                    txtlname.value = data[0].lname;
                    txtfname.value = data[0].fname;
                    dob.value = data[0].dob;
                    txtAdmissionCode.value = data[0].AdmissionCode;
                    txtphoto.value = data[0].photo;
                    txtsign.value = data[0].sign;
                    $("#uploadPreview1").attr('src',"upload/admission_photo/" + data[0].photo);
					$("#uploadPreview2").attr('src',"upload/admission_sign/" + data[0].sign);
                    //p1.value = data[0].photo;
                    //p2.value = data[0].sign; 
                }
            });
        }

        $("#btnUpload").click(function () {
            $('#btnSubmit').show(1000);
        });
        $("#btnSubmit").click(function () {
            if ($("#frmadmission").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfAdvanceCourseAdmission.php"; // the script where you handle the form input.
                if (document.getElementById('mediumEnglish').checked) //for radio button
                {
                    var medium_type = 'English';
                }
                else {
                    medium_type = 'Hindi';
                }
                if (document.getElementById('genderMale').checked) //for radio button
                {
                    var gender_type = 'Male';
                }
                else {
                    gender_type = 'Female';
                }
                if (document.getElementById('mstatusSingle').checked) //for radio button
                {
                    var marital_type = 'Single';
                }
                else {
                    marital_type = 'Married';
                }
                if (document.getElementById('physicallyChStYes').checked) //for radio button
                {
                    var physical_status = 'Yes';
                }
                else {
                    physical_status = 'No';
                }
				
                var lphoto = $('#txtLearnercode').val();
                var lsign = $('#txtLearnercode').val();
                var lscan = $('#txtLearnercode').val();
                var dob = $('#dob').val();                
                var fee = $('#txtCourseFee').val();
                var installmode = $('#txtInstallMode').val();
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&lname=" + txtlname.value + "&learnercode=" + txtLearnercode.value + "&parent=" + txtfname.value +
                            "&idproof=" + ddlidproof.value + "&coursecode=" + CourseCode + "&batchcode=" + BatchCode + "&fee=" + fee + "&install=" + installmode + "&packagecode=" + PackageCode +
							"&categorycode=" + CourseCategory +
                            "&idno=" + txtidno.value + "&dob=" + dob +
                            "&mothertongue=" + ddlmotherTongue.value + "&medium=" + medium_type +
                            "&gender=" + gender_type + "&marital_type=" + marital_type +
                            "&district=" + ddlDistrict.value + "&tehsil=" + ddlTehsil.value +
                            "&address=" + txtAddress.value + "&pincode=" + txtPIN.value +
                            "&resiph=" + txtResiPh.value + "&mobile=" + txtmobile.value +
                            "&qualification=" + ddlQualification.value + "&learnertype=" + ddlLearnerType.value +
                            "&physicallychallenged=" + physical_status +
                            "&email=" + txtemail.value + "&lphoto=" + lphoto +
                            "&lsign=" + lsign + "&lscan=" + lscan + ""; // serializes the form's elements.
                    //alert (data);
                }
                else
                {
                    data = "action=UPDATE&lname=" + txtlname.value + "&parent=" + txtfname.value + "&dob=" + dob + "&lphoto=" + lphoto + "&lsign=" + lsign + "&Code= " + txtAdmissionCode.value + ""; // serializes the form's elements.
                }
                //alert(data);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmadvancecourseadmission.php';
                            }, 3000);
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                        }
                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        // statement block end for submit click
    });

</script>

<!--<script src="scripts/signupload.js"></script>-->
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmadmission_validation.js" type="text/javascript"></script>	
</html>