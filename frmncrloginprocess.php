<?php
$title = "Organization Form Process";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">			 
        <div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">Approve Organization Login Request</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" action="" class="form-inline">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <br>
                    <div class="container">

                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Name of Organization/Center:</label>
                            <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="ename">Registration No:</label>
                            <input type="text" class="form-control" readonly="true" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="faname">Date of Establishment:</label>
                            <input type="text" class="form-control" readonly="true" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Type of Organization:</label>
                            <input type="text" class="form-control" readonly="true" name="txtType" id="txtType" placeholder="Type Of Organization">  
                        </div>
                    </div>




                    <div class="container">




                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Document Type:</label>
                            <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                        </div>

                        <div class="col-sm-4 form-group">     
                            <label for="SelectType">Select Type of Application:</label>
                            <input type="text" class="form-control" readonly="true" name="txtRole" id="txtRole" placeholder="Role"/>     

                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="email">Enter Email:</label>
                            <input type="text" class="form-control" readonly="true" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="Mobile">Enter Mobile Number:</label>
                            <input type="text" class="form-control" readonly="true" name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                        </div>


                    </div>	


                    <div class="container">




                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">District:</label>
                            <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                            <input type="hidden" class="form-control"  name="txtDistrictCode" id="txtDistrictCode">
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Tehsil:</label>
                            <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                        </div>

                        <!--                        <div class="col-sm-4 form-group"> 
                                                    <label for="address">Street:</label>
                                                    <input type="text" class="form-control" readonly="true" name="txtStreet" id="txtStreet" placeholder="Street">    
                                                </div>-->


                        <div class="col-sm-4 form-group">     
                            <label for="address">Address:</label>
                            <textarea class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road"></textarea>

                        </div>
                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">Uploaded Owner Documents</div>
                        <div class="panel-body">	

                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Pan Card:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview1" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">AADHAR Card:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview2" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">Address Proof:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview3" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="photo">NCR Applcation Form:<span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview4" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">Uploaded Organization Type Documents</div>
                        <div class="panel-body">
                            <button type="button" id="showmodal" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc1"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview10" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc2"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview11" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc3"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview12" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" > 
                                <label for="orgtypedoc4"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview13" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc5"> 
                                <label for="orgtypedoc5"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview14" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc6"> 
                                <label for="orgtypedoc6"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview15" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                            <div class="col-sm-4 form-group" id="doc7"> 
                                <label for="orgtypedoc7"><span class="star">*</span></label> </br>
                                <button type="button" id="uploadPreview16" class="viewimage thumbnailmodal btn btn-primary" name="filePhoto1" width="150px" height="150px">
                                    View Document</button>	
                            </div>
                        </div>
                    </div>

<!--                    <div class="container">
                        <div class="col-sm-4 form-group"> 
                            <label for="status">Action:<span class="star">*</span></label>
                            <select id="ddlstatus" name="ddlstatus" class="form-control" onchange="toggle_visibility1('remark')">
                                <option selected="true" value="">Select</option>
                                <option value="Approve" >Approve</option>
                                <option value="Reject" >Reject</option>
                            </select>    
                        </div>
                        <div class="col-sm-4 form-group" id="remark" style="display:none;"> 
                            <label for="pan">Remark:<span class="star">*</span></label>
                            <input type="text" class="form-control"  name="txtRemark" id="txtRemark"  placeholder="Remark">
                        </div>


                    </div>		-->

                    <div class="container" id="btnsubmitdiv">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                    </div>
            </div>
            </form>
        </div>  

    </div>

    <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" style="width: 100%;height: 500px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h3 id="heading-tittle" class="modal-title">Heading</h3>
                </div>
                <iframe id="viewimagesrc" src="uploads/news/1487051012.pdf" style="width: 100%;height: 500px;border: none;"></iframe>
                <!--<img id="viewimagesrc" class="thumbnail img-responsive" src="images/not-found.png" name="filePhoto3" width="800px" height="880px">-->
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>
<!--<script type="text/javascript">
    function toggle_visibility1(id) {
        var e = document.getElementById(id);

        //alert(e);
        var f = document.getElementById('ddlstatus').value;
        //alert(f);
        if (f == "Reject")
        {
            e.style.display = 'block';
        } else {
            e.style.display = 'none';
        }

    }
</script>-->

<script type="text/javascript">

    $(document).ready(function () {
        jQuery(".thumbnailmodal").click(function () {
            $('.modal-body').empty();
            var title = $(this).parent('a').attr("title");
            $('.modal-title').html(title);
            $($(this).parents('div').html()).appendTo('.modal-body');
            $('#showmodal').click();
        });
    });
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        } else if (Mode == 'Edit')
        {
            //alert(1);
            fillForm();

        }
        
         $(".close").on("click",function(){
        $("#viewimagesrc").attr("src"," "); 
    });

        function fillForm()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfNCRLoginApproval.php",
                data: "action=PROCESS&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    txtName1.value = data[0].orgname;
                    txtRegno.value = data[0].regno;
                    txtEstdate.value = data[0].fdate;
                    txtType.value = data[0].orgtype;
                    txtDocType.value = data[0].doctype;
                    txtRole.value = data[0].role;

                    txtEmail.value = data[0].email;
                    txtMobile.value = data[0].mobile;
                    txtDistrict.value = data[0].district;
                    txtDistrictCode.value = data[0].districtcode;
                    txtTehsil.value = data[0].tehsil;
                    //txtStreet.value = data[0].street;
                    txtRoad.value = data[0].road;
                    $("#uploadPreview1").click(function () {
                            //$("#viewimagesrc").attr('src', "upload/NCRPAN/" + data[0].orgdoc);
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRPAN/" + data[0].orgdoc);
                     });
                     $("#uploadPreview2").click(function () {
                            //$("#viewimagesrc").attr('src', "upload/NCRUID/" + data[0].orguid);
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRUID/" + data[0].orguid + "?nocache=" + Math.random());
                        });
                        $("#uploadPreview3").click(function () {
                            //$("#viewimagesrc").attr('src', "upload/NCRAddProof/" + data[0].orgaddproof);
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRAddProof/" + data[0].orgaddproof + "?nocache=" + Math.random());
                        });
                        $("#uploadPreview4").click(function () {
                            //$("#viewimagesrc").attr('src', "upload/NCRAppForm/" + data[0].orgappform);
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "NCRAppForm/" + data[0].orgappform + "?nocache=" + Math.random());
                        });
//                    $("#uploadPreview1").attr('src', "upload/NCRPAN/" + data[0].orgdoc);
//                    $("#uploadPreview2").attr('src', "upload/NCRUID/" + data[0].orguid);
//                    $("#uploadPreview3").attr('src', "upload/NCRAddProof/" + data[0].orgaddproof);
//                    $("#uploadPreview4").attr('src', "upload/NCRAppForm/" + data[0].orgappform);
                    
                    if (txtType.value == "Proprietorship/Individual")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Ownership Type Document");
                        jQuery("label[for='orgtypedoc2']").html("Shop Act License");
                        jQuery("label[for='orgtypedoc3']").html("PAN card copy of Proprietor");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled Cheque");
                        
                        $("#doc5").hide();
                        $("#doc6").hide();
                        $("#doc7").hide();
                        

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_owntype.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_owntype.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_salpi.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_salpi.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_panpi.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_panpi.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_ccpi.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Individual_ccpi.pdf");
                        });

                    }
                    if (txtType.value == "Partnership")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Copy of Partnership Deed");
                        jQuery("label[for='orgtypedoc2']").html("Authorization letter");
                        jQuery("label[for='orgtypedoc3']").html("PAN card of Partnership firm");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled cheque of firm");
                        jQuery("label[for='orgtypedoc5']").html("Address Proof documents");
                        jQuery("label[for='orgtypedoc6']").html("Registration certificate");
                        jQuery("label[for='orgtypedoc7']").html("Shop Act Registration Copy");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_copd.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_copd.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_al.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_al.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ppf.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ppf.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ccpf.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_ccpf.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_apdpf.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_apdpf.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_rcrf.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_rcrf.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_sarpf.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Partnership_sarpf.pdf");
                        });

                    }
                    if (txtType.value == "Private Ltd." || txtType.value == "Public Ltd.")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Incorporation");
                        jQuery("label[for='orgtypedoc2']").html("List Of Directors");
                        jQuery("label[for='orgtypedoc3']").html("Pan Card Of Company");
                        jQuery("label[for='orgtypedoc4']").html("Cancelled Cheque");
                        jQuery("label[for='orgtypedoc5']").html("Board Resolution Copy");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof Document");
                        jQuery("label[for='orgtypedoc7']").html("Shop Act Registration Copy");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_coi.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_coi.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_lod.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_lod.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_copan.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_copan.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_ccc.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_ccc.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_cobr.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_cobr.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_apd.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_apd.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_csar.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Company_csar.pdf");
                        });

                    }
                     if (txtType.value == "Limited Liability Partnership (LLP)")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Incorporation");
                        jQuery("label[for='orgtypedoc2']").html("List of Partners");
                        jQuery("label[for='orgtypedoc3']").html("Copy of PAN card of LLP");
                        jQuery("label[for='orgtypedoc4']").html("Canceled cheque of LLP");
                        jQuery("label[for='orgtypedoc5']").html("Copy of Board Resolutions");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof Document");
                        jQuery("label[for='orgtypedoc7']").html("Copy of Shop Act Registration");

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_coillp.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_coillp.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_lop.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_lop.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_panllp.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_panllp.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_ccllp.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_ccllp.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_cobrllp.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_cobrllp.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_apdllp.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_apdllp.pdf");
                        });

                        $("#uploadPreview16").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_csarllp.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_LLP_csarllp.pdf");
                        });

                    }
                    if (txtType.value == "Trust" || txtType.value == "Society" || txtType.value == "Coorperative Society" || txtType.value == "Others")
                    {
                        jQuery("label[for='orgtypedoc1']").html("Certificate of Registration");
                        jQuery("label[for='orgtypedoc2']").html("PAN Card of Organization");
                        jQuery("label[for='orgtypedoc3']").html("Cancelled cheque of Organization");
                        jQuery("label[for='orgtypedoc4']").html("List of Executive Body");
                        jQuery("label[for='orgtypedoc5']").html("Copy of Board Resolution");
                        jQuery("label[for='orgtypedoc6']").html("Address Proof documents");
                        
                        $("#doc7").hide();

                        $("#uploadPreview10").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cor.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cor.pdf");
                        });

                        $("#uploadPreview11").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_panoth.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_panoth.pdf");
                        });

                        $("#uploadPreview12").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_ccoth.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_ccoth.pdf");
                        });

                        $("#uploadPreview13").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_leb.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_leb.pdf");
                        });

                        $("#uploadPreview14").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cbr.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_cbr.pdf");
                        });

                        $("#uploadPreview15").click(function () {
                            $("#viewimagesrc").attr('src', "common/showpdfftp.php?src=" + "ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_adpoth.pdf" + "?nocache=" + Math.random());
                            //$("#viewimagesrc").attr('src', "upload/ORGTYPEDOC/" + data[0].orgtypedoc1 + "_Other_adpoth.pdf");
                        });

                    }
                }
            });
        }



        $("#btnSubmit").click(function () {
		

		$("#btnsubmitdiv").hide();
            if ($("#form").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfNCRLoginApproval.php"; // the script where you handle the form input.            
                var data;
                if (Mode == 'Add')
                {

                } else
                {
                    //data = "action=LoginApprove&orgcode=" + Code + "&status=" + ddlstatus.value + "&remark=" + txtRemark.value + "&districtcode=" + txtDistrictCode.value + ""; // serializes the form's elements.
                      data = "action=LoginApprove&orgcode=" + Code + "&districtcode=" + txtDistrictCode.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Login Approved Successfully</span></p>");
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Center Login Approved Successfully.</span>");
                            window.setTimeout(function () {
                                window.location.href = "frmncrloginapproval.php";
                            }, 3000);

                            Mode = "Add";
                            resetForm("form");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();


                    }
                });
            }
            return false;
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmcorrectionprocess_validation.js"></script>
</html>