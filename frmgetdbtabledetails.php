<?php
$title = "MYRKCL Table Details";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_UserRoll'] == '8') {
    ?>
    <link rel="stylesheet" href="css/datepicker.css">
    <script src="scripts/datepicker.js"></script>

    <div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 
            <div class="panel panel-primary" style="margin-top:36px !important;">  
                <div class="panel-heading">MYRKCL Table Details</div>
                <div class="panel-body">
                    <div class="form-inline"> 
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>

                            <div class="col-sm-4 form-group"> 
                                <label for="status">Select Tables:<span class="star">*</span></label>
                                <select id="ddltables" name="ddltables" class="form-control">								  
                                    <option value="">--Select--</option>
                                    <option value="17">Upload EOI Center List</option>
                                </select>  									
                            </div>
                            <div id="datewise" style="display:none;">
                                <div class="col-sm-4 form-group" id ="sdate"> 
                                    <label for="sdate">Start Date:<span class="star">*</span></label>                           
                                    <input type="text" class="form-control" id="txtstartdate" name="txtstartdate" readonly="true"
                                           placeholder="Start Date"/>
                                </div>

                                <div class="col-sm-4 form-group" id ="edate"> 
                                    <label for="sdate">End Date:<span class="star">*</span></label>                            
                                    <input type="text" class="form-control" id="txtenddate" name="txtenddate" readonly="true"
                                           placeholder="End Date"/>
                                </div>
                            </div>
                            <div id="uploadeoicenter" style="margin-top: -15px;display:none;">
                                <form name="frmcpydbtables" method="POST" id="frmcpydbtables" class="form-inline" role="form" enctype="multipart/form-data"> 
                                    <div class="col-sm-4 form-group"> 
                                        <label for="ecl">Upload IT-GK Data:<span class="star">*</span></label>
                                        <input type="file" class="form-control" id="Eoi_file" name="Eoi_file" accept=".csv"/>
                                        <span style="font-size:10px;">Note : Only .csv File Allowed</span>
                                    </div>
                                    <div class="col-sm-4 form-group" >   
                                        <label for="ecl"><span class="star"></span></label>
                                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" 
                                               value="Submit" style="margin-top: 24px;display:none"/>
                                    </div>                  
                                </form>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
    </body>

    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>
    <script type="text/javascript">
        $('#txtstartdate').datepicker({
            format: "yyyy-mm-dd",
            orientation: "bottom auto",
            todayHighlight: true,
            autoclose: true
        });
    </script>

    <script type="text/javascript">
        $('#txtenddate').datepicker({
            format: "yyyy-mm-dd",
            orientation: "bottom auto",
            todayHighlight: true,
            autoclose: true
        });
    </script>	
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

        $(document).ready(function () {
            //alert("hii");


            function FillTables() {
                $.ajax({
                    type: "post",
                    url: "common/cfGetDbTableDetails.php",
                    data: "action=GetTables",
                    success: function (data) {
                        $("#ddltables").html(data);
                    }
                });
            }
            //FillTables();

            $("#ddltables").change(function () {
                var TableId = $(this).val();
                //alert(TableId);
                if (TableId == '2' || TableId == '8' || TableId == '7' || TableId == '16' || TableId == '18' || TableId == '19') {
                    $('#datewise').show();
                    $('#uploadeoicenter').hide();
                    $('#btnSubmit').show();
                } else if (TableId == '3' || TableId == '6' || TableId == '9' || TableId == '10' || TableId == '13') {
                    $('#datewise').hide();
                    $('#uploadeoicenter').hide();
                    $('#btnSubmit').show();
                } else if (TableId == '17') {

                    $('#datewise').hide();
                    $('#uploadeoicenter').show();
                    $('#btnSubmit').show();

                } else if (TableId == '15') {
                    $('#datewise').hide();
                    $('#uploadeoicenter').hide();
                    $('#btnSubmit').show();
                }
            });

            $("#frmcpydbtables").on('submit', (function (e) {
                e.preventDefault();
                var ddltables = $("#ddltables").val();
                var formData = new FormData(this);
                formData.append('action', 'UpdateTable');
                formData.append('ddltables', ddltables);
				$('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
                    url: "common/cfGetDbTableDetails.php",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data)
                    {
						var data = data.trim();
                        if (data == "Successfully Inserted") {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Successfully Updated.</span></p>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmgetdbtabledetails.php';
                            }, 3000);
                        } else {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> " + data + " </span></p>");
                        }
                    },
                    error: function (e)
                    {
                        $("#errmsg").html(e).fadeIn();
                    }
                });
            }));


    //            $("#btnSubmit").click(function () {
    //
    //                $('#response').empty();
    //                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
    //
    //                var url = "common/cfGetDbTableDetails.php"; // the script where you handle the form input.
    //                var data;
    //                var forminput = $("#frmcpydbtables").serialize();
    //                if (Mode == 'Add')
    //                {
    //                    data = "action=UpdateTable&" + forminput;
    //                    //alert(data);
    //                }
    //
    //                $.ajax({
    //                    type: "POST",
    //                    url: url,
    //                    data: data,
    //                    success: function (data)
    //                    {
    //                        //alert(data);
    //
    //                        if (data == 1) {
    //                            $('#response').empty();
    //                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Successfully Updated.</span></p>");
    //                            window.setTimeout(function () {
    //                                Mode = "Add";
    //                                window.location.href = 'frmgetdbtabledetails.php';
    //                            }, 3000);
    //                        } else {
    //                            $('#response').empty();
    //                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Something Happned Wrong.</span></p>");
    //                        }
    //                    }
    //                });
    //
    //                return false; // avoid to execute the actual submit of the form.
    //            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });
    </script>

    <script src="rkcltheme/js/jquery.validate.min.js"></script>
    <script>
        $("#frmcpydbtables").validate({
            rules: {
                _file: {required: true}, ddlstatus: {required: true}
            },
            messages: {
                _file: {required: '<span style="color:red; font-size: 12px;">Please upload Govt. Excel Data.</span>'},
                ddlstatus: {required: '<span style="color:red; font-size: 12px;">Please Select Application Status.</span>'}
            },
        });
    </script>
    </html> 

    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>	