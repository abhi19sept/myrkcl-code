
var app = angular.module("sqlModule", []);

app.controller("firstController", function($scope) {
	$scope.useOnlySpecialCharacters = /^[a-zA-Z0-9]*$/;
});

app.directive('restrictSpecialCharactersDirective', function() {
    function link(scope, elem, attrs, ngModel) {
        ngModel.$parsers.push(function(viewValue) {
            var reg = /^[a-zA-Z0-9 ]*$/;
            if (viewValue.match(reg)) {
              return viewValue;
	        }
	        var transformedValue = ngModel.$modelValue;
	        ngModel.$setViewValue(transformedValue);
	        ngModel.$render();
	        return transformedValue;
	    });
    }
    return {
        restrict: 'A',
        require: 'ngModel',
        link: link
    };      
});
