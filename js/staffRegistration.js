/***** START FROM THIS FILE HERE ***/

$(function () { 
    $('#appnotification').validate({
        rules: {
            'notificationTittle': {
                required: true
            },
            'notificationType': {
                required: true
            },
            'notificationUsers': {
                required: true
            },
            'notificationMessage': {
                required: true
            }
        },
        messages: {
            'notificationTittle': {
                required: '<span style="color:red; font-size: 12px;">Please enter tittle</span>'
            },
            'notificationType': {
                required: '<span style="color:red; font-size: 12px;">please select type</span>'
            },
            'notificationUsers': {
                required: '<span style="color:red; font-size: 12px;">please select users</span>'
            },
            'notificationMessage':{
                required: '<span style="color:red; font-size: 12px;">please enter message</span>'
            }
        },
    });

});