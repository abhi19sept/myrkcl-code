﻿function Jsonp_Callback(json) {
    if (json.Error != null) {
        alert(json.Message);
    }
}

function crossDomainAjax(url, callback) {
    // IE8 & 9 only Cross domain JSON GET request
    if ('XDomainRequest' in window && window.XDomainRequest !== null) {
        var xdr = new XDomainRequest(); // Use Microsoft XDR
        xdr.open('get', url);
        xdr.onload = function () {
            var dom = new ActiveXObject('Microsoft.XMLDOM'),
                JSON = $.parseJSON(xdr.responseText);
            dom.async = false;
            if (JSON == null || typeof (JSON) == 'undefined') {
                JSON = $.parseJSON(data.firstChild.textContent);
            }
            callback(JSON);
        };
        xdr.onerror = function () {
            _result = false;
        };
        xdr.send();
    }
    // IE7 doesn't support cross domain request at all! :( :)
    // jQuery AJAX for other browsers
    else {
        $.support.cors = true;
        $("#wait_modal").modal("show");
        $.ajax({
            url: url,
            crossDomain: true,
            contentType: 'text/plain',
            cache: false,
            dataType: 'jsonp',
            type: 'GET',
            async: false, // must be set to false
            jsonpCallback: "Jsonp_Callback",
            success: function (data, success) {
                $("#wait_modal").modal("hide");
                callback(data);
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    }
}

function captureFP() {
    crossDomainAjax("https://localhost:4443/FM220/gettmpl?callback=?",
        function (result) {
            SuccessFunc(result);
        });

}

function MatchFP() { // you can add , separted list of base64 template for multiple match.
    crossDomainAjax("https://localhost:4443/FM220/GetMatchResult?MatchTmpl=" + encodeURIComponent(document.getElementById("tmplval").value.toString()) + "&callback=?",
        function (result) {
            SuccessMatch(result);
        });
}

function jsonp(url, callback) {
    // create a unique id
    var id = "_" + (new Date()).getTime();

    // create a global callback handler
    window[id] = function (result) {
        // forward the call to specified handler
        if (callback)
            callback(result);

        // clean up: remove script and id
        var sc = document.getElementById(id);
        sc.parentNode.removeChild(sc);
        window[id] = null;
    }

    url = url.replace("callback=?", "callback=" + id);

    // create script tag that loads the 'JSONP script'
    // and executes it calling window[id] function
    var script = document.createElement("script");
    script.setAttribute("id", id);
    script.setAttribute("src", url);
    script.onerror = ErrorFunc;
    script.setAttribute("type", "text/javascript");
    document.body.appendChild(script);
}

/*
 This functions is called if the service sucessfully returns some data in JSON object
 */
function SuccessFunc(result) {


    if (result.errorCode == 0) {

        if (result != null && result.bMPBase64.length > 0) {
            document.getElementById("FPImage1").src = "data:image/bmp;base64," + result.bMPBase64;
        }
        document.getElementById("tmplval").value = result.templateBase64;
        var tbl = "<table border=1>";
        tbl += "<tr>";
        tbl += "<td> Serial Number of device </td>";
        tbl += "<td> <b>" + result.serialNumber + "</b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> Image Height</td>";
        tbl += "<td> <b>" + result.imageHeight + "</b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> Image Width</td>";
        tbl += "<td> <b>" + result.imageWidth + "</b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> Image Resolution</td>";
        tbl += "<td> <b>" + result.imageDPI + "</b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> NFIQ (1-5)</td>";
        tbl += "<td> <b>" + result.nFIQ + "</b> </td>";
        tbl += "</tr>";
        tbl += "<td> ISO Template(base64)</td>";
        tbl += "<td> <b> <textarea rows=8 cols=50>" + result.templateBase64 + "</textarea></b> </td>";
        tbl += "</tr>";
        tbl += "<td> ISO Image(base64)</td>";
        tbl += "<td> <b> <textarea rows=8 cols=50>" + result.isoImgBase64 + "</textarea></b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> Session Key(base64)</td>";
        tbl += "<td> <b> <textarea cols=50>" + result.sessionKey + "</textarea></b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> PID XML ENCRYPTED(base64)</td>";
        tbl += "<td> <b> <textarea rows=8 cols=50>" + result.encryptedPidXml + "</textarea></b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> Encrypted HMAC(base64)</td>";
        tbl += "<td> <b> <textarea  cols=50>" + result.encryptedHmac + "</textarea></b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> Client IP</td>";
        tbl += "<td> <b> <textarea  cols=50>" + result.clientIP + "</textarea></b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> TimeStamp</td>";
        tbl += "<td> <b> <textarea  cols=50>" + result.timestamp + "</textarea></b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> fdc</td>";
        tbl += "<td> <b> <textarea  cols=50>" + result.fdc + "</textarea></b> </td>";
        tbl += "</tr>";
        tbl += "</table>";
        document.getElementById('result').innerHTML = tbl;
    }
    else {
        //document.getElementById('result').innerHTML ="";
        alert("Fingerprint Capture ErrorCode " + result.errorCode + "Desc :-" + result.status);
    }
}

/*
 This functions is called if the service sucessfully returns some data in JSON object
 */
function SuccessMatch(result) {
    if (result.errorCode == 0) {
        /* 	Display BMP data in image tag
         BMP data is in base 64 format
         */
        if (result != null && result.bMPBase64.length > 0) {
            document.getElementById("FPImage1").src = "data:image/bmp;base64," + result.bMPBase64;
        }
        var tbl = "<table border=1>";
        tbl += "<tr>";
        tbl += "<td> Serial Number of device </td>";
        tbl += "<td> <b>" + result.serialNumber + "</b> </td>";
        tbl += "</tr>";
        tbl += "<tr>";
        if (result.matchSuccess) {
            tbl += "<td> Match Result</td>";
            tbl += "<td> <b> <textarea rows=2 cols=50> Matching Success. Match Score is :- " + result.matchScore.toString() + "</textarea></b> </td>";
        } else {
            tbl += "<td> Match Result</td>";
            tbl += "<td> <b> <textarea rows=2 cols=50> Matching Failed. Match Score is :- " + result.matchScore.toString() + "</textarea></b> </td>";
        }
        tbl += "</tr>";
        tbl += "<tr>";
        tbl += "<td> Matchin ID seq No</td>";
        tbl += "<td> <b> <textarea rows=2 cols=50> Matchind ID seq no is :- " + result.matchIDseqNo.toString() + "</textarea></b> </td>";
        tbl += "</tr>";
        tbl += "</table>";
        document.getElementById('result').innerHTML = tbl;
    }
    else {
        document.getElementById('result').innerHTML = "";
        alert("Fingerprint Capture ErrorCode " + result.errorCode + "Desc :-" + result.status);
    }
}

function ErrorFunc(id) {
    var sc = document.getElementById(id);
    if (sc != null) {
        sc.parentNode.removeChild(sc);
        window[id] = null;
    }
    /*
     If you reach here, user is probabaly not running the
     service. Redirect the user to a page where he can download the
     executable and install it.
     */
    alert("Check if ACPL FM220 service is running ");

}

/*** THIS FUNCTION IS CALLED WHEN FETCHING THE DEVICE DETAILS ***/

function getStartekInfo() {
    crossDomainAjax("https://localhost:4443/FM220/gettmpl?callback=?",
        function (result) {
            populateTextBox(result);
        });
}

function initializeStartek() {
    crossDomainAjax_new("https://localhost:4443/FM220/gettmpl?callback=?",
        function (result) {
            populateTextBoxS(result);
        });
}


function populateTextBoxS(result) {
    if (result.serialNumber != '') {
        $("#showDevice").css("display", "block");
        document.getElementById('tdSerial').value = result.serialNumber;
        document.getElementById('tdMake').value = "STARTEK";
        document.getElementById('tdModel').value = "FM220";
        document.getElementById('tdWidth').value = result.imageWidth;
        document.getElementById('tdHeight').value = result.imageHeight;
        document.getElementById('tdLocalMac').value = localMAC;
        document.getElementById('tdLocalIP').value = localIP;
        document.getElementById('txtStatus').value = 'Success';

    } else {
        document.getElementById('tdSerial').value = '';
        document.getElementById('tdMake').value = "";
        document.getElementById('tdModel').value = "";
        document.getElementById('tdWidth').value = '';
        document.getElementById('tdHeight').value = '';
        document.getElementById('tdLocalMac').value = '';
        document.getElementById('tdLocalIP').value = '';
        document.getElementById('txtStatus').value = 'Device Not Connected';
    }
}


function crossDomainAjax_new(url, callback) {
    // IE8 & 9 only Cross domain JSON GET request
    if ('XDomainRequest' in window && window.XDomainRequest !== null) {
        var xdr = new XDomainRequest(); // Use Microsoft XDR
        xdr.open('get', url);
        xdr.onload = function () {
            var dom = new ActiveXObject('Microsoft.XMLDOM'),
                JSON = $.parseJSON(xdr.responseText);
            dom.async = false;
            if (JSON == null || typeof (JSON) == 'undefined') {
                JSON = $.parseJSON(data.firstChild.textContent);
            }
            callback(JSON);
        };
        xdr.onerror = function () {
            _result = false;
        };
        xdr.send();
    } else {
        $.support.cors = true;
        $("#wait_modal").modal("show");
        $.ajax({
            url: url,
            crossDomain: true,
            contentType: 'text/plain',
            cache: false,
            dataType: 'jsonp',
            type: 'GET',
            async: false, // must be set to false
            jsonpCallback: "Jsonp_Callback",
            success: function (data, success) {
                $("#wait_modal").modal("hide");
                if (data.status == "Pl connect Fingerprint Device or remove finger" || data.serialNumber == "") {
                    $("#error_modal").modal("show");
                }

                callback(data);
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    }
}


function captureFP_new() {
    crossDomainAjax_new("https://localhost:4443/FM220/gettmpl?callback=?",
        function (result) {
            if (result.errorCode == 0) {
                $("#showData").css("display", "block");
                if (result != null && result.bMPBase64.length > 0) {
                    document.getElementById("imgFinger").src = "data:image/bmp;base64," + result.bMPBase64;
                }

                document.getElementById('txtQuality').value = result.imageDPI;
                document.getElementById('txtNFIQ').value = result.nFIQ;
                document.getElementById('txtIsoTemplate').value = result.templateBase64;
                document.getElementById('txtIsoImage').value = result.isoImgBase64;
                document.getElementById('txtRawData').value = '';
                document.getElementById('txtWsqData').value = '';
            }
            else {
                if (result.status == "Finger not placed. Time Out!" || result.status == "Capture Failed! Time Out!") {

                    $("#error_modal_timeout").modal("show");
                    window.parent.opener.location.reload();
                    window.setTimeout("window.close();", 3000);
                }

                if (result.status == "Match Failed!") {
                    $("#error_modal_nomatch").modal("show");
                    window.parent.opener.location.reload();
                    window.setTimeout("window.close();", 3000);
                }
            }
        });
}

function crossDomainAjax_capture(url, callback) {
    // IE8 & 9 only Cross domain JSON GET request
    if ('XDomainRequest' in window && window.XDomainRequest !== null) {
        var xdr = new XDomainRequest(); // Use Microsoft XDR
        xdr.open('get', url);
        xdr.onload = function () {
            var dom = new ActiveXObject('Microsoft.XMLDOM'),
                JSON = $.parseJSON(xdr.responseText);
            dom.async = false;
            if (JSON == null || typeof (JSON) == 'undefined') {
                JSON = $.parseJSON(data.firstChild.textContent);
            }
            callback(JSON);
        };
        xdr.onerror = function () {
            _result = false;
        };
        xdr.send();
    } else {
        $.support.cors = true;
        $("#wait_modal_match").modal("show");
        $.ajax({
            url: url,
            crossDomain: true,
            contentType: 'text/plain',
            cache: false,
            dataType: 'jsonp',
            type: 'GET',
            async: false, // must be set to false
            jsonpCallback: "Jsonp_Callback",
            success: function (data, success) {
                if(data.status == "Pl wait for closing of earlier process!!, avoid refresh while capturing."){

                } else {
                    $("#wait_modal_match").modal("hide");
                }
                callback(data);
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    setTimeout(function (e) {
                        crossDomainAjax_capture(url,callback);
                    },3000);
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    }
}


function initializeStartek_capture() {
    crossDomainAjax_capture("https://localhost:4443/FM220/gettmpl?callback=?",
        function (result) {
            populateTextBox_capture(result);
        });
}

function populateTextBox_capture(result) {

    if (result.bMPBase64 != '') {
        $("#showData").css("display", "block");
        document.getElementById('txtIsoTemplate').value = result.templateBase64;
        document.getElementById('imgFinger').src = "data:image/bmp;base64," + result.bMPBase64;
        document.getElementById('tdSerial').value = result.serialNumber;
        document.getElementById('tdMake').value = "STARTEK";
        document.getElementById('tdModel').value = "FM220";
        document.getElementById('tdWidth').value = result.imageWidth;
        document.getElementById('tdHeight').value = result.imageHeight;
        document.getElementById('tdLocalMac').value = localMAC;
        document.getElementById('tdLocalIP').value = localIP;
        document.getElementById('txtStatus').value = 'Success';

    } else {

        console.log(result);

        $("#wait_modal_match").modal("hide");
        $("#error_modal_timeout").modal("show");
        $("#showData").css("display", "hide");
        document.getElementById('tdSerial').value = '';
        document.getElementById('tdMake').value = "";
        document.getElementById('tdModel').value = "";
        document.getElementById('tdWidth').value = '';
        document.getElementById('tdHeight').value = '';
        document.getElementById('tdLocalMac').value = '';
        document.getElementById('tdLocalIP').value = '';
        document.getElementById('txtStatus').value = 'Device Not Connected';
        //window.parent.opener.location.reload();
        //window.setTimeout("window.close();", 3000);
    }
}