function crossDomainAjax(url, method,callback) {
    // IE8 & 9 only Cross domain JSON GET request
    if ('XDomainRequest' in window && window.XDomainRequest !== null) {
        var xdr = new XDomainRequest(); // Use Microsoft XDR
        xdr.open(method, url);
        xdr.onload = function () {
            var dom = new ActiveXObject('Microsoft.XMLDOM'),
                JSON = $.parseJSON(xdr.responseText);
            dom.async = false;
            if (JSON == null || typeof (JSON) == 'undefined') {
                JSON = $.parseJSON(data.firstChild.textContent);
            }
            callback(JSON);
        };
        xdr.onerror = function () {
            _result = false;
        };
        xdr.send();
    }
    // IE7 doesn't support cross domain request at all! :( :)
    // jQuery AJAX for other browsers
    else {
        $.support.cors = true;
        // $("#wait_modal").modal("show");
        $.ajax({
            url: url,
            crossDomain: true,
            contentType: 'text/plain',
            cache: false,

            method: method,
            async: false, // must be set to false
            // jsonpCallback: "Jsonp_Callback",
            success: function (data, success) {
                //$("#wait_modal").modal("hide");
                //console.log(data);
                data = xml2json(data)
                callback(data);
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('Not connect.\n Verify Network.');
                } else if (jqXHR.status == 404) {
                    alert('Requested page not found. [404]');
                } else if (jqXHR.status == 500) {
                    alert('Internal Server Error [500].');
                } else if (exception === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (exception === 'timeout') {
                    alert('Time out error.');
                } else if (exception === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    }
}

function xml2json(xml) {
    try {
        var obj = {};
        if (xml.children.length > 0) {
            for (var i = 0; i < xml.children.length; i++) {
                var item = xml.children.item(i);
                var nodeName = item.nodeName;

                if (typeof (obj[nodeName]) == "undefined") {
                    obj[nodeName] = xml2json(item);
                } else {
                    if (typeof (obj[nodeName].push) == "undefined") {
                        var old = obj[nodeName];

                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(xml2json(item));
                }
            }
        } else {
            obj = xml.textContent;
        }
        return obj;
    } catch (e) {
        console.log(e.message);
    }
}


function populateTextBox(result) {
    result = result.DeviceInfo;

    if (result.SerialNo != '') {
        $("#showDevice").css("display", "block");
        document.getElementById('tdSerial').value = result.SerialNo;
        document.getElementById('tdMake').value = result.DeviceMake;
        document.getElementById('tdModel').value = result.DeviceModel;

        //document.getElementById('tdLocalMac').value = localMAC;
        document.getElementById('tdLocalIP').value = localIP;
        document.getElementById('txtStatus').value = 'Success';

    } else {
        document.getElementById('tdSerial').value = '';
        document.getElementById('tdMake').value = "";
        document.getElementById('tdModel').value = "";

        // document.getElementById('tdLocalMac').value = '';
        document.getElementById('tdLocalIP').value = '';
        document.getElementById('txtStatus').value = 'Device Not Connected';
    }
}



function initializeTatvik() {
    crossDomainAjax("https://127.0.0.1:31000",'DEVICEINFO',
        function (result) {
            populateTextBox(result);

        });
}


function initializeTatvik_capture() {
    crossDomainAjax("https://127.0.0.1:31000",'CAPTUREFP',
        function (result) {
            populateTextBox_capture_tatvik(result);

        });
}

function populateTextBox_capture_tatvik(result) {
    result =result.CaptureResult;
    if (result.Image_FMRbytes != '') {
        $("#showData").css("display", "block");
        document.getElementById('txtIsoTemplate').value = result.Image_FMRbytes;


        document.getElementById('tdMake').value = "TATVIK";
        document.getElementById('tdModel').value = "TMF20";
        document.getElementById('tdWidth').value = result.ImageWidth;
        document.getElementById('tdHeight').value = result.ImageHeight;
        document.getElementById('tdLocalMac').value = localMAC;
        document.getElementById('tdLocalIP').value = localIP;
        document.getElementById('txtStatus').value = 'Success';

    } else {

        console.log(result);

        $("#wait_modal_match").modal("hide");
        $("#error_modal_timeout").modal("show");
        $("#showData").css("display", "hide");

        document.getElementById('tdMake').value = "";
        document.getElementById('tdModel').value = "";
        document.getElementById('tdWidth').value = '';
        document.getElementById('tdHeight').value = '';
        document.getElementById('tdLocalMac').value = '';
        document.getElementById('tdLocalIP').value = '';
        document.getElementById('txtStatus').value = 'Device Not Connected';
        //window.parent.opener.location.reload();
        //window.setTimeout("window.close();", 3000);
    }
}

function captureTFP() {
    crossDomainAjax("https://127.0.0.1:31000",'CAPTUREFP',
        function (result) {
           return  result.CaptureResult.Image_FMRbytes;

        });
}