$("#frmvisitdetails").validate({
        rules: {
            rsp_code: { required: true },
            dateFrom: { required: true },
        },
        messages: {
			rsp_code: { required: '<span class="star small">Please select RSP</span>' },
            dateFrom: { required: '<span class="star small">Please select date from</span>' },
        }
	});