$("#frmbioenrollment").validate({
        rules: {
            employee_type: { required: true },
            fullname: { required: true },
            gender: { required: true },
            email_id: { required: true, email: true },
            deviceType: { required: true },
            mobile: { required: true, minlength:10 },
            otpmobile: { required: true, minlength:6 },
            otpemail: { required: true, minlength:6 },
        },
        messages: {
			employee_type: { required: '<span class="star small">Please select enroll type</span>' },
			fullname: { required: '<span class="star small">Please enter Full name</span>' },
			mobile: {  required: '<span class="star small">Please enter mobile number.</span>',
            },
            gender: {  required: '<span class="star small">Please select gender.</span>',
            },
            email_id: { required: '<span class="star small">Please enter email id.</span>',
                email: '<span class="star small">Invalid email id.</span>', 
            },
            deviceType: { required: '<span class="star small">Please select bio matric device.</span>', 
            },
            otpmobile: { required: '<span class="star small">OTP ?</span>',
            },
            otpemail: { required: '<span class="star small">OTP ?</span>',
            },
        }
	});