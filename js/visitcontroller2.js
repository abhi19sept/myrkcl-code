function filldeviceTypes() {
    	$.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=biodevices",
            success: function (data) {
                $("#deviceType").html(data);
            }
        });
    }

    function initializeMantra() {
        $("#wait_modal").modal("show");
        var res = GetMFS100Info();

        if (res.httpStaus) {
            document.getElementById('txtStatus').value = res.data.ErrorDescription;
            if (res.data.ErrorCode == "0" && res.data.DeviceInfo.Make != "NONE") {
                $("#wait_modal").modal("hide");
                document.getElementById('tdSerial').value = res.data.DeviceInfo.SerialNo;
                document.getElementById('tdMake').value = res.data.DeviceInfo.Make;
                document.getElementById('tdModel').value = res.data.DeviceInfo.Model;
                document.getElementById('tdWidth').value = res.data.DeviceInfo.Width;
                document.getElementById('tdHeight').value = res.data.DeviceInfo.Height;
                document.getElementById('tdLocalMac').value = res.data.DeviceInfo.LocalMac;
                document.getElementById('tdLocalIP').value = res.data.DeviceInfo.LocalIP;
            } else {
                $("#wait_modal").modal("hide");
                $("#error_modal").modal("show");
                setDeviceInfoEmpty();
            }
        } else {
            $("#wait_modal").modal("hide");
            setDeviceInfoEmpty();
        }
        return false;
    }

    function initializeMantra_capture() {
        $("#wait_modal_match").modal("show");
        try {
            var res = CaptureFinger(quality, timeout);
            if (res.httpStaus) {
                if (res.data.ErrorCode == "0") {
                    $("#wait_modal_match").modal("hide");
                    document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;
                    document.getElementById('txtQuality').value = res.data.Quality;
                    document.getElementById('txtNFIQ').value = res.data.Nfiq;
                    document.getElementById('txtIsoTemplate').value = res.data.IsoTemplate;
                    document.getElementById('txtIsoImage').value = res.data.IsoImage;
                    document.getElementById('txtRawData').value = res.data.RawData;
                    document.getElementById('txtWsqData').value = res.data.WsqImage;
                } else {
                    $("#wait_modal_match").modal("hide");
                    $("#error_modal_timeout").modal("show");
                }
            } else {
                $("#wait_modal_match").modal("hide");
                $("#error_modal").modal("show");
            }
        } catch (e) {
            $("#wait_modal_match").modal("hide");
            $("#error_modal").modal("show");
        }

        return false;
    }
    
     /* Added by Sunil: Tatvik*/
     function initializeTatvik_capture(bytype) {
    crossDomainAjax("https://127.0.0.1:31000",'CAPTUREFP',
        function (result) {
            populateTextBox_capture_2(result);
            //setCapturedImgView(bytype);
            verifyCapturedImpression(bytype);
        });
    }
    function initializeTatvik_capture_NEW(bytype) {
    crossDomainAjax("https://127.0.0.1:31000",'CAPTUREFP',
        function (result) {
            //alert(result);
            populateTextBox_capture_2(result);
            //setCapturedImgView(bytype);
            verifyCapturedImpression_2(bytype);
        });
    }
    function initializeTatvik_capture_0007() {
        //$("#wait_modal_match").modal("show");
        try {

                result =result.CaptureResult;
                if (result.Image_FMRbytes != '')  
                {
                    $("#wait_modal_match").modal("hide");
                    document.getElementById('txtIsoTemplate').value = result.Image_FMRbytes;
                    document.getElementById('tdMake').value = "TATVIK";
                    document.getElementById('tdModel').value = "TMF20";
                    document.getElementById('tdWidth').value = result.ImageWidth;
                    document.getElementById('tdHeight').value = result.ImageHeight;
                    document.getElementById('tdLocalMac').value = localMAC;
                    document.getElementById('tdLocalIP').value = localIP;
                    document.getElementById('txtStatus').value = 'Success';
                } 
                
                else {
                    $("#wait_modal_match").modal("hide");
                    $("#error_modal_timeout").modal("show");
                }

        } catch (e) {
            $("#wait_modal_match").modal("hide");
            $("#error_modal").modal("show");
        }

        return false;
    }
    /* Added by Sunil: Tatvik*/

    function setDeviceInfoEmpty() {
        $(".bioerr").empty();
        $(".biomval").val('');
        $('#txtStatus').val('Device Not Connected');
        resetCapture('');
    }

    function resetCapture(bytype) {
        $('#txtStatus').val('');
        document.getElementById('imgFinger').src = "images/biopic.png";
        $('#txtQuality').val('');
        $('#txtNFIQ').val('');
        $('#txtIsoTemplate').val('');
        $('#txtIsoImage').val('');
        $('#txtRawData').val('');
        $('#txtWsqData').val('');
        if (bytype != '') {
            setCapturedImgView(bytype);
        } else {
            setCapturedImgView('itgk');
            setCapturedImgView('visitor');
            setCapturedImgView('bio');
        }
    }

    function setCapturedImgView(bytype) {
        if ($('#match_' + bytype).length) {
            $('#match_' + bytype).empty();
            $('#matched_' + bytype).val('');
            document.getElementById('imgFinger_' + bytype).src = document.getElementById('imgFinger').src;
        }
    }

    function reinitializeStartek(oldserial, bytype) {
    crossDomainAjax_new("https://localhost:4443/FM220/gettmpl?callback=?",
        function (result) {
            populateTextBox(result);
            reverifyBioMachine(oldserial, bytype);
        });
    }
    
    /* Added tativik for frm_visit_confirm*/
    function reinitializeTatvik(oldserial, bytype) {
    crossDomainAjax("https://127.0.0.1:31000",'CAPTUREFP',
        function (result) {
            //alert(result);
            populateTextBox_capture_2(result);
            reverifyBioMachine(oldserial, bytype);

        });
    }
    /* Added tativik for frm_visit_confirm*/

    function getDeviceInfo(dname) {
    	switch (dname) {
    		case 'mantra': initializeMantra();
    			break;
    		case 'startek': initializeStartek();
    			break;
    		case 'cogent': initializeCogent();
    			break;
                case 'tatvik': initializeTatvik();
                        break;
    	}
    }

    function matchISOTemplates(data, needMatched, bytype) {
        var isvalid = 0;
        data = $.parseJSON(data);
        //alert(data);
        var device = deviceType.value;
        //alert(device);
        if (device == 'mantra') {
            var isotemplate = document.getElementById('txtIsoTemplate').value;
            $.each(data, function (i, item) {
                var flag = mantraImpressionValidate(isotemplate, data[i].BioMatric_ISO_Template, needMatched, data[i].id);
                if (flag > 0) {
                    isvalid = flag;
                }
            });
        } 
        
        else if (device == 'tatvik1') 
        {
            
                var isotemplate = document.getElementById('txtIsoTemplate').value.toString();
                //$("#wait_modal_match").modal("show");
                $.each(data, function (i, item) {
                    tatvikImpressionValidate(isotemplate, data[i].BioMatric_ISO_Template, needMatched, data[i].id);
                });

        } 
        
        else if (device == 'startek') {
            var isvalid = 'hold_on';
            $('#flag').val(0);
            if(typeof data[0] !== 'undefined') {
                $("#wait_modal_match").modal("show");
                startekImpressionValidate(isotemplate, data, needMatched, bytype, 0);
            };
        }
        //alert('sunil'); alert('sunil2');
       
        return isvalid;
    }


    function tatvikImpressionValidate(isotemplate, data, needMatched, bytype) {
        

                    var matchtemplates =  "<MatchTemplates>"+
                        "<ClaimedTemplateData>" + data + "</ClaimedTemplateData>"+
                        "<ReferenceTemplateData>"+ isotemplate +"</ReferenceTemplateData>"+
                        "</MatchTemplates>";
                    $.ajax({
                        url: "https://127.0.0.1:31000",

                        data: matchtemplates,
                        method: "MATCHFPTEMPLATE",
                        responsetype:'xml',
                        // jsonpCallback: "Jsonp_Callback",
                        success: function (data, success) {
                            var rd = xml2json(data);
                            var rd2 = rd.MatchResult;
                            console.log(rd2)
                            if (rd2.MatchStatus == 'Match Failed') {

                                //$("#error_modal_nomatch").modal("show");
                            }
                            else {

                                if (rd2.MatchStatus == "Finger not placed. Time Out!" || rd2.MatchStatus == "Capture Failed! Time Out!")
                                    //$("#error_modal_timeout").modal("show");

                                if (rd2.MatchStatus == "Match succesful") {

                                   var isvalid = bytype;
                                   return isvalid;
                                }
                            }
                        },
                        error: function (jqXHR, exception) {
                            if (jqXHR.status === 0) {
                                alert('Not connect.\n Verify Network.');
                            } else if (jqXHR.status == 404) {
                                alert('Requested page not found. [404]');
                            } else if (jqXHR.status == 500) {
                                alert('Internal Server Error [500].');
                            } else if (exception === 'parsererror') {
                                alert('Requested JSON parse failed.');
                            } else if (exception === 'timeout') {
                                alert('Time out error.');
                            } else if (exception === 'abort') {
                                alert('Ajax request aborted.');
                            } else {
                                alert('Uncaught Error.\n' + jqXHR.responseText);
                            }
                        }
                    });
    }


    function verifyCapturedImpression_2(bytype) {
        if (! $('#processvisitId').length) return;
        var matchid = '#match_' + bytype;
        var visitId = processvisitId.value;
        var matchedId = 0;
        $(matchid).empty();
        $(matchid).append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Matching...</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=get" + bytype + "BioPics&visitId=" + visitId,
            success: function (data) {
                $('#matched_' + bytype).val('');
                if (data != '0') {
                    data = $.parseJSON(data);
                    var isotemplate = document.getElementById('txtIsoTemplate').value.toString();
                    //tatvikImpressionValidate(isotemplate, data, 1, 1);
                    //matchedId = matchISOTemplates(data, 1, bytype);
                     $.each(data, function (i, item) {
                            var matchtemplates =  "<MatchTemplates>"+
                                "<ClaimedTemplateData>" + data[i].BioMatric_ISO_Template + "</ClaimedTemplateData>"+
                                "<ReferenceTemplateData>"+ isotemplate +"</ReferenceTemplateData>"+
                                "</MatchTemplates>";
                            var isvalid= data[i].id;
                            //alert(isvalid);
                            $.ajax({
                                url: "https://127.0.0.1:31000",

                                data: matchtemplates,
                                method: "MATCHFPTEMPLATE",
                                responsetype:'xml',
                                // jsonpCallback: "Jsonp_Callback",
                                success: function (data, success) {
                                    var rd = xml2json(data);
                                    var rd2 = rd.MatchResult;
                                    //alert(rd2.MatchStatus);
                                    //console.log(rd2)
                                    if (rd2.MatchStatus == "Match succesful") {
                                            if(bytype=='itgk'){
                                            $('#match_itgk').empty();
                                            //$('#match_visitor').empty();
                                            $('#matched_' + bytype).val(isvalid);
                                            $(matchid).append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span style='color:green;'>Matched!!</span></p>");
                                            return;   
                                            }
                                            if(bytype=='visitor'){
                                            //$('#match_itgk').empty();
                                            $('#match_visitor').empty();
                                            $('#matched_' + bytype).val(isvalid);
                                            $(matchid).append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span style='color:green;'>Matched!!</span></p>");
                                            return;   
                                            }
                                            
                                        }
                                     if (rd2.MatchStatus == 'Match Failed') {
                                         
                                         //return;
                                         if(bytype=='itgk'){
                                            $('#match_itgk').empty();
                                            //$('#match_visitor').empty();
                                            $('#matched_' + bytype).val(isvalid);
                                            $(matchid).append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Not Matched!!</span></p>");
                                            return;   
                                            }
                                            if(bytype=='visitor'){
                                            //$('#match_itgk').empty();
                                            $('#match_visitor').empty();
                                            $('#matched_' + bytype).val(isvalid);
                                            $(matchid).append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Not Matched!!</span></p>");
                                            return;   
                                            }
                                     }
//                                    else{
//                                        $('#match_itgk').empty();
//                                        $(matchid).append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Not Matched!!</span></p>");
//                                    }
                                    
                                },
                                error: function (jqXHR, exception) {
                                    if (jqXHR.status === 0) {
                                        alert('Not connect.\n Verify Network.');
                                    } else if (jqXHR.status == 404) {
                                        alert('Requested page not found. [404]');
                                    } else if (jqXHR.status == 500) {
                                        alert('Internal Server Error [500].');
                                    } else if (exception === 'parsererror') {
                                        alert('Requested JSON parse failed.');
                                    } else if (exception === 'timeout') {
                                        alert('Time out error.');
                                    } else if (exception === 'abort') {
                                        alert('Ajax request aborted.');
                                    } else {
                                        alert('Uncaught Error.\n' + jqXHR.responseText);
                                    }
                                }
                            });
                            //break Loop1;
                        });
                    
                    
                    
                    //alert(matchedId);
//                    if (matchedId > 0) {
//                        $('#matched_' + bytype).val(matchedId);
//                        $(matchid).append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span style='color:green;'>Matched!!</span></p>");
//                    } else {
//                        $(matchid).append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Not Matched!!</span></p>");
//                    }
                    
                }
                
            }
        });
    }
    
    function verifyCapturedImpression(bytype) {
        if (! $('#processvisitId').length) return;
        var matchid = '#match_' + bytype;
        var visitId = processvisitId.value;
        var matchedId = 0;
        $(matchid).empty();
        $(matchid).append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Matching...</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=get" + bytype + "BioPics&visitId=" + visitId,
            success: function (data) {//alert(data);
                $('#matched_' + bytype).val('');
                if (data != '0') {
                    matchedId = matchISOTemplates(data, 1, bytype);
                    //alert(matchedId);
                    if (matchedId != 'hold_on' && matchedId >= 1) {
                        $('#matched_' + bytype).val(matchedId);
                    }
                }
                if (matchedId != 'hold_on') {//alert('sunil');
                    markedMatched(bytype, matchedId, '');
                }
            }
        });
    }

    function markedMatched(bytype, matchedId, modalId) {//alert('hello');//alert(matchedId);
        $("#matching_records").modal("hide");
        if (bytype == 'bio') {
            if (modalId != '') {
                $("#" + modalId).modal("show");
            } else {
                var x = deviceType.value;
                if (x = 'startek') {
                    initializeStartekCapture(bytype, matchedId);
                } else {
                    registerBioImpression(matchedId);
                }
            }
            return;
        }

        var matchid = '#match_' + bytype;
        $(matchid).empty();
        
        if (matchedId > 0) {
            $('#matched_' + bytype).val(matchedId);
            $(matchid).append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span style='color:green;'>Matched!!</span></p>");
        } else {
            $(matchid).append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Not Matched!!</span></p>");
        }
    }

    function mantraImpressionValidate(isotemplate, storedISOTemplate, needMatched, existDbId) {
        try {
            var flag = 0;
            var res = VerifyFinger(isotemplate, storedISOTemplate);
            if (res.httpStaus) {
                if (res.data.Status) {
                    flag = existDbId;
                } else {
                    if (res.data.ErrorCode != "0") {
                        alert("Error description = " + res.data.ErrorDescription);
                    } /*else {
                        alert("Finger not matched");
                        flag = 1;
                    }*/
                }
            } else {
                alert(res.err);
            }

            if (flag) {
                $("#matching_records").modal("hide");
                if (needMatched == 0) {
                    $("#already_enrolled").modal("show");
                }
            }
        } catch (e) {
            alert(e);
            flag = 0;
        }

        return flag;
    }

    function crossDomainAjaxStartexCapture(url, callback) {
        // IE8 & 9 only Cross domain JSON GET request
        if ('XDomainRequest' in window && window.XDomainRequest !== null) {
            var xdr = new XDomainRequest(); // Use Microsoft XDR
            xdr.open('get', url);
            xdr.onload = function () {
                var dom = new ActiveXObject('Microsoft.XMLDOM'),
                    JSON = $.parseJSON(xdr.responseText);
                dom.async = false;
                if (JSON == null || typeof (JSON) == 'undefined') {
                    JSON = $.parseJSON(data.firstChild.textContent);
                }
                callback(JSON);
            };
            xdr.onerror = function () {
                _result = false;
            };
            xdr.send();
        } else {
            $.support.cors = true;
            $.ajax({
                url: url,
                crossDomain: true,
                contentType: 'text/plain',
                cache: false,
                dataType: 'jsonp',
                type: 'GET',
                async: false, // must be set to false
                jsonpCallback: "Jsonp_Callback",
                success: function (data, success) {
                    callback(data);
                },
                error: function (jqXHR, exception) {
                    if (jqXHR.status === 0) {
                        alert('Not connect.\n Verify Network.');
                    } else if (jqXHR.status == 404) {
                        alert('Requested page not found. [404]');
                    } else if (jqXHR.status == 500) {
                        alert('Internal Server Error [500].');
                    } else if (exception === 'parsererror') {
                        alert('Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        alert('Time out error.');
                    } else if (exception === 'abort') {
                        alert('Ajax request aborted.');
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText);
                    }
                }
            });
        }
    }

    function startekImpressionValidate(isotemplate, data, needMatched, bytype, i) {
        crossDomainAjaxStartexCapture("https://localhost:4443/FM220/GetMatchResult?MatchTmpl=" + encodeURIComponent(data[i].BioMatric_ISO_Template) + "&callback=?",
        function (result) {
            var flag = $('#flag').val();
            if (result.errorCode == 0) {
                var isvalid = data[i].id;
                if (flag == 0) $('#flag').val(isvalid);
            } else {
                if (result.status == "Finger not placed. Time Out!" || result.status == "Capture Failed! Time Out!") {
                    markedMatched(bytype, 0, 'error_modal_timeout');
                    if (flag == 0) $('#flag').val(0);
                    return false;
                } else if (result.status == "Match Failed!") {
                    /*** DUPLICITY CHECKED ***/
                }
            }

            i++;
            var matchedId = $('#flag').val();
            var modal = (matchedId > 0 && needMatched == 0) ? 'already_enrolled' : '';
            if (modal == '' && typeof data[i] !== 'undefined') {
                startekImpressionValidate(isotemplate, data, needMatched, bytype, i);
            } else {
                $("#wait_modal_match").modal("hide");
                markedMatched(bytype, matchedId, modal);
            }
        });
    }

    function initializeStartekCapture(bytype, matchedId) {
        crossDomainAjax_capture("https://localhost:4443/FM220/gettmpl?callback=?",
        function (result) {
            populateTextBox_capture(result);
            setCapturedImgView(bytype);
            if (matchedId == '') {
                verifyCapturedImpression(bytype);
            } else {
                registerBioImpression(matchedId);
            }
        });
    }

    function initializeCapture(bytype) {
        var x = deviceType.value;
        switch (x) {
            case "mantra": initializeMantra_capture();
                setCapturedImgView(bytype);
                verifyCapturedImpression(bytype);
                break;
            case "startek": initializeStartekCapture(bytype, '');
                break;
            case "cogent": intitializeCogent_capture();
                break;
           case "tatvik1": initializeTatvik_capture(bytype);
                break;
           case "tatvik": initializeTatvik_capture_NEW(bytype);
                            //setCapturedImgView(bytype);
                            //verifyCapturedImpression(bytype);
                break;
            default: resetCapture('');
                break;
        }
    }

    function reDetectMachine(bytype) {
        var oldserial = $('#tdSerial').val();
        if (oldserial == '') {
            setDeviceInfoEmpty();
            return;
        }
        var device = deviceType.value;
        if (device == 'mantra') {
            getDeviceInfo(device);
            reverifyBioMachine(oldserial, bytype);
        } else if (device == 'startek') {
            reinitializeStartek(oldserial, bytype);
        }
         else if (device == 'Tatvik' || device == 'tatvik') {
            getDeviceInfo(device);
            reverifyBioMachine(oldserial, bytype);
            //reinitializeTatvik(oldserial, bytype);
        }
    }

    function reverifyBioMachine(oldserial, bytype) {
        var newserial = $('#tdSerial').val();
        if (newserial == '') {
            $.wait(5000).then(verifyBioMachine(oldserial, bytype));
        } else {
            verifyBioMachine(oldserial, bytype);
        }
    }

    function verifyBioMachine(oldserial, bytype) {//alert(bytype);
        var newserial = $('#tdSerial').val();
        if (oldserial != '' && oldserial == newserial) {
            $('#frmvisit-response').empty();
            $('.bioerr').empty();
            $('.bioerr').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfvisitmaster.php",
                data: "action=verifybiodevice&serial=" + newserial,
                success: function (data) {//salert(data);
                    $('.bioerr').empty();
                    if (data == '1') {
                        initializeCapture(bytype);
                    } else {
                        $('#frmvisit-response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Attached device is not registered for this ITGK!!</span></p>")
                    }
                }
            });
        } else {
            setDeviceInfoEmpty();
            alert('Device miss match!!');
        }
    }