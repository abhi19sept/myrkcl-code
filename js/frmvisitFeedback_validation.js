$("#frmvisitdetails").validate({
        rules: {
            arearemark: { required: true },
            contactremark: { required: true },
            ddlMunicipalName: { required: true },
            ddlWardno: { required: true },
            ddlPanchayat: { required: true },
            ddlGramPanchayat: { required: true },
            ddlVillage: { required: true, min: 1 },
            police_station: { required: true },
            pincode: { required: true, min:6, maxlength: 6, minlength: 6 },
            owner_name: { required: true },
            counsellor_name: { required: true },
            faculty_name: { required: true },
            assistant_name: { required: true },
            ownerMobileNumber: { required: true, minlength:10, maxlength: 10 },
            counsellorMobileNumber: { required: true, minlength:10, maxlength: 10 }, 
            facultyMobileNumber: { required: true, minlength:10, maxlength: 10 },
            assistantMobileNumber: { required: true, minlength:10, maxlength: 10 },
            receptionArea: { required: true },
            receptionSeatingCapacity: { required: true },
            receptionRemark: { required: true },
            waitingArea: { required: true },
            waitingAreaCapacity: { required: true },
            waitingRemark: { required: true },
            theoryArea: { required: true },
            theoryAreaCapacity: { required: true },
            theoryAreaRemark: { required: true },
            labArea: { required: true },
            labCapacity: { required: true },
            labRemark: { required: true },
            toiletRemark: { required: true },
            waterRemark: { required: true },
            printerRemark: { required: true },
            scannerRemark: { required: true },
            mbpsRemark: { required: true },
            hphoneQty: { required: true },
            lmsRemark: { required: true },
            teachRemark: { required: true },
            assisRemark: { required: true },
            facultyRemark: { required: true },
            learningRemark: { required: true },
            infraqtyRemark: { required: true },
            txtOverAllRemark: { required: true },
            processer1: { required: true },
            processer2: { required: true },
            processer3: { required: true },
            processer4: { required: true },
            ram1: { required: true },
            ram2: { required: true },
            ram3: { required: true },
            ram4: { required: true },
            hdd1: { required: true },
            hdd2: { required: true },
            hdd3: { required: true },
            hdd4: { required: true },
            qty1: { required: true },
            qty2: { required: true },
            qty3: { required: true },
            qty4: { required: true },
            typesc1: { required: true },
            typesc2: { required: true },
            typesc3: { required: true },
            typesc4: { required: true },
            systemRemark1: { required: true },
            systemRemark2: { required: true },
            systemRemark3: { required: true },
            systemRemark4: { required: true },
            uploadImage7: { required: true },
            uploadImage8: { required: true },
            uploadImage9: { required: true },
            uploadImage6: { required: true },
        },
        messages: {				
			arearemark: { required: '<span class="star">Area remark is required</span>' },
            contactremark: { required: '<span class="star">Contact remark is required</span>' },
            ddlMunicipalName: { required: '<span class="star">Please select Municipality Name.</span>' },
            ddlWardno: { required: '<span class="star">Please select ward name.</span>' },
            ddlPanchayat: { required: '<span class="star">Please select panchayat samiti.</span>' },
            ddlGramPanchayat: { required: '<span class="star">Please select gram panchayat.</span>' },
            ddlVillage: { required: '<span class="star">Please select your Village.</span>',
                          min: '<span class="star">Please select your Village.</span>'
                         },
            police_station: { required: '<span class="star">Please enter area police station name</span>' },
            pincode: { required: '<span class="star">Please enter pincode</span>',
                minlength:'<span class="star">Pincode must be 6 digits</span>',
                maxlength:'<span class="star">Pincode must be 6 digits</span>',
                min:'<span class="star">Pincode must be 6 digits</span>'},
            owner_name: { required: '<span class="star">Please enter owner name</span>' },
            counsellor_name: { required: '<span class="star">Please enter counsellor name</span>' },
            faculty_name: { required: '<span class="star">Please enter faculty name</span>' },
            assistant_name: { required: '<span class="star">Please enter assistant name</span>' },
            ownerMobileNumber: { required: '<span class="star">Please enter owner mobile number</span>', 
                minlength:'<span class="star">Mobile number must be 10 digits</span>', 
                maxlength: '<span class="star">Mobile number must be 10 digits</span>' },
            facultyMobileNumber: { required: '<span class="star">Please enter faculty mobile number</span>', 
                minlength:'<span class="star">Mobile number must be 10 digits</span>', 
                maxlength: '<span class="star">Mobile number must be 10 digits</span>' },
            counsellorMobileNumber: { required: '<span class="star">Please enter counsellor mobile number</span>', 
                minlength:'<span class="star">Mobile number must be 10 digits</span>', 
                maxlength: '<span class="star">Mobile number must be 10 digits</span>' },
            assistantMobileNumber: { required: '<span class="star">Please enter assistant mobile number</span>', 
            minlength:'<span class="star">Mobile number must be 10 digits</span>', 
            maxlength: '<span class="star">Mobile number must be 10 digits</span>' },
            receptionArea: {  required: '<span class="star">Please enter Sq.Ft Area</span>' },
            receptionSeatingCapacity: { required: '<span class="star">Please enter area capacity.</span>' },
            receptionRemark: { required: '<span class="star">Please enter area remark.</span>' },
            waitingArea: { required: '<span class="star">Please enter Sq.Ft Area</span>' },
            waitingAreaCapacity: { required: '<span class="star">Please enter area capacity.</span>' },
            waitingRemark: { required: '<span class="star">Please enter area remark.</span>' },
            theoryArea: { required: '<span class="star">Please enter Sq.Ft Area</span>' },
            theoryAreaCapacity: { required: '<span class="star">Please enter area capacity.</span>' },
            theoryAreaRemark: { required: '<span class="star">Please enter area remark.</span>' },
            labArea: { required: '<span class="star">Please enter Sq.Ft Area</span>' },
            labCapacity: { required: '<span class="star">Please enter area capacity.</span>' },
            labRemark: { required: '<span class="star">Please enter area remark.</span>' },
            toiletRemark: { required: '<span class="star">Please enter area remark.</span>' },
            waterRemark: { required: '<span class="star">Please enter area remark.</span>' },
            printerRemark: { required: '<span class="star">Please enter printer remark.</span>' },
            scannerRemark: { required: '<span class="star">Please enter scanner remark.</span>' },
            mbpsRemark: { required: '<span class="star">Please enter internet speed in mbps.</span>' },
            hphoneQty: { required: '<span class="star">Please enter qty. of head phones.</span>' },
            lmsRemark: { required: '<span class="star">Please enter remark.</span>' },
            teachRemark: { required: '<span class="star">Please enter remark.</span>' },
            assisRemark: { required: '<span class="star">Please enter remark.</span>' },
            facultyRemark: { required: '<span class="star">Please enter remark.</span>' },
            learningRemark: { required: '<span class="star">Please enter remark.</span>' },
            infraqtyRemark: { required: '<span class="star">Please enter remark.</span>' },
            txtOverAllRemark: { required: '<span class="star">Please enter overall feedback remark.</span>' },
            processer1: { required: '<span class="star">Please enter processer details.</span>' },
            processer2: { required: '<span class="star">Please enter processer details.</span>' },
            processer3: { required: '<span class="star">Please enter processer details.</span>' },
            processer4: { required: '<span class="star">Please enter processer details.</span>' },
            ram1: { required: '<span class="star">Please enter RAM capacity.</span>' },
            ram2: { required: '<span class="star">Please enter RAM capacity.</span>' },
            ram3: { required: '<span class="star">Please enter RAM capacity.</span>' },
            ram4: { required: '<span class="star">Please enter RAM capacity.</span>' },
            hdd1: { required: '<span class="star">Please enter HDD capacity.</span>' },
            hdd2: { required: '<span class="star">Please enter HDD capacity.</span>' },
            hdd3: { required: '<span class="star">Please enter HDD capacity.</span>' },
            hdd4: { required: '<span class="star">Please enter HDD capacity.</span>' },
            qty1: { required: '<span class="star">Please enter qty.</span>' },
            qty2: { required: '<span class="star">Please enter qty.</span>' },
            qty3: { required: '<span class="star">Please enter qty.</span>' },
            qty4: { required: '<span class="star">Please enter qty.</span>' },
            typesc1: { required: '<span class="star">Please fill type specification.</span>' },
            typesc2: { required: '<span class="star">Please fill type specification.</span>' },
            typesc3: { required: '<span class="star">Please fill type specification.</span>' },
            typesc4: { required: '<span class="star">Please fill type specification.</span>' },
            systemRemark1: { required: '<span class="star">Please enter remark.</span>' },
            systemRemark2: { required: '<span class="star">Please enter remark.</span>' },
            systemRemark3: { required: '<span class="star">Please enter remark.</span>' },
            systemRemark4: { required: '<span class="star">Please enter remark.</span>' },  
            uploadImage7: { required: '<span class="star">Please Upload.</span>' },
            uploadImage8: { required: '<span class="star">Please Upload.</span>' },
            uploadImage9: { required: '<span class="star">Please Upload.</span>' },
            uploadImage6: { required: '<span class="star">Please Upload.</span>' },
        }
	});