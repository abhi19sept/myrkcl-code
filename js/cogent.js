﻿window.myCallback = function (data) {
    console.log(data);
};

/*** THIS FUNCTION IS CALLED WHEN FETCHING THE DEVICE DETAILS ***/


var url = "https://localhost:62016/MMMCSD200Service/ActionServlet?action=";

function initializeCogent() {
    var strMessage = "Initialize";
    $.ajax({
        type: "GET",
        url: url + strMessage,
        dataType: 'jsonp',
        crossDomain: true,
        success: function (jsonObj) {
            if (jsonObj.Message === "Initialization Successful.")
                getCogentDeviceInfo();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#error_modal").modal("show");
        }
    });
}

function getCogentDeviceInfo() {
    $("#wait_modal").modal("show");
    var strMessage = "DeviceInfo";
    $.ajax({
        url: url + strMessage,
        dataType: 'jsonp',
        crossDomain: true,
        jsonp: 'callback',
        jsonpCallback: 'myCallback',
        success: function (jsonObj) {
            var msg = jsonObj.Message;
            var fields = msg.split('\n');
            var refine0 = fields[0].split(': ');
            var refine1 = fields[1].split(': ');
            $("#showDevice").css("display", "block");
            $("#wait_modal").modal("hide");
            document.getElementById('tdSerial').value = refine1[1];
            document.getElementById('tdMake').value = "Cogent";
            document.getElementById('tdModel').value = refine0[1];
            document.getElementById('tdWidth').value = '320';
            document.getElementById('tdHeight').value = '480';
            document.getElementById('tdLocalMac').value = localMAC;
            document.getElementById('tdLocalIP').value = localIP;
            document.getElementById('txtStatus').value = 'Success';
        },
        error: function (xhr, status, error) {
            $("#wait_modal").modal("hide");
            $("#error_modal").modal("show");
            document.getElementById('tdSerial').value = '';
            document.getElementById('tdMake').value = "";
            document.getElementById('tdModel').value = "";
            document.getElementById('tdWidth').value = '';
            document.getElementById('tdHeight').value = '';
            document.getElementById('tdLocalMac').value = '';
            document.getElementById('tdLocalIP').value = '';
            document.getElementById('txtStatus').value = 'Device Not Connected';

        }
    });
}

var byteIsoImage = null;

function intitializeCogent_capture(){
    $("#wait_modal_match").modal("show");
    var strMessage = "Capture&image=1";
    $.ajax({
        url: url + strMessage,
        dataType: 'jsonp',
        crossDomain: true,
        jsonp: 'callback',
        jsonpCallback: 'myCallback',
        success: function (jsonObj) {

            if(jsonObj.Message === "Error: Capture Timeout."){
                $("#wait_modal_match").modal("hide");
                $("#errorText").html('Timeout, Please try again');
                $("#errorModal_Custom").modal('show');
                window.parent.opener.location.reload();
                window.setTimeout("window.close();", 3000);
            } else {
                byteIsoImage = jsonObj.byteIsoImage;

                $("#wait_modal_match").modal("hide");
                $("#showData").css("display","block");
                $("#txtIsoTemplate").val(jsonObj.byteIsoImage);
                document.getElementById('imgFinger').src = "data:image/png;base64," + jsonObj.byteImage;
                document.getElementById('txtQuality').value = '';
                document.getElementById('txtNFIQ').value = '';
                document.getElementById('txtIsoImage').value = jsonObj.byteFMRImage;
                document.getElementById('txtRawData').value = '';
                document.getElementById('txtWsqData').value = '';
            }
        },
        error: function (xhr, status, error) {
            $("#wait_modal_match").modal("hide");
            $("#error_modal_timeout").modal("show");
            $("#showData").css("display","hide");
        }
    });
}



function match_cogent(bim, data) {
}