window.BpInputMask = (function bpInputMask($) {
  const defaultOptions = {
    placeholder: ' ',
    showMaskOnFocus: false,
    showMaskOnHover: false,
    definitions: {
      '!': {
        validator: '[0-1]',
        cardinality: 1,
        prevalidator: null,
      },
      '@': {
        validator: '[0-2]',
        cardinality: 1,
        prevalidator: null,
      },
      '#': {
        validator: '[0-3]',
        cardinality: 1,
        prevalidator: null,
      },
      $: {
        validator: '[1]',
        cardinality: 1,
        prevalidator: null,
      },
      '%': {
        validator: '[1-2]',
        cardinality: 1,
        prevalidator: null,
      },
      '^': {
        validator: '[4-5]',
        cardinality: 1,
        prevalidator: null,
      },
      '&': {
        validator: '(4|8)',
        cardinality: 1,
        prevalidator: null,
      },
      '~': {
        validator: '[0-9a-zA-Z_\.]',
        cardinality: 1,
        prevalidator: null,
      },
      '?': {
        validator: '[0-9a-zA-Z]',
        cardinality: 1,
        prevalidator: null,
      },
    },
  };

  const maskOptions = {
    currency: {
      alias: 'currency',
      rightAlign: false,
      removeMaskOnSubmit: true,
    },
    'one-letter': {
      mask: 'A',
    },
    city: {
      mask: 'A[a| |-|.|\']{1,24}',
    },
    'zero-to-one': {
      mask: '!',
    },
    'zero-to-two': {
      mask: '@',
    },
    'number-one': {
      mask: '$',
    },
    'one-to-two': {
      mask: '%',
    },
    'four-to-five': {
      mask: '^',
    },
    'four-or-eight': {
      mask: '&',
    },
    'one-or-two-digits': {
      mask: '9[9]',
    },
    'two-digits': {
      mask: '99',
    },
    'three-digits': {
      mask: '999',
    },
    'four-digits': {
      mask: '9999',
    },
    'five-digits': {
      mask: '99999',
    },
    'six-digits': {
      mask: '999999',
    },
    'account-number': {
      mask: '9{8,18}',
    },
    'eight-number': {
      mask: '99999999',
    },
    'ten-number': {
      mask: '9999999999',
    },
    ssn: {
      mask: '999-99-9999',
      removeMaskOnSubmit: true,
    },
    premium: {
      mask: '9{1,6}[.9{1,4}]',
    },
    percent: {
      mask: '9{1,2}[.9{1,2}]',
    },
    phone: {
      mask: '(999) 999-9999',
      removeMaskOnSubmit: true,
    },
    adhar: {
      mask: '999999999999',
      removeMaskOnSubmit: true,
    },
    bhamashah: {
      mask: '9999999999999',
      removeMaskOnSubmit: true,
    },
    'date-mm-dd-yyyy': {
      mask: 'm/d/y',
    },
    'signed-decimal': {
      mask: '[-]9{1,6}[.9{1,4}]',
    },
    'phone-number': {
      mask: '999-999-9999',
    },
    'charonly': {
      mask: 'A[a| |]{1,100}',
    },
    'ssoId': {
      mask: '~{1,50}',
    },
    'alpha': {
      mask: '?{1,50}',
    },
    'chars': {
      mask: 'A[a]{1,100}',
    },
  };

  const me = {
    maskAll: (selectors) => {
      selectors.forEach((item) => {
        me.mask(item);
      });
    },

    mask: (elem) => {
      const mask = $(elem).data('mask');
      $(elem).inputmask(
        $.extend({}, defaultOptions, maskOptions[mask]),
      );
    },
  };

  $(() => {
    $('[data-mask]').each(function mask() {
      me.mask(this);
    });
  });

  return me;
}(jQuery));
