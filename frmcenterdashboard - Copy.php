<?php ob_start();
$title = "Center Dashboard ";
include ('header.php');
include ('root_menu.php');
//echo $_SESSION['User_UserRoll'];//die;
//echo "<pre>";print_r($_SESSION);
if ($_SESSION['User_UserRoll'] == 1 || $_SESSION['User_UserRoll'] == 11 || $_SESSION['User_UserRoll'] == 4 || $_SESSION['User_UserRoll'] == 7 || $_SESSION['User_UserRoll'] == 14) {
    // you are in
} else {
    header('Location: index.php');
    exit;
}

//echo "<pre>";print_r($_SESSION);
?>
<link rel="stylesheet" href="css/profile_style.css">

<style>
    .exampannelNew1 {width: 16%;}
    .exampannelNew2 { 
        float: left;
        margin-right: 5px;
        width: 32%;
    }
    .examname{ 
        font-weight: bold;
        text-align: left;
        width: 80%;
    }
    .persdetablog1 {clear: both;}
    .persdetacont span { width: 37%;}
    .persdetacont p { width: 50%;}
    .division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 15px;
        color: #575c5f;
        margin-bottom: 20px;
        font-weight: bold;
        padding-top: 15px;
        width: 96%; 

    }
    .peremail { width: 100%}
    .peremail span { width: 18%;}

    .marks_number {
        background-color: #f1f1f1;
        border: 1px solid #dddddd;
        border-radius: 5px;
        float: left;
        font-family: Verdana;
        font-size: 12px;
        margin: 10px 10px 0 15px;
        min-height: 33px;
        padding: 7px 5px 7px 10px;
        text-align: center;
        width: 100%;
    }

    .exampannelNew3  {
        margin-left: 15px;margin-right: 0px; 
    }

    .emailmobileupdate {
        font-weight: bold;
        text-align: center;
        width: 96%;
    }
    .exampannelNewBU { /*  for block unblock*/
        float: left;
        width: 12%;
        text-align: center;
        margin-left: 6px;
        margin-right: 0px;
    }
    .font11 {
        font-size: 11px;
    }
    .exampannelNewHeading {
        float: left;
        width: 11%;
        float: left;
/*        margin-left: 11px;*/
    }

    .persdetacontN span { width: 30%;}
    .persdetacontN p { width: 65%;}
</style>


  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="js/jquery-ui.js"></script>
    <script>
  $( function() {
    $( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
    $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
  } );
  </script>
 
  <style> 
      /* This css is for tab setting only*/
      .ui-tabs-vertical { width: 79em; }
      .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 15em; }
      .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
      .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
      .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
      .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; }

      /* This css is for page setup*/
/*      .centerdash{ background-color: #547382!important;} */
      .ui-state-active,
      .ui-widget-content .ui-state-active, 
      .ui-widget-header .ui-state-active, 
      a.ui-button:active, .ui-button:active, 
      .ui-button.ui-state-active:hover{ background-color: #399CFF!important;} 
      .persdeta span{ width: 100%; background-color: #399CFF; padding: 8px 20px;}
      .persdeta{ background-color: #fff; border-bottom: 1px solid #547382;border-top: none; border-right: none;}
      #triangle-bottomleft {  border-bottom: 40px solid #399CFF; border-right: 50px solid transparent;height: 0;width: 55%; margin-top: 0px}
      .bdrtop{border-top: 1px solid #399CFF} 
      .maindiv{ width: 98%; float: left; border:1px solid #399CFF; border-top: none; min-height: auto; padding: 0px 0px 20px 0px;}
      .ui-tabs-vertical .ui-tabs-nav{ margin-top: 14px;}
      .persdetablog{ padding: 15px 0 0 20px}

      /* This css is only for sub menu only*/

      /* Style the tab */
      div.tab {
          overflow: hidden;
          border: 1px solid #ccc;
          background-color: #f1f1f1;
      }

      /* Style the buttons inside the tab */
      div.tab button {
          background-color: inherit;
          float: left;
          border: none;
          outline: none;
          cursor: pointer;
          padding: 14px 16px;
          transition: 0.3s;
          font-size: 17px;
      }

      /* Change background color of buttons on hover */
      div.tab button:hover {
          background-color: #ddd;
      }

      /* Create an active/current tablink class */
      div.tab button.active {
          background-color: #ccc;
      }

      /* Style the tab content */
      .tabcontent {
          display: none;
          border: 1px solid #ccc;
          border-top: none;

      }

      .mapclass{
          width: 62%;
          height: 400px;
          margin-left: 235px;
          position: relative;
          overflow: hidden;
          margin-top: 15px;
      }



      #googleMap {

          position: absolute;
          top: 525px;

      }
      #googleMap {
          z-index: 10;
      }
      
      .greenarrow{
            width: 7%;
            height: 15%;
            z-index: 10;
            position: absolute;
            left: 16px;
            top: 74px;
      }
</style>

<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

<div style="min-height:450px !important;">

    <div class="container"  style=" padding: 0px;"> 
			 
        <div class="panel panel-primary" style="margin-top:36px !important;" >

            <div class="panel-heading centerdash" id="non-printable">Center Dashboard</div>
            <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            

             
              
                
                <form name="frmcenterdetail" id="frmcenterdetail" action="" class="form-inline"> 
                    <?php if($_SESSION['User_UserRoll']==1){?>
                    <div class="container" style="height: 40px; margin-bottom: 10px; padding: 0px;"> 
                        
                        <div class="col-sm-3 form-group1 marginB15"  style=" padding: 0px;"> 
                            
                            <select id="selRSP" name="selRSP" class="form-control">
                                <option value=''>Please Select RSPs</option>
                            </select>   
                        </div>
                        
                         <div class="col-sm-4 form-group1 marginB15"> 
                            <select id="selITGK" name="selITGK" class="form-control">
                            <option value=''>Please Select ITGKs</option>   
                           </select>   
                        </div>
                     
                    </div>
                   
                    
                    <?php }?> 
                    
                     <?php if($_SESSION['User_UserRoll']==14){?>
                    <div class="container"  style=" padding: 0px;"> 
                        <div class="col-sm-4 form-group1 marginB15"  style=" padding: 0px;"> 
                            <select id="selITGK" name="selITGK" class="form-control">
                            <option value=''>Please Select ITGKs</option>   
                           </select>   
                        </div>
                     
                    </div>
                    
                    <?php }?> 
                
                    <input type="hidden" value="itgkd" id="functionname" name="functionname" >
                    <input type="hidden" value="<?php echo $_SESSION['User_LoginId'];?>" id="itgkval" name="itgkval" >
                </form> 
             
           <div id="response"> </div>
           
<!-- <li><a href="#tabs-5">Area Details</a></li>
                  <li><a href="#tabs-6">Area Type </a></li>-->

            <div id="tabs" style=" display: none;">
                <ul>
                  <li><a href="#tabs-1" id="itgkd">ITGK (Center) Details</a></li>
                  <li><a href="#tabs-2" id="spd">Service Provider Details</a></li>
                  <li><a href="#tabs-3" id="bankd">Bank Account Details</a></li>
                  <li><a href="#tabs-4" id="lockd">Location Details</a></li>
                  <li><a href="#tabs-5" id="coursed">Registered Course Details </a></li>
                  <li><a href="#tabs-6" id="pred">Premises Details</a></li>
                  <li><a href="#tabs-7" id="comrd">Computer Resource Details </a></li>
                  <li><a href="#tabs-8" id="adana">SLA Admission  </a></li>
                  <li><a href="#tabs-10" id="slad">SLA Result </a></li>
<!--                  <li><a href="#tabs-12" id="sladu">SLA Analysis (University) </a></li>-->
                  <li><a href="#tabs-11" id="hrd" >HR Details </a></li>
                  
                  <li><a href="#tabs-9"  id="bus">Block / Unblock Status </a></li>
                  <li><a href="#tabs-13"  id="emul">Email / Mobile Update Log </a></li>
                  <li><a href="#tabs-14"  id="acnl">Address / Name Change Log </a></li>

                </ul>
                
                
                
                <div id="gird">
                  <div class="container">
                        <div class="row">
                            <div class="personalblog" style="width: 77%;">
                              <!---  this is the start of that div  --->  
                                
                              <div class='col-lg-12 col-md-12'  id='tabs-1' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span >ITGK (Center) Details</span>
                                  </div>
                                  <div class="maindiv" id="ITGKDivNodata1" style=" display: none;">
                                      <div class='persdetablog bdrtop' id="ITGKDivNodata2">
                                      </div>  
                                  </div>
                                  <div class="maindiv" id="ITGKDiv" style='min-height: 630px;'>
                                  </div>
                              </div>

                              <div class='col-lg-12 col-md-12'  id='tabs-2' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Service Provider Details</span>
                                  </div>
                                  <div class="maindiv" id="SPDivNodata1" style=" display: none;">
                                      <div class='persdetablog bdrtop' id="SPDivNodata2">
                                      </div>  
                                  </div>
                                  <div class="maindiv" id="SPDiv">
                                  </div>
                              </div>

                              <div class='col-lg-12 col-md-12 ' id='tabs-3' aria-hidden='true' style='display: none;'>
                                  <div class='persdeta' id="triangle-bottomleft">
                                      <span>Bank Account Details</span>
                                  </div>
                                  <div class="maindiv" id="bankdivNodata1" style=" display: none;">
                                      <div class='persdetablog bdrtop' id="bankdivNodata2">

                                      </div>  
                                  </div>
                                  <div  class="maindiv" style=" min-height: 0px; padding: 0px 0px 0px 0px;" id="bankDivMsg"></div>
                                  <div class="maindiv"  id="bankDiv">
                                      <div class='persdetablog bdrtop'>
                                          <div class="persdetacont" >
                                              <span>Account Name:</span>
                                              <p id="Bank_Account_Name"> </p>
                                          </div>
                                          <div class='persdetacont '>
                                              <span>Account Number:</span>
                                              <p id="Bank_Account_Number" > </p>
                                          </div>
                                      </div>
                                      <div class='persdetablog'>
                                          <div class='persdetacont'>
                                              <span>Account Type:</span>
                                              <p id="Bank_Account_Type"> </p>
                                          </div>
                                          <div class='persdetacont'>
                                              <span>IFSC Code:</span>
                                              <p id="Bank_Ifsc_code" > </p>
                                          </div>
                                      </div> 
                                      <div class='persdetablog'>
                                          <div class='persdetacont'>
                                              <span>Bank Name:</span>
                                              <p id="Bank_Name"> </p>
                                          </div>
                                          <div class='persdetacont'>
                                              <span>MICR Code:</span>
                                              <p id="Bank_Micr_Code" > </p>
                                          </div>
                                      </div> 
                                     
                                      <div class='persdetablog'>
                                          <div class='persdetacont'>
                                              <span>PAN No:</span>
                                              <p id="Pan_No"> </p>
                                          </div>
                                           <div class='persdetacont'>
                                              <span>Branch Name:</span>
                                              <p id="Bank_Branch_Name"> </p>
                                          </div>
                                      </div>
                                      
                                      <div class='persdetablog'>
                                         <div class='persdetacont'>
                                              <span>PAN Name:</span>
                                              <p  id="Pan_Name"> </p>
                                          </div>
                                          <div class='persdetacont'>
                                              <span>Id Proof:</span>
                                              <p id="Bank_Id_Proof" > </p>
                                          </div>
                                      </div>

                                      <div class='persdetablog'>
                                          <div class='persdetacont'>
                                              <span>PAN Document:</span>
                                              <input type="hidden" id="panimage" name="panimage" value="" >
                                              <p><a title=""  style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModalPan"><img id="Pan_Document" src='images/noimagefound.jpg' height='100' width='100'></a></p>
                                          </div>
                                          <div class='persdetacont'>
                                              <span>Bank Document:</span>
                                              <input type="hidden" id="bankimage"  name="bankimage" value="" >
                                              <p><a title=""  style="text-decoration:none;" href="#" data-toggle="modal" data-target="#myModalBank"><img id="Bank_Document" src='images/noimagefound.jpg'  height='100' width='100'></a> </p>
                                          </div>
                                      </div>
                                  </div> 

                                  <div tabindex="-1" class="modal fade" id="myModalPan" role="dialog">
                                      <div class="modal-dialog" style="z-index:100%">
                                          <div class="modal-content">
                                              <div class="modal-header" style=" background-color: #439943">
                                                  <button class="close" type="button" data-dismiss="modal">×</button>
                                                  <h3 class="modal-title" style="font-size: 24px;">Uploaded PAN Document</h3>
                                              </div> 
                                              <div class="modal-body"  style="text-align: center;">
                                                  <img id="PreviewPan" src="images/noimagefound.jpg" />
                                              </div>
                                              <div class="modal-footer">
                                                  <button type="button" class="btn btn-primary pull-left" id="download_document_pan"><i class="fa fa-download" aria-hidden="true"></i>
                                                      &nbsp;&nbsp;Download this Document</button>
                                                  <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
                                                      &nbsp;&nbsp;Close</button>
                                              </div>

                                          </div>
                                      </div>
                                  </div> 

                                  <div tabindex="-1" class="modal fade" id="myModalBank" role="dialog">
                                      <div class="modal-dialog" style="z-index:100%">
                                          <div class="modal-content">
                                              <div class="modal-header" style=" background-color: #439943">
                                                  <button class="close" type="button" data-dismiss="modal">×</button>
                                                  <h3 class="modal-title" style="font-size: 24px;">Uploaded Bank Document</h3>
                                              </div> 
                                              <div class="modal-body"  style="text-align: center;">
                                                  <img id="PreviewBank" src="images/noimagefound.jpg" />
                                              </div>
                                              <div class="modal-footer">
                                                  <button type="button" class="btn btn-primary pull-left" id="download_document_bank"><i class="fa fa-download" aria-hidden="true"></i>
                                                      &nbsp;&nbsp;Download this Document</button>
                                                  <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
                                                      &nbsp;&nbsp;Close</button>
                                              </div>

                                          </div>
                                      </div>
                                  </div>                 



                              </div>

                              <div class='col-lg-12 col-md-12'  id='tabs-4' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Location Details</span>
                                  </div>
                                  <div class="maindiv" id="lockDivNodata1" style=" display: none;">
                                      <div class='persdetablog bdrtop' id="lockDivNodata2">
                                      </div>  
                                  </div>
                                  <div class="maindiv" id="lockDiv">
                                  </div>
                              </div>

                              <div class='col-lg-12 col-md-12'  id='tabs-5' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Registered Course Details</span>
                                  </div>
                                  <div class="maindiv" id="CourseDivNodata1" style=" display: none;">
                                      <div class='persdetablog bdrtop' id="CourseDivNodata2">
                                      </div>  
                                  </div>
                                  <div class="maindiv" id="CourseDiv">
                                  </div>
                              </div>

                              <div class='col-lg-12 col-md-12'  id='tabs-6' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Premises Details</span>
                                  </div>
                                  <div class="maindiv" id="PreDivNodata1" style=" display: none;">
                                      <div class='persdetablog bdrtop' id="PreDivNodata2">
                                      </div>  
                                  </div>
                                  <div class="maindiv" id="PreDiv">
                                  </div>
                              </div>

                              <div class='col-lg-12 col-md-12'  id='tabs-7' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Computer Resource Details</span>
                                  </div>
                                  <div class="maindiv" id="ComRDivNodata1" style=" display: none;">
                                      <div class='persdetablog bdrtop' id="ComRDivNodata2">
                                      </div>  
                                  </div>
                                  <div class="maindiv" id="ComRDiv">
                                  </div>
                              </div>

                              <div class='col-lg-12 col-md-12'  id='tabs-11' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>HR Details</span>
                                  </div>
                                  <div class="maindiv" id="HRDivNodata1" style=" display: none;">

                                      <div class='persdetablog bdrtop' id="HRDivNodata2">
                                      </div> 

                                  </div>
                                  <div class="maindiv" id="HRDiv">
                                  </div>
                              </div>
                              
                              <div class='col-lg-12 col-md-12'  id='tabs-14' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Address / Name Change Log</span>
                                  </div>
                                  <div class="maindiv" id="ANCDivNodata1" style=" display: none;">

                                      <div class='persdetablog bdrtop' id="ANCDivNodata2">
                                      </div> 

                                  </div>
                                  <div class="maindiv" id="ANCDiv">
                                  </div>
                              </div>

                              
                              
                              <div class='col-lg-12 col-md-12'  id='tabs-81' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Admission Graphs</span>
                                  </div>
                                  <div class="maindiv" id="GRAPDivNodata1" style=" display: none;">

                                      <div class='persdetablog bdrtop' id="GRAPDivNodata2">
                                      </div> 

                                  </div>
                                  <div class="maindiv" id="GRAPDiv">
                                      <div class='persdetablog bdrtop'>
                                          <!--Div that will hold the pie chart-->
                                            <div id="chart_div"></div>	
                                      </div>
                                  </div>
                              </div>
                              
                              <div class='col-lg-12 col-md-12'  id='tabs-8' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>SLA Admission</span>
                                  </div>
                                   <div class="maindiv" id="ADANADivNodata1" style=" display: none;">

                                      

                                  </div>
                                  <div class="maindiv" >
                                      
                                      <div class='persdetablog bdrtop'>
                                          
                                       <div class='exampannelNew1' style="width: 25%;">
                                              <select id="txtCalYear" name="txtCalYear" class="form-control" >
                                            <option value=''>Select Calendar Year</option>  
                                            <?php 
                                                for ($yr = date("Y"); $yr >= 2009; $yr--) {
                                                    echo '<option value"' . $yr . '">' . $yr . '</option>';
                                                }
                                            ?>
                                        </select>
                                        </div>
                                       <div class='exampannelNew1' style="width: 6%; padding-left: 10px">OR </div>   
                                       <div class='exampannelNew1' style="width: 25%;">
                                              <select id="txtFinYear" name="txtFinYear" class="form-control" >
                                            <option value=''>Select Financial Year</option>  
                                            <?php 
                                                for ($yr = date("Y"); $yr >= 2009; $yr--) {
                                                    $yr_1=$yr+1;
                                                    echo '<option value"' . $yr . '">' . $yr .'-'.$yr_1.'</option>';
                                                }
                                            ?>
                                        </select>
                                       </div>
                                        <div class='exampannelNew1' style="width: 6%;  padding-left: 10px">OR </div>   
                                        <div class='exampannelNew1' style="width: 25%;">
                                              <input type="submit" name="btnSLA" id="btnSLA" class="btn btn-primary" value="SLA Admisiion"/> 
                                       </div>
<!--                                            <div class="exampannelNew1"> 
                                                    <label for="sdate">Start Date:</label>
                                                    <span class="star">*</span>
                                                    <input type="text" class="form-control" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="DD-MM-YYYY">     
                                            </div>
                                            <div class="exampannelNew1">     
                                                    <label for="edate">End Date:</label>	
                                                    <span class="star">*</span>
                                                    <input type="text" class="form-control" readonly="true" name="txtenddate" id="txtenddate"  placeholder="DD-MM-YYYY" value=" <?php echo date("d-m-Y"); ?>">
                                            </div>
                                            <div class="exampannelNew1" style="float: right;padding-top: 14px;">
                                                <input type="submit" name="btnAdmissionAnalysis" id="btnAdmissionAnalysis" class="btn btn-primary" value="Submit"/> 
                                            </div>-->
                                        
                                      </div>
                                      <div class='persdetablog'>
                                      
                                
                            
                                      </div>
                                      <div class='persdetablog' id="ADANADivNodata2">
                                      </div> 
                                      
                                  <div id="ADANADiv" style="margin-top: 26px; padding-left: 20px;">
                                   
                                  </div>
                                  </div>
                                  
                                 
                                  
                                  
                              </div>

                              <div class='col-lg-12 col-md-12'  id='tabs-10' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Service Level Agreement (SLA) Result</span>
                                  </div>
                                  <div class="maindiv" id="SLADivNodata1" style=" display: none;">

                                      <div class='persdetablog bdrtop' id="SLADivNodata2">
                                      </div> 

                                  </div>
                                  <div class="maindiv" id="SLADiv">
                                      <div class='persdetablog bdrtop'>
                                          Processing...
                                      </div>
                                  </div>
                            </div>
                              
                              <div class='col-lg-12 col-md-12'  id='tabs-12' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Service Level Agreement (SLA) Analysis (University)</span>
                                  </div>
                                  <div class="maindiv" id="SLAUDivNodata1" style=" display: none;">

                                      <div class='persdetablog bdrtop' id="SLAUDivNodata2">
                                      </div> 

                                  </div>
                                  <div class="maindiv" id="SLAUDiv">
                                      <div class='persdetablog bdrtop'>
                                          Processing...
                                      </div>
                                  </div>
                                  
                            </div>
                              
                              <div class='col-lg-12 col-md-12'  id='tabs-13' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Email / Mobile Update Log </span>
                                  </div>
                                  <div class="maindiv" id="EMUpdateDivNodata1" style=" display: none;">
                                      <div class='persdetablog bdrtop' id="EMUpdateDivNodata2">
                                      </div>  
                                  </div>
                                  <div class="maindiv" id="EMUpdateDiv">
                                  </div>
                                  <div> <img  class="greenarrow" src="images/GreenArrow.jpg"></div>
                              </div>
                                
                              
                             <div class='col-lg-12 col-md-12'  id='tabs-9' aria-hidden='true' style='display: none;'>
                                  <div class="persdeta" id="triangle-bottomleft">
                                      <span>Block / Unblock Status</span>
                                  </div>
                                  <div class="maindiv" id="busDivNodata1" style=" display: none;">
                                      <div class='persdetablog bdrtop' id="busDivNodata2">
                                      </div>  
                                  </div>
                                  <div class="maindiv" id="busDiv">
                                  </div>
                              </div>
                          <!---  this is the end of that div  --->      
                            </div>
                            
                             
                        </div>
                  </div>  
                    
                </div>
                
            </div> 
             
<div id="googleMap"  class="mapclass" style=" display: none"></div>
              
            </div>

        </div>   
    </div>
</div>


<script>
function myMap(slat,slong,add) {
//alert(slat);
//alert(slong);
//alert(add);
  var myCenter = new google.maps.LatLng(slat , slong);
  var mapCanvas = document.getElementById("googleMap");
  var mapOptions = {center: myCenter, zoom: 8};
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({position:myCenter});
  marker.setMap(map);
  
  google.maps.event.addListener(marker,'click',function() {
        var infowindow = new google.maps.InfoWindow({
        content:add
            });
        infowindow.open(map,marker);
    });

}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3LDfJa6bQsDLslWSt7MdkTD45zSyw4Ac&callback=myMap"></script>
 					
	</body>					
<?php include ('footer.php'); ?>				
<?php include'common/message.php';?>
<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        // autoclose: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script>
        
<!--    <script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
    
    <script type="text/javascript">
    /*
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
    function drawChart() {
      var jsonData = $.ajax({
          url: "getData.php",
          dataType:"json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
      chart.draw(data, {width: 800, height: 400});
    }
*/
    </script>
        
        
<style>
#errorBox{
 color:#F00;
 }
</style>
<script src="rkcltheme/js/jquery.validate.min.js"></script>

  
<script type="text/javascript">
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
$(document).ready(function () {
    
                $("#download_document_pan").on("click", function () {//alert(image);
                    var image= panimage.value;
                    downloadFile(image, 'pan');
             
                });
            
                $("#download_document_bank").on("click", function () {//alert(image);
                    var image= bankimage.value;
                    downloadFile(image, 'bank');
             
                });

                function downloadFile(img, na) {
                    window.location.href = 'download_all.php?imagename=' + img  +'&docname='+ na;
                }

                function CenterDetail_old() {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=CENTERDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                            $('#response').empty();
                                            $("#tabs").show();
                                            //$("#gird").html(data);
                                            $("#gird").show();
                                            data = $.parseJSON(data);
                                            $("#Organization_Name").html(data.Organization_Name);
                                            $("#User_LoginId").html(data.User_LoginId);
                                            $("#User_EmailId").html(data.User_EmailId);
                                            $("#User_MobileNo").html(data.User_MobileNo);
                                            $("#Organization_Address").html(data.Organization_Address);
                                    }
                                });
                           }
                           
                function CenterDetail() {
                                $("#ITGKDiv").show();
                                $("#ITGKDivNodata1").hide();
                                $('#ITGKDiv').empty();
                                $('#ITGKDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                                            
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=CENTERDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#ITGKDiv").show();
                                            $("#ITGKDivNodata1").hide();
                                            $("#ITGKDiv").html(data);
                                            
                                        }else{
                                            
                                           $("#ITGKDiv").hide();
                                           $("#ITGKDivNodata1").show();
                                           $("#ITGKDivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function SPDetail() {
                                $("#SPDiv").show();
                                $("#SPDivNodata1").hide();
                                $('#SPDiv').empty();
                                $('#SPDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=SPDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {  //alert(data);     
                                            $('#response').empty();
                                            $("#tabs").show();
                                            $("#gird").show();
                                        if(data!='NoData'){
                                           
                                            //data = $.parseJSON(data);
                                            $("#SPDiv").show();
                                            $("#SPDivNodata1").hide();
                                            $("#SPDiv").html(data);
                                            //$("#sp_Addresp").html(data.Addresp);
                                            //$("#sp_Organization_FoundedDate").html(data.Organization_FoundedDate);
                                            //$("#sp_Organization_Name").html(data.Organization_Name);
                                            //$("#sp_Organization_RegistrationNo").html(data.Organization_RegistrationNo);
                                            //$("#sp_Organization_RegistrationNo").html(data.noofcenter);
                                            //$("#sp_Organization_FoundedDate").html(data.spname);
                                            }else{
                                           $("#SPDiv").hide();
                                           $("#SPDivNodata1").show();
                                           $("#SPDivNodata2").htmlhtml("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function BankDetail() {
                                $('#bankDivMsg').empty();
                                $('#bankDivMsg').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=BANKDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#bankDivMsg').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                            data = $.parseJSON(data);  
                                           
                                            $("#bankDiv").show();
                                            $("#bankdivNodata1").hide();
                                            $("#Bank_Account_Name").html(data.Bank_Account_Name);
                                            $("#Bank_Account_Number").html(data.Bank_Account_Number);
                                            $("#Bank_Account_Type").html(data.Bank_Account_Type);
                                            $("#Bank_Ifsc_code").html(data.Bank_Ifsc_code);
                                            $("#Bank_Name").html(data.Bank_Name);
                                            $("#Bank_Micr_Code").html(data.Bank_Micr_Code);
                                            $("#Bank_Branch_Name").html(data.Bank_Branch_Name);
                                            $("#Bank_Id_Proof").html(data.Bank_Id_Proof);
                                            $("#Pan_No").html(data.Pan_No);
                                            $("#Pan_Name").html(data.Pan_Name);
                                            panimage.value = data.Pan_Document;
                                            bankimage.value = data.Bank_Document;
                                            
                                            //$("#").html(data.Bank_Document);
                                            $("#Bank_Document").attr('src', "upload/Bankdocs/" + data.Bank_Document);
                                            $("#PreviewBank").attr('src', "upload/Bankdocs/" + data.Bank_Document);
                                            $("#PreviewBank").attr('width', "400px");
                                            $("#PreviewPan").attr('width', "400px");
                                            $("#Pan_Document").attr('src', "upload/pancard/" + data.Pan_Document);
                                            $("#PreviewPan").attr('src', "upload/pancard/" + data.Pan_Document);
                                        }else{
                                            
                                           $("#bankDiv").hide();
                                           $("#bankdivNodata1").show();
                                           $("#bankdivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function LocationDetail() {
                                $("#lockDiv").show();
                                $("#lockDivNodata1").hide();
                                $('#lockDiv').empty();
                                $('#lockDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=LOCATIONDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#lockDiv").show();
                                            $("#lockDivNodata1").hide();
                                            $("#lockDiv").html(data);
                                            
                                        }else{
                                            
                                           $("#lockDiv").hide();
                                           $("#lockDivNodata1").show();
                                           $("#lockDivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function BUSDetail() {
                                $("#busDiv").show();
                                $("#busDivNodata1").hide();
                                $('#busDiv').empty();
                                $('#busDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=BLOCKUNBLOCKDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#busDiv").show();
                                            $("#busDivNodata1").hide();
                                            $("#busDiv").html(data);
                                            
                                        }else{
                                            
                                           $("#busDiv").hide();
                                           $("#busDivNodata1").show();
                                           $("#busDivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function CourseDetail() {
                                $("#CourseDiv").show();
                                $("#CourseDivNodata1").hide();
                                $('#CourseDiv').empty();
                                $('#CourseDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=COURSEDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#CourseDiv").show();
                                            $("#CourseDivNodata1").hide();
                                            $("#CourseDiv").html(data);
                                            
                                        }else{
                                            
                                           $("#CourseDiv").hide();
                                           $("#CourseDivNodata1").show();
                                           $("#CourseDivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function EMUDetail() {
                                $("#EMUpdateDiv").show();
                                $("#EMUpdateDivNodata1").hide();
                                $('#EMUpdateDiv').empty();
                                $('#EMUpdateDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=EMUDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#EMUpdateDiv").show();
                                            $("#EMUpdateDivNodata1").hide();
                                            $("#EMUpdateDiv").html(data);
                                            
                                        }else{
                                            
                                           $("#EMUpdateDiv").hide();
                                           $("#EMUpdateDivNodata1").show();
                                           $("#EMUpdateDivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function PremisesDetail() {
                                $("#PreDiv").show();
                                $("#PreDivNodata1").hide();
                                $('#PreDiv').empty();
                                $('#PreDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=PREMISESDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#PreDiv").show();
                                            $("#PreDivNodata1").hide();
                                            $("#PreDiv").html(data);
                                            
                                        }else{
                                            
                                           $("#PreDiv").hide();
                                           $("#PreDivNodata1").show();
                                           $("#PreDivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function ComputerRecourseDetail() {
                                $("#ComRDiv").show();
                                $("#ComRDivNodata1").hide();
                                $('#ComRDiv').empty();
                                $('#ComRDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=COMPUTERRECOURSEDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#ComRDiv").show();
                                            $("#ComRDivNodata1").hide();
                                            $("#ComRDiv").html(data);
                                            
                                        }else{
                                            
                                           $("#ComRDiv").hide();
                                           $("#ComRDivNodata1").show();
                                           $("#ComRDivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function HRDetail() {
                                $("#HRDiv").show();
                                $("#HRDivNodata1").hide();
                                $('#HRDiv').empty();
                                $('#HRDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=HRDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#HRDiv").show();
                                            $("#HRDivNodata1").hide();
                                            $("#HRDiv").html(data);
                                            
                                        }else{
                                            
                                           $("#HRDiv").hide();
                                           $("#HRDivNodata1").show();
                                           $("#HRDivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function ANCDetail() {
                                $("#ANCDiv").show();
                                $("#ANCDivNodata1").hide();
                                $('#ANCDiv').empty();
                                $('#ANCDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=ANCDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#ANCDiv").show();
                                            $("#ANCDivNodata1").hide();
                                            $("#ANCDiv").html(data);
                                            
                                        }else{
                                            
                                           $("#ANCDiv").hide();
                                           $("#ANCDivNodata1").show();
                                           $("#ANCDivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function SLADetail() {
                                $("#SLADiv").show();
                                $("#SLADivNodata1").hide();
                                $('#SLADiv').empty();
                                $('#SLADiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=SLADETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#SLADiv").show();
                                            $("#SLADivNodata1").hide();
                                            $("#SLADiv").html(data);
                                            
                                        }else{
                                            
                                           $("#SLADiv").hide();
                                           $("#SLADivNodata1").show();
                                           $("#SLADivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function SLAUDetail() {
                                $("#SLAUDiv").show();
                                $("#SLAUDivNodata1").hide();
                                $('#SLAUDiv').empty();
                                $('#SLAUDiv').append("<p class='error bdrtop' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                                var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmcenterdetail").serialize();
				data = "action=SLAUDETAILS&" +forminput; // serializes the form's elements.
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {//console.log();
                                      $('#response').empty();
                                      $("#tabs").show();
                                      $("#gird").show();
                                      if(data!='NoData'){
                                           // data = $.parseJSON(data);  
                                           
                                            $("#SLAUDiv").show();
                                            $("#SLAUDivNodata1").hide();
                                            $("#SLAUDiv").html(data);
                                            
                                        }else{
                                            
                                           $("#SLAUDiv").hide();
                                           $("#SLAUDivNodata1").show();
                                           $("#SLAUDivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");
                                            
                                        }
                                    }
                                });
                           } 
                           
                function ADGrapDetail(txtYear,itgk,cal) {
                    //alert(txtYear);
                     $("#ADANADiv").show();
                     $("#ADANADivNodata2").hide();
                     $('#ADANADiv').empty();
                     $('#ADANADiv').append("<p class='error' style='padding: 15px 0 0 20px'><span><img src=images/ajax-loader.gif width=10px /></span><span style='padding-left: 5px'>Please wait while we are fetching the data.....</span></p>");
                     var url = "common/cfcenterdashboard.php"; // the script where you handle the form input.
                     var data;
                     //var forminput=$("#frmcenterdetail").serialize();
                     data = "action=ADCOUNT&txtYear=" + txtYear + "&itgk=" + itgk + "&fyear=" + cal; // serializes the form's elements.
                     $.ajax({
                         type: "POST",
                         url: url,
                         data: data,
                         success: function (data)
                         {//console.log();
                           $('#response').empty();
                           if(data!='NoData'){
                                 $("#ADANADiv").show();
                                 $("#ADANADivNodata2").hide();
                                 $("#ADANADiv").html(data);

                             }else{

                                $("#ADANADiv").hide();
                                $("#ADANADivNodata2").show();
                                $("#ADANADivNodata2").html("<div class='error' style='padding-left: 20px; padding-top: 20px;'> Details are not available.</div>");

                             }
                         }
                     });
                } 
                
                function MapDetail() {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                var itgkcode = itgkval.value;
                                var input_data = "action=Learnerloc&itgk="+itgkcode;
                                $.ajax({
                                        url: 'common/cfcenterdashboard.php',
                                        type: "post",
                                        data: input_data,
                                        success: function (data) {
                                                if(data == "fail"){
                                                        BootstrapDialog.alert("Address not found by map.");
                                                }
                                                else{ 
                                                        //alert(data);
                                                        //var learner_latLng = data.replace('--', ',');
                                                        var new_address = data.split('***');
                                                        var learner_latLng = new_address[0];
                                                        //alert(new_address);
                                                        latLng_start = learner_latLng.split('--');
                                                        start_lat = latLng_start[0];
                                                        start_lng = latLng_start[1];
                                                        //start_lng = '76.6014862';

                                                        var learner_caddress = new_address[1];

                                                        //cname = learner_caddress;
                                                        myMap(start_lat,start_lng,learner_caddress)

                                                }
                                        }
                                });
                               
                              } 
                    
                //        
		$("#itgkd").click(function () { 
                      CenterDetail();
                      MapDetail();
                      functionname.value = "itgkd";
                      //$("#googleMap").show();
                      $('#googleMap').css("display","block");
                      $('#googleMap').css("top","500px");
                      
                });
                
		$("#spd").click(function () { 
                     SPDetail();
                     functionname.value = "spd";
                     $("#googleMap").hide();
                });
                
		$("#bankd").click(function () {
                     BankDetail();
                     functionname.value = "bankd";
                     $("#googleMap").hide();
                });
                
		$("#lockd").click(function () {
                     LocationDetail();
                     functionname.value = "lockd";
                     $("#googleMap").hide();
                });
                
		$("#coursed").click(function () {
                     CourseDetail();
                     functionname.value = "coursed";
                     $("#googleMap").hide();
                });
                
		$("#pred").click(function () {
                     PremisesDetail();
                     functionname.value = "pred";
                     $("#googleMap").hide();
                });
                
		$("#comrd").click(function () {
                     ComputerRecourseDetail();
                     functionname.value = "comrd";
                     $("#googleMap").hide();
                });
                
		$("#hrd").click(function () {
                     HRDetail();
                     functionname.value = "hrd";
                     $("#googleMap").hide();
                });
		$("#adana").click(function () {
                     //HRDetail();
                     functionname.value = "adana";
                     $("#googleMap").hide();
                });
                
		
		$("#slad").click(function () {
                     SLADetail();
                     functionname.value = "slad";
                     $("#googleMap").hide();
                });
                
		$("#sladu").click(function () {
                     SLAUDetail();  // SLA for University
                     functionname.value = "sladu";
                     $("#googleMap").hide();
                });
		
                $("#acnl").click(function () {
                     ANCDetail();// address name change log
                     functionname.value = "acnl";
                     $("#googleMap").hide();
                });
                
                $("#emul").click(function () {
                     EMUDetail();// email mobile update
                     functionname.value = "emul";
                     $("#googleMap").hide();
                });
                
                $("#bus").click(function () {
                     BUSDetail();// Block / Unblock Status 
                     functionname.value = "bus";
                     $("#googleMap").hide();
                });
                
                $("#btnSLA").click(function () {
                        txtFinYear.value='';
                        txtCalYear.value='';
                        var itgk= itgkval.value;
                        var cal= 'SLA';
                        ADGrapDetail(this.value,itgk,cal);
                });
                
                $("#txtCalYear").change(function(){
                    ///$("#txtFinYear").load();
                                txtFinYear.value='';
                                var itgk= itgkval.value;
                                var cal= 'Cal';
                                ADGrapDetail(this.value,itgk,cal);
                                                                
                            });
                            
                $("#txtFinYear").change(function(){//alert(this.value);
                    //$("#txtCalYear").load();
                                txtCalYear.value='';
                                var itgk= itgkval.value;
                                var cal= 1;
                                ADGrapDetail(this.value,itgk,cal);
                                                                
                            });
                
                
                         
                
                <?php if($_SESSION['User_UserRoll']==1){ ?>
                        
                        
                        function FillRSP() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfcenterdashboard.php",
                                    data: "action=FillRSP",
                                    success: function (data) {
                                        $("#selRSP").html(data);
                                    }
                                });
                            }
                        FillRSP();
                       
                        $("#selRSP").change(function(){
                                FillITGK(this.value);
                                $("#tabs").hide();
                                
                            });
                        function FillITGK(RSP) {
                               $.ajax({
                                    type: "post",
                                    url: "common/cfcenterdashboard.php",
                                    data: "action=FILLITGK&RSP=" + RSP,
                                    success: function (data) {
                                        $("#selITGK").html(data);
                                    }
                                });
                           }
        
                        
                        $("#selITGK").change(function(){
					
                                //alert(functionname.value);
                                
                                itgkval.value = this.value;
                                var tabname = functionname.value;
                                if(tabname=='itgkd'){
                                    CenterDetail();
                                     MapDetail();
                                    $('#googleMap').css("display","block");
                                    $('#googleMap').css("top","525px");
                                }
                                else if(tabname=='spd'){
                                    SPDetail();
                                }
                                else if(tabname=='bankd'){
                                    BankDetail();
                                }
                                else if(tabname=='lockd'){
                                    LocationDetail();
                                }
                                else if(tabname=='coursed'){
                                    CourseDetail();
                                }
                                else if(tabname=='pred'){
                                    PremisesDetail();
                                }
                                else if(tabname=='comrd'){
                                    ComputerRecourseDetail();
                                }
                                else if(tabname=='hrd'){
                                    HRDetail();
                                }
                                else if(tabname=='slad'){
                                    SLADetail();
                                }
                                else if(tabname=='acnl'){
                                    ANCDetail();
                                }
                                else if(tabname=='emul'){
                                    EMUDetail();
                                }
                                else if(tabname=='bus'){
                                    BUSDetail();
                                }
                                else{
                                   CenterDetail(); 
                                }
				//$("#ADANADiv").hide();
                               
                               
                                return false; // avoid to execute the actual submit of the form.
                            });
                                    
                                    
                <?php }else if($_SESSION['User_UserRoll']==7){ ?>
                        
                               CenterDetail();
                               MapDetail(); 
                                $('#googleMap').css("display","block");
                                $('#googleMap').css("top","500px");
                                  
                                    
                <?php }else{ ?>
                        
                        function FillOrgType() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfcenterdashboard.php",
                                    data: "action=FILLITGK",
                                    success: function (data) {
                                        $("#selITGK").html(data);
                                    }
                                });
                            }
                        FillOrgType();
                        
                        $("#selITGK").change(function(){
                                itgkval.value = this.value;
				 var tabname = functionname.value;
                                if(tabname=='itgkd'){
                                    CenterDetail();
                                     MapDetail();
                                      $('#googleMap').css("display","block");
                                      $('#googleMap').css("top","525px");
                                }
                                else if(tabname=='spd'){
                                    SPDetail();
                                }
                                else if(tabname=='bankd'){
                                    BankDetail();
                                }
                                else if(tabname=='lockd'){
                                    LocationDetail();
                                }
                                else if(tabname=='coursed'){
                                    CourseDetail();
                                }
                                else if(tabname=='pred'){
                                    PremisesDetail();
                                }
                                else if(tabname=='comrd'){
                                    ComputerRecourseDetail();
                                }
                                else if(tabname=='hrd'){
                                    HRDetail();
                                }
                                else if(tabname=='slad'){
                                    SLADetail();
                                }
                                else if(tabname=='acnl'){
                                    ANCDetail();
                                }
                                else if(tabname=='emul'){
                                    EMUDetail();
                                }
                                 else if(tabname=='bus'){
                                    BUSDetail();
                                }
                                else{
                                   CenterDetail(); 
                                }
				//$("#ADANADiv").hide();
				return false; // avoid to execute the actual submit of the form.
                            });
                        
                        
                <?php }?>
			
                            
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<!--<script src="bootcss/js/frmlearnerdetails.js"></script>-->
<style>
.error {
	color: #D95C5C!important;
}
td {
    font-family: times new roman;
    font-size: 15px;
}
legend {
    font-size: 17px !important;
}

.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border: 1px solid #dddddd;
    line-height: 1.42857;
    padding: 8px;
    vertical-align: top;
}
</style>

</html>