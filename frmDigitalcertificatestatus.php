<?php
$title = "Update Status";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '28') {
    //Login here with ITGK or Superadmin
} else {
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>
<style>
    .modal {z-index: 1050!important;} 
    #turmcandition{
        background-color: whitesmoke;
        text-align: justify;
        padding: 10px;
    }
    .noturm{
        margin-left: 10px !important;
    }
    .yesturm{
        margin-right: 15px;
    }
</style>
<div style="min-height:430px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Update Digital Certificate Status</div>
            <div class="panel-body">

                <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">

                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-3 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control" required="required">

                                </select>
                            </div> 

                            <div class="col-md-3 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control" required="required">

                                </select>
                            </div> 
                            
                            <div class="col-md-3 form-group">     
                                <label for="batch"> Enter Learner Code:<span class="star">*</span></label>
                                <input type="text" id="txtlearnercode" name="txtlearnercode" class="form-control" onkeypress="javascript:return allownumbers(event);" required="required" />
                            </div> 
                            
                            <div class="col-md-3 form-group">     
                                <label for="batch"> Select Status For Update:<span class="star">*</span></label>
                                <select id="status" name="status" class="form-control" required="required">
                                    <option value="">-- Select Status --</option>
                                    <option value="0">Link Aadhar</option>
                                    <option value="1">Generate RS-CIT E-Certificate</option>
                                    <option value="2">RS-CIT E-Certificate Generated</option>
                                    <option value="3">Update RS-CIT E-Certificate</option>
                                    <option value="4">Updated RS-CIT E-Certificate</option>
                                </select>
                            </div> 
                        </div>
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmitlist" id="btnSubmitlist" class="btn btn-primary" value="Update E-certificate Status"/>    
                            </div>
                        </div>
                        <div class="container">
                            <div id="response" style="width: 90%;"></div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>
                        <!--                        <div id="view" style="margin-top:5px; width:94%;"> </div>-->

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<link href="css/popup.css" rel="stylesheet" type="text/css">
<div id="myModalEresponse" class="modal" style="padding-top:180px !important;">         
    <div class="modal-content" style="width: 60%;border-radius: 15px; text-align: center;">
        <div class="modal-body" style="min-height: 175px;padding: 0 16px;" id="Eresponse">

        </div>
        <button class="btn btn-primary closeeresponse" style="margin-bottom: 15px;">Close</button>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">

    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")

        if (key == 8 || key == 0) {
            keychar = 8;
        }

        return reg.test(keychar);
    }

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfDigitalcertificatestatus.php",
                data: "action=FILLLinkAadharCourse",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfDigitalcertificatestatus.php",
                data: "action=FILLAdmissionBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });
        });

        $("#btnSubmitlist").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=20px style='margin: 5px;' /></span><span style='font-weight: bold;'> Processing please wait..... </span></p>");
                var url = "common/cfDigitalcertificatestatus.php"; // the script where you handle the form input.     
                var data;
                var course = $('#ddlCourse').val();
                var batch = $('#ddlBatch').val();
                var lcode = $('#txtlearnercode').val();
                var status = $('#status').val();
                data = "action=UPDATESTATUS&course=" + course + "&batch=" + batch + "&lcode=" + lcode + "&status=" + status + ""; //            
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) {
                        if (data === "success")
                        {
                            $('#response').empty();
                            $('#response').append("<p class='alert-success'><span><img src=images/correct.gif width=10px style='width: 25px;margin: 0 10px 4px 0;' /></span><span style='color: green;'>Status Update Successfully.</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmDigitalcertificatestatus.php";
                            }, 2000);

                        }else{
                            $('#response').empty();
                            $('#response').append("<p class='alert-success'><span><img src=images/error.gif width=10px style='width: 20px;margin: 0 10px 3px 0;' /></span style='color: red;'><span>Error To Update Status.</span></p>");
                        }
                    }
                });

            }
            return false;
        });

    });

</script>
<script>
    $("#frmAdmissionSummary").validate({
        rules: {
            ddlCourse: "required",
            ddlBatch: "required",
            txtlearnercode: "required",
            status: "required"

        },
        messages: {
            ddlCourse: "Please Select Course",
            ddlBatch: "Please Select Batch",
            txtlearnercode: "Please Enter Learner Code",
            status: "Please Select Status"
        },
    });
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
        background-color: #e2d9d9;
        width: 91%;
    }
</style>
</html>
?>