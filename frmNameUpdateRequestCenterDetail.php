<?php
    $title = "Name Update Request Center Detail";
    include('header.php');
    include('root_menu.php');
?>
<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container">

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">
                Name Update Requests
            </div>
            <div class="panel-body">
                <div id="response"></div>
                <div id="gird"></div>

            </div>

        </div>
    </div>

</div>
<?php
    include 'common/message.php';
    include 'footer.php';
    include "common/modals.php";
?>

<script type="text/javascript">

    var User_Roll = '<?php echo $_SESSION['User_UserRoll'];?>';
    var User_Code = '<?php echo $_SESSION['User_Code']?>';

    $(document).ready(function () {

        function showData() {
            var url = "common/cfmodifyITGK.php";
            var data;
            data = "action=showNameUpdateRequestTableITGK";
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    showDatatable(data);
                }
            });
        }

        function showDatatable(data) {
            $("#gird").html(data);
            $("#example").DataTable({
                dom: 'Bfrtip',
                "bProcessing": true,
                "serverSide": true,
                "ajax": {
                    url: "common/cfmodifyITGK.php",
                    data: {
                        "action": "showAddressUpdateRequestITGK",
                        "flag": "name",
                        "Code": User_Code
                    },
                    type: "POST"
                },
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columnDefs: [
                    {
                        targets: 1,
                        render: function (data, type, row, meta) {
                            if (type === 'display' && (User_Roll === '14') || (User_Roll === '4')) {
                                data = '<a href="viewDetail_name.php?Code=' + encryptString(data) + '">' + data + '</a>';
                            }

                            if(type === 'display' && User_Roll === '11'){
                                data = '<a href="viewDetail_name_rkcl.php?Code=' + encryptString(data) + '">' + data + '</a>';
                            }
                            return data;
                        }
                    }
                ]
            });
        }

        showData();
    });

</script>
</html>