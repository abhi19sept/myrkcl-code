<?php
    ini_set("upload_max_filesize","7M");
    $title = "Organization Details";
    include('header.php');
    include('root_menu.php');
    include 'common/modals.php';

    echo "<script>var OrgCode = '" . $_SESSION['User_Code'] . "'; </script>";

?>

<style>

    .btn-success {
        background-color: #00A65A !important;
    }

    .btn-success:hover {
        color: #fff !important;
        background-color: #04884D !important;
        border-color: #398439 !important;
    }

    .asterisk {
        color: red;
        font-weight: bolder;
        font-size: 18px;
        vertical-align: middle;
    }

    .division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 20px;
        color: #575c5f;
        margin-bottom: 20px;

    }

    .extra-footer-class {
        margin-top: 0;
        margin-bottom: -16px;
        padding: 16px;
        background-color: #fafafa;
        border-top: 1px solid #e5e5e5;
    }

    #errorBox {
        color: #F00;
    }

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eeeeee;
        box-shadow: inset 0 0 5px 1px #d5d5d5;
    }

    .form-control {
        border-radius: 2px;
    }

    input[type=text]:hover,
    textarea:hover {
        box-shadow: 0 1px 3px #aaa;
        -webkit-box-shadow: 0 1px 3px #aaa;
        -moz-box-shadow: 0 1px 3px #aaa;
    }

    .col-sm-3:hover {
        background: none !important;
    }

    .hidden-xs {
        display: inline-block !important;
    }

    input.parsley-success,
    select.parsley-success,
    textarea.parsley-success {
        color: #468847;
        background-color: #DFF0D8;
        border: 1px solid #D6E9C6;
    }

    input.parsley-error,
    select.parsley-error,
    textarea.parsley-error {
        color: #B94A48;
        background-color: #F2DEDE;
        border: 1px solid #EED3D7;
    }

    .parsley-errors-list {
        margin: 2px 0 3px;
        padding: 0;
        list-style-type: none;
        font-size: 0.9em;
        line-height: 0.9em;
        opacity: 0;

        transition: all .3s ease-in;
        -o-transition: all .3s ease-in;
        -moz-transition: all .3s ease-in;
        -webkit-transition: all .3s ease-in;
    }

    .parsley-errors-list.filled {
        opacity: 1;
    }

    .parsley-required {
        color: tomato;
        font-family: Calibri;
        margin-top: 4px;
        font-size: 15px;
    }

    select[disabled] {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 0.01px;
        text-overflow: '';
    }

    .bs-callout {
        padding: 0 0 0 20px;
        margin: 20px 0;
        border: 1px solid #eee;
        border-left-color: rgb(238, 238, 238);
        border-left-width: 5px;
        border-radius: 3px;
    }

    .bs-callout-danger {
        border-left-color: #ce4844;
        background-color: #fcf8e3;
    }

    .bs-callout-success {
        border-left-color: #3c763d;
        background-color: #dff0d8;
    }

    .bs-callout-danger h4 {
        color: #ce4844;
        margin-top: 15px;
    }

    .bs-callout-success h4 {
        color: #3c763d;
        margin-top: 15px;
    }

    .bs-callout-danger p {
        margin: 0 0 10px;
    }
</style>

<script src="bootcss/js/parsley.min.js"></script>

<div class="container" id="showdata">


    <div class="panel panel-primary" style="margin-top:46px !important;">

        <div class="panel-heading">Update Latitude Longitude</div>
        <div class="panel-body">

            <div style="padding-bottom: 5px;font-size: 15px;color: #575c5f;">
                Fields marked with <span class="asterisk">*</span> are mandatory
            </div>

            <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="uploadGeoTaggedImage" name="uploadGeoTaggedImage" enctype="multipart/form-data">

                <div class="division_heading">
                    Upload Geo-tagged Photograph &nbsp;&nbsp;
                    <button type="button" class="btn btn-primary" onclick="$('#geo_tag_support').modal('show')">
                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                    </button>
                </div>

                <div class="bs-callout bs-callout-danger" id="errorlatlong" style="display: none;">
                    <h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                        &nbsp;Error Occurred</h4>
                    <p id="errorlatlongtext"></p>
                </div>

                <div class="bs-callout bs-callout-success" id="successlatlong" style="display: none;">
                    <h4><i class="fa fa-check" aria-hidden="true"></i>
                        &nbsp;Success</h4>
                    <p id="successlatlongtext"></p>
                </div>

                <div class="box-body" style="margin: 0 100px;" id="mainForm">

                    <div class="form-group">

                        <label for="fld_document" class="col-sm-3 control-label">Upload supporting document&nbsp;<span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                            <input id="fld_document" name="fld_document" type="file" class="file-loading" accept="image/*" data-parsley-required="true">
                            <div id="document_block"></div>
                            <div class="small" style="margin-top: 10px;line-height: 1.3;font-family: Calibri;font-size: 14px;color: tomato;">Allowed file type: jpg, jpeg<br>
                                Allowed File Size: Minimum: 200kb, Maximum, 2000kb</div>
                            <br>

                            <button type="button" class="btn btn-primary" onclick="$('#geo_tag_support').modal('show')"><i class="fa fa-file-text" aria-hidden="true"></i>
                                &nbsp;What is Geo Tagged Photograph ?</button>
                        </div>

                    </div>
                </div>

                <input type="hidden" name="action" value="uploadGeoTaggedImage" id="action">

                <div class="box-footer extra-footer-class" id="submitFooter">
                    <button type="submit" class="btn btn-lg btn-danger"><i class="fa fa-upload"></i>&nbsp;&nbsp;Upload Image</button>
                </div>
                <!-- /.box-footer -->
            </form>

        </div>
    </div>
</div>


</body>
<?php
    include 'common/message.php';
    include 'footer.php';
?>

<script>
    $("#fld_document").fileinput({
        previewFileType: "image",
        allowedFileExtensions: ["jpg", "jpeg"],
        previewClass: "bg-default",
        showUpload: false,
        minImageWidth: 100,
        minImageHeight: 100,
        maxImageWidth: 5000,
        maxImageHeight: 5000,
        maxFileCount: 1,
        showCaption: false,
        maxFileSize: 5000,
        minFileSize: 100,
        browseClass: "btn btn-default",
        browseLabel: "Pick Photograph",
        browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
        removeClass: "btn btn-danger",
        removeLabel: "Delete",
        removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
    });

    $("#uploadGeoTaggedImage").on("submit", function (e) {

        $("#errorlatlong, #successlatlong").css("display","none");
        $("#successlatlongtext, #errorlatlongtext").html("");

        e.preventDefault();
        var form = $(this);
        form.parsley().validate();
        $("#submitting").modal("show");

        if (form.parsley().isValid()){
            var url = "common/cfmodifyITGK.php";
            var data;
            data = new FormData(this);

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data) {

                    data = $.parseJSON(data);

                    if (data.status === "Successfully Updated") {
                        $("#submitting").modal("hide");
                        $("#successlatlong").css("display","block");
                        $("#mainForm, #submitFooter").css("display","none");
                        $("#successlatlongtext").html("Latitude longitudes have been successfully updated.");
                    } else {
                        $("#submitting").modal("hide");
                        $("#errorlatlong").css("display","block");
                        $("#errorlatlongtext").html(data.status);
                    }
                }
            });
        } else {
            $("#submitting").modal("hide");
        }
        return false;
    });
</script>

</html>