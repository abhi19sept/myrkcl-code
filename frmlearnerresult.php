<?php
$title = "Exam Result";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading"> Exam Result</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="frmlearnerresult" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                    <div class="container">
                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div> 
                    </div>

                    <div id="grid" name="grid" style="margin-top:35px;"> </div> 
                    <div class="container">
                    <iframe id="testdoc" src="" style="width: 100%;height: 500px;border: none; display: none;"></iframe>
                    </div>                     

            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillEvent() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfEventMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlExamEvent").html(data);
                }
            });
        }
        FillEvent();
    });
    $("#ddlExamEvent").change(function () {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

        $.ajax({
            type: "post",
            url: "common/cflearnerresult.php",
            data: "action=schedule&examevent=" + ddlExamEvent.value + "",
            success: function (data)
            {
                $('#response').empty();
                $("#grid").html(data);
				$('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ],
					  scrollY: 400,
                        scrollCollapse: true,
                        paging: false

                });
				
            }
        });
    });
    $("#grid").on('click', '.approvalLetter', function () {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");


        var eid = $(this).attr('eid');
        var lid = $(this).attr('lid');

        $.ajax({
            type: "post",
            url: "common/cflearnerresult.php",
            data: "action=DwnldPrintRecp&eid=" + eid + "&lid=" + lid + "",
            success: function (data) {
             
                $('#response').empty();
var data = data.trim();
                window.open(data, '_blank');
               // window.setTimeout(function () {
               //     delgeninvoice(data);
               // }, 30000);
                if(data == 'error'){
                    $('#response').empty();
                    $('#response').append("<p class='error'>Receipt Not Available Right Now.</span></p>");
                }
                else{
                    $("#testdoc").attr('src', data);
                   window.setTimeout(function () {
                       delgeninvoice(data);
                   }, 5000);                    

                }              

            }
        });
    });
    function delgeninvoice(data){
        var lid = data;
        $.ajax({
            type: "post",
            url: "common/cflearnerresult.php",
            data: "action=delgeninvoice&lid=" + lid + "",
            success: function (data) {

            }
        });
    }
</script>
</html>