<?php
    $title = "Organization Details";
    include('header.php');
    include('root_menu.php');
    include 'common/modals.php';

    if ($_SESSION["User_UserRoll"] <> 14) {
        echo "<script>$('#unauthorized').modal('show')</script>";
        die;
    }

    echo "<script>var OrgCode = '" . $_SESSION['User_Code'] . "'; </script>";
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<style type="text/css">

    .asterisk {
        color: red;
        font-weight: bolder;
        font-size: 18px;
        vertical-align: middle;
    }

    .division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 20px;
        color: #000;
        margin-bottom: 20px;

    }

    .extra-footer-class {
        margin-top: 0;
        margin-bottom: -10px;
        padding: 16px;
        background-color: #fafafa;
        border-top: 1px solid #e5e5e5;
    }

    .ref_id {
        text-align: center;
        font-size: 20px;
        color: #000;
        margin: 0 0 10px 0;
    }

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eeeeee;
        box-shadow: inset 0 0 5px 1px #d5d5d5;
    }

    .form-control {
        border-radius: 2px;
    }

    input[type=text]:hover, textarea:hover {
        box-shadow: 0 1px 3px #aaa;
        -webkit-box-shadow: 0 1px 3px #aaa;
        -moz-box-shadow: 0 1px 3px #aaa;
    }

    .col-sm-3:hover {
        background: none !important;
    }

    .modal-open .container-fluid, .modal-open .container {
        -webkit-filter: blur(5px) grayscale(50%);
        filter: blur(5px) grayscale(50%);
    }

    .btn-success {
        background-color: #00A65A !important;
    }

    .btn-success:hover {
        color: #fff !important;
        background-color: #04884D !important;
        border-color: #398439 !important;
    }

    .addBottom {
        margin-bottom: 12px;
    }

    #image_address, #image_ownershipDocument, #image_utilityBill, #image_rentAgreement {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
        cursor: pointer;
    }

    #image_address:hover, #image_ownershipDocument:hover, #image_utilityBill:hover, #image_rentAgreement:hover {
        opacity: 1;
    }

    .middle:hover {
        opacity: 1;
    }

    .floatingHeader {
        position: fixed;
        top: 0;
        visibility: hidden;
        z-index: 99999999;
        background-color: #0B0809;
        color: #FFF;
        opacity: 0.7;
    }

    
    .fancybox-slide--iframe .fancybox-content {
    width  : 800px;
    height : 800px;
    max-width  : 80%;
    max-height : 80%;
    margin: 0;
}
</style>

<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>

<div class="container" id="showdata">
    <div class="panel panel-primary" style="margin-top:46px !important;">

        <div class="panel-heading">Address Update Request Made by Center : <span id="fld_ITGK_Code"></span></div>
        <div class="panel-body">

            <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateOrgDetails" name="updateOrgDetails"  enctype="multipart/form-data">
                <input type="hidden" id="action" name="action" value="commit">
                <input type="hidden" id="Code" name="Code" value="commit">
                <input type="hidden" id="fld_ref_id" name="fld_ref_id" value="commit">
                <div class="ref_id">
                    Reference ID: <span id="ref_id"></span>
                </div>

                <div class="persist-area">
                    <div class="row persist-header">
                        <div class="col-md-6"><h3 style="text-align: center">Previous</h3></div>
                        <div class="col-md-6"><h3 style="text-align: center">Requested</h3></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="division_heading">
                                Center Details
                            </div>


                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12 addBottom">
                                        Organization Name
                                        <input type="text" class="form-control" name="Organization_Name_old" id="Organization_Name_old" placeholder="Name of the Organization/Center"
                                               readonly='readonly'>
                                        <!--<input type="text" id="Organization_ITGK_Code" name="Organization_ITGK_Code">-->
                                    </div>


                                    <div class="col-sm-12 addBottom">
                                        Type of Organization
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Type_old" id="Organization_Type_old" readonly='readonly'>
                                    </div>
                                </div>


                            </div>

                            <div class="division_heading">
                                Location Details
                            </div>

                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12 addBottom">
                                        Country
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Country_old" id="Organization_Country_old" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        State
                                        <input type="text" class="form-control" maxlength="50" name="Organization_State_old" id="Organization_State_old" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Division/Region
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Region_old" id="Organization_Region_old" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        District
                                        <input type="text" class="form-control" maxlength="50" name="Organization_District_old" id="Organization_District_old" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Tehsil
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Tehsil_old" id="Organization_Tehsil_old" readonly='readonly'>
                                    </div>
                                </div>

                            </div>


                            <div class="division_heading">
                                Area Details
                            </div>


                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        Area Type
                                        <input type="text" class="form-control" maxlength="50" name="Organization_AreaType_old" id="Organization_AreaType_old" readonly='readonly'>
                                    </div>

                                </div>


                                <div id="urbanArea_old" style="display: block;">

                                    <div class="form-group">

                                        <div class="col-sm-12 addBottom">
                                            Municipality Type
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Municipal_Type_old" id="Organization_Municipal_Type_old"
                                                   readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">
                                            Municipality Name
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Municipal_old" id="Organization_Municipal_old" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">
                                            Ward Number
                                            <input type="text" class="form-control" maxlength="50" name="Organization_WardNo_old" id="Organization_WardNo_old" readonly='readonly'>
                                        </div>

                                    </div>

                                </div>

                                <div id="ruralArea_old" style="display: none;">
                                    <div class="form-group">

                                        <div class="col-sm-12 addBottom">Panchayat Samiti/Block
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Panchayat_old" id="Organization_Panchayat_old" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">Gram Panchayat
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Gram_old" id="Organization_Gram_old" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">Village
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Village_old" id="Organization_Village_old" readonly='readonly'>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="division_heading">
                                Address for Correspondence
                            </div>

                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">
                                    <div class="col-sm-12">Ownership Type
                                        <input type="text" class="form-control" maxlength="50" name="Organization_OwnershipType_old" id="Organization_OwnershipType_old"
                                               readonly='readonly'>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">Full Address for Correspondence
                                        <textarea id="Organization_Address_old" name="Organization_Address_old" class="form-control" rows="4" cols="50" readonly></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="division_heading">
                                &nbsp;
                            </div>


                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12 addBottom">
                                        Organization Name
                                        <input type="text" class="form-control" name="Organization_Name" id="Organization_Name" placeholder="Name of the Organization/Center"
                                               readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Type of Organization
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Type" id="Organization_Type" readonly='readonly'>
                                    </div>
                                </div>


                            </div>

                            <div class="division_heading">
                                &nbsp;
                            </div>

                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12 addBottom">
                                        Country
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Country" id="Organization_Country" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        State
                                        <input type="text" class="form-control" maxlength="50" name="Organization_State" id="Organization_State" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Division/Region
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Region" id="Organization_Region" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        District
                                        <input type="text" class="form-control" maxlength="50" name="Organization_District" id="Organization_District" readonly='readonly'>
                                    </div>

                                    <div class="col-sm-12 addBottom">
                                        Tehsil
                                        <input type="text" class="form-control" maxlength="50" name="Organization_Tehsil" id="Organization_Tehsil" readonly='readonly'>
                                    </div>
                                </div>

                            </div>


                            <div class="division_heading">
                                &nbsp;
                            </div>


                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">

                                    <div class="col-sm-12">
                                        Area Type
                                        <input type="text" class="form-control" maxlength="50" name="Organization_AreaType" id="Organization_AreaType" readonly='readonly'>
                                    </div>

                                </div>


                                <div id="urbanArea" style="display: block;">

                                    <div class="form-group">

                                        <div class="col-sm-12 addBottom">
                                            Municipality Type
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Municipal_Type" id="Organization_Municipal_Type" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">
                                            Municipality Name
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Municipal" id="Organization_Municipal" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">
                                            Ward Number
                                            <input type="text" class="form-control" maxlength="50" name="Organization_WardNo" id="Organization_WardNo" readonly='readonly'>
                                        </div>

                                    </div>

                                </div>

                                <div id="ruralArea" style="display: none;">
                                    <div class="form-group">

                                        <div class="col-sm-12 addBottom">Panchayat Samiti/Block
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Panchayat" id="Organization_Panchayat" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">Gram Panchayat
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Gram" id="Organization_Gram" readonly='readonly'>
                                        </div>


                                        <div class="col-sm-12 addBottom">Village
                                            <input type="text" class="form-control" maxlength="50" name="Organization_Village" id="Organization_Village" readonly='readonly'>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="division_heading">
                                &nbsp;
                            </div>

                            <div class="box-body" style="margin: 0 100px;">

                                <div class="form-group">
                                    <div class="col-sm-12">Ownership Type
                                        <input type="text" class="form-control" maxlength="50" name="Organization_OwnershipType" id="Organization_OwnershipType" readonly='readonly'>
                                    </div>
                                </div>
                                    <div class="form-group" id="owner" style="display:none;">
                                        <div class="col-sm-12">Owner Name
                                            <input type="text" class="form-control" name="Owner_Name" id="Owner_Name" readonly='readonly'>
                                        </div>
                                    </div>
                                
                                <div class="form-group">

                                    <div class="col-sm-12">Full Address for Correspondence
                                        <textarea id="Organization_Address" name="Organization_Address" class="form-control" rows="4" cols="50" readonly></textarea>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="division_heading">
                                Uploaded Document
                            </div>

                            <div id="onRent" style="display: none;">
                                <button type="button" id="ViewRentAgreement" class="btn btn-primary" data-toggle="collapse" data-target="#RA"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Rent
                                    Agreement
                                </button>
                                <div class="box-body collapse" style="margin: 0 100px;" id="RA">
                                    <div class="form-group">
                                        <div class="col-sm-12"><br><br>
                                            <iframe id="image_rentAgreement" style="width: 100%; height: 500px; border: none;"></iframe>
                                            <div id="image_rentAgreement_ftp"></div>
                                            <div class="middle">
                                                <div class="text">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="division_heading">
                                    &nbsp;
                                </div>

                                <button type="button" id="ViewUtilityBill" class="btn btn-primary" data-toggle="collapse" data-target="#UB"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Utility Bill</button>
                                <div class="box-body collapse" style="margin: 0 100px;" id="UB">
                                    <div class="form-group">
                                        <div class="col-sm-12"><br><br>
                                            <iframe id="image_utilityBill" style="width: 100%; height: 500px; border: none;"></iframe>
                                            <div id="image_utilityBill_ftp"></div>
                                            <div class="middle">
                                                <div class="text">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="division_heading">
                                    &nbsp;
                                </div>
                            </div>


                            <div id="onOwn" style="display: none;">
                                <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#OD"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Ownership Document</button>
                                <div class="box-body collapse" style="margin: 0 100px;" id="OD">
                                    <div class="form-group">
                                        <div class="col-sm-12"><br><br>
                                            <iframe id="image_ownershipDocument" style="width: 100%; height: 500px; border: none;"></iframe>
                                            <div id="image_ownershipDocument_ftp"></div>
                                            <div class="middle">
                                                <div class="text">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="division_heading">
                                    &nbsp;
                                </div>
                            </div>

                            <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#FF"><i class="fa fa-eye"></i>&nbsp;&nbsp;View Front Photo of New Location</button>
                            <div class="box-body collapse" style="margin: 0 100px;" id="FF">
                                <div class="form-group">
                                    <div class="col-sm-12"><br><br>
                                        <iframe id="image_address" style="width: 100%; height: 500px; border: none;"></iframe>
                                       <div id="image_address_ftp"></div>
                                        <div class="middle">
                                            <div class="text">&nbsp;</div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <p>&nbsp;</p>

                    <div class="row">
                        <div class="col-md-12">
<!--                            <div class="division_heading">
                                Remarks
                            </div>-->
<!--                            <div class="container" id="tnc"> 
                                    <div class="col-md-11"> 	

                                        <label for="learnercode" style="float:left;margin:left:15px" ><b>I have verified all the details mentioned by ITGK and found correct and in-line with RKCL’s address change policy. Recommended for approval.</b></label></br>                              
                                        <label class="checkbox-inline" style="float:left;"> <span class="star"> </span><input type="checkbox" name="chk" id="chk" value="1" >
                                            I Accept 
                                        </label>
                                    </div>
                               </div>-->

<!--                            <div class="box-body" style="margin: 0 100px;" >

                                <div class="form-group" id="remarks">
                                    <div class="col-sm-12"><span id="remark_label">Please enter the remarks for action you are taking</span> <span class="asterisk">*</span>
                                        <textarea id="fld_remarks" name="fld_remarks" class="form-control" rows="4" cols="50" style="border-radius: 8px; font-family: Calibri;" onkeypress="javascript:return validAddress2(event);"></textarea>
                                    </div>
                                </div>

                            </div>-->

                            <div class="box-body" style="margin: 0 100px;display: none;" id="rkcl_remarks">

                                <div class="form-group">
                                    <div class="col-sm-12"><span id="remark_label">Remarks by RKCL</span> <span class="asterisk">*</span>
                                        <textarea id="fld_remarks_rkcl" name="fld_remarks_rkcl" class="form-control" rows="4" cols="50" style="border-radius: 8px; font-family: Calibri;
"></textarea>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info" id="visitdiv">
                    <div class="panel-heading">Address Change Visit Details</div>
                    <div class="panel-body">
                        <div class="container">
                            <div class="col-sm-4 form-group">     
                                        <label for="faname">Date of Visit:<span class="star">*</span></label>
                                        <input type="text" class="form-control" data-parsley-required="true" maxlength="50" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD"  readonly="true">
                                    </div>
                        </div>
                        <div class="container">
                                    <div class="col-sm-4 form-group" > 
                                        <label for="photo">Front View Photo:<span class="star">*</span></label>
                                        <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->								  
                                        <input style=" width: 115%" id="uploadImage7" type="file" name="uploadImage7" onchange="checkPANPhoto(this);"/>	
                                        <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                        <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                                    </div>
                                
                                    <div class="col-sm-4 form-group" > 
                                        <label for="photo">Left View Photo:<span class="star">*</span></label>
                                        <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->								  
                                        <input style=" width: 115%" id="uploadImage8" type="file" name="uploadImage8" onchange="checkAADHARPhoto(this);"/>
                                        <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                        <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                                    </div>
                              
                                    <div class="col-sm-4 form-group" > 
                                        <label for="photo">Right View Photo:<span class="star">*</span></label>
                                        <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->								  
                                        <input style=" width: 115%" id="uploadImage9" type="file" name="uploadImage9" onchange="checkAddressProof(this);"/>	
                                        <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                        <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                                    </div>
                        
                                    <div class="col-sm-4 form-group" > 
                                        <label for="photo">Lab Space Photo:<span class="star">*</span></label>
                                        <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->								  
                                        <input style=" width: 115%" id="uploadImage6" type="file" name="uploadImage6" onchange="checkNCRApp(this);"/>	
                                        <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                        <span style="font-size:10px;">Note: JPG Allowed Size = 100 to 200 KB</span>
                                    </div>
                                </div>
                    </div>
                </div>

                        </div>
                    </div>
                    
                    
                    <div class="row" id="current_status" style="display: none;">
                        <div class="col-md-12">
                            <div class="division_heading">
                                Status Log
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="pending_submission">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request haven't submitted yet
                                    </div>
                                </div>
                            </div>


                            <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_approval_sp">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request Submitted on <span id="submission_date"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_approval_rkcl">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request Approved by Service Provider on <span id="submission_date_sp"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="pending_for_payment">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request Approved by RKCL on <span id="submission_date_rkcl"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="rkcl_rejected">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request has been Rejected by RKCL on <span id="rejected_date_rkcl"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="itgk_rejected">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request has been Rejected by ITGK on <span id="rejected_date_itgk"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="box-body" style="margin: 0 100px; display: none;" id="sp_rejected">
                                <div class="form-group">
                                    <div class="col-sm-12 text-danger" style="font-size: 15px;">
                                        Request has been Rejected by SP on <span id="rejected_date_sp"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="box-body" style="margin: 0 100px; display: none;" id="paid">
                                <div class="form-group">
                                    <div class="col-sm-8 text-success">
                                        Paid on <span id="payment_date"></span>
                                    </div>
 
<!--                        <button type="button" class="btn btn-lg btn-primary col-md-2" onclick="window.location.href='frmAddressUpdateRequest.php'"><i class="fa fa-arrow-left"
                                                                                                                                                      aria-hidden="true"></i>&nbsp;&nbsp;Back
                        </button>-->
                                    <div class="col-sm-4 pull-right">
                                        <button type="button" class="btn btn-lg btn-primary" onclick="viewTransactionDetail('<?php echo $_REQUEST['Code'] ?>')">Click here for transaction
                                            details&nbsp;<i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>


                    <!-- /.box-body -->
                    <div class="box-footer extra-footer-class col-md-12" id="updateActions" style="display: none;">
                        
                       
                        <div class="form-group">
                        <label for="area" class="col-sm-3 control-label">Select Appropriate Option <span class="asterisk">*</span></label>

                        <div class="col-sm-4" style="padding-top: 7px;">
                            <input type="radio" name="Application_Decision" value="Approve" id="Approve">
                            <label for="Organization_ownershipType">Address is complete and correct.</label>
                        </div>

                        <div class="col-sm-4" style="padding-top: 7px;">
                            <input type="radio" name="Application_Decision" value="Reject" id="Reject">
                            <label for="Organization_OwnershipType">Address is not complete or not correct.</label>
                        </div>
                        <input type="hidden" id="Organization_OwnershipType_old" name="Organization_OwnershipType_old">
                    </div>
                        <div class="panel panel-success" id="approvediv" style="display:none;">
                                <div class="panel-heading">Approve Application</div>
                                <div class="panel-body">
                                    <div class="container">
                                    <div class="form-group" id="tnc"> 
                                        <label for="learnercode" ><b>I have verified all the details mentioned by ITGK and found correct and in-line with RKCL’s address change policy.</b></label></br>                              
                                        <label class="checkbox-inline" class="form-group"> <span class="star"> </span><input type="checkbox" class="form-group" name="chk" id="chk" value="1" >
                                            I Accept 
                                        </label>
                               </div>
                                        
                                        <textarea id="fld_remarks" name="fld_remarks" class="form-group" rows="4" cols="170" style="border-radius: 8px; font-family: Calibri;" onkeypress="javascript:return validAddress2(event);"></textarea><br>
                                        <!--<button type="button" id="commit" style="display:none;" class="btn btn-lg btn-successt"><i class="fa fa-check" aria-hidden="true"></i>&nbsp;&nbsp;Accept</button>-->
                                        <input type="submit" id="commit" style="display:none;" class="btn btn-lg btn-success">
                                    </div>
                            </div>
<!--                        <div class="panel panel-danger form-group col-sm-4" style="margin-left:100px">
                                <div class="panel-heading">Reject Application</div>
                                <div class="panel-body">
                                    <div class="form-group"> 
                            <label for="empid">Select Reason to rejection of application:<span class="star">*</span></label>
                            <select id="ddlReason" name="ddlReason" class="form-control" required>
                                <option value="">Select</option>
                                <option value="ITGK did not uploaded required documents">ITGK did not uploaded required documents</option>
                                <option value="ITGK uploaded documents are not valid">ITGK uploaded documents are not valid/visible</option>
                                <option value="Address mentioned is not complete">Address mentioned is not complete/correct</option>
                                <option value="Based on SP Visit address change application is not recommended">Based on SP Visit address change application is not recommended</option>
                                <option value="ITGK mistakenly submitted application">ITGK mistakenly submitted application</option>
                                <option value="Other reasons">Other reasons</option>
                            </select>
                            <br><br>
                            <button type="button" style="display:none" id="reject" class="btn btn-lg btn-danger pull-right"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;Reject</button>
                        </div>
                                </div>
                            </div>-->
                        

                    </div>
                        <div class="container" id="showsmsdiv" style="display:none;">
                    <input type="button" name="btnshowsms"  id="btnshowsms" class="btn btn-default" value="Put Application On-Hold"/>   
                </div>
                <div class="panel panel-info" id="smspanel" style="display:none">
                    <div class="panel-heading">Put Application On-Hold</div>
                    <div class="panel-body">	
                        <div id="SMSresponse"></div>
                        <div class="container">
                            <div class="col-sm-8">     
                                <label for="learnercode">Enter SMS to be sent to ITGK. Applicaion will be On-Hold till Re-Submit from ITGK:</label>
                                <textarea class="form-control" width="700px" height="103px"   name="txtsms" id="txtsms" value=""></textarea>
                            </div>
                            <br><br>
                            <div class="col-sm-4"> 
                                <input type="button" name="btnsendsms" id="btnsendsms" class="btn btn-primary" value="Send SMS to ITGK"/>    
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- /.box-footer -->
                </div>

<!--                    <div class="container">
                    <input type="button" name="btnshowsms" id="btnshowsms" class="btn btn-default" value="Put Application On-Hold"/>   
                </div>
                <div class="panel panel-info" id="smspanel" style="display:none">
                    <div class="panel-heading">Put Application On-Hold</div>
                    <div class="panel-body">	
                        <div id="SMSresponse"></div>
                        <div class="container">
                            <div class="col-sm-8">     
                                <label for="learnercode">Enter SMS to be sent to ITGK. Applicaion will be On-Hold till Re-Submit from ITGK:</label>
                                <textarea class="form-control" width="700px" height="103px"   name="txtsms" id="txtsms" value=""></textarea>
                            </div>
                            <br><br>
                            <div class="col-sm-4"> 
                                <input type="button" name="btnsendsms" id="btnsendsms" class="btn btn-primary" value="Send SMS to ITGK"/>    
                            </div>
                        </div>
                    </div>
                </div>-->

        </div>


        </form>

    </div>
</div>
</div>


</body>
<?php
    include 'common/message.php';
    include 'footer.php';
?>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="bootcss/js/persistentHeader.js"></script>
<script language="javascript" type="text/javascript">
    function checkPANPhoto(target) {
        var ext = $('#uploadImage7').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg']) == -1) {
            alert('Image must be in JPG Format');
            document.getElementById("uploadImage7").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        }
        document.getElementById("uploadImage7").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkAADHARPhoto(target) {
        var ext = $('#uploadImage8').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg']) == -1) {
            alert('Image must be in JPG Format');
            document.getElementById("uploadImage8").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage8").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage8").value = '';
            return false;
        }
        document.getElementById("uploadImage8").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkAddressProof(target) {
        var ext = $('#uploadImage9').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg']) == -1) {
            alert('Image must be in JPG Format');
            document.getElementById("uploadImage9").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage9").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage9").value = '';
            return false;
        }
        document.getElementById("uploadImage9").innerHTML = "";
        return true;
    }
</script>
<script language="javascript" type="text/javascript">
    function checkNCRApp(target) {
        var ext = $('#uploadImage6').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg']) == -1) {
            alert('Image must be in JPG Format');
            document.getElementById("uploadImage6").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage6").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage6").value = '';
            return false;
        }
        document.getElementById("uploadImage6").innerHTML = "";
        return true;
    }
</script>
<script type="text/javascript">

    var image;
    var usercode;
    var Organization_User_Code;
    var fld_ownershipDocument;
    var fld_rentAgreement;
    var fld_utilityBill;


 $(function () {
            $("#txtEstdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
        
    function validAddress2(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("^[\\w.# ,-/]+$");

        if (key === 8 || key === 0 || key === 32) {
            keychar = "a";
        }
        return reg.test(keychar);
    }
    
    $("#Approve").click(function () {
            $("#approvediv").css("display", "block");
            $("#showsmsdiv").css("display", "none");
        });

        $("#Reject").click(function () {
            //alert("hi");
            $("#showsmsdiv").css("display", "block");
            //$("#btnshowsms").show();
            $("#approvediv").css("display", "none");
        });
    
    $('#chk').change(function () {
            if (this.checked) {
                 $('#commit').show(3000);
                 //$("#fld_remarks").css("display", "block");
                $('#chk').attr('disabled', true);
            } else {
                alert("Please select Check Box to Proceed.");
            }
        });
        
        $("#btnshowsms").click(function () {
// alert($("#fld_ITGK_Code").text());
            $("#submitdiv").hide();
            $("#btnshowsms").hide();
            $("#smspanel").show(3000);
            document.getElementById("txtsms").defaultValue = "Enter SMS that will be sent to ITGK.";

        });
        
                $("#btnsendsms").click(function () {
                $('#SMSresponse').empty();
                $('#SMSresponse').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfmodifyITGK.php"; // the script where you handle the form input.            
                var data;
                var CenterCode = $("#fld_ITGK_Code").text();
                    //data = "action=LoginApprove&orgcode=" + Code + "&status=" + ddlstatus.value + "&remark=" + txtRemark.value + "&districtcode=" + txtDistrictCode.value + ""; // serializes the form's elements.
                    data = "action=SendSMS&sms=" + txtsms.value + "&centercode=" + CenterCode + "&fld_ref_id=" + decryptString('<?php echo $_REQUEST['Code']?>') +""; // serializes the form's elements.
                
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        alert(data);
                        if (data == 'Success')
                        {
                            $('#SMSresponse').empty();
                            $('#SMSresponse').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>SMS has been sent to ITGK Successfully</span></p>");
                            BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; SMS has been sent to ITGK Successfully.</span>");
                            window.setTimeout(function () {
                                window.location.href = "frmAddressUpdateRequestCenterDetail.php";
                            }, 3000);

                            Mode = "Add";
                            resetForm("form");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();


                    }
                });

                return false;
            });
        
//        $('#ddlReason').change(function () {
//            if (this.value != '') {
//                $('#reject').show(3000);
//            } else {
//                $('#reject').hide();
//                alert("Please select reason to reject application.");
//            }
//        });

    function fillLocationDetails(Code) {
        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=FillLocationDetails&Code=" + Code+ "&fld_ref_id=" + decryptString('<?php echo $_REQUEST['Code']?>') +"",
            success: function (data) {
                data = $.parseJSON(data);
                $("#Organization_Country_old").val(data[0].Organization_Country);
                $("#Organization_State_old").val(data[0].Organization_State);
                $("#Organization_Region_old").val(data[0].Organization_Region);
                $("#Organization_District_old").val(data[0].Organization_District);
                $("#Organization_Tehsil_old").val(data[0].Organization_Tehsil_Old);

                $("#Organization_Country").val(data[0].Organization_Country);
                $("#Organization_State").val(data[0].Organization_State);
                $("#Organization_Region").val(data[0].Organization_Region);
                $("#Organization_District").val(data[0].Organization_District);
                $("#Organization_Tehsil").val(data[0].Organization_Tehsil);
            }
        });
    }

    function viewTransactionDetail(Code) {
        window.location.href = "viewTransactionDetails.php?Code=" + Code;
    }

    $("#image_address").on("click", function () {

        $("#download_document").attr("onclick", "downloadFile('" + image + "','" + usercode + "')");
        $("#view_document").modal('show');
    });

    $("#image_ownershipDocument").on("click", function () {

        $("#download_document_own").attr("onclick", "downloadFile('" + fld_ownershipDocument + "','" + usercode + "')");
        $("#view_document_ownershipDocument").modal('show');
    });

    $("#image_utilityBill").on("click", function () {

        $("#download_document_utility").attr("onclick", "downloadFile('" + fld_utilityBill + "','" + usercode + "')");
        $("#view_document_utilityBill").modal('show');
    });

    $("#image_rentAgreement").on("click", function () {

        $("#download_document_agree").attr("onclick", "downloadFile('" + fld_rentAgreement + "','" + usercode + "')");
        $("#view_document_rentAgreement").modal('show');
    });

    function downloadFile(img, usc) {
        window.location.href = 'download_document.php?id=' + img + '&usercode=' + usc + "&flag=address_proof";
    }

    $("#ref_id").html(decryptString('<?php echo $_REQUEST['Code']?>'));
    $("#Code").val(decryptString('<?php echo $_REQUEST['Code']?>'));
    $("#fld_ref_id").val(decryptString('<?php echo $_REQUEST['Code']?>'));

    function fillForm() {

        $("#revoke_success").modal("show");

        $.ajax({
            type: "post",
            url: "common/cfmodifyITGK.php",
            data: "action=FillUpdateAddressDetails&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "",
            success: function (data) {
                data = $.parseJSON(data);

                setTimeout(function () {

                    image = data[0].fld_document_encoded;
                    usercode = data[0].Organization_User_Code_encoded;
                    Organization_User_Code = data[0].Organization_User_Code;

                    if (data[0].Organization_OwnershipType === "Own") {
                        fld_ownershipDocument = data[0].fld_ownershipDocument_encoded;
                        $("#onRent").css("display", "none");
                        $("#onOwn").css("display", "block");
                    }

                    if (data[0].Organization_OwnershipType === "Rent") {
                        fld_rentAgreement = data[0].fld_rentAgreement_encoded;
                        fld_utilityBill = data[0].fld_utilityBill_encoded;
                        $("#onRent").css("display", "block");
                        $("#onOwn").css("display", "none");
                        $("#Owner_Name").val(data[0].Owner_Name);
                        $("#owner").css("display", "block");
                    }
                    
                    if (data[0].Organization_OwnershipType === "Relative") {
                                fld_rentAgreement = data[0].fld_rentAgreement_encoded;
                                fld_utilityBill = data[0].fld_utilityBill_encoded;
                                $("#onRent").css("display", "block");
                                $("#onOwn").css("display", "none");
                        $("#Owner_Name").val(data[0].Owner_Name);
                        $("#owner").css("display", "block");
                        $("#ViewRentAgreement").html('Ownership Document');
                        $("#ViewUtilityBill").html('NOC Certificate');
                            }
                            
                            if (data[0].Organization_OwnershipType === "Special") {
                                fld_rentAgreement = data[0].fld_rentAgreement_encoded;
                                fld_utilityBill = data[0].fld_utilityBill_encoded;
                                $("#onRent").css("display", "block");
                                $("#onOwn").css("display", "none");
                        $("#Owner_Name").val(data[0].Owner_Name);
                        $("#owner").css("display", "block");
                        $("#ViewRentAgreement").html('Authority Letter');
                        $("#ViewUtilityBill").html('NOC Letter by AUTHORIZED Person');
                            }

                    /*** OLD DATA ***/

                    $("#Organization_Name_old").val(data[0].Organization_Name);

                    $("#Organization_Country_old").val(data[0].Organization_Country);
                    $("#Organization_State_old").val(data[0].Organization_State);
                    $("#Organization_Region_old").val(data[0].Organization_Region);
                    $("#Organization_District_old").val(data[0].Organization_District);
                    $("#Organization_Tehsil_old").val(data[0].Organization_Tehsil);

                    $("#Organization_AreaType_old").val(data[0].Organization_AreaType_old);
                    $("#Organization_OwnershipType_old").val(data[0].Organization_OwnershipType_old);

//                    $("#view_image_large").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document);
//                    $("#view_image_large_rentAgreement").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_rentAgreement);
//                    $("#view_image_large_utilityBill").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_utilityBill);
//                    $("#view_image_large_ownershipDocument").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_ownershipDocument);
//
//                    $("#image_address").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document+"#zoom=95");
//                    $("#image_rentAgreement").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_rentAgreement+"#zoom=95");
//                    $("#image_utilityBill").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_utilityBill+"#zoom=95");
//                    $("#image_ownershipDocument").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_ownershipDocument+"#zoom=95");


$("#image_rentAgreement").hide();
$("#image_rentAgreement_ftp").html(data[0].fld_rentAgreement);

$("#image_address").hide();
$("#image_address_ftp").html(data[0].fld_document);

$("#image_ownershipDocument").hide();
$("#image_ownershipDocument_ftp").html(data[0].fld_ownershipDocument);

$("#image_utilityBill").hide();
$("#image_utilityBill_ftp").html(data[0].fld_utilityBill);





//                    $.ajax({
//                        url: "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document,
//                        type: 'HEAD',
//                        error: function ()
//                        {
//                            $("#image_address").attr("src", "upload/address_proof/" + data[0].fld_document + "#zoom=95");
//                        },
//                        success: function ()
//                        {
//                            $("#image_address").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_document + "#zoom=95");
//                        }
//                    });

//                    $.ajax({
//                        url: "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_rentAgreement,
//                        type: 'HEAD',
//                        error: function ()
//                        {
//                            $("#image_rentAgreement").attr("src", "upload/address_proof/" + data[0].fld_rentAgreement + "#zoom=95");
//                        },
//                        success: function ()
//                        {
//                            $("#image_rentAgreement").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_rentAgreement + "#zoom=95");
//                        }
//                    });
//
//                    $.ajax({
//                        url: "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_utilityBill,
//                        type: 'HEAD',
//                        error: function ()
//                        {
//                            $("#image_utilityBill").attr("src", "upload/address_proof/" + data[0].fld_utilityBill + "#zoom=95");
//                        },
//                        success: function ()
//                        {
//                            $("#image_utilityBill").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_utilityBill + "#zoom=95");
//                        }
//                    });
//
//                    $.ajax({
//                        url: "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_ownershipDocument,
//                        type: 'HEAD',
//                        error: function ()
//                        {
//                            $("#image_ownershipDocument").attr("src", "upload/address_proof/" + data[0].fld_ownershipDocument + "#zoom=95");
//                        },
//                        success: function ()
//                        {
//                            $("#image_ownershipDocument").attr("src", "upload/address_proof/" + data[0].Organization_User_Code + "/" + data[0].fld_ownershipDocument + "#zoom=95");
//                        }
//                    });


                    if (data[0].Organization_AreaType_old === "Urban") {
                        $("#urbanArea_old").css('display', 'block');
                        $("#ruralArea_old").css('display', 'none');
                        $("#Organization_Municipal_Type_old").val(data[0].Organization_Municipal_Type_old);
                        $("#Organization_Municipal_old").val(data[0].Organization_Municipal_old);
                        $("#Organization_WardNo_old").val(data[0].Organization_WardNo_old);
                        $("#Organization_Panchayat_old").val('');
                        $("#Organization_Gram_old").val('');
                        $("#Organization_Village_old").val('');
                    }

                    if (data[0].Organization_AreaType_old === "Rural") {
                        $("#urbanArea_old").css('display', 'none');
                        $("#ruralArea_old").css('display', 'block');
                        $("#Organization_Municipal_Type_old").val('');
                        $("#Organization_Municipal_old").val('');
                        $("#Organization_WardNo_old").val('');
                        $("#Organization_Panchayat_old").val(data[0].Organization_Panchayat_old);
                        $("#Organization_Gram_old").val(data[0].Organization_Gram_old);
                        $("#Organization_Village_old").val(data[0].Organization_Village_old);
                    }

                    $("#Organization_Address_old").val(data[0].Organization_Address_old);


                    /*** NEW DATA ***/

                    $("#Organization_OwnershipType").val(data[0].Organization_OwnershipType);

                    $("#Organization_ITGK_Code").html(data[0].Organization_ITGK_Code);
                    $("#fld_ITGK_Code").html(data[0].Organization_ITGK_Code);
                    $("#Organization_Name").val(data[0].Organization_Name);

                    $("#Organization_Type").val(data[0].Organization_Type);
                    $("#Organization_Type_old").val(data[0].Organization_Type_old);
                    $("#Organization_Country").val(data[0].Organization_Country);
                    $("#Organization_State").val(data[0].Organization_State);
                    $("#Organization_Region").val(data[0].Organization_Region);
                    $("#Organization_District").val(data[0].Organization_District);
                    $("#Organization_Tehsil").val(data[0].Organization_Tehsil);

                    $("#Organization_AreaType").val(data[0].Organization_AreaType);

                    if (data[0].Organization_AreaType === "Urban") {
                        $("#urbanArea").css('display', 'block');
                        $("#ruralArea").css('display', 'none');
                        $("#Organization_Municipal_Type").val(data[0].Organization_Municipal_Type);
                        $("#Organization_Municipal").val(data[0].Organization_Municipal);
                        $("#Organization_WardNo").val(data[0].Organization_WardNo);
                        $("#Organization_Panchayat").val('');
                        $("#Organization_Gram").val('');
                        $("#Organization_Village").val('');
                    }

                    if (data[0].Organization_AreaType === "Rural") {
                        $("#urbanArea").css('display', 'none');
                        $("#ruralArea").css('display', 'block');
                        $("#Organization_Municipal_Type").val('');
                        $("#Organization_Municipal").val('');
                        $("#Organization_WardNo").val('');
                        $("#Organization_Panchayat").val(data[0].Organization_Panchayat);
                        $("#Organization_Gram").val(data[0].Organization_Gram);
                        $("#Organization_Village").val(data[0].Organization_Village);
                    }
                    $("#Organization_Address").val(data[0].Organization_Address);
                    $("#submission_date").html(data[0].submission_date);
                    $("#submission_date_sp").html(data[0].submission_date_sp);
                    $("#submission_date_rkcl").html(data[0].submission_date_rkcl);

                    fillLocationDetails(Organization_User_Code);

                    $("#fld_remarks").val(data[0].fld_remarks);
                    $("#fld_remarks_rkcl").val(data[0].fld_remarks_rkcl);

                }, 3000);

                setTimeout(function () {
                    $("#revoke_success").modal("hide");
                }, 3000);

                $("#current_status").css("display", "block");


                /*** REQUEST NOT SUBMITTED ***/
                if (data[0].fld_status === "0") {
                    $("#current_status").css("display", "block");
                    $("#pending_submission").css("display", "block");
                }

                /*** REQUEST SUBMITTED, PENDING FOR APPROVAL BY SERVICE PROVIDER ***/
                if (data[0].fld_status === "1") {
                    $("#updateActions").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#revoke_div").css("display", "block");
                }

                /*** APPROVED BY SERVICE PROVIDER, PENDING FOR APPROVAL BY RKCL ***/
                if (data[0].fld_status === "2") {
                    $("#remark_label").html('Entered Remarks');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#pending_for_approval_rkcl").css("display", "block");
                    
                     $("#visitdiv").css("display", "none");
                }

                /*** APPROVED BY RKCL, PENDING FOR PAYMENT ***/
                if (data[0].fld_status === "3") {
                    //$("#remark_label").html('Entered Remarks');
                    //$("#fld_remarks").attr("readonly", "readonly");
                    //$("#fld_remarks_rkcl").attr("readonly", "readonly");
                    //$("#rkcl_remarks").css("display", "block");
                    //$("#submit_pending").css("display", "block");
                     $("#visitdiv").css("display", "none");
                    $("#remarks").css("display", "none");
                    $("#current_status").css("display", "block");
                    $("#btnshowsms").css("display", "none");
                    $("#pending_for_approval_sp").css("display", "block");
                    //$("#pending_for_approval_rkcl").css("display", "block");
                    //$("#pending_for_payment").css("display", "block");
                }

                /*** REJECTED BY RKCL ***/
                if (data[0].fld_status === "4") {
                    $("#remark_label").html('Entered Remarks');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_rkcl").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#rkcl_rejected").css("display", "block");
                    $("#rejected_date_rkcl").html(data[0].fld_updatedOn);
                    $("#visitdiv").css("display", "none");
                    $("#paid").css("display", "block");
                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>'),
                        success: function (data2) {
                            data2 = $.parseJSON(data2);
                            $("#payment_date").html(data2[0].fld_updatedOn);
                        }
                    });
                }

                /*** PAID BY ITGK ***/
                if (data[0].fld_status === '5') {
                    //$("#remark_label").html('Entered Remarks');
                    //$("#fld_remarks").attr("readonly", "readonly");
                    //$("#fld_remarks_rkcl").attr("readonly", "readonly");
                    //$("#rkcl_remarks").css("display", "block");
                    $("#updateActions").css("display", "block");
                     $("#remarks").css("display", "none");
                    $("#submit_pending").css("display", "block");
                    //$("#updateActions").css("display", "none");

                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code']?>'),
                        success: function (data2) {
                            data2 = $.parseJSON(data2);
                            $("#payment_date").html(data2[0].fld_updatedOn);
                        }
                    });

                    $("#pending_for_approval_sp").css("display", "block");
                    //$("#pending_for_approval_rkcl").css("display", "block");
                    //$("#pending_for_payment").css("display", "block");
                    
                    $("#revoke_div").css("display", "block");
                    
                    $("#paid").css("display", "block");
                }

                /*** REJECTED BY ITGK ***/
                if (data[0].fld_status === "6") {
                    $("#remark_label").html('Entered Remarks');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#itgk_rejected").css("display", "block");
                    $("#rejected_date_itgk").html(data[0].fld_updatedOn);
                }
                
                /*** REJECTED BY SP ***/
                if (data[0].fld_status === "7") {
                    $("#remark_label").html('Entered Remarks');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#remarks").css("display", "block");
                    $("#tnc").css("display", "none");
                    $("#submit_pending").css("display", "block");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#sp_rejected").css("display", "block");
                    $("#rejected_date_sp").html(data[0].fld_updatedOn);
                }
                
                 /*** RM APPROVED ***/
                if (data[0].fld_status === "9") {
                    $("#remark_label").html('Entered Remarks by RKCL');
                    $("#remark_label_rkcl").html('Entered Remarks by You');
                    $("#fld_remarks").attr("readonly", "readonly");
                    $("#fld_remarks_rkcl").attr("readonly", "readonly");
                    $("#rkcl_remarks").css("display", "block");
                    $("#sp_remarks").css("display", "block");
                    $("#submit_pending").css("display", "block");
                    $("#updateActions").css("display", "none");
                    $("#visitdiv").css("display", "none");
                    $("#current_status").css("display", "block");
                    $("#pending_for_approval_sp").css("display", "block");
                    $("#pending_for_approval_rkcl").css("display", "block");
                    $("#pending_for_payment").css("display", "block");
                    $("#submission_date_rkcl").html(data[0].fld_updatedOn);
                    $("#paid").css("display", "block");
                    $.ajax({
                        type: "post",
                        url: "common/cfmodifyITGK.php",
                        data: "action=getPaymentDetails&Code=" + decryptString('<?php echo $_REQUEST['Code'] ?>'),
                        success: function (data2) {
                            data2 = $.parseJSON(data2);
                            $("#payment_date").html(data2[0].fld_updatedOn);
                        }
                    });
                }
            }
        });
    }

    fillForm();
$("#updateOrgDetails").on('submit',(function(e) {
    e.preventDefault();
    var $validator = $("#updateOrgDetails").validate();
    var errors;
    var pattern = new RegExp("^[\\w.# ,-/]+$");
    var fld_remarks = $("#fld_remarks").val();

    if (!fld_remarks) {
        errors = {fld_remarks: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
        $validator.showErrors(errors);
    } else if (!pattern.test(fld_remarks)) {
        errors = {fld_remarks: "<span style=\"color:red; font-size: 12px;\">The value seems to be invalid</span>"};
        $validator.showErrors(errors);
    } else {
        $("#submitting").modal("show");
        $.ajax({ 
          url: "common/cfmodifyITGK.php",
          type: "POST",
          data:  new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          success: function(data)
            { 
                    if (data.search("Successfully Inserted") >= 0 || data.search("Successfully Updated") >= 0) {
                        setTimeout(function () {
                            $("#submitting").modal("hide");
                            $("#submitted_thanks").modal("show");
                        }, 3000);
                    }
            }   	        
      });
    }
}));

//$("#updateOrgDetailsss").submit(function () {
//    //$("#commit").on("click", function () {
//alert("hiii");
//        var $validator = $("#updateOrgDetails").validate();
//        var errors;
//        var pattern = new RegExp("^[\\w.# ,-/]+$");
//        var fld_remarks = $("#fld_remarks").val();
//
//        if (!fld_remarks) {
//            errors = {fld_remarks: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
//            $validator.showErrors(errors);
//        } else if (!pattern.test(fld_remarks)) {
//            errors = {fld_remarks: "<span style=\"color:red; font-size: 12px;\">The value seems to be invalid</span>"};
//            $validator.showErrors(errors);
//        } else {
//            $("#submitting").modal("show");
//            var forminput = $("#updateOrgDetails").serialize();
//            
//            var file_data7 = $("#uploadImage7").prop("files")[0];
//                var file_data8 = $("#uploadImage8").prop("files")[0];
//                var file_data9 = $("#uploadImage9").prop("files")[0];
//                var file_data6 = $("#uploadImage6").prop("files")[0];// Getting the properties of file from file field
//                
//                var form_data = new FormData(this);                   // Creating object of FormData class
//                
//                form_data.append("panno", file_data7)
//                form_data.append("aadharno", file_data8)
//                form_data.append("addproof", file_data9)
//                form_data.append("appform", file_data6)
//                form_data.append("forminput", forminput)
//                form_data.append("action", "commit")
//                alert(form_data);
//            $.ajax({
//                type: "post",
//                url: "common/cfmodifyITGK.php",
//                cache: false,
//                    contentType: false,
//                    processData: false,
//                data: form_data,
//                success: function (data) {
//alert(data);
////                    if (data.search("Successfully Inserted") >= 0 || data.search("Successfully Updated") >= 0) {
////                        setTimeout(function () {
////                            $("#submitting").modal("hide");
////                            $("#submitted_thanks").modal("show")
////                        }, 3000);
////                    }
//                }
//            });
//        }
//
//    });

    $("#reject").on("click", function () {
    //alert("Hi");

        var $validator = $("#updateOrgDetails").validate();
        var errors;
        var pattern = new RegExp("^[\\w.# ,-/]+$");
        var fld_remarks = $("#fld_remarks").val();

        if (!fld_remarks) {
            errors = {fld_remarks: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
            $validator.showErrors(errors);
        } else if (!pattern.test(fld_remarks)) {
            errors = {fld_remarks: "<span style=\"color:red; font-size: 12px;\">The value seems to be invalid</span>"};
            $validator.showErrors(errors);
        }else {
            $("#submitting").modal("show");
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=rejectbysp&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "&fld_remarks=" + fld_remarks + "&requestType=address" + "&Reason=" + ddlReason.value,
                success: function (data) {
                    if (data.search("Successfully Inserted") >= 0 || data.search("Successfully Updated") >= 0) {
                        setTimeout(function () {
                            $("#submitting").modal("hide");
                            $("#submitted_rejected").modal("show")
                        }, 3000);
                    }
                }
            });
        }


    });

    $("#revoke").on("click", function () {

        var $validator = $("#updateOrgDetails").validate();
        var errors;

        if (!$("#fld_remarks").val()) {
            errors = {fld_remarks: "<span style=\"color:red; font-size: 12px;\">Please enter remarks for the action you are committing</span>"};
            $validator.showErrors(errors);
        } else {
            $("#submitting").modal("show");
            $.ajax({
                type: "post",
                url: "common/cfmodifyITGK.php",
                data: "action=revoke&Code=" + decryptString('<?php echo $_REQUEST['Code']?>') + "&requestType=address",
                success: function (data) {
                    if (data.search("Sucscessfully Deleted") >= 0) {
                        setTimeout(function () {
                            $("#submitting").modal("hide");
                            $("#submitted_revoked").modal("show")
                        }, 3000);
                    }
                }
            });
        }
    });
</script>
<script type="text/javascript" src="bootcss/js/stringEncryption.js"></script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
</html>