<?php
$title = "Women Re-exam Application Payment Report";
include ('header.php');
include ('root_menu.php');

echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '7' ||
 $_SESSION['User_UserRoll'] == '14' || $_SESSION['User_UserRoll'] == '8') {	
?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Women Re-exam Application Payment Report</div>
            <div class="panel-body">

                <form name="frmwcdrexamapplicationpaymentreport" id="frmwcdrexamapplicationpaymentreport" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">

                                </select>
                            </div> 

                          <!--  <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                                                  <option value="0">All Batch</option>
                                </select>
                            </div> -->
							
							<div class="col-sm-4 form-group"> 
                            <label for="sdate">Start Date:</label>
                            <span class="star">*</span>
                            <input type="text" class="form-control" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="DD-MM-YYYY">     
                        </div>

                        <div class="col-sm-4 form-group">     
                            <label for="edate">End Date:</label>
                            <span class="star">*</span>
                            <input type="text" class="form-control" readonly="true" name="txtenddate" id="txtenddate"  placeholder="DD-MM-YYYY" value=" <?php echo date("d-m-Y"); ?>">
                        </div>

                        </div>

                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>


<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    $('#txtstartdate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script>

<script type="text/javascript">
    $('#txtenddate').datepicker({
        format: "dd-mm-yyyy",
        orientation: "bottom auto",
        todayHighlight: true,
        //autoclose: true
    });
</script>	

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
	
	$("#txtstartdate, #txtenddate").datepicker();
    $("#txtenddate").change(function () {
        var txtstartdate = document.getElementById("txtstartdate").value;
        var txtenddate = document.getElementById("txtenddate").value;
        if ((Date.parse(txtenddate) <= Date.parse(txtstartdate))) {
            alert("End date should be greater than Start date");
            document.getElementById("txtenddate").value = "";
        }
    });
	
    $(document).ready(function () {

//        
        function FillCourse() 
		{
			$.ajax({
                type: "post",
                url: "common/cfwcdrexamapplicationpaymentreport.php",
                data: "action=FILLWcdrexamapplicationCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () 
		{
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfwcdrexamapplicationpaymentreport.php",
                data: "action=FILLWcdrexamapplicationBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);

                }
            });

        });
		
		
		function showData() 
		{
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfwcdrexamapplicationpaymentreport.php"; // the script where you handle the form input.            
            var data;
			
			var startdate = $('#txtstartdate').val();
            var enddate = $('#txtenddate').val();
				
			if(ddlCourse.value==24)
			{
				data = "action=APPLICATION&course=" + ddlCourse.value + "&startdate=" + startdate + "&enddate=" + enddate + ""; //
			}
			else if(ddlCourse.value==5)
			{
				data = "action=CERTIFICATE&course=" + ddlCourse.value + "&startdate=" + startdate + "&enddate=" + enddate + ""; //
			}
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    $('#response').empty();                   
                    $("#grid").html(data);
					 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }

		
		$("#btnSubmit").click(function () {
           if ($("#frmwcdrexamapplicationpaymentreport").valid())
           { 
					showData();				
           } 

            return false; // avoid to execute the actual submit of the form.
        });
		
		
		 

        
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmwcdrexamapplicationpaymentreport_validation.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>