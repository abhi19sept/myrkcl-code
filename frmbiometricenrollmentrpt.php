<?php
$title = "Bio-Metric Enrollment Report";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code='" . $_SESSION['User_LoginId'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code='" . $_SESSION['User_LoginId'] . "'</script>";
    echo "<script>var Mode='Show'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 
    

    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading">Bio-Metric Enrollment Report</div>
        <div class="panel-body">					
            <form name="frmbiometricenrollment" id="frmbiometricenrollment" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>
                    </div>        
                    <div id="errorBox"></div>
					
					 <div class="col-sm-4 form-group"> 
                        <label for="course">Select Course:<span class="star">*</span></label>
                        <select id="ddlCourse" name="ddlCourse" class="form-control">  </select>
                    </div> 
					
					<div class="col-md-4 form-group">     
                        <label for="batch"> Select Batch:<span class="star">*</span></label>
                        <select id="ddlBatch" name="ddlBatch" class="form-control"> </select>
                    </div>
                    
					<div class="col-sm-4 form-group"> 
                        <label for="sdate">Select Status:</label>
                        <span class="star">*</span>
							<select id="ddlStatus" name="ddlStatus" class="form-control">                                  
                                <option value=""> Please Select </option>
								<option value="1"> Enrolled </option>
								<option value="2"> Not Enrolled </option>
							</select>     
                    </div>
				</div>  

                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View Report" style="margin-left:15px;"/>    
                </div>

               <div id="grid" style="margin-top:15px;"> </div>      
        </div>
    </div>   
</div>
</form>

</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

	$(document).ready(function () {
		
		function FillCourse() {            
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLAdmissionSummaryCourse",
                success: function (data) {                    
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();
		
		$("#ddlCourse").change(function () {
            var selCourse = $(this).val();            
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });
        });
       

        $("#btnSubmit").click(function () {
            if ($("#frmbiometricenrollment").valid()) {
                $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
				setTimeout(function(){$('#grid').load();}, 2000); 
				
				var url = "common/cfBioMetricEnrollmentRpt.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Show')
                {
                    data = "action=SHOW&status=" + ddlStatus.value + "&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; // serializes the form's elements.				 
                }
                else
                {
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                            $("#grid").html(data);
                            $('#example').DataTable({
							dom: 'Bfrtip',
							buttons: [
								'copy', 'csv', 'excel', 'pdf', 'print'
							]
						});
                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>

	<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmbiometricenrolmentreport_validation.js"></script>
</html>	