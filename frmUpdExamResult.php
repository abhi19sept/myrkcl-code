<?php
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);
ini_set("upload_max_filesize", "855M");
ini_set("post_max_size", "1000M");
ini_set("max_execution_time", "30000000000");
ini_set("max_input_time", "30000000000");
$title = "Upload Final Exam Result";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '8550') {

    $filename = date("Y-m-d_H-i-s");
    echo "<script>var filea='" . $filename . "'</script>";
    ?>
    <link rel="stylesheet" href="css/datepicker.css"/>
    <script src="scripts/datepicker.js"></script>
    <div style="min-height:430px !important;max-height:1500px !important;">
        <div class="container"> 


            <div class="panel panel-primary" style="margin-top:36px !important;">  
                <div class="panel-heading">Upload Final Exam Result</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> --> 
                    <form name="frmupdexamresult" id="frmupdexamresult" class="form-inline" role="form" enctype="multipart/form-data" >    
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>

                        </div>						
                        <div class="container" id="coursemode">
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Select Exam Event:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-6 form-group"> 
                                <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">									
                                </select>
                                <input type="text" id="txtfilename" name="txtfilename" class="form-control">
                            </div> 
                        </div>


                        <div class="container" style="display:none">
                            <div class="col-sm-6 form-group"> 

                                <input type="text" id="ddlEID" name="ddlEID" class="form-control" >


                                </select>
                            </div> 
                        </div>
                        <div class="container"> 

                            <div class="col-sm-4 form-group">  
                                <label>Download Sample File:</label>
                                <a href="/upload/sample_file_final_result.csv"> <input type="button" value="Download File" name="download">  </a>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="ecl">Upload Result Data:<span class="star">*</span></label>
                                <input type="file" class="form-control" id="_file" name="_file" accept=".csv"/>
                                <span style="font-size:10px;">Note : .xlsx Allowed Max Size =50MB</span>
                                <span class="btn submit" id="fileuploadbutton"> <input type="button" id="btnUpload" value="Upload File"/></span> 
                                <span class='btn submit'> <input type='button' id='btnReset' name='btnReset' value='Reset' /> </span>
                            </div>


                        </div>




                        <div class="container" >
                        <div class="col-md-6 ">       
                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Upload To Temp Table" style="display: none;" />
                            <input type="button" name="btnTransferResult" id="btnTransferResult" class="btn btn-primary" value="Transfer to Result Table" style="display: none;"/>
                        </div>
                        <div class="col-md-6 form-group" id="transferdiv" style="display: none;"> 
                        <label for="ecl"> Result Date<span class="star">*</span></label>    
                            <input type="text" class="form-control" readonly="true" maxlength="50" name="result_date" id="result_date"  placeholder="YYYY-MM-DD" style="display: none;">
                            <input type="button" name="btnTransferFinalResult" id="btnTransferFinalResult" class="btn btn-primary" value="Transfer to Final Result" style="display: none;"/>
                        </div>
                        </div>                  
                    </form>
                </div>
            </div>

           <!-- <div class="panel panel-primary" style="margin-top:36px !important;">  
                <div class="panel-heading">Upload Eligible Learner Data</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> 
                    <form name="frmupdellist" id="frmupdellist" class="form-inline" role="form" enctype="multipart/form-data">    
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>

                        </div>						
                        <div class="container" id="coursemode">
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Select Exam Event:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-6 form-group"> 
                                <select id="ddlExamEvent1" name="ddlExamEvent1" class="form-control">									
                                </select>
                                <input type="text" id="txtfoldertype" name="txtfoldertype" class="form-control">
                            </div> 
                        </div>


                        <div class="container" style="display:none">
                            <div class="col-sm-6 form-group"> 

                                <input type="text" id="ddlEID" name="ddlEID" class="form-control" >


                                </select>
                            </div> 
                        </div>
                        <div class="container"> 

                            <div class="col-sm-4 form-group">  
                                <label>Download Sample File:</label>
                                <a href="sample_file_EL_list.csv"> <input type="button" value="Download File" name="download">  </a>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="ecl">Upload Eligible Learner Data:<span class="star">*</span></label>
                                <input type="file" class="form-control" id="_file1" name="_file1" accept=".csv"/>
                                <span style="font-size:10px;">Note : .xlsx Allowed Max Size =50MB</span>
                                <span class="btn submit" id="fileuploadbutton1"> <input type="button" id="btnUploadellist" value="Upload File"/></span> 
                                <span class='btn submit'> <input type='button' id='btnReset' name='btnReset' value='Reset' /> </span>
                            </div>


                        </div>




                        <div class="container" >      
                            <input type="submit" name="btnSubmitellist" id="btnSubmitellist" class="btn btn-primary" value="Submit" style="display:none"/>
                        </div>                  
                    </form>
                </div>
            </div>
             <div class="panel panel-primary" style="margin-top:36px !important;">  
                <div class="panel-heading">Upload Final Exam Schedule</div>
                <div class="panel-body">
                     <div class="jumbotron">  
                    <form name="frmupdschedule" id="frmupdschedule" class="form-inline" role="form" enctype="multipart/form-data">    
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>

                        </div>						
                        <div class="container" id="coursemode">
                            <div class="col-sm-6 form-group"> 
                                <label for="course">Select Exam Event:<span class="star">*</span></label>

                            </div> 
                            <div class="col-sm-6 form-group"> 
                                <select id="ddlExamEvent2" name="ddlExamEvent2" class="form-control">									
                                </select>
                                <!--<input type="text" id="txtfoldertype" name="txtfoldertype" class="form-control">
                            </div> 
                        </div>


                        <div class="container" style="display:none">
                            <div class="col-sm-6 form-group"> 

                                <input type="text" id="ddlEID" name="ddlEID" class="form-control" >


                                </select>
                            </div> 
                        </div>
                        <div class="container"> 

                            <div class="col-sm-4 form-group">  
                                <label>Download Sample File:</label>
                                <a href="sample_file_Exam_schedule.csv"> <input type="button" value="Download File" name="download">  </a>
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="ecl">Upload Final Exam Schedule:<span class="star">*</span></label>
                                <input type="file" class="form-control" id="_file2" name="_file2" accept=".csv"/>
                                <span style="font-size:10px;">Note : .xlsx Allowed Max Size =50MB</span>
                                <span class="btn submit" id="fileuploadbutton2"> <input type="button" id="btnUploadeschedule" value="Upload File"/></span> 
                                <span class='btn submit'> <input type='button' id='btnReset' name='btnReset' value='Reset' /> </span>
                            </div>


                        </div>




                      <!--   <div class="container" >      
                            <input type="submit" name="btnSubmiteschedule" id="btnSubmiteschedule" class="btn btn-primary" value="Submit" style="display:none"/>
                        </div>                   
                    </form>
                </div>
            </div> -->
        </div>
    </div>
    </body>

    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>

                                                        <!--<script src="scripts/eoitnc.js"></script>-->
<script type="text/javascript">
                            $('#result_date').datepicker({
                                format: "yyyy-mm-dd",
                                orientation: "bottom auto",
                                todayHighlight: true
                            });
</script>

    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

        $(document).ready(function () {
            //alert("hii");
                var uploadfilepath = "";
                var updeventid = "";
            function FillEvent() {
                //alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfEventMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        //alert(data);
                        $("#ddlExamEvent").html(data);
                        $("#ddlExamEvent1").html(data);
                        $("#ddlExamEvent2").html(data);
                        ddlEID.value = ddlExamEvent.value;
                    }
                });
            }
            FillEvent();

            $("#ddlExamEvent").change(function () {
                var eidd = $(this).val();
                var conta = eidd.concat("_");
                var contb = conta.concat(filea);
                $('#txtfilename').val(contb);
                $('#txtfoldertype').val("eresult");
                // $.getScript("scripts/fileuploadfinalresult.js");
                $('#btnTransferFinalResult').hide();
                $('#result_date').hide();
                $('#transferdiv').hide();

            });
            $("#ddlExamEvent1").change(function () {
                var eidd = $(this).val();
                var conta = eidd.concat("_");
                var contb = conta.concat(filea);
                $('#txtfilename').val(contb);
                $('#txtfoldertype').val("ellist");
                $.getScript("scripts/fileuploadellist.js");

            });
            $("#ddlExamEvent2").change(function () {
                var eidd = $(this).val();
                var conta = eidd.concat("_");
                var contb = conta.concat(filea);
                $('#txtfilename').val(contb);
                $('#txtfoldertype').val("eschedule");
                $.getScript("scripts/fileuploadeschedule.js");

            });
            $("#btnUpload").click(function () {
                
                var url = "common/cfUpdExamResult.php";
                //var forminput = $("#frmupdexamresult").serialize();


var fd = new FormData();    
fd.append( 'resultfile', $("#_file").prop("files")[0] );
fd.append('action','updfile');
fd.append('ddlExamEvent', ddlExamEvent.value);
fd.append('txtfilename', txtfilename.value);
                $.ajax({
                    url: url,
                     data: fd,
                     processData: false,
                     contentType: false,
                     type: 'POST',

                    success: function (data)
                    {

                        //var obj = JSON.parse(data);
                        obj = $.parseJSON(data);
                      
                        if (obj['resultfile']['status'] == "1") {
                           
                            uploadfilepath = obj['resultfile']['filename'];
                            alert(uploadfilepath);
                            updeventid = obj['resultfile']['eventname'];
                            $('#btnSubmit').show(2000);
                        }

                    }
                });


            });
            $("#btnUploadellist").click(function () {

                $('#btnSubmitellist').show(5000);
            });
            $("#btnUploadeschedule").click(function () {

                $('#btnSubmiteschedule').show(5000);
            });

            $("#btnSubmit").click(function () {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfUpdExamResult.php"; // the script where you handle the form input.
                var data;
                data = "action=uploadexamresult&filepath=" + uploadfilepath + "&eventname=" + updeventid + ""; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {var obj = JSON.parse(data);
                        //alert(obj['rows']);
                        if (obj['status'] == 'success')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + obj['rows'] + " Records Imported from CSV</span></p>");

                                $('#btnTransferResult').show(2000);
                                 $('#btnSubmit').hide();
                           
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>error</span></p>");
                            $('#btnTransferResult').hide();
                            $('#btnTransferFinalResult').hide();
                            $('#result_date').hide();
                             $('#transferdiv').hide();
                        }
                        //showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });

            $("#btnTransferResult").click(function () {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfUpdExamResult.php"; // the script where you handle the form input.
                var data;
                data = "action=transferexamresult" + "&eventname=" + updeventid + ""; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {var obj = JSON.parse(data);
                        //alert(obj['rows']);
                        if (obj['status'] == 'success')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + obj['rows'] + "  Records Inserted in tbl_result</span></p>");
                                 $('#transferdiv').show();
                                $('#btnTransferFinalResult').show(2000);
                                 $('#result_date').show(2000);
                           
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>error</span></p>");
                             $('#transferdiv').hide();
                            $('#btnTransferFinalResult').hide();
                            $('#result_date').hide();
                        }
                        //showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            $("#btnTransferFinalResult").click(function () {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfUpdExamResult.php"; // the script where you handle the form input.
                var data;
                data = "action=transferexamresultfinal" + "&eventname=" + updeventid + "&result_date=" +result_date.value+ ""; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {var obj = JSON.parse(data);
                        //alert(obj['rows']);
                        if (obj['status'] == 'success')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + obj['rows'] + "  Records Tranfered in tbl_final_result</span></p>");
                                 
                           
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>error</span></p>");
                            
                        }
                        //showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            $("#btnSubmiteschedule").click(function () {

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfFileUploadFinalResult.php"; // the script where you handle the form input.
                var data;

                if (Mode == 'Add')
                {
                    // alert("mod");
                    data = "action=uploadexamschedule&eventid=" + txtfilename.value + ""; // serializes the form's elements.
                    //alert(data);
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        //alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");


                            Mode = "Add";
                            resetForm("frmupdschedule");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmupdexamresult.php";
                            }, 5000000);
                        }
                        //showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            $("#btnSubmitellist").click(function () {
                alert(1);

                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                var url = "common/cfFileUploadFinalResult.php"; // the script where you handle the form input.
                var data;

                if (Mode == 'Add')
                {
                    alert(txtfilename.value);
                    data = "action=uploadexamellist&eventid=" + txtfilename.value + ""; // serializes the form's elements.
                    alert(data);
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");


                            Mode = "Add";
                            resetForm("frmupdellist");
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmupdexamresult.php";
                            }, 5000000);
                        }
                        //showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });
    </script>

                                <!--    <script src="scripts/fileuploadfinalresult.js"></script>-->
    </html> 


    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>	