<?php
$title = "IT-GK Name Address Change Payment";
include ('header.php');
include ('root_menu.php');

echo "<div style='min-height:430px !important;max-height:1500px !important;'>";
echo "<div class='container'>";

$status = $_POST["status"];
$firstname = $_POST["firstname"];
$amount = $_POST["amount"];
$txnid = $_POST["txnid"];

$posted_hash = $_POST["hash"];
$key = $_POST["key"];
$productinfo = $_POST["productinfo"];
$email = $_POST["email"];
$udf1 = $_POST["udf1"];
$udf2 = $_POST["udf2"];
$udf3 = $_POST["udf3"];
$udf4 = $_POST["udf4"];
$udf5 = $_POST["udf5"];
$udf6 = $_POST["udf6"];
$salt = "8IaBELXB";

require 'DAL/classconnection.php';

$_ObjConnection = new _Connection();
$_Response = array();


If (isset($_POST["additionalCharges"])) {
    $additionalCharges = $_POST["additionalCharges"];
    $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||' . $udf6 . '|' . $udf5 . '|' . $udf4 . '|' . $udf3 . '|'.$udf2.'|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
} else {

    $retHashSeq = $salt . '|' . $status . '|||||' . $udf6 . '|' . $udf5 . '|' . $udf4 . '|' . $udf3 . '|'.$udf2.'|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
}
$hash = hash("sha512", $retHashSeq);

if ($hash != $posted_hash) {
    echo "<div class='alert alert-danger' role='alert'><b>Oh snap!</b> Payment Transaction Unsuccessful .</div>";
} else {

    echo "<br>";
    echo "<br>";
    echo "<div class='row'>";
    echo "<div class='col-md-8 col-md-offset-2'>";
    echo "<div class='panel panel-danger'>";
    echo " <div class='panel-heading'>";
    echo "    <h3 class='panel-title'>Payment Status</h3>";
    echo "  </div>";
    echo "  <div class='panel-body'>";
    echo " <table id='example' class='table table-hover table-striped'>";
    echo "  <tr class=''>";
    echo "    <td class='info' colspan='2'>";
    echo "<b>Oh snap</b> Your Payment is Failed";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Your Transaction ID is <br> You may try making the payment by clicking the link below.";
    echo "<p><a href=https://myrkcl.com/frmnameaddressfee.php> Try Again</a></p>";
    echo "</td>";
    echo "    <td class=''>";
    echo "$txnid";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Payment For";
    echo "</td>";
    echo "    <td class=''>";
    echo "$productinfo";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Center Code";
    echo "</td>";
    echo "    <td class=''>";
    echo "$udf1";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "User Name";
    echo "</td>";
    echo "    <td class=''>";
    echo "firstname";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Email";
    echo "</td>";
    echo "    <td class=''>";
    echo "$email";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Date";
    echo "</td>";
    echo "    <td class=''>";
    $date1 = date("d-m-Y");
    echo "$date1";
    echo "</td>";
    echo "  </tr>";
	echo "  <tr>";
    echo "<td colspan='2' align='center'>";
    echo "<input class='hide-from-printer' type='button' value='Print' onclick='window.print()'>";
    echo "</td>";
    echo "  </tr>";
    echo "</table>";

    echo "  </div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";


    global $_ObjConnection;
    $_ObjConnection->Connect();
    try {
        //$_User_Code =   $_SESSION['User_Code'];
		
		date_default_timezone_set('Asia/Calcutta');
		$NameAddressTimestamp = date("Y-m-d H:i:s");
		
		
        $_UpdateTransactionQuery = "Update tbl_address_name_transaction set 
								fld_Transaction_Status = '" . $status . "', fld_amount = '" . $amount . "', 
								fld_paymentTitle='" . $udf5 ."' Where
								fld_ref_no = '" . $udf3 . "' AND fld_RKCL_Trnid = '" . $udf2 . "'";
				
			$_DuplicateQuery = "Select * From tbl_address_name_transaction Where
									fld_transactionID='" . $txnid . "'";
            $_Response=$_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
            if($_Response[0]==Message::NoRecordFound)
            {
                $_Response=$_ObjConnection->ExecuteQuery($_UpdateTransactionQuery, Message::UpdateStatement);
				$_UpdatePayTranQuery = "Update tbl_payment_transaction set Pay_Tran_PG_Trnid = '" . $txnid . "', Pay_Tran_Status='PaymentFailure', Pay_Tran_Fname='" .$firstname. "' "			
								. "Where Pay_Tran_ITGK='" . $udf1 . "' AND Pay_Tran_RKCL_Trnid = '" . $udf2 . "'";								
				$_Response3=$_ObjConnection->ExecuteQuery($_UpdatePayTranQuery, Message::UpdateStatement);					
            }
    } catch (Exception $_e) {
        $_Response[0] = $_e->getTraceAsString();
        $_Response[1] = Message::Error;
    }
}
?>

</div>
</div>
<style>
    @media print {
        /* style sheet for print goes here */
        .hide-from-printer{  display:none; }
    }
</style>
<?php include ('footer.php'); ?>

</body>

</html>
