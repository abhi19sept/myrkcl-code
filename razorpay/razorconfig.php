<?php

if ($payment::PAY_MODE == 'Test') {
	if (isset($productinfo) && !empty($productinfo)) {
		if ($productinfo == 'LearnerFeePayment' || $productinfo == 'LearnerEnquiryPayment' || $productinfo == 'Learner Fee Payment') {
			$keyId = 'rzp_test_hu8LeDPnpJN3EE'; // 'rzp_live_mqNn5ETVfEUaDv';
			$keySecret = 'LTlYHJjl8LLifrrVzDnRKb5x'; // ''
		} else {
			//Key for all common (Non Scrow) live payments Of Razorpay
			$keyId = 'rzp_test_SuOCoBlPZJ3txB';
			$keySecret = '4SaAP68iiu6KMBXtEVjJ1wIQ';
		}
	}
} elseif (isset($productinfo) && !empty($productinfo)) {
	if ($productinfo == 'LearnerFeePayment' || $productinfo == 'LearnerEnquiryPayment' || $productinfo == 'Learner Fee Payment') {
		//Key for live Learner Fee Payment (Scrow Account) Of Razorpay
		$keyId = 'rzp_live_eqpXyQOgCDM2mV';
		$keySecret = 'OVN9EQ0schvcfZJzHgGDfx0O';
	} else {
		//Key for all common (Non Scrow) live payments Of Razorpay
		$keyId = 'rzp_live_sku6PYaanfLNPs';
		$keySecret = 'DvM5Pb3mSSlltxcfU6lyRSb7';
	}
}

$displayCurrency = 'INR';

//These should be commented out in production
// This is for error reporting
// Add it to config.php to report any errors
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

// Create the Razorpay Order
require_once('razorpay-php/Razorpay.php');

use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;

$api = new Api($keyId, $keySecret);

$checkout = 'manual';

$orderData = [
    'currency'        => $displayCurrency,
    'payment_capture' => 1 // auto capture
];

$theme = '#428bca';
$logo = 'images/myrkcl_logo.png';
