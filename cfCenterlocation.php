<?php

/*
 * Created by VIVEK

 */

include './commonFunction.php';
require 'BAL/clsCenterlocation.php';

$response = array();
$emp = new clsOrgUpdate();



if ($_action == "FillStates") {
    $response = $emp->FillStates();
    //echo "<option value=''>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
		if($_Row['State_Code'] == 29){
			echo "<option value='" . $_Row['State_Code'] . "' selected='selected'>" . $_Row['State_Name'] . "</option>";
		}
		/*else{
			echo "<option value='" . $_Row['State_Code'] . "' disabled='disabled'>" . $_Row['State_Name'] . "</option>";
		}*/
    }
}
if ($_action == "FillTehsils") {
    $response = $emp->FillTehsils($_actionvalue);
		
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
		
        echo "<option value='" . $_Row['Tehsil_Code'] . "'>" . $_Row['Tehsil_Name'] . "</option>";
    }
}
if ($_action == "FillDistricts") {
    $response = $emp->FillDistricts($_actionvalue);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
		
        echo "<option value='" . $_Row['District_Code'] . "'>" . $_Row['District_Name'] . "</option>";
    }
}
if ($_action == "FILLPanchayat") {
    $response = $emp->GetAllPanchayat($_actionvalue);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['Block_Code'] . ">" . $_Row['Block_Name'] . "</option>";
    }
}

if ($_action == "FILLGramPanchayat") {
    $response = $emp->GetAllGramPanchayat($_actionvalue);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value=" . $_Row['GP_Code'] . ">" . $_Row['GP_Name'] . "</option>";
    }
	//echo "<option value='0'>Others</option>";
}

if ($_action == "MapStart") {
    $response = $emp->MapStart($_REQUEST);
	
	$dom = new DOMDocument("1.0");
    $node = $dom->createElement("markers"); //Create new element node
    $parnode = $dom->appendChild($node); //make the node show up
    while ($_Row = mysqli_fetch_array($response[2])) {
        $node = $dom->createElement("marker");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("address", $_Row['Org_formatted_address']);
        $newnode->setAttribute("lat", $_Row['Organization_lat']);
        $newnode->setAttribute("lng", $_Row['Organization_long']);
        $newnode->setAttribute("name", $_Row['Organization_Name']);
    }
	echo $dom->saveXML();
}


if ($_action == "MapSave") {
	$lat = $_REQUEST['lat'];
	$long = $_REQUEST['long'];
	$address = $_REQUEST['madd'];
	$org_code = $_REQUEST['org_code'];
    $response = $emp->MapSave($lat,$long,$org_code,$address);
	echo "Google Map Location For Your ITGK is recorded successfully.";
}
if ($_action == "Learnerloc") {
	if($_REQUEST['selarea'] == "Urban"){
		$final_address = $_REQUEST['selward']." ".$_REQUEST['selTehsil']." ".$_REQUEST['selDistrict']." Rajasthan";
	}
	else{
		$final_address = $_REQUEST['samiti']." ".$_REQUEST['selTehsil']." ".$_REQUEST['selDistrict']." Rajasthan";
	}
	$final_address = implode(' ',array_unique(explode(' ', $final_address)));
	$final_address = urlencode($final_address);
	$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$final_address.'&key=AIzaSyB3LDfJa6bQsDLslWSt7MdkTD45zSyw4Ac';

	$location = file_get_contents($url);
	$location = json_decode($location);
	if($location->status == 'OK'){
		echo $lat = $location->results[0]->geometry->location->lat;
		echo "--";
		echo $long = $location->results[0]->geometry->location->lng;
		exit;
	}
	else{
		$final_address = $_REQUEST['selTehsil']." ".$_REQUEST['selDistrict']." Rajasthan";
		$final_address = implode(' ',array_unique(explode(' ', $final_address)));
		$final_address = urlencode($final_address);
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$final_address.'&key=AIzaSyB3LDfJa6bQsDLslWSt7MdkTD45zSyw4Ac';

		$location = file_get_contents($url);
		$location = json_decode($location);
		if($location->status == 'OK'){
			echo $lat = $location->results[0]->geometry->location->lat;
			echo "--";
			echo $long = $location->results[0]->geometry->location->lng;
		}
		else{
			echo "fail";
		}
	}
	
	
}

if ($_action == "sendsms") {
	$mobileno = $_REQUEST['mobileno'];
	$_SMS = $_REQUEST['sms'];
	$filledotp = $_REQUEST['filledotp'];
	
	echo $emp->Sendsms($mobileno,$_SMS,$filledotp);
}
if ($_action == "sendotp") { 
	$mobileno = $_REQUEST['mobileno'];
	//$_SMS = $_REQUEST['sms'];
	$emp->Sendotp($mobileno);
}
if ($_action == "Nearestcenter") {
	$_actionvalue = $_REQUEST['value'];
	
	$latlng = explode("--",$_actionvalue);
    $response = $emp->Nearestcenter($_actionvalue);
	$dom = new DOMDocument("1.0");
    $node = $dom->createElement("markers"); //Create new element node
    $parnode = $dom->appendChild($node); //make the node show up
    while ($_Row = mysqli_fetch_array($response[2])) {
        $node = $dom->createElement("marker");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("address", $_Row['Org_formatted_address']);
        $newnode->setAttribute("lat", $_Row['Organization_lat']);
        $newnode->setAttribute("lng", $_Row['Organization_long']);
        $newnode->setAttribute("name", $_Row['Organization_Name']);
        $newnode->setAttribute("distance", $_Row['distance']);
        $newnode->setAttribute("orgadd", $_Row['Organization_Address']);
		$sms = "IT-GK \n".$_Row['Organization_Name']."\nAddress:\n".$_Row['Organization_Address']."\n".$_Row['Org_Mobile']."\n http://maps.google.com/?q=".$_Row['Organization_lat'].",".$_Row['Organization_long'];
        $newnode->setAttribute("sms", $sms);
		// 
		// sleep(1);
    }
	echo $dom->saveXML();
	
}

if ($_action == "FillMunicipalName") {
    $response = $emp->FillMunicipalName($_actionvalue);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Municipality_Raj_Code'] . "'>" . $_Row['Municipality_Name'] . "</option>";
    }
}

if ($_action == "FILLWardno") {
    $response = $emp->FILLWardno($_actionvalue);
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Ward_Raj_Code'] . "'>" . $_Row['Ward_Name'] . "</option>";
    }
}

if ($_action == "FillMunicipalType") {
    $response = $emp->FillMunicipalType();
    echo "<option value='' selected='selected'>Select </option>";
    while ($_Row = mysqli_fetch_array($response[2])) {
        echo "<option value='" . $_Row['Municipality_Type_Code'] . "'>" . $_Row['Municipality_Type_Name'] . "</option>";
    }
}


if ($_action == "Center_locationdetail") {
    $response = $emp->Center_locationdetail();
    $_DataTable = array();
    $_i = 0;
    $co = mysqli_num_rows($response[2]);
    if ($co) {
        while ($_Row = mysqli_fetch_array($response[2])) {

            
				if(empty($_Row['Organization_lat']) ){
					
					$final_address = $_Row['Tehsil_Name']." ".$_Row['District_Name']." Rajasthan";
					$final_address = implode(' ',array_unique(explode(' ', $final_address)));
					$final_address = urlencode($final_address);
					$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$final_address.'&key=AIzaSyB3LDfJa6bQsDLslWSt7MdkTD45zSyw4Ac';

					$location = file_get_contents($url);
					$location = json_decode($location);
					if($location->status == 'OK'){
						$_Row['Organization_lat'] = $location->results[0]->geometry->location->lat;
						$_Row['Organization_long'] = $location->results[0]->geometry->location->lng;
						$_Row['Organization_Address'] = $location->results[0]->formatted_address;
						$response = $emp->MapSave($_Row['Organization_lat'],$_Row['Organization_long'],$_Row['Organization_User'],$_Row['Organization_Address']);
					}
					else{
						$_Row['Organization_lat'] = '26.960514208305806';
						$_Row['Organization_long'] = '74.76696749999996';
						$_Row['Organization_Address'] = 'Rajasthan, India';
						$response = $emp->MapSave($_Row['Organization_lat'],$_Row['Organization_long'],$_Row['Organization_User'],$_Row['Organization_Address']);
					}
				}
			$_DataTable[$_i] = array("orgname" => $_Row['Organization_Name'],
                "district" => $_Row['District_Name'],
                "tehsil" => $_Row['Tehsil_Name'],
                "districtcode" => $_Row['Organization_District'],
                "tehsilcode" => $_Row['Organization_Tehsil'],
                "lat" => $_Row['Organization_lat'],
                "lng" => $_Row['Organization_long'],
                "address" => $_Row['Organization_Address'],
                "system_address" => $_Row['Org_formatted_address']);	
            $_i = $_i + 1;
        }

        echo json_encode($_DataTable);
    } else {
        echo "";
    }
}
	
	if($_action == "Learnercenterlist"){
		$_DataTable = "";
		$response = $emp->Learnercenterlist($_REQUEST);
		// echo "<pre>";
		// print_r($response);
		// echo "</pre>";
		// die;
		
		echo "<div class='table-responsive'>";
		echo "<table id='example' border='1' cellpedding='0' cellspacing='0' class='table table-striped table-bordered' >";
		echo "<thead>";
		echo "<tr>";
		echo "<th>S No.</th>";
		echo "<th>ITGK Code </th>";
		echo "<th>ITGK Name</th>";
		
		echo "<th >District</th>";
		echo "<th >Tehsil</th>";
		echo "<th>Email </th>";
		echo "<th>Address </th>";
		echo "<th >Mobile  No</th>";
		
		
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		$_Count = 1;
		while ($_Row = mysqli_fetch_array($response[2])) {
			echo "<tr class='odd gradeX'>";
			echo "<td>" . $_Count . "</td>";
			echo "<td>" . $_Row['User_LoginId'] . "</td>";
			echo "<td>" . ucwords(strtolower($_Row['Organization_Name'])) . "</td>";
			echo "<td>" . ucwords(strtolower($_Row['District_Name'])) . "</td>";
			echo "<td>" . ucwords(strtolower($_Row['Tehsil_Name'])) . "</td>";
			echo "<td>" . $_Row['User_EmailId'] . "</td>";
			echo "<td>" . ucwords(strtolower($_Row['Organization_Address'])) . "</td>";
			echo "<td>" . $_Row['User_MobileNo'] . "</td>";
		   
			
			
			echo "</tr>";
			$_Count++;
		}
		echo "</tbody>";
		echo "</table>";
		echo "</div>";
	}

