<?php
$title="SMS Module";
include ('header.php'); 
include ('root_menu.php'); 

if (isset($_REQUEST['code'])) {
	echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
	echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
	echo "<script>var Code=0</script>";
	echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '8' || $_SESSION['User_Code'] == '14') {	
            ?>
    <link rel="stylesheet" href="css/bootstrap-multiselect.css">
    <style>
        .multiselect-container{
            max-height:300px;
            max-width:600px;
            overflow: auto;
        }
        .multiselect-container .glyphicon{
            position :absolute;
            right:4px;
            top:10px;
            z-index: 99;
        }
        #filters,#block,#filters1{
            margin-bottom: 20px;
            padding-top: 20px;
            visibility: hidden;
            box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);

        }
        .card {
            height: 100%;
            box-shadow: 0 9px 23px rgba(0, 0, 0, 0.09), 0 5px 5px rgba(0, 0, 0, 0.06) !important;
            -webkit-transition: box-shadow 0.7s cubic-bezier(0.25, 0.8, 0.25, 1) !important;
            -moz-transition: box-shadow 0.7s cubic-bezier(0.25, 0.8, 0.25, 1) !important;
            -o-transition: box-shadow 0.7s cubic-bezier(0.25, 0.8, 0.25, 1) !important;
            transition: box-shadow 0.7s cubic-bezier(0.25, 0.8, 0.25, 1) !important;
            -webkit-border-radius: 0.4167rem;
            -moz-border-radius: 0.4167rem;
            -ms-border-radius: 0.4167rem;
            -o-border-radius: 0.4167rem;
            /* border-radius: 0.4167rem; */
        }
        card .card-body {
            padding: 2.083rem;
            font-weight: 400;
        }
        .bg-gradient [class^="card-"], .bg-gradient [class^="card-"] * {
            color: #FFFFFF !important;
        }
        .bg-gradient [class^="card-"], .bg-gradient [class^="card-"] * {
            color: #FFFFFF !important;
        }
        .card.card-xs {
            height: 95px;
            overflow: hidden;
        }
        .card:not([class*=card-outline-]) {
            border: 0;
        }
        .badge, .btn, .card:not([class*=card-outline-]), .chip, .jumbotron, .modal-dialog.cascading-modal .modal-c-tabs .nav-tabs, .modal-dialog.modal-notify .modal-header, .navbar, .pagination .active .page-link, .z-depth-1 {
            box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
        }
        .card {
            position: relative;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgba(0,0,0,.125);
            border-radius: .25rem;
        }
        .card .card-body .tile-left {
            position: absolute;
        }
        .card .card-body .tile-right {
            text-align: right;
            line-height: 1.618;
        }
        .bg-gradient {
            color: #FFFFFF !important;
            background: #07a7e3;
            background: -moz-linear-gradient(-45deg, #07a7e3 0%, #32dac3 100%);
            background: -webkit-linear-gradient(-45deg, #07a7e3 0%, #32dac3 100%);
            background: linear-gradient(135deg, #07a7e3 0%, #32dac3 100%);

            -webkit-transition: opacity 0.2s ease-out;
            -moz-transition: opacity 0.2s ease-out;
            -o-transition: opacity 0.2s ease-out;
            transition: opacity 0.2s ease-out;
        }
        .bg-primary {
            color: #FFFFFF;
            background-color: #07a7e3 !important;
        }
        .p-4 {
            padding: 1.5rem!important;
        }
        .card:hover, .card:focus {
            box-shadow: 0 9px 23px rgba(0, 0, 0, 0.18), 0 5px 5px rgba(0, 0, 0, 0.12) !important;
        }
        .bg-secondary.bg-gradient {
            color: #FFFFFF !important;
            background: #4f5b60;
            background: -moz-linear-gradient(-45deg, #4f5b60 0%, #97a9b2 100%);
            background: -webkit-linear-gradient(-45deg, #4f5b60 0%, #97a9b2 100%);
            background: linear-gradient(135deg, #4f5b60 0%, #97a9b2 100%);

            -webkit-transition: opacity 0.2s ease-out;
            -moz-transition: opacity 0.2s ease-out;
            -o-transition: opacity 0.2s ease-out;
            transition: opacity 0.2s ease-out;
            -webkit-transition: width 0.3s ease-in-out;
            -moz-transition: width 0.3s ease-in-out;
            -o-transition: width 0.3s ease-in-out;
            transition: width 0.3s ease-in-out;
        }
        .nopadding {
            padding: 0 !important;
            margin: 0px;
        }
        .btn-success {
            color: #ffffff;
            background-color: #5cb85c !important;
            border-color: #4cae4c;
        }
        .tab-pane.fade{display: none}
        .tab-pane.fade.in{display: block}
        .nav-tabs { border-bottom: 2px solid #DDD; }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
        .nav-tabs > li > a { border: none; color: #666; }
        .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none; color: #4285F4 !important; background: transparent; }
        .nav-tabs > li > a::after { content: ""; background: #4285F4; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
        .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
        .tab-nav > li > a::after { background: #21527d none repeat scroll 0% 0%; color: #fff; }
        .tab-pane { padding: 15px 0; }
        .tab-content{padding:20px}
        .btn-bs-file{
            position:relative;
        }
        .btn-bs-file input[type="file"]{
            position: absolute;
            top: -9999999;
            filter: alpha(opacity=0);
            opacity: 0;

            outline: none;
            cursor: inherit;
        }
        #hint{
            position:absolute;
            z-index: 99999;
        }
        td{
            background: lavender;

        }
    </style>

<div style="min-height:500px !important;max-height:2500px !important">
        <div class="container">
            <div class="col-md-12" style="margin: 10px 0px">
            <div class="row">
            <div class="col-md-3">
                <div class="card card-tile card-xs bg-primary bg-gradient text-center">
                    <div class="card-body p-4">
                        <!-- Accepts .invisible: Makes the items. Use this only when you want to have an animation called on it later -->
                        <div class="tile-left">
                            <i class="fa fa-envelope" style="font: 40px/1 FontAwesome !important"></i>
                        </div>
                        <div class="tile-right">
                            <div class="tile-number"><p id="tech"></p></div>
                            <div class="tile-description">No Of Messages</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card card-tile card-xs bg-secondary bg-gradient  text-center">
                    <div class="card-body p-4">
                        <!-- Accepts .invisible: Makes the items. Use this only when you want to have an animation called on it later -->
                        <div class="tile-left">
                            <i class="fa fa-rupee" style="font: 40px/1 FontAwesome !important"></i>
                        </div>
                        <div class="tile-right">
                            <div class="tile-number"><p id="active"></p></div>
                            <div class="tile-description">Payment Status</div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3">
                <a href="frmlogmessage.php"> <div class="card card-tile card-xs bg-primary bg-gradient text-center">
                        <div class="card-body p-4">
                            <!-- Accepts .invisible: Makes the items. Use this only when you want to have an animation called on it later -->
                            <div class="tile-left">
                                <i class="fa fa-arrow-up" style="font: 40px/1 FontAwesome !important"></i>
                            </div>
                            <div class="tile-right">
                                <div class="tile-number"><p id="yogendra"></p></div>
                                <div class="tile-description">Messages Sent</div>
                            </div>
                        </div>
                    </div></a>
            </div>


            <div class="col-md-3">
                <div class="card card-tile card-xs bg-secondary bg-gradient text-center">
                    <div class="card-body p-4">
                        <!-- Accepts .invisible: Makes the items. Use this only when you want to have an animation called on it later -->
                        <div class="tile-left">
                            <i class="fa fa-comment" style="font: 40px/1 FontAwesome !important"></i>
                        </div>
                        <div class="tile-right">
                            <div class="tile-number"><p id="sunil"></p></div>
                            <div class="tile-description">Messages Remaining</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <a href="frmreqsms.php"><div class="card card-tile card-xs bg-primary bg-gradient text-center">
                        <div class="card-body p-4">
                            <!-- Accepts .invisible: Makes the items. Use this only when you want to have an animation called on it later -->
                            <div class="tile-left">
                                <i class="fa fa-plus" style="font: 40px/1 FontAwesome !important"></i>
                            </div>
                            <div class="tile-right">
                                <div class="tile-number"><p id="tech">Request Sms</p></div>
                                <div class="tile-description">Thanks</div>
                            </div>
                        </div>
                    </div></a>
            </div>
            </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
			<div class="panel panel-primary" style="">
				<div class="panel-heading btn-info ">RKCL Messaging System</div>
					<div class="panel-body">

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#sms1">Send Sms from RKCL</a></li>
                            <li><a data-toggle="tab" href="#sms2">Send Sms from CSV</a></li>
                            <li><a data-toggle="tab" href="#sms3">Send Personalized Sms</a></li>
                            <li><a data-toggle="tab" href="#sms4">Send Simple Sms</a></li>
                            <li><a data-toggle="tab" href="#sms5">Generate Csv</a></li>

                        </ul>

                   

                                <div id="response"></div>


							<div id="errorBox"></div>

<div id="sms1"  class="tab-pane fade in active">

                        <form name="frmadmingroupsmsmodule" id="frmadmingroupsmsmodule" class="form-inline" style="margin-top: 20px">
                            <input type="hidden" id="txttehsil"  class="form-control" value="" name="txttehsil">
                            <input type="hidden" id="txtcenters"  class="form-control" value="" name="txtcenters">
                            <input type="hidden" id="txttehsilall"  class="form-control" value="" name="txttehsilall">

                            <input type="hidden" id="txtallcenter"  class="form-control" value="" name="txtallcenter">
                            <div class="col-md-12 row">
                            <div class="col-sm-4  ">
                                <label for="txtentity">Entity Name<font color="red">*</font>
								:</label><br>

                                <select id="txtentity" name="txtentity[]" class="form-control" onchange="FillEntityRoll()" multiple><br>

                                </select>

                            </div>

                                <div class="col-sm-4">
                                    <label for="txtroll">Select Roll<font color="red">*</font>
                                        :</label><br>

                                    <select id="txtroll" name="txtroll[]" class="form-control itgk"  multiple="multiple" onchange="ShowFilters()">

                                    </select><br>

                                </div>
                            </div>

                                <div id="filters" class="col-md-12" style="display: none;margin-top: 10px">

                                </div>

                            <div id="filters1">

                            </div>

                                <div id="block" class="col-md-12" >
                                    <div  id="Urban" style="display:none">


                                        <div class="col-md-3 nopadding form-group">
                                            <label for="edistrict">Municipality Name:</label><br>
                                            <select id="ddlMunicipalName" name="ddlMunicipalName[]" class="form-control itgk" multiple="multiple" onchange="FillWards()">

                                            </select><br>
                                        </div>

                                        <div class="col-md-3 form-group">
                                            <label for="edistrict">Ward No.:</label><br>
                                            <select id="ddlWardno" name="ddlWardno[]" class="form-control itgk" multiple="multiple">

                                            </select>
                                        </div>
                                    </div>

                                    <div  id="Rural" style="display:none">
                                        <div class="col-md-2 nopadding form-group">
                                            <label for="edistrict">Panchayat Samiti/Block:</label><br>
                                            <select id="ddlPanchayat" name="ddlPanchayat[]" class="form-control itgk"  multiple="multiple" onchange="FillGramPanchayat()"  >

                                            </select><br>
                                        </div>

                                        <div class="col-md-2 form-group">
                                            <label for="edistrict">Gram Panchayat:</label><br>
                                            <select id="ddlGramPanchayat" name="ddlGramPanchayat[]" class="form-control itgk"  multiple="multiple" onchange="FillVillage()"  >

                                            </select><br>
                                        </div>

                                        <div class="col-sm-2 form-group">
                                            <label for="edistrict">Village:</label><br>
                                            <select id="ddlVillage" name="ddlVillage[]" class="form-control itgk"  multiple="multiple" >

                                            </select><br>
                                        </div>


                                    </div>
                                    <br>
                                    <button title="Fill Centers" id="showcenter2" type="button" style="display:none;margin-bottom: 20px;" class="btn btn-success" onclick="showCenters()"><i class="fa-check fa"></i></button>
                                </div>



							
							<div class="col-sm-6 "  id="non-printable">
                                <label for="ddlCenter">Center Code<font color="red">*</font>
								:</label><br>


                                <textarea class="form-control hindi" rows="6"   id="ddlCenter" name="ddlCenter" placeholder="Center codes" disabled></textarea>
								 
                                </select>    

                            </div>



							
							<div class="col-sm-4 "  id="learner" style="display:none;"> 
                                <label for="ddlCenter">Learner Code<font color="red">*</font>
								:</label><br>

                                <select id="ddllearner" name="ddllearner[]" class="form-control itgk"    multiple="multiple" onchange="updateTextarea()" >
								 
                                </select>    

                            </div>
                                <div class="col-md-5 ">

                                    <label for="txtmobile">Mobile NO:<font color="red">*</font></label><br>
                                        <textarea class="form-control hindi" rows="6"   id="txtmobile" name="txtmobile" placeholder="Mobile numbers" disabled></textarea>


                                </div>
                                <div class="col-sm-1" >
                                    <label for="ename" style='color:red;'> Count:</label>
                                    <p style="display:BLOCK;color:red;width:50px" id="txtcountadmin"  class="form-control"></p>
                                </div>

							

							<div class="col-sm-12" style="margin-top: 10px">
                                <label for="address">Message:<font color="red">*</font></label>


                                <select id="txttemplate" name="txttemplate" class="form-control txttemplate"></select>
                                <textarea class="form-control hindi"  style="margin-top:10px"  maxlength="16000" rows="4" cols="100" id="txtMessage" name="txtMessage" placeholder="Message" ></textarea>

                            </div>





                            <div id='CharCountLabel1'  class="col-sm-9" style="color:red;"></div><div id='MesCountLabel1'  class="col-sm-3 row" style="color:red;"></div>



                            <div class="col-md-12">
                                <button type="button" name="btnSubmit" id="btnSubmit" class="btn btn-success pull-right" value="Send" style="margin-top:10px">Send</button>
                            </div>


                        </form>
                    </div>
                        <div id="sms2" class="tab-pane fade">
                            <form name="frmadmingroupsmsmodule1" id="frmadmingroupsmsmodule1" class="form-inline" style="margin-top: 20px">
                            <div class="col-sm-12">
                                <label for="usertype">Send Sms To:<font color="red">*</font></label>
                                <input type="radio"  name="usertype" value="Learner">Learners</input> &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio"  name="usertype" value="Others">Others</input>

                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label for="txttemplate1">Message:<font color="red">*</font></label>

                                <select id="txttemplate1" name="txttemplate1" class="form-control txttemplate pull-right"></select>
                                <div class="col-md-5  pull-right">
                                    <span class="text-right" id="filename"></span>
                                    <label for="file"></label>
                                    <label class="btn-bs-file btn  btn-primary pull-right">
                                        Import From CSV
                                        <input name="file" type="file" required id="csvfile" accept="csv"/>
                                    </label> </div>
                                <textarea class="form-control hindi"   style="margin-top:10px;" maxlength="16000" rows="4" cols="100" id="txtMessagecsv" name="txtMessagecsv" placeholder="Message" ></textarea>
                                <div id='CharCountLabel2'  class="col-sm-9 row" style="color:red;"></div><div id='MesCountLabel2'  class="col-sm-3 row" style="color:red;"></div>

                            </div>
                            <div class="col-md-12 ">


                                <button type="button" name="btnSubmit1" id="btnSubmit1" class="btn btn-success pull-right" value="Send" style="margin-top:10px; ">Send</button>

                            </div>
                            </form>
                        </div>

                        <div id="sms3" class="tab-pane fade">
                            <form name="frmadmingroupsmsmodule2" id="frmadmingroupsmsmodule2" class="form-inline" style="margin-top: 20px">
                                <div class="col-sm-12">
                                    <label for="usertype2">Send Sms To:<font color="red">*</font></label>
                                    <input type="radio"  name="usertype2" value="Learner">Learners</input> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="radio"  name="usertype2" value="Others">Others</input>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <label for="address">Message:<font color="red">*</font></label>

                                    <select id="txttemplate2" name="txttemplate2" class="form-control txttemplate pull-right"></select>
                                    <div class="col-md-8 row pull-right">
                                        <span class="text-right" id="filename1"></span>
                                        <label class="btn-bs-file btn  btn-primary pull-right" style="margin-right: 5px;">
                                            Import From CSV
                                            <input type="file" required name="file1" id="csvfile1" />
                                        </label></div>
                                    <textarea class="form-control hindi"   style="margin-top:10px;margin-bottom:10px" maxlength="16000" rows="4" cols="100" id="txtMessagePer" name="txtMessagePer" placeholder="Message" ></textarea>
                                    <div id='CharCountLabel3'  class="col-sm-9 row" style="color:red;"></div><div id='MesCountLabel3'  class="col-sm-3 row" style="color:red;"></div>
                                </div>
                                <div class="col-md-12 ">
                                    <button type="button" name="btnSubmit2" id="btnSubmit2" class="btn btn-success pull-right" value="Send"  >Send</button>

                                </div>
                            </form>
                        </div>



                        <div id="sms4" class="tab-pane fade">
                            <form name="frmadmingroupsmsmodule3" id="frmadmingroupsmsmodule3" class="form-inline" style="margin-top: 20px">
                                <div class="col-sm-12">
                                    <div class="col-md-12 row">
                                        <label for="usertype3">Send Sms To:<font color="red">*</font></label>
                                        <input type="radio"  name="usertype3" value="Learner">Learners</input> &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="radio"  name="usertype3" value="Others">Others</input>
                                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <label for="address">Enter Number(Comma Seperated):<font color="red">*</font></label><br>
                                    <textarea class="form-control"   style="margin-bottom:10px;" rows="4" cols="100" id="txtNumber" name="txtNumber" placeholder="Comma seperated number here" ></textarea>
                                    </div>
                                    <div class="col-md-12 row ">
                                    <label for="address">Enter Message:<font color="red">*</font></label><select id="txttemplate3" name="txttemplate3" class="form-control txttemplate pull-right"></select><br>
                                    <textarea class="form-control" maxlength="16000" rows="4" cols="100" id="txtMessagesimple" name="txtMessagesimple" placeholder="Message" style="margin-top: 5px;" ></textarea>
                                        <div id='CharCountLabel4'  class="col-sm-9 row" style="color:red;"></div><div id='MesCountLabel4'  class="col-sm-3 row" style="color:red;"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 ">
                                    <button type="button" name="btnSubmit3" id="btnSubmit3" class="btn btn-success pull-right" value="Send" style="margin-top:10px;" >Send</button>

                                </div>
                            </form>
                        </div>
                        <div id="sms5" class="tab-pane fade">
                            <form name="frmadmingroupsmsmodule4" id="frmadmingroupsmsmodule4" class="form-inline" style="margin-top: 20px" >
                                <div class="col-sm-12">
                                    
                                    <div class="col-sm-3 " id="RecipientType">
                                        <label for="course">Select Recipient:<span class="star">*</span></label>
                                        <select id="ddlCategory" name="ddlCategory" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Category')" onchange="try {
                                            setCustomValidity('')
                                        } catch (e) {
                                        }">
                                            <option value="">Select</option>
                                            <option value="1">Learners</option>
                                            <option value="2">IT-GK</option>
                                            <option value="3">SP</option>
                                      
                                        </select>
                                    </div>
                                    <div class="col-sm-3 " id='ForLearner' style="display: none;">
                                        <label for="course">Select Message Type:<span class="star">*</span></label>
                                        <select id="ddlMsgCategory1" name="ddlMsgCategory1" class="form-control" required="required">
                                            <option value="0">Select Message Type</option>
                                            <option value="8">Uploaded but Unconfirmed learners</option>
                                            <option value="9">Uploaded but Confirmed learners</option>
                                             <option value="10">Receipt Uploaded but Unconfirmed learners</option>
                                            <option value="1">Biometric Enrollment Not Done</option>
                                            <option value="2">Attendance Count</option>
                                            <option value="3">Assessments Not Completed</option>
                                            <option value="4">Eligible NotEligible List Exam</option>
                                            <option value="5">Final Exam Schedule</option>
                                            <option value="6">Permission Letter Generate</option>
                                            <option value="7">Final Exam Result Declere</option>
                                           
                                           
                                        </select>
                                    </div>
                                     <div class="col-sm-3 " id='ForITGK' style="display: none;">
                                        <label for="course">Select Message Type:<span class="star">*</span></label>
                                        <select id="ddlMsgCategory2" name="ddlMsgCategory2" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Category')" onchange="try {
                                            setCustomValidity('')
                                        } catch (e) {
                                        }">
                                            <option value="0">Select Message Type</option>
                                            <option value="8">(SLA) Customized to All active/renewed ITGKs</option>

                                            <option value="9">(SLA) Non-renewed ITGKs Eligible for renewal without penalty</option>

                                            <option value="10">(SLA) Non-renewed ITGKs eligible for renewal with penalty</option>
                                             <option value="11">Intimation of Final Closing Date of Uploading and Learner Confirmation</option>
                                             <option value="12">Intimation of Zero Admission and Final Closing Date of Uploading and Learner Confirmation</option>
                                            <option value="1">Biometric Enrollment Not Done</option>
                                            <option value="2">Attendance Count</option>
                                            <option value="3">Assessments Not Completed</option>
                                            <option value="4">Eligible NotEligible List Exam</option>
                                            <option value="5">Final Exam Schedule</option>
                                            <option value="6">Permission Letter Generate</option>
                                            <option value="7">Final Exam Result Declere</option>
                                           
                                        </select>
                                    </div>
                                    <div class="col-sm-3 " id='ForSP' style="display: none;">
                                        <label for="course">Select Message Type:<span class="star">*</span></label>
                                        <select id="ddlMsgCategory3" name="ddlMsgCategory3" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Category')" onchange="try {
                                            setCustomValidity('')
                                        } catch (e) {
                                        }">
                                            <option value="0">Select Message Type</option>
                                            <option value="1">Biometric Enrollment Not Done</option>
                                            <option value="2">Attendance Count</option>
                                            <option value="3">Assessments Not Completed</option>
                                            <option value="4">Eligible NotEligible List Exam</option>
                                            <option value="5">Final Exam Schedule</option>
                                            <option value="6">Permission Letter Generate</option>
                                            <option value="7">Final Exam Result Declere</option>
                                            <option value="8">Support Tkt Pending to Resolve</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3" id="forCourse">
                                        <label for="course">Select Course:<span class="star">*</span></label>
                                        <select id="ddlCourse1" name="ddlCourse" class="form-control ddlCourseClass" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                                onchange="try {
                                                    setCustomValidity('')
                                                } catch (e) {
                                                }">  </select>
                                    </div>

                                    <div class="col-md-3" id="forBatch">
                                        <!-- <label for="batch"> Select Batch:<span class="star">*</span></label>
                                        <select id="ddlBatch1" name="ddlBatch" class="form-control ddlBatchClass" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                                onchange="try {
                                                    setCustomValidity('')
                                                } catch (e) {
                                                }"> </select> -->



                                       
                                            <label for="sdate">Select Batches:<span class="star">*</span></label>

                                       
                                            <select id="ddlBatch1" name="ddlBatch[]" multiple class="form-control ddlBatchClass" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                                onchange="try {
                                                    setCustomValidity('')
                                                } catch (e) {
                                                }">

                                            </select>
                                       
                                    </div>
                                    <div class="col-sm-3" id="ForLearnerExam" style="display: none;">
                                        <label >Select Exam Event:<span class="star">*</span></label>
                                        <select id="ddlExamEvent" name="ddlExamEvent" class="form-control ddlCourseClass" required="required" oninvalid="setCustomValidity('Please Select ExamEvent')"
                                                onchange="try {
                                                    setCustomValidity('')
                                                } catch (e) {
                                                }">  </select>
                                    </div>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="col-sm-12" style="margin-top: 10px">
                                        <label for="address">Message:<font color="red">*</font></label>


                                        <select id="txttemplate4" name="txttemplate4" class="form-control txttemplate"></select>
                                        <textarea class="form-control hindi"  style="margin-top:10px"  maxlength="16000" rows="4" cols="100" id="txtMessageper2" name="txtMessageper2" placeholder="Message" ></textarea>
                                        
                                    </div>
                                    <div id='CharCountLabel5'  class="col-sm-9 row" style="color:red;"></div><div id='MesCountLabel5'  class="col-sm-3 row" style="color:red;"></div>
                                     
                                <div class="col-md-12 row">
                                     <!-- <input type="button" name="btnDataSync" id="btnDataSync" class="btn btn-primary" style="display: none;" value="Sync Data" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Data Sycning.."/>
 -->
<button type="button" class="btn btn-primary" style="display: none;" name="btnDataSync" id="btnDataSync" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing...">Sync Data</button>


                                        <input type="button" name="btn-gencsv" id="btn-gencsv" class="btn btn-primary" value="Generate CSV" />

                                        <input type="button" name="btn-report-submit" id="btn-report-submit" class="btn btn-primary" value="Send SMS"/>
                                    </div>



                                </div>
                            </form>
                        </div>

                    </div>
</div>
</div>
</div>
</div>



    <?php include'common/message.php';?>
    <?php include ('footer.php'); ?>



</body>

<style>
#errorBox{
 color:#F00;
 }
</style>

<script language="javascript">
	$('.multiselectall').each(function(el){
		new MultipleSelect(el);
	});


    // focus on textbox

    // Enable on whole page
 //   pramukhIME.enable('typingarea');
</script>
<script type='text/javascript'>

CharacterCount = function(TextArea,FieldToCount,MesToCount){

	var myField = document.getElementById(TextArea);

	var myLabel = document.getElementById(FieldToCount); 
	var myMes = document.getElementById(MesToCount);

	if(!myField || !myLabel){return false}; // catches errors

	var MaxChars =  myField.maxLengh;

	if(!MaxChars){MaxChars =  myField.getAttribute('maxlength') ; }; 	if(!MaxChars){return false};

	var remainingChars =   MaxChars - myField.value.length

	myLabel.innerHTML = remainingChars+" Characters Remaining of Maximum "+MaxChars
    myMes.innerHTML = '<p class="pull-right">Message Count : '+ Math.ceil(myField.value.length/160)+'</p>';

}

 

setInterval(function(){
    CharacterCount('txtMessage','CharCountLabel1','MesCountLabel1');
    CharacterCount('txtMessagecsv','CharCountLabel2','MesCountLabel2');
    CharacterCount('txtMessagePer','CharCountLabel3','MesCountLabel3');
    CharacterCount('txtMessagesimple','CharCountLabel4','MesCountLabel4');
     CharacterCount('txtMessagePer2','CharCountLabel5','MesCountLabel5');

},55);

</script>
 <script type="text/javascript">
						var SuccessfullySend = "<?php echo Message::SuccessfullySend ?>";
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {


                            $('.itgk').multiselect({
                                includeSelectAllOption: true,
                                enableClickableOptGroups: true,
                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true,
                                templates: {
                                    button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                    filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                    filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                }
                            });

                            function FillTemplate(){
                                $(".txttemplate").append('<option value="">Select Template Category || Title || Name || MessageFor</option>');
                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=FILLTEMPLATE",
                                    success: function (data) {
                                        var parseData = $.parseJSON(data);
                                        $.each(parseData, function (i, v) {
                                            $(".txttemplate").append('<option value="' + v.Template_Code + '">' + v.Message_Cat_Name + ' || ' + v.Template_Title + ' || ' + v.Message_Type_Name + ' || ' + v.Message_For + '</option>');
                                        });

                                        $('.txttemplate').multiselect({

                                            buttonContainer: '<div class="btn-group pull-right" id="btnTemplate" />',
                                            enableFiltering:1,
                                            enableCaseInsensitiveFiltering:true,
                                            templates: {
                                                button: '<button type="button" class="multiselect dropdown-toggle   btn btn-success " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                            }
                                        });

                                    }
                                });
                            }
                            FillTemplate();
                            function FillMessage() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfadmingroupsmsmodule.php",
                                    data: "action=FILLMSG",
                                    success: function (data) {
                                        $("#tech").html(data);
                                    }
                                });
                            }

                            FillMessage();


                            function Fillstatus() {
                                //alert("hello");
                                $.ajax({
                                    type: "post",
                                    url: "common/cfadmingroupsmsmodule.php",
                                    data: "action=FILLSTATUS",
                                    success: function (data) {
                                        //alert(data);
                                        $("#active").html(data);

                                    }
                                });
                            }

                            Fillstatus();



                            FillCounting();


                            function FillEntityName() {

                                $.ajax(
                                    {
                                        type: "post",
                                        url: "common/cfadmingroupsmsmodule.php",
                                        data: "action=FILLENTITYNAME",
                                        success: function (data) {
                                            //alert(data);
                                            //alert(data);
                                            //data = $.parseJSON();
                                            //console.log(data);
                                            //alert(data.response);
                                            var parseData = $.parseJSON(data);
                                            $.each(parseData, function (i, v) {

                                                $("#txtentity").append($('<option>').text(v.Entity_Name).attr('value', v.Entity_Code));


                                            });
                                            $('#txtentity').multiselect({
                                                includeSelectAllOption: true,
                                                enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true,
                                                templates: {
                                                    button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                    filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                    filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                                }
                                            });


                                            $('#txtroll').multiselect({
                                                includeSelectAllOption: true,
                                               enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true,
                                                templates: {
                                                    button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                    filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                    filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                                }
                                            });


                                        }
                                    });
                            }

                            FillEntityName();


                            function FillDistrict() {
                                //alert();
                                $.ajax({
                                    type: "post",
                                    url: "common/cfDistrictMaster.php",
                                    data: "action=FILL",
                                    success: function (data) {
                                        $("#ddlDistrict").html(data);


                                    }
                                });
                            }

                            FillDistrict();


                            $("#ddlDistrict").change(function () {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                var selDistrict = $(this).val();
                                gettehsil(selDistrict);
                                document.getElementById("response").innerHTML = '';
                            });


                            $("#multiselectall").click(function () {

                                $('select#ddlCenter option').attr("selected", "selected");
                                //alert(ddlDistrict.value);
                                //alert(ddlTehsil);

                                var cen_val = "";

                                $('#ddlCenter :selected').each(function (i, sel) {

                                    if ($(sel).val() != 0) {
                                        cen_val += $(sel).val() + ",";
                                    }


                                });

                                cen_val = cen_val.substring(0, cen_val.length - 1);
                                //document.getElementById("txttehsilall").value=str_val;
                                //alert(str_val);


                                $('#multiDeselectcenter').click(function () {
                                    $('#response').empty();
                                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                    $('select#ddlCenter option').removeAttr("selected");

                                    document.getElementById("txtmobile").innerHTML = '';
                                    $("#selectalltehsil").trigger("click");
                                    document.getElementById("response").innerHTML = '';
                                });




                            });

                        });
                        function resetForm(formid) {
                            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                        }
                        $("body").on('click','#btnSubmit' , function (e) {
e.preventDefault();
                            if ($("#frmadmingroupsmsmodule").valid()) {
                                $('#btnSubmit').html('<i class="fa fa-spin fa-spinner"></i>Send');
                                $('#btnSubmit').prop('disabled', true);
                                var Status = document.getElementById('active').innerHTML;
                                var Remain = document.getElementById('sunil').innerHTML;
                                var sent = document.getElementById('yogendra').innerHTML;
                                var tot = document.getElementById('tech').innerHTML;
                                var tempsel = document.getElementById('txtcountadmin').innerHTML;

                                var totcount = Remain + tempsel;

                                var totalsend = parseInt(tempsel);
                                var totalremain = parseInt(Remain);
                                //alert(totalremain);
                                //alert(totalsend);
                                if ((totalsend <= totalremain) && (Status == 'Active')) {

                                    $('#response').empty();
                                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                    var url = "common/cfadmingroupsmsmodule.php"; // the script where you handle the form input.

                                    var data;
                                    var txtmobie = $('#txtmobile').val();
                                    var centers = $('#ddlCenter').val();
                                    var rolls = txtroll.value;
                                    //var forminput=$(frmsmsmodule).serialize();
                                    if (Mode == 'Add') {
                                        data = "action=ADD&roles="+rolls+"&centers=" + encodeURIComponent(centers) + "&txtmobile=" + txtmobie + "&txtMessage=" + txtMessage.value + "";
                                    }
                                    else {

                                        data = "action=UPDATE&code=" + OrganizationCode + "&" + forminput;
                                        //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                                    }
                                    $.ajax({
                                        type: "POST",
                                        url: url,
                                        data: data,
                                        success: function (data) {
                                            $('#btnSubmit').html('Send');
                                            $('#btnSubmit').prop('disabled', false);
                                            if (data == SuccessfullyInsert || data == SuccessfullyUpdate) {
                                                $('#response').empty();
                                                $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                                window.setTimeout(function () {
                                                      window.location.href = "frmsendsmstocenter.php";
                                                }, 1000);
                                                Mode = "Add";

                                                FillCounting()
                                                   resetForm("frmadmingroupsmsmodule");
                                            }
                                            else {
                                                $('#response').empty();
                                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                            }
                                           // resetForm("frmadmingroupsmsmodule");


                                        }
                                    });


                                }
                                else {

                                    $('#response').empty();
                                    $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Sorry, You are Not having Sufficient Message Pakckage</span></p>");

                                }
                            }
                            return false; // avoid to execute the actual submit of the form.
                        });

                            function FillEntityRoll() {
                                $("#txtroll").empty()
                                ddlCenter.value = '';
                                txtmobile.value = '';
                              //  $("#txtroll").multiselect('destroy');
                                var entities = $('#txtentity').val();
                                $('#filters').css('visibility', 'hidden');
                                $('#filters').hide(500)
                                $('#block').hide(500)
                                $('#filters1').remove();
                                $.ajax(
                                    {
                                        type: "post",
                                        url: "common/cfadmingroupsmsmodule.php",
                                        data: "action=FILLENTITYROLL&txtroll=" + entities,
                                        success: function (data) {
                                            $("#txtroll").multiselect('destroy');
                                            var parseData = $.parseJSON(data);
                                            $.each(parseData, function (i, v) {

                                                $("#txtroll").append($('<option value="' + v.UserRoll_Code + '">' + v.UserRoll_Name + '</select>'));


                                            });
                                            $('#txtroll').multiselect({
                                                includeSelectAllOption: true,
                                               enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true,
                                                templates: {
                                                    button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                    filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                    filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                                }
                                            });


                                        }
                                    });
                            }
                            var others = ['7','15','22'];
                            function ShowFilters() {
                                $('#filters').css('visibility', 'hidden');
                                $('#filters').hide(500)
                                $('#block').hide(500)

                                //  $("#txtroll").empty()
                                var rolls = $('#txtroll').val();




                                if (rolls.indexOf("7") > -1 || rolls.indexOf("15") > -1 || rolls.indexOf("22") > -1) {

                                    $('#filters').css('visibility', 'visible');
                                    $('#filters').show(500)
                                    $('#block').show(500)
                                    var html;

                                    html = '<div class="col-sm-3  nopadding" id="ddlDistrict_box"> \n' +
                                        '\t\t\t\t\t\t\t\t<label for="ddlCourse">Select Course:<span class="star">*</span></label><br>\n' +
                                        '\t\t\t\t\t\t\t\t<select id="ddlCourse" name="ddlCourse[]" class="form-control itgk" multiple="multiple" >\n' +
                                        '\n' +
                                        '\t\t\t\t\t\t\t\t</select><br>\n' +
                                        '\t\t\t\t\t\t\t</div>\n' +
                                        '\t\t\t\t\t\t\t\n' +
                                        '\t\t\t\t\t\t\t<div class="col-sm-3"> \n' +
                                        '\t\t\t\t\t\t\t\t<label for="ddlState">State:<span class="star">*</span></label><br>\n' +
                                        '\t\t\t\t\t\t\t\t<select id="ddlState" name="ddlState[]" class="form-control itgk"  multiple="multiple" onchange="FillDistricts()">\n' +
                                        '\n' +
                                        '\t\t\t\t\t\t\t\t</select><br>\n' +
                                        '\t\t\t\t\t\t\t</div>\n' +
                                        '\t\t\t\t\t\t\t<div class="col-sm-3 " id="ddlDistrict_box"> \n' +
                                        '\t\t\t\t\t\t\t\t<label for="ddlDistrict">District:<span class="star">*</span></label><br>\n' +
                                        '\t\t\t\t\t\t\t\t<select id="ddlDistrict" name="ddlDistrict[]" class="form-control itgk"  multiple="multiple" onchange="FillRSP()"><br>\n' +
                                        '\n' +
                                        '\t\t\t\t\t\t\t\t</select><br>\n' +
                                        '\t\t\t\t\t\t\t</div>\n' +
                                        '\n' +
                                        '\t\t\t\t\t\t\t<div class="col-sm-3 " id="ddlDistrict_box"> \n' +
                                        '\t\t\t\t\t\t\t\t<label for="ddlRSP">RSP:</label><br>\n' +
                                        '\t\t\t\t\t\t\t\t<select id="ddlRSP" name="ddlRSP[]" class="form-control itgk"  multiple="multiple">\n' +
                                        '\n' +
                                        '\t\t\t\t\t\t\t\t</select><br>\n' +
                                        '\t\t\t\t\t\t\t</div>\n' +
                                        '\n' +
                                        '\t\t\t\t\t\t\t' +
                                        '<div class="col-sm-3 nopadding" style="margin:10px 0px" id="ddlTehsil_box"> \n' +
                                        '\t\t\t\t\t\t\t\t<label for="ddlTehsil">Tehsil:</label><br>\n' +
                                        '\t\t\t\t\t\t\t\t<select id="ddlTehsil" name="ddlTehsil[]" class="form-control itgk" multiple="multiple" >\n' +
                                        '\n' +
                                        '\t\t\t\t\t\t\t\t</select><br>\n' +
                                        '\t\t\t\t\t\t\t</div><div class="col-sm-3" id="ddlArea_box" style="display2:none; margin:10px 0px"> \n' +
                                        '\t\t\t\t\t\t\t\t<label for="ddlArea">Area Type:</label><br>\n' +
                                        '\t\t\t\t\t\t\t\t<select id="ddlArea" name="ddlArea[]" class="form-control  pull-right itgk" onchange="ShowBlock()" multiple="multiple">' +
                                        '<option value="Rural">Rural</option><option value="Urban">Urban</option>\n' +
                                        '\n' +
                                        '\t\t\t\t\t\t\t\t</select><br>\n' +
                                        '\t\t\t\t\t\t\t </div> <div class="col-sm-3"  style="margin:10px 0px">\n' +
                                        '                                <label for="edistrict">Renewed/Non Renewed:</label><br>\n' +
                                        '                                <select id="ddlRenew" name="ddlRenew[]" class="form-control itgk"  multiple="multiple">\n' +
                                        '<option value="E">Eligible for Renewal</option>\n' +
                                 //ddlRenew[]       '                                    <option value="EP">Eligible for Renewal with panelty</option>\n' +

                                        '                                    <option value="N">Non Renewed</option>\n' +
                                        '<option value="NE">Not Eligible for Renewal</option>\n' + '   <option value="R">Renewed</option>\n' +
                                        '                                    <option value="S">SLA uploaded but Not Renewed</option>\n' +

                                        '                                </select><br>\n' +
                                        '                            </div><div class="col-sm-3"><br><button id="showcenter1" style="margin-top:10px" title="Fill Centers" class="btn btn-info" type="button" onclick="showCenters()"><i class="fa fa-check"></i></button></div> '


                                    $('#filters').html(html);
                                    $('.itgk').multiselect({
                                        includeSelectAllOption: true,enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                            button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                            filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                            filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                        }
                                    });


                                    $.ajax({
                                        url: 'common/cfadmingroupsmsmodule.php',
                                        type: "post",
                                        data: "action=FILLCourse",
                                        success: function (data) {
                                            var parseData = $.parseJSON(data);
                                            $.each(parseData, function (i, v) {

                                                $("#ddlCourse").append($('<option value="' + v.Course_Code + '">' + v.Course_Name + '</select>'));
                                            });
                                            $("#ddlCourse").multiselect('destroy');
                                            $('#ddlCourse').multiselect({
                                                includeSelectAllOption: true,
                                               enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true,
                                                templates: {
                                                    button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                    filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                    filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                                }
                                            });
                                            FillStates();

                                        }
                                    });
                                }

                                if (rolls.indexOf("13") > -1 ) {

                                    $('#filters1').remove();

                                    var html1 = '<div id="filters1" class="col-md-12" style="visibility:visible;display:block;margin-top:10px"><div class="col-md-2 col-lg-2 col-xl-2  nopadding form-group" id="ddlDistrict_box"> \n' +
                                        '\t\t\t\t\t\t\t\t<label for="ddlDistrict1">Select District:</label><br>\n' +
                                        '\t\t\t\t\t\t\t\t<select id="ddlDistrict1" name="ddlDistrict1[]" class="form-control itgk" multiple="multiple" onchange="FillRSPBYDISTRICT()" >\n' +
                                        '\n' +
                                        '\t\t\t\t\t\t\t\t</select><br>\n' +
                                        '\t\t\t\t\t\t\t</div>\n' +

                                        '\t\t\t\t\t\t\t<div class="col-md-2 col-lg-2 col-xl-2 form-group" id="ddlDistrict_box"> \n' +
                                        '\t\t\t\t\t\t\t\t<label for="ddlRSP1">RSP:</label><br>\n' +
                                        '\t\t\t\t\t\t\t\t<select id="ddlRSP1" name="ddlRSP1[]" class="form-control itgk"  multiple="multiple" onchange="FillCentersByRsp()" >\n' +
                                        '\n' +
                                        '\t\t\t\t\t\t\t\t</select><br>\n' +
                                        '\t\t\t\t\t\t\t</div>\n' +

                                        '\t\t\t\t\t\t\t </div></div>'


                                    $('#filters').before(html1);
                                    $('.itgk').multiselect({
                                        includeSelectAllOption: true,enableClickableOptGroups: true,
                                        numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                            button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                            filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                            filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                        }
                                    });

                                    $("#ddlDistrict1").empty()

                                    $.ajax({
                                        url: 'common/cfadmingroupsmsmodule.php',
                                        type: "post",
                                        data: "action=FillDistrictsBYRSP&values=29",
                                        success: function (data) {
                                            //alert(data);
                                            var parseData = $.parseJSON(data);
                                            $.each(parseData, function (i, v) {

                                                $("#ddlDistrict1").append($('<option value="' + v.District_Code + '" selected>' + v.District_Name + '</select>'));
                                            });
                                            $("#ddlDistrict1").multiselect('destroy');
                                            $('#ddlDistrict1').multiselect({
                                                includeSelectAllOption: true,enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                                    button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                    filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                    filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                                }
                                            });
                                            FillRSPBYDISTRICT();

                                        }
                                    });

                                }
                                var arr = rolls.filter(function(item){
                                    return others.indexOf(item) === -1;
                                });
                                var findOne = rolls.some(r=> others.indexOf(r) >= 0)


                                if(arr.length > 0 ) {

                                    txtmobile.value = '';
                                    ddlCenter.value = '';
                                    if (!findOne) {
                                        var roll = $("#txtroll").val();
                                        $.ajax({
                                            url: 'common/cfadmingroupsmsmodule.php',
                                            type: "post",
                                            data: "action=FillCentersBYRKCL&values=" + roll,
                                            success: function (data) {
                                                //alert(data);
                                                var parseData = $.parseJSON(data);
                                                $.each(parseData, function (i, v) {

                                                    txtmobile.value = (txtmobile.value === '') ? v.User_MobileNo : txtmobile.value + ', ' + v.User_MobileNo;
                                                    ddlCenter.value = ( ddlCenter.value==='')?  v.User_LoginId +' | '+ v.User_UserRoll+ ' | (' + v.User_MobileNo + ')':ddlCenter.value + '\n'+ v.User_LoginId + ' | '+ v.User_UserRoll+' | (' + v.User_MobileNo + ')';

                                                });
                                                document.getElementById("txtcountadmin").innerHTML = txtmobile.value.split(',').length;


                                            }
                                        });
                                    }
                                }

                            }
                        Array.prototype.diff = function(a) {
                            return this.filter(function(i) {return a.indexOf(i) < 0;});
                        };
                            function FillCentersByRsp(){
                               /* var rolls = $('#txtroll').val();
                                if(rolls.length==1 && rolls.indexOf("13") > -1) {
                                    txtmobile.value = '';
                                    ddlCenter.value = '';
                                    var values = $("#ddlRSP1").val();
                                    $.ajax({
                                        url: 'common/cfadmingroupsmsmodule.php',
                                        type: "post",
                                        data: "action=FillCentersBYRSP&values=" + values,
                                        success: function (data) {
                                            //alert(data);
                                            var parseData = $.parseJSON(data);
                                            $.each(parseData, function (i, v) {
                                                txtmobile.value = (txtmobile.value === '') ? v.User_MobileNo : txtmobile.value + ', ' + v.User_MobileNo;
                                                ddlCenter.value = (ddlCenter.value === '') ? v.Rsptarget_Code + ' | (' + v.Organization_Name + ')(' + v.Rsptarget_contactperson + ')' : ddlCenter.value + '\n' + v.Rsptarget_Code + ' | (' + v.Organization_Name + ')(' + v.Rsptarget_contactperson + ')';

                                            });
                                            document.getElementById("txtcountadmin").innerHTML = txtmobile.value.split(',').length;


                                        }
                                    });
                                }*/
                               showCenters()
                            }

                            function FillStates() {
                                $("#ddlState").empty()
                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=FillStates",
                                    success: function (data) {
                                        //alert(data);
                                        var parseData = $.parseJSON(data);
                                        $.each(parseData, function (i, v) {
                                            var selected = (v.State_Code == 29) ? 'selected' : '';
                                            $("#ddlState").append('<option value="' + v.State_Code + '" selected>' + v.State_Name + '</option>');
                                            if (v.State_Code == 29) {
                                                FillDistricts();
                                            }
                                        });
                                        $("#ddlState").multiselect('destroy');
                                        $('#ddlState').multiselect({
                                            includeSelectAllOption: true,enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                                button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                            }
                                        });

                                    }
                                });
                            }

                            function FillDistricts() {
                                $("#ddlDistrict").empty()
                                var state = $('#ddlState').val();
                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=FillDistricts&values=" + state,
                                    success: function (data) {
                                        //alert(data);
                                        var parseData = $.parseJSON(data);
                                        $.each(parseData, function (i, v) {

                                            $("#ddlDistrict").append($('<option value="' + v.District_Code + '">' + v.District_Name + '</select>'));
                                        });
                                        $("#ddlDistrict").multiselect('destroy');
                                        $('#ddlDistrict').multiselect({
                                            includeSelectAllOption: true,enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                                button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                            }
                                        });


                                    }
                                });
                            }

                            function FillRSP() {

                                $("#ddlRSP").empty();
                                var district = $('#ddlDistrict').val();

                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=FillRsp&values=" + district,
                                    success: function (data) {
                                        //alert(data);
                                        var parseData = $.parseJSON(data);
                                        $.each(parseData, function (i, v) {

                                            $("#ddlRSP").append($('<option value="' + v.User_Code + '">'+v.Rsptarget_Code+'(' + v.Organization_Name + ')(' + v.Rsptarget_contactperson + ')</option>'));
                                        });
                                        $("#ddlRSP").multiselect('destroy');
                                        $('#ddlRSP').multiselect({
                                            includeSelectAllOption: true,enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                                button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                            }
                                        });

                                        $("#ddlTehsil").empty();
                                        $.ajax({
                                            url: 'common/cfadmingroupsmsmodule.php',
                                            type: "post",
                                            data: "action=FillTehsils&values=" + district,
                                            success: function (data) {
                                                var parseData = $.parseJSON(data);
                                                $.each(parseData, function (i, v) {
                                                    $("#ddlTehsil").append($('<option value="' + v.Tehsil_Code + '">' + v.Tehsil_Name + '</option>'));
                                                });
                                                $("#ddlTehsil").multiselect('destroy');
                                                $('#ddlTehsil').multiselect({
                                                    includeSelectAllOption: true,
                                                   enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true,
                                                    templates: {
                                                        button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                        filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                        filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                                    }
                                                });
                                            }
                                        });


                                    }
                                });


                            }
                        function FillRSPBYDISTRICT() {
                            var rolls = $('#txtroll').val();
                            var findOne = rolls.some(r => others.indexOf(r) >= 0)

                            $("#ddlRSP1").empty();
                            var district = $('#ddlDistrict1').val();
                            if(rolls.length==1) {
                                txtmobile.value = '';
                                ddlCenter.value = '';
                            }
                            var findOne = rolls.some(r => others.indexOf(r) >= 0)

                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=FillRsp&values=" + district+"&roles="+rolls,
                                    success: function (data) {
                                        //alert(data);
                                        var parseData = $.parseJSON(data);
                                        $.each(parseData, function (i, v) {

                                            $("#ddlRSP1").append($('<option value="' + v.User_Code + '" selected>' + v.Rsptarget_Code + '(' + v.Organization_Name + ')(' + v.Rsptarget_contactperson + ')</option>'));
                                        });
                                       showCenters();
                                        $("#ddlRSP1").multiselect('destroy');
                                        $('#ddlRSP1').multiselect({
                                            includeSelectAllOption: true,
                                            enableClickableOptGroups: true,
                                            numberDisplayed: 0,
                                            enableFiltering: 1,
                                            enableCaseInsensitiveFiltering: true,
                                            templates: {
                                                button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                            }
                                        });
                                    }
                                });

                        }
                            function ShowBlock() {



                                var blocks = $("#ddlArea").val();
                                if(blocks){
                                    $('#block').css('visibility', 'visible');
                                    $('#block').show(500);
                                    $('#ddlGramPanchayat').val('');
                                    $('#ddlPanchayat').val('');
                                    $('#ddlVillage').val('');

                                    //Urban
                                    $('#ddlGramPanchayat').val('');
                                    $('#ddlPanchayat').val('');
                                    $('#ddlVillage').val('');

                                    $('#Urban').css('display', 'none');
                                    $('#Rural').css('display', 'none');
                                    $('#showcenter1').css('display', 'none');
                                    $('#showcenter2').css('display', 'block');
                                    if (blocks.indexOf("Urban") > -1 && blocks.indexOf("Rural") > -1) {
                                        $('#block').css('visibility', 'hidden');
                                        $('#block').hide(500);
                                        $('#showcenter1').css('display', 'block');
                                        $('#showcenter2').css('display', 'none');

                                    }
                                    else if (blocks.indexOf("Urban") > -1) {
                                        $('#Urban').css('display', 'block');
                                        $('#Rural').css('display', 'none');
                                        FillMunicipalName();

                                    }
                                    else if (blocks.indexOf("Rural") > -1) {
                                        $('#Urban').css('display', 'none');
                                        $('#Rural').css('display', 'block');
                                        FillPanchayat();
                                    }
                                }
                                else{
                                    $('#block').css('visibility', 'hidden');
                                    $('#block').hide(500);
                                    $('#showcenter1').css('display', 'block');
                                    $('#showcenter2').css('display', 'none');
                                }
                                // Rural


                            }

                            /***URBAN FIELDS ***/
                            function FillMunicipalName() {
                                $("#ddlMunicipalName").empty();
                                var district = $('#ddlDistrict').val();
                                //alert(selregion);
                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=FillMunicipalName&values=" + district + "",
                                    success: function (data) {
                                        //alert(data);
                                        var parseData = $.parseJSON(data);
                                        $.each(parseData, function (i, v) {
                                            $("#ddlMunicipalName").append($('<option value="' + v.Municipality_Raj_Code + '">' + v.Municipality_Name + '</option>'));
                                        });
                                        $("#ddlMunicipalName").multiselect('destroy');
                                        $('#ddlMunicipalName').multiselect({
                                            includeSelectAllOption: true,enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                                button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                            }
                                        });
                                    }
                                });

                            }

                            function FillWards() {
                                var selMunicipalName = $('#ddlMunicipalName').val();
                                $("#ddlWardno").html('');
                                //alert(selregion);
                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=FILLWardno&values=" + selMunicipalName + "",
                                    success: function (data) {

                                        var parseData = $.parseJSON(data);
                                        $.each(parseData, function (i, v) {
                                            $("#ddlWardno").append($('<option value="' + v.Ward_Code + '">' + v.Ward_Name + '</option>'));
                                        });
                                        $("#ddlWardno").multiselect('destroy');
                                        $('#ddlWardno').multiselect({
                                            includeSelectAllOption: true,enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                                button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                            }
                                        });

                                    }
                                });

                            }

                            /**Rural Fields**/

                            function FillPanchayat() {
                                var district = $('#ddlDistrict').val();
                                $("#ddlPanchayat").empty();
                                //alert(selDistrict);
                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=FILLPanchayat&values=" + district + "",
                                    success: function (data) {
                                        //alert(data);
                                        var parseData = $.parseJSON(data);
                                        $.each(parseData, function (i, v) {
                                            $("#ddlPanchayat").append($('<option value="' + v.Block_Code + '">' + v.Block_Name + '</option>'));
                                        });
                                        $("#ddlPanchayat").multiselect('destroy');
                                        $('#ddlPanchayat').multiselect({
                                            includeSelectAllOption: true,enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                                button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                            }
                                        });
                                    }
                                });

                            }


                            function FillGramPanchayat() {

                                var selPanchayat = $('#ddlPanchayat').val();
                                $("#ddlGramPanchayat").empty();
                                //alert(selregion);
                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                                    success: function (data) {
                                        var parseData = $.parseJSON(data);
                                        $.each(parseData, function (i, v) {
                                            $("#ddlGramPanchayat").append($('<option value="' + v.GP_Code + '">' + v.GP_Name + '</option>'));
                                        });
                                        $("#ddlGramPanchayat").multiselect('destroy');
                                        $('#ddlGramPanchayat').multiselect({
                                            includeSelectAllOption: true,enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                                button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                            }
                                        });

                                    }
                                });

                            }

                            function FillVillage() {

                                var selGramPanchayat = $('#ddlGramPanchayat').val();
                                $("#ddlVillage").empty();
                                //alert(selregion);
                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=FILLVillage&values=" + selGramPanchayat + "",
                                    success: function (data) {
                                        var parseData = $.parseJSON(data);
                                        $.each(parseData, function (i, v) {
                                            $("#ddlVillage").append($('<option value="' + v.Village_Code + '">' + v.Village_Name + '</option>'));
                                        });
                                        $("#ddlVillage").multiselect('destroy');
                                        $('#ddlVillage').multiselect({
                                            includeSelectAllOption: true,enableClickableOptGroups: true,
                                                numberDisplayed:0,enableFiltering:1,enableCaseInsensitiveFiltering:true, templates: {
                                                button: '<button type="button" class="multiselect dropdown-toggle   btn btn-info " data-toggle="dropdown"><span class="multiselect-selected-text"></span> </button>',
                                                filter: '<li class="multiselect-item filter"><div class="input-group"><input class="form-control multiselect-search" type="text"></div></li>',
                                                filterClearBtn: '<i class="glyphicon glyphicon-remove-circle"></i>',
                                            }
                                        });

                                    }
                                });

                            }


                            function showCenters() {
                               // $('selectmulti').prop('disabled', true);
                                $('#ddlCenter').val('');
                                $('#txtmobile').val('');
                                var rsp1 = $('#ddlRSP1').val();
                                $('#errorBox').html('');
                                document.getElementById("txtcountadmin").innerHTML ='';



                               // $('.selectmulti').disable();
                                var rolls = $('#txtroll').val();
                                var findOne = rolls.some(r=> others.indexOf(r) >= 0)
                                if(($('#ddlCourse').val() == null || $('#ddlState').val() == null || $('#ddlDistrict').val()== null ) && findOne ){
                                $('#errorBox').html('<span><i class="fa fa-close"></i>Please Select Required filed.</span>')
                                                                    return false;
                                                                }
                                else{
                                    $('.itgk').multiselect('disable');
                                    $('#showcenter2').html('<i class="fa fa-spin fa-spinner"></i>');
                                    $('#showcenter1').html('<i class="fa fa-spin fa-spinner"></i>');

                                    var renew = $('#ddlRenew').val();
                                   // if (rolls.indexOf("7") > -1 || rolls.indexOf("15") > -1 || rolls.indexOf("22") > -1) {
                                        var fields = new Object();
                                        var courses = $('#ddlCourse').val();
                                        fields.Organization_State = $('#ddlState').val();
                                        if ($('#ddlDistrict').val())
                                            fields.Organization_District = $('#ddlDistrict').val();

                                        if ($('#ddlTehsil').val())
                                            fields.Organization_Tehsil = $('#ddlTehsil').val();

                                        // if($('#ddlRSP').val())
                                        var rsp = $('#ddlRSP').val();

                                        if (rsp == null || rsp === false) {
                                            rsp = '';
                                        }
                                        if (renew == null || renew === false) {
                                            renew = '';
                                        }
                                        var blocks = $("#ddlArea").val();

                                        fields.Organization_Municipal = '';
                                        fields.Organization_WardNo = '';
                                        fields.Organization_Panchayat = '';
                                        fields.Organization_Gram = '';
                                        fields.Organization_Village = '';
                                        if (blocks) {
                                            if (blocks.indexOf("Urban") > -1 && blocks.indexOf("Rural") > -1) {
                                                /*$('#ddlMunicipalName').val()
                                                fields.Organization_Municipal = $('#ddlMunicipalName').val();

                                                $('#ddlWardno').val()
                                                fields.Organization_WardNo = $('#ddlWardno').val();

                                                $('#ddlPanchayat').val()
                                                fields.Organization_Panchayat = $('#ddlPanchayat').val();

                                                $('#ddlGramPanchayat').val()
                                                fields.Organization_Gram = $('#ddlGramPanchayat').val();

                                                $('#ddlVillage').val()
                                                fields.Organization_Village = $('#ddlVillage').val();*/

                                            }
                                            else if (blocks.indexOf("Urban") > -1) {

                                                $('#ddlMunicipalName').val()
                                                fields.Organization_Municipal = $('#ddlMunicipalName').val();

                                                $('#ddlWardno').val()
                                                fields.Organization_WardNo = $('#ddlWardno').val();

                                            }
                                            else if (blocks.indexOf("Rural") > -1) {
                                                $('#ddlPanchayat').val()
                                                fields.Organization_Panchayat = $('#ddlPanchayat').val();

                                                $('#ddlGramPanchayat').val()
                                                fields.Organization_Gram = $('#ddlGramPanchayat').val();

                                                $('#ddlVillage').val()
                                                fields.Organization_Village = $('#ddlVillage').val();
                                            }

                                        }

                                       // $("#ddlCenter").empty()
                                        $.ajax({
                                            url: 'common/cfadmingroupsmsmodule.php',
                                            type: "post",
                                            data: "action=FILLCENTER&rolls="+rolls+"&rsp1="+rsp1+"&course=" + courses + "&fields=" + JSON.stringify(fields) + "&rsp=" + rsp+'&renew='+renew,
                                            success: function (data) {

                                                $('.itgk').multiselect('enable');
                                                var parseData = $.parseJSON(data);
                                                var mob= $('#txtmobile').val();
                                                var centers= $('#ddlCenter').val();




                                                $.each(parseData, function (i, v) {

                                                    txtmobile.value = (txtmobile.value === '') ? v.User_MobileNo : txtmobile.value + ', ' + v.User_MobileNo;
                                                    if (v.Organization_Name !=='') {
                                                        ddlCenter.value = ( ddlCenter.value==='')?  v.User_LoginId + ' | '+ v.User_UserRoll+' | (' + v.Organization_Name + ')(' + v.Rsptarget_contactperson + ')':ddlCenter.value + '\n'+ v.User_LoginId +' | '+ v.User_UserRoll+ ' | (' + v.Organization_Name + ')(' + v.Rsptarget_contactperson + ')';
                                                    }
                                                    else{
                                                        ddlCenter.value = ( ddlCenter.value==='')?  v.User_LoginId +' | '+ v.User_UserRoll+ ' | (' + v.User_MobileNo + ')':ddlCenter.value + '\n'+ v.User_LoginId + ' | '+ v.User_UserRoll+' | (' + v.User_MobileNo + ')';
                                                    }





                                                });

                                                document.getElementById("txtcountadmin").innerHTML = txtmobile.value.split(',').length;
                                                //alert(data);

                                                $('#showcenter2').html('<i class="fa fa-check"></i>');
                                                $('#showcenter1').html('<i class="fa fa-check"></i>');
                                                //  document.getElementById("txtmobile").innerHTML = '';
                                            }
                                        });
                                    }
                               // }

                            }


                        $('body').on('change','#txttemplate',function(){

                            var val = $('#txttemplate').val()
                            $.ajax({
                                url: 'common/cfadmingroupsmsmodule.php',
                                type: "post",
                                data: "action=GETTEMPLATE&temp_id="+val,
                                success: function (data) {
                                    txtMessage.value=data;

                                }
                            });
                        })

                        $('body').on('change','#txttemplate1',function(){

                            var val = $('#txttemplate1').val()
                            $.ajax({
                                url: 'common/cfadmingroupsmsmodule.php',
                                type: "post",
                                data: "action=GETTEMPLATE&temp_id="+val,
                                success: function (data) {
                                    txtMessagecsv.value=data;

                                }
                            });
                        })

                        $('body').on('change','#txttemplate3',function(){

                            var val = $('#txttemplate3').val()
                            $.ajax({
                                url: 'common/cfadmingroupsmsmodule.php',
                                type: "post",
                                data: "action=GETTEMPLATE&temp_id="+val,
                                success: function (data) {
                                    txtMessagesimple.value=data;

                                }
                            });
                        })
                           $('body').on('change','#txttemplate4',function(){

                            var val = $('#txttemplate4').val()
                            $.ajax({
                                url: 'common/cfadmingroupsmsmodule.php',
                                type: "post",
                                data: "action=GETTEMPLATE&temp_id="+val,
                                success: function (data) {
                                    txtMessageper2.value=data;

                                }
                            });
                        })

                        $('body').on('change','#txttemplate2',function(){

                            var val = $('#txttemplate2').val()
                            $.ajax({
                                url: 'common/cfadmingroupsmsmodule.php',
                                type: "post",
                                data: "action=GETTEMPLATE&temp_id="+val,
                                success: function (data) {
                                    txtMessagePer.value=data;

                                }
                            });
                        })



                        $('body').on('change','#csvfile',function(){

                            var file = $('#csvfile')[0].files[0]


                            $('#filename').html(file.name)
                        })
     $('#btnSubmit1').on('click',function(){
         if ($("#frmadmingroupsmsmodule1").valid()) {
             $('#btnSubmit1').html('<i class="fa fa-spin fa-spinner"></i>Submitting SMS Log');
             $('#btnSubmit1').prop('disabled', true);
             // $('#filename').html($(this).val());
             var file = $('#csvfile')[0].files[0]

             var msg = $('#txtMessagecsv').val();

             var usertype = $('input[name=usertype]:checked').val();
             var fd = new FormData();
             fd.append('file', file);
             fd.append('action', 'SELECTCSV');
             fd.append('message', msg);
             fd.append('usertype', usertype);
             $.ajax({
                 url: 'common/cfadmingroupsmsmodule.php',
                 type: "post",
                 data: fd,
                 processData: false,
                 contentType: false,

                 success: function (data, status, jqxhr) {
                     var parseData = $.parseJSON(data);
                     if (parseData.data == SuccessfullyUpdate || parseData.data.search(SuccessfullyUpdate))
                     {
                         $('#btnSubmit1').html('<i class="fa fa-spin fa-spinner"></i>Sending SMS');
                         $.ajax({
                             url: 'common/cfadmingroupsmsmodule.php',
                             type: "post",
                             data: "action=SUBMITCSV&fileName="+parseData.file,

                             success: function (data, status, jqxhr) {
                                 if (data == SuccessfullyInsert || data.search(SuccessfullyInsert))
                                 {
                                     $('#response').empty();
                                     $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + SuccessfullyUpdate + "</span></p>");
                                     $('#btnSubmit1').html('Send');

                                     $('#btnSubmit1').prop('disabled', false);

                                     $('#filename').html('');
                                     $('#frmadmingroupsmsmodule1').trigger('reset')

                                     FillCounting()

                                 }

                             },
                             error: function (jqxhr, status, msg) {
                                 //error code
                             }
                         });



                         // resetForm('frmadmingroupsmsmodule1')

                     }
                 },
                 error: function (jqxhr, status, msg) {
                     //error code
                 }
             });
         }
     })





                        $('#btnSubmit3').on('click',function(){

                            if ($("#frmadmingroupsmsmodule3").valid()) {
                                $('#btnSubmit3').html('<i class="fa fa-spin fa-spinner"></i>Submitting SMS Log');
                                $('#btnSubmit3').prop('disabled', true);

                               var number = $('#txtNumber').val()
                               var message = $('#txtMessagesimple').val()
                               var usertype = $('input[name=usertype3]:checked').val();
                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: "action=SENDSIMPLESMSLOG&numbers="+number+"&message="+message+"&usertype="+usertype,

                                    success: function (data, status, jqxhr) {
                                        var parseData = $.parseJSON(data);
                                        if (parseData.data == SuccessfullyInsert)
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + SuccessfullyInsert + "</span></p>");

                                            $('#btnSubmit3').html('Sending Sms');
                                            $('#txtNumber').prop('disabled', true);
                                            $('#txtMessagesimple').prop('disabled', true);

                                            $.ajax({
                                                url: 'common/cfadmingroupsmsmodule.php',
                                                type: "post",
                                                data: "action=SENDSIMPLESMS&numbers="+JSON.stringify(parseData.file)+"&message="+message,

                                                success: function (data, status, jqxhr) {
                                                    if (data == SuccessfullyUpdate)
                                                    {
                                                        $('#response').empty();
                                                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + SuccessfullyUpdate + "</span></p>");

                                                        $('#btnSubmit3').html('Send');
                                                        $('#txtNumber').prop('disabled', false);
                                                        $('#txtMessagesimple').prop('disabled', false);
                                                        $('#btnSubmit3').prop('disabled', false);
                                                         $('#frmadmingroupsmsmodule3').trigger('reset')

                                                        FillCounting()
                                                    }
                                                },
                                                error: function (jqxhr, status, msg) {
                                                    //error code
                                                }
                                            });

                                           // resetForm('frmadmingroupsmsmodule3')

                                        }
                                    },
                                    error: function (jqxhr, status, msg) {
                                        //error code
                                    }
                                });
                            }
                        })



                        $('body').on('change','#csvfile1',function(){

                            var file = $('#csvfile1')[0].files[0]


                            $('#filename1').html(file.name)
                        })


                        $('#btnSubmit2').on('click',function(){
                            if ($("#frmadmingroupsmsmodule2").valid()) {

                                $('#btnSubmit2').html('<i class="fa fa-spin fa-spinner"></i>Submitting SMS Log');
                                $('#btnSubmit2').prop('disabled', true);
                                // $('#filename').html($(this).val());


                                // $('#filename').html($(this).val());
                                var file = $('#csvfile1')[0].files[0]

                                var msg = $('#txtMessagePer').val();

                                var usertype = $('input[name=usertype2]:checked').val();
                                var fd = new FormData();
                                fd.append('file', file);
                                fd.append('action', 'SELECTCSVPER');
                                fd.append('message', msg);
                                fd.append('usertype', usertype);
                                $.ajax({
                                    url: 'common/cfadmingroupsmsmodule.php',
                                    type: "post",
                                    data: fd,
                                    processData: false,
                                    contentType: false,

                                    success: function (data, status, jqxhr) {


                                        var parseData = $.parseJSON(data);
                                        if (parseData.data == SuccessfullyUpdate || parseData.data.search(SuccessfullyUpdate))
                                        {
                                            $('#btnSubmit2').html('<i class="fa fa-spin fa-spinner"></i>Sending SMS');
                                            $.ajax({
                                                url: 'common/cfadmingroupsmsmodule.php',
                                                type: "post",
                                                data: "action=SUBMITCSVPER&fileName="+parseData.file,

                                                success: function (data, status, jqxhr) {
                                                    if (data == SuccessfullyUpdate || data.search(SuccessfullyUpdate))
                                                    {
                                                        $('#response').empty();
                                                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + SuccessfullyUpdate + "</span></p>");
                                                        $('#btnSubmit2').html('Send');

                                                        $('#btnSubmit2').prop('disabled', false);

                                                        $('#filename1').html('');
                                                        FillCounting()
                                                        $('#frmadmingroupsmsmodule2').trigger('reset')

                                                    }
                                                },
                                                error: function (jqxhr, status, msg) {
                                                    //error code
                                                }
                                            });



                                        }
                                    },
                                    error: function (jqxhr, status, msg) {
                                        //error code
                                    }
                                });
                            }
                        })


                        function FillCounting() {



                            $.ajax(
                                {
                                    type: "post",
                                    url: "common/cfadmingroupsmsmodule.php",
                                    data: "action=FILL",
                                    success: function (data) {
                                        //alert(data);
                                        data = $.parseJSON(data);
                                        //console.log(data);
                                        //alert(data.response);
                                        document.getElementById("sunil").innerHTML = data.Ramain;
                                        document.getElementById("yogendra").innerHTML = data.sent;
                                        document.getElementById("tech").innerHTML = data.tot;

                                    }
                                });
                        }

                        function FillCourse() {
                            $.ajax({
                                type: "post",
                                url: "common/cfadmingroupsmsmodule.php",
                                data: "action=FILLCourse",
                                success: function (data) {
                                    
                                    $("#ddlCourse").html(data);
                                    
                                }
                            });
                        }
                        FillCourse();
                        function FillCourse1() {

                            $.ajax({
                                type: "post",
                                url: "common/cfadmingroupsmsmodule.php",
                                data: "action=FILLCourse1",
                                success: function (data) {
                                  //alert(data);
                                    $(".ddlCourseClass").html(data);
                                }
                            });
                        }
                        FillCourse1();

                        $("#ddlCategory").change(function () {

                             var selCategory = $(this).val();
                             if(selCategory == 1){
                                 $("#ForLearner").show();
                                  $("#ForITGK").hide();
                                  $("#ForSP").hide();
                             }
                             else if(selCategory == 2){
                                 $("#ForITGK").show();
                                 $("#ForSP").hide();
                                 $("#ForLearner").hide();
                             }
                             else if(selCategory == 3){
                                 $("#ForSP").show();
                                 $("#ForLearner").hide();
                                 $("#ForITGK").hide();
                             }

                           
                        });
                         $("#ddlMsgCategory1").change(function () {

                            var cattype = $(this).val();
                              if(cattype == '3'){
                                $("#btnDataSync").show();
                                $("#btn-gencsv").hide();
                                $("#btn-report-submit").hide();
                            }
                            
                            else if(cattype == '4' || cattype == '5' || cattype == '6' || cattype == '7'){
                                $("#forCourse").hide(); 
                                $("#forBatch").hide();
                                $.ajax({
                                type: "post",
                                url: "common/cfSentSMS2.php",
                                data: "action=FILLEXAMEVENT",
                                success: function (data) {
                                    $("#ddlExamEvent").html(data);
                                     $("#ForLearnerExam").show();
                                }
                            });
                                
                            }
                            else{
                                $("#btnDataSync").hide();
                                 $("#btn-gencsv").show();
                                $("#btn-report-submit").show(); 

                                 $("#forCourse").show(); 
                                $("#forBatch").show();
                                $("#ForLearnerExam").hide();
                            }
                            });
                      $("#ddlMsgCategory2").change(function () {

                            var cattype = $(this).val();
                            if(cattype == '3'){
                                $("#btnDataSync").show();
                                $("#btn-gencsv").hide();
                                $("#btn-report-submit").hide();
                            }
                            
                           else if(cattype == '8' || cattype == '9' || cattype == '10'){
                                $("#forCourse").hide(); 
                                $("#forBatch").hide();
                                 $("#ForLearnerExam").hide();
                            }
                            else if(cattype == '4' || cattype == '5' || cattype == '6' || cattype == '7'){
                                $("#forCourse").hide(); 
                                $("#forBatch").hide();
                                $.ajax({
                                type: "post",
                                url: "common/cfSentSMS2.php",
                                data: "action=FILLEXAMEVENT",
                                success: function (data) {
                                    $("#ddlExamEvent").html(data);
                                     $("#ForLearnerExam").show();
                                     
                                }
                            });
                                
                            }
                            else{
                                 $("#forCourse").show(); 
                                $("#forBatch").show();
                                $("#ForLearnerExam").hide();

                                $("#btnDataSync").hide();
                                 $("#btn-gencsv").show();
                                $("#btn-report-submit").show();
                            }
                            });
                            $("#btnDataSync").click(function () {
                                
                                 var data = $('#frmadmingroupsmsmodule4').serialize();
                              $('#response').empty();
                                $("#btnDataSync").button('loading');
                                    $.ajax({
                                        type: "post",
                                        url: "common/cfSentSMS2.php",
                                        // data: "action=sycn&type=check_insert",
                                         data: "action=sycn&type=check_insert&" + data + "",
                                        success: function (data) {
                                            if (data === 'Completed') {
                                                $('#response').empty();
                                                $("#btn-gencsv").show();
                                                $("#btn-report-submit").show();
                                                $("#btnDataSync").button('reset');

                                                $("#btnDataSync").hide();

                                            } else {
                                                $('#response').empty();
                                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>").show('slow');
                                                $("#btnDataSync").button('reset');
                                            }
                                             
                                        }
                                    });
                                      });
                        $("#ddlMsgCategory3").change(function () {

                            var cattype = $(this).val();
                            if(cattype == '4' || cattype == '5' || cattype == '6' || cattype == '7'){
                                $("#forCourse").hide(); 
                                $("#forBatch").hide();
                                $.ajax({
                                type: "post",
                                url: "common/cfSentSMS2.php",
                                data: "action=FILLEXAMEVENT",
                                success: function (data) {
                                    $("#ddlExamEvent").html(data);
                                     $("#ForLearnerExam").show();
                                }
                            });
                                
                            }
                            else{
                                 $("#forCourse").show(); 
                                $("#forBatch").show();
                                $("#ForLearnerExam").hide();
                            }
                            });
                        $("#ddlCourse").change(function () {
                            var selCourse = $(this).val();
                            $.ajax({
                                type: "post",
                                url: "common/cfSentSMS.php",
                                data: "action=FILLBatch&values=" + selCourse + "",
                                success: function (data) {
                                    $("#ddlBatch").html(data);
                                }
                            });
                        });
                function sortbatch() {

                var x = document.getElementById("ddlBatch1");
                var txt = "";
                var i;
                var tmpAry = new Array();
                for (i = 0; i < x.length; i++) {
                    tmpAry[i] = new Array();
                    tmpAry[i][0] = x.options[i].text;
                    tmpAry[i][1] = x.options[i].value;
                }
                tmpAry.sort(function (a, b) {
                    return a[1] - b[1];
                });

                tmpAry.reverse();
                while (x.options.length > 0) {
                    x.options[0] = null;
                }
                for (var i = 0; i < tmpAry.length; i++) {
                    var op = new Option(tmpAry[i][0], tmpAry[i][1]);
                    x.options[i] = op;
                }
                //alert(tmpAry);
                return;


            }
            function multisel() {
                $('#ddlBatch').multiselect({
                    nonSelectedText: 'Select Batch',
                    includeResetOption: true,
                    resetText: "Reset all",
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,

                    minHeight: 200,
                    maxHeight: '200',
                    width: '33%',
                    buttonWidth: '100%',
                    dropRight: true

                });
            }
                         $(".ddlCourseClass").change(function () {
                            var selCourse = $(this).val();
                            // $.ajax({
                            //     type: "post",
                            //     url: "common/cfadmingroupsmsmodule.php",
                            //     data: "action=FillBatch&values=" + selCourse + "",
                            //     success: function (data) {
                            //         $(".ddlBatchClass").html(data);
                            //     }
                            // });
                                            var val = selCourse;
                //alert(val);
                            if (val != 0) {

                                $.ajax({
                                    type: "post",
                                    url: "common/cfBatchMaster.php",
                                    data: "action=FILLAdmissionBatchCourse&values=" + val + "",
                                    success: function (data) {
                                        $(".ddlBatchClass").html(data);
                                        sortbatch();
                                        $(".ddlBatchClass option[value='0']").remove();
                                        multisel();

                                        $('.ddlBatchClass').multiselect('rebuild');

                                        $(".ddlBatchClass option[value='0']").remove();
                                    }
                                });
                            } else {
                                $(".ddlBatchClass").empty();
                            }
                        });
var gupshupfilename ='';
 var zipgupshupfilename = '';
                      $("#btn-gencsv").click(function () {
                          //  if ($("#frmsmsmodule").valid())
                         //   {
                                $('#response').empty();
                                $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                                var forminput = $("#frmadmingroupsmsmodule4").serialize();
                                //var usertype = $('input[name=usertype]:checked').val();
                                var data;
                                data = "action=GENCSV&" + forminput + "";
                                $.ajax({
                                    url: "common/cfSentSMS2.php",
                                    type: "POST",
                                    data: data,
                                   
                                    success: function (data)
                                    { 
                                        $('#response').empty();
                                        $('#response').append("<div class='alert-success'><span>CSV Generated</span></div>");
                                         gupshupfilename = data;
                                         //alert(gupshupfilename);
                                         converttozip(gupshupfilename);

                                    }
                                });
                         //   }
                          //  return false;
                        });
function converttozip(gupshupfilename){
            data = "action=convertzip&filetozip=" + gupshupfilename + "";
            $.ajax({
                url: "common/cfSentSMS2.php",
                type: "POST",
                data: data,
               
                success: function (data)
                { 
                    $('#response').empty();
                    $('#response').append("<div class='alert-success'><span>CSV Generated</span></div>");
                    zipgupshupfilename = data;
                    alert(zipgupshupfilename);

                }
            });
}

  $("#btn-report-submit").click(function () {
   
                var filename = zipgupshupfilename;
             //alert(filename);
//            if ($("#frmsmsmodule").valid())
//            {
            $('#response').empty();
            $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
            var forminput = $("#frmsmsmodule").serialize();
            var data;
            data = "action=sendpmsg&gupshupfilepath=" + filename + "";
            $.ajax({
                url: "common/cfSentSMS2.php",
                type: "POST",
                data: data,

                success: function (data)
                {
                    //alert(data);

                    $('#response').empty();
                    $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");

                }

            });

            return false;
});
 </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmadmingroupsmsmodule_validation.js"></script>
    <script src="js/bootstrap-multiselect.js"></script>

<style>
.error {
	color: #D95C5C!important;
}
.panel-primary > .panel-heading {
    background-color: #5bc0de !important;
}
@media screen and (min-width: 992px) {
    .col-md-3 {
       width:20%;
    }
}
</style>

</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>