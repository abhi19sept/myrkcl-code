<?php
$title = "Payment Reconcilation";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code='" . $_SESSION['User_LoginId'] . "'</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code='" . $_SESSION['User_LoginId'] . "'</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

<div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 
    

    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading">Payment Reconcilation</div>
        <div class="panel-body">					
            <form name="frmpayreconcile" id="frmpayreconcile" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>
                    </div>        
                    <div id="errorBox"></div>

                    <div class="col-sm-4 form-group"> 
                        <label for="sdate">Select Product Info:</label>
                        <span class="star">*</span>
							<select id="ddlProdInfo" name="ddlProdInfo" class="form-control">                                  
                                
							</select>     
                    </div>
					
					<div class="col-sm-4 form-group">                                  
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="show Details" style="margin-top:25px"/>    
                    </div>
				</div>  
				
				<div id="grid" style="margin-top:15px;"> </div>  

                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Reconcile Payment" style="margin-left:15px; display:none;"/>    
                </div>

                   
        </div>
    </div>   
</div>
</form>

</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

	$(document).ready(function () {
		
		function FillProdinfo() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfPaymentReconcile.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlProdInfo").html(data);
                }
            });
        }
        FillProdinfo();
       

        $("#btnShow").click(function () {
            //if ($("#frmbiometricregister").valid()) {
                $('#grid').html('<span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span>');
				setTimeout(function(){$('#grid').load();}, 2000); 
				
				var url = "common/cfPaymentReconcile.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=SHOWDATA&status=" + ddlProdInfo.value + ""; // serializes the form's elements.				 
                }
                else
                {
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
							$("#btnShow").hide();
                            $("#grid").html(data);
                            $('#example').DataTable({
							dom: 'Bfrtip',
							buttons: [
								'copy', 'csv', 'excel', 'pdf', 'print'
							]
						});
						$("#btnSubmit").show();
                    }
                });
            //}
            return false; // avoid to execute the actual submit of the form.
        });
		
		    $("#btnSubmit").click(function () {			
			//if ($("#form").valid()) {			    
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfPaymentReconcile.php"; // the script where you handle the form input.
            var data;
			 var forminput = $("#frmpayreconcile").serialize();
			 
            if (Mode == 'Add')
            {
                data = "action=Update&" + forminput; //serializes the form's elements.
            }
            else
            {
                
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   //alert(data);
				   if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmPaymentReconcile.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });
		 //}
            return false; // avoid to execute the actual submit of the form.
        });
		
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
</html>	