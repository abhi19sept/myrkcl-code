<?php
$title="Duplicate Correction Form Process";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
			echo "<script>var Cid=" . $_REQUEST['cid'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
			echo "<script>var Cid=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
 ?>

<div style="min-height:430px !important;max-height:auto !important;">
	<div class="container">			 
		<div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">Duplicate Correction Form</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data"> 
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-5 ">     
							  <label for="learnercode">Learner Code:<span class="star">*</span></label>
							  <input type="text" readonly="true" class="form-control" maxlength="50" name="txtLCode" id="txtLCode" placeholder="Learner Code">
							  <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
							</div>
							 
							 <div class="col-sm-5 ">     
							  <label for="faname">Application For:<span class="star">*</span></label>
							  <input type="text" readonly="true" class="form-control" id="txtapplication" name="txtapplication" placeholder="Application For">
						   </div>
						    
						</div>
						<div class="container" style="margin-top:20px;">
							<div class="col-sm-10 ">     
							  <label for="faname">Correction For:<span class="star">*</span></label>
							  <input type="text" readonly="true" class="form-control" id="txtCorrectionFor"
								name="txtCorrectionFor" placeholder="Correction For">
						   </div>
						</div>
						
						<div class="container" style="margin-top:20px;"> 
							<div class="col-sm-5"> 
							  <label for="learnername">Learner Name Applied For Correction:<span class="star">*</span></label>
							      <?php if($_REQUEST['Mode'] == 'Reject') { ?>
							  <input type="text" class="form-control" maxlength="50" name="txtLCorrectName" id="txtLCorrectName" placeholder="Learner Correct Name"> 
								 <?php } else {
									?>
								<input type="text" readonly="true" class="form-control" maxlength="50" name="txtLCorrectName" id="txtLCorrectName" placeholder="Learner Correct Name">  <?php } ?>
							     
							</div>
						   
						   <div class="col-sm-5">     
							  <label for="faname">Father Name Applied For Correction:<span class="star">*</span></label>
							  <?php if($_REQUEST['Mode'] == 'Reject') { ?>
							  <input type="text" class="form-control" id="txtFCorrectName" name="txtFCorrectName" placeholder="Father Correct Name">
							   <?php } else {
									?>
								 <input type="text" readonly="true" class="form-control" id="txtFCorrectName" name="txtFCorrectName" placeholder="Father Correct Name">
								  <?php } ?>
						   </div>							
                        </div> 
						
						<div class="container" style="margin-top:20px;">
							<div class="col-sm-5" > 
							  <label for="lname">Old Learner Name:<span class="star">*</span></label> </br>
							  <input type="text" readonly="true" class="form-control" name="txtoldlearnername" id="txtoldlearnername" 
								placeholder="Learner Name">    
							</div>						
							
							<div class="col-sm-5" > 
							  <label for="lname">Old Father Name:<span class="star">*</span></label> </br>
							  <input type="text" readonly="true" class="form-control" name="txtoldfaname" id="txtoldfaname"
								placeholder="Father Name">    
							</div>
						
						</div>
						
						<div class="container" style="margin-top:20px;">						
							<div class="col-sm-4 form-group"> 
							  <label for="exam_event">Exam Event Name:<span class="star">*</span></label>
							  <input type="text" readonly="true" class="form-control" name="EventName" id="EventName" placeholder="Event Name">    
						    </div>  
						   
							<div class="col-sm-4 form-group"> 
							  <label for="event_name">Event Name:</label>
								<input type="hidden" name="txtEventName" id="txtEventName" class="form-control"/> 
								<input type="hidden" name="txtTransId" id="txtTransId" class="form-control"/> 
							    <select name="ddlEventName" id="ddlEventName" class="form-control">
								
								</select>
						   </div>					                         
							
						   <div class="col-sm-4 form-group"> 
                                <label for="status">Status:<span class="star">*</span></label>
                                <select id="ddlstatus" name="ddlstatus" class="form-control" onchange="toggle_visibility1('remark')">								  
								
                                </select>    
                            </div>							

                            <div class="col-sm-6 form-group" id="apprvd"> 
                                <label for="lot">Lot:<span class="star">*</span></label>
                                <select id="ddlLot" name="ddlLot" class="form-control">
								
                                </select>								
                            </div>
							
                        </div>

						<div class="container" style="margin-top:10px;">
							<div class="col-sm-4 form-group"> 
							  <label for="t_marks">Total Marks:<span class="star">*</span></label>
							  <input type="text" class="form-control" name="txtMarks" id="txtMarks" placeholder="Total Marks">    
						   </div>   
							
							<div class="col-sm-4 form-group"> 
							  <label for="t_marks">Learner Mobile:<span class="star">*</span></label>
							  <input type="text" class="form-control" readonly="true" name="txtLearnerMobile" id="txtLearnerMobile"
									placeholder="Learner Mobile">    
						   </div>
						   
							<div class="col-sm-5" id="remark" style="display:none;"> 
                                <label for="pan">Remark:<span class="star">*</span></label>
                                <input type="text" class="form-control"  name="txtRemark" id="txtRemark"  placeholder="Remark">
                            </div>
							
							

							
						</div>
						
						
  
						<div class="container" style="margin-top:10px;">
						
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Correction Photo:<span class="star">*</span></label> </br>
								<div id="uploadPreview9"></div>
							  <img id="uploadPreview1" src="images/user icon big.png" name="filePhoto1" width="150px" height="150px">
							</div>
						<button type="button" id="showmodal" style="display:none;" class="btn btn-info btn-lg" data-toggle="modal" 			data-target="#myModal">Open Modal</button>
						
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Correction Certificate:<span class="star">*</span></label> </br>
								<button type="button" id="uploadPreview2" class="viewimage thumbnailmodal btn btn-primary"
									name="filePhoto2" width="150px" height="150px">View Correction Certificate</button>
							</div>
							
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Correction Marksheet:<span class="star">*</span></label> </br>
							  <button type="button" id="uploadPreview3" class="viewimage thumbnailmodal btn btn-primary"
									name="filePhoto2" width="150px" height="150px">View Correction Marksheet</button>
							</div>
							
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Application Form:<span class="star">*</span></label> </br>
							  <button type="button" id="uploadPreview5" class="viewimage thumbnailmodal btn btn-primary"
									name="filePhoto2" width="150px" height="150px">View Correction Application</button>
							</div>
							
							<div class="col-sm-4 form-group" id="provisional" > 
							  <label for="photo">Correction Provisional:<span class="star">*</span></label> </br>
							  <button type="button" id="uploadPreview4" class="viewimage thumbnailmodal btn btn-primary"
									name="filePhoto2" width="150px" height="150px">View Provisional Certificate</button>
							</div>
							
							<div class="col-sm-4 form-group" id="affidavit" > 
							  <label for="photo">Affidavit:<span class="star">*</span></label> </br>
							  <button type="button" id="uploadPreview6" class="viewimage thumbnailmodal btn btn-primary"
									name="filePhoto2" width="150px" height="150px">View Affidavit</button>
							</div>	
						
						
						</div>	

						<div class="container">
													  
						</div>
						
                        
					  <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                       </div>
					   
					   	 	<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
						  <div class="modal-content">
							<div class="modal-header">
								<button class="close" type="button" data-dismiss="modal">×</button>
								<h3 id="heading-tittle" class="modal-title">Heading</h3>
							</div>
							<div id="viewimagesrc"></div>
							<!--<img id="viewimagesrc" class="thumbnail img-responsive" src="images/not-found.png" name="filePhoto3" width="800px" height="880px">-->
							<div class="modal-footer">
								<button class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						   </div>
						  </div>
						</div>
					
                </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>
<style>
#errorBox{
 color:#F00;
 }
 .viewimage{    color: #ffffff;
    background: #3c763d none repeat scroll 0 0 !important;
    border-color: #125f13;}
</style>

<style>
  .modal-dialog {width:950px;}
.thumbnail {margin-bottom:6px; width:730px; height: 880px;}
  </style>
  	
    <script type="text/javascript">
  
  $(document).ready(function() {
		jQuery(".thumbnailmodal").click(function(){
      $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#showmodal').click();
});
});
  </script>
  
  <script type="text/javascript">
    function toggle_visibility1(id) {
        var e = document.getElementById(id);

        var f = document.getElementById('ddlstatus').value;
       if (f == "4")
        {
            e.style.display = 'block';
						
        }        
        else {
            e.style.display = 'none';
			
        }

    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {        
		
		function GenerateUploadId()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GENERATEID",
                    success: function (data) {                      
                        txtGenerateId.value = data;					
                    }
                });
            }
            GenerateUploadId();

        function FillStatus() {
            $.ajax({
                type: "post",
                url: "common/cfCorrectionApproved.php",
                data: "action=FILLAPPROVEDSTATUS",
                success: function (data) {
                    $("#ddlstatus").html(data);
                }
            });
        }
        FillStatus();

        //function FillLot() {
			$("#ddlstatus").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfCorrectionApproved.php",
                data: "action=GETALLLOT&status=" + ddlstatus.value + "",
                success: function (data) {
                    $("#ddlLot").html(data);
                }
            });
        });
        //FillLot();
		
		function FillExamEvent() {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FillExamEvent",
                success: function (data) {
                    $("#ddlEventName").html(data);
                }
            });
        }
        FillExamEvent();
		
		$("#ddlEventName").change(function () {
            $.ajax({
                type: "post",
                url: "common/cfcorrectionform.php",
                data: "action=FillEventById&values=" + ddlEventName.value + "",
                success: function (data) {
                    txtEventName.value = data;	
                }
            });
        });

		if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {	
                fillForm();
				fillOldLearnerDetails();
            }

			else if (Mode == 'Reject')
            {
				//alert(1);
                fillForm();
				fillOldLearnerDetails();
            }	
			
		function fillForm()
            {           //alert(Code);    
				$.ajax({
                    type: "post",
                    url: "common/cfCorrectionApproved.php",
                    data: "action=EDIT&values=" + Cid + "",
                    success: function (data) {
					data = $.parseJSON(data);					
					txtLCode.value = data[0].LearnerCode;
                        txtLCorrectName.value = data[0].lname;
                        txtFCorrectName.value = data[0].fname;
                        txtTransId.value = data[0].txnid;
                        txtapplication.value = data[0].applicationfor;
						txtCorrectionFor.value = data[0].correctionfor;
						EventName.value = data[0].exameventname;	
						txtMarks.value = data[0].marks;	
						$("#uploadPreview1").hide();
						$("#uploadPreview9").html(data[0].photo);
						
						$("#uploadPreview2").click(function () {
							$("#viewimagesrc").html(data[0].certificate);
								$("#heading-tittle").html('Correction Certificate');
							
						});
						
						//$("#uploadPreview2").attr('src',"upload/correction_certificate/" + data[0].certificate);
						$("#uploadPreview3").click(function () {
							
								
								$("#viewimagesrc").html(data[0].marksheet);
								$("#heading-tittle").html('Correction Marksheet');
							
						});
						//$("#uploadPreview3").attr('src',"upload/correction_marksheet/" + data[0].marksheet);
						
						$("#uploadPreview5").click(function () {
							
								$("#viewimagesrc").html(data[0].application);
								$("#heading-tittle").html('Application Form');
							
						});
						//$("#uploadPreview5").attr('src',"upload/correction_application/" + data[0].application);
						
						
						  var txtapp = document.getElementById("txtapplication").value;
						  if( txtapp == "Correction Certificate" ){
							  $("#provisional").hide();
							  $("#affidavit").hide();
						  }
						  else{	
								$("#uploadPreview4").click(function () {
							
								$("#viewimagesrc").html(data[0].provisional);
								$("#heading-tittle").html('Provisional Certificate');
							
						});
							 //$("#uploadPreview4").attr('src',"upload/correction_provisional/" + data[0].provisional);
							$("#uploadPreview6").click(function () {
							
								$("#viewimagesrc").html(data[0].affidavit);
								$("#heading-tittle").html('Affidavit');
							
						});
							 //$("#uploadPreview6").attr('src',"upload/correction_affidavit/" + data[0].affidavit); 
						  }
						
					}
                });
            }
			
			function fillOldLearnerDetails()
            {              
				$.ajax({
                    type: "post",
                    url: "common/cfCorrectionApproved.php",
                    data: "action=GetOldValues&values=" + Cid + "",
                    success: function (data) {						
                        data = $.parseJSON(data);
						txtoldlearnername.value = data[0].lname;
                        txtoldfaname.value = data[0].fname;                        
                        txtLearnerMobile.value = data[0].lmobile;						
					}
                });
            }		
		
        $("#btnSubmit").click(function () {	
		  if ($("#form").valid())
           {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfCorrectionApproved.php"; // the script where you handle the form input.            
            var data;			
            if (Mode == 'Add')
            {
            
			}
			else if (Mode == 'Reject'){
				 data = "action=UpdateRejectedLearner&cid=" + Cid + "&status=" + ddlstatus.value + "&lot=" + ddlLot.value + "&remark=" + txtRemark.value + "&eventname=" + txtEventName.value + "&marks=" + txtMarks.value + "&lname=" + txtLCorrectName.value + "&fname=" + txtFCorrectName.value + "&mobile=" + txtLearnerMobile.value + ""; // serializes the form's elements.
			}
            else
            {
                data = "action=UPDATE&cid=" + Cid + "&status=" + ddlstatus.value + "&lot=" + ddlLot.value + "&remark=" + txtRemark.value + "&eventname=" + txtEventName.value + "&marks=" + txtMarks.value + "&txnid=" + txtTransId.value + "&lcode=" + txtLCode.value + "&mobile=" + txtLearnerMobile.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
					var result = data.trim();
                    if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + result + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmcorrectionapproval.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + result + "</span></p>");
                    }
                    //showData();


                }
            });
			}
			return false;
		  });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcorrectionprocess_validation.js"></script>
</html>