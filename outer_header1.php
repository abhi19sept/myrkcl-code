<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<title> RKCL - Administration</title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0">		
		<meta name="description" content="Blueprint: Horizontal Drop-Down Menu" />
		<meta name="keywords" content="horizontal menu, microsoft menu, drop-down menu, mega menu, javascript, jquery, simple menu" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" href="bootcss/css/bootstrap.min.css">	
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="bootcss/js/bootstrap.min.js"></script>	
		<script src="rkcltheme/js/jquery.validate.min.js"></script>	
		<link rel="stylesheet" href="assets/header-search.css">
		<link rel="stylesheet" href="assets/footer-distributed-with-address-and-phones.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
		
		<script src="bootcss/js/jquery.dataTables.min.js"></script>	
		<script src="bootcss/js/dataTables.bootstrap.min.js"></script>			
		<link rel="stylesheet" href="bootcss/css/dataTables.bootstrap.min.css">		
            <!-- 		<link href="css/jquery.datepick.css" rel="stylesheet" type="text/css"/> -->
            <script type="text/javascript" src="scripts/validate.js"></script>
			<title><?php echo $title; ?></title>
			<link rel="stylesheet" href="css/style.css">
<!-- <script src="scripts/Menu.js" type="text/javascript"></script> -->
	<!--For all grid export and paging operations-->
<script src="bootcss/js/bootstrap-dialog.js"></script>
<script src="bootcss/js/bootstrap-dialog.min.js"></script>
<script src="bootcss/js/jquery-1.11.3.min.js"></script>
<script src="bootcss/js/jquery.dataTables.min.js"></script>
<script src="bootcss/js/dataTables.buttons.min.js"></script>
<script src="bootcss/js/buttons.flash.min.js"></script>
<script src="bootcss/js/jszip.min.js"></script>
<script src="bootcss/js/pdfmake.min.js"></script>
<script src="bootcss/js/vfs_fonts.js"></script>
<script src="bootcss/js/buttons.html5.min.js"></script>
<script src="bootcss/js/buttons.print.min.js"></script>
<link rel="stylesheet" href="bootcss/css/jquery.dataTables.min.css">	
<link rel="stylesheet" href="bootcss/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="bootcss/css/bootstrap-dialog.css">	
<link rel="stylesheet" href="bootcss/css/bootstrap-dialog.min.css">		


	</head>	
	<body style="background-image:url('images/demo1.jpg');min-height: 500px; background-repeat:repeat-y; ">   
		<header class="header-search">
			 <div class="header-limiter">       
			   <h1> <a href="#"><img src="images/rkcl-logo.jpg" style="width:175px; height:75px;"></a></h1>
			</div>		
		</header>