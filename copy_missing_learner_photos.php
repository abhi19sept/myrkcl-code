<?php
    require 'DAL/classconnection.php';
    $_ObjConnection = new _Connection();
	$_ObjConnection->Connect();

    $selectQuery = "SELECT el.eventid, el.learnercode, el.learnername, el.fathername, el.coursename, el.batchname, el.itgkcode, el.itgkname, el.examdate, '/upload/corrections/' AS basephotopath, CONCAT(el.learnercode, '_photo.png') AS photo, CONCAT(el.learnercode, '_sign.png') AS sign, CONCAT(IF (el.coursename LIKE ('%gov%'), CONCAT(REPLACE(el.batchname, ' ', '_'), '_Gov'), IF(el.coursename LIKE ('%women%'), CONCAT(REPLACE(el.batchname, ' ', '_'), '_Women'), REPLACE(el.batchname, ' ', '_'))), '/') AS dirname FROM tbl_eligiblelearners el WHERE el.eventid = 217 AND el.learnercode IN ('106160814041459789','244160814041922789','946160908054134789','400161025112457789','528161025105805789','219161025010220789','510161018102908789','889161128020341789','728161013062150789','892161025114647789','941161024105631789','814160908021458789','873161128122808789','180160729070401789','649161022022547789','353160729123448789','405160913030317789','204160913032838789','800160730062937789','235161021053412789','504161020115329789') ORDER BY el.itgkcode, el.learnername";
    $result = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);
    $report = [];
    $learnercodes = [];
    while ($row = mysqli_fetch_array($result[2])) {
        $photo = $_SERVER['DOCUMENT_ROOT'] . $row['basephotopath'] . $row['photo'];
        $photoJpg = str_replace('.png', '.jpg', $photo);

        $sign = $_SERVER['DOCUMENT_ROOT'] . $row['basephotopath'] . $row['sign'];
        $signJpg = str_replace('.png', '.jpg', $sign);

        $copyPhoto = $copySign = '';
        $dirpath = $_SERVER['DOCUMENT_ROOT'] . '/upload/permission_letters/photos/' . $row['dirname'];
        $newPhotoPath = $dirpath . $row['photo'];
        //if (!file_exists($newPhotoPath)) {
            if(file_exists($photoJpg)){
                $copyPhoto = $photoJpg;
                $learnercodes[] = $row['learnercode'];
            } elseif(file_exists($photo)) {
                $copyPhoto = $photo;
                $learnercodes[] = $row['learnercode'];
            }
            if (!empty($copyPhoto)) {
                makeDir($dirpath);
                if (! copy($copyPhoto, $newPhotoPath) ) {
                    $learnercodes[] = $row['learnercode'];
                }
            } else {
                //$learnercodes[] = $row['learnercode'];
            }
        //}

        $dirpath = $_SERVER['DOCUMENT_ROOT'] . '/upload/permission_letters/signs/' . $row['dirname'];
        $newPhotoPath = $dirpath . $row['sign'];
        //if (!file_exists($newPhotoPath)) {
            if(file_exists($signJpg)){
                $copySign = $signJpg;
                $learnercodes[] = $row['learnercode'];
            } elseif(file_exists($sign)) {
                $copySign = $sign;
                $learnercodes[] = $row['learnercode'];
            }

            if (!empty($copySign)) {
                makeDir($dirpath);
                if (! copy($copySign, $newPhotoPath) ) {
                    $learnercodes[] = $row['learnercode'];
                }
            } else {
                //$learnercodes[] = $row['learnercode'];
            }
        //}
    }

    $learnercodes = array_unique($learnercodes);
    print 'Total Centers: ' . count($learnercodes) . "<br /><br />";
    print implode("','", $learnercodes);

    function makeDir($path)
    {
         return is_dir($path) || mkdir($path);
    }

?>