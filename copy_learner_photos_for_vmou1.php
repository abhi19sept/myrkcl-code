<?php
    require 'DAL/classconnection.php';
    $_ObjConnection = new _Connection();
    $_ObjConnection->Connect();

    $job = $_GET['job'];
    $start = ($job - 1) * 50000;
    $end = 50000;

    //Get Photo/Sign of Eligible Learners
    //$selectQuery = "SELECT el.learnercode, el.coursename, el.batchname, CONCAT(el.learnercode, '_photo.png') AS photo, CONCAT(el.learnercode, '_sign.png') AS sign FROM tbl_eligiblelearners el INNER JOIN tbl_admission ta ON ta.Admission_LearnerCode = el.learnercode WHERE el.eventid = 1218 LIMIT $start, $end";

    //Get Photo/Sign of Given Learner Codes
    $selectQuery = "SELECT ta.Admission_LearnerCode AS learnercode, cm.Course_Name AS coursename, bm.Batch_Name AS batchname,  CONCAT(ta.Admission_LearnerCode, '_photo.png') AS photo, CONCAT(ta.Admission_LearnerCode, '_sign.png') AS sign FROM tbl_admission ta INNER JOIN tbl_batch_master bm ON bm.Batch_Code = ta.Admission_Batch 
        INNER JOIN tbl_course_master cm ON cm.Course_Code = ta.Admission_Course WHERE ta.Admission_LearnerCode IN ('439160825095053789','665160824092753789','363160419064149789','300160730023928789','538161017061625789','867161122113713789','322160824105127789','295161023074802789','680160824045557789','550161122124336789','827161010060920789','177160908110304789','819160430074845789','289160912070107789','982160430083044789','212160825100049789','543160912112522789','695161118022509789','190160831123842789','294160908111340789','959161010012943789','424160902082246789','281160825110140789','508160908115718789','330161010094553789','100161118021533789','518160908111857789','651161122090317789','869161018081636789','698160824094032789','564160425073115789','110161023072351789','795161010053311789','707160730043656789','606160415062129789','547161010054621789','244161010050908789','318161128125714789','406160824110218789','500161014053202789','533161010095423789','176161010013720789','835160425103006789','699160429082629789','835161010100524789','626161118024443789','457160908121155789','937160825045344789','323160730112953789','440160829083106789','137161116030150789','669161026081809789','596161017063424789','238160825051039789','811160425071508789','181161010113250789','657160730125520789','871161024100021789','816160824083408789','599161010071252789','598160908112714789','975161017060531789','768161119015054789','934161116030748789','758160830060346789','799161116031131789','293161023064609789','111160824095358789','544161010061919789','675161026091751789','870161010114033789','719160730111121789','620161020085532789','572160912073207789','213161123011525789','292160413083910789','175161010062735789','501161014040433789','883160824082353789','422161003112426789','147160907035754789','246160907121858789','661161004112043789','271161004105423789','731160907121028789','944161020082455789','673160913024610789','214161020074753789','593161027040736789','667160824074548789','589160825083049789','410160907120304789','382160804041623789','545161020081105789','537160907034951789','171160913024151789','388161011040614789','125160908073600789','394160907042716789','464160824063244789') ";

    $subdirName = 'vmoudemandphoto_LostCert';

    $result = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);
    $report = [];
    $learnercodes = [];

    while ($row = mysqli_fetch_array($result[2])) {

        $learnerphoto = getPath('photo', $row);
        $learnersign = getPath('sign', $row);

        $copyPhoto = $copySign = '';
        $dirpath = $_SERVER['DOCUMENT_ROOT'] . '/upload/permission_letters/photos/' . $subdirName.'/';
        makeDir($dirpath);

        $newPhotoPath = $dirpath . $row['photo'];
        if (!file_exists($newPhotoPath)) {
            //$learnercodes[] = $row['learnercode'];
            if(file_exists($learnerphoto)) {
                $copyPhoto = $learnerphoto;
            } else {
                $learnercodes[] = $row['learnercode'];
            }
            if (!empty($copyPhoto)) {
                //makeDir($dirpath);
                if (! copy($copyPhoto, $newPhotoPath) ) {
                    $learnercodes[] = $row['learnercode'];
                }
            } else {
                $learnercodes[] = $row['learnercode'];
            }
        }

        $dirpath = $_SERVER['DOCUMENT_ROOT'] . '/upload/permission_letters/signs/' . $subdirName.'/';
        makeDir($dirpath);

        $newPhotoPath = $dirpath . $row['sign'];
        if (!file_exists($newPhotoPath)) {
            //$learnercodes[] = $row['learnercode'];
            if(file_exists($learnersign)){
                $copySign = $learnersign;
            } else {
                $learnercodes[] = $row['learnercode'];
            }

            if (!empty($copySign)) {
                //makeDir($dirpath);
                if (! copy($copySign, $newPhotoPath) ) {
                    $learnercodes[] = $row['learnercode'];
                }
            } else {
                $learnercodes[] = $row['learnercode'];
            }
        }
    }

    $learnercodes = array_unique($learnercodes);
    print 'Total Centers: ' . count($learnercodes) . "<br /><br />";
    print implode("', '", $learnercodes);

    function makeDir($path)
    {
         return is_dir($path) || mkdir($path);
    }

    function getbatchname($row) {
        $batchname = trim(ucwords($row['batchname']));
        $coursename = $row['coursename'];
        $isGovtEmp = stripos($coursename, 'gov');
        if ($isGovtEmp) {
            $batchname = $batchname . '_Gov';
        } else {
            $isWoman = stripos($coursename, 'women');
            if ($isWoman) {
                $batchname = $batchname . '_Women';
            }
        }
        $batchname = str_replace(' ', '_', $batchname);

        return $batchname;
    }

    function getPath($type, $row) {
        $batchname = getbatchname($row);

        $dirPath = '/upload/admission_' . $type . '/' . $row['learnercode'] . '_' . $type . '.png';

        $imgPath = $_SERVER['DOCUMENT_ROOT'] . $dirPath;
        $imgPath = str_replace('//', '/', $imgPath);

        $dirPathJpg = str_replace('.png', '.jpg', $dirPath);
        $imgPathJpg = str_replace('.png', '.jpg', $imgPath);

        $imgBatchPath = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_' . $type . '/' . $batchname . '/' . $row['learnercode'] . '_' . $type . '.png';
        $imgBatchPath = str_replace('//', '/', $imgBatchPath);
        $imgBatchPathJpg = str_replace('.png', '.jpg', $imgBatchPath);

        $path = '';
        if (file_exists($imgPath)) {
            $path = $imgPath;
        } elseif (file_exists($imgPathJpg)) {
            $path = $imgPathJpg;
        } elseif (file_exists($imgBatchPath)) {
            $path = $imgBatchPath;
        } elseif (file_exists($imgBatchPathJpg)) {
            $path = $imgBatchPathJpg;
        } else {
            $imgPath = 'http://' . $_SERVER['HTTP_HOST'] . $dirPath;
            if(@getimagesize($imgPath)) {
                $path = $imgPath;
            } else {
                $path = 'http://' . $_SERVER['HTTP_HOST'] . $dirPathJpg;
            }
        }

        return $path;
    }

?>