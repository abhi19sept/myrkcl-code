<?php
$title="Security Question";
include ('header.php'); 
include ('root_menu.php'); 

         if (isset($_REQUEST['code'])) {
                echo "<script>var SQ_ID=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var CourseTypeCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
        <div class="container"> 
			

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Security Question Master</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmCourseTypemaster" id="frmCourseTypemaster" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container"  style=" padding: 0px;">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>
                            <div class="col-sm-5 col-md-5" style=" padding: 0px;">     
                                <label for="learnercode"> Security Question:</label>
                                <input type="text" class="form-control" maxlength="50" name="Security_Question" id="Security_Question"  onkeypress="javascript:return validUserName(event);" placeholder="Security Question">
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="ename">Question Status:</label>
				<select id="ddlStatus" name="ddlStatus" class="form-control" >
								 
                                </select>
                                     
                            </div>
                            <input  style=" margin-top: 24px;" type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>

                        <div id="gird"> </div>
                    </form>			
                </div> 
            </div>   
        </div>


    




</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>


<script type="text/javascript">
    function validUserName(e){
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("^[ 0-9A-Za-z_'.+-]*$");

            if (key === 8 || key === 0 || key === 32) {
                keychar = "a";
            }
            return reg.test(keychar);
        }
        
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
            
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfsecurityquestion.php",
                    data: "action=DELETE&values=" + SQ_ID + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmsecurityquestion.php";
                           }, 1000);
                           
                            Mode="Add";
                            resetForm("frmcoursetype");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfsecurityquestion.php",
                    data: "action=EDIT&values=" + SQ_ID + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Status']);
                        data = $.parseJSON(data);
                        Security_Question.value = data[0].Security_Question;
                        ddlStatus.value = data[0].Security_Status;
                        
                    }
                });
            }

            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfsecurityquestion.php",
                    data: "action=SHOW",
                    success: function (data) {
						//alert(data);
                        $("#gird").html(data);

                    }
                });
            }

            showData();


            $("#btnSubmit").click(function () {
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfsecurityquestion.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&Security_Question=" + Security_Question.value + "&status=" + ddlStatus.value +""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + SQ_ID + "&Security_Question=" + Security_Question.value + "&status=" + ddlStatus.value +""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                                window.location.href="frmsecurityquestion.php";
                            }, 500);

                            Mode="Add";
                            resetForm("frmcoursetype");
                        }
//                        else if(data=='EMPTY'){
//                            
//                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> " + data + "</span></p>");
                        }
                        showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>

<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>