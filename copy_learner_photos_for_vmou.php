<?php
    require 'DAL/classconnection.php';
    $_ObjConnection = new _Connection();
    $_ObjConnection->Connect();
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
    ini_set("memory_limit", "5000M");
    ini_set("max_execution_time", 0);
    ini_set("max_input_vars", 10000);
     
    set_time_limit(0);

    //ExampleUrl: http://10.1.1.10/copy_learner_photos_for_vmou.php?job=1&examid=1219&examDate=17thAugust2017&copy=1
   
    $job = $_REQUEST['job'];
    $start = ($job - 1) * 10000;
    $end = 10000;

    $subdirName = (isset($_GET['examDate'])) ? $_GET['examDate'] : date("dSFY"); //'17thAugust2017';
    $examId = $_REQUEST['examid'];
    $isCopy = (isset($_GET['copy'])) ? $_GET['copy'] : 0;

    //Get Photo/Sign of Eligible Learners
    $selectQuery = "SELECT el.learnercode, el.itgkcode, el.coursename, el.batchname, CONCAT(el.learnercode, '_photo.png') AS photo, CONCAT(el.learnercode, '_sign.png') AS sign FROM tbl_eligiblelearners el WHERE el.eventid = $examId AND el.status LIKE ('Eligible') ORDER BY el.batchname, el.coursename, el.learnercode LIMIT $start, $end";

    //Get Photo/Sign of Given Learner Codes
    //$selectQuery = "SELECT ta.Admission_LearnerCode AS learnercode, cm.Course_Name AS coursename, bm.Batch_Name AS batchname,  CONCAT(ta.Admission_LearnerCode, '_photo.png') AS photo, CONCAT(ta.Admission_LearnerCode, '_sign.png') AS sign FROM tbl_admission ta INNER JOIN tbl_batch_master bm ON bm.Batch_Code = ta.Admission_Batch INNER JOIN tbl_course_master cm ON cm.Course_Code = ta.Admission_Course WHERE ta.Admission_Batch > 120 LIMIT $start, $end";

    print $selectQuery;
    if (isset($_REQUEST['showqry']) && !empty($_REQUEST['showqry'])) {
        die;
    }
    //die;

    $result = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);
    $report = [];
    $learnercodes = [
        'photo' => [],
        'sign' => [],
    ];
    if (isset($result[2]) && mysqli_num_rows($result[2])) {
        while ($row = mysqli_fetch_array($result[2])) {

            $learnerphoto = getPath('photo', $row);
            $learnersign = getPath('sign', $row);

            $copyPhoto = $copySign = '';
            $dirpath = $general->getDocRootDir() . '/upload/permission_letters/photos/' . $subdirName . '/';
            makeDir($dirpath);

            $newPhotoPath = $dirpath . $row['photo'];
            if (!file_exists($newPhotoPath)) {
                //$learnercodes[] = $row['learnercode'];
                if(!empty($learnerphoto) && file_exists($learnerphoto)) {
                    $copyPhoto = $learnerphoto;
                } else {
                    $learnercodes['photo'][] = $row['learnercode']  . ',' . $row['batchname'] . ',' . $row['coursename'] . ',' . $row['itgkcode'];
                }
                if (!empty($copyPhoto)) {
                    //makeDir($dirpath);
                    if ($isCopy == 1) {
                        if (!copy($copyPhoto, $newPhotoPath) ) {
                            $learnercodes['photo'][] = $row['learnercode']  . ',' . $row['batchname'] . ',' . $row['coursename'] . ',' . $row['itgkcode'];
                        }
                    }
                } else {
                    $learnercodes['photo'][] = $row['learnercode']  . ',' . $row['batchname'] . ',' . $row['coursename'] . ',' . $row['itgkcode'];
                }
            }

            $dirpath = $general->getDocRootDir() . '/upload/permission_letters/signs/' . $subdirName . '/';
            makeDir($dirpath);

            $newPhotoPath = $dirpath . $row['sign'];
            if (!file_exists($newPhotoPath)) {
                //$learnercodes[] = $row['learnercode'];
                if(!empty($learnersign) && file_exists($learnersign)){
                    $copySign = $learnersign;
                } else {
                    $learnercodes['sign'][] = $row['learnercode']  . ',' . $row['batchname'] . ',' . $row['coursename'] . ',' . $row['itgkcode'];
                }

                if (!empty($copySign)) {
                    if ($isCopy == 1) {
                        if (!copy($copySign, $newPhotoPath) ) {
                            $learnercodes['sign'][] = $row['learnercode']  . ',' . $row['batchname'] . ',' . $row['coursename'] . ',' . $row['itgkcode'];
                        }
                    }
                } else {
                    $learnercodes['sign'][] = $row['learnercode']  . ',' . $row['batchname'] . ',' . $row['coursename'] . ',' . $row['itgkcode'];
                }
            }
        }
    }

    if ($learnercodes) {
        if (isset($learnercodes['photo']) && !empty($learnercodes['photo'])) {
            $learnercodes['photo'] = array_unique($learnercodes['photo']);
            print '<p>' . count($learnercodes['photo']) . " photos are not found of learnercodes :</p>";
            print 'learnercode,batchname,coursename,itgkcode<br />';
            print implode('<br />', $learnercodes['photo']);
            print '<p>&nbsp;</p>';
        }
        if (isset($learnercodes['sign']) && !empty($learnercodes['sign'])) {
            $learnercodes['sign'] = array_unique($learnercodes['sign']);
            print '<p>' . count($learnercodes['sign']) . " signs are not found of learnercodes :</p>";
            print 'learnercode,batchname,coursename,igtkcode<br />';
            print implode('<br />', $learnercodes['sign']);
        }
    } else {
        print "<p>No learner found with missing photo / sign.</p>";
    }

    function makeDir($path)
    {
         return is_dir($path) || mkdir($path);
    }

    function getbatchname($row) {
        $batchname = trim(ucwords($row['batchname']));
        $coursename = $row['coursename'];
        $isGovtEmp = stripos($coursename, 'gov');
        if ($isGovtEmp) {
            $batchname = $batchname . '_Gov';
        } else {
            $isWoman = stripos($coursename, 'women');
            if ($isWoman) {
                $batchname = $batchname . '_Women';
            } else {
                $isMadarsa = stripos($coursename, 'madar');
                if ($isMadarsa) {
                    $batchname = $batchname . '_Madarsa';
                }
            }
        }
        $batchname = str_replace(' ', '_', $batchname);

        return $batchname;
    }

    function getPath($type, $row) {
        global $general;

        $batchname = getbatchname($row);

        $dirPath = '/upload/admission' . $type . '/' . $row['learnercode'] . '_' . $type . '.png';

        $imgPath = $general->getDocRootDir() . $dirPath;
        $imgPath = str_replace('//', '/', $imgPath);

        $dirPathJpg = str_replace('.png', '.jpg', $dirPath);
        $imgPathJpg = str_replace('.png', '.jpg', $imgPath);

        $imgBatchPath = $general->getDocRootDir() . '/upload/admission' . $type . '/' . $batchname . '/' . $row['learnercode'] . '_' . $type . '.png';
        $imgBatchPath = str_replace('//', '/', $imgBatchPath);
        $imgBatchPathJpg = str_replace('.png', '.jpg', $imgBatchPath);

        $path = '';
        if (file_exists($imgBatchPath)) {
            $path = $imgBatchPath;
        } elseif (file_exists($imgBatchPathJpg)) {
            $path = $imgBatchPathJpg;
        } elseif (file_exists($imgPath)) {
            $path = $imgPath;
        } elseif (file_exists($imgPathJpg)) {
            $path = $imgPathJpg;
        } else {
            /*$imgPath = 'http://' . $_SERVER['HTTP_HOST'] . $dirPath;
            if(@getimagesize($imgPath)) {
                $path = $imgPath;
            } else {
                $path = 'http://' . $_SERVER['HTTP_HOST'] . $dirPathJpg;
            }*/
        }

        return $path;
    }

?>