<?php
$title = "Admission Form";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    $Mode = $_REQUEST['Mode'];
} else {
    echo "<script>var AdmissionCode=0</script>";
    echo "<script>var Mode='Add'</script>";
    $Mode = "Add";
}
if (isset($_REQUEST['ddlCourse'])) {
    $_SESSION['CourseCode'] = $_REQUEST['ddlCourse'];
    $_SESSION['BatchCode'] = $_REQUEST['ddlBatch'];
} else {
    echo "<script>var CourseCode=1</script>";
    echo "<script>var BatchCode=249</script>";
	//$_SESSION['CourseCode'] = 1;
    //$_SESSION['BatchCode'] = 249;
}

echo "<script>var CourseCode = '" . $_SESSION['CourseCode'] . "'</script>";
echo "<script>var BatchCode  = '" . $_SESSION['BatchCode'] . "'</script>";

//print_r($_SESSION['CourseCode']);
//print_r($_SESSION['BatchCode']);
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div class="container"> 

    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading" style="height:40px">
            <div  class="col-sm-3"> Admission Form </div>

            <div class="col-sm-3">
                Course: <div style="display: inline" id="txtcoursename"> </div>
            </div>

            <div class="col-sm-3">
                Batch: <div style="display: inline" id="txtbatchname"> </div>
            </div>

            <div class="col-sm-3">
                Available Intake: <div style="display: inline" id="txtintakeavailable"> </div>
            </div>


        </div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            <form name="frmadmission" id="frmadmission" class="form-inline" role="form" enctype="multipart/form-data"> 
                <div class="container">
                       <div id="loadingDiv" class="overlay">
                            <div class="overlay__inner">
                                <div class="overlay__content"><span class="spinner"></span></div>
                            </div>
                        </div>
                    <div class="container">
                        <div class="col-sm-4 form-group" id="response"></div>
                    </div>        
                    <div id="errorBox"></div>
                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Learner Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" name="txtlname" id="txtlname" style="text-transform:uppercase" onkeypress="javascript:return allowchar(event);" placeholder="Learner Name">
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtAdmissionCode" id="txtAdmissionCode"/>
                        <select id="ddlBatch" style="display:none;" name="ddlBatch" class="form-control">  </select>
                        <input type="hidden" class="form-control" maxlength="50" name="txtCourseFee" id="txtCourseFee"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtInstallMode" id="txtInstallMode"/>
                        <input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
                        <input type="hidden" class="form-control"  name="txtsign" id="txtsign"/>
                        <input type="text" class="form-control"  name="txtLearnercode" id="txtLearnercode" style="display: none;" />
                        <input type="text" class="form-control"  name="txtFTPBatchName" id="txtFTPBatchName" style="display: none;" />
                        <input type="text" class="form-control"  name="txtFTPCourseName" id="txtFTPCourseName" style="display: none;" />
                        <input type="hidden" class="form-control" maxlength="50" name="txtmode" id="txtmode" value="<?php echo $Mode; ?>" />
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="fname">Father/Husband Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" style="text-transform:uppercase" maxlength="50" name="txtfname" id="txtfname" onkeypress="javascript:return allowchar(event);" placeholder="Father Name">     
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="dob">Date of Birth:<span class="star">*</span></label>								
                        <input type="text" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="YYYY-MM-DD">
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="mtongue">Mother Tongue:</label>
                        <select id="ddlmotherTongue" name="ddlmotherTongue" class="form-control" >								  
                            <option value="Hindi" selected>Hindi</option>
                            <option value="English">English</option>
                        </select>    
                    </div>
                </div>  

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="gender">Gender:</label> <br/>                               
                        <label class="radio-inline"> <input type="radio" id="genderMale" checked="checked" name="gender" /> Male </label>
                        <label class="radio-inline"> <input type="radio" id="genderFemale" name="gender" /> Female </label>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="email">Marital Status:</label> <br/>  
                        <label class="radio-inline"><input type="radio" id="mstatusSingle" checked="checked" name="mstatus" /> Single </label>
                        <label class="radio-inline"><input type="radio" id="mstatusMarried" name="mstatus" /> Married	</label>							
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="address">Medium Of Study:</label> <br/> 

                        <label class="radio-inline"> 
                            <input type="radio" id="mediumHindi" name="Medium" checked="checked" />Combined (Hindi + English)</label>

                    </div>

                    <div class="col-sm-6 form-group"> 
                        <label for="deptname">Physically Challenged:</label> <br/> 
                        <label class="radio-inline"> <input type="radio" id="physicallyChStYes" name="PH" /> Yes </label>
                        <label class="radio-inline"> <input type="radio" id="physicallyChStNo" name="PH" checked="checked"/> No </label>
                    </div>
                </div>    

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="empid">Proof Of Identity:<span class="star">*</span></label>
                        <select id="ddlidproof" name="ddlidproof" class="form-control">
                            <option value="">Select</option>
                            <option value="PAN Card">PAN Card</option>
                            <option value="Voter ID Card">Voter ID Card</option>
                            <option value="Driving License">Driving License</option>
                            <option value="Passport">Passport</option>
                            <option value="Employer ID card">Employer ID card</option>
                            <option value="Government ID Card">Governments ID Card</option>
                            <option value="College ID Card">College ID Card</option>
                            <option value="School ID Card">School ID Card</option>
                        </select>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <!--<label for="gpfno">Aadhar No:</label>-->
                        <!--<input type="text" class="form-control" maxlength="12"  name="txtidno" id="txtidno" onkeypress="javascript:return allownumbers(event);" placeholder="Aadhar No">-->

                        <label for="empid">Do You Have Aadhar:<span class="star">*</span></label>
                        <input type="label" class="form-control" maxlength="12"  name="txtaadharno" id="txtaadharno" style="display:none" placeholder="Aadhar No">

                        <select id="ddlAadhar" name="ddlAadhar" class="form-control">
                            <option value="">Select</option>
                            <option value="y">Yes</option>
                            <option value="n">No</option>


                        </select>

                    </div>


                    <div class="col-sm-4 form-group"> 
                        <label for="designation">District:<span class="star">*</span></label>
                        <select id="ddlDistrict" name="ddlDistrict" class="form-control">                                  
                        </select>
                    </div>     

                    <div class="col-sm-4 form-group"> 
                        <label for="marks">Tehsil:<span class="star">*</span></label>
                        <select id="ddlTehsil" name="ddlTehsil" class="form-control">                                 
                        </select>
                    </div>
                </div>

                <div class="container">                          
                    <div class="col-sm-4 form-group"> 
                        <label for="attempt">Address:<span class="star">*</span></label>  
                        <textarea class="form-control" rows="3" style="text-transform:uppercase" id="txtAddress" name="txtAddress" placeholder="Address" onkeypress="javascript:return validAddress(event);"></textarea>

                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="pan">PINCODE:<span class="star">*</span></label>
                        <input type="text" class="form-control" name="txtPIN" id="txtPIN"  placeholder="PINCODE" onkeypress="javascript:return allownumbers(event);" maxlength="6"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankaccount">Mobile No:<span class="star">*</span></label>
                        <input type="text" class="form-control"  name="txtmobile"  id="txtmobile" placeholder="Mobile No" onkeypress="javascript:return allownumbers(event);" maxlength="10"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankdistrict">Phone No:</label>
                        <input type="text" placeholder="Phone No" class="form-control" id="txtResiPh" name="txtResiPh" onkeypress="javascript:return allownumbers(event);" maxlength="11"/>
                    </div>   
                </div>

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="bankname">Email Id:</label>
                        <input type="text" placeholder="Email Id" class="form-control" id="txtemail" name="txtemail" maxlength="100" />
                    </div>   

                    <div class="col-sm-4 form-group"> 
                        <label for="branchname">Qualification:<span class="star">*</span></label>
                        <select id="ddlQualification" name="ddlQualification" class="form-control">                                  
                        </select>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="ifsc">Type of Learner:<span class="star">*</span></label>
                        <select id="ddlLearnerType" name="ddlLearnerType" class="form-control" onChange="findselected()">                                 
                        </select>
                    </div> 

                    <?php if ($_SESSION['CourseCode'] == '1' || $_SESSION['CourseCode'] == '5' || $_SESSION['CourseCode'] == '20' || $_SESSION['CourseCode'] == '22' || $_SESSION['CourseCode'] == '25' ||
					$_SESSION['CourseCode'] == '26' ||
					$_SESSION['CourseCode'] == '27' ||
					$_SESSION['CourseCode'] == '28' ||
					$_SESSION['CourseCode'] == '29' ||
					$_SESSION['CourseCode'] == '31' ||
					$_SESSION['CourseCode'] == '30' ||
					$_SESSION['CourseCode'] == '32') { ?>
                        <div class="col-sm-4 form-group" style="display:none;"> 
                            <label for="micr">Employee ID:</label>
                            <input type="text" placeholder="GPF No" class="form-control" id="txtEmpId" name="txtEmpId" maxlength="20"/>
                        </div>
                    <?php } else if ($_SESSION['CourseCode'] == '4') { ?>
                        <div class="col-sm-4 form-group" > 
                            <label for="micr">Employee ID:</label>
                            <input type="text" placeholder="Employee ID" class="form-control" id="txtEmpId" name="txtEmpId" maxlength="20"/>
                        </div>
                    <?php } ?>
                </div>

                <fieldset style="border: 1px groove #ddd !important;" class="">
                    <br>
                    <div class="container">
                        <div class="col-sm-1"> 

                        </div>
                        <div class="col-sm-3" style="display: none"> 
                            <label for="payreceipt">Upload Scan Application Form:<span class="star">*</span></label>
                            <input type="file" class="form-control"  name="txtUploadScanDoc" id="txtUploadScanDoc" onchange="checkScanForm(this)"/>
                        </div>											

                        <div class="col-sm-3" > 
                            <label for="photo">Attach Photo:<span class="star">*</span></label> </br>
                            <img id="uploadPreview1" src="images/samplephoto.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">								  

                            <input style="margin-top:5px !important;" id="uploadImage1" type="file" name="p1" onchange="checkPhoto(this);
                                    PreviewImage(1)"/>									  
                        </div>

                        <div class="col-sm-3"> 
                            <label for="photo">Attach Signature:<span class="star">*</span></label> </br>
                            <img id="uploadPreview2" src="images/samplesign.png" id="uploadPreview2" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage2').click();">							  
                            <input id="uploadImage2" type="file" name="p2" onchange="checkSign(this);
                                    PreviewImage(2)"/>
                        </div>  
                        <div class="col-sm-3"> 
                            <input style="margin-top:50px !important;" type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload Photo Sign"/>   
                        </div>
                        <div class="col-sm-1"> 

                        </div>

                    </div>
                    <br>
                </fieldset>
                <br><br>
                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                </div>
        </div>
    </div>   
</div>

</form>
<link href="css/popup.css" rel="stylesheet" type="text/css">
<!-- Modal -->
<div id="myModalupdate" class="modal" style="padding-top:150px !important">

    <div class="modal-content" style="width: 90%;">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h6>Update Your Aadhar Number</h6>
        </div>
        <div class="modal-body" style="max-height: 800px;" id='formhtml'>
            <form name="frmCourseBatch" id="frmCourseBatch" class="form-horizontal" role="form"
                  enctype="multipart/form-data" style="margin: 25px 0 40px 0;">  
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6" id="responsesendotp"></div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Enter Aadhaar Number:</label>
                    <div class="col-sm-8">
                        <input class="form-control valid" name="txtACno" id="txtACno" placeholder="Aadhaar Card Number" aria-invalid="false"  maxlength="12"  onkeypress="javascript:return allownumbers(event);"  type="text">
                    </div>
                </div>
                <!--                  <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">Pin Code Number:</label>
                                      <div class="col-sm-6">
                                          <input class="form-control valid" name="txtPINno" id="txtPINno" placeholder="Pin Number" aria-invalid="false" maxlength="6"  onkeypress="javascript:return allownumbers(event);"  type="text">
                                      </div>
                                  </div>-->
                <div class="form-group" id="otpTxt" name="otpTxt"  style=" display: none;">
                    <label for="inputEmail3" class="col-sm-3 control-label">OTP:</label>
                    <div class="col-sm-8">
                        <input class="form-control valid" name="txtotpTxt" id="txtotpTxt" placeholder="OTP" aria-invalid="false" maxlength="9"  onkeypress="javascript:return allownumbers(event);"  type="text">
                    </div>
                </div>
                <div class="form-group" >
                    <div class="col-sm-3"></div>
                    <div class="col-sm-8" id="turmcandition">
                        <p>I,the holder of above mentioned Aadhar Number, hereby give my consent to RKCL to obtain my Aadhar Number, Name and Fingerprint/Iris for authentication with UIDAI. I have no objection using my identity and biometric information for validation with Aadhar(CIDR) only for the purpose of authentication will be used for generation and issuance of Digitally Signed RS-CIT certificate.</p>
                        <div><span class="yesturm">I Agree</span> <input type="checkbox" id="yes" name="fooby[1][]" /> Yes
<!--                              <input class="noturm" type="checkbox" id="no" name="fooby[1][]" /> No-->
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
                <div class="form-group last">
                    <div class="col-sm-offset-3 col-sm-6">
                        <input type="button" name="btnOTPSubmit" style="display: none;" id="btnOTPSubmit" class="btn btn-primary" value="Submit"/>
                        <input type="button" name="btnAadharSubmit"   style="display: none;" id="btnAadharSubmit" class="btn btn-primary valid" value="Submit"/>
                    </div>
                </div>   
                <div id="FinalResult" class="form-group"></div>
                <input class="form-control valid"  name="txtTxn" id="txtTxn" type="hidden">
                <input class="form-control valid" name="txtStatus" id="txtStatus" type="hidden">
            </form>

        </div>
    </div>
</div>

<div id="myModalupdate2" class="modal" style="padding-top:150px !important">

    <div class="modal-content panel-danger" style="width: 40%;">
        <div class=" panel-heading">
            <span class="close close2">&times;</span>
            <h6>Warning</h6>
        </div>
        <div class="modal-body" style="max-height: 800px;" id='formhtml'>

            <form name="frmCourseBatch1" id="frmCourseBatch1" class="form-horizontal" role="form"
                  enctype="multipart/form-data" style="margin: 15px;">  
                <div class="form-group" >
                  
                    <div class="col-sm-12" id="turmcandition">
                        <p>क्या आप अपना आधार नंबर दर्ज नहीं करना चाहते हैं ?? यह आपको  RS-CIT फ़ाइनल एक्जाम का डिजिटल साइन्ड ई-सर्टिफिकेट जनरेट करने मे मदद करता है|</p>
                        <div><span class="yesturm">I Agree</span> <input type="checkbox" id="yes1" name="fooby[1][]" /> Yes
<!--                              <input class="noturm" type="checkbox" id="no" name="fooby[1][]" /> No-->
                        </div>
                    </div>
                    
                </div>
                <div class="form-group last">
                    <div class="col-sm-offset-6 col-sm-6">
                        <input type="button" name="btnOTPSubmit1" style="display: none;" id="btnOTPSubmit1" class="btn btn-danger" value="Submit"/>

                    </div>
                </div>  
            </form>

        </div>
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script src="scripts/imageupload.js"></script>

<style>
    .overlay {
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    position: fixed;
    background: #222;
    z-index: 999;
    opacity: 0.2;
}

.overlay__inner {
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    position: absolute;
}

.overlay__content {
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
}

.spinner {
  
    display: inline-block;
    border: 16px solid #f3f3f3; /* Light grey */
      border-top: 16px solid #006fba; /* Blue */
      border-radius: 50%;
      width: 120px;
      height: 120px;
      animation: spin 2s linear infinite;
}

@keyframes spin {
  100% {
    transform: rotate(360deg);
  }
}
a {
  color: #FFFFFF;
  text-decoration: none;
}
    #errorBox
    {	color:#F00;	 } 
</style>

<script type="text/javascript">
    var $loading = $('#loadingDiv').hide();
    $(document)
      .ajaxStart(function () {
        $loading.show();
      })
      .ajaxStop(function () {
        $loading.hide();
      });
    $('#dob').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true
    });
</script>

<script type="text/javascript">
    $('#txtEmpId').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
</script>

<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>

<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
    ;
</script>	

<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#txtUploadScanDoc').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }
        document.getElementById("txtUploadScanDoc").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkPhoto(target) {
        var ext = $('#uploadImage1').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage1").value = '';
            return false;
        }

        if (target.files[0].size > 5000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 5 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        } else if (target.files[0].size < 3000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        document.getElementById("uploadImage1").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkSign(target) {
        var ext = $('#uploadImage2').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage2").value = '';
            return false;
        }

        if (target.files[0].size > 3000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 3 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        } else if (target.files[0].size < 1000)
        {
            alert("Image size should be greater than 1 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        document.getElementById("uploadImage2").innerHTML = "";
        return true;
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {
				if(BatchCode == ""){

            window.location.href = 'frmselbatch.php';
        }
        //alert(BatchCode);
        var aadharname = "";
        var aadhardob = "";
        var aadharno = "";
        function FillIntake() {
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=FILLIntake&values=" + BatchCode + "",
                success: function (data) {
					//alert(data);
                    document.getElementById('txtintakeavailable').innerHTML = data.trim();
                    if (document.getElementById('txtintakeavailable').innerHTML == '0') {
                        alert("Intake Not Available");
                        window.location.href = "frmmessage.php";
                    }
                }
            });
        }
        FillIntake();

        function FillCourseName() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FillAdmissionCourse&values=" + CourseCode + "",
                success: function (data) {
                    document.getElementById('txtcoursename').innerHTML = data;
                    txtFTPCourseName.value = CourseCode;
                }
            });
        }
        FillCourseName();

        function FillBatchName() {
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FillAdmissionBatch&values=" + BatchCode + "",
                success: function (data) {
                    document.getElementById('txtbatchname').innerHTML = data;
					txtFTPBatchName.value = data;
                    
                }
            });
        }
        FillBatchName();

        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                }
            });
        }
        GenerateUploadId();


        function GenerateLearnercode()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=GENERATELEARNERCODE",
                success: function (data) {
                    txtLearnercode.value = data;
                }
            });
        }
        GenerateLearnercode();

        function AdmissionFee()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=Fee&codes=" + BatchCode + "",
                success: function (data) {
                    txtCourseFee.value = data;
                }
            });
        }
        AdmissionFee();

        function InstallMode()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=Install&values=" + BatchCode + "",
                success: function (data) {
                    txtInstallMode.value = data;
                }
            });
        }
        InstallMode();

        $("#txtEmpId").prop("disabled", true);

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        } else if (Mode == 'Edit')
        {
            //alert(1);
            fillForm();
        }

        $("input[type='image']").click(function () {
            $("input[id='my_file']").click();
        });

        function FillDistrict() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        }
        FillDistrict();

        function FillQualification() {
            $.ajax({
                type: "post",
                url: "common/cfQualificationMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlQualification").html(data);
                }
            });
        }
        FillQualification();

        function FillLearnerType() {
            $.ajax({
                type: "post",
                url: "common/cfLearnerTypeMaster.php",
                data: "action=FILL&coursecode=" + CourseCode + "",
                success: function (data) {
                    $("#ddlLearnerType").html(data);
                }
            });
        }
        FillLearnerType();

        function findselected() {
            var selMenu = document.getElementById('ddlLearnerType');
            var txtField = document.getElementById('txtEmpId');
				txtField.disabled = false;
            /*if (selMenu.value != '34')
                txtField.disabled = true
            else
                txtField.disabled = true*/
        }

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });

        $("#ddlLearnerType").change(function () {
            findselected();
        });

        // statement block start for delete record function
        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=DELETE&values=" + AdmissionCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmLearnerProfile.php";
                        }, 1000);
                        Mode = "Add";
                        resetForm("frmLearnerProfile");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }
        // statement block end for delete function statment

        function fillForm()
        {

            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=EDIT&values=" + AdmissionCode + "",
                success: function (data) {

                    data = $.parseJSON(data);
                    txtlname.value = data[0].lname;
                    txtfname.value = data[0].fname;
                    dob.value = data[0].dob;
                    txtAdmissionCode.value = data[0].AdmissionCode;
                    txtphoto.value = data[0].photo;
                    txtsign.value = data[0].sign;
                    $("#uploadPreview1").attr('src', "upload/admission_photo/" + data[0].photo);
                    $("#uploadPreview2").attr('src', "upload/admission_sign/" + data[0].sign);
                    //p1.value = data[0].photo;
                    //p2.value = data[0].sign; 
                }
            });
        }


        $("#ddlAadhar").change(function () {
            var selAadhar = $(this).val();
            //alert(selregion);
            if (selAadhar == 'y') {
                var mybtnid = "myBtn_" + selAadhar;
                var modal = document.getElementById('myModalupdate');
                var btn = document.getElementById(mybtnid);
                var span = document.getElementsByClassName("close")[0];
                modal.style.display = "block";
                span.onclick = function () {
                    modal.style.display = "none";
//                    window.location.href = "frmadmission.php";
                    $("#btnOTPSubmit").show();
                    $("#otpTxt").hide();
                    $("#btnAadharSubmit").hide();
                    $("#txtACno").attr("readonly", false);
                }
            }
            if (selAadhar == 'n') {
                var mybtnid = "myBtn_" + selAadhar;
                var modal2 = document.getElementById('myModalupdate2');
                var btn = document.getElementById(mybtnid);
                var span = document.getElementsByClassName("close2")[0];
                modal2.style.display = "block";
                span.onclick = function () {
                    modal2.style.display = "none";
//                    window.location.href = "frmadmission.php";

                }
            }

        });

        $('#yes').change(function () {
            if ($(this).is(":checked")) {
                var returnVal = confirm("Are you sure?");
                if (returnVal) {
                    $("#btnOTPSubmit").css("display", "block");
                } else {
                    $("#btnOTPSubmit").css("display", "none");
                }

            } else {
                $("#btnOTPSubmit").css("display", "none");
            }
        });
        $('#yes1').change(function () {
            if ($(this).is(":checked")) {
                var returnVal = confirm("Are you sure?");
                if (returnVal) {
                    $("#btnOTPSubmit1").css("display", "block");
                } else {
                    $("#btnOTPSubmit1").css("display", "none");
                }

            } else {
                $("#btnOTPSubmit1").css("display", "none");

            }
        });
        $("#btnOTPSubmit1").click(function () {
            var modal2 = document.getElementById('myModalupdate2');
            modal2.style.display = "none";
        });
        $("#btnOTPSubmit").click(function () {
            if (txtACno.value == '')
            {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter Aadhaar Number." + "</span></p>");
            } else {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/ajax-loader.gif width=40px /></span><span>Please wait we are fetching and verifying your Aadhaar Data.</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfAdmission.php",
                    data: "action=generateOTP&txtACno=" + txtACno.value + "",
                    success: function (data)
                    {
                        data = $.parseJSON(data);
                        $('#responsesendotp').empty();
                        if (data.status == "n")
                        {
                            $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Invalid Aadhaar Number. Please check your Aadhaar Number." + "</span></p>");
                        } else
                        {           // console.log(data);
                            //alert(data.txn);
                            $('#responsesendotp').append("<p class='sucess'><span><img src=images/correct.gif width=10px /></span><span>" + " We have sent OTP on your registered Aadhaar Number. Please enter OTP as received on Mobile." + "</span></p>");
                            txtTxn.value = data.txn;
                            txtStatus.value = data.status;
                            $("#btnOTPSubmit").hide();
                            $("#otpTxt").show();
                            $("#btnAadharSubmit").show();
                            $("#txtACno").attr("readonly", "readonly");
                            $("#txtPINno").attr("readonly", "true");
                        }
                    }
                });
            }
        });

        $("#btnAadharSubmit").click(function () {
            if (txtotpTxt.value == '')
            {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter OTP." + "</span></p>");
            } else {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $("#txtotpTxt").attr("readonly", "true");
                $.ajax({
                    type: "post",
                    url: "common/cfAdmission.php",
                    data: "action=authenticateOTP&txtACno=" + txtACno.value + "&txtTxn=" + txtTxn.value + "&txtStatus=" + txtStatus.value + "&txtotpTxt=" + txtotpTxt.value + "",
                    success: function (data)
                    {
					if(txtACno.value == "200646132478"){
                            //alert(data);
                        }
                        //alert(data);
                        data = $.parseJSON(data);
                        $('#responsesendotp').empty();
                        if (data.status == 'y') {
                            $('#responsesendotp').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Aadhar Detail Verified</span></p>");
                            aadharname = data.learnername;
                            aadhardob = data.learnerdob;
                            aadharno = data.learneraadhar;

                            $('#txtaadharno').val(aadharno);
                            $("#txtaadharno").show();
                            $("#ddlAadhar").hide();
							$('#txtaadharno').prop('readonly', true);
                            window.setTimeout(function () {

                                $("#myModalupdate").css("display", "none");


                            }, 3000);




                        } else {
                            $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //$("#FinalResult").html(data);

                    }
                });
            }
        });
        $("#btnUpload").click(function () {
            $('#btnSubmit').show(5000);
        });
        $("#btnSubmit").click(function () {
            if ($("#frmadmission").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfAdmission.php"; // the script where you handle the form input.
//                if (document.getElementById('mediumEnglish').checked) //for radio button
//                {
//                    var medium_type = 'English';
//                }
//                else {
//                    medium_type = 'Hindi';
//                }
                var medium_type = 'HindiPlusEnglish';
                var selddlAadhar = $("#ddlAadhar").val();
                if (selddlAadhar == 'y') {
                    var formlname = $("#txtlname").val();
                    var formdob = $("#dob").val();
//                    if(formlname != aadharname && formdob != aadhardob){
//                        return false;
//                    }
                }

                if (document.getElementById('genderMale').checked) //for radio button
                {
                    var gender_type = 'Male';
                } else {
                    gender_type = 'Female';
                }
                if (document.getElementById('mstatusSingle').checked) //for radio button
                {
                    var marital_type = 'Single';
                } else {
                    marital_type = 'Married';
                }
                if (document.getElementById('physicallyChStYes').checked) //for radio button
                {
                    var physical_status = 'Yes';
                } else {
                    physical_status = 'No';
                }


                var selMenu = document.getElementById('ddlLearnerType');
                //var txtField = document.getElementById('txtEmpId');

                if (selMenu.value == '31' || selMenu.value == '32' || selMenu.value == '33' || selMenu.value == '34') {
                    var txtFields = $('#txtEmpId').val();
                    if (txtFields == '') {
                        //alert("Please Fill Employee ID");
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Fill Employee ID</span></div>");
                        return false;
                    }
                }
				                if (aadharno!=txtaadharno.value) //for radio button
                {                    
                     $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Verified Aadhar no must be same as entered Aadhar no.</span></div>");
                        return false;
                }
                var lphoto = $('#txtLearnercode').val();
                var lsign = $('#txtLearnercode').val();
                var lscan = $('#txtLearnercode').val();
                var dob = $('#dob').val();
                //var CourseCode = $('#ddlBatch').val();
                var fee = $('#txtCourseFee').val();
                var installmode = $('#txtInstallMode').val();
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&lname=" + txtlname.value + "&learnercode=" + txtLearnercode.value + "&parent=" + txtfname.value +
                            "&idproof=" + ddlidproof.value + "&coursecode=" + CourseCode + "&batchcode=" + BatchCode + "&fee=" + fee + "&install=" + installmode +
                            "&idno=" + aadharno + "&dob=" + dob + "&isaadhar=" + ddlAadhar.value +
                            "&mothertongue=" + ddlmotherTongue.value + "&medium=" + medium_type +
                            "&gender=" + gender_type + "&marital_type=" + marital_type +
                            "&district=" + ddlDistrict.value + "&tehsil=" + ddlTehsil.value +
                            "&address=" + txtAddress.value + "&pincode=" + txtPIN.value +
                            "&resiph=" + txtResiPh.value + "&mobile=" + txtmobile.value +
                            "&qualification=" + ddlQualification.value + "&learnertype=" + ddlLearnerType.value +
                            "&physicallychallenged=" + physical_status + "&GPFno=" + txtEmpId.value +
                            "&email=" + txtemail.value + "&lphoto=" + lphoto +
                            "&lsign=" + lsign + "&lscan=" + lscan + "&aadharname=" + aadharname + "&aadhardob=" + aadhardob + ""; // serializes the form's elements.
                    //alert(data);
                } else
                {
                    data = "action=UPDATE&lname=" + txtlname.value + "&parent=" + txtfname.value + "&dob=" + dob + "&lphoto=" + lphoto + "&lsign=" + lsign + "&Code= " + txtAdmissionCode.value + ""; // serializes the form's elements.
                }
                //alert(data);
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        var result = data.trim();
                        if (result == SuccessfullyInsert || result == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmadmission.php';
                            }, 3000);
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                        }
                    }
                });
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        // statement block end for submit click
    });

</script>

<!--<script src="scripts/signupload.js"></script>-->
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmadmission_validation.js" type="text/javascript"></script>	
</html>