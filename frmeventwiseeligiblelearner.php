<?php
$title="Eligible Learner Report";
include ('header.php'); 
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_Code'] == '8550') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}	
?>
<div style="min-height:430px !important;max-height:1500px !important;">
	 <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Eligible Learner Report</div>
                <div class="panel-body">
				   <form name="frmeligiblelearnerReport" id="frmeligiblelearnerReport" class="form-inline" role="form" enctype="multipart/form-data">
					<div class="container">
						<div class="container">						
							<div id="response"></div>
						</div>
							<div id="errorBox"></div>
								<div class="col-sm-6 form-group"> 
									<label for="edistrict">Event Name:</label>
									<select id="ddlEvent" name="ddlEvent" class="form-control" required="true">
									 
									</select>    
								</div> 
					
								

								<div class="col-sm-4 form-group">                                  
									<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show Details" style="margin-top:25px;display:none;"/>    
								</div>
                    </div>				
					
                         <div id="grid" style="margin-top:5px;"> </div> 
						    <div class="container">
								<iframe id="testdoc" src="" style="width: 100%;height: 500px;border: none; display: none;"></iframe>
							</div>                    
                 </div>
            </div>   
        </div>
	</form>
    </div>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>                
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
			
		/* funtion for Fillevent */	
		function FillEvent() {
            $.ajax({
                type: "post",
                url: "common/cfEventMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlEvent").html(data);
                }
            });
        }
        FillEvent();

	$("#ddlEvent").change(function () {
		var eventid = $(this).val();
		if(eventid==''){
				$('#btnSubmit').hide();
			}else{
				$('#btnSubmit').show(1000);
			}	
    });

		/* funtion for Grid  */	

		$("#btnSubmit").click(function () 
		{								
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			    var url = "common/cfeligiblelearnerReport.php"; // the script where you handle the form input.
				var data;
				$.ajax({
						type: "post",
						url: "common/cfEligibleLearnerReport.php",
						data: "action=SHOW&Event=" + ddlEvent.value + "",
						success: function (data)
						{
							var data = data.trim();
							$('#response').empty();
							if(data=='blank'){
								$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Select Event.</span></div>");
							}else{
								//window.open(data, '_blank');
								$("#testdoc").attr('src', data);
							}		
						}
				});			
			return false; // avoid to execute the actual submit of the form.
		});
});
    </script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
</html>