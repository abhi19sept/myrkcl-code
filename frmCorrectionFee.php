<?php
$title="Name Correction Payment";
include ('header.php'); 
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
if($_SESSION['User_UserRoll'] == 19) {
    echo "<script>window.location.href = 'frmLearnerCorrectionFee.php';</script>";
    exit;
}
?>
<div style="min-height:430px !important;max-height:auto !important;">	
	<div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:20px;">
            <div class="panel-heading">Correction Application  Form  Report</div>
                <div class="panel-body">
				 <form name="frmcorrectionfeepayment" id="frmcorrectionfeepayment" class="form-inline" role="form" enctype="multipart/form-data">
					<div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
							
							<div class="col-sm-4 form-group">     
										<label for="correction">Select Correction Name:</label>
										<select id="ddlCorrection" name="ddlCorrection" class="form-control">
										
										</select>
							</div> 
						<div>
						
						<div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Payment Mode:</label>
                        <select id="paymode" name="paymode" class="form-control" onchange="toggle_visibility1('online_mode');">

                        </select>									
                    </div> 
                </div>
				<input type="hidden" name="amounts" id="amounts" class="form-control"/>
                <input type="hidden" name="txtapplicationfor" id="txtapplicationfor" class="form-control"/>
                
				</div>
					</div>
                         <div id="gird" style="margin-top:10px;"> </div>                   
                 
				 <div class="container" id="submit" style="display:none;">
									<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
								</div>
				</div>
            </div>   
        </div>
    </form>
	</div>
   
	<form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
        <input type="hidden" id="txnid" name="txnid">
    </form>

  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>

<script type="text/javascript">
        $(document).ready(function () {
			
			function FillCorrectionName() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=FILL",
                    success: function (data) {
						//alert(data);
                        $("#ddlCorrection").html(data);
						 
                    }
                });
            }
            FillCorrectionName();
			
			$("#ddlCorrection").change(function(){				
				var DocCode = $('#ddlCorrection').val();
                showpaymentmode(DocCode);
                GetApplicationName(DocCode);
				$("#gird").html('');
                $("#submit").hide();
                $("#paymode").html('');
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=Fee&codes=" + DocCode + "",
                    success: function (data) {  
					//alert(data);
                    amounts.value = data;				
                    }
                });
			});

			function GetApplicationName(val) {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=GetApplicationName&for=" + val + "",
                    success: function (data) {
						//alert(data);
						txtapplicationfor.value = data;
                        //$("#txtapplicationfor").html(data);
						 
                    }
                });
            }
			
			function showpaymentmode(val) {
				//alert(val);
                $("#gird").html('');
                $("#submit").hide();
                $("#paymode").html('');
				$.ajax({
					type: "post",
					url: "common/cfEvents.php",
					data: "action=SHOWCorrectionPay&code=" + val + "",
					success: function (data) {
						//alert(data);
						$("#paymode").html(data);							
					}
				});
			}
		
    		$("#paymode").change(function () {
    			showData(txtapplicationfor.value, ddlCorrection.value, paymode.value);
    		});

            function showData(val,val1,val2) {
                $("#gird").html('');
                $("#submit").hide();
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=SHOW&application=" + val + "&correctioncode=" + val1 + "&paymode=" + val2 + "",
                    success: function (data) {
                            //alert(data);
                        $("#gird").html(data);
						$('#example').DataTable({
							    scrollY:        400,
								scrollCollapse: true,
								paging:         false
						});
						$("#submit").show();
                    }
                });
            }

            //showData();
			
			$("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfCorrectionFee.php"; // the script where you handle the form input.
                var data;
                var forminput=$("#frmcorrectionfeepayment").serialize();
                //alert(forminput);
				
                if (Mode == 'Add') {
                    data = "action=ADD&" + forminput;
                    $('#txnid').val('');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data) {
                            $('#response').empty();
						//	alert(data);
                            if (data == 0 || data == '') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Please try again, also ensure that, you have selected atleast one checkbox." + "</span></p>");
                            } else if (data != '') {
                               if (data == 'TimeCapErr') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                    alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                            } else {
                                $('#txnid').val(data);
                                $('#frmpostvalue').submit();
                            }
                            }
                        }
                    });
                }

                return false; // avoid to execute the actual submit of the form.
            });

        });

    </script>
</html>