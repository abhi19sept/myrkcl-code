<?php
$title = "Eligible Learners List ( RS-CIT Exam Event)";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '7') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}	
?>
<div style="min-height:430px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">List of all eligible learner's of selected exam event</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                    <div class="container">
                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div>
						
						<div class="col-sm-4 form-group">                                  
									<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show Details" style="margin-top:25px;display:none;"/>    
						</div>								
					</div>

                    <div id="grid" name="grid" style="margin-top:35px;"> </div> 


            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>              
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
			
		/* funtion for Fillevent */	
		        function FillEvent() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfEligibleLearnerWithMobile.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlExamEvent").html(data);
                }
            });
        }
        FillEvent();

	$("#ddlExamEvent").change(function () {
		var eventid = $(this).val();
		if(eventid==''){
				$('#btnSubmit').hide();
			}else{
				$('#btnSubmit').show(1000);
			}	
    });

		/* funtion for Grid  */	

		$("#btnSubmit").click(function () 
		{								
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			    //var url = "common/cfEligibleLearnerWithMobile.php"; // the script where you handle the form input.
				var data;
				$.ajax({
						type: "post",
						url: "common/cfEligibleLearnerWithMobile.php",
						data: "action=SHOW&Event=" + ddlExamEvent.value + "",
						success: function (data)
						{
							$('#response').empty();
							if(data=='blank'){
								$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Select Event.</span></div>");
							}else{
								window.open(data, '_blank');
							}		
						}
				});			
			return false; // avoid to execute the actual submit of the form.
		});
});
    </script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
</html>