<?php
$title="SMS Module";
include ('header.php'); 
include ('root_menu.php'); 

if (isset($_REQUEST['code'])) {
	echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
	echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
	echo "<script>var Code=0</script>";
	echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_UserRoll'] == '14') {	
            ?>
<div style="min-height:500px !important;max-height:2500px !important">
        <div class="container"> 
			<div class="panel panel-primary" style="margin-top:46px !important;">
				<div class="panel-heading btn-info ">RKCL Messaging System</div>
					<div class="panel-body">
						<form name="frmgroupsmsmodule" id="frmgroupsmsmodule" class="form-inline"> 
					
						<input type="hidden" id="txtCourse"  class="form-control" value="" name="txtCourse">
						<input type="hidden" id="txtcenter"  class="form-control" value="" name="txtcenter">
						<input type="hidden" id="txttehsilall"  class="form-control" value="" name="txttehsilall">
						<input type="hidden" id="txtcenterall"  class="form-control" value="" name="txtcenterall">
						<input type="hidden" id="txttehsil"  class="form-control" value="" name="txttehsil">
						
						<!-- <p style="display:none;" id="txtcountrsp"  class="form-control" /> -->
						<p style="display:none;" id="txttotrsp"  class="form-control" />
						<div class="row">
						
						<a href="#"><div class="boxone col-md-3 col-xs-12 col-sm-3 " style="background-color:#0072BB"><span><img src="images/message.png" /></span><p>No Of Messages </p><p id="tot"></p></div></a>
						<a href="#"><div class="boxtwo col-md-3 col-xs-12 col-sm-3 " style="background-color:#FF4C3B"><span><img src="images/payment-status.png" /></span><p>Payment Status </p><p id="active"></p></div></a>
						<a href="frmlogmessage.php"> <div class="boxthree col-md-3 col-xs-12 col-sm-3 " style="background-color:#FFD034"><span><img src="images/message-remain.png" /></span><p>Messages Sent</p><p id="yogendra"></p></div></a>
						<a href="#"><div class="boxfour col-md-3 col-xs-12 col-sm-3 " style="background-color:#C6C8CA"><span><img src="images/message-sent.png" /></span><p>Messages Remaining</p><p id="sunil"></p></div></a>
						<a href="frmreqsms.php"> <div class="boxthree col-md-3 col-xs-12 col-sm-3 " style="background-color:#FFD034"><span><img src="images/payment-status.png" /></span><p>Purchase SMS Pack (Thanks)</p><p ></p></div></a>
						</div>
                   
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
							
							<div class="container">
                            <div class="col-sm-4 form-group"> 
                               
                            </div>
							</div>
							
							  <div class="col-sm-4 form-group"  id="non-printable"> 
							  <label for="edistrict">RSP Name :</label>
							   <input type="text" id="txtrspname"  name="txtrspname" class="form-control" value="" >
							  </div>
								  
								  
								<div class="col-sm-4 form-group"  > 
                                <label for="edistrict">District Name<font color="red">*</font>
								:</label>
                                <select id="ddlDistrict" name="ddlDistrict" class="form-control" >
								 
                                </select>    
								</div>
								
								
								<div class="col-sm-4" > 
                                <label for="edistrict">Tehsil Name<font color="red">*</font>
								:</label>
								<button type='button' id='selectalltehsil' > Select all </button>
								
								<button type='button' id='deselectalltehsil' > DeSelect all </button>
                                <select id="ddlTehsil" name="ddlTehsil" class="form-control" multiple="multiple">
								 
                                </select>    
								</div>
								
								
								
								<div class="col-sm-4 "  id="non-printable"> 
                                <label for="ddlCenter">Center Code<font color="red">*</font>
								:</label>
								<button type='button' id='multiselectallrsp' > Select all </button>
								<button type='button' id='multiDeselectrsp' > Deselect all </button>
                                <select id="ddlCenterrsp" name="ddlCenterrsp[]" class="form-control" rows="100" cols="100"   multiple="multiple" onchange="updateTextarea()" >
								 
                                </select>    
								
								</ul>
                            </div>
							
							
							
							<div class="col-sm-4 "  id="learner" style="display:none;"> 
                                <label for="ddlCenter">Learner Code<font color="red">*</font>
								:</label>
								<button type='button' id='MultiselectLearner' > Select all / Unselect All</button>
                                <select id="ddllearner" name="ddllearner[]" class="form-control" rows="100" cols="100"   multiple="multiple" onchange="updateTextarea()" >
								 
                                </select>    
								
								</ul>
                            </div>
							
							</div>
							
							<div class="container">
							
                            <div class="col-sm-10 "> 
                                <label for="ename">Mobile NO:<font color="red">*</font></label>
                               <textarea class="form-control" readonly="true" maxlength="5000" rows="4" cols="100" id="txtmobile" name="txtmobile" placeholder="Mobile" ></textarea>
                            </div>
							<div class="col-sm-1" style='Margin-top:20px;'> 
							 <label for="ename" style='color:red;'> Count:</label>
                                <p style="display:block;color:red; "id="txtcountrsp" />
                            </div>
							</div>	
							
                            
							<div class="container">
							<div class="col-sm-10">     
                                <label for="address">Message:<font color="red">*</font></label>
                                <textarea  class="form-control"  maxlength="160" rows="4" cols="100" id="txtMessage" name="txtMessage" placeholder="Message" ></textarea>

                            </div>
							</div>	
							
							
							
							<div class="container">
							<div id='CharCountLabel1'  class="col-sm-10" style="color:red;"></div>
							
							</div>
					
                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Send" style='margin-left:10px'/>    
                        </div>
						</div>
						</div>
						
						
						
                 </div>
            </div>   
        </div>


    </form>

</div>



</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>


 <script type='text/javascript'>

CharacterCount = function(TextArea,FieldToCount){

	var myField = document.getElementById(TextArea);

	var myLabel = document.getElementById(FieldToCount); 

	if(!myField || !myLabel){return false}; // catches errors

	var MaxChars =  myField.maxLengh;

	if(!MaxChars){MaxChars =  myField.getAttribute('maxlength') ; }; 	if(!MaxChars){return false};

	var remainingChars =   MaxChars - myField.value.length

	myLabel.innerHTML = remainingChars+" Characters Remaining of Maximum "+MaxChars

}

 

setInterval(function(){CharacterCount('txtMessage','CharCountLabel1')},55);

</script>
 <script type="text/javascript">
						var SuccessfullySend = "<?php echo Message::SuccessfullySend ?>";
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
						 
												 
						function FillMessage() 
						{
							$.ajax({
								type: "post",
								url: "common/cfgroupsmsmodule.php",
								data: "action=FILLMSG",
								success: function (data) {
									$("#tech").html(data);
								}
							});
						}
						 FillMessage();
						 
						 
						 function FillRSPstatus() 
							{
								//alert("hello");
								$.ajax({
									type: "post",
									url: "common/cfgroupsmsmodule.php",
									data: "action=FILLRSPSTATUS",
									success: function (data) {
										//alert(data);
										$("#active").html(data);
										
									}
								});
							}
							FillRSPstatus();
						 
						
						
						function FillCounting()
						{
								$.ajax(
								{
								type: "post",
								url: "common/cfcentergroupsms.php",
								data: "action=FILL",
								success: function (data) 
								{
								 //alert(data);
								 data = $.parseJSON(data);
								 //console.log(data);
								 //alert(data.response);
								 document.getElementById("sunil").innerHTML=data.Ramain;
								 document.getElementById("yogendra").innerHTML=data.sent;
								 document.getElementById("tot").innerHTML=data.tot;

								}
								});
						}
						FillCounting();
						
						
						
						function FillRspName()
						{
							$.ajax(
							{
							type: "post",
							url: "common/cfgroupsmsmodule.php",
							data: "action=FILLRSPNAME",
							success: function (data) 
							{
							 //alert(data);
							 //alert(data);
							 //data = $.parseJSON();
							 //console.log(data);
							 //alert(data.response);
							txtrspname.value = data;
							document.getElementById("txtrspname").readOnly = true;
							 

							}
							});
						}
						FillRspName();
						
						
						function FillRSPDistrict() {
						//alert();
						$.ajax({
							type: "post",
							url: "common/cfgroupsmsmodule.php",
							data: "action=FILLRSPDISTRICT",
							success: function (data) {
								$("#ddlDistrict").html(data);
								document.getElementById("ddlCenterrsp").innerHTML='';
								document.getElementById("txtmobile").innerHTML='';
								
							}
						});
						}
						FillRSPDistrict();
						
						
						$("#ddlDistrict").click(function () {
						$('#response').empty();
					    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");	
						var selDistrict = $(this).val();
						gettehsil(selDistrict);
						document.getElementById("response").innerHTML='';
						});
						
						function gettehsil(selDistrict)
						{
							
						//alert(selregion);
						$.ajax({
							url: 'common/cfTehsilMaster.php',
							type: "post",
							data: "action=FILL&values=" + selDistrict + "",
							success: function (data) {
								//alert(data);
								$('#ddlTehsil').html(data);
								$('#ddlCenterrsp').val("");
								document.getElementById("txtmobile").innerHTML='';
								document.getElementById("ddlCenterrsp").innerHTML='';
								document.getElementById("txtcountrsp").innerHTML='';
								
							}
						});
						}
						
						$("#ddlTehsil").click(function () {
						var ddlTehsil = $(this).val(); 
						document.getElementById("txttehsil").value=ddlTehsil;
						//alert(ddlTehsil);
						$.ajax({
							url: 'common/cfgroupsmsmodule.php',
							type: "post",
							data: "action=FILLRSPCENTERS&district=" + ddlDistrict.value + "&tehsil=" + txttehsil.value +  "&Rspcode=" + txtrspname.value + "",
							success: function (data) {
								//alert(data);
								$('#ddlCenterrsp').html(data);
								document.getElementById("txtmobile").innerHTML='';
								document.getElementById("txtcountrsp").innerHTML='';
							}
						});
						});
						
						
						
						$("#ddlTehsil").blur(function () {
						var ddlTehsil = $(this).val(); 
						document.getElementById("txttehsil").value=ddlTehsil;
						//alert(ddlTehsil);
						$.ajax({
							url: 'common/cfgroupsmsmodule.php',
							type: "post",
							data: "action=FILLRSPCENTERS&district=" + ddlDistrict.value + "&tehsil=" + txttehsil.value +  "&Rspcode=" + txtrspname.value + "",
							success: function (data) {
								//alert(data);
								$('#ddlCenterrsp').html(data);
								document.getElementById("txtmobile").innerHTML='';
								document.getElementById("txtcountrsp").innerHTML='';
							}
						});
						});
						
						
						
						
						$("#ddlCenterrsp").click(function()
						{
						var ddlCenterrsp = $(this).val(); 
						//alert(ddlCenterrsp);
						$.ajax({
							  url: 'common/cfgroupsmsmodule.php',
							  type: "post",
							  data: "action=FILLMOBILERSP&center=" + ddlCenterrsp +  "",
							  success: function(data)
							  {
								//alert(data);
									 data = $.parseJSON(data);
									 //alert(data.countmob);
									 document.getElementById("txtcountrsp").innerHTML=data.countall;
									 document.getElementById("txtmobile").innerHTML=data.moball;
								document.getElementById("txtmobile").readOnly = true;
							  }
							  });
						 });
						 
						 
						 
						 
						 
						 $("#ddlCenterrsp").blur(function()
						{
						var ddlCenterrsp = $(this).val(); 
						//alert(ddlCenterrsp);
						$.ajax({
							  url: 'common/cfgroupsmsmodule.php',
							  type: "post",
							  data: "action=FILLMOBILERSP&center=" + ddlCenterrsp +  "",
							  success: function(data)
							  {
								//alert(data);
									 data = $.parseJSON(data);
									 //alert(data.countmob);
									 document.getElementById("txtcountrsp").innerHTML=data.countall;
									 document.getElementById("txtmobile").innerHTML=data.moball;
								document.getElementById("txtmobile").readOnly = true;
							  }
							  });
						 });
						 
						
						

							 
							
						
						
						
						
						
						
						
						$("#selectalltehsil").click(function()
						{

					    $('#response').empty();
					    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");	
						$('select#ddlTehsil option').attr("selected",true);
							
						//alert(ddlDistrict.value);
						//alert(ddlTehsil);
						
						getdata();
					
						$.ajax({
							url: 'common/cfgroupsmsmodule.php',
							type: "post",
							data: "action=FILLALLTEHSILCENTERS&district=" + ddlDistrict.value +  "&Rspcode=" + txtrspname.value +  "&tehsil=" + txttehsilall.value +   "",
							success: function (data) {
								//alert(data);
								$('#ddlCenterrsp').html(data);
								document.getElementById("txtmobile").innerHTML='';
								document.getElementById("txtcountrsp").innerHTML='';
								document.getElementById("response").innerHTML='';
							}
						});
						
						
						});
						
						
						function getdata()
						{
						var str_val = "";
						
						$('#ddlTehsil :selected').each(function(i, sel){ 
						
						if($(sel).val() !=0){
							
							 str_val += $(sel).val()+",";
						}
						
						});
						
						str_val = str_val.substring(0, str_val.length - 1);
						//document.getElementById("txttehsilall").value=str_val;
						//alert(str_val);
						
						$("#txttehsilall").val(str_val);
						}
						
						
						$('#deselectalltehsil').click(function() {
						
										
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						 document.getElementById("txtcountrsp").innerHTML='';
						 document.getElementById("ddlCenterrsp").innerHTML='';
						 
						 gettehsil($("#ddlDistrict").val());
						 document.getElementById("response").innerHTML='';	
						 	
						});
						
						
						
						///// WORK HERE //////////////////////////////////////////////////////////////
						
						
						
						$("#multiselectallrsp").click(function()
						{	
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
					    $('select#ddlCenterrsp option').attr("selected","selected");
						//alert(ddlDistrict.value);
						//alert(ddlTehsil);
						
						var cen_val = "";
						
						$('#ddlCenterrsp :selected').each(function(i, cen){ 
						
						if($(cen).val() !=0){
							
							cen_val += $(cen).val()+",";
						}
							
								
							
						});
						
						cen_val = cen_val.substring(0, cen_val.length - 1);
						//document.getElementById("txttehsilall").value=str_val;
						//alert(str_val);
						
						$("#txtcenterall").val(cen_val); 
							
							$('select#ddlCenterrsp option').attr("selected","selected");
							$.ajax({
								  url: 'common/cfgroupsmsmodule.php',
								  type: "post",
								  data: "action=FILLRSPCENTERMOBILE&center=" + txtcenterall.value + "",
								  success: function(data)
								  {
									//alert(data);
									//alert(data);
									 data = $.parseJSON(data);
									 //alert(data.countmob);
									 document.getElementById("txtcountrsp").innerHTML=data.countmob;
									 document.getElementById("txtmobile").innerHTML=data.mobile;
								     document.getElementById("txtmobile").readOnly = true;
									 document.getElementById("response").innerHTML='';	
								  }
								   });
						});
						
						
						
						
						
						$('#multiDeselectrsp').click(function() {
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
						 $('select#ddlCenterrsp').removeAttr("selected"); 
						  document.getElementById("txtmobile").innerHTML='';
						  document.getElementById("txtcountrsp").innerHTML='';
						  
						  $("#selectalltehsil").trigger("click");
						  document.getElementById("response").innerHTML='';
						
						});
						 
						////////////////////// WORK END HERE ////////////////////////////////////
						
						
						$("#btnSubmit").click(function () {
							
							if ($("#frmgroupsmsmodule").valid())
							{	
								var Status = document.getElementById('active').innerHTML;
								var Remain = document.getElementById('sunil').innerHTML;
								var sent = document.getElementById('yogendra').innerHTML;
								var tot = document.getElementById('txttotrsp').innerHTML;
								var tempsel = document.getElementById('txtcountrsp').innerHTML;

								
								var totalsend = parseInt(tempsel);
								var totalremain = parseInt(Remain);
							    //alert(totalremain);
								//alert(Status);
							if((totalsend<=totalremain) && (Status == 'Active')) 
							{	
							    
								$('#response').empty();
								$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");	
								var url = "common/cfgroupsmsmodule.php"; // the script where you handle the form input.
							
								var data;
								//var forminput=$(frmsmsmodule).serialize();
								if (Mode == 'Add')
								{
									data = "action=ADD&txtmobile="+txtmobile.value+"&txtMessage="+txtMessage.value+"";
								}
								else
								{
									
									data = "action=UPDATE&code=" + OrganizationCode +"&" + forminput;
									//data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
								}
								$.ajax({
									type: "POST",
									url: url,
									data: data,
									success: function (data)
									{
										if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
										{
											$('#response').empty();
											$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
											 window.setTimeout(function () {
												window.location.href = "frmgroupsmsmodule.php";
											}, 1000);
											Mode = "Add";
											resetForm("frmgroupsmsmodule");
										}
										else
										{
											$('#response').empty();
											$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
										}
										showData();


									}
								});
									
									
								}
								else 
								{
									
									$('#response').empty();
								    $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Sorry, You are Not having Sufficient Message Pakckage</span></p>");
								
								}
							}
							return false; // avoid to execute the actual submit of the form.
							});
											
						function resetForm(formid) 
						{
							$(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
						}

					});

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmgroupsms_validation.js"></script>
		
		
		   <script type="text/javascript">
      
    </script>
<style>
.error {
	color: #D95C5C!important;
}
.panel-primary > .panel-heading {
    background-color: #5bc0de !important;
}
.col-md-3 {
    width: 20%;
}
</style>

</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>