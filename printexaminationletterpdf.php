<?php
$title="Exam Event ";
include ('header.php'); 
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
ini_set("max_input_vars", 10000);
 
set_time_limit(0);

//include ('root_menu.php');  
//include 'common/commonFunction.php';
require_once("common/cfexaminationpdf.php");
require 'functions/learner_photo_signs.php';

//session_start();
		require_once 'mpdf60/mpdf.php';
		$data = array();
		$requestArgs = $_REQUEST;
		$examEventId = (!empty($requestArgs["code"])) ? $requestArgs["code"] : 1221;
		$requestArgs['code'] = $examEventId;
		if ($requestArgs['act'] == 'byupdatepics') {
			$centers = uploadPhotoSigns($examEventId);
		} else {
			$centers = getCenters($requestArgs);
		}
		if(! mysqli_num_rows($centers)) {
			die('No Record Found!!');
		}
			
		$learnerCode = !empty($requestArgs['learnerCode']) ? $requestArgs['learnerCode'] : '';
		
		$path = $general->getDocRootDir() . '/upload/permissionletter';
		$pathBkp = $general->getDocRootDir() . '/upload/permissionletter_' . date("d-m-Y_h_i_s_a") . '_bkp';
		//rename($path, $pathBkp);

		makeDir($path);
		
		while($centerRow = mysqli_fetch_array($centers)) {
		
			$filepath = $general->getDocRootDir() . '/upload/permissionletter/' . $centerRow['centercode'] . '_permissionletter.pdf';
		//	if (file_exists($filepath)) continue;

            $data = getdata($examEventId, $centerRow['centercode']);
	
			$_Count = 1;
			$html = '';

			$examDate = $centerRow['examdate'] . ' , Sunday';

			while($row= mysqli_fetch_array($data))
				
			{

				mysqli_set_charset('utf8');
				
				$photo = getPath('photo', $row);
				$sign = getPath('sign', $row);

			$html .='<center>
			<DIV id="page_1"  style="background-image: url(images/422900248x1.jpg);	background-repeat: no-repeat !imporatant;height:970px !imporatant;width:700px !important;padding:10px;margin-top:20px;">
			<DIV id="dimg1">
			</DIV>

			<P align="center" style="line-height:0.5px; font-size:15px"><b>VARDHMAN MAHAVEER OPEN UNIVERSITY, KOTA</b></P>
			<P align="center"  style="line-height:0.5px;font-size:15px"><NOBR><b>RS-CIT</b></NOBR> <b>EXAMINATION, ' . strtoupper($centerRow['eventname']) . '</b></P>
			<P align="center" style="line-height:0.5px;font-size:15px"><NOBR><b>PERMISSION LETTER</b></P>
			<P style="margin-top:15px;height:20px;width:792px;font-family:Frutiger"><IMG src="' . $general->getDocRootDir() . '/images/Capture1.jpg" style="image-rendering: pixelated;" ></P>

			<TABLE cellpadding=0 cellspacing=0 class="t0" style="width:700px;border:1px solid #333;">
			<TR>
				<TD class="tr0 td0" width="350" style="border-right:1px solid ;border-bottom: 1px solid;padding-left:5px"><P class="p4 ft1" style="font-size:17px"><b>Learner Code : </b>'.$row['learnercode'].' </P></TD>
				<TD class="tr0 td1" width="350" style="padding-left:5px;border-bottom: 1px solid;"><P class="p5 ft2" style="font-size:17px"><b>Exam Center / Venue</b></P></TD>
			</TR>

			<TR>
				<TD rowspan=2 width="350" class="tr2 td4" style="border-right:1px solid ;font-size:15px;padding-left:5px;"><P class="p7 ft4"><b>Date of Examination: </b> ' . $examDate . '  </P></TD>
				<TD class="tr3 td5" width="350" style="border:none !important;font-size:15px;padding-left:5px;"><P class="p8 ft5" style="font-size:12px;"><b>Center Code</b>: '.$row['examcentercode'].'</P></TD>
			</TR>
			<TR>
				<TD class="tr4 td5" style="font-size:15px;padding-left:5px;"><P class="p8 ft6" style="font-size:12px;"><b>Center Name</b>: '.preg_replace('/[^A-Za-z0-9\. -]/', '',$row['examcentername']).'</P></TD>
			</TR>
			<TR>
				<TD class="tr5 td4" width="350" style="border-right:1px solid ;padding-left:5px;"><P class="p6 ft7">&nbsp;</P></TD>
				<TD class="tr5 td5" width="350" style="border:none !important;font-size:12px;padding-left:5px;"><P class="p8 ft8" ><b>Address</b>: '.preg_replace('/[^A-Za-z0-9\. -]/', '',$row['examcenteraddress']).'</P></TD>
			</TR>
			<TR>
				<TD class="tr6 td4" width="350" style="border-right:1px solid ;font-size:15px;padding-left:5px;"><P class="p7 ft2"><b>Time of Examination</b>: ' . $centerRow['examtime'] . '</P></TD>
				<TD class="tr6 td5"  width="350" style="border:none !important;font-size:15px;padding-left:5px;"><P class="p8 ft6"  style="font-size:12px;"><b>Tehsil</b>:  '.$row['examcentertehsil'].', <b> District</b>:  '.$row['examcenterdistrict'].'</P></TD>
			</TR>
			<TR>
				<TD class="tr7 td2" width="350" style="border-right:1px solid ;"><P class="p6 ft9">&nbsp;</P></TD>
				<TD class="tr7 td3"  width="350" style="border:none !important;"><P class="p6 ft9">&nbsp;</P></TD>
			</TR>
			</TABLE>
			<TABLE cellpadding=0 cellspacing=0 class="t1" style="width:700px !important">

			<TR>
				<TD colspan="1" class="t1" width="200"></TD>
				<TD colspan="2" class="t2" width="350"><P >&nbsp;</P></TD>
				<TD colspan="3" class="t3" style="padding-top:10px;"><P style="font-size:17px !important;">Candidate Photo</p></TD>
			</TR>

			<TR>
				
				<TD colspan="3" class="t1"  width="200" style="padding-left:30px;height:25px"><P style="font-size:17px  !important; font-family: serif !important;height:25px !important;" > 1.)  ROLL No: '.$row['rollno'].'</P></TD>
				
				
				
				<TD colspan="3" class="t3" rowspan="20" style="margin"><IMG src="'.$photo.'"   style="margin-left:2px;width:129px;height:150px;margin-top:10px;border:1px solid  "> </TD>
				
			</TR>
			<TR>
				
				<TD colspan="3" class="t1" style="padding-left:30px;height:25px" ><P class="p6 ft11" style="font-size:17px  !important; font-family: serif !important;height:25px !important;" >2)  Name of the Candidate: '.$row['learnername'].' </P></TD>
				
				<TD colspan="3" class="t3" ></TD>
			</TR>

			<TR>
				
				<TD colspan="3" class="t1" style="padding-left:30px;height:25px"><P class="p6 ft11" style="font-size:17px  !important; font-family: serif !important;height:25px !important;" >3)  Father /Husband Name: '.$row['fathername'].'</P></TD>
				
				<TD colspan="3" class="t3" ></TD>
			</TR>
			<TR>
				
				<TD colspan="3" class="t1" style="padding-left:30px;height:25px !important;" ><P class="p6 ft4" style="font-size:17px  !important; font-family: serif !important;height:25px !important;">4)  Date of Birth: '.$row['dob'].'</P></TD>
				
				<TD colspan="3" class="t3" ></TD>
				
				
			</TR>

			<TR>
				<TD colspan="1"  width="200"></TD>
				<TD colspan="2" width="350"><P >&nbsp;</P></TD>
				<TD colspan="3"><P style="font-size:15px !important;"></p></TD>
			</TR>
			<TR>
				
				<TD colspan="1" width="350" align="left" style="padding-left:30px;"><IMG src="images/sign.png" style="border:1px solid #333; height:40px; width:140px;"></TD>
				<TD colspan="2" width="250" align="right"></TD>
				<TD colspan="3"></TD>
				
			</TR>

			<TR>
				
				<TD colspan="1" width="350" align="left" style="padding-left:30px;"><P class="p6 ft4" style="font-size:15px !important;">Controller of Examination</b></P></TD>
				<TD colspan="2" width="250" align="right"><img src="'.$sign.'" style="border:1px solid #333; height:40px; width:140px;margin-right:20px"></TD>
				<TD colspan="3"></TD>
				
				
			</TR>


			<TR>
				
				<TD colspan="1" width="400" style="padding-left:30px;"><P class="p6 ft4" style="font-size:15px !important;">VARDHMAN MAHAVEER OPEN UNIVERSITY, KOTA</b></P></TD>
				<TD colspan="2" width="250" align="right" style="padding-right:20px;"><P class="p6 ft4" style="font-size:15px ;margin-right:20px;">Candidate`s Signature</b></P></TD>
				<TD colspan="3"></TD>
				
			</TR>
			</TABLE>

			<IMG src="' . $general->getDocRootDir() . '/images/instruction_new.jpg" style="width:100% !important;height:500px !important;margin-top:30px">
			</DIV> 
			</center>';
			$_Count++; 

       
		    }
           
		   $path = $general->getDocRootDir() . '/upload/permissionletter/' . $centerRow['centercode'] . '_permissionletter.pdf';
		
		   //die();
		   $mpdf = new mPDF('c','A4','','' ,12, 12, 12, 12, 12, 12);

				$mpdf->SetDisplayMode('fullpage');

				$mpdf->list_indent_first_level = 0;

				$mpdf->WriteHTML($stylesheet,1);
				$mpdf->WriteHTML($html, 2);
				$mpdf->Output($path, 'F');
				 
			
	}

function getPath($type, $row) {
	global $general;
	$batchname = trim(ucwords($row['batchname']));
    $coursename = $row['coursename'];
    $isGovtEmp = stripos($coursename, 'gov');
    if ($isGovtEmp) {
        $batchname = $batchname . '_Gov';
    } else {
        $isWoman = stripos($coursename, 'women');
        if ($isWoman) {
            $batchname = $batchname . '_Women';
        } else {
            $isMadarsa = stripos($coursename, 'madar');
            if ($isMadarsa) {
                $batchname = $batchname . '_Madarsa';
            }
        }
    }

    $batchname = str_replace(' ', '_', $batchname);
    $dirPath = '/upload/admission' . $type . '/' . $row['learnercode'] . '_' . $type . '.png';

    $imgPath = $general->getDocRootDir() . $dirPath;
	$imgPath = str_replace('//', '/', $imgPath);

    $dirPathJpg = str_replace('.png', '.jpg', $dirPath);
    $imgPathJpg = str_replace('.png', '.jpg', $imgPath);

    $imgBatchPath = $general->getDocRootDir() . '/upload/admission' . $type . '/' . $batchname . '/' . $row['learnercode'] . '_' . $type . '.png';
	$imgBatchPath = str_replace('//', '/', $imgBatchPath);
    $imgBatchPathJpg = str_replace('.png', '.jpg', $imgBatchPath);

    $path = '';
    if (file_exists($imgBatchPath)) {
    	$path = $imgBatchPath;
    } elseif (file_exists($imgBatchPathJpg)) {
    	$path = $imgBatchPathJpg;
    } elseif (file_exists($imgPath)) {
    	$path = $imgPath;
    } elseif (file_exists($imgPathJpg)) {
    	$path = $imgPathJpg;
    } else {
    	/*$imgPath = 'http://' . $_SERVER['HTTP_HOST'] . $dirPath;
    	if(@getimagesize($imgPath)) {
    		$path = $imgPath;
    	} else {
    		$path = 'http://' . $_SERVER['HTTP_HOST'] . $dirPathJpg;
    	}*/
    }

    return $path;
}

?>
