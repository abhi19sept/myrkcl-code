<?php
$title = "Eligible Learners List ( RS-CIT Exam Event)";
include ('header.php');
include ('root_menu.php');
echo "<script>var docroot='" . str_replace('frmexamdetails.php', '', $_SERVER['PHP_SELF']) . "'</script>";
?>
<div style="min-height:430px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">List of all eligible learner's of selected exam event</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                    <div class="container">
                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div> 
                    </div>

                    <div id="grid" name="grid" style="margin-top:35px;"> </div> 


            </div>
        </div>   
    </div>
</form>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>




<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillEvent() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfexamdetails.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlExamEvent").html(data);
                }
            });
        }
        FillEvent();

        $('#grid').on('click', '#syncrecords', function (){
            synclearners();
        });

        $('#grid').on('click', '#liveschedule', function (){
            if (confirm("Are you sure to make selected exam schedule as live for all users?")) {
                liveschedule();
            }
        });

        $('#grid').on('change', '#syncscores', function () {
            var type = this.value;
            if (type != '') {
                if (confirm("Are you sure to auto sync scores of " + type + " learners ?")) {
                    syncscores(type);
                }
            }
        });
    });
    $("#ddlExamEvent").change(function () {
        getSchedule();        
    });

    function getSchedule() {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

        $.ajax({
            type: "post",
            url: "common/cfexamdetails.php",
            data: "action=schedule&id=" + ddlExamEvent.value + "",
            success: function (data)
            {
                $('#response').empty();
                $("#grid").html(data);
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ]
                });
            }
        });
    }

    function liveschedule() {
        if (ddlExamEvent.value == '') {
            $('#response').html("<p class='error'><span><img src=images/drop.png width=10px /></span><span>Please select exam event first.</span></p>");
            return;
        }
        $('#liveExamSchedule').html("<span><img src=images/ajax-loader.gif width=10px /></span>Making exam schedule live, please wait for a while...</span>");
        var url = "action=liveschedule&examid=" + ddlExamEvent.value;
        $.ajax({
            type: "post",
            url: "common/cfexamdetails.php",
            data: url,
            success: function (data)
            {
                if (data == '1') {
                    $('#liveExamSchedule').html("Exam schedule has been placed as live.");
                } else {
                    $('#liveExamSchedule').html("Unable to process!");
                }
            }
        });
    }

    function synclearners() {
        if (ddlExamEvent.value == '') {
            $('#response').html("<p class='error'><span><img src=images/drop.png width=10px /></span><span>Please select exam event first.</span></p>");
            return;
        }
        $('#syncingRecords').empty();
        $('#syncingRecords').append("<span><img src=images/ajax-loader.gif width=10px /></span>Syncing Eligible Learners, Please wait ! this will take time aprox. 20 miniutes.....</span>");
        var url = "action=syncEligibleLearners&examid=" + ddlExamEvent.value;
        $.ajax({
            type: "post",
            url: "common/cfexamdetails.php",
            data: url,
            success: function (data)
            {
                if (data == '1') {
                    $('#syncingScore').html("Records Synced.");
                    getSchedule();
                } else {
                    $('#syncingScore').html("Unable to Sync.");
                }
            }
        });
    }

    function syncscores(type) {
        if (ddlExamEvent.value == '') {
            $('#response').html("<p class='error'><span><img src=images/drop.png width=10px /></span><span>Please select exam event first.</span></p>");
            return;
        }
        var url = "syncscores.php?action=syncScores&examid=" + ddlExamEvent.value + "&type=" + type;
        window.open(
          docroot + url,
          '_blank' // <- This is what makes it open in a new window.
        );
    }

</script>
</html>