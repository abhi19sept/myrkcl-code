<?php
$title = "Create MyRKCL Events";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var BatchCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var BatchCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {
    ?>
    <!--<link rel="stylesheet" href="css/datepicker.css">
    <script src="scripts/datepicker.js"></script>-->
    <link rel="stylesheet" href="bootcss/css/bootstrap-datetimepicker.min.css">
    <script src="bootcss/js/moment.min.js"></script>
    <script src="bootcss/js/bootstrap-datetimepicker.min.js"></script>



    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Create MyRKCL Event</div>
            <div class="panel-body">					
                <form name="frmmanageevent" id="frmmanageevent" class="form-inline" role="form" enctype="multipart/form-data">

                    <div class="container">
                        <div class="container">
                            <div class="col-sm-6 form-group" id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-6 form-group"> 
                            <label for="course">Event Category:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-6 form-group"> 

                            <select id="ddlCat" name="ddlCat" class="form-control" onchange="toggle_visibility2('batchmode', 'examevent', 'coursemode', 'correctionevent', 'eoimode')">		

                            </select>
                        </div> 
                    </div>
                    <div class="container">
                        <div class="col-sm-6 form-group"> 
                            <label for="course">Event Name:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-6 form-group"> 

                            <select id="ddlEname" name="ddlEname" class="form-control" onchange="toggle_visibility1('paymode')" >

                            </select>
                        </div> 
                    </div>
                    <div class="container" id="paymode" style="display:none;">
                        <div class="col-sm-6 form-group" > 
                            <label for="install">Payment Mode:<span class="star">*</span></label>

                        </div>
                        <div class="col-sm-6 form-group"> 

                            <select id="ddlPaymode" name="ddlPaymode" class="form-control">

                            </select>
                        </div> 
                    </div>

                    <div class="container" id="correctionevent" style="display:none;">
                        <div class="col-sm-6 form-group" > 
                            <label for="install">Correction Event Name:<span class="star">*</span></label>

                        </div>
                        <div class="col-sm-6 form-group"> 

                            <select id="ddlcorrectionevent" name="ddlcorrectionevent" class="form-control">

                            </select>
                        </div> 
                    </div>

                    <div class="container" id="coursemode">
                        <div class="col-sm-6 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-6 form-group"> 
                            <select id="ddlCourse" name="ddlCourse" class="form-control">									
                            </select>
                            <select id="coursecode" name="coursecode" style="display:none;" class="form-control">									
                            </select>	
                        </div> 
                    </div>

                    <div class="container" id="eoimode" style="display:none;">
                        <div class="col-sm-6 form-group"> 
                            <label for="course">Select EOI:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-6 form-group"> 
                            <select id="ddlEOI" name="ddlEOI" class="form-control">									
                            </select>
                            <select id="eoicode" name="eoicode" style="display:none;" class="form-control">									
                            </select>	
                        </div> 
                    </div>

                    <div class="container" id="examevent" style="display:none;">
                        <div class="col-sm-6 form-group" > 
                            <label for="install">Exam Event Name:<span class="star">*</span></label>

                        </div>
                        <div class="col-sm-6 form-group"> 

                            <select id="ddlexamevent" name="ddlexamevent" class="form-control">

                            </select>
                        </div> 
                    </div>
                    <div class="container" id="batchmode" >
                        <div class="col-sm-6 form-group"> 
                            <label for="course">Select Batch:<span class="star">*</span></label>

                        </div> 
                        <div class="col-sm-6 form-group"> 
                            <select id="ddlBatch" name="ddlBatch" class="form-control">									
                            </select>

                        </div> 
                    </div>
                    <div class="container">
                        <div class="col-sm-6 form-group"> 
                            <label for="sdate">Start Date:<span class="star">*</span></label>

                        </div> 
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" id="txtstartdate" name="txtstartdate" />
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-sm-6 form-group"> 
                            <label for="sdate">End Date:<span class="star">*</span></label>

                        </div> 
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" id="txtenddate" name="txtenddate" />
                        </div>
                    </div>


                    <div class="container">
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Sumbit"/>    
                    </div>

                    <div id="grid" style="margin-top:35px;"> </div>


                </form>
            </div>
        </div>
    </div>
    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>


    <script type="text/javascript">


        $('#txtstartdate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            widgetPositioning: {horizontal: "auto", vertical: "bottom"}
        });
        $('#txtenddate').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            widgetPositioning: {horizontal: "auto", vertical: "bottom"}
        });
    </script>


    <script type="text/javascript">


        function toggle_visibility1(id) {
            var e = document.getElementById(id);

            //alert(e);
            var f = document.getElementById('ddlEname').value;
            //alert(f);
            if (f == "3")
            {
                e.style.display = 'block';
            } else if (f == "5")
            {
                e.style.display = 'block';
            } else if (f == "7")
            {
                e.style.display = 'block';
            } else if (f == "8")
            {
                e.style.display = 'block';
            }
			  else if (f == "15")
            {
                e.style.display = 'block';
            }
            else if (f == "18")
            {
                e.style.display = 'block';
            }
            else if (f == "20")
            {
                e.style.display = 'block';
            }
			else {
                e.style.display = 'none';
            }

        }
        function toggle_visibility2(id1, id2, id3, id4, id5) {
            var e = document.getElementById(id1);

            var g = document.getElementById('ddlCat').value;

            var h = document.getElementById(id2);

            var i = document.getElementById(id3);

            var k = document.getElementById(id4);

            var L = document.getElementById(id5);

            // var h = document.getElementById('ddlexamevent').value;
            //alret (h);
            if (g == "2")
            {
                e.style.display = 'none';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'block';
            } else if (g == "3")
            {
                e.style.display = 'none';
                h.style.display = 'block';
                k.style.display = 'none';
                L.style.display = 'none';
            } else if (g == "4")
            {
                i.style.display = 'none';
                e.style.display = 'none';
                h.style.display = 'none';
                k.style.display = 'block';
                L.style.display = 'none';
            } else
            {
                e.style.display = 'block';
                i.style.display = 'block';
                h.style.display = 'none';
                k.style.display = 'none';
                L.style.display = 'none';
            }
        }

        //-->
    </script> 

    <script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
    </script>

    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $("#txtstartdate, #txtenddate").datetimepicker();
        $("#txtenddate").change(function () {

            var txtstartdate = document.getElementById("txtstartdate").value;
            var txtenddate = document.getElementById("txtenddate").value;
            if ((Date.parse(txtenddate) <= Date.parse(txtstartdate))) {
                alert("End date should be greater than Start date");
                document.getElementById("txtenddate").value = "";
            }
        });
        $(document).ready(function () {
            function FillCategory() {
                $.ajax({
                    type: "post",
                    url: "common/cfManageEvent.php",
                    data: "action=FILLCategory",
                    success: function (data) {
                        $("#ddlCat").html(data);
                    }
                });
            }
            FillCategory();


            $("#ddlCat").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfManageEvent.php",
                    data: "action=FILLEvent&category=" + ddlCat.value + "",
                    success: function (data) {
                        $("#ddlBatch").append("<option value='0'>SELECT</option>");
                        $("#ddlEname").html(data);
                    }
                });
            });
            function FillCourse() {
                $.ajax({
                    type: "post",
                    url: "common/cfCourseMaster.php",
                    data: "action=FILLCourseName",
                    success: function (data) {
                        $("#ddlCourse").html(data);
                    }
                });
            }
            FillCourse();

            $("#ddlEname").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionFee.php",
                    data: "action=FILL",
                    success: function (data) {
                        //alert(data);
                        $("#ddlcorrectionevent").html(data);
                        // showdata();
                    }
                });

            });

            $("#ddlCourse").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLAdmissionBatchcode&values=" + ddlCourse.value + "",
                    success: function (data) {

                        $("#ddlBatch").html(data);
                    }
                });
            });

            $("#ddlCourse").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfApplyEOI.php",
                    data: "action=FILLEOIBYCOURSE&values=" + ddlCourse.value + "",
                    success: function (data) {
                        $("#ddlEOI").html(data);
                    }
                });
            });

            $("#ddlCourse").change(function () {
                $.ajax({
                    type: "post",
                    url: "common/cfReexamAapplication.php",
                    data: "action=FILLEXAMEVENTMANAGE&values=" + ddlCourse.value + "",
                    success: function (data) {
                        $("#ddlexamevent").html(data);
                    }
                });
            });

            function FillPaymentMode() {
                $.ajax({
                    type: "post",
                    url: "common/cfManageEvent.php",
                    data: "action=FILLPaymentMode",
                    success: function (data) {
                        $("#ddlPaymode").html(data);
                    }
                });
            }
            FillPaymentMode();

            function showdata()
            {  // alert("hi");
                var url = "common/cfManageEvent.php";
                var data1;
                //data1 = "action=SHOWDATA&course=" + ddlCourse.value + "";
                data1 = "action=SHOWEVENT&category=" + ddlCat.value + "&eventname=" + ddlEname.value + "";
                // alert(data1);
                $.ajax({
                    type: "post",
                    url: url,
                    data: data1,
                    success: function (data) {
    //                    $('#response').empty();
                        $("#grid").html(data);
                        $('#example').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]
                        });

                    }
                });
            }

            $("#btnSubmit").click(function () {
    //            if ($("#frmmanageevent").valid())
    //            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfManageEvent.php"; // the script where you handle the form input.
                var data;
                if (ddlCat.value == '10' && ddlEname.value == '13') {
                    
                    var learnerattendance = 1;

                }
      //           else if (ddlCat.value == '12' && ddlEname.value == '17') {
                  
      //               var aadharupdateevent = 1;

      //           } else {
      //               var learnerattendance = 0;
					 // var aadharupdateevent = 0;
      //           }
                else if (ddlCat.value == '12') {
                    var coreectionbfrexam=1;
                }
                else if (ddlCat.value == '14') {
                    var rscfacertificatepayment=1;
                }
                 else if (ddlCat.value == '15') {
                    var rscfacertificatepayment=1;
                }
                else{
                    var learnerattendance=0;
                    var coreectionbfrexam=0;
                    var rscfacertificatepayment=0;
                }

                data = "action=ADD&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "&category=" + ddlCat.value + "&eventname=" + ddlEname.value + "&paymode=" + ddlPaymode.value + "&startdate=" + txtstartdate.value + "&enddate=" + txtenddate.value + "&reexemevent=" + ddlexamevent.value + "&correctionevent=" + ddlcorrectionevent.value + "&courseeoi=" + ddlEOI.value + "&learnerattendanceevent=" + learnerattendance + "&coreectionbfrexamevent=" + coreectionbfrexam + 
                    "&rscfacertificatepayment=" + rscfacertificatepayment +"";
                //alert(data);
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) {
                        //alert(data);
                        if (data == "Successfully Inserted" || data == "Successfully Updated")
                        {
                            
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmManageEvent.php';
                            }, 2000);
                        } else
                        {
                            alert("else");
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span> This Event is Already Launched <br><br></div>");
                        }


                    }
                });
                //showdata();

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }
        });
    </script>
    </html>
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>
<!--<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmmanageevent_validation.js" type="text/javascript"></script>	-->