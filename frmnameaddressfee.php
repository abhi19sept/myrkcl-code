<?php
$title = "IT-GK Name Address Change Fee Payment";
include ('header.php');
include ('root_menu.php');

echo "<script>var Mode='Add'</script>";
echo "<script>var PayType= 'Name Address Change Fee Payment' </script>";
echo "<script>var EmptyAreaType='empty'</script>";


if ($_SESSION["User_UserRoll"] == 1 || $_SESSION["User_UserRoll"] == 7) {
    ?>
    <style>
        .marginB15{ margin-bottom:15px;}
    </style>
    <div style="min-height:430px !important;max-height:auto !important;">
        <div class="container"> 			  
            <div class="panel panel-primary" style="margin-top:36px;">
                <div class="panel-heading">IT-GK Name Address Change Fee Payment</div>
                <div class="panel-body">
                    <form name="frmfeepayment" id="frmfeepayment" class="form-inline" role="form" enctype="multipart/form-data">
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
                            <div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="nameaddress">Select Payment For:</label>
                                <select id="ddlnameaddress" name="ddlnameaddress" class="form-control" 				required="required">
                                    <option value=""> Please Select </option>
                                    <option value="1"> Name Change </option>
                                    <option value="2"> Address Change </option>								
                                </select>
                            </div> 

                            <div class="col-sm-4 form-group" style="margin-top:25px;">
                                <label for="nameaddress"></label>
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary"
                                       value="Show Details"/>
                            </div> 	
                        </div>
                        <div id="PreviousName" style="display:none;">	
                            <fieldset style="border: 1px groove #ddd !important;" class="">
                                <div style="background-color: #71baf9;padding: 10px; color: #fff; margin-bottom: 10px;">Previous Organization/Center Details</div>
                                <div class="container">
                                    <div id="errorBox"></div>
                                    <div class="col-sm-3 marginB15">     
                                        <label for="learnercode">Name of Organization/Center:</label>
                                        <input type="text" class="form-control" readonly="true" name="OrgNameOld"
                                               id="OrgNameOld" placeholder="Name of the Organization/Center">
                                    </div>


                                    <div class="col-sm-4 marginB15"> 
                                        <label for="ename">Organization Type:</label>
                                        <input type="text" class="form-control" readonly="true" name="OrgTypeOld"
                                               id="OrgTypeOld" placeholder="Organization Type">     
                                    </div>


                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Organization Area Type:</label>
                                        <input type="text" class="form-control" readonly="true" name="OrgAreaTypeOld"
                                               id="OrgAreaTypeOld"  placeholder="Organization Area Type">
                                    </div>

                                </div> 

                            </fieldset>						  
                            <br>
                        </div>	

                        <div id="CurrentName" style="display:none;">
                            <fieldset style="border: 1px groove #ddd !important;" class="">
                                <div style="background-color: #71baf9;padding: 10px; color: #fff; margin-bottom: 10px;">Current Organization/Center Details</div>
                                <div class="container">

                                    <div class="col-sm-3 marginB15">     
                                        <label for="learnercode">Name of Organization/Center:</label>
                                        <input type="text" class="form-control" readonly="true" name="OrgName"
                                               id="OrgName" placeholder="Name of the Organization/Center">
                                    </div>


                                    <div class="col-sm-4 marginB15"> 
                                        <label for="ename">Organization Type:</label>
                                        <input type="text" class="form-control" readonly="true" name="OrgType"
                                               id="OrgType" placeholder="Organization Type">     
                                    </div>


                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Organization Area Type:</label>
                                        <input type="text" class="form-control" readonly="true" name="OrgAreaType"
                                               id="OrgAreaType"  placeholder="Organization Area Type">
                                    </div>							
                                </div> 
                            </fieldset>						  
                            <br>
                        </div>

                        <div id="CurrentAddressOld" style="display:none;">
                            <fieldset style="border: 1px groove #ddd !important;" class="">
                                <div style="background-color: #71baf9;padding: 10px; color: #fff; margin-bottom: 10px;">Previous Organization/Center Details</div>
                                <div class="container">
                                    <div class="col-sm-3 marginB15">     
                                        <label for="learnercode">Name of Organization/Center:</label>
                                        <input type="text" class="form-control" readonly="true" name="OrgAddName"
                                               id="OrgAddName" placeholder="Name of the Organization/Center">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Organization Area Type:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="OrgAddAreaOld" id="OrgAddAreaOld" placeholder="Organization Area Type">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Organization Address:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="orgaddressOld" id="orgaddressOld" placeholder="Organization Address">
                                    </div>

                                </div>

                                <div id="CurrentAddressUrbanOld" style="display:none;" class="container"> 
                                    <div class="col-sm-3 marginB15">     
                                        <label for="faname">Municipal Type:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="mtypeOld" id="mtypeOld" placeholder="Municipal Type">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Municipal Name:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="mnameOld" id="mnameOld" placeholder="Municipal Name">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Organization Ward No:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="orgwardOld" id="orgwardOld" placeholder="Organization Ward No">
                                    </div>
                                </div>

                                <div id="CurrentAddressRuralOld" style="display:none;" class="container"> 
                                    <div class="col-sm-3 marginB15">     
                                        <label for="faname"> Panchayat:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="panchayatOld" id="panchayatOld" placeholder="Panchayat">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname"> Gram Panchayat:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="gmpanchayatOld" id="gmpanchayatOld" placeholder="Gram Panchayat">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Village:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="villageOld" id="villageOld" placeholder="Village">
                                    </div>
                                </div>
                            </fieldset>						  
                            <br>
                        </div>

                        <div id="CurrentAddressNew" style="display:none;" >
                            <fieldset style="border: 1px groove #ddd !important;" class="">
                                <div style="background-color: #71baf9;padding: 10px; color: #fff; margin-bottom: 10px;">Current Organization/Center Details</div>
                                <div class="container">
                                    <div class="col-sm-3 marginB15">     
                                        <label for="learnercode">Name of Organization/Center:</label>
                                        <input type="text" class="form-control" readonly="true" name="OrgAddNameNew"
                                               id="OrgAddNameNew" placeholder="Name of the Organization/Center">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Organization Area Type:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="OrgAddAreaNew" id="OrgAddAreaNew" placeholder="Organization Area Type">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Organization Address:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="orgaddressNew" id="orgaddressNew" placeholder="Organization Address">
                                    </div>

                                </div>

                                <div id="CurrentAddressUrbanNew" style="display:none;" class="container"> 
                                    <div class="col-sm-3 marginB15">     
                                        <label for="faname">Municipal Type:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="mtypeNew" id="mtypeNew" placeholder="Municipal Type">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Municipal Name:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="mnameNew" id="mnameNew" placeholder="Municipal Name">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Organization Ward No:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="orgwardNew" id="orgwardNew" placeholder="Organization Ward No">
                                    </div>
                                </div>

                                <div id="CurrentAddressRuralNew" style="display:none;" class="container"> 
                                    <div class="col-sm-3 marginB15">     
                                        <label for="faname"> Panchayat:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="panchayatNew" id="panchayatNew" placeholder="Panchayat">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname"> Gram Panchayat:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="gmpanchayatNew" id="gmpanchayatNew" placeholder="Gram Panchayat">
                                    </div>

                                    <div class="col-sm-4 marginB15">     
                                        <label for="faname">Village:</label>
                                        <input type="text" class="form-control" readonly="true" 
                                               name="villageNew" id="villageNew" placeholder="Village">
                                    </div>
                                </div>
                            </fieldset>						  
                            <br>
                        </div>

                        <div id="grid" style="margin-top:35px; width:94%; display:none;">
                            <div class="table-responsive">
                                <div id="example_wrapper" class="dataTables_wrapper no-footer">
                                    <div class="dt-buttons">
                                        <a class="dt-button buttons-copy buttons-html5" tabindex="0" aria-controls="example">
                                            <span>Copy</span>
                                        </a><a class="dt-button buttons-csv buttons-html5" tabindex="0" aria-controls="example"><span>CSV</span></a><a class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example"><span>Excel</span></a><a class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="example"><span>PDF</span></a><a class="dt-button buttons-print" tabindex="0" aria-controls="example"><span>Print</span></a></div><div id="example_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="example"></label></div><table id="example" border="0" cellpedding="0" cellspacing="0" width="100%" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="example_info" style="width: 100%;"><thead><tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label=" S No.: activate to sort column descending" style="width: 49px;"> S No.</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label=" ITGK Code : activate to sort column ascending" style="width: 87px;"> ITGK Code </th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label=" Transaction Number : activate to sort column ascending" style="width: 155px;"> Transaction Number </th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label=" Payment Amount: activate to sort column ascending" style="width: 134px;"> Payment Amount</th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label=" Payment Status : activate to sort column ascending" style="width: 123px;"> Payment Status </th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label=" Payment Type : activate to sort column ascending" style="width: 111px;"> Payment Type </th><th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label=" Payment Date: activate to sort column ascending" style="width: 111px;"> Payment Date</th></tr></thead><tbody><tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No data available in table</td></tr></tbody></table><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 0 to 0 of 0 entries</div><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><a class="paginate_button previous disabled" aria-controls="example" data-dt-idx="0" tabindex="0" id="example_previous">Previous</a><span></span><a class="paginate_button next disabled" aria-controls="example" data-dt-idx="1" tabindex="0" id="example_next">Next</a></div></div></div></div>

                        <div>
                            <input type="hidden" name="amounts" id="amounts"/>
                            <input type="hidden" name="payforchange" id="payforchange"/>
                            <input type="hidden" name="txtareatype" id="txtareatype"/>
                            <input type="hidden" name="refid" id="refid"/>
                        </div>


                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Proceed" style="display:none;"/>    
							<label for="tnc" id="btnVerified" style="margin-top:35px; display:none; color:green; font-size:15px"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;<b>Payment Already Done.</b></label>&nbsp;&nbsp;
                        </div>
                </div>
            </div>   
        </div>
    </form>
    </div>
    <form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
        <input type="hidden" id="txnid" name="txnid">
    </form>

    </body>
    <?php include ('footer.php'); ?>
    <?php include'common/message.php'; ?>


    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
		
		 function Check_Eligiblity() {
            $.ajax({
                type: "post",
                url: "common/cfNameAddressFeePayment.php",
                data: "action=CheckCenter",
                success: function (data) {
                    //alert(data);
                    if (data === '2') {
                        alert("You are Not allowed for this process. Please contact RKCL for more details.");
                        window.location.href = "frmprofileDetails.php";
                    }
                    else {
                    }

                }
            });
        }
        //Check_Eligiblity();

            $("#btnShow").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfNameAddressFeePayment.php",
                    data: "action=GetPayEligibility&payfor=" + ddlnameaddress.value + "",
                    success: function (data) {

                        $('#response').empty();
                        
                        if (data == 1) {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please Select Payment For." + "</span></p>");

                            $('#grid').hide();
                            $('#btnSubmit').hide();
                            $("#PreviousName").hide();
                            $("#CurrentName").hide();

                            $("#CurrentAddressOld").hide();
                            $("#CurrentAddressRuralOld").hide();
                            $("#CurrentAddressNew").hide();
                            $("#CurrentAddressRuralNew").hide();
                            $("#CurrentAddressUrbanOld").hide();
                            $("#CurrentAddressUrbanNew").hide();


                        } else if (data == 3) {
                            $("#PreviousName").hide();
                            $("#CurrentName").hide();

                            $("#CurrentAddressOld").hide();
                            $("#CurrentAddressRuralOld").hide();
                            $("#CurrentAddressNew").hide();
                            $("#CurrentAddressRuralNew").hide();
                            $("#CurrentAddressUrbanOld").hide();
                            $("#CurrentAddressUrbanNew").hide();

                            $('#grid').show();
                            $('#btnSubmit').hide();
                        } else if (data == 4) {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Something has gone wrong. Please try again." + "</span></p>");

                        } else {
                            $('#grid').hide();
                            ChkPayEvent();

                            data = $.parseJSON(data);

                            if (data[0].payfor == 'name') {
                                GetAmount(data[0].payfor);
                                $("#PreviousName").show();
                                $("#CurrentName").show();

                                $("#CurrentAddressOld").hide();
                                $("#CurrentAddressRuralOld").hide();
                                $("#CurrentAddressNew").hide();
                                $("#CurrentAddressRuralNew").hide();
                                $("#CurrentAddressUrbanOld").hide();
                                $("#CurrentAddressUrbanNew").hide();

                                OrgNameOld.value = data[0].orgnameold;
                                OrgTypeOld.value = data[0].orgtypeold;
                                OrgAreaTypeOld.value = data[0].areatypeold;
                                OrgName.value = data[0].orgname;
                                OrgType.value = data[0].orgtype;
                                OrgAreaType.value = data[0].areatype;

                                refid.value = data[0].refid;
                                payforchange.value = data[0].payfor;
                                if (data[0].areatype == '') {
                                    //txtareatype.value = EmptyAreaType;
                                    $('#txtareatype').val(EmptyAreaType);
                                } else {
                                    txtareatype.value = data[0].areatype;
                                }
                            } else if (data[0].payfor == 'address') {
//                                alert(data[0].areatypeold);
//                                alert(data[0].areatype);

                                GetAmount(data[0].payfor);
                                
                                    $("#PreviousName").hide();
                                    $("#CurrentName").hide();
                                    $("#CurrentAddressOld").show();
                                    $("#CurrentAddressNew").show();

                                if (data[0].areatype == 'Rural') {


                                    $("#CurrentAddressRuralNew").show();
                                    OrgAddName.value = data[0].orgnameold;
                                    OrgAddAreaOld.value = data[0].areatypeold;
                                    orgaddressOld.value = data[0].addressold;
                                    //panchayatOld.value = data[0].panchayatold;
                                    //gmpanchayatOld.value = data[0].gramold;
                                    //villageOld.value = data[0].villageold;

                                    OrgAddNameNew.value = data[0].orgname;
                                    OrgAddAreaNew.value = data[0].areatype;
                                    orgaddressNew.value = data[0].address;
                                    panchayatNew.value = data[0].panchayat;
                                    gmpanchayatNew.value = data[0].gram;
                                    villageNew.value = data[0].village;

                                    refid.value = data[0].refid;
                                    payforchange.value = 'address';
                                    txtareatype.value = 'Rural';
                                } if (data[0].areatypeold == 'Rural')  {
                                    //alert(data[0].areatypeold);
//                                    $("#PreviousName").hide();
//                                    $("#CurrentName").hide();

                                    $("#CurrentAddressRuralOld").show();
                                    //$("#CurrentAddressUrbanNew").show();
                                    OrgAddName.value = data[0].orgnameold;
                                    OrgAddAreaOld.value = data[0].areatypeold;
                                    orgaddressOld.value = data[0].addressold;
                                    panchayatOld.value = data[0].panchayatold;
                                    gmpanchayatOld.value = data[0].gramold;
                                    villageOld.value = data[0].villageold;

                                    OrgAddNameNew.value = data[0].orgname;
                                    OrgAddAreaNew.value = data[0].areatype;
                                    orgaddressNew.value = data[0].address;
                                    //mtypeNew.value = data[0].mtype;
                                    //mnameNew.value = data[0].municipal;
                                    //orgwardNew.value = data[0].wardnum;

                                    refid.value = data[0].refid;
                                    payforchange.value = 'address';
                                    txtareatype.value = 'Urban';
                                } if (data[0].areatype == 'Urban')  {
                                    //alert(data[0].areatypeold);
//                                    $("#PreviousName").hide();
//                                    $("#CurrentName").hide();
                                    $("#CurrentAddressUrbanNew").show();
                                    OrgAddName.value = data[0].orgnameold;
                                    OrgAddAreaOld.value = data[0].areatypeold;
                                    orgaddressOld.value = data[0].addressold;
                                    //mtypeOld.value = data[0].mtypeold;
                                    //mnameOld.value = data[0].municipalold;
                                    //orgwardOld.value = data[0].wardnumold;

                                    OrgAddNameNew.value = data[0].orgname;
                                    OrgAddAreaNew.value = data[0].areatype;
                                    orgaddressNew.value = data[0].address;
                                    mtypeNew.value = data[0].mtype;
                                    mnameNew.value = data[0].municipal;
                                    orgwardNew.value = data[0].wardnum;

                                    refid.value = data[0].refid;
                                    payforchange.value = 'address';
                                    txtareatype.value = 'Urban';
                                    
                                } if (data[0].areatypeold == 'Urban')  {
                                    //alert(data[0].areatypeold);
//                                    $("#PreviousName").hide();
//                                    $("#CurrentName").hide();

                                    $("#CurrentAddressUrbanOld").show();
                                    //$("#CurrentAddressUrbanNew").show();
                                    OrgAddName.value = data[0].orgnameold;
                                    OrgAddAreaOld.value = data[0].areatypeold;
                                    orgaddressOld.value = data[0].addressold;
                                    mtypeOld.value = data[0].mtypeold;
                                    mnameOld.value = data[0].municipalold;
                                    orgwardOld.value = data[0].wardnumold;

                                    OrgAddNameNew.value = data[0].orgname;
                                    OrgAddAreaNew.value = data[0].areatype;
                                    orgaddressNew.value = data[0].address;
//                                    mtypeNew.value = data[0].mtype;
//                                    mnameNew.value = data[0].municipal;
//                                    orgwardNew.value = data[0].wardnum;

                                    refid.value = data[0].refid;
                                    payforchange.value = 'address';
                                    txtareatype.value = 'Urban';
                                }

                            }
                        }
                    }
                });
                return false; // avoid to execute the actual submit of the form.
            });


            function GetAmount(val)
            {
                $.ajax({
                    type: "post",
                    url: "common/cfNameAddressFeePayment.php",
                    data: "action=GetPayForAmt&values=" + val + "",
                    success: function (data) {
                        amounts.value = data;
                    }
                });
            }

            function ChkPayEvent()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfNameAddressFeePayment.php",
                    data: "action=ChkPayEvent",
                    success: function (data) {
                      var paystatus=data;
                         
                                if(paystatus == "1"){
                               $('#btnSubmit').show();
							   ChkPaymentStatus();
                            }
                   
                    }
                });
            }
function ChkPaymentStatus()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfNameAddressFeePayment.php",
                    data: "action=ChkPaymentStatus&refid=" + refid.value + "",
                    success: function (data) {
                      var paystatus=data;
                         
                                if(paystatus == "1"){
                               $('#btnSubmit').hide();
                               $('#btnVerified').show();
                            }
                   
                    }
                });
            }
			
            $("#btnSubmit").click(function () {
                //alert("xfsdfse");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $('#btnSubmit').hide();
                var url = "common/cfNameAddressFeePayment.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmfeepayment").serialize();
                //alert(forminput);

                if (Mode == 'Add') {
                    data = "action=ADD&" + forminput;
                    $('#txnid').val('');
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data) {
                            //alert(data);
                            $('#response').empty();
                            if (data == 0 || data == '') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Something went Wrong. Please try again." + "</span></p>");
                            } else if (data != '') {
                                if (data == 'TimeCapErr') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                    alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                            } else {
                                $('#txnid').val(data);
                                $('#frmpostvalue').submit();
                            }
                            }
                        }
                    });
                }

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
    </body>

    </html>
    <?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>