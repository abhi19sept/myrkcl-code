<?php
$title = 'Check Missing Photo Signs';
include ('header.php');
include ('root_menu.php');
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Execute Jobs to <?php echo $title; ?></div>
            <div class="panel-body">
            	<form name="frmexameventmaster" id="frmexameventmaster" action="" class="form-inline">     
            			<div class="container">
            			<div id="response"></div>
            		</div>
                        <div class="container">
							<div id="errorBox"></div>
							<div class="col-sm-4 form-group"  id="non-printable"> 
                                <label for="edistrict">Event Name 
								:</label>
                                <select id="ddlEvent" name="code" class="form-control" >
								 
                                </select>    
                            </div>
                    </div>
                </form>
                <div id="gird"></div> 
            </div>
        </div>
    </div>
</div>


</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
    	FillEvent();
    	$('#gird').on('click', '.exejob', function () {
            executejob(this.id, 1);
        });
    });

    function FillEvent() {
        $.ajax({
            type: "post",
            url: "common/cflearnerdetails.php",
            data: "action=FILLPUBLISHEVENT",
            success: function (data) {
                $("#ddlEvent").html(data);
    			getJobs();
            }
        });
    }

    function setresponse(msg) {
    	$('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>" + msg +"</span></p>");
    }

    function getJobs() {
    	var eventid = $("#ddlEvent").val();
    	if (eventid == '') {
    		setresponse('Event Not Found!!');
    		return;
    	}
        setresponse('Fetching Jobs wait.....');
        var url = "common/cflearnerdetails.php"; // the script where you handle the form input.
        var data = "action=getJobs&eventid=" + eventid; // serializes the form's elements.
        $.ajax({
            type: "post",
            url: url,
            data: data,
            success: function (data) {
                $("#gird").html(data);
                $('#response').empty();
            }
        });
    }

    function executejob(jobid, showqry) {
    	var eventid = $("#ddlEvent").val();
    	if (eventid == '') {
    		setresponse('Event Not Found!!');
    		return;
    	}

    	var url = "copy_learner_photos_for_vmou.php"; // the script where you handle the form input.
        var data = "job=" + jobid + "&examid=" + eventid + "&showqry=" + showqry; // serializes the form's elements.
        $.ajax({
            type: "post",
            url: url,
            data: data,
            success: function (data) {
            	if (showqry == 1) {
                	$("#job"+jobid).html(data);
                	$("#job"+jobid).append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing...</span></p>");
                	executejob(jobid, 0);
            	} else {
            		$('#footer').hide();
            		$("#job"+jobid).html(data);
            	}
            }
        });

    }



</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>