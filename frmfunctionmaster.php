<?php
$title="Child Menu Add";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var FunctionCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {
?>
	 <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Child Menu Master</div>
                <div class="panel-body">
                    <form name="frmFunctionMaster" id="frmFunctionMaster" class="form-inline" role="form" enctype="multipart/form-data">
						<div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>							
							
                            <div class="col-sm-4 form-group">     
                                <label for="order">Function Name:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtFunctionName" id="txtFunctionName" placeholder="Function Name" >
                            </div> 
						</div>
						
						<div class="container">
						<div class="col-md-6 form-group">     
                                <label for="learnercode"> Root Menu Name:</label>
                                <select id="ddlRoot" name="ddlRoot" class="form-control">

                                </select>
                            </div> 
                        </div>  
						
						<div class="container">
						<div class="col-md-6 form-group">     
                                <label for="learnercode"> Parent Menu Name:</label>
                                <select id="ddlParent" name="ddlParent" class="form-control">

                                </select>
                            </div> 
                        </div> 

						<div class="container">
                            <div class="col-sm-8 form-group">     
                                <label for="order">Function URL:</label>
                                <input type="text" class="form-control" name="txtFunctionURL" id="txtFunctionURL" placeholder="Function URL" >
                            </div>
						</div>   

						<div class="container">
                            <div class="col-sm-8 form-group">     
                                <label for="order">Display Order:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtDisplayOrder" id="txtDisplayOrder" placeholder="Display Order" >
                            </div>
                        </div>            	
                     
						<div class="container">
                            <div class="col-sm-10 form-group"> 
                                <label for="status">Function Status:</label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control">
                                          
                                </select>
                            </div>                            
                        </div>

                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>                  

                         <div id="gird" style="margin-top:35px;"> </div>                   
                 </div>
            </div>   
        </div>
    </form>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>                
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
            
             function FillRoot() {
                $.ajax({
                    type: "post",
                    url: "common/cfRootMenu.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlRoot").html(data);
                    }
                });
            }

            FillRoot();
            
            function FillParent(rootmenu) {
                $.ajax({
                    type: "post",
                    url: "common/cfParentFunctionMaster.php",
                    data: "action=FILL&values=" + rootmenu + "",
                    success: function (data) {
                        $("#ddlParent").html(data);
                    }
                });
            }

            $("#ddlRoot").change(function(){
                
                FillParent(this.value);
            });
            
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfFunctionMaster.php",
                    data: "action=DELETE&values=" + FunctionCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href="frmfunctionmaster.php";
                            }, 3000);
                            Mode="Add";
                            resetForm("frmfunctionmaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfFunctionMaster.php",
                    data: "action=EDIT&values=" + FunctionCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Status']);
                        data = $.parseJSON(data);
                        txtFunctionName.value = data[0].FunctionName;
                        txtFunctionURL.value=data[0].URL;
                        ddlRoot.value=data[0].Root;
                        ddlParent.value=data[0].Parent;
                        ddlStatus.value = data[0].Status;
                        txtDisplayOrder.value=data[0].Display;
                        
                        
                    }
                });
            }

            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfFunctionMaster.php",
                    data: "action=SHOW",
                    success: function (data) {
                            //alert(data);
                        $("#gird").html(data);

                    }
                });
            }

            showData();


            $("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfFunctionMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtFunctionName.value + "&URL=" + txtFunctionURL.value + "&parent=" + ddlParent.value + "&status=" + ddlStatus.value + "&display=" + txtDisplayOrder.value +""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + FunctionCode + "&name=" + txtFunctionName.value + "&URL=" + txtFunctionURL.value + "&parent=" + ddlParent.value + "&status=" + ddlStatus.value + "&display=" + txtDisplayOrder.value +""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href="frmfunctionmaster.php";
                            }, 1000);

                            
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
</html>
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>