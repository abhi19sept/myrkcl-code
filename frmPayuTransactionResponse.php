<?php

  //   ini_set("display_errors", "1");
//	 error_reporting(E_ALL);

    //error_reporting(E_ALL);
    require_once 'DAL/classconnection.php';
    require_once('common/payments.php');
	require_once ('DAL/sendsms.php');

    $_ObjConnection = new _Connection();
	$_ObjConnection->Connect();
	
	date_default_timezone_set("Asia/Kolkata");

	ini_set("memory_limit", "5000M");
    ini_set("max_execution_time", 0);
    set_time_limit(0);
	
	$response = $_POST;

	if (isset($response['key']) && $response['key'] == 'XEX0bO') {
	die();
	}
	if (isset($response['razorpay_payment_id']) && empty($response['razorpay_payment_id']) === false) {
		//This block executes when normal response send by razorpay on transaction done.
		$productinfo = ('LearnerFeePayment' || 'LearnerEnquiryPayment');
		require 'razorpay/razorconfig.php';
		$payDetails = getRazorpayPaymentDetails($response['razorpay_payment_id']);

		if (!$payDetails) {
			$productinfo = 'NonLearnerFeePayment';
			require 'razorpay/razorconfig.php';
			$payDetails = getRazorpayPaymentDetails($response['razorpay_payment_id']);
		}

		$response = [];
		
	} elseif (!empty($response['txnid']) && !empty($response['mihpayid'])) {
		//This block executes when normal / server to server(S2S) response send by payu on transaction done.
		
			insertPayUResponseLog($response);
			$response = setKeySalt($response);
			$response['verify_by'] = 'VerifiedByServerApi';
		
	} else {
		//Get Json response of Razorpay, if not found then execute normally else process Json data.

		$json = file_get_contents('php://input');
		if (!empty($json)) {
			//This block executes when Server to server(S2S) response send by razorpay for each captured transaction.
			$paymentCode = savepaymentrazorpayresponse($json);
			$response = getResponseByPaymentTransaction(0, '', $paymentCode);
		} else {
			//This block executes frmPayuTransactionResponse file execute by cron / directly hit through URL.
			$response = (!empty($response['txnid'])) ? getResponseByPaymentTransaction(0, $response['txnid']) : getResponseByPaymentTransaction();
		}

		if ($response && empty($response['razorpay_order_id'])) {
			$response = setKeySalt($response);
			$response = getTransactionStatusByPayU($response);
		} elseif ($response && $response['paymentgateway'] == 'Razorpay' && !empty($response['razorpay_order_id'])) {
			$productinfo = $response["productinfo"];
			require_once 'razorpay/razorconfig.php';
    		$response = getTransactionStatusByRazorPay($response);
		} else {
			exit;
		}
	}

	if (!empty($response)) {
		//print_r($response);
		//die;
		processResponse($response);
	}
//echo $razorpaytime; die;
/**
*	Function to get database connection object.
*/
	function dbConnection() {
		global $_ObjConnection;

		return $_ObjConnection;
	}

/**
*	Function to Insert Razorpay response into tbl_paymentrazorpayresponse table as Log.
*/
	function savepaymentrazorpayresponse($json) {
		$_ObjConnection = dbConnection();
		$response = json_decode($json, true);
		$insertResponseSql = "INSERT IGNORE INTO tbl_paymentrazorpayresponse SET
			response = '" . serialize($response) . "'";

		$_Response = $_ObjConnection->ExecuteQuery($insertResponseSql, Message::InsertStatement);

		return $response['payload']['payment']['entity']['notes']['merchant_order_id'];
	}

/**
*	Function to Insert PayU response into tbl_payutransactionresponse table as Log.
*/
	function insertPayUResponseLog($response) {
		$_ObjConnection = dbConnection();

		$responsedIP = $_SERVER['REMOTE_ADDR'];

		$responseFields = ['txnid','mihpayid','mode','status','amount','udf1','udf2','udf3','udf4','udf5','udf6','udf7','udf8','udf9','udf10','field0','field1','field2','field3','field4','field5','field6','field7','field8','field9','hash','bank_ref_no','surl','curl','furl','card_token','card_no','card_hash','productinfo','offer','discount','offer_availed','unmappedstatus','firstname','lastname','address1','address2','city','state','country','zipcode','email','phone'];
		$insert = "";
		foreach ($responseFields as $field) {
			$response[$field] = !empty($response[$field]) ? $response[$field] : '';
			$insert .= $field . " = '" . $response[$field] . "', ";
		}

		$insertResponseSql = "INSERT IGNORE INTO tbl_payutransactionresponse SET
			" . $insert . " 
			tx_key = '" . ((!empty($response['key'])) ? $response['key'] : '') . "',
			responseIP = '" . $responsedIP . "',
			response = '" . serialize($response) . "',
			serverInfo = ''";

		$_Response = $_ObjConnection->ExecuteQuery($insertResponseSql, Message::InsertStatement);
	}

/**
*	Function to process transaction response.
*/
	function processResponse($response) {
		//print_r($response);
		////echo "<br>";
		//echo $razorpaytime;
		//die;
		$status = $response["status"];
		/*	if(isset($response["addedon"]) && !empty($response['addedon'])){
				$paytime = $response["addedon"];
			}
			else{
				$paytime = $razorpaytime;
			}*/
		$paytime = $response["addedon"];
		$firstname = $response["firstname"];
		$amount = $response["amount"];
		$txnid = $response["txnid"];
		$dbData = getResponseByPaymentTransaction(0, $txnid);
        $productinfo = ($dbData) ? $dbData['productinfo'] : $response["productinfo"];
		$udf1 = $response["udf1"];
		$udf2 = $response["udf2"];
		$udf3 = $response["udf3"];
		$udf4 = $response["udf4"];
		$udf5 = (isset($response["udf5"])) ? $response["udf5"] : 0;
		$verifyBy = $response["verify_by"];

		$email = (!empty($response['email'])) ? $response['email'] : '';
		$key = (!empty($response['key'])) ? $response['key'] : '';
		$posted_hash = (!empty($response['hash'])) ? $response['hash'] : '';

		if ($status == 'success' || $status == 'PaymentReceive' || $status == 'Money Settled') {
			//Do further process on success;
			switch ($productinfo) {
				case 'ReexamPayment':
				case 'Re Exam Event Name':
					setSuccessForReExamTransaction($txnid, 'ReexamPayment', $udf1, $udf2, $firstname, $amount, $verifyBy, $key, $posted_hash);
					break;
				case 'LearnerFeePayment':
				case 'Learner Fee Payment':
					setSuccessForLearnerFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $udf3, $udf4, $firstname, $amount, $verifyBy, $key, $posted_hash, $paytime);
					break;
				case 'RedHatFeePayment':
				case 'Red Hat Fee Payment':
					setSuccessForRedHatFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $udf3, $udf4, $udf5, 
										$firstname, $amount, $verifyBy, $key, $posted_hash);
					break;
				case 'Correction Certificate':
				case 'Correction Fee Payment':
				case 'Duplicate Certificate':
				case 'Duplicate Certificate with Correction':
					setSuccessForCorrectionFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $verifyBy, $key, $posted_hash);
					break;
				case 'NcrFeePayment':
				case 'Ncr Fee Payment':
					setSuccessForNcrFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $verifyBy, $key, $posted_hash);
					break;
				case 'Name Change Fee Payment':
                case 'Address Change Fee Payment':
				case 'NameAddressFeePayment':
                	setSuccessForNameAddressChangeFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $verifyBy, $key, $posted_hash);
                    break;
                case 'EOI Fee Payment':
                	setSuccessForEOIFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $email, $amount, $verifyBy, $key, $posted_hash);
                    break;
				case 'OwnershipFeePayment':
				case 'Ownership Fee Payment':
					setSuccessForOwnershipFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $verifyBy, $key, $posted_hash);
					break;	
					
				case 'PenaltyFeePayment':
                	setSuccessForPenaltyFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $email, $amount, $verifyBy, $key, $posted_hash);
                    break;		
					
			    case 'AadharUpdFeePayment':
                setSuccessForAadharUpdFeePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $udf3, $udf4, $firstname, $amount, $verifyBy, $key, $posted_hash);
                break;
				
				case 'ExperienceCenterFeePayment':
                setSuccessForExpCenterPaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $email, $amount, $verifyBy, $key, $posted_hash);
                break;
	            case 'BuyRedHatSubscription':
                setSuccessForRHLSPaymentTransaction($txnid, $productinfo, $udf1, $udf2, $firstname, $email, $amount, $verifyBy, $key, 
				$posted_hash);
                break;
				case 'LearnerEnquiryPayment':
					setSuccessForLearnerEnquiryPaymentTransaction($txnid, $productinfo, $udf1, $udf2, $udf3, $udf4, $firstname, $amount, $verifyBy, $key, $posted_hash);
					break;
				case 'RscfaCertificatePayment':
					setSuccessForRscfaCertificatePaymentTransaction($txnid, $productinfo, $udf1, $udf2, $udf3, $udf4, $firstname, $amount, $verifyBy, $key, $posted_hash, $paytime);
					break;
				case 'RscfaReExamPayment':
					setSuccessForRscfaReExamPaymentTransaction($txnid, $productinfo, $udf1, $udf2, $udf3, $udf4, $firstname, $amount, $verifyBy, $key, $posted_hash, $paytime);
					break;
			}
		} elseif ($status != 'pending') {
			//Do further process on failer;
			switch ($productinfo) {
				case 'ReexamPayment':
				case 'Re Exam Event Name':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateExamDataPaymentStatus(0, $txnid, $udf1);
					processReExamTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $key, $posted_hash);
					break;
				case 'LearnerFeePayment':
				case 'Learner Fee Payment':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateAdmissionPaymentStatus(0, $txnid, $udf1, $udf3, $udf4, $paytime);
					processAdmissionTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $udf3, $udf4, $key, $posted_hash, $paytime);
					break;
				case 'RedHatFeePayment':
				case 'Red Hat Fee Payment':
					updateRedHatPaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateRedHatAdmissionPaymentStatus(0, $txnid, $udf1, $udf3, $udf4, $udf5);
					processRedHatAdmissionTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $udf3, $udf4, $udf5,$key, $posted_hash);
					break;
				case 'Correction Certificate':
				case 'Correction Fee Payment':
				case 'Duplicate Certificate':
				case 'Duplicate Certificate with Correction':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateCorrectionPaymentStatus(0, $txnid, $udf1);
					processCorrectionTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $key, $posted_hash);
					break;
				case 'NcrFeePayment':
				case 'Ncr Fee Payment':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateOrgMasterPaymentStatus(0, $txnid);
					processNCRTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $key, $posted_hash);
					break;
			    case 'Name Change Fee Payment':
                case 'Address Change Fee Payment':
				case 'NameAddressFeePayment':
                	updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
                	updateNACTransactionStatus($status, $txnid, $udf1, $amount);
                	updateChangeAddressITGK(3, $txnid, $udf1);
                    break;
                case 'EOI Fee Payment':
                	updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
                	updateEOITransactionStatus($status, $firstname, $email, $amount, $txnid, $productinfo, $udf1, $udf2, $key, $posted_hash);
                	updateCourseITGKMapping(0, $txnid, $udf1);
                	processEOITransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $email, $amount, $key, $posted_hash);
                    break;
				case 'OwnershipFeePayment':
				case 'Ownership Fee Payment':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateOwnershipPaymentStatus(0, $txnid);
					processOwnershipTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $key, $posted_hash);
					break;
                case 'PenaltyFeePayment':
                	updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
                	updatePenaltyFeeStatus(0, $txnid, $udf1);
                	break;	
			    case 'AadharUpdFeePayment':
                updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
                updateAadharPaymentStatus(0, $txnid, $udf1, $udf3, $udf4);
                processAadharTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $udf3, $udf4, $key, $posted_hash);
                break;
				
				case 'ExperienceCenterFeePayment':
                updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
                updateExpCenterPaymentStatus(0, $txnid, $udf1, $udf2, $udf3);
				processExpCenterTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $email, $amount, $key, $posted_hash);
                break;
				
				case 'BuyRedHatSubscription':
                updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
                updateBuyRHLSPaymentStatus(0, $txnid, $udf1, $udf2, $udf3);
				processRHLSTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $email, $amount, $key, $posted_hash);
                break;				
				case 'LearnerEnquiryPayment':
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateAdmissionEnquiryPaymentStatus(0, $txnid, $udf1, $udf3, $udf4);
					processAdmissionEnquiryTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $udf3, $udf4, $key, $posted_hash);
					break;
				case 'RscfaCertificatePayment':				
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateRscfaCertificatePaymentStatus(0, $txnid, $udf1, $udf3, $udf4, $paytime);
					processRscfaCertificateTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $udf3, $udf4, $key, $posted_hash, $paytime);
					break;
				case 'RscfaReExamPayment':				
					updatePaymentTransactionStatus($verifyBy, $status, 'Reconciled', $txnid);
					updateRscfaReExamPaymentStatus(0, $txnid, $udf1, $udf3, $udf4, $paytime);
					processRscfaReExamTransaction($status, $txnid, $productinfo, $udf1, $udf2, $firstname, $amount, $udf3, $udf4, $key, $posted_hash, $paytime);
					break;
			}
		}
	}

/**
*	Function to get pending transaction as response.
*/
	function getResponseByPaymentTransaction($min = 40, $trnxid = '', $trnxCode = '') {
		$_ObjConnection = dbConnection();
		$timestamp = mktime(date("H"), date("i") - $min, date("s"), date("m"), date("d"), date("Y"));
		$time = date("Y-m-d H:i:s", $timestamp);
		$filter = '';
		if (empty($trnxid) && empty($trnxCode)) {
			$filter = "AND timestamp <= '" . $time . "' AND Pay_Tran_Reconcile_status = 'Not Reconciled' AND ( Pay_Tran_Status IN ('PaymentInProcess', 'PaymentFailure') OR paymentgateway LIKE ('Razorpay'))";
		} else {
			$filter .= (!empty($trnxid)) ? " AND Pay_Tran_PG_Trnid = '" . $trnxid . "'" : '';
			$filter .= (!empty($trnxCode)) ? " AND Pay_Tran_Code = '" . $trnxCode . "'" : '';
		}

		$selectQuery = "SELECT *, IF (Pay_Tran_Status = 'PaymentReceive', 'Successful', IF (Pay_Tran_Status = 'PaymentToRefund', 'Set for Refund', 'Failed')) AS paystate FROM tbl_payment_transaction WHERE (
			Pay_Tran_ProdInfo LIKE ('%exam%') OR 
			Pay_Tran_ProdInfo LIKE ('%learner%') OR 
			Pay_Tran_ProdInfo LIKE ('%Correction%') OR 
			Pay_Tran_ProdInfo LIKE ('%Duplicate%') OR 
			Pay_Tran_ProdInfo LIKE ('%ncr%') OR
			Pay_Tran_ProdInfo LIKE ('%name%') OR 
			Pay_Tran_ProdInfo LIKE ('%change%') OR 
			Pay_Tran_ProdInfo LIKE ('%RedHat%') OR 
			Pay_Tran_ProdInfo LIKE ('%owner%') OR
			Pay_Tran_ProdInfo LIKE ('%eoi%') OR
			Pay_Tran_ProdInfo LIKE ('%aadhar%') OR
			Pay_Tran_ProdInfo LIKE ('%penalty%') OR
			Pay_Tran_ProdInfo LIKE ('%ExperienceCenter%') OR
			Pay_Tran_ProdInfo LIKE ('%BuyRedHatSubscription%')  OR
			Pay_Tran_ProdInfo LIKE ('%LearnerEnquiryPayment%') OR
			Pay_Tran_ProdInfo LIKE ('%RscfaReExamPayment%') OR
			Pay_Tran_ProdInfo LIKE ('%RscfaCertificatePayment%')
		) $filter ORDER BY Pay_Tran_Code ASC LIMIT 1";
		
	//$selectQuery = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = 'a7e6bd2c21828c9e372a'";
		$result = $_ObjConnection->ExecuteQuery($selectQuery, Message::SelectStatement);
		$response = [];
		if (mysqli_num_rows($result[2])) {
			$data = mysqli_fetch_array($result[2]);
			$response = [
				'firstname' => $data['Pay_Tran_Fname'],
				'amount' => $data['Pay_Tran_Amount'],
				'txnid' => $data['Pay_Tran_PG_Trnid'],
				'productinfo' => $data['Pay_Tran_ProdInfo'],
				'udf1' => $data['Pay_Tran_ITGK'],
				'udf2' => $data['Pay_Tran_RKCL_Trnid'],
				'udf3' => $data['Pay_Tran_Course'],
				'udf4' => $data['Pay_Tran_Batch'],
				'udf5' => $data['Pay_Tran_CoursePkg'],
				'verify_by' => 'VerifiedByCron',
				'paystate' => (isset($data['paystate'])) ? $data['paystate'] : '',
				'paymentgateway' => $data['paymentgateway'],
				'razorpay_order_id' => $data['razorpay_order_id'],
			];
		}

		return (!empty($response)) ? $response : false;
	}

	function deleteFromPaymentRefund($transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "DELETE FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
	}
	
	function deleteFromRedHatPaymentRefund($transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "DELETE FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($query, Message::DeleteStatement);
	}
/**
*	Function to get transaction status from PayU as response.
*/
	function setKeySalt($response) {
		global $payment;

		switch ($response['productinfo']) {
			case 'LearnerFeePayment':
			case 'LearnerEnquiryPayment':
			case 'Learner Fee Payment':
				$key = "Es3Ozb";
				$salt = "o9aZmTfx";
			//	$key = "X2ZPKM";
			//	$salt = "8IaBELXB";				
				break;
			default:
				$key = "X2ZPKM";
				$salt = "8IaBELXB";
				break;
		}

		if ($payment::PAY_MODE == 'Test') {
			// For Test Mode
			$salt = "eCwWELxi";
	    	$key = 'gtKFFx';
    	}

		$response['key'] = $key;
		$response['salt'] = $salt;

    	return $response;
	}


	/**
	* 
	*/
	function getRazorpayPaymentDetails($paymentId) {
		global $api;
		$return = 0;
		try {
			$return = $api->payment->fetch($paymentId);
		} catch(Razorpay\Api\Errors\BadRequestError $be){
			$return = 0;
		} catch(Exception $e) {
			$return = 0;
		}
//print_r($return);
//die;
		return $return;
	}

	/**
	*	Function to get transaction status from Razorpay as response.
	*/
	function getTransactionStatusByRazorPay($response) {
		global $api;
try{
		$order = $api->order->fetch($response['razorpay_order_id']);
		//echo $order->created_at;
		//die;
		$razorpaytime =  date("Y-m-d H:i:s", $order->created_at);
		$response['addedon'] = $razorpaytime;
		$response['amount'] = intval($order->amount / 100 );
		$response['status'] = ($order && isset($order->status) && $order->status == 'paid') ? 'success' : 'PaymentnotDone';
		//print_r($response);
		//die;
		//$response['addedon'] = $order->created_at;
} catch(Exception $e) {
  $response['status'] = 'PaymentnotDone';
}
//print_r($response);
//die;
		return $response;
	}

	function getTransactionStatusByPayU($response) {
		global $payment;

		$txnid = $response['txnid'];
		if (!empty($txnid)) {
		
			$key = $response['key'];
			$salt = $response['salt'];

			$wsUrl = "https://info.payu.in/merchant/postservice?form=2";

			if ($payment::PAY_MODE == 'Test') {
				// For Test Mode
				$wsUrl = "https://test.payu.in/merchant/postservice.php?form=2";
			}
		
			$command = "verify_payment";
			$hash_str = $key  . '|' . $command . '|' . $txnid . '|' . $salt ;
			$hash = strtolower(hash('sha512', $hash_str));
			$request = [
				'key' => $key,
				'hash' => $hash,
				'var1' => $txnid,
				'command' => $command
			];
			
			$qs = http_build_query($request);
			$payuResponses = curlCall($wsUrl, $qs, TRUE);
			//echo "<br>";
			//print_r($payuResponses);
			$response['hash'] = $hash;
		}

		if(!empty($payuResponses['status']) && $payuResponses['status'] == 1) {
			foreach($payuResponses['transaction_details'] as $val) {
				$status = $val['status'];
				$paymenttime = $val['addedon'];
				$response['amount'] = intval($val['amt']);
			}
		} else {
			$status = 'PaymentnotDone';
		}

		$response['status'] = $status;
		$response['addedon'] = $paymenttime;
		return $response;
	}

/**
*	Function to make cURL request to get response.
*/
	function curlCall($wsUrl, $qs, $true) {
		$c = curl_init();
		
		curl_setopt($c, CURLOPT_URL, $wsUrl);
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
	
		curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		
		$o = curl_exec($c);
		if (curl_errno($c)) {
		    $c_error = curl_error($c);
			if (empty($c_error)) {
			  $c_error = 'Some server error';
			}

			return array('curl_status' => 'FAILURE', 'error' => $c_error);
		}
		$out = trim($o);
		$arr = json_decode($out,true);

		return $arr;
	}

/**
*	Functions for Re Exam Transactions Start
*/
	function setSuccessForReExamTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$refund = checkIfRefundRequiredForReExamTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		} else {
			processToRefundReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		}
	}

	function processToConfirmReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		updateExamDataPaymentStatus(1, $transactionId, $centerCode);
		processReExamTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createReExamTransactionInvoices($transactionId, $amount, $centerCode);
	}

	function createReExamTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT examdata_code AS ref_code FROM examdata WHERE itgkcode = '" . $centerCode ."' AND reexam_TranRefNo = '" . $transactionId . "'  AND paymentstatus = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		insertInvoice($_Responses, $amount, 'tbl_reexam_invoice');
	}

	function isPaymentTransactionFound($productInfo, $transactionId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);

		return mysqli_num_rows($response[2]);
	}

	function updatePaymentTransactionStatus($verifyApiStatus, $transactionStatus, $transactionReconcileStatus, $transactionId, $centerCode = false) {
		$_ObjConnection = dbConnection();
		$filter = ($centerCode) ?  " AND Pay_Tran_ITGK = '" . $centerCode . "'" : '';
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status = '" . $transactionStatus . "' AND Pay_Tran_PG_Trnid = '" . $transactionId . "'" . $filter;
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_payment_transaction SET 
				pay_verifyapi_status = '" . $verifyApiStatus . "',
				Pay_Tran_Status = '" . $transactionStatus . "',
				Pay_Tran_Reconcile_status = '" . $transactionReconcileStatus . "' 
			WHERE 
			Pay_Tran_PG_Trnid = '" . $transactionId . "'" . $filter;
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}
	
	function updateExamDataPaymentStatus($paymentstatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM examdata WHERE paymentstatus = " . $paymentstatus . " AND itgkcode = '" . $centerCode . "'  AND reexam_TranRefNo = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "UPDATE examdata SET paymentstatus = '" . $paymentstatus ."' WHERE itgkcode = '" . $centerCode . "'  AND reexam_TranRefNo = '" . $transactionId . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}
	
	function insertReExamTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Status = '" . $transactionStatus . "' AND Reexam_Transaction_Amount = '" . $amount . "' AND Reexam_Transaction_Txtid = '" . $transactionId . "' AND  Reexam_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_reexam_transaction (
				Reexam_Transaction_Code,
				Reexam_Transaction_Status,
				Reexam_Transaction_Fname,
				Reexam_Transaction_Amount,
				Reexam_Transaction_Txtid,
				Reexam_Transaction_ProdInfo,
				Reexam_Transaction_CenterCode,
				Reexam_Transaction_RKCL_Txid,
				Reexam_Transaction_Hash,
				Reexam_Transaction_Key,
				Reexam_Reconcile_Status,
				Reexam_DateTime
			) 
			SELECT CASE WHEN MAX(Reexam_Transaction_Code) IS NULL THEN 1 ELSE MAX(Reexam_Transaction_Code) + 1 END AS Reexam_Transaction_Code,
			'" . $transactionStatus . "' AS Reexam_Transaction_Status,
			'" . $firstName . "' AS Reexam_Transaction_Fname,
			'" . $amount . "' AS Reexam_Transaction_Amount,
			'" . $transactionId . "' AS Reexam_Transaction_Txtid,
			'" . $productInfo . "' AS Reexam_Transaction_ProdInfo,
			'" . $centerCode . "' AS Reexam_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Reexam_Transaction_RKCL_Txid,
			'" . $posted_hash . "' AS Reexam_Transaction_Hash,
			'" . $key . "' AS Reexam_Transaction_Key,
			'Reconciled'  AS Reexam_Reconcile_Status,
			'" . date("Y-m-d H:i:s") . "' AS Reexam_DateTime 
			FROM tbl_reexam_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateReExamTransactionStatus($transactionStatus, $reExamTransactionAmount, $transactionId, $centerCode, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "
			UPDATE tbl_reexam_transaction SET 
				Reexam_Transaction_Status = '" . $transactionStatus . "', Reexam_Transaction_Amount = '" . $reExamTransactionAmount . "', Reexam_Transaction_Hash = '" . $posted_hash . "', Reexam_Transaction_Key = '" . $key . "',
				Reexam_Reconcile_Status = 'Reconciled' 
			WHERE 
				Reexam_Transaction_Txtid = '" . $transactionId . "' AND  Reexam_Transaction_CenterCode = '" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function checkIfRefundRequiredForReExamTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(reexam_TranRefNo) AS ReexamTxnid FROM examdata WHERE reexam_TranRefNo = '" . $transactionId . "' AND itgkcode = '" . $centerCode . "' GROUP BY reexam_TranRefNo";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$reExamCounts = mysqli_fetch_array($response[2]);
				$reExamAmount = ($reExamCounts['ReexamTxnid'] * 300);
				if ($reExamAmount != $amount) {
					$refund = true;
				}
			} else {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processReExamTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_reexam_transaction WHERE Reexam_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertReExamTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updateReExamTransactionStatus($status, $amount, $transactionId, $centerCode, $key, $posted_hash);
		}
	}
	
	function processToRefundReEaxmTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processReExamTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updateExamDataPaymentStatus(0, $transactionId, $centerCode);
		
	}

	function setForPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_payment_refund (
				Payment_Refund_Id,
				Payment_Refund_Txnid,
				Payment_Refund_Amount,
				Payment_Refund_Status,
				Payment_Refund_ProdInfo,
				Payment_Refund_ITGK,
				Payment_Refund_RKCL_Txid
			)
			SELECT CASE WHEN MAX(Payment_Refund_Id) IS NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
			'" . $transactionId . "' AS Payment_Refund_Txnid,
			'" . $amount . "' AS Payment_Refund_Amount,
			'" . $status . "' AS Payment_Refund_Status,
			'" . $productInfo . "' AS Payment_Refund_ProdInfo,
			'" . $centerCode . "' AS Payment_Refund_ITGK,
			'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid
			FROM tbl_payment_refund";

			$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "'";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (! mysqli_num_rows($response[2])) {
				$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
			}
		}
	}
/**
*	Functions of Re Exam Transactions End
*/

/**
*	Functions for Learner Fee Transactions Start
*/
	function setSuccessForLearnerFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $firstName, $amount, $verifyBy, $key, $posted_hash, $paytime) {
		$refund = checkIfRefundRequiredForLearnerFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime);
		} else {
			processToRefundAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		}
	}

	function processAdmissionTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * From tbl_admission_transaction WHERE Admission_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		$numRows = mysqli_num_rows($response[2]);
		if(!$numRows) {
			insertAdmissionTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash, $paytime);
		} else {
			updateAdmissionTransactionStatus($status, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash);
		}
	}

	function processToConfirmAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime) {
		$_ObjConnection = dbConnection();
		updateAdmissionPaymentStatus(1, $transactionId, $centerCode, $course, $batch, $paytime);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createAdmissionTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch, $paytime);
		processAdmissionTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime);
		sendsmstolearners($transactionId, $centerCode, $course, $batch);
	}
	
	function sendsmstolearners($transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Admission_Name, Admission_LearnerCode, Admission_Mobile FROM tbl_admission WHERE Admission_ITGK_Code = '" . $centerCode ."' AND Admission_TranRefNo = '" . $transactionId . "'  AND Admission_Payment_Status = '1' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		while ($_Row = mysqli_fetch_array($_Responses[2])) {
			$sms = 'प्रिय ' . $_Row['Admission_Name'] . ', आपका एड्मिशन RKCL सर्वर पर कन्फ़र्म हो गया है| आप अपने ज्ञान केंद्र पर अपना बायोमेट्रिक पंजीकरण करवाए तथा RKCL के i-Learn सॉफ्टवेर के माध्यम से प्रशिक्षण प्रारम्भ करे| आपके द्वारा दी गयी रजिस्टर डीटेल को देखने के लिए गूगल प्ले स्टोर से RKCL की मोबाइल एप्लिकेशन डाउन लोड कर उसमे अपने  लर्नर कोड (' . $_Row['Admission_LearnerCode'] . ')  की सहायता से लॉगिन करे एवं लर्नर  को दी जाने वाली सर्विसेस का लाभ उठाये|';
			SendSMS($_Row['Admission_Mobile'], $sms);
		}
	}
	
	function createAdmissionTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch, $paytime) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Admission_Code AS ref_code FROM tbl_admission WHERE Admission_ITGK_Code = '" . $centerCode ."' AND Admission_TranRefNo = '" . $transactionId . "'  AND Admission_Payment_Status = '1' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		insertAdmissionInvoice($_Responses, $amount, 'tbl_admission_invoice', $batch, $paytime);
	}

	
	function insertAdmissionInvoice($_Responses, $amount, $table, $batch, $paytime) {
		$_ObjConnection = dbConnection();
		$admissionTimestamp = date("Y-m-d H:i:s");
		if($_Responses[0] == "Success") {
			$count = mysqli_num_rows($_Responses[2]);
			$amt = ($amount / $count);
			$refField = ($table == 'tbl_correction_invoice') ? 'transaction_ref_id' : (($table == 'tbl_reexam_invoice') ? 'exam_data_code' : 'invoice_ref_id');
			while ($_Row = mysqli_fetch_array($_Responses[2])) {
				$_Insert = "INSERT IGNORE INTO $table (invoice_no, " . $refField . ", amount, batch_code, addtime)
							SELECT (MAX(invoice_no) + 1) AS invoice_number, '" . $_Row['ref_code'] . "' AS invoice_ref_id, 
							'" . $amt . "' AS amount, '" . $batch . "' AS batch_code, '" . $paytime . "' AS addtime 
							FROM $table";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
			}
		}
	}
	
	
	
	function insertInvoice($_Responses, $amount, $table) {
		$_ObjConnection = dbConnection();
		$admissionTimestamp = date("Y-m-d H:i:s");
		if($_Responses[0] == "Success") {
			$count = mysqli_num_rows($_Responses[2]);
			$amt = ($amount / $count);
			$refField = ($table == 'tbl_correction_invoice') ? 'transaction_ref_id' : (($table == 'tbl_reexam_invoice') ? 'exam_data_code' : 'invoice_ref_id');
			while ($_Row = mysqli_fetch_array($_Responses[2])) {
				$_Insert = "INSERT IGNORE INTO $table (invoice_no, " . $refField . ", amount, addtime) SELECT (MAX(invoice_no) + 1) AS invoice_number, '" . $_Row['ref_code'] . "' AS invoice_ref_id, '" . $amt . "' AS amount, '" . $admissionTimestamp . "' AS addtime FROM $table";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
			}
		}
	}
	
	function updateAdmissionPaymentStatus($paymentstatus, $transactionId, $centerCode, $course, $batch, $paytime) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "UPDATE tbl_admission SET Admission_Payment_Status = '" . $paymentstatus . "',
			Admission_Date_Payment = '".$paytime."' WHERE Admission_ITGK_Code = '" . $centerCode . "' AND
			Admission_TranRefNo = '" . $transactionId . "' AND Admission_Course = '" . $course . "' AND
			Admission_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}
	
	function insertAdmissionTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash, $paytime) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT IGNORE INTO tbl_admission_transaction SET 
			Admission_Transaction_Status = '" . $transactionStatus . "',
			Admission_Transaction_Fname = '" . $firstName . "',
			Admission_Transaction_Amount = '" . $amount . "',
			Admission_Transaction_Txtid = '" . $transactionId . "',
			Admission_Transaction_ProdInfo = '" . $productInfo . "',
			Admission_Transaction_CenterCode = '" . $centerCode . "',
			Admission_Transaction_RKCL_Txid = '" . $transactionTxId . "',
			Admission_Transaction_Course = '" . $course . "',
			Admission_Transaction_Batch = '" . $batch . "',
			Admission_Transaction_Key = '" . $key . "',
			Admission_Transaction_Hash = '" . $posted_hash . "',
			Admission_Transaction_Reconcile_Status = 'Reconciled',
			Admission_Transaction_DateTime = '".$paytime."'";
		$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
	}

	
	function updateAdmissionTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
					$updateQuery = "
			UPDATE tbl_admission_transaction SET 
				Admission_Transaction_Status = '" . $transactionStatus . "',
				Admission_Transaction_Amount = '" . $amount . "',
				Admission_Transaction_Key = '" . $key . "',
				Admission_Transaction_Hash = '" . $posted_hash . "',
				Admission_Transaction_Reconcile_Status = 'Reconciled'
			WHERE 
				Admission_Transaction_Txtid = '" . $transactionId . "' AND 
				Admission_Transaction_CenterCode = '" . $centerCode . "' AND 
				Admission_Transaction_Course = '" . $course . "' AND 
				Admission_Transaction_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function checkIfRefundRequiredForLearnerFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(Admission_TranRefNo) AS AdmissionTxnid, Admission_Course, Admission_Batch FROM tbl_admission WHERE Admission_TranRefNo = '" . $transactionId . "' AND Admission_ITGK_Code = '" . $centerCode . "' GROUP BY Admission_TranRefNo, Admission_Course, Admission_Batch";
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['AdmissionTxnid'];
			    $course = $row['Admission_Course'];
			    //$baseAmount = (($course == '1') ? 2850 : (($course == '4') ? 2700 : (($course == '5') ? 3000 : 0)));
	//		    $baseAmount = (($course == '1') ? 1000 : (($course == '4') ? 1000 : (($course == '5') ? 1000 : 0)));
				
			$baseAmount = (($course == '1') ? 1000 : (($course == '23') ? 300 : (($course == '25') ? 2600: (($course == '28') ? 4000: (($course == '29') ? 5500: (($course == '30') ? 5500: (($course == '31') ? 5500: (($course == '32') ? 4000 : (($course == '4') ? 1000 : (($course == '5') ? 1000 : 0))))))))));
                                $coursefee='';
                                 $admissionAmount = $payTxnid * $baseAmount;
                                        if($course=='1' || $course=='4' || $course=='5'){
                                                $coursefee='1000';
                                        }
                                        elseif($course=='29' || $course=='30' || $course=='31'){
                                                $coursefee='5500';
                                        }
                                        elseif($course=='28' || $course=='32'){
                                                $coursefee='4000';
                                        }
                                        elseif ($course=='23'){
                                                $coursefee='300';
                                        }
                                        elseif ($course=='25'){
                                                $coursefee='2600';
                                        }
                                        else{
                                                $coursefee='1';
                                        }
				 $mode=fmod($amount,$coursefee);
				 $amount=$amount-$mode;
				if ($admissionAmount != $amount) {
					$refund = true;
					//die('c3');
				}
			} else {
				$refund = true;
			//die('c1');
			}
		} else {
			$refund = true;
			//die('c2');
			
		}

		return $refund;
	}

	function processToRefundAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		setForAdmissionPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processAdmissionTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime);
		updateAdmissionPaymentStatus(0, $transactionId, $centerCode, $course, $batch, $paytime);
	}

	function setForAdmissionPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT INTO tbl_payment_refund (
			Payment_Refund_Id,
			Payment_Refund_Txnid,
			Payment_Refund_Amount,
			Payment_Refund_Status,
			Payment_Refund_ProdInfo,
			Payment_Refund_ITGK,
			Payment_Refund_RKCL_Txid,
			Payment_Refund_Course,
			Payment_Refund_Batch
		) 
		SELECT CASE WHEN MAX(Payment_Refund_Id) Is NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
		'" . $transactionId . "' AS Payment_Refund_Txnid,
		'" . $amount . "' AS Payment_Refund_Amount,
		'" . $status . "' AS Payment_Refund_Status,
		'" . $productInfo . "' AS Payment_Refund_ProdInfo,
		'" . $centerCode . "' AS Payment_Refund_ITGK,
		'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid,
		'" . $course . "' AS Payment_Refund_Course,
		'" . $batch . "' AS Payment_Refund_Batch 
		FROM tbl_payment_refund";

		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "' AND Payment_Refund_Course = '" . $course . "' AND Payment_Refund_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

/**
*	Functions of Learner Fee Transactions End
*/

/**
*	Functions for Correction Fee Transactions Start
*/
	function setSuccessForCorrectionFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForCorrectionFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		} else {
			processToRefundCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		}
	}

	function processCorrectionTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if(! mysqli_num_rows($response[2])) {
			insertCorrectionTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updateCorrectionTransactionStatus($status, $amount, $transactionId, $centerCode, $key, $posted_hash);
		}
	}

	function processToConfirmCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		updateCorrectionPaymentStatus(1, $transactionId, $centerCode);
		processCorrectionTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createCorrectionTransactionInvoices($transactionId, $amount, $centerCode);
	}

	function createCorrectionTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT cid AS ref_code FROM tbl_correction_copy WHERE Correction_ITGK_Code = '" . $centerCode ."' AND Correction_TranRefNo = '" . $transactionId . "'  AND Correction_Payment_Status = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		insertInvoice($_Responses, $amount, 'tbl_correction_invoice');
	}

	function updateCorrectionPaymentStatus($paymentstatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "UPDATE tbl_correction_copy SET Correction_Payment_Status = '" . $paymentstatus . "' WHERE Correction_ITGK_Code = '" . $centerCode . "'  AND Correction_TranRefNo = '" . $transactionId . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function insertCorrectionTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_correction_transaction WHERE Correction_Transaction_Status = '" . $transactionStatus . "' AND Correction_Transaction_Amount = '" . $amount . "' AND Correction_Transaction_Txtid = '" . $transactionId . "' AND  Correction_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_correction_transaction (
				Correction_Transaction_Code,
				Correction_Transaction_Status,
				Correction_Transaction_Fname,
				Correction_Transaction_Amount,
				Correction_Transaction_Txtid,
				Correction_Transaction_ProdInfo,
				Correction_Transaction_CenterCode,
				Correction_Transaction_RKCL_Txid,
				Correction_Transaction_Hash,
				Correction_Transaction_Key,
				Correction_Transaction_Reconcile_Status,
				Correction_Transaction_DateTime,
				Correction_Transaction_Month,
				Correction_Transaction_Year
			) 
			SELECT CASE WHEN MAX(Correction_Transaction_Code) Is NULL THEN 1 ELSE MAX(Correction_Transaction_Code) + 1 END AS Correction_Transaction_Code,
			'" . $transactionStatus . "' AS Correction_Transaction_Status,
			'" . $firstName . "' AS Correction_Transaction_Fname,
			'" . $amount . "' AS Correction_Transaction_Amount,
			'" . $transactionId . "' AS Correction_Transaction_Txtid,
			'" . $productInfo . "' AS Correction_Transaction_ProdInfo,
			'" . $centerCode . "' AS Correction_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Correction_Transaction_RKCL_Txid,
			'" . $posted_hash . "' AS Correction_Transaction_Hash,
			'" . $key . "' AS Correction_Transaction_Key,
			'Reconciled' AS Correction_Transaction_Reconcile_Status,
			'" . date("Y-m-d H:i:s") . "' AS Correction_Transaction_DateTime,
			" . date("m") . " AS Correction_Transaction_Month,
			" . date("Y") . " AS Correction_Transaction_Year 
			FROM tbl_correction_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateCorrectionTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "
			UPDATE tbl_correction_transaction SET 
				Correction_Transaction_Status = '" . $transactionStatus . "',
				Correction_Transaction_Amount='" . $amount . "'
				Correction_Transaction_Hash = '" . $posted_hash . "',
				Correction_Transaction_Key ='" . $key . "',
				Correction_Transaction_Reconcile_Status ='Reconciled'
			WHERE 
				Correction_Transaction_Txtid = '" . $transactionId . "' AND 
				Correction_Transaction_CenterCode='" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function checkIfRefundRequiredForCorrectionFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(Correction_TranRefNo) AS CorrectionTxnid,Correction_Fee FROM tbl_correction_copy WHERE 
						Correction_TranRefNo = '" . $transactionId . "' AND Correction_ITGK_Code = '" . $centerCode . "' 
						GROUP BY Correction_TranRefNo";
			$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['CorrectionTxnid'];
				$fee = $row['Correction_Fee'];
				$correctionAmount = $payTxnid * $fee;
				if ($correctionAmount != $amount) {
					$refund = true;
				}
			} else {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundCorrectionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processCorrectionTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updateCorrectionPaymentStatus(0, $transactionId, $centerCode);
	}

/**
*	Functions of Correction Fee Transactions End
*/


/**
*	Functions of NCR Fee Transactions Start
*/
	function setSuccessForNcrFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForNCRFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmNCRTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		} else {
			processToRefundNCRTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		}
	}


	function checkIfRefundRequiredForNCRFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;

		//$query = "SELECT SUM(pt.Pay_Tran_Amount) AS Pay_Tran_Amount FROM tbl_org_master om INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = om.Org_TranRefNo WHERE Org_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' AND pt.Pay_Tran_Status NOT LIKE('%Receive%') GROUP BY om.Org_TranRefNo";
		
		$query = "SELECT pt.Pay_Tran_Amount FROM tbl_org_master om INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = om.Org_TranRefNo WHERE Org_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "'";
		
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2]) == 1) {
			$ncrFee = mysqli_fetch_array($response[2]);
			if ($ncrFee['Pay_Tran_Amount'] != $amount) {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundNCRTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		updateOrgMasterPaymentStatus(0, $transactionId);
		processNCRTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
	}

	function updateOrgMasterPaymentStatus($status, $transactionId) {
		$_ObjConnection = dbConnection();
		$_UpdateQuery = "UPDATE tbl_org_master SET Org_PayStatus = '" . $status . "' WHERE Org_TranRefNo = '" . $transactionId . "'";
        $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
	}

	function processNCRTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_ncr_transaction WHERE Ncr_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertNCRTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updateNCRTransactionStatus($status, $amount, $transactionId, $centerCode, $key, $posted_hash);
		}
	}

	function insertNCRTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_ncr_transaction WHERE Ncr_Transaction_Status = '" . $transactionStatus . "' AND Ncr_Transaction_Amount = '" . $amount . "' AND Ncr_Transaction_Txtid = '" . $transactionId . "' AND  Ncr_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_ncr_transaction (
				Ncr_Transaction_Code,
				Ncr_Transaction_Status,
				Ncr_Transaction_Fname,
				Ncr_Transaction_Amount,
				Ncr_Transaction_Txtid,
				Ncr_Transaction_ProdInfo,
				Ncr_Transaction_CenterCode,
				Ncr_Transaction_RKCL_Txid,
				Ncr_Transaction_Hash,
				Ncr_Transaction_Key,
				Ncr_Transaction_DateTime
			) 
			SELECT CASE WHEN MAX(Ncr_Transaction_Code) IS NULL THEN 1 ELSE MAX(Ncr_Transaction_Code) + 1 END AS Ncr_Transaction_Code,
			'" . $transactionStatus . "' AS Ncr_Transaction_Status,
			'" . $firstName . "' AS Ncr_Transaction_Fname,
			'" . $amount . "' AS Ncr_Transaction_Amount,
			'" . $transactionId . "' AS Ncr_Transaction_Txtid,
			'" . $productInfo . "' AS Ncr_Transaction_ProdInfo,
			'" . $centerCode . "' AS Ncr_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Ncr_Transaction_RKCL_Txid,
			'" . $posted_hash . "' AS Ncr_Transaction_Hash,
			'" . $key . "' AS Ncr_Transaction_Key,
			'" . date("Y-m-d H:i:s") . "' AS Ncr_Transaction_DateTime 
			FROM tbl_ncr_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateNCRTransactionStatus($transactionStatus, $reExamTransactionAmount, $transactionId, $centerCode, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "
			UPDATE tbl_ncr_transaction SET 
				Ncr_Transaction_Status = '" . $transactionStatus . "', Ncr_Transaction_Amount = '" . $reExamTransactionAmount . "', Ncr_Transaction_Hash = '" . $posted_hash . "', Ncr_Transaction_Key = '" . $key . "'
			WHERE 
				Ncr_Transaction_Txtid = '" . $transactionId . "' AND Ncr_Transaction_CenterCode = '" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function processToConfirmNCRTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		updateOrgMasterPaymentStatus(1, $transactionId);
		processNCRTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createNCRTransactionInvoices($transactionId, $amount, $centerCode);
		updateUserMasterRole($centerCode);
	}

	function createNCRTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Org_Ack AS ref_code FROM tbl_org_master WHERE  Org_TranRefNo = '" . $transactionId . "'  AND Org_PayStatus = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$orgRow = mysqli_fetch_array($_Responses[2]);
			$addTimestamp = date("Y-m-d H:i:s");
			$_Insert = "INSERT IGNORE INTO tbl_ncr_invoice (invoice_no, Ao_Itgkcode, Ao_Ack_No, amount, addtime) SELECT (MAX(invoice_no) + 1) AS invoice_number, '" . $centerCode . "' AS Ao_Itgkcode, '" . $orgRow['ref_code'] . "' AS Org_Ack, '" . $amount . "' AS Amount, '" . $addTimestamp . "' AS addtime FROM tbl_ncr_invoice";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		}
	}

	function updateUserMasterRole($centerCode) {
		$_ObjConnection = dbConnection();
	     $updateQuery = "
			UPDATE tbl_user_master SET 
				User_UserRoll = '15'
			WHERE 
				User_LoginId = '" . $centerCode . "' AND User_UserRoll = '22'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

/**
*	Functions of NCR Fee Transactions End
*/

/**
*	Functions of Name Address Change Fee Transactions Start
*/
	function setSuccessForNameAddressChangeFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForNACFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmNACTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		} else {
			processToRefundNACTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		}
	}


	function checkIfRefundRequiredForNACFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;

		$query = "SELECT SUM(pt.Pay_Tran_Amount) AS Pay_Tran_Amount FROM tbl_address_name_transaction ant INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = ant.fld_transactionID WHERE ant.fld_transactionID = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' GROUP BY ant.fld_transactionID";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2]) == 1) {
			$ncrFee = mysqli_fetch_array($response[2]);
			if ($ncrFee['Pay_Tran_Amount'] != $amount) {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundNACTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		updateNACTransactionStatus('Refund', $transactionId, $centerCode, $amount);
	}

	function updateNACTransactionStatus($transactionStatus, $transactionId, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$paystatus = ($transactionStatus == 'Success') ? 1 : 0;
		$updateQuery = "
			UPDATE tbl_address_name_transaction SET 
				fld_Transaction_Status = '" . $transactionStatus . "', 
				fld_amount = '" . $amount . "', 
				fld_paymentStatus = '" . $paystatus . "',
				fld_updatedOn = '" . date("Y-m-d h:i:s") . "'
			WHERE 
				fld_transactionID = '" . $transactionId . "' AND 
				fld_ITGK_Code = '" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function processToConfirmNACTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		updateNACTransactionStatus('Success', $transactionId, $centerCode, $amount);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		updateITGKNameAddressDetails($centerCode, $transactionId);
		createNACTransactionInvoices($transactionId, $amount, $centerCode);
		updateChangeAddressITGK(5, $transactionId, $centerCode);
	}

	function updateITGKNameAddressDetails($centerCode, $transactionTxId) {
		$_ObjConnection = dbConnection();

		$query = "SELECT od.Organization_Code, od.Organization_User, ca.* FROM tbl_organization_detail od INNER JOIN tbl_user_master um ON um.User_Code = od.Organization_User INNER JOIN tbl_payment_transaction pt ON um.User_LoginId = pt.Pay_Tran_ITGK INNER JOIN  tbl_change_address_itgk ca ON ca.fld_ref_no = pt.Pay_Tran_AdmissionArray WHERE pt.Pay_Tran_ITGK = '" . $centerCode . "' AND pt.Pay_Tran_PG_Trnid = '" . $transactionTxId . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$nacRow = mysqli_fetch_array($_Responses[2]);
			if ($nacRow['fld_requestChangeType'] == 'name') {
				$updateQuery = "UPDATE tbl_organization_detail SET Organization_Name = '" . $nacRow['Organization_Name'] . "' WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
			if ($updateQuery) {
				$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
			}
			
			} 
			
	/**		elseif ($nacRow['fld_requestChangeType'] == 'address' && $nacRow['Organization_AreaType'] == 'Urban') {
				$updateQuery = "UPDATE tbl_organization_detail SET
			Organization_Type = '" . $nacRow['Organization_Type'] . "',
			Organization_Address = '" . $nacRow['Organization_Address'] . "',
			Organization_Tehsil = '" . $nacRow['Organization_Tehsil'] . "',
			Organization_AreaType = '" . $nacRow['Organization_AreaType'] . "',
			Organization_Municipal_Type = '" . $nacRow['Organization_Municipal_Type'] . "',
			Organization_Municipal = '" . $nacRow['Organization_Municipal'] . "', 
			Organization_WardNo = '" . $nacRow['Organization_WardNo'] . "' 
			WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
			} elseif ($nacRow['fld_requestChangeType'] == 'address' && $nacRow['Organization_AreaType'] == 'Rural') {
				$updateQuery = "UPDATE tbl_organization_detail SET
			Organization_Type = '" . $nacRow['Organization_Type'] . "',
			Organization_Address = '" . $nacRow['Organization_Address'] . "',
			Organization_Tehsil = '" . $nacRow['Organization_Tehsil'] . "',
			Organization_AreaType = '" . $nacRow['Organization_AreaType'] . "',
			Organization_Village = '" . $nacRow['Organization_Village'] . "',
			Organization_Gram = '" . $nacRow['Organization_Gram'] . "', 
			Organization_Panchayat = '" . $nacRow['Organization_Panchayat'] . "' 
			WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
			}
			
			
			
			**/
		}
	}
	
	function createNACTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT ant.fld_ref_no AS ref_code, ant.fld_paymentTitle, um.User_Code FROM tbl_address_name_transaction ant INNER JOIN tbl_user_master um ON um.User_LoginId = ant.fld_ITGK_Code WHERE ant.fld_transactionID = '" . $transactionId . "'  AND ant.fld_paymentStatus = '1' AND ant.fld_ITGK_Code = '" . $centerCode . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$nacRow = mysqli_fetch_array($_Responses[2]);
			$addTimestamp = date("Y-m-d H:i:s");
			$gstCode = ($nacRow['fld_paymentTitle'] == 'name') ? 125 : 126;
			 $_Insert = "INSERT IGNORE INTO tbl_nlc_itgk_gstinvoice (fld_invoice, fld_ITGK_Code, fld_ITGK_UserCode, fld_referenceID, fld_gst_invoice_masterID, fld_addedOn) SELECT CONCAT('NLC/Online/', MAX(fld_ID)) AS invoice_number, '" . $centerCode . "' AS Ao_Itgkcode, '" . $nacRow['User_Code'] . "' AS User_Code, '" . $nacRow['ref_code'] . "' AS ref_code, '" . $gstCode . "' AS gstCode, '" . $addTimestamp . "' AS addtime FROM tbl_nlc_itgk_gstinvoice";
			$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);

			$_Insert = "INSERT IGNORE INTO tbl_nameaddress_invoice (invoice_no, invoice_ref_id, amount,addtime) SELECT CASE WHEN MAX(invoice_no) IS NULL THEN 1000 ELSE MAX(invoice_no) + 1 END AS invoice_no, '" . $nacRow['ref_code'] . "' AS invoice_ref_id, '" . $amount . "' as amount, '" . $addTimestamp . "' as addtime FROM tbl_nameaddress_invoice";
			$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
			if($amount > 1500){
                            $amt = $amount - 1500;
                                    
            date_default_timezone_set('Asia/Calcutta');
            $_Date = date("Y-m-d H:i:s");
            $_InsertQuery = "Insert into tbl_ncramount_master_log select A.*,'" . $_Date . "' from tbl_ncramount_master as a "
                    . "Where NCRamount_ITGK='" . $centerCode . "'";
            $_Response5 = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            
            $_UpdateQuery = "Update tbl_ncramount_master set NCRamount_Amount = NCRamount_Amount + '" . $amt . "' "
                     . "where NCRamount_ITGK='" . $centerCode . "'";
            
            $_Response8=$_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
                            
                        }
		}
	}

	function updateChangeAddressITGK($payStatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
	/**	$updateQuery = "
			UPDATE tbl_change_address_itgk ca INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_AdmissionArray = ca.fld_ref_no SET 
				ca.fld_status = '" . $payStatus . "'
			WHERE 
				pt.Pay_Tran_PG_Trnid = '" . $transactionId . "' AND ca.fld_ITGK_Code = '" . $centerCode . "'";  **/
				
				$updateQuery = "UPDATE tbl_change_address_itgk as ca, tbl_payment_transaction as pt SET 
				ca.fld_status = '" . $payStatus . "'
			WHERE pt.Pay_Tran_AdmissionArray = ca.fld_ref_no AND 
				pt.Pay_Tran_PG_Trnid = '" . $transactionId . "' AND ca.fld_ITGK_Code = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

/**
*	Functions of Name Address Change Fee Transactions End
*/

/**
*	Functions of EOI Fee Transactions Start
*/
	function setSuccessForEOIFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForEOIFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmEOITransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		} else {
			processToRefundEOITransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		}
	}


	function checkIfRefundRequiredForEOIFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;

		//$query = "SELECT SUM(pt.Pay_Tran_Amount) AS Pay_Tran_Amount FROM tbl_courseitgk_mapping cm INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = cm.Courseitgk_TranRefNo WHERE cm.Courseitgk_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' AND cm.EOI_Fee_Confirm = '0' GROUP BY cm.Courseitgk_TranRefNo";
		
		
		$query = "SELECT pt.Pay_Tran_Amount FROM tbl_courseitgk_mapping cm INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = cm.Courseitgk_TranRefNo WHERE cm.Courseitgk_TranRefNo = '" . $transactionId . "'";		
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2]) == 1) {
			$ncrFee = mysqli_fetch_array($response[2]);
			if ($ncrFee['Pay_Tran_Amount'] != $amount) {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundEOITransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processEOITransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		updateCourseITGKMapping(0, $transactionId, $centerCode);
	}

	function processEOITransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_eoi_transaction WHERE EOI_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertEOITransaction($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updateEOITransactionStatus($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		}
	}

	function insertEOITransaction($transactionStatus, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_eoi_transaction WHERE EOI_Transaction_Status = '" . $transactionStatus . "' AND EOI_Transaction_Amount = '" . $amount . "' AND EOI_Transaction_Txtid = '" . $transactionId . "' AND  EOI_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_eoi_transaction (
				EOI_Transaction_Code,
				EOI_Transaction_Status,
				EOI_Transaction_Fname,
				EOI_Transaction_Amount,
				EOI_Transaction_Txtid,
				EOI_Transaction_ProdInfo,
				EOI_Transaction_CenterCode,
				EOI_Transaction_Hash,
				EOI_Transaction_Key,
				EOI_Payment_Mode,
				EOI_Transaction_Email,
				EOI_Transaction_timestamp
			) 
			SELECT CASE WHEN MAX(EOI_Transaction_Code) IS NULL THEN 1 ELSE MAX(EOI_Transaction_Code) + 1 END AS EOI_Transaction_Code,
			'" . $transactionStatus . "' AS EOI_Transaction_Status,
			'" . $firstName . "' AS EOI_Transaction_Fname,
			'" . $amount . "' AS EOI_Transaction_Amount,
			'" . $transactionId . "' AS EOI_Transaction_Txtid,
			(SELECT Pay_Tran_AdmissionArray FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "') AS EOI_Transaction_ProdInfo,
			'" . $centerCode . "' AS EOI_Transaction_CenterCode,
			'" . $posted_hash . "' AS EOI_Transaction_Hash,
			'" . $key . "' AS EOI_Transaction_Key,
			'online' AS EOI_Payment_Mode,
			'" . $email . "' AS EOI_Transaction_Email,
			'" . date("Y-m-d H:i:s") . "' AS EOI_Transaction_timestamp 
			FROM tbl_eoi_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateEOITransactionStatus($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$updateQuery = "
			UPDATE tbl_eoi_transaction SET 
				EOI_Transaction_Status = '" . $status . "',
				EOI_Transaction_Fname = '" . $firstName . "',
				EOI_Transaction_Amount = '" . $amount . "',
				EOI_Transaction_ProdInfo = (SELECT Pay_Tran_AdmissionArray FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "'),
				EOI_Transaction_Hash = '" . $posted_hash . "',
				EOI_Transaction_Key = '" . $key . "',
				EOI_Payment_Mode = 'online',
				EOI_Transaction_Email = '" . $email . "',
				EOI_Transaction_timestamp = '" . date("Y-m-d H:i:s") . "'
			WHERE 
				EOI_Transaction_Txtid = '" . $transactionId . "' AND 
				EOI_Transaction_CenterCode = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

	function processToConfirmEOITransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		processEOITransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		updateCourseITGKMapping(1, $transactionId, $centerCode);
		createEOITransactionInvoices($transactionId, $amount, $centerCode);
	}

	function createEOITransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT cm.Courseitgk_EOI FROM tbl_courseitgk_mapping cm INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = cm.Courseitgk_TranRefNo WHERE cm.Courseitgk_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' AND cm.EOI_Fee_Confirm = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$eoiRow = mysqli_fetch_array($_Responses[2]);
			$eoiTimestamp = date("Y-m-d H:i:s");
			$_Insert = "INSERT INTO tbl_eoi_invoice (invoice_no, Itgkcode, eoi_code, amount, addtime) SELECT CASE WHEN MAX(invoice_no) IS NULL THEN 1000 ELSE MAX(invoice_no) + 1 END AS invoice_no, '" . $centerCode . "' AS Itgkcode, '" . $eoiRow['Courseitgk_EOI'] . "' AS eoi_code, '" . $amount . "' AS amount, '" . $eoiTimestamp . "' AS addtime FROM tbl_eoi_invoice";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		}
	}

	function updateCourseITGKMapping($payStatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$setFields = "EOI_Fee_Confirm = '" . $payStatus . "'";
		if ($payStatus) {
			$year = (date('m') > 3) ? date('Y') + 1 : date('Y');
			$expire_date = $year . "-03-31";
			$setFields .= ", CourseITGK_ExpireDate = IF (Courseitgk_Course LIKE ('%cfa%'), '" . $expire_date . "', CourseITGK_ExpireDate)";
		}
		
		$updateQuery = "
			UPDATE tbl_courseitgk_mapping SET 
				$setFields
			WHERE 
				Courseitgk_TranRefNo = '" . $transactionId . "' AND 
				Courseitgk_ITGK = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

/**
*	Functions of EOI Fee Transactions End
*/

/**
*	Functions for Red Hat Fee Transactions Start
*/
	function setSuccessForRedHatFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch,
				$coursepkg, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForRedHatFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmRedHatAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $coursepkg, $key, $posted_hash);
		} else {
			processToRefundRedHatAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $coursepkg, $key, $posted_hash);
		}
	}
	
	function processRedHatAdmissionTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, 
					$amount, $course, $batch, $coursepkg, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * From tbl_admission_transaction WHERE Admission_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		$numRows = mysqli_num_rows($response[2]);
		if(!$numRows) {
			insertRedHatAdmissionTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId,
										$course, $batch, $coursepkg, $key, $posted_hash);
		} else {
			updateRedHatAdmissionTransactionStatus($status, $amount, $transactionId, $centerCode, $course, $batch, $coursepkg, $key, 
											$posted_hash);
		}
	}
	
	function processToConfirmRedHatAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName,
					$amount, $course, $batch, $coursepkg, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		updateRedHatAdmissionPaymentStatus(1, $transactionId, $centerCode, $course, $batch, $coursepkg);
		updateRedHatPaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromRedHatPaymentRefund($transactionId, $centerCode);
		createRedHatAdmissionTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch, $coursepkg);
		processRedHatAdmissionTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $coursepkg, $key, $posted_hash);
		sendsmstoRedHatlearners($transactionId, $centerCode, $course, $batch);
	}
	
		function sendsmstoRedHatlearners($transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Admission_Name, Admission_LearnerCode, Admission_Mobile FROM tbl_admission WHERE Admission_ITGK_Code = '" . $centerCode ."' AND Admission_TranRefNo = '" . $transactionId . "'  AND Admission_Payment_Status = '1' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		while ($_Row = mysqli_fetch_array($_Responses[2])) {
			$sms = 'प्रिय ' . $_Row['Admission_Name'] . ', आपका एड्मिशन RKCL सर्वर पर कन्फ़र्म हो गया है|  आपके द्वारा दी गयी रजिस्टर डीटेल को देखने के लिए गूगल प्ले स्टोर से RKCL की मोबाइल एप्लिकेशन डाउन लोड कर उसमे अपने  लर्नर कोड (' . $_Row['Admission_LearnerCode'] . ')  की सहायता से लॉगिन करे एवं लर्नर  को दी जाने वाली सर्विसेस का लाभ उठाये|';
			SendSMS($_Row['Admission_Mobile'], $sms);
		}
	}
	
	function createRedHatAdmissionTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch, $coursepkg) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Admission_Code AS ref_code FROM tbl_admission WHERE Admission_ITGK_Code = '" . $centerCode ."' AND 
		Admission_TranRefNo = '" . $transactionId . "'  AND Admission_Payment_Status = '1' AND Admission_Course = '" . $course . "'
		AND Admission_Batch = '" . $batch . "' AND Admission_Advance_CourseCode='".$coursepkg."'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		insertRedHatInvoice($_Responses, $amount, 'tbl_admission_invoice' , $batch);
	}
	
	function isRedHatPaymentTransactionFound($productInfo, $transactionId) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);

		return mysqli_num_rows($response[2]);
	}
	
	function updateRedHatPaymentTransactionStatus($verifyApiStatus, $transactionStatus, $transactionReconcileStatus, $transactionId, $centerCode = false) {
		$_ObjConnection = dbConnection();
		$filter = ($centerCode) ?  " AND Pay_Tran_ITGK = '" . $centerCode . "'" : '';
		$query = "SELECT * FROM tbl_payment_transaction WHERE Pay_Tran_Status = '" . $transactionStatus . "' AND Pay_Tran_PG_Trnid = '" . $transactionId . "'" . $filter;
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$updateQuery = "
			UPDATE tbl_payment_transaction SET 
				pay_verifyapi_status = '" . $verifyApiStatus . "',
				Pay_Tran_Status = '" . $transactionStatus . "',
				Pay_Tran_Reconcile_status = '" . $transactionReconcileStatus . "' 
			WHERE 
			Pay_Tran_PG_Trnid = '" . $transactionId . "'" . $filter;
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		}
	}
	
	function insertRedHatInvoice($_Responses, $amount, $table, $batch) {
		$_ObjConnection = dbConnection();
		$admissionTimestamp = date("Y-m-d H:i:s");
		if($_Responses[0] == "Success") {
			$count = mysqli_num_rows($_Responses[2]);
			$amt = ($amount / $count);
			$refField = ($table == 'tbl_correction_invoice') ? 'transaction_ref_id' : (($table == 'tbl_reexam_invoice') ? 'exam_data_code' : 'invoice_ref_id');
			while ($_Row = mysqli_fetch_array($_Responses[2])) {
				$_Insert = "INSERT IGNORE INTO $table (invoice_no, " . $refField . ", amount, batch_code, addtime) 
				SELECT (MAX(invoice_no) + 1) AS invoice_number, '" . $_Row['ref_code'] . "' AS invoice_ref_id, 
				'" . $amt . "' AS amount, '" . $batch . "' AS batch_code, '" . $admissionTimestamp . "' AS addtime FROM $table";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
			}
		}
	}
	
	function updateRedHatAdmissionPaymentStatus($paymentstatus, $transactionId, $centerCode, $course, $batch, $coursepkg) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "UPDATE tbl_admission SET Admission_Payment_Status = '" . $paymentstatus . "',
							Admission_Date_Payment = now() WHERE Admission_ITGK_Code = '" . $centerCode . "' AND
							Admission_TranRefNo = '" . $transactionId . "' AND Admission_Course = '" . $course . "' 
							AND Admission_Batch = '" . $batch . "' AND Admission_Advance_CourseCode='".$coursepkg."'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}
	
	function insertRedHatAdmissionTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $coursepkg, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT IGNORE INTO tbl_admission_transaction SET 
			Admission_Transaction_Status = '" . $transactionStatus . "',
			Admission_Transaction_Fname = '" . $firstName . "',
			Admission_Transaction_Amount = '" . $amount . "',
			Admission_Transaction_Txtid = '" . $transactionId . "',
			Admission_Transaction_ProdInfo = '" . $productInfo . "',
			Admission_Transaction_CenterCode = '" . $centerCode . "',
			Admission_Transaction_RKCL_Txid = '" . $transactionTxId . "',
			Admission_Transaction_Course = '" . $course . "',
			Admission_Transaction_Batch = '" . $batch . "',
			Admission_Transaction_CoursePkg = '" . $coursepkg . "',
			Admission_Transaction_Key = '" . $key . "',
			Admission_Transaction_Hash = '" . $posted_hash . "',
			Admission_Transaction_Reconcile_Status = 'Reconciled',
			Admission_Transaction_DateTime = '" . date("Y-m-d H:i:s") . "'";
		$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
	}
	
	function updateRedHatAdmissionTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $course, $batch, 
								$coursepkg, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
					$updateQuery = "
			UPDATE tbl_admission_transaction SET 
				Admission_Transaction_Status = '" . $transactionStatus . "',
				Admission_Transaction_Amount = '" . $amount . "',
				Admission_Transaction_Key = '" . $key . "',
				Admission_Transaction_Hash = '" . $posted_hash . "',
				Admission_Transaction_Reconcile_Status = 'Reconciled'
			WHERE 
				Admission_Transaction_Txtid = '" . $transactionId . "' AND 
				Admission_Transaction_CenterCode = '" . $centerCode . "' AND 
				Admission_Transaction_Course = '" . $course . "' AND 
				Admission_Transaction_Batch = '" . $batch . "' AND Admission_Transaction_CoursePkg = '" . $coursepkg . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

		function checkIfRefundRequiredForRedHatFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isRedHatPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(Admission_TranRefNo) AS AdmissionTxnid, Admission_Advance_CourseCode, Admission_Batch 
						FROM tbl_admission WHERE Admission_TranRefNo = '" . $transactionId . "' AND 
						Admission_ITGK_Code = '" . $centerCode . "' GROUP BY Admission_TranRefNo, Admission_Advance_CourseCode, 
						Admission_Batch";
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['AdmissionTxnid'];
			    $course = $row['Admission_Advance_CourseCode'];
			    $baseAmount = 1500;
				 $admissionAmount = $payTxnid * $baseAmount;
				if ($admissionAmount != $amount) {
					$refund = true;
					//die('c3');
				}
			} else {
				$refund = true;
			//die('c1');
			}
		} else {
			$refund = true;
			//die('c2');
			
		}

		return $refund;
	}
	
	function processToRefundRedHatAdmissionTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, 
												$course, $batch, $coursepkg, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		setForRedHatAdmissionPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course,
											$batch);
		updateRedHatPaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processRedHatAdmissionTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $coursepkg, $key, $posted_hash);
		updateRedHatAdmissionPaymentStatus(0, $transactionId, $centerCode, $course, $batch, $coursepkg);
	}
	
	function setForRedHatAdmissionPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId,
							$course, $batch) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT INTO tbl_payment_refund (
			Payment_Refund_Id,
			Payment_Refund_Txnid,
			Payment_Refund_Amount,
			Payment_Refund_Status,
			Payment_Refund_ProdInfo,
			Payment_Refund_ITGK,
			Payment_Refund_RKCL_Txid,
			Payment_Refund_Course,
			Payment_Refund_Batch
		) 
		SELECT CASE WHEN MAX(Payment_Refund_Id) Is NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
		'" . $transactionId . "' AS Payment_Refund_Txnid,
		'" . $amount . "' AS Payment_Refund_Amount,
		'" . $status . "' AS Payment_Refund_Status,
		'" . $productInfo . "' AS Payment_Refund_ProdInfo,
		'" . $centerCode . "' AS Payment_Refund_ITGK,
		'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid,
		'" . $course . "' AS Payment_Refund_Course,
		'" . $batch . "' AS Payment_Refund_Batch 
		FROM tbl_payment_refund";

		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "' AND Payment_Refund_Course = '" . $course . "' AND Payment_Refund_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

/**
*	Functions of Red Hat Fee Transactions End
*/

/**
*	Functions of Ownership Fee Transactions Start
*/
	function setSuccessForOwnershipFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForOwnershipFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmOwnershipTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		} else {
			processToRefundOwnershipTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		}
	}


	function checkIfRefundRequiredForOwnershipFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;

		//$query = "SELECT SUM(pt.Pay_Tran_Amount) AS Pay_Tran_Amount FROM tbl_org_master om INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = om.Org_TranRefNo WHERE Org_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' AND pt.Pay_Tran_Status NOT LIKE('%Receive%') GROUP BY om.Org_TranRefNo";
		
		$query = "SELECT pt.Pay_Tran_Amount FROM tbl_ownership_change om INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = om.Org_TranRefNo WHERE Org_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "'";
		
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2]) == 1) {
			$OwnershipFee = mysqli_fetch_array($response[2]);
			if ($OwnershipFee['Pay_Tran_Amount'] != $amount) {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundOwnershipTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		updateOwnershipPaymentStatus(0, $transactionId);
		processOwnershipTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
	}

	function updateOwnershipPaymentStatus($status, $transactionId) {
		$_ObjConnection = dbConnection();
		$_UpdateQuery = "UPDATE tbl_ownership_change SET Org_PayStatus = '" . $status . "' WHERE Org_TranRefNo = '" . $transactionId . "'";
        $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
	}

	function processOwnershipTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_ownershipchange_transaction WHERE Ownership_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertOwnershipTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updateOwnershipTransactionStatus($status, $amount, $transactionId, $centerCode, $key, $posted_hash);
		}
	}

	function insertOwnershipTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_ownershipchange_transaction WHERE Ownership_Transaction_Status = '" . $transactionStatus . "' AND Ownership_Transaction_Amount = '" . $amount . "' AND Ownership_Transaction_Txtid = '" . $transactionId . "' AND  Ownership_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_ownershipchange_transaction (
				Ownership_Transaction_Code,
				Ownership_Transaction_Status,
				Ownership_Transaction_Fname,
				Ownership_Transaction_Amount,
				Ownership_Transaction_Txtid,
				Ownership_Transaction_ProdInfo,
				Ownership_Transaction_CenterCode,
				Ownership_Transaction_RKCL_Txid,
				Ownership_Transaction_Hash,
				Ownership_Transaction_Key,
				Ownership_Transaction_DateTime
			) 
			SELECT CASE WHEN MAX(Ownership_Transaction_Code) IS NULL THEN 1 ELSE MAX(Ownership_Transaction_Code) + 1 END AS Ownership_Transaction_Code,
			'" . $transactionStatus . "' AS Ownership_Transaction_Status,
			'" . $firstName . "' AS Ownership_Transaction_Fname,
			'" . $amount . "' AS Ownership_Transaction_Amount,
			'" . $transactionId . "' AS Ownership_Transaction_Txtid,
			'" . $productInfo . "' AS Ownership_Transaction_ProdInfo,
			'" . $centerCode . "' AS Ownership_Transaction_CenterCode,
			'" . $transactionTxId . "' AS Ownership_Transaction_RKCL_Txid,
			'" . $posted_hash . "' AS Ownership_Transaction_Hash,
			'" . $key . "' AS Ownership_Transaction_Key,
			'" . date("Y-m-d H:i:s") . "' AS Ownership_Transaction_DateTime 
			FROM tbl_ownershipchange_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateOwnershipTransactionStatus($transactionStatus, $reExamTransactionAmount, $transactionId, $centerCode, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "
			UPDATE tbl_ownershipchange_transaction SET 
				Ownership_Transaction_Status = '" . $transactionStatus . "', Ownership_Transaction_Amount = '" . $reExamTransactionAmount . "', Ownership_Transaction_Hash = '" . $posted_hash . "', Ownership_Transaction_Key = '" . $key . "'
			WHERE 
				Ownership_Transaction_Txtid = '" . $transactionId . "' AND Ownership_Transaction_CenterCode = '" . $centerCode . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function processToConfirmOwnershipTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash) {
		updateOwnershipPaymentStatus(1, $transactionId);
		processOwnershipTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $key, $posted_hash);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createOwnershipTransactionInvoices($transactionId, $amount, $centerCode);
	}

	function createOwnershipTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Org_Ack AS ref_code FROM tbl_ownership_change WHERE  Org_TranRefNo = '" . $transactionId . "'  AND Org_PayStatus = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$orgRow = mysqli_fetch_array($_Responses[2]);
			$addTimestamp = date("Y-m-d H:i:s");
			$_Insert = "INSERT IGNORE INTO tbl_ownershipchange_invoice (invoice_no, Ao_Itgkcode, Ao_Ack_No, amount, addtime) SELECT (MAX(invoice_no) + 1) AS invoice_number, '" . $centerCode . "' AS Ao_Itgkcode, '" . $orgRow['ref_code'] . "' AS Org_Ack, '" . $amount . "' AS Amount, '" . $addTimestamp . "' AS addtime FROM tbl_ownershipchange_invoice";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		}
	}

/**
*	Functions of Ownership Fee Transactions End
*/

function setSuccessForPenaltyFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForPenaltyFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmPenaltyFeeTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		} else {
			processToRefundPenaltyFeeTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		}
	}

	function checkIfRefundRequiredForPenaltyFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		
		$query = "SELECT rp.RenewalPenalty_Penalty FROM tbl_renewal_penalty rp INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = rp.RenewalPenalty_Trans_Id WHERE rp.RenewalPenalty_Trans_Id = '" . $transactionId . "'";		
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2]) == 1) {
			$ncrFee = mysqli_fetch_array($response[2]);
			if ($ncrFee['RenewalPenalty_Penalty'] != $amount) {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundPenaltyFeeTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processPenaltyFeeTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		updatePenaltyFeeStatus(0, $transactionId, $centerCode);
	}

	function processPenaltyFeeTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_renewal_penalty_transaction WHERE RP_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertPenaltyFeeTransaction($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updatePenaltyFeeTransactionStatus($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		}
	}

	function insertPenaltyFeeTransaction($transactionStatus, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_renewal_penalty_transaction WHERE RP_Transaction_Status = '" . $transactionStatus . "' AND RP_Transaction_Amount = '" . $amount . "' AND RP_Transaction_Txtid = '" . $transactionId . "' AND  RP_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_renewal_penalty_transaction (
				RP_Transaction_Code,
				RP_Transaction_Status,
				RP_Transaction_Fname,
				RP_Transaction_Amount,
				RP_Transaction_Txtid,
				RP_Transaction_ProdInfo,
				RP_Transaction_CenterCode,
				RP_Transaction_Hash,
				RP_Transaction_Key,
				RP_Payment_Mode,
				RP_Transaction_Email,
				RP_Transaction_timestamp
			) 
			SELECT CASE WHEN MAX(RP_Transaction_Code) IS NULL THEN 1 ELSE MAX(RP_Transaction_Code) + 1 END AS RP_Transaction_Code,
			'" . $transactionStatus . "' AS RP_Transaction_Status,
			'" . $firstName . "' AS RP_Transaction_Fname,
			'" . $amount . "' AS RP_Transaction_Amount,
			'" . $transactionId . "' AS RP_Transaction_Txtid,
			(SELECT Pay_Tran_AdmissionArray FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "') AS RP_Transaction_ProdInfo,
			'" . $centerCode . "' AS RP_Transaction_CenterCode,
			'" . $posted_hash . "' AS RP_Transaction_Hash,
			'" . $key . "' AS RP_Transaction_Key,
			'online' AS RP_Payment_Mode,
			'" . $email . "' AS RP_Transaction_Email,
			'" . date("Y-m-d H:i:s") . "' AS RP_Transaction_timestamp 
			FROM tbl_renewal_penalty_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updatePenaltyFeeTransactionStatus($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$updateQuery = "
			UPDATE tbl_renewal_penalty_transaction SET 
				RP_Transaction_Status = '" . $status . "',
				RP_Transaction_Fname = '" . $firstName . "',
				RP_Transaction_Amount = '" . $amount . "',
				RP_Transaction_ProdInfo = (SELECT Pay_Tran_AdmissionArray FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "'),
				RP_Transaction_Hash = '" . $posted_hash . "',
				RP_Transaction_Key = '" . $key . "',
				RP_Payment_Mode = 'online',
				RP_Transaction_Email = '" . $email . "',
				RP_Transaction_timestamp = '" . date("Y-m-d H:i:s") . "'
			WHERE 
				RP_Transaction_Txtid = '" . $transactionId . "' AND 
				RP_Transaction_CenterCode = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

	function processToConfirmPenaltyFeeTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		processPenaltyFeeTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		updatePenaltyFeeStatus(1, $transactionId, $centerCode);
		createPenaltyFeeTransactionInvoices($transactionId, $amount, $centerCode);
		Additgkrenewaldata(1, $centerCode, 'OnlinePayment');
	}

	function updatePenaltyFeeStatus($status, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		$updateQuery = "
			UPDATE tbl_renewal_penalty SET 
				RenewalPenalty_Payment_Status = '" . $status . "'
			WHERE 
				RenewalPenalty_Trans_Id = '" . $transactionId . "' AND 
				RenewalPenalty_ItgkCode = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

	function createPenaltyFeeTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT pt.* FROM tbl_renewal_penalty rp INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = rp.RenewalPenalty_Trans_Id WHERE rp.RenewalPenalty_Trans_Id = '" . $transactionId . "' AND rp.RenewalPenalty_ItgkCode = '" . $centerCode . "' AND rp.RenewalPenalty_Payment_Status = '1'";	
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$eoiRow = mysqli_fetch_array($_Responses[2]);
			$addTimestamp = date("Y-m-d H:i:s");
			$_Insert = "INSERT INTO tbl_renewal_penalty_invoice (invoice_no, Itgkcode, panelty_code, amount, addtime) SELECT CASE WHEN MAX(invoice_no) IS NULL THEN 1000 ELSE MAX(invoice_no) + 1 END AS invoice_no, '" . $centerCode . "' AS Itgkcode, '" . $eoiRow['Pay_Tran_AdmissionArray'] . "' AS panelty_code, '" . $amount . "' AS amount, '" . $addTimestamp . "' AS addtime FROM tbl_renewal_penalty_invoice";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		}
	}
	
	function Additgkrenewaldata($EOI_Code, $itgkcode, $_fname) {
        $_ObjConnection = dbConnection();
		$_Date = date("Y-m-d");
		$_SelectQuery = "SELECT EXTRACT(Year FROM CourseITGK_ExpireDate) AS CourseITGK_ExpireDate, datediff(CourseITGK_ExpireDate,'" . $_Date . "') as DateDiffernce From tbl_courseitgk_mapping WHERE Courseitgk_ITGK = '" . $itgkcode . "' AND Courseitgk_EOI='" . $EOI_Code . "'";
		$_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
		if($_Response[0]== Message::NoRecordFound) return;
		$row=mysqli_fetch_array($_Response[2]);
		$expire_year=$row['CourseITGK_ExpireDate'];
		$EoiDate=$row['DateDiffernce'];
		$previous_year = ($expire_year-1);
		if ($EoiDate <= 60) {
			$_DuplicateQuery = "SELECT * FROM tbl_eoi_centerlist WHERE EOI_Code = '" . $EOI_Code . "' AND EOI_ECL = '" . $itgkcode . "' AND EOI_Year='" . $previous_year . "' AND EOI_Status_Flag='Y'";
			$_Responsess = $_ObjConnection->ExecuteQuery($_DuplicateQuery, Message::SelectStatement);
			if ($_Responsess[0] == Message::NoRecordFound) {
				$_InsertQuery = "INSERT INTO tbl_eoi_centerlist (EOI_ID, EOI_Code, EOI_ECL, EOI_Year, EOI_DateTime, EOI_Filename, EOI_Status_Flag) Select Case When Max(EOI_ID) Is Null Then 1 Else Max(EOI_ID)+1 End as EOI_ID, '" . $EOI_Code . "' as EOI_Code,'" . $itgkcode . "' as EOI_ECL,'" . $previous_year . "' as EOI_Year, '" . $_Date . "' as EOI_DateTime, '" . $_fname . "' as EOI_Filename, 'Y' as EOI_Status_Flag FROM tbl_eoi_centerlist";
				$_Responses = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
				$_UpdateQuery1 = "UPDATE tbl_courseitgk_mapping SET CourseITGK_Renewal_Status = 'Y' WHERE Courseitgk_ITGK='" . $itgkcode . "' AND Courseitgk_EOI IN('" . $EOI_Code . "','2')";
				$_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery1, Message::UpdateStatement);
				$_InsertQuery1 = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year, EOI_Log_DateTime, EOI_Log_Filename,EOI_Log_Status_Flag) SELECT Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID, '" . $EOI_Code . "' as EOI_Log_Code,'" . $itgkcode . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year, '" . $_Date . "' as EOI_Log_DateTime, '" . $_fname . "' as EOI_Log_Filename, 'Updated' as EOI_Log_Status_Flag FROM tbl_eoi_centerlist_Log";
				$_ResponseLog = $_ObjConnection->ExecuteQuery($_InsertQuery1, Message::InsertStatement);								
			} else {
				$_InsertQuery = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year, EOI_Log_DateTime, EOI_Log_Filename) Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID, '" . $EOI_Code . "' as EOI_Log_Code,'" . $itgkcode . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year, '" . $_Date . "' as EOI_Log_DateTime, '" . $_fname . "' as EOI_Log_Filename From tbl_eoi_centerlist_Log";
				$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			}
		} else {
			$_InsertQuery = "INSERT INTO tbl_eoi_centerlist_Log (EOI_Log_ID, EOI_Log_Code, EOI_Log_ECL, EOI_Log_Year, EOI_Log_DateTime, EOI_Log_Filename)  Select Case When Max(EOI_Log_ID) Is Null Then 1 Else Max(EOI_Log_ID)+1 End as EOI_Log_ID, '" . $EOI_Code . "' as EOI_Log_Code,'" . $itgkcode . "' as EOI_Log_ECL,'" . $previous_year . "' as EOI_Log_Year, '" . $_Date . "' as EOI_Log_DateTime, '" . $_fname . "' as EOI_Log_Filename FROM tbl_eoi_centerlist_Log";
			$_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
			}
	}
	
/**
 * 	Functions for Aadhar Fee Transactions Start
 */
function setSuccessForAadharUpdFeePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $firstName, $amount, $verifyBy, $key, $posted_hash) {
    $refund = checkIfRefundRequiredForAadharFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
    if (!$refund) {
        processToConfirmAadharTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
    } else {
        processToRefundAadharTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
    }
}

function processAadharTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
    $_ObjConnection = dbConnection();
    $query = "SELECT * From tbl_aadhar_upd_transaction WHERE Aadhar_Transaction_Txtid = '" . $transactionId . "'";
    $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
    $numRows = mysqli_num_rows($response[2]);
    if (!$numRows) {
        insertAadharTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash);
    } else {
        updateAadharTransactionStatus($status, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash);
    }
}

function processToConfirmAadharTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
    $_ObjConnection = dbConnection();
    updateAadharPaymentStatus(1, $transactionId, $centerCode, $course, $batch);
    updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
    deleteFromPaymentRefund($transactionId, $centerCode);
    createAadharTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch);
    processAadharTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
    //sendsmstoaadharlearners($transactionId, $centerCode, $course, $batch);
    updateAdmTableFromLog($transactionId, $centerCode, $course, $batch);
}

function updateAdmTableFromLog($transactionId, $centerCode, $course, $batch) {
     $_ObjConnection = dbConnection();
   $_UpdateQuery = "Update tbl_admission as a, tbl_adm_log_foraadhar as b set a.Admission_Name=b.Adm_Log_Lname_New,
            a.Admission_Fname=b.Adm_Log_Fname_New, a.Admission_DOB=b.Adm_Log_Dob_New,a.Admission_Aadhar_Status='1', 
            a.Admission_UID=b.Adm_Log_Uid_New, a.IsNewRecord='Y' where  b.Adm_Log_ApproveStatus='1' and b.Adm_Log_PayStatus='1'
            and b.Adm_Log_TranRefNo='" . $transactionId . "' and a.Admission_Code=b.Adm_Log_AdmissionCode 
           and b.Adm_Log_ITGK_Code='" . $centerCode . "' and a.Admission_Batch='" . $batch . "' and a.Admission_Course='" . $course . "'";
    $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
}

function sendsmstoaadharlearners($transactionId, $centerCode, $course, $batch) {
    $_ObjConnection = dbConnection();
    $query = "SELECT Admission_Name, Admission_LearnerCode, Admission_Mobile FROM tbl_admission WHERE Admission_ITGK_Code = '" . $centerCode . "' AND Admission_TranRefNo = '" . $transactionId . "'  AND Admission_Payment_Status = '1' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
    $_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
    while ($_Row = mysqli_fetch_array($_Responses[2])) {
        $sms = '????? ' . $_Row['Admission_Name'] . ', ???? ??????? RKCL ????? ?? ??????? ?? ??? ??| ?? ???? ????? ?????? ?? ???? ??????????? ??????? ????? ??? RKCL ?? i-Learn ???????? ?? ?????? ?? ????????? ???????? ???| ???? ?????? ?? ??? ??????? ????? ?? ????? ?? ??? ???? ???? ????? ?? RKCL ?? ?????? ????????? ???? ??? ?? ???? ????  ????? ??? (' . $_Row['Admission_LearnerCode'] . ')  ?? ?????? ?? ????? ??? ??? ?????  ?? ?? ???? ???? ???????? ?? ??? ?????|';
        SendSMS($_Row['Admission_Mobile'], $sms);
    }
}

function createAadharTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch) {
    $_ObjConnection = dbConnection();
    $query = "SELECT Adm_Log_Code AS ref_code FROM tbl_adm_log_foraadhar WHERE Adm_Log_ITGK_Code = '" . $centerCode . "' AND Adm_Log_TranRefNo = '" . $transactionId . "'  AND Adm_Log_PayStatus = '1' AND Adm_Log_Course = '" . $course . "' AND Adm_Log_Batch = '" . $batch . "'";
    $_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
    insertInvoice($_Responses, $amount, 'tbl_aadhar_upd_invoice');
}

function updateAadharPaymentStatus($paymentstatus, $transactionId, $centerCode, $course, $batch) {
    $_ObjConnection = dbConnection();

    $updateQuery = "UPDATE tbl_adm_log_foraadhar SET Adm_Log_PayStatus = '" . $paymentstatus . "' WHERE Adm_Log_ITGK_Code = '" . $centerCode . "' AND Adm_Log_TranRefNo = '" . $transactionId . "' AND Adm_Log_Course = '" . $course . "' AND Adm_Log_Batch = '" . $batch . "'";
    $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
}

function insertAadharTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash) {
    $_ObjConnection = dbConnection();
    $insertQuery = "INSERT IGNORE INTO tbl_aadhar_upd_transaction SET 
			Aadhar_Transaction_Status = '" . $transactionStatus . "',
			Aadhar_Transaction_Fname = '" . $firstName . "',
			Aadhar_Transaction_Amount = '" . $amount . "',
			Aadhar_Transaction_Txtid = '" . $transactionId . "',
			Aadhar_Transaction_ProdInfo = '" . $productInfo . "',
			Aadhar_Transaction_CenterCode = '" . $centerCode . "',
			Aadhar_Transaction_RKCL_Txid = '" . $transactionTxId . "',
			Aadhar_Transaction_Course = '" . $course . "',
			Aadhar_Transaction_Batch = '" . $batch . "',
			Aadhar_Transaction_Key = '" . $key . "',
			Aadhar_Transaction_Hash = '" . $posted_hash . "',
			Aadhar_Transaction_Reconcile_Status = 'Reconciled',
			Aadhar_Transaction_DateTime = '" . date("Y-m-d H:i:s") . "'";
    $_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
}

function updateAadharTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash) {
    $_ObjConnection = dbConnection();
    $updateQuery = "
			UPDATE tbl_aadhar_upd_transaction SET 
				Aadhar_Transaction_Status = '" . $transactionStatus . "',
				Aadhar_Transaction_Amount = '" . $amount . "',
				Aadhar_Transaction_Key = '" . $key . "',
				Aadhar_Transaction_Hash = '" . $posted_hash . "',
				Aadhar_Transaction_Reconcile_Status = 'Reconciled'
			WHERE 
				Aadhar_Transaction_Txtid = '" . $transactionId . "' AND 
				Aadhar_Transaction_CenterCode = '" . $centerCode . "' AND 
				Aadhar_Transaction_Course = '" . $course . "' AND 
				Aadhar_Transaction_Batch = '" . $batch . "'";
    $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
}

function checkIfRefundRequiredForAadharFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
    $_ObjConnection = dbConnection();
    $refund = false;
    if (isPaymentTransactionFound($productInfo, $transactionId)) {
        $query = "SELECT COUNT(Adm_Log_TranRefNo) AS AdmissionTxnid, Adm_Log_Course, Adm_Log_Batch, Aadhar_Fee_Amount FROM tbl_adm_log_foraadhar as a left join tbl_aadhar_upd_fee_master as b on Adm_Log_TranRefNo = '" . $transactionId . "' WHERE Adm_Log_ITGK_Code = '" . $centerCode . "' AND Aadhar_Fee_Status='Active' GROUP BY Adm_Log_TranRefNo, Adm_Log_Course, Adm_Log_Batch";

        $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
        if (mysqli_num_rows($response[2]) == 1) {
            $row = mysqli_fetch_array($response[2]);
            $payTxnid = $row['AdmissionTxnid'];
            $course = $row['Adm_Log_Course'];
            $baseAmount = $row['Aadhar_Fee_Amount'];
            // $correctionAmount = $payTxnid * $fee;
            // $baseAmount = 300;
            $admissionAmount = $payTxnid * $baseAmount;
            if ($admissionAmount != $amount) {
                $refund = true;
                //die('c3');
            }
        } else {
            $refund = true;
            //die('c1');
        }
    } else {
        $refund = true;
        //die('c2');
    }

    return $refund;
}

function processToRefundAadharTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
    $_ObjConnection = dbConnection();
    setForAadharPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch);
    updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
    processAadharTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
    updateAadharPaymentStatus(0, $transactionId, $centerCode, $course, $batch);
}

function setForAadharPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch) {
    $_ObjConnection = dbConnection();
    $insertQuery = "INSERT INTO tbl_payment_refund (
			Payment_Refund_Id,
			Payment_Refund_Txnid,
			Payment_Refund_Amount,
			Payment_Refund_Status,
			Payment_Refund_ProdInfo,
			Payment_Refund_ITGK,
			Payment_Refund_RKCL_Txid,
			Payment_Refund_Course,
			Payment_Refund_Batch
		) 
		SELECT CASE WHEN MAX(Payment_Refund_Id) Is NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
		'" . $transactionId . "' AS Payment_Refund_Txnid,
		'" . $amount . "' AS Payment_Refund_Amount,
		'" . $status . "' AS Payment_Refund_Status,
		'" . $productInfo . "' AS Payment_Refund_ProdInfo,
		'" . $centerCode . "' AS Payment_Refund_ITGK,
		'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid,
		'" . $course . "' AS Payment_Refund_Course,
		'" . $batch . "' AS Payment_Refund_Batch 
		FROM tbl_payment_refund";

    $query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "' AND Payment_Refund_Course = '" . $course . "' AND Payment_Refund_Batch = '" . $batch . "'";
    $response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
    if (!mysqli_num_rows($response[2])) {
        $_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
    }
}

/**
 * 	Functions of Aadhar Fee Transactions End
 */

 /**
 * 	Functions for Exp Center Fee Transactions Start
 */
function setSuccessForExpCenterPaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForExpCenterFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmExpCenterTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		} else {
			processToRefundExpCenterTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		}
	}


	function checkIfRefundRequiredForExpCenterFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;

		//$query = "SELECT SUM(pt.Pay_Tran_Amount) AS Pay_Tran_Amount FROM tbl_courseitgk_mapping cm INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = cm.Courseitgk_TranRefNo WHERE cm.Courseitgk_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' AND cm.EOI_Fee_Confirm = '0' GROUP BY cm.Courseitgk_TranRefNo";
		
		
		$query = "SELECT pt.Pay_Tran_Amount FROM tbl_redhat_exp_center cm INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = cm.exp_center_txnid WHERE cm.exp_center_txnid = '" . $transactionId . "'";		
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2]) == 1) {
			$expcenterFee = mysqli_fetch_array($response[2]);
			if ($expcenterFee['Pay_Tran_Amount'] != $amount) {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundExpCenterTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processExpCenterTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		updateExpCenterPayStatus(0, $transactionId, $centerCode);
	}

	function processExpCenterTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_redhat_expcenter_transaction WHERE ExpCenter_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertExpCenterTransaction($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		} else {
			updateExpCenterTransactionStatus($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash);
		}
	}

	function insertExpCenterTransaction($transactionStatus, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_redhat_expcenter_transaction WHERE ExpCenter_Transaction_Status = '" . $transactionStatus . "' AND
					ExpCenter_Transaction_Amount = '" . $amount . "' AND ExpCenter_Transaction_Txtid = '" . $transactionId . "' AND 
					ExpCenter_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_redhat_expcenter_transaction (
				ExpCenter_Transaction_Code,
				ExpCenter_Transaction_Status,
				ExpCenter_Transaction_Fname,
				ExpCenter_Transaction_Amount,
				ExpCenter_Transaction_Txtid,
				ExpCenter_Transaction_ProdInfo,
				ExpCenter_Transaction_CenterCode,
				ExpCenter_Transaction_Hash,
				ExpCenter_Transaction_Key,
				ExpCenter_Payment_Mode,
				ExpCenter_Transaction_Email,
				ExpCenter_Transaction_timestamp
			) 
			SELECT CASE WHEN MAX(ExpCenter_Transaction_Code) IS NULL THEN 1 ELSE MAX(ExpCenter_Transaction_Code) + 1 END AS ExpCenter_Transaction_Code,
			'" . $transactionStatus . "' AS ExpCenter_Transaction_Status,
			'" . $firstName . "' AS ExpCenter_Transaction_Fname,
			'" . $amount . "' AS ExpCenter_Transaction_Amount,
			'" . $transactionId . "' AS ExpCenter_Transaction_Txtid,
			(SELECT Pay_Tran_ProdInfo FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "') AS ExpCenter_Transaction_ProdInfo,
			'" . $centerCode . "' AS ExpCenter_Transaction_CenterCode,
			'" . $posted_hash . "' AS ExpCenter_Transaction_Hash,
			'" . $key . "' AS ExpCenter_Transaction_Key,
			'online' AS ExpCenter_Payment_Mode,
			'" . $email . "' AS ExpCenter_Transaction_Email,
			'" . date("Y-m-d H:i:s") . "' AS ExpCenter_Transaction_timestamp 
			FROM tbl_redhat_expcenter_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateExpCenterTransactionStatus($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$updateQuery = "
			UPDATE tbl_redhat_expcenter_transaction SET 
				ExpCenter_Transaction_Status = '" . $status . "',
				ExpCenter_Transaction_Fname = '" . $firstName . "',
				ExpCenter_Transaction_Amount = '" . $amount . "',
				ExpCenter_Transaction_ProdInfo = (SELECT Pay_Tran_AdmissionArray FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "'),
				ExpCenter_Transaction_Hash = '" . $posted_hash . "',
				ExpCenter_Transaction_Key = '" . $key . "',
				ExpCenter_Payment_Mode = 'online',
				ExpCenter_Transaction_Email = '" . $email . "',
				ExpCenter_Transaction_timestamp = '" . date("Y-m-d H:i:s") . "'
			WHERE 
				ExpCenter_Transaction_Txtid = '" . $transactionId . "' AND 
				ExpCenter_Transaction_CenterCode = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

	function processToConfirmExpCenterTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		processExpCenterTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		updateExpCenterPayStatus(1, $transactionId, $centerCode);
		createExpcenterTransactionInvoices($transactionId, $amount, $centerCode);
	}

	function createExpcenterTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT cm.exp_center_id FROM tbl_redhat_exp_center cm INNER JOIN tbl_payment_transaction pt ON 
					pt.Pay_Tran_PG_Trnid = cm.exp_center_txnid WHERE cm.exp_center_txnid = '" . $transactionId . "' AND 
					pt.Pay_Tran_ITGK = '" . $centerCode . "' AND cm.exp_center_pay_status = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$eoiRow = mysqli_fetch_array($_Responses[2]);
			$eoiTimestamp = date("Y-m-d H:i:s");
			$_Insert = "INSERT INTO tbl_redhat_expcenter_invoice (invoice_no, Itgkcode, expcenter_eoi_code, amount, addtime)
						SELECT CASE WHEN MAX(invoice_no) IS NULL THEN 1000 ELSE MAX(invoice_no) + 1 END AS invoice_no, 
						'" . $centerCode . "' AS Itgkcode, '" . $eoiRow['exp_center_id'] . "' AS expcenter_eoi_code, 
						'" . $amount . "' AS amount, '" . $eoiTimestamp . "' AS addtime FROM tbl_redhat_expcenter_invoice";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		}
	}
	
	function updateExpCenterPayStatus($payStatus, $transactionId, $centerCode) {
		$_ObjConnection = dbConnection();
		//$setFields = "exp_center_pay_status = '" . $payStatus . "'";
		
		$updateQuery = "UPDATE tbl_redhat_exp_center SET exp_center_pay_status = '" . $payStatus . "'
						WHERE exp_center_txnid = '" . $transactionId . "' AND exp_center_code = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

/**
 * 	Functions of Exp Center Fee Transactions End
 */
 
  /**
 * 	Functions for Buy RHLS Fee Transactions Start
 */
function setSuccessForRHLSPaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForRHLSFeeTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmRHLSTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		} else {
			processToRefundRHLSTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		}
	}


	function checkIfRefundRequiredForRHLSFeeTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;

		//$query = "SELECT SUM(pt.Pay_Tran_Amount) AS Pay_Tran_Amount FROM tbl_courseitgk_mapping cm INNER JOIN tbl_payment_transaction pt ON pt.Pay_Tran_PG_Trnid = cm.Courseitgk_TranRefNo WHERE cm.Courseitgk_TranRefNo = '" . $transactionId . "' AND pt.Pay_Tran_ITGK = '" . $centerCode . "' AND cm.EOI_Fee_Confirm = '0' GROUP BY cm.Courseitgk_TranRefNo";
		
		
		$query = "SELECT pt.Pay_Tran_Amount FROM tbl_buy_RHLS cm INNER JOIN tbl_payment_transaction pt ON 
					pt.Pay_Tran_PG_Trnid = cm.rhls_txnid WHERE cm.rhls_txnid = '" . $transactionId . "'";		
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($response[2]) == 1) {
			$rhlsFee = mysqli_fetch_array($response[2]);
			if ($rhlsFee['Pay_Tran_Amount'] != $amount) {
				$refund = true;
			}
		} else {
			$refund = true;
		}

		return $refund;
	}

	function processToRefundRHLSTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		setForPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processRHLSTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		updateRHLSPayStatus(0, $transactionId, $centerCode);
	}

	function processRHLSTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount,
					$key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_buy_rhls_transaction WHERE RHLS_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			insertRHLSTransaction($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId,
			$key, $posted_hash);
		} else {
			updateRHLSTransactionStatus($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode,
				$transactionTxId, $key, $posted_hash);
		}
	}

	function insertRHLSTransaction($transactionStatus, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode,
			$transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * FROM tbl_buy_rhls_transaction WHERE RHLS_Transaction_Status = '" . $transactionStatus . "' AND
					RHLS_Transaction_Amount = '" . $amount . "' AND RHLS_Transaction_Txtid = '" . $transactionId . "' AND 
					RHLS_Transaction_CenterCode = '" . $centerCode . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$insertQuery = "INSERT INTO tbl_buy_rhls_transaction (
				RHLS_Transaction_Code,
				RHLS_Transaction_Status,
				RHLS_Transaction_Fname,
				RHLS_Transaction_Amount,
				RHLS_Transaction_Txtid,
				RHLS_Transaction_ProdInfo,
				RHLS_Transaction_CenterCode,
				RHLS_Transaction_Hash,
				RHLS_Transaction_Key,
				RHLS_Payment_Mode,
				RHLS_Transaction_Email,
				RHLS_Transaction_timestamp
			) 
			SELECT CASE WHEN MAX(RHLS_Transaction_Code) IS NULL THEN 1 ELSE MAX(RHLS_Transaction_Code) + 1 END AS RHLS_Transaction_Code,
			'" . $transactionStatus . "' AS RHLS_Transaction_Status,
			'" . $firstName . "' AS RHLS_Transaction_Fname,
			'" . $amount . "' AS RHLS_Transaction_Amount,
			'" . $transactionId . "' AS RHLS_Transaction_Txtid,
			(SELECT Pay_Tran_ProdInfo FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "') AS RHLS_Transaction_ProdInfo,
			'" . $centerCode . "' AS RHLS_Transaction_CenterCode,
			'" . $posted_hash . "' AS RHLS_Transaction_Hash,
			'" . $key . "' AS RHLS_Transaction_Key,
			
			'online' AS RHLS_Payment_Mode,
			'" . $email . "' AS RHLS_Transaction_Email,
			'" . date("Y-m-d H:i:s") . "' AS RHLS_Transaction_timestamp 
			FROM tbl_buy_rhls_transaction";
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

	function updateRHLSTransactionStatus($status, $firstName, $email, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$updateQuery = "
			UPDATE tbl_buy_rhls_transaction SET 
				RHLS_Transaction_Status = '" . $status . "',
				RHLS_Transaction_Fname = '" . $firstName . "',
				RHLS_Transaction_Amount = '" . $amount . "',
				RHLS_Transaction_ProdInfo = (SELECT Pay_Tran_AdmissionArray FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $transactionId . "'),
				RHLS_Transaction_Hash = '" . $posted_hash . "',
				RHLS_Transaction_Key = '" . $key . "',
				RHLS_Payment_Mode = 'online',
				RHLS_Transaction_Email = '" . $email . "',
				RHLS_Transaction_timestamp = '" . date("Y-m-d H:i:s") . "'
			WHERE 
				RHLS_Transaction_Txtid = '" . $transactionId . "' AND 
				RHLS_Transaction_CenterCode = '" . $centerCode . "'";
		$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
	}

	function processToConfirmRHLSTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash) {
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		processRHLSTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $email, $amount, $key, $posted_hash);
		updateRHLSPayStatus(1, $transactionId, $centerCode, $firstName);
		createRHLSTransactionInvoices($transactionId, $amount, $centerCode);
	}

	function createRHLSTransactionInvoices($transactionId, $amount, $centerCode) {
		$_ObjConnection = dbConnection();
		$query = "SELECT cm.rhls_id FROM tbl_buy_rhls cm INNER JOIN tbl_payment_transaction pt ON 
					pt.Pay_Tran_PG_Trnid = cm.rhls_txnid WHERE cm.rhls_txnid = '" . $transactionId . "' AND 
					pt.Pay_Tran_ITGK = '" . $centerCode . "' AND cm.rhls_pay_status = '1'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			$eoiRow = mysqli_fetch_array($_Responses[2]);
			$eoiTimestamp = date("Y-m-d H:i:s");
			$_Insert = "INSERT INTO tbl_buy_rhls_invoice (invoice_no, Itgkcode, RHLS_eoi_code, amount, addtime)
						SELECT CASE WHEN MAX(invoice_no) IS NULL THEN 1000 ELSE MAX(invoice_no) + 1 END AS invoice_no, 
						'" . $centerCode . "' AS Itgkcode, '" . $eoiRow['rhls_id'] . "' AS RHLS_eoi_code, 
						'" . $amount . "' AS amount, '" . $eoiTimestamp . "' AS addtime FROM tbl_buy_rhls_invoice";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
		}
	}
	
function updateRHLSPayStatus($payStatus, $transactionId, $centerCode, $firstName) {
		$_ObjConnection = dbConnection();
		//$setFields = "exp_center_pay_status = '" . $payStatus . "'";
		$paydate = date("Y-m-d H:i:s");
		$updateQuery = "UPDATE tbl_buy_rhls SET rhls_pay_status = '" . $payStatus . "',payment_date='".$paydate."'
						WHERE rhls_txnid = '" . $transactionId . "' AND rhls_code = '" . $centerCode . "'";
		$_Responsess = $_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
		$query1 = "Select rhls_id from tbl_buy_rhls where rhls_txnid = '" . $transactionId . "' AND rhls_code = '" . $centerCode . "'";
		$_Respond = $_ObjConnection->ExecuteQuery($query1, Message::SelectStatement);
			$rows=mysqli_fetch_array($_Respond[2]);							 
			$ackno = $rows['rhls_id'];		
				if ($_Responsess[0] == 'Successfully Updated') {
					
					$query = "Select User_MobileNo from tbl_user_master where User_LoginId='".$centerCode."'";
					$_Resp = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
						if($_Resp[0]=="Success"){
							 $_SMS = "Dear " . $firstName . ", You Have Subscribed Yourself For RED HAT Learning Subscription(RHLS) to RKCL. Generally RED HAT Learning Subscription Details Delivery Take 7 Working Days. Your Acknowledgement Number Is ".$ackno;
							 $row=mysqli_fetch_array($_Resp[2]);							 
							 $_Mobile = $row['User_MobileNo'];
							 SendSMS($_Mobile, $_SMS);
						}
                   
                }
	}
/**
 * 	Functions of Buy RHLS Fee Transactions End
 */
 
 /**
*	Functions for Learner Enquiry Transactions Start
*/
	function setSuccessForLearnerEnquiryPaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $firstName, $amount, $verifyBy, $key, $posted_hash) {
		$refund = checkIfRefundRequiredForLearnerEnquiryTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmAdmissionEnquiryTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		} else {
			processToRefundAdmissionEnquiryTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		}
	}

	function processAdmissionEnquiryTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * From tbl_admission_transaction WHERE Admission_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		$numRows = mysqli_num_rows($response[2]);
		if(!$numRows) {
			insertAdmissionEnquiryTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash);
		} else {
			updateAdmissionEnquiryTransactionStatus($status, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash);
		}
	}

	function processToConfirmAdmissionEnquiryTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		updateAdmissionEnquiryPaymentStatus(1, $transactionId, $centerCode, $course, $batch);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createAdmissionEnquiryTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch);
		processAdmissionEnquiryTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		sendsmstoEnquirylearners($transactionId, $centerCode, $course, $batch);
	}
	
	function sendsmstoEnquirylearners($transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Admission_Name, Admission_LearnerCode, Admission_Mobile FROM tbl_admission WHERE Admission_ITGK_Code = '" . $centerCode ."' AND Admission_TranRefNo = '" . $transactionId . "'  AND Admission_Payment_Status = '1' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		while ($_Row = mysqli_fetch_array($_Responses[2])) {
			$sms = 'प्रिय ' . $_Row['Admission_Name'] . ', आपका एड्मिशन RKCL सर्वर पर कन्फ़र्म हो गया है| आप अपने ज्ञान केंद्र पर अपना बायोमेट्रिक पंजीकरण करवाए तथा RKCL के i-Learn सॉफ्टवेर के माध्यम से प्रशिक्षण प्रारम्भ करे| आपके द्वारा दी गयी रजिस्टर डीटेल को देखने के लिए गूगल प्ले स्टोर से RKCL की मोबाइल एप्लिकेशन डाउन लोड कर उसमे अपने  लर्नर कोड (' . $_Row['Admission_LearnerCode'] . ')  की सहायता से लॉगिन करे एवं लर्नर  को दी जाने वाली सर्विसेस का लाभ उठाये|';
			SendSMS($_Row['Admission_Mobile'], $sms);
		}
	}
	
	function createAdmissionEnquiryTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT Admission_Code AS ref_code FROM tbl_admission WHERE Admission_ITGK_Code = '" . $centerCode ."' AND Admission_TranRefNo = '" . $transactionId . "'  AND Admission_Payment_Status = '1' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		insertAdmissionEnquiryInvoice($_Responses, $amount, 'tbl_admission_invoice', $batch);
	}

	
	function insertAdmissionEnquiryInvoice($_Responses, $amount, $table, $batch) {
		$_ObjConnection = dbConnection();
		$admissionTimestamp = date("Y-m-d H:i:s");
		if($_Responses[0] == "Success") {
			$count = mysqli_num_rows($_Responses[2]);
			$amt = ($amount / $count);
			$refField = ($table == 'tbl_correction_invoice') ? 'transaction_ref_id' : (($table == 'tbl_reexam_invoice') ? 'exam_data_code' : 'invoice_ref_id');
			while ($_Row = mysqli_fetch_array($_Responses[2])) {
				$_Insert = "INSERT IGNORE INTO $table (invoice_no, " . $refField . ", amount, batch_code, addtime)
							SELECT (MAX(invoice_no) + 1) AS invoice_number, '" . $_Row['ref_code'] . "' AS invoice_ref_id, 
							'" . $amt . "' AS amount, '" . $batch . "' AS batch_code, '" . $admissionTimestamp . "' AS addtime 
							FROM $table";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
			}
		}
	}
	
	function updateAdmissionEnquiryPaymentStatus($paymentstatus, $transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "UPDATE tbl_admission SET Admission_Payment_Status = '" . $paymentstatus . "', Admission_Date_Payment = now() WHERE Admission_ITGK_Code = '" . $centerCode . "' AND Admission_TranRefNo = '" . $transactionId . "' AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}
	
	function insertAdmissionEnquiryTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT IGNORE INTO tbl_admission_transaction SET 
			Admission_Transaction_Status = '" . $transactionStatus . "',
			Admission_Transaction_Fname = '" . $firstName . "',
			Admission_Transaction_Amount = '" . $amount . "',
			Admission_Transaction_Txtid = '" . $transactionId . "',
			Admission_Transaction_ProdInfo = '" . $productInfo . "',
			Admission_Transaction_CenterCode = '" . $centerCode . "',
			Admission_Transaction_RKCL_Txid = '" . $transactionTxId . "',
			Admission_Transaction_Course = '" . $course . "',
			Admission_Transaction_Batch = '" . $batch . "',
			Admission_Transaction_Key = '" . $key . "',
			Admission_Transaction_Hash = '" . $posted_hash . "',
			Admission_Transaction_Reconcile_Status = 'Reconciled',
			Admission_Transaction_DateTime = '" . date("Y-m-d H:i:s") . "'";
		$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
	}

	
	function updateAdmissionEnquiryTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
					$updateQuery = "
			UPDATE tbl_admission_transaction SET 
				Admission_Transaction_Status = '" . $transactionStatus . "',
				Admission_Transaction_Amount = '" . $amount . "',
				Admission_Transaction_Key = '" . $key . "',
				Admission_Transaction_Hash = '" . $posted_hash . "',
				Admission_Transaction_Reconcile_Status = 'Reconciled'
			WHERE 
				Admission_Transaction_Txtid = '" . $transactionId . "' AND 
				Admission_Transaction_CenterCode = '" . $centerCode . "' AND 
				Admission_Transaction_Course = '" . $course . "' AND 
				Admission_Transaction_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function checkIfRefundRequiredForLearnerEnquiryTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(Admission_TranRefNo) AS AdmissionTxnid, Admission_Course, Admission_Batch FROM tbl_admission WHERE Admission_TranRefNo = '" . $transactionId . "' AND Admission_ITGK_Code = '" . $centerCode . "' GROUP BY Admission_TranRefNo, Admission_Course, Admission_Batch";
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['AdmissionTxnid'];
			    $course = $row['Admission_Course'];
			    //$baseAmount = (($course == '1') ? 2850 : (($course == '4') ? 2700 : (($course == '5') ? 3000 : 0)));
	//		    $baseAmount = (($course == '1') ? 1000 : (($course == '4') ? 1000 : (($course == '5') ? 1000 : 0)));
				
				$baseAmount = (($course == '1') ? 1000 : (($course == '23') ? 300 : (($course == '4') ? 1000 : (($course == '5') ? 1000 : 0))));
				
				 $admissionAmount = $payTxnid * $baseAmount;
				if ($admissionAmount != $amount) {
					$refund = true;
					//die('c3');
				}
			} else {
				$refund = true;
			//die('c1');
			}
		} else {
			$refund = true;
			//die('c2');
			
		}

		return $refund;
	}

	function processToRefundAdmissionEnquiryTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		setForAdmissionEnquiryPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processAdmissionEnquiryTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		updateAdmissionEnquiryPaymentStatus(0, $transactionId, $centerCode, $course, $batch);
	}

	function setForAdmissionEnquiryPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT INTO tbl_payment_refund (
			Payment_Refund_Id,
			Payment_Refund_Txnid,
			Payment_Refund_Amount,
			Payment_Refund_Status,
			Payment_Refund_ProdInfo,
			Payment_Refund_ITGK,
			Payment_Refund_RKCL_Txid,
			Payment_Refund_Course,
			Payment_Refund_Batch
		) 
		SELECT CASE WHEN MAX(Payment_Refund_Id) Is NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
		'" . $transactionId . "' AS Payment_Refund_Txnid,
		'" . $amount . "' AS Payment_Refund_Amount,
		'" . $status . "' AS Payment_Refund_Status,
		'" . $productInfo . "' AS Payment_Refund_ProdInfo,
		'" . $centerCode . "' AS Payment_Refund_ITGK,
		'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid,
		'" . $course . "' AS Payment_Refund_Course,
		'" . $batch . "' AS Payment_Refund_Batch 
		FROM tbl_payment_refund";

		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "' AND Payment_Refund_Course = '" . $course . "' AND Payment_Refund_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

/**
*	Functions of Learner Enquiry Transactions End
*/

/**
*	Functions for RSCFA Certificate Transactions Start
*/
	function setSuccessForRscfaCertificatePaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $firstName, $amount, $verifyBy, $key, $posted_hash, $paytime) {
		$refund = checkIfRefundRequiredForRscfaCertificateTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmRscfaCertificateTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime);
		} else {
			processToRefundRscfaCertificateTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		}
	}

	function processRscfaCertificateTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * From tbl_apply_rscfa_certificate_transaction WHERE
					Rscfa_Certificate_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		$numRows = mysqli_num_rows($response[2]);
		if(!$numRows) {
			insertRscfaCertificateTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash, $paytime);
		} else {
			updateRscfaCertificateTransactionStatus($status, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash);
		}
	}

	function processToConfirmRscfaCertificateTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime) {
		$_ObjConnection = dbConnection();
		updateRscfaCertificatePaymentStatus(1, $transactionId, $centerCode, $course, $batch, $paytime);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createRscfaCertificateTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch, $paytime);
		processRscfaCertificateTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime);
		sendsmstoRSCFAlearners($transactionId, $centerCode, $course, $batch);
	}
	
	function sendsmstoRSCFAlearners($transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT apply_rscfa_cert_lname, apply_rscfa_cert_lcode, apply_rscfa_cert_lmobile FROM tbl_apply_rscfa_certificate
					WHERE apply_rscfa_cert_itgk = '" . $centerCode ."' AND apply_rscfa_cert_txnid = '" . $transactionId . "'  AND
					apply_rscfa_cert_payment_status = '1' AND apply_rscfa_cert_course = '" . $course . "' AND
					apply_rscfa_cert_batch = '" . $batch . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		while ($_Row = mysqli_fetch_array($_Responses[2])) {
			$sms = 'प्रिय ' . $_Row['apply_rscfa_cert_lname'] . ', आपका ऐप्लीकेशन RKCL सर्वर पर कन्फ़र्म हो गया है| आपके द्वारा दी गयी रजिस्टर डीटेल को देखने के लिए गूगल प्ले स्टोर से RKCL की मोबाइल एप्लिकेशन डाउन लोड कर उसमे अपने  लर्नर कोड (' . $_Row['apply_rscfa_cert_lcode'] . ')  की सहायता से लॉगिन करे एवं लर्नर  को दी जाने वाली सर्विसेस का लाभ उठाये|';
			SendSMS($_Row['apply_rscfa_cert_lmobile'], $sms);
		}
	}
	
	function createRscfaCertificateTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch, $paytime) {
		$_ObjConnection = dbConnection();
		$query = "SELECT apply_rscfa_cert_id AS ref_code FROM tbl_apply_rscfa_certificate WHERE
					apply_rscfa_cert_itgk = '" . $centerCode ."' AND apply_rscfa_cert_txnid = '" . $transactionId . "'  AND apply_rscfa_cert_payment_status = '1' AND apply_rscfa_cert_course = '" . $course . "' AND
					apply_rscfa_cert_batch = '" . $batch . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		insertRscfaCertificateInvoice($_Responses, $amount, 'tbl_apply_rscfa_exam_invoice', $batch, $paytime);
	}

	
	function insertRscfaCertificateInvoice($_Responses, $amount, $table, $batch, $paytime) {
		$_ObjConnection = dbConnection();
		$admissionTimestamp = date("Y-m-d H:i:s");
		if($_Responses[0] == "Success") {
			$count = mysqli_num_rows($_Responses[2]);
			$amt = ($amount / $count);
			
			while ($_Row = mysqli_fetch_array($_Responses[2])) {
				$_Insert = "INSERT IGNORE INTO $table (invoice_no, transaction_ref_id, amount, batch_code, addtime)
							SELECT CASE WHEN MAX(invoice_no) IS NULL THEN 1000 ELSE MAX(invoice_no) + 1 END AS invoice_no,
							'" . $_Row['ref_code'] . "' AS transaction_ref_id, 
							'" . $amt . "' AS amount, '" . $batch . "' AS batch_code, '" . $paytime . "' AS addtime 
							FROM $table";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
			}
		}
	}
	
	
	
	/*function insertInvoice($_Responses, $amount, $table) {
		$_ObjConnection = dbConnection();
		$admissionTimestamp = date("Y-m-d H:i:s");
		if($_Responses[0] == "Success") {
			$count = mysql_num_rows($_Responses[2]);
			$amt = ($amount / $count);
			$refField = ($table == 'tbl_correction_invoice') ? 'transaction_ref_id' : (($table == 'tbl_reexam_invoice') ? 'exam_data_code' : 'invoice_ref_id');
			while ($_Row = mysql_fetch_array($_Responses[2])) {
				$_Insert = "INSERT IGNORE INTO $table (invoice_no, " . $refField . ", amount, addtime) SELECT (MAX(invoice_no) + 1) AS invoice_number, '" . $_Row['ref_code'] . "' AS invoice_ref_id, '" . $amt . "' AS amount, '" . $admissionTimestamp . "' AS addtime FROM $table";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
			}
		}
	}*/
	
	function updateRscfaCertificatePaymentStatus($paymentstatus, $transactionId, $centerCode, $course, $batch, $paytime) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "UPDATE tbl_apply_rscfa_certificate SET apply_rscfa_cert_payment_status = '" . $paymentstatus . "',
			apply_rscfa_cert_payment_date = '".$paytime."' WHERE apply_rscfa_cert_itgk = '" . $centerCode . "' AND
			apply_rscfa_cert_txnid = '" . $transactionId . "' AND apply_rscfa_cert_course = '" . $course . "' AND
			apply_rscfa_cert_batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}
	
	function insertRscfaCertificateTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash, $paytime) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT IGNORE INTO tbl_apply_rscfa_certificate_transaction SET 
			Rscfa_Certificate_Transaction_Status = '" . $transactionStatus . "',
			Rscfa_Certificate_Transaction_Fname = '" . $firstName . "',
			Rscfa_Certificate_Transaction_Amount = '" . $amount . "',
			Rscfa_Certificate_Transaction_Txtid = '" . $transactionId . "',
			Rscfa_Certificate_Transaction_ProdInfo = '" . $productInfo . "',
			Rscfa_Certificate_Transaction_CenterCode = '" . $centerCode . "',
			Rscfa_Certificate_Transaction_RKCL_Txid = '" . $transactionTxId . "',
			Rscfa_Certificate_Transaction_Course = '" . $course . "',
			Rscfa_Certificate_Transaction_Batch = '" . $batch . "',
			Rscfa_Certificate_Transaction_Key = '" . $key . "',
			Rscfa_Certificate_Transaction_Hash = '" . $posted_hash . "',
			Rscfa_Certificate_Transaction_Reconcile_Status = 'Reconciled',
			Rscfa_Certificate_Transaction_DateTime = '".$paytime."'";
		$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
	}

	
	function updateRscfaCertificateTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
					$updateQuery = "
			UPDATE tbl_apply_rscfa_certificate_transaction SET 
				Rscfa_Certificate_Transaction_Status = '" . $transactionStatus . "',
				Rscfa_Certificate_Transaction_Amount = '" . $amount . "',
				Rscfa_Certificate_Transaction_Key = '" . $key . "',
				Rscfa_Certificate_Transaction_Hash = '" . $posted_hash . "',
				Rscfa_Certificate_Transaction_Reconcile_Status = 'Reconciled'
			WHERE 
				Rscfa_Certificate_Transaction_Txtid = '" . $transactionId . "' AND 
				Rscfa_Certificate_Transaction_CenterCode = '" . $centerCode . "' AND 
				Rscfa_Certificate_Transaction_Course = '" . $course . "' AND 
				Rscfa_Certificate_Transaction_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function checkIfRefundRequiredForRscfaCertificateTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(apply_rscfa_cert_txnid) AS AdmissionTxnid, apply_rscfa_cert_course, apply_rscfa_cert_batch FROM tbl_apply_rscfa_certificate WHERE apply_rscfa_cert_txnid = '" . $transactionId . "' AND
			apply_rscfa_cert_itgk = '" . $centerCode . "' GROUP BY apply_rscfa_cert_txnid, apply_rscfa_cert_course,
			apply_rscfa_cert_batch";
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['AdmissionTxnid'];
			    $course = $row['apply_rscfa_cert_course'];
		//	    $baseAmount = ($course == '5') ? 1500 : 0;
				$baseAmount = ($course == '5') ? 1250 : 0;
				$coursefee='';
				 $admissionAmount = $payTxnid * $baseAmount;
					if($course=='5'){
				//		$coursefee='1500';
						  $coursefee='1250';
					}					
					else{
						$coursefee='1';
					}
				 $mode=fmod($amount,$coursefee);
				 $amount=$amount-$mode;
				if ($admissionAmount != $amount) {
					$refund = true;
					//die('c3');
				}
			} else {
				$refund = true;
			//die('c1');
			}
		} else {
			$refund = true;
			//die('c2');
			
		}

		return $refund;
	}

	function processToRefundRscfaCertificateTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		setForRscfaCertificatePaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processRscfaCertificateTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime);
		updateRscfaCertificatePaymentStatus(0, $transactionId, $centerCode, $course, $batch, $paytime);
	}

	function setForRscfaCertificatePaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT INTO tbl_payment_refund (
			Payment_Refund_Id,
			Payment_Refund_Txnid,
			Payment_Refund_Amount,
			Payment_Refund_Status,
			Payment_Refund_ProdInfo,
			Payment_Refund_ITGK,
			Payment_Refund_RKCL_Txid,
			Payment_Refund_Course,
			Payment_Refund_Batch
		) 
		SELECT CASE WHEN MAX(Payment_Refund_Id) Is NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
		'" . $transactionId . "' AS Payment_Refund_Txnid,
		'" . $amount . "' AS Payment_Refund_Amount,
		'" . $status . "' AS Payment_Refund_Status,
		'" . $productInfo . "' AS Payment_Refund_ProdInfo,
		'" . $centerCode . "' AS Payment_Refund_ITGK,
		'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid,
		'" . $course . "' AS Payment_Refund_Course,
		'" . $batch . "' AS Payment_Refund_Batch 
		FROM tbl_payment_refund";

		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "' AND Payment_Refund_Course = '" . $course . "' AND Payment_Refund_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

/**
*	Functions of RSCFA Certificate Transactions End
*/


/**
*	Functions for RSCFA ReExam Transactions Start
*/
	function setSuccessForRscfaReExamPaymentTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $firstName, $amount, $verifyBy, $key, $posted_hash, $paytime) {
		$refund = checkIfRefundRequiredForRscfaReExamTransaction($transactionId, $productInfo, $centerCode, $amount);
		if (!$refund) {
			processToConfirmRscfaReExamTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime);
		} else {
			processToRefundRscfaReExamTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash);
		}
	}

	function processRscfaReExamTransaction($status, $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime) {
		$_ObjConnection = dbConnection();
		$query = "SELECT * From tbl_rscfa_reexam_transaction WHERE Rscfa_ReExam_Transaction_Txtid = '" . $transactionId . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		$numRows = mysqli_num_rows($response[2]);
		if(!$numRows) {
			insertRscfaReExamTransaction($status, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash, $paytime);
		} else {
			updateRscfaReExamTransactionStatus($status, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash);
		}
	}

	function processToConfirmRscfaReExamTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime) {
		$_ObjConnection = dbConnection();
		updateRscfaReExamPaymentStatus(1, $transactionId, $centerCode, $course, $batch, $paytime);
		updatePaymentTransactionStatus('VerifiedAndConfirmed', 'PaymentReceive', 'Reconciled', $transactionId, $centerCode);
		deleteFromPaymentRefund($transactionId, $centerCode);
		createRscfaReExamTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch, $paytime);
		processRscfaReExamTransaction('Success', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime);
		//sendsmstoRSCFAReexanlearners($transactionId, $centerCode, $course, $batch);
	}
	
	/*function sendsmstoRSCFAReexanlearners($transactionId, $centerCode, $course, $batch) {
		$_ObjConnection = dbConnection();
		$query = "SELECT rscfa_reexam_lname, rscfa_reexam_lcode, rscfa_reexam_mobile FROM tbl_rscfa_reexam_application
					WHERE itgkcode = '" . $centerCode ."' AND rscfa_reexam_TranRefNo = '" . $transactionId . "'  AND
					rscfa_reexam_paymentstatus = '1' AND coursename = '" . $course . "' AND
					batchname = '" . $batch . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		while ($_Row = mysql_fetch_array($_Responses[2])) {
			$sms = 'प्रिय ' . $_Row['rscfa_reexam_lname'] . ', आपका ऐप्लीकेशन RKCL सर्वर पर कन्फ़र्म हो गया है| आपके द्वारा दी गयी रजिस्टर डीटेल को देखने के लिए गूगल प्ले स्टोर से RKCL की मोबाइल एप्लिकेशन डाउन लोड कर उसमे अपने  लर्नर कोड (' . $_Row['rscfa_reexam_lcode'] . ')  की सहायता से लॉगिन करे एवं लर्नर  को दी जाने वाली सर्विसेस का लाभ उठाये|';
			SendSMS($_Row['rscfa_reexam_mobile'], $sms);
		}
	}*/
	
	function createRscfaReExamTransactionInvoices($transactionId, $amount, $centerCode, $course, $batch, $paytime) {
		$_ObjConnection = dbConnection();
		$query = "SELECT rscfa_reexam_code AS ref_code FROM tbl_rscfa_reexam_application WHERE
					itgkcode = '" . $centerCode ."' AND rscfa_reexam_TranRefNo = '" . $transactionId . "'  AND rscfa_reexam_paymentstatus = '1' AND coursename = '" . $course . "' AND
					batchname = '" . $batch . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		insertRscfaReExamInvoice($_Responses, $amount, 'tbl_rscfa_reexam_invoice', $batch, $paytime);
	}

	
	function insertRscfaReExamInvoice($_Responses, $amount, $table, $batch, $paytime) {
		$_ObjConnection = dbConnection();
		$admissionTimestamp = date("Y-m-d H:i:s");
		if($_Responses[0] == "Success") {
			$count = mysqli_num_rows($_Responses[2]);
			$amt = ($amount / $count);
			
			while ($_Row = mysqli_fetch_array($_Responses[2])) {
				$_Insert = "INSERT IGNORE INTO $table (invoice_no, transaction_ref_id, amount, batch_code, addtime)
							SELECT CASE WHEN MAX(invoice_no) IS NULL THEN 1000 ELSE MAX(invoice_no) + 1 END AS invoice_no,
							'" . $_Row['ref_code'] . "' AS transaction_ref_id, 
							'" . $amt . "' AS amount, '" . $batch . "' AS batch_code, '" . $paytime . "' AS addtime 
							FROM $table";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
			}
		}
	}
	
	
	
	/*function insertInvoice($_Responses, $amount, $table) {
		$_ObjConnection = dbConnection();
		$admissionTimestamp = date("Y-m-d H:i:s");
		if($_Responses[0] == "Success") {
			$count = mysql_num_rows($_Responses[2]);
			$amt = ($amount / $count);
			$refField = ($table == 'tbl_correction_invoice') ? 'transaction_ref_id' : (($table == 'tbl_reexam_invoice') ? 'exam_data_code' : 'invoice_ref_id');
			while ($_Row = mysql_fetch_array($_Responses[2])) {
				$_Insert = "INSERT IGNORE INTO $table (invoice_no, " . $refField . ", amount, addtime) SELECT (MAX(invoice_no) + 1) AS invoice_number, '" . $_Row['ref_code'] . "' AS invoice_ref_id, '" . $amt . "' AS amount, '" . $admissionTimestamp . "' AS addtime FROM $table";
				$_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
			}
		}
	}*/
	
	function updateRscfaReExamPaymentStatus($paymentstatus, $transactionId, $centerCode, $course, $batch, $paytime) {
		$_ObjConnection = dbConnection();
		
			$updateQuery = "UPDATE tbl_rscfa_reexam_application SET rscfa_reexam_paymentstatus = '" . $paymentstatus . "',
			rscfa_reexam_paydate = '".$paytime."' WHERE itgkcode = '" . $centerCode . "' AND
			rscfa_reexam_TranRefNo = '" . $transactionId . "' AND coursename = '" . $course . "' AND
			batchname = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}
	
	function insertRscfaReExamTransaction($transactionStatus, $firstName, $amount, $transactionId, $productInfo, $centerCode, $transactionTxId, $course, $batch, $key, $posted_hash, $paytime) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT IGNORE INTO tbl_rscfa_reexam_transaction SET 
			Rscfa_ReExam_Transaction_Status = '" . $transactionStatus . "',
			Rscfa_ReExam_Transaction_Fname = '" . $firstName . "',
			Rscfa_ReExam_Transaction_Amount = '" . $amount . "',
			Rscfa_ReExam_Transaction_Txtid = '" . $transactionId . "',
			Rscfa_ReExam_Transaction_ProdInfo = '" . $productInfo . "',
			Rscfa_ReExam_Transaction_CenterCode = '" . $centerCode . "',
			Rscfa_ReExam_Transaction_RKCL_Txid = '" . $transactionTxId . "',
			Rscfa_ReExam_Transaction_Course = '" . $course . "',
			Rscfa_ReExam_Transaction_Batch = '" . $batch . "',
			Rscfa_ReExam_Transaction_Key = '" . $key . "',
			Rscfa_ReExam_Transaction_Hash = '" . $posted_hash . "',
			Rscfa_ReExam_Transaction_Reconcile_Status = 'Reconciled',
			Rscfa_ReExam_Transaction_DateTime = '".$paytime."'";
		$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
	}

	
	function updateRscfaReExamTransactionStatus($transactionStatus, $amount, $transactionId, $centerCode, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
					$updateQuery = "
			UPDATE tbl_rscfa_reexam_transaction SET 
				Rscfa_ReExam_Transaction_Status = '" . $transactionStatus . "',
				Rscfa_ReExam_Transaction_Amount = '" . $amount . "',
				Rscfa_ReExam_Transaction_Key = '" . $key . "',
				Rscfa_ReExam_Transaction_Hash = '" . $posted_hash . "',
				Rscfa_ReExam_Transaction_Reconcile_Status = 'Reconciled'
			WHERE 
				Rscfa_ReExam_Transaction_Txtid = '" . $transactionId . "' AND 
				Rscfa_ReExam_Transaction_CenterCode = '" . $centerCode . "' AND 
				Rscfa_ReExam_Transaction_Course = '" . $course . "' AND 
				Rscfa_ReExam_Transaction_Batch = '" . $batch . "'";
			$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
		
	}

	function checkIfRefundRequiredForRscfaReExamTransaction($transactionId, $productInfo, $centerCode, $amount) {
		$_ObjConnection = dbConnection();
		$refund = false;
		if (isPaymentTransactionFound($productInfo, $transactionId)) {
			$query = "SELECT COUNT(rscfa_reexam_TranRefNo) AS AdmissionTxnid, coursename, batchname FROM tbl_rscfa_reexam_application 
						WHERE rscfa_reexam_TranRefNo = '" . $transactionId . "' AND itgkcode = '" . $centerCode . "'
						GROUP BY rscfa_reexam_TranRefNo, coursename, batchname";
		
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
			if (mysqli_num_rows($response[2]) == 1) {
				$row = mysqli_fetch_array($response[2]);
				$payTxnid = $row['AdmissionTxnid'];
			    $course = $row['coursename'];
			    $baseAmount = ($course == '24') ? 700 : 0;
				$coursefee='';
				 $admissionAmount = $payTxnid * $baseAmount;
					if($course=='24'){
						$coursefee='700';
					}					
					else{
						$coursefee='1';
					}
				 $mode=fmod($amount,$coursefee);
				 $amount=$amount-$mode;
				if ($admissionAmount != $amount) {
					$refund = true;
					//die('c3');
				}
			} else {
				$refund = true;
			//die('c1');
			}
		} else {
			$refund = true;
			//die('c2');
			
		}

		return $refund;
	}

	function processToRefundRscfaReExamTransaction($transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash) {
		$_ObjConnection = dbConnection();
		setForRscfaReExamPaymentRefund('Refund', $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch);
		updatePaymentTransactionStatus('VerifiedAndRefund', 'PaymentToRefund', 'Reconciled', $transactionId);
		processRscfaReExamTransaction('Refund', $transactionId, $productInfo, $centerCode, $transactionTxId, $firstName, $amount, $course, $batch, $key, $posted_hash, $paytime);
		updateRscfaReExamPaymentStatus(0, $transactionId, $centerCode, $course, $batch, $paytime);
	}

	function setForRscfaReExamPaymentRefund($status, $transactionId, $amount, $productInfo, $centerCode, $transactionTxId, $course, $batch) {
		$_ObjConnection = dbConnection();
		$insertQuery = "INSERT INTO tbl_payment_refund (
			Payment_Refund_Id,
			Payment_Refund_Txnid,
			Payment_Refund_Amount,
			Payment_Refund_Status,
			Payment_Refund_ProdInfo,
			Payment_Refund_ITGK,
			Payment_Refund_RKCL_Txid,
			Payment_Refund_Course,
			Payment_Refund_Batch
		) 
		SELECT CASE WHEN MAX(Payment_Refund_Id) Is NULL THEN 1 ELSE MAX(Payment_Refund_Id) + 1 END AS Payment_Refund_Id,
		'" . $transactionId . "' AS Payment_Refund_Txnid,
		'" . $amount . "' AS Payment_Refund_Amount,
		'" . $status . "' AS Payment_Refund_Status,
		'" . $productInfo . "' AS Payment_Refund_ProdInfo,
		'" . $centerCode . "' AS Payment_Refund_ITGK,
		'" . $transactionTxId . "' AS Payment_Refund_RKCL_Txid,
		'" . $course . "' AS Payment_Refund_Course,
		'" . $batch . "' AS Payment_Refund_Batch 
		FROM tbl_payment_refund";

		$query = "SELECT * FROM tbl_payment_refund WHERE Payment_Refund_Txnid = '" . $transactionId . "' AND  Payment_Refund_ITGK = '" . $centerCode . "' AND Payment_Refund_Course = '" . $course . "' AND Payment_Refund_Batch = '" . $batch . "'";
		$response = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (! mysqli_num_rows($response[2])) {
			$_ObjConnection->ExecuteQuery($insertQuery, Message::InsertStatement);
		}
	}

/**
*	Functions of RSCFA ReExam Transactions End
*/
?>