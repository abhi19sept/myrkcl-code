<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Human Resource Details</title>
          <link href="css/jquery-ui.css" rel="Stylesheet" type="text/css" />
          
    </head>

    <body>
        <div class="wrapper">
            <?php
            include './include/header.html';

            include './include/menu.php';

            if (isset($_REQUEST['code'])) {
                echo "<script>var HRCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var HRCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
            <div class="main">
                <h1>Organization HR Details</h1>
                <form name="frmHRDetail" id="frmHRDetail" action="">

                    <table border="0" cellpadding="0" cellspacing="10" width="100%" class="field">
                        <tbody>
                            <tr>
                                <td colspan="6" id="response">

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Designation 
                                </td>
                                <td>:</td>

                                <td>
                                    <select name="ddlDesignation" id="ddlDesignation" class="text">

                                    </select>
                                    
                                </td>
                            </tr>

                            <tr >
                                <td colspan="2">Full Name</td>
                                <td>:</td>
                                <td colspan="3"><input type="text"  name="txtName" id="txtName" placeholder="Full Name"  class="text" maxlength="" /></td>
                            </tr>


                            <tr>
                                <td colspan="2">Gender</td>
                                <td>:</td>
                                <td><input type="radio" name="usergender" id="usergender1" value="Male"> <a>Male </a>
                                    <input type="radio" name="usergender" id="usergender2" value="Female"> <a>Female</a> </td>

                            </tr>

                            <tr >
                                <td colspan="2">Date of Birth</td>
                                <td>:</td>
                                <td colspan="3"><input type="text"  name="txtDob" id="txtDob" placeholder="MM-DD-YYYY"  class="text" maxlength="" /></td>
                            </tr>

                            <tr >
                                <td colspan="2">Mobile No.</td>
                                <td>:</td>
                                <td colspan="3">
                                    <input type="text" name="txtMobile" id="txtMobile" placeholder="Mobile Number"  class="text" maxlength="" /></td>
                            </tr>

                            <tr >
                                <td colspan="2">Email ID</td>
                                <td>:</td>
                                <td colspan="3">
                                    <input type="text" name="txtEmail" id="txtEmail" placeholder="Email ID"  class="text" maxlength="" /></td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    Max. Qualification 
                                </td>
                                <td>:</td>

                                <td>
                                    <select name="ddlQualification1" id="ddlQualification1" class="text">

                                    </select>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Total Work Experience 
                                </td>
                                <td>:</td>

                                <td>
                                    <input type="text" name="txtExperience" id="txtExperience" class="text" placeholder="In Years" />

                                   
                                    
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    Certification Details
                                </td>
                                <td>:</td>

                                <td>
                                    <input type="text" name="txtQualification2" id="txtQualification2"  class="text" maxlength="" />
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    Other Qualification
                                </td>
                                <td>:</td>

                                <td>
                                    <textarea name="txtQualification3" id="txtQualification3" rows="10" cols="40" style="border-style: groove;">
									
                                    </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right">
                                    <p class="btn submit"><input type="submit" id="btnSubmit" name="btnSubmit" value="Submit" /></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
    </body>
    

    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

          
            $(function() {
              $( "#txtDob" ).datepicker({
                changeMonth: true,
                changeYear: true
              });
            });
          

            function FillDesignation() {
                $.ajax({
                    type: "post",
                    url: "common/cfDesignationMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlDesignation").html(data);
                    }
                });
            }

            FillDesignation();

            function FillQyalification() {
                $.ajax({
                    type: "post",
                    url: "common/cfQualificationMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlQualification1").html(data);
                    }
                });
            }
            FillQyalification();


            $("#btnSubmit").click(function () {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfHRDetail.php"; // the script where you handle the form input.

                if (document.getElementById('usergender1').checked) //for radio button
                {
                    var gender_type = 'Male';
                }
                else {
                    gender_type = 'Female';
                }
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name="+txtName.value+"&designation="+ddlDesignation.value+
                            "&gender="+gender_type+"&dob="+txtDob.value+
                            "&mobile="+txtMobile.value+"&email="+txtEmail.value+
                            "&qualification1="+ddlQualification1.value+"&experience="+txtExperience.value+
                            "&qualification2="+txtQualification2.value+"&qualification3="+txtQualification3.value+ ""; // serializes the form's elements.
                     alert(data);
                }
                else
                {
                    data = "action=UPDATE&code=" + UserProfileCode +
                            "&name="+txtName.value+"&designation="+ddlDesignation.value+
                            "&gender="+gender_type+"&dob="+txtDob.value+
                            "&mobile="+txtMobile.value+"&email="+txtEmail.value+
                            "&qualification1="+ddlQualification1.value+"&experience="+txtExperience.value+
                            "&qualification2="+txtQualification2.value+"&qualification3="+txtQualification3.value+ ""; // serializes the form's elements.
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmHRDetail.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("frmHRDetail");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
</html>