<?php

$title = "Online Class URL Report";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var UserParentID=" . $_SESSION['User_ParentId'] . "</script>";
    echo "<script>var UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var UserParentID=0</script>";
    echo "<script>var UserRole=0</script>";
}
echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
?>
    <link rel="stylesheet" href="bootcss/css/bootstrap-datetimepicker.min.css">
    <script src="bootcss/js/moment.min.js"></script>
    <script src="bootcss/js/bootstrap-datetimepicker.min.js"></script>


<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Online Class URL Report</div>
            <div class="panel-body">

                <form name="frmuploadonlineclassurl" id="frmuploadonlineclassurl" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                            </div>
                            <div class="col-md-4 form-group"> 
                                <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                                onchange="try{setCustomValidity('')}catch(e){}">

                                </select>
                            </div> 
                        </div>
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                            </div>
                            <div class="col-md-4 form-group"> 
                                <select id="ddlBatch" name="ddlBatch" class="form-control">
                                    <!--                                <option value="0">All Batch</option>-->
                                </select>
                            </div> 

                        </div>
                        <div class="container">
                            <div class="col-sm-6 form-group"> 
                                <label for="sdate">Select Month:<span class="star">*</span></label>

                            </div> 
                            <div class="form-group col-sm-10">
                                
                                <select id="monthviewtxt" name="monthviewtxt" class="form-control" required="required">
                                        </select>
                            </div>
                        </div>


                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="myModal">
    <div class="modal-content">
        <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal">×</button>
            <h3 id="heading-tittle" class="modal-title">Heading</h3>
        </div>
        <div class="modal-body">
            <div class="container">
            <div id="responses"></div>

            </div>        
            <div id="errorBox"></div>

             <div id="grid2" style="margin-top:5px; width:94%;"> </div>
        </div>
        </div>
</div>

<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">


    $('#txtstartdate').datetimepicker({

        sideBySide: true,
        format: 'YYYY-MM-DD HH:mm',
        widgetPositioning: {horizontal: "auto", vertical: "bottom"}
    });

</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillMonth(prevStatus) {
                $("#monthviewtxt").append($('<option selected="selected">').text("- - Select Month - -").attr('value', ''));

                $.ajax({
                    type: "post",
                    url: "common/cfUploadOnlineClassurl.php",
                    data: "action=FILLMONTH",
                    success: function (data) {
                        //alert(data);
                        var parseData = $.parseJSON(data);
                        $.each(parseData, function (i, v) {
                            if (prevStatus == v.Employee_ID) {
                                $("#monthviewtxt").append($('<option>').text(v.Month_Name).attr('value', v.Month));
                            } else {
                                $("#monthviewtxt").append($('<option>').text(v.Month_Name).attr('value', v.Month));
                            }
                        });
                    }
                });
            }
        FillMonth();
        
        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfUploadOnlineClassurl.php",
                data: "action=FILLCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfUploadOnlineClassurl.php",
                data: "action=FILLBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });




        function showDataAdmin(month) {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfUploadOnlineClassurl.php"; // the script where 

            var data;
            data = "action=EmployeeDetailForOneViewAttendence&course=" + ddlCourse.value + "&batch=" + ddlBatch.value  + "&month=" + month + ""; //
            // alert(data);
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }


        $("#btnSubmit").click(function () {
            var month = $('#monthviewtxt').val();
             if(month === ""){
                 return fasle;
             }else{
                 showDataAdmin(month);
             }

            //showDataAdmin();

            return false; // avoid to execute the actual submit of the form.
        });
        $("#grid").on('click', '.totalupdcount',function(){
                    var autoid = $(this).attr('autoid');


                    
                    var modal = document.getElementById('myModal');
                    var span = document.getElementsByClassName("close")[0];
                    modal.style.display = "block";
                    span.onclick = function() {
                       
                        modal.style.display = "none";                       
                    }

                    $("#heading-tittle").html('View URL');
                    $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
              
            var url = "common/cfUploadOnlineClassurl.php"; // the script where you handle the form input.
            var data;
            
            data = "action=GETURL&autoid=" + autoid + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                   
                    $('#response').empty();
                    $("#grid2").html(data);
                    $('#example2').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });
         });
        $("#grid").on("click","#printbtn",function(){
            //var month = $('#monthviewtxt').text();
            var month = $("#monthviewtxt option:selected").text();
            var divToPrint=document.getElementById('printotdiv');

            var newWin=window.open('','Print-Window');

            newWin.document.open();
            newWin.document.write('<html><head><title>' + month  + ' Attendance One View Report</title></head><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

            newWin.document.close();

            setTimeout(function(){newWin.close();},10);
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<!-- <script src="bootcss/js/frmuploadonlineclassurl.js" type="text/javascript"></script> -->
</html>
