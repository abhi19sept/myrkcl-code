<?php
$title = "Update NCR Feedback";
include ('header.php');
include ('root_menu.php');

echo "<script>var UserRoll='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Update NCR Feedback

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
                <form name="frmselectvisit" id="frmselectvisit" class="form-inline" action=""> 
                        <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Select Center Code:</label>
                            <select id="ddlCC" name="ddlCC" class="form-control" >

                            </select>    
                        </div>
                        <div class="col-sm-4 form-group" id="showdiv"> 
                            <label for="edistrict">Click Here to View Details:</label>
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="View Details"/>    
                        </div>

                    </div> 
                    <div class="panel panel-info" id="main-content" style="display:none;">
                        <div class="panel-heading">ITGK Details</div>
                        <div class="panel-body">

                            <div class="container">
                                <div class="col-sm-4 form-group"> 
                                    <label for="address">ITGK Code:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtITGKCode" id="txtITGKCode" placeholder="Street">
                                    <input type="hidden" class="form-control" readonly="true" name="txtvisitid" id="txtvisitid" placeholder="Street">    
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="address">ITGK Name:</label>
                                    <textarea class="form-control" readonly="true" id="txtITGKName" name="txtITGKName" placeholder="Road"></textarea>

                                </div>
                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Visitor Code:</label>
                                    <input type="text"  class="form-control" readonly="true" name="txtVisitor" id="txtVisitor" placeholder="Name of the Organization/Center">
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">SP Name:</label>
                                    <textarea class="form-control" readonly="true" name="txtspname" id="txtspname" placeholder="Registration No"></textarea>     
                                </div>



                            </div>

                            <div class="container">
                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Do AO have Separate Lab and Theory Rooms:<span class="star">*</span></label> <br/>                               
                                        <input type="text" class="form-control" name="Separate" id="Separate" readonly="true" value="0">     
                                    </div>


                                    <div class="container" id="Yes">


                                        <div class="col-sm-4 form-group" id="theory"> 
                                            <label for="ename">Enter Theory Room Area in Sq.Ft.:</label>
                                            <input type="text" class="form-control" name="TheoryRoomArea" id="TheoryRoomArea" onkeyup="calc()" value="0" placeholder="Theory Room Area" onkeypress="javascript:return allownumbers(event);">     
                                        </div>


                                        <div class="col-sm-4 form-group" id="lab">     
                                            <label for="faname">Enter Total LAB Room Area in Sq.Ft.:</label>
                                            <input type="text" class="form-control" name="LabArea" id="LabArea" onkeyup="calc()" value="0" placeholder="LAB Area" onkeypress="javascript:return allownumbers(event);">
                                        </div>

                                        <div class="col-sm-4 form-group"> 
                                            <label for="edistrict">Total Area (Theory + LAB) in Sq.Ft.:</label>
                                            <input type="text" class="form-control" name="TotalArea" id="TotalArea" placeholder="Total Area">  
                                        </div>
                                    </div>

<!--                                    <div class="container" id="No" style="display:none">
                                        <div class="col-sm-4 form-group"> 
                                            <label for="edistrict">Enter Total Area (Theory + LAB) in Sq.Ft.:</label>
                                            <input type="text" class="form-control" name="TotalArea1" id="TotalArea1" placeholder="Total Area">  
                                        </div>
                                    </div>-->

                                </div>
<br>
      <div class="col-md-2 form-group">     
                        <label for="visit"> Enter Reason to Update Feedback:<span class="star">*</span></label>
                    </div>
                    <div class="col-md-6 form-group"> 
                        <textarea class="form-control" name="Reason" id="Reason" placeholder="Feedback Update reason"></textarea>  
                    </div>

                    <div class="container">
                        <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Update Feedback"/> </a>
                    </div>
                        </div>
                    </div> 
                    
              
                </form>

            </div>

        </div>   
    </div>

</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        
//        
//        getLocation();
//        var lat;  
//        var long;
//    function getLocation() {
//    var run = 0;
//    navigator.geolocation.watchPosition(function(position) {
//         lat = position.coords.latitude; 
//         long = position.coords.longitude;
//        run++;
//        if(run == 1){
//            //FillTrackLocation(lat,long); 
//              FillCenterCode();
//        }else{
//           
//        } 
//    },
//    function(error) {
//        if (error.code == error.PERMISSION_DENIED)
//        alert("Please allow Location in Browser settings to Proceed");
//        return false;
//    });
//}

          
        
        function FillCenterCode() {
            $.ajax({
                type: "post",
                url: "common/cfNcrFeedbackUpdate.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCC").html(data);
                }
            });
        }
        FillCenterCode();
        
          function FillVisitCloseReason() {
            $.ajax({
                type: "post",
                url: "common/cfProcessVisitClose.php",
                data: "action=FILLREASON",
                success: function (data) {
                    $("#ddlreason").html(data);
                }
            });
        }
        
        
         $("#btnShow").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

             $.ajax({
                type: "post",
                url: "common/cfNcrFeedbackUpdate.php",
                data: "action=DETAILS&cc=" + ddlCC.value + "",
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select a RSP for Selection" + "</span></p>");
                    } else {
                        data = $.parseJSON(data);
                        txtvisitid.value = data[0].feedbackid;
                        txtITGKCode.value = data[0].itgkcode;
                        txtITGKName.value = data[0].itgkname;
                        txtVisitor.value = data[0].visitorid;
                        txtspname.value = data[0].spname;
                        Separate.value = data[0].areaseparate;
                        TheoryRoomArea.value = data[0].throeyarea;
                        LabArea.value = data[0].labarea;
                        TotalArea.value = data[0].totalarea;
                        if(data[0].areaseparate == 'No'){
                            $('#TheoryRoomArea').attr('readonly', true);
                            $('#LabArea').attr('readonly', true);
                        }
//                        txtDistrict.value = data[0].district;
//                        txtTehsil.value = data[0].tehsil;
//                        txtStreet.value = data[0].cc;
//                        txtRoad.value = data[0].refno;

                        $("#main-content").show(3000);
                        $("#showdiv").hide(3000);
                        $("#submitdiv").show(3000);
                        FillVisitCloseReason();
                    }

                }
            });
        });

       // showData();

        $("#btnSubmit").click(function () {
           // alert(ddlreason.value);
            if(Reason.value==''){
                //alert("Please Enter Reason to Update Feedback");
                BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/error.gif width=10px /></span><span>Please Enter Reason to Update Feedback.</span></p>");
                return false;
            }
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfNcrFeedbackUpdate.php"; // the script where you handle the form input.
            var data;
                data = "action=ADD&reason=" + Reason.value + "&cc=" + ddlCC.value + "&id=" + txtvisitid.value + "&theory=" + TheoryRoomArea.value + "&lab=" + LabArea.value + "&total=" + TotalArea.value + ""; // serializes the form's elements.
            
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/correct.gif width=10px /></span><span>Feedback Updated Successfully.</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmncrfeedbackupdate.php";
                        }, 1000);

                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });
            //}
            return false; // avoid to execute the actual submit of the form.
        });

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

</html>
