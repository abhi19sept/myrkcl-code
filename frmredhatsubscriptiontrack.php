<?php
$title="Track Subscription Status";
include ('header.php'); 
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4' || $_SESSION['User_UserRoll'] == '14' 
|| $_SESSION['User_UserRoll'] == '7'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}

?>
<style>
    .breadcrumbs {
        position: absolute;
        display: inline;
        display: inline-block;
        left: 0;
        width: 100%;
        padding: 0 20px;
        width: 100%;
        background: rgba(6, 48, 87, 0.9);
        color: #fff;
        font-size: 15px;
        line-height: 35px;
        text-align: left;
        z-index: 0;
    }
    .breadcrumbs a{
       color: #ffffff;
    }
    .breadcrumbs a:hover{
        color: #ffc14f;
    }
    .activecls{
        color: #ffc14f !important
    }
    .heading {
        color: #333;
        font-size: 32px;
        font-weight: 700;
        margin: 0;
        padding: 8px 0 10px;
    }
    .messagecls{
        color: black;
        font-weight: bold;
        font-size: 15px;
    }
    .messageclsalert{
        color: red;
        font-weight: bold;
        font-size: 15px;
    }
</style>
<link href="css/progress-wizard.min.css" rel="stylesheet">
<div style="min-height:430px !important;max-height:auto !important;">   
	<div class="container"> 			
		<div class="panel panel-primary" style="margin-top:20px !important;">  
			<div class="panel-heading">Track Subscription Status</div>
				<div class="panel-body">
					<form id="frmtrackorder" name="frmtrackorder" method="post" id="form" class="form-inline">
						<div class="container">
							<div class="container">
								<div id="response"></div>
							</div>        
								<div id="errorBox"></div>
            
                        <div class="col-sm-4">
                            <label for="email" class="col-form-label">ACKNOWLEDGEMENT NUMBER <small>*</small></label>
                            <input type="text" name="txtacknownumber" id="txtacknownumber" class="form-control" placeholder="ACKNOWLEDGEMENT NUMBER" maxlength="10" onkeypress="javascript:return allownumbers(event);">
                            <div id="first_name_feedback" class="invalid-feedback">

                            </div>
                        </div>
                       
                        <div class="form-group col-sm-4">
                            
                                <button type="button" class="btn btn-primary" id="btnSubmittrackorder" name="btnSubmittrackorder"
									style="margin-left: 0;width: 40%;margin-top: 25px;">Submit</button>
                           
                        </div>
                        
                    </div>   
                </div>
            </div>
    </form>
    <div id="resulttrack" style="margin-top:60px;">
        
    </div>
</div>

<?php include'common/message.php'; ?>
<script type="text/javascript">
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
$(document).ready(function () {
    $("#btnSubmittrackorder").click(function ()
    {
        if ($("#frmtrackorder").valid())
        {
            $('#response').empty();
            $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
            var url = "common/cfRedHatAdmission.php"; 
            var data;
            var forminput = $("#frmtrackorder").serialize();
            data = "action=TRACKSTATUS&" + forminput; 
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                   if (data == "Mobile Number Or Acknowledgement Number Not Exist With Us")
                    {
                         $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif style='width: 20px;' /></span><span style='color:red;font-size: 15px;margin: 5px; font-weight: bold;'>" + data + "</span></p>");
                        
                    } 
                    else
                    {
                        $('#response').empty();
                        $('#resulttrack').html(data);
                    } 
                }
            });
        }
        return false;
    });
    $("#frmtrackorder").validate({
        rules: {
            txtacknownumber: { required: true },
            txtmobileno: { required: true }
        },			
        messages: {		
            txtacknownumber: { required: '<span style="color:red; font-size: 12px;">Please Enter Acknowledgement Number</span>' },
            txtmobileno: { required: '<span style="color:red; font-size: 12px;">Please Enter Mobile Number</span>' }		
        },
    });
});
</script>

<?php include('footer.php'); ?> 