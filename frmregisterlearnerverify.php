<?php
$title="Bio-Matric";
include ('header.php'); 
include ('root_menu.php');	
if (isset($_REQUEST['code'])) {
        echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
        echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    } else {
        echo "<script>var UserCode=0</script>";
        echo "<script>var Mode='Add'</script>";
    }	   
?>
 
	<div class="container">
		<div class="panel panel-primary" style="margin-top:36px;">
			<div class="panel-heading">First time capture may take time, so wait after click the button "Click To Capture"</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
						<form name="frmLearnerCapture" id="frmLearnerCapture" class="form-inline" role="form" enctype="multipart/form-data"> 
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>												
						</div>
				 <div class="container">
							 <div class="col-md-4 form-group">     
                                 <img id="imgFinger" name="imgFinger"  class="form-control" style="width:120px;height:150px;" alt="" /> <br/><br/>	
								 <input id="btnCaptureAndMatch" type="submit" value="Capture and Match" width="150px" onclick="return Match()" />
							 </div>
						</div>
						
			<div style="display:none;">
			 <div class="container">
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Serial No:<span class="star">*</span></label>
                                    <input type="text" name="tdSerial" id="tdSerial" class="form-control" readonly="true"/>
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Make:<span class="star">*</span></label>
                                    <input type="text" name="tdMake" id="tdMake" class="form-control" readonly="true"/>
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Model:<span class="star">*</span></label>
                                    <input type="text" name="tdModel" id="tdModel" class="form-control" readonly="true"/>
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Width:<span class="star">*</span></label>
                                    <input type="text" name="tdWidth" id="tdWidth" class="form-control" readonly="true"/>
							 </div>                                                        
                        </div>
						
						<div class="container">							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Height:<span class="star">*</span></label>
                                    <input type="text" name="tdHeight" id="tdHeight"  class="form-control" readonly="true"/>
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Local MAC:<span class="star">*</span></label>
                                    <input type="text" name="tdLocalMac" id="tdLocalMac"  class="form-control" readonly="true"/>
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Local IP:<span class="star">*</span></label>
                                    <input type="text" name="tdLocalIP" id="tdLocalIP" class="form-control" readonly="true"/>
							 </div>

							<div class="col-md-4 form-group">     
                                <label for="batch"> Status:<span class="star">*</span></label>
                                    <input type="text" name="txtStatus" id="txtStatus" class="form-control" readonly="true"/>
							 </div> 
						</div>			
			</div>										
						
						<div class="container">							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> Quality:<span class="star">*</span></label>
                                    <input type="text" name="txtQuality" id="txtQuality" value="" class="form-control" readonly="true"/>
									<input type="hidden" name="admission_code" id="admission_code" value="<?php echo $_REQUEST['code']; ?>">
							 </div> 
							 
							 <div class="col-md-4 form-group">     
                                <label for="batch"> NFIQ:<span class="star">*</span></label>
                                    <input type="text" name="txtNFIQ" id="txtNFIQ" value="" class="form-control" readonly="true"/>
							 </div> 							  
						</div>
					
					<div style="display:none;">
						<div class="container">							 
							 <div class="col-md-10 ">     
                                <label for="batch"> Base64Encoded ISO Template:<span class="star">*</span></label>
                                    <textarea id="txtIsoTemplate" rows="4" cols="12" name="txtIsoTemplate" class="form-control" readonly="true"></textarea>
							 </div> 
					    </div>
						
						<div class="container">							 
							 <div class="col-md-10">     
                                <label for="batch"> Base64Encoded ISO Image:<span class="star">*</span></label>
                                    <textarea id="txtIsoImage" rows="4" cols="12" name="txtIsoImage" class="form-control" readonly="true"></textarea>
							 </div> 
					    </div>
						
						<div class="container">							 
							 <div class="col-md-10">     
                                <label for="batch"> Base64Encoded Raw Data:<span class="star">*</span></label>
                                    <textarea id="txtRawData" rows="4" cols="12" name="txtRawData" class="form-control" readonly="true"></textarea>
							 </div> 
					    </div>
						
						<div class="container">							 
							 <div class="col-md-10">     
                                <label for="batch"> Base64Encoded Wsq Image Data:<span class="star">*</span></label>
                                    <textarea id="txtWsqData" rows="4" cols="12" name="txtWsqData" class="form-control" readonly="true"></textarea>
							 </div> 
					    </div>
					</div>	
					
					<div id="grid" name="grid" style="margin-top:35px;"> </div> 
					
					 <div class="container" style="margin-top:20px; margin-left:15px;">						
						   <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/> 					 
                     </div>							
                </div>
            </div>   
        </div>
    </form>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
</body>

<script src="js/jquery-1.8.2.js"></script>
<script src="js/mfs100.js"></script>

<script language="javascript" type="text/javascript">        
	var quality = 60; //(1 to 100) (recommanded minimum 55)
	var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )
		
		function Match() {
            try {
                var isotemplate = document.getElementById('txtIsoTemplate').value;
                var res = MatchFinger(quality, timeout, isotemplate);			
				 var res1 = CaptureFinger(quality, timeout);
         
                if (res.httpStaus) {
					document.getElementById('txtStatus').value = "ErrorCode: " + res1.data.ErrorCode + " ErrorDescription: " + res1.data.ErrorDescription;
                    if (res.data.Status) {
						document.getElementById('imgFinger').src = "data:image/bmp;base64," + res1.data.BitmapData;
                        alert("Finger matched");
                    }
                    else {
                        if (res.data.ErrorCode != "0") {
                            alert(res.data.ErrorDescription);
                        }
                        else {
                            alert("Finger not matched");
                        }
                    }
                }
                else {
                    alert(res.err);
                }
            }
            catch (e) {
                alert(e);
            }
            return false;
        }				
</script>

<script type="text/javascript">
	var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
	var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
	var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
	var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
	
		$(document).ready(function () {			
			var quality = 60; //(1 to 100) (recommanded minimum 55)
			var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

        function GetInfo() {			
            document.getElementById('tdSerial').value = "";
            document.getElementById('tdMake').value = "";
            document.getElementById('tdModel').value = "";
            document.getElementById('tdWidth').value = "";
            document.getElementById('tdHeight').value = "";
            document.getElementById('tdLocalMac').value = "";
            document.getElementById('tdLocalIP').value = "";

            var res = GetMFS100Info();
            if (res.httpStaus) {
                document.getElementById('txtStatus').value = res.data.ErrorDescription;
					if (res.data.ErrorCode == "0") {
						document.getElementById('tdSerial').value = res.data.DeviceInfo.SerialNo;
						document.getElementById('tdMake').value = res.data.DeviceInfo.Make;
						document.getElementById('tdModel').value = res.data.DeviceInfo.Model;
						document.getElementById('tdWidth').value = res.data.DeviceInfo.Width;
						document.getElementById('tdHeight').value = res.data.DeviceInfo.Height;
						document.getElementById('tdLocalMac').value = res.data.DeviceInfo.LocalMac;
						document.getElementById('tdLocalIP').value = res.data.DeviceInfo.LocalIP;
					}
			}
            else {
                alert(res.err);
            }
            return false;
        }		
		GetInfo();
	
		function Capture() {
            try {
                document.getElementById('txtStatus').value = "";
                document.getElementById('imgFinger').src = "data:image/bmp;base64,";
                document.getElementById('txtQuality').value = "";
                document.getElementById('txtNFIQ').value = "";
                document.getElementById('txtIsoTemplate').value = "";
                document.getElementById('txtIsoImage').value = "";
                document.getElementById('txtRawData').value = "";
                document.getElementById('txtWsqData').value = "";

                var res = CaptureFinger(quality, timeout);
                if (res.httpStaus) {
                    document.getElementById('txtStatus').value = "ErrorCode: " + res.data.ErrorCode + " ErrorDescription: " + res.data.ErrorDescription;

                    if (res.data.ErrorCode == "0") {
                        document.getElementById('imgFinger').src = "data:image/bmp;base64," + res.data.BitmapData;
                        document.getElementById('txtQuality').value = res.data.Quality;
                        document.getElementById('txtNFIQ').value = res.data.Nfiq;
                        document.getElementById('txtIsoTemplate').value = res.data.IsoTemplate;
                        document.getElementById('txtIsoImage').value = res.data.IsoImage;
                        document.getElementById('txtRawData').value = res.data.RawData;
                        document.getElementById('txtWsqData').value = res.data.WsqImage;
                    }
                }
                else {
                    alert(res.err);
                }
            }
            catch (e) {
                alert(e);
            }
            return false;
        }

	   function Verify() {
            try {
                var isotemplate = document.getElementById('txtIsoTemplate').value;				
                var res = VerifyFinger(isotemplate, isotemplate);			

                if (res.httpStaus) {
                    if (res.data.Status) {
                        //alert("Finger matched");
                    }
                    else {
                        if (res.data.ErrorCode != "0") {
                            alert(res.data.ErrorDescription);
                        }
                        else {
                            //alert("Finger not matched");
                        }
                    }
                }
                else {
                    alert(res.err);
                }			
            }
            catch (e) {
                alert(e);
            }
            return false;
        }        
			
	    var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.       
			$.ajax({
				type: "POST",
				url: url,
				data: "action=GetVerifyLearnerData&values=" + AdmissionCode + "",
				success: function (data)
				{				
					data = $.parseJSON(data);							
							admission_code.value = data[0].AdmissionCode;
							txtQuality.value = data[0].quality;
							txtNFIQ.value = data[0].nfiq;
							txtIsoTemplate.value = data[0].template;
							txtIsoImage.value= data[0].image;
							txtRawData.value=data[0].raw;
							txtWsqData.value=data[0].wsq;
					Verify();
				}
		});
			
		$("#btnSubmit").click(function () {        	 
		$('#response').empty();
		$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
		var url = "common/cfbiomatricenroll.php"; // the script where you handle the form input.
	    var data;
		var forminput=$("#frmLearnerCapture").serialize();
	   if (Mode == 'In') {
			data = "action=In&" + forminput;
		}
		else{
			data = "action=Out&" + forminput;
		}
			
			$.ajax({
				type: "POST",
				url: url,
				data: data,
				success: function (data)
				{
					if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
					{
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
						 window.setTimeout(function () {
							window.location.href = "frmverifylearner.php";
						}, 1000);
						Mode = "Add";
						resetForm("frmLearnerCapture");
					}
					else
					{
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
					}
					//showData();
				}
			}); 
		return false; // avoid to execute the actual submit of the form.
	});
});
</script>
</html>