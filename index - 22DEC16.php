<?php
include ('outer_header.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>


<div class="container" style="min-height:350px !important;max-height:1500px !important">
    <div class="row"  >
        <div class="col-md-12 col-md-12"  style="margin-top:35px;">
            <div class="panel panel-default">
                <div class="panel-heading" style="
                     background: rgba(0, 102, 255, 0.97) none repeat scroll 0 0 !important;color:#fff;"> <span class="glyphicon glyphicon-list-alt"></span><b>Important Information</b></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="demo11">
                                <li class="news-item1">
                                    <table cellpadding="4">
                                        <tr>
                                            <td><img src="images/blue-important-icon.png" width="48" class="img-circle" /></td>
                                            <td>The programs and data stored on this system are licensed to or are the property of RKCl. This is a private computing system for use only by authorized users. Unauthorized access to any program or data on this system is not permitted. By accessing and using this system you are consenting to system monitoring for law enforcement and other purposes. Unauthorized use of this system may subject you to criminal prosecution and penalties.</td>
                                        </tr>
                                    </table>
                                </li>


                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-6 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"  style="
                     background: rgba(0, 102, 255, 0.97) none repeat scroll 0 0 !important;color:#fff;" > <span class="glyphicon glyphicon-list-alt"></span>
                    <b>Login</b>

                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" name="frmlogin" id="frmlogin">
                        <div id="response"></div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">User Name</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="txtUserName" name="txtUserName" placeholder="Username" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="txtPassword" name="txtPassword" placeholder="Password" type="password">
                            </div>
                        </div>
						<div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Enter the code</label>
                            <div class="col-sm-9">
                               <img src="captcha_code_file.php?rand=<?php echo rand(); ?>" id='captchaimg' >
							   
							   
							   <input class="input-sm"   id="6_letters_code" name="6_letters_code" type="text"  placeholder="Enter Captcha">
							  
								<br>

								 
								 <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
								 
                            </div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <label class="option block">
                                    <a href="forgot_password.php">Forgot Password ?</a>            
                                </label>                                       
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" name="btnSubmit"  id="btnSubmit" class="btn btn-success btn-sm">Sign in</button>
                                <button type="reset" class="btn btn-default btn-sm">Reset</button>
                              
                            </div>
                        </div>
                    </form>
                </div>




            </div>
        </div>




        <div class="col-md-6 col-md-6" style="float:right;">
            <div class="panel panel-default" >
                <div class="panel-heading" style="
                     background: rgba(0, 102, 255, 0.97) none repeat scroll 0 0 !important;color:#fff;" > <span class="glyphicon glyphicon-list-alt"></span><b>News</b></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12" style="height:258px !important">
                            <ul class="demo1">
                                <div id="news"> </div>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>




    </div>
</div>

<?php include ('./common/message.php');
include ('footer.php');
?>
</div>  
	<script language='JavaScript' type='text/javascript'>
function refreshCaptcha()
{
	var img = document.images['captchaimg'];
	img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
}
</script>

<script>
    $(document).ready(function () {
        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfLogin.php",
                data: "action=FILL1",
                success: function (data) {

                    $("#news").html(data);

                }
            });
        }
        showData();




    });
</script>
<script type="text/javascript">
    var Success = "<?php echo Message::SuccessfullyFetch; ?>";
    $("#btnSubmit").click(function () {
        //alert('Button Click');
        //alert(txtUserName.value);
        //alert(txtPassword.value);
        $('#response').empty();

        $('#response').append("<p class='state-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Authenticating.....</span></p>");
        var url = "./common/cfLogin.php"; // the script where you handle the form input.
        var data;
        var forminput = $("#frmlogin").serialize();
        if (Mode == 'Add')
        {
            data = "action=CheckLogin&" + forminput; // serializes the form's elements.
        }
        //alert(data)
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (data)
            {
               //alert(data);
                // show response from the php script.
                if (data === Success)
                {
                    //alert(data);
                    $('#response').empty();
                    $('#response').append("<p class='state-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                    window.setTimeout(function ()
                    {
                        SuccessfullyLogin();
                    }, 1000);

                }
				else if(data == 'EMPTY')
                        {
                            $('#response').empty();
                           $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span><font color=red>" + "   Please Enter User Name and Password." + "</font></span></p>");
                            resetForm("frmlogin");
                        }
						else if(data == 'No')
                        {
                            $('#response').empty();
                           $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span><font color=red>" + "   Invalid Captcha. Please try again." + "</font></span></p>");
                            resetForm("frmlogin");

				}
                else
                {
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/warning.jpg width=20px height=20px /></span><span><font color=red>" + "   Invalid Username or Password. Please try again." + "</font></span></p>");
                    resetForm("frmlogin");
                }

            }

        });

        return false; // avoid to execute the actual submit of the form.
    });
    function SuccessfullyLogin()
    {

        window.location.href = 'dashboard.php';
    }
    function resetForm(formid) {
        $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
    }

    $(function () {
        $(".demo1").bootstrapNews({
            newsPerPage: 3,
            autoplay: true,
            pauseOnHover: true,
            direction: 'up',
            newsTickerInterval: 4000,
            onToDo: function () {
                //console.log(this);
            }
        });

        $(".demo2").bootstrapNews({
            newsPerPage: 4,
            autoplay: true,
            pauseOnHover: true,
            navigation: false,
            direction: 'down',
            newsTickerInterval: 2500,
            onToDo: function () {
                //console.log(this);
            }
        });

        $("#demo3").bootstrapNews({
            newsPerPage: 3,
            autoplay: false,
            onToDo: function () {
                //console.log(this);
            }
        });
    });



</script>
