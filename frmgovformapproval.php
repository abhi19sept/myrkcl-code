<?php
$title = "Govt. Reimbursement Approval";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}

if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '30' || $_SESSION['User_UserRoll'] == '11' || $_SESSION['User_UserRoll'] == '18') {
//print_r($_SESSION);
?>
<div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">Govt. Reimbursement Approval</div>
        <div class="panel-body">
            <form name="frmgovapproved" id="frmgovapproved" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>			
                    
					 <div class="col-sm-4 form-group">     
                        <label for="batch"> CenterCode:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" name="txtCentercode" id="txtCentercode" placeholder="CenterCode">									
                    </div> 	
					
					<div class="col-sm-1">     
                        <label for="batch"> OR:</label>
                       									
                    </div> 
					
					 <div class="col-md-4 form-group"> 
                                <label for="lcode">LearnerCode:<span class="star">*</span></label>
								  <input type="text" name="txtlcode" id="txtlcode" class="form-control" placeholder="LearnerCode">
                     </div>
					
					<div class="col-sm-1">     
                        <label for="batch"> OR:</label>
                       									
                    </div> 
					
					 <div class="col-md-4 form-group"> 
                                <label for="eid">Employee Id:<span class="star">*</span></label>
								  <input type="text" name="txteid" id="txteid" class="form-control" placeholder="Employee Id">
                     </div> 
					
					 <div class="col-md-4 form-group"> 
								<label for="course">Status:<span class="star">*</span></label>
								  <input type="text" name="ddlstatus" id="ddlstatus" readonly="true" class="form-control">
                     </div> 
					
                </div>                

                <div class="container">
                 	<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show" style="margin-left:15px; display:none;"/> 
                </div>
				
				<div id="menuList" name="menuList" style="margin-top:35px;"> </div> 
				<div id="gird" style="margin-top:10px;"> </div> 
        </div>
    </div>   
</div>
</form>
</div>
</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showAllLearnerData() {
           $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");				 
				$.ajax({
					type: "post",
					url: "common/cfGovApproved.php",
					data: "action=ShowDetails&code=" + txtCentercode.value + " &lcode=" + txtlcode.value + "&eid=" + txteid.value + "",
					success: function (data) {						
								$('#response').empty();
								//$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
								$("#menuList").html(data);
								$('#example').DataTable({
							    scrollY:        400,
								scrollCollapse: true,
								paging:         false
						});
								//$("#btnSubmit").hide();
								//$('#txtCentercode').attr('readonly', true);					
					}
				}); 					
        }	
		
		function showData() {
                //alert(val);
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			
                $.ajax({
                    type: "post",
                    url: "common/cfGovApproved.php",
                    data: "action=GetPendingApplicationData",
                    success: function (data) {
                            //alert(data);
						$('#response').empty();
                        $("#gird").html(data);
						$('#examples').DataTable({
							    scrollY:        400,
								scrollCollapse: true,
								paging:         false
						});	
						$("#btnSubmit").show();
                    }
                });
            }

            

			function FillGovtStatus() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfGovApproved.php",
                    data: "action=FILLStatus",
                    success: function (data) {
						//alert(data);
                        //$("#ddlstatus").html(data);
						 ddlstatus.value = data;
						 showData();
                    }
                });
            }
            FillGovtStatus();
		
        $("#btnSubmit").click(function () {
			//var lcode = $('#txtLearnercode').val();
			//var ccode = $('#txtCentercode').val();			
			showAllLearnerData();
			var x = document.getElementById('gird');
			x.style.display = 'none';
        });
        
    });

</script>
</body>
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "index.php";
    </script>
    <?php
}
?>