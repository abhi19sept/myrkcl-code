<?php
$title = "NCR Feedback Form";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var VisitCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var CC=" . $_REQUEST['cc'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var OrganizationCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
$random = (mt_rand(1000, 9999));
$random .= date("y");
$random .= date("m");
$random .= date("d");
$random .= date("H");
$random .= date("i");
$random .= date("s");

echo "<script>var OrgDocId= '" . $random . "' </script>";
?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:46px !important;">

            <div class="panel-heading">NCR Feedback Form</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmncrfeedback" id="frmncrfeedback"  role="form" action="" class="form-inline" enctype="multipart/form-data">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <div id="main-content">
                        <div class="panel panel-info">
                            <div class="panel-heading">AO Contact Details</div>
                            <div class="panel-body">

                                <div id="errorBox"></div>
                                <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">SP Name:<span class="star">*</span></label>
                                        <textarea class="form-control" name="SPName" id="SPName" readonly="true" placeholder="Name of SP"></textarea>   
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">AO Code:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="AOCode" id="AOCode" readonly="true" placeholder="AO Code">   
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="learnercode">Name of Organization/Center:<span class="star">*</span></label>
                                        <textarea class="form-control" name="txtName1" id="txtName1" readonly="true" placeholder="Name of the Organization/Center" oncopy="return false" onpaste="return false" oncut="return false" onkeypress="javascript:return allowchar(event);"></textarea>
                                    </div>
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">AO Located on Reg. Address:<span class="star">*</span></label>
                                        <label class="radio-inline"> <input type="radio" id="AddressYes" name="Address" value="Yes"/> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="AddressNo" name="Address" value="No"/> No </label>

                                    </div>



                                    <!--                                    <div class="col-sm-4 form-group"> 
                                                                            <label for="edistrict">Tehsil:<span class="star">*</span></label>
                                                                        <input type="text" class="form-control" name="SPName" id="SPName" placeholder="Name of SP">   
                                                                        </div>-->
                                </div>

                                <div class="panel panel-info" id="RITGKDetails" style="display:none;">
                                    <div class="panel-heading">ITGK Details</div>
                                    <div class="panel-body">
                                        <div class="container">

                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">Country:<span class="star">*</span></label>
                                                <input type="text" class="form-control" name="RCountry" id="RCountry" readonly="true" placeholder="Name of SP">    
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">State:<span class="star">*</span></label>
                                                <input type="text" class="form-control" name="RState" id="RState" readonly="true" placeholder="Name of SP">    
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">Divison:<span class="star">*</span></label>
                                                <input type="text" class="form-control" name="RDivision" id="RDivision" readonly="true" placeholder="Name of SP">
                                                <input type="hidden" class="form-control" name="CDivision" id="CDivision" readonly="true">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">District:<span class="star">*</span></label>
                                                <input type="text" class="form-control" name="RDistrict" id="RDistrict" readonly="true" placeholder="Name of SP">
                                                <input type="hidden" class="form-control" name="CDistrict" id="CDistrict" readonly="true">
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">Tehsil:<span class="star">*</span></label>
                                                <input type="text" class="form-control" name="RTehsil" id="RTehsil" readonly="true" placeholder="Name of SP">
                                                <input type="hidden" class="form-control" name="CTehsil" id="CTehsil" readonly="true">
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="address">Pin Code:<span class="star">*</span></label>
                                                <input type="text" class="form-control" name="RPin" id="RPin" maxlength="6" readonly="true" placeholder="Pin Code">    
                                            </div>

                                            <div class="col-sm-4 form-group">     
                                                <label for="address">Address:<span class="star">*</span></label>
                                                <textarea class="form-control" id="RAddress" name="RAddress" readonly="true" placeholder="Address"></textarea>

                                            </div>
                                            <div class="col-sm-4 form-group">     
                                                <label for="address">Nearest Landmark:<span class="star">*</span></label>
                                                <input type="text" class="form-control" name="RLandmark" 
                                                       id="RLandmark" placeholder="Nearest Landmark" readonly="true" >

                                            </div>
                                        </div>

                                        <div class="container">	

                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">Police Station:</label>
                                                <input type="text" class="form-control" name="RPolice" id="RPolice" readonly="true" value="NA" placeholder="Police Station">       

                                            </div>
                                        </div>	

                                        <div class="container">
                                            <div class="col-sm-4 form-group">     
                                                <label for="area">Area Type:<span class="star">*</span></label> <br/>                               
                                                <input type="text" class="form-control" name="RAreaType" id="RAreaType" readonly="true" value="NA" placeholder="Police Station">
                                            </div>


                                            <div class="container" id="RUrban" style="display:none">
                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Municipality Type:</label>
                                                    <input type="text" class="form-control" name="RMunicipalityType" id="RMunicipalityType" readonly="true" value="NA">
                                                    <input type="hidden" class="form-control" name="CMunicipalityType" id="CMunicipalityType">
                                                </div>

                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Municipality Name:</label>
                                                    <input type="text" class="form-control" name="RMunicipalityName" id="RMunicipalityName" readonly="true" value="NA">
                                                    <input type="hidden" class="form-control" name="CMunicipalityName" id="CMunicipalityName">
                                                </div>

                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Ward No.:</label>
                                                    <input type="text" class="form-control" name="RWardno" id="RWardno" readonly="true" value="NA">
                                                    <input type="hidden" class="form-control" name="CWardno" id="CWardno">
                                                </div>
                                            </div>

                                            <div class="container" id="RRural" style="display:none">
                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Panchayat Samiti/Block:</label>
                                                    <input type="text" class="form-control" name="RPanchayat" id="RPanchayat" readonly="true" value="NA" placeholder="Police Station">
                                                    <input type="hidden" class="form-control" name="CPanchayat" id="CPanchayat">
                                                </div>
                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Gram Panchayat:</label>
                                                    <input type="text" class="form-control" name="RGramPanchayat" id="RGramPanchayat" readonly="true" value="NA" placeholder="Police Station">
                                                    <input type="hidden" class="form-control" name="CGramPanchayat" id="CGramPanchayat">
                                                </div>
                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Village:</label>
                                                    <input type="text" class="form-control" name="RVillage" id="RVillage" readonly="true" value="NA" placeholder="Police Station">
                                                    <input type="hidden" class="form-control" name="CVillage" id="CVillage">
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="panel panel-info" id="ITGKDetails" style="display:none;">
                                    <div class="panel-heading">Enter ITGK Details</div>
                                    <div class="panel-body">
                                        <div class="container">

                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">Country:<span class="star">*</span></label>
                                                <select id="ddlCountry" name="ddlCountry" class="form-control" >

                                                </select>    
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">State:<span class="star">*</span></label>
                                                <select id="ddlState" name="ddlState" class="form-control" >

                                                </select>    
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">Divison:<span class="star">*</span></label>
                                                <select id="ddlRegion" name="ddlRegion" class="form-control" >

                                                </select>    
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">District:<span class="star">*</span></label>
                                                <select id="ddlDistrict" name="ddlDistrict" class="form-control" >

                                                </select>    
                                            </div>
                                        </div>
                                        <div class="container">
                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">Tehsil:<span class="star">*</span></label>
                                                <select id="ddlTehsil" name="ddlTehsil" class="form-control" >

                                                </select>    
                                            </div>
                                            <div class="col-sm-4 form-group"> 
                                                <label for="address">Pin Code:<span class="star">*</span></label>
                                                <input type="text" class="form-control" name="txtPinCode" id="txtPinCode" maxlength="6" onkeypress="javascript:return allownumbers(event);" placeholder="Pin Code">    
                                            </div>

                                            <div class="col-sm-4 form-group">     
                                                <label for="address">Address:<span class="star">*</span></label>
                                                <textarea class="form-control" id="txtRoad" name="txtRoad" placeholder="Address" onkeypress="javascript:return validAddress(event);"></textarea>

                                            </div>
                                            <div class="col-sm-4 form-group">     
                                                <label for="address">Nearest Landmark:<span class="star">*</span></label>
                                                <input type="text" class="form-control" name="txtLandmark" 
                                                       id="txtLandmark" placeholder="Nearest Landmark" onkeypress="javascript:return validAddress(event);" >

                                            </div>
                                        </div>

                                        <div class="container">	

                                            <div class="col-sm-4 form-group"> 
                                                <label for="edistrict">Police Station:</label>
                                                <input type="text" class="form-control" name="txtPoliceStation" id="txtPoliceStation" value="NA" placeholder="Police Station" onkeypress="javascript:return validAddress(event);">       

                                            </div>
                                        </div>	

                                        <div class="container">
                                            <div class="col-sm-4 form-group">     
                                                <label for="area">Area Type:<span class="star">*</span></label> <br/>                               
                                                <label class="radio-inline"> <input type="radio" id="areaUrban" name="area" value="Urban" /> Urban </label>
                                                <label class="radio-inline"> <input type="radio" id="areaRural" name="area" value="Rural"/> Rural </label>
                                            </div>


                                            <div class="container" id="Urban" style="display:none">
                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Municipality Type:</label>
                                                    <select id="ddlMunicipalType" name="ddlMunicipalType" class="form-control" >

                                                    </select>                                         
                                                </div>

                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Municipality Name:</label>
                                                    <select id="ddlMunicipalName" name="ddlMunicipalName" class="form-control" >

                                                    </select>                                            
                                                </div>

                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Ward No.:</label>
                                                    <select id="ddlWardno" name="ddlWardno" class="form-control" >

                                                    </select>                                       
                                                </div>
                                            </div>

                                            <div class="container" id="Rural" style="display:none">
                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Panchayat Samiti/Block:</label>
                                                    <select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >

                                                    </select>
                                                </div>
                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Gram Panchayat:</label>
                                                    <select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control" >

                                                    </select>
                                                </div>
                                                <div class="col-sm-4 form-group"> 
                                                    <label for="edistrict">Village:</label>
                                                    <select id="ddlVillage" name="ddlVillage" class="form-control" >

                                                    </select>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>



                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">AO HR Details</div>
                            <div class="panel-body">
                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Name of Owner:<span class="star">*</span></label>
                                    <input type="text" class="form-control" name="OwnerName" id="OwnerName" placeholder="Name of Owner" oncopy="return false" onpaste="return false" oncut="return false" onkeypress="javascript:return allowchar(event);">    
                                </div>
                                <div class="col-sm-4 form-group"> 
                                    <label for="address">Mobile No:<span class="star">*</span></label>
                                    <input type="text" class="form-control" name="txtMobileNo" id="txtMobileNo" maxlength="10" minlength="10" onkeypress="javascript:return allownumbers(event);" placeholder="Mobile Number">    
                                </div>

                                <div class="col-sm-4 form-group">     
                                    <label for="address">Name of Faculty:<span class="star">*</span></label>
                                    <input type="text" class="form-control" id="FacultyName" name="FacultyName" placeholder="Faculty Name" onkeypress="javascript:return allowchar(event);">

                                </div>
                                <div class="col-sm-4 form-group">     
                                    <label for="address">Qualification:<span class="star">*</span></label>
                                    <select name="ddlQualification1" id="ddlQualification1" class="form-control">

                                    </select>

                                </div> 
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">AO Premises Details.</div>
                            <div class="panel-body">

                                <div class="container">

                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Sitting Furniture:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="Yfurniture" name="furniture" value="Yes" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="Nfurniture" name="furniture" value="No"/> No </label>
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Lighting/Electricity:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="Yelectrict" name="electrict" value="Yes" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="Nelectrict" name="electrict" value="No"/> No </label>
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Drinking Water Facility:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="Ywater" name="water" value="Yes" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="Nwater" name="water" value="No"/> No </label>
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Toilet Facility:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="Ytoilet" name="toilet" value="Yes" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="Ntoilet" name="toilet" value="No"/> No </label>
                                    </div>

                                </div>
                                <div class="container">
                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Do AO have Separate Lab and Theory Rooms:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="separateYes" name="premisesarea" value="Yes" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="separateNo" name="premisesarea" value="No"/> No </label>
                                    </div>


                                    <div class="container" id="Yes" style="display:none">


                                        <div class="col-sm-4 form-group"> 
                                            <label for="ename">Enter Theory Room Area in Sq.Ft.:</label>
                                            <input type="text" class="form-control" name="TheoryRoomArea" id="TheoryRoomArea" onkeyup="calc()" value="0" placeholder="Theory Room Area" onkeypress="javascript:return allownumbers(event);">     
                                        </div>


                                        <div class="col-sm-4 form-group">     
                                            <label for="faname">Enter Total LAB Room Area in Sq.Ft.:</label>
                                            <input type="text" class="form-control" name="LabArea" id="LabArea" onkeyup="calc()" value="0" placeholder="LAB Area" onkeypress="javascript:return allownumbers(event);">
                                        </div>

                                        <div class="col-sm-4 form-group"> 
                                            <label for="edistrict">Total Area (Theory + LAB) in Sq.Ft.:</label>
                                            <input type="text" class="form-control" readonly="true" name="TotalArea" id="TotalArea" onkeyup="calc()" value="0" placeholder="Total Area" onkeypress="javascript:return allownumbers(event);">  
                                        </div>
                                    </div>

                                    <div class="container" id="No" style="display:none">
                                        <div class="col-sm-4 form-group"> 
                                            <label for="edistrict">Enter Total Area (Theory + LAB) in Sq.Ft.:</label>
                                            <input type="text" class="form-control" name="TotalArea1" id="TotalArea1" placeholder="Total Area">  
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">AO IT Infrastructure Details.</div>
                            <div class="panel-body">
                                <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="ename">Desktop Count:</label>
                                        <input type="text" class="form-control" name="desktop" id="desktop" value="0" placeholder="Desktop Count" onkeypress="javascript:return allownumbers(event);">     
                                    </div>


                                    <div class="col-sm-4 form-group">     
                                        <label for="faname">Laptop Count:</label>
                                        <input type="text" class="form-control" name="laptop" id="laptop" value="0" placeholder="Laptop" onkeypress="javascript:return allownumbers(event);">
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">N-Computing Count:</label>
                                        <input type="text" class="form-control" name="NComputing" id="NComputing" value="0" placeholder="N-Computing" onkeypress="javascript:return allownumbers(event);">  
                                    </div>

                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Printer:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="YPrinter" name="Printer" value="Yes" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="NPrinter" name="Printer" value="No"/> No </label>
                                    </div>

                                </div>

                                <div class="container">

                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Webcam:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="YWebcam" name="Webcam" value="Yes" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="NWebcam" name="Webcam" value="No"/> No </label>
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Biometric Device:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="YBiometric" name="Biometric" value="Yes" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="NBiometric" name="Biometric" value="No"/> No </label>
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Internet:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="YInternet" name="Internet" value="Yes" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="NInternet" name="Internet" value="No"/> No </label>
                                    </div>
                                    <div class="col-sm-4 form-group">     
                                        <label for="area">Power Backup:<span class="star">*</span></label> <br/>                               
                                        <label class="radio-inline"> <input type="radio" id="YBackup" name="Backup" value="Yes" /> Yes </label>
                                        <label class="radio-inline"> <input type="radio" id="NBackup" name="Backup" value="No"/> No </label>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">Overall Summary.</div>
                            <div class="panel-body">
                                <div class="container">
                                    <div class="col-sm-8"> 
                                        <label for="ename">AO Infrastructure:</label>
                                        <label class="radio-inline"> <input type="radio" id="AO_Infrastructure_Excellent" name="AO_Infrastructure" value="Excellent" /> Excellent </label>
                                        <label class="radio-inline"> <input type="radio" id="AO_Infrastructure_Average" name="AO_Infrastructure" value="Average"/> Average </label>
                                        <label class="radio-inline"> <input type="radio" id="AO_Infrastructure_Poor" name="AO_Infrastructure" value="Poor"/> Poor </label>
                                    </div>
                                </div>
                                <div class="container">

                                    <div class="col-sm-8">     
                                        <label for="faname">AO Faculty Quality:</label>
                                        <label class="radio-inline"> <input type="radio" id="AO_Faculty_Quality_Excellent" name="AO_Faculty_Quality" value="Excellent" /> Excellent </label>
                                        <label class="radio-inline"> <input type="radio" id="AO_Faculty_Quality_Average" name="AO_Faculty_Quality" value="Average"/> Average </label>
                                        <label class="radio-inline"> <input type="radio" id="AO_Faculty_Quality_Poor" name="AO_Faculty_Quality" value="Poor"/> Poor </label>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="col-sm-8"> 
                                        <label for="edistrict">Overall Remarks:</label>
                                        <label class="radio-inline"> <input type="radio" id="Overall_Remarks_Excellent" name="Overall_Remarks" value="Excellent" /> Excellent </label>
                                        <label class="radio-inline"> <input type="radio" id="Overall_Remarks_Average" name="Overall_Remarks" value="Average"/> Average </label>
                                        <label class="radio-inline"> <input type="radio" id="Overall_Remarks_Poor" name="Overall_Remarks" value="Poor"/> Poor </label>
                                    </div>
                                </div>      
                            </div>
                        </div>

                        <div class="panel panel-info" id="ITGKDetails">
                            <div class="panel-heading">Declaration and Submission</div>
                            <div class="panel-body">

                                <div class="col-sm-6"> 
                                    <label for="edistrict">Name of Visitor:<span class="star">*</span></label>
                                    <input type="text" class="form-control" name="SpEmpName" id="SpEmpName" placeholder="Name of Visitor" oncopy="return false" onpaste="return false" oncut="return false" onkeypress="javascript:return allowchar(event);">    
                                </div>
                                <div class="col-sm-6"> 
                                    <label for="address">Mobile No:<span class="star">*</span></label>
                                    <input type="text" class="form-control" name="txtSPMobileNo" id="txtSPMobileNo" maxlength="10" minlength="10" onkeypress="javascript:return allownumbers(event);" placeholder="Mobile Number">    
                                </div>
                                <br>
                                <br><br>
                                <br>
                                <div class="container"> 
                                    <div class="col-md-11"> 	

                                        <label for="learnercode"><b>1. I hereby declare that all the details provided with visit report are true and correct & site visit has been done by myself and proposed Applicant has complied all the terms of BID documents as stipulated thereon.</b></label></br>  
                                        <label for="learnercode"><b>2. The final approval shall be given after due diligence of above AO by RKCL in due course of time and if any facts are found incorrect RKCL may reject the AO and may also take appropriate action against us. </b></label><br>  <br>  
                                        <div class="container">
                                            <label class="checkbox-inline" style="float:left;"> <input type="checkbox" name="chk" id="chk" value="1" />
                                                I Accept 
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="container" id="divsubmit" style="display:none;">
                                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>   
        </div>
    </div>
</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    $('#txtEstdate').datepicker({
        format: "yyyy-mm-dd"
    });
</script>

<!--<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
    ;
</script>-->
<script>
    function calc()
    {
        var TA = parseInt(document.getElementById('TheoryRoomArea').value);
        var LA = parseInt(document.getElementById('LabArea').value);
         var Total = TA + LA;
         
        if (Total)
        {
            document.getElementById("TotalArea").value = Total;
        }
        
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

//       $("#theLink").hover(
//        function () {
//            $("#theDiv").fadeIn();
//        },
//        function () {
//            $("#theDiv").fadeOut();
//        }
//    );

        $('#chk').change(function () {
            if (this.checked) {
                $('#divsubmit').show();
                $('#chk').attr('disabled', true);
            } else {
                alert("Please select Check Box to Proceed.");
            }
        });


        $('#txtGenerateId').val(OrgDocId);

        $(function () {
            $("#txtEstdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });

        FillForm();
        
        function FillQyalification() {
                $.ajax({
                    type: "post",
                    url: "common/cfQualificationMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlQualification1").html(data);
                    }
                });
            }
            FillQyalification();
            
        function FillForm() {
            $.ajax({
                type: "post",
                url: "common/cfNcrFeedback.php",
                data: "action=GETDETAILS&cc=" + CC + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "[]") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Some unfair means has been used and the same has been informed to RKCL." + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>&nbsp; Some unfair means has been used and the same has been informed to RKCL.</span>");
                            window.setTimeout(function () {
                                window.location.href = "logout.php";
                            }, 4000);
                    } else {
                        data = $.parseJSON(data);
                        $("#SPName").val(data[0].spname);
                        $("#AOCode").val(data[0].itgkcode);
                        $("#txtName1").val(data[0].itgkname);
                        $("#RCountry").val(data[0].country);
                        $("#RState").val(data[0].state);
                        
                        $("#RDivision").val(data[0].regionname);
                        $("#RDistrict").val(data[0].district);
                        $("#RTehsil").val(data[0].tehsil);
                        
                        $("#CDivision").val(data[0].regioncode);
                        $("#CDistrict").val(data[0].districtcode);
                        $("#CTehsil").val(data[0].tehsilcode);

                        $("#RPin").val(data[0].pincode);
                        $("#RAddress").val(data[0].address);
                        $("#RLandmark").val(data[0].landmark);
                        $("#RPolice").val(data[0].police);
                        $("#RAreaType").val(data[0].areatype);

                        if (data[0].areatype == 'Urban') {
                            $("#RUrban").show();
                            $("#RRural").hide();

                            $("#RMunicipalityType").val(data[0].at1);
                            $("#RMunicipalityName").val(data[0].at2);
                            $("#RWardno").val(data[0].at3);
                            
                            $("#CMunicipalityType").val(data[0].atc1);
                            $("#CMunicipalityName").val(data[0].atc2);
                            $("#CWardno").val(data[0].atc3);

                        } else if (data[0].areatype == 'Rural') {
                            $("#RUrban").hide();
                            $("#RRural").show();

                            $("#RPanchayat").val(data[0].at1);
                            $("#RGramPanchayat").val(data[0].at2);
                            $("#RVillage").val(data[0].at3);
                            
                            $("#CPanchayat").val(data[0].atc1);
                            $("#CGramPanchayat").val(data[0].atc2);
                            $("#CVillage").val(data[0].atc3);

                        }

                        $("#Organization_OwnershipType_old").val(data[0].ownertype);
                        $("#Organization_Address_old").val(data[0].address);

                        $("#Organization_OwnershipType").val(data[0].ownertype);
                        $("#Organization_Address").val(data[0].address);


                        if (data[0].creationamount === 'Rural') {
                            $('#areatypecondition').show();
                            $('#panchayat').text(data[0].panchayat);
                            $('#gram').text(data[0].gram);
                        }

                        $('#existing_details').show(3000);
                        $('#step1').hide(3000);
                        $('#step2').show(3000);
                    }
                }
            });
        }

        function FillParent() {
            $.ajax({
                type: "post",
                url: "common/cfCountryMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCountry").html(data);
                }
            });
        }
        FillParent();

        $("#ddlCountry").change(function () {
            var selcountry = $(this).val();
            //alert(selcountry);
            $.ajax({
                url: 'common/cfStateMaster.php',
                type: "post",
                data: "action=FILL&values=" + selcountry + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlState').html(data);
                }
            });
        });


        $("#ddlState").change(function () {
            var selState = $(this).val();
            //alert(selState);
            $.ajax({
                url: 'common/cfRegionMaster.php',
                type: "post",
                data: "action=FILL&values=" + selState + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlRegion').html(data);
                }
            });
        });

        $("#ddlRegion").change(function () {
            var selregion = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfDistrictMaster.php',
                type: "post",
                data: "action=FILL&values=" + selregion + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlDistrict').html(data);
                }
            });
        });

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLPanchayat&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlPanchayat').html(data);
                }
            });
        });

        $("#ddlPanchayat").change(function () {
            var selPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterRegistration.php',
                type: "post",
                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlGramPanchayat').html(data);
                }
            });
        });

        $("#ddlGramPanchayat").change(function () {
            var selGramPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfVillageMaster.php',
                type: "post",
                data: "action=FILL&values=" + selGramPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlVillage').html(data);
                }
            });
        });

        $("#ddlTehsil").change(function () {
            var selTehsil = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfAreaMaster.php',
                type: "post",
                data: "action=FILL&values=" + selTehsil + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlArea').html(data);
                }
            });
        });

        function FillMunicipalName() {
            var selDistrict = ddlDistrict.value;
            //alert(selDistrict);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalName&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlMunicipalName').html(data);
                }
            });
        }

        function FillMunicipalType() {
            var selDistrict = ddlDistrict.value;
            //alert(selDistrict);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalType",
                success: function (data) {
                    //alert(data);
                    $('#ddlMunicipalType').html(data);
                }
            });
        }

        $("#ddlMunicipalName").change(function () {
            var selMunicipalName = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FILLWardno&values=" + selMunicipalName + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlWardno').html(data);
                }
            });
        });

        $("#AddressYes").change(function () {

            $("#ITGKDetails").hide();
            $("#RITGKDetails").show();
        });
        $("#AddressNo").change(function () {

            $("#ITGKDetails").show();
            $("#RITGKDetails").hide();
        });

        $("#areaUrban").change(function () {
            $("#Urban").show();
            $("#Rural").hide();
            FillMunicipalName();
            FillMunicipalType();
        });
        $("#areaRural").change(function () {
            $("#Rural").show();
            $("#Urban").hide();
            FillPanchayat();
        });

        $("#separateYes").change(function () {

            $("#Yes").show();
            $("#No").hide();
        });

        $("#separateNo").change(function () {

            $("#No").show();
            $("#Yes").hide();
        });

        $("#frmncrfeedback").submit(function () {
            //alert("1");
            if ($("#frmncrfeedback").valid())
            {

//                if (document.getElementById('areaUrban').checked) //for radio button
//                {
//                    var area_type = 'Urban';
//                } else {
//                    area_type = 'Rural';
//                }

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            //BootstrapDialog.alert("<div class='alert-error'><p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfNcrFeedback.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmncrfeedback").serialize();
//alert(forminput);
            data = "action=SubmitFeedback&" + forminput; // serializes the form's elements.
            //alert(data);
//            var file_data7 = $("#uploadImage7").prop("files")[0];
//            var file_data8 = $("#uploadImage8").prop("files")[0];
//            var file_data9 = $("#uploadImage9").prop("files")[0];
//            var file_data6 = $("#uploadImage6").prop("files")[0];// Getting the properties of file from file field
//            var form_data = new FormData(this);                  // Creating object of FormData class
//            form_data.append("panno", file_data7)
//            form_data.append("aadharno", file_data8)
//            form_data.append("addproof", file_data9)
//            form_data.append("appform", file_data6)// Appending parameter named file with properties of file_field to form_data
//            form_data.append("action", "ADD")
//            form_data.append("data", data)
//alert(form_data);
            $.ajax({
                url: url,
//                cache: false,
//                contentType: false,
//                processData: false,
                data: data, // Setting the data attribute of ajax with file_data
                type: 'post',
                success: function (data)
                {
                    //alert(data);

                    if (data === "Successfully Inserted")
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Center Feedback Registered Successfully.</span>");
                        window.setTimeout(function () {
                            window.location.href = "frmncrvisitconfirm.php";
                        }, 3000);

                        Mode = "Add";
                        resetForm("frmncrvisitconfirm");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }



                }
            });
            }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmncrfeedback_validation.js"></script>



<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>