<?php
    $title = "Update Learner Email";
    include('header.php');
    include('root_menu.php');
    include 'common/modals.php';


    if ($_SESSION["User_UserRoll"] <> 19) {
        echo "<script>$('#unauthorized').modal('show')</script>";
        die;
    }

?>

<style>

    .btn-success {
        background-color: #00A65A !important;
    }

    .btn-success:hover {
        color: #fff !important;
        background-color: #04884D !important;
        border-color: #398439 !important;
    }

    .asterisk {
        color: red;
        font-weight: bolder;
        font-size: 18px;
        vertical-align: middle;
    }

    .division_heading {
        border-bottom: 1px solid #e5e5e5;
        padding-bottom: 10px;
        font-size: 20px;
        color: #00A65A;
        margin-bottom: 20px;

    }

    .extra-footer-class {
        margin-top: 0;
        margin-bottom: -16px;
        padding: 16px;
        background-color: #fafafa;
        border-top: 1px solid #e5e5e5;
    }

    #errorBox {
        color: #F00;
    }

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eeeeee;
        box-shadow: inset 0 0 5px 1px #d5d5d5;
    }

    .form-control {
        border-radius: 2px;
    }

    input[type=text]:hover,
    textarea:hover {
        box-shadow: 0 1px 3px #aaa;
        -webkit-box-shadow: 0 1px 3px #aaa;
        -moz-box-shadow: 0 1px 3px #aaa;
    }

    .col-sm-3:hover {
        background: none !important;
    }

    .hidden-xs {
        display: inline-block !important;
    }

    input.parsley-success,
    select.parsley-success,
    textarea.parsley-success {
        color: #468847;
        background-color: #DFF0D8;
        border: 1px solid #D6E9C6;
    }

    input.parsley-error,
    select.parsley-error,
    textarea.parsley-error {
        color: #B94A48;
        background-color: #F2DEDE;
        border: 1px solid #EED3D7;
    }

    .parsley-errors-list {
        margin: 2px 0 3px;
        padding: 0;
        list-style-type: none;
        font-size: 0.9em;
        line-height: 0.9em;
        opacity: 0;

        transition: all .3s ease-in;
        -o-transition: all .3s ease-in;
        -moz-transition: all .3s ease-in;
        -webkit-transition: all .3s ease-in;
    }

    .parsley-errors-list.filled {
        opacity: 1;
    }

    .parsley-required {
        color: tomato;
        font-family: Calibri;
        margin-top: 4px;
        font-size: 15px;
    }

    select[disabled] {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 0.01px;
        text-overflow: '';
    }
</style>

<div class="container" id="showdata" style="display: block;">


    <div class="panel panel-primary" style="margin-top:46px !important;">

        <div class="panel-heading">Learner Details</div>


        <div class="panel-body" id="showSuccessEmail_response" style="display: none;">
            <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateOrgDetails" name="updateOrgDetails">

                <div class="row">
                    <div class="col-md-12">
                        <p>&nbsp;</p>
                        <div class="division_heading">
                            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Your Email ID has been successfully updated
                        </div>

                        <div class="box-body" style="margin: 0 100px;">
                            <div class="form-group">
                                <div class="col-sm-12 addBottom">
                                    <button type="button" class="btn btn-danger" onclick='window.location.href="dashboard.php"'><i
                                            class="fa fa-dashboard"
                                            aria-hidden="true"></i>&nbsp;
                                        Go to Dashboard
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="panel-body" id="showErrorEmail_response" style="display: none;">
            <form class="form-horizontal" style="margin-top: 10px;" method="POST" id="updateOrgDetails" name="updateOrgDetails">

                <div class="row">
                    <div class="col-md-12">
                        <p>&nbsp;</p>
                        <div class="division_heading" style="color: tomato !important;">
                            <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Invalid Input or Already Updated
                        </div>

                        <div class="box-body" style="margin: 0 100px;">
                            <div class="form-group">
                                <div class="col-sm-12 addBottom">
                                    <button type="button" class="btn btn-danger" onclick='window.location.href="dashboard.php"'><i
                                        class="fa fa-dashboard"
                                        aria-hidden="true"></i>&nbsp;
                                    Go to Dashboard
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


</body>
<?php
    include 'common/message.php';
    include 'footer.php';
?>

<script>

    $('#loading').modal('show');

    $.ajax({
        type: "post",
        url: "common/cflearnerdetails.php",
        data: "action=verifyEmail&Code="+'<?php echo $_REQUEST['Code'];?>',
        success: function (data) {
            if(data === "Successfully Updated"){
                $('#loading').modal('hide');
                $("#showSuccessEmail_response").css("display", "block");
                $("#showErrorEmail_response").css("display", "none;");
            } else {
                $('#loading').modal('hide');
                $("#showErrorEmail_response").css("display", "block");
                $("#showSuccessEmail_response").css("display", "none");
            }
        }
    });
</script>

</html>