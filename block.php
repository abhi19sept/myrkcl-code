<?php
//test final bitbucket
$title="Block Center";
include ('header.php'); 
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '4'){
	
}
else{
	session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
	 <?php
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
			
			<div style="min-height:430px !important;max-height:auto !important;">
			<div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">Block A Center</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmclock" id="frmclock" class="form-inline" role="form" enctype="multipart/form-data"> 
						<div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Center Code:</label>
                                <input type="text" class="form-control" maxlength="50" name="txtCenterCode" id="txtCenterCode" placeholder="Center Code"/>								
                            </div>  
						</div>  

                       <div class="container">
						<p class="btn submit" id="btnviewcontainer"> 
                            <input type="button" name="btnView" id="btnView" class="btn btn-primary" value="View Detail"/> 
						</p>
                        </div>
                   
						<div id="tblBlock" style="display:none;">
						<div class="container">                            
                            <div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Center Name:</label>
								<input type="text" id="txtCenterName" name="txtCenterName" class="form-control"  readonly="true">                               
                            </div>											
							
								<div class="col-sm-4 form-group" > 
								  <label for="photo">Tehsil:</label> 
								   <input type="text" id="txtTehsil" name="txtTehsil" class="form-control" disabled="true">								  									  
								</div>
							
							<div class="col-sm-4 form-group" > 
								  <label for="photo">District:</label> 
								   <input type="text" id="txtDistrict" name="txtDistrict" class="form-control" disabled="true">								  									  
								</div>
								
								<div class="col-sm-4 form-group" > 
								  <label for="photo"> Mobile No:</label> 
								   <input type="text" id="txtMobile" name="txtMobile" class="form-control" disabled="true">								  									  
								</div>						
						</div>
						
						<div class="container">                            
<!--                            <div class="col-sm-4 form-group"> 
                                <label for="payreceipt">DLC:</label>
								<input type="text" id="txtDLC" name="txtDLC" class="form-control" disabled="true">                               
                            </div>											
							
								<div class="col-sm-4 form-group" > 
								  <label for="photo">PSA:</label> 
								   <input type="text" id="txtPSA" name="txtPSA" class="form-control" disabled="true">								  									  
								</div>
							-->
                                                        <div class="col-sm-4 form-group" > 
								  <label for="photo">Service Provider:</label> 
								   <input type="text" id="txtSP" name="txtSP" class="form-control" disabled="true">								  									  
								</div>
							<div class="col-sm-4 form-group" > 
								  <label for="photo">Category:</label> 
								  <select id="ddlBlockCategory" name="ddlBlockCategory" class="form-control">
                                  </select>								 							  									  
							</div>
								
							<div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Date:</label>
								<input type="text" id="txtBlockDate" name="txtBlockDate" class="form-control" placeholder="YYYY-MM-DD">                               
                            </div>	
											
						</div>
						
						<div class="container">  
							<div class="col-sm-4 form-group" > 
							  <label for="photo"> Duration:</label> 
							  <input type="text" id="txtBlockDuration" name="txtBlockDuration" class="form-control" value="1">
							</div>
							<div class="col-sm-4 form-group">
								<label> Duration Type: </label>
                                            <select id="ddlDurationType" name="ddlDurationType" class="form-control">
                                                <option selected="selected">Days</option>
                                                <option >Week</option>
                                                <option >Month</option>
                                                <option>Year</option>
                                            </select>							   							  									  
							</div>                           										
							
							<div class="col-sm-4 form-group" > 
							  <label for="photo">Reason:</label> 
							   <textarea id="txtRemark" name="txtRemark" class="form-control"></textarea>								  									  
							</div>		
						</div>
						
						<div class="container">
						<div class="col-sm-5">     
                                <label for="address">Block Category:</label> <br/> 
                              <label class="radio-inline"> <input type="radio" onclick="toggle_visibility1('ddlcourse');" id="center" name="blockmethod" checked="checked"/> Block Center </label>
							  <label class="radio-inline"> <input type="radio" onclick="toggle_visibility('ddlcourse');" id="course" name="blockmethod"  /> Block Course </label>
                            </div>
							
						<div class="col-sm-4 form-group" id="ddlcourse" style="display:none;"> 
						  <label for="photo">Select Course:</label> 
						   <select id="ddlCourse" name="ddlCourse[]"  multiple = "multiple" class="form-control">
									
							</select>						  									  
						</div>	
							
				</div>
						
						<div class="container" style="margin-top:15px;">
                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>						
					</div>
            </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>   

	<script type="text/javascript"> 
 $('#txtBlockDate').datepicker({                   
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
	});  
	</script>
	
    <script type="text/javascript">
<!--
    function toggle_visibility(id) {
       var e = document.getElementById(id);       
          e.style.display = 'block';
    }
	
	function toggle_visibility1(id) {
       var e = document.getElementById(id);      
          e.style.display = 'none';
    }
//-->
</script>   
		   
    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {			
			
            function GetCenterDetail() {				
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GETCENTERDETAIL&CenterCode=" + txtCenterCode.value,
                    success: function (data) {                       
                        if (data == 'Error')
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>Center Already Blocked. or Not Found Please Check your Center Code</span></p>");
                        }
                        else
                        {
                           data = $.parseJSON(data);
							txtCenterName.value = data.CenterName;
                            txtTehsil.value=data.Tehsil;
                            txtDistrict.value=data.District;
                            txtMobile.value=data.Mobile;
                           // txtDLC.value=data.DLC;
                            txtSP.value=data.SP;
                            tblBlock.style['display'] = "block";
                            btnviewcontainer.style['display'] = "none";
                            //txtCenterCode.readonly = true;						   
                        }

                    }
                });
            }
			
			
			function GetCourse(){
				$.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=GETCourse&CenterCode=" + txtCenterCode.value,
                    success: function (data) { 
						//alert(data);
					//data = $.parseJSON(data);
						$('#ddlCourse').html(data);
					}
                });
			}
			
			

            $("#btnView").click(function () {
                $('#response').empty();
                GetCenterDetail();
				GetCourse();
                $('#txtCenterCode').attr('readonly', true);             
            });


            $("#btnReset").click(function () {
                window.location.href = "block.php";

            });


            function FillBlockCategory() {
                $.ajax({
                    type: "post",
                    url: "common/cfBlockUnblock.php",
                    data: "action=FILLBLOCKCATEGORY",
                    success: function (data) {
                        $("#ddlBlockCategory").html(data);
                    }
                });
            }
            FillBlockCategory();

            $("#btnSubmit").click(function () {
				$('#response').empty();
				$('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				var url = "common/cfBlockUnblock.php"; // the script where you handle the form input.
				if (document.getElementById('center').checked) //for radio button
					{
						var radiocheck = 'no';
					}
					else {
					radiocheck = 'yes';
					}
				 var data;				
				 var forminput=$("#frmclock").serialize();
				 
				 data = "action=BLOCK&radiovaue=" + radiocheck + "&" + forminput;
				 // data= "action=BLOCK&BlockCategory=" + ddlBlockCategory.value + "&Duration=" + txtBlockDuration.value + "&DurationType=" + ddlDurationType.value + "&CenterCode=" + txtCenterCode.value + "&CenterName=" + txtCenterName.value + "&coursename=" + ddlCourse.value + "&radiovaue=" + radiocheck + "&Remark=" + txtRemark.value + "&BlockDate=" + txtBlockDate.value + "";
                $.ajax({
                   type: "POST",
                        url: url,
                        data: data,
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {

                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "block.php";
                            }, 1000);

                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                    }
                });
				 return false; 
            });

			function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }
        });

    </script>
</html>
