<?php
	$title="Exam Certificates ";
	include ('header.php'); 
	//error_reporting(0);

	ini_set("memory_limit", "5000M");
	ini_set("max_execution_time", 0);
	ini_set("max_input_vars", 10000);	 
	set_time_limit(0);

	require_once("common/cfexamcertificates.php");

	$requestArgs = $_REQUEST;
	$examEventId = (!empty($requestArgs["code"])) ? $requestArgs["code"] : 1221;
	$requestArgs['code'] = $examEventId;
	$path = $general->getDocRootDir() . '/upload/certificates';
	makeDir($path);

	$qrpath = $path . '/qrcodes/';
    makeDir($qrpath);
	
	$cropperpath = $path . '/cropper';
    makeDir($cropperpath);

	$masterPdf = $path . '/certificate_verified.pdf';
	//$masterPdf = $path . '/certificate_not_verified.pdf';

	if (!file_exists($masterPdf)) {
		die('Main pdf not found.');
	}

	$results = $emp->getLearnerResults($requestArgs);
	if(! mysqli_num_rows($results[2])) {
		die('No Record Found!!');
	}

	$pathBkp = $general->getDocRootDir() . '/upload/certificates_' . date("d-m-Y_h_i_s_a") . '_bkp';
	//rename($path, $pathBkp);

	while($centerRow = mysqli_fetch_assoc($results[2])) {
		$filepath = $general->getDocRootDir() . '/upload/certificates/' . $centerRow['learnercode'] . '_certificate.pdf';
		//if (file_exists($filepath)) continue;

		$learnerBatch = $emp->getData($centerRow['learnercode']);
		if ($learnerBatch) {
			$centerRow = array_merge($centerRow, $learnerBatch);
			$exam_date = explode('-', $centerRow['exam_date']);
			$centerRow['exam_year'] = $exam_date[2];
		}
		
		//print_r($centerRow); die;

		echo $photo = getPath('photo', $centerRow);
		$fdfPath = generateFDF($centerRow, $path);
		if ($fdfPath) {
			generateCertificate($centerRow, $path, $fdfPath, $masterPdf, $photo);
			$AddAadhar = $emp->UpdateLearnerAadhar($centerRow['learnercode'],$centerRow['centercode']);			
		}
	}

function getPath($type, $row) {
	global $general;
	$batchname = trim(ucwords($row['batchname']));
    $coursename = $row['coursename'];
    $isGovtEmp = stripos($coursename, 'gov');
    if ($isGovtEmp) {
        $batchname = $batchname . '_Gov';
    } else {
        $isWoman = stripos($coursename, 'women');
        if ($isWoman) {
            $batchname = $batchname . '_Women';
        } else {
            $isMadarsa = stripos($coursename, 'madar');
            if ($isMadarsa) {
                $batchname = $batchname . '_Madarsa';
            }
        }
    }

    $batchname = str_replace(' ', '_', $batchname);
    $dirPath = '/upload/admission_' . $type . '/' . $row['learnercode'] . '_' . $type . '.png';

    $imgPath = $general->getDocRootDir() . $dirPath;
	$imgPath = str_replace('//', '/', $imgPath);

    $dirPathJpg = str_replace('.png', '.jpg', $dirPath);
    $imgPathJpg = str_replace('.png', '.jpg', $imgPath);

    
	$imgBatchPath = $general->getDocRootDir() . '/upload/admission_' . $type . '/' . $batchname . '/' . $row['learnercode'] . '_' . $type . '.png';
	$imgBatchPath = str_replace('//', '/', $imgBatchPath);
    $imgBatchPathJpg = str_replace('.png', '.jpg', $imgBatchPath);

    $path = '';
    if (file_exists($imgBatchPath)) {
    	$path = $imgBatchPath;
    } elseif (file_exists($imgBatchPathJpg)) {
    	$path = $imgBatchPathJpg;
    } elseif (file_exists($imgPath)) {
    	$path = $imgPath;
    } elseif (file_exists($imgPathJpg)) {
    	$path = $imgPathJpg;
    } else {
    	/*$imgPath = 'http://' . $_SERVER['HTTP_HOST'] . $dirPath;
    	if(@getimagesize($imgPath)) {
    		$path = $imgPath;
    	} else {
    		$path = 'http://' . $_SERVER['HTTP_HOST'] . $dirPathJpg;
    	}*/
    }

    return $path;
}

function makeDir($path) {
    return is_dir($path) || mkdir($path);
}

function generateFDF($data, $path) {
	$fdfContent = '%FDF-1.2
		%Ã¢Ã£Ã?Ã“
		1 0 obj 
		<<
		/FDF 
		<<
		/Fields [';
    foreach ($data as $key => $val) {
        $fdfContent .= '
				<<
				/V (' . $val . ')
				/T (' . $key . ')
				>> 
				';
    }
    $fdfContent .= ']
		>>
		>>
		endobj 
		trailer
		<<
		/Root 1 0 R
		>>
		%%EOF';

	$filePath = $path . '/' . $data['learnercode'] . '.fdf';
    file_put_contents($filePath, $fdfContent);
    $return = '';
    if (file_exists($filePath)) {
    	$return = $filePath;
    }

    return $return;
}

function generateCertificate($centerRow, $path, $fdfPath, $masterPdf, $photo) {
	$resultPath = $path . '/' . $centerRow['learnercode'] . '_certificate.pdf';

	$pdftkPath = ($_SERVER['HTTP_HOST']=="myrkcl.com") ? '"C://inetpub//vhosts//myrkcl.com//httpdocs//upload//PDFtk Server//bin//pdftk.exe"' : (($_SERVER['HTTP_HOST']=="staging.rkcl.co.in") ? '"C://inetpub//vhosts//staging.myrkcl.com//httpdocs//myrkcl//upload//PDFtk Server//bin//pdftk.exe"' : '"C://Program Files (x86)//PDFtk Server//bin//pdftk.exe"') ;
	
	$command = $pdftkPath . '  ' . $masterPdf . ' fill_form   ' . $fdfPath . ' output  '. $resultPath . ' flatten';
	exec($command);
	unlink($fdfPath);
	addPhotoInPDF($centerRow['learnercode'], $photo, $path, $resultPath);
}

function addPhotoInPDF($learnercode, $Imagepath, $path, $pdfPath) {

    require_once('fpdi/fpdf/fpdf.php');
    require_once('fpdi/fpdi.php');
    require_once('fpdi/fpdf_tpl.php');
    require_once("cropper.php");

	global $general;
	global $cropperpath;
	
	//echo $Imagepath; die;
    $qrResource = $picResource = 0;
    $qrpath = ''; //generateQRCode($learnercode, $path);
    if (!empty($qrpath) && file_exists($qrpath)) {
    	$qrResource = imagecreatefromstring(file_get_contents($qrpath));
	}
	if (!empty($Imagepath) && file_exists($Imagepath)) {
    	$picResource = imagecreatefromstring(file_get_contents($Imagepath));
	}

    $pdf = new FPDI();
    $pdf->AddPage();
    $pdf->setSourceFile($pdfPath);
    $template = $pdf->importPage(1);
    $pdf->useTemplate($template);
    try {
	    if (!empty($qrpath) && $qrResource) {
	    	$pdf->Image($qrpath, 21, 242, 25, 25);
	    	unlink($qrpath);
	    }
	} catch (Exception $_ex) { }

    try {
    	if(!empty($Imagepath) && $picResource) $pdf->Image($Imagepath, 92.8, 79.5, 29, 32.8);
	} catch (Exception $_ex) {
		$pdf->Output($pdfPath, "F");

		if (!empty($Imagepath) && file_exists($Imagepath)) {
			$cropper = new Cropper($cropperpath);
			$args = array("size" => array(30,30), "image" => $Imagepath, "placeholder" => 2);
			$newImgPath = $cropper->crop($args);

			if (file_exists($newImgPath)) {
				addPhotoInPDF($learnercode, $newImgPath, $path, $pdfPath);
			}
		}
		return;
    }

    $pdf->Output($pdfPath, "F");
}

function generateQRCode($learnercode, $path) {
	$return = '';
    $qrpath = $path . '/qrcodes/' . $learnercode . '.png';
    if (file_exists($qrpath)) return $qrpath;

    try {
    	require_once ('qr_sat/qr_img.php');
    	$id = $learnercode; //'471170616094512789';
		$data='?id='.$id;
		$image = new Qrcode();
		$image->setdata($data);
		$image->calculate();
		$image->save($qrpath);
    } catch (Exception $_ex) {
		print_r($_ex->getTrace());
	}

	if (file_exists($qrpath)) {
		$return = $qrpath;
	}

	return $return;
}

function generateBarCode($learnercode, $path) {
	require_once('phpbarcode/code128.class.php');

	$return = '';
    $barpath = $path . '/barcodes/' . $learnercode . '.png';
    if (file_exists($barpath)) return $barpath;
	try {
	$barcode = new phpCode128($learnercode, 90, $path . '\verdana.ttf', 10);
	$barcode->setEanStyle(false);
	$barcode->setShowText(true);
	$barcode->setPixelWidth(2);
	$barcode->setBorderWidth(0);
	$barcode->saveBarcode($barpath);
	} catch (Exception $_ex) {
		print_r($_ex->getTrace());
	}

	if (file_exists($barpath)) {
		$return = $barpath;
	}

	return $return;
}

?>
