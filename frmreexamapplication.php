<?php ob_start();
$title = "Re-Exam Application";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var FunctionCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
//echo "<pre>";print_r($_SESSION);


 if($_SESSION['User_UserRoll']==19){
    header('Location: frmreexamapplicationforl.php');
    exit;
    }


?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Re-Exam Application</div>
            <div class="panel-body">
                <form name="frmreexamapplication" id="frmreexamapplication" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">

                            </select>
                        </div> 
                    </div>  
                    <div class="container">
                        <div class="col-md-6 form-group">     
                            <label for="batch"> Select Exam Event:<span class="star">*</span></label>
                            <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                            </select>
                        </div> 

                    </div>
                    <div class="container">
                        <div class="col-md-6 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>
                        </div> 

                    </div>
                    <div id="menuList" name="menuList" style="margin-top:35px;"></div> 
                    <div class="container">
                        <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display: none"/>    
                    </div>                  


            </div>
        </div>   
    </div>
</form>
</body>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>                
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLCOURSEBLOCKCENTER",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {

            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfReexamAapplication.php",
                data: "action=FILLEXAMEVENT&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlExamEvent").html(data);
                }
            });

        });

        $("#ddlExamEvent").change(function () {
            var selExamEvent = $(this).val();
            //alert(selExamEvent);
            $.ajax({
                type: "post",
                url: "common/cfReexamAapplication.php",
                data: "action=FILLREEXAMBATCH&examevent=" + selExamEvent + "&coursename=" + ddlCourse.value + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });
        function showAllData(val, val1) {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

            $.ajax({
                type: "post",
                url: "common/cfReexamAapplication.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "&ExamEvent=" + ddlExamEvent.value + "",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#menuList").html(data);
                    $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                    $('#btnSubmit').show();
                }
            });
        }
        $("#ddlBatch").change(function () {
            //alert(this.value);
            showAllData(this.value, ddlCourse.value);

        });

        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfReexamAapplication.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmreexamapplication").serialize();
            //alert(forminput);

            if (Mode == 'Add')
            {
                data = "action=ADD&" + forminput; // serializes the form's elements.
            }
            else
            {
                //data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    // alert(data);
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                    window.setTimeout(function () {
                        window.location.href = "frmreexamapplication.php";
                    }, 3000);

                    Mode = "Add";
                    resetForm("frmreexamapplication");

                }
            });

            return false; // avoid to execute the actual submit of the form.
        });

        $('#menuList').on('click', '#checkuncheckall', function (){
            checkuncheckall(this.checked);
        });
    });

    function checkuncheckall(checked) {
        var aa = document.getElementById('frmreexamapplication');
        for (var i =0; i < aa.elements.length; i++) {
            aa.elements[i].checked = checked;
        }
    }

</script>
</html>