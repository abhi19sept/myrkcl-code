<?php
$title = "Admission Payment";
include ('header.php');
include ('root_menu.php');
require("razorpay/checkout/manual.php");

if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}
echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
echo "<script>var CenterCode='" . $_SESSION['User_LoginId'] . "'</script>";
//echo "<script>var amount= " . $_REQUEST['amount'] . " </script>";
echo "<script>var PayType= 'Learner Fee Payment' </script>";
//print_r($_SESSION);
$payutxnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
echo "<script>var PayUTranID= '" . $payutxnid . "' </script>";

$random = (mt_rand(1000, 9999));
$random.= date("y");
$random.= date("m");
$random.= date("d");
$random.= date("H");
$random.= date("i");
$random.= date("s");
//echo $random;

echo "<script>var RKCLTxnId= '" . $random . "' </script>";

?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Fee Payment</div>
            <div class="panel-body">
                <form name="frmfeepayment" id="frmfeepayment" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                        <div class="col-sm-4 form-group">     
                            <label for="course">Select Course:</label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">

                            </select>
                        </div> 
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Batch:</label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>									
                        </div> 
                    </div>

                     <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Payment Mode:</label>
                            <select id="paymode" name="paymode" class="form-control" onchange="toggle_visibility1('online_mode');">

                            </select>                                   
                        </div>
                    </div>

                    <div class="container">
                        <div class="col-sm-4 form-group">     
                            <label for="gender">Choose Payment Gateway:</label> <br />
                            <label class="radio-inline"> <input type="radio" id="gatewayPayu" checked="checked" class="gateway" name="gateway" value="payu" onchange="toggle_visibility1('online_mode');"/> Payu </label>
                            <label class="radio-inline"> <input type="radio" id="gatewayRazor" class="gateway" name="gateway" value="razorpay" onchange="toggle_visibility1('online_mode');"/> Razorpay </label>
                        </div>
                    </div>
                    
                    <div class="container" id="online_mode" style="display:none;">
                        <div class="col-md-12 form-group" style="display:none;">                                
                            <label class="radio-inline"> <input type="radio" id="PayUMoney" value="payumoney"  name="Role" checked="true" /> Pay Using PayUMoney </label>
                            <label class="radio-inline"> <input type="radio" id="PayDDMode" value="emitra" name="Role" /> Pay Using e-Mitra Gateway </label>                                
                            <!--<label class="radio-inline"> <input type="radio" id="RoleITGK"  name="Role" /> ITGK </label>-->
                                                        <!--<label class="radio-inline"> <input type="radio" id="genderFemale" name="gender" /> Female </label>-->
                        </div> 
                    </div>



                    <div>
                        <input type="hidden" name="amounts" id="amounts"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId" value="<?php echo $random; ?>"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtGeneratePayUId" id="txtGeneratePayUId" value="<?php echo $payutxnid; ?>"/>
                    </div>
                    <div id="menuList" name="menuList" style="margin-top:5px;"> </div> 

                    <div class="container">
                        <br><input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                    </div>
            </div>
        </div>   
    </div>
</form>
</div>
<form id="frmpostvalue" name="frmpostvalue" action="frmadmissionpayment.php" method="post">
    <input type="hidden" id="code" name="code">
<!--    <input type="hidden" id="paytype" name="paytype"> -->
    <input type="hidden" id="paytypename" name="paytypename"> 
    <input type="hidden" id="RKCLtranid" name="RKCLtranid"/>
    <input type="hidden" id="amount" name="amount">	
	 <input type="hidden" id="txnid" name="txnid">
	 <input type="hidden" id="course" name="course">
	 <input type="hidden" id="batch" name="batch">
</form>

<form id="frmwithoutfeepostvalue" name="frmwithoutfeepostvalue" action="frmwithoutfeeadmissionpayment.php" method="post">
    <input type="hidden" id="withoutfeecode" name="withoutfeecode">
    <input type="hidden" id="withoutfeepaytype" name="withoutfeepaytype">                
    <input type="hidden" id="withoutfeeamount" name="withoutfeeamount">				
</form>

<form id="frmddpostvalue" name="frmddpostvalue" action="frmddadmissionpayment.php" method="post">
    <input type="hidden" id="ddcode" name="ddcode">
    <input type="hidden" id="ddpaytype" name="ddpaytype">                
    <input type="hidden" id="ddamount" name="ddamount">
    <input type="hidden" id="ddRKCLtranid" name="ddRKCLtranid"/>
     <input type="hidden" id="ddtxnid" name="ddtxnid">

</form>
</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
<!--

    function toggle_visibility1(id) {
        var e = document.getElementById(id);
        var f = document.getElementById('paymode').value;
        if (f == "1")
        {
            e.style.display = 'block';
        }
        else {
            e.style.display = 'none';
        }
    }
//-->
</script>  

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {



        $("#ddlBatch").change(function () {

            var BatchCode = $('#ddlBatch').val();
            //alert(BatchCode);
            $.ajax({
                type: "post",
                url: "common/cfFeePayment_Ver1.php",
                data: "action=Fee&codes=" + BatchCode + "",
                success: function (data) {
                    //alert(data);
                    amounts.value = data;
                }
            });

        });


        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLAdmissionSummaryCourse",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlBatch").html(data);
                }
            });

        });

        $("#ddlBatch").change(function () {
            showpaymentmode(ddlBatch.value, ddlCourse.value);
        });

        function showpaymentmode(val, val1) {
            $.ajax({
                type: "post",
                url: "common/cfEvents.php",
                data: "action=SHOWAdmissionPay&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#paymode").html(data);
                }
            });
        }
        function showAllData(val, val1, val2) {
            $.ajax({
                type: "post",
                url: "common/cfFeePayment_Ver1.php",
                data: "action=SHOWALL&batch=" + val + "&course=" + val1 + "&paymode=" + val2 + "",
                success: function (data) {
                    // alert(data);
                    $("#menuList").html(data);
                    $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                    $('#btnSubmit').show();
                }
            });
        }

        $("#paymode").change(function () {
            //showAllData(ddlBatch.value, ddlCourse.value, paymode.value);
			var gateway = $(".gateway").val();
            if (gateway != "") {
                showAllData(ddlBatch.value, ddlCourse.value, paymode.value);
            }
        });
		
		$(".gateway").click(function () {
            if (this.value == "") {
                showAllData(ddlBatch.value, ddlCourse.value, this.value);
            } else {
                showAllData(ddlBatch.value, ddlCourse.value, paymode.value);
            }
        });


        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			 $('#btnSubmit').hide();
            var url = "common/cfFeePayment_Ver1.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmfeepayment").serialize();
            //alert(forminput);

            if (Mode == 'Add')
            {
                data = "action=ADD&" + forminput; // serializes the form's elements.
            }
            else
            {
                //data = "action=UPDATE&code=" + RoleCode + "&name=" + txtRoleName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == 0) {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Please Checked Atleast one checkbox." + "</span></p>");
                    }
					else if (data == 1) {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Something has gone wrong. Please try again." + "</span></p>");
						  window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmFeePayment_Ver1.php';
                            }, 3000);
                    }
                    else {
                        //$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        data = $.parseJSON(data);
                        if (data.payby == 'payu') {
                        var paymode2 = $('#paymode').val();

                        $('#code').val(CenterCode);
                        $('#paytype').val(PayType);


                        var payname = 'Learner Fee Payment';
                        //var rkcltranid = $('#txtGenerateId').val();
                        $('#RKCLtranid').val(RKCLTxnId);
                        $('#ddRKCLtranid').val(RKCLTxnId);


                        $('#paytypecode').val(ddlBatch.value);
						
						$('#batch').val(ddlBatch.value);
						$('#course').val(ddlCourse.value);

                        $('#paytypename').val(payname);
                        $('#txnid').val(PayUTranID);
                        $('#ddtxnid').val(PayUTranID);

                        $('#ddcode').val(CenterCode);
                        $('#ddpaytype').val(PayType);

                        //$('#ddmode') = $('#paymode').val();
                        amount.value = data;
                        ddamount.value = data;
                        if (paymode2 == "2") {
                            $('#frmddpostvalue').submit();
                        }
                        else if (paymode2 == "4") {
                            $('#frmwithoutfeepostvalue').submit();
                        }
                        else {
                            $('#frmpostvalue').submit();
                        }
					  } else {
                            var options = data;
                            <?php include("razorpay/razorpay.js"); ?>
                        }
                    }

                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>