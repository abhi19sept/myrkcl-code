<?php
$title = "Apply RS-CFA Certificate";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '7') {
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading" style="height:40px">
                <div  class="col-sm-5"> Apply RS-CFA Certificate </div>

            </div>

            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmfeedback" id="frmfeedback" class="form-inline" role="form" enctype="multipart/form-data">     
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                    onchange="try {
                                                setCustomValidity('')
                                            } catch (e) {
                                            }">  </select>
                        </div>
                        <div class="col-md-6 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                    onchange="try {
                                                setCustomValidity('')
                                            } catch (e) {
                                            }"> </select>
                        </div>
                    </div>    
                    <div class="container" id="view">
                        <div class="col-md-4 form-group">     
                            <input type="button" name="btnView" id="btnView" class="btn btn-primary" value="View"/>    
                        </div>
                    </div>   
                    <div id="grid" style="margin-top:5px; width:100%;"> </div>

            </div>
        </div>
    </div>
</div>
<div id="myModalupdateemail" class="modal" style="padding-top:140px !important">        
    <div class="modal-content" style="width: 80%;">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h6>Update / Confirm Your Email Address</h6>
        </div>
        <div class="modal-body" style="min-height: 250px;" id='formhtml'>
            <form name="frmUpdateEmail" id="frmUpdateEmail" class="form-horizontal" role="form"
                  enctype="multipart/form-data" style="margin: 25px 0 40px 0;">  


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label" style="text-align: right;margin-top: 6px;">Enter Email Address:</label>
                    <div class="col-sm-6">
                        <input class="form-control valid" name="txtEmail" id="txtEmail" placeholder="Enter Email Address"  type="text">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6" id="responseemail" style="margin-top: 6px;min-height: 35px;"></div>
                </div>
                <div class="form-group last">
                    <div class="col-sm-offset-3 col-sm-6">
                        <input type="button" name="Submitbtn" id="Submitbtn" class="btn btn-primary" value="Submit" />
                    </div>
                </div>   
            </form>		
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include 'common/message.php'; ?>

<script type="text/javascript">
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
$(document).ready(function () {
    var AckCode = "";
    var CourseCode = "";
    var BatchCode = "";
    var myBookId = "";
    function FillCourse() {
        $('#response').empty();
        $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfApplyRscfaCertificate.php",
            data: "action=FillCourse",
            success: function (data) {
                if (data == '1') {
                    $('#response').empty();
                    $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + "   You are not authorised for RS-CFA Course." + "</span></p>");
                } else {
                    $('#response').empty();
                    $("#ddlCourse").html(data);
                }

            }
        });
    }
    FillCourse();

    $("#ddlCourse").change(function () {
        var selCourse = $(this).val();
        $.ajax({
            type: "post",
            url: "common/cfApplyRscfaCertificate.php",
            data: "action=FillBatch&values=" + selCourse + "",
            success: function (data) {
                $("#ddlBatch").html(data);
            }
        });

    });

    $("#btnView").click(function () {
        if ($("#frmfeedback").valid())
        {
            $('#response').empty();
            $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfApplyRscfaCertificate.php",
                data: "action=viewlearners&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + "",
                success: function (data)
                {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }
        return false; // avoid to execute the actual submit of the form.
    });

    $("#grid").on('click', '.updcount', function () {
        var myBookId = $(this).attr('id');
        var AckCode = $(this).attr('AckCode');
        var CourseCode = $(this).attr('CourseCode');
        var BatchCode = $(this).attr('BatchCode');
        var EmailAdd = $(this).attr('EmailAdd');
        $("#txtEmail").val(EmailAdd);
        $(this).prop("disabled", true);
        var mybtnid = "myBtn_" + myBookId;
        var modal = document.getElementById('myModalupdateemail');
        var btn = document.getElementById(mybtnid);
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        span.onclick = function () {
            modal.style.display = "none";
            window.location.href = 'frmapplyrscfacertificate.php';
        }
        $("#Submitbtn").click(function () {
            var EmailAddd = $("#txtEmail").val();
            $('#responseemail').empty();
            $('#responseemail').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span> Processing..... </span></p>");
            $.ajax({
                type: "post",
                url: "common/cfApplyRscfaCertificate.php",
                data: "action=ApplyForCertificate&lcode=" + AckCode + "&ccode=" + CourseCode + "&bcode=" + BatchCode + "&EmailAdd=" + EmailAddd,
                success: function (data) {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#responseemail').empty();
                        $('#responseemail').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span> " + data + "</span></div>");
                        window.setTimeout(function () {
                            Mode = "Add";
                            window.location.href = 'frmapplyrscfacertificate.php';
                        }, 5000);
                    } else
                    {
                        $('#responseemail').empty();
                        $('#responseemail').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span> " + data + "</span></div>");
                    }
                }
            });
        });
    });

});
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>

</html>

<?php
} else {
    session_destroy();
    ?>
    <script>
    window.location.href = "logout.php";
    </script>
    <?php
}
?>




