<?php
$title="Result Date Update";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var ExamId=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    $Mode = $_REQUEST['Mode'];
} else {
    echo "<script>var ExamId=0</script>";
    echo "<script>var Mode='Add'</script>";
    $Mode = "Add";
} 
if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '8550'){
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
	<div class="container">
		<div class="panel panel-primary" style="margin-top:36px !important;">
			<div class="panel-heading">Update Result Date in Exam Event</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmupdresultdate" id="frmupdresultdate" class="form-inline" role="form"
						enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="event">Select Exam Event:<span class="star">*</span></label>
                                <select id="ddlExamEvent" name="ddlExamEvent" class="form-control" required="true">
								  
                                </select> 
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Result Date:</label>
                                <input type="text" class="form-control" readonly="true" maxlength="50" name="rdate"
									id="rdate"  placeholder="YYYY-MM-DD" required="true">    
                            </div>
						</div>                   

                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>						
						
						<div id="grid" style="margin-top:5px;"> </div> 
					</form>
				</div>
						
		</div>   
	</div>


<div class="modal" id="myModal">
    <div class="modal-content">
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal">×</button>
			<h3 id="heading-tittle" class="modal-title">Heading</h3>
		</div>
		<div class="modal-body">
			    <form name="frmupdresultdatemodal" id="frmupdresultdatemodal" class="form-inline" role="form"
						enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="responses"></div>

                            </div>        
							<div id="errorBox"></div>
                            <div class="col-sm-4 form-group">     
                                <label for="event">Select Exam Event:<span class="star">*</span></label>
                                <input type="text" id="ddlExamEventModal" readonly="true" name="ddlExamEventModal"
									class="form-control" required="true" placeholder="Exam Event">
								<input type="hidden" name="eventmodalid" id="eventmodalid" class="form-control"/>
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Result Date:</label>
                                <input type="text" class="form-control" readonly="true" maxlength="50" name="rdatemodal"
									id="rdatemodal"  placeholder="YYYY-MM-DD" required="true">    
                            </div>
						</div>                   

                        <div class="container">
                        <input type="submit" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Update Date"/>    
                        </div>						
						
						
					</form>
		</div>
		</div>
</div>

</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
 .star{
	color:red;
}
</style>
<script type="text/javascript">
	$('#rdate').datepicker({
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
	});
</script>
<script type="text/javascript">
	$('#rdatemodal').datepicker({
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
	});
</script>
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
            
            function FillExamEvent() {
                $.ajax({
                    type: "post",
                    url: "common/cfUpdateResultDate.php",
                    data: "action=FillExamEvent",
                    success: function (data) {
                        $("#ddlExamEvent").html(data);
                    }
                });
            }
            FillExamEvent();            
            
            function showData() {
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfUpdateResultDate.php",
                    data: "action=SHOW",
                    success: function (data) {
						$('#response').empty();
						$("#grid").html(data);
						$('#example').DataTable({
								dom: 'Bfrtip',
								buttons: [
									'copy', 'csv', 'excel', 'pdf', 'print'
								]
						});
                    }
                });
            }
            showData();
			
			$("#grid").on('click', '.rsltdate',function(){
					var eventid = $(this).attr('id');
					var eventname = $(this).attr('name');
					var modal = document.getElementById('myModal');
					var span = document.getElementsByClassName("close")[0];
					modal.style.display = "block";
					span.onclick = function() {
						$("#eventmodalid").val("");
						$("#ddlExamEventModal").val("");
						modal.style.display = "none";						
					}
					$("#eventmodalid").val(eventid);
						$("#ddlExamEventModal").val(eventname);
					$("#heading-tittle").html('Update Result Date');
				});

            $("#btnSubmit").click(function () {
			if ($("#frmupdresultdate").valid())
			{				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfUpdateResultDate.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&examid=" + ddlExamEvent.value + "&resultdate=" + rdate.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + CountryCode + "&name=" + txtCountryName.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {						
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmupdateresultdate.php";
                           }, 3000);

                            Mode="Add";
                            resetForm("frmupdresultdate");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();
                    }
                });
			}
                return false; // avoid to execute the actual submit of the form.
            });
			
			 $("#btnSubmit1").click(function () {
			if ($("#frmupdresultdatemodal").valid())
			{				
                $('#responses').empty();
                $('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfUpdateResultDate.php"; // the script where you handle the form input.
                var data;
               
                    data = "action=Update&examidmodal=" + eventmodalid.value + "&resultdatemodal=" + rdatemodal.value + ""; // serializes the form's elements.
               
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#responses').empty();
                            $('#responses').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmupdateresultdate.php";
                           }, 1000);

                            Mode="Add";
                            resetForm("frmupdresultdatemodal");
                        }
                        else
                        {
                            $('#responses').empty();
                            $('#responses').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();
                    }
                });
			}
                return false; // avoid to execute the actual submit of the form.
            });
			
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>