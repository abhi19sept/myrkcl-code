<?php
$title="IT PERIPHERALS";
include ('header.php'); 
include ('root_menu.php'); 
    if (isset($_REQUEST['code'])) {
                echo "<script>var ItPeripheralsCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var DeviceCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
        <div class="container"> 
			 
            <div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading"> IT PERIPHERALS DETAILS
				 
				</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmItPeripherals" id="frmItPeripherals" class="form-inline" action=""> 
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
							
							
							<div class="col-sm-4 form-group"> 
                                <label for="edistrict">Select IT Peripheral Device:<span class="star">*</span></label>
                                <select id="ddlDevice" name="ddlDevice" class="form-control" >
								 
                                </select>    
                            </div>
							<div class="col-sm-4 form-group"style="margin-top:30px"> 
                                <label for="edistrict">Available:</label>
                                <input type="radio" name="available" value="yes" id="availableYes" checked="checked">Yes</input> &nbsp;&nbsp;&nbsp;&nbsp; 
                            <input type="radio" name="available" value="no" id="availableNo">No</input>
				
                            </div>
							
							 <div class="col-sm-4 form-group">     
                                <label for="address">Make:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtMake" 
									id="txtMake" placeholder=" Make"  >
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="address">Model:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtModel" id="txtModel" placeholder="Model" onkeypress="javascript:return allownumbers(event);">    
                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address">Number of ITEMS:<span class="star">*</span></label>
                                <input type="text" class="form-control" id="txtQuantity" name="txtQuantity" placeholder="ITEMS"  maxlength="3"  onkeypress="javascript:return allownumbers(event);">

                            </div>
							
							
							<div class="col-sm-4 form-group">     
                                <label for="address">Details:<span class="star">*</span></label>
                                <textarea class="form-control" rows="4" id="txtDetail" name="txtDetail" placeholder="Details"></textarea>

                            </div>
                            
						</div>

						<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
						
						<div id="gird"></div>
                </div>
            </div>   
        </div>
    </form>

</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>

<script type="text/javascript">
	function PreviewImage(no) {
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
		oFReader.onload = function (oFREvent) {
			document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
		};
	};
</script>
<style>
#errorBox
{	color:#F00;	 } 
</style>

<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfDeviceMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlDevice").html(data);
                    }
                });
            }

            FillStatus();
            
            
            
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfItPeripherals.php",
                    data: "action=DELETE&values=" + ItPeripheralsCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmitperipherals.php";
                           }, 1000);
                            
                            Mode="Add";
                            resetForm("frmItPeripherals");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfItPeripherals.php",
                    data: "action=EDIT&values=" + ItPeripheralsCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Make']);
                        //alert(data);
                        data = $.parseJSON(data);
                        ddlDevice.value = data[0].Name;
                        txtMake.value = data[0].Make;
                        txtModel.value = data[0].Model;
                        txtQuantity.value = data[0].Quantity;
                        txtDetail.value = data[0].Detail;
                        
                    }
                });
            }

            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfItPeripherals.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						

                    }
                });
            }

            showData();


            $("#btnSubmit").click(function () {
				if ($("#frmItPeripherals").valid())
                {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfItPeripherals.php"; // the script where you handle the form input
                if (document.getElementById('availableYes').checked) 
		{
                    var avail_value = 'yes';
		}
		else{
		    avail_value = 'no';
		    }
                    
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + ddlDevice.value + "&available=" + avail_value + "&make=" + txtMake.value + "&model=" + txtModel.value + "&quantity=" + txtQuantity.value + "&detail=" + txtDetail.value + ""; // serializes the form's elements.           
                }
                else
                {
                    data = "action=UPDATE&code=" + ItPeripheralsCode + "&name=" + ddlDevice.value + "&available=" + avail_value + "&make=" + txtMake.value + "&model=" + txtModel.value + "&quantity=" + txtQuantity.value + "&detail=" + txtDetail.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
								window.location.href="frmitperipherals.php";
                            }, 3000);

                            Mode="Add";
                            resetForm("frmItPeripherals");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
				}
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
	<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmpheripherals_validation.js" type="text/javascript"></script>	
</html>