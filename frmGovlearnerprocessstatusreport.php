<?php
$title="Govt. Entry Form Status Report";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var FunctionCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var FunctionCode=0</script>";
echo "<script>var Mode='Add'</script>";
}

?>
<link rel="stylesheet" href="css/profile_style.css">
<div style="min-height:430px !important;max-height:auto !important;">
	 <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Reimbursement Application Form Status Report</div>
                <div class="panel-body">
				   <form name="frmgovlearnerstatusreport" id="frmgovlearnerstatusreport" class="form-inline" role="form" enctype="multipart/form-data">
					<div class="container">
						<div class="container">						
							<div id="response"></div>
						</div>
							<div id="errorBox"></div>
							 <?php if($_SESSION['User_UserRoll'] == 19) {?>
								 <div class="col-md-4 form-group"> 
									<label for="lcode">Learner Code:<span class="star">*</span></label>
									<input type="text" name="txtlcode" id="txtlcode" readonly="true" class="form-control" placeholder="Learner Code" onkeypress="javascript:return allownumbers(event);" value="<?php echo $_SESSION['User_LearnerCode']  ?>" maxlength="18">
								  </div>
								   <div class="col-sm-4 form-group" style="display:none;">                                  
									<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
								</div>
							 <?php echo "<script> var ClickMode='auto' </script>"; } else {?>
								<div class="col-md-4 form-group"> 
									<label for="lcode">Learner Code:<span class="star">*</span></label>
									<input type="text" name="txtlcode" id="txtlcode" class="form-control" placeholder="Learner Code" onkeypress="javascript:return allownumbers(event);" maxlength="18">
								  </div>
								  
								  
				
								<div class="col-sm-4 form-group">                                  
									<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
								</div>
							 <?php echo "<script> var ClickMode='click' </script>"; } ?>
								<div class="col-md-4 form-group"></div>
								<div class="col-md-4 form-group" style="margin: 30px 0 0 70px;font-size: large;">
										<a href="upload/documents/Claim_Workflow.pdf" style="float: right;" target="_blank">View Claim Workflow</a> 
								</div>
                    </div>				
					<div class="govt_stepmain" id="processs" style="display:none;">
						 <div  class="govt_step25" ><p>Govt. Reimbursement Application Form</p></div>
						<div  class="govt_step25" ><p>Govt. Reimbursement Upload Ack. Receipt</p></div>
						<div  class="govt_step25" ><p>Govt. Reimbursement Current Status</p></div>
					<!--	<div  class="govt_step23" style="display:none;" ><p>Govt. Reimbursement Current Status</p></div> -->
					</div>
					<div class="process" id="process" style="display:none;">
						<div class="process-row">
							<div class="process-step">
								<button type="button" id="step1"  class="btn btn-default btn-circle redstep" disabled="disabled">Step 1</button>
								<p id="p1"> Pending </p>
							</div>
							<div class="process-step">
								<button type="button" id="step2" class="btn btn-default btn-circle redstep" disabled="disabled">Step 2</button>
								<p id="p2"> Pending </p>
							</div>
							<div class="process-step">
								<button type="button" id="step3" class="btn btn-default btn-circle orangestep" disabled="disabled">Step 3</button>
								<p id="p3"> Pending </p>
							</div> 
							<!-- <div class="process-step">
								<button type="button" id="step4" class="btn  btn-circle redstep" disabled="disabled" style="display:none;">Step 4</button>
								<p id="p4" style="display:none;"> Pending </p>
							   
							</div> -->
						</div>
					</div>
					
                    <div id="gird" style="margin-top:15px;" > </div>  
               
                                          
                 </div>
            </div>   
        </div>
	</form>
    </div>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?> 
<style>
 .stepwizard-step p {
    margin-top: 10px;    
}

.process-row {
    display: table-row;
}

.process {
    display: table;     
    width: 100%;
    position: relative;
     margin-top: 25px;
}

.process-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.process-row:before {
    top: 25px;
    left:16%;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 69%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
    
}

.process-step {    
    display: table-cell;
    text-align: center;
    position: relative; width:33%;
}

.process-step p {
    margin-top:10px;
    
}

.btn-circle {
  width: 50px;
  height: 50px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 50px;
}
.govt_stepmain{width: 100%;float: left;text-align: center;}
.govt_step25{width: 33%;float: left;}
</style>

<style>
   .orangestep{
	   background-color: orange !important;
         font-weight: bold;
         color: #fff;
   }
    .redstep{
        background-color: #f00 !important;
        font-weight: bold;
        color: #fff;
    }
    .greenstep{
        background-color: green !important;
         font-weight: bold;
         color: #fff;
    }
</style>               
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {				
			
					$("#btnSubmit").click(function () {					
					showHistoryData(txtlcode.value);
					showData(txtlcode.value);			   
				});			
			
			if (ClickMode == 'auto') {		//alert("inseet");				
                           $("#btnSubmit").click();                       
			}
			function showHistoryData(val1) {
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			    $.ajax({							   
                    type: "post",
                    url: "common/cfGovLearnerProcessStatusReport.php",
                    data: "action=ShowHistory&lcode=" + val1 + "",
                    success: function (data) {						
						data = $.parseJSON(data);					
						$('#response').empty();
						if(data==0){
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> Please Enter LearnerCode </span></p>");
							}
							
						 if(data[0].step1 == "1") {
							 $("#process").show();
							 $("#processs").show();
							 document.getElementById("step1").classList.add('greenstep');
							 document.getElementById("step1").classList.remove('redstep');
							 $('#p1').html("Completed");
						 }
						 
						 
                        if(data[0].step3 == "3") { 
							 document.getElementById("step2").classList.add('greenstep');
							 document.getElementById("step2").classList.remove('redstep');
							 $('#p2').html("Completed");
						 }
						 
						 
						if(data[0].step4 == "4") {
							$('#p3').html(data[0].cstatus);
							}	
                    }
                });
			return false;
			  }

            function showData(val1) {		
                $('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			    $.ajax({							   
                    type: "post",
                    url: "common/cfGovLearnerProcessStatusReport.php",
                    data: "action=SHOW&lcode=" + val1 + "",
                    success: function (data) {
						$('#response').empty();                            
							if(data==0){
								$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span> Please Enter LearnerCode </span></p>");
							}
							else {							
									$("#gird").html(data);									
									$('#example').DataTable({						
									dom: 'Bfrtip',
									buttons: [
										'copy', 'csv', 'excel', 'pdf', 'print'
									]
								});
									
							}
                    }
                });			
            return false;
            }
		});

    </script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmgovlearnerstatus_validation.js"></script>
</html>