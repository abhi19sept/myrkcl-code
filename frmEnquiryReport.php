<?php
$title = "Enquiry Report";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == 1) {
    // you are in
} else {
    header('Location: logout.php');
    exit;
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;min-height:330px; ">  
            <div class="panel-heading">RKCL Website Enquiry Report</div>
            <div class="panel-body">
                <form name="formEnquiryReport" id="formEnquiryReport" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="Type" class="headlinelabel">Select Enquiry Type</label>
                                <select id="ddlEnquirytype" name="ddlEnquirytype" class="form-control">
                                    <option value="">Are you looking for ?</option>
                                    <option value="learner">Learner Enquiry Report</option>
                                    <option value="center">Center Enquiry Report</option>
                                </select>
                            </div> 
                            <div class="col-md-4 form-group">     
                                <label for="Start" class="headlinelabel"> Date From</label>
                                <input type="text" id="ddlstartdate" name="ddlstartdate" class="form-control" placeholder="YYYY-MM-YY" autocomplete="off" />
                            </div> 
                            <div class="col-md-4 form-group">     
                                <label for="End" class="headlinelabel"> Date To</label>
                                <input type="text" id="ddlenddate" name="ddlenddate" class="form-control" placeholder="YYYY-MM-YY" autocomplete="off" />
                            </div> 
                        </div>
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="hidden" name="action" id="action" value="ViewEnquiryReport"/>    
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>
                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">
$('#ddlstartdate').datepicker({                   
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        autoclose: true
}); 
$('#ddlenddate').datepicker({                   
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true,
        autoclose: true
}); 
var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
$(document).ready(function () {
    $("#btnSubmit").click(function () {
        if ($("#formEnquiryReport").valid())
            {
                $('#response').empty();
                $('#response').append("<div class='alert-success'><span><img src=images/ajax-loader.gif width=10px /></span><span>Proccessing.....</span></div>");
                $.ajax({ 
                    url: "common/cfEnquiryReport.php",
                    type: "POST",
                    data: $("#formEnquiryReport").serialize(),
                    success: function(data)
                      {   //alert(data);
                          $('#response').empty();
                          $("#grid").html(data);
                          $('#example').DataTable({
                                      dom: 'Bfrtip',
                                      buttons: [
                                              'copy', 'csv', 'excel', 'pdf', 'print'
                                      ]
                              });
                      }	        
                });
            }
            else
            {
              return false;
            }
    });
});
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script>
$("#formEnquiryReport").validate({
    rules: {
      ddlEnquirytype: "required",
      ddlstartdate: "required",
      ddlenddate: "required"
      
    },
    messages: {
      ddlEnquirytype: "Please Select Report Type",
      ddlstartdate: "Please Select Start Date",
      ddlenddate: "Please Select End Date"
    },  
});
</script>
<style>
    .error {
        color: #D95C5C!important;
    }
    .headlinelabel{
        font-weight: bold;
    }
</style>
</html>
