<?php
session_start();
$title = "SMS Payment";
include ('header.php');

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$status = $_POST["status"];
$firstname = $_POST["firstname"];
$amount = $_POST["amount"];
$txnid = $_POST["txnid"];
$posted_hash = $_POST["hash"];
$key = $_POST["key"];
$productinfo = $_POST["productinfo"];
$email = $_POST["email"];
$udf1 = $_POST["udf1"];
$udf2 = $_POST["udf2"];

$salt = "eCwWELxi";

require_once 'DAL/classconnection.php';
$_ObjConnection = new _Connection();
$_Response = array();
//    $_ObjConnection->Connect();

if ($udf1 == "superadmin") {

    echo "<div class='alert alert-danger' role='alert'><b>Invalid User Input!</b></div>";
} else {
    if (isset($_SESSION['User_LoginId']) && !empty($_SESSION['User_LoginId'])) {
        include ('root_menu.php');
    } else {

        global $_ObjConnection;
        $_ObjConnection->Connect();

        $_GetSessionQuery = "Select * From  tbl_user_session Where User_Session_LoginId='" . $udf1 . "'";
        $_ResponseGetSession = $_ObjConnection->ExecuteQuery($_GetSessionQuery, Message::SelectStatement);


        $_Row = mysqli_fetch_array($_ResponseGetSession[2]);


        $_SESSION['Login'] = 1;
        $_SESSION['User_LoginId'] = $_Row['User_Session_LoginId'];
        $_SESSION['User_UserRoll'] = $_Row['User_Session_UserRoll'];
        $_SESSION['User_ParentId'] = $_Row['User_Session_ParentId'];
        $_SESSION['User_Status'] = $_Row['User_Session_Status'];
        $_SESSION['User_Code'] = $_Row['User_Session_UserCode'];
        $_SESSION['User_EmailId'] = $_Row['User_Session_Email'];
        $_SESSION['UserRoll_Name'] = $_Row['User_Session_UserName'];
        $_SESSION['Organization_Name'] = $_Row['User_Session_OrgName'];
        $_SESSION['LAST_ACTIVITY'] = time();

        $_SESSION['Menu'] = CreateMenubyUserRole($_SESSION['User_UserRoll']);

        include ('root_menu.php');
    }
}

function CreateMenubyUserRole($_UserRole) {
    global $emp;
    global $response;
    $_Menu = "";

    $response = GetRootMenuByUserRole($_UserRole);

    if ($response[0] == Message::SuccessfullyFetch) {

        while ($_Row = mysqli_fetch_array($response[2])) {
            $_Menu.="<li class='dropdown menu-large'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>" . $_Row['RootName'] . "</a>";
            $parentmenu = GetParentMenuByUserRole($_UserRole, $_Row['RootCode']);

            if ($parentmenu[0] == Message::SuccessfullyFetch) {
                $_Menu.="<ul class='dropdown-menu megamenu row' > ";
                while ($_Row1 = mysqli_fetch_array($parentmenu[2])) {
                    $_Menu.= "<li class='col-sm-3' style='min-height:250px !important;'> <ul><li class='dropdown-header' style='color: #fff;'>" . $_Row1['ParentName'] . "</li>";
                    $childmenu = GetChildMenuUserRole($_UserRole, $_Row1['ParentCode']);
                    if ($childmenu[0] == Message::SuccessfullyFetch) {

                        while ($_ChildRow = mysqli_fetch_array($childmenu[2])) {
                            $_Menu.= "
							<li><a href='" . $_ChildRow['FunctionURL'] . "'>" . $_ChildRow['FunctionName'] . "</a></li>
							
							";
                        }
                    }
                    $_Menu.="</ul></li>";
                }
                $_Menu.= " </ul>";
            }
            $_Menu.="</li>";
        }
    }

    return $_Menu;
}

function GetRootMenuByUserRole($_UserRole) {
    global $_ObjConnection, $_Response;
    $_ObjConnection->Connect();
    try {
        $_SelectQuery = "Select * From vw_userrolewiserootmenu Where UserRole='" . $_UserRole . "' Order By DisplayOrder";
        //echo $_SelectQuery;
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
    } catch (Exception $_ex) {
        $_Response[0] = $_ex->getTraceAsString();
        $_Response[1] = Message::Error;
    }
    return $_Response;
}

function GetParentMenuByUserRole($_UserRole, $_RootMenu) {
    global $_ObjConnection, $_Response;
    $_ObjConnection->Connect();
    try {
        $_SelectQuery = "Select * From vw_userrolewiseparentmenu Where UserRole='" . $_UserRole . "' and RootMenu='" . $_RootMenu . "' Order By DisplayOrder";
        //echo $_SelectQuery;
        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
    } catch (Exception $_ex) {
        $_Response[0] = $_ex->getTraceAsString();
        $_Response[1] = Message::Error;
    }
    return $_Response;
}

function GetChildMenuUserRole($_UserRole, $_Parent) {
    global $_ObjConnection, $_Response;
    $_ObjConnection->Connect();
    try {

        $_SelectQuery = "Select * From vw_userrolewisefunction where "
                . "UserRole='" . $_UserRole . "' and Parent='" . $_Parent . "' Order by Display";

        $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
    } catch (Exception $_ex) {
        $_Response[0] = $_ex->getTraceAsString();
        $_Response[1] = Message::Error;
    }
    return $_Response;
}

echo "<div style='min-height:430px !important;max-height:1500px !important;'>";
echo "<div class='container'>";

require 'DAL/sendsms.php';


If (isset($_POST["additionalCharges"])) {
    $additionalCharges = $_POST["additionalCharges"];
    $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
} else {

    $retHashSeq = $salt . '|' . $status . '|||||||||' . $udf2 . '|' . $udf1 . '|' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
}
$hash = hash("sha512", $retHashSeq);

if ($hash != $posted_hash) {
    echo "<div class='alert alert-danger' role='alert'><b>Oh snap!</b> Payment Transaction Unsuccessful .</div>";
} else {

    global $_ObjConnection;
    $_ObjConnection->Connect();
    try {
        //$_User_Code =   $_SESSION['User_Code'];
       if (isset($_POST["udf2"]) && !empty($_POST["udf2"])) {

            echo "<br>";
            echo "<br>";
            echo "<div class='row'>";
            echo "<div class='col-md-8 col-md-offset-2'>";
            echo "<div class='panel panel-success'>";
            echo " <div class='panel-heading'>";
            echo "    <h3 class='panel-title'>Payment Status</h3>";
            echo "  </div>";
            echo "  <div class='panel-body'>";
            echo " <table class='table table-hover table-bordered' >";
            echo "  <tr class=''>";
            echo "    <td class='info' colspan='2' align='center'>";
            echo "<b>Thank You</b> Your Payment is Successful.";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Your Transaction ID is ";

            echo "</td>";
            echo "    <td class=''>";
            echo "$txnid";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";

            echo "We have received a payment of Rs. ";
            echo "</td>";
            echo "    <td class=''>";
            echo "$amount";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Payment For";
            echo "</td>";
            echo "    <td class=''>";
            echo "$productinfo";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Center Code";
            echo "</td>";
            echo "    <td class=''>";
            echo "$udf1";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "User Name";
            echo "</td>";
            echo "    <td class=''>";
            echo "$firstname";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Email";
            echo "</td>";
            echo "    <td class=''>";
            echo "$email";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Date";
            echo "</td>";
            echo "    <td class=''>";
            $date1 = date("d-m-Y");
            echo "$date1";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr>";
            echo "<td colspan='2' align='center'>";
            echo "<input class='hide-from-printer' type='button' value='Print' onclick='window.print()'>";
            echo "</td>";
            echo "  </tr>";
			
			
			
			
			if ($_SESSION['User_UserRoll'] == '7') {	
            
			echo "  <tr>";
            echo "<td colspan='2' align='center'>";
            echo '<a href="frmcentergroupsms.php" target="_blank">Back To Send SmS</a>';
            echo "</td>";
            echo "  </tr>";
			}
			else 		
			{
			
			echo "  <tr>";
            echo "<td colspan='2' align='center'>";
            echo '<a href="frmgroupsmsmodule.php" target="_blank">Back To Send SmS</a>';
            echo "</td>";
            echo "  </tr>";
			} 
			
			
			
            echo "</table>";

            echo "  </div>";
            echo "</div>";
            echo "</div>";
            echo "</div>";
            global $_ObjConnection;
            $_ObjConnection->Connect();
				date_default_timezone_set('Asia/Calcutta');
				$smsTimestamp = date("Y-m-d H:i:s");
				//$admissionTimestamp = strtotime($mysqlTimestamp);
            $_SelectQuery = "CALL `smssuccesspayment_new`('".$status."','". $firstname."',".$amount.",'".$txnid."','".$posted_hash."','".$key."','".$productinfo."','".$email."','".$udf1."','".$udf2."','".$smsTimestamp."')";
            $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::MultipleStatement);
            
        } else {
            echo "<br>";
            echo "<br>";
            echo "<div class='row'>";
            echo "<div class='col-md-8 col-md-offset-2'>";
            echo "<div class='panel panel-warning'>";
            echo " <div class='panel-heading'>";
            echo "    <h3 class='panel-title'>Payment Status</h3>";
            echo "  </div>";
            echo "  <div class='panel-body'>";
            echo " <table class='table table-hover table-bordered' >";
            echo "  <tr class=''>";
            echo "    <td class='info' colspan='2' align='center'>";
            echo "<b>Thank You</b> Your Payment is Successful Please wait for Learner Confirmation.";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Your Transaction ID is ";

            echo "</td>";
            echo "    <td class=''>";
            echo "$txnid";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";

            echo "Amount in Rs. ";
            echo "</td>";
            echo "    <td class=''>";
            echo "$amount";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Payment For";
            echo "</td>";
            echo "    <td class=''>";
            echo "$productinfo";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Center Code";
            echo "</td>";
            echo "    <td class=''>";
            echo "$udf1";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "User Name";
            echo "</td>";
            echo "    <td class=''>";
            echo "$firstname";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Email";
            echo "</td>";
            echo "    <td class=''>";
            echo "$email";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr class=''>";
            echo "    <td class=''>";
            echo "Date";
            echo "</td>";
            echo "    <td class=''>";
            $date1 = date("d-m-Y");
            echo "$date1";
            echo "</td>";
            echo "  </tr>";
            echo "  <tr>";
            echo "<td colspan='2' align='center'>";
            echo "<input class='hide-from-printer' type='button' value='Print' onclick='window.print()'>";
            echo "</td>";
            echo "  </tr>";
            echo "</table>";

            echo "  </div>";
            echo "</div>";
            echo "</div>";
            echo "</div>";
            global $_ObjConnection;
            $_ObjConnection->Connect();
            $_UpdatePayTranQuery = "Update tbl_payment_transaction set Pay_Tran_PG_Trnid = '" . $txnid . "', Pay_Tran_Status='SMSFeeNotConfirmed', Pay_Tran_Fname='" . $firstname . "' "
                    . "Where Pay_Tran_ITGK='" . $udf1 . "' AND Pay_Tran_RKCL_Trnid = '" . $udf2 . "'";
            $_Response3 = $_ObjConnection->ExecuteQuery($_UpdatePayTranQuery, Message::UpdateStatement);
        }
    } catch (Exception $_e) {
        $_Response[0] = $_e->getTraceAsString();
        $_Response[1] = Message::Error;
    }
}
?>	
</div>
</div>
<style>
    @media print {
        /* style sheet for print goes here */
        .hide-from-printer{  display:none; }
    }
</style>
<?php include ('footer.php'); ?>

</body>
</html>


