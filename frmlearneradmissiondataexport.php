<?php
$title="Batch Wise Learner Admission Data";
include ('header.php'); 
include ('root_menu.php');

if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_Code'] == '8550' || $_SESSION['User_UserRoll'] == '8'
	|| $_SESSION['User_UserRoll'] == '29') 
{
}
else{
	session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}	
?>
<div style="min-height:430px !important;max-height:1500px !important;">
	 <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Batch Wise Learner Admission Data</div>
                <div class="panel-body">
				   <form name="frmadmissiondataforfreshlearners" id="frmadmissiondataforfreshlearners" class="form-inline" role="form" enctype="multipart/form-data">
					<div class="container">
						<div class="container">						
							<div id="response"></div>
						</div>
							<div id="errorBox"></div>
								
								
								
							<div class="col-sm-4 form-group"> 
								<label for="course">Select Course:<span class="star">*</span></label>
								<select id="ddlCourse" name="ddlCourse" class="form-control" required="true">

								</select>
							</div> 
                    
							<div class="col-md-4 form-group">     
								<label for="batch"> Select Batch:<span class="star">*</span></label>
								<select id="ddlBatch" name="ddlBatch" class="form-control" required="true">

								</select>
							</div>	
								<div class="col-sm-4 form-group">                                  
									<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary"
										value="Show Details" style="margin-top:25px; display:none;"/>    
								</div>
                    </div>				
					
                         <div id="grid" style="margin-top:5px;"> </div>                   
                 </div>
            </div>   
        </div>
	</form>
    </div>
  </body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>                
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {
		
		function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfLearnerAdmissionDataExport.php",
                data: "action=FillCourse",
                success: function (data) {
                     $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            $.ajax({
                type: "post",
                url: "common/cfLearnerAdmissionDataExport.php",
                data: "action=FillBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });

		/* funtion for Data  */	

		$("#ddlBatch").change(function (){
			var selBatch = $(this).val();
			
				if(selBatch=='0'){
					$('#btnSubmit').hide();
				}else{
					$('#btnSubmit').show(1000);
				}
		});
		
	    $("#btnSubmit").click(function () 
		{          
			$('#response').empty();
			$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				var url = "common/cfLearnerAdmissionDataExport.php"; // the script where you handle the form input.            
				var data;
				data = "action=SHOW&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
					$.ajax({
						type: "post",
						url: url,
						data: data,
						success: function (data) {
							$('#response').empty();
							if(data=='c'){
								$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Select Course.</span></div>");
							}
							else if(data=='b'){
								$('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Please Select Batch.</span></div>");
							}
							else{
								window.open(data, '_blank');
								
							}
						}							
					});			
		   
            return false; // avoid to execute the actual submit of the form.
        });
    });

    </script>
	
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
</html>