<?php
$title = "RKCL-Dashboard";
include ('header.php'); 
include ('root_menu.php'); 
include 'common/message.php';

if (isset($_REQUEST['code'])) {
    $queryString = explode("|", base64_decode($_REQUEST["code"]));
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    $Mode = strtoupper($_REQUEST['Mode']) . 'Notifications';
    $Mode_Form = $_REQUEST['Mode'];
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='ADD'</script>";
    $Mode = 'Notifications';
    $Mode_Form = "ADD";
}

if ($_SESSION['User_Code'] == '1' || $_SESSION['User_UserRoll'] == 8) {

    $disabled = ($Mode_Form == "DELETE") ? "disabled" : "";

    ?>
<link href="css/application.css" rel="stylesheet">	
<div style="min-height:350px !important;max-height:1500px !important">	
    <div class="container" style=" margin-top:50px">
    <div class="row">
    <div class="col-md-12" >
        <div class="panel-body">
            <div class="col-md-12 bhoechie-tab-container">
                <div class="col-md-10 bhoechie-tab" style="width: 100%;">
                  <div class="bhoechie-tab-content active">
                    <center>
                        <div class="panel panel-default credit-card-box" style="min-height: 300px;">
                          <ul class="tab">
                              <li><a href="javascript:void(0)" id="showsliderdata" class="tablinks active" onclick="openCity(event, 'View')">Push New Notification</a></li>
                              <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Add')">History</a></li>
                            </ul>
                            <div id="View" class="tabcontent" style="display: block;">
                              <h4>Push New Notification</h4>
                              <form class="form-horizontal" name="appnotification" id="appnotification" method="post" >
                                  
                                  <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">Notification Tittle</label>
                                      <div class="col-sm-6">
                                          <input class="form-control" id="notificationTittle" name="notificationTittle" placeholder="Enter Notification Tittle" type="text">
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label">Notification type</label>
                                      <div class="col-sm-6">
                                          <select class="form-control" id="notificationType" name="notificationType">
                                             
                                          </select>
                                      </div>
                                      <div id='ntyperesponse' class="col-sm-3"></div>
                                  </div>
                                  <div class="form-group">
                                      <label for="inputPassword3" class="col-sm-3 control-label">Message</label>
                                      <div class="col-sm-6">
                                          <textarea class="form-control" id="notificationMessage" name="notificationMessage"></textarea>
                                      </div>
                                  </div>
                                  
                                  <div class="form-group">
                                      <div class="col-sm-12">
                                          <div class="col-sm-1"></div>
                                          <div class="col-sm-2"><input id="radiorkcl" name="rkcl" type="radio" value="rkclstaff"><label  for='radiorkcl' class="radiospan">RKCL</label ></div>
                                          <div class="col-sm-2"><input id="radiosp" name="rkcl" type="radio" value="rkclsp"><label  for='radiosp' class="radiospan">SP</label ></div>
                                          <div class="col-sm-2"><input id="radioitgk" name="rkcl" type="radio" value="rkclitgk"><label  for='radioitgk' class="radiospan">IT-GK</label ></div>
                                          <div class="col-sm-2"><input id="radioall" name="rkcl" type="radio" value="rkclall"><label  for='radioall' class="radiospan">ALL</label ></div>
                                          <div class="col-sm-2"><input id="radiolearner" name="rkcl" type="radio" value="rkcllearner"><label  for='radiolearner' class="radiospan">LEARNER</label ></div>
                                      </div>
                                  </div>
                                  <div id="rkclstaff" class="desc">
                                      <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Select Roll Name</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" id="notificationRoll" name="notificationRoll" multiple>
                                                    
                                                </select>
                                                <input  id="selectedroll" name="selectedroll" type="hidden">
                                            </div>
                                            <div class="col-md-3">
                                                <button type="button" name="rkclselectallbtnroll"  id="rkclselectallbtnroll" class="btn btn-info active" style="float: left;">Select All</button>
                                            </div>
                                        </div>
                                      <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Select Users</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" id="notificationUsersddl" name="notificationUsersddl" multiple>
                                                </select>
                                                <input class="rkcluserdisable"  id="notificationUsers" name="notificationUsers" type="hidden">
                                            </div>
                                            <div class="col-md-3">
                                                <button type="button" name="rkclselectallbtnusers"  id="rkclselectallbtnusers" class="btn btn-info active" style="float: left;">Select All</button>
                                            </div>
                                            <div id='userresponse' class="col-sm-3"></div>
                                        </div>
                                  </div>
                                  
                                  <div id="rkclsp" class="desc">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Select District</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" id="notificationDistrict" name="notificationDistrict" multiple>
                                               
                                            </select> 
                                            <input  id="selecteddistrictforsp" name="selecteddistrictforsp" type="hidden">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" name="spselectallbtndistrict"  id="spselectallbtndistrict" class="btn btn-info active" style="float: left;">Select All</button>
                                        </div>
                                        <div id='distresponse' class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Select Service Provider</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" id="notificationSp" name="notificationSp" multiple>
                                               
                                            </select> 
                                            <input class="spuserdisable"  id="notificationUsersforsp" name="notificationUsers" type="hidden">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" name="spselectallbtnspuser"  id="spselectallbtnspuser" class="btn btn-info active" style="float: left;">Select All</button>
                                        </div>
                                        <div id='userspresponse' class="col-sm-3"></div>
                                    </div>
                                  </div>
                                  
                                  <div id="rkclitgk" class="desc">
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Select Course</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" id="courseforitgk" name="courseforitgk">
                                               
                                            </select> 
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Select District</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" id="notificationDistrictitgk" name="notificationDistrictitgk" multiple>
                                               
                                            </select> 
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" name="itgkselectallbtndist"  id="itgkselectallbtndist" class="btn btn-info active" style="float: left;">Select All</button>
                                        </div>
                                        <div id='distresponse' class="col-sm-3"></div>
                                    </div>
                                      <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Select IT-GK</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" id="notificationUsersI" name="notificationUsersI" multiple>
                                                   
                                                </select>
                                                <input class="itgkuserdisable"  id="notificationUsersforitgk" name="notificationUsers" type="hidden">
                                            </div>
                                            <div class="col-md-3">
                                                <button type="button" name="itgkselectallbtnitgk"  id="itgkselectallbtnitgk" class="btn btn-info active" style="float: left;">Select All</button>
                                            </div>
                                        </div><div id='itgkloadresponse' class="col-sm-3"></div>
                                       
                                  </div>
                                  
                                  <div id="rkclall" class="desc">
                                      <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Select Users</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" id="notificationUsersA" name="notificationUsers" multiple>
                                                   
                                                </select>
                                                <input class="allgkuserdisable"  id="notificationUsersforall" name="notificationUsers" type="hidden">
                                            </div>
                                            <div class="col-md-3">
                                                <button type="button" name="allselectallbtn"  id="allselectallbtn" class="btn btn-info active" style="float: left;">Select All</button>
                                            </div>
                                            <div id='alluserresponseload' class="col-sm-3"></div>
                                        </div>
                                  </div>
                                  
                                  <div id="rkcllearner" class="desc">
                                      <div class="form-group">
                                      <label for="inputEmail3" class="col-sm-3 control-label examlebel">Course Name</label>
                                      <div class="col-sm-6">
                                          <select class="form-control" id="examCourse" name="examCourse" required>
                                              
                                          </select>
                                      </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label examlebel">Batch Name</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" id="examBatch" name="examBatch" required>

                                                </select>
                                            </div>
                                        </div>
                                      <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Select District</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" id="notificationDistrictitgkforlearner" name="notificationDistrictitgkforlearner" multiple>
                                               
                                            </select> 
                                            <input  id="itgkuserdisableforL" name="itgkuserdisableforL" type="hidden">
                                        </div>
                                        <div class="col-md-3">
                                            <button type="button" name="learnerselectallbtndist"  id="learnerselectallbtndist" class="btn btn-info active" style="float: left;">Select All</button>
                                        </div>
                                        <div id='distresponse' class="col-sm-3"></div>
                                    </div>
                                      <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Select IT-GK</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" id="notificationUserslearnerIT" name="notificationUsers" multiple>
                                                    
                                                </select>
                                                <input  id="selecteditgkforl" name="selecteditgkforl" type="hidden">
                                            </div>
                                            <div class="col-md-3">
                                                <button type="button" name="learnerselectallbtnitgk"  id="learnerselectallbtnitgk" class="btn btn-info active" style="float: left;">Select All</button>
                                            </div>
                                        </div>
                                      <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-3 control-label">Select Learner</label>
                                            <div class="col-sm-6">
                                                <select class="form-control" id="notificationUsersL" name="notificationUsers" multiple>
                                                   
                                                </select>
                                                <input class="learneruserdisable"  id="selectedlearneruser" name="notificationUsers" type="hidden">
                                            </div>
                                            <div class="col-md-3">
                                                <button type="button" name="learnerselectallbtnler"  id="learnerselectallbtnler" class="btn btn-info active" style="float: left;">Select All</button>
                                            </div>
                                            <div id='selectedlresponse' class="col-sm-3"></div>
                                        </div>
                                  </div>
                                  
                                  
                                  <div class="form-group">
                                      <div class="col-sm-offset-3 col-sm-6" id="response"></div>
                                  </div>
                                  <div class="form-group last">
                                      <div class="col-sm-offset-3 col-sm-6">
                                          <button type="submit" name="btn-slider-submit"  id="btn-slider-submit" class="btn btn-success btn-sm">Submit</button>
                                          <button type="reset" class="btn btn-default btn-sm">Reset</button>			
                                      </div>
                                  </div>
                                <input type="hidden" name="action" id="action" value="createToken">
                                <input type="hidden" name="mode" id="mode" value="<?php echo $Mode_Form; ?>">
                                <input type="hidden" name="token" id="token">
                              </form>
                            </div>
                            <div id="Add" class="tabcontent">
                              <h4>History Of Notifications</h4>
                              <div id="grid" style="margin-top:15px;"></div> 
                            </div>
                        </div>
                    </center>
                  </div>
                 </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</div>
    <?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "index.php";
    </script>
    <?php
}
?>
    <!-- LOAD JQUERY VALIDATION BASE FILE HERE -->
    <script src="js/jquery.validate.js"></script>

    <!-- LOAD FORM VALIDATION FILE HERE -->
    <script src="js/staffRegistration.js"></script>

    <script type="text/javascript">

        var Code = '<?php echo (isset($_REQUEST["code"])) ? $_REQUEST["code"] : '';?>';
        $(document).prop('title', 'Administrator : MYRKCL Application System : ' + '<?php echo $Mode?>');
        $(document).ready(function () {
            if (Mode === 'UPDATE' || Mode === 'DELETE') {
                fillForm();
            }
            else {
                
            }

            $("#appnotification").on('submit', (function (e) { 
                e.preventDefault();
                if ($(this).valid()) {
                    $('#response').empty();
                    $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    var url = "common/cfAppnotification.php";
                    var data;
                    data = $(this).serialize();

                    $.post(url, data, function (returnedData) {

                        var rd1 = $.parseJSON(returnedData);

                        if (rd1.tokenGenerationStatus === "Success") {
                              
                            var data2 = {"token": rd1.generatedToken, "action": "createToken", "mode": Mode};
                                
                            $.post(url, data2, function (returnedData2) {
                                
                                var rd2 = $.parseJSON(returnedData2);
                                if (rd2.status === "Success" || rd2.status.search === "Success") {
                                    $('#response').empty();
                                    $('#response').append("<p class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>Successfully sent notification</span></p>");
                                    window.setTimeout(function () {
                                        $('#response').empty();
                                        location.reload();
                                    }, 3000);
                                } else {
                                    $('#response').empty();
                                    $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Error to send notification</span></p>");
                                    window.setTimeout(function () {
                                        $('#response').empty();
                                    }, 5000);
                                }
                            });
                        }
                        else {
                            var messages = "We have found the following errors in the form: <BR><BR>";
                            $.each(rd1.tokenGenerationStatus, function (i, v) {
                                messages += "-  " + v + "<BR>";
                            });

                            $("#fld_response").css("display", "block")
                                .html("<div class=\"alert alert-danger text-left\"  role=\"alert\">" + messages + "</div>");
                        }
                    });
                }
            }));

            function FillNotificationType() {
               $('#ntyperesponse').empty();
               $('#ntyperesponse').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Loading.....</span></p>");
               $.ajax({
                   type: "post",
                   url: "common/cfAppnotification.php",
                   data: "action=FILLNOTIFICATION",
                   success: function (data) {
                       $("#notificationType").html(data);
                       $('#ntyperesponse').empty();
                   }
               });
           }
           FillNotificationType();
           
            function FillALLUSERNotification() {
               $('#alluserresponseload').empty();
               $('#alluserresponseload').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Loading.....</span></p>");
               $.ajax({
                   type: "post",
                   url: "common/cfAppnotification.php",
                   data: "action=FILLALLUSERNOTIFICATION",
                   success: function (data) {
                       $("#notificationUsersA").html(data);
                       $('#alluserresponseload').empty();
                   }
               });
           }
           FillALLUSERNotification();
           
           function FillDistrict() {
               
               $('#distresponse').empty();
               $('#distresponse').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Loading.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfAppnotification.php",
                    data: "action=FILLDISTRICT",
                    success: function (data) {
                        $("#notificationDistrict").html(data);
                        $("#notificationDistrictitgk").html(data);
                        $("#notificationDistrictitgkforlearner").html(data);
                        $('#distresponse').empty();
                    }
                });
            }
            FillDistrict();
            function FillCourse() {
                $.ajax({
                    type: "post",
                    url: "common/cfAppnotification.php",
                    data: "action=FILLCourseName",
                    success: function (data) {
                        $("#examCourse").html(data);
                    }
                });
            }
            FillCourse();
            function FillCourseName() {
                $.ajax({
                    type: "post",
                    url: "common/cfAppnotification.php",
                    data: "action=FILLCourseNameforitgk",
                    success: function (data) {
                        $("#courseforitgk").html(data);
                    }
                });
            }
            FillCourseName();
            $("#examCourse").change(function () {
                var selCourse = $(this).val();
                $.ajax({
                        type: "post",
                        url: "common/cfAppnotification.php",
                        data: "action=FILLALLBatch&course="+ selCourse +"",
                        success: function (data) {
                            $("#examBatch").html(data);
                        }
                    });
            });
            
            function FillRole() {
                $.ajax({
                    type: "post",
                    url: "common/cfAppnotification.php",
                    data: "action=FILLBYENTITY",
                    success: function (data) { 
                        $("#notificationRoll").html(data);
                    }
                });
            }
            FillRole();
            
            $("#notificationRoll").change(function(){
                var selMultiC = $.map($("#notificationRoll option:selected"), function (el, i) {
                      return $(el).val();
                  });
                  var UserRoll = selMultiC.join(",");
                 $("#selectedroll").val(UserRoll);
             function FillselectedUser() {
                 $('#userresponse').empty();
                 $('#userresponse').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Loading.....</span></p>");
                 $.ajax({
                     type: "post",
                     url: "common/cfAppnotification.php",
                     data: "action=FillUserNotification&UserRoll=" + UserRoll + "",
                     success: function (data) { 
                         $("#notificationUsersddl").html(data);
                         $('#userresponse').empty();
                     }
                 });
             }
             FillselectedUser();
             });
             
            $("#notificationDistrict").change(function(){
                var selMultiC = $.map($("#notificationDistrict option:selected"), function (el, i) {
                      return $(el).val();
                  });
                  var UserSPD = selMultiC.join(",");
                 $("#selecteddistrictforsp").val(UserSPD);
             function FillselectedSP() {
                 $('#useritgkpresponse').empty();
                 $('#useritgkpresponse').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Loading.....</span></p>");
                 $.ajax({
                     type: "post",
                     url: "common/cfAppnotification.php",
                     data: "action=FillUserNotificationForSP&UserSPD=" + UserSPD + "",
                     success: function (data) { 
                         $("#notificationSp").html(data);
                         $('#useritgkpresponse').empty();
                     }
                 });
             }
             FillselectedSP();
             });
             
            $("#notificationDistrictitgk").change(function(){
                var selMultiC = $.map($("#notificationDistrictitgk option:selected"), function (el, i) {
                      return $(el).val();
                  });
                  var UserTIGK = selMultiC.join(",");
                 var course = $("#courseforitgk").val();
                 $("#notificationUsersI").html('');
             function FillselectedITGK() {
                 $('#itgkloadresponse').empty();
                 $('#itgkloadresponse').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Loading.....</span></p>");
                 $.ajax({
                     type: "post",
                     url: "common/cfAppnotification.php",
                     data: "action=FillUserNotificationForITGK&UserTIGK=" + UserTIGK + "&course=" + course + "",
                     success: function (data) { 
                         $("#notificationUsersI").html(data);
                         $('#itgkloadresponse').empty();
                     }
                 });
             }
             FillselectedITGK();
             });
             
             $("#notificationDistrictitgkforlearner").change(function(){
                var selMultiC = $.map($("#notificationDistrictitgkforlearner option:selected"), function (el, i) {
                      return $(el).val();
                  });
                  var UserTIGK = selMultiC.join(",");
                 $("#itgkuserdisableforL").val(UserTIGK);
                 //var course = $("#examCourse").attr("name");
                 var course = $("#examCourse").find('option:selected').attr("name");
             function FillselectedITGKFORL() {
                 $('#userlearnerpresponse').empty();
                 $('#userlearnerpresponse').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Loading.....</span></p>");
                 $.ajax({
                     type: "post",
                     url: "common/cfAppnotification.php",
                     data: "action=FillUserNotificationForITGK&UserTIGK=" + UserTIGK + "&course=" + course + "",
                     success: function (data) { 
                         $("#notificationUserslearnerIT").html(data);
                         $('#userlearnerpresponse').empty();
                     }
                 });
             }
             FillselectedITGKFORL();
             });
             
             $("#notificationUserslearnerIT").change(function(){
                var selMultiC = $.map($("#notificationUserslearnerIT option:selected"), function (el, i) {
                      return $(el).val();
                  });
                  var UserTIGK = selMultiC.join(",");
                 $("#selecteditgkforl").val(UserTIGK);
                 var batch = $("#examBatch").val();
                 var course = $("#examCourse").val();
             function FillselectedLearner() {
                 $('#selectedlresponse').empty();
                 $('#selectedlresponse').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Loading.....</span></p>");
                 $.ajax({
                     type: "post",
                     url: "common/cfAppnotification.php",
                     data: "action=FillSELETEDLEARNER&UserTIGK=" + UserTIGK + "&course=" + course + "&batch=" + batch + "",
                     success: function (data) {
                         $("#notificationUsersL").html(data);
                         $('#selectedlresponse').empty();
                     }
                 });
             }
             FillselectedLearner();
             });
             
             
            $("#notificationUsersL").change(function(){
                var selMultiC = $.map($("#notificationUsersL option:selected"), function (el, i) {
                      return $(el).val();
                  });
                  var UserRoll = selMultiC.join(", ");
                 $("#selectedlearneruser").val(UserRoll);
             });
            $("#notificationUsersddl").change(function(){
                var selMultiC = $.map($("#notificationUsersddl option:selected"), function (el, i) {
                      return $(el).val();
                  });
                  var UserRoll = selMultiC.join(", ");
                 $("#notificationUsers").val(UserRoll);
             });
             
            $("#notificationSp").change(function(){
                var selMultiC = $.map($("#notificationSp option:selected"), function (el, i) {
                      return $(el).val();
                  });
                  var UserSP = selMultiC.join(", ");
                 $("#notificationUsersforsp").val(UserSP);
             });
            $("#notificationUsersI").change(function(){
                var selMultiC = $.map($("#notificationUsersI option:selected"), function (el, i) {
                      return $(el).val();
                  });
                  var UserSP = selMultiC.join(", ");
                 $("#notificationUsersforitgk").val(UserSP);
             });
            $("#notificationUsersA").change(function(){
                var selMultiC = $.map($("#notificationUsersA option:selected"), function (el, i) {
                      return $(el).val();
                  });
                  var ALLUser = selMultiC.join(", ");
                 $("#notificationUsersforall").val(ALLUser);
             });
            
            // form feild set accoring to radio electio 
            $("div.desc").hide();
            $("input[name$='rkcl']").click(function() {
                var test = $(this).val();
                $("div.desc").hide();
                $("#" + test).show();
            });
            
            //userloing post data hinden or show to post on cf
            $("#radiorkcl").click(function(){
                $('.learneruserdisable').prop('disabled', true);
                $('.allgkuserdisable').prop('disabled', true);
                $('.itgkuserdisable').prop('disabled', true);
                $('.spuserdisable').prop('disabled', true);
                $('.rkcluserdisable').prop('disabled', false);
            });
            $("#radiosp").click(function(){
                $('.learneruserdisable').prop('disabled', true);
                $('.allgkuserdisable').prop('disabled', true);
                $('.itgkuserdisable').prop('disabled', true);
                $('.spuserdisable').prop('disabled', false);
                $('.rkcluserdisable').prop('disabled', true);
            });
            $("#radioitgk").click(function(){
                $('.learneruserdisable').prop('disabled', true);
                $('.allgkuserdisable').prop('disabled', true);
                $('.itgkuserdisable').prop('disabled', false);
                $('.spuserdisable').prop('disabled', true);
                $('.rkcluserdisable').prop('disabled', true);
            });
            $("#radioall").click(function(){
                $('.learneruserdisable').prop('disabled', true);
                $('.allgkuserdisable').prop('disabled', false);
                $('.itgkuserdisable').prop('disabled', true);
                $('.spuserdisable').prop('disabled', true);
                $('.rkcluserdisable').prop('disabled', true);
            });
            $("#radiolearner").click(function(){
                $('.learneruserdisable').prop('disabled', false);
                $('.allgkuserdisable').prop('disabled', true);
                $('.itgkuserdisable').prop('disabled', true);
                $('.spuserdisable').prop('disabled', true);
                $('.rkcluserdisable').prop('disabled', true);
            });
            
            //select all button 
            $('#rkclselectallbtnroll').click( function() {
                $('select#notificationRoll > option').prop('selected', 'selected').change();
            });
            $('#rkclselectallbtnusers').click( function() {
                $('select#notificationUsersddl > option').prop('selected', 'selected').change();
            });
            $('#spselectallbtndistrict').click( function() {
                $('select#notificationDistrict > option').prop('selected', 'selected').change();
            });
            $('#spselectallbtnspuser').click( function() {
                $('select#notificationSp > option').prop('selected', 'selected').change();
            });
            $('#itgkselectallbtndist').click( function() {
                var course = $("#courseforitgk").val();
                function FillselectedITGKALL() {
                    $('#itgkloadresponse').empty();
                    $('#itgkloadresponse').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Loading.....</span></p>");
                    $.ajax({
                        type: "post",
                        url: "common/cfAppnotification.php",
                        data: "action=FillUserNotificationForITGKALL&course=" + course + "",
                        success: function (data) { 
                            $("#notificationUsersI").html(data);
                            $('#itgkloadresponse').empty();
                        }
                    });
                   
                }
                FillselectedITGKALL();
            });
            $('#itgkselectallbtnitgk').click( function() {
                $('select#notificationUsersI > option').prop('selected', 'selected').change();
            });
            $('#allselectallbtn').click( function() {
                $('select#notificationUsersA > option').prop('selected', 'selected').change();
            });
            $('#learnerselectallbtndist').click( function() {
                $('select#notificationDistrictitgkforlearner > option').prop('selected', 'selected').change();
            });
            $('#learnerselectallbtnitgk').click( function() {
                $('select#notificationUserslearnerIT > option').prop('selected', 'selected').change();
            });
            $('#learnerselectallbtnler').click( function() {
                $('select#notificationUsersL > option').prop('selected', 'selected').change();
            });
            
        });
// Tab Events Changes Script....    
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

    </script>

<?php include ('footer.php'); ?>