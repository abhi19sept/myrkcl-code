<?php
$title = "User Details";
include ('header.php');
include ('root_menu.php');
$superUsers = [];
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<style type="text/css">

    form > div.container {
        filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
    }

    .modal-open .container-fluid, .modal-open .container{
        filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
    }
</style>
<div style="min-height:450px !important;max-height:1500px !important">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Visit Requests</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="frmvisitdetails" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div> 
                        <div class="container">
                            <?php if (!in_array($_SESSION['User_UserRoll'], $superUsers)) { ?>
                                <div id="new-visit-btn" class="text-right">
                                    <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Schedule New Visit" style="margin-right:62px"/>
                                </div>
                                <?php if (!in_array($_SESSION['User_UserRoll'], [7])) { ?>
                                    <div class="col-md-4 form-group"><br />
                                        <p><center>Visits Between:</center></p>
                                    </div>
                                    <div class="col-md-4 form-group">     
                                        <label for="dateFrom"> Date From: <span class="star">*</span></label>
                                        <input type="text" class="form-control" name="dateFrom" id="dateFrom" value="<?php echo date("d-m-Y") ?>" readonly="true" placeholder="DD-MM-YYYY"> 
                                    </div> 

                                    <div class="col-md-4 form-group">     
                                        <label for="dateTo"> To:</label>
                                        <input type="text" class="form-control" name="dateTo" id="dateTo" readonly="true" placeholder="DD-MM-YYYY"> 
                                    </div> 
                                    <div class="col-md-4 form-group"><br />     
                                        <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Search"/>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>  
                            <div class="col-md-4 form-group"><br />
                                <p><center>Visits Confirmed Between:</center></p>
                            </div>
                            <div class="col-md-4 form-group">     
                                <label for="dateFrom"> Date From: <span class="star">*</span></label>
                                <input type="text" class="form-control" name="dateFrom" id="dateFrom" readonly="true" placeholder="DD-MM-YYYY"> 
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="dateTo"> To:</label>
                                <input type="text" class="form-control" name="dateTo" id="dateTo" readonly="true" placeholder="DD-MM-YYYY"> 
                            </div> 
                        </div> 
                        <div class="container">
                            <div class="col-md-4 form-group"><br />
                                <p><center>Visits Related To:</center></p>
                            </div>
                            <div class="col-md-4 form-group">     
                                <label for="rsp_code"> RSP: <span class="star">*</span></label>
                                <select id="rsp_code" name="rsp_code" class="form-control" >
                                    <option value="">Select RSP</option>
                                </select> 
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="itgk_code"> ITGK:</label>
                                <select id="itgk_code" name="itgk_code" class="form-control" >
                                    <option value="">Select ITGK</option>
                                </select>  
                            </div>
                            <div class="col-md-4 form-group"><br />     
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Search"/>
                            </div>
                        </div> 
                    <?php } ?>
            </div> 
            <div id="errorBox"></div>
        </div>
        <div id="grid" name="grid" style="margin-top:35px;"> </div>
        </form>
    </div>
</div>   
</div>

<div id="set_new_visit" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 900;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Schedule a new visit</h4>
            </div>
            <div class="modal-body">
                <form name="form" id="frmsetvisit" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div id="frmvisit-response"></div>

                    </div> 
                    <div class="container">
                        <div class="col-sm-4 form-group"  id="non-printable"> 
                            <label for="edistrict">Select ITGK 
                                :<span class="star">*</span></label>
                            <select id="ddlITGK" name="code" class="form-control" >
                                <option value="">Select ITGK</option>
                            </select>    
                        </div>
                        <div class="col-md-4 form-group">     
                            <label for="dateFrom">Visit Date:<span class="star">*</span></label>
                            <input type="text" class="form-control" name="visitDate" id="visitDate" readonly="true" placeholder="DD-MM-YYYY"> 
                        </div>

                        <div class="col-md-4 form-group">   
                            <input type="button" name="setVisitBtn" id="setVisitBtn" class="btn btn-primary" value="Set Visit" style="margin-top:25px;" />
                        </div>

                    </div> 

                    <div id="itgk-grid" name="itgk-grid" class="small" style="margin-top:35px;"> </div>

                    <!-- Modal -->
<div class="container">
                    <iframe id="testdoc" src="" style="width: 100%;height: 500px;border: none; display: none;"></iframe>
                    </div> 

                </form>
                <!--                <div id="itgkAddmission_list" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-dialog-normal" style="width:80%!important;">
                                 Modal content
                                <div class="modal-content">
                                        <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ITGK Admission</h4>
                                        </div>
                                        <div class="modal-body">
                                        
                                    <iframe id="viewimagesrc1" src="uploads/news/1487051012.pdf" style="width: 100%;height: 500px;border: none;"></iframe>
                                    <iframe id="viewimagesrc2" src="uploads/news/1487051012.pdf" style="width: 100%;height: 500px;border: none;"></iframe>
                                    <iframe id="viewimagesrc3" src="uploads/news/1487051012.pdf" style="width: 100%;height: 500px;border: none;"></iframe>
                                    <iframe id="viewimagesrc4" src="uploads/news/1487051012.pdf" style="width: 100%;height: 500px;border: none;"></iframe>
                                    <img id="viewimagesrc1" alt="Smiley face" height="42" width="42">
                                    <img id="viewimagesrc2" alt="Smiley face" height="42" width="42">
                                    <img id="viewimagesrc3" alt="Smiley face" height="42" width="42">
                                    <img id="viewimagesrc4" alt="Smiley face" height="42" width="42">
                                </div>	
                                    <div class="modal-footer">
                                                <button type="button" id="modal_dismiss_apl" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                </div>
                        </div>
                                   
                </div>-->
            </div>
        </div>
    </div>
</div>
</div>
<div id="myModalimage" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close closeimg">&times;</span>
            <h6>File Preview</h6>
        </div>
        <div class="modal-body" style="text-align: center;">
            <div class="panel panel-success">
                <div class="panel-heading">Exterior Photo</div>
                <div class="panel-body">
                    <div id="viewimagesrc1ftp"></div>
                    <img id="viewimagesrc1" alt="Smiley face" height="542" width="842">
                </div>
            </div>
            <div class="panel panel-success">
                <div class="panel-heading">Reception Photo</div>
                <div class="panel-body">
                    <div id="viewimagesrc2ftp"></div>
                    <img id="viewimagesrc2" alt="Smiley face" height="542" width="842">
                </div>
            </div>
            <div class="panel panel-success">
                <div class="panel-heading">Lab Photo</div>
                <div class="panel-body">
                    <div id="viewimagesrc3ftp"></div>
                    <img id="viewimagesrc3" alt="Smiley face" height="542" width="842">
                </div>
            </div>
            <div class="panel panel-success">
                <div class="panel-heading">Class Photo</div>
                <div class="panel-body">
                    <div id="viewimagesrc4ftp"></div>
                    <img id="viewimagesrc4" alt="Smiley face" height="542" width="842">
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
    .aslink {
        cursor: pointer;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    function listvisits() {
        var forminput = $("#frmvisitdetails").serialize();
        $("#grid").empty();
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=listvisits&VisitCode=0&" + forminput,
            success: function (data) {
                var data = data.trim();
                $('#response').empty();
                $("#grid").html(data);
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ],
                    scrollY: 400,
                    scrollCollapse: true,
                    paging: false
                });
            }
        });
    }

    $('#grid').on('click', 'tr td .clicklist_action', function () {
        var itgk = $(this).attr("id");
        itgk = itgk.replace('itgkAddmission_', '');
        showDataITGKpopup(itgk);


        //var  mybtn = $(this).attr("id");
        var modal = document.getElementById('myModalimage');
        //var btn = document.getElementById(mybtn);
        var span = document.getElementsByClassName("closeimg")[0];
        modal.style.display = "block";
        span.onclick = function () {
            modal.style.display = "none";
            $("#viewimagesrc1ftp").attr('src', "");
            $("#viewimagesrc2ftp").attr('src', "");
            $("#viewimagesrc3ftp").attr('src', "");
            $("#viewimagesrc4ftp").attr('src', "");
        }
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
                $("#viewimagesrc1ftp").attr('src', "");
                $("#viewimagesrc2ftp").attr('src', "");
                $("#viewimagesrc3ftp").attr('src', "");
                $("#viewimagesrc4tp").attr('src', "");
            }
        }
    });

    function showDataITGKpopup(itgk) {
        //alert(itgk);
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=showDataITGKpopup&itgk=" + itgk + "",
            success: function (data) {
                //alert(data);
                var data = data.trim();
                data = $.parseJSON(data);
//                $("#viewimagesrc1").attr('src', "upload/VISITPHOTO/" + data[0].extphoto);
//                $("#viewimagesrc2").attr('src', "upload/VISITPHOTO/" + data[0].recphoto);
//                $("#viewimagesrc3").attr('src', "upload/VISITPHOTO/" + data[0].classphoto);
//                $("#viewimagesrc4").attr('src', "upload/VISITPHOTO/" + data[0].labphoto);
$("#viewimagesrc1").hide();
$("#viewimagesrc2").hide();
$("#viewimagesrc3").hide();
$("#viewimagesrc4").hide();
                $("#viewimagesrc1ftp").html(data[0].extphoto);
                $("#viewimagesrc2ftp").html(data[0].recphoto);
                $("#viewimagesrc3ftp").html(data[0].classphoto);
                $("#viewimagesrc4ftp").html(data[0].labphoto);
            }
        });
    }


    function FillITGK() {
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=listitgk",
            success: function (data)
            {var data = data.trim();
                $("#ddlITGK").html(data);
                $("#itgk_code").html(data);
            }
        });
    }

    function getITGKInfo(itgkCode) {
        $('#itgk-grid').html('');
        if (itgkCode != '') {
            $.ajax({
                type: "post",
                url: "common/cfvisitmaster.php",
                data: "action=itgkinfo&itgk=" + itgkCode,
                success: function (data)
                {var data = data.trim();
                    $("#itgk-grid").html(data);
                }
            });
        }
    }

    function setNewVisit() {
        var itgk = $("#ddlITGK").val();
        var vdate = $("#visitDate").val();
        var itgkbdr = (itgk == '') ? "2px solid red" : "";
        $("#ddlITGK").css("border", itgkbdr);
        if (itgk != '') {
            $('#frmvisit-response').empty();
            $('#frmvisit-response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfvisitmaster.php",
                data: "action=setvisit&itgk=" + itgk + "&vdate=" + vdate,
                success: function (data) {
                    var data = data.trim();
                    $('#frmvisit-response').empty();
                    if (data == '1') {
                        listvisits();
                        $("#set_new_visit").modal("hide");
                    } else if (data != '1' && data != '0') {
                        $("#frmvisit-response").html(data);
                    } else {
                        $("#frmvisit-response").html('Unable to set, please re-try.');
                    }
                }
            });
        }
    }

    function performAction(eObj) {
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        var act = eObj.id.split('-');
        if (act[0] == 'fb') {
            window.location = 'frmvisitfeedback.php?code=' + act[1];
        } else {
            var doact = 1;
            if (act[0] == 'del') {
                doact = confirm("Are you sure to delete this visit request ?");
            }
            if (doact == 1) {
                $.ajax({
                    type: "post",
                    url: "common/cfvisitmaster.php",
                    data: "action=doAction&id=" + eObj.id,
                    success: function (data) {
                        var data = data.trim();
                       $('#response').empty();
                       $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                        if(data =='0'){
                            alert("Insufficient data to Generate Feedback");
                        }
                        else if (act[0] == 'dfb' && data != '') {
                            //window.location = 'downloadvisitfb.php?path=' + data;
                             $("#testdoc").attr('src', "common/showpdfftp.php?src=" + "visitFeedbacks/visit_feedback_" + act[1] + ".pdf");
                            $('#response').empty();
                        } else if (data == 1) {
                            listvisits();
                        }
                    }
                });
            } else {
                $('#response').empty();
            }
        }
    }

    function getRSPList() {
        $.ajax({
            type: "POST",
            url: "common/cfvisitmaster.php",
            data: "action=getrsplist",
            success: function (data) {
                var data = data.trim();
                $('#rsp_code').html(data);
            }
        });
    }

    function getITGKList() {
        rsp = rsp_code.value;
        $.ajax({
            type: "POST",
            url: "common/cfvisitmaster.php",
            data: "action=rspitgk&rsp_code=" + rsp,
            success: function (data) {
                var data = data.trim();
                $('#itgk_code').html(data);
            }
        });
    }

    $(document).ready(function () {

        FillITGK();

        listvisits();

        getRSPList();

        $("#btnShow").click(function () {
            $("#set_new_visit").modal("show");
        });

        $("#ddlITGK").change(function () {
            $(this).css("border", '');
            getITGKInfo(this.value);
        });

        $("#setVisitBtn").click(function () {
            setNewVisit();
        });

        $("#btnSubmit").click(function () {
            var isvalid = $('#frmvisitdetails').valid();
            if (isvalid) {
                listvisits();
            }
        });

        $('#visitDate').datepicker({
            format: "dd-mm-yyyy",
            orientation: "bottom auto",
            todayHighlight: true,
            //autoclose: true
        });

        $("#grid").on('click', '.aslink', function () {
            performAction(this);
        });

        $('#dateFrom').datepicker({
            format: "dd-mm-yyyy",
            orientation: "bottom auto",
            todayHighlight: true,
            //autoclose: true
        });

        $('#dateTo').datepicker({
            format: "dd-mm-yyyy",
            orientation: "bottom auto",
            todayHighlight: true,
            //autoclose: true
        });

        $("#rsp_code").change(function () {
            getITGKList();
        });

        $("#itgk_code").change(function () {
            if (!$('#btnSubmit').length) {
                listvisits();
            }
        });

    });


</script>
<script src="js/frmvisitsfilter_validation.js" type="text/javascript"></script>
</html>