<?php
$title = "Organization Form Process";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">			 
        <div class="panel panel-primary" style="margin-top:20px !important;">
            <div class="panel-heading">Approve/Reject Organization Request</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" action="" class="form-inline">     


                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <br>
                    <div class="container">

                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="learnercode">Name of Organization/Center:</label>
                            <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="ename">Registration No:</label>
                            <input type="text" class="form-control" readonly="true" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="faname">Date of Establishment:</label>
                            <input type="text" class="form-control" readonly="true" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Type of Organization:</label>
                            <input type="text" class="form-control" readonly="true" name="txtType" id="txtType" placeholder="Type Of Organization">  
                        </div>
                    </div>




                    <div class="container">




                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Document Type:</label>
                            <input type="text" class="form-control" readonly="true" name="txtDocType" id="txtDocType" placeholder="Document Type">   
                        </div>

                        <div class="col-sm-4 form-group">     
                            <label for="SelectType">Select Type of Application:</label>
                            <input type="text" class="form-control" readonly="true" name="txtRole" id="txtRole" placeholder="Role"/>     

                        </div>


                        <div class="col-sm-4 form-group"> 
                            <label for="email">Enter Email:</label>
                            <input type="text" class="form-control" readonly="true" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                        </div>


                        <div class="col-sm-4 form-group">     
                            <label for="Mobile">Enter Mobile Number:</label>
                            <input type="text" class="form-control" readonly="true" name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                        </div>


                    </div>	


                    <div class="container">




                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">District:</label>
                            <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                            <input type="hidden" class="form-control"  name="txtDistrictCode" id="txtDistrictCode">
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="edistrict">Tehsil:</label>
                            <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                        </div>

<!--                        <div class="col-sm-4 form-group"> 
                            <label for="address">Street:</label>
                            <input type="text" class="form-control" readonly="true" name="txtStreet" id="txtStreet" placeholder="Street">    
                        </div>-->


                        <div class="col-sm-4 form-group">     
                            <label for="address">Address:</label>
                            <textarea class="form-control" readonly="true" id="txtRoad" name="txtRoad" placeholder="Road"></textarea>

                        </div>
                    </div>

                    <div class="container">	

                        <div class="col-sm-4 form-group" > 
                            <label for="photo">Organization Document:<span class="star">*</span></label> </br>
<a title="Organization Document" href="#" data-toggle="modal" >
                            <img id="uploadPreview1" class="thumbnail img-responsive" src="images/user icon big.png" name="orgdoc" width="150" height="150">								  																	  
</a>
                        </div>
                        <div class="col-sm-4 form-group"> 
                            <label for="status">Action:<span class="star">*</span></label>
                            <select id="ddlstatus" name="ddlstatus" class="form-control" onchange="toggle_visibility1('remark')">
                                <option selected="true" value="">Select</option>
                                <option value="Approve" >Approve</option>
                                <option value="Reject" >Reject</option>
                            </select>    
                        </div>
                        <div class="col-sm-4 form-group" id="remark" style="display:none;"> 
                            <label for="pan">Remark:<span class="star">*</span></label>
                            <input type="text" class="form-control"  name="txtRemark" id="txtRemark"  placeholder="Remark">
                        </div>


                    </div>		

                    <div class="container">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                    </div>
                    <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
						  <div class="modal-dialog">
						  <div class="modal-content">
							<div class="modal-header">
								<button class="close" type="button" data-dismiss="modal">×</button>
								<h3 class="modal-title">Heading</h3>
							</div>
							<div class="modal-body">
								
							</div>
							<div class="modal-footer">
								<button class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						   </div>
						  </div>
						</div>
                </form> 
            </div>
        </div>   
    </div>

</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<style>
    .modal-dialog {width:800px;}
    .thumbnail {margin-bottom:6px; width:800px;}
</style>
<script type="text/javascript">
    function toggle_visibility1(id) {
        var e = document.getElementById(id);

        //alert(e);
        var f = document.getElementById('ddlstatus').value;
        //alert(f);
        if (f == "Reject")
        {
            e.style.display = 'block';
        }
        else {
            e.style.display = 'none';
        }

    }
</script>

<script type="text/javascript">

    $(document).ready(function () {
        jQuery(".thumbnail").click(function () {
            $('.modal-body').empty();
            var title = $(this).parent('a').attr("title");
            $('.modal-title').html(title);
            $($(this).parents('div').html()).appendTo('.modal-body');
            $('#myModal').modal({show: true});
        });
    });
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            //alert(1);
            fillForm();

        }

        function fillForm()
        {           //alert(Code);    
            $.ajax({
                type: "post",
                url: "common/cfOrgMaster.php",
                data: "action=EDIT&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    data = $.parseJSON(data);
                    txtName1.value = data[0].orgname;
                    txtRegno.value = data[0].regno;
                    txtEstdate.value = data[0].fdate;
                    txtType.value = data[0].orgtype;
                    txtDocType.value = data[0].doctype;
                    txtRole.value = data[0].role;
                    
                    txtEmail.value = data[0].email;
                    txtMobile.value = data[0].mobile;
                    txtDistrict.value = data[0].district;
                    txtDistrictCode.value = data[0].districtcode;
                    txtTehsil.value = data[0].tehsil;
                    //txtStreet.value = data[0].street;
                    txtRoad.value = data[0].road;
                    $("#uploadPreview1").attr('src', "upload/orgdocupload/" + data[0].orgdoc);
                    
                    

                }
            });
        }



        $("#btnSubmit").click(function () {
            if ($("#form").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfOrgMaster.php"; // the script where you handle the form input.            
                var data;
                if (Mode == 'Add')
                {

                }
                else
                {
                    data = "action=UPDATE&orgcode=" + Code + "&status=" + ddlstatus.value + "&remark=" + txtRemark.value + "&districtcode=" + txtDistrictCode.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmorgapproval.php";
                            }, 1000);

                            Mode = "Add";
                            resetForm("form");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();


                    }
                });
            }
            return false;
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmcorrectionprocess_validation.js"></script>
</html>