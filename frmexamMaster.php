<?php
$title="Exam Master";
include ('header.php'); 
include ('root_menu.php'); 
	 if (isset($_REQUEST['code'])) {
                echo "<script>var Examcode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Examcode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            if ($_SESSION['User_UserRoll'] == '1' OR $_SESSION['User_UserRoll'] == '11' OR $_SESSION['User_UserRoll'] == '14') {	
            ?>
			
 <link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:450px !important;max-height:2500px !important">
        <div class="container"> 
			

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Launch Exam Event</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                   <div id="response"></div>
						 <form name="basicform" id="basicform"  role="form" enctype="multipart/form-data">
        
        <div id="sf1" class="frm">
          <fieldset>
            <legend>Step 1 of 3</legend>

            <!--<div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Affilate : </label>
              <div class="col-lg-6">
                <input type="text" placeholder="Affilate Name" id="txtAffilate" name="txtAffilate" class="form-control" autocomplete="off">
              </div>
            </div>
			
            <div class="clearfix" style="height: 10px;clear: both;"></div>-->
			
			  <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Exam Authority: </label>
              <div class="col-lg-6">
               <select id="ddlAffilate" name="ddlAffilate" class="form-control" >
								  
                                </select>  
              </div>
            </div>
			
			
			<div class="clearfix" style="height: 10px;clear: both;"></div>
			
			
			
			 <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Exam Event Name: </label>
              <div class="col-lg-6">
               <select id="ddlEvent" size="15" name="ddlEvent" class="form-control" >
								  
                                </select>  
              </div>
            </div>
			
			
			<div class="clearfix" style="height: 10px;clear: both;"></div>
			
			
			<div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Course : </label>
              <div class="col-lg-6">
               <select id="ddlCourse" name="ddlCourse[]" size="15" class="form-control"  multiple="multiple">
								  
                                </select>  
              </div>
            </div>
			
			
			<div class="clearfix" style="height: 10px;clear: both;"></div>
			
			
			
						<div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Assessment Type : </label>
              <div class="col-lg-6">
               <select id="ddlAssessment" name="ddlAssessment" class="form-control" >
							
                                </select>  
              </div>
            </div>
			
			
			<div class="clearfix" style="height: 10px;clear: both;"></div>
			
			
				<div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Re Exam Fee : </label>
              <div class="col-lg-6">
               <select id="ddlFee" name="ddlFee" class="form-control" >
								  
                                </select>  
              </div>
            </div>
			
			
			<div class="clearfix" style="height: 10px;clear: both;"></div>
			
			
			
			<div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Exam start Date : </label>
              <div class="col-lg-6">
                
				
				<input type="text" class="form-control" name="txtAstart" id="datepicker" readonly="true" placeholder="YYYY-MM-DD">     
              </div>
            </div>
			
            <div class="clearfix" style="height: 10px;clear: both;"></div>
			
			
			
			
			<div class="form-group">
			
			
			
              <label class="col-lg-2 control-label" for="uname">Exam End Date : </label>
              <div class="col-lg-6">
                
				
				<input type="text" class="form-control" readonly="true" name="txtEnd" id="datepicker1"  placeholder="YYYY-MM-DD">
              </div>
            </div>
			
            <div class="clearfix" style="height: 10px;clear: both;"></div>
			 <div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Description: </label>
              <div class="col-lg-6">
               <textarea class="form-control" rows="3" id="txtDescription" name="txtDescription" placeholder="Description"></textarea>
              </div>
            </div>
			
			
			
            <div class="clearfix" style="height: 10px;clear: both;"></div>
			
			
			


            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-primary open1" type="button">Next <span class="fa fa-arrow-right"></span></button> 
              </div>
            </div>

          </fieldset>
        </div>

        <div id="sf2" class="frm" style="display: none;">
          <fieldset>
            <legend>Batch Master</legend>
			
			
			
			
			<div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Fresh Batches for the Events : </label>
              <div class="col-lg-6">
               <select id="ddlFreshBaches" size="15" name="ddlFreshBaches[]" class="form-control" multiple="multiple" >
								  
                                </select>  
              </div>
            </div>
			
			
			<div class="clearfix" style="height: 10px;clear: both;"></div>
			
			
			
			
			
			<div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Re-exam Batches : </label>
              <div class="col-lg-6">
               <select id="ddlReexamBatches" size="15" rows="15" cols="15" name="ddlReexamBatches[]" class="form-control" multiple="multiple" >
								  
                                </select>  
              </div>
            </div>
			
			
			
			<div class="clearfix" style="height: 10px;clear: both;"></div>


          


            <div class="clearfix" style="height: 10px;clear: both;"></div>

            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back2" type="button"><span class="fa fa-arrow-left"></span> Back</button> 
                <button class="btn btn-primary open2" type="button">Next <span class="fa fa-arrow-right"></span></button> 
              </div>
            </div>

          </fieldset>
        </div>

        <div id="sf3" class="frm" style="display: none;">
          <fieldset>
            <legend>Exam event creations</legend>
			 <legend>Step 3 of 3</legend>
			
			
			
			
			<div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Exceptional Learner Cases: </label>
              <div class="col-lg-6">
               <select id="ddlEvents" name="ddlEvents" class="form-control" multiple="multiple" >
								  
                                </select>  
              </div>
            </div>
			
			<div class="clearfix" style="height: 10px;clear: both;"></div>
			
			<div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Exceptional Learner Baches: </label>
              <div class="col-lg-6">
               <select id="ddlBaches" name="ddlBaches" class="form-control" multiple="multiple" >
								  
                                </select>  
              </div>
            </div>
			<div class="clearfix" style="height: 10px;clear: both;"></div>
			<div class="form-group">
              <label class="col-lg-2 control-label" for="uname">Data Entry start Date : </label>
              <div class="col-lg-6">
                
				
				<input type="text" class="form-control" name="txtEventAstart" id="txtEventAstart" readonly="true" placeholder="YYYY-MM-DD">     
              </div>
            </div>
			
            <div class="clearfix" style="height: 10px;clear: both;"></div>
			
			
			
			
			<div class="form-group">
			
			
			
              <label class="col-lg-2 control-label" for="uname">Data Entry  End Date : </label>
              <div class="col-lg-6">
                
				
				<input type="text" class="form-control" readonly="true" name="txtEventAEnd" id="txtEventAEnd"  placeholder="YYYY-MM-DD">
              </div>
            </div>
			
			<div class="clearfix" style="height: 10px;clear: both;"></div>

			<div class="form-group">
              <label class="col-lg-2 control-label" for="uemail">Event Status</label>
              <div class="col-lg-6">
                
                              
							 <select id="ddlStatus" name="ddlStatus" class="form-control"  >
								  
                                </select>  
							
              </div>
            </div>

            <div class="clearfix" style="height: 10px;clear: both;"></div>
            

            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <button class="btn btn-warning back3" type="button"><span class="fa fa-arrow-left"></span> Back</button> 
 <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" /> 
                <img src="spinner.gif" alt="" id="loader" style="display: none">
              </div>
			  
            </div>
			
          </fieldset>
        </div>
		
		
		
		
		
      </form>
		
							
							
                       

                       



                       

						
                </div>
				
            </div>   
        </div>
	
   
</div>



</body>

<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<script src="bootcss/js/jquery.validate.js"></script>
<script type="text/javascript"> 
 $('#txtEventAstart').datepicker({                   
	format: "yyyy-mm-dd"
});  
</script>
<script type="text/javascript"> 
 $('#txtEventAEnd').datepicker({                   
	format: "yyyy-mm-dd"
});  
</script>
<script type="text/javascript"> 
 $('#datepicker').datepicker({                   
	format: "yyyy-mm-dd"
});  
</script>

<script type="text/javascript"> 
 $('#datepicker1').datepicker({
     format: "yyyy-mm-dd"
	});  
</script>	


<script>
function validate(){
if (radioEnable.checked == 1){
          alert("Success") ;
}else{
alert("Please check checkbox and click on submit");

}
}
</script>
<script type="text/javascript">
	var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

  
		jQuery().ready(function() {
			
		// validate form on keyup and submit
		var v = jQuery("#basicform").validate({
		  rules: {
			ddlAffilate: {
			  required: true,
			   equalTo: "#ddlAffilate",
			},
			ddlEvent: {
			  required: true,
			  equalTo: "#ddlEvent",
			 
			},
			ddlCourse: {
			  required: true,
			  equalTo: "#ddlCourse",
			},
			ddlAssessment: {
			  required: true,
			   equalTo: "#ddlAssessment",
			 
			},
			 ddlFee: {
			  required: true,
			  equalTo: "#ddlFee",
			},
			 ddlAssessment: {
			  required: true,
			 equalTo: "#ddlAssessment",
			},
			
			 txtDescription: {
			  required: true,
			 equalTo: "#txtDescription",
			},
			
		
			 
			 txtEventAstart: {
			  required: true,
			 equalTo: "#txtEventAstart",
			},
			txtEventAEnd: {
			  required: true,
			 equalTo: "#txtEventAEnd",
			},
			
			 ddlStatus: {
			  required: true,
			 equalTo: "#ddlStatus",
			}

		  },
		  errorElement: "span",
		  errorClass: "help-inline-error",
		});
		
		$(".open1").click(function() {
		  if (v.form()) {
			$(".frm").hide("fast");
			$("#sf2").show("slow");
		  }
		});

		$(".open2").click(function() {
		  if (v.form()) {
			$(".frm").hide("fast");
			$("#sf3").show("slow");
		  }
		});
		
		$(".open3").click(function() {
		  if (v.form()) {
			$("#loader").show();
			 setTimeout(function(){
			   $("#basicform").html('<h2>Thanks for your time.</h2>');
			 }, 1000);
			return false;
		  }
		});
		
		$(".back2").click(function() {
		  $(".frm").hide("fast");
		  $("#sf1").show("slow");
		});

		$(".back3").click(function() {
		  $(".frm").hide("fast");
		  $("#sf2").show("slow");
		});
		$("#datepicker1").change(function () {
    var startDate = document.getElementById("datepicker").value;
    var endDate = document.getElementById("datepicker1").value;
 
    if ((Date.parse(startDate) >= Date.parse(endDate))) {
        alert("End date should be greater than Start date");
        document.getElementById("datepicker1").value = "";
    }
});




	$("#txtEventAEnd").change(function () {
    var startDate = document.getElementById("txtEventAstart").value;
    var endDate = document.getElementById("txtEventAEnd").value;
 
    if ((Date.parse(startDate) >= Date.parse(endDate))) {
        alert("End date should be greater than Start date");
        document.getElementById("txtEventAstart").value = "";
    }
});
      
	
	if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
			
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
            
            function FillAffiliate() {
                $.ajax({
                    type: "post",
                    url: "common/cfAffiliateMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlAffilate").html(data);
                    }
                });
            }

            FillAffiliate();
			
			
			
			
			 function FillEvent() {
                $.ajax({
                    type: "post",
                    url: "common/cfEventMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlEvent").html(data);
                    }
                });
            }

            FillEvent();
			
			 function FillCourse() {
                $.ajax({
                    type: "post",
                    url: "common/cfCourseMaster.php",
                    data: "action=FILLCourseName",
                    success: function (data) {
                        $("#ddlCourse").html(data);
                    }
                });
            }

            FillCourse();
			
			
			
			
			function FillExamType() {
                $.ajax({
                    type: "post",
                    url: "common/cfFeeTypeMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlAssessment").html(data);
                    }
                });
            }

            FillExamType();
			
			
			function FillExamFee() {
                $.ajax({
                    type: "post",
                    url: "common/cfExamMaster.php",
                    data: "action=FILLEXAMFEE",
                    success: function (data) {
                        $("#ddlFee").html(data);
                    }
                });
            }

            FillExamFee();
			
			
			
			function FillExceptional() {
                $.ajax({
                    type: "post",
                    url: "common/cfExamMaster.php",
                    data: "action=FILLEXCEPTION",
                    success: function (data) {
                        $("#ddlEvents").html(data);
                    }
                });
            }
            FillExceptional();
			
			$("#ddlEvents").change(function () {
                var selEvents = $(this).val();
                //alert(selEvents);
                $.ajax({
                    url: 'common/cfExamMaster.php',
                    type: "post",
                    data: "action=FILLEXAMBATCH&values=" + selEvents + "",
                    success: function (data) {
                        //alert(data);
                        $('#ddlBaches').html(data);
                    }
                });
            });
			
			$("#ddlCourse").change(function () {
				
              var selectedValues = $('#ddlCourse').val();
			 //alert(selectedValues);
                //alert(selEvents);
					FillExamBatch(selectedValues);
					FillReExamBatch(selectedValues);
                
            });
			           
			function FillExamBatch(selectedValues) {
				//alert(selectedValues);
                $.ajax({
                    type: "post",
                    url: "common/cfExamMaster.php",
                    data: "action=FILLExamAdmissionBatchcode&values=" + selectedValues + "",
                    success: function (data) {
						//alert(data);
                        $("#ddlFreshBaches").html(data);
                    }
                });
            }

            //FillBatch();
			
			
			function FillReExamBatch(selectedValues) {
                $.ajax({
                    type: "post",
                    url: "common/cfExamMaster.php",
                    data: "action=FILLExamAdmissionBatchcode&values=" + selectedValues + "",
                    success: function (data) {
                        $("#ddlReexamBatches").html(data);
                    }
                });
            }

           
			
			
			
			function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfExamMaster.php",
                    data: "action=DELETE&values=" + Examcode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmdisplayexams.php";
                           }, 1000);
                            Mode="Add";
                            resetForm("basicform");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
						
                    }
                });
            }
			
            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfExamMaster.php",
                    data: "action=EDIT&values=" + Examcode + "",
                    success: function (data) {
                        
                        //alert($.parseJSON(data)[0]['Status']);
                       // alert(data);
                        data = $.parseJSON(data);
                        ddlAffilate.value = data[0].Affilate_authority;
                        ddlEvent.value=data[0].Affilate_Event;
                        ddlCourse.value = data[0].Course_Code;
						ddlAssessment.value=data[0].Affilate_Assessment;
                        ddlFee.value = data[0].Affilate_Fee;
						datepicker.value=data[0].Affilate_Astart;
                        datepicker1.value = data[0].Affilate_End;
						txtDescription.value = data[0].Affilate_Description;
						ddlFreshBaches.value = data[0].Affilate_FreshBaches;
						ddlReexamBatches.value=data[0].Affilate_ReexamBatches;
                        ddlEvents.value = data[0].Affilate_cases;
						ddlBaches.value=data[0].Affilate_baches;
                        txtEventAstart.value = data[0].Event_Start;
						txtEventAEnd.value = data[0].Event_End;
						ddlStatus.value = data[0].Event_Status;
                        
                    }
                });
            }

         
            

            $("#btnSubmit").click(function () {
			if ($("#basicform").valid())
			{	
				
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfExamMaster.php"; // the script where you handle the form input.
               var data;
                var forminput=$("#basicform").serialize();
                if (Mode == 'Add')
                {
                    data = "action=ADD&" + forminput; // serializes the form's elements.
                }
                else
                {
					
					data = "action=UPDATE&code=" + Examcode + "&" + forminput;
                    //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                             window.setTimeout(function () {
                                window.location.href = "frmdisplayexams.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("basicform");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			}
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }


  });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example-getting-started').multiselect();
    });
</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmexammastervalidation.js"></script>

</html>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>