<?php
$title = "Digital Certificate";
include ('header.php');
include ('root_menu.php');
if ($_SESSION['User_UserRoll'] == '1') {
    //Login here with ITGK or Superadmin
} else {
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>
<style>
    .modal {z-index: 1050!important;} 
    #turmcandition{
        background-color: whitesmoke;
        text-align: justify;
        padding: 10px;
    }
    .noturm{
        margin-left: 10px !important;
    }
    .yesturm{
        margin-right: 15px;
    }
</style>
<div style="min-height:430px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Generate Digital Certificate</div>
            <div class="panel-body" style="min-height: 300px;">

                <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">

                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Event:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">

                                </select>
                            </div> 
                            <div id="details" style="display:none;">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Generated:<span class="star"></span></label>
                                <input type="text" id="noofcertificategenterated" name="noofcertificategenterated" class="form-control" disabled="disabled" />
                            </div> 
                            <div class="col-md-4 form-group"> 
                                <label for="course">Generate:<span class="star"></span></label>
                                <input type="text" id="noofcertificatepending" name="noofcertificatepending" class="form-control" disabled="disabled" />
                            </div>
                            <div class="col-md-4 form-group"> 
                                <label for="course">Signed:<span class="star"></span></label>
                                <input type="text" id="signedcertificate" name="signedcertificate" class="form-control" disabled="disabled" />
                            </div>
                            </div>    
                        </div>

                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmitlist" id="btnSubmitlist" class="btn btn-primary" value="View Status"/>    
                            </div>
                            <div class="col-md-4 form-group"></div>
                            <div class="col-md-4 form-group" id="btngen" style="display:none;">     
                                <input type="submit" name="btnGenerate" id="btnGenerate" class="btn btn-primary" value="Click To Generate Certificates"/>    
                            </div>
                            <div class="col-md-4 form-group" id="btnsig" style="display:none;">     
                                <input type="submit" name="btnSign" id="btnSign" class="btn btn-primary" value="Click To Sign Certificates"/>    
                            </div>
                        </div>
                        <div class="container">
                            <div id="response"></div>
                            <div id="responsecertificate" style="width: 90%;"></div>

                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>
                        <!--                        <div id="view" style="margin-top:5px; width:94%;"> </div>-->

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<link href="css/popup.css" rel="stylesheet" type="text/css">
<div id="myModalEresponse" class="modal" style="padding-top:180px !important;">         
    <div class="modal-content" style="width: 60%;border-radius: 15px; text-align: center;">
        <div class="modal-body" style="min-height: 175px;padding: 0 16px;" id="Eresponse">

        </div>
        <button class="btn btn-primary closeeresponse" style="margin-bottom: 15px;">Close</button>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        $("#details").css("display","none");
        function FillEvents() {
            $.ajax({
                type: "post",
                url: "common/cfDigitalCertificateBulk.php",
                data: "action=FILLLinkAadharCourse",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillEvents();
        $("#btnSubmitlist").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=20px style='margin: 5px;' /></span><span style='font-weight: bold;'> Processing please wait..... </span></p>");
                var url = "common/cfDigitalCertificateBulk.php"; // the script where you handle the form input.     
                var data;
                var event = $('#ddlCourse').val();
                data = "action=GETDATAITGKWISE&event=" + event + ""; //            
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#response').empty();
                        $("#details").css("display","block");
                        $("#btngen").css("display","block");
                        $("#btnsig").css("display","block");
                        data = $.parseJSON(data);
                        $('#noofcertificategenterated').val(data[0].noofcertificategenterated);
                        $('#noofcertificatepending').val(data[0].noofcertificatepending);
                        $('#signedcertificate').val(data[0].signedcertificate);
                    }
                });

            }
            return false;
        });
        $("#btnGenerate").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=20px style='margin: 5px;' /></span><span style='font-weight: bold;'> Processing please wait..... </span></p>");
                var url = "common/cfDigitalCertificateBulk.php"; // the script where you handle the form input.     
                var data;
                var event = $('#ddlCourse').val();
                data = "action=GENRATECERTIFICATES&event=" + event + ""; //            
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#response').empty();
                        alert(data);
                    }
                });

            }
            return false;
        });
        $("#btnSign").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=20px style='margin: 5px;' /></span><span style='font-weight: bold;'> Processing please wait..... </span></p>");
                var url = "common/cfDigitalCertificateBulk.php"; // the script where you handle the form input.     
                var data;
                var event = $('#ddlCourse').val();
                data = "action=SIGNCERTIFICATES&event=" + event + ""; //            
                $.ajax({
                    type: "post",
                    url: url,
                    data: data,
                    success: function (data) {
                        $('#response').empty();
                        alert(data);
                    }
                });

            }
            return false;
        });

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
        background-color: #e2d9d9;
        width: 91%;
    }
</style>
</html>
?>