<?php 

require_once 'DAL/classconnection.php';

    $_ObjConnection = new _Connection();
	$_ObjConnection->Connect();
	
	
function dbConnection() {
		global $_ObjConnection;

		return $_ObjConnection;
	}	
	
processNameChangeTransactions();
	
function processNameChangeTransactions() {
	$_ObjConnection = dbConnection();
	echo $query = "SELECT Pay_Tran_ITGK, Pay_Tran_PG_Trnid FROM tbl_payment_transaction WHERE timestamp > '2018-03-01 00:00:00' AND Pay_Tran_ProdInfo LIKE('%change%') AND Pay_Tran_Status LIKE ('%rec%')";
	$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
	if (mysqli_num_rows($_Responses[2])) {
		echo "p1";
		while($row = mysqli_fetch_array($_Responses[2])) {
			echo "p2";
			updateITGKNameAddressDetails($row['Pay_Tran_ITGK'], $row['Pay_Tran_PG_Trnid']);
		}
	}
	echo "p3";
}

function updateITGKNameAddressDetails($centerCode, $transactionTxId) {
		$_ObjConnection = dbConnection();

		echo $query = "SELECT od.Organization_Code, od.Organization_User, ca.* FROM tbl_organization_detail od INNER JOIN tbl_user_master um ON um.User_Code = od.Organization_User INNER JOIN tbl_payment_transaction pt ON um.User_LoginId = pt.Pay_Tran_ITGK INNER JOIN  tbl_change_address_itgk ca ON ca.fld_ref_no = pt.Pay_Tran_AdmissionArray WHERE pt.Pay_Tran_ITGK = '" . $centerCode . "' AND pt.Pay_Tran_PG_Trnid = '" . $transactionTxId . "'";
		$_Responses = $_ObjConnection->ExecuteQuery($query, Message::SelectStatement);
		if (mysqli_num_rows($_Responses[2])) {
			echo "C1";
			$nacRow = mysqli_fetch_array($_Responses[2]);
			print_r($nacRow);
			if ($nacRow['fld_requestChangeType'] == 'name') {
				echo "C2";
				echo $updateQuery = "UPDATE tbl_organization_detail SET Organization_Name = '" . $nacRow['Organization_Name'] . "' WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
			} elseif ($nacRow['fld_requestChangeType'] == 'address' && $nacRow['Organization_AreaType'] == 'Urban') {
				echo "C3";
				$updateQuery = "UPDATE tbl_organization_detail SET
			Organization_Type = '" . $nacRow['Organization_Type'] . "',
			Organization_Address = '" . $nacRow['Organization_Address'] . "',
			Organization_AreaType = '" . $nacRow['Organization_AreaType'] . "',
			Organization_Municipal_Type = '" . $nacRow['Organization_Municipal_Type'] . "',
			Organization_Municipal = '" . $nacRow['Organization_Municipal'] . "', 
			Organization_WardNo = '" . $nacRow['Organization_WardNo'] . "' 
			WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
			} elseif ($nacRow['fld_requestChangeType'] == 'address' && $nacRow['Organization_AreaType'] == 'Rural') {
				echo "C4";
				$updateQuery = "UPDATE tbl_organization_detail SET
			Organization_Type = '" . $nacRow['Organization_Type'] . "',
			Organization_Address = '" . $nacRow['Organization_Address'] . "',
			Organization_AreaType = '" . $nacRow['Organization_AreaType'] . "',
			Organization_Village = '" . $nacRow['Organization_Village'] . "',
			Organization_Gram = '" . $nacRow['Organization_Gram'] . "', 
			Organization_Panchayat = '" . $nacRow['Organization_Panchayat'] . "' 
			WHERE Organization_Code = '" . $nacRow['Organization_Code'] . "' AND Organization_User = '" . $nacRow['Organization_User'] . "'";
			}
			echo "C5";
			if ($updateQuery) {
				$_ObjConnection->ExecuteQuery($updateQuery, Message::UpdateStatement);
			}
		}
	}