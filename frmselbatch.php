<?php
$title = "Select Course Batch";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var CourseBatch=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var CourseBatch=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Select Course Batch
                <div style="float:right">
                    Available Intake: <div style="display: inline" id="txtintakeavailable"> </div>
                </div>
            </div>

            <div class="panel-body">           
                <form name="frmCourseBatch" id="frmCourseBatch" class="form-inline" role="form" method="post" action="frmadmission.php"> 
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-10 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }">  </select>
                        </div> 
                    </div>                          

                    <div class="container">
                        <div class="col-md-6 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"> </select>
                        </div>
                    </div>

                    <div class="container">
                        <a href="frmadmission.php" style="text-decoration:none;"> <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/> </a>
                    </div>

                    <div id="gird" style="margin-top:35px;"> </div>	
            </div>
        </div>   
    </div>
</form>
</div>
<?php include ('footer.php');
include'common/message.php'; ?>
</body>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            if (selCourse == 3) {
                //alert("ok");
                $("#ddlCourse").val('');
                //$("#ddlBatch").val('');
                $('#ddlBatch').html(' ');
                return false;
            }
                else if (selCourse == 23) {
                window.location.href = "frmclickadmission.php";
            }
			else {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLEventBatch&values=" + selCourse + "",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });
            }
        });

        $("#ddlBatch").change(function () {
            var selBatch = $(this).val();
//alert(selBatch);			
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=FILLIntake&values=" + selBatch + "",
                success: function (data) {
                    document.getElementById('txtintakeavailable').innerHTML = data.trim();
                    if (document.getElementById('txtintakeavailable').innerHTML < '1') {
                        alert("Intake Not Available");
                        window.location.href = "frmmessage.php";
                    }
                }
            });
        });
    });
</script>
</html>