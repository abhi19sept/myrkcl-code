<?php
$title = "Exam choice Status";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
// echo $_SESSION['User_UserRoll'];
if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '8' ) {
?>
<link href="css/extra.css" rel="stylesheet" type="text/css" />
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Manage Exam Locations

            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <div id="response"></div>
                <form name="frmexamchoicestatus" id="frmexamchoicestatus" class="form-inline" action=""> 
            <?php
            if ($_SESSION['User_UserRoll'] == '1') {
            ?>
                <div class="container">
                    <div class="col-sm-6 form-group"> 
                        <div id="hard_reset">
                            <input type="button" name="hardreset" id="hardreset" class="btn btn-primary" value="Step1: Start / Hard Reset"/>
                        </div>
                    </div>
                    <div class="col-sm-6 form-group"> 
                        <div id="assign_choices">
                            <input type="button" name="assignchoices" id="assignchoices" class="btn btn-primary" value="Step2: Assign Locations"/>
                        </div>
                    </div>
                    <div class="col-sm-6 form-group"> 
                        <div id="auto_switch">
                            <input type="button" name="autoswitch" id="autoswitch" class="btn btn-primary" value="Step3: Auto Switch Choices"/>
                        </div>
                    </div>
                    <div class="col-sm-6 form-group"> 
                        <div id="reset_choices">
                            <input type="button" name="resetchoices" id="resetchoices" class="btn btn-primary" value="Reset On Initial Allotments"/>
                        </div>
                    </div>
                </div>
            <?php } ?>
                <div class="container">
                
                    
                    <div class="col-sm-6 form-group"> 
                        <div id="set_as_finalized">
                            <input type="button" name="setasfinalized" id="setasfinalized" class="btn btn-primary" value="Set allotments as finalized"/><br /><span class="small">** If location counts are ready to send to the vmou</span>
                        </div>
                    </div>
                    <div class="col-sm-6 form-group"> 
                        <div id="reset_on_finalized">
                            <input type="button" name="resetonfinalized" id="resetonfinalized" class="btn btn-primary" value="Reset on finalized allotments"/>
                        </div>
                    </div>
                    <div class="col-sm-6 form-group"> 
                        &nbsp;
                    </div>
                    <div class="col-sm-6 form-group"> 
                        <div id="final_mapping">
                            <input type="button" name="finalmapping" id="finalmapping" class="btn btn-primary" value="Generate Final Mapping"/>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="col-sm-6 form-group"> 
                        <label for="batch"> Select Exam Event:<span class="star">*</span></label>
                        <select id="ddlExamEvent" name="ddlExamEvent" class="form-control">

                        </select>
                    </div>
                    <div class="col-sm-6 form-group"> 
                        <label for="batch"> Select District:<span class="star">*</span></label>
                        <select id="ddlDistrict" name="ddlDistrict" class="form-control">

                        </select>
                        <input type="hidden" maxlength="8" id="ddlExamITGK" name="ddlExamITGK" value="" class="form-control" />
                    </div>
                    <!-- <div class="col-sm-4 form-group"> 
                        <label for="batch">ITGK Code: </label>
                        
                    </div> -->


                    
                </div>
                <br />
                    <div id="districts"></div>
                    <br />
                    <div id="lcounts_gird"></div>
                    <div id="gird"></div>
                </form>

            </div>

        </div>   
    </div>

<div id="location_allotment_list" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>

                <h4 class="modal-title" id="location_title">Exam Location Allotments of, <span id="location_name"></span></h4>
            </div>
            <div class="modal-body" id="location_allotments">
            </div>
        </div>
    </div>
</div>

</div>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<style type="text/css">
    #footer{display:none;}
    .aslink{color:#3333CC;}
    .aslink:hover{color:#990066; text-decoration: underline; font-weight: bold; cursor: pointer;}
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    function showProcessing(element, showImg, msg) {
        $(element).empty();
        if (showImg == 1) {
            addloader();
        	msg = (msg == '') ? 'Processing.....' : msg;
            $(element).append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>" + msg + "</span></p>");
        } else {
            removeloader();
        }
    }

    function FillEvent() {
        showProcessing('#response', 1, '');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=FillExamEvents",
            success: function (data) {
                $("#ddlExamEvent").html(data);
                showProcessing('#response', 0, '');
            }
        });
    }

    function showData() {
        var eventid = $("#ddlExamEvent").val();
        var itgk = $("#ddlExamITGK").val();
        if (eventid != '') {
            showProcessing('#response', 1, '');
            $.ajax({
                type: "post",
                url: "common/cfexamlocations.php",
                data: "action=ShowDistricts&eventid=" + eventid + "&itgk=" + itgk,
                success: function (data) {
                    showProcessing('#response', 0, '');
                    $("#ddlDistrict").html(data);
                }
            });
        } else {
            alert('Please select exam event.');
        }
    }

    function showDistrictLocationCounts(districtId, eventid) {
        showProcessing('#response', 1, '');
        var itgk = $("#ddlExamITGK").val();
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=showDistrictLocationCounts&eventid=" + eventid + "&districtId=" + districtId + "&itgk=" + itgk,
            success: function (data) {
                $('#lcounts_gird').html(data);
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'csv', 'pdf', 'print'
                    ]
                });
                showEditableLocationCounts(districtId, eventid, itgk);
            }
        });
    }

    function showEditableLocationCounts(districtId, eventid, itgk) {
        showProcessing('#response', 1, '');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=showEditableLocationCounts&eventid=" + eventid + "&districtId=" + districtId + "&itgk=" + itgk,
            success: function (data) {
                showProcessing('#response', 0, '');
                $('#gird').html(data);
            }
        });
    }

    function chooseThesil(obj) {
        var ddclsname = '.ddlThesil_' + obj.id;
        var countclsname = '.ThesilCount_' + obj.id;
        showProcessing(ddclsname, 1, '');
        showProcessing(countclsname, 0, '');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=getDistrictExamThesils&districtId=" + obj.value + "&eventId=" + ddlExamEvent.value + "&tehsilId=" + obj.id,
            success: function (data) {
                showProcessing(ddclsname, 0, '');
                $(ddclsname).html(data);
            }
        });
    }

    function getThesilDD(obj) {
        var ddclsname = '#district_thesils';
        showProcessing(ddclsname, 1, '');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=getDistrictThesilsDD&districtId=" + obj.value + "&eventId=" + ddlExamEvent.value + "&elem=" + obj.id,
            success: function (data) {
                showProcessing(ddclsname, 0, '');
                $(ddclsname).html(data);
            }
        });
    }

    function setExamThesil(obj) {
        if (obj.value != '') {
            var follow = $('.follow_choice').is(':checked');
            var chk = (follow) ? 1 : 0;
            var itgk = $("#ddlExamITGK").val();
            $.ajax({
                type: "post",
                url: "common/cfexamlocations.php",
                data: "action=setExamThesil&tehsilId=" + obj.value + "&elementId=" + obj.id + "&itgk=" + itgk + "&follow=" + chk,
                success: function (data) {
                    $('#response').html(data);
                    alert('Exam location has been updated successfully.');
                }
            });
            showDistrictLocationCounts(ddlDistrict.value, ddlExamEvent.value);
        }
    }

    function autoswitch(eventid) {
        showProcessing('#auto_switch', 1, 'Please wait! auto switching of choices will take time aprox. 15 miniutes...');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=SwitchChoices&eventid=" + eventid,
            success: function (data) {
                $('#auto_switch').html(data);
                showProcessing('#response', 0, '');
                showData();
            }
        });
    }

    function resetchoices(eventid) {
        showProcessing('#reset_choices', 1, '');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=resetChoices&eventid=" + eventid,
            success: function (data) {
                $('#reset_choices').html(data);
                showProcessing('#response', 0, '');
                showData();
            }
        });
    }

    function resetonfinalized(eventid) {
        showProcessing('#reset_on_finalized', 1, '');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=resetonfinalized&eventid=" + eventid,
            success: function (data) {
                $('#reset_on_finalized').html(data);
                showProcessing('#response', 0, '');
                showData();
            }
        });
    }

    function hardresetchoices() {
        showProcessing('#hard_reset', 1, '');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=hardreset",
            success: function (data) {
                $('#hard_reset').html(data);
                showProcessing('#response', 0, '');
            }
        });
    }

    function assignchoices() {
        showProcessing('#assign_choices', 1, 'Please wait ! assign locations will take time aprox. 20 miniuts...');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=assignchoices",
            success: function (data) {
                $('#assign_choices').html(data);
                showProcessing('#response', 0, '');
                alert('Assign choices has been done, now please perform Step 3, of Auto Switch.');
            }
        });
    }

    function setasfinalized(eventid) {
        showProcessing('#set_as_finalized', 1, '');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=setasfinalized&eventid=" + eventid,
            success: function (data) {
                $('#set_as_finalized').html(data);
                showProcessing('#response', 0, '');
            }
        });
    }

    function finalmapping(eventid) {

        showProcessing('#final_mapping', 1, '');
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=generateFinalMapping&eventid=" + eventid,
            success: function (data) {
                $('#final_mapping').html(data);
                showProcessing('#response', 0, '');
                window.reload();
            }
        });
    }

    function showAllotments(elem) {
        showProcessing('#response', 1, '');
        var itgk = $("#ddlExamITGK").val();
        var eventid = $("#ddlExamEvent").val();
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=getLocationName&eventid=" + eventid + "&elem=" + elem.id + "&itgk=" + itgk,
            success: function (data) {
                showProcessing('#response', 0, '');
                showProcessing('#location_allotments', 1, '');
                $('#location_name').html(data);
                $("#location_allotment_list").modal("show");
                getLocationList(eventid, elem, itgk);
            }
        });
    }

    function getLocationList(eventid, elem, itgk) {
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=getLocationList&eventid=" + eventid + "&elem=" + elem.id + "&itgk=" + itgk,
            success: function (data) {
                $('#location_allotments').html(data);
                showProcessing('#response', 0, '');
            }
        });
    }

    function markNewLocationITGK(elem) {
        var eventid = $("#ddlExamEvent").val();
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=getLocationItgkIds&eventid=" + eventid + "&newthesil=" + elem.value + "&elem=" + elem.id,
            success: function (data) {
                $('.chooseItgk option').addClass('star');
                if (data != '') {
                    var ids = data.split(',');
                    for (var i = 0; i < ids.length; i++) {
                        $('.chooseItgk option[value="' + ids[i] + '"]').removeClass('star');
                    }
                }
            }
        });
    }

    function showItgkLearnersList(elem, byType, chk) {
        var eventid = $("#ddlExamEvent").val();
        showProcessing('#itgk_list', 1, '');
        var eid = elem.id.split('_');
        var idname = '';
        var itgkval = elem.value;
        var locval = elem.value;
        if (byType == 'itgk') {
            idname = '#location_' + eventid + '_' + eid[1] + '_' + eid[2];
            locval = $(idname).val();
        } else {
            idname = '#itgk_' + eid[2] + '_' + eid[3];
            itgkval = $(idname).val();
        }
        var follow = (chk) ? 1 : 0;
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=getItgkList&eventid=" + eventid + "&elem=" + elem.id + "&itgk=" + itgkval + "&newthesil=" + locval + "&follow=" + follow,
            success: function (data) {
                $('#itgk_list').html(data);
                showProcessing('#response', 0, '');
            }
        });
    }

    function checkedAll (checked, limit) {
        $(".checklearner").prop('checked', false);
        if (checked && limit == 500000) {
            $("#checkbycount").val('');
            $(".checklearner").prop('checked', checked);
        } else if (limit > 0) {
            $("#checkall").prop('checked', false);
            var chk = 1;
            $('.checklearner').each(function() {
                if (chk <= limit) {
                    $(this).prop("checked",checked);
                }
                chk++;
            });
        } else {
            $("#checkbycount").val('');
        }
    }

    function switchLocation(eid) {
        var eventid = $("#ddlExamEvent").val();
        var itgk = $('#itgk_' + eid).val();
        var district = $('#district_' + eid).val();
        var location = $('#location_' + eventid + '_' + eid).val();
        if (itgk != '' && district != '' && location != '') {
            var chkbox = false;
            var ids = '';
            if ($('#checkall').is(':checked')) {
                var chkbox = true;
                ids = 'all';
            } else {
                $('.checklearner').each(function() {
                    if ($(this).is(':checked')) {
                        chkbox = true;
                        ids = ids + $(this).val() + ',';
                    }
                });
            }
            if (chkbox) {
                switchSelectedLearners(eventid, itgk, district, location, eid, ids);
            } else {
                alert("Please select any learner to switch.");
            }
        } else {
            alert("Unable to process, due to invalid input values.");
        }
    }

    function switchSelectedLearners(eventid, itgk, district, location, elem, ids) {
        var eventid = $("#ddlExamEvent").val();
        showProcessing('#itgk_list', 1, '');
        var follow = $('.follow_choice').is(':checked');
        var chk = (follow) ? 1 : 0;
        $.ajax({
            type: "post",
            url: "common/cfexamlocations.php",
            data: "action=switchSelectedLearnerLocation&eventid=" + eventid + "&district=" + district + "&itgk=" + itgk + "&location=" + location + "&elem=" + elem + "&ids=" + ids + "&follow=" + chk,
            success: function (data) {
                $('#itgk_list').html(data);
                alert('Exam location of selected learners has been switced.');
                showDistrictLocationCounts(ddlDistrict.value, ddlExamEvent.value);
                $("#location_allotment_list").modal("hide");
            }
        });
    }

    function addloader() {
        $("body").append("<div id='loader-wrapper'><div id='loader'></div></div>");
    }

    function removeloader() {
        $('div[id=loader-wrapper]').remove();
    }

    $(document).ready(function () {
        FillEvent();
        $("#ddlExamEvent").change(function () {
            $('#lcounts_gird').html('');
            $('#gird').html('');
            $('#ddlDistrict').html('');
            if (this.value != '') {
                showData();
            }
        });
        $("#ddlExamITGK").change(function () {
            $('#lcounts_gird').html('');
            $('#gird').html('');
            $('#ddlDistrict').html('');
            if (this.value != '' && this.value.length == 8) {
                showData();
            }
        });
        $("#ddlDistrict").change(function () {
            $('#lcounts_gird').html('');
            $('#gird').html('');
            if (this.value != '') {
                showDistrictLocationCounts(this.value, ddlExamEvent.value);
            }
        });
        $("#autoswitch").click(function () {
            var eventid = $("#ddlExamEvent").val();
            if (eventid != '') {
                if (confirm("Are you sure to auto switch the choices?")) {
                    $('#lcounts_gird').html('');
                    $('#gird').html('');
                    $('#ddlDistrict').html('');
                    autoswitch(eventid);
                }
            } else {
                alert('Please select exam event first.');
            }
        });

        $("#hardreset").click(function () {
            if (confirm("This will remove and reset all the data of assigned locations, are you sure to hard reset ?")) {
                $('#lcounts_gird').html('');
                $('#gird').html('');
                $('#ddlDistrict').html('');
                hardresetchoices();
            }
        });

        $("#assignchoices").click(function () {
            if (confirm("Are you sure, to assign exam locations, to all eligibile learners, according to their center choices ?")) {
                $('#lcounts_gird').html('');
                $('#gird').html('');
                $('#ddlDistrict').html('');
                assignchoices();
            }
        });

        $("#resetchoices").click(function () {
            var eventid = $("#ddlExamEvent").val();
            if (eventid != '') {
                if (confirm("Are you sure to reset choices?")) {
                    $('#lcounts_gird').html('');
                    $('#gird').html('');
                    $('#ddlDistrict').html('');
                    resetchoices(eventid);
                }
            }
        });
        $("#finalmapping").click(function () {
            var eventid = $("#ddlExamEvent").val();
            if (eventid != '') {
                if (confirm("Sure ? You need to generate final exam mapping, \n\nNote: Once you generated this, then you are not allow to map exam locations any more.")) {
                    finalmapping(eventid);
                }
            }
        });

        $("#setasfinalized").click(function () {
            var eventid = $("#ddlExamEvent").val();
            if (eventid != '') {
                if (confirm("Ensure your location counts are perfect, to send to VMOU.")) {
                    setasfinalized(eventid);
                }
            }
        });
        
        $('#gird').on('change', '.chooseThesil', function () {
            chooseThesil(this);
        });
        $('#gird').on('change', '.setExamThesil', function () {
            if (confirm('Are you sure to switch exam locations?')) {
                setExamThesil(this);
            }
        });
        $('#gird').on('click', '.ThesilCounts', function () {
            showAllotments(this);
        });
        $("#location_allotment_list").on('change', '.chooseItgk', function () {
            var chk = $('.follow_choice').is(':checked');
            showItgkLearnersList(this, 'itgk', chk);
        });
        $("#location_allotment_list").on('change', '.switch_thesil', function () {
            var chk = $('.follow_choice').is(':checked');
            if (chk) {
                markNewLocationITGK(this);
                showItgkLearnersList(this, 'location', chk);
            }
        });
        $("#location_allotment_list").on('change', '#checkbycount', function () {
            checkedAll (true, this.value);
        });
        $("#location_allotment_list").on('change', '#checkall', function () {
            checkedAll (this.checked, 500000);
        });
        $("#location_allotment_list").on('click', '.switch_location', function () {
            switchLocation(this.id);
        });
        $("#location_allotment_list").on('change', '.chooseDistrict', function () {
            getThesilDD(this);
        });
    });
</script>
</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>
