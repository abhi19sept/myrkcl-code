<?php
$title = "Admission Form";
include ('outer_header_admission.php');
if (isset($_POST['MobNo'])) {
    echo "<script>var MobNo=" . $_POST['MobNo'] . "</script>";
} else {
    echo "<script>var MobNo=0</script>";
}
?>
<script type="text/javascript">
    if(MobNo == '0'){
            window.location.href = 'frmlearneradmission.php';
        }
</script>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:1500px !important;">
<div class="container"> 
<div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading" style="height:auto !important">
                <div class="container">
                    <div  class="col-sm-4"> Application Form </div>

<!--                    <div class="col-sm-4">
                        Course: <div style="display: inline" id="txtcoursename"> </div>
                    </div>

                    <div class="col-sm-4">
                        Application Month: <div style="display: inline" id="txtbatchname"> </div>
                    </div>-->

                </div>


            </div>

            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmAdmissionForm" id="frmAdmissionForm" class="form-inline" role="form" enctype="multipart/form-data"> 
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                         <div class="col-sm-3 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }">  </select>
                        </div> 
                        <div class="col-md-3 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Batch')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"> </select>
                        </div>
                        <div class="col-sm-3 form-group"> 
                            <label for="designation">District:<span class="star">*</span></label>
                            <select id="ddlCenterDistrict" name="ddlCenterDistrict" class="form-control">                                  
                            </select>
                        </div>     

                        <div class="col-sm-3 form-group"> 
                            <label for="marks">Tehsil:<span class="star">*</span></label>
                            <select id="ddlCenterTehsil" name="ddlCenterTehsil" class="form-control">                                 
                            </select>
                        </div>
                        <div class="col-sm-3">     
                            <label for="batch"> Select IT-GK Code:</label>
                            <select id="ddlCenter" name="ddlCenter" class="form-control">

                            </select>                                   
                        </div>  

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" style="margin-top:25px; display:none;"/>    
                        </div>
                    </div>
                    <br>
                    <div id="CenterDetail" style="display:none;">
                        <div class="panel panel-info">
                            <div class="panel-heading">Your Selected IT-GK Detail</div>
                            <div class="panel-body">
                                <div class="container">
                                    <div class="col-sm-4 form-group">     
                                        <label for="learnercode">Name of Organization/Center:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="ename">Mobile No:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtMobile" id="txtMobile" placeholder="Registration No">     
                                    </div>


                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Email Id:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtEmail" id="txtEmail"  placeholder="District">   
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Address:</label>
                                        <textarea class="form-control" readonly="true" name="txtAddress1" id="txtAddress1"  placeholder="Tehsil"></textarea>   
                                    </div>
                                </div>  
                                 <div class="container">
                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">District:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtDistrict" id="txtDistrict"  placeholder="District">   
                                    </div>

                                    <div class="col-sm-4 form-group"> 
                                        <label for="edistrict">Tehsil:</label>
                                        <input type="text" class="form-control" readonly="true" name="txtTehsil" id="txtTehsil"  placeholder="Tehsil">   
                                    </div>
                                </div> 

                            </div>
                        </div>

                    
                    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading" style="height:40px">
            <div  class="col-sm-4"> Admission Form </div>

            <div class="col-sm-4">
                Course: <div style="display: inline" id="txtcoursename"> </div>
            </div>

            <div class="col-sm-4">
                Batch: <div style="display: inline" id="txtbatchname"> </div>
            </div>


        </div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            
                <div class="container">
                    <div class="container">
                        <div class="col-sm-4 form-group" id="response"></div>
                    </div>        
                    <div id="errorBox"></div>
                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Learner Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" name="txtlname" id="txtlname" style="text-transform:uppercase" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || event.charCode ==32" placeholder="Learner Name">
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtAdmissionCode" id="txtAdmissionCode"/>
                        
                        <input type="hidden" class="form-control" maxlength="50" name="txtCourseFee" id="txtCourseFee"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtInstallMode" id="txtInstallMode"/>
                        <input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
                        <input type="hidden" class="form-control"  name="txtsign" id="txtsign"/>
                        <input type="hidden" class="form-control"  name="txtLearnercode" id="txtLearnercode"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtmode" id="txtmode" value="<?php echo $Mode; ?>" />
                        <input type="hidden" class="form-control" name="ITGK_Code" id="ITGK_Code" />
                        <input type="hidden" class="form-control"  name="User_Code" id="User_Code" />
                        <input type="hidden" class="form-control"  name="User_Rsp" id="User_Rsp" />
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="fname">Father/Husband Name:<span class="star">*</span></label>
                        <input type="text" class="form-control" style="text-transform:uppercase" maxlength="50" name="txtfname" id="txtfname" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || event.charCode ==32" placeholder="Father Name">     
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="dob">Date of Birth:<span class="star">*</span></label>                              
                        <input type="text" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="YYYY-MM-DD">
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="mtongue">Mother Tongue:</label>
                        <select id="ddlmotherTongue" name="ddlmotherTongue" class="form-control" >                                
                            <option value="Hindi" selected>Hindi</option>
                            <option value="English">English</option>
                        </select>    
                    </div>
                </div>  

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="gender">Gender:</label> <br/>                               
                        <label class="radio-inline"> <input type="radio" id="genderMale" checked="checked" name="gender" /> Male </label>
                        <label class="radio-inline"> <input type="radio" id="genderFemale" name="gender" /> Female </label>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="email">Marital Status:</label> <br/>  
                        <label class="radio-inline"><input type="radio" id="mstatusSingle" checked="checked" name="mstatus" /> Single </label>
                        <label class="radio-inline"><input type="radio" id="mstatusMarried" name="mstatus" /> Married   </label>                            
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="address">Medium Of Study:</label> <br/> 

                        <label class="radio-inline"> 
                            <input type="radio" id="mediumHindi" name="Medium" checked="checked" />Combined (Hindi + English)</label>

                    </div>

                    <div class="col-sm-6 form-group"> 
                        <label for="deptname">Physically Challenged:</label> <br/> 
                        <label class="radio-inline"> <input type="radio" id="physicallyChStYes" name="PH" /> Yes </label>
                        <label class="radio-inline"> <input type="radio" id="physicallyChStNo" name="PH" checked="checked"/> No </label>
                    </div>
                </div>    

                <div class="container">
                    <div class="col-sm-4 form-group"> 
                        <label for="empid">Proof Of Identity:<span class="star">*</span></label>
                        <select id="ddlidproof" name="ddlidproof" class="form-control">
                            <option value="">Select</option>
                            <option value="PAN Card">PAN Card</option>
                            <option value="Voter ID Card">Voter ID Card</option>
                            <option value="Driving License">Driving License</option>
                            <option value="Passport">Passport</option>
                            <option value="Employer ID card">Employer ID card</option>
                            <option value="Government ID Card">Governments ID Card</option>
                            <option value="College ID Card">College ID Card</option>
                            <option value="School ID Card">School ID Card</option>
                        </select>
                    </div>
                    <div class="col-sm-4 form-group"> 
                        <label for="bankname">Email Id:</label>
                        <input type="text" placeholder="Email Id" class="form-control" id="txtemail" name="txtemail" maxlength="100" />
                    </div> 

<!-- 
                    <div class="col-sm-4 form-group"> 
                       

                        <label for="empid">Do You Have Aadhar:<span class="star">*</span></label>
                        <input type="label" class="form-control" maxlength="12"  name="txtaadharno" id="txtaadharno" style="display:none" placeholder="Aadhar No">

                        <select id="ddlAadhar" name="ddlAadhar" class="form-control">
                            <option value="">Select</option>
                            <option value="y">Yes</option>
                            <option value="n">No</option>


                        </select>

                    </div> -->


                    <div class="col-sm-4 form-group"> 
                        <label for="designation">District:<span class="star">*</span></label>
                        <select id="ddlDistrict" name="ddlDistrict" class="form-control">                                  
                        </select>
                    </div>     

                    <div class="col-sm-4 form-group"> 
                        <label for="marks">Tehsil:<span class="star">*</span></label>
                        <select id="ddlTehsil" name="ddlTehsil" class="form-control">                                 
                        </select>
                    </div>
                </div>

                <div class="container">                          
                    <div class="col-sm-4 form-group"> 
                        <label for="attempt">Address:<span class="star">*</span></label>  
                        <textarea class="form-control" rows="3" style="text-transform:uppercase" id="txtAddress" name="txtAddress" placeholder="Address"onkeypress="return validAddress(event);" ></textarea>

                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="pan">PINCODE:<span class="star">*</span></label>
                        <input type="text" class="form-control" name="txtPIN" id="txtPIN"  placeholder="PINCODE" onkeypress="javascript:return allownumbers(event);" maxlength="6"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankaccount">Mobile No:<span class="star">*</span></label>
                        <input type="text" class="form-control" readonly="true" name="txtmobile"  id="txtmobile" placeholder="Mobile No" onkeypress="javascript:return allownumbers(event);" maxlength="10"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankdistrict">Phone No:</label>
                        <input type="text" placeholder="Phone No" class="form-control" id="txtResiPh" name="txtResiPh" onkeypress="javascript:return allownumbers(event);" maxlength="11"/>
                    </div>   
                </div>

                <div class="container">
  

                    <div class="col-sm-4 form-group"> 
                        <label for="branchname">Qualification:<span class="star">*</span></label>
                        <select id="ddlQualification" name="ddlQualification" class="form-control">                                  
                        </select>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="ifsc">Type of Learner:<span class="star">*</span></label>
                        <select id="ddlLearnerType" name="ddlLearnerType" class="form-control" onChange="findselected()">                                 
                        </select>
                    </div> 

                   
                        <div class="col-sm-4 form-group" style="display:none;" id="divEmpId"> 
                            <label for="micr">Employee ID:</label>
                            <input type="text" placeholder="GPF No" class="form-control" id="txtEmpId" name="txtEmpId" maxlength="20"/>
                        </div>
                   
                </div>

                <fieldset style="border: 1px groove #ddd !important;" class="">
                    <br>
                    <div class="container">
                        <div class="col-sm-1"> 

                        </div>
                        <div class="col-sm-3" style="display: none"> 
                            <label for="payreceipt">Upload Scan Application Form:<span class="star">*</span></label>
                            <input type="file" class="form-control"  name="txtUploadScanDoc" id="txtUploadScanDoc" onchange="checkScanForm(this)"/>
                        </div>                                          

                        <div class="col-sm-3" > 
                            <label for="photo">Attach Photo:<span class="star">*</span></label> </br>
                            <img id="uploadPreview1" src="images/samplephoto.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">                                 

                            <input style="margin-top:5px !important;" id="uploadImage1" type="file" name="p1" onchange="checkPhoto(this);
                                    PreviewImage(1)"/>                                    
                        </div>

                        <div class="col-sm-3"> 
                            <label for="photo">Attach Signature:<span class="star">*</span></label> </br>
                            <img id="uploadPreview2" src="images/samplesign.png" id="uploadPreview2" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage2').click();">                              
                            <input id="uploadImage2" type="file" name="p2" onchange="checkSign(this);
                                    PreviewImage(2)"/>
                        </div>  
                        <div class="col-sm-3"> 
                            <input style="margin-top:50px !important;" type="button" name="btnUpload" id="btnUpload" class="btn btn-primary" value="Upload Photo Sign"/>   
                        </div>
                        <div class="col-sm-1"> 

                        </div>

                    </div>
                    <br>
                </fieldset>
                <br><br>
                <div class="container">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                </div>
                </div>
            </div> 
                </div>
                                    </form>

            </div>
        </div>
  
</div>
</div>
<link href="css/popup.css" rel="stylesheet" type="text/css">
<!-- Modal -->



</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>

<!-- <script src="scripts/imageupload.js"></script> -->

<style>
    #errorBox
    {	color:#F00;	 } 
</style>

<script type="text/javascript">
                            $('#dob').datepicker({
                                format: "yyyy-mm-dd",
                                orientation: "bottom auto",
                                todayHighlight: true
                            });
</script>

<script type="text/javascript">
    $('#txtEmpId').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
</script>

<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>
<script language="javascript" type="text/javascript">
    function validAddress(e) {
        var keyCode = e.keyCode || e.which;
        var lblError = document.getElementById("txtAddress");
        lblError.innerHTML = "";
 
        //Regex for Valid Characters i.e. Alphabets and Numbers.
        var regex = /^([a-zA-Z0-9 _-]+)$/;
 
        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            lblError.innerHTML = "Only Alphabets and Numbers allowed.";
        }
 
        return isValid;
    }
</script>

<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
    ;
</script>	

<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#txtUploadScanDoc').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("txtUploadScanDoc").value = '';
            return false;
        }
        document.getElementById("txtUploadScanDoc").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkPhoto(target) {
        var ext = $('#uploadImage1').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage1").value = '';
            return false;
        }

        if (target.files[0].size > 5000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 5 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        } else if (target.files[0].size < 3000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        document.getElementById("uploadImage1").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkSign(target) {
        var ext = $('#uploadImage2').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage2").value = '';
            return false;
        }

        if (target.files[0].size > 3000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 3 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        } else if (target.files[0].size < 1000)
        {
            alert("Image size should be greater than 1 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        document.getElementById("uploadImage2").innerHTML = "";
        return true;
    }
</script>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {
        $("#txtmobile").val(MobNo);
        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {

            document.getElementById('txtcoursename').innerHTML = ddlCourse.options[ddlCourse.selectedIndex].text;
            var selCourse = $(this).val();
            if(selCourse == 1 || selCourse == 5 || selCourse == 20 || selCourse == 22){
                $("#divEmpId").hide();
            }
            if(selCourse == 4){
                $("#divEmpId").show();
            }
            if (selCourse == 3) {
                //alert("ok");
                $("#ddlCourse").val('');
                //$("#ddlBatch").val('');
                $('#ddlBatch').html(' ');
                return false;
            }
                else if (selCourse == 23) {
                window.location.href = "frmclickadmission.php";
            }
            else {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLEventBatch&values=" + selCourse + "",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                       
                    }
                });
            }
        });
         $("#ddlBatch").change(function () {

            document.getElementById('txtbatchname').innerHTML = $("#ddlBatch option:selected" ).text();
              AdmissionFee();
                          InstallMode();
        });
         FillDistrict();

        $("#ddlCenterDistrict").change(function () {
            $("#btnShow").hide();
        });

        $("#ddlCenterTehsil").change(function () {
            $("#btnShow").hide();
        });

        $("#ddlCenter").change(function () {
             $("#ITGK_Code").val((ddlCenter.options[ddlCenter.selectedIndex].text).substring(0, 8));
            $("#User_Code").val(ddlCenter.value);
            
            $("#btnShow").show();
            $.ajax({
                type: "post",
                url: "common/cfLearnerAdmission.php",
                data: "action=CENTERDETAILS&values=" + ddlCenter.value + "",
                success: function (data) {
                  //  alert(data);
                    data = $.parseJSON(data);
                  //  alert(data);
                    //txtAddress.value = data[0].itgk_address;
                    User_Rsp.value = data[0].rsp;
                }
            });
        });



        function FillCenterDistrict() {
            //alert("hi");
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    //alert(data);
                    $("#ddlCenterDistrict").html(data);
                }
            });
        }
        FillCenterDistrict();
        
        $("#ddlCenterDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlCenterTehsil').html(data);
                }
            });
        });
        
        $("#ddlCenterTehsil").change(function () {
            var selTehsil = $(this).val();
            var Course = ddlCourse.value;
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfLearnerAdmission.php",
                data: "action=FILLCenter&tehsil=" + selTehsil + "&course=" + Course + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlCenter").html(data);


                }
            });
        });
        $("#btnShow").click(function () {
            var a = $('#ddlCenter').val();
            var b = $('#ddlCourse').val();
            var c = $('#ddlBatch').val();
            //alert(a);
            if (a == "0" || a == "" || b == "" || c == "")
            {
                alert("Please Select all to Proceed");
            } else {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfLearnerAdmission.php",
                    data: "action=CENTERDETAILS&values=" + ddlCenter.value + "",
                    success: function (data)
                    {
                        //alert(data);
                        $('#response').empty();
                        if (data == "") {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Select Center To Proceed" + "</span></p>");
                        }
                        else {
                            data = $.parseJSON(data);
                            txtName1.value = data[0].orgname;
                            txtMobile.value = data[0].regno;
                            txtDistrict.value = data[0].district;
                            txtTehsil.value = data[0].tehsil;
                            txtEmail.value = data[0].emailid;
                            txtAddress1.value = data[0].itgk_address;
                            $("#CenterDetail").show(3000);
                        }
                    }
                });
            }
        });

        function FillCourseName() {
            // $.ajax({
            //     type: "post",
            //     url: "common/cfCourseMaster.php",
            //     data: "action=FillAdmissionCourse&values=" + CourseCode + "",
            //     success: function (data) {
                   // document.getElementById('txtcoursename').innerHTML = ddlCourse.value;
            //     }
            // });
             document.getElementById('txtcoursename').innerHTML = $("#ddlCourse option:selected").text();
        }
        FillCourseName();

        function FillBatchName() {
            // $.ajax({
            //     type: "post",
            //     url: "common/cfBatchMaster.php",
            //     data: "action=FillAdmissionBatch&values=" + BatchCode + "",
            //     success: function (data) {
                   // document.getElementById('txtbatchname').innerHTML = ddlBatch.value;
            //     }
            // });
             document.getElementById('txtbatchname').innerHTML = $("#ddlBatch option:selected" ).text();
        }
        FillBatchName();

        function GenerateUploadId()
        {
            $.ajax({
                type: "post",
                url: "common/cfBlockUnblock.php",
                data: "action=GENERATEID",
                success: function (data) {
                    txtGenerateId.value = data;
                }
            });
        }
        GenerateUploadId();


        function GenerateLearnercode()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmissionForm.php",
                data: "action=GENERATELEARNERCODE",
                success: function (data) {
                    txtLearnercode.value = data;
                }
            });
        }
        GenerateLearnercode();

        function AdmissionFee()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmissionForm.php",
                data: "action=Fee&codes=" + ddlBatch.value + "",
                success: function (data) {
                    txtCourseFee.value = data;
                }
            });
        }
       // AdmissionFee();

        function InstallMode()
        {  // alert("txtInstallMode");
            $.ajax({
                type: "post",
                url: "common/cfAdmissionForm.php",
                data: "action=Install&values=" + ddlBatch.value + "",
                success: function (data) {
                    txtInstallMode.value = data;
                }
            });
        }
        //InstallMode();

        $("#txtEmpId").prop("disabled", true);



        $("input[type='image']").click(function () {
            $("input[id='my_file']").click();
        });

        function FillDistrict() {
            //alert("ddgg");
            $.ajax({
                type: "post",
                url: "common/cfDistrictMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlDistrict").html(data);
                }
            });
        }
        

        function FillQualification() {
            $.ajax({
                type: "post",
                url: "common/cfQualificationMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlQualification").html(data);
                }
            });
        }
        FillQualification();

        function FillLearnerType() {
            $.ajax({
                type: "post",
                url: "common/cfLearnerTypeMaster.php",
                data: "action=FILL&coursecode=" + ddlCourse.value + "",
                success: function (data) {
                    $("#ddlLearnerType").html(data);
                }
            });
        }
        FillLearnerType();

        function findselected() {
            var selMenu = document.getElementById('ddlLearnerType');
            var txtField = document.getElementById('txtEmpId');
            if (selMenu.value != '34')
                txtField.disabled = false
            else
                txtField.disabled = true
        }

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });

        $("#ddlLearnerType").change(function () {
            findselected();
        });

        // statement block start for delete record function
        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='alert-info'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfAdmissionForm.php",
                data: "action=DELETE&values=" + AdmissionCode + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmLearnerProfile.php";
                        }, 1000);
                        Mode = "Add";
                        resetForm("frmLearnerProfile");
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }
        // statement block end for delete function statment

        function fillForm()
        {

            $.ajax({
                type: "post",
                url: "common/cfAdmissionForm.php",
                data: "action=EDIT&values=" + AdmissionCode + "",
                success: function (data) {

                    data = $.parseJSON(data);
                    txtlname.value = data[0].lname;
                    txtfname.value = data[0].fname;
                    dob.value = data[0].dob;
                    txtAdmissionCode.value = data[0].AdmissionCode;
                    txtphoto.value = data[0].photo;
                    txtsign.value = data[0].sign;
                    $("#uploadPreview1").attr('src', "upload/admission_photo/" + data[0].photo);
                    $("#uploadPreview2").attr('src', "upload/admission_sign/" + data[0].sign);
                    //p1.value = data[0].photo;
                    //p2.value = data[0].sign; 
                }
            });
        }


        $("#btnUpload").click(function () {
            $('#btnSubmit').show(1000);
        });
        $("#frmAdmissionForm").submit(function () {
			 if ($("#frmAdmissionForm").valid())
			 {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfAdmissionForm.php"; // the script where you handle the form input.

                if (document.getElementById('genderMale').checked) //for radio button
                {
                    var gender_type = 'Male';
                } else {
                    gender_type = 'Female';
                }
                if (document.getElementById('mstatusSingle').checked) //for radio button
                {
                    var marital_type = 'Single';
                } else {
                    marital_type = 'Married';
                }
                if (document.getElementById('physicallyChStYes').checked) //for radio button
                {
                    var physical_status = 'Yes';
                } else {
                    physical_status = 'No';
                }
            var data;
            var forminput = $("#frmAdmissionForm").serialize();
            data = forminput; // serializes the form's elements.
            var form_data = new FormData(this);
            form_data.append("action", "ADD")
            form_data.append("data", data)
            form_data.append("medium_type", "HindiPlusEnglish")
            form_data.append("gender_type", gender_type)
            form_data.append("physical_status", physical_status)
            form_data.append("marital_type", marital_type)
            form_data.append("CourseCode", ddlCourse.value)
            form_data.append("BatchCode", ddlBatch.value)
            form_data.append("ddlAadhar", "n")

           
            $.ajax({
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                url: url,
                data: form_data,
                success: function (data)
                {                        
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmAdmissionForm.php';
                            }, 3000);
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                        }


                }
            });
            }
            return false; // avoid to execute the actual submit of the form.
        });

       function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        // statement block end for submit click
    });

</script>

<!--<script src="scripts/signupload.js"></script>-->
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmadmissionform_validation.js" type="text/javascript"></script>	
</html>