<?php
$title = "Payment Failed";
include ('header.php');
include ('root_menu.php');

echo "<div style='min-height:430px !important;max-height:auto !important;'>";
echo "<div class='container'>";

if (!empty($_POST["txnid"])) {
    $status=$_POST["status"];
    $firstname=$_POST["firstname"];
    $amount=$_POST["amount"];
    $txnid=$_POST["txnid"];
    $posted_hash=$_POST["hash"];
    $key=$_POST["key"];
    $productinfo=$_POST["productinfo"];
    $email=$_POST["email"];
    $udf1=$_POST["udf1"];
    $udf2=$_POST["udf2"];
    $udf3=$_POST["udf3"];
    $udf4=$_POST["udf4"];
	$udf5 = $_POST["udf5"];
    $verify_by = 'payu';
} elseif (isset($response) && !empty($response["txnid"])) {
    //print_r($response);
    $status = $response["status"];
    $firstname = $response["firstname"];
    $amount = $response["amount"];
    $txnid = $response["txnid"];
    $posted_hash = (isset($response["hash"])) ? $response["hash"] : '';
    $key = (isset($response["key"])) ? $response["key"] : '';
    $productinfo = $response["productinfo"];
    $email = (isset($response["email"])) ? $response["email"] : '';
    $udf1 = $response["udf1"];
    $udf2 = $response["udf2"];
    $udf3 = $response["udf3"];
    $udf4 = $response["udf4"];
	$udf5 = $_POST["udf5"];
    $verify_by = $response['verify_by'];
}

require_once 'frmPayuTransactionResponse.php';

$salt = (isset($response["salt"])) ? $response["salt"] : '';
	
$_ObjConnection=new _Connection();
$_Response=array();

If (isset($_POST["additionalCharges"])) {
       $additionalCharges=$_POST["additionalCharges"];
        $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'||||||' . $udf5 . '|'.$udf4.'|'.$udf3.'|'.$udf2.'|'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        
                  }
	else {	  

        $retHashSeq = $salt.'|'.$status.'||||||' . $udf5 . '|'.$udf4.'|'.$udf3.'|'.$udf2.'|'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

         }
		 $hash = hash("sha512", $retHashSeq);
  
       if ($hash != $posted_hash && $verify_by != 'VerifiedByRazorPay') {
	       echo "<div class='alert alert-danger' role='alert'><b>Oh snap!</b> Payment Transaction Unsuccessful .</div>";
		   }
	   else {

    echo "<br>";
    echo "<br>";
    echo "<div class='row'>";
    echo "<div class='col-md-8 col-md-offset-2'>";
    echo "<div class='panel panel-danger'>";
    echo " <div class='panel-heading'>";
    echo "    <h3 class='panel-title'>Payment Status</h3>";
    echo "  </div>";
    echo "  <div class='panel-body'>";
    echo " <table id='example' class='table table-hover table-striped'>";
    echo "  <tr class=''>";
    echo "    <td class='info' colspan='2'>";
    echo "<b>Oh snap</b> Your Payment is Failed";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Your Transaction ID is <br> You may try making the payment by clicking the link below.";
    echo "<p><a href='javascript:void(0);' id='try-again'> Try Again</a></p>";
    echo "</td>";
    echo "    <td class=''>";
    echo "$txnid";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Payment For";
    echo "</td>";
    echo "    <td class=''>";
    echo "$productinfo";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "ITGK Code";
    echo "</td>";
    echo "    <td class=''>";
    echo "$udf1";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "User Name";
    echo "</td>";
    echo "    <td class=''>";
    echo "$firstname";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Email";
    echo "</td>";
    echo "    <td class=''>";
    echo "$email";
    echo "</td>";
    echo "  </tr>";
    echo "  <tr class=''>";
    echo "    <td class=''>";
    echo "Date";
    echo "</td>";
    echo "    <td class=''>";
    $date1 = date("d-m-Y");
    echo "$date1";
    echo "</td>";
    echo "  </tr>";
	echo "  <tr>";
    echo "<td colspan='2' align='center'>";
    echo "<input class='hide-from-printer' type='button' value='Print' onclick='window.print()'>";
    echo "</td>";
    echo "  </tr>";
    echo "</table>";

    echo "  </div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";

    }          
?>
<!--Please enter your website homepagge URL -->

</div>

<div style="display:none;">
    
    <?php 
        print_r($response);
    ?>

</div>

</div>
<style>
    @media print {
        /* style sheet for print goes here */
        .hide-from-printer{  display:none; }
    }
</style>
<?php include ('footer.php'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#try-again").on("click", function () {
            <?php 
            switch($productinfo) {
                case 'RedHatFeePayment':
				case 'Red Hat Fee Payment':
                    $goto = 'frmadvancecourse.php';
                    break;                
            }
            echo "window.location.href = '" . $goto . "';";
            ?>
        });
    });
</script>
</body>

</html>

