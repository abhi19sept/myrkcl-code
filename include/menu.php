<?php
session_start();
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 180)) {

    session_unset();
    session_destroy();
    header("Location:./index.php");
}
$_SESSION['LAST_ACTIVITY'] = time();
require 'common/message.php';
if ($_SESSION['Login'] == 1) {
    ?>
    <div class="nav">
        <ul class="dropdown" id="menu">
            <?php echo $_SESSION['Menu']; ?>
        </ul>

        <div class="clr">
        </div>
    </div>
<?php } else { ?>     
    <div class="nav">
        <ul class="dropdown" id="menu">
            <?php echo Message::DefaultMenu; ?>

        </ul>
        <div class="clr">
        </div>
    </div>
<?php } ?>