<?php

/* Auther: HItesh Singhal 20 Jan 2020

  /*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
date_default_timezone_set('Asia/Kolkata');

/**
 * Mode: developer, Production
 * Developer mode use to when developer working on system and
 * production Mode : System Live
 * maintenance :  Website process of preserving a condition or situation or the state of being preserved. 
 */
define("Mode", "developer");



/**
 * BASE_PATH : Website Domain name 
 * Using SSL : https://domianname.com
 * Without SSL : http://domianname.com
 */
define("BASE_PATH", "http://localhost/myrkclnew/");

/*
 * PLUGIN_PATH :  PLUGIN DIR WEB PATH
 */
define("PLUGIN_PATH", BASE_PATH . "plugins");

/*
 * ASSET_PATH :  ASSETS PATH WEB
 */
define("ASSET_PATH", BASE_PATH . "assets");

/*
 * UPLOAD_PATH :  upload FOLDER WEB PATH
 */
define("UPLOAD_PATH", BASE_PATH . "/upload");



/**
 * DIR_PATH :  SERVER ROOT DIR PATH
 */
define("DIR_PATH", $_SERVER["DOCUMENT_ROOT"].'/myrkclnew/');







if (Mode == "developer") {
    // Report runtime errors
    error_reporting(E_ERROR | E_WARNING | E_PARSE);

// Report all errors
    error_reporting(E_ALL);

// Same as error_reporting(E_ALL);
    ini_set("error_reporting", E_ALL);

// Report all errors except E_NOTICE
    error_reporting(E_ALL & ~E_NOTICE);
} elseif (Mode == "production") {
    
    // trun off error runtime
    error_reporting(0);
    ini_set('display_errors', 0);
    
}elseif(Mode == "maintenance"){
    // Show message when webiste technical issue
    echo "Website Under construction";
    exit();
}









