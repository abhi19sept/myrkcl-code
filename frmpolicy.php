<?php
$title="Terms and Conditions";
include ('header.php'); 
include ('root_menu.php'); 

    if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
if ($_SESSION['User_UserRoll'] == '1') {	
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div class="container"> 

    <div class="panel panel-primary" style="margin-top:36px !important;">  
        <div class="panel-heading">Terms and Conditions</div>
        <div class="panel-body">

            <form name="frmpolicy" id="frmpolicy" class="form-inline" >
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>
                    <div class="container">
                        <div class="col-sm-10 form-group"> 
                            <label for="Mesage">Enter Text:<span class="star">*</span></label>
                            <textarea class="" maxlength="5000" rows="4" cols="100" name="txtmessage" id="txtmessage" placeholder="Enter Message" ></textarea>
                        </div> 
                    </div>
                    <br>
                    <div class="container">

                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="margin-left:15px; "/>    

                    </div>
                    <div class="container">
                        <div id="gird" style="margin-top:25px; width:94%;"> </div>
                    </div>

                </div>   
            </form>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>


<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        if (Mode == 'Delete')
        {
                deleteRecord();
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }
		
		
		function FillEntity() 
		{
                $.ajax({
                    type: "post",
                    url: "common/cfUserRoleMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlRole").html(data);
                    }
                });
        }

        FillEntity();

	  
        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfpolicy.php",
                data: "action=DELETE&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmpolicy.php";
                        }, 1000);
                        Mode = "Add";
                        resetForm("frmpolicy");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


       

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfpolicy.php",
                data: "action=SHOW",
                success: function (data) {
                    //alert(data);
                    $("#gird").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });
        }

        showData();


     $("#btnSubmit").click(function () {
				 
				// debugger;
			 if ($("#frmpolicy").valid())
                {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfpolicy.php"; // the script where you handle the form input.
               var data;
                var forminput=$("#frmpolicy").serialize();
                if (Mode == 'Add')
                {
                    data = "action=ADD&" + forminput; // serializes the form's elements.
                }
                else
                {
					
					data = "action=UPDATE&code=" + code + "&" + forminput;
                    //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data1)					
                    {
						//debugger;
					//alert(SuccessfullyInsert + '=' + data);
                        if (data1 == SuccessfullyInsert)
                        {
							
                            $('#response').empty();
                            $('#response').append("<p class='success'><span><img src=images/correct.gif width=10px /></span><span>" + data1 + "</span></p>");
                             window.setTimeout(function () {
                                window.location.href = "frmpolicy.php";
                            }, 3000);
                            Mode = "Add";
                            //resetForm("frmsurvey");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img  width=10px /></span><span>" + data1 + "</span></p>");
							 window.setTimeout(function () {
                                window.location.href = "frmpolicy.php";
                            }, 3000);
                            Mode = "Add";
                            resetForm("frmpolicy");
                        }
                        //showData();


                    }
                });
		   }
                return false; // avoid to execute the actual submit of the form.
            });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });
</script>

	<style>
.error {
	color: #D95C5C!important;
}
</style>


<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmpolicy_validation.js"></script>
</html>
 <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>