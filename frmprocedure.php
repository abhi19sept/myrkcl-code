<?php
$title="Execute Procedure";
include ('header.php'); 
include ('root_menu.php'); 

    if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:300px !important;min-height:500px !important">
        <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:46px !important;">

                <div class="panel-heading">Execute Procedure</div>
                <div class="panel-body">
				
				
                    <!-- <div class="jumbotron"> -->
                    <form name="frmprocedure" id="frmprocedure" action="" class="form-inline">     

                        <div class="container">
						 
                          <div class="clearfix" style="height: 10px;clear: both;"></div>
						  <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                           
							
							
							
								<div class="col-sm-4 form-group" id='foo' > 
                                <label for="edistrict">Event Name 
								:</label>
                                <select id="ddlEvent" name="ddlEvent" class="form-control" multiple="multiple" >
								 
                                </select>    
                            </div>
							
							

							
							
					</div>
							
					

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
						
						
					
                 </div>
            </div>   
        </div>


    </form>


</div>


</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>

<script type="text/javascript">
<!--
    function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'none')
          e.style.display = 'block';
      
    }
//-->
</script>
<style>
#errorBox{
 color:#F00;
 }
</style>

 <script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
							if (Mode == 'Delete')
							{
								if (confirm("Do You Want To Delete This Item ?"))
								{
		                   		 deleteRecord();
		               		}
		           		 }
						   else if (Mode == 'Edit')
						   {
								fillForm();
						   }
				   
						   function FillBatch() {
						$.ajax({
							type: "post",
							url: "common/cfexamdatamaster.php",
							data: "action=FILLEXAM",
							success: function (data) {
								$("#ddlEvent").html(data);
							}
						});
					}

					FillBatch();
			
							
							$("#btnSubmit").click(function () {
								
								//$("#frmexamdatamaster").valid();
								
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                               var url = "common/cfprocedure.php"; // the script where you handle the form input.
								var data;
								var forminput=$("#frmprocedure").serialize();
								if (Mode == 'Add')
								{
									data = "action=ADD&" + forminput; // serializes the form's elements.
								}
                                else
                                {
                                  data = "action=UPDATE&code=" + Code + 
								  "&ddlDistrict=" + ddlDistrict.value + 
								   "&ddlCenter=" + ddlCenter.value + 
								   "&txtName=" + txtName.value + 
								   "&txtdate=" + txtdate.value + 
								   "&txtAddress=" + txtAddress.value + 
								   "&txtMobile=" + txtMobile.value + 
								   "&txtEmail=" + txtEmail.value + 
								   "&txtCapacity=" + txtCapacity.value + 
								   "&txtRooms=" + txtRooms.value + 
								   "&txtCoordinator=" + txtCoordinator.value + 
								    "&ddlEvent=" + ddlEvent.value + 
								  "&txtexamid=" + txtexamid.value + ""; 
										
                                }

                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                            window.setTimeout(function () {
                                                window.location.href = "frmexamdatamaster.php";
                                            }, 1000);

                                            Mode = "Add";
                                            resetForm("frmexamdatamaster");
                                        }
                                        else
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                        }
                                        showData();


                                    }
                                });

                                return false; // avoid to execute the actual submit of the form.
                            });
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

                        });

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmorgmastervalidation.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>