<?php
$title="Lot Master";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var BatchCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var LotCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {	
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>

 <div style="min-height:430px !important;max-height:1500px !important;">
		<div class="container">
     		<div class="panel panel-primary" style="margin-top:36px !important;">
                <div class="panel-heading">Lot Master </div>
                <div class="panel-body">					
					 <form name="frmlotmaster" id="frmlotmaster" class="form-inline" role="form" enctype="multipart/form-data">
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
							
							<div class="col-sm-4 form-group">     
                                <label for="batchname">Lot Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtLotName" id="txtLotName" placeholder="Lot Name">	
                            </div>						

                            <div class="col-sm-4 form-group"> 
                                <label for="sdate">Date:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="YYYY-MM-DD">     
                            </div>
							
                            <div class="col-sm-4 form-group"> 
                                <label for="mtongue">Lot Status:<span class="star">*</span></label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control" >									
                                </select>    
                            </div>
							
							<div class="col-sm-4 form-group"> 
                                <label for="FYear">Year:<span class="star">*</span></label>
                                 <select id="ddlYear" name="ddlYear" class="form-control">
									<option value=""> Please Select </option>
									
									<option value="2014"> 2014</option>
									<option value="2015"> 2015</option>
									<option value="2016"> 2016</option>
									<option value="2017"> 2017</option>
									<option value="2018"> 2018</option>
								</select>
                            </div>
                        </div>  
                	
						<div class="container"> 
                            
                            <div class="col-sm-4 form-group"> 
                                <label for="install">Lot Role:<span class="star">*</span></label>
                                <select id="ddlrole" name="ddlrole" class="form-control">
									<option value=""> Please Select Role </option>
									<option value="1"> Govt. Reimbursement</option>
									<option value="2"> Duplicate/Correction</option>									
								</select>
                            </div>                           
                        </div>
						
						<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Next"/>    
                        </div>
						
						<div id="gird" style="margin-top:35px;"> </div>
                </div>
            </div>   
        </div>
    </form>
</div>
</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
                        
 <script type="text/javascript"> 
 $('#txtstartdate').datepicker({                   
	format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
});  
</script>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>
                 
<script type="text/javascript">
		var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
		var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
		var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
		var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
		
	$("#txtstartdate, #txtenddate").datepicker();
	$("#txtenddate").change(function () {
    var txtstartdate = document.getElementById("txtstartdate").value;
    var txtenddate = document.getElementById("txtenddate").value; 
    if ((Date.parse(txtenddate) <= Date.parse(txtstartdate))) {
        alert("End date should be greater than Start date");
        document.getElementById("txtenddate").value = "";
    }
});
		
 $(document).ready(function(){
	 	function FillStatus() {
			$.ajax({
				type: "post",
				url: "common/cfStatusMaster.php",
				data: "action=FILL",
				success: function (data) {
					$("#ddlStatus").html(data);
				}
			});
		}
			FillStatus();
						
		
        $("#btnSubmit").click(function() {			
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");				
			    				  
			   //var startdate = $('#txtstartdate').val();			   
			   var url = "common/cfLotMaster.php"; // the script where you handle the form input.
			   var data;
			   var forminput = $("#frmlotmaster").serialize();
				if (Mode == 'Add')
				{
					data = "action=ADD&" + forminput; // serializes the form's elements.				 
			}
			
			$.ajax({
				type: "POST",
				url: url,
				data: data,
				success: function (data)
				{
					if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
					{
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
						window.setTimeout(function () {
							window.location.href = "frmlotmaster.php";
						}, 2000);
						Mode = "Add";
						resetForm("frmlotmaster");
					}
					else
					{
						$('#response').empty();
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
					}
					//showData();
				}
			});
		
        return false; // avoid to execute the actual submit of the form.
    });
	function resetForm(formid) {
 		$(':input','#'+formid) .not(':button, :submit, :reset, :hidden') .val('') .removeAttr('checked') .removeAttr('selected');
 		}
});
   </script>
</html>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmbatchmaster_validation.js" type="text/javascript"></script>	
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>