<?php
$title = "Advance Course Admission Payment";
include ('header.php');
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
require("razorpay/checkout/manual.php");
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Advance Course Fee Payment</div>
            <div class="panel-body">
                <form name="frmfeepayment" id="frmfeepayment" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                       <div class="col-sm-5 form-group boxsize"> 
                            <label for="course">Select Advance Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control" required="required" oninvalid="setCustomValidity('Please Select Course')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }">  </select>
                        </div> 
						
						<div class="col-md-5 form-group boxsize">     
                            <label for="batch">Select Course Category:<span class="star">*</span></label>
                            <select id="ddlcategory" name="ddlcategory" class="form-control" required="required"
								oninvalid="setCustomValidity('Please Select Advance Course Category')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"> </select>
                        </div>
						
						<div class="col-md-5 form-group boxsize">     
                            <label for="batch"> Select Course Name:<span class="star">*</span></label>
                            <select id="ddlPkg" name="ddlPkg" class="form-control" required="required"
								oninvalid="setCustomValidity('Please Select Advance Course Name')"
                                    onchange="try {
                                                            setCustomValidity('')
                                                        } catch (e) {
                                                        }"> </select>
                        </div>	
						
                    </div>
					
					
                    <div class="container">
                        <div class="col-sm-4 form-group boxsize">     
                            <label for="batch"> Select Batch:</label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>									
                        </div> 
                    
                        <div class="col-sm-4 form-group boxsize">     
                            <label for="batch"> Select Payment Mode:</label>
                            <select id="paymode" name="paymode" class="form-control">

                            </select>
                        </div> 
                    
                        <div class="col-sm-4 form-group boxsize">     
                            <label for="batch"> Select Payment Gateway:</label>
                            <select id="gateway" name="gateway" class="form-control">
                                <option value="">Select</option>
                                <option value="razorpay">Razorpay</option>
								<option value="payu">Payu</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <input type="hidden" name="amounts" id="amounts"/>
                    </div>
                    <div id="menuList" name="menuList" style="margin-top:5px;"> </div> 

                    <div class="container">
                        <br><input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                    </div>
            </div>
        </div>   
    </div>
</form>
</div>
<form id="frmpostvalue" name="frmpostvalue" action="frmonlineadcoursepayment.php" method="post">
    <input type="hidden" id="txnid" name="txnid">
</form>

</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<style>
.boxsize { max-width: 31% !important; }
</style>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
         function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfAdvanceFeePayment.php",
                data: "action=FILLAdvanceCourse",
                success: function (data) {
					
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();
		
		$("#ddlCourse").change(function () {
            var selCourse = $(this).val(); 				
				FillCategory(selCourse);                    
        });
		
		 function FillBatch(val) {
            
        }
		
		function FillCategory(val) {           			
            $.ajax({
                type: "post",
                url: "common/cfAdvanceFeePayment.php",
                data: "action=FILLCourseCategory&values=" + val + "",
                success: function (data) {                   
				   $("#ddlcategory").html(data);
                }
            });
        }		
		
		$("#ddlcategory").change(function () {
            var selcategory = $(this).val();			
            $.ajax({
                type: "post",
                url: "common/cfAdvanceFeePayment.php",
                data: "action=FILLAdvanceCourseName&values=" + selcategory + "",
                success: function (data) {
                    $("#ddlPkg").html(data);
                }
            });
        });
		
		
        $("#ddlPkg").change(function () {
            $("#menuList").html('');
            $('#btnSubmit').hide();
            $("#ddlBatch").html('');
            $("#paymode").html('');
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + ddlCourse.value + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlBatch").html(data);
                }
            });

        });
		
        $("#ddlBatch").change(function () {
            var BatchCode = $('#ddlBatch').val();
            //alert(BatchCode);
            $.ajax({
                type: "post",
                url: "common/cfAdvanceFeePayment.php",
                data: "action=Fee&codes=" + BatchCode + "",
                success: function (data) {
                    //alert(data);
                    amounts.value = data;
                }
            });
        });


        $("#ddlBatch").change(function () {
            showpaymentmode(ddlBatch.value, ddlCourse.value);
        });

        function showpaymentmode(val, val1) {
            $("#menuList").html('');
            $('#btnSubmit').hide();
            $("#paymode").html('');
            $.ajax({
                type: "post",
                url: "common/cfEvents.php",
                data: "action=SHOWAdmissionPay&batch=" + val + "&course=" + val1 + "",
                success: function (data) {
                    //alert(data);
                    $("#paymode").html(data);
                }
            });
        }
        function showAllData(batch, course, paymode, coursepkg) {
            $("#menuList").html('');
            $('#btnSubmit').hide();
            $.ajax({
                type: "post",
                url: "common/cfAdvanceFeePayment.php",
                data: "action=SHOWALL&batch=" + batch + "&course=" + course + "&paymode=" + paymode + "&coursepkg=" + coursepkg + "",
                success: function (data) {
                    // alert(data);
                    $("#menuList").html(data);
                    $('#example').DataTable({
                        scrollY: 400,
                        scrollCollapse: true,
                        paging: false
                    });
                    $('#btnSubmit').show();
                }
            });
        }

        $("#paymode").change(function () {
            //showAllData(ddlBatch.value, ddlCourse.value, paymode.value, ddlPkg.value);
        });

        $("#gateway").change(function () {
            if (this.value == "") {
                showAllData(ddlBatch.value, ddlCourse.value, this.value, ddlPkg.value);
            } else {
                showAllData(ddlBatch.value, ddlCourse.value, paymode.value, ddlPkg.value);
            }
        });


        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $('#btnSubmit').hide();
            var url = "common/cfAdvanceFeePayment.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmfeepayment").serialize();
            //alert(forminput);

            if (Mode == 'Add') {
                data = "action=ADD&" + forminput;
                $('#txnid').val('');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {						
                        $('#response').empty();
                        data = data.trim();
                        if (data == 0 || data == '') {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Please try again, also ensure that, you have selected atleast one checkbox." + "</span></p>");
                        } else if (data == 'TimeCapErr') {
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                        } else {
                            if ($('#gateway').val() == 'razorpay') {
                                var options = $.parseJSON(data);
                                <?php include("razorpay/razorpay.js"); ?>
                            } else {
                                $('#txnid').val(data);
                                $('#frmpostvalue').submit();
                            }
                        }
                    }
                });
            }

            return false; // avoid to execute the actual submit of the form.
        });

    });

</script>
</body>

</html>