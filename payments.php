<?php
	class paymentFunctions 
	{
		private function dbConnection() {
			global $_ObjConnection;
        	$_ObjConnection->Connect();

        	return $_ObjConnection;
		}

        public function generateTransactionId() {
            $txtGeneratePayUId = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            $txtGenerateId = (mt_rand(1000, 9999)) . date("ymdHis");
            $_RKCL_Id = $txtGenerateId . '_RKCLtranid';

            return [$txtGeneratePayUId, $_RKCL_Id];
        }
	 	/**
	     * Add Payment Transaction Record
	     */
	 	public function insertPaymentTransaction($productInfo, $_ITGK_Code, $unitAmount, $codes, $course, $event, $batch = 0) {
	 		$_ObjConnection = $this->dbConnection();

            if (! $unitAmount || $unitAmount < 0 || empty($productInfo) || empty($_ITGK_Code)) return '';

            $transactionIds = $this->generateTransactionId();
            $txtGeneratePayUId = $transactionIds[0];
            $_RKCL_Id = $transactionIds[1];

            $Lcount = count($codes);
            $amount = ($unitAmount * $Lcount);
            $return = '';

            $payStatus = 'PaymentInitiated';

            $_InsertQuery = "INSERT IGNORE INTO tbl_payment_transaction (Pay_Tran_ITGK, Pay_Tran_Fname, Pay_Tran_RKCL_Trnid, Pay_Tran_AdmissionArray, Pay_Tran_LCount, Pay_Tran_Amount, Pay_Tran_Status, Pay_Tran_ProdInfo, Pay_Tran_Course, Pay_Tran_ReexamEvent, Pay_Tran_Batch, Pay_Tran_PG_Trnid) VALUES ('" . $_ITGK_Code . "', (SELECT up.UserProfile_FirstName FROM tbl_user_master um INNER JOIN tbl_userprofile up ON up.UserProfile_User = um.User_Code WHERE um.User_LoginId = '" . $_ITGK_Code . "'),'" . $_RKCL_Id . "', '" . implode(',', $codes) . "', '" . $Lcount . "', '" . $amount . "', '" . $payStatus . "', '" . $productInfo . "', '" . $course . "', '" . $event . "', '" . $batch . "', '" . $txtGeneratePayUId . "')";
            $_Response = $_ObjConnection->ExecuteQuery($_InsertQuery, Message::InsertStatement);
            if ($codes && $_Response[0] == Message::SuccessfullyInsert) {
                $_SelectQuery = "SELECT Pay_Tran_PG_Trnid FROM tbl_payment_transaction WHERE Pay_Tran_PG_Trnid = '" . $txtGeneratePayUId . "' AND Pay_Tran_ProdInfo = '" . $productInfo . "' AND Pay_Tran_Status = '" . $payStatus . "'";
                $_Response1 = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
                if ($_Response1[0] != Message::NoRecordFound) {
                    $getStoredTrnxId = mysqli_fetch_array($_Response1[2]);
                    switch ($productInfo) {
                        case 'ReexamPayment':
                            $return = $this->updateExamData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes);
                            break;
                        case 'LearnerFeePayment':
                            $return = $this->updateAdmissionData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes, $course, $batch);
                            break;
                        case 'Correction Certificate':
                        case 'Duplicate Certificate':
                            $return = $this->updateCorrectionData($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes);
                            break;
                        case 'Ncr Fee Payment':
                            $return = $this->updateOrgMaster($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes[0]);
                            break;
                        case 'Name Change Fee Payment':
                        case 'Address Change Fee Payment':
                            $return = $this->updateAddressNameTrnx($getStoredTrnxId['Pay_Tran_PG_Trnid'], $_RKCL_Id, $codes[0], $productInfo);
                            break;
                        case 'EOI Fee Payment':
                            $return = $this->updateCourseitgkMapping($getStoredTrnxId['Pay_Tran_PG_Trnid'], $codes[0], $_ITGK_Code);
                            break;
                    }
                }
            }

            return $return;
        }

        private function updateAdmissionData($transactionId, $rkclId, $codes, $course, $batch) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_admission SET Admission_RKCL_Trnid = '" . $rkclId . "', Admission_TranRefNo = '" . $transactionId . "' WHERE Admission_Code IN (" . implode(',', $codes) . ") AND Admission_Course = '" . $course . "' AND Admission_Batch = '" . $batch . "'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }

        private function updateExamData($transactionId, $rkclId, $codes) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE examdata SET reexam_TranRefNo = '" . $transactionId . "', reexam_RKCL_Trnid = '" . $rkclId . "' WHERE examdata_code IN (" . implode(',', $codes) . ")";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }

        private function updateCorrectionData($transactionId, $rkclId, $codes) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_correction_copy SET Correction_RKCL_Trnid = '" . $rkclId . "', Correction_TranRefNo = '" . $transactionId . "' WHERE cid IN (" . implode(',', $codes) . ")";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }

        private function updateOrgMaster($transactionId, $rkclId, $ackCode) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_org_master SET Org_RKCL_Trnid = '" . $rkclId . "', Org_TranRefNo = '" . $transactionId . "' WHERE Org_Ack = '" . $ackCode . "'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }

        private function updateAddressNameTrnx($transactionId, $rkclId, $refCode, $productInfo) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $payFor = ($productInfo == 'Name Change Fee Payment') ? 'name' : 'address';
            $_UpdateQuery = "UPDATE tbl_address_name_transaction SET fld_RKCL_Trnid = '" . $rkclId . "',
                fld_paymentTitle = '" . $payFor . "', 
                fld_transactionID = '" . $transactionId . "' 
                WHERE fld_ref_no = '" . $refCode . "' AND fld_ITGK_Code = '" . $_SESSION['User_LoginId'] . "'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }

        private function updateCourseitgkMapping($transactionId, $eoiCode, $ITGK_Code) {
            $_ObjConnection = $this->dbConnection();
            $return = '';
            $_UpdateQuery = "UPDATE tbl_courseitgk_mapping SET Courseitgk_TranRefNo = '" . $transactionId . "' WHERE Courseitgk_ITGK = '" . $ITGK_Code . "' AND Courseitgk_EOI = '" . $eoiCode . "'";
            $_Response2 = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);
            if ($_Response2[0] == Message::SuccessfullyUpdate) {
                $return = $transactionId;
            }

            return $return;
        }

        public function GetPaymentDataByTrnxid($trnxId, $status = 'PaymentInitiated') {
            $_ObjConnection = $this->dbConnection();
            try {
                if (isset($_SESSION['User_UserRoll']) && $_SESSION['User_UserRoll'] == 19) {
                    $_SelectQuery = "SELECT pt.Pay_Tran_PG_Trnid, pt.Pay_Tran_ProdInfo, pt.Pay_Tran_Amount, pt.Pay_Tran_ITGK, pt.Pay_Tran_RKCL_Trnid, pt.Pay_Tran_Course, pt.Pay_Tran_Batch, pt.Pay_Tran_Status, pt.timestamp, ad.Admission_Name AS UserProfile_FirstName, ad.Admission_Mobile AS UserProfile_Mobile, ad.Admission_Email AS UserProfile_Email FROM tbl_payment_transaction pt 
                        LEFT OUTER JOIN examdata ed ON (pt.Pay_Tran_PG_Trnid = ed.reexam_TranRefNo AND pt.Pay_Tran_ProdInfo = 'ReexamPayment')
                        LEFT OUTER JOIN tbl_correction_copy tp ON (pt.Pay_Tran_PG_Trnid = tp.Correction_TranRefNo AND pt.Pay_Tran_ProdInfo LIKE '%Certificate%')
                        INNER JOIN tbl_admission ad ON (
                            ed.learnercode = ad.Admission_LearnerCode OR 
                            tp.lcode = ad.Admission_LearnerCode
                        ) 
                        WHERE pt.Pay_Tran_PG_Trnid = '" . $trnxId . "' AND pt.Pay_Tran_Status = '" . $status . "' ORDER BY pt.timestamp LIMIT 1";
                } else {
                    $_SelectQuery = "SELECT pt.Pay_Tran_PG_Trnid, pt.Pay_Tran_ProdInfo, pt.Pay_Tran_Amount, pt.Pay_Tran_ITGK, pt.Pay_Tran_RKCL_Trnid, pt.Pay_Tran_Course, pt.Pay_Tran_Batch, um.User_EmailId AS UserProfile_Email, IF (up.UserProfile_FirstName IS NULL, om.Organization_Name, up.UserProfile_FirstName) AS UserProfile_FirstName, IF (up.UserProfile_Mobile IS NULL, om.Org_Mobile, up.UserProfile_Mobile) AS UserProfile_Mobile FROM tbl_payment_transaction pt INNER JOIN tbl_user_master um ON pt.Pay_Tran_ITGK = um.User_LoginId 
                        LEFT OUTER JOIN tbl_userprofile up ON (up.UserProfile_User = um.User_Code AND um.User_Type LIKE ('old') AND um.User_Ack = 0)
                        LEFT OUTER JOIN tbl_org_master om ON (om.Org_Ack = um.User_Ack AND um.User_Type LIKE ('new'))
                        WHERE pt.Pay_Tran_PG_Trnid = '" . trim($trnxId) . "' AND pt.Pay_Tran_Status = '" . $status . "'";
                }
                $_Response = $_ObjConnection->ExecuteQuery($_SelectQuery, Message::SelectStatement);
            } catch (Exception $_ex) {
                $_Response[0] = $_ex->getLine() . $_ex->getTrace();
                $_Response[1] = Message::Error;
            }

            return $_Response;
        }
	}

	$payment = new paymentFunctions();
	
?>