<?php
$title = "ITGK Details";
include ('outer_header1.php');
//include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<div style="min-height:450px !important;max-height:1500px !important">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading"> ITGK Details</div>
            <div class="panel-body">
				<form name="frmcenter_learner" id="frmcenter_learner" class="form-inline" role="form" enctype="multipart/form-data">     
					<div class="container">
						<div class="container">
							<div id="response"></div>
						</div>        
						<div id="errorBox"></div>
					</div>
					<div id="main-content">
						<div class="container">

							<div id="errorBox"></div>
							<div class="col-sm-4 form-group" id="ddlDistrict_box"> 
								<label for="ddlCourse">Select Course:<span class="star">*</span></label>
								<select id="ddlCourse" name="ddlCourse" class="form-control" >

								</select>
							</div>
							
							<div class="col-sm-4 form-group"> 
								<label for="ddlState">State:<span class="star">*</span></label>
								<select id="ddlState" name="ddlState" class="form-control" >

								</select>
							</div>
							<div class="col-sm-4 form-group" id="ddlDistrict_box"> 
								<label for="ddlDistrict">District:<span class="star">*</span></label>
								<select id="ddlDistrict" name="ddlDistrict" class="form-control" >

								</select>
							</div>

							<div class="col-sm-4 form-group" id="ddlTehsil_box" style="display2:none;"> 
								<label for="ddlTehsil">Tehsil:</label>
								<select id="ddlTehsil" name="ddlTehsil" class="form-control" >

								</select>
							</div>
						</div>    

						<div class="container">
							<div class="col-sm-4 form-group">     
								<label for="area">Area Type:<span class="star">*</span></label> <br/>                               
								<label class="radio-inline"> <input type="radio" id="areaUrban" name="area" value="Urban" /> Urban </label>
								<label class="radio-inline"> <input type="radio" id="areaRural" name="area" value="Rural" /> Rural </label>
								<div id="areaErrorMessageBox"></div>
							</div>

							<div class="container" id="Urban" style="display:none">
								<!--<div class="col-sm-4 form-group"> 
									<label for="edistrict">Municipality Type:</label>
									<select id="ddlMunicipalType" name="ddlMunicipalType" class="form-control" >

									</select>                                         
								</div>-->
								
								 <div class="col-sm-4 form-group"> 
									<label for="edistrict">Municipality Name:</label>
									<select id="ddlMunicipalName" name="ddlMunicipalName" class="form-control" >

									</select>                                            
								</div>

								<div class="col-sm-4 form-group"> 
									<label for="edistrict">Ward No.:</label>
									<select id="ddlWardno" name="ddlWardno" class="form-control" >

									</select>                                       
								</div>
							</div>
							
							<div class="container" id="Rural" style="display:none">
								<div class="col-sm-4 form-group"> 
									<label for="edistrict">Panchayat Samiti/Block:</label>
									<select id="ddlPanchayat" name="ddlPanchayat" class="form-control" >

									</select>
								</div>

								<div class="col-sm-4 form-group"> 
									<label for="edistrict">Gram Panchayat:</label>
									<select id="ddlGramPanchayat" name="ddlGramPanchayat" class="form-control" >

									</select>
								</div>

								<div class="col-sm-4 form-group"> 
									<label for="edistrict">Village:</label>
									<select id="ddlVillage" name="ddlVillage" class="form-control" >

									</select>
								</div>
							</div>
						</div> 
						<div class="container">
							<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
						</div>
					</div>
				</form>
				<div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>


                    </div>
                   

                    <div id="grid" name="grid" style="margin-top:35px;"> </div>
				
            </div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        function FillEvent() {
            //alert("hello");
			$('#response').empty();
			$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfsearchitgk.php",
                data: "action=schedule",
                success: function (data)
				{
					$('#response').empty();
					$("#grid").html(data);
					$('#example').DataTable({
						dom: 'Bfrtip',
						buttons: [
							
						],
						paging: true

					});
                }
            });
        }
        //FillEvent();
		
		function FillCourse() {
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FILLCourse",
                success: function (data) {
                    //alert(data);
                    $('#ddlCourse').html(data);
                }
            });
        }
		FillCourse();
		
		$("#frmcenter_learner").validate({
			errorPlacement: function(error, element) {
				if (element.attr("name") == "area") {
					error.insertAfter("#areaErrorMessageBox");
				   // error.html($("#areaErrorMessageBox"));
				} else {
					error.insertAfter(element);
				}
			},
			rules: {
				ddlCourse: { required: true },
				ddlContact: { required: true },
				ddlDistrict: { required: true },
				ddlTehsil: { required: true },
				area: { required: true },
				ddlMunicipalName: { required: {
					depends: function(element) {
						 if ($("#frmcenter_learner input[name='area']:checked").val() == "Urban"){
						   return true;}
						 else{
						   return false;}
					   }
					} 
				},
				ddlPanchayat: { required: {
					depends: function(element) {
						 if ($("#frmcenter_learner input[name='area']:checked").val() == "Rural"){
						   return true;}
						 else{
						   return false;}
					   }
					} 
				}
							
			},			
			messages: {	
				ddlContact: { required: '<span style="color:red; font-size: 12px;">Please fill your contact no.</span>' },
				ddlCourse: { required: '<span style="color:red; font-size: 12px;">Please Select Course</span>' },
				ddlDistrict: { required: '<span style="color:red; font-size: 12px;">Select your District</span>' },
				ddlTehsil: { required: '<span style="color:red; font-size: 12px;">Select your Tehsil</span>' },
				area: { required: '<span style="color:red; font-size: 12px;">Select your Area Type</span>' },
				ddlMunicipalName: { required: '<span style="color:red; font-size: 12px;">Select your Municipality Name:</span>' },
				ddlPanchayat: { required: '<span style="color:red; font-size: 12px;">Select your Panchayat Samiti/Block.</span>' },
			},
		});
		$("#btnSubmit").click(function(e){
			if($("#frmcenter_learner").valid()){
				e.preventDefault();
				var selCourse = $("#ddlCourse option:selected").text();
				var selDistrict = $("#ddlDistrict option:selected").text();
				var selTehsil = $("#ddlTehsil option:selected").text();
				var selarea = $("input[name=area]:checked").val();
				var municipalname = $("#ddlMunicipalName option:selected").text();
				var selward = $("#ddlWardno option:selected").text();
				var samiti = $("#ddlPanchayat option:selected").text();
				var grampanch = $("#ddlGramPanchayat option:selected").text();
				var village = $("#ddlVillage option:selected").text();
				
				//var input_data = "action=Learnercenterlist&selDistrict="+selDistrict+"&selTehsil="+selTehsil+"&selarea="+selarea+"&municipalname="+municipalname+"&selward="+selward+"&samiti="+samiti+"&grampanch="+grampanch+"&village="+village;
				
				var serialize_data = $("#frmcenter_learner").serialize();
				var input_data = "action=Learnercenterlist&"+serialize_data;
				//alert(input_data);
					
				$.ajax({
					url: 'common/cfCenterlocation.php',
					type: "post",
					data: input_data,
					success: function (data) {
						$('#response').empty();
						$("#grid").html(data);
						$('#example').DataTable({
							dom: 'Bfrtip',
							buttons: [
								
							],
							  
								
								paging: true

						});
					}
				});
				
			}
		});
		
		
		
    });
	
    $(document).ready(function () { 
        FillStates();
		function FillStates() {
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillStates",
                success: function (data) {
                    //alert(data);
                    $('#ddlState').html(data);
					FillDistricts();
                }
            });
        }
		
		function FillDistricts() {
			var state = $('#ddlState').val();
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillDistricts&values="+state,
                success: function (data) {
					//alert(data);
                    $('#ddlDistrict').html(data);
					$("input:radio").attr("checked", false);
                }
            });
        }
		
		$("#ddlState").change(function(){
			$("input:radio").attr("checked", false);
			var state = $(this).val();
			$('#ddlDistrict_box').css('display','block');
			$.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillDistricts&values="+state,
                success: function (data) {
                    $('#ddlDistrict').html(data);
					$('#ddlTehsil').html("");
					$("input:radio").attr("checked", false);
					
					$('#ddlGramPanchayat').val('');
					$('#ddlPanchayat').val('');
					$('#ddlVillage').val('');
					$('#ddlMunicipalName').val('');
					$('#ddlMunicipalType').val('');
					$('#ddlWardno').val('');
					$("#Urban").css('display','none');
					$("#Rural").css('display','none');
					$("#grid").html("");
                }
            });
		}); 
		$("#ddlDistrict").change(function(){
			$("input:radio").attr("checked", false);
			var district = $(this).val();
			$('#ddlTehsil_box').css('display','block');
			$.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillTehsils&values="+district,
                success: function (data) {
                    $('#ddlTehsil').html(data);
					$('#ddlGramPanchayat').val('');
					$('#ddlPanchayat').val('');
					$('#ddlVillage').val('');
					$('#ddlMunicipalName').val('');
					$('#ddlMunicipalType').val('');
					$('#ddlWardno').val('');
					$("#Urban").css('display','none');
					$("#Rural").css('display','none');
					$("#grid").html("");
                }
            });
		}); 

        function FillPanchayat() {
            var selDistrict = ddlDistrict.value;
            //alert(selDistrict);
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FILLPanchayat&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlPanchayat').html(data);
                }
            });
        }

		function FillMunicipalName() {
            var selDistrict = ddlDistrict.value;
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FillMunicipalName&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlMunicipalName').html(data);
                }
            });
        }
		
		function FillMunicipalType() {
            var selDistrict = ddlDistrict.value;
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FillMunicipalType",
                success: function (data) {
                    //alert(data);
                    $('#ddlMunicipalType').html(data);
                }
            });
        }
		
		 $("#ddlMunicipalName").change(function () {
            var selMunicipalName = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfOrgUpdate.php',
                type: "post",
                data: "action=FILLWardno&values=" + selMunicipalName + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlWardno').html(data);
					$("#grid").html("");
                }
            });
        });
		
		
        $("#ddlPanchayat").change(function () {
            var selPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfCenterlocation.php',
                type: "post",
                data: "action=FILLGramPanchayat&values=" + selPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlGramPanchayat').html(data);
					$("#grid").html("");
                }
            });
        });
        
        $("#ddlGramPanchayat").change(function () {
            var selGramPanchayat = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfVillageMaster.php',
                type: "post",
                data: "action=FILL&values=" + selGramPanchayat + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlVillage').html(data);
					$("#grid").html("");
                }
            });
        });
		
		function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }

        function findselected() {

            var UrbanDiv = document.getElementById("areaUrban");
            var category = document.getElementById("Urban");

            if (UrbanDiv) {
                Urban.style.display = areaUrban.checked ? "block" : "none";
                Rural.style.display = "none";
            }
            else
            {
                Urban.style.display = "none";
            }
        }
        function findselected1() { 
            var RuralDiv = document.getElementById("areaRural");
            var category1 = document.getElementById("Rural");

            if (RuralDiv) {
                Rural.style.display = areaRural.checked ? "block" : "none";
                Urban.style.display = "none";
            }
            else
            {
                Rural.style.display = "none";
            }
        }
		
        $("#areaUrban").change(function () {
			$('#ddlGramPanchayat').val('');
			$('#ddlPanchayat').val('');
			$('#ddlVillage').val('');
			$("#grid").html("");

            findselected();
			FillMunicipalName();
			FillMunicipalType();
        });
        $("#areaRural").change(function () {
			$('#ddlMunicipalName').val('');
			$('#ddlMunicipalType').val('');
			$('#ddlWardno').val('');
			$("#grid").html("");
            findselected1();
            FillPanchayat();
        });        
		$("#marker_count").on("click",".send_locmodal",function(){
			$(".send_locmodal").removeClass("active_send_locmodal");
			$(this).addClass("active_send_locmodal");
		});
		$(".sendotp_btn").click(function(){
			if($("#ddlContact").val().length == 10){
				send_locotp();
			}
			else{
				var filter = /[^0-9]/;

				if (filter.test(phoneNumber)) {
				  if(phoneNumber.length==10){
					   var validate = true;
				  } else {
					  BootstrapDialog.alert('Please put 10  digit mobile number');
					  var validate = false;
				  }
				}
				else {
				  BootstrapDialog.alert('Not a valid number');
				  var validate = false;
				}
			}
		});
		$(".modal-footer").on('click','#modal_dismiss',function(){
			$("#otpval_locmodal").val("");
			$("#ddlContactOTP").val("");
			$("#ddlContact").val("");
			$("#OTP_divblock").css("display","none");
			$(".sendotp_btn").css("display","inline");
			$(".sendsms").css("display","none");
			$("#ddlContactOTP_error").html("");
		});
		$(".modal-footer").on('click','.sendsms',function(){
			var mobileno = $("#ddlContact").val();
			//var contactotp = $("#otpval_locmodal").val();
			var filledotp = $("#ddlContactOTP").val();
			var sms = $(".active_send_locmodal").attr('sms');
			var input_data = "action=sendsms&sms="+sms+"&mobileno="+mobileno+"&filledotp="+filledotp;
			if(filledotp != ""){
				$.ajax({
					url: 'common/cfCenterlocation.php',
					type: "post",
					data: input_data,
					success: function (data) { 
						if(data == "success"){ 
							$("#modal_dismiss").click();
							BootstrapDialog.alert("ITGK Location's details with google map link sent successfully on mobile no. "+mobileno+"\n Click on link received at your mobile no. to start navigation.");
						}
						else{
							$("#ddlContactOTP_error").html("invalid OTP.");
						}
						
					}
				});
			}else{
				$("#ddlContactOTP_error").html("Please enter OTP.");
			}
		});
		
    });
   

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
</html>