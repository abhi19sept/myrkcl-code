<?php
$title = "WCD Learner Status Update";
include ('header.php');
include ('root_menu.php');
?>

<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">WCD Learner Status Update</div>
            <div class="panel-body">

                <form name="frmupdatewcdlearnerstatus" id="frmupdatewcdlearnerstatus" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">IT-Gk Code:<span class="star">*</span></label>
								  <input type="text" name="txtitgk" id="txtitgk" class="form-control" placeholder="Enter IT-Gk Code">
                             </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Applicant Id:<span class="star">*</span></label>
								<input type="text" name="txtApplicantId" id="txtApplicantId" class="form-control" placeholder="Enter Applicant Id"/>
                             </div> 
							 
							 <div class="col-md-4 form-group" id="status" style="display:none">     
                                <label for="batch"> Select Status:<span class="star">*</span></label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control">
                                    <option value="">Please Select</option>
									<option value="1">Pending</option>
                                </select>
                            </div> 
						
                        </div>

						<div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="View Details"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>
						
						<div class="container" id="pending" style="display:none">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnPending" id="btnPending" class="btn btn-primary" value="Pending"/>    
                            </div>
                        </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showData() {			
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfUpdateWcdLearnerStatus.php"; // the script where you handle the form input.
            data = "action=GETLEARNERDATA&itgk=" + txtitgk.value + "&applicantid=" + txtApplicantId.value + ""; //
            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
					if (data == 1){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please Enter IT-Gk Code OR Applicant Id." + "</span></div>");
                            
						}
					
					else if (data == 2){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " No Record Found" + "</span></div>");
                            
						}
						
					else if (data == 3){
							$('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please Enter Either IT-Gk Code OR Applicant Id." + "</span></div>");
                            
						}
						
					else {
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
					$("#status").show();
					$("#txtitgk").attr('readonly','true');
					$("#txtApplicantId").attr('readonly','true');
					$("#pending").show();
					$("#btnSubmit").hide();
				}

                }
            });
        }

        $("#btnSubmit").click(function () {
            showData();
          return false; // avoid to execute the actual submit of the form.
        });
		
		  $("#btnPending").click(function () {		 
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfUpdateWcdLearnerStatus.php"; // the script where you handle the form input.            
            var data;			
            
				 data = "action=UpdateWcdLearner&lcode=" + txtApplicantId.value + "&status=" + ddlStatus.value + "&itgk=" + txtitgk.value + ""; // serializes the form's 
           
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmChangeWcdLearnerStatus.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("form");
                    }
					else if (data == 1)
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" +    " Please select Status." + "</span></div>");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
				}
            });
		
			return false;
		  });
		  
		  
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
