<?php 
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);

ini_set("memory_limit", "5000M");
ini_set("max_execution_time", 0);
set_time_limit(0);

$jobs = (isset($_REQUEST['job'])) ? $_REQUEST['job'] : 100;

?>

<span id="jobs"></span>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script>
$(document).ready(function(e) {
	var hit = 1;
	var jobTxt = '';
	var jobs = <?php echo $jobs . ';'; ?>
	function hitfnc(){
		$.ajax({
			url:'frmPayuTransactionResponse.php',
			type:'get'
		}).done(function(data){
			jobTxt = $('#jobs').html();
			$('#jobs').html(jobTxt + ', Done ' + hit);
			hit++;
			if(hit < jobs){
				hitfnc();
			}
		})
	}
	
	hitfnc();
	
});
</script>