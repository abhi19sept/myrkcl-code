<?php
$title = "Modify Admission Before FInal Exam";
include('header.php');
include('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var UserCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:20px;">

            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmadmissioncorrection" id="frmadmissioncorrection" class="form-inline" method="post"  role="form"
                      enctype="multipart/form-data">

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>
                        <div id="errorBox"></div>
                    </div>

                    <div class="panel panel-primary" >
                        <div class="panel-heading">Existing Learner Details</div>
                        <div class="panel-body">

                            <div id="grid" name="grid"></div>
                        </div>                      

                    </div>

                    <div class="panel panel-primary" style="margin-top:20px;">
                        <div class="panel-heading">Update Details For Correction</div>
                        <div class="panel-body">
                            <div class="col-sm-4 form-group"  id="picdiv" style="display: none">     
                                <label for="photo">New Photo:<span class="star">*</span></label><img id="uploadPreview1" src="images/samplephoto.png" id="pic" name="pic" width="80px" height="107px" onclick="javascript:document.getElementById('pic').click();">                                 

                            <input style="margin-top:5px !important;" id="uploadImage1" type="file" name="uploadImage1" onchange="checkPhoto(this);
                                    "/>  
                            </div>
                            <div class="col-sm-4 form-group"> 
                                <label for="empid">Proof Of Identity:<span class="star">*</span></label>
                                <select id="ddlidproof" name="ddlidproof" class="form-control">
                                    <option value="">Select</option>
                                    <option value="PAN Card">PAN Card</option>
                                    <option value="Voter ID Card">Voter ID Card</option>
                                    <option value="Driving License">Driving License</option>
                                    <option value="Passport">Passport</option>
                                    <option value="Employer ID card">Employer ID card</option>
                                    <option value="Government ID Card">Governments ID Card</option>
                                    <option value="College ID Card">College ID Card</option>
                                    <option value="School ID Card">School ID Card</option>
                                    <option value="Aadhar Card">Aadhar Card</option>
									<option value="10th Marksheet">10th Marksheet</option>
                                </select>

                            </div>
                            <div class="col-sm-4 form-group panel panel-danger">
                                
                                <div class="panel-body">
                                    <div class="col-sm-12" > 
                                        <label for="photo">Attach Document Proof:<span class="star">*</span></label>
                                        <!--<img id="uploadPreview7" src="images/sampleproof.png" id="uploadPreview7" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage7').click();">-->                                  
                                        <input style=" width: 115%" id="uploadImage7" type="file" name="uploadImage7" onchange="checkPANPhoto(this);"/> 
                                        <!--<input type="file"  name="copd" id="copd" onchange="ValidateSingleInput(this);"/>-->
                                        <span style="font-size:10px;">Note: PDF Allowed Size = 100 to 200 KB</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 form-group" id="lnamediv" style="display: none">     
                                <label for="learnercode">Learner Name:<span class="star">*</span></label>
                                <input type="text" class="form-control text-uppercase" maxlength="50" name="txtlname" id="txtlname" onkeypress="javascript:return allowchar(event);" placeholder="Learner Name">
                                <!--                                <div class="W50">
                                                                        <div id="regTitle"></div>
                                                                        <div id="progressbar"></div>
                                                                </div>-->

                                <div class="matchingblog" style="display:none" id="lnamematchdiv">
                                    <div id="regTitle1" class="matchingleft">Matching: </div>
                                    <div id="progressbarlname" class="matchingright"></div>
                                </div>


                            </div>

                            <div class="col-sm-4 form-group"  id="fnamediv" style="display: none"> 
                                <label for="fname">Father/Husband Name:<span class="star">*</span></label>
                                <input type="text" class="form-control text-uppercase" maxlength="50" name="txtfname" id="txtfname" onkeypress="javascript:return allowchar(event);" placeholder="Father Name">     
                                <div class="matchingblog" style="display:none" id="fnamematchdiv">
                                    <div id="regTitle1" class="matchingleft">Matching: </div>
                                    <div id="progressbarfname" class="matchingright"></div>
                                </div>
                            </div>


                            <div class="col-sm-4 form-group"  id="dobdiv" style="display: none">     
                                <label for="dob">Date of Birth:<span class="star">*</span></label>								
                                <input type="text" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="DD-MM-YYYY">
                            </div>
                            <div class="col-sm-4 form-group"  id="aadhardiv" style="display: none">
                                <label for="inputEmail3">Enter Aadhaar Number:<span class="star">*</span></label>

                                <input class="form-control valid" name="txtACno" id="txtACno" placeholder="Aadhaar Card Number" aria-invalid="false"  maxlength="12"  onkeypress="javascript:return allownumbers(event);"  type="text" >

                            </div>
                            <div class="col-sm-4 form-group" id="divVerifyLearner" name="divVerifyLearner" style="display: none" >
                                <input type="button" name="btnVarify" id="btnVarify" class="btn btn-primary" value="Verify Details"/>
                            </div>
                            <div class="col-sm-4 form-group" id="divUpdLearner" name="divUpdLearner" style="display: none">

                                <input type="submit" name="btnUpdLearner" id="btnUpdLearner" class="btn btn-primary" value="Submit"/>
                            </div>
                        </div>                      

                    </div>
                    <div class="panel panel-primary" >
                        <div class="panel-heading">Already Applied Learner Details For Correction</div>
                        <div class="panel-body">

                            <div id="grid2" name="grid"></div>
                        </div>                      

                    </div>
                    <input type="hidden" id="action" name="action" value="UpdLearner">
              <!--   </form> -->
                <link href="css/popup.css" rel="stylesheet" type="text/css">
                <!-- Modal -->
                <div id="myModalupdate" class="modal" style="padding-top:50px !important">

                    <div class="modal-content" style="width: 90%;">
                        <div class="modal-header">
                            <span class="close">&times;</span>
                            <h6>Verify Your Aadhar Number</h6>
                        </div>
                        <div class="modal-body" style="max-height: 800px;" id='formhtml'>
                           <!--  <form name="frmCourseBatch" id="frmCourseBatch" class="form-horizontal" role="form"
                                  enctype="multipart/form-data" style="margin: 25px 0 40px 0;">  --> 
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6" style="max-width: 90%!important;" id="responsesendotp"></div>
                                </div>

                                <div class="form-group" id="otpTxt" name="otpTxt" >
                                    <label for="inputEmail3" class="col-sm-3 control-label">OTP:</label>
                                    <div class="col-sm-8">
                                        <input class="form-control valid" name="txtotpTxt" id="txtotpTxt" placeholder="OTP" aria-invalid="false" maxlength="9"  onkeypress="javascript:return allownumbers(event);"  type="text">
                                    </div>
                                </div>
                                <div class="form-group" style="max-width: 90%!important;">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-8" id="turmcandition">
                                        <p>I,the holder of above mentioned Aadhar Number, hereby give my consent to RKCL to obtain my Aadhar Number, Name and Fingerprint/Iris for authentication with UIDAI. I have no objection using my identity and biometric information for validation with Aadhar(CIDR) only for the purpose of authentication will be used for generation and issuance of Digitally Signed RS-CIT certificate.</p>
                                        <div><span class="yesturm">I Agree</span> <input type="checkbox" id="yes" name="fooby[1][]" /> Yes
                <!--                              <input class="noturm" type="checkbox" id="no" name="fooby[1][]" /> No-->
                                        </div>
                                    </div>
                                    <div class="col-sm-2"></div>
                                </div>
                                <div class="form-group last">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <!--<input type="button" name="btnOTPSubmit" style="display: none;" id="btnOTPSubmit" class="btn btn-primary" value="Submit"/>-->
                                        <input type="button" name="btnAadharSubmit"   style="display: none;" id="btnAadharSubmit" class="btn btn-primary valid" value="Submit"/>
                                    </div>
                                </div>   
                                <div id="FinalResult" class="form-group"></div>
                                <input class="form-control valid"  name="txtTxn" id="txtTxn" type="hidden">
                                <input class="form-control valid" name="txtStatus" id="txtStatus" type="hidden">
                            <!-- </form> -->

                        </div>
                    </div>
                </div>

                <div id="myModalupdate2" class="modal" style="padding-top:150px !important">

                    <div class="modal-content panel-danger" style="width: 40%;">
                        <div class=" panel-heading">
                            <span class="close close2">&times;</span>
                            <h6>Warning</h6>
                        </div>
                        <div class="modal-body" style="max-height: 800px;" id='formhtml'>

                            <!-- <form name="frmCourseBatch1" id="frmCourseBatch1" class="form-horizontal" role="form"
                                  enctype="multipart/form-data" style="margin: 15px;">  
                                <div class="form-group" > -->

                                    <div class="col-sm-12" id="turmcandition">
                                        <p>क्या आप अपनी डिटेल्स आधार नंबर से सत्यापित नहीं करना चाहते हैं ?? यह आपको RS-CIT फ़ाइनल एक्जाम का डिजिटल साइन्ड ई-सर्टिफिकेट जनरेट करने मे मदद करता है|</p>
                                        <div><span class="yesturm">I Agree</span> <input type="checkbox" id="yes1" name="fooby[1][]" /> Yes
                <!--                              <input class="noturm" type="checkbox" id="no" name="fooby[1][]" /> No-->
                                        </div>
                                    </div>

                               
                                <div class="form-group" style="max-width: 90%!important;">
                                    <div class=" col-sm-6">
                                        <input type="button" name="btnOTPSubmitNo" style="display: none;" id="btnOTPSubmitNo" class="btn btn-danger" value="Verify With Aadhar"/>

                                    </div>
                                    <div class=" col-sm-6">
                                        <input type="submit" name="btnOTPSubmit1" style="display: none;" id="btnOTPSubmit1" class="btn btn-danger" value="Submit Without Verifcation" />
                                        <!-- <input type="button" name="uploadtest"  id="uploadtest" class="btn btn-danger" value="Without Verifcation" /> -->
                                     

                                    </div>
                                </div>  
                             </div>

                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php
include('footer.php');
include 'common/message.php';
include 'common/modals.php';
$mac = explode(" ", exec('getmac'));
?>
</body>

<style>

    .matchingblog{ margin-top: 5px;}
    .matchingleft{ width: 30%; float: left;}
    .matchingright{ width: 68%; float: right; height: 20px; margin-left: 2%;}

</style>

<script type="text/javascript">
    $('#dob').datepicker({
        format: "dd-mm-yyyy",
        orientation: "top auto",
        todayHighlight: true
    });
</script>

<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>
<script language="javascript" type="text/javascript">
    function checkPhoto(target) {
        var ext = $('#uploadImage1').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage1").value = '';
            return false;
        }

        if (target.files[0].size > 5000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 5 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        } else if (target.files[0].size < 3000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        document.getElementById("uploadImage1").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkPANPhoto(target) {
        var ext = $('#uploadImage7').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['pdf']) == -1) {
            alert('Image must be in PDF Format');
            document.getElementById("uploadImage7").value = '';
            return false;
        }

        if (target.files[0].size > 2000000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 2 MB");
            document.getElementById("uploadImage7").value = '';
            return false;
        } else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("uploadImage7").value = '';
            return false;
        }
        document.getElementById("uploadImage7").innerHTML = "";
        return true;
    }
</script>
<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
    }
    ;
</script>   
<script language="javascript" type="text/javascript">


    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";


    $(document).ready(function () {

        var cnt = 0;
        var aadharname = "";
        var aadhardob = "";
        var aadharno = "";
        var flagnameupd = 0;
        var flagdobupd = 0;
        var flaguidupd = 0;
        var flaguidbtn = 0;
        var flagpicupd = 0;

        function FillDetail() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfModifyAdmForAadhar.php",
                data: "action=GetLearner&values=" + AdmissionCode + "",
                success: function (data) {
                    //alert(data);
                    $("#grid").html(data);
                }
            });
        }
        FillDetail();
        function FillDetailExsist() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfModifyAdmForAadhar.php",
                data: "action=GetLearnerExsist&values=" + AdmissionCode + "",
                success: function (data) {
                    //alert(data);
                    $("#grid2").html(data);
                }
            });
        }
        FillDetailExsist();
        $("#grid").on("click", ".editbtnsreset", function () {

            //  alert(cnt);
            if (cnt <= 0)
            {
                $('#divUpdLearner').hide();
            }
//            $("#divVerifyLearner").show();
        });
        $("#grid").on("click", ".editbtns", function () {

            //alert(cnt);

        });
        $("#grid").on("click", "#btnlnameupd", function () {
            cnt = cnt + 1;
            $("#lnamediv").show();
            if (flaguidbtn == 0) {
                $('#divUpdLearner').show();
            }
        });
        $("#grid").on("click", "#btnfnameupd", function () {
            cnt = cnt + 1;
            $("#fnamediv").show();
            if (flaguidbtn == 0) {
                $('#divUpdLearner').show();
            }

        });
        $("#grid").on("click", "#btndobupd", function () {
            cnt = cnt + 1;
            $("#dobdiv").show();
            if (flaguidbtn == 0) {
                $('#divUpdLearner').show();
            }

        });
        $("#grid").on("click", "#btnlphotoupd", function () {
            cnt = cnt + 1;
            $("#picdiv").show();
            if (flaguidbtn == 0) {
                $('#divUpdLearner').show();
            }

        });
        $("#grid").on("click", "#btnaadharupd", function () {
            cnt = cnt + 1;
            $("#aadhardiv").show();
            $('#divUpdLearner').hide();
            $("#divVerifyLearner").show();
            flaguidbtn = 1;

        });

        $("#grid").on("click", "#btnlnameupdreset", function () {
            $("#lnamematchdiv").hide();
            $("#lnamediv").hide();
            $("#txtlname").val('');
            cnt = cnt - 1;
            if (flaguidbtn == 0) {
                $('#divUpdLearner').show();
            }
        });
        $("#grid").on("click", "#btnfnameupdreset", function () {
            $("#fnamematchdiv").hide();
            $("#fnamediv").hide();
            $("#txtfname").val('');
            cnt = cnt - 1;
            if (flaguidbtn == 0) {
                $('#divUpdLearner').show();
            }
        });
        $("#grid").on("click", "#btndobupdreset", function () {

            $("#dobdiv").hide();
            $("#dob").val('');
            cnt = cnt - 1;
            if (flaguidbtn == 0) {
                $('#divUpdLearner').show();
            }
        });
        $("#grid").on("click", "#btnaadharupdreset", function () {

            $("#aadhardiv").hide();
            $("#txtACno").val('');
            $("#divVerifyLearner").hide();
            flaguidupd = 0;
            flaguidbtn = 0;
            cnt = cnt - 1;
            if (flaguidbtn == 0) {
                $('#divUpdLearner').show();
            }
        });
        $("#grid").on("click", "#btnlphotoupdreset", function () {

            $("#picdiv").hide();
            $("#pic").val('');
            cnt = cnt - 1;
            if (flaguidbtn == 0) {
                $('#divUpdLearner').show();
            }
        });

//
//        function similitude(a, b) {
//            for (var i = 0, len = Math.max(a.length, b.length); i < len; i++)
//                if (a.charAt(i) != b.charAt(i))
//                    return Math.round(i / len * 100);
//        }
        function similitude(a, b) {


            var $str1 = a;
            var $str2 = b;

            var perc = Math.round(similarity($str1, $str2) * 10000) / 100;

            return perc;
//            var txt = "The strings: <span class=\"str\">'" + $str1 + "'</span> and <span class=\"str\">'" + $str2 + "'</span>";
//            txt += "<br>are similar <span class=\"perc\">" + perc + "%</span>";
//document.write(txt);
        }
        function similarity(s1, s2) {
            var longer = s1;
            var shorter = s2;
            if (s1.length < s2.length) {
                longer = s2;
                shorter = s1;
            }
            var longerLength = longer.length;
            if (longerLength === 0) {
                return 1.0;
            }
            return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
        }

        function editDistance(s1, s2) {
            s1 = s1.toLowerCase();
            s2 = s2.toLowerCase();

            var costs = new Array();
            for (var i = 0; i <= s1.length; i++) {
                var lastValue = i;
                for (var j = 0; j <= s2.length; j++) {
                    if (i == 0)
                        costs[j] = j;
                    else {
                        if (j > 0) {
                            var newValue = costs[j - 1];
                            if (s1.charAt(i - 1) != s2.charAt(j - 1))
                                newValue = Math.min(Math.min(newValue, lastValue),
                                        costs[j]) + 1;
                            costs[j - 1] = lastValue;
                            lastValue = newValue;
                        }
                    }
                }
                if (i > 0)
                    costs[s2.length] = lastValue;
            }
            return costs[s2.length];
        }
        $("#txtlname").keyup(function () {
            $("#lnamematchdiv").show();
            var a = $(this).val();
            var b = $("#lnameold").val();
            // var mm = similitude($(this).val(), $("#fnameold").val());

            //document.write("Similitude: " + similitude($(this).val(), $("#fnameold").val()) + "%");
            // $('#regTitle').html("Similitude: " + similitude(a, b) + "%");
//            $('#regTitle').empty().append("Matching: " + similitude(a, b) + "%");
            //$('.matchingright').empty().append(similitude(a, b) + "%");
            //  $("#progressbar").progressbar("value", similitude(a, b));
            var value = 100 - similitude(a, b);
            $("#progressbarlname").progressbar({
                value: similitude(a, b)
            });
            if (value < 25) {
                $("#progressbarlname .ui-widget-header").css({"background": "#58fb05", "color": "#333333"});
            }
            if (value > 25 && value < 50) {
                $("#progressbarlname .ui-widget-header").css({"background": "#fbaf05", "color": "#333333"});
            }
            if (value > 50 && value < 75) {
                $("#progressbarlnamev .ui-widget-header").css({"background": "#fb5405", "color": "#333333"});
            }
            if (value > 75) {
                $("#progressbarlname .ui-widget-header").css({"background": "#fb0505", "color": "#333333"});
            }
        });

        $("#txtfname").keyup(function () {
            $("#fnamematchdiv").show();
            var a = $(this).val();
            var b = $("#fnameold").val();

            var value = 100 - similitude(a, b);
            $("#progressbarfname").progressbar({
                value: similitude(a, b)
            });
            if (value < 25) {
                $("#progressbarfname .ui-widget-header").css({"background": "#58fb05", "color": "#333333"});
            }
            if (value > 25 && value < 50) {
                $("#progressbarfname .ui-widget-header").css({"background": "#fbaf05", "color": "#333333"});
            }
            if (value > 50 && value < 75) {
                $("#progressbarfname .ui-widget-header").css({"background": "#fb5405", "color": "#333333"});
            }
            if (value > 75) {
                $("#progressbarfname .ui-widget-header").css({"background": "#fb0505", "color": "#333333"});
            }
        });
        $("#btnVarify").click(function () {
            var selAadhar = $(this).val();
            //alert(selregion);
            var mybtnid = "myBtn_" + selAadhar;
            var modal = document.getElementById('myModalupdate');
            var btn = document.getElementById(mybtnid);
            var span = document.getElementsByClassName("close")[0];
            modal.style.display = "block";

            if (txtACno.value == '')
            {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter Aadhaar Number." + "</span></p>");
            } else {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/ajax-loader.gif width=40px /></span><span>Please wait we are fetching and verifying your Aadhaar Data.</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfAdmission.php",
                    data: "action=generateOTP&txtACno=" + txtACno.value + "",
                    success: function (data)
                    {
                        data = $.parseJSON(data);
                        $('#responsesendotp').empty();
                        if (data.status == "n")
                        {
                            $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Invalid Aadhaar Number. Please check your Aadhaar Number." + "</span></p>");
                        } else
                        {           // console.log(data);
                            //alert(data.txn);
                            $('#responsesendotp').append("<p class='sucess'><span><img src=images/correct.gif width=10px /></span><span>" + " We have sent OTP on your registered Aadhaar Number. Please enter OTP as received on Mobile." + "</span></p>");
                            txtTxn.value = data.txn;
                            txtStatus.value = data.status;
                            // $("#btnOTPSubmit").hide();
                            $("#otpTxt").show();
                            // $("#btnAadharSubmit").show();
                            $("#txtACno").attr("readonly", "readonly");
                            $("#txtPINno").attr("readonly", "true");
                        }
                    }
                });
            }

            span.onclick = function () {
                modal.style.display = "none";
//                    window.location.href = "frmadmissioncorrection.php";
                //$("#btnOTPSubmit").show();
                $("#otpTxt").hide();
                $("#btnAadharSubmit").hide();
                $("#txtACno").attr("readonly", false);



            }


        });
        $('#yes').change(function () {
            if ($(this).is(":checked")) {
                var returnVal = confirm("Are you sure?");
                if (returnVal) {
                    $("#btnAadharSubmit").css("display", "block");
                } else {
                    $("#btnAadharSubmit").css("display", "none");
                }

            } else {
                $("#btnAadharSubmit").css("display", "none");
            }
        });

        $('#yes1').change(function () {
            if ($(this).is(":checked")) {
                var returnVal = confirm("Are you sure?");
                if (returnVal) {
                    $("#btnOTPSubmit1").css("display", "block");
                    $("#btnOTPSubmitNo").css("display", "block");
                } else {
                    $("#btnOTPSubmit1").css("display", "none");
                    $("#btnOTPSubmitNo").css("display", "none");
                }

            } else {
                $("#btnOTPSubmit1").css("display", "none");
                $("#btnOTPSubmitNo").css("display", "none");

            }
        });
        $("#btnOTPSubmitNo").click(function () {
            var modal2 = document.getElementById('myModalupdate2');
            modal2.style.display = "none";
        });

        // $("#frmadmissioncorrection").on('submit',(function(e) {

        //     e.preventDefault();
        //     var modal2 = document.getElementById('myModalupdate2');
        //     modal2.style.display = "none";
        //     if (aadharno != txtACno.value) //for radio button
        //     {
        //         $('#response').empty();
        //         $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Verified Aadhar no must be same as entered Aadhar no.</span></div>");
        //         return false;
        //     }
        //   var data = new FormData(this);
        //   data.append('uidname', aadharname);
        //   data.append('uiddob', aadhardob);
        //   data.append('flagnameupd', flagnameupd);

        //   data.append('flagdobupd', flagdobupd);
  
        //   data.append('flaguidupd', flaguidupd);
        //   data.append('lcode', AdmissionCode);

        //     //var forminput = $("#frmadmissioncorrection").serialize();
        //     $.ajax({
        //         url: "common/cfModifyAdmForAadhar.php",
        //           type: "POST",
        //           data:  data,
        //           contentType: false,
        //           cache: false,
        //           processData:false,              
            
        //         // data: "action=UpdLearner&newname=" + txtlname.value + "&newfname=" + txtfname.value +
        //         //         "&newdob=" + dob.value + "&lcode=" + AdmissionCode + "&lnameold=" + lnameold.value +
        //         //         "&fnameold=" + fnameold.value + "&ldobold=" + ldobold.value + "&itgkcode=" + itgkcode.value +
        //         //         "&uidold=" + uidold.value + "&uidnew=" + txtACno.value + "&course=" + course.value + "&batch=" + batch.value +
        //         //         "&uidname=" + aadharname + "&uiddob=" + aadhardob + "&learnercode=" + learnercode.value +
        //         //         "&flagnameupd=" + flagnameupd + "&flagdobupd=" + flagdobupd + "&flaguidupd=" + flaguidupd + "&filedata=" + {data : jsonString} + "",
        //         success: function (data)
        //         {//alert(data);
        //             if (data == SuccessfullyInsert || data == SuccessfullyUpdate || data == 'Successfully Inserted	' || data == 'Successfully Updated	')
        //             {
        //                 $('#response').empty();
        //                 $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
        //                 window.setTimeout(function () {
        //                     Mode = "Add";
        //                     window.location.href = 'frmModifyAdmForAadhar.php';
        //                 }, 3000);
        //             } else
        //             {
        //                 $('#response').empty();
        //                 $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
        //             }
        //         }
        //     });
        // }));

        $("#btnAadharSubmit").click(function () {
            $("#btnAadharSubmit").hide();

            if (txtotpTxt.value == '')
            {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter OTP." + "</span></p>");
            } else {
                $('#responsesendotp').empty();
                $('#responsesendotp').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $("#txtotpTxt").attr("readonly", "true");
                $.ajax({
                    type: "post",
                    url: "common/cfadmission.php",
                    data: "action=authenticateOTP&txtACno=" + txtACno.value + "&txtTxn=" + txtTxn.value + "&txtStatus=" + txtStatus.value + "&txtotpTxt=" + txtotpTxt.value + "",
                    success: function (data)
                    {

                        data = $.parseJSON(data);
                        $('#responsesendotp').empty();
                        if (data.status == 'y') {
                            $('#responsesendotp').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>Aadhar Detail Verified</span></p>");
                            aadharname = data.learnername;
                            aadhardob = data.learnerdob;
                            aadharno = data.learneraadhar;

                            //$('#txtaadharno').val(aadharno);
                            //$("#txtaadharno").show();
//                            $("#ddlAadhar").hide();
                            window.setTimeout(function () {

                                $("#myModalupdate").css("display", "none");

                            }, 3000);

                            $('#divVerifyLearner').hide();
                            $('#divUpdLearner').show();

                            flagnameupd = 1;
                            flagdobupd = 1;
                            flaguidupd = 1;

                        } else {
                            $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data.message + "</span></p>");
                        }
                        //$("#FinalResult").html(data);

                    }
                });
            }
        });
       // $("#btnUpdLearner").click(function () {
            $("#frmadmissioncorrection").on('submit',(function(e) {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                if (flaguidupd == 1) {
                            e.preventDefault();
                            var data = new FormData(this);
                          data.append('uidname', aadharname);
                          data.append('uiddob', aadhardob);
                          data.append('flagnameupd', flagnameupd);

                          data.append('flagdobupd', flagdobupd);
                  
                          data.append('flaguidupd', flaguidupd);
                          data.append('lcode', AdmissionCode);

//            if ($("#frmadmissioncorrection").valid())
//            {

            if (($('#lnamediv').css('display') == 'none') && ($('#fnamediv').css('display') == 'none') && ($('#dobdiv').css('display') == 'none') && ($('#aadhardiv').css('display') == 'none')) {
                alert("Edit At Least one Field");
                return false;
            }
//            alert(txtACno.value);
            if (txtACno.value == '' && flaguidupd == 0) {
                var modal2 = document.getElementById('myModalupdate2');
                modal2.style.display = "block";
            }
            
                if (aadharno != txtACno.value) //for radio button
                {
                    $('#response').empty();
                    $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Verified Aadhar no must be same as entered Aadhar no.</span></div>");
                    return false;
                }
                $.ajax({
                    url: "common/cfModifyAdmForAadhar.php",
                      type: "POST",
                      data:  data,
                      contentType: false,
                      cache: false,
                      processData:false,   
                    // type: "post",
                    // url: "common/cfModifyAdmForAadhar.php",
                    // data: "action=UpdLearner&newname=" + txtlname.value + "&newfname=" + txtfname.value +
                    //         "&newdob=" + dob.value + "&lcode=" + AdmissionCode + "&lnameold=" + lnameold.value +
                    //         "&fnameold=" + fnameold.value + "&ldobold=" + ldobold.value + "&itgkcode=" + itgkcode.value +
                    //         "&uidold=" + uidold.value + "&uidnew=" + txtACno.value + "&course=" + course.value + "&batch=" + batch.value +
                    //         "&uidname=" + aadharname + "&uiddob=" + aadhardob + "&learnercode=" + learnercode.value +
                    //         "&flagnameupd=" + flagnameupd + "&flagdobupd=" + flagdobupd + "&flaguidupd=" + flaguidupd + "",
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate || data == 'Successfully Inserted	' || data == 'Successfully Updated	')
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                            window.setTimeout(function () {
                                Mode = "Add";
                                window.location.href = 'frmModifyAdmForAadhar.php';
                            }, 3000);
                        } else
                        {
                            $('#response').empty();
                            $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                        }
                    }
                });
            }
            else{
            e.preventDefault();
            var modal2 = document.getElementById('myModalupdate2');
            modal2.style.display = "none";
            if (aadharno != txtACno.value) //for radio button
            {
                $('#response').empty();
                $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>Verified Aadhar no must be same as entered Aadhar no.</span></div>");
                return false;
            }
          var data = new FormData(this);
          data.append('uidname', aadharname);
          data.append('uiddob', aadhardob);
          data.append('flagnameupd', flagnameupd);

          data.append('flagdobupd', flagdobupd);
  
          data.append('flaguidupd', flaguidupd);
          data.append('lcode', AdmissionCode);

            //var forminput = $("#frmadmissioncorrection").serialize();
            $.ajax({
                url: "common/cfModifyAdmForAadhar.php",
                  type: "POST",
                  data:  data,
                  contentType: false,
                  cache: false,
                  processData:false,              
            
                // data: "action=UpdLearner&newname=" + txtlname.value + "&newfname=" + txtfname.value +
                //         "&newdob=" + dob.value + "&lcode=" + AdmissionCode + "&lnameold=" + lnameold.value +
                //         "&fnameold=" + fnameold.value + "&ldobold=" + ldobold.value + "&itgkcode=" + itgkcode.value +
                //         "&uidold=" + uidold.value + "&uidnew=" + txtACno.value + "&course=" + course.value + "&batch=" + batch.value +
                //         "&uidname=" + aadharname + "&uiddob=" + aadhardob + "&learnercode=" + learnercode.value +
                //         "&flagnameupd=" + flagnameupd + "&flagdobupd=" + flagdobupd + "&flaguidupd=" + flaguidupd + "&filedata=" + {data : jsonString} + "",
                success: function (data)
                {//alert(data);
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate || data == 'Successfully Inserted  ' || data == 'Successfully Updated  ')
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-success'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></div>");
                        window.setTimeout(function () {
                            Mode = "Add";
                            window.location.href = 'frmModifyAdmForAadhar.php';
                        }, 3000);
                    } else
                    {
                        $('#response').empty();
                        $('#response').append("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></div>");
                    }
                }
            });
            }

            //}
            // }
            return false;
        }));
    });



</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmadmissioncorrection_validation.js" type="text/javascript"></script>
</html>