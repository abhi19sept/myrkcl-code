<?php
$title="Learner GST Receipt Download ";
include ('header.php'); 
include ('root_menu.php');   ?>

<div style="min-height:450px !important;">

        <div class="container" > 
			 
            <div class="panel panel-primary" style="margin-top:36px !important;" >

            <div class="panel-heading" id="non-printable">Learner GST Receipt Download</div>
                <div class="panel-body">
                    <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">    

                            <div class="container">

                                <div id="response"> </div>
                                <div id="errorBox"></div>

                                <div class="col-sm-2 form-group"   >

                                </div>
                                <div class="col-sm-2 form-group"   >

                                </div>

                                <div class="col-sm-4 form-group"   > 
                                    <label for="learnercode">Learner Code:<span class="star">*</span></label>
                                    <input type="text" maxlength="18" class="form-control"   name="LearnerCode" id="LearnerCode" onkeypress="javascript:return allownumbers(event);"  placeholder="Learner Code">
                                </div>

                            <div class="container" >
                                <label for="learnercode"></label>
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Download" style="margin-top:25px"/> 
                                 <!-- <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary"  value="Download" style="margin-top:25px"/>    -->
                            </div>
                            <div id="grid" style="margin-top:5px; width:94%;"> </div>
                         </div> 

                     </form> 

                </div>
					
            </div>   
        </div>
  </div>
						
	</body>					
<?php
    include "common/modals.php";
    include 'footer.php';
    include 'common/message.php';
?>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        $("#btnSubmit").click(function () {

            var lcode = $('#LearnerCode').val();
            var url = "common/cfGenerateLearnerReceipt.php"; 
             if(LearnerCode === ""){
                 return fasle;
             }else{
                $.ajax({
                type: "post",
                url: url,
                data: "action=SHOWALL&lcode=" + lcode,
                success: function (data) {
                   
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                }
            });

             }


            return false; // avoid to execute the actual submit of the form.
        });

    $("#grid").on('click', '.updcount',function(){
        var acode = $(this).attr('value');
        
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
          
        var url = "common/cfGenerateLearnerReceipt.php"; // the script where you handle the form input.
        var data;
        $.ajax({
            type: "post",
            url: url,
            data: "action=DwnldPrintRecp&lid=" + acode,
            success: function (data) {
                alert(data);
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                if(data == 0){
                   $('#response').empty();
                    $('#response').append("<p class='error'><span>Receipt Not Available..</span></p>");
                }
                else{

                    window.open(data, '_blank');
                    $('#response').empty();
                }
            }
        });
            

    });
            function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
 });
</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<!-- <script src="bootcss/js/frmuploadonlineclassurl.js" type="text/javascript"></script> -->
</html>