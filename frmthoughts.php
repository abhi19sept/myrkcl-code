<?php
$title="Thoughts";
include ('header.php'); 
include ('root_menu.php'); 

   if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
 ?>
<div style="min-height:430px !important;max-height:1500px !important;">
        <div class="container"> 
			

            <div class="panel panel-primary" style="margin-top:36px;">

                <div class="panel-heading">Thought of Day</div>
                <div class="panel-body">
                    <!-- <div class="jumbotron"> -->
                    <form name="frmthoughts" id="frmthoughts" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div class="container">
                                <div id="response"></div>

                            </div>        
							<div id="errorBox"></div>
                            

                            

                        </div>  

                     
                        
                        <div class="container">
                           <div class="col-sm-6"> 
                                <label for="ifsc">Thoughts:</label>
                                <textarea class="form-control" rows="3" cols="6" id="txtthoughts" name="txtthoughts" placeholder="Add Thoughts"></textarea>
								<input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                            </div> 
                            
                            
					</div>
					
					
					 <div class="container">
					
					       <div class="col-sm-4 form-group"> 
                                <label for="payreceipt">Upload Image:</label>
                                <input type="file" class="form-control"  name="thoughtimage" id="thoughtimage">
                            </div>
					</div>
					
                          

                        <div class="container">

                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="margin-top:20px;margin-left:15px"/>    
                        </div>
						
					<div id="gird"></div>
					

                </div>
            </div>   
        </div>


    </form>
	
</div>



</body>
<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>
<script src="scripts/uploadthoughts.js"></script>
<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        function Fillicons() {
            $.ajax({
                type: "post",
                url: "common/cfthoughts.php",
                data: "action=FILL",
                success: function (data) {
                    $("#icons").html(data);
                }
            });
        }
        Fillicons();

        

        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }
       
	   
	   
	    function GenerateUploadId()
                   {
					$.ajax({
						type: "post",
						url: "common/cfBlockUnblock.php",
						data: "action=GENERATEID",
						success: function (data) {                      
							txtGenerateId.value = data;					
						}
					});
                  }
                  GenerateUploadId();

        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfthoughts.php",
                data: "action=DELETE&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmthoughts.php";
                        }, 3000);
                        Mode = "Add";
                        resetForm("frmthoughts");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfthoughts.php",
                data: "action=EDIT&values=" + Code + "",
                success: function (data) {
                    data = $.parseJSON(data);
                    //alert(data[0].thoughts);
                    txtthoughts.value = data[0].thoughts;
					 
                    //alert($.parseJSON(data)[0]['StatusName']);
                }
            });
        }

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfthoughts.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);
								
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            
                        ]
                    });

                }
            });
        }

        showData();		
		
        $("#btnSubmit").click(function () {
			 if ($("#frmthoughts").valid())
             {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfthoughts.php"; // the script where you handle the form input.
			var thoughtimage = $('#txtGenerateId').val();
            var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&thoughts=" +txtthoughts.value + "&thoughtimage="+thoughtimage+""; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + Code + "&thoughts=" + txtthoughts.value +"&thoughtimage="+thoughtimage+ ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmthoughts.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmthoughts");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });
			 }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmthoughts_validation.js" type="text/javascript"></script>	
<style>
.error {
	color: #D95C5C!important;
}
</style>
<?php include ('footer.php'); ?>
</html>