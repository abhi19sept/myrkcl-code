<?php
$title = "Modify Batch";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Batch_Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Batch_Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
//print_r($_SESSION);
if ($_SESSION['User_Code'] == '1') {	
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 			  
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">Modify Batch</div>
            <div class="panel-body">
                <form name="frmmodifybatch" id="frmmodifybatch" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">

                            </select>

                        </div> 
                        <!--                    </div>
                        
                                            <div class="container">-->
                        <div class="col-sm-4 form-group">     
                            <label for="batch"> Select Batch:<span class="star">*</span></label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">

                            </select>									
                        </div> 
                    </div>


                    <div id="main-content" style="display:none;">
                        <div class="container">
                            <div class="container">
                                <div id="response"></div>
                            </div>        
                            <div id="errorBox"></div>

<!--                            <div class="col-sm-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">									
                                </select>
                                <select id="coursecode" name="coursecode" style="display:none;" class="form-control">									
                                </select>								
                            </div>						-->

                            <div class="col-sm-4 form-group"> 
                                <label for="sdate">Start Date:<span class="star">*</span></label>
                                <input type="text" class="form-control" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="YYYY-MM-DD">     
                            </div>

                            <div class="col-sm-4 form-group">     
                                <label for="edate">End Date:<span class="star">*</span></label>								
                                <input type="text" class="form-control" readonly="true" name="txtenddate" id="txtenddate"  placeholder="YYYY-MM-DD">
                            </div>

<!--                            <div class="col-sm-4 form-group"> 
                                <label for="mtongue">Batch Status:<span class="star">*</span></label>
                                <select id="ddlStatus" name="ddlStatus" class="form-control" >									
                                </select>    
                            </div>-->
                        </div>  

                        <div class="container"> 
<!--                            <div class="col-sm-4 form-group">     
                                <label for="batchname">Batch Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtBatchName" id="txtBatchName" placeholder="Batch Name">	
                            </div>-->

<!--                            <div class="col-sm-4 form-group"> 
                                <label for="install">Installation Mode:<span class="star">*</span></label>
                                <select id="ddlinstallmode" name="ddlinstallmode" class="form-control">
                                    <option value=""> Please Select Mode </option>
                                    <option value="1"> Single Installment Mode</option>
                                    <option value="2"> Double Installment Mode</option>
                                    <option value="3"> Tripple Installment Mode</option>
                                </select>
                            </div>-->

<!--                            <div class="col-sm-4 form-group"> 
                                <label for="certificate">Certificate_Code:<span class="star">*</span></label>
                                <select id="ddlcertificatecode" name="ddlcertificatecode" class="form-control">
                                    <option value=""> Please Select </option>
                                    <option value="C"> C - 70-30 Pattern</option>
                                    <option value="B"> B - 80-20 Pattern</option>
                                    <option value="A"> A - 90-10 Pattern</option>
                                    <option value="D"> D - 100-0 Pattern</option>
                                </select>
                            </div>     -->

<!--                            <div class="col-sm-4 form-group"> 
                                <label for="FYear">Financial Year:<span class="star">*</span></label>
                                <select id="ddlFinancial" name="ddlFinancial" class="form-control">
                                    <option value=""> Please Select </option>

                                    <option value="2009-2010"> Apr-2009 To Mar-2010</option>
                                    <option value="2010-2011"> Apr-2010 To Mar-2011</option>
                                    <option value="2011-2012"> Apr-2011 To Mar-2012</option>
                                    <option value="2012-2013"> Apr-2012 To Mar-2013</option>
                                    <option value="2013-2014"> Apr-2013 To Mar-2014</option>
                                    <option value="2014-2015"> Apr-2014 To Mar-2015</option>
                                    <option value="2015-2016"> Apr-2015 To Mar-2016</option>
                                    <option value="2016-2017"> Apr-2016 To Mar-2017</option>
                                    <option value="2017-2018"> Apr-2017 To Mar-2018</option>
                                </select>
                            </div>-->
                        </div>

                        <div class="container">  
                            <!-- <div class="col-sm-4 form-group"> 
                                <label for="batch">Release Batch Intake:</label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">									
                                                                </select>
                                                        </div> -->

<!--                            <div class="col-sm-4 form-group"> 
                                <label for="year">Year:<span class="star">*</span></label>
                                <select id="ddlYear" name="ddlYear" class="form-control">
                                    <option value=""> Please Select </option>
                                    <option value="2009"> 2009</option>
                                    <option value="2010"> 2010</option>
                                    <option value="2011"> 2011</option>
                                    <option value="2012"> 2012</option>
                                    <option value="2013"> 2013</option>
                                    <option value="2014"> 2014</option>
                                    <option value="2015"> 2015</option>
                                    <option value="2016"> 2016</option>
                                </select>
                            </div>-->

                            <div class="col-sm-4 form-group"> 
                                <label for="fee">Course Fee:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtCourseFee" id="txtCourseFee" onkeypress="javascript:return allownumbers(event);" placeholder="Course Fee">	
                            </div>

                            <div class="col-sm-4 form-group"> 
                                <label for="share">RKCL Share:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtShare" id="txtShare" onkeypress="javascript:return allownumbers(event);" placeholder="RKCL Share">	
                            </div>                           
                        </div>

                        <div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Update"/>    
                        </div>
                    </div>
            </div> 

            <div class="container">
            <!--	<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    -->
            </div>
        </div>
    </div>   
</div>
</form>
</div>
</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
<!--<script type="text/javascript"> 
 $('#txtstartdate').datepicker({                   
	format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
});  
</script>-->

<script type="text/javascript"> 
 $('#txtenddate').datepicker({    
		format: "yyyy-mm-dd",
		orientation: "bottom auto",
		todayHighlight: true
	});  
</script>	
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        //alert("hi");


        function FillCourse() {
            //alert("hello");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLCourseName",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });




        function showAllData() {
            //alert(val);
            //alert(val1);
            
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=EDIT&batch=" + ddlBatch.value + "&course=" + ddlCourse.value + "",
                success: function (data)
                {
                    //alert(data);
                    data = $.parseJSON(data);
                    txtstartdate.value = data[0].Batch_StartDate;
                    txtenddate.value = data[0].Batch_EndDate;
                    //ddlStatus.value = data[0].Batch_Status;
                    txtCourseFee.value = data[0].Course_Fee;
                    txtShare.value = data[0].RKCL_Share;
                   // txtGpfNo.value = data[0].Admission_GPFNO;

                    $("#main-content").show();
                  //  $('#txtLCode').attr('readonly', true);
                   // $("#btnShow").hide();

                }
            });
        }

        $("#ddlBatch").change(function () {
            showAllData();
        });


//        function showAllData(val) {
//            //alert(val);
//            //alert(val1);
//            $.ajax({
//                type: "post",
//                url: "common/cfBatchMaster.php",
//                data: "action=SHOW&batch=" + val + "",
//                success: function (data) {
//                    //alert(data);
//                    $("#menuList").html(data);
//                    $('#example').DataTable({
//                        dom: 'Bfrtip',
//                        buttons: [
//                            'copy', 'csv', 'excel', 'pdf', 'print'
//                        ]
//                    });
//                }
//            });
//        }

        $("#ddlBatch").change(function () {
            showAllData(this.value);
        });

//        if (Mode === 'Delete')
//        {
//            if (confirm("Do You Want To Delete This Item ?"))
//            {
//                deleteRecord();
//            }
//        }


//        function deleteRecord()
//        {
//            $('#response').empty();
//            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
//            $.ajax({
//                type: "post",
//                url: "common/cfAdmissionModify.php",
//                data: "action=DELETE&values=" + Admission_Name + "&batchcode=" + BatchCode + "",
//                success: function (data) {
//                    //alert(data);
//                    if (data == SuccessfullyDelete)
//                    {
//                        $('#response').empty();
//                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
//                        window.setTimeout(function () {
//                            window.location.href = "frmModifyAdmission.php";
//                        }, 1000);
//
//                        Mode = "Add";
//                        resetForm("frmmodifyadmission");
//                    }
//                    else
//                    {
//                        $('#response').empty();
//                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
//                    }
//                    //showData();
//                }
//            });
//        }


        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfBatchMaster.php"; // the script where you handle the form input.
            var data;
            var forminput = $("#frmmodifybatch").serialize();
            //alert(forminput);
            data = "action=UPDATE&" + forminput; // serializes the form's elements.
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmmodifybatch.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmmodifybatch");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>