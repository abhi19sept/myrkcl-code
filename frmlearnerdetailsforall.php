<?php
$title="Learner Dashboard ";
include ('header.php'); 
include ('root_menu.php');  

if (isset($_REQUEST['txtlcode'])) {
                echo "<script>var Code='" . $_REQUEST['txtlcode'] . "'</script>";
                echo "<script>var Mode='" . $_REQUEST['txtMode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            //echo "<pre>"; print_r($_SESSION);
            ?>
			
		
<link rel="stylesheet" href="css/datepicker.css">
<link rel="stylesheet" href="css/profile_style.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:450px !important;">

        <div class="container" > 
			 
            <div class="panel panel-primary" style="margin-top:36px !important;" >

            <div class="panel-heading" id="non-printable">Learner Enrolment Dashboard</div>
                <div class="panel-body">
                    <form name="frmlearnerdetails" id="frmlearnerdetails" action="" class="form-inline">     

                            <div class="container"  >



                                                            <div id="response"> </div>
                                                            <div id="errorBox"></div>

                                                            <div class="col-sm-2 form-group"   >

                                                            </div>
                                                            <div class="col-sm-2 form-group"   >

                                                            </div>

                                                            <div class="col-sm-4 form-group"   > 
                                    <label for="learnercode">Learner Code:<span class="star">*</span></label>
                                    <input type="text" maxlength="18" class="form-control"   name="LearnerCode" id="LearnerCode" onkeypress="javascript:return allownumbers(event);"  placeholder="Learner Code">
                                </div>

                            <div class="container" >
                                                            <label for="learnercode"></label>
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show" style="margin-top:25px"/>    
                            </div>
                                                     </div> 

                            <div id="gird"  ></div>	
                     </form> 

                </div>
					
            </div>   
        </div>
  </div>
						
	</body>					
<?php
    include "common/modals.php";
    include 'footer.php';
    include 'common/message.php';
?>

<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
</script>
<style>
#errorBox{
 color:#F00;
 }
</style>
<script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
							if(Code==""){
								//showdetails();
							}else{
								$('#LearnerCode').val(Code);
								showdetails();
								
							}
							
							$('#otp_modal').on('hidden.bs.modal', function () {
            $("#errorTextOTP, #successText, #responseText").html("");
            $("#showError, #showSuccess, #responseText, #verifyOTP").css("display","none");
            $("#newMobile, #otp").removeAttr("readonly");
            $("#newMobile, #otp").val("");
            $("#sendOTP").text("Send OTP");
            $("#sendOTP").removeClass("disabled");
        });

        $("#sendOTP").click(function () {

            $("#sendOTP").text("Please wait...");
            $("#sendOTP").addClass("disabled");

            var pattern = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;

            var phoneNumber = $("#newMobile").val();

            if (pattern.test(phoneNumber)) {
                if(phoneNumber.length==10){

                    $("#errorTextOTP, #successText").html("");
                    $("#showError, #showSuccess").css("display","none");

                    var url = "common/cflearnerdetails.php"; // the script where you handle the form input.
                    var data;
                    var forminput = "phoneNumber="+phoneNumber;
                    data = "action=sendOTP&" + forminput; // serializes the form's elements.

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data) {
                            $("#responseText").html(data);
                            $("#responseText").css("display","block");
                        }
                    });

                } else {
                    $("#responseText, #successText").html('');
                    $("#responseText, #showSuccess").css("display","none");
                    $("#errorTextOTP").html("Invalid mobile number");
                    $("#showError").css("display","block");
                    $("#sendOTP").text("Send OTP");
                    $("#sendOTP").removeClass("disabled");
                }
            }
            else {
                $("#responseText, #successText").html('');
                $("#responseText, #showSuccess").css("display","none");
                $("#errorTextOTP").html("Invalid mobile number");
                $("#showError").css("display","block");
                $("#sendOTP").text("Send OTP");
                $("#sendOTP").removeClass("disabled");
            }
        });

        $("#verifyOTPbtn").click(function () {

            $("#newMobile").attr("readonly","readonly");
            $("#otp").attr("readonly","readonly");

            if($("#otp").val()==""){

                $("#otp").removeAttr("readonly");
                $("#otp").val("");

                $("#responseText, #successText").html('');
                $("#responseText, #showSuccess").css("display","none");

                $("#errorTextOTP").html("Invalid OTP");
                $("#showError").css("display","block");
            } else {
                var url = "common/cflearnerdetails.php"; // the script where you handle the form input.
                var data;
                var forminput = "otp="+$("#otp").val();
                forminput = forminput + "&learner="+$("#LearnerCode").val()+"&mobile="+$("#newMobile").val();
                data = "action=verifyOTP&" + forminput; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        $("#responseText").html(data);
                        $("#responseText").css("display","block");
                    }
                });
            }
        });
		
		function showdetails(){
			if ($("#frmlearnerdetails").valid())
				{			 
				$('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                var url = "common/cflearnerdetails.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmlearnerdetails").serialize();
				data = "action=DETAILS&" +forminput; // serializes the form's elements.
								
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                       $('#response').empty();
		                    //$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span></span></p>");
                                       $("#gird").html(data);
                                       $('#example').DataTable({
                                                dom: 'Bfrtip',
                                                buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
                                       });      

                                    }
                                });
				}
		}
		
		/*	$("#btnSubmit").click(function () {
							 
			if ($("#frmlearnerdetails").valid())
				{
								 
				$('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                var url = "common/cflearnerdetails.php"; // the script where you handle the form input.
				var data;
				var forminput=$("#frmlearnerdetails").serialize();
				data = "action=DETAILS&" +forminput; // serializes the form's elements.
								
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                       $('#response').empty();
		                    //$('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span></span></p>");
                                       $("#gird").html(data);
                                       $('#example').DataTable({
                                                dom: 'Bfrtip',
                                                buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
                                       });      

                                    }
                                });
				}
                                return false; // avoid to execute the actual submit of the form.
                            });*/
			$("#btnSubmit").click(function () {
						showdetails();	 
			 //return false; // avoid to execute the actual submit of the form.
            });
                            
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }

            $(document).on("click", '.lphoto', function(event) { 
                    $('#gird').find('#lphoto').click();
                });

                $(document).on("change", '#lphoto', function(event) { 
                    PreviewImage('lphoto');
                });

                $(document).on("click", '.lsign', function(event) { 
                    $('#gird').find('#lsign').click();
                });

                $(document).on("change", '#lsign', function(event) { 
                    PreviewImage('lsign');
                });

                        });


                function checkPhoto(idname) {
                var ext = $(document).find('#' + idname).val().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
                    alert('Image must be in either PNG or JPG Format');
                    document.getElementById(idname).value = '';
                    return false;
                } else {
                    return true;
                }
            }

            function PreviewImage(idname) {
                var type = (idname == 'lphoto') ? 'photo' : 'sign';
                var ok = confirm('Are you sure, want to update learner ' + type);
                if (!ok || ! checkPhoto(idname)) {
                    return;
                }
                var oFReader = new FileReader();
                oFReader.readAsDataURL(document.getElementById(idname).files[0]);
                oFReader.onload = function (oFREvent) {
                    $(document).find('.' + idname).attr('src', oFREvent.target.result);
                }

                var data = new FormData();
                data.append(idname, document.getElementById(idname).files[0]);
                data.append('action', 'uploadphotosign');
                data.append('phototype', idname);
                data.append('learnercode', $('#LearnerCode').val());

                var request = new XMLHttpRequest();
                request.onreadystatechange = function() {
                    if (request.readyState === 4) {
                        try {
                            var resp = JSON.parse(request.response);
                        } catch (e) {
                            var resp = {
                                status: 'error',
                                data: 'Unknown error occurred: [' + request.responseText + ']'
                            };
                        }
                        if (resp.status==='success') {alert(resp.data);}
                    }
                };
                request.open('POST', 'common/cflearnerdetails.php');
                request.send(data);
            }

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmlearnerdetails.js"></script>
<style>
.error {
	color: #D95C5C!important;
}
.lphoto,
.lsign {
    cursor: pointer;
}
td {
    font-family: times new roman;
    font-size: 15px;
}
legend {
    font-size: 17px !important;
}

.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border: 1px solid #dddddd;
    line-height: 1.42857;
    padding: 8px;
    vertical-align: top;
}
</style>

</html>