<?php
$title = "Learner Feedback Report";
include ('header.php');
include ('root_menu.php');
//print_r($_SESSION);
if (isset($_REQUEST['code'])) {
    echo "<script>var UserLoginID=" . $_SESSION['User_LoginId'] . "</script>";
    echo "<script>var UserCode=" . $_SESSION['User_Code'] . "</script>";
    echo "<script>var UserParentID=" . $_SESSION['User_ParentId'] . "</script>";
    echo "<script>var UserRole=" . $_SESSION['User_UserRoll'] . "</script>";
} else {
    echo "<script>var UserLoginID=0</script>";
    echo "<script>var UserRole=0</script>";
    echo "<script>var UserParentID=0</script>";
    echo "<script>var UserRole=0</script>";
}
echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
?>

<div style="min-height:430px !important;max-height:1500px !important;">
    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Learner Feedback Report</div>
            <div class="panel-body">

                <form name="frmWcdFeedbackRpt" id="frmWcdFeedbackRpt" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-sm-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control">
                                    <option value="0">Select Course</option>
                                    <option value="3">RS-CIT Women</option>
                                </select>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control">

                                </select>
                            </div> 

                        </div>
                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Click to Generate Report"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


        /*for Course  function */
		function FillCourse() {
			//alert("hello");
			$.ajax({
				type: "post",
				url: "common/cfInnaugrationdetails.php",
				data: "action=FILLCOURSE",
				success: function (data) {
					//alert(data);
					
					$("#ddlCourse").html(data);
				}
			});
		}
		FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();
            //alert(selCourse);
            $.ajax({
                type: "post",
                url: "common/cfBatchMaster.php",
                data: "action=FILLAdmissionBatchcode&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });

        });


        function showData() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdFeedbackRpt.php"; // the script where you handle the form input.

            var data;
            //alert (data);
			if(ddlBatch.value==249 || ddlBatch.value==250)
			{
				data = "action=GETCUSTOM&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
			}
			else
			{
				//data = "action=GETDATA&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
				data = "action=GETCUSTOM&course=" + ddlCourse.value + "&batch=" + ddlBatch.value + ""; //
			}
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {

                    $('#response').empty();

                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });


                }
            });
        }

        function showDataPO() {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfWcdFeedbackRpt.php"; // the script where you handle the form input.
            var role_type = '';

            var startdate = $('#txtstartdate').val();
            var enddate = $('#txtenddate').val();
            var data;
            data = "action=GETDATASP&rolecode=" + UserLoginID + ""; //
            // alert(data);
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
                }
            });
        }


        $("#btnSubmit").click(function () {


//                if (User_UserRole == 14) {
//
//                    //alert(1);
//                    showDataSP();
//                } else {
//                   alert(2);
            showData();


        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>

<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
