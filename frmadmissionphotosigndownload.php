<?php
$title="Learner Admission Photo Sign Download";
include ('header.php'); 
include ('root_menu.php'); 
//echo $_SESSION['User_UserRoll'];
if (isset($_REQUEST['code'])) {
                echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var Code=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
			if ($_SESSION['User_UserRoll'] == '1' || $_SESSION['User_UserRoll'] == '8' || $_SESSION['User_UserRoll'] == '28') {	
            ?>			

<div style="min-height:430px !important;max-height:auto !important;" >
        <div class="container"> 
			<div class="panel panel-primary" style="margin-top:46px !important;">
                <div class="panel-heading">Learner Admission Photo Sign Download</div>
					<div class="panel-body">                    
						<form name="frmadmissionphotosigndownload" id="frmadmissionphotosigndownload"  class="form-inline">				
                            <div class="container">
                                <div id="response"></div>
                            </div>        
							<div id="errorBox"></div>
							
							<div id="search">
								 <div class="col-sm-4 form-group"> 
									<label for="course">Select Course:<span class="star">*</span></label>
									<select id="ddlCourse" name="ddlCourse" class="form-control">

									</select>
								</div> 
                    
								<div class="col-md-4 form-group">     
									<label for="batch"> Select Batch:<span class="star">*</span></label>
									<select id="ddlBatch" name="ddlBatch" class="form-control">

									</select>
								</div>

								<div class="col-md-4 form-group">     
									<label for="batch"> Select Type:<span class="star">*</span></label>
									<select id="ddlType" name="ddlType" class="form-control">
										<option value="photo">Download Photo</option>
										<option value="sign">Download Sign</option>
									</select>
								</div>	
								<div class="col-sm-4 form-group" id="step1">				
									<input type="button" name="btnstep1" id="btnstep1" class="btn btn-primary" 
											value="Step 1" style="margin-top:25px"/>							
								
								</div>
								<div class="col-sm-4 form-group" id="step2">				
									<input type="button" name="btnstep2" id="btnstep2" class="btn btn-primary" 
											value="Step 2" style="margin-top:25px"/>							
								
								</div>
								<div class="col-sm-4 form-group" id="step3">				
									<input type="button" name="btnstep3" id="btnstep3" class="btn btn-primary" 
											value="Step 3 Download Zip"  style="margin-top:25px"/>							
								
								</div>							
								<div class="col-sm-4 form-group" id="btnShow1" style="display:none">				
									<input type="button" name="btnShow" id="btnShow" class="btn btn-primary" 
											value="Generate Zip" style="margin-top:25px"/>							
								
								</div>
								<div class="col-sm-4 form-group" style="display:none" id="btnShowZip1">				
									<input type="button" name="btnCreateZip" id="btnCreateZip" class="btn btn-primary" 
											value="Create Zip" style="margin-top:25px;"/>							
								
								</div>
								<div class="col-sm-4 form-group" style="display:none" id="btnShowZip1">				
									<input type="button" name="btnShowZip" id="btnShowZip" class="btn btn-primary" 
											value="Download Zip" style="margin-top:25px;"/>							
								
								</div>
							</div>
							
							
						 <div class="row">
							<div class="col-xs-12">
								<div id="gird" ></div>					
							</div>
						 </div>
						
                    </div>
				</form>
			</div>   
        </div>			
	</div>
</div>
    

</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>
  


 <script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {	
    $("#btnstep2").prop('disabled', true);				
    $("#btnstep3").prop('disabled', true);				
	var foldername ="";
	function FillCourse() {
		$.ajax({
			type: "post",
			url: "common/cfAdmissionPhotoSignDownload.php",
			data: "action=FillCourse",
			success: function (data) {
				//alert(data);
				$("#ddlCourse").html(data);
			}
		});
	}
	FillCourse();

	$("#ddlCourse").change(function () {
		var selCourse = $(this).val();
		//alert(selCourse);
		$.ajax({
			type: "post",
			url: "common/cfAdmissionPhotoSignDownload.php",
			data: "action=FillBatch&values=" + selCourse + "",
			success: function (data) {
				$("#ddlBatch").append(data);

			}
		});

	});
	$("#btnstep1").click(function () 
	{
		    //alert(ddlBatch.value);
			var batchname= $("#ddlBatch option:selected").text();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
           var url = "common/cfAdmissionPhotoSignDownload.php"; // the script where you handle the form input.
			var data;
			var forminput=$("#frmadmissionphotosigndownload").serialize();
			
				data = "action=stepfirst&" +forminput+"&batchname= " + batchname; // serializes the form's elements.
			
                $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {										
					$('#response').empty();
					if (data == 1) {
						$('#response').append("<p>Text File Created Go to Step 2</p>");
						$("#btnstep2").prop('disabled', false);	
												
					}
					else{
						$('#response').append("<p>Something went wrong</p>");
						$("#btnstep2").prop('disabled', true);	
					}
					
                }
            });
		   return false; // avoid to execute the actual submit of the form.
	});
	$("#btnstep2").click(function () 
	{
		    //alert(ddlBatch.value);
			var batchname= $("#ddlBatch option:selected").text();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
           var url = "common/cfAdmissionPhotoSignDownload.php"; // the script where you handle the form input.
			var data;
			var forminput=$("#frmadmissionphotosigndownload").serialize();
			
				data = "action=stepsecond&" +forminput+"&batchname= " + batchname; // serializes the form's elements.
			
                $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {										
					$('#response').empty();
				    $("#btnstep3").prop('disabled', false);	
					
                }
            });
		   return false; // avoid to execute the actual submit of the form.
	});
	$("#btnstep3").click(function () 
	{
		    //alert(ddlBatch.value);
			var batchname= $("#ddlBatch option:selected").text();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
           var url = "common/cfAdmissionPhotoSignDownload.php"; // the script where you handle the form input.
			var data;
			var forminput=$("#frmadmissionphotosigndownload").serialize();
			
				data = "action=stepthird&" +forminput+"&batchname= " + batchname; // serializes the form's elements.
			
                $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {										
					$('#response').empty();
					
                }
            });
		   return false; // avoid to execute the actual submit of the form.
	});
	$("#btnShow").click(function () 
	{							
		    //alert(ddlBatch.value);
			var batchname= $("#ddlBatch option:selected").text();
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
           var url = "common/cfAdmissionPhotoSignDownload.php"; // the script where you handle the form input.
			var data;
			var forminput=$("#frmadmissionphotosigndownload").serialize();
			
				data = "action=FillDetails&" +forminput+"&batchname= " + batchname; // serializes the form's elements.
			
                $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {										
					$('#response').empty();
					if (data == 0) {
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " No Image Available.." + "</span></p>");$("#btnShowZip").show();
						$("#btnShowZip1").hide();											
					}
					else if (data == "c") {
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Please Select Course." + "</span></p>");	
						$("#btnShowZip1").hide();											
					}
					else if (data == "b") {
						$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Please Select Batch." + "</span></p>");	
						$("#btnShowZip1").hide();											
					}
					else 
					{		foldername = data;												
						$("#btnShowZip1").show();
						// $("#btnShow1").hide();
					}
                }
            });
		   return false; // avoid to execute the actual submit of the form.
        });
									
			function deletefile(filename){
				$.ajax({
						type: "post",
						url: "common/cfAdmissionPhotoSignDownload.php",
						data: "action=Deletefile&file="+ filename + "",
						success: function (data) {													 
						}
					});
			}
		  $("#btnShowZip").click(function () 
						{
							var url = "common/cfAdmissionPhotoSignDownload.php";
											 $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
											
							data = "action=DOWNLOADZIP&foldername=" +foldername+ "&type=" + ddlType.value;
																	
											$.ajax({
												type: "POST",
												url: url,
												data: data,
												success: function (data)
												{
													$('#response').empty();
													window.open(data,"_blank");
													 window.setTimeout(function () {
														deletefile(data);
													}, 33000);													
												}
											});
						});
					function resetForm(formid) 
					{
						$(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
					}

                        });

                    </script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
	
<style>
.error {
	color: #D95C5C!important;
}
</style>

</html>
<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "logout.php";

    </script>
    <?php
}
?>