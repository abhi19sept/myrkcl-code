<?php
$title = "Batch Master";
include ('header.php');
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
    echo "<script>var BatchCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var BatchCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
if ($_SESSION['User_Code'] == '1') {
    ?>
    <link rel="stylesheet" href="css/datepicker.css">
    <script src="scripts/datepicker.js"></script>

    <div class="container"> 


        <div class="panel panel-primary" style="margin-top:36px !important;">
            <div class="panel-heading">Batch Master </div>
            <div class="panel-body">					
                <form name="frmbatchmaster" id="frmbatchmaster" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>

                        <div class="col-sm-4 form-group"> 
                            <label for="course">Select Course:<span class="star">*</span></label>
                            <select id="ddlCourse" name="ddlCourse" class="form-control">									
                            </select>
                            <select id="coursecode" name="coursecode" style="display:none;" class="form-control">									
                            </select>								
                        </div>						

                        <div class="col-sm-4 form-group"> 
                            <label for="sdate">Start Date:<span class="star">*</span></label>
                            <input type="text" class="form-control" name="txtstartdate" id="txtstartdate" readonly="true" placeholder="YYYY-MM-DD">     
                        </div>

                        <div class="col-sm-4 form-group">     
                            <label for="edate">End Date:<span class="star">*</span></label>								
                            <input type="text" class="form-control" readonly="true" name="txtenddate" id="txtenddate"  placeholder="YYYY-MM-DD">
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="mtongue">Batch Status:<span class="star">*</span></label>
                            <select id="ddlStatus" name="ddlStatus" class="form-control" >									
                            </select>    
                        </div>
                    </div>  

                    <div class="container"> 
                        <div class="col-sm-4 form-group">     
                            <label for="batchname">Batch Name:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="50" name="txtBatchName" id="txtBatchName" placeholder="Batch Name">	
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="install">Installation Mode:<span class="star">*</span></label>
                            <select id="ddlinstallmode" name="ddlinstallmode" class="form-control">
                                <option value=""> Please Select Mode </option>
                                <option value="1"> Single Installment Mode</option>
                                <option value="2"> Double Installment Mode</option>
                                <option value="3"> Tripple Installment Mode</option>
                            </select>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="certificate">Certificate_Code:<span class="star">*</span></label>
                            <select id="ddlcertificatecode" name="ddlcertificatecode" class="form-control">
                                <option value=""> Please Select </option>
                                <option value="C"> C - 70-30 Pattern</option>
                                <option value="B"> B - 80-20 Pattern</option>
                                <option value="A"> A - 90-10 Pattern</option>
                                <option value="D"> D - 100-0 Pattern</option>
                            </select>
                        </div>     

                        <div class="col-sm-4 form-group"> 
                            <label for="FYear">Financial Year:<span class="star">*</span></label>
                            <select id="ddlFinancial" name="ddlFinancial" class="form-control">
                                <option value=""> Please Select </option>
                                <!--<option value="2018-2019"> Apr-2018 To Mar-2019</option>
                                <option value="2019-2020"> Apr-2019 To Mar-2020</option>-->
								<option value="2020-2021"> Apr-2020 To Mar-2021</option>
								<option value="2021-2022"> Apr-2021 To Mar-2022</option>
								<option value="2022-2023"> Apr-2022 To Mar-2023</option>
								<option value="2023-2024"> Apr-2023 To Mar-2024</option>
								<option value="2024-2025"> Apr-2024 To Mar-2025</option>
                            </select>
                        </div>
                    </div>

                    <div class="container">  
                        <!-- <div class="col-sm-4 form-group"> 
                            <label for="batch">Release Batch Intake:</label>
                            <select id="ddlBatch" name="ddlBatch" class="form-control">									
                                                            </select>
                                                    </div> -->

                        <div class="col-sm-4 form-group"> 
                            <label for="year">Year:<span class="star">*</span></label>
                            <select id="ddlYear" name="ddlYear" class="form-control">
                                <option value=""> Please Select </option>                               
                                <!--<option value="2017"> 2017</option>
                                <option value="2018"> 2018</option>
                                <option value="2019"> 2019</option>
								<option value="2020"> 2020</option>-->
								<option value="2021"> 2021</option>
								<option value="2022"> 2022</option>
								<option value="2023"> 2023</option>
								<option value="2024"> 2024</option>
								<option value="2025"> 2025</option>
                            </select>
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="fee">Course Fee:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="50" name="txtCourseFee" id="txtCourseFee" onkeypress="javascript:return allownumbers(event);" placeholder="Course Fee">	
                        </div>

                        <div class="col-sm-4 form-group"> 
                            <label for="share">RKCL Share:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="50" name="txtShare" id="txtShare" onkeypress="javascript:return allownumbers(event);" placeholder="RKCL Share">	
                        </div> 

                        <div class="col-sm-4 form-group"> 
                            <label for="share">VMOU Share:</label>
                            <input type="text" class="form-control" maxlength="50" name="txtVMOUShare" id="txtVMOUShare" onkeypress="javascript:return allownumbers(event);" placeholder="VMOU Share">	
                        </div>  
                    </div>

                    <div class="container">
                        <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                    </div>

                    <div id="gird" style="margin-top:35px;"> </div>
            </div>
        </div>   
    </div>
    </form>
    </body>
    <?php include'common/message.php'; ?>
    <?php include ('footer.php'); ?>

    <script type="text/javascript">
        $('#txtstartdate').datepicker({
            format: "yyyy-mm-dd",
            orientation: "bottom auto",
            todayHighlight: true
        });
    </script>

    <script type="text/javascript">
        $('#txtenddate').datepicker({
            format: "yyyy-mm-dd",
            orientation: "bottom auto",
            todayHighlight: true
        });
    </script>	

    <script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")
            if (key == 8 || key == 0) {
                keychar = 8;
            }
            return reg.test(keychar);
        }
    </script>

    <script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

        $("#txtstartdate, #txtenddate").datepicker();
        $("#txtenddate").change(function () {
            var txtstartdate = document.getElementById("txtstartdate").value;
            var txtenddate = document.getElementById("txtenddate").value;
            if ((Date.parse(txtenddate) <= Date.parse(txtstartdate))) {
                alert("End date should be greater than Start date");
                document.getElementById("txtenddate").value = "";
            }
        });

        $(document).ready(function () {
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }
            FillStatus();

            function FillCourse() {
                $.ajax({
                    type: "post",
                    url: "common/cfCourseMaster.php",
                    data: "action=FILLCourseName",
                    success: function (data) {
                        $("#ddlCourse").html(data);
                    }
                });
            }
            FillCourse();

            function FillBatch() {
                $.ajax({
                    type: "post",
                    url: "common/cfBatchMaster.php",
                    data: "action=FILLALLBatch",
                    success: function (data) {
                        $("#ddlBatch").html(data);
                    }
                });
            }
            FillBatch();

            function GetCourseCode(val) {
                alert(val);
                $.ajax({
                    type: "post",
                    url: "common/cfCourseMaster.php",
                    data: "action=FILLCourseCode&values=" + val + "",
                    success: function (data) {
                        alert(data);
                        $("#coursecode").html(data);
                    }
                });
            }

            $("#ddlCourse").change(function () {
                //GetCourseCode(this.value);
            });

            $("#btnSubmit").click(function () {
                if ($("#frmbatchmaster").valid())
                {
                    $('#response').empty();
                    $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");

                    var startdate = $('#txtstartdate').val();
                    var enddate = $('#txtenddate').val();
                    var url = "common/cfBatchMaster.php"; // the script where you handle the form input.
                    var data;
                    if (Mode == 'Add')
                    {
                        data = "action=ADD&name=" + txtBatchName.value + "&course=" + ddlCourse.value + "&status=" + ddlStatus.value +
                                "&startdate=" + startdate + "&enddate=" + enddate + "&installmode=" + ddlinstallmode.value + "&certcode=" + ddlcertificatecode.value +
                                "&finance=" + ddlFinancial.value + "&year=" + ddlYear.value +
                                "&cfees=" + txtCourseFee.value + "&share=" + txtShare.value + "&vmoushare=" + txtVMOUShare.value + ""; // serializes the form's elements.				 
                    } else
                    {
                        data = "action=UPDATE&code=" + EoiConfigCode +
                                "&name=" + txtEoiName.value + "&course=" + ddlCourse.value +
                                "&startdate=" + txtstartdate.value + "&enddate=" + txtenddate.value +
                                "&pfees=" + txtPFees.value + "&cfees=" + txtCFees.value +
                                "&eclist=" + eclist + "&tncdoc=" + tncdoc + ""; // serializes the form's elements.
                    }
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data)
                        {
                            if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                window.setTimeout(function () {
                                    window.location.href = "frmbatchmaster.php";
                                }, 1000);
                                Mode = "Add";
                                resetForm("frmbatchmaster");
                            } else
                            {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                            }
                            //showData();
                        }
                    });
                }
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }
        });
    </script>
    </html>
    <script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
    <script src="bootcss/js/frmbatchmaster_validation.js" type="text/javascript"></script>	
    <?php
} else {
    session_destroy();
    ?>
    <script>

    window.location.href = "index.php";

    </script>
    <?php
}
?>