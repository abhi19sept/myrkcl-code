<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>EOI Configuration</title>
         <link href="css/jquery-ui.css" rel="Stylesheet" type="text/css" />
    </head>
    <body>
        <div class="wrapper">
            <?php include './include/header.html';
				  include './include/menu.php';
            if (isset($_REQUEST['code'])) {
                echo "<script>var EoiConfigCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var EoiConfigCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
            <div class="main">
                <h1>EOI Configuration</h1>
                <form name="frmEoiConfig" id="frmEoiConfig" action="" enctype="multipart/form-data">
                    <table border="0" cellpadding="0" cellspacing="10" width="100%" class="field">
                        <tr>
                            <td colspan="6" id="response"> </td>
                        </tr>                       
                                    <tr align="left">
							<td nowrap="nowrap">Name of the EOI</td>
							<td>:</td>
							<td colspan="3"><input type="text" name="txtEoiName" id="txtEoiName" class="text" maxlength="" /></td>
							
							<td nowrap="nowrap">Select Course</td>
							<td>:</td>
							<td>
								<select id="ddlCourse" name="ddlCourse" class="text">
								<option selected="true"value="">Select</option>
									<option value="RF-CFA" > RS-CFA </option>																	
								</select>
								&nbsp;<font color="red">*</font>
							</td>						
                                    </tr>				
						
						<tr align="left">
							<td nowrap="nowrap">Start Date</td>
							<td>:</td>
							<td colspan="3"><input type="text" name="txtstartdate" id="txtstartdate" class="text" maxlength="" placeholder="MM-DD-YYYY"/></td>						
                                                        <td nowrap="nowrap">End Date</td>
							<td>:</td>
							<td colspan="3"><input type="text" name="txtenddate" id="txtenddate" class="text" maxlength="" placeholder="MM-DD-YYYY"/></td>						
                                                </tr>
								
						<tr>
							<td nowrap="nowrap">Processing Fee</td>
							<td>:</td>
							<td colspan="3"><input type="text" name="txtPFees" id="txtPFees" class="text" maxlength="" /></td>                                                        
                                                        <td nowrap="nowrap">Course Fee</td>
							<td>:</td>
							<td colspan="3"><input type="text" name="txtCFees" id="txtCFees" class="text" maxlength="" /></td>
						</tr>
						
						<tr>
                            <td style="width: 25%;">
                               Upload Eligible Centre List
                            </td>
                            <td style="width: 25%;">
                                <input type='file' id='_file' class="text" accept=".xls">
                            </td>
                            <td style="width: 50%;">
                                <p class="btn submit" id="fileuploadbutton"> <input type="button" id="btnUpload" value="Upload File"></p> 
                                <p class='btn submit'> <input type='button' id='btnReset' name='btnReset' value='Reset' /> </p>
                            </td>
						</tr>
						
						<tr>
                            <td colspan="3">
                                <div class='progress_outer'>
                                    <div id='_progress' class='progress'></div>
                                </div>
                            </td>
                        </tr>
						
						</tr>
						<td nowrap="nowrap">Upload Terms and Condition Document</td>
							<td>:</td>
							<td colspan="3"><input type="file" id="tncdoc"></td>
						</tr>                                    
                                     <tr>
                                       <td>&nbsp;</td>
                                    </tr>
                                     <tr>
                                       <td>&nbsp;</td>
                                    </tr>                          
                        <tr>
                            <td colspan="3" align="right">
                                <p class="btn submit"><input type="submit" id="btnSubmit" name="btnSubmit" value="Launch EOI" /></p>
                            </td>
                        </tr>                        
                    </table>
                    </form>
                    </div>
                    <?php include './include/footer.html'; ?>
                    </div>
                    </body>
					
					<script src="scripts/fileupload.js"></script>
					
                    <script type="text/javascript">
                        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
                        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
                        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
                        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
                        $(document).ready(function () {
//                            if (Mode == 'Delete')
//                            {
//                                if (confirm("Do You Want To Delete This Item ?"))
//                                {
//                                    deleteRecord();
//                                }
//                            }
//                            else if (Mode == 'Edit')
//                            {
//                                fillForm();
//                            }
//
//                            function FillStatus() {
//                                $.ajax({
//                                    type: "post",
//                                    url: "common/cfStatusMaster.php",
//                                    data: "action=FILL",
//                                    success: function (data) {
//                                        $("#ddlStatus").html(data);
//                                    }
//                                });
//                            }//
//                            FillStatus();
//
//                            function deleteRecord()
//                            {
//                                $('#response').empty();
//                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
//                                $.ajax({
//                                    type: "post",
//                                    url: "common/cfOrganization.php",
//                                    data: "action=DELETE&values=" + OrganizationCode + "",
//                                    success: function (data) {
//                                        //alert(data);
//                                        if (data == SuccessfullyDelete)
//                                        {
//                                            $('#response').empty();
//                                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
//                                            window.setTimeout(function () {
//                                                $('#response').empty();
//                                            }, 3000);
//                                            Mode = "Add";
//                                            resetForm("frmOrganization");
//                                        }
//                                        else
//                                        {
//                                            $('#response').empty();
//                                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
//                                        }
//                                        showData();
//                                    }
//                                });
//                            }//
//
//                            function fillForm()
//                            {
//                                $.ajax({
//                                    type: "post",
//                                    url: "common/cfEoiConfig.php",
//                                    data: "action=EDIT&values=" + EoiConfigCode + "",
//                                    success: function (data) {
//                                        //alert(data);
//                                        //alert($.parseJSON(data)[0]['DesignationName']);
//                                        data = $.parseJSON(data);
//                                        txtstartdate.value = data[0].SDate;
//                                        txtenddate.value = data[0].EDate;
//                                        txtPFees.value = data[0].PFees;
//                                        txtCFees.value = data[0].CFees;
//                                    }
//                                });
//                            }
//
//                            function showData() {
//
//                                $.ajax({
//                                    type: "post",
//                                    url: "common/cfOrganization.php",
//                                    data: "action=SHOW",
//                                    success: function (data) {//
//                                        $("#gird").html(data);//
//                                    }
//                                });
//                            }//
//                            showData();

                            $(function() {
                                $( "#txtstartdate" ).datepicker({
                                  changeMonth: true,
                                  changeYear: true
                                });
                              });
                              $(function() {
                                $( "#txtenddate" ).datepicker({
                                  changeMonth: true,
                                  changeYear: true
                                });
                              });                              
                              
                            function FillCourse() {
                                $.ajax({
                                    type: "post",
                                    url: "common/cfCourseMaster.php",
                                    data: "action=FILL",
                                    success: function (data) {
                                        $("#ddlCourse").html(data);
                                    }
                                });
                            }
                             FillCourse();                             

                            $("#btnSubmit").click(function () {
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                                var url = "common/cfEoiConfig.php"; // the script where you handle the form input.
                                //var eclist=$('#eclist').val(); 
                                var tncdoc=$('#tncdoc').val(); 
								//alert(eclist);                             
                                var data;
                                if (Mode == 'Add')
                                {                                    
                                    data = "action=ADD&name="+txtEoiName.value+"&course="+ddlCourse.value+
                                            "&startdate="+txtstartdate.value+"&enddate="+txtenddate.value+
                                            "&pfees="+txtPFees.value+"&cfees="+txtCFees.value+
                                            "&tncdoc="+tncdoc+""; // serializes the form's elements.
                                     //alert(data);
                                }
                                else
                                {
                                    data = "action=UPDATE&code=" + EoiConfigCode + 
                                            "&name="+txtEoiName.value+"&course="+ddlCourse.value+
                                            "&startdate="+txtstartdate.value+"&enddate="+txtenddate.value+
                                            "&pfees="+txtPFees.value+"&cfees="+txtCFees.value+
                                            "&eclist="+eclist+"&tncdoc="+tncdoc+""; // serializes the form's elements.
                                }
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
                                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                            window.setTimeout(function () {
                                                window.location.href = "frmeoiconfig.php";
                                            }, 1000);
                                            Mode = "Add";
                                            resetForm("frmEoiConfig");
                                        }
                                        else
                                        {
                                            $('#response').empty();
                                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                                        }
                                        //showData();
                                    }
                                });
                                return false; // avoid to execute the actual submit of the form.
                            });
                            function resetForm(formid) {
                                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
                            }
                        });
                    </script>
                    </html> 