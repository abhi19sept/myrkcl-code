<?php
$title = "Bio-Matric";
include('header.php');
include('root_menu.php');
?>

<div style="min-height:430px !important;max-height:500px !important">
    <div class="container">
        <div class="panel panel-primary" style="margin-top:36px;">
            <div class="panel-heading">First time capture may take time, so wait after click the button "Click To
                Capture"
            </div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmdeviceregister" id="frmdeviceregister" class="form-inline" role="form" method="post">

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>
                        <div id="errorBox"></div>

                    </div>

                    <div class="container">
                        <div class="col-md-12 form-group">
                            <label for="deviceType">Select Device Make and Model Number<span class="star">*</span></label>
                            <select name="deviceType" class="form-control" onchange="javascript:callAPI(this.value)">
                                <option value="n">- - - Please Select - - -</option>
                                <option value="m">Mantra(Model No. MFS100)</option>
                                <option value="s">Startek(Model No. FM220)</option>
                                <option value="c">Cogent(Model No. CSD200)</option>
                                <option value="t">Tatvik(Model No. TMF20)</option>
                            </select>

                        </div>
                    </div>




                    <div id="showDevice" style="display: none">

                        <div class="container">
                            <div class="col-md-4 form-group">
                                <label for="batch"> Serial No:<span class="star">*</span></label>
                                <input type="text" name="tdSerial" id="tdSerial" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Make:<span class="star">*</span></label>
                                <input type="text" name="tdMake" id="tdMake" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Model:<span class="star">*</span></label>
                                <input type="text" name="tdModel" id="tdModel" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Width:<span class="star">*</span></label>
                                <input type="text" name="tdWidth" id="tdWidth" class="form-control" readonly="true"/>
                            </div>
                        </div>

                        <div class="container">
                            <div class="col-md-4 form-group">
                                <label for="batch"> Height:<span class="star">*</span></label>
                                <input type="text" name="tdHeight" id="tdHeight" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Local MAC:<span class="star">*</span></label>
                                <input type="text" name="tdLocalMac" id="tdLocalMac" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Local IP:<span class="star">*</span></label>
                                <input type="text" name="tdLocalIP" id="tdLocalIP" class="form-control" readonly="true"/>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="batch"> Status:<span class="star">*</span></label>
                                <input type="text" name="txtStatus" id="txtStatus" class="form-control" readonly="true"/>
                            </div>
                        </div>


                        <div class="container" style="margin-top:20px; margin-left:15px;">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>
                        </div>

                    </div>
            </div>
        </div>
    </div>
</form>
<div id="page" data-ng-app="TatvikFPServiceApp" data-ng-controller="TatvikFPServiceCtrl">
    <button class="btn success" data-ng-click="isDeviceConnected()" id="tatvikbtn" style="visibility:hidden">Is Device Connected</button>
<!--    <input data-ng-model="outputData" style="visibility:hidden"/>
    <p data-ng-bind="outputData" id="outputData"></p>-->
</div>
</div>
<?php
include('footer.php');
include 'common/message.php';
include 'common/modals.php';
$mac = explode(" ", exec('getmac'));
?>
</body>

<script src="js/mfs100.js"></script>
<script src="js/star.js"></script>
<script src="js/cogent.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>


<script type="text/javascript">
        var localIP = "<?php echo getHostByName(getHostName()); ?>";
        var localMAC = "<?php echo str_ireplace('-', '', $mac[0]); ?>";
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

        $(document).ready(function () {
            var quality = 60; //(1 to 100) (recommanded minimum 55)
            var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )

            $("#btnSubmit").click(function () {
                //alert("hhkj");
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfbiomatricregister.php"; // the script where you handle the form input.
                var data;
                var forminput = $("#frmdeviceregister").serialize();

                data = "action=REGISTER&" + forminput;
                //data = "action=UPDATE&code=" + Examcode + "&name=" + txtAffilate.value + "&Affilate=" + ddlAffilate.value + "&Course=" + ddlCourse.value + "&Description=" + txtDescription.value + ""; // serializes the form's elements.

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data) {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate || data == "Device has been successfully registered") {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span>&nbsp;&nbsp;<span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                window.location.href = "frmbiomatricdeviceregistration.php";
                            }, 1000);
                            Mode = "Add";
                            resetForm("frmdeviceregister");
                        } else {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        //showData();
                    }
                });

                return false; // avoid to execute the actual submit of the form.
            });

        });

        function initializeMantra() {
            $("#wait_modal").modal("show");
            var res = GetMFS100Info();

            if (res.httpStaus) {
                document.getElementById('txtStatus').value = res.data.ErrorDescription;
                if (res.data.ErrorCode == "0" && res.data.DeviceInfo.Make != "NONE") {
                    $("#wait_modal").modal("hide");
                    $("#showDevice").css("display", "block");
                    document.getElementById('tdSerial').value = res.data.DeviceInfo.SerialNo;
                    document.getElementById('tdMake').value = res.data.DeviceInfo.Make;
                    document.getElementById('tdModel').value = res.data.DeviceInfo.Model;
                    document.getElementById('tdWidth').value = res.data.DeviceInfo.Width;
                    document.getElementById('tdHeight').value = res.data.DeviceInfo.Height;
                    document.getElementById('tdLocalMac').value = res.data.DeviceInfo.LocalMac;
                    document.getElementById('tdLocalIP').value = res.data.DeviceInfo.LocalIP;
                } else {
                    $("#wait_modal").modal("hide");
                    $("#error_modal").modal("show");
                    $("#showDevice").css("display", "none");
                }
            } else {
                $("#wait_modal").modal("hide");
                //alert(res.err);
            }
            return false;
        }

        function callAPI(x) {
            $("#showDevice").css("display", "none");
            if (x !== "n") {
                switch (x) {
                    case "m":
                        initializeMantra();
                        break;

                    case "s":
                        initializeStartek();
                        break;

                    case "c":
                        initializeCogent();
                        break;

                    case "t":
                        
                        $("#tatvikbtn").click();

                        break;

                    default:
                        break;
                }
            } else {
                $("#showDevice").css("display", "none");
            }
        }
</script>
<script>

    var app = angular.module("TatvikFPServiceApp", []);
    var referenceTemplateData = null;
    var claimedTemplateData = null;
    var dom;
    var devicestatustatvik = "";
    var devicetatvikSerialNo = "";
    var devicetatvikModel = "";
    var devicetatvikMake = "";

    app.controller("TatvikFPServiceCtrl", function ($scope, $http)
    {
        //alert("hi2");
        $scope.isDeviceConnected = function ()
        {
            $scope.outputData = "";
            $http({
                method: 'ISDEVICECONNECTED',
                url: 'https://127.0.0.1:31000'
            }).
                    then(function (response)
                    {
                        $scope.outputData = response.data;
                        //alert(response.data);
                        var parser = new DOMParser();
                        dom = parser.parseFromString(response.data, "text/xml");
                        //alert(dom.getElementsByTagName("DeviceStatus")[0].childNodes[0].nodeValue);
                        devicestatustatvik = dom.getElementsByTagName("DeviceStatus")[0].childNodes[0].nodeValue;
                        //alert(devicestatustatvik);
                        if (devicestatustatvik == "Device connected")
                        {

                            $scope.outputData = "";
                            $http({
                                method: 'DEVICEINFO',
                                url: 'https://127.0.0.1:31000'
                            }).
                                    then(function (response)
                                    {

                                        $("#showDevice").css("display", "block");
                                        $scope.outputData = response.data;
                                        var parser = new DOMParser();
                                        dom = parser.parseFromString(response.data, "text/xml");
                                        devicetatvikSerialNo = dom.getElementsByTagName("SerialNo")[0].childNodes[0].nodeValue;
                                        devicetatvikModel = dom.getElementsByTagName("DeviceModel")[0].childNodes[0].nodeValue;
                                        devicetatvikMake = dom.getElementsByTagName("DeviceMake")[0].childNodes[0].nodeValue;
                                        devicetatvikStatus = dom.getElementsByTagName("ErrMsg")[0].childNodes[0].nodeValue;
                                        //alert("1");
                                        //$scope.addtatvikdetails(devicetatvikSerialNo, devicetatvikModel, devicetatvikMake);
                                         
                                        document.getElementById('tdSerial').value = devicetatvikSerialNo;
                                        document.getElementById('tdMake').value = devicetatvikMake;
                                        document.getElementById('tdModel').value = devicetatvikModel;
                                        document.getElementById('tdWidth').value = 0;
                                        document.getElementById('tdHeight').value = 0;
                                        document.getElementById('tdLocalMac').value = localMAC;
                                        document.getElementById('tdLocalIP').value = localIP;
                                        document.getElementById('txtStatus').value = devicetatvikStatus;
                                    });

                        } else {
                            alert("Device Not Connected Please attach device");
                        }
                    });


        }
    });

</script>

</html>