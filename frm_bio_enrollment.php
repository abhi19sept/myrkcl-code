<?php
$title = "User Details";
include ('header.php');
include ('root_menu.php');
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<style type="text/css">

form > div.container {
   filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
}

    .modal-open .container-fluid, .modal-open .container{
        filter: blur(0px) !important;
        -webkit-filter:blur(0px) !important;
        -moz-filter:blur(0px) !important;
        -o-filter:blur(0px) !important;
    }
</style>
<div style="min-height:450px !important;max-height:1500px !important">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Bio Enrollment List</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="frmbioenrollments" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div> 
                        <div class="container">
                            <div id="new-visit-btn" class="text-left">
                                <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Register New" style="margin-left:-25px"/>
                            </div>
                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <div id="grid" name="grid" style="margin-top:35px;"> </div>
                    </form>
            </div>
        </div>   
    </div>

<div id="set_new_visit" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 900;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Bio-Metric Enrollment</h4>
            </div>
            <div class="modal-body">
                <form name="frmbioenrollment" id="frmbioenrollment" class="form-inline" role="form" enctype="multipart/form-data">     

                        <div class="container">
                            <div id="frmvisit-response"></div>

                        </div> 
                        <div class="container">
                            <div class="col-sm-4 form-group"  id="enrollas"> 
                                <label for="edistrict">Enroll As 
                                :<span class="star">*</span></label>
                                <select id="employee_type" name="employee_type" class="form-control" >
                                 <option value="">Select Type</option>
                                </select>
                                <div id="dpo-list"></div>
                                <div id="itgk-list"></div>
                                <div id="rsp-list"></div>
                            </div>
                            <div class="col-sm-4 form-group"  id="non-printable"> 
                                <label for="edistrict">Full Name 
                                :<span class="star">*</span></label>
                                <input type="text" data-mask="charonly" placeholder="Enter full name" class="form-control" name="fullname" id="fullname"> 
                            </div>
                            <div class="col-sm-4 form-group"  id="non-printable"> 
                            <label for="dateFrom">Mobile:<span class="star">*</span></label>
                                    <input type="text" maxlength="10" data-mask="adhar" placeholder="Mobile number" class="form-control" name="mobile" id="mobile"> 
                            </div>
                            <div class="col-sm-4 form-group"  id="otpmobile-fld"> 
                                <label for="dateFrom">OTP:<span class="star">*</span></label><br />
                                <input type="text" style="width: 80px;" maxlength="6" data-mask="six-digits" placeholder="OTP" size="5" class="form-control" name="otpmobile" id="otpmobile">
                                <br />
                                <span id="mob_resend_otp"><a href="#" id="mob_resend_otp">Resend</a></span>
                            </div>
                        </div>
                        <div class="container">
                            <div class="col-sm-4 form-group"  id="non-printable"> 
                                <label for="edistrict">Gender 
                                :<span class="star">*</span></label>
                                <select id="gender" name="gender" class="form-control" >
                                 <option value="">Select</option>
                                 <option value="Male">Male</option>
                                 <option value="Female">Female</option>
                                </select>    
                            </div>
                            <div class="col-md-4 form-group"><center>     
                                <label for="dateFrom">Bio Impression:<span class="star">*</span></label></center>
                             </div>

                             <div class="col-md-4 form-group">     
                                <label for="dateFrom">Email:<span class="star">*</span></label>
                                    <input type="text" placeholder="Email id" class="form-control" maxlength="100" name="email_id" id="email_id"> 
                                    <span id="emailerr" class="star small"></span>
                             </div>

                             <div class="col-sm-4 form-group"  id="otpemail-fld"> 
                                <label for="dateFrom">OTP:<span class="star">*</span></label><br />
                                <input type="text" style="width: 80px;" size="5" maxlength="6" data-mask="six-digits" placeholder="OTP" class="form-control" name="otpemail" id="otpemail"> 
                                <br />
                                <span id="mail_resend_otp"><a href="#" id="mail_resend_otp">Resend</a></span>
                            </div>

                        </div> 

                        <div class="container">
                            <div class="col-md-4 form-group">
                                <label for="dateFrom">Select Bio-Metric Device:<span class="star">*</span></label>
                                <select name="deviceType" id="deviceType" class="form-control biomval">
                                    <option value="">- - - Please Select - - -</option>
                                    <option value="mantra">Mantra (MFS100)</option>
                                    <option value="startek">Startek (FM220)</option>
                                    <option value="tatvik1">Tatvik (TMF20 )</option>
                                    <!--<option value="cogent">Cogent (CSD200)</option>-->
                                </select>
                                <span id="deviceerr" class="star small"></span>
                             </div>
                             <div class="col-md-4 form-group">     
                                <center><img id="imgFinger" name="imgFinger" class="form-control" style="width:80px; height:107px; margin-top: -48px;"/><br /><span class="star" id="match_bio"></span><br />
                                <span id="picerr" class="star small"></span>
                            </center>
                            <div style="display: none;"><img id="imgFinger_bio" name="imgFinger" class="form-control" style="width:80px; height:107px; margin-top: -48px;"/><br /><span class="star" id="match_bio"></span></div>
                             </div>
                             <div class="col-md-4 form-group">
                                <p>&nbsp;</p>
                                <input type="button" id="bio_impression" class="bio_impression" value="Capture" name="bio_impression" />
                                <span class="star bioerr"></span>
                                <input type="hidden" class="biomval" name="txtStatus" id="txtStatus"/>
                                <input type="hidden" class="biomval" name="tdLocalIP" id="tdLocalIP"/>
                                <input type="hidden" class="biomval" name="tdLocalMac" id="tdLocalMac"/>
                                <input type="hidden" class="biomval" name="tdHeight" id="tdHeight"/>
                                <input type="hidden" class="biomval" name="tdWidth" id="tdWidth"/>
                                <input type="hidden" class="biomval" name="tdModel" id="tdModel"/>
                                <input type="hidden" class="biomval" name="tdMake" id="tdMake"/>
                                <input type="hidden" class="biomval" name="tdSerial" id="tdSerial"/>

                                <input type="hidden" name="txtStatus" id="txtStatus"/>
                                <input type="hidden" name="txtQuality" id="txtQuality"/>
                                <input type="hidden" name="txtNFIQ" id="txtNFIQ"/>
                                <input type="hidden" name="txtIsoTemplate" id="txtIsoTemplate"/>
                                <input type="hidden" name="txtIsoImage" id="txtIsoImage"/>
                                <input type="hidden" name="txtRawData" id="txtRawData"/>
                                <input type="hidden" name="txtWsqData" id="txtWsqData"/>
                                <input type="hidden" name="matched_bio" id="matched_itgk"/>
                                <input type="hidden" name="flag" id="flag"/>
                             </div>
                        </div> 
                        <div class="container">
                            <div class="col-md-4 form-group">&nbsp;</div>
                            <div class="col-md-4 form-group"><p>&nbsp;</p><center><input type="button" name="registerBtn" id="registerBtn" class="btn btn-primary" value="Register"/> <input type="reset" name="resetBtn" id="resetBtn" class="btn btn-primary" value="Reset"/></center></div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); 
    include 'common/modals.php';
    $mac = explode(" ", exec('getmac'));
?>
<style>
    #errorBox{
        color:#F00;
    }
    .aslink {
        cursor: pointer;
    }
</style>

<script src="js/mfs100.js"></script>
<script src="js/star.js"></script>
<script src="js/cogent.js"></script>
<script src="js/tatvik2.js"></script>
<script src="js/visitcontroller2.js"></script>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    var quality = 60; //(1 to 100) (recommanded minimum 55)
    var timeout = 10; // seconds (minimum=10(recommanded), maximum=60, unlimited=0 )
    var localIP = "<?php echo getHostByName(getHostName());?>";
    var localMAC = "<?php echo str_ireplace('-', '', $mac[0]);?>";

    function listvisits() {
        $("#grid").empty();
        $('#response').empty();
        $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=bioenrolledlist",
            success: function (data) {
                $('#response').empty();
                $("#grid").html(data);
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'print'
                    ],
                    scrollY: 400,
                    scrollCollapse: true,
                    paging: false
                });
            }
        });
    }

    function validateEmail(email) {
        if (email=='') return true;
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var isvalid = regex.test(email);
        $('#emailerr').html('');
        if (!isvalid) {
            $('#emailerr').html('Please enter valid email id');
        }
        return isvalid;
    }

    function validateCapture() {
        $('#picerr').empty();
        $('#deviceerr').empty();
        if (tdSerial.value == '') {
            $('#deviceerr').html('Device not detucted.');
        } else if (txtIsoTemplate.value == '') {
            $('#picerr').html('Please capture the bio-impression.');
        } else {
            validateImpression();
        }
    }

    function validateImpression() {
        //$("#matching_records").modal("show");
        var isvalid = 1;
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=bioenrolledpics",
            success: function (data) {
                var isvalid = 1;
                if (data == '1') {
                    $("#matching_records").modal("hide");
                    var isvalid = 0;
                } else if (data == '0') {
                    $("#matching_records").modal("hide");
                    alert('Oops!! something went wrong please retry after login.');
                } else {
                    isvalid = matchISOTemplates(data, 0, 'bio');
                }
                if (!isvalid && isvalid != 'hold_on') {
                    registerBioImpression(isvalid);
                }
            }
        });
    }

    function registerBioImpression(rowid) {
        $("#matching_records").modal("hide");
        $("#please_wait").modal("show");
        var formdata = $("#frmbioenrollment").serialize();
        var data = "action=bioRegister&rowid=" + rowid + "&" + formdata; // serializes the form's elements.
        $.ajax({
            type: "POST",
            url: "common/cfvisitmaster.php",
            data: data,
            success: function (data) {
                $("#please_wait").modal("hide");
                if (data == SuccessfullyInsert || data == SuccessfullyUpdate) {
                    setDeviceInfoEmpty();
                    $("#set_new_visit").modal("hide");
                    $("#successfully_submitted").modal("show");
                } else {
                    $("#errorText").empty();
                    $("#errorText").html(data);
                    $("#errorModal_Custom").modal("show");
                }
            }
        });
    }

    function performAction(eObj) {
        var act = eObj.id.split('-');
        var effOn = '#biostatus_' + act[1];
        $(effOn).empty();
        $(effOn).append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
        $.ajax({
            type: "post",
            url: "common/cfvisitmaster.php",
            data: "action=changeEnrollStatus&elemId=" + eObj.id,
            success: function (data) {
                $(effOn).html(act[0] + 'd');
                $('#' + eObj.id).remove();
            }
        });
    }

    function fillEnrollas() {
        $.ajax({
            type: "POST",
            url: "common/cfvisitmaster.php",
            data: "action=enrollas",
            success: function (data) {
                $('#employee_type').html(data);
            }
        });
    }

    function getDPOList() {
        $.ajax({
            type: "POST",
            url: "common/cfvisitmaster.php",
            data: "action=dpolist",
            success: function (data) {
                $('#dpo-list').html(data);
            }
        });
    }

    function getITGKList() {
        $.ajax({
            type: "POST",
            url: "common/cfvisitmaster.php",
            data: "action=itgklist",
            success: function (data) {
                $('#dpo-list').html(data);
            }
        });
    }

    function getRSPList() {
        $.ajax({
            type: "POST",
            url: "common/cfvisitmaster.php",
            data: "action=rsplist",
            success: function (data) {
                $('#dpo-list').html(data);
            }
        });
    }

    function sendMobileOTP(mob, email) {
        if (mob != '') {
            $("#mobile").attr('readonly','readonly');
            $("#otpmobile").val('');
            $("#otpmobile-fld").show();
        }
        if (email != '') {
            $("#email_id").attr('readonly','readonly');
            $("#otpemail").val('');
            $("#otpemail-fld").show();
        }
        if (mob != '' || email != '') {
            $.ajax({
                type: "POST",
                url: "common/cfvisitmaster.php",
                data: "action=setmobileotp&email=" + email + "&mobile=" + mob,
                success: function (data) {
                    if (data == 'nootp') {
                        resetOTP();
                    }
                }
            });
        }
    }

    function resetOTP() {
        resetMobileOTP();
        resetEmailOTP();
    }

    function resetMobileOTP() {
        $("#mobile").removeAttr('readonly');
        $("#otpmobile-fld").hide();
    }

    function resetEmailOTP() {
        $("#email_id").removeAttr('readonly');
        $("#otpemail-fld").hide();
    }

    $(document).ready(function () {
        listvisits();
        resetCapture('bio');
        fillEnrollas();
        $("#otpmobile-fld").hide();
        $("#otpemail-fld").hide();

        $("#btnShow").click(function () {
            $("#resetBtn").click();
            $("#set_new_visit").modal("show");
        });
        
        $("#mob_resend_otp").click(function () {
            sendMobileOTP(mobile.value, '');
        });
        $("#mail_resend_otp").click(function () {
            sendMobileOTP('', email_id.value);
        });

        $("#employee_type").change(function () {
            $("#dpo-list").empty();
            $("#itgk-list").empty();
            $("#rsp-list").empty();
            if (this.value == 'DPO') {
                getDPOList();
            } else if (this.value == 'Faculty' || this.value == 'Coordinator' || this.value == 'Counsler') {
                getITGKList();
            } else if (this.value == 'Employee' || this.value == 'Owner') {
                getRSPList();
            }
        });
        $("#deviceType").change(function () {
            resetCapture('bio');
            getDeviceInfo(this.value);
        });
        $("#bio_impression").click(function () {
            initializeCapture(deviceType.value);
        });
        $("#resetBtn").click(function () {
            setDeviceInfoEmpty();
            resetOTP();
        });

        $("#email_id").blur(function () {
            validateEmail(this.value);
        });

        $('.close').click(function () {
            resetOTP();
        });

        $("#mobile").change(function () {
            if ($(this).valid()) {
                sendMobileOTP(this.value, '');
            }
        });

/**        $("#email_id").change(function () {
            if ($(this).valid()) {
                sendMobileOTP('', this.value);
            }
        });
**/
        $("#registerBtn").click(function () {
            var ifvalid = $('#frmbioenrollment').valid();
       //     var isvalidemail = validateEmail($('#email_id').val());
     //       if(ifvalid && isvalidemail) {
	  if(ifvalid) {
                validateCapture();
            }
        });

        $("#successfully_submitted").on('click', '.btn-default', function () {
            window.location.reload();
        });

        $("#successfully_submitted").on('click', '.close', function () {
            window.location.reload();
        });

        $("#grid").on('click', '.aslink', function () {
            performAction(this);
        });

    });
   

</script>
<script src="js/frmbioenrollment_validation.js" type="text/javascript"></script>
</html>