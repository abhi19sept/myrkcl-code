<?php
$title = "Update Mobile Number";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Update'</script>";
}
?>
<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Update Mobile Number</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="frmUpdateMobileNumber" id="frmUpdateMobileNumber" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="cc">Enter Center Code:<span class="star">*</span></label>
                            <input type="text" class="form-control" maxlength="8" onkeypress="javascript:return validUserName(event);" name="ITGK_Code" id="ITGK_Code" placeholder="Enter Center Code">
                            
                        </div>

                        <div class="col-sm-4 form-group">                                  
                            <input type="button" name="btnShow" id="btnShow" class="btn btn-primary" value="Show Details" style="margin-top:25px"/>    
                        </div>
                    </div>

                </form>
                
                 
                    <div id="main-content" style="display:none">

                        
                        <div class="panel panel-info" id="impreg">
                            <div class="panel-heading">Registerede IT-GK Details</div>
                            <div class="panel-body">

                                <div class="container">
                                    <div class="col-sm-4"> 
                                        <label for="edistrict">IT-GK Name:<span class="star">*</span></label>
                                        <input type="text" maxlength="10" class="form-control" name="txtCenterName" id="txtCenterName" readonly="true"/>
                                        <input type="hidden" class="form-control" maxlength="10" name="txtOrgCode" id="txtOrgCode"/>
                                    </div>
                                    <div class="col-sm-2"> 
                                        <label for="edistrict">District:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtDistrictName" id="txtDistrictName" readonly="true"/>
                                        <input type="hidden" class="form-control" maxlength="10" name="txtDistrictCode" id="txtDistrictCode"/>
                                    </div>
                                     <div class="col-sm-2"> 
                                        <label for="edistrict">Current Tehsil:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="txtTehsilName" id="txtTehsilName" readonly="true"/>
                                        <input type="hidden" class="form-control" maxlength="10" name="txtTehsilCode" id="txtTehsilCode"/>
                                    </div>
                                    <div class="col-sm-2"> 
                                        <label for="edistrict">Address:<span class="star">*</span></label>
                                        <input type="text" class="form-control" name="address" id="address" readonly="true"/>
                                        <input type="hidden" class="form-control" name="address" id="address"/>
                                    </div>

                                </div>
                                <div class="container" style="padding-top: 22px;">
                                    <div class="col-sm-4"> 
                                        <label for="edistrict">Email:<span class="star">*</span></label>
                                        <input type="text" maxlength="10" class="form-control" name="User_EmailId" id="User_EmailId" readonly="true"/>
                                        <input type="hidden" class="form-control" maxlength="10" name="User_EmailId" id="User_EmailId"/>
                                    </div>
                                    <div class="col-sm-2"> 
                                        <label for="edistrict">Mobile:<span class="star">*</span></label>
                                        <input type="text" class="form-control" maxlength="10" name="ITGK_MobileNo" id="ITGK_MobileNo" readonly="true"/>
                                        <input type="hidden" class="form-control" maxlength="10" name="ITGK_MobileNo" id="ITGK_MobileNo"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                        
                    </div>
               
                
                
                <form class="form-inline" role="form" name="Update_mob_form" id="Update_mob_form">

                    <div class="container" id="main-content2" style="display:none">
                        <div class="container">
                            <div id="response2"></div>

                        </div>        
                        <div id="errorBox2"></div>
                        <div class="col-sm-4 form-group">     
                            <label for="cc">Enter Mobile Number:<span class="star">*</span></label>
                            <input type="text" class="form-control" minlength="10" maxlength="10" onkeypress="javascript:return allownumbers(event);" name="User_MobileNo" id="User_MobileNo" placeholder="Enter Mobile number">
                            
                        </div>
                        <div class="col-sm-4 form-group">     
                            <label for="cc">&nbsp;<span class="star">&nbsp;</span></label>
                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Update Mobile Number" style="margin-top:25px"/>
                            
                        </div>
                         
                    </div>

                </form>
            </div>
        </div>   
    </div>
</div>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/updatemobilenumbervalidation.js" type="text/javascript"></script>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    function validUserName(e){
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("^[ 0-9A-Za-z_@.-]*$");

            if (key === 8 || key === 0 || key === 32) {
                keychar = "a";
            }
            return reg.test(keychar);
        }
    function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
        
        
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    
    $(document).ready(function () {



        $("#btnShow").click(function () {
        if ($("#frmUpdateMobileNumber").valid()) 
            {
                $('#response').empty();
                var ITGK_Code = $("#ITGK_Code").val();
                
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                type: "post",
                url: "common/cfUpdateMobileNumber.php",
                data: "action=DETAILS&values=" + ITGK_Code,
                success: function (data)
                {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter a valid Center Code" + "</span></p>");
                    } 
                    else if (data == "NORECD") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span style='color: red;font-weight: bold;'>" + " " + ITGK_Code + "  This ITGK is not found in our record." + "</span></p>");
                    } 
                    else {
                        data = $.parseJSON(data);
                        txtCenterName.value = data[0].orgname;
                        txtOrgCode.value = data[0].orgcode;
                        address.value = data[0].address;
                        txtDistrictName.value = data[0].districtname;
                        txtTehsilName.value = data[0].tehsilname;
                        ITGK_MobileNo.value = data[0].ITGK_MobileNo;
                        User_EmailId.value = data[0].User_EmailId;

                        //filltehsil();

                        $("#main-content").show();
                        $("#main-content2").show();
                        $('#ITGK_Code').attr('readonly', true);
                        $("#btnShow").hide();

                    }

                }
            });
            }
            return false; // avoid to execute the actual submit of the form.
        });

        $("#btnSubmit").click(function () {
            if ($("#frmUpdateMobileNumber").valid()) 
                {
                    $('#response2').empty();
                    $('#response2').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                    var ITGK_Code = $("#ITGK_Code").val();
                    var User_MobileNo = $("#User_MobileNo").val();
                    $.ajax({
                        type: "POST",
                        url: "common/cfUpdateMobileNumber.php",
                        data: "action=UPDATEMOBILE&ITGK_Code=" + ITGK_Code + "&User_MobileNo=" + User_MobileNo,
                        success: function (data)
                        {
                            if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                            {
                                $('#response2').empty();
                                $('#response2').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                                //BootstrapDialog.alert("<div class='alert-error'><span><img src=images/correct.gif width=10px /></span><span>&nbsp; Mobile Number Updated Successfully.</span>");
                                window.setTimeout(function () {
                                    window.location.href = "frmupdatemobilenumber.php";
                                }, 2000);

                               // Mode = "Add";
                                //resetForm("frmUpdateLocationDetails");
                            } else
                            {
                                $('#response2').empty();
                                $('#response2').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span  style='color: red;font-weight: bold;'>" + data + "</span></p>");
                            }

                        }
                    });
                }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

</html>