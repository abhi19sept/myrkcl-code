<?php
$title = "Link Aadhar";
include ('header.php');
include ('root_menu.php');

echo "<script>var UserLoginID='" . $_SESSION['User_LoginId'] . "'</script>";
echo "<script>var User_UserRole='" . $_SESSION['User_UserRoll'] . "'</script>";
if($_SESSION['User_UserRoll']=='7' || $_SESSION['User_UserRoll']=='28' || $_SESSION['User_UserRoll']=='1') {
?>
<style>
 .modal {z-index: 1050!important;} 
 #turmcandition{
         background-color: whitesmoke;
         text-align: justify;
         padding: 10px;
 }
 .noturm{
         margin-left: 10px !important;
 }
 .yesturm{
         margin-right: 15px;
 }
 </style>
<div style="min-height:430px !important;">
    <div class="container"> 
        <div class="panel panel-primary" style="margin-top:36px !important;">  
            <div class="panel-heading">Link Aadhar</div>
            <div class="panel-body">

                <form name="frmAdmissionSummary" id="frmAdmissionSummary" class="form-inline" role="form" enctype="multipart/form-data">
                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                            <div id="responsecertificate" style="width: 90%;"></div>

                        </div>        
                        <div id="errorBox"></div>
                        <div class="container">
                            <div class="col-md-4 form-group"> 
                                <label for="course">Select Course:<span class="star">*</span></label>
                                <select id="ddlCourse" name="ddlCourse" class="form-control" required="required">

                                </select>
                            </div> 

                            <div class="col-md-4 form-group">     
                                <label for="batch"> Select Batch:<span class="star">*</span></label>
                                <select id="ddlBatch" name="ddlBatch" class="form-control" required="required">
                                    
                                </select>
                            </div> 
                            <?php
                                if($_SESSION['User_UserRoll']=='28' || $_SESSION['User_UserRoll']=='1'){
                                ?>
                                <div class="col-md-4 form-group">     
                                    <label for="batch"> Enter Learner Code:<span class="star">*</span></label>
                                    <input type="text" id="txtlcode" name="txtlcode" class="form-control" required="required">
                                </div> 
                                <?php    
                                }
                            ?>
                        </div>

                        <div class="container">
                            <div class="col-md-4 form-group">     
                                <input type="submit" name="btnSubmitlist" id="btnSubmitlist" class="btn btn-primary" value="View"/>    
                            </div>
                        </div>
                        <div id="grid" style="margin-top:5px; width:94%;"> </div>
<!--                        <div id="view" style="margin-top:5px; width:94%;"> </div>-->

                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>
<link href="css/popup.css" rel="stylesheet" type="text/css">
<!-- Modal -->
<div id="myModalupdate" class="modal" style="padding-top:150px !important">
            
  <div class="modal-content" style="width: 90%;">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h6>Update Your Aadhar Number</h6>
    </div>
      <div class="modal-body" style="max-height: 800px;" id='formhtml'>
          <form name="frmCourseBatch" id="frmCourseBatch" class="form-horizontal" role="form"
                    enctype="multipart/form-data" style="margin: 25px 0 40px 0;">  
                  <div class="form-group">
                      <div class="col-sm-offset-3 col-sm-6" id="responsesendotp"></div>
                  </div>

                  <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Enter Aadhaar Number:</label>
                      <div class="col-sm-8">
                          <input class="form-control valid" name="txtACno" id="txtACno" placeholder="Aadhaar Card Number" aria-invalid="false"  maxlength="12"  onkeypress="javascript:return allownumbers(event);"  type="text">
                      </div>
                  </div>
<!--                  <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Pin Code Number:</label>
                      <div class="col-sm-6">
                          <input class="form-control valid" name="txtPINno" id="txtPINno" placeholder="Pin Number" aria-invalid="false" maxlength="6"  onkeypress="javascript:return allownumbers(event);"  type="text">
                      </div>
                  </div>-->
                  <div class="form-group" id="otpTxt" name="otpTxt"  style=" display: none;">
                      <label for="inputEmail3" class="col-sm-3 control-label">OTP:</label>
                      <div class="col-sm-8">
                          <input class="form-control valid" name="txtotpTxt" id="txtotpTxt" placeholder="OTP" aria-invalid="false" maxlength="9"  onkeypress="javascript:return allownumbers(event);"  type="text">
                      </div>
                  </div>
                  <div class="form-group" >
                      <div class="col-sm-3"></div>
                      <div class="col-sm-8" id="turmcandition">
                          <p>I,the holder of above mentioned Aadhar Number, hereby give my consent to RKCL to obtain my Aadhar Number, Name and Fingerprint/Iris for authentication with UIDAI. I have no objection using my identity and biometric information for validation with Aadhar(CIDR) only for the purpose of authentication will be used for generation and issuance of Digitally Signed RS-CIT certificate.</p>
                          <div><span class="yesturm">I Agree</span> <input type="checkbox" id="yes" name="fooby[1][]" /> Yes
<!--                              <input class="noturm" type="checkbox" id="no" name="fooby[1][]" /> No-->
                          </div>
                      </div>
                      <div class="col-sm-2"></div>
                  </div>
                  <div class="form-group last">
                      <div class="col-sm-offset-3 col-sm-6">
                          <input type="button" name="btnOTPSubmit" style="display: none;" id="btnOTPSubmit" class="btn btn-primary" value="Submit"/>
                          <input type="button" name="btnSubmit"   style="display: none;" id="btnSubmit" class="btn btn-primary valid" value="Submit"/>
                      </div>
                  </div>   
                  <div id="FinalResult" class="form-group"></div>
                  <input class="form-control valid"  name="txtTxn" id="txtTxn" type="hidden">
                  <input class="form-control valid" name="txtStatus" id="txtStatus" type="hidden">
                  <input class="form-control" name="txtCourse" id="txtCourse" type="hidden">
                  <input class="form-control" name="txtBatch" id="txtBatch" type="hidden">
                  <input class="form-control" name="txtLcode" id="txtLcode" type="hidden">
              </form>		
    </div>
  </div>
</div>

<div id="myModalEresponse" class="modal" style="padding-top:180px !important;">         
  <div class="modal-content" style="width: 60%;border-radius: 15px; text-align: center;">
      <div class="modal-body" style="min-height: 175px;padding: 0 16px;" id="Eresponse">
          		
      </div>
      <button class="btn btn-primary closeeresponse" style="margin-bottom: 15px;">Close</button>
  </div>
</div>


<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>
</body>

<script type="text/javascript">
    
    function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
    
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
      
        $('#yes').change(function() {
            if($(this).is(":checked")) {
                var returnVal = confirm("Are you sure?");
                if(returnVal){
                    $("#btnOTPSubmit").css("display", "block");
                }else{
                    $("#btnOTPSubmit").css("display", "none");
                }
                
            }else{
                $("#btnOTPSubmit").css("display", "none");
            }           
        });
        $("#btnOTPSubmit").click(function () {
            if(txtACno.value == '')
                {  $('#responsesendotp').empty();
                   $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter Aadhaar Number." + "</span></p>");
                }
            else{                  
				$('#responsesendotp').empty();
				$('#responsesendotp').append("<p class='error'><span><img src=images/ajax-loader.gif width=40px /></span><span>Please wait we are fetching and verifying your Aadhaar Data.</span></p>");		
				$.ajax({
					type: "post",
					url: "common/cfLinkAadhar.php",			
					data: "action=generateOTP&txtACno=" + txtACno.value + "",
					success: function (data)
						{
							data = $.parseJSON(data);				
							   $('#responsesendotp').empty(); 
								if(data.status == "n")
								{   
									$('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Invalid Aadhaar Number. Please check your Aadhaar Number." + "</span></p>");
								}
								else
								{           // console.log(data);
												//alert(data.txn);
											$('#responsesendotp').append("<p class='sucess'><span><img src=images/correct.gif width=10px /></span><span>" + " We have sent OTP on your register Aadhaar Number. Please enter OTP as received on Mobile." + "</span></p>");
											txtTxn.value = data.txn;
											txtStatus.value = data.status;
											$("#btnOTPSubmit").hide();
											$("#otpTxt").show();
											$("#btnSubmit").show();                                               
											$("#txtACno").attr("readonly", "readonly"); 
											$("#txtPINno").attr("readonly", "true");
								}
						}
				});	                  
			}            	
          });
          
        $("#btnSubmit").click(function () {
            if(txtotpTxt.value == '')
                {  $('#responsesendotp').empty();
                   $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "   Please Enter OTP." + "</span></p>");
                }            
            else{                  
            $('#responsesendotp').empty();
            $('#responsesendotp').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");		
            $("#txtotpTxt").attr("readonly", "true");
		$.ajax({
			type: "post",
			url: "common/cfLinkAadhar.php",
			data: "action=authenticateOTP&txtACno=" + txtACno.value + "&txtTxn=" + txtTxn.value + "&txtStatus=" + txtStatus.value + "&txtotpTxt=" + txtotpTxt.value + "&txtCourse=" + txtCourse.value + "&txtBatch=" + txtBatch.value + "&txtLcode=" + txtLcode.value + "",
			success: function (data)
				{                                    
					  $('#responsesendotp').empty();
					   if(data == SuccessfullyInsert || data == SuccessfullyUpdate){
						  $('#responsesendotp').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
							window.setTimeout(function () {
								window.location.href = "frmlinkaadhar.php";
							}, 3000);

					  }else{
						 $('#responsesendotp').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>"); 
					  }
					  //$("#FinalResult").html(data);
					
					}
		});	                  
	}            	
});
        
        function FillCourse() {           
            $.ajax({
                type: "post",
                url: "common/cfLinkAadhar.php",
                data: "action=FILLLinkAadharCourse",
                success: function (data) {                    
                    $("#ddlCourse").html(data);
                }
            });
        }
        FillCourse();

        $("#ddlCourse").change(function () {
            var selCourse = $(this).val();            
            $.ajax({
                type: "post",
                url: "common/cfLinkAadhar.php",
                data: "action=FILLAdmissionBatch&values=" + selCourse + "",
                success: function (data) {
                    $("#ddlBatch").html(data);
                }
            });
        });
		
		$("#grid").on("click",".link_aadhar",function(){
			var edit_id = $(this).attr("id");
			var mybtnid = "myBtn_"+edit_id;
			var modal = document.getElementById('myModalupdate');
			var btn = document.getElementById(mybtnid);
			var span = document.getElementsByClassName("close")[0];
				modal.style.display = "block";
				span.onclick = function() { 
					modal.style.display = "none";
                                        window.location.href = "frmlinkaadhar.php";
                                        $("#btnOTPSubmit").show();
                                        $("#otpTxt").hide();
                                        $("#btnSubmit").hide();                                               
                                        $("#txtACno").attr("readonly", false); 					
				}

			$('#responseupdate').empty();
            $('#responseupdate').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
			var url = "common/cfLinkAadhar.php"; // the script where you handle the form input.
			var data;
			data = "action=EDITFILLAADHAR&lcode=" + edit_id +""; // serializes the form's elements.				 
			$.ajax({
				type: "POST",
				url: url,
				data: data,
				success: function (data)
				{   $('#responseupdate').empty();
					data = $.parseJSON(data);					
					txtACno.value = data[0].Admission_UID;
					txtBatch.value = data[0].Admission_Batch;
					txtLcode.value = data[0].Admission_LearnerCode;
					txtCourse.value = data[0].Admission_Course;
				}
			});
		});
	
		$("#grid").on("click",".generate_certificate",function(){
                        $('.generate_certificate').attr('disabled','disabled');
                        $('#btnSubmitlist').attr('disabled','disabled');
			var edit_id = $(this).attr("id");
			$('#responsecertificate').empty();
                        $('#responsecertificate').append("<div class='progress'><div id='thisProg' class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width:100%;background-color: green;font-size: 17px;color: red;'>Please wait don't click and refresh the page.</div>");
			var url = "common/cfLinkAadhar.php"; // the script where you handle the form input.
			var data;
			data = "action=generateandaddcertificate&lcode=" + edit_id +""; // serializes the form's elements.				 
			$.ajax({
				type: "POST",
				url: url,
				data: data,
				success: function (data)
				{   
					$('#responsecertificate').empty();
                                        var modal = document.getElementById('myModalEresponse');
                                        var span = document.getElementsByClassName("closeEresponse")[0];
                                        modal.style.display = "block";
                                        span.onclick = function() { 
                                                modal.style.display = "none";
                                                window.location.href = "frmlinkaadhar.php";				
                                        }
					$("#Eresponse").html(data);
					 window.setTimeout(function () {
                                            Mode = "Add";
                                         }, 5000);
				}
			});
		});
		
		$("#grid").on("click",".update_certificate",function(){
                        $('.update_certificate').attr('disabled','disabled');
                        $('#btnSubmitlist').attr('disabled','disabled');
			var edit_id = $(this).attr("id");
			$('#responsecertificate').empty();
                        $('#responsecertificate').append("<div class='progress'><div id='thisProg' class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width:100%;background-color: green;font-size: 17px;color: red;'>Please wait don't click and refresh the page.</div>");
			var url = "common/cfLinkAadhar.php"; // the script where you handle the form input.
			var data;
			data = "action=generateandupdatecertificate&lcode=" + edit_id +""; // serializes the form's elements.				 
			$.ajax({
				type: "POST",
				url: url,
				data: data,
				success: function (data)
				{   
					$('#responsecertificate').empty();
                                        var modal = document.getElementById('myModalEresponse');
                                        var span = document.getElementsByClassName("closeEresponse")[0];
                                        modal.style.display = "block";
                                        span.onclick = function() { 
                                                modal.style.display = "none";
                                                window.location.href = "frmlinkaadhar.php";				
                                        }
					$("#Eresponse").html(data);
					 window.setTimeout(function () {
                                            Mode = "Add";
                                         }, 5000);
				}
			});
		});
		
        function showDataITGK() {
            var ddlCourse = $("#ddlCourse").val();
            var ddlBatch = $("#ddlBatch").val();
            var txtlcode = $("#txtlcode").val();
            var rollwise = "";
            if(User_UserRole === '28' || User_UserRole === '1'){
                rollwise = "&txtlcode="+txtlcode;
            }else{
                rollwise = "";
            }
            
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfLinkAadhar.php"; // the script where you handle the form input.     
			var data;
            
            data = "action=GETDATAITGKWISE&course=" + ddlCourse + "&batch=" + ddlBatch + rollwise +""; //            
            $.ajax({
                type: "post",
                url: url,
                data: data,
                success: function (data) {                    
                    $('#response').empty();
                    $("#grid").html(data);
                    $('#example').DataTable();
                }
            });
        }

        $("#btnSubmitlist").click(function () {
            if ($("#frmAdmissionSummary").valid())
            {
                
                    showDataITGK();
                
               
            }
            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
    });

</script>
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmAdmissionSummary.js" type="text/javascript"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>
</html>
<?php
} else {
    session_destroy();
    ?>
    <script>
        window.location.href = "logout.php";
    </script>
    <?php
}
?>