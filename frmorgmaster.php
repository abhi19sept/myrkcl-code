<?php
include ('outer_header_old.php');
//include ('root_menu.php'); 

if (isset($_REQUEST['code'])) {
    echo "<script>var OrganizationCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var OrganizationCode=0</script>";
    echo "<script>var Mode='Add'</script>";
}
$random = (mt_rand(1000, 9999));
$random.= date("y");
$random.= date("m");
$random.= date("d");
$random.= date("H");
$random.= date("i");
$random.= date("s");

echo "<script>var OrgDocId= '" . $random . "' </script>";
?>

<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div class="container"> 


    <div class="panel panel-primary" style="margin-top:46px !important;">

        <div class="panel-heading">MYRKCL User Registration</div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            <form name="frmOrgMaster" id="frmOrgMaster" action="" class="form-inline" enctype="multipart/form-data">     


                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>

                    <fieldset style="border: 1px groove #ddd !important;" class="col-sm-11">
                        <br>
                        <div class="col-sm-3">     
                            <label for="SelectType">Select Type of Application:</label>
                            <select id="ddlOrgType" name="ddlOrgType" class="form-control" >
                                <option selected="true" value="">Select</option>
                                <option value="14" >RKCL Service Provider</option>
                                <!-- <option value="15" >IT-GK</option> -->
                            </select>  
                            <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        </div>


                        <div class="col-sm-3 form-group"> 
                            <label for="email">Enter Email:</label>
                            <input type="text" class="form-control" maxlength="50" name="txtEmail" id="txtEmail" placeholder="Email ID">     
                        </div>


                        <div class="col-sm-3">     
                            <label for="Mobile">Enter Mobile Number:</label>
                            <input type="text" class="form-control" maxlength="10" name="txtMobile" id="txtMobile"  placeholder="Mobiile Number">
                        </div>
                        <br>
                    </fieldset>

                </div>
                <br>
                <div class="container">

                    <div id="errorBox"></div>
                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Name of Organization/Center:</label>
                        <input type="text" class="form-control" maxlength="50" name="txtName1" id="txtName1" placeholder="Name of the Organization/Center">
                    </div>


                    <div class="col-sm-4 form-group"> 
                        <label for="ename">Registration No:</label>
                        <input type="text" class="form-control" maxlength="25" name="txtRegno" id="txtRegno" placeholder="Registration No">     
                    </div>


                    <div class="col-sm-4 form-group">     
                        <label for="faname">Date of Establishment:</label>
                        <input type="text" class="form-control" maxlength="50" name="txtEstdate" id="txtEstdate"  placeholder="YYYY-MM-DD">
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">Type of Organization:</label>
                        <select id="txtType" name="txtType" class="form-control" >

                        </select>    
                    </div>
                </div>




                <div class="container">




                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">Document Type:</label>
                        <select id="txtDoctype" name="txtDoctype" class="form-control" >
                            <option value="">Select</option>
                            <option value="PAN Card">PAN Card</option>
                     <!--       <option value="Voter ID Card">Voter ID Card</option>
                            <option value="Driving License">Driving License</option>
                            <option value="Passport">Passport</option>
                            <option value="Employer ID card">Employer ID card</option>
                            <option value="Government ID Card">Governments ID Card</option>
                            <option value="College ID Card">College ID Card</option>
                            <option value="School ID Card">School ID Card</option> -->
                        </select>    
                    </div>
                    <div class="col-sm-4 form-group"> 
                        <label for="payreceipt">Upload Proof:</label>
                        <input type="file" class="form-control"  name="orgdocproof" id="orgdocproof" onchange="checkScanForm(this)">
                    </div>
                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">Country:</label>
                        <select id="ddlCountry" name="ddlCountry" class="form-control" >

                        </select>    
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">State:</label>
                        <select id="ddlState" name="ddlState" class="form-control" >

                        </select>    
                    </div>
                </div>	


                <div class="container">



                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">Divison:</label>
                        <select id="ddlRegion" name="ddlRegion" class="form-control" >

                        </select>    
                    </div>
                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">District:</label>
                        <select id="ddlDistrict" name="ddlDistrict" class="form-control" >

                        </select>    
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="edistrict">Tehsil:</label>
                        <select id="ddlTehsil" name="ddlTehsil" class="form-control" >

                        </select>    
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="address">House No:</label>
                        <input type="text" class="form-control" maxlength="12" name="txtHouseno" 
                               id="txtHouseno" placeholder="House No" >
                    </div>
                </div>

                <div class="container">	





                    <div class="col-sm-4 form-group"> 
                        <label for="address">Street:</label>
                        <input type="text" class="form-control" name="txtStreet" id="txtStreet" placeholder="Street">    
                    </div>


                    <div class="col-sm-4 form-group">     
                        <label for="address">Road:</label>
                        <input type="text" class="form-control" id="txtRoad" name="txtRoad" placeholder="Road">

                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="address">Nearest Landmark:</label>
                        <input type="text" class="form-control" maxlength="50" name="txtLandmark" 
                               id="txtLandmark" placeholder="Nearest Landmark" >

                    </div>


                </div>		

                <div class="container">

                </div>  

                <div class="container">

                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                </div>
        </div>
    </div>   
</div>

</form>
</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<script src="scripts/orgdocupload.js"></script>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">
    $('#txtEstdate').datepicker({
        format: "yyyy-mm-dd"
    });
</script>
<script language="javascript" type="text/javascript">
    function checkScanForm(target) {
        var ext = $('#orgdocproof').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("orgdocproof").value = '';
            return false;
        }

        if (target.files[0].size > 200000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 200 KB");
            document.getElementById("orgdocproof").value = '';
            return false;
        }
        else if (target.files[0].size < 100000)
        {
            alert("Image size should be greater than 100 KB");
            document.getElementById("orgdocproof").value = '';
            return false;
        }
        document.getElementById("orgdocproof").innerHTML = "";
        return true;
    }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
        $('#txtGenerateId').val(OrgDocId);
        function FillCourse() {
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=FILLCourseName",
                success: function (data) {
                    $("#txtCourse").html(data);
                }
            });
        }

        FillCourse();


        $(function () {
            $("#txtEstdate").datepicker({
                changeMonth: true,
                changeYear: true
            });
        });

        function FillOrgType() {
            $.ajax({
                type: "post",
                url: "common/cfOrgTypeMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#txtType").html(data);
                }
            });
        }
        FillOrgType();

        function FillParent() {
            $.ajax({
                type: "post",
                url: "common/cfCountryMaster.php",
                data: "action=FILL",
                success: function (data) {
                    $("#ddlCountry").html(data);
                }
            });
        }
        FillParent();

        $("#ddlCountry").change(function () {
            var selcountry = $(this).val();
            //alert(selcountry);
            $.ajax({
                url: 'common/cfStateMaster.php',
                type: "post",
                data: "action=FILL&values=" + selcountry + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlState').html(data);
                }
            });
        });


        $("#ddlState").change(function () {
            var selState = $(this).val();
            //alert(selState);
            $.ajax({
                url: 'common/cfRegionMaster.php',
                type: "post",
                data: "action=FILL&values=" + selState + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlRegion').html(data);
                }
            });
        });

        $("#ddlRegion").change(function () {
            var selregion = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfDistrictMaster.php',
                type: "post",
                data: "action=FILL&values=" + selregion + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlDistrict').html(data);
                }
            });
        });

        $("#ddlDistrict").change(function () {
            var selDistrict = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfTehsilMaster.php',
                type: "post",
                data: "action=FILL&values=" + selDistrict + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlTehsil').html(data);
                }
            });
        });

        $("#ddlTehsil").change(function () {
            var selTehsil = $(this).val();
            //alert(selregion);
            $.ajax({
                url: 'common/cfAreaMaster.php',
                type: "post",
                data: "action=FILL&values=" + selTehsil + "",
                success: function (data) {
                    //alert(data);
                    $('#ddlArea').html(data);
                }
            });
        });


        $("#btnSubmit").click(function () {
            if ($("#frmOrgMaster").valid())
            {


            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfOrgMaster.php"; // the script where you handle the form input.
            var filename = $('#txtGenerateId').val();
            //alert(filename);    

            var data;
            // alert(Mode);
            if (Mode == 'Add')
            {

                data = "action=ADD&orgtype=" + ddlOrgType.value + "&email=" + txtEmail.value + "&mobile=" + txtMobile.value +
                        "&name=" + txtName1.value + "&type=" + txtType.value + "&regno=" + txtRegno.value +
                        "&state=" + ddlState.value + "&region=" + ddlRegion.value +
                        "&district=" + ddlDistrict.value + "&tehsil=" + ddlTehsil.value +
                        "&landmark=" + txtLandmark.value + "&road=" + txtRoad.value +
                        "&street=" + txtStreet.value + "&houseno=" + txtHouseno.value +
                        "&estdate=" + txtEstdate.value + "&doctype=" + txtDoctype.value +
                        "&docproof=" + filename + "&country=" + ddlCountry.value + ""; // serializes the form's elements.
                //alert(data);
            }
            else
            {
                data = "action=UPDATE&code=" + OrganizationCode +
                        "&name=" + txtName1.value +
                        "&type=" + txtType.value + "&regno=" + txtRegno.value +
                        "&course=" + txtCourse.value + "&state=" + ddlState.value +
                        "&region=" + ddlRegion.value + "&district=" + ddlDistrict.value +
                        "&tehsil=" + ddlTehsil.value + "&area=" + ddlArea.value +
                        "&landmark=" + txtLandmark.value + "&road=" + txtRoad.value +
                        "&street=" + txtStreet.value + "&houseno=" + txtHouseno.value +
                        "&estdate=" + txtEstdate.value + "&doctype=" + txtDoctype.value +
                        "&docproof=" + filename + "&country=" + ddlCountry.value + ""; // serializes the form's elements.
            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmregistrationsucess.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmorgmaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });
            }

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmorgregistrationvalidation.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>