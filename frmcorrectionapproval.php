<?php
$title = "Duplicate Correction Form Approval";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Admission_Name=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['batchcode'] . "'</script>";
} else {
    echo "<script>var Admission_Name=0</script>";
    echo "<script>var Mode='Add'</script>";
}

if ($_SESSION['User_Code'] == '1' || $_SESSION['User_Code'] == '4257' || $_SESSION['User_Code'] == '4258' 
|| $_SESSION['User_Code'] == '6588') {
//print_r($_SESSION);
?>
<div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">Correction / Duplicate Application Form Approval</div>
        <div class="panel-body">
            <form name="frmcorrectionapproved" id="frmcorrectionapproved" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>			
                    
					 <div class="col-sm-4 form-group">     
                        <label for="batch"> CenterCode:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="50" name="txtCentercode" id="txtCentercode" placeholder="CenterCode">									
                    </div> 	

					<div class="col-sm-4 form-group">     
                        <label for="batch"> Select Status:</label>
                        <select id="ddlstatus" name="ddlstatus" class="form-control">

                        </select>									
                    </div> 
                </div>                

                <div class="container">
                 	<input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Show"/> 
                </div>
				
				<div id="menuList" name="menuList" style="margin-top:35px;"> </div> 
        </div>
    </div>   
</div>
</form>
</div>

<div id="LearnerDetails" class="modal" style="padding-top:50px !important">            
	<div class="modal-content" style="width: 90%;">
		<div class="modal-header">
			<span class="close mm">&times;</span>
				<h6>Learner Details</h6>
		</div>
		<div class="modal-body" style="max-height: 400px; overflow-y: scroll; text-align: center;">
			<div id="responses"></div>
				<div id="gird" ></div>		
		</div>
	</div>
</div>
<form id="frmpostvalue" name="frmpostvalue" action="frmlearnerdetailsforall.php" method="post" target="_blank">
    <input type="hidden" id="txtlcode" name="txtlcode">
    <input type="hidden" id="txtMode" name="txtMode">
</form>
</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function showAllLearnerData(val,val1) {
		 if ($("#frmcorrectionapproved").valid())
           {	
				$('#response').empty();
				$('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
				$.ajax({
					type: "post",
					url: "common/cfCorrectionApproved.php",
					data: "action=ShowDetails&code=" + val + " &status=" + val1 + "",
					success: function (data) {
						$('#response').empty();
						$("#menuList").html(data);
						$('#example').DataTable();
						//$("#btnSubmit").hide();
						$('#txtCentercode').attr('readonly', true);
					}
				});
			}
            return false;
        }

		function FillCorrectionStatus() {
				//alert("hello");
                $.ajax({
                    type: "post",
                    url: "common/cfCorrectionApproved.php",
                    data: "action=FILLStatus",
                    success: function (data) {
						//alert(data);
                        $("#ddlstatus").html(data);
						 
                    }
                });
            }
            FillCorrectionStatus();
		
        $("#btnSubmit").click(function () {
			//var lcode = $('#txtLearnercode').val();
			var ccode = $('#txtCentercode').val();			
			showAllLearnerData(txtCentercode.value, ddlstatus.value);			   
        });
		
		$("#menuList").on('click', '.GetHistory',function(){
			var learnercode = $(this).attr('id');
			var modal = document.getElementById('LearnerDetails');
					var span = document.getElementsByClassName("mm")[0];
					modal.style.display = "block";
					span.onclick = function() { 
						modal.style.display = "none";
						$("#gird").html("");
					}					
			$('#responses').empty();
				$('#responses').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
					
			$.ajax({
				type: "post",
				url: "common/cfCorrectionApproved.php",
				data: "action=GetLearnerHistory&learnercode="+learnercode,
				success: function (data) {
					$('#responses').empty();
					$("#gird").html(data);											
					$('#examples').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
				}
			});
		});
		
			$("#menuList").on('click', '.learnerdashboard',function(){
			var learnercode = $(this).attr('id');
				//alert(learnercode);
			$('#txtlcode').val(learnercode);
			$('#txtMode').val("fromcorrection");
            $('#frmpostvalue').submit();
		});
        
    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmcorrectionapproval_validation.js"></script>
		
</body>

</html>

<?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>