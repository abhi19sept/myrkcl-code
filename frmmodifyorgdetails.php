<?php
$title="Edit Organisation Details";
include ('header.php'); 
include ('root_menu.php'); 

    if (isset($_REQUEST['code'])) {
                echo "<script>var OrgCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var OrgCode=0</script>";
                echo "<script>var Mode='UPDATE'</script>";
            }
            ?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div style="min-height:450px !important;max-height:1500px !important">
        <div class="container"> 
			 

            <div class="panel panel-primary" style="margin-top:46px !important;">

                <div class="panel-heading">Modify Organisation Details</div>
                <div class="panel-body">
                    
                    <form name="frmmodifyorgdetails" id="frmmodifyorgdetails" action="" class="form-inline">     

                        <div class="container">
                            <div class="container">
                              
									

                            </div>        
							<div id="errorBox"></div>
							 
                            <div class="col-sm-4 form-group">     
                                <label for="learnercode">Center Code:<font color="red">*</font></label>
                               <input type="text" class="form-control" maxlength="500" name="txtCenter" id="txtCenter" placeholder="Center Code"  >
                            </div>


							
							</div>
							
							

                        <div class="container">

                            <input type="submit" name="btnSubmit " id="btnSubmit" class="btn btn-primary" value="submit" style='margin-left:10px'/>    
                        </div>
						
						
						
						<div id="gird"></div>
                 </div>
            </div>   
        </div>


    </form>

	
	</div>


</body>
<?php include'common/message.php';?>
<?php include ('footer.php'); ?>
<style>
#errorBox{
 color:#F00;
 }
</style>

<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {
    
			if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
			
			function fillForm()
		    {
		                $.ajax({
		                    type: "post",
		                    url: "common/cfeditorgdetails.php",
		                    data: "action=EDIT&values=" + OrgCode + "",
		                    success: function (data) {
		                        //alert($.parseJSON(data)[0]['Status']);
		                        data = $.parseJSON(data);
							   //alert(data);
		                        txtCenter.value = data[0].Organization_Name;
		                        $("#gird").hide();
		                    }
		                });
		      }
			   function showData() 
			   {
                
                $.ajax({
                    type: "post",
                    url: "common/cfmodifyorgdetails.php",
                    data: "action=SHOWEDIT&Center=" + txtCenter.value + "",
                    success: function (data) {

                        $("#gird").html(data);
						 $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
						

                    }
                });
            }

           
			
			$(".btn").click(function ()
			{
			if ($("#frmmodifyorgdetails").valid())
            {
								//alert(1);
                                $('#response').empty();
                                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                               var url = "common/cfmodifyorgdetails.php"; // the script where you handle the form input.
								
								var forminput=$("#frmmodifyorgdetails").serialize();
								
								if (Mode == 'Edit')
								{
									//alert(1);
									data = "action=UPDATE&code=" + OrgCode +"&" + forminput;
									
								}
								else 
								{
									//alert(2);
									data = "action=SHOWEDIT&Center=" + txtCenter.value + "";
								
								}
                                    $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: data,
                                    success: function (data)
                                    {
										//alert(data);
										
										if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
										{
											//alert(2);
											
											$('#response').empty();
											$('#errorBox').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
										   window.setTimeout(function () {
											   window.location.href="frmmodifyorgdetails.php";
										  }, 1000);
											
											Mode="Add";
											resetForm("frmmodifyorgdetails");
											
										}
										else
										{
											$('#response').empty();
											$('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
										}
									   showData();
										
										
										
											
                                    }
                                });
							}
                                return false; // avoid to execute the actual submit of the form.
                            });
							
		
        
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmmodifyorgdetails.js"></script>
 

