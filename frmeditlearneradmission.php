<?php


$title = "Edit Admission Form";
include ('header.php');
include ('root_menu.php');

include 'DAL/upload_ftp_doc.php';
//print_r($_REQUEST);

if (isset($_REQUEST['code'])) {
    echo "<script>var AdmissionCode=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
    $Mode = $_REQUEST['Mode'];
} else {
    echo "<script>var AdmissionCode=0</script>";
    echo "<script>var Mode='Add'</script>";
    $Mode = "Add";
}
if (isset($_REQUEST['ddlCourse'])) {
    echo "<script>var CourseCode='" . $_REQUEST['ddlCourse'] . "'</script>";
    echo "<script>var BatchCode='" . $_REQUEST['ddlBatch'] . "'</script>";
} else {
    echo "<script>var CourseCode=0</script>";
    echo "<script>var BatchCode=0</script>";
}

$msg = "";
$flag = 0;
//echo empty($_POST);

if (isset($_POST["txtlname"]) && !empty($_POST["txtlname"])) {

 $FtpBatchName = $_POST['txtBatchNamePath'];


 $_UploadDirectory1 = "/admission_photo/".$FtpBatchName."";
 $_UploadDirectory2 = "/admission_sign/".$FtpBatchName."";



    // $_UploadDirectory1 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_photo/';
    // $_UploadDirectory2 = $_SERVER['DOCUMENT_ROOT'] . '/upload/admission_sign/';

    $_LearnerName = trim($_POST["txtlname"]);
    $_ParentName = trim($_POST["txtfname"]);
    $_DOB = trim($_POST["dob"]);
    $_Code = trim($_POST['txtAdmissionCode']);
    $_GeneratedId = trim($_POST['txtlcode']);

    $_Gender = trim($_POST["gender"]);
    $_MStatus = trim($_POST["mstatus"]);
    $_Medium = trim($_POST["Medium"]);
    $_PH = trim($_POST["PH"]);

    $_Address = trim($_POST["txtAddress"]);
    $_PinCode = trim($_POST["txtPIN"]);
    $_Mobile = trim($_POST["txtmobile"]);
    $_ResPhone = trim($_POST["txtResiPh"]);
    $_Email = trim($_POST["txtemail"]);

    if ($_FILES["p1"]["name"] != '') {
        $imag = $_FILES["p1"]["name"];
        $newpicture = $_GeneratedId . '_photo' . '.png';
        $_FILES["p1"]["name"] = $newpicture;
    } else {
        $newpicture = $_POST['txtphoto'];
    }

    $flag = 1;
    if ($_FILES["p1"]["name"] != '') {
        $imageinfo = pathinfo($_FILES["p1"]["name"]);
        if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg") {
            $msg = "Image must be in either PNG or JPG Format";
            $flag = 10;
        } else {
            if (file_exists("$_UploadDirectory1/" . $_GeneratedId . '_photo' . '.png')) {
                unlink("$_UploadDirectory1/" . $newpicture);
            }
            else{
                ftpUploadFile($_UploadDirectory1,$newpicture,$_FILES["p1"]["tmp_name"]);
            }

        }
    }


    if ($_FILES["p2"]["name"] != '') {
        $imag = $_FILES["p2"]["name"];
        $newsign = $_GeneratedId . '_sign' . '.png';
        $_FILES["p2"]["name"] = $newsign;
    } else {
        $newsign = $_POST['txtsign'];
    }

    $flag = 1;
    if ($_FILES["p2"]["name"] != '') {
        $imageinfo = pathinfo($_FILES["p2"]["name"]);
        if (strtolower($imageinfo['extension']) != "png" && strtolower($imageinfo['extension']) != "jpg") {
            $msg = "Image must be in either PNG or JPG Format";
            $flag = 10;
        } else {
            if (file_exists("$_UploadDirectory2/" . $_GeneratedId . '_sign' . '.png')) {
                unlink("$_UploadDirectory2/" . $newsign);
            }
            else{
                ftpUploadFile($_UploadDirectory2,$newsign,$_FILES["p2"]["tmp_name"]);
            }
        }
    }

    if ($flag != 10) {

//include 'common/commonFunction.php';		    
        require 'DAL/classconnection.php';
        $_ObjConnection = new _Connection();
        $_ObjConnection->Connect();


        $_ITGK_Code = $_SESSION['User_LoginId'];
        $_User_Code = $_SESSION['User_Code'];
		
		date_default_timezone_set('Asia/Calcutta');
				$admissionTimestamp  = date("Y-m-d H:i:s");
				
       $_UpdateQuery = "UPDATE tbl_admission ad INNER JOIN tbl_event_management em ON em.Event_Batch = ad.Admission_Batch
	   SET ad.Admission_Name = '" . $_LearnerName . "', ad.Admission_Fname='" . $_ParentName . "', ad.Admission_DOB = '" . $_DOB . "',
	   ad.Admission_Address = '" . $_Address . "', ad.Admission_PIN = '" . $_PinCode . "',
	   ad.Admission_Mobile = '" . $_Mobile . "', ad.Admission_Phone = '" . $_ResPhone . "', ad.Admission_Gender = '" . $_Gender . "', 
	   ad.Admission_MaritalStatus = '" . $_MStatus . "', ad.Admission_Medium = '" . $_Medium . "', ad.Admission_PH = '" . $_PH . "',
	   ad.Admission_Email = '" . $_Email . "', ad.Admission_Photo = '" . $newpicture . "', ad.Admission_Sign = '" . $newsign . "',
	   ad.IsNewRecord='Y'
	   WHERE ad.Admission_LearnerCode = '" . $_GeneratedId . "' AND ad.Admission_ITGK_Code = '" . $_ITGK_Code . "' AND
	   CURDATE() >= em.Event_Startdate AND CURDATE() <= em.Event_Enddate AND em.Event_Name='2'";
        $_Response = $_ObjConnection->ExecuteQuery($_UpdateQuery, Message::UpdateStatement);

        $_Insert = "Insert Into tbl_admission_log (Admission_Log_Code,Admission_Log_LearnerCode,Admission_Log_ITGK_Code,"
                . "Admission_Log_Photo,Admission_Log_Sign,Admission_Log_ProcessingStatus,Admission_Log_User_Code) "
                . "VALUES ('" . $_Code . "','" . $_GeneratedId . "','" . $_ITGK_Code . "','" . $newpicture . "','" . $newsign . "','Pending','" . $_User_Code . "')";
//                . "'" . $_Code . "' as Admission_Log_Code,"
//                . "'" . $_GeneratedId . "' as Admission_Log_LearnerCode,'" . $_ITGK_Code . "' as Admission_Log_ITGK_Code,"
//                . "'" . $newpicture . "' as Admission_Log_Photo,'" . $newsign . "' as Admission_Log_Sign, 'Pending' as Admission_Log_ProcessingStatus,"
//                . "'" . $_User_Code . "' as Admission_Log_User_Code"
//                . " From tbl_admission_log";
        $_Response1 = $_ObjConnection->ExecuteQuery($_Insert, Message::InsertStatement);
        $flag = 3;
        $msg = "Successfully Updated";
    }
} else {
    include'common/message.php';
}
?>
<link rel="stylesheet" href="css/datepicker.css">
<script src="scripts/datepicker.js"></script>
<div class="container"> 

    <div class="panel panel-primary" style="margin-top:36px !important;">
        <div class="panel-heading">Modify Admission Form </div>
        <div class="panel-body">
            <!-- <div class="jumbotron"> -->
            <form name="frmadmission" method="post" action="" id="frmadmission" class="form-inline" role="form" enctype="multipart/form-data"> 
                <div class="container">
                    <div class="container">
                        <div id="loadingDiv" class="overlay">
                            <div class="overlay__inner">
                                <div class="overlay__content"><span class="spinner"></span></div>
                            </div>
                        </div>
<?php if ($flag != 0) { ?>
                            <div id="response"><?php
    echo "<span><img src=images/correct.gif width=10px /></span>";
    echo $msg;
    echo "<script>window.setTimeout(\"window.location.href = 'frmModifyAdmission.php';\",2000);</script>";
    ?>
                            </div>
<?php } ?>
                    </div>        
                    <div id="errorBox"></div>
                    <div class="col-sm-4 form-group">     
                        <label for="learnercode">Learner Name:<span class="star">*</span></label>
                        <input type="text" class="form-control text-uppercase" maxlength="50" name="txtlname" id="txtlname" onkeypress="javascript:return allowchar(event);" placeholder="Learner Name">
                        <input type="hidden" class="form-control" maxlength="50" name="txtGenerateId" id="txtGenerateId"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtAdmissionCode" id="txtAdmissionCode"/>
                        <select id="ddlBatch" style="display:none;" name="ddlBatch" class="form-control">  </select>
                        <input type="hidden" class="form-control" maxlength="50" name="txtCourseFee" id="txtCourseFee"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtBatchNamePath" id="txtBatchNamePath"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtInstallMode" id="txtInstallMode"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtlcode" id="txtlcode"/>
                        <input type="hidden" class="form-control" name="txtphoto" id="txtphoto"/>
                        <input type="hidden" class="form-control"  name="txtsign" id="txtsign"/>
                        <input type="hidden" class="form-control"  name="action" id="action" value="UPDATE"/>
                        <input type="hidden" class="form-control" maxlength="50" name="txtmode" id="txtmode" value="<?php echo $Mode; ?>" />

                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="fname">Father/Husband Name:<span class="star">*</span></label>
                        <input type="text" class="form-control text-uppercase" maxlength="50" name="txtfname" id="txtfname" onkeypress="javascript:return allowchar(event);" placeholder="Father Name">     
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="dob">Date of Birth:<span class="star">*</span></label>								
                        <input type="text" class="form-control" readonly="true" maxlength="50" name="dob" id="dob"  placeholder="YYYY-MM-DD">
                    </div>

                    <div class="col-sm-4 form-group" style="display:none;"> 
                        <label for="mtongue">Mother Tongue:</label>
                        <select id="ddlmotherTongue" name="ddlmotherTongue" class="form-control" >								  
                            <option value="Hindi" selected>Hindi</option>
                            <option value="English">English</option>
                        </select>    
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankname">Email Id:<span class="star">*</span></label>
                        <input type="text" placeholder="Email Id" class="form-control" id="txtemail" name="txtemail" maxlength="100" />
                    </div>  
                </div>  

                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="gender">Gender:</label> <br/>                               
                        <label class="radio-inline"> <input type="radio" id="genderMale" name="gender" value="Male"/> Male </label>
                        <label class="radio-inline"> <input type="radio" id="genderFemale" name="gender" value="Female"/> Female </label>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="email">Marital Status:</label> <br/>  
                        <label class="radio-inline"><input type="radio" id="mstatusSingle" name="mstatus" value="Single"/> Single </label>
                        <label class="radio-inline"><input type="radio" id="mstatusMarried" name="mstatus" value="Married"/> Married	</label>							
                    </div>

                    <div class="col-sm-4 form-group">     
                        <label for="address">Medium Of Study:</label> <br/> 
                        <label class="radio-inline"> <input type="radio" id="mediumEnglish" name="Medium" value="English"/> English </label>
                        <label class="radio-inline"> <input type="radio" id="mediumHindi" name="Medium" value="Hindi"/> Hindi </label>
                    </div>

                    <div class="col-sm-6 form-group"> 
                        <label for="deptname">Physically Challenged:</label> <br/> 
                        <label class="radio-inline"> <input type="radio" id="physicallyChStYes" name="PH" value="Yes"/> Yes </label>
                        <label class="radio-inline"> <input type="radio" id="physicallyChStNo" name="PH" value="No"/> No </label>
                    </div>
                </div>    

                <div class="container" style="display:none;">
                    <div class="col-sm-4 form-group"> 
                        <label for="empid">Proof Of Identity:<span class="star">*</span></label>
                        <select id="ddlidproof" name="ddlidproof" class="form-control">
                            <option value="">Select</option>
                            <option value="PAN Card">PAN Card</option>
                            <option value="Voter ID Card">Voter ID Card</option>
                            <option value="Driving License">Driving License</option>
                            <option value="Passport">Passport</option>
                            <option value="Employer ID card">Employer ID card</option>
                            <option value="Government ID Card">Governments ID Card</option>
                            <option value="College ID Card">College ID Card</option>
                            <option value="School ID Card">School ID Card</option>
                        </select>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="gpfno">Aadhar No:<span class="star">*</span></label>
                        <input type="text" class="form-control" maxlength="12"  name="txtidno" id="txtidno" onkeypress="javascript:return allownumbers(event);" placeholder="Aadhar No">
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="designation">District:<span class="star">*</span></label>
                        <select id="ddlDistrict" name="ddlDistrict" class="form-control">                                  
                        </select>
                    </div>     

                    <div class="col-sm-4 form-group"> 
                        <label for="marks">Tehsil:<span class="star">*</span></label>
                        <select id="ddlTehsil" name="ddlTehsil" class="form-control">                                 
                        </select>
                    </div>
                </div>

                <div class="container">                          
                    <div class="col-sm-4 form-group"> 
                        <label for="attempt">Address:<span class="star">*</span></label>  
                        <textarea class="form-control" rows="3" id="txtAddress" name="txtAddress" placeholder="Address" onkeypress="javascript:return validAddress(event);"></textarea>

                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="pan">PINCODE:<span class="star">*</span></label>
                        <input type="text" class="form-control" name="txtPIN" id="txtPIN"  placeholder="PINCODE" onkeypress="javascript:return allownumbers(event);" maxlength="6"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankaccount">Mobile No:<span class="star">*</span></label>
                        <input type="text" class="form-control"  name="txtmobile"  id="txtmobile" placeholder="Mobile No" onkeypress="javascript:return allownumbers(event);" maxlength="10"/>
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <label for="bankdistrict">Phone No:<span class="star">*</span></label>
                        <input type="text" placeholder="Phone No" class="form-control" id="txtResiPh" name="txtResiPh" onkeypress="javascript:return allownumbers(event);" maxlength="12"/>
                    </div>   
                </div>

                <div class="container">                            
                    <div class="col-sm-4 form-group" style="display:none;"> 
                        <label for="branchname">Qualification:<span class="star">*</span></label>
                        <select id="ddlQualification" name="ddlQualification" class="form-control">                                  
                        </select>
                    </div>

                    <div class="col-sm-4 form-group" style="display:none;"> 
                        <label for="ifsc">Type of Learner:<span class="star">*</span></label>
                        <select id="ddlLearnerType" name="ddlLearnerType" class="form-control" onChange="findselected()">                                 
                        </select>
                    </div> 

                    <div class="col-sm-4 form-group" style="display:none;"> 
                        <label for="micr">GPF No:</label>
                        <input type="text" placeholder="GPF No" class="form-control" id="txtGPFno" name="txtGPFno"  onkeypress="javascript:return allownumbers(event);" maxlength="8"/>
                    </div>
                </div>

                <div class="container">                            
                    <div class="col-sm-4 form-group" style="display:none;"> 
                        <label for="payreceipt">Upload Scan Application Form:<span class="star">*</span></label>
                        <input type="file" class="form-control"  name="txtUploadScanDoc" id="txtUploadScanDoc"/>
                    </div>											

                    <div class="col-sm-4 form-group" > 
                                <div id="uploadPreview9">
                                    
                                </div>
                        <label for="photo">Attach Photo:<span class="star">*</span></label> </br>
                        <img id="uploadPreview1" src="images/user icon big.png" id="uploadPreview1" name="filePhoto" width="80px" height="107px" onclick="javascript:document.getElementById('uploadImage1').click();">								  
                        <input id="uploadImage1" type="file" name="p1" onchange="checkPhoto(this);
                                PreviewImage(1)"/>									  
                    </div>

                    <div class="col-sm-4 form-group"> 
                        <div id="uploadPreview8">
                                    
                                </div>
                        <label for="photo">Attach Signature:<span class="star">*</span></label> </br>
                        <img id="uploadPreview2" src="images/sign.jpg" id="uploadPreview2" name="filePhoto" width="80px" height="35px" onclick="javascript:document.getElementById('uploadImage2').click();">							  
                        <input id="uploadImage2" type="file" name="p2" onchange="checkSign(this);
                                PreviewImage(2)" />
                    </div>  
                </div>

                <div class="container">
                    <input type="submit" name="btnSubmit1" id="btnSubmit1" class="btn btn-primary" value="Submit"/>    
                </div>
        </div>
    </div>   
</div>
</form>

</body>

<?php include ('footer.php'); ?>
<style type="text/css">
.overlay {
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    position: fixed;
    background: #222;
    z-index: 999;
    opacity: 0.2;
}

.overlay__inner {
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    position: absolute;
}

.overlay__content {
    left: 50%;
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
}

.spinner {
  
    display: inline-block;
    border: 16px solid #f3f3f3; /* Light grey */
      border-top: 16px solid #006fba; /* Blue */
      border-radius: 50%;
      width: 120px;
      height: 120px;
      animation: spin 2s linear infinite;
}

@keyframes spin {
  100% {
    transform: rotate(360deg);
  }
}
a {
  color: #FFFFFF;
  text-decoration: none;
}
    #errorBox
    {	color:#F00;	 } 
</style>

<script type="text/javascript">
var $loading = $('#loadingDiv').hide();
$(document)
  .ajaxStart(function () {
    $loading.show();
  })
  .ajaxStop(function () {
    $loading.hide();
  });
    $('#dob').datepicker({
        format: "yyyy-mm-dd",
        orientation: "bottom auto",
        todayHighlight: true
    });
</script>

<script language="javascript" type="text/javascript">
    function allownumbers(e) {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        var reg = new RegExp("[0-9.,]")
        if (key == 8 || key == 0) {
            keychar = 8;
        }
        return reg.test(keychar);
    }
</script>

<script type="text/javascript">
    function PreviewImage(no) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage" + no).files[0]);
        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview" + no).src = oFREvent.target.result;
        };
       if(no == 1){
         $("#uploadPreview" + no).show();
         $("#uploadPreview9").hide();
       }
       else if(no == 2){
            $("#uploadPreview" + no).show();
            $("#uploadPreview8").hide();
       }
        
    }
    ;
</script>

<script language="javascript" type="text/javascript">


    //$('#uploadImage1').click(function () {
    //    $("#uploadPreview1").show();
    //    $("#uploadPreview9").hide();
    //});
    function checkPhoto(target) {
        var ext = $('#uploadImage1').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage1").value = '';
            return false;
        }

        if (target.files[0].size > 5000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 5 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        else if (target.files[0].size < 3000)
        {
            alert("Image size should be greater than 3 KB");
            document.getElementById("uploadImage1").value = '';
            return false;
        }
        document.getElementById("uploadImage1").innerHTML = "";
        return true;
    }
</script>

<script language="javascript" type="text/javascript">
    function checkSign(target) {
        var ext = $('#uploadImage2').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
            alert('Image must be in either PNG or JPG Format');
            document.getElementById("uploadImage2").value = '';
            return false;
        }

        if (target.files[0].size > 3000) {

            //document.getElementById("photodiv").innerHTML = "Image too big (max 100kb)";
            alert("Image size should less or equal 3 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        else if (target.files[0].size < 1000)
        {
            alert("Image size should be greater than 1 KB");
            document.getElementById("uploadImage2").value = '';
            return false;
        }
        document.getElementById("uploadImage2").innerHTML = "";
        return true;
    }
</script>	

<?php
if (isset($_REQUEST['Mode']) == 'Edit') {
    ?>
    <script>
        //$( "#ddlmotherTongue" ).prop( "disabled", true );	
        //$("input[type=radio]").attr('disabled', true);
        //$( "#ddlidproof" ).prop( "disabled", true );
        //$( "#txtidno" ).prop( "disabled", true );
        //$( "#ddlDistrict" ).prop( "disabled", true );
        //$( "#ddlTehsil" ).prop( "disabled", true );
        //$( "#txtAddress" ).prop( "disabled", true );
        //$( "#txtPIN" ).prop( "disabled", true );
        //$( "#txtmobile" ).prop( "disabled", true );
        //$( "#txtResiPh" ).prop( "disabled", true );
        //$( "#txtemail" ).prop( "disabled", true );
        //$( "#ddlQualification" ).prop( "disabled", true );	
        //$( "#ddlLearnerType" ).prop( "disabled", true );
        //$( "#txtGPFno" ).prop( "disabled", true );	
        //$( "#txtUploadScanDoc" ).prop( "disabled", true );				
    </script>
<?php } ?>    
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";

    $(document).ready(function () {
        //alert(BatchCode);



        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            //alert(1);
            fillForm();
        }

        $("input[type='image']").click(function () {
            $("input[id='my_file']").click();
        });




        function fillForm()
        {
            $.ajax({
                type: "post",
                url: "common/cfAdmission.php",
                data: "action=EDIT&values=" + AdmissionCode + "",
                success: function (data) {
				    if (data != '') {
                    data = $.parseJSON(data);
                    txtlcode.value = data[0].lcode;
                    txtlname.value = data[0].lname;
                    txtfname.value = data[0].fname;
                    dob.value = data[0].dob;
                    txtAdmissionCode.value = data[0].AdmissionCode;

                    ddlmotherTongue.value = data[0].tongue;
                    txtAddress.value = data[0].address;
                    txtPIN.value = data[0].pin;
                    txtmobile.value = data[0].mobile;

                    txtResiPh.value = data[0].phone;
                    txtemail.value = data[0].email;

                    txtphoto.value = data[0].photoad;
                    txtsign.value = data[0].signad;
                    txtBatchNamePath.value = data[0].batchname;
                    $("#uploadPreview9").html(data[0].photo);
                    $("#uploadPreview1").hide();

                    $("#uploadPreview8").html(data[0].sign);
                    $("#uploadPreview2").hide();
                    // $("#uploadPreview1").attr('src', "upload/admission_photo/" + data[0].photo);
                    //$("#uploadPreview2").attr('src', "upload/admission_sign/" + data[0].sign);
                    //p1.value = data[0].photo;
                    //p2.value = data[0].sign; 
					} else {
                        window.location.href="frmModifyAdmission.php";
                    }
                }
            });
        }

        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }
        // statement block end for submit click
    });

</script>

<!--<script src="scripts/editadmissionimageupload.js"></script>
<script src="scripts/signupload.js"></script>-->
<script type="text/javascript" src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmmodifyadmission_validation.js" type="text/javascript"></script>	
</html>