<?php
$title = "Get User Login Password";
include ('header.php');
include ('root_menu.php');

//if (isset($_REQUEST['code'])) {
//    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
//    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
//} else {
//    echo "<script>var Code=0</script>";
//    echo "<script>var Mode='Add'</script>";
//}
if ($_SESSION['User_Code'] == '1') {
?>
<div style="min-height:430px !important">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:36px !important;">

            <div class="panel-heading">Get User Login Password</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>

                        </div>        
                        <div id="errorBox"></div>
                       
                       
                    </div>

                    <div id="grid" name="grid" style="margin-top:25px;"> </div> 
                    
                     <div class="col-sm-4 form-group" id="updatepass" style=" display:  none;">                                  
                            <input type="button" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Update Password"/>    
                        </div>

                </form>
            </div>
        </div>   
    </div>

</div>

</body>
<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>
<style>
    #errorBox{
        color:#F00;
    }
</style>
<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        $("#btnShow").click(function () {
            //alert("1");			
            
        });
        
        function getdecryptpassword(){
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            
            $.ajax({
                type: "post",
                url: "common/cfgetpassword.php",
                data: "action=DETAILSDEPASS",
                success: function (data)
                {$('#response').empty();
                    //alert(data);
                    $("#grid").html(data);
                    $('#example').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

                    $("#updatepass").show();
                   
                  

                }
            });
        }


getdecryptpassword();

        $("#btnSubmit").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfgetpassword.php"; // the script where you handle the form input.
            var data;
            data = "action=UPDATEDPASS"; // serializes the form's elements.
            
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmgetdecryptpassword.php";
                        }, 3000);

                        //Mode = "Add";
                        resetForm("form");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    //showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</html>
    <?php
} else {
    session_destroy();
    ?>
    <script>

        window.location.href = "index.php";

    </script>
    <?php
}
?>