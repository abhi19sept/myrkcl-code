<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Bank Account Details For Share Disbursement</title>
	</head>
	<body>
		<div class="wrapper">
			<?php
            include './include/header.html';

            include './include/menu.php';
            
            if (isset($_REQUEST['code'])) {
                echo "<script>var BankCode=" . $_REQUEST['code'] . "</script>";
                echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
            } else {
                echo "<script>var BankCode=0</script>";
                echo "<script>var Mode='Add'</script>";
            }
            ?>
			<div class="main">
				<h1>Bank Account Details For Share Disbursement</h1>
				<form action="" method="post" name="frmbankdetails" id="frmbankdetails">
					<input type="hidden" id="taskId" name="taskId" value="0">
					<fieldset>
						<legend>Payment Type</legend>
						<table width="100%">
							<tbody>
								<tr>
									<td class="myLabels">Payment Type <font color="red">*</font></td>
									<td>
										<input type="radio" value="3" name="paymentType" id="paymentType_wire">Bank Transfer
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" value="4" name="paymentType" id="paymentType_ecs">ECS
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" value="2" name="paymentType" id="paymentType_cheque" disabled="disabled">Cheque
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</td>
								</tr>
							</tbody>
						</table>
					</fieldset>
					<fieldset>
						<legend>Bank Account Information</legend>
						<table width="100%">
							<tbody>
								<tr>
									<td class="myLabels">Account Name</td>
									<td><input type="text" id="txtaccountName" name="txtaccountName" />
										&nbsp;<font color="red">*</font>
										&nbsp;&nbsp;&nbsp;<span id="accountNameRej" class="red" style="display: none;">Rejected/Please Change</span>
									</td>
								</tr>
								<tr>
									<td class="myLabels">Account Number</td>
									<td><input type="text" name="txtaccountNumber" id="txtaccountNumber">
										&nbsp;<font color="red">*</font>
										&nbsp;&nbsp;&nbsp;<span id="accountNumberRej" class="red" style="display: none;">Rejected/Please Change</span>
									</td>
								</tr>
								<tr>
									<td class="myLabels">Account Type</td>
									<td>
										<input type="radio" value="10" name="accountType" id="accountType_saving">Savings
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" value="11" name="accountType" id="accountType_current">Current
										<font color="red">*</font>
										&nbsp;&nbsp;&nbsp;<span id="accountTypeRej" class="red" style="display: none;">Rejected/Please Change</span>	
									</td>
								</tr>
							</tbody>
						</table>
					</fieldset>
					<fieldset>
						<legend>Bank Details</legend>
						<table width="100%" border="0">
							<tbody>
								<tr>
									<td class="myLabels" id="ifscSearchOption">Search by IFSC Code </td>
									<td>
										<input type="text" name="txtifscSearch" id="txtifscSearch">
									</td>
								</tr>
								<tr>
									<td class="myLabels">Bank Name</td>
									<td><select name="selbankId" id="selbankId">
									<option value="">-- Select Bank   -----</option>
											<option value="SBI">SBI</option>
											<option value="SBBJ">SBBJ</option>
											<option value="DENA BANK">DENA BANK</option>
											<option value="ICICI">ICICI</option>
									</select> &nbsp;<font color="red">*</font> 
										&nbsp;&nbsp;&nbsp;
									</td>
								</tr>
								<tr>
									<td class="myLabels">Branch Name</td>
									<td><select name="selbranchId" id="selbranchId">
									<option value="">-- Select Branch --</option>
											<option value="SANGANER">SANGANER</option>
											<option value="JHALANA">JHALANA</option>
											<option value="RAJA PARK">RAJA PARK</option>
											<option value="C-SCHEME">C-SCHEME</option>
									</select> &nbsp;<font color="red">*</font>
										&nbsp;&nbsp;&nbsp;
									</td>
								</tr>
								
								
								<tr>
									<td  id="displayIFSC">MICR Code</td>
									<td><input type="text" id="txtmicrcode" name="txtmicrcode"></td>
									
								</tr>
							</tbody>
						</table>
					</fieldset>
					<fieldset>
						<legend>Image Upload</legend>
						<table width="100%">
							<tbody>
								<tr>
									<td>Upload Images of Bank Documents, For example: 
										<br>1. Scanned Copy of canceled blank cheque
										<br>2. Scanned Copy of Bank Passbook
										<br>3. Scanned Copy of Welcome letter from bank/ any other
										<br>(Maximum 3 images)
									</td>
									<td id="file"><input type="file" id="chequeImage" name="chequeImage">	
									</td>
								</tr>
							</tbody>
						</table>
					</fieldset>
					<br>
					<table width="100%">
						<tbody>
							<tr>
								<td align="center">
									<input type="submit" value="  Save and Submit &gt;&gt;&gt;" name="btnSubmit" id="btnSubmit">
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
                    <?php
            include './include/footer.html';
            ?>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function(){
			function showData(){
					
		                     $.ajax({
		                       type:"post",
		                       url:"common/commonFunction.php",
		                       data:"action=SHOW&pagename=statusmaster",
		                       success:function(data){
		                            $("#gird").html(data);
		                       }
		                     });
		                   }
		
		                   showData();
		                   
		       $("#btnSubmit").click(function() {
		           var url = "common/commonFunction.php"; // the script where you handle the form input.
		           
		           var chequeimage=$('#chequeImage').val(); 
		           
		           if (document.getElementById('paymentType_wire').checked) 
		           {
		       		var payment_type = 'Bank Transfer';
		       	}
		   	    else {
		   	    	payment_type = 'ECS';
		        }

		           if (document.getElementById('accountType_saving').checked) 
		           {
		       		var acc_type = 'Savings';
		       	}
		   	    else {
		   	    	acc_type = 'Current';
		        }
		           
		           $.ajax({
		                  type: "POST",
		                  url: url,
		                  data:"action=ADD&pagename=bankdetails&paymenttype="+payment_type+"&accname="+txtaccountName.value+"&accno="+txtaccountNumber.value+"&acctype="+acc_type+"&ifsccode="+txtifscSearch.value+"&bankname="+selbankId.value+"&branchname="+selbranchId.value+"&micrcode="+txtmicrcode.value+"&chequeimage="+chequeimage+"", // serializes the form's elements.
		                 
		                  success: function(data)
		                  {
		                	  
		                      $('#response').empty();
		                      $('#response').append("<span class='error'>"+data+"</span>"); // show response from the php script.
						   showData();
						   resetForm("frmbankdetails");
						   
		                  }
		                });
	                alert(data);
		
		       return false; // avoid to execute the actual submit of the form.
		   });
		function resetForm(formid) {
				$(':input','#'+formid) .not(':button, :submit, :reset, :hidden') .val('') .removeAttr('checked') .removeAttr('selected');
				}
		});
		
		  
	</script>
</html>