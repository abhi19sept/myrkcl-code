<?php
$title="premises Details";
include ('header.php'); 
include ('root_menu.php'); 
  if (isset($_REQUEST['code'])) {
            echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
            echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
        } else {
            echo "<script>var Code=0</script>";
            echo "<script>var Mode='Add'</script>";
        }
        ?>

          <div class="container"> 
			  

            <div class="panel panel-primary" style="margin-top:36px !important;">

                <div class="panel-heading">Course Master</div>
                <div class="panel-body">
	<div id="gird"> </div>
	</div>
   </div>
 </div>
 
 <script>
		$(document).ready(function() {
    $('#example').DataTable();
} );
	</script>


    




</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
<style>
#errorBox{
 color:#F00;
 }
</style>
<script language="javascript" type="text/javascript">
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.,]")

            if (key == 8 || key == 0) {
                keychar = 8;
            }

            return reg.test(keychar);
        }
</script>
<script type="text/javascript">
    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {


       


        if (Mode == 'Delete')
        {
            if (confirm("Do You Want To Delete This Item ?"))
            {
                deleteRecord();
            }
        }
        else if (Mode == 'Edit')
        {
            fillForm();
        }


        function deleteRecord()
        {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=DELETE&values=" + Code + "",
                success: function (data) {
                    //alert(data);
                    if (data == SuccessfullyDelete)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmdisplaycourses.php";
                        }, 1000);
                        Mode = "Add";
                        resetForm("frmdisplaycourses");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();
                }
            });
        }


       

        function showData() {

            $.ajax({
                type: "post",
                url: "common/cfCourseMaster.php",
                data: "action=SHOW",
                success: function (data) {

                    $("#gird").html(data);

                }
            });
        }

        showData();		
		
        $("#btnSubmit").click(function () {
			
			$("#frmCourseMaster").valid();
			
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            var url = "common/cfCourseMaster.php"; // the script where you handle the form input.
            var data;
			
            if (Mode == 'Add')
            {
                data = "action=ADD&txtName=" + txtName.value + "&ddlCourseType=" + ddlCourseType.value + "&txtDuration="+ txtDuration.value + 
					   "&ddlMedium="+ ddlMedium.value +
						"&ddlFeeType="+ ddlFeeType.value +
						"&txtFee="+ txtFee.value +
						"&ddlExamination="+ ddlExamination.value +
						"&ddlClasstype="+ ddlClasstype.value +
						"&ddlAffiliate="+ ddlAffiliate.value +
						"&ddlStatus="+ ddlStatus.value +

					   ""; // serializes the form's elements.
            }
            else
            {
                data = "action=UPDATE&code=" + Code + "txtName=" + txtName.value + "&ddlCourseType=" + ddlCourseType.value + "&txtDuration="+ txtDuration.value + 
					   "&ddlMedium="+ ddlMedium.value +
						"&ddlFeeType="+ ddlFeeType.value +
						"&txtFee="+ txtFee.value +
						"&ddlExamination="+ ddlExamination.value +
						"&ddlClasstype="+ ddlClasstype.value +
						"&ddlAffiliate="+ ddlAffiliate.value +
						"&ddlStatus="+ ddlStatus.value + ""; // serializes the form's elements.
            }
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data)
                {
                    if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmcoursemaster.php";
                        }, 1000);

                        Mode = "Add";
                        resetForm("frmcoursemaster");
                    }
                    else
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                    }
                    showData();


                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

