<?php
$title = "EOI Payment";
include ('header.php');
include ('root_menu.php');
echo "<script>var Mode='Add'</script>";
?>
<div style="min-height:430px !important;max-height:auto !important;">
<div class="container"> 			  
    <div class="panel panel-primary" style="margin-top:36px;">
        <div class="panel-heading">Course EOI Payment</div>
        <div class="panel-body">
            <form name="frmfeepayment" id="frmfeepayment" class="form-inline" role="form" enctype="multipart/form-data">
                <div class="container">
                    <div class="container">
                        <div id="response"></div>

                    </div>        
                    <div id="errorBox"></div>
                        
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Course:</label>
                        <select id="ddlCourse" name="ddlCourse" class="form-control">

                        </select>	
                        <select id="ddlCourseCode" name="ddlCourseCode" class="form-control" style="display: none;">

                        </select>
                    </div>                    
                </div>

                <div class="container"> 
                 <div class="col-sm-4 form-group">     
                        <label for="course">Select EOI Name:</label>
                        <select id="ddlEOI" name="ddlEOI" class="form-control">

                        </select>
                    </div> 
                </div>
                
                <div class="container">
                    <div class="col-sm-4 form-group">     
                        <label for="batch"> Select Payment Mode:</label>
                        <select id="paymode" name="paymode" class="form-control" onchange="toggle_visibility1('online_mode');">

                        </select>									
                    </div> 
                </div>

                <div class="container" id="online_mode" style="display:none;">
                    <div class="col-md-12 form-group" style="display:none;">                                
                        <label class="radio-inline"> <input type="radio" id="PayUMoney" value="payumoney"  name="Role" checked="true" /> Pay Using PayUMoney </label>
                        <label class="radio-inline"> <input type="radio" id="PayDDMode" value="emitra" name="Role" /> Pay Using e-Mitra Gateway </label>                                
                        <!--<label class="radio-inline"> <input type="radio" id="RoleITGK"  name="Role" /> ITGK </label>-->
                                                    <!--<label class="radio-inline"> <input type="radio" id="genderFemale" name="gender" /> Female </label>-->
                    </div> 
                </div>



                <div>
                    <input type="hidden" name="amounts" id="amounts"/>
                </div>
                
                <div class="container">
                        <br><input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit" style="display:none;"/>    
                </div>
        </div>
    </div>   
</div>
</form>
</div>

<form id="frmpostvalue" name="frmpostvalue" action="frmonlinepayment.php" method="post">
	<input type="hidden" id="txnid" name="txnid">
</form>

<form id="frmwithoutfeepostvalue" name="frmwithoutfeepostvalue" action="frmwithoutfeeadmissionpayment.php" method="post">
    <input type="hidden" id="withoutfeecode" name="withoutfeecode">
    <input type="hidden" id="withoutfeepaytype" name="withoutfeepaytype">                
    <input type="hidden" id="withoutfeeamount" name="withoutfeeamount">				
</form>

<form id="frmddpostvalue" name="frmddpostvalue" action="frmddeoipayment.php" method="post">
    <input type="hidden" id="ddcode" name="ddcode">
    

</form>
</body>
<?php include ('footer.php'); ?>
<?php include'common/message.php'; ?>

<script type="text/javascript">
<!--

    function toggle_visibility1(id) {
        var e = document.getElementById(id);
        var f = document.getElementById('paymode').value;
        if (f == "1")
        {
            e.style.display = 'block';
        }
        else {
            e.style.display = 'none';
        }
    }
//-->
</script>  

<script type="text/javascript">

    function FillCourse() {            
        $.ajax({
            type: "post",
            url: "common/cfApplyEoi.php",
            data: "action=FILLEOICourse",
            success: function (data) {
                //alert(data);
                $("#ddlCourse").html(data);
            }
        });
    }

    function showpaymentmode() {
       // alert(val);
        $.ajax({
            type: "post",
            url: "common/cfEvents.php",
            data: "action=SHOWEOIPay&courseeoi=" + ddlEOI.value + "",
            success: function (data) {
                //alert(data);
                $("#paymode").html(data);                   
            }
        });
    }
    
    $(document).ready(function () {
		
        FillCourse();
        
         $("#ddlCourse").change(function () {           
            $.ajax({
                type: "post",
                url: "common/cfApplyEoi.php",
                data: "action=FILLEOICourseCode&code=" + ddlCourse.value + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlCourseCode").html(data);
                }
            });
        });
      
       $("#ddlCourse").change(function () {            
            $.ajax({
                type: "post",
                url: "common/cfApplyEoi.php",
                data: "action=FILLEOIPayment&eoicode=" + ddlCourse.value + "",
                success: function (data) {
                    //alert(data);
                    $("#ddlEOI").html(data);
                }
            });
        });
      

        $("#ddlEOI").change(function () {
            showpaymentmode();
            //alert(ddlEOI.value);
        });

        $("#paymode").change(function () {
            //showAllData(ddlBatch.value, ddlCourse.value);
			$('#btnSubmit').show();
        });


        $("#btnSubmit").click(function () {
            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
           
             var url = "common/cfApplyEoi.php"; // the script where you handle the form input.
          
            $.ajax({
                type: "POST",
                url: url,
                data: "action=EDIT&values=" + ddlCourse.value + "&paymode=" + paymode.value + "&eoicode=" + ddlEOI.value + "",
                success: function (data) {
                   //alert(data);
				    $('#response').empty();
				    if (data==0) {
					   $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + "     Invalid User Input." + "</span></p>");
				    } else {
                        data = $.parseJSON(data);
                        var paymode = $('#paymode').val();
                        if (paymode == "2") {
                            ddcode.value = data[0].Ecode;
                            $('#frmddpostvalue').submit();
                        } else if (paymode == "4") {
                            $('#frmwithoutfeepostvalue').submit();
                        } else {
                            if (data[0].txnid == 0 || data[0].txnid == '') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Something went Wrong. Please try again." + "</span></p>");
                            } else {
                                
                                if (data == 'TimeCapErr') {
                                $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>You have already initiated the payment for any one of these learners , Please try again after 15 minutues.</span></p>");
                                    alert('You have already initiated the payment for any one of these learners , Please try again after 15 minutues.');
                            } else {
                                $('#txnid').val(data[0].txnid);
                                $('#frmpostvalue').submit();
                            }
                            }
                        }
				    }
                }
            });

            return false; // avoid to execute the actual submit of the form.
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>
</body>

</html>