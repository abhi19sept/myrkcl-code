<?php
$title = "Submit RSP Details";
include ('header.php');
include ('root_menu.php');

if (isset($_REQUEST['code'])) {
    echo "<script>var Code=" . $_REQUEST['code'] . "</script>";
    echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
    echo "<script>var Code=0</script>";
    echo "<script>var Mode='Add'</script>";
}
?>

		


<div style="min-height:430px !important;max-height:auto !important;">
    <div class="container"> 

        <div class="panel panel-primary" style="margin-top:20px !important;">

            <div class="panel-heading">Submit Service Provider Details</div>
            <div class="panel-body">
                <!-- <div class="jumbotron"> -->
                <form name="form" id="form" class="form-inline" role="form" enctype="multipart/form-data">     

                    <div class="container">
                        <div class="container">
                            <div id="response"></div>
                        </div>        
                        <div id="errorBox"></div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading">Current Service Provider's Registered - Details</div>
                        <div class="panel-body">

                            <div class="container">
                                <div class="col-sm-4 form-group">     
                                    <label for="learnercode">Name of Organization/Center:</label>
                                    <textarea class="form-control" readonly="true" name="txtrspname" id="txtrspname" placeholder="Name of the Organization/Center"></textarea>
                                </div>


                                <div class="col-sm-4 form-group"> 
                                    <label for="ename">Mobile No:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtmobno" id="txtmobno" placeholder="Registration No">     
                                </div>


                                <div class="col-sm-4 form-group">     
                                    <label for="faname">Date of Establishment:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtdate" id="txtdate"  placeholder="YYYY-MM-DD">
                                </div>

                                <div class="col-sm-4 form-group"> 
                                    <label for="edistrict">Type of Organization:</label>
                                    <input type="text" class="form-control" readonly="true" name="txtRspType" id="txtRspType" placeholder="Type Of Organization">  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">

                        <button type="button" class="btn btn-primary" name="btnSubmit" id="btnSubmit">Submit</button>

                    </div>
                </form>

            </div>
        </div>
    </div>  

</div>
</body>

<?php include'common/message.php'; ?>
<?php include ('footer.php'); ?>


<style>
    #errorBox{
        color:#F00;
    }
</style>

<script type="text/javascript">

    var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
    var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
    var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
    var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
    $(document).ready(function () {

        function FillCurrentRsp() {
            //alert();
            $.ajax({
                type: "post",
                url: "common/cfSubmitRspDetails.php",
                data: "action=GETREGRSP",
                success: function (data) {
                    //alert(data);
                    $('#response').empty();
                    if (data == "") {
                        $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + " Serice Provider Not Found " + "</span></p>");
                    }
                    else {
                        data = $.parseJSON(data);
                        txtrspname.value = data[0].rspname;
                        txtmobno.value = data[0].mobno;
                        txtdate.value = data[0].date;
                        txtRspType.value = data[0].rsptype;
                    }
                }
            });
        }
        FillCurrentRsp();


        $("#btnSubmit").click(function () {

            $('#response').empty();
            $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
            $.ajax({
                type: "post",
                url: "common/cfSubmitRspDetails.php",
                data: "action=SubmitDetails",
                success: function (data)
                {//alert(data);

                    if (data === "Successfully Inserted")
                    {
                        $('#response').empty();
                        $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                        window.setTimeout(function () {
                            window.location.href = "frmsubmitrspdetails.php";
                        }, 1000);

                    } else
                    {
                          $('#response').empty();        
                          BootstrapDialog.alert("<div class='alert-error'><span><img src=images/error.gif width=10px /></span><span>&nbsp; Details Already Submitted.</span>");
                          window.setTimeout(function () {
                            window.location.href = "frmsubmitrspdetails.php";
                        }, 3000);
                    }
                }
            });
        });
        function resetForm(formid) {
            $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
        }

    });

</script>

<script src="rkcltheme/js/jquery.validate.min.js"></script>
<script src="bootcss/js/frmchangersp_validation.js"></script>
<style>
    .error {
        color: #D95C5C!important;
    }
</style>

</html>