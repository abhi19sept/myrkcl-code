<?php
$title="Area Master";
include ('header.php'); 
include ('root_menu.php');
if (isset($_REQUEST['code'])) {
echo "<script>var AreaCode=" . $_REQUEST['code'] . "</script>";
echo "<script>var Mode='" . $_REQUEST['Mode'] . "'</script>";
} else {
echo "<script>var AreaCode=0</script>";
echo "<script>var Mode='Add'</script>";
}
?>
        <div class="container"> 
			

			<div class="panel panel-primary" style="margin-top:36px !important;">  
				<div class="panel-heading">Admission Summary</div>
				<div class="panel-body"> 
		 
				<form name="frmArealMaster" id="frmArealMaster" class="form-inline" role="form" enctype="multipart/form-data">
					<div class="container">
						<div class="container">
							<div id="response"></div>
						</div>        
							<div id="errorBox"></div>
							
						<div class="col-md-6 form-group">     
                                <label for="learnercode"> Area Name:<span class="star">*</span></label>
                                <input type="text" class="form-control" maxlength="50" name="txtAreaName" id="txtAreaName" placeholder="Area Name">
                        </div> 
					</div>  

					<div class="container">
						<div class="col-sm-8 form-group">     
							<label for="order">Area PIN CODE:</label>
							<input type="text" class="form-control" maxlength="50" name="txtAreaPin" id="txtAreaPin" placeholder="Area PIN CODE" >
						</div>                 
					</div>    

					<div class="container">
						<div class="col-sm-10 form-group"> 
							<label for="status">Country Name:</label>
							<select id="ddlCountry" name="ddlCountry" class="form-control">
							            
							</select>
						</div>                            
					</div>
					
					<div class="container">
						<div class="col-sm-10 form-group"> 
							<label for="status">State Name:</label>
							<select id="ddlState" name="ddlState" class="form-control">
							            
							</select>
						</div>                            
					</div>
					
					<div class="container">
						<div class="col-sm-10 form-group"> 
							<label for="status">Region Name:</label>
							<select id="ddlRegion" name="ddlRegion" class="form-control">
							            
							</select>
						</div>                            
					</div>
					
					<div class="container">
						<div class="col-sm-10 form-group"> 
							<label for="status">District Name:</label>
							<select id="ddlDistrict" name="ddlDistrict" class="form-control">
							            
							</select>
						</div>                            
					</div>
					
					<div class="container">
						<div class="col-sm-10 form-group"> 
							<label for="status">Tehsil Name:</label>
							<select id="ddlTehsil" name="ddlTehsil" class="form-control">
							            
							</select>
						</div>                            
					</div>
					
					<div class="container">
						<div class="col-sm-10 form-group"> 
							<label for="status">Area Status:</label>
							<select id="ddlStatus" name="ddlStatus" class="form-control">
							            
							</select>
						</div>                            
					</div>
					
					<div class="container">
                            <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit"/>    
                        </div>
						
							<div id="gird" style="margin-top:35px;"> </div>		
						
                </div>
            </div>   
        </div>
    </form>
<?php include ('footer.php'); ?>
<?php include'common/message.php';?>
</body>
 <style>
 .star{
	color:red;
}
</style> 
<script type="text/javascript">
        var SuccessfullyInsert = "<?php echo Message::SuccessfullyInsert ?>";
        var SuccessfullyFetch = "<?php echo Message::SuccessfullyFetch ?>";
        var SuccessfullyDelete = "<?php echo Message::SuccessfullyDelete ?>";
        var SuccessfullyUpdate = "<?php echo Message::SuccessfullyUpdate ?>";
        $(document).ready(function () {

            if (Mode == 'Delete')
            {
                if (confirm("Do You Want To Delete This Item ?"))
                {
                    deleteRecord();
                }
            }
            else if (Mode == 'Edit')
            {
                fillForm();
            }
            
            function FillStatus() {
                $.ajax({
                    type: "post",
                    url: "common/cfStatusMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlStatus").html(data);
                    }
                });
            }

            FillStatus();
            
            function FillParent() {
                $.ajax({
                    type: "post",
                    url: "common/cfCountryMaster.php",
                    data: "action=FILL",
                    success: function (data) {
                        $("#ddlCountry").html(data);
                    }
                });
            }

            FillParent();
            
            function deleteRecord()
            {
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                $.ajax({
                    type: "post",
                    url: "common/cfAreaMaster.php",
                    data: "action=DELETE&values=" + AreaCode + "",
                    success: function (data) {
                        //alert(data);
                        if (data == SuccessfullyDelete)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                                $('#response').empty();
                            }, 3000);
                            Mode="Add";
                            resetForm("frmAreaMaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();
                    }
                });
            }


            function fillForm()
            {
                $.ajax({
                    type: "post",
                    url: "common/cfAreaMaster.php",
                    data: "action=EDIT&values=" + AreaCode + "",
                    success: function (data) {
                        //alert($.parseJSON(data)[0]['Status']);
                        data = $.parseJSON(data);
                        txtAreaName.value = data[0].AreaName;
                        ddlTehsil.value=data[0].Tehsil;
                        ddlStatus.value = data[0].Status;
                        txtAreaPin.value = data[0].AreaPin;
                        
                    }
                });
            }

            function showData() {
                
                $.ajax({
                    type: "post",
                    url: "common/cfAreaMaster.php",
                    data: "action=SHOW",
                    success: function (data) {

                        $("#gird").html(data);

                    }
                });
            }

            showData();
            
            $("#ddlCountry").change(function(){
				var selcountry = $(this).val(); 
				//alert(selcountry);
				$.ajax({
			          url: 'common/cfStateMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selcountry + "",
			          success: function(data){
						//alert(data);
						$('#ddlState').html(data);
			          }
			        });
                            });
            
           
            $("#ddlState").change(function(){
				var selState = $(this).val(); 
				//alert(selState);
				$.ajax({
			          url: 'common/cfRegionMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selState + "",
			          success: function(data){
						//alert(data);
						$('#ddlRegion').html(data);
			          }
			        });
                             });
            $("#ddlRegion").change(function(){
				var selregion = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfDistrictMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selregion + "",
			          success: function(data){
						//alert(data);
						$('#ddlDistrict').html(data);
			          }
			        });
                            });
            $("#ddlDistrict").change(function(){
				var selDistrict = $(this).val(); 
				//alert(selregion);
				$.ajax({
			          url: 'common/cfTehsilMaster.php',
			          type: "post",
			          data: "action=FILL&values=" + selDistrict + "",
			          success: function(data){
						//alert(data);
						$('#ddlTehsil').html(data);
			          }
			        });
                            });

            $("#btnSubmit").click(function () {
			if ($("#frmArealMaster").valid())
			{	
					
                $('#response').empty();
                $('#response').append("<p class='error'><span><img src=images/ajax-loader.gif width=10px /></span><span>Processing.....</span></p>");
                var url = "common/cfAreaMaster.php"; // the script where you handle the form input.
                var data;
                if (Mode == 'Add')
                {
                    data = "action=ADD&name=" + txtAreaName.value + "&pincode=" + txtAreaPin.value + "&parent=" + ddlCountry.value + "&state=" + ddlState.value + "&region=" + ddlRegion.value + "&district=" + ddlDistrict.value + "&tehsil=" + ddlTehsil.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                else
                {
                    data = "action=UPDATE&code=" + AreaCode + "&name=" + txtAreaName.value + "&pincode=" + txtAreaPin.value + "&parent=" + ddlCountry.value + "&state=" + ddlState.value + "&region=" + ddlRegion.value + "&district=" + ddlDistrict.value + "&tehsil=" + ddlTehsil.value + "&status=" + ddlStatus.value + ""; // serializes the form's elements.
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function (data)
                    {
                        if (data == SuccessfullyInsert || data == SuccessfullyUpdate)
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/correct.gif width=10px /></span><span>" + data + "</span></p>");
                            window.setTimeout(function () {
                               window.location.href="frmAreaMaster.php";
                           }, 1000);

                            Mode="Add";
                            resetForm("frmAreaMaster");
                        }
                        else
                        {
                            $('#response').empty();
                            $('#response').append("<p class='error'><span><img src=images/error.gif width=10px /></span><span>" + data + "</span></p>");
                        }
                        showData();


                    }
                });
			}
                return false; // avoid to execute the actual submit of the form.
            });
            function resetForm(formid) {
                $(':input', '#' + formid).not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
            }

        });

    </script>
	<script src="rkcltheme/js/jquery.validate.min.js"></script>
		<script src="bootcss/js/frmareamastervalidation.js"></script>
</html>
